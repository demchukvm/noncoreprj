package com.tmx.web.actions.core.balance.operator_balance;

import com.tmx.as.blogic.balance.AbstractBalanceManager;
import com.tmx.as.blogic.balance.OperatorBalanceManager;
import com.tmx.as.blogic.TransactionManager;
import com.tmx.as.entities.bill.balance.AbstractBalance;
import com.tmx.as.entities.bill.balance.OperatorBalance;
import com.tmx.as.entities.general.user.User;
import com.tmx.as.exceptions.DatabaseException;
import com.tmx.beng.base.BillException;
import com.tmx.util.InitException;
import com.tmx.web.actions.core.balance.AbstractInfoBalanceAction;
import com.tmx.web.controls.table.Parameter;
import com.tmx.web.forms.core.balance.AbstractInfoBalanceForm;
import com.tmx.web.forms.core.balance.operator_balance.InfoOperatorBalanceForm;


public class InfoOperatorBalanceAction extends AbstractInfoBalanceAction {

    protected String getManageAction() {
        return "manageOperatorBalancesAction";
    }

    protected String getInfoPage() {
        return "infoOperatorBalancePage";
    }

    protected String getManageDeleteAction() {
        return "manageDeleteAction";
    }

    protected String getInfoTransactionAction() {
        return "/core/transactions/operator_transaction/infoOperatorTransaction";
    }

    protected AbstractBalanceManager getBalanceManager() throws InitException, BillException {
        return new OperatorBalanceManager();
    }

    protected void preloadForUpdate(AbstractInfoBalanceForm balanceForm, AbstractBalance balance, User user) {
        InfoOperatorBalanceForm form = (InfoOperatorBalanceForm) balanceForm;
        OperatorBalance operBalance = (OperatorBalance) balance;
        form.setOperatorId(operBalance.getOperatorId());

        form.getTransactionsTable().addParameterToFilter("by_operator_balance_id", new Parameter("operator_balance_id", operBalance.getBalanceId()));
        form.getTransactionsTable().addParameterToFilter("by_operator", new Parameter("operator", operBalance.getOperatorId()));
    }

    protected void preloadForCreate(AbstractInfoBalanceForm form) {
        form.getTransactionsTable().addParameterToFilter("by_operator_balance_id", new Parameter("operator_balance_id", (long)0));
        form.getTransactionsTable().addParameterToFilter("by_operator", new Parameter("operator", (long)0));        
    }

    protected void preload(AbstractInfoBalanceForm form, AbstractBalance balance) throws Throwable {
        OperatorBalance operatorBalance = (OperatorBalance)balance;

        if(operatorBalance.getOperator()!=null)
            form.getScrollBoxBalanceOwner().setSelectedKey(operatorBalance.getOperator().getOperatorId().toString());
    }

    @Override
    protected TransactionManager.CashCalculation getCashCalculation(AbstractInfoBalanceForm form) throws DatabaseException {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

}
