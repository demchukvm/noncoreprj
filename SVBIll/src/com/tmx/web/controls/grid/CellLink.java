package com.tmx.web.controls.grid;

public class CellLink implements Cell {
    private Cell cell;

    public CellLink(Cell cell) {
        this.cell = cell;
    }

    public int getColSpan() {
        return cell.getColSpan();
    }

    public int getRowSpan() {
        return cell.getRowSpan();
    }

    public String getValue() {
        return cell.getValue();
    }

    public String getStyleClass() {
        return cell.getStyleClass();
    }

    public String getStyle(){
        return cell.getStyle();
    }
}
