package com.tmx.beng.medaccess.test;

import com.tmx.beng.medaccess.MediumAccess;
import com.tmx.beng.medaccess.MediumConnection;
import com.tmx.beng.medaccess.GateException;

/**

 */
public class TestGateMediumAccess implements MediumAccess {

    public void init() {
    }

    public MediumConnection getConnection() throws GateException {
        return new TestGateMediumConnection();
    }
}
