package com.tmx.as.entities.bill.restriction;

import com.tmx.as.entities.bill.operator.Operator;

/**
 * 
 */
public class OperatorRestriction extends AbstractRestriction{
    private Operator operator;
    private Long operatorId;

    //------------Properties
    public Operator getOperator() {
        return operator;
    }

    public void setOperator(Operator operator) {
        this.operator = operator;
    }

    public Long getOperatorId() {
        return operatorId;
    }

    public void setOperatorId(Long operatorId) {
        this.operatorId = operatorId;
    }
}
