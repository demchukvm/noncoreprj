package com.tmx.as.entities.mobipay.region;

import com.tmx.as.base.BasicEntity;

import java.io.Serializable;

/**
 */
public class Region extends BasicEntity {
    private Long regionId;
    private String name;
    private String beelineRegionCode;
    private String kyivstarRegionCode;


    public String getKyivstarRegionCode() {
        return kyivstarRegionCode;
    }

    public void setKyivstarRegionCode(String kyivstarRegionCode) {
        this.kyivstarRegionCode = kyivstarRegionCode;
    }

    public Long getRegionId() {
        return regionId;
    }

    public void setRegionId(Long regionId) {
        this.regionId = regionId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBeelineRegionCode() {
        return beelineRegionCode;
    }

    public void setBeelineRegionCode(String beelineRegionCode) {
        this.beelineRegionCode = beelineRegionCode;
    }

    public Serializable getId() {
        return getRegionId();
    }

    public int getValid() {
        return valid;
    }

    public void setValid(int valid) {
        this.valid = valid;
    }
}
