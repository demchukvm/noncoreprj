package com.tmx.beng.iso8583Gate.pdu.substructure;

import com.tmx.beng.iso8583Gate.pdu.RuntimeDataElement;
import com.tmx.beng.iso8583Gate.pdu.substructure.tagged.RuntimeSimpleTaggedSubDE;

/**

 */
public abstract class BasicSubDEFormat implements SubDataElementFormat {

    public abstract RuntimeSubDataElement getRuntimeSubDataElementInstance();


    public final RuntimeSubDataElement prepare(RuntimeDataElement rde) {
        RuntimeSubDataElement runtimeSDE = getRuntimeSubDataElementInstance();
        rde.setValue(runtimeSDE);
        runtimeSDE.setOwnerRDE(rde);
        return runtimeSDE;
    }
}
