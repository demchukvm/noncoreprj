package com.tmx.web.actions.core.restriction;

import com.tmx.web.actions.core.AbstractSvbillAction;
import com.tmx.web.forms.core.restriction.AbstractManageRestrictionsForm;
import com.tmx.as.entities.general.user.User;
import com.tmx.web.controls.ScrollBox;
import com.tmx.web.base.WebException;
import com.tmx.as.blogic.restriction.AbstractRestrictionManager;
import com.tmx.as.blogic.restriction.SellerRestrictionManager;
import com.tmx.as.entities.bill.restriction.AbstractRestriction;
import com.tmx.as.exceptions.DatabaseException;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


abstract public class AbstractManageRestrictionsAction extends AbstractSvbillAction {
    public static final String DEFAULT_VALID_ACTION = "/core/restriction/seller_restriction/infoSellerRestriction";
    public static final String PARAMETER_OWNER_NAME = "ownerId";
    private static final String FORWARD_MANAGE_RESTRICTIONS_PAGE = "manageRestrictionsPage";

    protected AbstractRestrictionManager getRestrictionManager() {
        return new SellerRestrictionManager();
    }

    public ActionForward defaultAction(ActionMapping mapping,
                                       ActionForm form,
                                       HttpServletRequest request,
                                       HttpServletResponse response,
                                       String methodName) throws Throwable {
        AbstractManageRestrictionsForm restrictionsForm = (AbstractManageRestrictionsForm) form;
        restrictionsForm.getRestrictionsTable().refresh();
        if(restrictionsForm.getFormName().equals("core.restriction.seller_restriction.manageSellerRestrictionsForm")){
            changeButtonsVisibility(restrictionsForm);
        }
        return mapping.findForward(FORWARD_MANAGE_RESTRICTIONS_PAGE);
    }

    public ActionForward refreshRestrictionsTable(ActionMapping mapping,
                                                  ActionForm form,
                                                  HttpServletRequest request,
                                                  HttpServletResponse response) throws Throwable {
        AbstractManageRestrictionsForm restrictionsForm = (AbstractManageRestrictionsForm) form;
        restrictionsForm.getRestrictionsTable().refresh();
        if(restrictionsForm.getFormName().equals("core.restriction.seller_restriction.ManageSellerRestrictionsForm")){
            changeButtonsVisibility(restrictionsForm);
        }

        return mapping.findForward("manageRestrictionsPage");
    }

    abstract public ActionForward onCreateRestrictionAction(ActionMapping mapping,
                                                            ActionForm form,
                                                            HttpServletRequest request,
                                                            HttpServletResponse response) throws Throwable;


    
    private void changeButtonsVisibility(AbstractManageRestrictionsForm restrictionsForm) {
        ScrollBox scrollBoxSubSellers = (ScrollBox) restrictionsForm.getRestrictionsTable().getParameterControl("by_seller_id", "seller_id");

        if (scrollBoxSubSellers.isNullKeySelected() || restrictionsForm.getRestrictionsTable().getData().isEmpty()) {
            restrictionsForm.getOnCopyButton().setReadonly(true);
            restrictionsForm.getOnCopyAllButton().setReadonly(true);
        } else {
            restrictionsForm.getOnCopyButton().setReadonly(false);
            restrictionsForm.getOnCopyAllButton().setReadonly(false);
        }

        if (restrictionsForm.getRestrictionsTable().getData().isEmpty()) {
            restrictionsForm.getRestrictionsTable().setReadonly(true);
            restrictionsForm.getRestrictionsTable().setReadonly(true);
        } else {
            restrictionsForm.getOnDeleteIRButton().setReadonly(false);
            restrictionsForm.getOnEditIRButton().setReadonly(false);
        }
    }

}
