package com.tmx.web.actions.core.balance.terminal_group_balance;

import com.tmx.web.actions.core.base.AbstractBrowseAction;


public class TerminalGroupBrowseAction extends AbstractBrowseAction {

    protected String getForwardName() {
        return "browseTermianlGroupsPage";  
    }
}
