package com.tmx.as.entities.bill.tariff;

import com.tmx.as.entities.bill.voucher.VoucherType;

/**

 */
public class TerminalVoucherTariff extends AbstractTariff{
    private VoucherType voucherType;

    public VoucherType getVoucherType() {
        return voucherType;
    }

    public void setVoucherType(VoucherType voucherType) {
        this.voucherType = voucherType;
    }
}
