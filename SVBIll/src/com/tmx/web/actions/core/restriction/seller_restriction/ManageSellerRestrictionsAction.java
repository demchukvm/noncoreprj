package com.tmx.web.actions.core.restriction.seller_restriction;

import com.tmx.web.actions.core.restriction.AbstractManageRestrictionsAction;
import com.tmx.web.base.Roadmap;
import com.tmx.web.forms.core.restriction.seller_restriction.ManageSellerRestrictionsForm;
import com.tmx.web.forms.core.base.AbstractFormFactory;
import com.tmx.web.controls.ScrollBox;
import com.tmx.as.blogic.SellerManager;
import com.tmx.as.blogic.restriction.SellerRestrictionManager;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForm;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class ManageSellerRestrictionsAction extends AbstractManageRestrictionsAction {

    public ActionForward preloadForDelete(ActionMapping mapping,
                                          ActionForm form,
                                          HttpServletRequest request,
                                          HttpServletResponse response) throws Throwable {
        Roadmap roadmap = getRoadmap();
        roadmap.collectParameter("/core/message/confirmation", "leftMenuName", "restricions");
        roadmap.setPageForward("confirmation", "onOk", "local:" + this.getName() + "#" + "deleteRestrictioAction");
        roadmap.setPageForward("confirmation", "onCancel", "local:" + this.getName() + "#" + "manageRestrictionsAction");
        return mapping.findForward("confirmation");
    }


    public ActionForward preloadForDeleteIR(ActionMapping mapping,
                                          ActionForm form,
                                          HttpServletRequest request,
                                          HttpServletResponse response) throws Throwable {
        Roadmap roadmap=getRoadmap();
        roadmap.collectParameter("/core/message/confirmation", "leftMenuName", "restrictions");
        roadmap.setPageForward("confirmation","onOk","local:"+this.getName()+"#"+"deleteIncomingRestrictionsAction");
        roadmap.setPageForward("confirmation","onCancel","local:"+this.getName()+"#"+ "manageRestrictionsAction");
        return mapping.findForward("confirmation");
    }


    public ActionForward deleteIncomingRestriction(ActionMapping mapping,
                              ActionForm form,
                              HttpServletRequest request,
                              HttpServletResponse response) throws Throwable {
        ManageSellerRestrictionsForm restrictionsForm = (ManageSellerRestrictionsForm) form;
        getRestrictionManager().deleteRestriction(restrictionsForm.getIncomingRestrictionId());
        return defaultAction(mapping, form, request, response, null);
    }

    public ActionForward delete(ActionMapping mapping,
                              ActionForm form,
                              HttpServletRequest request,
                              HttpServletResponse response) throws Throwable {
        ManageSellerRestrictionsForm restrictionsForm = (ManageSellerRestrictionsForm) form;
        return defaultAction(mapping, form, request, response, null);
    }

    public ActionForward refreshIncomingRestrictionsTable(ActionMapping mapping,
                              ActionForm form,
                              HttpServletRequest request,
                              HttpServletResponse response) throws Throwable {
        ManageSellerRestrictionsForm restrictionsForm = (ManageSellerRestrictionsForm) form;
        if (!restrictionsForm.getSellersScrollBox().isNullKeySelected()) {
            restrictionsForm.bindSubSellers(new SellerManager().getNestedSellersInSeller(restrictionsForm.getSellersScrollBox().getSelectedKey(),
                    false));
            restrictionsForm.getSubSellersScrollBox().setSelectedKey(ScrollBox.NULL_KEY);
            restrictionsForm.bindRestrictionNames(new SellerRestrictionManager().getAllRestrictions(restrictionsForm.getSelectedSellerIdIncomingTable()));
        }
        restrictionsForm.getIncomingRestrictionsTable().refresh();
        restrictionsForm.getRestrictionsTable().refresh();
        return mapping.findForward("manageRestrictionsPage");
    }

    public ActionForward defaultAction(ActionMapping mapping, ActionForm form, HttpServletRequest request,
                                       HttpServletResponse response, String methodName) throws Throwable {
        ManageSellerRestrictionsForm restrictionsForm = (ManageSellerRestrictionsForm) form;
        AbstractFormFactory.getInstance(getLoggedInUser(request)).setUpForm(restrictionsForm);
        restrictionsForm.getSubSellersScrollBox().setSelectedKey(ScrollBox.NULL_KEY);
        restrictionsForm.getRestrScrollBox().setSelectedKey(ScrollBox.NULL_KEY);
        restrictionsForm.bindRestrictionNames(new SellerRestrictionManager().getAllRestrictions(restrictionsForm.getSelectedSellerIdIncomingTable()));
        restrictionsForm.bindSubSellers(new SellerManager().getNestedSellersInSeller(restrictionsForm.getSelectedSellerIdIncomingTable(),
                false));
//--------------------------------------restrictionsForm.getSellersScrollBox() is always null - that is why next cod block is commented and moved to another method (refreshIncomingRestrictionsTable)
//        restrictionsForm.bindSellers(new SellerManager().getAllSellers());to form factory
//        restrictionsForm.bindSubSellers(new SellerManager().getNestedSellersInSeller(restrictionsForm.getSellersScrollBox().getSelectedKey(),
//                false));
//{!!!!}
//        getRoadmap().collectParameter("/core/restriction/seller_restriction/infoSellerRestriction", "onCancelAction", "/core/restriction/seller_restriction/manageSellerRestrictions");
        restrictionsForm.getIncomingRestrictionsTable().refresh();
//        AbstractFormFactory.getInstance(getLoggedInUser(request)).setUpFuncTypes(restrictionsForm);
        return super.defaultAction(mapping, form, request, response, methodName);
    }

    public ActionForward preloadForCopy(ActionMapping mapping,
                                       ActionForm form,
                                       HttpServletRequest request,
                                       HttpServletResponse response) throws Throwable {
        ManageSellerRestrictionsForm restrictionsForm = (ManageSellerRestrictionsForm) verifyForm(form, ManageSellerRestrictionsForm.class);
        restrictionsForm.copySelectedResrtiction();
        return defaultAction(mapping, form, request, response, null);
    }

    public ActionForward preloadForCopyAll(ActionMapping mapping,
                                       ActionForm form,
                                       HttpServletRequest request,
                                       HttpServletResponse response) throws Throwable {
        ManageSellerRestrictionsForm restrictionsForm = (ManageSellerRestrictionsForm) verifyForm(form, ManageSellerRestrictionsForm.class);
        restrictionsForm.copyAllRestrictions(getLoggedInUser(request));
        return defaultAction(mapping, form, request, response, null);
    }

    public ActionForward onCreateRestrictionAction(ActionMapping mapping,
                                                   ActionForm form,
                                                   HttpServletRequest request,
                                                   HttpServletResponse response) throws Throwable {
        ManageSellerRestrictionsForm manageSellerForm = (ManageSellerRestrictionsForm) form;
        String selectedKey = ((ScrollBox) manageSellerForm.getRestrictionsTable().getParameterControl("by_seller_id", "seller_id")).getSelectedKey();
        getRoadmap().collectParameter("/core/restriction/seller_restriction/infoSellerRestriction", PARAMETER_OWNER_NAME, selectedKey);
        return mapping.findForward("createRestrictionAction");
    }

    public ActionForward onCreateIncomingRestrictionAction(ActionMapping mapping,
                                       ActionForm form,
                                       HttpServletRequest request,
                                       HttpServletResponse response) throws Throwable {
        ManageSellerRestrictionsForm restrictionsForm = (ManageSellerRestrictionsForm) form;
        String selectedKey = ((ScrollBox) restrictionsForm.getIncomingRestrictionsTable().getParameterControl("by_seller_id", "seller_id")).getSelectedKey();
        getRoadmap().collectParameter("/core/restriction/seller_restriction/infoSellerRestriction", PARAMETER_OWNER_NAME, selectedKey);
        return mapping.findForward("createRestrictionAction");
    }

}
