package com.tmx.beng.httpsgate.avancel;

import com.tmx.as.base.EntityManager;
import com.tmx.as.base.criterion.CriterionWrapper;
import com.tmx.as.base.criterion.Restrictions;
import com.tmx.as.entities.bill.balance.GatesBalance;
import com.tmx.as.exceptions.DatabaseException;
import org.apache.log4j.Logger;

import com.tmx.util.HexConverter;
import com.tmx.beng.base.StatusDictionary;
import com.tmx.beng.httpsgate.BadResponseException;

/**
 */
public class AvancelResponse {
    private static Logger logger = Logger.getLogger("avancel_gate." + AvancelResponse.class);
    /** Original one-byte status issued by Avancel server. Represented in our system like string (actually redundant type here) to prevent cross-DB problems. */
    private String statusCodeByteByAvancel;
    /** Status message by Avancel */
    private String statusMsgByAvancel;
    /** Operation status code evaluated to pass into our billing. */
    private Integer statusCodeForBilling;
    /** Operation status message to pass into our billing */
    private String statusMsgForBilling;
    /** Server acknowledge GUID received in response on authorization operation. */
    private String serverGuidAcknowledge;
    /** Unique identifier of transaction issued by Avancel server. */
    private String avancelTransactionId;
    /** Service name. Human-readable string from Avancel. */
    private String serviceName;
    /** Voucher serial number. */
    private String voucherSerialNumber;
    /** Voucher secret code. */
    private String voucherSecretCode;
    /** Voucher expriration date. */
    private String voucherExpirationDate;
    private String instruction;
    private String salesPointInfo;


    /** Takes given string of Avancel protocol, parses it and populates AvancelResponse object.
     * @param responseData raw data received from Avancel server to be parsed.
     * @param operation operation during which given responseData string was received.
     * @return populated from raw string AvancelResponse object.
     * @throws com.tmx.beng.httpsgate.BadResponseException bad response because of parsing error. */
    public static AvancelResponse parseResponse(byte[] responseData, BasicAvancelGate.Operation operation) throws BadResponseException {
        switch (operation){
            case AUTHORIZATION:
                return parseAuthorizationResponse(responseData);
            case REFILL:
                return parseRefillResponse(responseData);
            case GETSTATUS:
                return parseGetStatus(responseData);
            case GETLASTRESPONSE:
                return parseGetLastResponse(responseData);
            case GETVOUCHER:
                return parseGetVoucherResponse(responseData);
            case GEN_VOUCHER_SEND:
                return parseGenVoucherSendResponse(responseData);
            case GEN_VOUCHER_STAT:
                return parseGenVoucherStatResponse(responseData);
        }
        throw new BadResponseException("Given operation is unsupported by parsing method: " + operation); 
    }

    private static AvancelResponse parseAuthorizationResponse(byte[] responseDataBytes) throws BadResponseException {
        AvancelResponse avancelResponse = new AvancelResponse();
        HexConverter hexConverter = new HexConverter();
        avancelResponse.setStatusCodeByteByAvancel(hexConverter.getStringFromHex(new byte[]{responseDataBytes[0]}));
        avancelResponse.setStatusCodeForBilling(StatusDictionary.STATUS_OK);
        final byte okStatus = 0x66;
        if(responseDataBytes[0] != okStatus){
            final String msg = "Incorrect status byte in response from server: 0x"+ hexConverter.getStringFromHex(new byte[]{responseDataBytes[0]}) + ", should be 0x" + okStatus + ", operation: " + BasicAvancelGate.Operation.AUTHORIZATION;
            avancelResponse.setStatusCodeForBilling(StatusDictionary.AVANCEL_AUTHORIZATION_ERROR);
            avancelResponse.setStatusMsgForBilling(msg);
            logger.error(msg);
            return avancelResponse;
        }
        //cut of first byte
        final String responseData = new String(responseDataBytes, 1, responseDataBytes.length-1, BasicAvancelGate.charset);

        //parse response
        String[] lexems = responseData.substring(0, responseData.length()-1).split("\\&");
        for(int i = 0; i<lexems.length; i++){
            if(i == 0){
                //fetch acknowledge GUID
                avancelResponse.setServerGuidAcknowledge(lexems[0].replaceAll("\\{","").replaceAll("\\}",""));
                logger.debug("Server Acknosledge GUID: " + avancelResponse.getServerGuidAcknowledge());
                continue;
            }
            final String article = lexems[i].substring(0, lexems[i].indexOf(","));
            final String pincode = lexems[i].substring(article.length()+1, lexems[i].indexOf("="));
            final String nominals = lexems[i].substring(lexems[i].indexOf("=")+1);  

            //  we only dump into log this information. Any further processing. 
            {
                logger.debug("---Avancel Remains---");
                logger.debug("Article: " + article);
                logger.debug("Pincode: " + pincode);
                logger.debug("Nominals: " + nominals);
            }
        }

        return avancelResponse;
    }

    private static AvancelResponse parseRefillResponse(byte[] responseDataBytes) throws BadResponseException {
        AvancelResponse avancelResponse = new AvancelResponse();
        HexConverter hexConverter = new HexConverter();
        avancelResponse.setStatusCodeByteByAvancel(hexConverter.getStringFromHex(new byte[]{responseDataBytes[0]}));
        avancelResponse.setStatusCodeForBilling(StatusDictionary.AVANCEL_UNCONFIRMED_OK);
        String responseData = new String(responseDataBytes).trim();
        final byte okStatus = 0x06;
        final byte businessErrorStatus = 0x30;
        if(responseDataBytes[0] != okStatus){
            String msg;
            int statusCode;
            if(responseDataBytes[0] == businessErrorStatus){
                if(responseData.indexOf("sid=-3440") >= 0){
                    msg = "There is no enough money to complete transation";
                    statusCode = StatusDictionary.AVANCEL_NO_ENOUGH_MONEY;
                }
                else if(responseData.indexOf("sid=-3410") >= 0){
                    msg = "There is no prices on requested article";
                    statusCode = StatusDictionary.AVANCEL_NO_PRICES;
                }
                else if(responseData.indexOf("t_st_i bpr>spr") >= 0){
                    msg = "Incorrected prices on requested article";
                    statusCode = StatusDictionary.AVANCEL_INCORRECT_PRICES;
                }
                else if(responseData.indexOf("sid=-3320") >= 0){
                    msg = "There is no requested article";
                    statusCode = StatusDictionary.AVANCEL_NO_REQUESTED_ARTICLE;
                }
                else if(responseData.indexOf("@amount not in [min,max]") >= 0){
                    msg = "Refill amount is incorrect";
                    statusCode = StatusDictionary.AVANCEL_INCORRECT_REFILL_AMOUNT;
                }
                else if(responseData.indexOf("service unaviable") >= 0){
                    msg = "Service temporary unavailable";
                    statusCode = StatusDictionary.AVANCEL_SERVICE_UNAVAILABLE;
                }
                else if(responseData.indexOf("not in Cells") >= 0){
                    msg = "Incorrect msisdn or account number";
                    statusCode = StatusDictionary.AVANCEL_SERVICE_UNAVAILABLE;
                }
                else{
                    msg = "Incorrect status byte in response from server: 0x"+ hexConverter.getStringFromHex(new byte[]{responseDataBytes[0]}) + ", should be 0x" + hexConverter.getStringFromHex(new byte[]{okStatus}) + ", operation: " + BasicAvancelGate.Operation.REFILL;
                    statusCode = StatusDictionary.AVANCEL_REFILL_ERROR;
                }
            }
            else {
                msg = "Incorrect status byte in response from server: 0x"+ hexConverter.getStringFromHex(new byte[]{responseDataBytes[0]}) + ", should be 0x" + hexConverter.getStringFromHex(new byte[]{okStatus}) + ", operation: " + BasicAvancelGate.Operation.REFILL;
                statusCode = StatusDictionary.AVANCEL_REFILL_ERROR; 
            }
            avancelResponse.setStatusMsgForBilling(msg);
            avancelResponse.setStatusCodeForBilling(statusCode);
            avancelResponse.setStatusMsgByAvancel(msg + "; response from Avancel: " + responseData);
            logger.error(msg + "; response from Avancel: " + responseData);
            return avancelResponse;//do not parse further
        }
        //cut of first byte
        responseData = new String(responseDataBytes, 1, responseDataBytes.length-1, BasicAvancelGate.charset);

        //parse response
        String[] lexems = responseData.split("\\&");
        avancelResponse.setAvancelTransactionId(lexems[0]);
        avancelResponse.setServiceName(lexems[1]);
        avancelResponse.setVoucherSerialNumber(lexems[2]);
        avancelResponse.setVoucherSecretCode(lexems[3]);
        avancelResponse.setVoucherExpirationDate(lexems[4]);
        avancelResponse.setInstruction(lexems[5]);
        avancelResponse.setSalesPointInfo(lexems[6]);

        changeAvancelGateBalance(lexems[8]);

        return avancelResponse;
    }

    private static void changeAvancelGateBalance(String respBalance)
    {
        CriterionWrapper criterions[] = new CriterionWrapper[] {
                Restrictions.eq("gateName", "Avancel")};
        EntityManager entityManager = new EntityManager();
        GatesBalance gatesBalance = new GatesBalance();

        try {
            gatesBalance = (GatesBalance) entityManager.RETRIEVE(GatesBalance.class, null, criterions, null);
        } catch (DatabaseException e) {
            logger.error("Failed to get Avancel balance from GB_GATEBALANCE", e);
        } catch (Exception ee) {
            logger.error("Failed to get Avancel balance from GB_GATEBALANCE", ee);
        }

        
        gatesBalance.setBalance(respBalance);

        try {
            entityManager.SAVE(gatesBalance);
        } catch (DatabaseException e) {
            logger.error("Failed to change Avancel balance in GB_GATEBALANCE", e);
        }

    }

    private static AvancelResponse parseGetVoucherResponse(byte[] responseDataBytes) throws BadResponseException
    {
        AvancelResponse avancelResponse = new AvancelResponse();
        HexConverter hexConverter = new HexConverter();
        avancelResponse.setStatusCodeByteByAvancel(hexConverter.getStringFromHex(new byte[]{responseDataBytes[0]}));
        avancelResponse.setStatusCodeForBilling(StatusDictionary.STATUS_OK);
        final byte okStatus = 0x13;
        if(responseDataBytes[0] != okStatus){
            final String msg = "Incorrect status byte in response from server: 0x"+ hexConverter.getStringFromHex(new byte[]{responseDataBytes[0]}) + ", should be 0x" + hexConverter.getStringFromHex(new byte[]{okStatus}) + ", operation: " + BasicAvancelGate.Operation.GETSTATUS;
            avancelResponse.setStatusCodeForBilling(StatusDictionary.AVANCEL_GETSTATUS_ERROR);
            avancelResponse.setStatusMsgForBilling(msg);
            logger.error(msg);
            return avancelResponse;//do not parse further
        }
        //cut of first byte
        String responseData = new String(responseDataBytes, 1, responseDataBytes.length-1, BasicAvancelGate.charset);

        //parse response
        String[] lexems = responseData.split("\\&");
        try{
            final int transactionProgressStatus = Integer.parseInt(lexems[0]);
            switch (transactionProgressStatus) {
                case 0:{
                    avancelResponse.setStatusCodeForBilling(StatusDictionary.AVANCEL_OPERATOR_FAILED_TRANSACTION);
                    avancelResponse.setStatusMsgForBilling("Transaction was failed in inside operator billing: Msg:" + (lexems.length > 1 ? lexems[1].trim() : ""));
                    break;
                }
                case 1:{
                    avancelResponse.setStatusCodeForBilling(StatusDictionary.STATUS_OK);
                    //don't overwrite old status message. if it was, then it can be used in future for investigation of problem cases.
                    break;
                }
                case 2:{
                    avancelResponse.setStatusCodeForBilling(StatusDictionary.AVANCEL_TRANSACTION_PROCESSED_BY_PARTNER);
                    avancelResponse.setStatusMsgForBilling("Transaction still in queue");
                    break;
                }
            }

            avancelResponse.setVoucherSecretCode(lexems[1]);
            avancelResponse.setVoucherSerialNumber(lexems[2]);
            avancelResponse.setVoucherExpirationDate(lexems[3].replace(" ", "T"));
        }
        catch (Exception e){
            //avancelResponse.setStatusCodeForBilling(StatusDictionary.AVANCEL_GETSTATUS_ERROR);
            avancelResponse.setStatusMsgForBilling("Failed to evaluate received from Avancel status for addressed transaction. AvancelStatus: " + lexems[0]);
        }

        return avancelResponse;
    }

    private static AvancelResponse parseGetStatus(byte[] responseDataBytes) throws BadResponseException {
        AvancelResponse avancelResponse = new AvancelResponse();
        HexConverter hexConverter = new HexConverter();
        avancelResponse.setStatusCodeByteByAvancel(hexConverter.getStringFromHex(new byte[]{responseDataBytes[0]}));
        avancelResponse.setStatusCodeForBilling(StatusDictionary.STATUS_OK);
        final byte okStatus = 0x13;
        if(responseDataBytes[0] != okStatus){
            final String msg = "Incorrect status byte in response from server: 0x"+ hexConverter.getStringFromHex(new byte[]{responseDataBytes[0]}) + ", should be 0x" + hexConverter.getStringFromHex(new byte[]{okStatus}) + ", operation: " + BasicAvancelGate.Operation.GETSTATUS;
            avancelResponse.setStatusCodeForBilling(StatusDictionary.AVANCEL_GETSTATUS_ERROR);
            avancelResponse.setStatusMsgForBilling(msg);
            logger.error(msg);
            return avancelResponse;//do not parse further
        }
        //cut of first byte
        String responseData = new String(responseDataBytes, 1, responseDataBytes.length-1, BasicAvancelGate.charset);

        //parse response
        String[] lexems = responseData.split("\\&");
        try{
            final int transactionProgressStatus = Integer.parseInt(lexems[0]);
            switch (transactionProgressStatus) {
                case 0:{
                    avancelResponse.setStatusCodeForBilling(StatusDictionary.AVANCEL_OPERATOR_FAILED_TRANSACTION);
                    avancelResponse.setStatusMsgForBilling("Transaction was failed in inside operator billing: Msg:" + (lexems.length > 1 ? lexems[1].trim() : ""));
                    break;
                }
                case 1:{
                    avancelResponse.setStatusCodeForBilling(StatusDictionary.STATUS_OK);
                    //don't overwrite old status message. if it was, then it can be used in future for investigation of problem cases.
                    break;
                }
                case 2:{
                    avancelResponse.setStatusCodeForBilling(StatusDictionary.AVANCEL_TRANSACTION_PROCESSED_BY_PARTNER);
                    avancelResponse.setStatusMsgForBilling("Transaction still in queue");
                    break;
                }
            }
        }
        catch (Exception e){
            //avancelResponse.setStatusCodeForBilling(StatusDictionary.AVANCEL_GETSTATUS_ERROR);
            avancelResponse.setStatusMsgForBilling("Failed to evaluate received from Avancel status for addressed transaction. AvancelStatus: " + lexems[0]);
        }

        return avancelResponse;
    }

    private static AvancelResponse parseGetLastResponse(byte[] responseDataBytes) throws BadResponseException {
        AvancelResponse avancelResponse = new AvancelResponse();

        HexConverter hexConverter = new HexConverter();
        avancelResponse.setStatusCodeByteByAvancel(hexConverter.getStringFromHex(new byte[]{responseDataBytes[0]}));
        avancelResponse.setStatusCodeForBilling(StatusDictionary.STATUS_OK);
        final byte okStatus = 0x27;
        if(responseDataBytes[0] != okStatus){
            final String msg = "Incorrect status byte in response from server: 0x"+ hexConverter.getStringFromHex(new byte[]{responseDataBytes[0]}) + ", should be 0x" + hexConverter.getStringFromHex(new byte[]{okStatus}) + ", operation: " + BasicAvancelGate.Operation.GETLASTRESPONSE;
            //avancelResponse.setStatusCodeForBilling(StatusDictionary.AVANCEL_GETLASTRESPONSE_ERROR);
            avancelResponse.setStatusMsgForBilling(msg);
            logger.error(msg);
            return avancelResponse;
        }
        //cut of first byte
        String responseData = new String(responseDataBytes, 1, responseDataBytes.length-1, BasicAvancelGate.charset);

        //parse response
        String[] lexems = responseData.split("\\&");
        avancelResponse.setAvancelTransactionId(lexems[0]);
        avancelResponse.setServiceName(lexems[1]);
        avancelResponse.setVoucherSerialNumber(lexems[2]);
        avancelResponse.setVoucherSecretCode(lexems[3]);
        avancelResponse.setVoucherExpirationDate(lexems[4]);
        avancelResponse.setInstruction(lexems[5]);
        avancelResponse.setSalesPointInfo(lexems[6]);

        return avancelResponse;
    }

    private static AvancelResponse parseGenVoucherSendResponse(byte[] responseDataBytes) throws BadResponseException
    {
        AvancelResponse avancelResponse = new AvancelResponse();
        HexConverter hexConverter = new HexConverter();
        avancelResponse.setStatusCodeByteByAvancel(hexConverter.getStringFromHex(new byte[]{responseDataBytes[0]}));
        avancelResponse.setStatusCodeForBilling(StatusDictionary.AVANCEL_UNCONFIRMED_OK);
        String responseData = new String(responseDataBytes).trim();
        final byte okStatus = 0x06;
        final byte businessErrorStatus = 0x30;
        if(responseDataBytes[0] != okStatus)
        {
            String msg;
            int statusCode;
            if(responseDataBytes[0] == businessErrorStatus)
            {
                if(responseData.indexOf("sid=-3440") >= 0)
                {
                    msg = "There is no enough money to complete transation";
                    statusCode = StatusDictionary.AVANCEL_NO_ENOUGH_MONEY;
                }
                else if(responseData.indexOf("sid=-3410") >= 0)
                {
                    msg = "There is no prices on requested article";
                    statusCode = StatusDictionary.AVANCEL_NO_PRICES;
                }
                else if(responseData.indexOf("t_st_i bpr>spr") >= 0)
                {
                    msg = "Incorrected prices on requested article";
                    statusCode = StatusDictionary.AVANCEL_INCORRECT_PRICES;
                }
                else if(responseData.indexOf("sid=-3320") >= 0)
                {
                    msg = "There is no requested article";
                    statusCode = StatusDictionary.AVANCEL_NO_REQUESTED_ARTICLE;
                }
                else if(responseData.indexOf("@amount not in [min,max]") >= 0)
                {
                    msg = "Refill amount is incorrect";
                    statusCode = StatusDictionary.AVANCEL_INCORRECT_REFILL_AMOUNT;
                }
                else if(responseData.indexOf("service unaviable") >= 0)
                {
                    msg = "Service temporary unavailable";
                    statusCode = StatusDictionary.AVANCEL_SERVICE_UNAVAILABLE;
                }
                else if(responseData.indexOf("not in Cells") >= 0)
                {
                    msg = "Incorrect msisdn or account number";
                    statusCode = StatusDictionary.AVANCEL_SERVICE_UNAVAILABLE;
                }
                else
                {
                    msg = "Incorrect status byte in response from server: 0x"+ hexConverter.getStringFromHex(new byte[]{responseDataBytes[0]}) + ", should be 0x" + hexConverter.getStringFromHex(new byte[]{okStatus}) + ", operation: " + BasicAvancelGate.Operation.REFILL;
                    statusCode = StatusDictionary.AVANCEL_REFILL_ERROR;
                }
            }
            else
            {
                msg = "Incorrect status byte in response from server: 0x"+ hexConverter.getStringFromHex(new byte[]{responseDataBytes[0]}) + ", should be 0x" + hexConverter.getStringFromHex(new byte[]{okStatus}) + ", operation: " + BasicAvancelGate.Operation.REFILL;
                statusCode = StatusDictionary.AVANCEL_REFILL_ERROR;
            }
            avancelResponse.setStatusMsgForBilling(msg);
            avancelResponse.setStatusCodeForBilling(statusCode);
            avancelResponse.setStatusMsgByAvancel(msg + "; response from Avancel: " + responseData);
            logger.error(msg + "; response from Avancel: " + responseData);
            return avancelResponse;
        }

        responseData = new String(responseDataBytes, 1, responseDataBytes.length-1, BasicAvancelGate.charset);

        String[] lexems = responseData.split("\\&");
        avancelResponse.setAvancelTransactionId(lexems[0]);
        avancelResponse.setServiceName(lexems[1]);

        changeAvancelGateBalance(lexems[8]);

        return avancelResponse;
    }

    private static AvancelResponse parseGenVoucherStatResponse(byte[] responseDataBytes) throws BadResponseException
    {
        AvancelResponse avancelResponse = new AvancelResponse();
        HexConverter hexConverter = new HexConverter();
        avancelResponse.setStatusCodeByteByAvancel(hexConverter.getStringFromHex(new byte[]{responseDataBytes[0]}));
        avancelResponse.setStatusCodeForBilling(StatusDictionary.STATUS_OK);
        final byte okStatus = 0x13;
        if(responseDataBytes[0] != okStatus){
            final String msg = "Incorrect status byte in response from server: 0x"+ hexConverter.getStringFromHex(new byte[]{responseDataBytes[0]}) + ", should be 0x" + hexConverter.getStringFromHex(new byte[]{okStatus}) + ", operation: " + BasicAvancelGate.Operation.GETSTATUS;
            avancelResponse.setStatusCodeForBilling(StatusDictionary.AVANCEL_GETSTATUS_ERROR);
            avancelResponse.setStatusMsgForBilling(msg);
            logger.error(msg);
            return avancelResponse;
        }

        String responseData = new String(responseDataBytes, 1, responseDataBytes.length-1, BasicAvancelGate.charset);

        String[] lexems = responseData.split("\\&");
        try{
            final int transactionProgressStatus = Integer.parseInt(lexems[0]);
            switch (transactionProgressStatus) {
                case 0:{
                    avancelResponse.setStatusCodeForBilling(StatusDictionary.AVANCEL_OPERATOR_FAILED_TRANSACTION);
                    avancelResponse.setStatusMsgForBilling("Transaction was failed in inside operator billing: Msg:" + (lexems.length > 1 ? lexems[1].trim() : ""));
                    break;
                }
                case 1:{
                    avancelResponse.setStatusCodeForBilling(StatusDictionary.STATUS_OK);
                    break;
                }
                case 2:{
                    avancelResponse.setStatusCodeForBilling(StatusDictionary.AVANCEL_TRANSACTION_PROCESSED_BY_PARTNER);
                    avancelResponse.setStatusMsgForBilling("Transaction still in queue");
                    break;
                }
            }

            avancelResponse.setVoucherSecretCode(lexems[1]);
            avancelResponse.setVoucherSerialNumber(lexems[2]);
            avancelResponse.setVoucherExpirationDate(lexems[3].replace(" ", "T"));
        }
        catch (Exception e){
            avancelResponse.setStatusMsgForBilling("Failed to evaluate received from Avancel status for addressed transaction. AvancelStatus: " + lexems[0]);
        }

        return avancelResponse;
    }

    public static Logger getLogger() {
        return logger;
    }

    public static void setLogger(Logger logger) {
        AvancelResponse.logger = logger;
    }

    public String getServerGuidAcknowledge() {
        return serverGuidAcknowledge;
    }

    public void setServerGuidAcknowledge(String serverGuidAcknowledge) {
        this.serverGuidAcknowledge = serverGuidAcknowledge;
    }

    public Integer getStatusCodeForBilling() {
        return statusCodeForBilling;
    }

    public void setStatusCodeForBilling(Integer statusCodeForBilling) {
        this.statusCodeForBilling = statusCodeForBilling;
    }

    public String getStatusCodeByteByAvancel() {
        return statusCodeByteByAvancel;
    }

    public void setStatusCodeByteByAvancel(String statusCodeByteByAvancel) {
        this.statusCodeByteByAvancel = statusCodeByteByAvancel;
    }

    public String getStatusMsgByAvancel() {
        return statusMsgByAvancel;
    }

    public void setStatusMsgByAvancel(String statusMsgByAvancel) {
        this.statusMsgByAvancel = statusMsgByAvancel;
    }

    public String getStatusMsgForBilling() {
        return statusMsgForBilling;
    }

    public void setStatusMsgForBilling(String statusMsgForBilling) {
        this.statusMsgForBilling = statusMsgForBilling;
    }

    public String getAvancelTransactionId() {
        return avancelTransactionId;
    }

    public void setAvancelTransactionId(String avancelTransactionId) {
        this.avancelTransactionId = avancelTransactionId;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public String getVoucherSerialNumber() {
        return voucherSerialNumber;
    }

    public void setVoucherSerialNumber(String voucherSerialNumber) {
        this.voucherSerialNumber = voucherSerialNumber;
    }

    public String getVoucherSecretCode() {
        return voucherSecretCode;
    }

    public void setVoucherSecretCode(String voucherSecretCode) {
        this.voucherSecretCode = voucherSecretCode;
    }

    public String getVoucherExpirationDate() {
        return voucherExpirationDate;
    }

    public void setVoucherExpirationDate(String voucherExpirationDate) {
        this.voucherExpirationDate = voucherExpirationDate;
    }

    public String getInstruction() {
        return instruction;
    }

    public void setInstruction(String instruction) {
        this.instruction = instruction;
    }

    public String getSalesPointInfo() {
        return salesPointInfo;
    }

    public void setSalesPointInfo(String salesPointInfo) {
        this.salesPointInfo = salesPointInfo;
    }

}
