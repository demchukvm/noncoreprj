<%@ page import="com.tmx.web.forms.core.tools.CyberplatTransactionInfoForm" %>
<%@ taglib prefix="nested" uri="http://jakarta.apache.org/struts/tags-nested" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/tld/tmx-controls.tld" prefix="tmx_ctrl" %>

<table>
    <tr>
        <td colspan="3">
            <tmx_ctrl:editBox property="ebBillNumber" />
            <tmx_ctrl:selectBox property="sbProduct" />
            <tmx_ctrl:button property="bOk" />
        </td>
    </tr>
    <tr>
        <td>
            <table border="1">
                <tr>
                    <td>Биллинговый номер</td>
                    <td></td>
                </tr>
                <tr>
                    <td>Клиентский номер</td>
                    <td></td>
                </tr>
                <tr>
                    <td>Время</td>
                    <td></td>
                </tr>
                <tr>
                    <td>Номер</td>
                    <td></td>
                </tr>

                <tr>
                    <td>Сумма</td>
                    <td></td>
                </tr>
                <tr>
                    <td>Товар</td>
                    <td></td>
                </tr>
                <tr>
                    <td>Терминал</td>
                    <td></td>
                </tr>
                <tr>
                    <td>Дилер</td>
                    <td></td>
                </tr>
                <tr>
                    <td>Статус</td>
                    <td></td>
                </tr>
            </table>
        </td>
        <td width="50"></td>
        <td>
            <table border="1">
                <tr>
                    <td>avancel_gate_1</td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td>citypay_gate</td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td>citypay_gate_life</td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td>cyberplat_gate</td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td>dacard_gate</td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td>kyivstar_bonus_no_commission_gate</td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td>kyivstar_exclusive_gate</td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td>kyivstar_gate</td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td>umc_gate</td>
                    <td></td>
                    <td></td>
                </tr>
            </table>
        </td>
    </tr>
</table>






























<%--<html:form action="/cyberplatTransactionInfoAction">--%>
<%--
<table>
    <tr>
        <td align="left" colspan="2">
            № биллинговой транзакции
        </td>
        <td>
            <tmx_ctrl:editBox property="billingNumber" />
            <tmx_ctrl:button property="getStatus" />
        </td>
    </tr>

</table>

<%= ((CyberplatTransactionInfoForm) pageContext.getSession().getAttribute("cyberplatTransactionInfoForm")).getCyberplatStatusInfo() %>

<br />
<br />
<b>Значения поля "ERROR" (коды ошибок сервера Киберплат)</b>
<table style="text-align:left;" border="1">
    <tr align="center">
        <th>Код</th>
        <th>Описание</th>
    </tr>
    <tr>
        <td>0</td>
        <td>Успешное завершение</td>
    </tr>
    <tr>
        <td>1</td>
        <td>Сессия с таким номером уже существует</td>
    </tr>
    <tr>
        <td>2</td>
        <td>Неверный код дилера</td>
    </tr>
    <tr>
        <td>3</td>
        <td>Неверный код точки приема</td>
    </tr>
    <tr>
        <td>4</td>
        <td>Неверный код оператора</td>
    </tr>
    <tr>
        <td>5</td>
        <td>Неверный формат кода сессии</td>
    </tr>
    <tr>
        <td>6</td>
        <td>Неверная ЭЦП</td>
    </tr>
    <tr>
        <td>7</td>
        <td>Неверный формат или значение суммы вне допустимого диапазона</td>
    </tr>
    <tr>
        <td>8</td>
        <td>Неверный формат номера телефона</td>
    </tr>
    <tr>
        <td>9</td>
        <td>Неверный формат номера лицевого счета</td>
    </tr>
    <tr>
        <td>10</td>
        <td>Неверный формат запроса</td>
    </tr>
    <tr>
        <td>11</td>
        <td>Сессия с таким номером не существует</td>
    </tr>
    <tr>
        <td>12</td>
        <td>Запрос сделан с незарегистрированного IP</td>
    </tr>
    <tr>
        <td>13</td>
        <td>Точка не зарегистрирована у Провайдера услуг</td>
    </tr>
    <tr>
        <td>15</td>
        <td>Платежи данному оператору не поддерживаются системой</td>
    </tr>
    <tr>
        <td>17</td>
        <td>Номер телефона не соответствует введенному ранее</td>
    </tr>
    <tr>
        <td>18</td>
        <td>Сумма платежа не соответствует введенной ранее</td>
    </tr>
    <tr>
        <td>19</td>
        <td>Номер счета (контракта) не соответствует введенному ранее</td>
    </tr>
    <tr>
        <td>20</td>
        <td>Платеж находится в состоянии завершения</td>
    </tr>
    <tr>
        <td>21</td>
        <td>Недостаточно средств для проведения платежа</td>
    </tr>
    <tr>
        <td>22</td>
        <td>Платеж не принят. Ошибка при переводе средств</td>
    </tr>
    <tr>
        <td>23</td>
        <td>Неверный номер телефона/счета</td>
    </tr>
    <tr>
        <td>24</td>
        <td>Ошибка связи с сервером провайдера или технологический перерыв в Киберплат</td>
    </tr>
    <tr>
        <td>25</td>
        <td>Проведение данного типа платежей приостановлено</td>
    </tr>
    <tr>
        <td>26</td>
        <td>Платежи данного дилера временно блокированы</td>
    </tr>
    <tr>
        <td>27</td>
        <td>Операции по счету приостановлены</td>
    </tr>
    <tr>
        <td>30</td>
        <td>Общая ошибка системы</td>
    </tr>
    <tr>
        <td>31</td>
        <td>Превышено количество одновременно обрабатываемых запросов (Киберплат)</td>
    </tr>
    <tr>
        <td>32</td>
        <td>Повторный платеж в течение 60 минут с момента окончания платежа (Киберплат)</td>
    </tr>
    <tr>
        <td>33</td>
        <td>Карт данного номинала в системе нет</td>
    </tr>
    <tr>
        <td>34</td>
        <td>Транзакция с таким номером не найдена</td>
    </tr>
    <tr>
        <td>35</td>
        <td>Ошибка при изменении состояния платежа</td>
    </tr>
    <tr>
        <td>36</td>
        <td>Неверный статус платежа</td>
    </tr>
    <tr>
        <td>37</td>
        <td>Попытка обращения в шлюз, отличный от шлюза на предыдущем шаге</td>
    </tr>
    <tr>
        <td>38</td>
        <td>Превышен лимит времени между проверкой номера и платежом (1 час)</td>
    </tr>
    <tr>
        <td>39</td>
        <td>Неверный счет</td>
    </tr>
    <tr>
        <td>40</td>
        <td>Карта указанного номинала в системе не зарегистрирована</td>
    </tr>
    <tr>
        <td>41</td>
        <td>Ошибка сохранения платежа в системе</td>
    </tr>
    <tr>
        <td>42</td>
        <td>Ошибка при сохранении чека в базе данных</td>
    </tr>
    <tr>
        <td>43</td>
        <td>Сеанс Вашей работы в системе недействителен (возможно, истек срок действия сессии), попробуйте войти заново</td>
    </tr>
    <tr>
        <td>44</td>
        <td>Клиент не может работать на этом торговом сервере</td>
    </tr>
    <tr>
        <td>45</td>
        <td>Отсутствует разрешение на прием платежей в пользу данного провайдера</td>
    </tr>
    <tr>
        <td>46</td>
        <td>Не удалось завершить ошибочный платеж</td>
    </tr>
    <tr>
        <td>47</td>
        <td>Сработало временное ограничение прав</td>
    </tr>
    <tr>
        <td>48</td>
        <td>Ошибка при сохранении данных сессии в базе данных</td>
    </tr>
    <tr>
        <td>50</td>
        <td>Проведение платежей в системе временно невозможно</td>
    </tr>
    <tr>
        <td>51</td>
        <td>Не найдены данные в системе</td>
    </tr>
    <tr>
        <td>52</td>
        <td>Возможно, дилер заблокирован. Текущий статус дилера не позволяет проводить платежи</td>
    </tr>
    <tr>
        <td>53</td>
        <td>Возможно, точка приема заблокирована. Текущий статус точки приема не позволяет проводить платежи</td>
    </tr>
    <tr>
        <td>54</td>
        <td>Возможно, оператор заблокирован. Текущий статус оператора не позволяет проводить платежи</td>
    </tr>
    <tr>
        <td>55</td>
        <td>Тип дилера не позволяет проводить платежи</td>
    </tr>
    <tr>
        <td>56</td>
        <td>Ожидалась точка другого типа. Тип точки приема не позволяет проводить платежи</td>
    </tr>
    <tr>
        <td>57</td>
        <td>Ожидался оператор другого типа. Тип оператора не позволяет проводить платежи</td>
    </tr>
    <tr>
        <td>81</td>
        <td>Превышена максимальная сумма платежа</td>
    </tr>
    <tr>
        <td>82</td>
        <td>Превышена сумма списаний за день</td>
    </tr>
    <tr>
        <td>223</td>
        <td>Контракт клиента не позволяет производить пополнение счета</td>
    </tr>
</table>
--%>