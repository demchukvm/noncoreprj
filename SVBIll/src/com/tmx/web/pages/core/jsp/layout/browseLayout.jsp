<%@ page language="java" pageEncoding="UTF-8" contentType="text/html; charset=utf-8" errorPage="/core/jsp/message/jsperror.jsp" isELIgnored="false" %>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c" %>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean" %>

<table width="100%" border="0" cellpadding="0" cellspacing="0" class="layouttable">
    <tr class="noBorder">
        <td class="noBorder" align="center" id="content">
            <c:if test="${requestScope['tile.browseLayout.content'] != null}">
                <tiles:insert page="${requestScope['tile.browseLayout.content']}" ignore="true"/>
            </c:if>
        </td>
    </tr>
</table>