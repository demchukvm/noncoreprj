/*//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                        L O C A L     V A R I A N T
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////*/

/*
package com.tmx.web.actions.core.tools;

import com.tmx.beng.base.BillException;
import com.tmx.beng.base.BillingEngine;
import com.tmx.beng.base.MediumAccessPool;
import com.tmx.beng.base.MediumResolver;
import com.tmx.web.forms.core.tools.GatesBlockForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForm;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.tmx.web.base.BasicDispatchedAction;
import java.util.*;

public class GatesBlockAction extends BasicDispatchedAction
{
    private final String FORWARD = "gatesBlock";

    private void initMediumMap(GatesBlockForm form) throws BillException
    {
        form.getProductGate().clear();

        HashMap mediumMap = MediumResolver.getInstance().getMediumMap();
        if(mediumMap.isEmpty() || mediumMap == null) mediumMap = MediumResolver.getInstance().getMediumAccessName();

        Set set = mediumMap.keySet();
        for(Object s : set)
        {
            String line = "";
            if(form.getGateOff().contains(mediumMap.get(s)))
                line = "<span id=\""+s+"\"/><td style=\"border: black 1px dotted; color: red;\">"+s+"</td>"+"<td style=\"border:black 1px dotted; color: red;\">"+mediumMap.get(s)+"</td>";
             else
                line = "<span id=\""+s+"\"/><td style=\"border: black 1px dotted;\">"+s+"</td>"+"<td style=\"border:black 1px dotted;\">"+mediumMap.get(s)+"</td>";
            form.getProductGate().add(line);
            Collections.sort(form.getProductGate());
        }
    }

    private void initGateOn(GatesBlockForm form, Set<String> openGates) throws BillException
    {
        ArrayList gatesOn = new ArrayList(openGates);
        Collections.sort(gatesOn);
        form.setGateOn(gatesOn);
    }

    private void initGateOff(GatesBlockForm form, Set<String> closeGates) throws BillException
    {
        ArrayList gatesOff = new ArrayList(closeGates);
        Collections.sort(gatesOff);
        form.setGateOff(gatesOff);
    }

    private void setChange(GatesBlockForm form, Set<String> openGates, Set<String> closeGates) throws BillException
    {
        initGateOn(form, openGates);
        initGateOff(form, closeGates);
        initMediumMap(form);
        clearCB(form);
    }

    private Set getSelectedGateList(GatesBlockForm form)
    {
        ArrayList gateList = new ArrayList();

        if(form.isCb_avancel_gate_1() == true)
            gateList.add("avancel_gate_1");
        if(form.isCb_avancel_gate_2() == true)
            gateList.add("avancel_gate_2");
        if(form.isCb_avancel_gate_3() == true)
            gateList.add("avancel_gate_3");
        if(form.isCb_beeline_gate() == true)
            gateList.add("beeline_gate");
        if(form.isCb_citypay_gate() == true)
            gateList.add("citypay_gate");
        if(form.isCb_citypay_gate_life() == true)
            gateList.add("citypay_gate_life");
        if(form.isCb_cyberplat_gate() == true)
            gateList.add("cyberplat_gate");
        if(form.isCb_dacard_gate() == true)
            gateList.add("dacard_gate");
        if(form.isCb_kyivstar_bonus_commission_gate() == true)
            gateList.add("kyivstar_bonus_commission_gate");
        if(form.isCb_kyivstar_bonus_no_commission_gate() == true)
            gateList.add("kyivstar_bonus_no_commission_gate");
        if(form.isCb_kyivstar_exclusive_gate() == true)
            gateList.add("kyivstar_exclusive_gate");
        if(form.isCb_kyivstar_gate() == true)
            gateList.add("kyivstar_gate");
        if(form.isCb_life_gate() == true)
            gateList.add("life_gate");
        if(form.isCb_test_gate() == true)
            gateList.add("test_gate");
        if(form.isCb_umc_gate() == true)
            gateList.add("umc_gate");
        if(form.isCb_ussd_gate() == true)
            gateList.add("ussd_gate");
        if(form.isCb_ussd_gate_reserve() == true)
            gateList.add("ussd_gate_reserve");

        return new HashSet(gateList);
    }

    private Set getAllGateOffList(GatesBlockForm form)
    {
        return new HashSet(form.getGateOn());
    }

    private Set getAllGateOnList(GatesBlockForm form)
    {
        return new HashSet(form.getGateOff());    
    }

    private void clearCB(GatesBlockForm form)
    {
        form.setCb_avancel_gate_1(false);
        form.setCb_avancel_gate_2(false);
        form.setCb_avancel_gate_3(false);
        form.setCb_beeline_gate(false);
        form.setCb_citypay_gate(false);
        form.setCb_citypay_gate_life(false);
        form.setCb_cyberplat_gate(false);
        form.setCb_dacard_gate(false);
        form.setCb_kyivstar_bonus_commission_gate(false);
        form.setCb_kyivstar_bonus_no_commission_gate(false);
        form.setCb_kyivstar_exclusive_gate(false);
        form.setCb_kyivstar_gate(false);
        form.setCb_life_gate(false);
        form.setCb_test_gate(false);
        form.setCb_umc_gate(false);
        form.setCb_ussd_gate(false);
        form.setCb_ussd_gate_reserve(false);

        form.setCbAllOn(false);
        form.setCbAllOff(false);
    }


    protected ActionForward defaultAction(ActionMapping mapping,
                                          ActionForm form,
                                          HttpServletRequest request,
                                          HttpServletResponse response,
                                          String methodName) throws Throwable {

        GatesBlockForm gatesBlockForm = (GatesBlockForm) form;

        MediumAccessPool mediumAccessPool = BillingEngine.getInstance().getMediumAccessPool();
        Set<String> openGates = mediumAccessPool.getMediumAccessPoolKeys();
        Set<String> closeGates = mediumAccessPool.getRemovedElemetsKeys();

        setChange(gatesBlockForm, openGates, closeGates);

        return mapping.findForward(FORWARD);
    }

    public ActionForward removeMediumAccesses(ActionMapping mapping,
                                         ActionForm form,
                                         HttpServletRequest request,
                                         HttpServletResponse response) throws Throwable {

        GatesBlockForm gatesBlockForm = (GatesBlockForm) form;

        MediumAccessPool mediumAccessPool = BillingEngine.getInstance().getMediumAccessPool();
        if(gatesBlockForm.isCbAllOff()) mediumAccessPool.removeElements(getAllGateOffList(gatesBlockForm));
        else mediumAccessPool.removeElements(getSelectedGateList(gatesBlockForm));
        Set<String> openGates = mediumAccessPool.getMediumAccessPoolKeys();
        Set<String> closeGates = mediumAccessPool.getRemovedElemetsKeys();

        setChange(gatesBlockForm, openGates, closeGates);

        return mapping.findForward(FORWARD);

    }

    public ActionForward restoreMediumAccesses(ActionMapping mapping,
                                             ActionForm form,
                                             HttpServletRequest request,
                                             HttpServletResponse response) throws Throwable {

        GatesBlockForm gatesBlockForm = (GatesBlockForm) form;

        MediumAccessPool mediumAccessPool = BillingEngine.getInstance().getMediumAccessPool();
        if(gatesBlockForm.isCbAllOn()) mediumAccessPool.restoreElements(getAllGateOnList(gatesBlockForm));
        else mediumAccessPool.restoreElements(getSelectedGateList(gatesBlockForm));
        Set<String> openGates = mediumAccessPool.getMediumAccessPoolKeys();
        Set<String> closeGates = mediumAccessPool.getRemovedElemetsKeys();

        setChange(gatesBlockForm, openGates, closeGates);

        return mapping.findForward(FORWARD);
    }

}
*/

/*//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                        S E R V E R     V A R I A N T
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////*/

package com.tmx.web.actions.core.tools;

import com.tmx.as.modules.SpringModule;
import com.tmx.beng.access.GateConnection;
import com.tmx.beng.base.BillException;
import com.tmx.web.forms.core.tools.GatesBlockForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForm;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.tmx.web.base.BasicDispatchedAction;
import java.util.*;

public class GatesBlockAction extends BasicDispatchedAction
{
    private final String FORWARD = "gatesBlock";

    private void initMediumMap(GatesBlockForm form) throws BillException
    {
        form.getProductGate().clear();

        GateConnection conn = (GateConnection) SpringModule.getApplicationContext().getBean("gateConnection");
        HashMap mediumMap = conn.getMediumMap();
        if(mediumMap.isEmpty() || mediumMap == null) mediumMap = conn.getMediumAccessName();

        Set set = mediumMap.keySet();
        for(Object s : set)
        {
            String line = "";
            if(form.getGateOff().contains(mediumMap.get(s)))
                line = "<span id=\""+s+"\"/><td style=\"border: black 1px dotted; color: red;\">"+s+"</td>"+"<td style=\"border:black 1px dotted; color: red;\">"+mediumMap.get(s)+"</td>";
             else
                line = "<span id=\""+s+"\"/><td style=\"border: black 1px dotted;\">"+s+"</td>"+"<td style=\"border:black 1px dotted;\">"+mediumMap.get(s)+"</td>";
            form.getProductGate().add(line);
            Collections.sort(form.getProductGate());
        }
    }

    private void initGateOn(GatesBlockForm form, Set<String> openGates) throws BillException
    {
        ArrayList gatesOn = new ArrayList(openGates);
        Collections.sort(gatesOn);
        form.setGateOn(gatesOn);
    }

    private void initGateOff(GatesBlockForm form, Set<String> closeGates) throws BillException
    {
        ArrayList gatesOff = new ArrayList(closeGates);
        Collections.sort(gatesOff);
        form.setGateOff(gatesOff);
    }

    private void setChange(GatesBlockForm form, Set<String> openGates, Set<String> closeGates) throws BillException
    {
        initGateOn(form, openGates);
        initGateOff(form, closeGates);
        initMediumMap(form);
        clearCB(form);
    }

    private Set getSelectedGateList(GatesBlockForm form)
    {
        ArrayList gateList = new ArrayList();

        if(form.isCb_avancel_gate_1() == true)
            gateList.add("avancel_gate_1");
        if(form.isCb_avancel_gate_2() == true)
            gateList.add("avancel_gate_2");
        if(form.isCb_avancel_gate_3() == true)
            gateList.add("avancel_gate_3");
        if(form.isCb_beeline_gate() == true)
            gateList.add("beeline_gate");
        if(form.isCb_citypay_gate() == true)
            gateList.add("citypay_gate");
        if(form.isCb_citypay_gate_life() == true)
            gateList.add("citypay_gate_life");
        if(form.isCb_cyberplat_gate() == true)
            gateList.add("cyberplat_gate");
        if(form.isCb_dacard_gate() == true)
            gateList.add("dacard_gate");
        if(form.isCb_kyivstar_bonus_commission_gate() == true)
            gateList.add("kyivstar_bonus_commission_gate");
        if(form.isCb_kyivstar_bonus_no_commission_gate() == true)
            gateList.add("kyivstar_bonus_no_commission_gate");
        if(form.isCb_kyivstar_exclusive_gate() == true)
            gateList.add("kyivstar_exclusive_gate");
        if(form.isCb_kyivstar_gate() == true)
            gateList.add("kyivstar_gate");
        if(form.isCb_life_gate() == true)
            gateList.add("life_gate");
        if(form.isCb_test_gate() == true)
            gateList.add("test_gate");
        if(form.isCb_umc_gate() == true)
            gateList.add("umc_gate");
        if(form.isCb_ussd_gate() == true)
            gateList.add("ussd_gate");
        if(form.isCb_ussd_gate_reserve() == true)
            gateList.add("ussd_gate_reserve");

        return new HashSet(gateList);
    }

    private Set getAllGateOffList(GatesBlockForm form)
    {
        return new HashSet(form.getGateOn());
    }

    private Set getAllGateOnList(GatesBlockForm form)
    {
        return new HashSet(form.getGateOff());
    }

    private void clearCB(GatesBlockForm form)
    {
        form.setCb_avancel_gate_1(false);
        form.setCb_avancel_gate_2(false);
        form.setCb_avancel_gate_3(false);
        form.setCb_beeline_gate(false);
        form.setCb_citypay_gate(false);
        form.setCb_citypay_gate_life(false);
        form.setCb_cyberplat_gate(false);
        form.setCb_dacard_gate(false);
        form.setCb_kyivstar_bonus_commission_gate(false);
        form.setCb_kyivstar_bonus_no_commission_gate(false);
        form.setCb_kyivstar_exclusive_gate(false);
        form.setCb_kyivstar_gate(false);
        form.setCb_life_gate(false);
        form.setCb_test_gate(false);
        form.setCb_umc_gate(false);
        form.setCb_ussd_gate(false);
        form.setCb_ussd_gate_reserve(false);

        form.setCbAllOn(false);
        form.setCbAllOff(false);
    }


    protected ActionForward defaultAction(ActionMapping mapping,
                                          ActionForm form,
                                          HttpServletRequest request,
                                          HttpServletResponse response,
                                          String methodName) throws Throwable {

        GatesBlockForm gatesBlockForm = (GatesBlockForm) form;

        GateConnection conn = (GateConnection) SpringModule.getApplicationContext().getBean("gateConnection");
        Set<String> openGates = conn.getMediumAccessPoolKeys();
        Set<String> closeGates = conn.getRemovedElemetsKeys();

        setChange(gatesBlockForm, openGates, closeGates);

        return mapping.findForward(FORWARD);
    }

    public ActionForward removeMediumAccesses(ActionMapping mapping,
                                         ActionForm form,
                                         HttpServletRequest request,
                                         HttpServletResponse response) throws Throwable {

        GatesBlockForm gatesBlockForm = (GatesBlockForm) form;

        GateConnection conn = (GateConnection) SpringModule.getApplicationContext().getBean("gateConnection");
        if(gatesBlockForm.isCbAllOff()) conn.removeElements(getAllGateOffList(gatesBlockForm));
        else conn.removeElements(getSelectedGateList(gatesBlockForm));
        Set<String> openGates = conn.getMediumAccessPoolKeys();
        Set<String> closeGates = conn.getRemovedElemetsKeys();

        setChange(gatesBlockForm, openGates, closeGates);

        return mapping.findForward(FORWARD);

    }

    public ActionForward restoreMediumAccesses(ActionMapping mapping,
                                             ActionForm form,
                                             HttpServletRequest request,
                                             HttpServletResponse response) throws Throwable {

        GatesBlockForm gatesBlockForm = (GatesBlockForm) form;

        GateConnection conn = (GateConnection) SpringModule.getApplicationContext().getBean("gateConnection");
        if(gatesBlockForm.isCbAllOn()) conn.restoreElements(getAllGateOnList(gatesBlockForm));
        else conn.restoreElements(getSelectedGateList(gatesBlockForm));
        Set<String> openGates = conn.getMediumAccessPoolKeys();
        Set<String> closeGates = conn.getRemovedElemetsKeys();

        setChange(gatesBlockForm, openGates, closeGates);

        return mapping.findForward(FORWARD);
    }

}