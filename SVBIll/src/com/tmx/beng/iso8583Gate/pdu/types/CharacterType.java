package com.tmx.beng.iso8583Gate.pdu.types;

import com.tmx.beng.iso8583Gate.pdu.RuntimeDataElement;
import com.tmx.beng.iso8583Gate.pdu.IsoParseException;
import com.tmx.beng.iso8583Gate.pdu.IsoSerializeException;
import com.tmx.beng.iso8583Gate.pdu.IncorrectFormatException;

import java.io.InputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Arrays;

/**

 */
public abstract class CharacterType extends Type{

    public void parse(InputStream is, int length, RuntimeDataElement rde) throws IsoParseException {
        try{
            byte[] data = new byte[length];//1 char in 1 byte
            is.read(data);
            rde.setValue(new String(data));
        }
        catch(IOException e){
            throw new IsoParseException("err.character_type.parsing_failed", e);
        }
    }

    /** used to parse sub fields */
    public String parse(InputStream is, int length) throws IsoParseException {
        try{
            byte[] data = new byte[length];//1 char in 1 byte
            is.read(data);
            return new String(data);
        }
        catch(IOException e){
            throw new IsoParseException("err.character_type.parsing_failed", e);
        }
    }

    public SerializedValue serialize(boolean fixedLength, int byteLengthLimit, RuntimeDataElement rde) throws IsoSerializeException{
        return serialize(fixedLength, byteLengthLimit, rde.getString());
    }

    public SerializedValue serialize(boolean fixedLength, int byteLengthLimit, String value) throws IsoSerializeException{
        try{
            byte[] charData = value != null ? value.getBytes() : new byte[0];
            if(charData.length > byteLengthLimit)
                throw new IncorrectFormatException("err.character_type.char_array_size_more_then_maxlength", new String[]{String.valueOf(charData.length), String.valueOf(byteLengthLimit)});

            if(fixedLength && (charData.length < byteLengthLimit))
                charData = expandFixedLengthValue(charData, byteLengthLimit);

            SerializedValue sv = new SerializedValue();
            sv.data = charData;
            sv.length = charData.length;
            return sv;
        }
        catch(IncorrectFormatException e){
            throw new IsoSerializeException("err.character_type.serialization_failed", e);
        }
    }


    protected byte[] expandFixedLengthValue0(byte[] value, int fixedByteLength) throws IncorrectFormatException{
        //fill tail of byte array by 'Space' to match fixed length
        byte[] expandedData = new byte[fixedByteLength];
        Arrays.fill(expandedData, value.length, expandedData.length, " ".getBytes()[0]);
        System.arraycopy(value, 0, expandedData, 0, value.length);
        return expandedData;
    }

}
