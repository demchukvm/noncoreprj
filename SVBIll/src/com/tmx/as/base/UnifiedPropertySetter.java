package com.tmx.as.base;

import com.tmx.as.resolver.ResolverException;


public interface UnifiedPropertySetter extends Cloneable{
    public void set(String fieldName, String fieldValue) throws ResolverException;
}
