package com.tmx.as.modules;

import com.tmx.beng.httpsgate.beeline.BeelineGate;
import com.tmx.beng.httpsgate.citypay.CitypayGateForLifeExc;
import com.tmx.beng.httpsgate.cyberplat.CyberplatGate;
import com.tmx.beng.httpsgate.globalmoney.GlobalMoneyGate;
import com.tmx.beng.httpsgate.kyivstar.KyivstarExclusiveGate;
import com.tmx.beng.httpsgate.kyivstar.KyivstarGate;
import com.tmx.beng.httpsgate.kyivstar.KyivstarBonusCommissionGate;
import com.tmx.beng.httpsgate.kyivstar.KyivstarBonusNoCommissionGate;
import com.tmx.beng.httpsgate.umc.UmcGate;
import com.tmx.beng.httpsgate.avancel.AvancelGate1;
import com.tmx.beng.httpsgate.avancel.AvancelGate2;
import com.tmx.beng.httpsgate.avancel.AvancelGate3;
import com.tmx.beng.httpsgate.life.LifeGate;
import com.tmx.beng.httpsgate.dacard.DacardGate;
import com.tmx.beng.ussdgate.UssdGate;
import com.tmx.beng.httpsgate.citypay.CitypayGate;
import com.tmx.beng.testgate.TestGate;
import com.tmx.util.InitException;

import java.util.Properties;

/**
 *   Module of outgoing gates
 */
public class GatesModule extends AbstractModule{
    private static final String PROP_KYIVSTAR = "Kyivstar";
    private static final String PROP_KYIVSTAR_EXCLUSIVE = "KyivstarExclusive";
    private static final String PROP_KYIVSTAR_BONUS_COMMISSION = "KyivstarBonusCommission";
    private static final String PROP_KYIVSTAR_BONUS_NO_COMMISSION = "KyivstarBonusNoCommission";
    private static final String PROP_BEELINE = "Beeline";
    private static final String PROP_UMC = "Umc";
    private static final String PROP_AVANCEL_1 = "Avancel1";
    private static final String PROP_AVANCEL_2 = "Avancel2";
    private static final String PROP_AVANCEL_3 = "Avancel3";
    private static final String PROP_LIFE = "Life";
    private static final String PROP_DACARD = "Dacard";    
    private static final String PROP_USSD = "Ussd";
    private static final String PROP_CITYPAY = "Citypay";
    private static final String PROP_CITYPAY_LIFE = "CitypayForLifeExc";
    private static final String PROP_CYBERPLAT = "Cyberplat";
    private static final String PROP_TEST = "Test";
    private static final String PROP_TIMEOUT = "timeout";
    private static final String PROP_GLOBAL_MONEY = "GlobalMoney";

    public void start(Properties props)throws ModuleException{
        try{
            if(Boolean.TRUE.toString().equalsIgnoreCase(props.getProperty(PROP_KYIVSTAR))){
                KyivstarGate.init();
            }
            if(Boolean.TRUE.toString().equalsIgnoreCase(props.getProperty(PROP_KYIVSTAR_EXCLUSIVE))){
                KyivstarExclusiveGate.init();
            }
            if(Boolean.TRUE.toString().equalsIgnoreCase(props.getProperty(PROP_KYIVSTAR_BONUS_COMMISSION))){
                KyivstarBonusCommissionGate.init();
            }
            if(Boolean.TRUE.toString().equalsIgnoreCase(props.getProperty(PROP_KYIVSTAR_BONUS_NO_COMMISSION))){
                KyivstarBonusNoCommissionGate.init();
            }
            if(Boolean.TRUE.toString().equalsIgnoreCase(props.getProperty(PROP_BEELINE))){
                BeelineGate.init();
            }
            if(Boolean.TRUE.toString().equalsIgnoreCase(props.getProperty(PROP_UMC))){
                UmcGate.init();
            }
            if(Boolean.TRUE.toString().equalsIgnoreCase(props.getProperty(PROP_AVANCEL_1))){
                AvancelGate1.init();
            }
            if(Boolean.TRUE.toString().equalsIgnoreCase(props.getProperty(PROP_AVANCEL_2))){
                AvancelGate2.init();
            }
            if(Boolean.TRUE.toString().equalsIgnoreCase(props.getProperty(PROP_AVANCEL_3))){
                AvancelGate3.init();
            }
            if(Boolean.TRUE.toString().equalsIgnoreCase(props.getProperty(PROP_LIFE))){
                LifeGate.init();
            }
            if(Boolean.TRUE.toString().equalsIgnoreCase(props.getProperty(PROP_DACARD))){
                DacardGate.init();
            }
            if(Boolean.TRUE.toString().equalsIgnoreCase(props.getProperty(PROP_USSD))){
                UssdGate.init();
            }
            if(Boolean.TRUE.toString().equalsIgnoreCase(props.getProperty(PROP_TEST))){
                TestGate.init();
            }
            if(Boolean.TRUE.toString().equalsIgnoreCase(props.getProperty(PROP_CITYPAY))){
                CitypayGate.init();
            }
            if(Boolean.TRUE.toString().equalsIgnoreCase(props.getProperty(PROP_CITYPAY_LIFE))){
                CitypayGateForLifeExc.init();
            }
            if(Boolean.TRUE.toString().equalsIgnoreCase(props.getProperty(PROP_CYBERPLAT))){
                CyberplatGate.init();
            }
            if(Boolean.TRUE.toString().equalsIgnoreCase(props.getProperty(PROP_GLOBAL_MONEY))) {
                GlobalMoneyGate.init();
            }
        }catch(InitException e){
            throw new ModuleException(e.getLocalizedMessage());
        }
    }

    public void shutdownGraceful(Properties props)throws ModuleException{
        try{

            long timeout = Long.MAX_VALUE;
            if(props.getProperty(PROP_TIMEOUT) != null){
                try{
                    timeout = Long.parseLong(props.getProperty(PROP_TIMEOUT));
                }
                catch(Throwable e){
                    e.printStackTrace();
                }
            }
            if("true".equalsIgnoreCase(props.getProperty(PROP_KYIVSTAR))){
                KyivstarGate.shutdown(timeout);
            }
            if(Boolean.TRUE.toString().equalsIgnoreCase(props.getProperty(PROP_KYIVSTAR_EXCLUSIVE))){
                KyivstarExclusiveGate.shutdown(timeout);
            }
            if(Boolean.TRUE.toString().equalsIgnoreCase(props.getProperty(PROP_KYIVSTAR_BONUS_COMMISSION))){
                KyivstarBonusCommissionGate.shutdown(timeout);
            }
            if(Boolean.TRUE.toString().equalsIgnoreCase(props.getProperty(PROP_KYIVSTAR_BONUS_NO_COMMISSION))){
                KyivstarBonusNoCommissionGate.shutdown(timeout);
            }
            if("true".equalsIgnoreCase(props.getProperty(PROP_BEELINE))){
                BeelineGate.shutdown(timeout);
            }
            if("true".equalsIgnoreCase(props.getProperty(PROP_UMC))){
                UmcGate.shutdown(timeout);
            }
            if("true".equalsIgnoreCase(props.getProperty(PROP_AVANCEL_1))){
                AvancelGate1.shutdown(timeout);
            }
            if("true".equalsIgnoreCase(props.getProperty(PROP_AVANCEL_2))){
                AvancelGate2.shutdown(timeout);
            }
            if("true".equalsIgnoreCase(props.getProperty(PROP_AVANCEL_3))){
                AvancelGate3.shutdown(timeout);
            }
            if("true".equalsIgnoreCase(props.getProperty(PROP_LIFE))){
                LifeGate.shutdown(timeout);
            }
            if("true".equalsIgnoreCase(props.getProperty(PROP_USSD))){
                UssdGate.shutdown(false, timeout);
            }
            if("true".equalsIgnoreCase(props.getProperty(PROP_CITYPAY))){
                CitypayGate.shutdown(timeout);
            }
            if("true".equalsIgnoreCase(props.getProperty(PROP_CITYPAY_LIFE))){
                CitypayGateForLifeExc.shutdown(timeout);
            }
            if("true".equalsIgnoreCase(props.getProperty(PROP_CYBERPLAT))){
                CyberplatGate.shutdown(timeout);
            }
            if("true".equalsIgnoreCase(props.getProperty(PROP_TEST))){
                TestGate.shutdown(false, timeout);
            }
        }
        catch(InitException e){
            throw new ModuleException(e.getLocalizedMessage());
        }
    }

    public void shutdownAbort(Properties props)throws ModuleException{
        try{
            if("true".equalsIgnoreCase(props.getProperty(PROP_KYIVSTAR))){
                KyivstarGate.shutdown(0);
            }
            if(Boolean.TRUE.toString().equalsIgnoreCase(props.getProperty(PROP_KYIVSTAR_EXCLUSIVE))){
                KyivstarExclusiveGate.shutdown(0);
            }
            if(Boolean.TRUE.toString().equalsIgnoreCase(props.getProperty(PROP_KYIVSTAR_BONUS_COMMISSION))){
                KyivstarBonusCommissionGate.shutdown(0);
            }
            if(Boolean.TRUE.toString().equalsIgnoreCase(props.getProperty(PROP_KYIVSTAR_BONUS_NO_COMMISSION))){
                KyivstarBonusNoCommissionGate.shutdown(0);
            }
            if("true".equalsIgnoreCase(props.getProperty(PROP_BEELINE))){
                BeelineGate.shutdown(0);
            }
            if("true".equalsIgnoreCase(props.getProperty(PROP_UMC))){
                UmcGate.shutdown(0);
            }
            if("true".equalsIgnoreCase(props.getProperty(PROP_AVANCEL_1))){
                AvancelGate1.shutdown(0);
            }
            if("true".equalsIgnoreCase(props.getProperty(PROP_AVANCEL_2))){
                AvancelGate2.shutdown(0);
            }
            if("true".equalsIgnoreCase(props.getProperty(PROP_AVANCEL_3))){
                AvancelGate3.shutdown(0);
            }
            // Proximan
            if("true".equalsIgnoreCase(props.getProperty(PROP_LIFE))){
                LifeGate.shutdown(0);
            }
            if("true".equalsIgnoreCase(props.getProperty(PROP_CITYPAY))){
                CitypayGate.shutdown(0);
            }
            if("true".equalsIgnoreCase(props.getProperty(PROP_CITYPAY_LIFE))){
                CitypayGateForLifeExc.shutdown(0);
            }
            if("true".equalsIgnoreCase(props.getProperty(PROP_CYBERPLAT))){
                CyberplatGate.shutdown(0);
            }
            
            if("true".equalsIgnoreCase(props.getProperty(PROP_USSD))){
                UssdGate.shutdown(true, 0);
            }
            if("true".equalsIgnoreCase(props.getProperty(PROP_TEST))){
                TestGate.shutdown(true, 0);
            }
        }
        catch(InitException e){
            throw new ModuleException(e.getLocalizedMessage());
        }
    }
}
