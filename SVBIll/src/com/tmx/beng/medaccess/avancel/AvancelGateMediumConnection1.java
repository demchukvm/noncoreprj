package com.tmx.beng.medaccess.avancel;

import com.tmx.beng.httpsgate.HttpsGateException;
import com.tmx.beng.httpsgate.avancel.AvancelGate1;
import com.tmx.beng.base.BillingMessage;
import com.tmx.beng.medaccess.MediumConnection;

/**
 */
public class AvancelGateMediumConnection1 implements MediumConnection {
    private AvancelGate1 avancelGate = null;

    AvancelGateMediumConnection1() throws HttpsGateException {
        avancelGate = AvancelGate1.getInstance();
    }

    public void send(BillingMessage msg) {
        avancelGate.enqueueBillMessage(msg);
    }
}
