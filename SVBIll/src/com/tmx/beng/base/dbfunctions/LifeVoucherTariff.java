package com.tmx.beng.base.dbfunctions;

import java.util.List;
import java.util.Arrays;


public class LifeVoucherTariff extends VoucherTariff{

    protected List<String> getServiceCodes() {
        return Arrays.asList("Life");  
    }

}
