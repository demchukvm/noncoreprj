package com.tmx.web.controls.grid;

import com.tmx.util.StructurizedException;

public class GridException extends StructurizedException{
    public GridException(String msg) {
        super(msg);
    }

    public GridException(String msg, String[] params, Throwable cause) {
        super(msg, params, cause);
    }
}
