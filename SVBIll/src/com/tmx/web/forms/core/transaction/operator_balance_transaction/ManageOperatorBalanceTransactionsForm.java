package com.tmx.web.forms.core.transaction.operator_balance_transaction;

import com.tmx.web.base.BasicActionForm;
import com.tmx.web.controls.*;
import com.tmx.web.controls.table.Table;
import com.tmx.web.controls.table.Filter;
import com.tmx.web.controls.table.Parameter;
import com.tmx.util.i18n.DictionaryWrapper;
import com.tmx.beng.base.StatusDictionary;
import com.tmx.as.entities.bill.operator.Operator;
import com.tmx.as.entities.bill.transaction.TransactionType;


public class ManageOperatorBalanceTransactionsForm extends BasicActionForm {

    private Table operatorTransactionsTable;
    private DictionaryWrapper transactionStatusDictionary;

    private EditBox editBoxAllTransaction;
    private EditBox editBoxSuccessTransaction;

    private EditBox editBoxAllTransAmount;
    private EditBox editBoxSuccessTransAmount;

    protected void init() {

        editBoxAllTransaction = new EditBox();
        editBoxAllTransaction.setReadonly(true);

        editBoxSuccessTransaction = new EditBox();
        editBoxSuccessTransaction.setReadonly(true);

        editBoxAllTransAmount = new EditBox();
        editBoxAllTransAmount.setReadonly(true);

        editBoxSuccessTransAmount = new EditBox();
        editBoxSuccessTransAmount.setReadonly(true);

        operatorTransactionsTable = new Table("query(query_type=retrieve_all," +
                "entity_class=com.tmx.as.entities.bill.transaction.OperatorBalanceTransaction," +
                "table_attr=operator," +
                "table_attr=operatorBalance," +
                "order(name=transactionTime,type=desc)," +
                "page_size=15" +
                ")");
        operatorTransactionsTable.setRefreshActionName("/core/transactions/operator_transaction/manageOperatorTransactions");

        Filter amountFilterFrom = new Filter();
        amountFilterFrom.addParameter("from_amount", new Parameter(new EditBox()));
        operatorTransactionsTable.getFilterSet().addFilter("by_client_amount_from", amountFilterFrom);

        Filter amountFilterTo = new Filter();
        amountFilterTo.addParameter("to_amount", new Parameter(new EditBox()));
        operatorTransactionsTable.getFilterSet().addFilter("by_client_amount_to", amountFilterTo);

        ScrollBox scrollBoxOperators = new ScrollBox(){
            protected String getKey(Object dataListElement) {
                return ((Operator)dataListElement).getOperatorId().toString();
            }

            protected String getValue(Object dataListElement) {
                return ((Operator)dataListElement).getName();
            }
        };
        scrollBoxOperators.setNullable(true);
        Filter operatorFilter = new Filter();
        operatorFilter.addParameter("operator", new Parameter(scrollBoxOperators));
        operatorTransactionsTable.getFilterSet().addFilter("by_operator", operatorFilter);

        ScrollBox scrollBoxTransTypes = new ScrollBox(){
            protected String getKey(Object dataListElement) {
                return ((TransactionType)dataListElement).getId().toString();
            }

            protected String getValue(Object dataListElement) {
                return ((TransactionType)dataListElement).getName();
            }
        };
        scrollBoxTransTypes.setNullable(true);
        Filter transTypeFilter = new Filter();
        transTypeFilter.addParameter("type", new Parameter(scrollBoxTransTypes));
        operatorTransactionsTable.getFilterSet().addFilter("by_transaction_type", transTypeFilter);

        //selectFrom filter
        Filter fromDateFilter = new Filter();
        TimeSelector timeFromSelector = new TimeSelector();
        fromDateFilter.addParameter("date", new Parameter(timeFromSelector));
        operatorTransactionsTable.getFilterSet().addFilter("by_FromDate", fromDateFilter);

        //selectTo filter
        Filter toDateFilter = new Filter();
        TimeSelector timeToSelector = new TimeSelector();
        toDateFilter.addParameter("date", new Parameter(timeToSelector));
        operatorTransactionsTable.getFilterSet().addFilter("by_ToDate", toDateFilter);

        //for billing transaction number
        Filter billTransactionFilter = new Filter();
        billTransactionFilter.addParameter("billnum", new Parameter(new EditBox()));
        operatorTransactionsTable.getFilterSet().addFilter("by_billnum_like", billTransactionFilter);

        //for client transaction number
        Filter cliTransactionFilter = new Filter();
        cliTransactionFilter.addParameter("clinum", new Parameter(new EditBox()));
        operatorTransactionsTable.getFilterSet().addFilter("by_clinum_like", cliTransactionFilter);


        transactionStatusDictionary = new DictionaryWrapper(getLocale(),
                StatusDictionary.class, StatusDictionary.STATUS_MSGS_BUNDLE);

    }

    //--------Properties

    public Table getOperatorTransactionsTable() {
        return operatorTransactionsTable;
    }

    public void setOperatorTransactionsTable(Table operatorTransactionsTable) {
        this.operatorTransactionsTable = operatorTransactionsTable;
    }

    public DictionaryWrapper getTransactionStatusDictionary() {
        return transactionStatusDictionary;
    }

    public void setTransactionStatusDictionary(DictionaryWrapper transactionStatusDictionary) {
        this.transactionStatusDictionary = transactionStatusDictionary;
    }

    public EditBox getEditBoxAllTransaction() {
        return editBoxAllTransaction;
    }

    public void setEditBoxAllTransaction(EditBox editBoxAllTransaction) {
        this.editBoxAllTransaction = editBoxAllTransaction;
    }

    public EditBox getEditBoxSuccessTransaction() {
        return editBoxSuccessTransaction;
    }

    public void setEditBoxSuccessTransaction(EditBox editBoxSuccessTransaction) {
        this.editBoxSuccessTransaction = editBoxSuccessTransaction;
    }

    public EditBox getEditBoxAllTransAmount() {
        return editBoxAllTransAmount;
    }

    public void setEditBoxAllTransAmount(EditBox editBoxAllTransAmount) {
        this.editBoxAllTransAmount = editBoxAllTransAmount;
    }

    public EditBox getEditBoxSuccessTransAmount() {
        return editBoxSuccessTransAmount;
    }

    public void setEditBoxSuccessTransAmount(EditBox editBoxSuccessTransAmount) {
        this.editBoxSuccessTransAmount = editBoxSuccessTransAmount;
    }
}
