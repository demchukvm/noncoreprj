package com.tmx.web.controls;

/**
 Validation rule to handle coupled controls
 */
public abstract class MultiControlValidationRule extends ValidationRule{
    private BasicControl[] coupledControls = null;

    protected MultiControlValidationRule(String errorMessageKey, Object[] args, BasicControl[] coupledCtrls) {
        super(errorMessageKey, args);
        this.coupledControls = coupledCtrls;
    }

    boolean executeValidation(){
        validationPassed = validate(control, arguments, coupledControls);
        return validationPassed;
    }

    /** implement this redundant method, to prevent its implementation in successors. */
    public final boolean validate(BasicControl ctrl, Object[] args){return true;}

    public abstract boolean validate(BasicControl control, Object[] args, BasicControl[] coupledCtrls);

}
