package com.tmx.as.resolver.functions.order;

import com.tmx.as.base.FilterParameterWrapper;
import com.tmx.as.resolver.functions.Function;
import com.tmx.as.resolver.FunctionSyntaxException;
import com.tmx.as.resolver.ResolverException;

public class Parameter implements Function {
    FilterParameterWrapper filterParameter=new FilterParameterWrapper();

    public void set(int paramIndex, String paramName, Object paramValue) throws FunctionSyntaxException {
        try {
            filterParameter.set(paramName,(String)paramValue);
        } catch (ResolverException e) {
            throw new FunctionSyntaxException("projection error", e);
        }
    }

    public Object returnValue() throws FunctionSyntaxException {
        return filterParameter;
    }
}
