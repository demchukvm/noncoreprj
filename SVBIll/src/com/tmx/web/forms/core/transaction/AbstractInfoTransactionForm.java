package com.tmx.web.forms.core.transaction;

import com.tmx.web.base.BasicActionForm;
import com.tmx.web.controls.EditBox;
import com.tmx.web.controls.Button;
import com.tmx.web.controls.TimeSelector;
import com.tmx.web.controls.TextArea;
import com.tmx.as.entities.bill.transaction.AbstractTransaction;
import com.tmx.as.blogic.OperatorManager;
import com.tmx.as.exceptions.DatabaseException;
import com.tmx.util.i18n.DictionaryWrapper;
import com.tmx.beng.base.StatusDictionary;

/**
 * this class is super for all transaction form
 * @author Andrey Nagorniy
 */
abstract public class AbstractInfoTransactionForm extends BasicActionForm {

    protected Long selectedTransactionId;
    protected EditBox editBoxTransactionId;
    protected TimeSelector timeSelectorTransactionTime;
    protected TimeSelector timeSelectorClientTime;
    protected EditBox editBoxTransactionGUID;
    protected TextArea textAreaClientTransactionNum;
    protected EditBox editBoxExecutedUser;
    protected EditBox editBoxAmount;
    protected EditBox editBoxType;
    protected EditBox editBoxOperationType;

    protected EditBox editBoxStatus;
    protected TextArea textAreaStatusDescription;

    protected EditBox editBoxTransErrCode;
    protected EditBox editBoxAccountNumber;
    protected EditBox editBoxOperator;
    protected TextArea textAreaTransErrMessage;

    //Button----------------------------------
    protected Button onCancelButton;

    private DictionaryWrapper transactionStatusDictionary;

    protected void init() {
        //initialze and register controls
        editBoxTransactionId = new EditBox();
        editBoxTransactionId.setReadonly(true);

        timeSelectorTransactionTime = new TimeSelector();
        timeSelectorTransactionTime.setReadonly(true);

        timeSelectorClientTime = new TimeSelector();
        timeSelectorClientTime.setReadonly(true);

        textAreaClientTransactionNum = new TextArea();
        textAreaClientTransactionNum.setReadonly(true);

        editBoxTransactionGUID = new EditBox();
        editBoxTransactionGUID.setReadonly(true);

        editBoxAmount = new EditBox();
        editBoxAmount.setReadonly(true);

        editBoxStatus = new EditBox();
        editBoxStatus.setReadonly(true);

        textAreaStatusDescription = new TextArea();
        textAreaStatusDescription.setReadonly(true);

        editBoxExecutedUser = new EditBox();
        editBoxExecutedUser.setReadonly(true);

        transactionStatusDictionary = new DictionaryWrapper(getLocale(),
                                                            StatusDictionary.class,
                                                            StatusDictionary.STATUS_MSGS_BUNDLE);

        editBoxTransErrCode = new EditBox();
        editBoxTransErrCode.setReadonly(true);

        textAreaTransErrMessage = new TextArea();
        textAreaTransErrMessage.setReadonly(true);

        editBoxType = new EditBox();
        editBoxType.setReadonly(true);

        editBoxOperationType = new EditBox();
        editBoxOperationType.setReadonly(true);


        onCancelButton = new Button();
        onCancelButton.setLabelKey("ctrl.button.label.back");

        editBoxOperator = new EditBox();
        editBoxOperator.setReadonly(true);

        editBoxAccountNumber = new EditBox();
        editBoxAccountNumber.setReadonly(true);
    }

    protected void bindData(AbstractTransaction abstractTransaction) {
        editBoxTransactionId.setValue(abstractTransaction.getTansactionId().toString());
        timeSelectorTransactionTime.setTime(abstractTransaction.getTransactionTime());
        timeSelectorClientTime.setTime(abstractTransaction.getClientTime());
        textAreaClientTransactionNum.setValue(abstractTransaction.getCliTransactionNum());
        editBoxAmount.setValue(abstractTransaction.getAmount().toString());
        editBoxStatus.setValue(abstractTransaction.getStatus());
        editBoxTransactionGUID.setValue(abstractTransaction.getTransactionGUID());
        textAreaStatusDescription.setValue(transactionStatusDictionary.getValue(abstractTransaction.getStatus()));

        //for transaction error
        if(abstractTransaction.getError() != null){
            editBoxTransErrCode.setValue(abstractTransaction.getError().getCode());
            textAreaTransErrMessage.setValue(abstractTransaction.getError().getMessage());
        }

        //for transaction type
        if(abstractTransaction.getType() != null){
            editBoxType.setValue(abstractTransaction.getType().getName());
        }
        if(abstractTransaction.getOperationType() != null){
            editBoxOperationType.setValue(abstractTransaction.getOperationType().getName());
        }
        if (abstractTransaction.getClientTransaction() != null) {
            if(abstractTransaction.getClientTransaction().getAccountNumber() != null) {
                editBoxAccountNumber.setValue(abstractTransaction.getClientTransaction().getAccountNumber());
            } else {
                editBoxAccountNumber.setValue("???");
            }

            if(abstractTransaction.getClientTransaction().getOperator().getOperatorId() != null) {
                try {
                    editBoxOperator.setValue(new OperatorManager().getOperator(abstractTransaction.getClientTransaction().getOperator().getOperatorId()).getName());
                } catch (DatabaseException e) {
                    e.printStackTrace();
                }
            } else {
                editBoxOperator.setValue("???");
            }
        }

    }

    public Button getOnCancelButton() {
        return onCancelButton;
    }

    public void setOnCancelButton(Button onCancelButton) {
        this.onCancelButton = onCancelButton;
    }

    public Long getSelectedTransactionId() {
        return selectedTransactionId;
    }

    public void setSelectedTransactionId(Long selectedTransactionId) {
        this.selectedTransactionId = selectedTransactionId;
    }

    public EditBox getEditBoxTransactionId() {
        return editBoxTransactionId;
    }

    public void setEditBoxTransactionId(EditBox editBoxTransactionId) {
        this.editBoxTransactionId = editBoxTransactionId;
    }

    public TimeSelector getTimeSelectorTransactionTime() {
        return timeSelectorTransactionTime;
    }

    public void setTimeSelectorTransactionTime(TimeSelector timeSelectorTransactionTime) {
        this.timeSelectorTransactionTime = timeSelectorTransactionTime;
    }

    public TimeSelector getTimeSelectorClientTime() {
        return timeSelectorClientTime;
    }

    public void setTimeSelectorClientTime(TimeSelector timeSelectorClientTime) {
        this.timeSelectorClientTime = timeSelectorClientTime;
    }

    public EditBox getEditBoxTransactionGUID() {
        return editBoxTransactionGUID;
    }

    public void setEditBoxTransactionGUID(EditBox editBoxTransactionGUID) {
        this.editBoxTransactionGUID = editBoxTransactionGUID;
    }

    public TextArea getTextAreaClientTransactionNum() {
        return textAreaClientTransactionNum;
    }

    public void setTextAreaClientTransactionNum(TextArea textAreaClientTransactionNum) {
        this.textAreaClientTransactionNum = textAreaClientTransactionNum;
    }

    public EditBox getEditBoxAmount() {
        return editBoxAmount;
    }

    public void setEditBoxAmount(EditBox editBoxAmount) {
        this.editBoxAmount = editBoxAmount;
    }

    public EditBox getEditBoxStatus() {
        return editBoxStatus;
    }

    public void setEditBoxStatus(EditBox editBoxStatus) {
        this.editBoxStatus = editBoxStatus;
    }

    public TextArea getTextAreaStatusDescription() {
        return textAreaStatusDescription;
    }

    public void setTextAreaStatusDescription(TextArea textAreaStatusDescription) {
        this.textAreaStatusDescription = textAreaStatusDescription;
    }

    public DictionaryWrapper getTransactionStatusDictionary() {
        return transactionStatusDictionary;
    }

    public void setTransactionStatusDictionary(DictionaryWrapper transactionStatusDictionary) {
        this.transactionStatusDictionary = transactionStatusDictionary;
    }

    public EditBox getEditBoxTransErrCode() {
        return editBoxTransErrCode;
    }

    public void setEditBoxTransErrCode(EditBox editBoxTransErrCode) {
        this.editBoxTransErrCode = editBoxTransErrCode;
    }

    public TextArea getTextAreaTransErrMessage() {
        return textAreaTransErrMessage;
    }

    public void setTextAreaTransErrMessage(TextArea textAreaTransErrMessage) {
        this.textAreaTransErrMessage = textAreaTransErrMessage;
    }

    public EditBox getEditBoxType() {
        return editBoxType;
    }

    public void setEditBoxType(EditBox editBoxType) {
        this.editBoxType = editBoxType;
    }

    public EditBox getEditBoxOperationType() {
        return editBoxOperationType;
    }

    public void setEditBoxOperationType(EditBox editBoxOperationType) {
        this.editBoxOperationType = editBoxOperationType;
    }

    public EditBox getEditBoxExecutedUser() {
        return editBoxExecutedUser;
    }

    public void setEditBoxExecutedUser(EditBox editBoxExecutedUser) {
        this.editBoxExecutedUser = editBoxExecutedUser;
   }

    public EditBox getEditBoxAccountNumber() {
        return editBoxAccountNumber;
    }

    public void setEditBoxAccountNumber(EditBox editBoxAccountNumber) {
        this.editBoxAccountNumber = editBoxAccountNumber;
    }

    public EditBox getEditBoxOperator() {
        return editBoxOperator;
    }

    public void setEditBoxOperator(EditBox editBoxOperator) {
        this.editBoxOperator = editBoxOperator;
    }
}
