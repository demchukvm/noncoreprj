package com.tmx.web.actions.core.restriction.operator_restriction;

import com.tmx.web.actions.core.base.AbstractBrowseAction;
import com.tmx.web.forms.core.restriction.BrowseFunctionTypesForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForm;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletRequest;


public class BrowseOperatorFuncTypesAction extends AbstractBrowseAction {

    public ActionForward defaultAction(ActionMapping mapping, ActionForm form, HttpServletRequest request,
                                       HttpServletResponse response, String methodName) throws Throwable {

        BrowseFunctionTypesForm abstractBrowseForm = (BrowseFunctionTypesForm) form;
        abstractBrowseForm.setUpCategory("OPERATOR_RESTRICTION", "RESTRICTION");
        return super.defaultAction(mapping, form, request, response, methodName);
    }

    protected String getForwardName() {
        return "browseFunctionTypesPage";
    }

}
