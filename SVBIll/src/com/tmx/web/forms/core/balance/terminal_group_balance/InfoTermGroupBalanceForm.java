package com.tmx.web.forms.core.balance.terminal_group_balance;

import com.tmx.web.forms.core.balance.AbstractInfoBalanceForm;
import com.tmx.web.controls.*;
import com.tmx.web.controls.table.Table;
import com.tmx.web.controls.table.ControlManagerTable;
import com.tmx.as.entities.bill.balance.AbstractBalance;
import com.tmx.as.entities.bill.balance.TerminalGroupBalance;
import com.tmx.as.entities.bill.terminal_group.TerminalGroup;


public class InfoTermGroupBalanceForm extends AbstractInfoBalanceForm {

    private BrowseBox browseBoxAddTermGroup;

     protected void initButtonsAction() {
        onApplyButton.setAction("/core/balance/terminal_group_balance/infoTerminalGroupBalance");
        onCancelButton.setAction("/core/balance/terminal_group_balance/manageTerminalGroupBalances");
        onSaveButton.setAction("/core/balance/terminal_group_balance/infoTerminalGroupBalance");
        onChangeBalanceButton.setAction("/core/balance/terminal_group_balance/changeTerminalGroupBalance");
    }

    protected ScrollBox initScrollBoxBalanceOwner() {
        return scrollBoxBalanceOwner = new ScrollBox(){
            protected String getKey(Object dataListElement) {
                return ((TerminalGroup)dataListElement).getId().toString();
            }
            protected String getValue(Object dataListElement) {
                return ((TerminalGroup)dataListElement).getLogin();
            }
        };
    }

    protected ControlManagerTable initTransactionsTable() {
        ControlManagerTable transactionsTable = new ControlManagerTable();
        transactionsTable.setRefreshActionName("/core/balance/terminal_balance/infoTerminalBalance");
        transactionsTable.setRefreshCommandName("refreshTransactionsTable");
        return transactionsTable;
    }

    protected ControlManagerTable initRestrictionsTable() {

        return null;
    }

    protected ControlManagerTable initTariffsTable() {

        return null;
    }

    protected void bindData(AbstractBalance balance) {}

    protected void init(){
        super.init();

        browseBoxAddTermGroup = new BrowseBox();
        browseBoxAddTermGroup.setAction("/core/balance/terminal_group_balance/terminalGroupBrowseAction");
        browseBoxAddTermGroup.setCommand("openBrowseWindow");
    }

    public AbstractBalance populateFromControls() {
        Long termGroupId = PopulateUtil.getLongKey(browseBoxAddTermGroup);
        TerminalGroupBalance balance = new TerminalGroupBalance();
        TerminalGroup terminalGroup = new TerminalGroup();
        terminalGroup.setTerminalGroupId(termGroupId);
        balance.setTerminalGroup(terminalGroup);
        return balance;
    }

    public BrowseBox getBrowseBoxAddTermGroup() {
        return browseBoxAddTermGroup;
    }

    public void setBrowseBoxAddTermGroup(BrowseBox browseBoxAddTermGroup) {
        this.browseBoxAddTermGroup = browseBoxAddTermGroup;
    }
}
