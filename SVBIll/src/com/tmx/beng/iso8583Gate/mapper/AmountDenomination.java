package com.tmx.beng.iso8583Gate.mapper;

/**

 */
public class AmountDenomination {
    private String protocolVersion = null;
    private float coefficient = 1;

    public AmountDenomination(){
    }

    public String getProtocolVersion() {
        return protocolVersion;
    }

    public void setProtocolVersion(String protocolVersion) {
        this.protocolVersion = protocolVersion;
    }

    public float getCoefficient() {
        return coefficient;
    }

    /** set only positive value. */
    public void setCoefficient(float coefficient) {
        this.coefficient = coefficient > 0 ? coefficient : 1;
    }
}
