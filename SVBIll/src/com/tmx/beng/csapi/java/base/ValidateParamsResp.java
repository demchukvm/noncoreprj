package com.tmx.beng.csapi.java.base;

import com.tmx.beng.base.dbfunctions.ValidationError;

import java.util.List;


public class ValidateParamsResp {
    private List<ValidationError> errors;

    ValidateParamsResp(List<ValidationError> errors) {
        this.errors = errors;
    }

    public Boolean isCorrect() {
        return errors.isEmpty();
    }

    public List<ValidationError> getValidationErrors() {
        return errors;
    }
}
