package com.tmx.web.forms.core.auth;

import com.tmx.web.base.BasicActionForm;
import com.tmx.web.controls.EditBox;
import com.tmx.web.controls.Button;

public class LoginForm extends BasicActionForm
{
    private EditBox ebLogin;
    private EditBox ebPassword;

    private Button bEnter;

    protected void init()
    {
        initEditBoxes();
        initButton();
    }

    private void initEditBoxes()
    {
        ebLogin = new EditBox();
        ebLogin.setMandatory(true);

        ebPassword = new EditBox();
        ebPassword.setMandatory(true);
    }

    private void initButton()
    {
        bEnter = new Button();
        bEnter.setAction("/core/auth/login");
        bEnter.setCommand("login");
        bEnter.setLabelKey("login.button.enter");
    }

    public EditBox getEbLogin() {
        return ebLogin;
    }

    public void setEbLogin(EditBox ebLogin) {
        this.ebLogin = ebLogin;
    }

    public EditBox getEbPassword() {
        return ebPassword;
    }

    public void setEbPassword(EditBox ebPassword) {
        this.ebPassword = ebPassword;
    }

    public Button getbEnter() {
        return bEnter;
    }

    public void setbEnter(Button bEnter) {
        this.bEnter = bEnter;
    }



    //    private EditBox editBoxLogin;
//    private EditBox editBoxPassword;
//    private Button onLoginButton;

//    protected void init() {
//        //initialze and register controls
//        editBoxLogin = new EditBox();
//        editBoxLogin.setMandatory(true);
//        editBoxPassword = new EditBox();
//        editBoxPassword.setMandatory(true);
//
//        onLoginButton = new Button();
//        onLoginButton.setLabelKey("ctrl.button.label.login");
//        onLoginButton.setAction("/core/auth/login");
//        onLoginButton.setCommand("login");
//    }


//    public EditBox getEditBoxLogin() {
//        return editBoxLogin;
//    }
//
//    public void setEditBoxLogin(EditBox editBoxLogin) {
//        this.editBoxLogin = editBoxLogin;
//    }
//
//    public EditBox getEditBoxPassword() {
//        return editBoxPassword;
//    }
//
//    public void setEditBoxPassword(EditBox editBoxPassword) {
//        this.editBoxPassword = editBoxPassword;
//    }
//
//    public Button getOnLoginButton() {
//        return onLoginButton;
//    }
//
//    public void setOnLoginButton(Button onLoginButton) {
//        this.onLoginButton = onLoginButton;
//    }
}
