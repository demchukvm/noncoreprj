package com.tmx.web.actions.core.restriction.seller_restriction;

import com.tmx.as.blogic.restriction.SellerRestrictionManager;
import com.tmx.as.blogic.restriction.AbstractRestrictionManager;
import com.tmx.as.entities.bill.restriction.AbstractRestriction;
import com.tmx.as.entities.bill.restriction.SellerRestriction;
import com.tmx.as.entities.general.user.User;
import com.tmx.as.exceptions.DatabaseException;

import com.tmx.web.actions.core.restriction.AbstractInfoRestrictionAction;
import com.tmx.web.actions.core.restriction.AbstractManageRestrictionsAction;
import com.tmx.web.forms.core.restriction.AbstractInfoRestrictionForm;
import com.tmx.web.forms.core.restriction.seller_restriction.InfoSellerRestrictionForm;
import com.tmx.web.forms.core.base.AbstractFormFactory;
import com.tmx.web.base.WebException;


public class InfoSellerRestrictionAction extends AbstractInfoRestrictionAction {

    protected void preloadForCreate(AbstractInfoRestrictionForm form, AbstractRestriction restriction, User loggedInUser) throws DatabaseException, WebException {
        InfoSellerRestrictionForm restrForm = (InfoSellerRestrictionForm) form;
        String sellerId = getRoadmap().getParameterValue(getName(), AbstractManageRestrictionsAction.PARAMETER_OWNER_NAME, true).toString();
        AbstractFormFactory.getInstance(loggedInUser).setUpForm(restrForm);
        ((InfoSellerRestrictionForm)restrForm).getScrollBoxSeller().setSelectedKey(sellerId);
//        prepareRoadmap();
    }

    private void prepareRoadmap() throws WebException {
        getRoadmap().collectParameter("/core/restriction/seller_restriction/sellerRestrictionParameter", "infoAction", getName());
        getRoadmap().collectParameter("/core/restriction/seller_restriction/sellerRestrictionParameter", "paramAction", "/core/restriction/seller_restriction/sellerRestrictionParameter");        
    }

    protected void preloadForUpdate(AbstractInfoRestrictionForm form, AbstractRestriction restriction, User loggedInUser) throws DatabaseException, WebException {
        InfoSellerRestrictionForm restrForm = (InfoSellerRestrictionForm) form;
        AbstractFormFactory.getInstance(loggedInUser).setUpForm(restrForm);
        restrForm.getScrollBoxSeller().setSelectedKey(((SellerRestriction)restriction).getSeller().getSellerId().toString());
        restrForm.getScrollBoxSeller().setReadonly(true);
//        prepareRoadmap();
    }
    protected void preloadForUpdate(AbstractInfoRestrictionForm form, AbstractRestriction restriction) throws DatabaseException {
        //do nothing
    }

    protected AbstractRestrictionManager getRestrictionManager() {
        return new SellerRestrictionManager();
    }//on scrollBoxFunctionTypes change

}
