package com.tmx.beng.httpsgate.dacard.pdu;

import org.apache.log4j.Logger;

import java.util.List;
import java.util.LinkedList;
import java.util.Map;
import java.util.HashMap;
import java.io.*;
import java.nio.ByteBuffer;

import com.tmx.util.HexConverter;

/**
 */
public class DacardMessage {
    /** MAC sign of this message */
    private MacSign mac;
    /** Ordered list of data elements */
    private List<DataElement> dataElementsOrdered;
    /** data elements by name */
    private Map<DataElementName,DataElement> dataElementsByName;
    private Logger logger;

    DacardMessage(byte[] sessionKey, Logger logger){
        mac = new MacSign(sessionKey);
        dataElementsOrdered = new LinkedList<DataElement>();
        dataElementsByName = new HashMap<DataElementName,DataElement>();
        this.logger = logger; 
    }

    void parse(InputStream is) throws DacardPadException, DacardCypherException{
        logger.debug("Parsing incoming message...");
        //1. read transaction size and convert it from little endian to big endian format
        int transactionSize;
        try{
            byte[] buffer = new byte[2];
            is.read(buffer);
            transactionSize = ByteUtil.convertLittleEndianToShort(buffer);
            if(transactionSize <= 0)
                throw new DacardPadException("Empty answer from server (transaction size is 0)");
            logger.debug("MSG_SIZE: " + transactionSize);
        }
        catch (IOException e){
            throw new DacardPadException("Failed to parse Dacard transaction size");
        }

        try{
            byte[] macAndData = new byte[transactionSize];
            is.read(macAndData);
            is = new ByteArrayInputStream(macAndData); // replace socket input stream with memory stream
            logger.debug(new HexConverter().getStringFromHex(macAndData));
        }
        catch (IOException e){
            throw new DacardPadException("Failed to read Dacard transaction from socket stream", e);
        }

        //2. Read and validate MAC
        transactionSize = transactionSize - mac.parse(is);

        //4. Validate MAC
        try{
            is.mark(transactionSize); //at this point transactionSize = data field size.
            final byte[] data = new byte[transactionSize];
            is.read(data);
            mac.validateMac(data);
            is.reset();
        }
        catch (IOException e){
            throw new DacardPadException("Failed to read data for MAC", e);
        }

        //3. Read and parse data
        while(transactionSize > 0){
            final DataElement de = new DataElement(logger);
            final int dataElementSize = de.parse(is);
            transactionSize = transactionSize - dataElementSize;
            addDataElement(de);
        }



    }

    void serialize(OutputStream os) throws DacardPadException, DacardCypherException{
        logger.debug("Serializing outgoing message...");
        //1. serialize data in memory first to evaluate its MAC and size
        final ByteArrayOutputStream dataBaos = new ByteArrayOutputStream(); //is used to collect data in and evaluate MAC over it
        final ByteArrayOutputStream msgBaos = new ByteArrayOutputStream(); // is used to collect message in before sending to Dacard gate
        final HexConverter hexConv = new HexConverter();

        for(DataElement de : dataElementsOrdered){
            de.serialize(dataBaos);
        }
        //2. evaluate and serialize MAC
        mac.calculateMac(dataBaos.toByteArray());

        //3. evaluate and serialize transaction size
        final short transactionSize = (short)(mac.getSize() + dataBaos.size());
        try{
            //write to less significant bytes of transaction's size in little-endian format.
            msgBaos.write(ByteUtil.convertShortIntoLittleEndian(transactionSize));
        }
        catch (IOException e){
            throw new DacardPadException("Failed to serialize Dacard transaction size", new String[]{String.valueOf(transactionSize)}, e);
        }

        //4. copy MAC and data from memory to output stream
        try{
            mac.serialize(msgBaos);
            logger.debug("MSG_SIZE (2 bytes) & MSG_MAC (8 bytes): " + hexConv.getStringFromHex(msgBaos.toByteArray())); //skip first 2 bytes and take 8 MAC bytes.
            dataBaos.writeTo(msgBaos);
        }
        catch (IOException e){
            throw new DacardPadException("Failed to copy Dacard transaction MAC and data", e);
        }

        //5. log serialized msg and send it to socket output stream
        logger.debug("MSG_HEX: " + hexConv.getStringFromHex(msgBaos.toByteArray()));
        try{
            msgBaos.writeTo(os);
        }
        catch (IOException e){
            throw new DacardPadException("Failed to write data from memory buffer to socket", e);
        }
    }

    public <T> void addDataElement(DataElementName name, DataElementType type, T value){
        DataElement de = new DataElement<T>(logger);
        de.setName(name);
        de.setType(type);
        de.setValue(value);

        addDataElement(de);
    }

    public void addDataElement(DataElement de){
        dataElementsOrdered.add(de);
        dataElementsByName.put(de.getName(), de);
    }

    public DataElement getDataElement(DataElementName name){
        return dataElementsByName.get(name);
    }

    public DataElement getDataElement(int position){
        return dataElementsOrdered.get(position);
    }
}
