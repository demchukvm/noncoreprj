<%@ page language="java" pageEncoding="UTF-8" contentType="text/html; charset=utf-8" isELIgnored="false" %>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-nested" prefix="nested" %>
<%@ taglib uri="http://tmx.com/taglib/tmx-controls" prefix="tmx_ctrl" %>
<%@ taglib uri="http://tmx.com/taglib/tmx-logic" prefix="tmx_logic" %>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html" %>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean" %>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>

<table width="100%" border="0">
    <!-- LABEL -->
    <%--<tr>--%>
        <%--<td>--%>
            <%--<h4 class="dataLabel">--%>
                <%--<bean:message bundle="core.labels" key="core.page.ui_permission.permission.title"/>--%>
            <%--</h4>--%>
        <%--</td>--%>
    <%--</tr>--%>
    <!-- BUTTONS -->
    <tr>
        <td style="text-align: left">
            <tmx_ctrl:button property="onCreateButton"/>
            <tmx_ctrl:button property="onEditButton"/>
            <tmx_ctrl:button property="onDeleteButton"/>
        </td>
    </tr>
    <!-- FILTER SET -->
    <tr>
        <td valign="top" width="100%" style="text-align: left">
            <tmx_ctrl:panelFilterSet tableName="table">
            <table>
                <tr>
                    <td class="dataLabel">
                        <tmx_ctrl:writeEntityLabel
                                attrName="role"
                                entityClassName="com.tmx.as.entities.general.ui_permission.Permission"/>:
                    </td>

                    <td class="dataField">
                            <tmx_ctrl:selectBox property="table.filterSet.filter(com_tmx_as_entities_general_ui_permission_Permission_role).parameter(value).control"/>
                    </td>

                    <td class="dataLabel">
                        <tmx_ctrl:writeEntityLabel
                                attrName="permissionType"
                                entityClassName="com.tmx.as.entities.general.ui_permission.Permission"/>:
                    </td>

                    <td class="dataField">
                            <tmx_ctrl:selectBox property="table.filterSet.filter(com_tmx_as_entities_general_ui_permission_Permission_permissionType).parameter(value).control"/>
                    </td>
                </tr>
                <tr>
                    <td class="dataLabel">
                        <tmx_ctrl:writeEntityLabel
                                attrName="userInterface"
                                entityClassName="com.tmx.as.entities.general.ui_permission.Permission"/>:
                    </td>

                    <td class="dataField">
                            <tmx_ctrl:browseBox property="table.filterSet.filter(com_tmx_as_entities_general_ui_permission_Permission_userInterface).parameter(value).control" width="290px"/>
                    </td>
                </tr>
            </table>
        </tmx_ctrl:panelFilterSet>
        </td>
    </tr>
    <!-- TABLE -->
    <tr>
        <td width="100%">
            <table class='one_outer_cell_table' cellspacing='0' cellpadding='0' width="100%">
                <tr>
                    <td>
                        <table cellspacing='1' cellpadding='1' width="100%">
                            <!-- Table header -->
                            <tr>
                                <th class="column_header_cell">
                                    <bean:message bundle="core.labels" key="core.common.th.choose"/>
                                </th>
                                <th class="column_header_cell">
                                    <tmx_ctrl:imgTableOrdering tableName="table"
                                                           orderByFieldName="role.roleName"
                                                           labelKey="role"
                                                           labelBundle="as:com.tmx.as.entities.general.ui_permission.Permission"/>
                                </th>
                                <th class="column_header_cell">
                                    <tmx_ctrl:imgTableOrdering tableName="table"
                                                           orderByFieldName="userInterface.type.name"
                                                           labelKey="type"
                                                           labelBundle="as:com.tmx.as.entities.general.ui_permission.UserInterface"/>
                                </th>
                                <th class="column_header_cell">
                                    <tmx_ctrl:imgTableOrdering tableName="table"
                                                           orderByFieldName="userInterface.name"
                                                           labelKey="userInterface"
                                                           labelBundle="as:com.tmx.as.entities.general.ui_permission.Permission"/>
                                </th>
                                <th class="column_header_cell">
                                    <tmx_ctrl:imgTableOrdering tableName="table"
                                                           orderByFieldName="permissionType.name"
                                                           labelKey="permissionType"
                                                           labelBundle="as:com.tmx.as.entities.general.ui_permission.Permission"/>
                                </th>
                            </tr>
                            <nested:iterate id="el"  property="table.data" type="com.tmx.as.entities.general.ui_permission.Permission">
                                <tr>
                                    <td class='column_data_cell'>
                                        <input type="radio" name="selectedPermissionId" value="<c:out value="${el.id}"/>"
                                               checked="false">
                                    </td>
                                    <td class='column_data_cell'>
                                        <tmx_ctrl:write name="el" property="role.roleName" ignore="true"/>
                                    </td>
                                    <td class='column_data_cell'>
                                        <tmx_ctrl:write name="el" property="userInterface.type.name" ignore="true"/>
                                    </td>
                                    <td class='column_data_cell'>
                                        <tmx_ctrl:write name="el" property="userInterface.name" ignore="true"/>
                                    </td>
                                    <td class='column_data_cell'>
                                        <tmx_ctrl:write name="el" property="permissionType.name" ignore="true"/>
                                    </td>
                                </tr>
                            </nested:iterate>
                            <!-- Empty row -->
                            <nested:define id="tableSize" property="table.resultSize" type="java.lang.Integer"/>
                            <c:if test="${tableSize <= 0}">
                                <tr>
                                    <td class='column_data_cell' colspan="5" align="center">
                                        <bean:message bundle="core.labels" key="core.common.th.empty_table" />
                                    </td>
                                </tr>
                            </c:if>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td style="text-align: right">
            <tmx_ctrl:imgTablePaging tableName="table"/>
        </td>
    </tr>
    <!-- BUTTONS -->
    <tr>
        <td style="text-align: left">
            <tmx_ctrl:button property="onCreateButton"/>
            <tmx_ctrl:button property="onEditButton"/>
            <tmx_ctrl:button property="onDeleteButton"/>
        </td>
    </tr>
     <!--INTERACTIVE PANEL -->
    <tr>
        <!--Pack info-->
        <td valign="center" align="center" class="tabForm">
            <table width="100%" border="0">
                <%--<!-- Form header -->--%>
                <tr>
                    <td colspan="2">
                        <h4 class="dataLabel">
                            <bean:message bundle="core.labels" key="core.page.ui_permission.interactive.title"/>
                        </h4>
                    </td>
                </tr>
                <tr>
                    <!-- readonly -->
                    <td class="dataLabel">
                        <bean:message bundle="core.labels" key="core.page.ui_permission.interactivePanel.title"/>:
                        <tmx_ctrl:checkBox property="interactiveCheckBox"/>
                        <tmx_ctrl:button property="interactiveButton"/>
                    </td>
                    <td class="dataField">
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
