package com.tmx.web.forms.core.tools;

import com.tmx.web.controls.Button;
import com.tmx.web.base.BasicActionForm;
import java.util.ArrayList;

public class GatesBlockForm extends BasicActionForm
{
    private Button btnRemoveMediumAccesses;
    private Button btnRestoreMediumAccesses;

    private ArrayList gateOn;
    private ArrayList gateOff;

    private ArrayList productGate;

    private boolean cb_avancel_gate_1;
    private boolean cb_avancel_gate_2;
    private boolean cb_avancel_gate_3;
    private boolean cb_beeline_gate;
    private boolean cb_citypay_gate;
    private boolean cb_citypay_gate_life;
    private boolean cb_cyberplat_gate;
    private boolean cb_dacard_gate;
    private boolean cb_kyivstar_bonus_commission_gate;
    private boolean cb_kyivstar_bonus_no_commission_gate;
    private boolean cb_kyivstar_exclusive_gate;
    private boolean cb_kyivstar_gate;
    private boolean cb_life_gate;
    private boolean cb_test_gate;
    private boolean cb_umc_gate;
    private boolean cb_ussd_gate;
    private boolean cb_ussd_gate_reserve;
    private boolean cbAllOn;
    private boolean cbAllOff;

    public void init()
    {
        gateOn = new ArrayList();
        gateOff = new ArrayList();
        productGate = new ArrayList();

        btnRemoveMediumAccesses = new Button();
        btnRemoveMediumAccesses.setAction("/GatesBlockAction");
        btnRemoveMediumAccesses.setLabelKey("ctrl.button.label.blocked");
        btnRemoveMediumAccesses.setCommand("removeMediumAccesses");

        btnRestoreMediumAccesses = new Button();
        btnRestoreMediumAccesses.setAction("/GatesBlockAction");
        btnRestoreMediumAccesses.setLabelKey("ctrl.button.label.unblocked");
        btnRestoreMediumAccesses.setCommand("restoreMediumAccesses");
    }

    public Button getBtnRemoveMediumAccesses() {
        return btnRemoveMediumAccesses;
    }

    public void setBtnRemoveMediumAccesses(Button btnRemoveMediumAccesses) {
        this.btnRemoveMediumAccesses = btnRemoveMediumAccesses;
    }

    public Button getBtnRestoreMediumAccesses() {
        return btnRestoreMediumAccesses;
    }

    public void setBtnRestoreMediumAccesses(Button btnRestoreMediumAccesses) {
        this.btnRestoreMediumAccesses = btnRestoreMediumAccesses;
    }

    public ArrayList getGateOn() {
        return gateOn;
    }

    public void setGateOn(ArrayList gateOn) {
        this.gateOn = gateOn;
    }

    public ArrayList getGateOff() {
        return gateOff;
    }

    public void setGateOff(ArrayList gateOff) {
        this.gateOff = gateOff;
    }

    public ArrayList getProductGate() {
        return productGate;
    }

    public void setProductGate(ArrayList productGate) {
        this.productGate = productGate;
    }

    public boolean isCb_avancel_gate_1() {
        return cb_avancel_gate_1;
    }

    public void setCb_avancel_gate_1(boolean cb_avancel_gate_1) {
        this.cb_avancel_gate_1 = cb_avancel_gate_1;
    }

    public boolean isCb_avancel_gate_2() {
        return cb_avancel_gate_2;
    }

    public void setCb_avancel_gate_2(boolean cb_avancel_gate_2) {
        this.cb_avancel_gate_2 = cb_avancel_gate_2;
    }

    public boolean isCb_avancel_gate_3() {
        return cb_avancel_gate_3;
    }

    public void setCb_avancel_gate_3(boolean cb_avancel_gate_3) {
        this.cb_avancel_gate_3 = cb_avancel_gate_3;
    }

    public boolean isCb_beeline_gate() {
        return cb_beeline_gate;
    }

    public void setCb_beeline_gate(boolean cb_beeline_gate) {
        this.cb_beeline_gate = cb_beeline_gate;
    }

    public boolean isCb_citypay_gate() {
        return cb_citypay_gate;
    }

    public void setCb_citypay_gate(boolean cb_citypay_gate) {
        this.cb_citypay_gate = cb_citypay_gate;
    }

    public boolean isCb_citypay_gate_life() {
        return cb_citypay_gate_life;
    }

    public void setCb_citypay_gate_life(boolean cb_citypay_gate_life) {
        this.cb_citypay_gate_life = cb_citypay_gate_life;
    }

    public boolean isCb_cyberplat_gate() {
        return cb_cyberplat_gate;
    }

    public void setCb_cyberplat_gate(boolean cb_cyberplat_gate) {
        this.cb_cyberplat_gate = cb_cyberplat_gate;
    }

    public boolean isCb_dacard_gate() {
        return cb_dacard_gate;
    }

    public void setCb_dacard_gate(boolean cb_dacard_gate) {
        this.cb_dacard_gate = cb_dacard_gate;
    }

    public boolean isCb_kyivstar_bonus_commission_gate() {
        return cb_kyivstar_bonus_commission_gate;
    }

    public void setCb_kyivstar_bonus_commission_gate(boolean cb_kyivstar_bonus_commission_gate) {
        this.cb_kyivstar_bonus_commission_gate = cb_kyivstar_bonus_commission_gate;
    }

    public boolean isCb_kyivstar_bonus_no_commission_gate() {
        return cb_kyivstar_bonus_no_commission_gate;
    }

    public void setCb_kyivstar_bonus_no_commission_gate(boolean cb_kyivstar_bonus_no_commission_gate) {
        this.cb_kyivstar_bonus_no_commission_gate = cb_kyivstar_bonus_no_commission_gate;
    }

    public boolean isCb_kyivstar_exclusive_gate() {
        return cb_kyivstar_exclusive_gate;
    }

    public void setCb_kyivstar_exclusive_gate(boolean cb_kyivstar_exclusive_gate) {
        this.cb_kyivstar_exclusive_gate = cb_kyivstar_exclusive_gate;
    }

    public boolean isCb_kyivstar_gate() {
        return cb_kyivstar_gate;
    }

    public void setCb_kyivstar_gate(boolean cb_kyivstar_gate) {
        this.cb_kyivstar_gate = cb_kyivstar_gate;
    }

    public boolean isCb_life_gate() {
        return cb_life_gate;
    }

    public void setCb_life_gate(boolean cb_life_gate) {
        this.cb_life_gate = cb_life_gate;
    }

    public boolean isCb_test_gate() {
        return cb_test_gate;
    }

    public void setCb_test_gate(boolean cb_test_gate) {
        this.cb_test_gate = cb_test_gate;
    }

    public boolean isCb_umc_gate() {
        return cb_umc_gate;
    }

    public void setCb_umc_gate(boolean cb_umc_gate) {
        this.cb_umc_gate = cb_umc_gate;
    }

    public boolean isCb_ussd_gate() {
        return cb_ussd_gate;
    }

    public void setCb_ussd_gate(boolean cb_ussd_gate) {
        this.cb_ussd_gate = cb_ussd_gate;
    }

    public boolean isCb_ussd_gate_reserve() {
        return cb_ussd_gate_reserve;
    }

    public void setCb_ussd_gate_reserve(boolean cb_ussd_gate_reserve) {
        this.cb_ussd_gate_reserve = cb_ussd_gate_reserve;
    }

    public boolean isCbAllOn() {
        return cbAllOn;
    }

    public void setCbAllOn(boolean cbAllOn) {
        this.cbAllOn = cbAllOn;
    }

    public boolean isCbAllOff() {
        return cbAllOff;
    }

    public void setCbAllOff(boolean cbAllOff) {
        this.cbAllOff = cbAllOff;
    }
}
