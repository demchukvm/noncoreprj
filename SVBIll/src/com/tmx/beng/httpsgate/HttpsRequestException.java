package com.tmx.beng.httpsgate;

import com.tmx.beng.medaccess.GateException;

import java.util.Locale;

/**
 */
public class HttpsRequestException extends GateException {

    public HttpsRequestException(String msg, String[] params, Throwable e, Locale locale) {
        super(msg, params, e, locale);
    }

    public HttpsRequestException(String msg, String[] params) {
        super(msg, params);
    }

    public HttpsRequestException(String msg, String[] params, Locale locale) {
        super(msg, params, locale);
    }

    public HttpsRequestException(String msg, Throwable e, Locale locale) {
        super(msg, e, locale);
    }

    public HttpsRequestException(String msg, Throwable e) {
        super(msg, e);
    }

    public HttpsRequestException(String msg, Locale locale) {
        super(msg, locale);
    }

    public HttpsRequestException(String msg) {
        super(msg);
    }

    public HttpsRequestException(String msg, String[] params, Throwable e) {
        super(msg, params, e);
    }
}
