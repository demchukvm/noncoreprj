package com.tmx.as.entities.bill.restriction;

import com.tmx.as.base.BasicEntity;
import com.tmx.as.entities.bill.function.FunctionInstance;

import java.io.Serializable;

/**

 */
public abstract class AbstractRestriction extends BasicEntity{
    private Long restrictionId;
    private String name;
    private FunctionInstance functionInstance;

    //------------Properties
    public Long getRestrictionId() {
        return restrictionId;
    }

    public void setRestrictionId(Long restrictionId) {
        this.restrictionId = restrictionId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public FunctionInstance getFunctionInstance() {
        return functionInstance;
    }

    public void setFunctionInstance(FunctionInstance functionInstance) {
        this.functionInstance = functionInstance;
    }

    public Serializable getId() {
        return getRestrictionId();
    }

    public int getValid() {
        return valid;
    }

    public void setValid(int valid) {
        this.valid = valid;
    }

}
