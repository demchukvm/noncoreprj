package com.tmx.beng.iso8583Gate.keygen;

import java.util.List;
import java.util.ArrayList;

/**

 */
public class KeyProfile {
    private List keys;
    private final int MAX_COLLECTION_SIZE = 12;


    public void generate(String terminalName, byte[] hklmKey, int collectionSize) throws Exception{
        keys = new ArrayList();
        Key key;
        for(int i=0; i < collectionSize && i < MAX_COLLECTION_SIZE; i++){
            key = new Key();
            key.generate(terminalName, hklmKey, i+1 /** start index from 1 */);
            keys.add(key);
        }
    }

    public String toCsvString(){
        if(keys != null && keys.size() > 0){
            StringBuffer sb = new StringBuffer();
            for(int i=0; i<keys.size(); i++)
                sb.append(((Key)keys.get(i)).toCsvString()).append("\n");    
            return sb.toString();
        }
        return "EMPTY";
    }

    public static String toCsvHeaderString(){
        return Key.toCsvHeaderString();
    }

    public String toXmlString(){
        if(keys != null && keys.size() > 0){
            StringBuffer sb = new StringBuffer("<keyprofile>");
            for(int i=0; i<keys.size(); i++)
                sb.append(((Key)keys.get(i)).toXmlString());    
            sb.append("</keyprofile>");
            return sb.toString();
        }
        else{
            return "<keyprofile/>";
        }
    }

}
