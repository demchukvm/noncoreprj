package com.tmx.as.base;

import com.tmx.util.InitException;

/**
    implementators should supports hot reloading of configuration
 */
public interface Reconfigurable {
    /** reload configuration  */
    public void reload() throws InitException;
}
