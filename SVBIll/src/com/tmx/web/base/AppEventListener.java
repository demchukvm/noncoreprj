package com.tmx.web.base;

import com.tmx.util.InitException;
import javax.servlet.ServletContextEvent;

import org.apache.log4j.Logger;


public class AppEventListener implements javax.servlet.ServletContextListener{
    private Logger logger = Logger.getLogger("web.AppEventListener");

    /** Initialize application */
    public void contextInitialized(ServletContextEvent servletContextEvent) {
        try{
            ApplicationEnvironment.init(servletContextEvent.getServletContext());
        }
        catch(InitException e){
            logger.fatal("Critical initialization exception: "+e.toString());
            System.exit(-1);
        }
        logger.info("Tomcat context listener has been initialized.");
    }

    public void contextDestroyed(ServletContextEvent servletContextEvent) {
        //To change body of implemented methods use File | Settings | File Templates.
    }
}
