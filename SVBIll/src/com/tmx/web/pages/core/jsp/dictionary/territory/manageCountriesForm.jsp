<%@ page language="java" pageEncoding="UTF-8" contentType="text/html; charset=utf-8" isELIgnored="false" %>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-nested" prefix="nested" %>
<%@ taglib uri="http://tmx.com/taglib/tmx-controls" prefix="tmx_ctrl" %>
<%@ taglib uri="http://tmx.com/taglib/tmx-logic" prefix="tmx_logic" %>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html" %>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean" %>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>

<html:form action="/core/dictionary/territory/manageCountries">
<table width="100%" border="0">
    <!-- BUTTONS -->
    <tr>
        <td style="text-align: left">
            <tmx_ctrl:button property="onCreateButton"/>
            <tmx_ctrl:button property="onEditButton"/>
            <tmx_ctrl:button property="onDeleteButton"/>
        </td>
    </tr>
    <!-- FILTER SET -->
    <tr>
        <td valign="top" width="100%"  style="text-align: left">
            <tmx_ctrl:panelFilterSet tableName="table">
            <table>
                <tr>
                    <td class="dataLabel">
                        <tmx_ctrl:writeEntityLabel
                                attrName="countryId"
                                entityClassName="com.tmx.as.entities.network.territory.Country"/>:
                    </td>

                    <td class="dataField">
                        <tmx_ctrl:editBoxFilter
                                property="table"
                                filterName="countryId"
                                parameterName="value"/>
                    </td>

                    <td class="dataLabel">
                        <tmx_ctrl:writeEntityLabel
                                attrName="shortName"
                                entityClassName="com.tmx.as.entities.network.territory.Country"/>:
                    </td>

                    <td class="dataField">
                        <tmx_ctrl:editBoxFilter
                                property="table"
                                filterName="shortName"
                                parameterName="value"/>
                    </td>

                    <td class="dataLabel">
                        <tmx_ctrl:writeEntityLabel
                                attrName="name"
                                entityClassName="com.tmx.as.entities.network.territory.Country"/>:
                    </td>

                    <td class="dataField">
                        <tmx_ctrl:editBoxFilter
                                property="table"
                                filterName="name"
                                parameterName="value"/>
                    </td>
                </tr>
            </table>
        </tmx_ctrl:panelFilterSet>
        </td>
    </tr>
    <!-- TABLE -->
    <tr>
        <td width="100%">
            <table class='one_outer_cell_table' cellspacing='0' cellpadding='0' width="100%">
                <tr>
                    <td>
                        <table cellspacing='1' cellpadding='1' width="100%">
                            <!-- Table header -->
                            <tr>
                                <th class="column_header_cell">
                                    <bean:message bundle="core.labels" key="core.common.th.choose"/>
                                </th>

                                <th class="column_header_cell">
                                    <tmx_ctrl:imgTableOrdering tableName="table"
                                                               orderByFieldName="countryId"
                                                               entityClassName="com.tmx.as.entities.network.territory.Country"/>
                                </th>

                                <th class="column_header_cell">
                                    <tmx_ctrl:imgTableOrdering tableName="table"
                                                               orderByFieldName="shortName"
                                                               entityClassName="com.tmx.as.entities.network.territory.Country"/>
                                </th>

                                <th class="column_header_cell">
                                    <tmx_ctrl:imgTableOrdering tableName="table"
                                                               orderByFieldName="name"
                                                               entityClassName="com.tmx.as.entities.network.territory.Country"/>
                                </th>
                            </tr>
                            <nested:iterate id="el" property="table.data" type="com.tmx.as.entities.network.territory.Country">
                                <tr>
                                    <td class='column_data_cell'>
                                        <input type="radio" name="selectedId" value="<c:out value="${el.id}" />"
                                               checked="false">
                                    </td>
                                    <td class='column_data_cell'>
                                        <fmt:formatNumber value="${el.countryId}" />
                                    </td>
                                    <td class='column_data_cell'>
                                        <c:out value="${el.shortName}" />
                                    </td>
                                    <td class='column_data_cell'>
                                        <c:out value="${el.name}" />
                                    </td>
                                </tr>
                            </nested:iterate>
                            <!-- Empty row -->
                            <nested:define id="tableSize" property="table.resultSize" type="java.lang.Integer"/>
                            <c:if test="${tableSize <= 0}">
                                <tr>
                                    <td class='column_data_cell' colspan="4" align="center">
                                        <bean:message bundle="core.labels" key="core.common.th.empty_table" />
                                    </td>
                                </tr>
                            </c:if>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td style="text-align: right">
            <tmx_ctrl:imgTablePaging maxBookmarkCount="10" tableName="table"/>
        </td>
    </tr>
    <!-- BUTTONS -->
    <tr>
        <td style="text-align: left">
            <tmx_ctrl:button property="onCreateButton"/>
            <tmx_ctrl:button property="onEditButton"/>
            <tmx_ctrl:button property="onDeleteButton"/>
        </td>
    </tr>
</table>
</html:form>