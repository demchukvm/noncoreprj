package com.tmx.web.base;

/**
 Uses
 /WEB-XML/role_behavior_mapping.xml and
 /WEB-XML/action_behavior_mapping.xml to evaluate
 role-form init behavior..

 @See action_behavior_mapping.xsd
 @See role_behavior_mapping.xsd
 for details
 
 */
public abstract class BasicBehaviorizedActionForm extends BasicActionForm{
    //todo RM implement init method dispatherization based on role behavior mapping
    //do not use introspection to call default method!
}
