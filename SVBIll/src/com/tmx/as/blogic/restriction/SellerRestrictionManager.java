package com.tmx.as.blogic.restriction;

import com.tmx.as.entities.bill.restriction.AbstractRestriction;
import com.tmx.as.entities.bill.restriction.SellerRestriction;
import com.tmx.as.entities.bill.seller.Seller;
import com.tmx.as.exceptions.DatabaseException;
import com.tmx.as.blogic.BlogicException;
import com.tmx.as.base.FilterWrapper;
import com.tmx.as.base.QueryParameterWrapper;

import java.util.List;
import java.util.Iterator;


public class SellerRestrictionManager extends AbstractRestrictionManager{


    public void saveRestriction(AbstractRestriction rest) throws DatabaseException, BlogicException {
        SellerRestriction sellerRest = (SellerRestriction) rest;
        try {
            BEGIN(rest);
            SAVE(sellerRest.getFunctionInstance());
            if(sellerRest.getSeller() == null)
               throw new BlogicException("err.seller_restriction_manager.save_restriction.absent_seller_in_seller_restriction");
            if(sellerRest.getSeller().getSellerId() == null)
                throw new BlogicException("err.seller_restriction_manager.save_restriction.absent_seller_id_in_seller");
            SAVE(sellerRest);
            COMMIT(rest);
        } catch (DatabaseException e) {
            ROLLBACK(rest);
            throw e;
        }
    }

    public SellerRestriction getRestriction(Long restId) throws DatabaseException, BlogicException {
        return (SellerRestriction) RETRIEVE(SellerRestriction.class, restId, new String[]{"seller","functionInstance.functionType"});
    }

    public List<SellerRestriction> getAllRestrictions(Long sellerId) throws DatabaseException, BlogicException {
        QueryParameterWrapper selectedSellerId = new QueryParameterWrapper("selectedSellerId", sellerId);
        List<SellerRestriction> result = EXECUTE_QUERY(SellerRestriction.class, "getAllRestrictions", new QueryParameterWrapper[]{selectedSellerId});
        return result;
    }

    public void deleteRestriction(Long restId) throws DatabaseException, BlogicException {
        DELETE(SellerRestriction.class, restId);
    }

    protected void copySpecificRestriction(Long restId, Long destOwnerId) throws DatabaseException, BlogicException {
        BEGIN("svbill");
            Seller seller = new Seller();
            seller.setSellerId(destOwnerId);
            SellerRestriction restriction = (SellerRestriction) RETRIEVE(SellerRestriction.class, restId, new String[]{"functionInstance.functionType","functionInstance.functionParameters"});

            if (restriction == null)
                throw new BlogicException("err.seller_restriction_manager.copy_restriction.absent_tariff_in_db");

            try {
                SellerRestriction cloneRestriction = (SellerRestriction) restriction.clone();
                cloneRestriction.setSeller(seller);

                if(cloneRestriction.getFunctionInstance() == null)
                    throw new BlogicException("err.seller_restriction_manager.copy_restriction.function_instance_is_absent");

                SAVE(cloneRestriction.getFunctionInstance());
                SAVE(cloneRestriction);
            } catch (CloneNotSupportedException e) {
                ROLLBACK("svbill");
                throw new BlogicException("err.seller_restriction_manager.copy_restriction.clone_fail",e);
            }
        COMMIT("svbill");
    }

    protected void copyAllSpecificRestrictions(Long srcOwnerId, Long destOwnerId) throws DatabaseException, BlogicException {
        BEGIN("svbill");
            FilterWrapper bySellerId = new FilterWrapper("by_seller_id");
            bySellerId.setParameter("seller_id", srcOwnerId);
            List restrictions = RETRIEVE_ALL(SellerRestriction.class, new FilterWrapper[]{bySellerId});

            if( restrictions==null || restrictions.size()<=0)
                throw new BlogicException("err.seller_restriction_manager.copy_all_restriction.absent_restriction_in_db");

            Seller seller = new Seller();
            seller.setSellerId(destOwnerId);

            Iterator iterator = restrictions.iterator();
            try {
                while (iterator.hasNext()){
                    SellerRestriction cloneRestriction = (SellerRestriction) ((SellerRestriction)iterator.next()).clone();
                    cloneRestriction.setSeller(seller);

                    if(cloneRestriction.getFunctionInstance() == null)
                        throw new BlogicException("err.seller_tariff_manager.copy_tariff.function_instance_is_absent");

                    SAVE(cloneRestriction.getFunctionInstance());
                    SAVE(cloneRestriction);
                }
            } catch (CloneNotSupportedException e) {
                ROLLBACK("svbill");
                throw new BlogicException("err.seller_restriction_manager.copy_all_restriction.clone_fail");
            }
        COMMIT("svbill");
    }

    public void copyRestriction(Long restId, Long destOwnerId) throws DatabaseException, BlogicException{
        if(restId == null || destOwnerId == null)
            throw new BlogicException("err.abstract_restriction_manager.copy_restriction.incorrect_method_parameter");
        copySpecificRestriction(restId, destOwnerId);
    }

    public void copyAllRestrictions(Long srcOwnerId, Long destOwnerId) throws DatabaseException, BlogicException{
        if(srcOwnerId == null || destOwnerId == null)
            throw new BlogicException("err.abstract_tariff_manager.copy_all_tariff.incorrect_method_parameter");

        copyAllSpecificRestrictions(srcOwnerId, destOwnerId);
    }
}
