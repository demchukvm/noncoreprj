package com.tmx.web.forms.trans_mgmt;

import com.tmx.web.controls.Button;
import com.tmx.web.controls.EditBox;
import com.tmx.web.base.BasicActionForm;

public class TransactionInfoCitypayForm extends BasicActionForm
{
    private Button onRefreshBalance = null;
    private EditBox editBoxCitypayBalance = null;

    public void init()
    {
        onRefreshBalance = new Button();
        onRefreshBalance.setLabelKey("ctrl.button.label.refresh_balance");
        onRefreshBalance.setAction("/transactionInfoCitypayAction");
        onRefreshBalance.setCommand("refreshBalance");

        editBoxCitypayBalance = new EditBox();
        editBoxCitypayBalance.setReadonly(true);
    }

    public Button getOnRefreshBalance() {
        return onRefreshBalance;
    }

    public void setOnRefreshBalance(Button onRefreshBalance) {
        this.onRefreshBalance = onRefreshBalance;
    }

    public EditBox getEditBoxCitypayBalance() {
        return editBoxCitypayBalance;
    }

    public void setEditBoxCitypayBalance(EditBox editBoxCitypayBalance) {
        this.editBoxCitypayBalance = editBoxCitypayBalance;
    }
}
