package com.tmx.beng.httpsgate;

import com.tmx.util.StructurizedException;
import com.tmx.beng.medaccess.GateException;

import java.util.Locale;

/**
 */
public class HttpsGateException extends GateException {

    public HttpsGateException(String msg, String[] params, Throwable e, Locale locale) {
        super(msg, params, e, locale);
    }

    public HttpsGateException(String msg, String[] params) {
        super(msg, params);
    }

    public HttpsGateException(String msg, String[] params, Locale locale) {
        super(msg, params, locale);
    }

    public HttpsGateException(String msg, Throwable e, Locale locale) {
        super(msg, e, locale);
    }

    public HttpsGateException(String msg, Throwable e) {
        super(msg, e);
    }

    public HttpsGateException(String msg, Locale locale) {
        super(msg, locale);
    }

    public HttpsGateException(String msg) {
        super(msg);
    }

    public HttpsGateException(String msg, String[] params, Throwable e) {
        super(msg, params, e);
    }
}
