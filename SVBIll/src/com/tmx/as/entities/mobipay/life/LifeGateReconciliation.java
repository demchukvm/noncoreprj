package com.tmx.as.entities.mobipay.life;

import com.tmx.as.base.BasicEntity;

import java.util.Date;
import java.io.Serializable;

/**
    One reconciliation entity should exists for exactly one each day since the system work begining.
 */
public class  LifeGateReconciliation extends BasicEntity {
    private Long lifeGateReconciliationId;
    private Date reconDate; //yyyyMMdd
    private String stan;
    private String status;
    private String responseCode;
    private Integer attemptsCount;
    private Date startTime;
    private Date endTime;
    //set of fields for operation code #791 ('voucherSaleAndActivation')
    private Integer oc791OperationCount;
    private Integer oc791TotalAmount;
    private String oc791CurrencyCode;
    private String oc791ResponseCode;
    private Integer oc727OperationCount;
    private Integer oc727TotalAmount;
    private String oc727CurrencyCode;
    private String oc727ResponseCode;



    public Long getLifeGateReconciliationId() {
        return lifeGateReconciliationId;
    }

    public void setLifeGateReconciliationId(Long lifeGateReconciliationId) {
        this.lifeGateReconciliationId = lifeGateReconciliationId;
    }

    public Integer getOc791OperationCount() {
        return oc791OperationCount;
    }

    public void setOc791OperationCount(Integer oc791OperationCount) {
        this.oc791OperationCount = oc791OperationCount;
    }

    public Integer getOc791TotalAmount() {
        return oc791TotalAmount;
    }

    public void setOc791TotalAmount(Integer oc791TotalAmount) {
        this.oc791TotalAmount = oc791TotalAmount;
    }

    public String getOc791CurrencyCode() {
        return oc791CurrencyCode;
    }

    public void setOc791CurrencyCode(String oc791CurrencyCode) {
        this.oc791CurrencyCode = oc791CurrencyCode;
    }

    public String getOc791ResponseCode() {
        return oc791ResponseCode;
    }

    public void setOc791ResponseCode(String oc791ResponseCode) {
        this.oc791ResponseCode = oc791ResponseCode;
    }

    public Date getReconDate() {
        return reconDate;
    }

    public void setReconDate(Date reconDate) {
        this.reconDate = reconDate;
    }

    public String getStan() {
        return stan;
    }

    public void setStan(String stan) {
        this.stan = stan;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getAttemptsCount() {
        return attemptsCount;
    }

    public void setAttemptsCount(Integer attemptsCount) {
        this.attemptsCount = attemptsCount;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    public Serializable getId() {
        return getLifeGateReconciliationId();
    }

    public int getValid() {
        return valid;
    }

    public void setValid(int valid) {
        this.valid = valid;
    }

    public Integer getOc727OperationCount() {
        return oc727OperationCount;
    }

    public void setOc727OperationCount(Integer oc727OperationCount) {
        this.oc727OperationCount = oc727OperationCount;
    }

    public Integer getOc727TotalAmount() {
        return oc727TotalAmount;
    }

    public void setOc727TotalAmount(Integer oc727TotalAmount) {
        this.oc727TotalAmount = oc727TotalAmount;
    }

    public String getOc727CurrencyCode() {
        return oc727CurrencyCode;
    }

    public void setOc727CurrencyCode(String oc727CurrencyCode) {
        this.oc727CurrencyCode = oc727CurrencyCode;
    }

    public String getOc727ResponseCode() {
        return oc727ResponseCode;
    }

    public void setOc727ResponseCode(String oc727ResponseCode) {
        this.oc727ResponseCode = oc727ResponseCode;
    }
}