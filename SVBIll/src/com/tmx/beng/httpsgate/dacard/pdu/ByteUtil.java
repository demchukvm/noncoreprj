package com.tmx.beng.httpsgate.dacard.pdu;

import java.util.Arrays;

/**
    Used to convert little-endian and big-endian conversions
 */
public class ByteUtil {

    public static byte[] convertShortIntoLittleEndian(short val){
        byte[] data = new byte[2];
        data[0] = (byte)(val & 0xff); //write most little byte
        data[1] = (byte)((val >>> 8) & 0xff);//write most big byte
        return data;
    }

    public static byte[] convertIntIntoLittleEndian(int val){
        byte[] data = new byte[4];
        data[0] = (byte)(val & 0xff); //write most little byte
        data[1] = (byte)((val >>> 8) & 0xff);//write the next bigger byte
        data[2] = (byte)((val >>> 16) & 0xff);//write the next bigger byte
        data[3] = (byte)((val >>> 24) & 0xff);//write the next bigger byte (the most big byte)
        return data;
    }

    public static byte[] convertLongIntoLittleEndian(long val){
        byte[] data = new byte[8];
        data[0] = (byte)(val & 0xff); //write most little byte
        data[1] = (byte)((val >>> 8) & 0xff);//write the next bigger byte
        data[2] = (byte)((val >>> 16) & 0xff);//write the next bigger byte
        data[3] = (byte)((val >>> 24) & 0xff);//write the next bigger byte
        data[4] = (byte)((val >>> 32) & 0xff);//write the next bigger byte
        data[5] = (byte)((val >>> 40) & 0xff);//write the next bigger byte
        data[6] = (byte)((val >>> 48) & 0xff);//write the next bigger byte
        data[7] = (byte)((val >>> 56) & 0xff);//write the next bigger byte (the most big byte)
        return data;
    }

    public static short convertLittleEndianToShort(byte[] data){
        return (short)((data[0] & 0xff) | (data[1] & 0xff) << 8);
    }

    public static int convertLittleEndianToInt(byte[] data){
        return convertLittleEndianToInt(data, 0);
    }

    public static long convertLittleEndianToLong(byte[] data){
        return ((long)convertLittleEndianToInt(data, 4) << 32) +
                ((long)convertLittleEndianToInt(data, 0) & 0xffffffffL);
    }

    private static int convertLittleEndianToInt(byte[] data, int offset){
        return  (data[offset] & 0xff) | 
                ((data[1 + offset] & 0xff) << 8) |
                ((data[2 + offset] & 0xff) << 16) |
                ((data[3 + offset] & 0xff) << 24);
    }
}
