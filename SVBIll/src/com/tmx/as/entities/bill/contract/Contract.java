package com.tmx.as.entities.bill.contract;

import com.tmx.as.base.BasicEntity;
import com.tmx.as.entities.bill.seller.Seller;

import java.io.Serializable;
import java.util.Date;

public class Contract extends BasicEntity {
    private Long contractId;
    private String contractNumber;
    private Seller seller;
    private Date registrationDate;
    private Date deadLine;

    public Long getContractId() {
        return contractId;
    }

    public void setContractId(Long contractId) {
        this.contractId = contractId;
    }

    public String getContractNumber() {
        return contractNumber;
    }

    public void setContractNumber(String contractNumber) {
        this.contractNumber = contractNumber;
    }

    public Seller getSeller() {
        return seller;
    }

    public void setSeller(Seller seller) {
        this.seller = seller;
    }

    public Serializable getId() {
        return contractId;
    }

    public int getValid() {
        return valid;
    }

    public void setValid(int valid) {
        this.valid = valid;
    }

    public Date getRegistrationDate() {
        return registrationDate;
    }

    public void setRegistrationDate(Date registrationDate) {
        this.registrationDate = registrationDate;
    }

    public Date getDeadLine() {
        return deadLine;
    }

    public void setDeadLine(Date deadLine) {
        this.deadLine = deadLine;
    }
}
