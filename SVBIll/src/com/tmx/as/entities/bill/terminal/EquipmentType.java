package com.tmx.as.entities.bill.terminal;

import com.tmx.as.base.BasicEntity;

import java.io.Serializable;

public class EquipmentType extends BasicEntity {
    private Long equipmentTypeId;
    private String name;
    private String description;

    public Long getEquipmentTypeId() {
        return equipmentTypeId;
    }

    public void setEquipmentTypeId(Long equipmentTypeId) {
        this.equipmentTypeId = equipmentTypeId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Serializable getId() {
        return equipmentTypeId;
    }

    public int getValid() {
        return valid;
    }

    public void setValid(int valid) {
        this.valid = valid;    
    }
}

