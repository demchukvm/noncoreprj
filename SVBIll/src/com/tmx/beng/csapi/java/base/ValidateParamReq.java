package com.tmx.beng.csapi.java.base;

import com.tmx.beng.access.Connection;
import com.tmx.beng.base.*;
import com.tmx.beng.base.BillingMessage;
import com.tmx.beng.base.dbfunctions.ValidationError;

import java.util.List;


public class ValidateParamReq extends ClientRequest<ValidateParamsResp> {
    ValidateParamReq(Connection connect) {
        super(connect);
        requestMsg.setOperationName(com.tmx.beng.base.BillingMessage.O_VALIDATE_FUNCTION_PARAMS);
    }

    public void setActualFunctionParams(List params) {
        requestMsg.setAttribute(BillingMessage.ACTUAL_PARAMS_LIST, params);
    }

    public void setFunctionTypeId(Long functionTypeId) {
        requestMsg.setAttribute(BillingMessage.FUNCTION_TYPE_ID, functionTypeId);
    }

    protected ValidateParamsResp handleResponseMessage(BillingMessage responseMsg) {
        List<ValidationError> errors = (List<ValidationError>) responseMsg.getAttribute(BillingMessage.ERROR_LIST);
        return new ValidateParamsResp(errors);
    }
}
