package com.tmx.web.forms.core.dictionary.seller;

import com.tmx.as.blogic.SellerManager;
import com.tmx.as.blogic.ProcessingManager;
import com.tmx.as.blogic.AddressManager;
import com.tmx.as.entities.bill.address.Address;
import com.tmx.as.entities.bill.bank.Bank;
import com.tmx.as.entities.bill.bank.BankAccount;
import com.tmx.as.entities.bill.person.Person;
import com.tmx.as.entities.bill.seller.Seller;
import com.tmx.as.entities.bill.processing.Processing;
import com.tmx.as.entities.bill.contract.Contract;
import com.tmx.as.entities.general.user.User;
import com.tmx.as.entities.network.territory.City;
import com.tmx.as.entities.network.territory.District;
import com.tmx.as.entities.network.territory.Region;
import com.tmx.as.exceptions.DatabaseException;

import com.tmx.web.base.BasicActionForm;
import com.tmx.web.base.ApplicationEnvironment;
import com.tmx.web.controls.*;

import java.util.Date;
import java.util.List;
import java.util.ArrayList;

public class InfoSellerForm extends BasicActionForm {

    private Long selectedSellerId;//null if new seller is transient
    private Long physAddressId;
    private Long legalAddressId;
    private Long bankId;
    private Long directorId;
    private Long accountantId;
    private Long bankAccountId;
    private Long contractId;

    private EditBox editBoxSellerId;

    private EditBox editBoxName;
    private EditBox editBoxCode;

    private EditBox editBoxZipCodePhys;
    private EditBox editBoxAddressPhys;
    //Billing paremeters
    private CheckBox checkBoxIsAttachRestriction;
    private CheckBox checkBoxIsAttachKSTariff;
    private CheckBox checkBoxIsAttachMTSTariff;
    private CheckBox checkBoxIsAttachLifeTariff;
    private CheckBox checkBoxIsAttachBeelineTariff;
    private CheckBox checkBoxIsAttachCdmaTariff;
    private CheckBox checkBoxIsAttachInterTelecomTariff;

    private EditBox editBoxMinSellerBalance;
    private EditBox editBoxKSDiscountPercent;
    private EditBox editBoxMTSDiscountPercent;
    private EditBox editBoxLifeDiscountPercent;
    private EditBox editBoxBeelineDiscountPercent;
    private EditBox editBoxCdmaDiscountPercent;
    private EditBox editBoxInterTelecomDiscountPercent;
    private EditBox editBoxSellerDelta;

//    private EditBox editBoxCountryPhys;
    private ScrollBox scrollBoxDistrictPhys;
    private ScrollBox scrollBoxRegionPhys;
    private ScrollBox scrollBoxCityPhys;
    private EditBox editBoxBuildingPhys;
    private EditBox editBoxOfficePhys;
    private TextArea textAreaNotePhys;

    private EditBox editBoxZipCodeLegal;
    private EditBox editBoxAddressLegal;
//    private EditBox editBoxCountryLegal;
    private ScrollBox scrollBoxDistrictLegal;
    private ScrollBox scrollBoxRegionLegal;
    private ScrollBox scrollBoxCityLegal;
    private EditBox editBoxBuildingLegal;
    private EditBox editBoxOfficeLegal;
    private TextArea textAreaNoteLegal;

    //bank
    private EditBox editBoxBankName;
    private EditBox editBoxBankMfo;
    private EditBox editBoxBankAccountNumber;

    //contact data
    private EditBox editBoxPhone0;
    private EditBox editBoxPhone1;
    private EditBox editBoxMobPhone0;
    private EditBox editBoxMobPhone1;
    private EditBox editBoxMail0;
    private EditBox editBoxMail1;
    private EditBox editBoxMail2;

    //add information
    private TimeSelector timeSelectorSignatureDate;
    private TimeSelector timeSelectorDeadLine;
    private EditBox editBoxSymbol;

    //codes
    private EditBox editBoxCertificateNumber;
    private EditBox editBoxInn;
    private EditBox editBoxOkpo;

    private EditBox editBoxDirectorEmail;
    private EditBox editBoxDirectorPhone;

    private EditBox editBoxAccountantEmail;
    private EditBox editBoxAccountantPhone;
    
    private TimeSelector timeSelectorRegistrationDate;
    private TextArea textAreaDescription;

    private ScrollBox scrollBoxParentSellers;
    private ScrollBox scrollBoxProcessings;

    private CheckBox checkBoxActive;

    private Button onApplyButton;
    private Button onSaveButton;
    private Button onResetButton;
    private Button onCancelButton;

    //we must populated seller befor populate Contract
    private Seller populatedSeller;

    protected void init() {
        initIds();
        initEditBoxes();
        initTextAreas();
        initTimeSelectors();
        initScrollBoxes();
        initButtons();
        initCheckBoxes();
        populatedSeller = null;
    }

    //TODO:AN move to control(EditBox)
    private void initIds(){
        physAddressId = null;
        legalAddressId = null;
        bankId = null;
        directorId = null;
        accountantId = null;
        bankAccountId = null;
        contractId = null;
    }

    private void initCheckBoxes(){
        checkBoxActive = new CheckBox();
        checkBoxActive.setSelected(true);

        checkBoxIsAttachRestriction = new CheckBox();
        checkBoxIsAttachRestriction.setSelected(false);

        checkBoxIsAttachKSTariff = new CheckBox();
        checkBoxIsAttachKSTariff.setSelected(false);

        checkBoxIsAttachMTSTariff = new CheckBox();
        checkBoxIsAttachMTSTariff.setSelected(false);

        checkBoxIsAttachLifeTariff = new CheckBox();
        checkBoxIsAttachLifeTariff.setSelected(false);

        checkBoxIsAttachBeelineTariff = new CheckBox();
        checkBoxIsAttachBeelineTariff.setSelected(false);

        checkBoxIsAttachCdmaTariff = new CheckBox();
        checkBoxIsAttachCdmaTariff.setSelected(false);

        checkBoxIsAttachInterTelecomTariff = new CheckBox();
        checkBoxIsAttachInterTelecomTariff.setSelected(false);
    }

    private void initScrollBoxes(){
        String appDeploymentName = ApplicationEnvironment.getAppDeploymentName();
        scrollBoxParentSellers = new ScrollBox() {
            protected String getKey(Object dataListElement) {
                return ((Seller)dataListElement).getSellerId().toString();
            }

            protected String getValue(Object dataListElement) {
                return ((Seller)dataListElement).getName();  
            }
        };
//        scrollBoxParentSellers.setReadonly(true);

        setScrollBoxProcessings(new ScrollBox() {

            protected String getKey(Object dataListElement) {
                return ((Processing)dataListElement).getProcessingId().toString();
            }

            protected String getValue(Object dataListElement) {
                return ((Processing)dataListElement).getName();
            }

        });
        getScrollBoxProcessings().setNullable(true);
        getScrollBoxProcessings().setMandatory(true);

        //address
        scrollBoxDistrictPhys = new ScrollBox(){
            protected String getKey(Object dataListElement) {
                return ((District)dataListElement).getId().toString();
            }
            protected String getValue(Object dataListElement) {
                return ((District)dataListElement).getName();
            }
        };
        scrollBoxDistrictPhys.setOnChange("document.forms[0].action=\"" +
                                            appDeploymentName +"/core/dictionary/seller/infoSeller.do?command=" +
                                            "updateRegionScrollBoxePhys\";document.forms[0].submit()");
        scrollBoxDistrictPhys.setNullable(true);

        scrollBoxRegionPhys = new ScrollBox(){
            protected String getKey(Object dataListElement) {
                return ((Region)dataListElement).getId().toString();
            }
            protected String getValue(Object dataListElement) {
                return ((Region)dataListElement).getName();
            }
        };
        scrollBoxRegionPhys.setOnChange("document.forms[0].action=\"" +
                                            appDeploymentName +  "/core/dictionary/seller/infoSeller.do?command=" +
                                            "updateScrollBoxCityPhys\";document.forms[0].submit()");
        scrollBoxRegionPhys.setNullable(true);

        scrollBoxCityPhys = new ScrollBox(){
            protected String getKey(Object dataListElement) {
                return ((City)dataListElement).getId().toString();
            }
            protected String getValue(Object dataListElement) {
                return ((City)dataListElement).getName();
            }
        };
        scrollBoxCityPhys.setNullable(true);

        scrollBoxDistrictLegal = new ScrollBox(){
            protected String getKey(Object dataListElement) {
                return ((District)dataListElement).getId().toString();
            }
            protected String getValue(Object dataListElement) {
                return ((District)dataListElement).getName();
            }
        };
        scrollBoxDistrictLegal.setOnChange("document.forms[0].action=\""+
                                            appDeploymentName + "/core/dictionary/seller/infoSeller.do?command=" +
                                            "updateRegionScrollBoxeLegal\";document.forms[0].submit()");
        scrollBoxDistrictLegal.setNullable(true);

        scrollBoxRegionLegal = new ScrollBox(){
            protected String getKey(Object dataListElement) {
                return ((Region)dataListElement).getId().toString();
            }
            protected String getValue(Object dataListElement) {
                return ((Region)dataListElement).getName();
            }
        };
        scrollBoxRegionLegal.setOnChange("document.forms[0].action=\"" +
                                            appDeploymentName + "/core/dictionary/seller/infoSeller.do?command=" +
                                            "updateScrollBoxCityLegal\";document.forms[0].submit()");
        scrollBoxRegionLegal.setNullable(true);

        scrollBoxCityLegal = new ScrollBox(){
            protected String getKey(Object dataListElement) {
                return ((City)dataListElement).getId().toString();
            }
            protected String getValue(Object dataListElement) {
                return ((City)dataListElement).getName();
            }
        };
        scrollBoxCityLegal.setNullable(true);
    }

    private void initButtons() {
        onSaveButton = new Button();
        onSaveButton.setLabelKey("ctrl.button.label.save");
        onSaveButton.setAction("/core/dictionary/seller/infoSeller");
        onSaveButton.setCommand("save");

        onResetButton = new Button();
        onResetButton.setLabelKey("ctrl.button.label.reset");

        onCancelButton = new Button();
        onCancelButton.setLabelKey("ctrl.button.label.cancel");
        onCancelButton.setAction("/core/dictionary/seller/manageSellers");

        onApplyButton = new Button();
        onApplyButton.setLabelKey("ctrl.button.label.apply");
        onApplyButton.setAction("/core/dictionary/seller/infoSeller");
        onApplyButton.setCommand("apply");
    }

    private void initTimeSelectors() {
        timeSelectorRegistrationDate = new TimeSelector();
        timeSelectorRegistrationDate.setReadonly(true);

        timeSelectorSignatureDate = new TimeSelector();
        timeSelectorSignatureDate.setReadonly(false);

        timeSelectorDeadLine = new TimeSelector();
        timeSelectorDeadLine.setReadonly(false);

    }

    private void initTextAreas() {
        textAreaDescription = new TextArea();
    }

    private void initEditBoxes() {
        editBoxName = new EditBox();
        editBoxName.setMandatory(true);

        editBoxCode = new EditBox();
        editBoxCode.setMandatory(true);

        editBoxZipCodePhys  = new EditBox();
        editBoxAddressPhys  = new EditBox();

        editBoxBuildingPhys = new EditBox();
        editBoxOfficePhys   = new EditBox();
        textAreaNotePhys   = new TextArea();

        editBoxZipCodeLegal  = new EditBox();
        editBoxAddressLegal  = new EditBox();

        editBoxBuildingLegal = new EditBox();
        editBoxOfficeLegal   = new EditBox();
        textAreaNoteLegal   = new TextArea();

        //Billing data
        editBoxMinSellerBalance = new EditBox();
        editBoxKSDiscountPercent = new EditBox();
        editBoxMTSDiscountPercent = new EditBox();
        editBoxLifeDiscountPercent = new EditBox();
        editBoxBeelineDiscountPercent = new EditBox();
        editBoxCdmaDiscountPercent = new EditBox();
        editBoxInterTelecomDiscountPercent = new EditBox();
        editBoxSellerDelta = new EditBox();

        //contact data
        editBoxPhone0 = new EditBox();
        editBoxPhone1 = new EditBox();
        editBoxMobPhone0 = new EditBox();
        editBoxMobPhone1 = new EditBox();
        editBoxMail0 = new EditBox();
        editBoxMail1 = new EditBox();
        editBoxMail2 = new EditBox();

        //bank
        editBoxBankName = new EditBox();
        editBoxBankMfo = new EditBox();
        editBoxBankAccountNumber = new EditBox();

        //add information
        editBoxSymbol = new EditBox();

        //codes
        editBoxCertificateNumber = new EditBox();
        editBoxInn = new EditBox();
        editBoxOkpo = new EditBox();

        editBoxDirectorEmail = new EditBox();
        editBoxDirectorPhone = new EditBox();

        editBoxAccountantEmail = new EditBox();
        editBoxAccountantPhone = new EditBox();

        editBoxSellerId = new EditBox();
        editBoxSellerId.setReadonly(true);

    }


    public void bindData(Seller seller, User loggedInUser) throws DatabaseException {
        timeSelectorRegistrationDate.setTime( seller.getRegistrationDate() );

        editBoxSellerId.setValue(seller.getSellerId().toString());

        textAreaDescription.setValue(seller.getDescription());
        editBoxName.setValue(seller.getName());
        editBoxCode.setValue(seller.getCode());
        editBoxCertificateNumber.setValue(seller.getCertificateNumber());
        editBoxOkpo.setValue(seller.getOkpo());
        editBoxInn.setValue(seller.getInn());
        timeSelectorRegistrationDate.setTime(seller.getRegistrationDate());

        editBoxPhone0.setValue(seller.getPhone0());
        editBoxPhone1.setValue(seller.getPhone1());
        editBoxMobPhone0.setValue(seller.getMobPhone0());
        editBoxMobPhone1.setValue(seller.getMobPhone1());
        editBoxMail0.setValue(seller.getMail0());
        editBoxMail1.setValue(seller.getMail1());
        editBoxMail2.setValue(seller.getMail2());

        checkBoxActive.setSelected(seller.getActive().booleanValue());

        bindParentSellers(loggedInUser, seller);
        bindProcessings(loggedInUser, seller);
        bindScrollBoxes();
        
        //addition information can be absent
        if(seller.getLegalAddress() != null)
            bindLegalAddress(seller.getLegalAddress());

        if(seller.getPhysicalAddress() != null)
            bindPhysAddress(seller.getPhysicalAddress());

        if(seller.getDirector()!=null && seller.getAccountant()!=null)
            bindPerson(seller.getDirector(), seller.getAccountant());
        //----------------------------------

//        disableControls(true);
    }

    public void bindData(User loggedInUser) throws DatabaseException {
        bindProcessings(loggedInUser);
        bindScrollBoxes();
//        disableControls(false);
    }

    public void bindData(BankAccount account, User loggedInUser) throws DatabaseException {
        if(account != null){ //addition information can be absent
            bankAccountId = account.getBankAccountId();
            editBoxBankAccountNumber.setValue(account.getAccountNumber());
            bindBank(account.getBank());
        }
    }

    public void bindData(Contract contract){
        if(contract != null){
            contractId = contract.getContractId();
            editBoxSymbol.setValue(contract.getContractNumber());
            timeSelectorSignatureDate.setTime(contract.getRegistrationDate());
            timeSelectorDeadLine.setTime(contract.getDeadLine());
        }
    }
    //defaull binding
    private void bindScrollBoxes() throws com.tmx.as.exceptions.DatabaseException {
        AddressManager am = new AddressManager();

        scrollBoxDistrictLegal.bind(am.getAllDistricts());
        scrollBoxRegionLegal.bind(am.getAllRegions()); //TODO: to filter by district
        scrollBoxCityLegal.bind(am.getAllCities()); //TODO: to filter by region

        scrollBoxDistrictPhys.bind(am.getAllDistricts());
        scrollBoxRegionPhys.bind(am.getAllRegions());//TODO: to filter by district
        scrollBoxCityPhys.bind(am.getAllCities());//TODO: to filter by region 
    }
    
    private void bindPhysAddress(Address physAddress) throws com.tmx.as.exceptions.DatabaseException {
        physAddressId = physAddress.getAddressId();
        editBoxZipCodePhys.setValue(physAddress.getZipCode());
        editBoxAddressPhys.setValue(physAddress.getAddressLine1());

        if (physAddress.getDistrict() != null) {
            scrollBoxDistrictPhys.setSelectedKey(physAddress.getDistrict().getDistrictId().toString());
        }
        if (physAddress.getRegion() != null) {
            scrollBoxRegionPhys.setSelectedKey(physAddress.getRegion().getRegionId().toString());
        }
        if (physAddress.getCity() != null) {
            scrollBoxCityPhys.setSelectedKey(physAddress.getCity().getCityId().toString());
        }
        editBoxAddressPhys.setValue(physAddress.getAddressLine1());//street

        editBoxBuildingPhys.setValue(physAddress.getBuilding());
        editBoxOfficePhys.setValue(physAddress.getOffice());
        textAreaNotePhys.setValue(physAddress.getNote());
    }

    private void bindLegalAddress (Address legalAddress) throws com.tmx.as.exceptions.DatabaseException {
        legalAddressId = legalAddress.getAddressId();
        editBoxZipCodeLegal.setValue(legalAddress.getZipCode());
        editBoxAddressLegal.setValue(legalAddress.getAddressLine1());

        if (legalAddress.getDistrict() != null) {
            scrollBoxDistrictLegal.setSelectedKey(legalAddress.getDistrict().getDistrictId().toString());
        }
        if (legalAddress.getDistrict() != null) {
            scrollBoxDistrictLegal.setSelectedKey(legalAddress.getDistrict().getDistrictId().toString());
        }
        if (legalAddress.getRegion() != null) {
            scrollBoxRegionLegal.setSelectedKey(legalAddress.getRegion().getRegionId().toString());
        }
        if (legalAddress.getCity() != null) {
            scrollBoxCityLegal.setSelectedKey(legalAddress.getCity().getCityId().toString());
        }

        editBoxAddressLegal.setValue(legalAddress.getAddressLine1());//street
        editBoxBuildingLegal.setValue(legalAddress.getBuilding());
        editBoxOfficeLegal.setValue(legalAddress.getOffice());
        textAreaNoteLegal.setValue(legalAddress.getNote());
    }

    private void bindPerson(Person director, Person accountant){
        directorId = director.getPersonId();
        accountantId = accountant.getPersonId();
        editBoxDirectorEmail.setValue(director.getEmail());
        editBoxDirectorPhone.setValue(director.getPhone());
        editBoxAccountantEmail.setValue(accountant.getEmail());
        editBoxAccountantPhone.setValue(accountant.getPhone());
    }

    private void bindBank(Bank bank){
        bankId = bank.getBankId();
        editBoxBankName.setValue(bank.getName());
        editBoxBankMfo.setValue(bank.getName());
    }

    private void disableControls(boolean enable){
        scrollBoxParentSellers.setReadonly(enable);
    }

    private void bindParentSellers(User loggedInUser, Seller seller) throws DatabaseException {
        List<Seller> sellers = new SellerManager().getNestedSellersInSeller(loggedInUser.getSellerOwner(), true);
        if(seller.getParentSeller() != null) {
            sellers.add(seller.getParentSeller());
        }
        scrollBoxParentSellers.bind(sellers);
        /*if(seller.getParentSeller() != null) {
            scrollBoxParentSellers.setSelectedKey(seller.getParentSeller().getSellerId().toString());
        } else {*/
            scrollBoxParentSellers.setNullable(true);            
            scrollBoxParentSellers.setSelectedKey(ScrollBox.NULL_KEY);            
        /*}*/
    }

    private void bindProcessings(User loggedInUser, Seller seller) throws DatabaseException {
        bindProcessings(loggedInUser);
        if(seller.getProcessing() != null)
            scrollBoxProcessings.setSelectedKey(seller.getProcessing().getProcessingId().toString());
    }

    private void bindProcessings(User loggedInUser) throws DatabaseException {
        scrollBoxProcessings.bind(new ProcessingManager().getAllProcessings());
    }

    public Processing populateProcessing(){
         if( isNullKey(scrollBoxProcessings) )
            return null;

        Processing processing = new Processing();
        processing.setProcessingId(PopulateUtil.getLongKey(scrollBoxProcessings));
        return processing;
    }

    public Seller populateSeller() throws DatabaseException {
        Seller seller = new Seller();
        seller.setSellerId( PopulateUtil.getLongValue(editBoxSellerId) );
        seller.setDescription( textAreaDescription.getValue() );
        seller.setRegistrationDate( new Date() );
        seller.setName( editBoxName.getValue() );
        seller.setCode( editBoxCode.getValue() );
        if (!scrollBoxDistrictPhys.isNullKeySelected()) {
            seller.setPhysicalAddress(populatePhysAddress());
        }
        if (!scrollBoxDistrictLegal.isNullKeySelected()) {
            seller.setLegalAddress(populateLegalAddress());
        }
        seller.setActive(Boolean.valueOf(checkBoxActive.isSelected()));
        seller.setOkpo(editBoxOkpo.getValue());
        seller.setInn(editBoxInn.getValue());
        seller.setCertificateNumber(editBoxCertificateNumber.getValue());

        seller.setPhone0(editBoxPhone0.getValue());
        seller.setPhone1(editBoxPhone1.getValue());
        seller.setMobPhone0(editBoxMobPhone0.getValue());
        seller.setMobPhone1(editBoxMobPhone1.getValue());
        seller.setMail0(editBoxMail0.getValue());
        seller.setMail1(editBoxMail1.getValue());
        seller.setMail2(editBoxMail2.getValue());

        if (!scrollBoxParentSellers.isNullKeySelected()) {
            seller.setParentSeller(getParentSeller());
        } else {
            seller.setParentSeller(null);            
        }
        seller.setDirector( populateDirector() );
        seller.setAccountant( populateAccountant() );
        seller.setProcessing( populateProcessing() );
//        if (seller.getSellerId() != null) {
//            seller.setPrimaryBalance(new SellerManager().getSeller(seller.getSellerId()).getPrimaryBalance());
//        }
        populatedSeller = seller;
        return seller;
    }

    private Address populatePhysAddress() throws DatabaseException {
        Address address = new Address();
        AddressManager adm = new AddressManager();

        address.setAddressId(physAddressId);
        address.setAddressLine1(editBoxAddressPhys.getValue());

        if (!scrollBoxDistrictPhys.isNullKeySelected()) {
            address.setDistrict(adm.getDistrict(new Long(scrollBoxDistrictPhys.getSelectedKey())));
        }
        if (!scrollBoxRegionPhys.isNullKeySelected()) {
            address.setRegion(adm.getRegion(new Long(scrollBoxRegionPhys.getSelectedKey())));
        }
        if (!scrollBoxCityPhys.isNullKeySelected()) {
            address.setCity(adm.getCity(new Long(scrollBoxCityPhys.getSelectedKey())));
        }
        address.setBuilding(editBoxBuildingPhys.getValue());
        address.setZipCode(editBoxZipCodePhys.getValue());
        address.setOffice(editBoxOfficePhys.getValue());
        address.setNote(textAreaNotePhys.getValue());

        return address;
    }

    private Address populateLegalAddress() throws DatabaseException {
        Address address = new Address();
        AddressManager adm = new AddressManager();

        address.setAddressId(legalAddressId);
        address.setAddressLine1(editBoxAddressLegal.getValue());

        if (!scrollBoxDistrictLegal.isNullKeySelected()) {
            address.setDistrict(adm.getDistrict(new Long(scrollBoxDistrictLegal.getSelectedKey())));
        }
        if (!scrollBoxRegionLegal.isNullKeySelected()) {
            address.setRegion(adm.getRegion(new Long(scrollBoxRegionLegal.getSelectedKey())));
        }
        if (!scrollBoxCityLegal.isNullKeySelected()) {
            address.setCity(adm.getCity(new Long(scrollBoxCityLegal.getSelectedKey())));
        }
        address.setBuilding(editBoxBuildingLegal.getValue());
        address.setZipCode(editBoxZipCodeLegal.getValue());
        address.setOffice(editBoxOfficeLegal.getValue());
        address.setNote(textAreaNoteLegal.getValue()); //TODO: fix (set as office)
//        address.setPhone0(editBoxPhone0Legal.getValue());
//        address.setPhone1(editBoxPhone1Legal.getValue());
//        address.setMobPhone0(editBoxMobPhone0Legal.getValue());
//        address.setMobPhone1(editBoxMobPhone1Legal.getValue());
//        address.setEMail0(editBoxEMail0Legal.getValue());
//        address.setEMail1(editBoxEMail1Legal.getValue());
//        address.setEMail2(editBoxEMail2Legal.getValue());
        return address;
    }

    private Person populateDirector(){
        Person person = new Person();
        person.setPersonId(directorId);
        person.setEmail(editBoxDirectorEmail.getValue());
        person.setPhone(editBoxDirectorPhone.getValue());
        return person;
    }

    private Person populateAccountant(){
        Person person = new Person();
        person.setPersonId(accountantId);
        person.setEmail(editBoxAccountantEmail.getValue());
        person.setPhone(editBoxAccountantPhone.getValue());
        return person;
    }

    public BankAccount populateBankAccount() throws DatabaseException {
        BankAccount account = new BankAccount();
        account.setBankAccountId(bankAccountId);
        account.setSeller( populateSeller() );
        account.setAccountNumber( editBoxBankAccountNumber.getValue() );
        account.setBank( populateBank() );
        return account;
    }

    public Bank populateBank(){
        Bank bank = new Bank();
        bank.setBankId(bankId);
        bank.setMfo(editBoxBankMfo.getValue());
        bank.setName(editBoxBankName.getValue());
        return bank;
    }

    public Contract populateContract(){
        if( populatedSeller == null )
            throw new IllegalStateException("you must populate seller before populate Contract");
        
        Contract contract = new Contract();
        contract.setRegistrationDate(timeSelectorSignatureDate.getTime());
        contract.setContractNumber(editBoxSymbol.getValue());
        contract.setSeller(populatedSeller);
        contract.setContractId(contractId);
        contract.setDeadLine(timeSelectorDeadLine.getTime());
        return contract;
    }

    private Seller getParentSeller(){
        Seller parentSeller = new Seller();
        parentSeller.setSellerId(PopulateUtil.getLongKey(scrollBoxParentSellers));
        return parentSeller;
    }

    public List<BasicControl> getControls() {
        List controls = new ArrayList<BasicControl>();
        List  buttons = this.getOwnedControlsByClass(getOnApplyButton().getClass());
        List editBoxes = this.getOwnedControlsByClass(EditBox.class);
        List textAries = this.getOwnedControlsByClass(TextArea.class);
        List timeSelectors = this.getOwnedControlsByClass(TimeSelector.class);

        controls.addAll(buttons);
        controls.addAll(editBoxes);
        controls.addAll(textAries);
        controls.addAll(timeSelectors);

        controls.add(getScrollBoxParentSellers());
        controls.add(getScrollBoxProcessings());
        controls.add(getScrollBoxDistrictLegal());
        controls.add(getScrollBoxDistrictPhys());
        controls.add(getScrollBoxRegionLegal());
        controls.add(getScrollBoxRegionPhys());
        controls.add(getScrollBoxCityLegal());
        controls.add(getScrollBoxCityPhys());
        controls.add(getCheckBoxActive());

        return controls;
    }

    private boolean isNullKey(ScrollBox scrollBox){
        return ScrollBox.NULL_KEY.equals( scrollBox.getSelectedKey() );
    }

    public Long getSelectedSellerId() {
        return selectedSellerId;
    }

    public void setSelectedSellerId(Long selectedSellerId) {
        this.selectedSellerId = selectedSellerId;
    }

    public EditBox getEditBoxSellerId() {
        return editBoxSellerId;
    }

    public void setEditBoxSellerId(EditBox editBoxSellerId) {
        this.editBoxSellerId = editBoxSellerId;
    }

    public EditBox getEditBoxName() {
        return editBoxName;
    }

    public void setEditBoxName(EditBox editBoxName) {
        this.editBoxName = editBoxName;
    }

    public EditBox getEditBoxCode() {
        return editBoxCode;
    }

    public void setEditBoxCode(EditBox editBoxCode) {
        this.editBoxCode = editBoxCode;
    }

    public TextArea getTextAreaDescription() {
        return textAreaDescription;
    }

    public void setTextAreaDescription(TextArea textAreaDescription) {
        this.textAreaDescription = textAreaDescription;
    }

    public TextArea getTextAreaNotePhys() {
        return textAreaNotePhys;
    }

    public void setTextAreaNotePhys(TextArea textAreaNotePhys) {
        this.textAreaNotePhys = textAreaNotePhys;
    }

    public TextArea getTextAreaNoteLegal() {
        return textAreaNoteLegal;
    }

    public void setTextAreaNoteLegal(TextArea textAreaNoteLegal) {
        this.textAreaNoteLegal = textAreaNoteLegal;
    }

    public Button getOnApplyButton() {
        return onApplyButton;
    }

    public void setOnApplyButton(Button onApplyButton) {
        this.onApplyButton = onApplyButton;
    }

    public Button getOnSaveButton() {
        return onSaveButton;
    }

    public void setOnSaveButton(Button onSaveButton) {
        this.onSaveButton = onSaveButton;
    }

    public Button getOnResetButton() {
        return onResetButton;
    }

    public void setOnResetButton(Button onResetButton) {
        this.onResetButton = onResetButton;
    }

    public Button getOnCancelButton() {
        return onCancelButton;
    }

    public void setOnCancelButton(Button onCancelButton) {
        this.onCancelButton = onCancelButton;
    }

    public TimeSelector getTimeSelectorRegistrationDate() {
        return timeSelectorRegistrationDate;
    }

    public void setTimeSelectorRegistrationDate(TimeSelector timeSelectorRegistrationDate) {
        this.timeSelectorRegistrationDate = timeSelectorRegistrationDate;
    }

    public ScrollBox getScrollBoxParentSellers() {
        return scrollBoxParentSellers;
    }

    public void setScrollBoxParentSellers(ScrollBox scrollBoxParentSellers) {
        this.scrollBoxParentSellers = scrollBoxParentSellers;
    }

    public CheckBox getCheckBoxActive() {
        return checkBoxActive;
    }

    public void setCheckBoxActive(CheckBox checkBoxActive) {
        this.checkBoxActive = checkBoxActive;
    }

    public EditBox getEditBoxZipCodePhys() {
        return editBoxZipCodePhys;
    }

    public void setEditBoxZipCodePhys(EditBox editBoxZipCodePhys) {
        this.editBoxZipCodePhys = editBoxZipCodePhys;
    }

    public EditBox getEditBoxAddressPhys() {
        return editBoxAddressPhys;
    }

    public void setEditBoxAddressPhys(EditBox editBoxAddressPhys) {
        this.editBoxAddressPhys = editBoxAddressPhys;
    }

    public ScrollBox getScrollBoxCityPhys() {
        return scrollBoxCityPhys;
    }

    public void setScrollBoxCityPhys(ScrollBox scrollBoxCityPhys) {
        this.scrollBoxCityPhys = scrollBoxCityPhys;
    }

    public EditBox getEditBoxBuildingPhys() {
        return editBoxBuildingPhys;
    }

    public void setEditBoxBuildingPhys(EditBox editBoxBuildingPhys) {
        this.editBoxBuildingPhys = editBoxBuildingPhys;
    }

    public EditBox getEditBoxOfficePhys() {
        return editBoxOfficePhys;
    }

    public void setEditBoxOfficePhys(EditBox editBoxOfficePhys) {
        this.editBoxOfficePhys = editBoxOfficePhys;
    }

    public EditBox getEditBoxZipCodeLegal() {
        return editBoxZipCodeLegal;
    }

    public void setEditBoxZipCodeLegal(EditBox editBoxZipCodeLegal) {
        this.editBoxZipCodeLegal = editBoxZipCodeLegal;
    }

    public EditBox getEditBoxAddressLegal() {
        return editBoxAddressLegal;
    }

    public void setEditBoxAddressLegal(EditBox editBoxAddressLegal) {
        this.editBoxAddressLegal = editBoxAddressLegal;
    }

    public ScrollBox getScrollBoxCityLegal() {
        return scrollBoxCityLegal;
    }

//    public EditBox getEditBoxCountryPhys() {
//        return editBoxCountryPhys;
//    }

//    public void setEditBoxCountryPhys(EditBox editBoxCountryPhys) {
//        this.editBoxCountryPhys = editBoxCountryPhys;
//    }

    public ScrollBox getScrollBoxDistrictPhys() {
        return scrollBoxDistrictPhys;
    }

    public void setScrollBoxDistrictPhys(ScrollBox scrollBoxDistrictPhys) {
        this.scrollBoxDistrictPhys = scrollBoxDistrictPhys;
    }

    public ScrollBox getScrollBoxRegionPhys() {
        return scrollBoxRegionPhys;
    }

    public void setScrollBoxRegionPhys(ScrollBox scrollBoxRegionPhys) {
        this.scrollBoxRegionPhys = scrollBoxRegionPhys;
    }

//    public EditBox getEditBoxCountryLegal() {
//        return editBoxCountryLegal;
//    }
//
//    public void setEditBoxCountryLegal(EditBox editBoxCountryLegal) {
//        this.editBoxCountryLegal = editBoxCountryLegal;
//    }

    public ScrollBox getScrollBoxDistrictLegal() {
        return scrollBoxDistrictLegal;
    }

    public void setScrollBoxDistrictLegal(ScrollBox scrollBoxDistrictLegal) {
        this.scrollBoxDistrictLegal = scrollBoxDistrictLegal;
    }

    public ScrollBox getScrollBoxRegionLegal() {
        return scrollBoxRegionLegal;
    }

    public void setScrollBoxRegionLegal(ScrollBox scrollBoxRegionLegal) {
        this.scrollBoxRegionLegal = scrollBoxRegionLegal;
    }

    public void setScrollBoxCityLegal(ScrollBox scrollBoxCityLegal) {
        this.scrollBoxCityLegal = scrollBoxCityLegal;
    }

    public EditBox getEditBoxBuildingLegal() {
        return editBoxBuildingLegal;
    }

    public void setEditBoxBuildingLegal(EditBox editBoxBuildingLegal) {
        this.editBoxBuildingLegal = editBoxBuildingLegal;
    }

    public EditBox getEditBoxOfficeLegal() {
        return editBoxOfficeLegal;
    }

    public void setEditBoxOfficeLegal(EditBox editBoxOfficeLegal) {
        this.editBoxOfficeLegal = editBoxOfficeLegal;
    }



    public EditBox getEditBoxPhone0() {
        return editBoxPhone0;
    }

    public void setEditBoxPhone0(EditBox editBoxPhone0) {
        this.editBoxPhone0 = editBoxPhone0;
    }

    public EditBox getEditBoxPhone1() {
        return editBoxPhone1;
    }

    public void setEditBoxPhone1(EditBox editBoxPhone1) {
        this.editBoxPhone1 = editBoxPhone1;
    }

    public EditBox getEditBoxMobPhone0() {
        return editBoxMobPhone0;
    }

    public void setEditBoxMobPhone0(EditBox editBoxMobPhone0) {
        this.editBoxMobPhone0 = editBoxMobPhone0;
    }

    public EditBox getEditBoxMobPhone1() {
        return editBoxMobPhone1;
    }

    public void setEditBoxMobPhone1(EditBox editBoxMobPhone1) {
        this.editBoxMobPhone1 = editBoxMobPhone1;
    }

    public EditBox getEditBoxMail0() {
        return editBoxMail0;
    }

    public void setEditBoxMail0(EditBox editBoxMail0) {
        this.editBoxMail0 = editBoxMail0;
    }

    public EditBox getEditBoxMail1() {
        return editBoxMail1;
    }

    public void setEditBoxMail1(EditBox editBoxMail1) {
        this.editBoxMail1 = editBoxMail1;
    }

    public EditBox getEditBoxMail2() {
        return editBoxMail2;
    }

    public void setEditBoxMail2(EditBox editBoxMail2) {
        this.editBoxMail2 = editBoxMail2;
    }

    public Long getPhysAddressId() {
        return physAddressId;
    }

    public void setPhysAddressId(Long physAddressId) {
        this.physAddressId = physAddressId;
    }

    public Long getLegalAddressId() {
        return legalAddressId;
    }

    public void setLegalAddressId(Long legalAddressId) {
        this.legalAddressId = legalAddressId;
    }

    public EditBox getEditBoxBankName() {
        return editBoxBankName;
    }

    public void setEditBoxBankName(EditBox editBoxBankName) {
        this.editBoxBankName = editBoxBankName;
    }

    public EditBox getEditBoxBankMfo() {
        return editBoxBankMfo;
    }

    public void setEditBoxBankMfo(EditBox editBoxBankMfo) {
        this.editBoxBankMfo = editBoxBankMfo;
    }

    public EditBox getEditBoxBankAccountNumber() {
        return editBoxBankAccountNumber;
    }

    public void setEditBoxBankAccountNumber(EditBox editBoxBankAccountNumber) {
        this.editBoxBankAccountNumber = editBoxBankAccountNumber;
    }

    public TimeSelector getTimeSelectorSignatureDate() {
        return timeSelectorSignatureDate;
    }

    public void setTimeSelectorSignatureDate(TimeSelector timeSelectorSignatureDate) {
        this.timeSelectorSignatureDate = timeSelectorSignatureDate;
    }

    public TimeSelector getTimeSelectorDeadLine() {
        return timeSelectorDeadLine;
    }

    public void setTimeSelectorDeadLine(TimeSelector timeSelectorDeadLine) {
        this.timeSelectorDeadLine = timeSelectorDeadLine;
    }

    public EditBox getEditBoxSymbol() {
        return editBoxSymbol;
    }

    public void setEditBoxSymbol(EditBox editBoxSymbol) {
        this.editBoxSymbol = editBoxSymbol;
    }

    public EditBox getEditBoxCertificateNumber() {
        return editBoxCertificateNumber;
    }

    public void setEditBoxCertificateNumber(EditBox editBoxCertificateNumber) {
        this.editBoxCertificateNumber = editBoxCertificateNumber;
    }

    public EditBox getEditBoxInn() {
        return editBoxInn;
    }

    public void setEditBoxInn(EditBox editBoxInn) {
        this.editBoxInn = editBoxInn;
    }

    public EditBox getEditBoxOkpo() {
        return editBoxOkpo;
    }

    public void setEditBoxOkpo(EditBox editBoxOkpo) {
        this.editBoxOkpo = editBoxOkpo;
    }

    public EditBox getEditBoxDirectorEmail() {
        return editBoxDirectorEmail;
    }

    public void setEditBoxDirectorEmail(EditBox editBoxDirectorEmail) {
        this.editBoxDirectorEmail = editBoxDirectorEmail;
    }

    public EditBox getEditBoxDirectorPhone() {
        return editBoxDirectorPhone;
    }

    public void setEditBoxDirectorPhone(EditBox editBoxDirectorPhone) {
        this.editBoxDirectorPhone = editBoxDirectorPhone;
    }

    public EditBox getEditBoxAccountantEmail() {
        return editBoxAccountantEmail;
    }

    public void setEditBoxAccountantEmail(EditBox editBoxAccountantEmail) {
        this.editBoxAccountantEmail = editBoxAccountantEmail;
    }

    public EditBox getEditBoxAccountantPhone() {
        return editBoxAccountantPhone;
    }

    public void setEditBoxAccountantPhone(EditBox editBoxAccountantPhone) {
        this.editBoxAccountantPhone = editBoxAccountantPhone;
    }

    public Long getBankId() {
        return bankId;
    }

    public void setBankId(Long bankId) {
        this.bankId = bankId;
    }

    public Long getDirectorId() {
        return directorId;
    }

    public void setDirectorId(Long directorId) {
        this.directorId = directorId;
    }

    public Long getAccountantId() {
        return accountantId;
    }

    public void setAccountantId(Long accountantId) {
        this.accountantId = accountantId;
    }

    public Long getBankAccountId() {
        return bankAccountId;
    }

    public void setBankAccountId(Long bankAccountId) {
        this.bankAccountId = bankAccountId;
    }

     public ScrollBox getScrollBoxProcessings() {
        return scrollBoxProcessings;
    }

    public void setScrollBoxProcessings(ScrollBox scrollBoxProcessings) {
        this.scrollBoxProcessings = scrollBoxProcessings;
    }

    public Long getContractId() {
        return contractId;
    }

    public void setContractId(Long contractId) {
        this.contractId = contractId;
    }

    public Seller getPopulatedSeller() {
        return populatedSeller;
    }

    public void setPopulatedSeller(Seller populatedSeller) {
        this.populatedSeller = populatedSeller;
    }

    public CheckBox getCheckBoxIsAttachRestriction() {
        return checkBoxIsAttachRestriction;
    }

    public void setCheckBoxIsAttachRestriction(CheckBox checkBoxIsAttachRestriction) {
        this.checkBoxIsAttachRestriction = checkBoxIsAttachRestriction;
    }

    public EditBox getEditBoxKSDiscountPercent() {
        return editBoxKSDiscountPercent;
    }

    public void setEditBoxKSDiscountPercent(EditBox editBoxKSDiscountPercent) {
        this.editBoxKSDiscountPercent = editBoxKSDiscountPercent;
    }

    public EditBox getEditBoxMTSDiscountPercent() {
        return editBoxMTSDiscountPercent;
    }

    public void setEditBoxMTSDiscountPercent(EditBox editBoxMTSDiscountPercent) {
        this.editBoxMTSDiscountPercent = editBoxMTSDiscountPercent;
    }

    public EditBox getEditBoxSellerDelta() {
        return editBoxSellerDelta;
    }

    public void setEditBoxSellerDelta(EditBox editBoxSellerDelta) {
        this.editBoxSellerDelta = editBoxSellerDelta;
    }

    public CheckBox getCheckBoxIsAttachKSTariff() {
        return checkBoxIsAttachKSTariff;
    }

    public void setCheckBoxIsAttachKSTariff(CheckBox checkBoxIsAttachKSTariff) {
        this.checkBoxIsAttachKSTariff = checkBoxIsAttachKSTariff;
    }

    public CheckBox getCheckBoxIsAttachMTSTariff() {
        return checkBoxIsAttachMTSTariff;
    }

    public void setCheckBoxIsAttachMTSTariff(CheckBox checkBoxIsAttachMTSTariff) {
        this.checkBoxIsAttachMTSTariff = checkBoxIsAttachMTSTariff;
    }

    public CheckBox getCheckBoxIsAttachLifeTariff() {
        return checkBoxIsAttachLifeTariff;
    }

    public void setCheckBoxIsAttachLifeTariff(CheckBox checkBoxIsAttachLifeTariff) {
        this.checkBoxIsAttachLifeTariff = checkBoxIsAttachLifeTariff;
    }

    public EditBox getEditBoxLifeDiscountPercent() {
        return editBoxLifeDiscountPercent;
    }

    public void setEditBoxLifeDiscountPercent(EditBox editBoxLifeDiscountPercent) {
        this.editBoxLifeDiscountPercent = editBoxLifeDiscountPercent;
    }

    public EditBox getEditBoxBeelineDiscountPercent() {
        return editBoxBeelineDiscountPercent;
    }

    public void setEditBoxBeelineDiscountPercent(EditBox editBoxBeelineDiscountPercent) {
        this.editBoxBeelineDiscountPercent = editBoxBeelineDiscountPercent;
    }

    public CheckBox getCheckBoxIsAttachBeelineTariff() {
        return checkBoxIsAttachBeelineTariff;
    }

    public void setCheckBoxIsAttachBeelineTariff(CheckBox checkBoxIsAttachBeelineTariff) {
        this.checkBoxIsAttachBeelineTariff = checkBoxIsAttachBeelineTariff;
    }

    public EditBox getEditBoxMinSellerBalance() {
        return editBoxMinSellerBalance;
    }

    public void setEditBoxMinSellerBalance(EditBox editBoxMinSellerBalance) {
        this.editBoxMinSellerBalance = editBoxMinSellerBalance;
    }

    public CheckBox getCheckBoxIsAttachCdmaTariff() {
        return checkBoxIsAttachCdmaTariff;
    }

    public void setCheckBoxIsAttachCdmaTariff(CheckBox checkBoxIsAttachCdmaTariff) {
        this.checkBoxIsAttachCdmaTariff = checkBoxIsAttachCdmaTariff;
    }

    public CheckBox getCheckBoxIsAttachInterTelecomTariff() {
        return checkBoxIsAttachInterTelecomTariff;
    }

    public void setCheckBoxIsAttachInterTelecomTariff(CheckBox checkBoxIsAttachInterTelecomTariff) {
        this.checkBoxIsAttachInterTelecomTariff = checkBoxIsAttachInterTelecomTariff;
    }

    public EditBox getEditBoxCdmaDiscountPercent() {
        return editBoxCdmaDiscountPercent;
    }

    public void setEditBoxCdmaDiscountPercent(EditBox editBoxCdmaDiscountPercent) {
        this.editBoxCdmaDiscountPercent = editBoxCdmaDiscountPercent;
    }

    public EditBox getEditBoxInterTelecomDiscountPercent() {
        return editBoxInterTelecomDiscountPercent;
    }

    public void setEditBoxInterTelecomDiscountPercent(EditBox editBoxInterTelecomDiscountPercent) {
        this.editBoxInterTelecomDiscountPercent = editBoxInterTelecomDiscountPercent;
    }
}
