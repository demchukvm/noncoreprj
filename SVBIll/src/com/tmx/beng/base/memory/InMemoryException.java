package com.tmx.beng.base.memory;

import com.tmx.util.StructurizedException;

import java.util.Locale;

/**
 */
public class InMemoryException extends StructurizedException {
    public InMemoryException(String msg, String[] params, Throwable e, Locale locale) {
        super(msg, params, e, locale);
    }

    public InMemoryException(String msg, String[] params) {
        super(msg, params);
    }

    public InMemoryException(String msg, String[] params, Locale locale) {
        super(msg, params, locale);
    }

    public InMemoryException(String msg, Throwable e, Locale locale) {
        super(msg, e, locale);
    }

    public InMemoryException(String msg, Throwable e) {
        super(msg, e);
    }

    public InMemoryException(String msg, Locale locale) {
        super(msg, locale);
    }

    public InMemoryException(String msg) {
        super(msg);
    }

    public InMemoryException(String msg, String[] params, Throwable e) {
        super(msg, params, e);
    }
}
