package com.tmx.as.entities.bill.person;

import com.tmx.as.base.BasicEntity;

import java.io.Serializable;


public class Position extends BasicEntity{
    private Long positionId;
    private String name;
    private String description;

    public Serializable getId() {
        return positionId;
    }

    public int getValid() {
        return valid;
    }

    public void setValid(int valid) {
        this.valid = valid;    
    }

    public Long getPositionId() {
        return positionId;
    }

    public void setPositionId(Long positionId) {
        this.positionId = positionId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
