package com.tmx.beng.base.processors;

import com.tmx.as.entities.bill.operator.Operator;
import com.tmx.as.entities.bill.seller.Seller;
import com.tmx.as.entities.bill.terminal.Terminal;
import com.tmx.beng.base.*;
import com.tmx.beng.base.cache.DBCache;

import java.util.Iterator;
import java.util.List;
import java.util.ArrayList;

/**
 */
public class CancelReservationProcessor extends TaskProcessor {

    public Task process0(Task task, DBCache dbCache, BillingMessageWritable msg)throws BillException {
        try{
            int statusCode = Integer.parseInt(msg.getAttributeString(BillingMessage.STATUS_CODE));
            if(statusCode != StatusDictionary.STATUS_OK){ // Gate OK
                logger.debug("Cancel Reservation...");

                // find sending task
                Task senderTask = task.getGlobalContext().getTransactionPool().lookupTask(
                        msg.getAttributeString(BillingMessage.REQUEST_ID),
                        BillingMessage.O_REFILL_PAYMENT
                );

                if(senderTask == null)
                    throw new BillException("err.bill.reg_processor.sender_task_not_found");

                ReservationSet reservationSet = (ReservationSet)senderTask.getProcesedMessage().getAttribute(BillingMessage.BALANCE_RESERVATION_SET);
                List reservesIdToCancel = new ArrayList();

                // cancel operator reservations
                String operatorCode = senderTask.getProcesedMessage().getAttributeString(BillingMessage.SERVICE_CODE);
                Reservation reservation = reservationSet.getReservation(Operator.class, operatorCode);
                reservesIdToCancel.clear();
                for(Iterator iter = reservation.reservesIterator(); iter.hasNext();){
                    Reservation.Reserve reserve = (Reservation.Reserve)iter.next();
                    dbCache.changeOperatorBalance(
                            operatorCode,
                            reserve.getBalanceCode(),
                            -reserve.getAmount(),
                            reserve.getTransTypeCode());
                    reservesIdToCancel.add(reserve.getId());
                }
                cancelReservesById(reservation, reservesIdToCancel);

                // cancel seller reservations
                String sellerCode = senderTask.getProcesedMessage().getAttributeString(BillingMessage.SELLER_CODE);
                List sellersParentsChainCodes = dbCache.getSellerWrapperParentsChainCodes(sellerCode);

                for(Iterator sellersIter = sellersParentsChainCodes.iterator(); sellersIter.hasNext();){
                    String nextSellerCode = (String)sellersIter.next();
                    reservation = reservationSet.getReservation(Seller.class, nextSellerCode);
                    reservesIdToCancel.clear();
                    for(Iterator iter = reservation.reservesIterator(); iter.hasNext();){
                        Reservation.Reserve reserve = (Reservation.Reserve)iter.next();
                        dbCache.changeSellerBalance(
                                nextSellerCode,
                                reserve.getBalanceCode(),
                                -reserve.getAmount(),
                                reserve.getTransTypeCode());
                        reservesIdToCancel.add(reserve.getId());
                    }
                    cancelReservesById(reservation, reservesIdToCancel);
                }

                // cancel terminal reservations
                String terminalSn = senderTask.getProcesedMessage().getAttributeString(BillingMessage.TERMINAL_SN);
                reservation = reservationSet.getReservation(Terminal.class, terminalSn);
                reservesIdToCancel.clear();
                for(Iterator iter = reservation.reservesIterator(); iter.hasNext();){
                    Reservation.Reserve reserve = (Reservation.Reserve)iter.next();
                    dbCache.changeTerminalBalance(
                            terminalSn,
                            reserve.getBalanceCode(),
                            -reserve.getAmount(),
                            reserve.getTransTypeCode());
                    reservesIdToCancel.add(reserve.getId());
                }
                cancelReservesById(reservation, reservesIdToCancel);
                
            }
            else{
                msg.setStatusCode(StatusDictionary.STATUS_OK);
            }
        }
        catch(Throwable e){
            msg.setStatusCode(StatusDictionary.CANCEL_RESERVATION_ERROR);
            logger.error(e.getLocalizedMessage(), e);
            if(e instanceof BillException)
                throw (BillException)e;
            throw new BillException(e.getLocalizedMessage(), e);
        }
        return task;
    }

    /** Uses reservation cancelation after iterations finished to avoid ConcurrentModificationException
     * @param reservation to cancel reserves in
     * @param reservesIds the list of reserves IDs to cancel */
    private void cancelReservesById(Reservation reservation, List reservesIds){
        if(reservesIds != null && reservesIds.size() > 0)
            for(Iterator iter = reservesIds.iterator(); iter.hasNext();)
                reservation.cancelReserve((String)iter.next());
    }

}
