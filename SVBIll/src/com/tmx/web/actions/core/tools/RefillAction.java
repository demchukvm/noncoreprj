package com.tmx.web.actions.core.tools;

import com.tmx.as.modules.SpringModule;
import com.tmx.beng.access.Access;
import com.tmx.beng.access.Connection;
import com.tmx.beng.base.BillException;
import com.tmx.beng.csapi.xml.v1.base.ClientPAD;
import com.tmx.util.InitException;
import com.tmx.web.actions.core.AbstractSvbillAction;
import com.tmx.web.forms.core.tools.RefillForm;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForm;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.text.SimpleDateFormat;
import java.util.Date;

public class RefillAction extends AbstractSvbillAction
{
    Logger logger = Logger.getLogger("webcsapi."+getClass().getName());
    private final String[] prefixMTS = {"050", "066", "095", "099"};
    private final String[] prefixKS = {"067", "096", "097", "098"};
    RefillProp refillProp = new RefillProp();

    //private static final String FORWARD_TRANSACTION_UTILS_PAGE = "transactionRefillPage";

    private static final String FORWARD_TRANSACTION_REFILL_PAGE = "transactionRefillPage";

    // тут формируем запрос
    public ActionForward defaultAction(ActionMapping mapping,
                                       ActionForm form,
                                       HttpServletRequest request,
                                       HttpServletResponse response,
                                       String methodName) throws Throwable {

        RefillForm refillForm = (RefillForm) form;

        //if(refillForm.getRefillNum().equals("") || refillForm.getRefillSum().equals("") )
        if(refillForm.getRefillNum().isEmpty() || refillForm.getRefillSum().isEmpty())
        {

            refillForm.setResponseMessage("Error");
            return mapping.findForward(FORWARD_TRANSACTION_REFILL_PAGE);
        }
        else
        {
            refillForm.setRequestXml(createRefillXML(refillForm));

            if(!refillProp.getRefillNum().equals("null"))
            {
                return clientSideApiCall(mapping, refillForm, request, response);
            }
            else
            {
                return mapping.findForward(FORWARD_TRANSACTION_REFILL_PAGE);
            }
        }
    }

    private String createRefillXML(RefillForm refillForm)
    {
        //RefillProp refillProp = new RefillProp();
        String operator = getOperator(refillForm);

        if(operator.equals("KS"))
        {
            refillProp.setAmount(refillForm.getRefillSum().trim());
            refillProp.setRefillNum(refillForm.getRefillNum().trim());
            refillProp.setRefillType(refillForm.getRefillType());
            refillProp.setSellerCode("LocalSVCOM");
            refillProp.setTerminalSn("local-01");
            refillProp.setServiceCode("KyivstarPoP");
            refillProp.setTradePoint("local-01");
        }
        if(operator.equals("MTS"))
        {
            refillProp.setAmount(refillForm.getRefillSum().trim());
            refillProp.setRefillNum(refillForm.getRefillNum().trim());
            refillProp.setRefillType(refillForm.getRefillType());
            refillProp.setSellerCode("LocalSSM");
            refillProp.setTerminalSn("localssm-01");
            refillProp.setServiceCode("MTSExc");
            refillProp.setTradePoint("UN_00004");
        }

        return makeRequestXML(refillProp);
     }

    private String makeRequestXML(RefillProp refillProp)
    {
        SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy.MM.dd HH:mm:ss");
        SimpleDateFormat sdf2 = new SimpleDateFormat("HH:mm:ss");
        Date now = new Date();

        return
                "<root xmlns=\"http://tmx.com/gate/csapi/v1/base/beans\">\n" +
                "<request protocol='1.1'>\n" +
                "\t<auth>\n" +
                "\t\t<terminalSn>" + refillProp.getTerminalSn() + "</terminalSn>\n" +
                "\t\t<sellerCode>" + refillProp.getSellerCode() + "</sellerCode>\n" +
                "\t\t<processingCode>" + refillProp.processingCode + "</processingCode>\n" +
                "\t\t<login>" + refillProp.login + "</login>\n" +
                "\t\t<password>" + refillProp.password + "</password>\n" +
                "\t\t<clientTime>" + sdf1.format(now) + "</clientTime>\n" +
                "\t</auth>\n" +
                "\t<operations>\n" +
                "\t\t<refillPayment>\n" +
                "\t\t\t<cliTransactionNum>" + refillProp.getTradePoint() + "_" + refillProp.getRefillNum() + "_" + sdf2.format(now) + "</cliTransactionNum>\n" +
                "\t\t\t<serviceCode>" + refillProp.getServiceCode() + "</serviceCode>\n" +
                "\t\t\t<amount>" + refillProp.getAmount() + "</amount>\n" +
                "\t\t\t<" + refillProp.getRefillType() + ">8" + refillProp.getRefillNum() + "</" + refillProp.getRefillType() + ">\n" +
                "\t\t\t<tradePoint>" + refillProp.getTradePoint() + "</tradePoint>\n" +
                "\t\t</refillPayment>\n" +
                "\t</operations>\n" +
                "</request>\n" +
                "</root>";
    }

    private String getOperator(RefillForm refillForm)
    {
        String operator = "";
        String refillType = refillForm.getRefillType();
        String refillNum = refillForm.getRefillNum();
        if(refillType.equals("msisdn"))
        {
            operator = getOperatorByMSISDN(refillNum);
        }
        if(refillType.equals("payAccount"))
        {
            operator = getOperatorByPayAccount(refillNum);
        }

        return operator;
     }

    private String getOperatorByMSISDN(String refillNum)
    {
        String operator = "";

        String prefix = refillNum.substring(0, 3);

        for(String prf : prefixMTS)
        {
             if(prf.equals(prefix))
             {
                 operator = "MTS";
                 break;
             }
        }

        if(operator.isEmpty())
        {
            for(String prf : prefixKS)
            {
                 if(prf.equals(prefix))
                 {
                     operator = "KS";
                     break;
                 }
            }
        }

        if(operator.isEmpty())
        {
            operator = "null";
        }

        return operator;
    }

    // дописать
    private String getOperatorByPayAccount(String refillNum)
    {
        String operator = "";

        return operator;
    }

    public ActionForward clientSideApiCall(ActionMapping mapping,
                                           RefillForm refillForm,
                                           HttpServletRequest request,
                                           HttpServletResponse response) throws Throwable
    {
        ClientPAD clientPAD = new ClientPAD();
        try {

            //TODO(A.N.) Use this spring RMI connection 
            Connection conn = (Connection) SpringModule.getApplicationContext().getBean("billingConnection");

//            try {
//                Access access = obtainBillingEngineAccess(refillForm);
//                conn = access.obtainConnection();
//            } catch(InitException e) {
//                logger.error("Client initialization error: \n"+e.getTraceDump());
//                refillForm.setXmlDocSerialized(clientPAD.buildErrorResponse(ClientPAD.CSAPI_CLIENT_INIT_ERROR, getLocale(request)));
//                throw new HandledBillingException();
//            }

            long t = System.currentTimeMillis();
            ClientPAD.ParsingResult resp;

            try {
                logger.info("Request XML: " + refillForm.getRequestXml());
                resp = clientPAD.parseRequest(refillForm.getRequestXml());
                logger.debug("Parse time: " + (System.currentTimeMillis() - t));
            } catch(BillException e) {
                logger.error("Parsing error: \n"+e.getTraceDump());
                refillForm.setXmlDocSerialized(clientPAD.buildErrorResponse(ClientPAD.CSAPI_REQUEST_PARSING_ERROR, getLocale(request)));
                throw new HandledBillingException();
            }

            try {
                resp.setBillingMessage(conn.processSync(resp.getBillingMessage()));
            } catch(BillException e) {
                logger.error("Processing error: \n"+e.getTraceDump());
                refillForm.setXmlDocSerialized(clientPAD.buildErrorResponse(ClientPAD.CSAPI_REQUEST_PROCESSING_ERROR, getLocale(request)));
                throw new HandledBillingException();
            }

            refillForm.setXmlDocSerialized(clientPAD.safeSerializeResponse(resp.getBillingMessage(), resp.getVersion()));
            logger.info("Response XML: " + refillForm.getXmlDocSerialized());
            
        } catch(HandledBillingException e) { }
        catch(Throwable e) {
            logger.error("Unknown error: \n"+e.toString());
            refillForm.setXmlDocSerialized(clientPAD.buildErrorResponse(ClientPAD.CSAPI_UNKNOWN_ERROR, getLocale(request)));
        }

        refillForm.setResponseMessage(refillProp.getServiceCode() + " : " + makeResponseMessage(refillForm));
        clear(mapping, refillForm);

        return mapping.findForward(FORWARD_TRANSACTION_REFILL_PAGE);
    }

    public ActionForward clear(ActionMapping mapping,
                                   ActionForm form) throws Throwable {
        RefillForm refillForm = (RefillForm) verifyForm(form, RefillForm.class);
        refillForm.setRefillNum("");
        refillForm.setRefillSum("");
        refillForm.setResponseMessage("");

        refillProp.setAmount("");
        refillProp.setRefillNum("");
        refillProp.setRefillType("");
        refillProp.setSellerCode("");
        refillProp.setServiceCode("");
        refillProp.setTerminalSn("");
        refillProp.setTradePoint("");

        return mapping.findForward(FORWARD_TRANSACTION_REFILL_PAGE);
    }

    private String makeResponseMessage(RefillForm refillForm)
    {
        String response = "";

        String message = refillForm.getXmlDocSerialized().replaceAll("\r", "")
                                                         .replaceAll("\n", "")
                                                         .replaceAll("\t", "")
                                                         .replaceAll(" ", "")
                                                         .trim();

        String openingTag = "<statusCode>";
        String closingTag = "</statusCode>";
        if (message.indexOf ("statusCode") > 0)
            message = message.substring (message.indexOf (openingTag) + openingTag.length (), message.indexOf (closingTag));
        else
            message = "";

        if(message.equals("0"))
            response = "OK";
        else
            response = "Error : " + message;

        return response;
    }

    private Access obtainBillingEngineAccess(RefillForm refillForm) throws InitException
    {
        if(refillForm.getBillingEngineAccess() == null)
        {
            InitialContext ctx = null;            
            try {
                ctx = new InitialContext();
                System.setProperty("java.naming.factory.url.pkgs", "");
                Access newAccess = (Access) ctx.lookup("java:comp/services/billingEngine");
                if(newAccess == null) throw new InitException("err.csapi.null_basic_access_obtained");
                refillForm.setBillingEngineAccess(newAccess);
            } catch(NamingException e) {
                throw new InitException("err.csapi.failed_to_obtain_billing_engine_access", e);
            } finally {
                if(ctx != null)
                {
                    try { ctx.close(); } catch (NamingException e) {  }
                }
            }
        }
        return refillForm.getBillingEngineAccess();
    }


    private class RefillProp
    {
        final String processingCode = "Local";
        final String login = "Local";
        final String password = "7460dfdf78c61247fc883fd5e4553fb4";

        private String terminalSn;
        private String sellerCode;
        private String serviceCode;
        private String amount;

        private String refillType;
        private String refillNum;
        private String tradePoint;

        public RefillProp()
        {
            terminalSn = "null";
            sellerCode = "null";
            serviceCode = "null";
            amount = "null";
            refillType = "null";
            refillNum = "null";
            tradePoint = "null";
        }

        public String getTerminalSn() {
            return terminalSn;
        }

        public void setTerminalSn(String terminalSn) {
            this.terminalSn = terminalSn;
        }

        public String getSellerCode() {
            return sellerCode;
        }

        public void setSellerCode(String sellerCode) {
            this.sellerCode = sellerCode;
        }

        public String getServiceCode() {
            return serviceCode;
        }

        public void setServiceCode(String serviceCode) {
            this.serviceCode = serviceCode;
        }

        public String getAmount() {
            return amount;
        }

        public void setAmount(String amount) {
            this.amount = amount;
        }

        public String getRefillType() {
            return refillType;
        }

        public void setRefillType(String refillType) {
            this.refillType = refillType;
        }

        public String getRefillNum() {
            return refillNum;
        }

        public void setRefillNum(String refillNum) {
            this.refillNum = refillNum;
        }

        public String getTradePoint() {
            return tradePoint;
        }

        public void setTradePoint(String tradePoint) {
            this.tradePoint = tradePoint;
        }
    }

    private class HandledBillingException extends Throwable{ }

}