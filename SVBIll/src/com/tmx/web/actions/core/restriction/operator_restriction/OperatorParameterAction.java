package com.tmx.web.actions.core.restriction.operator_restriction;

import com.tmx.web.actions.core.restriction.AbstractRestrictionParameterAction;
import com.tmx.web.forms.core.restriction.AbstractInfoRestrictionForm;
import com.tmx.web.forms.core.restriction.operator_restriction.InfoOperatorRestrictionForm;
import com.tmx.web.base.WebException;
import com.tmx.web.base.SessionEnvironment;
import com.tmx.as.blogic.balance.AbstractBalanceManager;
import com.tmx.as.blogic.balance.OperatorBalanceManager;
import com.tmx.as.blogic.balance.SellerBalanceManager;
import org.apache.struts.action.ActionMapping;

import javax.servlet.http.HttpServletRequest;


public class OperatorParameterAction extends AbstractRestrictionParameterAction {

    protected AbstractBalanceManager getBalanceManager() {
        return new SellerBalanceManager();
    }

    protected AbstractInfoRestrictionForm getInfoRestrictionForm(ActionMapping mapping, HttpServletRequest request) throws WebException {
        return (AbstractInfoRestrictionForm) SessionEnvironment.allocateForm(
                InfoOperatorRestrictionForm.class, "core.restriction.operator_restriction.infoOperatorRestrictionForm", mapping, request);

    }
}
