package com.tmx.web.actions.core.transactions.seller_balance_transaction;

import com.tmx.as.blogic.TransactionManager;
import com.tmx.as.exceptions.DatabaseException;
import com.tmx.as.base.FilterWrapper;
import com.tmx.as.base.criterion.CriterionWrapper;
import com.tmx.web.actions.core.AbstractSvbillAction;
import com.tmx.web.controls.ScrollBox;
import com.tmx.web.controls.table.FilterSet;
import com.tmx.web.forms.core.base.AbstractFormFactory;
import com.tmx.web.forms.core.transaction.seller_balance_transaction.ManageSellerBalanceTransactionsForm;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class ManageSellerBalanceTransactionsAction extends AbstractSvbillAction {
    
    private static final String FORWARD_MANAGE_SELLER_TRANSACTIONS_PAGE = "manageSellerTransactionsPage";

    public ActionForward defaultAction(ActionMapping mapping,
                                       ActionForm form,
                                       HttpServletRequest request,
                                       HttpServletResponse response,
                                       String methodName) throws Throwable {
        ManageSellerBalanceTransactionsForm transactionsForm = (ManageSellerBalanceTransactionsForm) form;
        preloadFilters(transactionsForm);
        AbstractFormFactory.getInstance(getLoggedInUser(request)).setUpForm(transactionsForm);
        
        transactionsForm.getSellerTransactionsTable().refresh();
        setUpCalculationResult(transactionsForm);
        getRoadmap().collectParameter("/core/transactions/seller_transaction/infoSellerTransaction", "onCancelAction", "manageSellerTransactionsPage");
        return mapping.findForward(FORWARD_MANAGE_SELLER_TRANSACTIONS_PAGE);
    }

    private void setUpCalculationResult(ManageSellerBalanceTransactionsForm form) throws ClassNotFoundException, DatabaseException {
        FilterWrapper[] filterWrappers = null;
        CriterionWrapper[] criterion = null;
        try {
            filterWrappers = form.getSellerTransactionsTable().getQuery().getFilters();
            criterion = form.getSellerTransactionsTable().getQuery().getCriterions();
        } catch(Exception ex) {
            ex.printStackTrace();
        }
        String entityClassName = form.getSellerTransactionsTable().getQuery().getEntityClass();
        TransactionManager.CalculationResult calcResult
                = new TransactionManager().calculateTransactions(filterWrappers, entityClassName, criterion);
        form.getEditBoxAllTransaction().setValue( calcResult.getTransactionCaunt().toString() );
        form.getEditBoxSuccessTransaction().setValue( calcResult.getTransactionSuccessCount().toString() );
//        form.getEditBoxAllTransAmount().setValue( calcResult.getTransactionAmount().toString() );
        form.getEditBoxAllTransAmount().setValue( calcResult.getFeeForTx().toString() );
        form.getEditBoxSuccessTransAmount().setValue( calcResult.getTransactionSuccessAmount().toString() );
    }

    private void preloadFilters(ManageSellerBalanceTransactionsForm form) throws DatabaseException, com.tmx.as.exceptions.SecurityException {
        FilterSet filterSet = form.getSellerTransactionsTable().getFilterSet();
//        ScrollBox scrollBoxSellers = (ScrollBox)filterSet.getFilter("by_seller").getParameter("seller").getControl();
//        scrollBoxSellers.bind(new SellerManager().getNestedSellersInSeller(getLoggedInUser(request).getSellerOwner()));
        ScrollBox scrollBoxTransType = (ScrollBox)filterSet.getFilter("by_transaction_type").getParameter("type").getControl();
        scrollBoxTransType.bind(new TransactionManager().getAllTransactionType());
    }

}
