package com.tmx.web.base;

import org.apache.struts.util.MessageResources;
import com.sun.org.apache.xpath.internal.XPathAPI;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.w3c.dom.Node;
import org.w3c.dom.Element;
import org.dom4j.DocumentException;
import org.dom4j.io.SAXReader;

import javax.servlet.ServletContext;
import java.util.HashMap;
import java.util.Locale;
import java.net.URL;
import java.io.*;

import com.tmx.util.XMLUtil;
import com.tmx.util.StringUtil;
import com.tmx.util.InitException;

/**
 * Provides access to such resources:
 * 1) *.properties
 */
public class ResourceService implements Service {
    /**
     * The complete map of all MesssageResources defined in the struts-config.xml
     */
    private HashMap messageResources = null;
    /**
     * Default path to struts main config file. Overrite it in the web.xml using <code>struts.config_file</code> parameter
     */
    private String pathStrutsConfig = "/WEB-XML/struts-config.xml";
    /** Struts config XML. Could be used by another services. For instance by UserInterfaceService. */
    private Document strutsConfigXML = null;

    /**
     * This servise is initialized before Struts ActionServlet does.
     * So the ModuleConfig is not created and stored in the ServletContext yet.
     * Thus we should parse struts-config.xml ourselves to get message-resource.
     */
    public void init(ServletContext context) throws InitException {
        try {
            messageResources = new HashMap();
            //Get the struts-config.xml filename
            String strutsConfigFile = ApplicationEnvironment.getInitParameter("struts.config_file", pathStrutsConfig);
            URL url = context.getResource(strutsConfigFile);
            // If the config isn't in the servlet context, try the class loader
            // which allows the config files to be stored in a jar
            if (url == null) {
                url = getClass().getResource(strutsConfigFile);
            }
            if (url == null)
                throw new InitException("err.init.resource_service.struts_config_not_found", Locale.getDefault());

            Document doc = XMLUtil.getDocumentFromURL(url);
            strutsConfigXML = doc; //save the struts-config.xml to be available for other services at startup time.
            String xpathMsgResources = "/struts-config/message-resources";
            NodeList resourcesNodes = XPathAPI.selectNodeList(doc, xpathMsgResources);
            if (resourcesNodes != null) {
                for (int i = 0; i < resourcesNodes.getLength(); i++) {
                    Node resNode = resourcesNodes.item(i);
                    if (resNode.getNodeType() == Node.ATTRIBUTE_NODE)
                        continue;
                    String key = ((Element) resNode).getAttribute("key");
                    String parameter = ((Element) resNode).getAttribute("parameter");
                    String showNull = ((Element) resNode).getAttribute("null");
                    MessageResources resorces = MessageResources.getMessageResources(parameter);

                    if (showNull != null && "false".equals(showNull.trim().toLowerCase()))
                        resorces.setReturnNull(false);

                    messageResources.put(key, resorces);
                }

            }

        }
        catch (InitException e) {
            throw e;
        }
        catch (Exception e) {
            throw new InitException("err.init.resource_service.general_failure", Locale.getDefault());
        }
    }

    /**
     * Get message from resource properties by <code>locale</code>, <code>bundle</code> and <code>key</code>
     */
    public String getMessage(Locale locale, String bundle, String key, String[] params) {
        MessageResources resource = (MessageResources) messageResources.get(bundle);
        if (resource == null)
            return null;
        if (locale == null)
            locale = Locale.getDefault();
        if (params == null)
            return resource.getMessage(locale, key);
        switch (params.length) {
            case 1 :
                return resource.getMessage(locale, key, params[0]);
            case 2 :
                return resource.getMessage(locale, key, params[0], params[1]);
            case 3 :
                return resource.getMessage(locale, key, params[0], params[1], params[2]);
            case 4 :
                return resource.getMessage(locale, key, params[0], params[1], params[2], params[3]);
                //If count of icoming params exceeds four then cut all after four.
            default :
                return resource.getMessage(locale, key, params[0], params[1], params[2], params[3]);
        }
    }

    /**
     * Get message from resource properties by <code>bundle</code> and <code>key</code>.
     * The default server locale are used.
     */
    public String getMessage(String bundle, String key, String[] params) {
        return getMessage(null, bundle, key, params);
    }

    /**
     * Saves uploded file in given folder. And generates random unique file name.
     */
    public String saveUploadedFile(String uploadDir, String fileName, byte[] uploadedFile) throws ResourceException {
        String uniqueFileName = null;
        try {
            uniqueFileName = generateUniqueFileName(fileName);
            String uploadedFilePath = uploadDir + File.separator + uniqueFileName;
            FileOutputStream fos = new FileOutputStream(uploadedFilePath);
            fos.write(uploadedFile);
            fos.flush();
        }
        catch (IOException e) {
            throw new ResourceException("err.run.resource_service.failed_to_save_file", e);
        }
        return uniqueFileName;
    }


    private String generateUniqueFileName(String originalFileName) {
        String uniqueFileName = StringUtil.generateUniqueDigitString();
        uniqueFileName = uniqueFileName + originalFileName.substring(originalFileName.lastIndexOf("."));
        return uniqueFileName;
    }

    /**
     * Return source ClassLoader.
     */
    private ClassLoader getSource() {
        return Thread.currentThread().getContextClassLoader();
    }

    /**
     * Return the URL for searching resource
     *
     * @param resourceName name of resource
     * @return a URL obect for reading the resource, or null if the resource could not be found
     */
    public URL getResource(String resourceName) {
        return getSource().getResource(resourceName);
    }

    /**
     * Return the InputStream for searching resource.
     * <b>ATTENTION: </b> you must close InputStream after use.
     *
     * @param resourceName name of resource
     * @return a InputStream obect for reading the resource, or null if the resource could not be found
     */
    public InputStream getResourceAsStream(String resourceName) {
        return getSource().getResourceAsStream(resourceName);
    }

    /**
     * Return the InputStream for searching resource
     * <b>ATTENTION: </b> you must close InputStream after use.
     *
     * @param resourceUrl URL object, that represent the resource
     * @return a InputStream obect for reading the resource, or null if the resource could not be found
     */
    public InputStream getResourceAsStream(URL resourceUrl) {
        return getSource().getResourceAsStream(resourceUrl.toString());
    }

    /**
     * Return the Document object, that have been readed from file with specified name.
     *
     * @param fileName name of file with xml document
     * @return the Document object for work with xml
     * @throws org.dom4j.DocumentException if an error occurs in the file parcing process
     */
    public org.dom4j.Document getXMLDocument(String fileName) throws DocumentException {
        org.dom4j.Document result;
        try {
            InputStream input = getResourceAsStream(fileName);
            result = (new SAXReader()).read(input);
            input.close();
        } catch (Exception e) {
            throw new DocumentException("File parse error: " + e);
        }
        return result;
    }

    /**
     * Return the InputStream for file
     * <b>ATTENTION: </b> you must close InputStream after use.
     *
     * @param fullFileName full name of file with path
     * @return a InputStream obect for reading the resource, or null if the resource could not be found
     */
    public InputStream getFileAsStream(String fullFileName) throws FileNotFoundException {
        return new FileInputStream(fullFileName);
    }

    /**
     * Return the Document object, that have been readed from file with specified name.
     *
     * @param fullFileName full name of file with xml document
     * @return the Document object for work with xml
     * @throws DocumentException if an error occurs in the file parcing process
     */
    public org.dom4j.Document getXMLDocumentFromFile(String fullFileName) throws DocumentException {
        org.dom4j.Document result;
        try {
            InputStream input = getFileAsStream(fullFileName);
            result = (new SAXReader()).read(input);
            input.close();
        } catch (Exception e) {
            throw new DocumentException("File parse error: " + e);
        }
        return result;
    }

    /**
     * Save text to the plaintext file
     *
     * @param fileName file name with full path
     * @param content  content to save
     * @param encoding content encoding
     */
    public void savePlaintextToFile(String fileName, String content, String encoding) throws IOException {
        // Check for destination directory exist. If directory is not exist - create it.
        java.io.File file = new java.io.File(StringUtil.extractPath(fileName));
        if (! file.exists()) {
            file.mkdirs();
        }

        PrintWriter htmlValue = new PrintWriter(
                new BufferedWriter(
                        new OutputStreamWriter(
                                new FileOutputStream(fileName),
                                encoding
                        )
                )
        );
        htmlValue.print(content);
        htmlValue.close();
    }

    /**
     * Read text from the plaintext file
     *
     * @param fileName file name with full path
     * @param encoding content encoding
     */
    public String readPlaintextFromFile(String fileName, String encoding) throws IOException {
        StringBuffer res = new StringBuffer();
        BufferedReader value = new BufferedReader(
                new InputStreamReader(
                        new FileInputStream(fileName),
                        encoding
                )
        );
        String line;
        while ((line = value.readLine()) != null) {
            res.append(line);
        }
        value.close();
        return res.toString();
    }

    /**
     * Delete directory recursivelly
     */
    public void deleteDirectory(String pathToDirectory) {
        if (pathToDirectory == null)
            return;

        File currentFile = new File(pathToDirectory);
        if(!currentFile.isDirectory())
            return;

        File allFiles[] = currentFile.listFiles();
        for (int i = 0; i < allFiles.length; i++) {
            if (allFiles[i].isDirectory())
                deleteDirectory(allFiles[i].toString());
            else
                allFiles[i].delete();
        }
        currentFile.delete();
    }

    /** Recurcively copies <code>fromDir</code> into <code>toDir</code>  */
    public void copyDirectory(String fromDir, String toDir) throws ResourceException{
        try {
            fromDir = StringUtil.normalize(fromDir);
            toDir = StringUtil.normalize(toDir);

            File[] fromDirListing = new File(fromDir).listFiles();

            //create to_dir
            new File(toDir).mkdirs();

            for (int i = 0; i < fromDirListing.length; i++) {
                String tempString = toDir + java.io.File.separatorChar + fromDirListing[i].getName();
                if (fromDirListing[i].isDirectory()) {
                    new File(tempString).mkdirs();
                    copyDirectory(fromDirListing[i].getAbsolutePath(), tempString);
                }
                else {
                    FileInputStream in = new FileInputStream(fromDirListing[i]);
                    FileOutputStream out = new FileOutputStream(tempString);
                    int c;
                    while ((c = in.read()) != -1)
                        out.write(c);
                    out.flush();
                    in.close();
                    out.close();
                }
                fromDirListing[i] = null;//free resource
            }
        }
        catch (IOException e) {
            throw new ResourceException("err.failed_to_copy_dir", new String[]{fromDir, toDir}, e);
        }
    }

    /** Return struts config as parsed XML. This method could be useful at startup time
     * then Struts servlet is not inited yet and that config is not loaded.
     * Then you should parse struts-config.jsp yourself. */
    public Document getStrutsConfigXML() {
        return strutsConfigXML;
    }
}