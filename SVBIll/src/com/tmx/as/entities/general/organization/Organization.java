package com.tmx.as.entities.general.organization;

import com.tmx.as.base.BasicEntity;

import java.io.Serializable;

/**
 * Created by IntelliJ IDEA.
 * user: galyamov
 * Date: 18/2/2006
 * Time: 13:16:05
 * To change this template use File | Settings | File Templates.
 */
public class Organization extends BasicEntity {

    private Long organizationId;
    private String name;

    public Long getOrganizationId() {
        return organizationId;
    }

    public void setOrganizationId(Long organizationId) {
        this.organizationId = organizationId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Serializable getId() {
        return getOrganizationId();
    }

    public int getValid() {
        return valid;
    }

    public void setValid(int valid) {
        this.valid = valid;
    }

}
