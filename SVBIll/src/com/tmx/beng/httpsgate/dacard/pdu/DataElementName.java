package com.tmx.beng.httpsgate.dacard.pdu;

import java.io.InputStream;
import java.io.OutputStream;
import java.io.IOException;

/**

 */
public enum DataElementName {
    TAG_MSGTYPE("MTID"),
    TAG_TERMINALID("TerminalID"),
    TAG_ERRORCODE("Error"),
    TAG_ERRORDESC("ErrDesc"),
    TAG_MSGCOUNT("MsgCount"),
    TAG_RECONINDICATOR("RI"),
    TAG_LOCALDATE("DATE"),
    TAG_AMOUNT("AMOUNT"),
    TAG_ARTICLEID("ARTICLE"),
    TAG_FW_VWERION("FW_VERSION"),
    TAG_ARTICLE_GROUP("AGroup"),
    TAG_ARTICLE_NAME("ProductName"),
    TAG_TEMPLATE("TEMPLATE"),
    TAG_FREEITEMS("FREEITEMS"),
    TAG_APPROVAL("APPROVAL"),
    TAG_ITEMID("Item"),
    TAG_ORIGCOUNT("OrigCount"),
    TAG_ORIGLDT("OrigLDT"),
    TAG_RRN("RRN"),
    TAG_CACHE_AMOUNT("CacheAmount"),
    TAG_CACHE_WARNING("CacheWarning"),
    TAG_MIN_CACHE_QUANTITY("Min_CQ"),
    TAG_MAX_CACHE_QUANTITY("Max_CQ"),
    TAG_ARTICLE_CODE("ArticleCode"),
    TAG_TEMPLATE_BLOB("TPBLOB"),
    TAG_PRODUCT_TYPE("PTypeCode"),
    TAG_REVERSAL_MODE("EnableRev"),
    TAG_COMMIT_MODE("EnableCommit"),
    TAG_VERIFY_MODE("EnableVerify"),
    TAG_IS_PRODUCR_TEST("IsTestProd"),
    TAG_INVOICE_INFO("IInfo"),
    TAG_INVOICE_RRN("IRRN"),
    TAG_INVOICE_UNIQUE_RRN("IUniqRRN"),
    TAG_CASHIER_NO("CashierNo"),
    TAG_OFFLINE_SALE_DATETIME("OfflineDate"),
    TAG_VERSION_INFO("Ver"),
    TAG_VERSION_INFO_STRING("VerStr"),
    TAG_TERMINAL_MODEL_INFO("Model"),
    TAG_FILE_REQUEST_POS("FilePos"),
    TAG_FILE_SIZE("FileSize"),
    TAG_FILE_CRC16("FileCRC16"),
    TAG_FILE_MAX_RESPONSE_CHUNK_DATA_SIZE("ChunkMaxSize"),
    TAG_FILE_CONTENT("FileContent"),
    TAG_NEWTERMINALID("NewTerminalID"),
    TAG_MK("MK"),
    TAG_SK("SK"),
    TAG_TI_COMPANY("TICompany"),
    TAG_TI_CITY("TICity"),
    TAG_TI_ADDRESS("TIAddr"),
    TAG_TI_DEPARTMENT("TIDep"),
    TAG_TI_PHONES("TIPhones"),
    TAG_TI_DESCRIPTION("TIDesc"),
    TAG_TI_MFO("TIMFO"),
    TAG_TI_REGIONID("TIRegID"),
    TAG_FEE1_CALC_TYPE("Fee1CT"),
    TAG_FEE1_VALUE("Fee1Val"),
    TAG_FEE1_MIN("Fee1Min"),
    TAG_FEE1_MAX("Fee1Max"),
    TAG_FEE2_CALC_TYPE("Fee2CT"),
    TAG_FEE2_VALUE("Fee2Val"),
    TAG_FEE2_MIN("Fee21Min"),
    TAG_FEE2_MAX("Fee2Max"),
    TAG_FEE1("Fee1"),
    TAG_FEE2("Fee2"),
    TAG_FEE("Fee"),//Added by RM
    TAG_PRN_1_NAME("PRN1Name"),
    TAG_PRN_1_VALUE("PRN1Val"),
    TAG_PRN_2_NAME("PRN2Name"),
    TAG_PRN_2_VALUE("PRN2Val"),
    TAG_PRN_3_NAME("PRN3Name"),
    TAG_PRN_3_VALUE("PRN3Val"),
    TAG_PRN_4_NAME("PRN4Name"),
    TAG_PRN_4_VALUE("PRN4Val"),
    TAG_PRN_5_NAME("PRN5Name"),
    TAG_PRN_5_VALUE("PRN5Val"),
    TAG_ENABLE_EXT_INFO("EXI"),
    TAG_ORIGINAL_AC("OrigAC"),
    TAG_UNKNOWN("NOT_SUPPORT");

    private String value;

    DataElementName(String value) {
        this.value = value;
    }

    String getValue(){
        return value;
    }

    int getSize(){
        return value.getBytes().length;
    }

    /** Parse and validate name. 
     * @param is input stream to read data from;
     * @param size amount of bytes to read
     * @return produced DataElementName 
     * @throws DacardPadException on parsing or validation error.
     *  */
    static DataElementName parse(InputStream is, int size) throws DacardPadException{
        byte[] buffer = new byte[size];
        try{
            is.read(buffer);
        }
        catch (IOException e){
            throw new DacardPadException("Failed to parse DataElement name", e);                                                        
        }
        final String value = new String(buffer);
        for(DataElementName den : DataElementName.values()){
            if(den.getValue().equals(value)) {
                return den;
            }
        }
        return DataElementName.TAG_UNKNOWN;
        //throw new DacardPadException("Unknown DataElement name", new String[]{value});
    }

    void serialize(OutputStream os) throws DacardPadException{
        try{
            os.write(value.getBytes(), 0, getSize());
        }
        catch (IOException e){
            throw new DacardPadException("Failed to serialize DataElement name", e);
        }
    }
    
}
