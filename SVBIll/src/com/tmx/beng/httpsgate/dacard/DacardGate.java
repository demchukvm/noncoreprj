package com.tmx.beng.httpsgate.dacard;

import com.tmx.beng.httpsgate.HttpsGateException;
import com.tmx.util.InitException;
import org.apache.log4j.Logger;

/**
 */
public class DacardGate extends BasicDacardGate{
    protected static final String GATE_NAME = "dacard_gate";
    protected static Logger logger = Logger.getLogger(GATE_NAME);
    private static DacardGate gate = null;

    public static DacardGate getInstance() throws HttpsGateException {
        if(gate == null){
            logger.error("Attempt to obtain not inited billing engine");
            throw new HttpsGateException("err.DacardGate.null_instance");
        }
        return gate;
    }

    public static void init() throws InitException {
        logger.info("Start DacardGate initialization");
        gate = new DacardGate();
        gate.init0();
        logger.info("DacardGate initialized");
    }


    public static void shutdown(final long timeout) throws InitException {
        logger.info("Start DacardGate Shutdown");
        gate.shutdown0(timeout);
        logger.info("DacardGate shutdown");
    }

    protected String getGateName(){
        return GATE_NAME;
    }

}
