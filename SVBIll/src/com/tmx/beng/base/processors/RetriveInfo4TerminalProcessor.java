package com.tmx.beng.base.processors;

import com.tmx.beng.base.*;
import com.tmx.beng.base.cache.DBCache;
import com.tmx.beng.base.cache.DBCacheException;
import com.tmx.as.entities.bill.terminal.Terminal;
import com.tmx.as.entities.bill.balance.SellerBalance;
import com.tmx.as.entities.bill.balance.TerminalBalance;
import com.tmx.as.entities.bill.seller.Seller;
import com.tmx.as.blogic.TransactionManager;
import com.tmx.as.blogic.balance.SellerBalanceManager;
import com.tmx.as.blogic.balance.TerminalBalanceManager;
import com.tmx.as.exceptions.DatabaseException;

import java.util.GregorianCalendar;
import java.util.Date;
import java.util.Calendar;
import java.text.SimpleDateFormat;
import java.text.ParseException;

/**

 */
public class RetriveInfo4TerminalProcessor extends TaskProcessor {

    protected Task process0(Task task, DBCache dbCache, BillingMessageWritable msg) throws BillException {
        logger.debug("Retrive info for terminal processor...");

        String voucherNominal = msg.getAttributeString(BillingMessage.VOUCHER_NOMINAL);
        String tBalanceAmount;
        String sBalanceAmount;
        /*request balances*/        
        if (voucherNominal.toLowerCase().indexOf("balances") > 0) {
            try {
                 Terminal terminal = dbCache.getTerminal(msg.getAttribute(BillingMessage.TERMINAL_SN).toString());
                //retrive terminal balance
                try {
                    TerminalBalance terminalBalance = (TerminalBalance)(new TerminalBalanceManager()).getBalance(terminal.getPrimaryBalance().getBalanceId());
                    tBalanceAmount = terminalBalance.getAmount().toString();
                } catch (DatabaseException e) {
                    logger.error("can`t retrive terminal balance " + e.getMessage());
                    tBalanceAmount = e.getMessage();
                }
                //retrive seller balance
                try {
                    Seller seller = dbCache.getSellerById(terminal.getSellerOwnerId());
                    SellerBalance sellerBalance = (SellerBalance)(new SellerBalanceManager()).getBalance(seller.getPrimaryBalance().getBalanceId());
                    sBalanceAmount = sellerBalance.getAmount().toString();
                } catch (DatabaseException e) {
                    logger.error("can`t retrive seller balance " + e.getMessage());
                    sBalanceAmount = e.getMessage();
                }
                msg.setStatusCode(StatusDictionary.UNKNOWN_ERROR);
                throw new BillException("", new String[]{"TERMINAL= ".concat(tBalanceAmount), "DEALER= ".concat(sBalanceAmount)});
            } catch (DBCacheException e) {
                msg.setStatusCode(StatusDictionary.TEST_ERROR);
                throw new BillException("failed_to_get_balances", new String[]{msg.getAttribute(BillingMessage.TERMINAL_SN).toString()});
            } catch (Exception e) {
                msg.setStatusCode(StatusDictionary.TEST_ERROR);
                throw new BillException("failed_to_get_balances " + e.getMessage());
            }
        } else if (voucherNominal.toLowerCase().indexOf("finactivites") > 0) {/*retrive financial activites for period*/
            GregorianCalendar dateFrom = new GregorianCalendar();
            GregorianCalendar dateTo = new GregorianCalendar();
            dateTo.set(Calendar.HOUR_OF_DAY, 23);
            dateTo.set(Calendar.SECOND, 59);
            dateTo.set(Calendar.MINUTE, 59);

            try {
                String periodFrom = msg.getAttribute(BillingMessage.PAY_ACCOUNT_NUMBER).toString();
                if (periodFrom.length() == 6) {
                    dateFrom.setTime((new SimpleDateFormat("ddMMyy")).parse(msg.getAttribute(BillingMessage.PAY_ACCOUNT_NUMBER).toString()));
                } else if (periodFrom.length() == 4) {
                    dateFrom.setTime((new SimpleDateFormat("ddMM")).parse(msg.getAttribute(BillingMessage.PAY_ACCOUNT_NUMBER).toString()));
                    dateFrom.set(Calendar.YEAR, dateTo.get(Calendar.YEAR));
                } else {
                    throw new BillException("Date is not set correctly. [Day Month Year]");
                }
            } catch (ParseException e) {
               throw new BillException("Date is not set correctly. [Day Month Year] " + e.getMessage());
            } catch (NullPointerException e) {
               throw new BillException("Date is not set. [Day Month Year] " + e.getMessage());
            }
            dateFrom.set(Calendar.HOUR_OF_DAY, 0);
            dateFrom.set(Calendar.SECOND, 0);
            dateFrom.set(Calendar.MINUTE, 0);

            TransactionManager.CashCalculation calculation = getFinActivities(dbCache, msg, dateFrom.getTime(), dateTo.getTime());
            msg.setStatusCode(StatusDictionary.UNKNOWN_ERROR);
            throw new BillException("", new String[]{"Bonus= ".concat(calculation.getFullProfit().toString()),
                                                        "Tx= ".concat(calculation.getAmountTx().toString()),
                                                        "Credit= ".concat(calculation.getTakenFromRootSeller().toString())});
        }
        return task;
    }

    private TransactionManager.CashCalculation getFinActivities(DBCache dbCache, BillingMessageWritable msg,
                                                                Date dateFrom, Date dateTo) throws BillException {

        TransactionManager manager = new TransactionManager();
        Long sellerId;
        Long balanceId;
        try {
            sellerId = (dbCache.getTerminal(msg.getAttribute(BillingMessage.TERMINAL_SN).toString())).getSellerOwnerId();
            balanceId = dbCache.getSellerById(sellerId).getPrimaryBalance().getBalanceId();
        } catch (DBCacheException e) {
            throw new BillException("err_get_seller_owner " + e.getMessage());
        }
        try {
            return manager.getCashCalculation(balanceId, sellerId, dateFrom, dateTo);
        } catch (DatabaseException e) {
            throw  new BillException("err_get_fin.activities " + e.getMessage());
        }

    }
}