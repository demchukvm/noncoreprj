package com.tmx.as.entities.iso8583.collation;

import com.tmx.as.base.BasicEntity;
import com.tmx.as.entities.iso8583.transaction.Iso8583Transaction;
import java.util.Date;
import java.io.Serializable;

/**

 */
public class Collation extends BasicEntity {
    private Long collationId;
    private String terminalId;
    private String merchantId;
    private Date startCollationTime;
    private Date endCollationTime;
    private boolean successful;
    private boolean closed;
    private Long numberOfBatch;
    private Long billingSalesCountCollation;
    private Long terminalSalesCountCollation;
    private Long billingSalesAmountCollation;
    private Long terminalSalesAmountCollation;
    private Long billingVoidCountCollation;
    private Long terminalVoidCountCollation;
    private Long billingVoidAmountCollation;
    private Long terminalVoidAmountCollation;
    private Long billingSalesCountEndBatch;
    private Long terminalSalesCountEndBatch;
    private Long billingSalesAmountEndBatch;
    private Long terminalSalesAmountEndBatch;
    private Long billingVoidCountEndBatch;
    private Long terminalVoidCountEndBatch;
    private Long billingVoidAmountEndBatch;
    private Long terminalVoidAmountEndBatch;

    private Iso8583Transaction startCollationTransaction;
    private Iso8583Transaction endCollationTransaction;


    public Long getCollationId() {
        return collationId;
    }

    public void setCollationId(Long collationId) {
        this.collationId = collationId;
    }

    public String getTerminalId() {
        return terminalId;
    }

    public void setTerminalId(String terminalId) {
        this.terminalId = terminalId;
    }

    public String getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    public Date getStartCollationTime() {
        return startCollationTime;
    }

    public void setStartCollationTime(Date startCollationTime) {
        this.startCollationTime = startCollationTime;
    }

    public Date getEndCollationTime() {
        return endCollationTime;
    }

    public void setEndCollationTime(Date endCollationTime) {
        this.endCollationTime = endCollationTime;
    }

    public boolean getSuccessful() {
        return successful;
    }

    public void setSuccessful(boolean successful) {
        this.successful = successful;
    }

    public boolean getClosed() {
        return closed;
    }

    public void setClosed(boolean closed) {
        this.closed = closed;
    }

    public Long getNumberOfBatch() {
        return numberOfBatch;
    }

    public void setNumberOfBatch(Long numberOfBatch) {
        this.numberOfBatch = numberOfBatch;
    }

    public Long getBillingSalesCountCollation() {
        return billingSalesCountCollation;
    }

    public void setBillingSalesCountCollation(Long billingSalesCountCollation) {
        this.billingSalesCountCollation = billingSalesCountCollation;
    }

    public Long getTerminalSalesCountCollation() {
        return terminalSalesCountCollation;
    }

    public void setTerminalSalesCountCollation(Long terminalSalesCountCollation) {
        this.terminalSalesCountCollation = terminalSalesCountCollation;
    }

    public Long getBillingSalesAmountCollation() {
        return billingSalesAmountCollation;
    }

    public void setBillingSalesAmountCollation(Long billingSalesAmountCollation) {
        this.billingSalesAmountCollation = billingSalesAmountCollation;
    }

    public Long getTerminalSalesAmountCollation() {
        return terminalSalesAmountCollation;
    }

    public void setTerminalSalesAmountCollation(Long terminalSalesAmountCollation) {
        this.terminalSalesAmountCollation = terminalSalesAmountCollation;
    }

    public Long getBillingVoidCountCollation() {
        return billingVoidCountCollation;
    }

    public void setBillingVoidCountCollation(Long billingVoidCountCollation) {
        this.billingVoidCountCollation = billingVoidCountCollation;
    }

    public Long getTerminalVoidCountCollation() {
        return terminalVoidCountCollation;
    }

    public void setTerminalVoidCountCollation(Long terminalVoidCountCollation) {
        this.terminalVoidCountCollation = terminalVoidCountCollation;
    }

    public Long getBillingVoidAmountCollation() {
        return billingVoidAmountCollation;
    }

    public void setBillingVoidAmountCollation(Long billingVoidAmountCollation) {
        this.billingVoidAmountCollation = billingVoidAmountCollation;
    }

    public Long getTerminalVoidAmountCollation() {
        return terminalVoidAmountCollation;
    }

    public void setTerminalVoidAmountCollation(Long terminalVoidAmountCollation) {
        this.terminalVoidAmountCollation = terminalVoidAmountCollation;
    }

    public Long getBillingSalesCountEndBatch() {
        return billingSalesCountEndBatch;
    }

    public void setBillingSalesCountEndBatch(Long billingSalesCountEndBatch) {
        this.billingSalesCountEndBatch = billingSalesCountEndBatch;
    }

    public Long getTerminalSalesCountEndBatch() {
        return terminalSalesCountEndBatch;
    }

    public void setTerminalSalesCountEndBatch(Long terminalSalesCountEndBatch) {
        this.terminalSalesCountEndBatch = terminalSalesCountEndBatch;
    }

    public Long getBillingSalesAmountEndBatch() {
        return billingSalesAmountEndBatch;
    }

    public void setBillingSalesAmountEndBatch(Long billingSalesAmountEndBatch) {
        this.billingSalesAmountEndBatch = billingSalesAmountEndBatch;
    }

    public Long getTerminalSalesAmountEndBatch() {
        return terminalSalesAmountEndBatch;
    }

    public void setTerminalSalesAmountEndBatch(Long terminalSalesAmountEndBatch) {
        this.terminalSalesAmountEndBatch = terminalSalesAmountEndBatch;
    }

    public Long getBillingVoidCountEndBatch() {
        return billingVoidCountEndBatch;
    }

    public void setBillingVoidCountEndBatch(Long billingVoidCountEndBatch) {
        this.billingVoidCountEndBatch = billingVoidCountEndBatch;
    }

    public Long getTerminalVoidCountEndBatch() {
        return terminalVoidCountEndBatch;
    }

    public void setTerminalVoidCountEndBatch(Long terminalVoidCountEndBatch) {
        this.terminalVoidCountEndBatch = terminalVoidCountEndBatch;
    }

    public Long getBillingVoidAmountEndBatch() {
        return billingVoidAmountEndBatch;
    }

    public void setBillingVoidAmountEndBatch(Long billingVoidAmountEndBatch) {
        this.billingVoidAmountEndBatch = billingVoidAmountEndBatch;
    }

    public Long getTerminalVoidAmountEndBatch() {
        return terminalVoidAmountEndBatch;
    }

    public void setTerminalVoidAmountEndBatch(Long terminalVoidAmountEndBatch) {
        this.terminalVoidAmountEndBatch = terminalVoidAmountEndBatch;
    }

    public Iso8583Transaction getStartCollationTransaction() {
        return startCollationTransaction;
    }

    public void setStartCollationTransaction(Iso8583Transaction startCollationTransaction) {
        this.startCollationTransaction = startCollationTransaction;
    }

    public Iso8583Transaction getEndCollationTransaction() {
        return endCollationTransaction;
    }

    public void setEndCollationTransaction(Iso8583Transaction endCollationTransaction) {
        this.endCollationTransaction = endCollationTransaction;
    }

    public Serializable getId() {
        return getCollationId();
    }

    public int getValid() {
        return valid;
    }

    public void setValid(int valid) {
        this.valid = valid; 
    }
}
