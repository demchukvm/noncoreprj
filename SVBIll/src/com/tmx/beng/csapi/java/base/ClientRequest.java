package com.tmx.beng.csapi.java.base;

import com.tmx.beng.access.Connection;
import com.tmx.beng.base.*;
import com.tmx.beng.base.BillingMessage;
import org.apache.log4j.Logger;


abstract public class ClientRequest<T> {
    private Connection connect;
    protected BillingMessageWritable requestMsg = new BillingMessageImpl();
    private Boolean sent = false;

    // Proximan 04.11.2010
  //  Logger logger = Logger.getLogger("webcsapi."+getClass().getName());
    //Logger logger1 = Logger.getLogger("gen."+getClass().getName());
    //

    ClientRequest(Connection connect) {
        this.connect = connect;
    }

    public T send() throws BillException {
        if (isSent())
            throw new IllegalStateException("connection is closed");
        com.tmx.beng.base.BillingMessage responseMsg = connect.processSync(requestMsg);
        connect = null;

        // Proximan 04.11.2010
      //  if()
       // logger.info("Response XML GEN_Voucher: " + responseMsg);
        //logger1.info("Response XML: " + responseMsg);
        //

        return handleResponseMessage(responseMsg);
    }

    abstract protected T handleResponseMessage(BillingMessage responseMsg);

    public Boolean isSent() {
        return sent;
    }
}
