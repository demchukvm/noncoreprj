package com.tmx.beng.base.dbfunctions;

import com.tmx.as.entities.bill.balance.SellerBalance;
import com.tmx.as.entities.bill.seller.Seller;
import com.tmx.beng.base.BillingMessage;
import com.tmx.beng.base.BillingMessageWritable;
import com.tmx.beng.base.Reservation;
import com.tmx.beng.base.ReservationSet;
import com.tmx.beng.base.cache.InvokeFunctionException;

import java.util.Iterator;
import java.util.Map;

/**
    To  controll minimun and maximum balance limit of
    seller balance after tariff caculation for given transaction.
 */
public class SellerBalanceMinMaxLimit extends BasicLimitFunction{
    private static final String MIN_LIMIT = "min_limit";
    private static final String MAX_LIMIT = "max_limit";
    private static final String BALANCE_CODE = "balance_code";
    private static final String SELLER_CODE = "seller_code";

    public void checkLimit(Map actualParams) throws InvokeFunctionException {
        try{
            BillingMessageWritable billingMessage = task.getProcesedMessage();
            //get checking limits
            Double minLimit = actualParams.get(MIN_LIMIT) != null ? new Double((String)actualParams.get(MIN_LIMIT)) : null;
            Double maxLimit = actualParams.get(MAX_LIMIT) != null ? new Double((String)actualParams.get(MAX_LIMIT)) : null;
            String balanceCode = (String)actualParams.get(BALANCE_CODE);
            balanceCode = (balanceCode != null && balanceCode.trim().length() <= 0) ? null : balanceCode;//reset empty code to null
            if(minLimit == null && maxLimit == null)
                return;//nothing to check                        

            //obtain current seller balance
            String sellerCode;
            if (actualParams.containsKey(SELLER_CODE)) {
                sellerCode = (String)actualParams.get(SELLER_CODE);
            } else {
                sellerCode = billingMessage.getAttributeString(BillingMessage.SELLER_CODE);
            }

            //obtain seller tariffed amount
            ReservationSet reserveationSet = (ReservationSet)billingMessage.getAttribute(BillingMessage.BALANCE_RESERVATION_SET);
            Reservation reservation = reserveationSet.getReservation(Seller.class, sellerCode);
            for(Iterator<Reservation.Reserve> iter = reservation.reservesIterator(); iter.hasNext();){
                Reservation.Reserve reserve = iter.next();
                SellerBalance sellerBalance = task.getSessionDBCache().getSellerBalance(sellerCode, reserve.getBalanceCode());

                if(balanceCode == null && !sellerBalance.getPrimary())
                    continue;//ignore this balance, look for primary balance

                if(balanceCode != null && !balanceCode.equals(sellerBalance.getCode()))
                    continue;//ignore this balance, look for balance with same code

                double operationAmount = reserve.getAmount();

                //invert operation amount on debit operation
                if(BillingMessage.O_REFILL_PAYMENT.equals(billingMessage.getOperationName()) ||
                   BillingMessage.O_BUY_VOUCHER.equals(billingMessage.getOperationName())){
                    operationAmount = - operationAmount;
                }

                //evalulate new seller balance if this transaction will be commited
                double newSellerBalance = sellerBalance.getAmount() + operationAmount;

                //check min limit
                if(minLimit != null && newSellerBalance < minLimit)
                    throw new InvokeFunctionException("err.SellerBalance<MIN",
                            new String[]{minLimit.toString(),
                                         String.valueOf(operationAmount),
                                         sellerCode});

                //check max limit
                if(maxLimit != null && newSellerBalance > maxLimit)
                    throw new InvokeFunctionException("err.SellerBalance>MAX",
                            new String[]{maxLimit.toString(),
                                         String.valueOf(operationAmount),
                                         sellerCode});

            }

            //limits is ok
        }
        catch(Throwable e){
            throw new InvokeFunctionException(e.getLocalizedMessage(), e);
        }
    }

    public ValidationError[] validate(Map actualParams) {
        if (actualParams.get(MIN_LIMIT) == null || actualParams.get(MAX_LIMIT) == null) {
            ValidationError error = new ValidationError("error");
            return new ValidationError[] {error};
        }
        return null;
    }

    public TariffParameter[] getDefaultParams() {
        return new TariffParameter[] {
                new TariffParameter(MIN_LIMIT, "0", true),
                new TariffParameter(MAX_LIMIT, "1000000", true),
        };
    }

}
