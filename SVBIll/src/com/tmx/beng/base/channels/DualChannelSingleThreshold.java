package com.tmx.beng.base.channels;

import com.tmx.beng.base.BillingMessage;
import com.tmx.beng.base.BillException;

/**  Dual out channel with single threshold.
 * if(amount < amountThreshold) { first channel used } else { second channel used } */
public class DualChannelSingleThreshold extends AbstractChannel{

    private final static String AMOUNT_SEPARATOR = "amountThreshold";

    public OutChannel process(BillingMessage msg) throws BillException {
        if(msg.getAttributeString(BillingMessage.AMOUNT) == null)
            throw new BillException("err.bill.null_pointer_amount_atribute_is_absent");

        try {
            Double amount = Double.valueOf(msg.getAttributeString(BillingMessage.AMOUNT));
            return (amount.doubleValue() < getAmountThreshold()) ?
                    getOutChannel(1) :
                    getOutChannel(2);
        }
        catch (NumberFormatException e) {
            throw new BillException("err.bill.cannot_parse_amount", e);
        }
    }


    //from parameters take double amount value
    private double getAmountThreshold() throws BillException {
        if(getParamValue(AMOUNT_SEPARATOR) == null)
            throw new BillException("err.bill.null_pointer_amount_threshold_is_absent"); 

        return Double.parseDouble(getParamValue(AMOUNT_SEPARATOR));
    }

}
