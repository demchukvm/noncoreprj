<%@ page language="java" pageEncoding="UTF-8" contentType="text/html; charset=utf-8"
         errorPage="/core/jsp/message/jsperror.jsp" isELIgnored="false" %>
<%@ taglib uri="/WEB-INF/tld/struts-nested.tld" prefix="nested" %>
<%@ taglib uri="/WEB-INF/tld/tmx-controls.tld" prefix="tmx_ctrl" %>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>


<html:form action="/core/balance/terminal_balance/infoTerminalBalance">

<bean:define id="form" name="core.balance.terminal_balance.infoTerminalBalanceForm"
                       type="com.tmx.web.forms.core.balance.terminal_balance.InfoTerminalBalanceForm"/>
    <input type="hidden" name="balanceId" value="${form.balanceId}"/>
    <input type="hidden" name="ownerId" value="${form.terminalId}"/>

<table border="0" width="100%">

    <!-- BUTTONS -->
    <tr>
        <td style="text-align: left">
            <tmx_ctrl:button property="onApplyButton"/>
            <tmx_ctrl:button property="onSaveButton"/>
            <tmx_ctrl:button property="onResetButton" onClick="javascript:document.forms[0].reset()"/>
            <tmx_ctrl:button property="onCancelButton"/>
            <tmx_ctrl:button property="onChangeBalanceButton"/>
        </td>
    </tr>
    <tr>
        <td valign="center" align="center" class="tabForm">
        <table width="100%" border="0">
            <%--<!-- Form header -->--%>
            <tr>
                <td colspan="2">
                    <h4 class="dataLabel">
                        <bean:message bundle="core.labels" key="core.balance.terminalbalance.infoterminalbalanceform.titleGeneral"/>
                    </h4>
                </td>
            </tr>

            <!--BEGIN for abstract info transaction form    -->
            <tr>
                <td class="dataLabel" width="15%">
                    <tmx_ctrl:writeEntityLabel attrName="balanceId"
                                               entityClassName="com.tmx.as.entities.bill.balance.SellerBalance"/>:
                </td>
                <td class="dataField" width="35%">
                    <tmx_ctrl:editBox property="editBoxBalanceId"/>
                </td>

                <td class="dataLabel" width="15%">
                    <tmx_ctrl:writeEntityLabel attrName="{@name}"
                                               entityClassName="com.tmx.as.entities.bill.terminal.Terminal"/>:
                </td>
                <td class="dataField" width="35%">
                     <tmx_ctrl:browseBox property="browseBoxTerminals"/>
                </td>
            </tr>

            <tr>
                <td class="dataLabel" width="15%">
                    <tmx_ctrl:writeEntityLabel attrName="name"
                                               entityClassName="com.tmx.as.entities.bill.balance.SellerBalance"/>:
                </td>
                <td class="dataField" width="35%">
                    <tmx_ctrl:editBox property="editBoxName" width="270px"/>
                </td>

                <td class="dataLabel" width="15%">
                    <tmx_ctrl:writeEntityLabel attrName="code"
                                               entityClassName="com.tmx.as.entities.bill.balance.SellerBalance"/>:
                </td>
                <td class="dataField" width="35%">
                    <tmx_ctrl:editBox property="editBoxCode" width="270px"/>
                </td>
            </tr>


            <tr>
                <td class="dataLabel">
                    <tmx_ctrl:writeEntityLabel attrName="registrationDate"
                                               entityClassName="com.tmx.as.entities.bill.balance.SellerBalance"/>:
                </td>
                <td class="dataField">
                    <tmx_ctrl:timeSelector property="timeSelectorRegDate"/>
                </td>

                <td class="dataLabel" width="15%">
                    <tmx_ctrl:writeEntityLabel attrName="amount"
                                               entityClassName="com.tmx.as.entities.bill.balance.SellerBalance"/>:
                </td>
                <td class="dataField" width="35%">
                    <tmx_ctrl:editBox property="editBoxAmount"/>
                </td>
            </tr>

            <tr>
                <td class="dataLabel">
                    <tmx_ctrl:writeEntityLabel attrName="blocked"
                                               entityClassName="com.tmx.as.entities.bill.balance.SellerBalance"/>:
                </td>
                <td class="dataField">
                    <tmx_ctrl:checkBox property="checkBoxBlocked"/>
                </td>

                <td class="dataLabel" >
                    <tmx_ctrl:writeEntityLabel attrName="description"
                                               entityClassName="com.tmx.as.entities.bill.balance.SellerBalance"/>:
                </td>
                <td class="dataField" >
                       <tmx_ctrl:textArea property="textAreaDescription" />
                </td>
            </tr>

        </table>
        </td>

    </tr>



    <!--financal activities tab-->

    <%--<tr>--%>
        <%--<td valign="top" align="center" class="tabForm">--%>
            <%--<table width="100%" border="0">--%>

<%--<!-- Form header -->--%>

                <%--<tr>--%>
                    <%--<td colspan="2">--%>
                        <%--<h4 class="dataLabel">--%>
                            <%--<bean:message bundle="core.labels"--%>
                                          <%--key="core.balance.sellerbalance.infosellerbalanceform.titleballanceinfo"/>--%>
                        <%--</h4>--%>
                    <%--</td>--%>
                <%--</tr>--%>


                <%--<td class="dataLabel" width="25%">--%>
                    <%--<tmx_ctrl:writeEntityLabel attrName="transactionCurrentAmmount"--%>
                                               <%--entityClassName="com.tmx.as.entities.bill.balance.SellerBalance"/>:--%>
                <%--</td>--%>

                <%--<td class="dataField" width="35%">--%>
                    <%--<tmx_ctrl:editBox property="editBoxTransactionCurrentAmmount"/>--%>
                <%--</td>--%>


                <%--<tr>--%>

                    <%--<td class="dataLabel" width="25%">--%>
                        <%--<tmx_ctrl:writeEntityLabel attrName="transactionIncreaceAmount"--%>
                                                   <%--entityClassName="com.tmx.as.entities.bill.balance.SellerBalance"/>:--%>
                    <%--</td>--%>

                    <%--<td class="dataField" width="35%">--%>
                        <%--<tmx_ctrl:editBox property="editBoxTransactionIncreaceAmount"/>--%>
                    <%--</td>--%>

                    <%--<td class="dataLabel">--%>
                        <%--<tmx_ctrl:writeEntityLabel--%>
                                <%--attrName="transactionFromTime"--%>
                                <%--entityClassName="com.tmx.as.entities.bill.transaction.ClientTransaction"/>:--%>
                    <%--</td>--%>
                    <%--<td class="dataField">--%>
                        <%--<tmx_ctrl:timeSelector--%>
                                <%--property="timeSelectorTransFrom"/>--%>
                    <%--</td>--%>
                <%--</tr>--%>

                <%--<tr>--%>
                    <%--<td class="dataLabel" width="25%">--%>
                        <%--<tmx_ctrl:writeEntityLabel attrName="transactionAmount"--%>
                                                   <%--entityClassName="com.tmx.as.entities.bill.balance.SellerBalance"/>:--%>
                    <%--</td>--%>

                    <%--<td class="dataField" width="35%">--%>
                        <%--<tmx_ctrl:editBox property="editBoxNominalTransactionAmount"/>--%>
                    <%--</td>--%>
                    <%--<td class="dataLabel">--%>
                        <%--<tmx_ctrl:writeEntityLabel--%>
                                <%--attrName="transactionToTime"--%>
                                <%--entityClassName="com.tmx.as.entities.bill.transaction.ClientTransaction"/>:--%>
                    <%--</td>--%>
                    <%--<td class="dataField">--%>
                        <%--<tmx_ctrl:timeSelector--%>
                                <%--property="timeSelectorTransTo"/>--%>
                    <%--</td>--%>
                <%--</tr>--%>

                <%--<tr>--%>
                    <%--<td class="dataLabel" width="15%">--%>
                        <%--<tmx_ctrl:writeEntityLabel attrName="transferAmount"--%>
                                                   <%--entityClassName="com.tmx.as.entities.bill.balance.SellerBalance"/>:--%>
                    <%--</td>--%>

                    <%--<td class="dataField" width="35%">--%>
                        <%--<tmx_ctrl:editBox property="editBoxTransferAmount"/>--%>
                    <%--</td>--%>

                    <%--<td style="text-align: center">--%>
                        <%--<tmx_ctrl:button property="onApplyTransButton"/>--%>
                    <%--</td>--%>
                <%--</tr>--%>

                <%--<tr>--%>
                    <%--<td class="dataLabel" width="15%">--%>
                        <%--<tmx_ctrl:writeEntityLabel attrName="transactionAward"--%>
                                                   <%--entityClassName="com.tmx.as.entities.bill.balance.SellerBalance"/>:--%>
                    <%--</td>--%>

                    <%--<td class="dataField" width="35%">--%>
                        <%--<tmx_ctrl:editBox property="editBoxTransactionAward"/>--%>
                    <%--</td>--%>
                <%--</tr>--%>
            <%--</table>--%>
        <%--</td>--%>
    <%--</tr>--%>


    <!--transaction form-->
    <tr>
        <td valign="center" align="center" >
            <table width="100%" border="0">
            <tr>
                <td valign="top" width="100%" style="text-align: left">
                    <tmx_ctrl:panelFilterSet tableName="transactionsTable">
                        <table>
                            <tr>
                                <td class="dataLabel">
                                    <tmx_ctrl:writeEntityLabel
                                            attrName="transactionFromTime"
                                            entityClassName="com.tmx.as.entities.bill.transaction.ClientTransaction"/>:
                                </td>
                                <td class="dataField">
                                    <tmx_ctrl:timeSelector
                                            property="transactionsTable.filterSet.filter(by_FromDate).parameter(date).control"/>
                                </td>

                                <td class="dataLabel">
                                    <tmx_ctrl:writeEntityLabel
                                            attrName="transactionToTime"
                                            entityClassName="com.tmx.as.entities.bill.transaction.ClientTransaction"/>:
                                </td>
                                <td class="dataField">
                                    <tmx_ctrl:timeSelector
                                            property="transactionsTable.filterSet.filter(by_ToDate).parameter(date).control"/>
                                </td>
                            </tr>

                            <tr>
                                <td class="dataLabel">
                                    <tmx_ctrl:writeEntityLabel
                                            attrName="amountFrom"
                                            entityClassName="com.tmx.as.entities.bill.transaction.ClientTransaction"/>:
                                </td>
                                <td class="dataField">
                                    <tmx_ctrl:editBox
                                            property="transactionsTable.filterSet.filter(by_client_amount_from).parameter(from_amount).control"/>
                                </td>

                                <td class="dataLabel">
                                    <tmx_ctrl:writeEntityLabel
                                            attrName="amountTo"
                                            entityClassName="com.tmx.as.entities.bill.transaction.ClientTransaction"/>:
                                </td>
                                <td class="dataField">
                                    <tmx_ctrl:editBox
                                            property="transactionsTable.filterSet.filter(by_client_amount_to).parameter(to_amount).control"/>
                                </td>
                            </tr>

                            <tr>
                                <td class="dataLabel">
                                    <tmx_ctrl:writeEntityLabel
                                            attrName="transactionGUID"
                                            entityClassName="com.tmx.as.entities.bill.transaction.ClientTransaction"/>:
                                </td>
                                <td class="dataField">
                                    <tmx_ctrl:editBox
                                            property="transactionsTable.filterSet.filter(by_billnum_like).parameter(billnum).control"/>
                                </td>

                                <td class="dataLabel">
                                    <tmx_ctrl:writeEntityLabel
                                            attrName="cliTransactionNum"
                                            entityClassName="com.tmx.as.entities.bill.transaction.ClientTransaction"/>:
                                </td>
                                <td class="dataField">
                                    <tmx_ctrl:editBox
                                            property="transactionsTable.filterSet.filter(by_clinum_like).parameter(clinum).control"/>
                                </td>
                            </tr>

                            <tr>
                                <td class="dataField">
                                    <tmx_ctrl:writeEntityLabel
                                            attrName="{@name}"
                                            entityClassName="com.tmx.as.entities.bill.transaction.TransactionType"/>:
                                </td>
                                <td class="dataField">
                                    <tmx_ctrl:selectBox
                                            property="transactionsTable.filterSet.filter(by_transaction_type).parameter(type).control"/>
                                </td>

                            </tr>
                        </table>
                    </tmx_ctrl:panelFilterSet>
                </td>

            <tr>
                <td width="100%">
                    <table class='one_outer_cell_table' cellspacing='0' cellpadding='0' width="100%">
                        <tr>
                            <td>
                                <table cellspacing='1' cellpadding='1' width="100%">
                                    <%--<!-- Table header -->--%>
                                    <tr>
                                        <th class="column_header_cell" width="110">
                                            <tmx_ctrl:imgTableOrdering tableName="transactionsTable"
                                                                       orderByFieldName="transactionTime"
                                                                       labelBundle="as:com.tmx.as.entities.bill.transaction.ClientTransaction"
                                                                       labelKey="transactionTime"/>
                                        </th>

                                        <th class="column_header_cell" width="110">
                                            <tmx_ctrl:imgTableOrdering tableName="transactionsTable"
                                                                       orderByFieldName="cliTransactionNum"
                                                                       labelBundle="as:com.tmx.as.entities.bill.transaction.ClientTransaction"
                                                                       labelKey="cliTransactionNum"/>
                                        </th>

                                        <th class="column_header_cell" width="110">
                                            <tmx_ctrl:imgTableOrdering tableName="transactionsTable"
                                                                       orderByFieldName="transactionGUID"
                                                                       labelBundle="as:com.tmx.as.entities.bill.transaction.ClientTransaction"
                                                                       labelKey="transactionGUID"/>
                                        </th>

                                        <th class="column_header_cell" width="110">
                                            <tmx_ctrl:imgTableOrdering tableName="transactionsTable"
                                                                       orderByFieldName="terminal.hardwareSerialNumber"
                                                                       labelBundle="as:com.tmx.as.entities.bill.terminal.Terminal"
                                                                       labelKey="{@name}"/>
                                        </th>

                                        <th class="column_header_cell" width="110">
                                            <tmx_ctrl:imgTableOrdering tableName="transactionsTable"
                                                                       orderByFieldName="amount"
                                                                       labelBundle="as:com.tmx.as.entities.bill.transaction.ClientTransaction"
                                                                       labelKey="amount"/>
                                        </th>

                                        <th class="column_header_cell" width="110">
                                            <tmx_ctrl:imgTableOrdering tableName="transactionsTable"
                                                                       orderByFieldName="clientTransaction.amount"
                                                                       labelBundle="as:com.tmx.as.entities.bill.transaction.ClientTransaction"
                                                                       labelKey="clientAmount"/>
                                        </th>

                                        <th class="column_header_cell" width="110">
                                            <tmx_ctrl:writeEntityLabel
                                                            attrName="feeAmount"
                                                            entityClassName="com.tmx.as.entities.bill.transaction.ClientTransaction"/>
                                        </th>

                                        <th class="column_header_cell" width="110">
                                            <tmx_ctrl:imgTableOrdering tableName="transactionsTable"
                                                   orderByFieldName="postTxBalance"
                                                   labelBundle="as:com.tmx.as.entities.bill.transaction.ClientTransaction"
                                                   labelKey="postTxBalance"/>
                                        </th>

                                        <th class="column_header_cell" width="110">
                                            <tmx_ctrl:imgTableOrdering tableName="transactionsTable"
                                                                       orderByFieldName="status"
                                                                       labelBundle="as:com.tmx.as.entities.bill.transaction.ClientTransaction"
                                                                       labelKey="status"/>
                                        </th>
                                    </tr>
                                    <nested:iterate id="terminalTransaction" property="transactionsTable.data" type="com.tmx.as.entities.bill.transaction.TerminalBalanceTransaction">
                                        <tr>
                                            <td class='column_data_cell'>
                                                    <fmt:formatDate value="${terminalTransaction.transactionTime}" pattern="yyyy/MM/dd HH:mm:ss"/>
                                            </td>
                                            <td class='column_data_cell'>
                                                    <c:out value="${terminalTransaction.cliTransactionNum}"/>
                                            </td>
                                            <td class='column_data_cell'>
                                                <html:link action="/core/transactions/terminal_transaction/infoTerminalTransaction"
                                                               paramId="selectedTransactionId" paramName="terminalTransaction" paramProperty="tansactionId">
                                                    <c:out value="${terminalTransaction.transactionGUID}" />
                                                </html:link>
                                            </td>
                                            <td class='column_data_cell'>
                                                <c:if test="${terminalTransaction.terminal != null}">
                                                    <c:out value="${terminalTransaction.terminal.serialNumber}" />
                                                </c:if>
                                            </td>
                                            <td class='column_data_cell'>
                                                <c:out value="${terminalTransaction.amount}" />
                                            </td>
                                            <td class='column_data_cell'>
                                                <c:if test="${terminalTransaction.clientTransaction.amount != null}">
                                                     <c:out value="${terminalTransaction.clientTransaction.amount}" />
                                                </c:if>
                                            </td>
                                            <td class='column_data_cell'>
                                                <c:out value="${terminalTransaction.amount - terminalTransaction.clientTransaction.amount}" />
                                            </td>

                                            <td class='column_data_cell'>
                                                <c:out value="${terminalTransaction.preTxBalance}" />
                                            </td>
                                            <td class='column_data_cell'>
                                                <tmx_ctrl:write
                                                        name="core.balance.terminal_balance.infoTerminalBalanceForm"
                                                        property="transactionStatusDictionary.value(status.${terminalTransaction.status})" ignore="true" length="13"/>
                                            </td>
                                        </tr>
                                    </nested:iterate>
                                    <!-- Empty row -->
                                    <nested:define id="tableSize" property="transactionsTable.resultSize" type="java.lang.Integer"/>
                                    <c:if test="${tableSize <= 0}">
                                        <tr>
                                            <td class='column_data_cell' colspan="8" align="center">
                                                <bean:message bundle="core.labels" key="core.common.th.empty_table" />
                                            </td>
                                        </tr>
                                    </c:if>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="text-align: right">
                    <tmx_ctrl:imgTablePaging maxBookmarkCount="10" tableName="transactionsTable"/>
                </td>
            </tr>
            <tr>
                <td valign="center" align="center" class="tabForm">
                    <table width="100%" border="0">
                    <%--<!-- Form header -->--%>
                        <tr>
                            <td colspan="2">
                                <h4 class="dataLabel">
                                    <bean:message bundle="core.labels" key="core.transaction.clienttransaction.infoclienttransactionform.titleAmount"/>
                                </h4>
                            </td>
                        </tr>
                        <tr>
                            <td class="dataLabel">
                                <tmx_ctrl:writeEntityLabel
                                        attrName="successTransactions"
                                        entityClassName="com.tmx.as.entities.bill.transaction.ClientTransaction"/>:
                            </td>
                            <td class="dataField">
                                <tmx_ctrl:editBox
                                        property="editBoxSuccessTransaction"/>
                            </td>


                            <td class="dataLabel">
                                <tmx_ctrl:writeEntityLabel
                                        attrName="allTransactions"
                                        entityClassName="com.tmx.as.entities.bill.transaction.ClientTransaction"/>:
                            </td>
                            <td class="dataField">
                                <tmx_ctrl:editBox
                                        property="editBoxAllTransaction"/>
                            </td>
                        </tr>

                        <tr>
                            <td class="dataLabel">
                                <tmx_ctrl:writeEntityLabel
                                        attrName="successTransactionAmount"
                                        entityClassName="com.tmx.as.entities.bill.transaction.ClientTransaction"/>:
                            </td>
                            <td class="dataField">
                                <tmx_ctrl:editBox
                                        property="editBoxSuccessTransAmount"/>
                            </td>


                            <td class="dataLabel">
                                <tmx_ctrl:writeEntityLabel
                                        attrName="allTransactionAmount"
                                        entityClassName="com.tmx.as.entities.bill.transaction.ClientTransaction"/>:
                            </td>
                            <td class="dataField">
                                <tmx_ctrl:editBox
                                        property="editBoxAllTransAmount"/>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>

        </table>
        </td>
    </tr>

    <!-- BUTTONS -->
    <tr>
        <td style="text-align: left">
            <tmx_ctrl:button property="onApplyButton"/>
            <tmx_ctrl:button property="onSaveButton"/>
            <tmx_ctrl:button property="onResetButton" onClick="javascript:document.forms[0].reset()"/>
            <tmx_ctrl:button property="onCancelButton"/>
            <tmx_ctrl:button property="onChangeBalanceButton"/>
        </td>
    </tr>

</table>
</html:form>