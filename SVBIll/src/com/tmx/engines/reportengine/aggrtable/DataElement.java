package com.tmx.engines.reportengine.aggrtable;

import com.tmx.engines.reportengine.aggrtable.validation.DataValidationError;

import java.text.DecimalFormat;
import java.text.FieldPosition;
import java.math.BigDecimal;

/**
 */
public class DataElement {
    Object value = null;
    DataValidationError validationError = null;

    /** Get string value */
    public String getValue(){
        return (value != null) ? value.toString() : null;
    }

    /** Get formatted string value */
    public String getFormattedValue(){
        double resultValue = 0;
        if(value instanceof Long){
            resultValue = ((Long)value).doubleValue();
        }
        else if(value instanceof Integer){
            resultValue = ((Integer)value).doubleValue();
        }
        else if(value instanceof Float){
            resultValue = ((Float)value).doubleValue();
        }
        else if(value instanceof Double){
            resultValue = ((Double)value).doubleValue();
        }
        else if(value instanceof Short){
            resultValue = ((Short)value).doubleValue();
        }
        else if(value instanceof BigDecimal){
            resultValue = ((BigDecimal)value).doubleValue();
        }
        else{
            return (value != null) ? value.toString() : "";//return value as is
        }
        DecimalFormat df = new DecimalFormat(",###");
        return df.format(resultValue, new StringBuffer(), new FieldPosition(0)).toString();
    }


    //--------package local methods
    DataElement(Object value){
        this.value = value;
    }

    //Used in aggregated functions evaluation and data validation
    public Object getOriginalValue(){
        return value;
    }

    public DataValidationError getValidationError() {
        return validationError;
    }
}
