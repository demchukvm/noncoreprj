package com.tmx.beng.base.dbfunctions;

import com.tmx.beng.base.Reservation;
import com.tmx.beng.base.BillingMessage;
import com.tmx.beng.base.cache.InvokeFunctionException;
import com.tmx.beng.base.cache.DBCache;
import com.tmx.as.entities.bill.transaction.TransactionType;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


abstract public class VoucherTariff extends BasicTariffFunction{

    boolean isTariffApplicable(Map actualParams) {
        return BillingMessage.O_BUY_VOUCHER.equals(task.getProcesedMessage().getOperationName());
    }

    abstract protected List<String> getServiceCodes();

    void applyTariff(Map actualParams, Reservation reservation) throws InvokeFunctionException {
        String vn = task.getProcesedMessage().getAttributeString(BillingMessage.VOUCHER_NOMINAL);
        double voucherCost = Double.valueOf((String) actualParams.get(vn));
        reservation.reserve(null, voucherCost, null, TransactionType.CODE_CREDIT);
    }

    private List<String> getVoucherNominals(DBCache cache, List<String> serviceCodes) {
        List<String> result = new ArrayList<String>(30);
        for (String s : serviceCodes) {
            result.addAll(cache.getVoucherAllNominals(s));
        }
        return result;
    }

    private List<String> getVoucherNominalsAndPrices(DBCache cache, List<String> serviceCodes) {
        List<String> result = new ArrayList<String>(30);
        for (String s : serviceCodes) {
            result.addAll(cache.getVoucherAllNominalsAndPrices(s));
        }
        return result;                                                                                                                                          
    }


    public ValidationError[] validate(Map actualParams) {
        List<String> nominals = getVoucherNominals(task.getSessionDBCache(), getServiceCodes());
        List<ValidationError> errs = ParameterValidator.validateParamNames(actualParams, nominals);
        return errs.toArray(new ValidationError[errs.size()]);
    }

    public TariffParameter[] getDefaultParams() {
//        List nominals = getVoucherNominalsAndPrices(task.getSessionDBCache(), getServiceCodes());
        List<String> nominals = getServiceCodes();
        TariffParameter[] result = new TariffParameter[nominals.size()];

        for (int i = 0; i < result.length; i++) {
            String nominal = nominals.get(i);
            String digits = getDigits(nominal);
            result[i] = new TariffParameter(nominal, digits, false);
        }
        return result;
    }

    private String getDigits(String nominal) {
        String num_str = "";

        try {
            String regex = "\\d+"; //matches one or more digits, might not work with (rank 1)
            Pattern myPattern = Pattern.compile(regex);
            Matcher myMatcher = myPattern.matcher(nominal);

            while(myMatcher.find()) {
                num_str = myMatcher.group(); //returns whole match
            }
            return num_str;
        } catch(Exception e) {
            e.printStackTrace();
            return "0";
        }
    }
}
