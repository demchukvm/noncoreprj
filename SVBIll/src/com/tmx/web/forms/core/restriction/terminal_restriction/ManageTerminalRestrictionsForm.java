package com.tmx.web.forms.core.restriction.terminal_restriction;

import com.tmx.web.forms.core.restriction.AbstractManageRestrictionsForm;
import com.tmx.web.controls.table.Parameter;
import com.tmx.web.controls.BrowseBox;

public class ManageTerminalRestrictionsForm extends AbstractManageRestrictionsForm {

    protected void init(){
        super.init();
        initRestrictionsTable();
        initButtons();
        initFilters();
    }

    private void initRestrictionsTable() {
         getRestrictionsTable().loadQuery("query(query_type=retrieve_all," +
                "entity_class=com.tmx.as.entities.bill.restriction.TerminalRestriction," +
                "table_attr=functionInstance.functionType," +
                "table_attr=terminal," +
                "order(name=restrictionId,type=asc)," +
                "page_size=15" +
                ")");
        getRestrictionsTable().setRefreshActionName("/core/restriction/terminal_restriction/manageTerminalRestrictions");
    }

    private void initFilters(){
        BrowseBox browseBox = new BrowseBox("/core/balance/terminal_balance/browseTerminalsAction");
        getRestrictionsTable().addParameterToFilter("by_terminal_id", new Parameter("terminal_id", browseBox));
    }

    public BrowseBox getTerminalFilter(){
        return (BrowseBox) getRestrictionsTable().getParameterControl("by_terminal_id", "terminal_id");
    }

    private void initButtons(){
        getOnCreateButton().setAction("/core/restriction/terminal_restriction/manageTerminalRestrictions");
        getOnEditButton().setAction("/core/restriction/terminal_restriction/infoTerminalRestriction");
        getOnDeleteButton().setAction("/core/restriction/terminal_restriction/infoTerminalRestriction");
    }

}
