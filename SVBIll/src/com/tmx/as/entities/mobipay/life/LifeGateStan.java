package com.tmx.as.entities.mobipay.life;

import com.tmx.as.base.BasicEntity;

import java.util.Date;
import java.io.Serializable;

/**
 * Handle the life stan
 */
public class LifeGateStan extends BasicEntity {
    private Long lifeGateStanId;
    private Date day;
    private Integer stan;
    private Date allocationTime;

    public Long getLifeGateStanId() {
        return lifeGateStanId;
    }

    public void setLifeGateStanId(Long lifeGateStanId) {
        this.lifeGateStanId = lifeGateStanId;
    }

    public Date getDay() {
        return day;
    }

    public void setDay(Date day) {
        this.day = day;
    }

    public Integer getStan() {
        return stan;
    }

    public void setStan(Integer stan) {
        this.stan = stan;
    }

    public Date getAllocationTime() {
        return allocationTime;
    }

    public void setAllocationTime(Date allocationTime) {
        this.allocationTime = allocationTime;
    }

    public Serializable getId() {
        return getLifeGateStanId();
    }

    public int getValid() {
        return valid;
    }

    public void setValid(int valid) {
        this.valid = valid;
    }
}
