package com.tmx.util.cache;

/**
to identified is cached object uptodate
 */
public interface CachedObjectSignature {
    public boolean isUpToDate() throws CacheException;
}
