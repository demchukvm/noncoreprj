package com.tmx.web.forms.trans_mgmt;

import com.tmx.web.base.BasicActionForm;
import com.tmx.web.controls.Button;
import com.tmx.web.controls.EditBox;
import com.tmx.web.controls.ScrollBox;
import com.tmx.beng.access.Access;

/**
 * This is the example Form that shows how to use nested form's controls
 */
public class TransactionInfoForm extends BasicActionForm {
    private Access billingEngineAccess = null;
    private Button onCancelButton = null;
    private Button onInfoButton = null;
    private Button onInfoMobipayButton = null;
    private Button onRefreshBalance = null;
    private EditBox billTransaction = null;
    private EditBox payId = null;
    private EditBox cliTransaction = null;
    private EditBox receiptNumMobipay = null;
    private EditBox statusCode = null;
    private EditBox payStatusMobipay = null;
    private EditBox statusCodeMobipay = null;
    private EditBox statusMessage = null;
    private EditBox transactionTime = null;
    private EditBox ctsMobipay = null;
    private EditBox amount = null;
    private EditBox clientTime = null;
    private EditBox accountNumber = null;
    private EditBox sellerName = null;
    private EditBox terminalNumber = null;
    private EditBox operatorName = null;
    private EditBox kyivstarBalance = null;
    private EditBox kyivstarWCBalance = null;
    private EditBox kyivstarFixConnectBalance = null;

    private ScrollBox scrollBoxKyivstarGates;

    public void init() {
        //set up buttons
        onCancelButton = new Button();
        onCancelButton.setLabelKey("ctrl.button.label.cancel");
        onCancelButton.setAction("/transactionInfo");
        onCancelButton.setCommand("cancelTransaction");

        onInfoButton = new Button();
        onInfoButton.setLabelKey("ctrl.button.label.infoSvBill");
        onInfoButton.setAction("/transactionInfo");
        onInfoButton.setCommand("info");

        onInfoMobipayButton = new Button();
        onInfoMobipayButton.setLabelKey("ctrl.button.label.infoMobipay");
        onInfoMobipayButton.setAction("/transactionInfo");
        onInfoMobipayButton.setCommand("infoMobipay");

        onRefreshBalance = new Button();
        onRefreshBalance.setLabelKey("ctrl.button.label.refresh_balance");
        onRefreshBalance.setAction("/transactionInfo");
        onRefreshBalance.setCommand("refreshBalance");

        // set up edit
        billTransaction = new EditBox();
        cliTransaction = new EditBox();

        payId = new EditBox();
        payId.setReadonly(false);

        statusCode = new EditBox();
        statusCode.setReadonly(true);

        statusMessage = new EditBox();
        statusMessage.setReadonly(true);

        statusCodeMobipay = new EditBox();
        statusCodeMobipay.setReadonly(true);

        payStatusMobipay = new EditBox();
        payStatusMobipay.setReadonly(true);

        transactionTime = new EditBox();
        transactionTime.setReadonly(true);

        ctsMobipay = new EditBox();
        ctsMobipay.setReadonly(true);

        amount = new EditBox();
        amount.setReadonly(true);

        clientTime = new EditBox();
        clientTime.setReadonly(true);

        accountNumber = new EditBox();
        accountNumber.setReadonly(true);

        sellerName = new EditBox();
        sellerName.setReadonly(true);

        terminalNumber = new EditBox();
        terminalNumber.setReadonly(true);

        receiptNumMobipay = new EditBox();
        receiptNumMobipay.setReadonly(true);

        operatorName = new EditBox();
        operatorName.setReadonly(true);

        kyivstarBalance = new EditBox();
        kyivstarBalance.setReadonly(true);

        kyivstarWCBalance = new EditBox();
        kyivstarWCBalance.setReadonly(true);

        // NEW
        kyivstarFixConnectBalance = new EditBox();
        kyivstarFixConnectBalance.setReadonly(true);
        //


        scrollBoxKyivstarGates = new ScrollBox<String>() {

            protected String getKey(String dataListElement) {
                return dataListElement;
            }

            protected String getValue(String dataListElement) {
                return dataListElement;
            }
        };
    }

    public void clear(){
        statusCode.setValue("");
        statusMessage.setValue("");
        transactionTime.setValue("");
        amount.setValue("");
        clientTime.setValue("");
        accountNumber.setValue("");
        sellerName.setValue("");
        terminalNumber.setValue("");
        operatorName.setValue("");
        receiptNumMobipay.setValue("");
        statusCodeMobipay.setValue("");
        payId.setValue("");
        payStatusMobipay.setValue("");
        ctsMobipay.setValue("");
    }

    public Button getOnRefreshBalance() {
        return onRefreshBalance;
    }

    public void setOnRefreshBalance(Button onRefreshBalance) {
        this.onRefreshBalance = onRefreshBalance;
    }

    public EditBox getKyivstarBalance() {
        return kyivstarBalance;
    }

    public void setKyivstarBalance(EditBox kyivstarBalance) {
        this.kyivstarBalance = kyivstarBalance;
    }

    public EditBox getOperatorCode() {
        return operatorName;
    }

    public void setOperatorName(EditBox operatorName) {
        this.operatorName = operatorName;
    }

    public EditBox getTransactionTime() {
        return transactionTime;
    }

    public void setTransactionTime(EditBox transactionTime) {
        this.transactionTime = transactionTime;
    }

    public EditBox getAmount() {
        return amount;
    }

    public void setAmount(EditBox amount) {
        this.amount = amount;
    }

    public EditBox getClientTime() {
        return clientTime;
    }

    public void setClientTime(EditBox clientTime) {
        this.clientTime = clientTime;
    }

    public EditBox getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(EditBox accountNumber) {
        this.accountNumber = accountNumber;
    }

    public EditBox getSellerName() {
        return sellerName;
    }

    public void setSellerName(EditBox sellerName) {
        this.sellerName = sellerName;
    }

    public EditBox getTerminalNumber() {
        return terminalNumber;
    }

    public void setTerminalNumber(EditBox terminalNumber) {
        this.terminalNumber = terminalNumber;
    }

    public EditBox getCliTransaction() {
        return cliTransaction;
    }

    public void setCliTransaction(EditBox cliTransaction) {
        this.cliTransaction = cliTransaction;
    }

    public Button getOnInfoButton() {
        return onInfoButton;
    }

    public void setOnInfoButton(Button onInfoButton) {
        this.onInfoButton = onInfoButton;
    }

    public Button getOnCancelButton() {
        return onCancelButton;
    }

    public void setOnCancelButton(Button onCancelButton) {
        this.onCancelButton = onCancelButton;
    }

    public EditBox getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(EditBox statusCode) {
        this.statusCode = statusCode;
    }

    public EditBox getStatusMessage() {
        return statusMessage;
    }

    public void setStatusMessage(EditBox statusMessage) {
        this.statusMessage = statusMessage;
    }

    public Access getBillingEngineAccess() {
        return billingEngineAccess;
    }

    public void setBillingEngineAccess(Access billingEngineAccess) {
        this.billingEngineAccess = billingEngineAccess;
    }

    public EditBox getBillTransaction() {
        return billTransaction;
    }

    public void setBillTransaction(EditBox billTransaction) {
        this.billTransaction = billTransaction;
    }

    public EditBox getKyivstarWCBalance() {
        return kyivstarWCBalance;
    }

    public void setKyivstarWCBalance(EditBox kyivstarWCBalance) {
        this.kyivstarWCBalance = kyivstarWCBalance;
    }

    public EditBox getReceiptNumMobipay() {
        return receiptNumMobipay;
    }

    public void setReceiptNumMobipay(EditBox receiptNumMobipay) {
        this.receiptNumMobipay = receiptNumMobipay;
    }

    public EditBox getStatusCodeMobipay() {
        return statusCodeMobipay;
    }

    public void setStatusCodeMobipay(EditBox statusCodeMobipay) {
        this.statusCodeMobipay = statusCodeMobipay;
    }

    public EditBox getCtsMobipay() {
        return ctsMobipay;
    }

    public void setCtsMobipay(EditBox ctsMobipay) {
        this.ctsMobipay = ctsMobipay;
    }

    public EditBox getPayStatusMobipay() {
        return payStatusMobipay;
    }

    public void setPayStatusMobipay(EditBox payStatusMobipay) {
        this.payStatusMobipay = payStatusMobipay;
    }

    public Button getOnInfoMobipayButton() {
        return onInfoMobipayButton;
    }

    public void setOnInfoMobipayButton(Button onInfoMobipayButton) {
        this.onInfoMobipayButton = onInfoMobipayButton;
    }

    public EditBox getPayId() {
        return payId;
    }

    public void setPayId(EditBox payId) {
        this.payId = payId;
    }

    public ScrollBox getScrollBoxKyivstarGates() {
        return scrollBoxKyivstarGates;
    }

    public void setScrollBoxKyivstarGates(ScrollBox scrollBoxKyivstarGates) {
        this.scrollBoxKyivstarGates = scrollBoxKyivstarGates;
    }

    public EditBox getKyivstarFixConnectBalance() {
        return kyivstarFixConnectBalance;
    }

    public void setKyivstarFixConnectBalance(EditBox kyivstarFixConnectBalance) {
        this.kyivstarFixConnectBalance = kyivstarFixConnectBalance;
    }
}
