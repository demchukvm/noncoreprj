package com.tmx.engines.reportengine.util;

import java.util.Date;
import java.text.SimpleDateFormat;
import java.text.ParseException;

/**
Time period handler
 */
public class TimePeriod {
    private String format = "yyyy-MM-dd HH:mm";
    private Date timeFrom = null;
    private Date timeTo = null;
    private String label = null;
    private Type type = null;


    public TimePeriod(Type type, Date timeFrom, Date timeTo, String label){
        this.type = (type == null) ? Type.getDefault() : type;
        this.timeFrom = (timeFrom == null) ? new Date() : timeFrom;
        this.timeTo = (timeTo == null) ? new Date() : timeTo;
        this.label = label;
    }

    public TimePeriod(Type type, Date timeFrom, Date timeTo, String label, String format){
        this(type, timeFrom, timeTo, format);
        this.format = format;
    }

    public Date getTimeFrom() {
        return timeFrom;
    }

    public void setTimeFromString(String timeFrom){
        if(timeFrom == null){
            this.timeFrom = new Date();
        }
        else{
            try{
                this.timeFrom = new SimpleDateFormat(format).parse(timeFrom);
            }
            catch(ParseException e){
                this.timeFrom = new Date();
            }
        }
    }

    public String getTimeFromString() {
        return new SimpleDateFormat(format).format(this.timeFrom);
    }

    public void setTimeFrom(Date timeFrom) {
        this.timeFrom = timeFrom;
    }

    public Date getTimeTo() {
        return timeTo;
    }

    public void setTimeToString(String timeTo){
        if(timeTo == null){
            this.timeTo = new Date();
        }
        else{
            try{
                this.timeTo = new SimpleDateFormat(format).parse(timeTo);
            }
            catch(ParseException e){
                this.timeTo = new Date();
            }
        }
    }

    public String getTimeToString() {
        return new SimpleDateFormat(format).format(this.timeTo);
    }

    public void setTimeTo(Date timeTo) {
        this.timeTo = timeTo;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public static class Type{
        private String typeName = null;
        public static final Type PREV_YEAR = new Type("PREV_YEAR");
        public static final Type THIS_YEAR = new Type("THIS_YEAR");
        public static final Type PREV_QUARTAL = new Type("PREV_QUARTAL");
        public static final Type THIS_QUARTAL = new Type("THIS_QUARTAL");
        public static final Type PREV_MONTH = new Type("PREV_MONTH");
        public static final Type THIS_MONTH = new Type("THIS_MONTH");
        public static final Type PREV_WEEK = new Type("PREV_WEEK");
        public static final Type THIS_WEEK = new Type("THIS_WEEK");
        public static final Type YESTERDAY = new Type("YESTERDAY");
        public static final Type TODAY = new Type("TODAY");
        public static final Type CUSTOM = new Type("CUSTOM");

        private Type(String typeName){
            this.typeName = (typeName == null) ? getDefault().typeName : typeName;
        }

        public static Type getDefault(){
            return TODAY; 
        }

        public String getName(){
            return typeName;
        }

        public boolean equals(Object object){
            return object != null && object instanceof Type && this.typeName.equals(((Type)object).typeName);
        }

        public String toString(){
            return typeName;
        }
    }
}
