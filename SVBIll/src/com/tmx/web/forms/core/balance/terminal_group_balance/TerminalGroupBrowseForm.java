package com.tmx.web.forms.core.balance.terminal_group_balance;

import com.tmx.web.forms.core.base.AbstractBrowseForm;
import com.tmx.web.controls.table.Table;
import com.tmx.web.controls.table.ControlManagerTable;
import com.tmx.as.entities.general.user.User;


public class TerminalGroupBrowseForm extends AbstractBrowseForm {


    protected void init(){

        setBrowseTable(new ControlManagerTable("query(query_type=retrieve_all," +
                "entity_class=com.tmx.as.entities.bill.terminal_group.TerminalGroup," +
                "order(name=terminalGroupId,type=desc)," +
                "table_attr=sellerOwner," +
                "page_size=15" +
                ")"));
        
    }


    public void bindData(User loggedInUser) throws Throwable {
        
    }
}
