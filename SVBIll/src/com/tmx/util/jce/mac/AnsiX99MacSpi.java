package com.tmx.util.jce.mac;

import javax.crypto.Cipher;
import java.security.Key;
import java.security.InvalidKeyException;
import java.security.InvalidAlgorithmParameterException;
import java.security.spec.AlgorithmParameterSpec;

/**
    ANSI X9.9 MAC SPI
    Not a thread safe implementation.
 */
public final class AnsiX99MacSpi extends AnsiXMacSpi {

    //-----------Engine methods
    //todo replace with protected
    public void engineInit(Key key, AlgorithmParameterSpec algorithmParameterSpec) throws InvalidKeyException, InvalidAlgorithmParameterException {
        if(algorithmParameterSpec == null)
            throw new InvalidAlgorithmParameterException("Mandatory algoritm parameter is absent");

        if(algorithmParameterSpec instanceof X99AlgParameterSpec){
            desCipher = ((X99AlgParameterSpec)algorithmParameterSpec).getDesCipher();
            if(desCipher == null)
                throw new InvalidAlgorithmParameterException("Algoritm parameter has null DES cipher");
            if(!DES_ALGORITHM.equals(desCipher.getAlgorithm()))
                throw new InvalidAlgorithmParameterException("Algoritm parameter has not a DES cipher");    
        }
        else
            throw new InvalidAlgorithmParameterException("Algoritm parameter should be instatnce of "+X99AlgParameterSpec.class);    

        desCipher.init(Cipher.ENCRYPT_MODE, key);
    }
    //todo replace with protected
    public byte[] engineDoFinal() {
        byte[] lastChunk = propagateZeroUpToChunk(chunkExcess);
        chunkExcess = null;
        if(lastChunk != null)
            calculateMac(lastChunk, 0, CHUNK_LENGTH);
        byte[] tmpCurrentMac = new byte[CHUNK_LENGTH];
        System.arraycopy(currentMac, 0, tmpCurrentMac, 0, CHUNK_LENGTH); //save MAC to chunk size to save it during reset. Fit it to chunk size

        engineReset();
        return tmpCurrentMac;
    }

    //-----------Auxilary methods
    /** Unite excess data from previos step and authentificated data from this step to
     * prodice divisible to chunk size data block. New excess data is stored.
     * Returns null if nothing to process on this step */
    protected byte[] collectToFitChunkSize(byte[] authentificatedData, int offset, int authentificatedDataLength){
        //1. Prepare shunk length divisible data block
        if(chunkExcess == null)
            chunkExcess = new byte[0];

        int unionLength = chunkExcess.length + authentificatedDataLength;
        if(unionLength < CHUNK_LENGTH){
            // Nothing to process with MAC. Authentificated data with previos chunk excess less then 1 chunk.
            // Collect authentificated data in the chunkExcess.
            byte[] buffer = new byte[unionLength];
            System.arraycopy(chunkExcess, 0, buffer, 0, chunkExcess.length);
            System.arraycopy(authentificatedData, offset, buffer, chunkExcess.length, authentificatedDataLength);
            chunkExcess = buffer;
            return null;
        }
        else if(unionLength % CHUNK_LENGTH > 0){
            // Prepare chunks for process with MAC.
            int newAuthDataLength = (unionLength / CHUNK_LENGTH) * CHUNK_LENGTH;
            byte[] authDataBuffer = new byte[newAuthDataLength];
            System.arraycopy(chunkExcess, 0, authDataBuffer, 0, chunkExcess.length);
            System.arraycopy(authentificatedData, offset, authDataBuffer, chunkExcess.length, newAuthDataLength-chunkExcess.length);

            byte[] excessBuffer = new byte[authentificatedDataLength + chunkExcess.length - newAuthDataLength];
            System.arraycopy(authentificatedData, newAuthDataLength-chunkExcess.length+offset, excessBuffer, 0, excessBuffer.length);
            authentificatedData = authDataBuffer;
            chunkExcess = excessBuffer;
        }
        else {
            // Union of previos excess data and authentificatedData divisible to chunk size.
            // Prepare chunks for process with MAC.
            byte[] buffer = new byte[unionLength];
            System.arraycopy(chunkExcess, 0, buffer, 0, chunkExcess.length);
            System.arraycopy(authentificatedData, offset, buffer, chunkExcess.length, authentificatedDataLength);
            authentificatedData = buffer;
            chunkExcess = null;
        }
        return authentificatedData;
    }
}
