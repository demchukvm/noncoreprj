package com.tmx.beng.base;

import com.tmx.beng.base.beans.PipeType;
import com.tmx.beng.base.beans.ProcessorType;
import com.tmx.beng.base.beans.PipelineDocument;
import com.tmx.beng.base.processors.BasicProcessor;
import com.tmx.util.Configuration;
import org.apache.log4j.Logger;

import java.util.List;
import java.util.Vector;
import java.util.HashMap;
import java.io.File;

/**

 */
public class Pipeline {
    protected final Logger logger = Logger.getLogger("bill.Pipeline");
    private final String CONFIG_FILE = "pipeline.config_file";

    private Pipe pipe = null;
    private GlobalContext globalContext = null;

    private static Pipeline instance = null;
    private HashMap pipeMap = null;

    public static Pipeline getInstance(){
        if(instance == null){
            instance = new Pipeline();
            instance.reload();
        }
        return instance;
    }

    private Pipeline(){
    }

    private void reload(){
        try{
            HashMap localPipeMap = new HashMap();
            PipelineDocument root = PipelineDocument.Factory.parse(
                    new File(Configuration.getInstance().getProperty(CONFIG_FILE)));
            PipeType[] pipes = root.getPipeline().getPipeArray();
            if(pipes != null){
                for(int i = 0; i < pipes.length; i++){
                    ProcessorType[] processors = pipes[i].getProcessorArray();
                    Pipe pipe = new Pipe();
                    pipe.processors = new Vector();
                    if(processors != null){
                        for(int j = 0; j < processors.length; j++){
                            BasicProcessor processor = (BasicProcessor)Class.forName(processors[j].getClass1()).newInstance();
                            if(processors[j].getConfig() != null){
                                processor.setConfigFile(
                                        Configuration.getInstance().substituteVariablesInString(processors[j].getConfig())
                                );
                            }
                            pipe.processors.add(processor);
                        }
                    }
                    pipe.timeToLive = pipes[i].getTimeToLive();
                    localPipeMap.put(pipes[i].getName(), pipe);
                }
            }
            this.pipeMap = localPipeMap;
        }catch(Throwable e){
            logger.error(e.getLocalizedMessage(), e);
        }
    }

    public void runTask(Task task) {
        int i = -1;
        try{
            if(pipe != null){
                for(i = 0; i < pipe.processors.size(); i++)
                task = ((Processor)pipe.processors.get(i)).process(task);
           }else{
                task.getProcesedMessage().setStatusCode(StatusDictionary.UNCKNOWN_COMMAND);
                throw new BillException("err.bill.reg_processor.uncknown_command",
                        new String[]{task.getProcesedMessage().getOperationName()});
            }
        }catch(Throwable e){
            if(pipe != null){
                for(; i >= 0; i--)
                    ((Processor)pipe.processors.get(i)).rollback(task, e);
            }
        }
        finally{
            //remove task from task pool
            globalContext.getTaskPool().remove(task);
            task = null;
        }

    }

    public Pipeline getPipeline(String operationName, GlobalContext globalContext){
        Pipeline pipeline = new Pipeline();
        pipeline.globalContext = globalContext;

        if(pipeMap != null){
            pipeline.pipe = (Pipe)pipeMap.get(operationName);
        }

        return pipeline;
    }

    private class Pipe{
        private List processors = null;
        private long timeToLive = 0;
    }

}
