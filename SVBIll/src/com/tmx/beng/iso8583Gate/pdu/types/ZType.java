package com.tmx.beng.iso8583Gate.pdu.types;

import com.tmx.beng.iso8583Gate.pdu.util.HexConverter;
import com.tmx.beng.iso8583Gate.pdu.RuntimeDataElement;
import com.tmx.beng.iso8583Gate.pdu.IsoParseException;
import com.tmx.beng.iso8583Gate.pdu.IsoSerializeException;
import com.tmx.beng.iso8583Gate.pdu.IncorrectFormatException;

import java.io.InputStream;
import java.io.IOException;
import java.io.OutputStream;

/**
 */
public class ZType extends Type{
    {
        name = "z";
    }
    private HexConverter hexConverter = new HexConverter();
    
    public void parse(InputStream is, int length, RuntimeDataElement rde) throws IsoParseException {
        try{
            rde.setValue(hexConverter.getStringFromHex(is, length));
        }
        catch(IOException e){
            throw new IsoParseException("err.z_type.parsing_failed", e);
        }
    }

    public SerializedValue serialize(boolean fixedLength, int lengthLimit, RuntimeDataElement rde) throws IsoSerializeException{
        try{
            String hexString = rde.getString();
            SerializedValue sv = new SerializedValue();
            sv.data = hexConverter.getHexFromString(hexString);
            sv.length = hexConverter.getHexSize(sv.data.length);
            if(sv.length > lengthLimit)
                throw new IncorrectFormatException("err.z_type.hex_array_size_more_then_maxlength", new String[]{String.valueOf(sv.length), String.valueOf(lengthLimit)});

            if(fixedLength && (sv.data.length < hexConverter.getByteSize(lengthLimit)))
                sv.data = expandFixedLengthValue(sv.data, lengthLimit);
            return sv;
        }
        catch (IncorrectFormatException e) {
            throw new IsoSerializeException("err.z_type.serialization_failed", e);
        }
    }

    protected byte[] expandFixedLengthValue0(byte[] value, int fixedByteLength) throws IncorrectFormatException{
        //fill head of byte array by 0 to match fixed length
        byte[] expandedData = new byte[fixedByteLength];
        System.arraycopy(value, 0, expandedData, expandedData.length-value.length, value.length);
        return expandedData;
    }

}
