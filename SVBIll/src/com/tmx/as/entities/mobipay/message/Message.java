package com.tmx.as.entities.mobipay.message;

import com.tmx.as.base.BasicEntity;
import com.tmx.as.entities.bill.operator.Operator;

import java.io.Serializable;

/**
 */
public class Message extends BasicEntity {
    private Long messageId;
    private String message;
    private Integer messageType;
    private Long code;
    private Operator operator;


    public Long getCode() {
        return code;
    }

    public void setCode(Long code) {
        this.code = code;
    }

    public Integer getMessageType() {
        return messageType;
    }

    public void setMessageType(Integer messageType) {
        this.messageType = messageType;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Operator getOperator() {
        return operator;
    }

    public void setOperator(Operator operator) {
        this.operator = operator;
    }


    public Long getMessageId() {
        return messageId;
    }

    public void setMessageId(Long messageId) {
        this.messageId = messageId;
    }


    public Serializable getId() {
        return getMessageId();
    }

    public int getValid() {
        return valid;
    }

    public void setValid(int valid) {
        this.valid = valid;
    }
}
