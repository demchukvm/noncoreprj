package com.tmx.as.entities.bill.restriction;

import com.tmx.as.entities.bill.terminal_group.TerminalGroup;

/**

 */
public class TerminalGroupRestriction extends AbstractRestriction{
    private TerminalGroup terminalGroup;
    private Long terminalGroupId;

    //--------------Properties

    public TerminalGroup getTerminalGroup() {
        return terminalGroup;
    }

    public void setTerminalGroup(TerminalGroup terminalGroup) {
        this.terminalGroup = terminalGroup;
    }

    public Long getTerminalGroupId() {
        return terminalGroupId;
    }

    public void setTerminalGroupId(Long terminalGroupId) {
        this.terminalGroupId = terminalGroupId;
    }
}
