package com.tmx.web.forms.core.reports;

import com.tmx.web.base.BasicActionForm;
import com.tmx.as.entities.bill.seller.Seller;
import com.tmx.as.entities.bill.transaction.TransactionType;
import com.tmx.as.base.criterion.CriterionWrapper;
import com.tmx.as.base.criterion.Restrictions;
import com.tmx.web.controls.table.Filter;
import com.tmx.web.controls.table.Parameter;
import com.tmx.web.controls.table.ControlManagerTable;
import com.tmx.web.controls.ScrollBox;
import com.tmx.web.controls.TimeSelector;
import com.tmx.web.controls.EditBox;
import com.tmx.web.controls.CriterionFactory;

import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class ReportPaymentForm extends BasicActionForm {
    private ControlManagerTable table;
    private ScrollBox sellersScrollBox;
    private ScrollBox subSellersScrollBox;
    private EditBox editBoxPaymentReason;
    private EditBox editBoxSummaryAmount;

    protected void init() {
        initTable();
        initFilters();
//        setFilters();

        sellersScrollBox =new ScrollBox(){
            protected String getKey(Object dataListElement) {
                return ((Seller)dataListElement).getSellerId().toString();
            }

            protected String getValue(Object dataListElement) {
                return ((Seller)dataListElement).getName();
            }
        };
        sellersScrollBox.setOnChange("document.forms[0].action=\"/svbill/core/report/reportPayment.do?command=changeSellersScrollBox\";document.forms[0].submit()");
        sellersScrollBox.setNullable(true);
        subSellersScrollBox =new ScrollBox() {
            protected String getKey(Object dataListElement) {
                return ((Seller)dataListElement).getSellerId().toString();
            }

            protected String getValue(Object dataListElement) {
                return ((Seller)dataListElement).getName();
            }
        };
        subSellersScrollBox.setNullable(true);

        editBoxSummaryAmount = new EditBox();
    }

    private void initFilters() {
        Filter filter = new Filter();
        filter.addParameter("date", new Parameter(new TimeSelector()));
        table.getFilterSet().addFilter("by_FromDate", filter);

        filter = new Filter();
        filter.addParameter("date", new Parameter(new TimeSelector()));
        table.getFilterSet().addFilter("by_ToDate", filter);

        filter = new Filter();
        EditBox editBox=new EditBox();
        editBox.addValidationRule(EditBox.createValidationRule_INTEGER_FORMAT(getLocale()));
        filter.addParameter("from_amount", new Parameter(editBox));
        table.getFilterSet().addFilter("by_client_amount_from", filter);

        filter = new Filter();
        editBox=new EditBox();
        editBox.addValidationRule(EditBox.createValidationRule_INTEGER_FORMAT(getLocale()));
        filter.addParameter("to_amount", new Parameter(editBox));
        table.getFilterSet().addFilter("by_client_amount_to", filter);

        ScrollBox scrollBoxOperTypes = new ScrollBox(){
            protected String getKey(Object dataListElement) {
                return ((TransactionType)dataListElement).getId().toString();
            }

            protected String getValue(Object dataListElement) {
                return ((TransactionType)dataListElement).getName();
            }
        };
        scrollBoxOperTypes.setNullable(true);
        filter = new Filter();
        filter.addParameter("type", new Parameter(scrollBoxOperTypes));
        table.getFilterSet().addFilter("by_transaction_type",filter);

        editBoxPaymentReason = new EditBox();

        CriterionFactory criterion = new CriterionFactory() {

             public CriterionWrapper createCriterion() {
                 if (editBoxPaymentReason.getValue() != null) {
                     if (!editBoxPaymentReason.getValue().equals("")) {
                         return Restrictions.ilike("transactionSupport.paymentReason", "%" + editBoxPaymentReason.getValue() + "%");
                     }
                 }
                 return null;//To change body of implemented methods use File | Settings | File Templates.
             }
         };
         table.setCriterionFactory(criterion);

    }

    private void initTable() {
        table = new ControlManagerTable("query(query_type = retrieve_all," +
                "entity_class=com.tmx.as.entities.bill.transaction.SellerBalanceTransaction," +
                "filter(name=by_operation_type, parameter(name=operationType, value=4, type=long)),"+
                "table_attr=seller.parentSeller," +
                "table_attr=transactionSupport.userActor," +
                "table_attr=transactionSupport," +
                "table_attr=type," +
                "order(name=id, type=desc)," +
                "page_size=15" +
                ")");
    }

    public void bindSellersScrollBox(List sellers) {
        Collections.sort(sellers);
        sellersScrollBox.bind(sellers);
    }

    public void bindSubSellersScrollBox(List subSellers) {
        //Collections.sort(subSellers);
        //subSellersScrollBox.bind(subSellers);

        Collections.sort(subSellers, new Comparator<Seller>()
        {
          public int compare(Seller s1, Seller s2) {
            return s1.getCode().compareTo(s2.getCode());
          }
        });
        subSellersScrollBox.bind(subSellers);
    }

    public void bindFilters(final List sellerList, final List subSellerList) {

        CriterionFactory subSellersFactory = new CriterionFactory() {
            public CriterionWrapper createCriterion() {
                return Restrictions.in("seller", subSellerList);
        }};
        CriterionFactory sellersFactory = new CriterionFactory() {
            public CriterionWrapper createCriterion() {
                return Restrictions.in("seller", sellerList);
        }};

        if (!sellersScrollBox.isNullKeySelected()) {
//            table.addParameterToFilter("by_seller", "seller", sellersScrollBox);
            if (!subSellersScrollBox.isNullKeySelected()) {
                       sellersFactory = new CriterionFactory() {
                            public CriterionWrapper createCriterion() {
                                return Restrictions.eq("seller.sellerId", new Long(subSellersScrollBox.getSelectedKey()));
                        }};
                table.setCriterionFactory(sellersFactory);
//                table.addParameterToFilter("by_seller", "seller", subSellersScrollBox);
            } else {
                table.setCriterionFactory(subSellersFactory);
            }
        } else {
            table.setCriterionFactory(sellersFactory);            
        }
    }       

    public ControlManagerTable getTable() {
        return table;
    }

    public void setTable(ControlManagerTable table) {
        this.table = table;
    }

    public ScrollBox getSellersScrollBox() {
        return sellersScrollBox;
    }

    public void setSellersScrollBox(ScrollBox sellersScrollBox) {
        this.sellersScrollBox = sellersScrollBox;
    }

    public ScrollBox getSubSellersScrollBox() {
        return subSellersScrollBox;
    }

    public void setSubSellersScrollBox(ScrollBox subSellersScrollBox) {
        this.subSellersScrollBox = subSellersScrollBox;
    }

    public EditBox getEditBoxPaymentReason() {
        return editBoxPaymentReason;
    }

    public void setEditBoxPaymentReason(EditBox editBoxPaymentReason) {
        this.editBoxPaymentReason = editBoxPaymentReason;
    }

    public EditBox getEditBoxSummaryAmount() {
        return editBoxSummaryAmount;
    }

    public void setEditBoxSummaryAmount(EditBox editBoxSummaryAmount) {
        this.editBoxSummaryAmount = editBoxSummaryAmount;
    }
}
