package com.tmx.as.entities.bill.terminal_group;

import com.tmx.as.base.BasicEntity;
import com.tmx.as.entities.bill.seller.Seller;
import com.tmx.as.entities.bill.balance.TerminalGroupBalance;

import java.io.Serializable;

/**
 */
public class TerminalGroup extends BasicEntity {
    private Long terminalGroupId;
    private String name;
    private Seller sellerOwner;
    private Long sellerOwnerId;
    private String login;
    private String password;
    private boolean blocked = false;
    private TerminalGroupBalance primaryBalance;

    public Long getSellerOwnerId() {
        return sellerOwnerId;
    }

    public void setSellerOwnerId(Long sellerOwnerId) {
        this.sellerOwnerId = sellerOwnerId;
    }

    public boolean isBlocked() {
        return blocked;
    }

    public void setBlocked(boolean blocked) {
        this.blocked = blocked;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Long getTerminalGroupId() {
        return terminalGroupId;
    }

    public void setTerminalGroupId(Long terminalGroupId) {
        this.terminalGroupId = terminalGroupId;
    }

    public Seller getSellerOwner() {
        return sellerOwner;
    }

    public void setSellerOwner(Seller sellerOwner) {
        this.sellerOwner = sellerOwner;
    }

    public TerminalGroupBalance getPrimaryBalance() {
        return primaryBalance;
    }

    public void setPrimaryBalance(TerminalGroupBalance primaryBalance) {
        this.primaryBalance = primaryBalance;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Serializable getId() {
        return getTerminalGroupId();
    }

    public int getValid() {
        return valid;
    }

    public void setValid(int valid) {
        this.valid = valid;
    }
}
