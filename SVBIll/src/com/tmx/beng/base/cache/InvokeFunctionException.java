package com.tmx.beng.base.cache;

import com.tmx.util.StructurizedException;

import java.util.Locale;

/**
 */
public class InvokeFunctionException extends StructurizedException {

    public InvokeFunctionException(String msg, String[] params, Throwable e, Locale locale) {
        super(msg, params, e, locale);
    }

    public InvokeFunctionException(String msg, String[] params) {
        super(msg, params);
    }

    public InvokeFunctionException(String msg, String[] params, Locale locale) {
        super(msg, params, locale);
    }

    public InvokeFunctionException(String msg, Throwable e, Locale locale) {
        super(msg, e, locale);
    }

    public InvokeFunctionException(String msg, Throwable e) {
        super(msg, e);
    }

    public InvokeFunctionException(String msg, Locale locale) {
        super(msg, locale);
    }

    public InvokeFunctionException(String msg) {
        super(msg);
    }

    public InvokeFunctionException(String msg, String[] params, Throwable e) {
        super(msg, params, e);
    }
}
