package com.tmx.web.base;

import com.tmx.util.StructurizedException;

import java.util.Locale;


public class WebException extends StructurizedException {

    public WebException(String errKey){
        super(errKey);
    }

    public WebException(String errKey, Locale locale){
        super(errKey, locale);
    }

    public WebException(String errKey, String[] params){
        super(errKey, params);
    }

    public WebException(String errKey, String[] params, Throwable cause){
        super(errKey, params, cause);
    }

    public WebException(String errKey, String[] params, Locale locale){
        super(errKey, params, locale);
    }

    public WebException(String errKey, Throwable cause){
        super(errKey, cause);
    }

}
