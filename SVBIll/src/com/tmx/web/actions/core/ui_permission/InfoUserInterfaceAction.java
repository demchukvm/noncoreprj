package com.tmx.web.actions.core.ui_permission;

import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForm;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.tmx.web.base.BasicDispatchedAction;
import com.tmx.web.base.Roadmap;
import com.tmx.web.forms.core.ui_permission.InfoUserInterfaceForm;
import com.tmx.as.entities.general.ui_permission.UserInterface;
import com.tmx.as.entities.general.ui_permission.UserInterfaceType;
import com.tmx.as.base.EntityManager;

public class InfoUserInterfaceAction extends BasicDispatchedAction {
    private static final String FORWARD_MANAGE_ACTION = "manageAction";
    private static final String FORWARD_INFO_PAGE = "infoPage";
    private static final String FORWARD_MANAGE_DELETE_ACTION = "manageDeleteAction";

    public ActionForward preloadForCreate(ActionMapping mapping,
                                          ActionForm form,
                                          HttpServletRequest request,
                                          HttpServletResponse response) throws Throwable {
        InfoUserInterfaceForm userInterfaceForm = (InfoUserInterfaceForm) verifyForm(form, InfoUserInterfaceForm.class);
        preload0(userInterfaceForm);
        userInterfaceForm.bindData(new UserInterface());
        return mapping.findForward(FORWARD_INFO_PAGE);
    }

    public ActionForward preloadForUpdate(ActionMapping mapping,
                                          ActionForm form,
                                          HttpServletRequest request,
                                          HttpServletResponse response) throws Throwable {
        InfoUserInterfaceForm userInterfaceForm = (InfoUserInterfaceForm) verifyForm(form, InfoUserInterfaceForm.class);
        preload0(userInterfaceForm);
        userInterfaceForm.bindData((UserInterface)new EntityManager().RETRIEVE(UserInterface.class,userInterfaceForm.getSelectedUserInterfaceId()));
        return mapping.findForward(FORWARD_INFO_PAGE);
    }

    public ActionForward apply(ActionMapping mapping,
                              ActionForm form,
                              HttpServletRequest request,
                              HttpServletResponse response) throws Throwable {
        validateTransactionToken(request);
        InfoUserInterfaceForm userInterfaceForm = (InfoUserInterfaceForm) verifyForm(form, InfoUserInterfaceForm.class);
        userInterfaceForm.validate(mapping.findForward(FORWARD_INFO_PAGE));
        UserInterface userInterface=userInterfaceForm.populateFromControls();
        new EntityManager().SAVE(userInterface);
        //render after saving
        userInterfaceForm.setSelectedUserInterfaceId(userInterface.getUserInterfaceId());
        userInterfaceForm.getActionExecStatus().setNewStatus("apply", "applied");
        return preloadForUpdate(mapping,form,request,response);
    }

    public ActionForward save(ActionMapping mapping,
                              ActionForm form,
                              HttpServletRequest request,
                              HttpServletResponse response) throws Throwable {
        apply(mapping, form, request, response);
        return mapping.findForward(FORWARD_MANAGE_ACTION);
    }

    public ActionForward preloadForDelete(ActionMapping mapping,
                                          ActionForm form,
                                          HttpServletRequest request,
                                          HttpServletResponse response) throws Throwable {
        Roadmap roadmap=getRoadmap();
        roadmap.collectParameter("/core/message/confirmation", "leftMenuName", "ui_permission");
        roadmap.setPageForward("confirmation","onOk","local:"+this.getName()+"#"+FORWARD_MANAGE_DELETE_ACTION);
        roadmap.setPageForward("confirmation","onCancel","local:"+this.getName()+"#"+FORWARD_MANAGE_ACTION);
        return mapping.findForward("confirmation");
    }

    public ActionForward delete(ActionMapping mapping,
                              ActionForm form,
                              HttpServletRequest request,
                              HttpServletResponse response) throws Throwable {
        InfoUserInterfaceForm userInterfaceForm = (InfoUserInterfaceForm) verifyForm(form, InfoUserInterfaceForm.class);
        new EntityManager().DELETE(UserInterface.class,userInterfaceForm.getSelectedUserInterfaceId());
        return mapping.findForward(FORWARD_MANAGE_ACTION);
    }

    private void preload0(InfoUserInterfaceForm form) throws Throwable{
        form.getType().bind(new EntityManager().RETRIEVE_ALL(UserInterfaceType.class,null));        
    }
}
