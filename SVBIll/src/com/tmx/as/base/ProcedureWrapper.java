package com.tmx.as.base;

import com.tmx.as.exceptions.DatabaseException;

/**
 * Created by IntelliJ IDEA.
 * User: Shake
 * Date: 05.03.2007
 * Time: 11:43:55
 * To change this template use File | Settings | File Templates.
 */
public class ProcedureWrapper {
    private QueryParameterWrapper[] params = null;
    private Class entityClass = null;
    private String name = null;

    public ProcedureWrapper(QueryParameterWrapper[] params, Class entityClass, String name){
        this.params = params;
        this.entityClass = entityClass;
        this.name = name;
    }

    public void invoke(EntityManager entityManager) throws DatabaseException {
        entityManager.EXECUTE_PROCEDURE(entityClass, name, params);
    }

    public void invoke() throws DatabaseException {
        invoke(new EntityManager());
    }
}
