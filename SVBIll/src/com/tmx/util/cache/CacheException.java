package com.tmx.util.cache;

import com.tmx.util.StructurizedException;

import java.util.Locale;

public class CacheException extends StructurizedException {

    public CacheException(String errKey){
        super(errKey);
    }

    public CacheException(String errKey, Locale locale){
        super(errKey, locale);
    }

    public CacheException(String errKey, String[] params){
        super(errKey, params);
    }

    public CacheException(String errKey, String[] params, Throwable cause){
        super(errKey, params, cause);
    }

    public CacheException(String errKey, String[] params, Locale locale){
        super(errKey, params, locale);
    }

    public CacheException(String errKey, Throwable cause){
        super(errKey, cause);
    }
}
