package com.tmx.beng.base.dbfunctions;

import com.tmx.as.entities.bill.transaction.TransactionType;
import com.tmx.beng.base.BillingMessage;
import com.tmx.beng.base.Reservation;
import com.tmx.beng.base.cache.InvokeFunctionException;

import java.util.Map;


/**
 * This tariff decreases primary seller balance with nominal transaction amount and
 * increses specified auxilary balance with seller's profit.
 */
public class SellerTariffDualBalances extends BasicTariffFunction {
    /** Seller discount [0; 1] */
    private static final String DISCOUNT_PERCENT = "discount_percent";
    /** Seller's balance code used to accumulate profit. */
    private static final String PROFIT_BALANCE_CODE = "profit_balance_code";

    boolean isTariffApplicable(Map actualParams) {
        return BillingMessage.O_REFILL_PAYMENT.equals(task.getProcesedMessage().getOperationName());
    }

    void applyTariff(Map actualParams, Reservation reservation) throws InvokeFunctionException {
        try {
            double discontPercent = Double.parseDouble((String) actualParams.get(DISCOUNT_PERCENT));
            double amount = Double.parseDouble(task.getProcesedMessage().getAttributeString(BillingMessage.AMOUNT));
            String profitBalanceCode = (String) actualParams.get(PROFIT_BALANCE_CODE);
            double profit = amount * (1 - discontPercent);

            //prepare reservation on primary tariff
            reservation.reservePrimaryBalance(amount, null, TransactionType.CODE_CREDIT);
            reservation.reserve(profitBalanceCode, profit, null, TransactionType.CODE_CREDIT);
        }
        catch (Throwable e) {
            throw new InvokeFunctionException("err.seller_tariff.calc_error", e);
        }
    }
}