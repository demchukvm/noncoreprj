package com.tmx.util.cache;

import java.util.*;

/**
basic cache impl
 */
public abstract class BasicCacheImpl implements Cache{
    protected Map objectStore = new HashMap();
    /** max cache size */
    protected int maxSize = 1000;
    /** clean objects then max cache size reached */
    protected int cleanCount = 50;
    /** clean algorithm type  */
    protected CleanAlgorithm.Type cleanType = CleanAlgorithm.Type.CLEAN_N_SELDOM_ACCESSED;


    //--------abstract
    protected abstract Object getRealObject(Object key) throws CacheException;

    protected abstract CachedObjectSignature createSignature(Object key) throws CacheException;

    //--------public

    public void init()
    {
        System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");    
    }

    public Object get(Object key) throws CacheException{
        CachedObjectHandler handler = (CachedObjectHandler)objectStore.get(key);
        if(handler == null){
            //
            //put new object into cache
            handler = new CachedObjectHandler();
            handler.accessCount = 1;
            handler.putTime = System.currentTimeMillis();
            handler.lastAccessTime = handler.putTime;
            handler.cachedObject = getRealObject(key);
            handler.signature = createSignature(key);
            safePut(key, handler);
            return handler.cachedObject;
        }
        else if(!handler.signature.isUpToDate()){
            //update obj in cache
            handler.cachedObject = getRealObject(key);
            handler.accessCount++;
            handler.lastAccessTime = System.currentTimeMillis();
            handler.signature = createSignature(key);
            safePut(key, handler);
            return handler.cachedObject;
        }
        else{
            //upd obj access statistics in cache
            handler.accessCount++;
            handler.lastAccessTime = System.currentTimeMillis();
            return handler.cachedObject;
        }
    }

    /** clear all cache */
    public void clear(){
        objectStore.clear();
    }

    public int getObjectCount(){
        return objectStore.size();
    }

    //---------private

    private void safePut(Object key, CachedObjectHandler handler){
        synchronized(objectStore){
            if(objectStore.size() >= maxSize){
                //clean place in cache
                new CleanAlgorithm(cleanType, cleanCount, objectStore).execute();
            }
            objectStore.put(key, handler);
        }
    }

    private class CachedObjectHandler{
        private Object cachedObject = null;
        private CachedObjectSignature signature = null;
        private long putTime = -1L;
        private long lastAccessTime = -1L;
        private int accessCount = -1;
    }

    protected static class CleanAlgorithm{
        private Type cleanType = null;
        private int cleanCount = -1;
        private Map objectStore = null;

        private CleanAlgorithm(Type cleanType, int cleanCount, Map objectStore){
            this.cleanCount = cleanCount;
            this.cleanType = cleanType;
            this.objectStore = objectStore;
        }

        private void execute(){
            TreeMap sortMap = new TreeMap(cleanType);
            for(Iterator iter = objectStore.keySet().iterator(); iter.hasNext();){
                Object key = iter.next();
                CachedObjectHandler handler = (CachedObjectHandler)objectStore.get(key);
                sortMap.put(handler, key);
            }
            //
            int maxSize = objectStore.size();//set max size before cleaning
            objectStore.clear();
            for(Iterator iter = sortMap.keySet().iterator(); iter.hasNext();){
                if(objectStore.size() >= maxSize - cleanCount)
                    break;//break then in the tree map left 'cleanCount' objects to clean
                CachedObjectHandler handler = (CachedObjectHandler)iter.next();
                Object key = sortMap.get(handler);
                objectStore.put(key, handler);
            }
        }

        //------basic clean algorythm type
        public abstract static class Type implements Comparator{
            public static Type CLEAN_N_OLDEST_PUTTED = new CleanNOldestPutted();
            public static Type CLEAN_N_OLDEST_ACCESSED = new CleanNOldestAccessed();
            public static Type CLEAN_N_SELDOM_ACCESSED = new CleanNSeldomAccessed();

            protected abstract int compare(CachedObjectHandler h1, CachedObjectHandler h2);

            public int compare(Object o1, Object o2){
                CachedObjectHandler h1 = (CachedObjectHandler)o1;
                CachedObjectHandler h2 = (CachedObjectHandler)o2;
                int result = compare(h1, h2);
                //to avoid overwriting objects in TreeMap this comparison
                // shoudn't returns 0. See compare() spec.
                if(result == 0){
                    int hashDif = h1.hashCode() - h2.hashCode();
                    if(hashDif < 0)
                        return -1;
                    else if(hashDif > 0)
                        return 1;
                    else
                        return 0;//seems it is realy the same object
                }
                else
                    return result;
            }

        }

        //------clean algorithms
        private static class CleanNOldestPutted extends Type{
            public int compare(CachedObjectHandler h1, CachedObjectHandler h2){
                long dif = h1.putTime - h2.putTime;
                if(dif > 0)
                    return -1;
                else if(dif < 0)
                    return 1;
                else
                    return 0;
            }
        }

        private static class CleanNOldestAccessed extends Type{
            public int compare(CachedObjectHandler h1, CachedObjectHandler h2){
                long dif = h1.lastAccessTime - h2.lastAccessTime;
                if(dif > 0)
                    return -1;
                else if(dif < 0)
                    return 1;
                else
                    return 0;
            }
        }

        private static class CleanNSeldomAccessed extends Type{
            public int compare(CachedObjectHandler h1, CachedObjectHandler h2){
                long dif = h1.accessCount - h2.accessCount;
                if(dif > 0)
                    return -1;
                else if(dif < 0)
                    return 1;
                else
                    return 0;
            }
        }
    }
}



