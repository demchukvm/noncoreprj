package com.tmx.util;

import com.tmx.util.StructurizedException;

import java.util.Locale;


public class InitException extends StructurizedException {

    public InitException(String msg, String[] params, Throwable e, Locale locale) {
        super(msg, params, e, locale);
    }

    public InitException(String msg, String[] params) {
        super(msg, params);
    }

    public InitException(String msg, String[] params, Locale locale) {
        super(msg, params, locale);
    }

    public InitException(String msg, Throwable e, Locale locale) {
        super(msg, e, locale);
    }

    public InitException(String msg, Throwable e) {
        super(msg, e);
    }

    public InitException(String msg, Locale locale) {
        super(msg, locale);
    }

    public InitException(String msg) {
        super(msg);
    }

    public InitException(String msg, String[] params, Throwable e) {
        super(msg, params, e);
    }
}
