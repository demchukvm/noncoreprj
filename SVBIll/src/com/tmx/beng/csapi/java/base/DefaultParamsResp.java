package com.tmx.beng.csapi.java.base;

import com.tmx.as.entities.bill.function.ActualFunctionParameter;

import java.util.List;


public class DefaultParamsResp {
    private List<ActualFunctionParameter> parameters;

    DefaultParamsResp(List<ActualFunctionParameter> parameters) {
        this.parameters = parameters;
    }

    public List<ActualFunctionParameter> getDefaultParams() {
        return parameters;
    }
}
