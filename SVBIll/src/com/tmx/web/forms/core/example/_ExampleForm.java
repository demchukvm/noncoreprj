package com.tmx.web.forms.core.example;

import com.tmx.web.base.BasicActionForm;
import com.tmx.web.controls.*;
import com.tmx.as.entities.general.role.Role;

import org.apache.struts.action.ActionMapping;

import javax.servlet.http.HttpServletRequest;

/**
 * This is the example Form that shows how to use nested form's controls
 */
public class _ExampleForm extends BasicActionForm {
    private TimePeriodSelector timePeriodSelector = null;
    private TimePeriodSelector timePeriodSelector555 = null;
    private CheckBox checkBox1 = null;
    private ScrollBox selectRoles1 = null;
    private RadioButtonGroup radioRoles1 = null;
    private TextArea textArea1 = null;

    /**
     * You could use this method to initialize your controls
     */
    protected void init() {
        //initialze and register controls
        timePeriodSelector = new TimePeriodSelector();
        timePeriodSelector555 = new TimePeriodSelector();

        checkBox1 = new CheckBox();
        checkBox1.setSelected(true);
        checkBox1.setLabel("Check box 1");

        selectRoles1 = new ScrollBox(){
            protected String getKey(Object dataListElement) {
                return ((Role)dataListElement).getRoleId().toString();
            }

            protected String getValue(Object dataListElement) {
                return ((Role)dataListElement).getRoleName();
            }
        };

        radioRoles1 = new RadioButtonGroup(){
            protected String getKey(Object dataListElement) {
                return ((Role)dataListElement).getRoleId().toString();
            }

            protected String getValue(Object dataListElement) {
                return ((Role)dataListElement).getRoleName();
            }
        };
        textArea1 = new TextArea();
        textArea1.setValue("Hello !!!!! \r hhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhh \r rrrrrrrrrrrrrr\r      \r   sfasdf\r");
        textArea1.setReadonly(false);
    }


    /**
     * IMPORTANT!
     * BasicActionForm introspects its sucessor and nested
     * hierarchy of ControlContainers to find and register
     * controls. So, your control should has standard getter
     * method to be registered by form.
     */
    public TimePeriodSelector getTimePeriodSelector555() {
        return timePeriodSelector555;
    }

    public void setTimePeriodSelector555(TimePeriodSelector timePeriodSelector555) {
        this.timePeriodSelector555 = timePeriodSelector555;
    }

    public CheckBox getCheckBox1() {
        return checkBox1;
    }

    public void setCheckBox1(CheckBox checkBox1) {
        this.checkBox1 = checkBox1;
    }

    public ScrollBox getSelectRoles1() {
        return selectRoles1;
    }

    public void setSelectRoles1(ScrollBox selectRoles1) {
        this.selectRoles1 = selectRoles1;
    }

    public RadioButtonGroup getRadioRoles1() {
        return radioRoles1;
    }

    public void setRadioRoles1(RadioButtonGroup radioRoles1) {
        this.radioRoles1 = radioRoles1;
    }

    public TextArea getTextArea1() {
        return textArea1;
    }

    public void setTextArea1(TextArea textArea1) {
        this.textArea1 = textArea1;
    }

    public void reset(ActionMapping mapping, HttpServletRequest request) {
        /**
         * IMPORTANT!
         * This mandatory to call super.reset() method, because it
         * initializes Form's controls
         * */
        super.reset(mapping, request);
        /**
         * Put you reset code here
         * */
    }

    /**
     * You can define nested ControlContainers. Panel is
     * on of the available implementation.
     */
    private MyPanel myPanel = new MyPanel();

    /**
     * Nested Panel (or other ControlContainer implementation)
     * should has standart getter to be registered on form initialization.
     *  */
    public MyPanel getMyPanel() {
        return myPanel;
    }

    public void setMyPanel(MyPanel myPanel) {
        this.myPanel = myPanel;
    }

    public class MyPanel extends Panel {
        /**
         * Nested Panel could has other Controls
         * */
        private TimePeriodSelector timePeriodSelector777 = null;
        private CheckBox checkBox2 = null;

        /**
         * Controls should has getters
         * */
        public TimePeriodSelector getTimePeriodSelector777() {
            return timePeriodSelector777;
        }

        public void setTimePeriodSelector777(TimePeriodSelector timePeriodSelector777) {
            this.timePeriodSelector777 = timePeriodSelector777;
        }

        public CheckBox getCheckBox2() {
            return checkBox2;
        }

        public void setCheckBox2(CheckBox checkBox2) {
            this.checkBox2 = checkBox2;
        }

        public void init() {
            timePeriodSelector777 = new TimePeriodSelector();
            checkBox2 = new CheckBox();
            checkBox2.setSelected(false);
            checkBox2.setLabel("Check box 2");
        }

        /**
         * Below there is an example of Panel nested to another Panel.
         * You could build your own hierarchies of nested Panels with
         * any deep. The rules of building each level in hierarchy
         * are the same as mentioned above for first Panel.
         * */
        private MyPanel2 myPanel2 = new MyPanel2();

        public MyPanel2 getMyPanel2() {
            return myPanel2;
        }

        public void setMyPanel2(MyPanel2 myPanel2) {
            this.myPanel2 = myPanel2;
        }

        public class MyPanel2 extends Panel {
            private TimePeriodSelector tps999 = null;
            private CheckBox checkBox3 = null;
            private ScrollBox selectRoles3 = null;
            private RadioButtonGroup radioRoles3 = null;

            public TimePeriodSelector getTps999() {
                return tps999;
            }

            public void setTps999(TimePeriodSelector tps999) {
                this.tps999 = tps999;
            }

            public CheckBox getCheckBox3() {
                return checkBox3;
            }

            public void setCheckBox3(CheckBox checkBox3) {
                this.checkBox3 = checkBox3;
            }

            public ScrollBox getSelectRoles3() {
                return selectRoles3;
            }

            public void setSelectRoles3(ScrollBox selectRoles3) {
                this.selectRoles3 = selectRoles3;
            }

            public RadioButtonGroup getRadioRoles3() {
                return radioRoles3;
            }

            public void setRadioRoles3(RadioButtonGroup radioRoles3) {
                this.radioRoles3 = radioRoles3;
            }

            public void init() {
                tps999 = new TimePeriodSelector();
                checkBox3 = new CheckBox();
                checkBox3.setSelected(true);
                checkBox3.setLabel("Check box 3");

                selectRoles3 = new ScrollBox(){
                    protected String getKey(Object dataListElement) {
                        return ((Role)dataListElement).getRoleId().toString();
                    }

                    protected String getValue(Object dataListElement) {
                        return ((Role)dataListElement).getRoleName();
                    }
                };

                radioRoles3 = new RadioButtonGroup(){
                    protected String getKey(Object dataListElement) {
                        return ((Role)dataListElement).getRoleId().toString();
                    }

                    protected String getValue(Object dataListElement) {
                        return ((Role)dataListElement).getRoleName();
                    }
                };
            }
        }
    }

}