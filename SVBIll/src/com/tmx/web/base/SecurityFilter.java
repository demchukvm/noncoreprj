package com.tmx.web.base;

import org.apache.struts.config.ModuleConfig;
import org.apache.struts.config.ForwardConfig;
import org.apache.struts.Globals;
import org.apache.log4j.Logger;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.StringTokenizer;
import com.tmx.util.StructurizedException;
import com.tmx.util.Configuration;
import com.tmx.util.StringUtil;
import com.tmx.as.modules.EntityResourcesModule;

/**
 * SecurityFilter is a filter for login sequence processing. Servlet container must be configured
 * to call this filter before any other. Authorization delegate must implment SecurityAgent interface and provide
 * public default constructor.
 */
public class SecurityFilter implements Filter {

    private int DEFAULT_SESSION_TIMEOUT = 15;//min
//    private Map locales = new HashMap();
    private ServletContext context;
    Logger logger = Logger.getLogger("web.SecurityFilter");

    /**
     * Set Inactivity interval and supported for user Locale
     * */
    private void configureSession(HttpServletRequest request) {
        int interval = DEFAULT_SESSION_TIMEOUT * 60;
        try {
            int value = Integer.parseInt(ApplicationEnvironment.getInitParameter("security.inactive-interval", String.valueOf(DEFAULT_SESSION_TIMEOUT)));
            interval = value * 60;
        } catch (Throwable ex) {/** ignore */
        }
        // if it was invalidated - recreate;
        HttpSession session = request.getSession();
        session.setMaxInactiveInterval(interval);

        // negotiate and set locale, if it was not set manually
        if(!SessionEnvironment.isLocaleManuallyChanged(request.getSession())){
            Locale locale = getSupportedLocaleForUser(request);
            SessionEnvironment.setLocale(request.getSession(), locale);
        }

    }

    private void storeState(HttpServletRequest req) {
        HttpSession s = req.getSession();
        if (s != null && s.getAttribute("old_request") == null && req.getPathInfo() != null && req.getPathInfo().length() > 0) {
            String q = req.getQueryString();
            if (q != null && q.length() > 0) {
                q = "?" + q;
            } else {
                q = "";
            }
            s.setAttribute("old_request", req.getRequestURL() + q);
        }
    }

    private String restoreState(HttpServletRequest req) {
        HttpSession s = req.getSession();
        if (s != null && s.getAttribute("old_request") != null) {
            String ret = s.getAttribute("old_request").toString();
            s.removeAttribute("old_request");
            return ret;
        }
        return null;
    }

    public void init(FilterConfig filterConfig) throws ServletException {
/*      RM: don't use filter parameter 'languages'.
        Use 'supported_locales' parameter from setup.properties and
        new Server().getEntityResourses().getSupportedLocales() instead.


        String languages = filterConfig.getInitParameter("languages");
        if(languages == null)
            return;
        StringTokenizer locz = new StringTokenizer(languages, ";");
        while (locz.hasMoreTokens()) {
            String loc = locz.nextToken();
            StringTokenizer tz = new StringTokenizer(loc, "= ");
            if (tz.countTokens() == 2) {
                String lang = tz.nextToken();
                String locale = tz.nextToken();
                tz = new StringTokenizer(locale, "_");
                if (tz.countTokens() == 2) {
                    String xlang = tz.nextToken();
                    String xregion = tz.nextToken();
                    locales.put(lang, new Locale(xlang, xregion));
                }
            }
        }*/
    }

    /** For using outside the Struts */
    private void forwardToErrorPage(HttpServletRequest req, HttpServletResponse res, StructurizedException structurizedException) {
        RequestEnvironment.saveError(req, structurizedException);
        //Use this forward name to find URL in the struts-config.xml
        final String forwardName = "accessDenied";
        ForwardConfig forwardConfig = getForwardConfig(req, forwardName);
        String accessDeniedURL = null;
        try{
            accessDeniedURL = forwardConfig.getPath();//Null pointer exception is possible
            RequestDispatcher reqDispather = ApplicationEnvironment.getRequestDispatcher(accessDeniedURL);
            if(reqDispather != null){
                //try to do forward
                reqDispather.forward(req, res);
                return;
            }
        }
        catch(Exception e){/** ignore and try to redirect instead of forward */}
        //try to do redirect
        try{
            res.sendRedirect(accessDeniedURL);
        }
        catch(IOException e){
            //write error just here
            res.setContentType("text/html; charset=UTF-8");
            res.setHeader("Pragma", "no-cache");
            res.setHeader("Cache-Control", "no-cache");
            try {
                java.io.PrintWriter out = res.getWriter();
                try {
                    out.println("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\">");
                    try {
                        out.print("<HTML><BODY><H1>Generic error</H1><PRE>");
                        out.println(structurizedException.toString());
                        out.print("</PRE></BODY></HTML>");
                    }
                    catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
                catch (RuntimeException ex) {
                    ex.printStackTrace(out);
                }
            } catch (Exception ex) {
                logger.error("General failure: " + e.toString());
            }
        }
    }

    /** Find ActionConfig inside the Struts config */
    private ForwardConfig getForwardConfig(HttpServletRequest request, String forwardName){
        //Get ModuleConfig
        ModuleConfig moduleConfig = (ModuleConfig)
            request.getAttribute(Globals.MODULE_KEY);
        if (moduleConfig == null) {
            moduleConfig = (ModuleConfig)
                request.getSession().getServletContext().getAttribute(Globals.MODULE_KEY);
        }
        //Get ForwardConfig
        return (ForwardConfig)moduleConfig.findForwardConfig(forwardName);
    }

    /** Return the available locale from server to user based on supported by server and client locales. */
    private Locale getSupportedLocaleForUser(HttpServletRequest request){
        Map locales = EntityResourcesModule.getEntityResources().getSupportedLocales();
        Locale locale = null;
        String languages = request.getHeader("Accept-Language");
        if (languages != null) {
            StringTokenizer tz = new StringTokenizer(languages, ",; ");
            while (tz.hasMoreTokens()) {
                String lang = tz.nextToken();
                Locale loc = (Locale) locales.get(lang);
                if (loc == null && lang.indexOf("-") > 0) {
                    lang = lang.substring(0, lang.indexOf("-"));
                    loc = (Locale) locales.get(lang);
                }
                if (loc != null) {
                    locale = loc;
                    break;
                }
            }
        }
        if (locale == null) {
            locale = (Locale) locales.get("en");
        }
        return locale;
    }


    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) servletRequest;
        HttpServletResponse resp = (HttpServletResponse) servletResponse;
        SecurityService securityService = (SecurityService)ApplicationEnvironment.getService(SecurityService.class);
        Locale locale = getSupportedLocaleForUser(req);
        if(securityService == null)
            throw new ServletException("SecurityService is not found");

        logger.debug(req.getRequestURI());

        if (!securityService.isSessionValid(req)){
            //If session not exists yet or invalidated (due a time-out) or the login&password were sent bu user.
            configureSession(req);
            storeState(req);
            WebException webException = null;

            if(!securityService.isPublicAccessGranted(req)){
                //If public access is not granted, check is there login attempt?
                //if (!securityService.isLoginRequested(req)){
                    /** that mean the login and password are not sended and the session was invalidated.
                     * That mean the session is invalidated or timed out. */
                    webException = new WebException("err.security_filter.access_denied", new String[]{req.getRequestURI()}, locale);

                //}
            }


            /** Show the exception */
            if (webException == null) {
                String rest = restoreState(req);
                if (rest != null) {
                    resp.sendRedirect(resp.encodeRedirectURL(rest));
                } else {
                    chain.doFilter(servletRequest, servletResponse);
                }
            } else {
                forwardToErrorPage(req, resp, webException);
                //req.getSession().invalidate();
            }

        } else {
            /** Check realms */
            if(!securityService.isAccessGranted(req)){
                WebException webException = new WebException("err.security_filter.access_denied", new String[]{req.getRequestURI()}, locale);
                forwardToErrorPage(req, resp, webException);
                return;
            }

            /** If user sent login or password */
/*            if(securityService.isLoginRequested(req)){
                Locale locale = getSupportedLocaleForUser(req);
                WebException webException = new WebException("err.you_should_logout_first", locale);
                //printPage(req, resp, structuredException);
                req.getSession().invalidate();
            }*/
            String rest = restoreState(req);
            if (rest != null) {
                resp.sendRedirect(resp.encodeRedirectURL(rest));
            } else {
                chain.doFilter(servletRequest, servletResponse);
            }
        }
    }


    /** Free the refs */
    public void destroy() {
    }


}