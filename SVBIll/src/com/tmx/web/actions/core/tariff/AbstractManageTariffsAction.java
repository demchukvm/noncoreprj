package com.tmx.web.actions.core.tariff;

import com.tmx.web.actions.core.AbstractSvbillAction;
import com.tmx.web.forms.core.tariff.AbstractManageTariffsForm;
import com.tmx.as.blogic.tariff.AbstractTariffManager;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


abstract public class AbstractManageTariffsAction extends AbstractSvbillAction {

    // valid action for all roadmap extraction, roadmap too much unintelligible :-)
    public static final String DEFAULT_VALID_ACTION = "/core/tariff/seller_tariff/infoSellerTariff";

    protected static final String FORWARD_MANAGE_RESTRICTIONS_PAGE = "manageTariffsPage";

    abstract protected AbstractTariffManager getTariffManager();

    public ActionForward defaultAction(ActionMapping mapping,
                                       ActionForm form,
                                       HttpServletRequest request,
                                       HttpServletResponse response,
                                       String methodName) throws Throwable {
        
        AbstractManageTariffsForm manageTariffsForm = (AbstractManageTariffsForm) verifyForm(form, AbstractManageTariffsForm.class);
        manageTariffsForm.getTariffsTable().refresh();
        return mapping.findForward(FORWARD_MANAGE_RESTRICTIONS_PAGE);
    }

//    public ActionForward onCreateTariffAction(ActionMapping mapping,
//                                       ActionForm form,
//                                       HttpServletRequest request,
//                                       HttpServletResponse response) throws Throwable {
//
//        AbstractManageTariffsForm manageSellerForm = (AbstractManageTariffsForm) verifyForm(form, AbstractManageTariffsForm.class);
//        manageSellerForm.getTariffsTable().refresh();
//        return mapping.findForward(FORWARD_MANAGE_RESTRICTIONS_PAGE);
//    }


}
