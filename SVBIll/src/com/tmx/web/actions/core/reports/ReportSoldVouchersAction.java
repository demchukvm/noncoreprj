package com.tmx.web.actions.core.reports;

import com.tmx.web.base.BasicDispatchedAction;
import com.tmx.web.base.SecurityService;
import com.tmx.web.base.ApplicationEnvironment;
import com.tmx.web.forms.core.reports.ReportVouchersForm;
import com.tmx.web.forms.core.base.AbstractFormFactory;
import com.tmx.web.controls.grid.Grid;
import com.tmx.web.controls.grid.GridException;
import com.tmx.web.controls.ScrollBox;
import com.tmx.as.blogic.VoucherManager;
import com.tmx.as.exceptions.DatabaseException;
import com.tmx.as.entities.bill.voucher.Voucher;
import com.tmx.as.entities.general.user.User;
import com.tmx.util.i18n.MessageResources;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForm;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

public class ReportSoldVouchersAction extends BasicDispatchedAction {
    private static final String FORWARD = "page";

    public ActionForward defaultAction(ActionMapping mapping,
                                       ActionForm form,
                                       HttpServletRequest request,
                                       HttpServletResponse response,
                                       String methodName) throws Throwable {
        ReportVouchersForm reportVauchersForm = (ReportVouchersForm) form;

        reportVauchersForm.bindData(); //TODO: move to separate method
        AbstractFormFactory.getInstance(getLoggedInUser(request)).setUpForm(reportVauchersForm);
        reportVauchersForm.getScrollBoxSeller().setReadonly(false);
        reportVauchersForm.refresh();
        reportVauchersForm.getButtonSubmit().setAction("/core/report/reportSoldVauchersAction");

        return mapping.findForward(FORWARD);
    }

    public ActionForward refreshVoucherReportGrid(ActionMapping mapping,
                              ActionForm form,
                              HttpServletRequest request,
                              HttpServletResponse response) throws Throwable {
        ReportVouchersForm reportVauchersForm = (ReportVouchersForm) form;

        if (!reportVauchersForm.getScrollBoxNominal().isNullKeySelected()) {
            reportVauchersForm.getScrollBoxOperator().setSelectedKey(ScrollBox.NULL_KEY); //clear operatot filter
        }

        reportVauchersForm.refresh();

        return mapping.findForward(FORWARD);
    }

    protected User getLoggedInUser(HttpServletRequest request) throws com.tmx.as.exceptions.SecurityException {
        SecurityService securityService = (SecurityService) ApplicationEnvironment.getService(SecurityService.class);
        return securityService.getLoggedInUser(request.getSession());
    }
}