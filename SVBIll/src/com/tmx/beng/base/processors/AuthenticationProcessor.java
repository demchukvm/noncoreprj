package com.tmx.beng.base.processors;

import com.tmx.beng.base.*;
import com.tmx.beng.base.cache.DBCache;
import com.tmx.beng.base.cache.DBCacheException;

/**
 * Authentificate client
 */
public class AuthenticationProcessor extends TaskProcessor {

    public Task process0(Task task, DBCache dbCache, BillingMessageWritable msg) throws BillException {
        logger.debug("Authentication...");

        //logger.debug(DebugPipelineUtil.dumpBillingMessage(msg, new StringBuffer()));

        // chesk is seller allowed
        if(!dbCache.isSellerAllowed(msg.getAttributeString(BillingMessage.SELLER_CODE))){
            msg.setStatusCode(StatusDictionary.AUTHENTICATION_ERROR);
            throw new BillException("err.bill.reg_processor.seller_not_allowed", new String[]{
                    msg.getAttributeString(BillingMessage.SELLER_CODE)
            });
        }                                                                            

        // check is processing allowed
        if(!dbCache.isProcessingAllowed(
                msg.getAttributeString(BillingMessage.PROCESSING_CODE))){
            msg.setStatusCode(StatusDictionary.AUTHENTICATION_ERROR);
            throw new BillException("err.bill.reg_processor.processing_not_allowed", new String[]{
                    msg.getAttributeString(BillingMessage.PROCESSING_CODE)
            });
        }

         // auth processing
        if(!dbCache.authenticateProcessing(
                msg.getAttributeString(BillingMessage.PROCESSING_CODE),
                msg.getAttributeString(BillingMessage.LOGIN),
                msg.getAttributeString(BillingMessage.PASSWORD))){
            msg.setStatusCode(StatusDictionary.AUTHENTICATION_ERROR);
            throw new BillException("err.bill.reg_processor.authentication_error", new String[]{
                    msg.getAttributeString(BillingMessage.PROCESSING_CODE)
            });
        }

        //auth terminal
        try {
            // проверяет есть ли такой терминал в базе
            if(!dbCache.authenticateTerminal(
                                msg.getAttributeString(BillingMessage.TERMINAL_SN),
                                msg.getAttributeString(BillingMessage.TERMINAL_PSWD))) {
                throw new BillException("err.bill.auth_terminal_error", new String[] {
                                        msg.getAttributeString(BillingMessage.TERMINAL_SN)});
            }
            } catch (DBCacheException e) {
                throw new BillException("err.bill.auth_terminal_error" + e.getMessage());


        }


        // check conformity
        // проверяет прописан ли за диллером данный терминал
        if(!dbCache.checkConformity(
                msg.getAttributeString(BillingMessage.PROCESSING_CODE),
                msg.getAttributeString(BillingMessage.SELLER_CODE),
                msg.getAttributeString(BillingMessage.TERMINAL_SN))){

            msg.setStatusCode(StatusDictionary.AUTHENTICATION_ERROR);
            throw new BillException("err.bill.reg_processor.conformity_error");
        }
        // get terminal allowed
//RM: todo ++support unregistered terminals
//        if(!task.getGlobalContext().getDbCache().isTerminalAllowed(
//                task.getProcesedMessage().getAttributeString(BillingMessage.TERMINAL_SN))){
//            task.getProcesedMessage().setStatusCode(StatusDictionary.AUTHENTICATION_ERROR);
//            throw new BillException("err.bill.reg_processor.terminal_not_allowed", new String[]{
//                    task.getProcesedMessage().getAttributeString(BillingMessage.TERMINAL_SN)
//            });
//        }

        // auth terminal
//RM: todo ++support unregistered terminals        
//        if(!task.getGlobalContext().getDbCache().authenticateTerminal(
//                task.getProcesedMessage().getAttributeString(BillingMessage.TERMINAL_SN),
//                task.getProcesedMessage().getAttributeString(BillingMessage.LOGIN),
//                task.getProcesedMessage().getAttributeString(BillingMessage.PASSWORD))){
//            task.getProcesedMessage().setStatusCode(StatusDictionary.AUTHENTICATION_ERROR);
//            throw new BillException("err.bill.reg_processor.authentication_error", new String[]{
//                    task.getProcesedMessage().getAttributeString(BillingMessage.TERMINAL_SN)
//            });
//        }



        /* 2010.09.03 Proximan */
      /*     if(msg.getAttributeString(BillingMessage.SELLER_CODE).equals("LocalSSM")
                   || msg.getAttributeString(BillingMessage.SELLER_CODE).equals("Winner_h2h")
                   || msg.getAttributeString(BillingMessage.SELLER_CODE).equals("SVCard")) {

               if(msg.getAttributeString(BillingMessage.SERVICE_CODE).equals("MTSExc")) {
                   msg.setAttributeString(BillingMessage.TERMINAL_SN, BillingMessage.TRADE_POINT);
                     //billMsg.setAttributeString(BillingMessage.TERMINAL_SN, operation.getTradePoint());
               }
           }
        */



        task.setAuthenticated(true);
        return task;
    }

}
