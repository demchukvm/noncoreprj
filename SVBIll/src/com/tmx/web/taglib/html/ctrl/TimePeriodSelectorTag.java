package com.tmx.web.taglib.html.ctrl;

import com.tmx.web.controls.TimePeriodSelector;
import com.tmx.web.base.ApplicationEnvironment;
import com.tmx.engines.reportengine.util.TimePeriod;
import javax.servlet.jsp.JspException;
import java.util.Iterator;
import java.util.List;

import org.apache.struts.taglib.TagUtils;

/**
Render html for time selector control and handle its actions
 */
public class TimePeriodSelectorTag extends com.tmx.web.taglib.html.ctrl.AbstractControlTag {
    private String allowManualChange = "true";
    private String calendarButtonImage = null; //"/management/images/img.gif
    private String editBoxStyleClass = null;
    private String dropdownBoxStyleClass = null;
    //-----------------------------Constants
    //JS calls
    public static final String ON_TIME_PERIOD_SELECT = "onTimePeriodSelect()";
    public static final String ON_CUSTOM_DATE_CHANGE = "onCustomDateChange()";
    //Style classes
    private final String CALENDAR_BUTTON_CLASS = "calendar_button";
    //html id suffix of calendar buttons 'From' and 'To'
    public static final String CALENDER_BUTTON_FROM_ID_SUFFIX = "TimeFromButton";
    public static final String CALENDER_BUTTON_TO_ID_SUFFIX = "TimeToButton";

    //------------------------------Properties
     public String getAllowManualChange() {
        return allowManualChange;
    }

    public void setAllowManualChange(String allowManualChange) {
        this.allowManualChange = allowManualChange;
    }

    public String getCalendarButtonImage() {
        return (calendarButtonImage != null) ? calendarButtonImage : getDefaultProperty("tag.timePeriodSelector.img.calendarButtonImage");
    }

    public void setCalendarButtonImage(String calendarButtonImage) {
        this.calendarButtonImage = (calendarButtonImage != null && calendarButtonImage.startsWith("/") && !calendarButtonImage.startsWith(ApplicationEnvironment.getAppDeploymentName()) ? ApplicationEnvironment.getAppDeploymentName() : "") + calendarButtonImage;
    }

    public String getEditBoxStyleClass() {
        return (editBoxStyleClass != null) ? editBoxStyleClass : getDefaultProperty("tag.timePeriodSelector.style.editBoxStyleClass");
    }

    public void setEditBoxStyleClass(String editBoxStyleClass) {
        this.editBoxStyleClass = editBoxStyleClass;
    }

    public String getDropdownBoxStyleClass() {
        return (dropdownBoxStyleClass != null) ? dropdownBoxStyleClass : getDefaultProperty("tag.timePeriodSelector.style.dropdownBoxStyleClass");
    }

    public void setDropdownBoxStyleClass(String dropdownBoxStyleClass) {
        this.dropdownBoxStyleClass = dropdownBoxStyleClass;
    }

    //------------------------------Rendering methods
    protected int controlStartTag() throws JspException {
        StringBuffer resultHTML = new StringBuffer();
        TimePeriodSelector tps = getTimePeriodSelector();



        //Render HTML
        resultHTML.append("<table border=\"0\" cellpadding=\"2\" cellspacing=\"2\">\r");
        resultHTML.append(" <tr>\r");

        //dropdown list
        resultHTML.append("     <td>\r");
        resultHTML.append("         <select name=\""+getProperty() +"."+TimePeriodSelector.TIME_PERIOD_SUFFIX+"\" class=\"" + getDropdownBoxStyleClass() + "\" onchange=\"" + getSeparatorReplacedName(getProperty()) + "_" + ON_TIME_PERIOD_SELECT + "\">\r");
        List orderedTimePeriods = tps.getOrderedTimePeriods();
        for(Iterator iter = orderedTimePeriods.iterator(); iter.hasNext();){
            TimePeriod timePeriod = (TimePeriod)iter.next();
            String attrSelectedHTML = (timePeriod.getType().equals(tps.getCurrentTimePeriod().getType()) ? "selected=\"selected\"" : "");
            resultHTML.append("             <option value="+timePeriod.getType()+ " " + attrSelectedHTML + ">\r");
            resultHTML.append(timePeriod.getLabel());
            resultHTML.append("             </option>\r");
        }
        resultHTML.append("     </td>\r");


        String readonly = (getAllowManualChange().equalsIgnoreCase("false")) ? " readonly=\"true\" " :"";

        //time from
        resultHTML.append("     <td>\r");
        resultHTML.append("         "+getLocalizedLabel("tag.timePeriodSelector.label.timeFrom", null)+"\r");
        resultHTML.append("     </td>\r");
        resultHTML.append("     <td>\r");
        resultHTML.append("         <input name=\""+getProperty()+ "." + TimePeriodSelector.FROM_DATE_SUFFIX+ "\" value=\""+tps.getCurrentTimePeriod().getTimeFromString()+"\" type=\"text\" class=\""+getEditBoxStyleClass()+"\" "+readonly+" onchange=\""+ getSeparatorReplacedName(getProperty()) + "_" + ON_CUSTOM_DATE_CHANGE +";\">\r");
        resultHTML.append("     </td>\r");
        resultHTML.append("     <td>\r");
        resultHTML.append("         <img alt=\"\" src=\""+getCalendarButtonImage()+"\" id=\""+getProperty() + "." + CALENDER_BUTTON_FROM_ID_SUFFIX+"\" class=\""+CALENDAR_BUTTON_CLASS+"\"\r");
        resultHTML.append("     </td>\r");

        //time to
        resultHTML.append("     <td>\r");
        resultHTML.append("         "+getLocalizedLabel("tag.timePeriodSelector.label.timeTo", null)+"\r");
        resultHTML.append("     </td>\r");
        resultHTML.append("     <td>\r");
        resultHTML.append("         <input name=\""+getProperty()+ "." + TimePeriodSelector.TO_DATE_SUFFIX+ "\" value=\""+tps.getCurrentTimePeriod().getTimeToString()+"\" type=\"text\" class=\""+getEditBoxStyleClass()+"\" "+readonly+" onchange=\""+ getSeparatorReplacedName(getProperty()) + "_" + ON_CUSTOM_DATE_CHANGE +";\">\r");
        resultHTML.append("     </td>\r");
        resultHTML.append("     <td>\r");
        resultHTML.append("         <img alt=\"\" src=\""+getCalendarButtonImage()+"\" id=\""+getProperty() + "." + CALENDER_BUTTON_TO_ID_SUFFIX +"\" class=\""+CALENDAR_BUTTON_CLASS+"\"\r");
        resultHTML.append("     </td>\r");

        resultHTML.append("  </tr>\r");

        resultHTML.append("</table>\r");

        //Render JS
        // calendar for 'time From'
        resultHTML.append("<script type=\"text/javascript\">");
        resultHTML.append("     Calendar.setup({\r");
        resultHTML.append("         inputField     :    \""+ tps.getFullName() +"."+ TimePeriodSelector.FROM_DATE_SUFFIX +"\",      // id of the input field\r");
        resultHTML.append("         ifFormat       :    \"%Y-%m-%d %H:%M\",       // format of the input field\r");
        resultHTML.append("         button         :    \""+ tps.getFullName() +"."+ TimePeriodSelectorTag.CALENDER_BUTTON_FROM_ID_SUFFIX +"\",   // trigger for the calendar (button ID)\r");
        resultHTML.append("         singleClick    :    true,         // one-click mode\r");
        resultHTML.append("         step           :    1,            // show all years in drop-down boxes (instead of every other year as default)\r");
        resultHTML.append("         showsTime      :    true,\r");
        resultHTML.append("         timeFormat     :    \"24\"\r");
        resultHTML.append("     });\r");
        resultHTML.append("\r");
        // calendar for 'time To'
        resultHTML.append("     Calendar.setup({\r");
        resultHTML.append("         inputField     :    \""+ tps.getFullName() +"."+ TimePeriodSelector.TO_DATE_SUFFIX +"\",      // id of the input field\r");
        resultHTML.append("         ifFormat       :    \"%Y-%m-%d %H:%M\",       // format of the input field\r");
        resultHTML.append("         button         :    \""+ tps.getFullName() +"."+ TimePeriodSelectorTag.CALENDER_BUTTON_TO_ID_SUFFIX +"\",   // trigger for the calendar (button ID)\r");
        resultHTML.append("         singleClick    :    true,         // one-click mode\r");
        resultHTML.append("         step           :    1,            // show all years in drop-down boxes (instead of every other year as default)\r");
        resultHTML.append("         showsTime      :    true,\r");
        resultHTML.append("         timeFormat     :    \"24\"\r");
        resultHTML.append("     });\r");
        resultHTML.append("\r");
        String replacedDotName = getSeparatorReplacedName(tps.getFullName());
        resultHTML.append("     //Prepare time period selector 'time from' and 'time to' arrays. They are the same for all\r");
        resultHTML.append("     //TimePeriodSelector objects on the page.\r");
        resultHTML.append("     var "+ replacedDotName + "_TimeFromMap = new Object();\r");
        resultHTML.append("     var "+ replacedDotName + "_TimeToMap = new Object();\r");

        //populate associated arrays
        for(Iterator periodsIter = tps.getOrderedTimePeriods().iterator(); periodsIter.hasNext();){
            TimePeriod tp = (TimePeriod)periodsIter.next();
            String timeFrom = tp.getTimeFromString();
            String timeTo = tp.getTimeToString();

            resultHTML.append("     "+replacedDotName +"_TimeFromMap['"+ tp.getType().getName() +"'] = '"+ timeFrom +"';\r");
            resultHTML.append("     "+replacedDotName +"_TimeToMap['"+ tp.getType().getName() +"'] = '"+ timeTo +"';\r");
        }
        resultHTML.append("\r");
        resultHTML.append("     //Called on time period selection in the TimePeriodSelectors. Set appropriate 'From' and 'To' dates into TimePeriodSelector.\r");
        resultHTML.append("     //@param timePeriodSelectorObjName name of used TimePeriodSelector object\r");
        resultHTML.append("     function "+ replacedDotName +"_"+ TimePeriodSelectorTag.ON_TIME_PERIOD_SELECT +"{\r");
        resultHTML.append("         var selectObject = getRawObject('"+ tps.getFullName() +"."+ TimePeriodSelector.TIME_PERIOD_SUFFIX +"');\r");
        resultHTML.append("         var from = getRawObject('"+ tps.getFullName() +"."+ TimePeriodSelector.FROM_DATE_SUFFIX +"');\r");
        resultHTML.append("         from.value = "+ replacedDotName +"_TimeFromMap[selectObject.value];\r");
        resultHTML.append("         var to = getRawObject('"+ tps.getFullName() +"."+ TimePeriodSelector.TO_DATE_SUFFIX +"');\r");
        resultHTML.append("         to.value = "+ replacedDotName +"_TimeToMap[selectObject.value];\r");
        resultHTML.append("     }\r");
        resultHTML.append("\r");
        resultHTML.append("     //Called on custom 'From' or 'To' date change. Sets TimePeriod type = CUSTOM\r");
        resultHTML.append("     function "+ replacedDotName +"_"+ TimePeriodSelectorTag.ON_CUSTOM_DATE_CHANGE +"{\r");
        resultHTML.append("         var selectObject = getRawObject('"+ tps.getFullName() +"." +TimePeriodSelector.TIME_PERIOD_SUFFIX +"');\r");
        resultHTML.append("         selectObject.value = '"+ TimePeriod.Type.CUSTOM +"';\r");
        resultHTML.append("}\r");
        resultHTML.append("</script>\r");
        resultHTML.append("\r");

        TagUtils.getInstance().write(pageContext,resultHTML.toString());
        return SKIP_BODY;
    }

    private TimePeriodSelector getTimePeriodSelector() throws JspException{
        Object object = getPropertyObject();
        return (object != null && object instanceof TimePeriodSelector) ? (TimePeriodSelector)object : new TimePeriodSelector();
    }

}