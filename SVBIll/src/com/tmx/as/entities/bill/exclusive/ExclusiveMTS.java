package com.tmx.as.entities.bill.exclusive;

import com.tmx.as.base.BasicEntity;
import com.tmx.as.entities.general.user.User;
import java.util.Date;
import java.io.Serializable;

public class ExclusiveMTS extends BasicEntity {

    private Long excMTSId;
    private String terminal;
    private Date addDate;
    private User addUser;
    private Long addUserId;

    public Serializable getId() {
        return getExcMTSId();
    }

    public int getValid() {
        return valid;
    }

    public void setValid(int valid) {
        this.valid = valid;
    }

    public Long getExcMTSId() {
        return excMTSId;
    }

    public void setExcMTSId(Long excMTSId) {
        this.excMTSId = excMTSId;
    }

    public String getTerminal() {
        return terminal;
    }

    public void setTerminal(String terminal) {
        this.terminal = terminal;
    }

    public Date getAddDate() {
        return addDate;
    }

    public void setAddDate(Date addDate) {
        this.addDate = addDate;
    }

    public User getAddUser() {
        return addUser;
    }

    public void setAddUser(User addUser) {
        this.addUser = addUser;
    }

    public Long getAddUserId() {
        return addUserId;
    }

    public void setAddUserId(Long addUserId) {
        this.addUserId = addUserId;
    }
}
