package com.tmx.beng.base.processors;

import com.tmx.beng.base.*;
import com.tmx.beng.base.cache.DBCache;
import com.tmx.as.entities.bill.transaction.ClientTransaction;
import com.tmx.as.entities.mobipay.kyivstar.MobipayKyivstar;
import com.tmx.as.entities.mobipay.beeline.Beeline;
import com.tmx.as.base.EntityManager;
import com.tmx.as.base.FilterWrapper;
import com.tmx.as.exceptions.DatabaseException;

import java.util.Date;

/**
 * Identify request status processor
 */
public class IdentifyStatusProcessor extends TaskProcessor {

    public Task process0(Task task, DBCache dbCache, BillingMessageWritable msg) throws BillException {
        logger.debug("Identify status...");
        Status status;
        if (msg.getAttributeString(BillingMessage.CLI_TRANSACTION_NUM) != null &&
                msg.getAttributeString(BillingMessage.BILL_TRANSACTION_NUM) == null) {

            status = identifyTxStatus(task, dbCache, msg, true);
        } else if (msg.getAttributeString(BillingMessage.BILL_TRANSACTION_NUM) != null &&
                msg.getAttributeString(BillingMessage.CLI_TRANSACTION_NUM) == null) {

            status = identifyTxStatus(task, dbCache, msg, false);
        } else {
            BillException e = new BillException("err.identify_status_proc.incorrect_trans_numbers_combination");
            msg.setStatus(StatusDictionary.IDENTIFY_STATUS_ERROR, e.getLocalizedMessage());
            throw e;
        }

        msg.setStatus(status.statusCode, status.statusDescription);
        msg.setAttribute(BillingMessage.REGISTRATION_TIME, status.registrationTime);
        msg.setAttribute(BillingMessage.PAY_ID, status.payId);
        msg.setAttribute(BillingMessage.RECEIPT_NUM, status.receiptNum);
        msg.setAttribute(BillingMessage.AMOUNT, status.amount);
        return task;
    }

    protected Status identifyTxStatus(Task task, DBCache dbCache, BillingMessageWritable msg, boolean client) throws BillException {
        FilterWrapper byTransactionNum;
        String transactionNum;
        Status status = new Status();

        if (client) {
            transactionNum = msg.getAttributeString(BillingMessage.CLI_TRANSACTION_NUM);
            byTransactionNum = new FilterWrapper("by_clinum");
            byTransactionNum.setParameter("clinum", transactionNum);
        } else {
            transactionNum = msg.getAttributeString(BillingMessage.BILL_TRANSACTION_NUM);
            byTransactionNum = new FilterWrapper("by_billnum");
            byTransactionNum.setParameter("billnum", transactionNum);
        }

        try {
            EntityManager entitityManager = new EntityManager();

            // get client transaction
            ClientTransaction cliTx = (ClientTransaction) entitityManager.
                    RETRIEVE(ClientTransaction.class, new FilterWrapper[]{byTransactionNum}, new String[]{"error"});

            if (cliTx == null) {
                msg.setStatusCode(StatusDictionary.IDENTIFY_STATUS_ERROR);
                throw new BillException("err.identify_status_proc.transaction_not_found", new String[]{transactionNum});
            }
            String stutusCodeStr = cliTx.getStatus();
            status.setStatusCode((stutusCodeStr != null) ? Integer.parseInt(stutusCodeStr) : StatusDictionary.UNKNOWN_STATUS);
            if (cliTx.getError() != null) {
                status.setStatusDescription(cliTx.getError().getMessage());
            }
            status.setRegistrationTime(cliTx.getTransactionTime());
            status.setAmount(cliTx.getAmount());
                                               
//            try {
//                // try to get info from MobipayKyivstar
//                FilterWrapper by_billnum = new FilterWrapper("mobipay_by_billnum");
//                by_billnum.setParameter("billnum", cliTx.getTransactionGUID());
//                MobipayKyivstar kyivstar = (MobipayKyivstar) entitityManager
//                        .RETRIEVE(MobipayKyivstar.class, new FilterWrapper[]{by_billnum}, null);
//                if (kyivstar != null) {
//                    status.setPayId(kyivstar.getPayId());
//                    status.setReceiptNum(kyivstar.getReceiptNum());
//                } else {
//                    // try to get from beeline
//                    Beeline beeline = (Beeline) entitityManager
//                            .RETRIEVE(Beeline.class, new FilterWrapper[]{by_billnum}, null);
//                    if (beeline != null) {
//                        status.setPayId(beeline.getPayId());
//                        status.setReceiptNum(beeline.getReceiptNum());
//                    }
//                }              l
//            } catch (Exception e) {
//                logger.error("err.identify_status_proc.igored_error", e);
//                //ignore
//            }

        } catch (DatabaseException e) {
            msg.setStatusCode(StatusDictionary.IDENTIFY_STATUS_ERROR);
            throw new BillException("err.identify_status_proc.error", e);
        }
        return status;
    }

    protected class Status {
        private int statusCode;
        private String statusDescription;
        private Date registrationTime;
        private Long payId;
        private Double amount;
        private Integer receiptNum;

        public void setAmount(Double amount) {
            this.amount = amount;
        }

        public void setStatusCode(int statusCode) {
            this.statusCode = statusCode;
        }

        public void setStatusDescription(String statusDescription) {
            this.statusDescription = statusDescription;
        }

        public void setRegistrationTime(Date registrationTime) {
            this.registrationTime = registrationTime;
        }

        public void setPayId(Long payId) {
            this.payId = payId;
        }

        public void setReceiptNum(Integer receiptNum) {
            this.receiptNum = receiptNum;
        }
    }
}
