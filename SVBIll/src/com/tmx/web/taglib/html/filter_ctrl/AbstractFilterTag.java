package com.tmx.web.taglib.html.filter_ctrl;

import com.tmx.web.taglib.html.ctrl.AbstractControlTag;
import com.tmx.web.controls.table.Table;
import com.tmx.web.controls.table.FilterSet;
import com.tmx.web.controls.table.Filter;

import javax.servlet.jsp.JspException;

/**
Basic filter tag to render all controls
 */
public abstract class AbstractFilterTag extends AbstractControlTag {
    private String filterName = null;
    protected FilterSet filterSet = null;
    /** Form bean name */
    private String name = null;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFilterName() {
        return filterName;
    }

    public void setFilterName(String filterName) {
        this.filterName = filterName;
    }

    protected int controlStartTag() throws JspException {
        filterSet = getFilterSetObject();
        return SKIP_BODY;
    }

    protected FilterSet getFilterSetObject() throws JspException{
        Object object = getPropertyObject();
        return (object != null && object instanceof Table) ?
               (FilterSet)((Table)object).getFilterSet() : null;
    }

    protected Filter getFilter(){
        return filterSet.getFilter(filterName);
    }

    protected String getOnEnterPress(){
        return " onkeypress=\"submit_on_enter_press(event)\" ";
    }

}
