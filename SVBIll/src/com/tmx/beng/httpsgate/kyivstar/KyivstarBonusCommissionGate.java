package com.tmx.beng.httpsgate.kyivstar;

import org.apache.log4j.Logger;
import com.tmx.beng.httpsgate.HttpsGateException;
import com.tmx.util.InitException;

/**
 */
public class KyivstarBonusCommissionGate extends BasicKyivstarGate {
    public static final String GATE_NAME = "kyivstar_bonus_commission_gate";
    protected static Logger logger = Logger.getLogger(GATE_NAME);
    private static KyivstarBonusCommissionGate gate = null;

    public static KyivstarBonusCommissionGate getInstance()throws HttpsGateException {
        if(gate == null){
            logger.error("Attempt to obtain not inited billing engine");
            throw new HttpsGateException("err.KyivstarBonusCommissionGate.null_instance");
        }
        return gate;
    }

    public static void init()throws InitException {
        logger.info("Start KyivstarBonusCommissionGate initialization");
        gate = new KyivstarBonusCommissionGate();
        gate.init0();
        logger.info("KyivstarBonusCommissionGate initialized");
    }

    public static void shutdown(final long timeout) throws InitException {
        logger.info("Start KyivstarBonusCommissionGate Shutdown");
        gate.shutdown0(timeout);
        logger.info("KyivstarBonusCommissionGate shutdown");
    }

    protected String getGateName(){
        return GATE_NAME;
    }
}

