package com.tmx.web.taglib.html;

import javax.servlet.jsp.JspException;
import org.apache.struts.taglib.nested.bean.NestedWriteTag;
import org.apache.struts.taglib.TagUtils;

public class WriteTag extends NestedWriteTag {

    private String length;//target length,

    private static final String SKIP_CHARS = "...";

    public WriteTag(){
        super();
        filter = false;
        ignore = true;
    }

    public int doStartTag() throws JspException {
        if (!ignore)
            return super.doStartTag();

        try {
            return super.doStartTag();
        } catch (Throwable t) {
            return SKIP_BODY;
        }
    }

    private String getText(String text, int targetLength){
        String result = "";
        targetLength -= 3;//(A.N.) what a fuck???

        if(targetLength > 0)
            result = text.substring(0, targetLength);

        return result + SKIP_CHARS;
    }




    protected String formatValue(Object valueToFormat) throws JspException {
        String result = super.formatValue(valueToFormat);
        if(length != null){
            int targetLength = Integer.parseInt(length);

            if(result.length() > targetLength){
                StringBuffer sb = new StringBuffer("<font title=\"").append(result).append("\">");
                sb.append(TagUtils.getInstance().filter( getText(result,targetLength) ));
                sb.append("</font>");

                result = sb.toString();
            }
        }
        return result;
    }

    public String getLength() {
        return length;
    }

    public void setLength(String length) {
        this.length = length;
    }

}

