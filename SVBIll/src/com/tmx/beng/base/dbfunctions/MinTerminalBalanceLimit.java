package com.tmx.beng.base.dbfunctions;

import com.tmx.as.entities.bill.balance.TerminalBalance;
import com.tmx.as.entities.bill.terminal.Terminal;
import com.tmx.beng.base.BillingMessage;
import com.tmx.beng.base.Reservation;
import com.tmx.beng.base.ReservationSet;
import com.tmx.beng.base.BillingMessageWritable;
import com.tmx.beng.base.cache.DBCacheException;
import com.tmx.beng.base.cache.InvokeFunctionException;

import java.util.Iterator;
import java.util.Map;

/**
 * Check minimal terminal balance limit
 */
public class MinTerminalBalanceLimit extends BasicLimitFunction {
    public static final String MIN_TERM_BALANCE_LIMIT = "min_terminal_balance_limit";

    public void checkLimit(Map actualParams) throws InvokeFunctionException {
        BillingMessageWritable billingMessage = task.getProcesedMessage();
        // check min terminal balance
        Double amount = new Double(billingMessage.getAttributeString(BillingMessage.AMOUNT));

        Double minTermLimit = new Double((String) actualParams.get(MIN_TERM_BALANCE_LIMIT));
        String terminalSn = billingMessage.getAttributeString(BillingMessage.TERMINAL_SN); 

        //obtain terminal tariffed amount
        ReservationSet reserveationSet = (ReservationSet)billingMessage.getAttribute(BillingMessage.BALANCE_RESERVATION_SET);
        Reservation reservation = reserveationSet.getReservation(Terminal.class, terminalSn);
        for(Iterator iter = reservation.reservesIterator(); iter.hasNext();){
            Reservation.Reserve reserve = (Reservation.Reserve)iter.next();
            TerminalBalance terminalBalance = null;
            try{
                terminalBalance = task.getSessionDBCache().getTerminalBalance(terminalSn, reserve.getBalanceCode());
            }
            catch(DBCacheException e){
                throw new InvokeFunctionException("err.bill.dbfunctions.terminal_balance_not_found", new String[]{terminalSn, reserve.getBalanceCode()}, e);
            }
            if (minTermLimit.doubleValue() > (terminalBalance.getAmount().doubleValue() - amount.doubleValue()))
                throw new InvokeFunctionException("err.bill.dbfunctions.min_terminal_limit_exceed");
        }
    }

    public TariffParameter[] getDefaultParams() {
        return new TariffParameter[] {
                new TariffParameter(MIN_TERM_BALANCE_LIMIT, "0", true)
        };
    }
}
