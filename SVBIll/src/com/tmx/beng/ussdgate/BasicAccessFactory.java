package com.tmx.beng.ussdgate;

import com.tmx.beng.medaccess.MediumAccess;
import com.tmx.beng.medaccess.ussd.UssdGateMediumAccess;
import com.tmx.beng.medaccess.kyivstar.KyivstarGateMediumAccess;

import javax.naming.Name;
import javax.naming.Context;
import javax.naming.Reference;
import javax.naming.RefAddr;
import java.util.Hashtable;
import java.util.Properties;
import java.io.ByteArrayInputStream;


public class BasicAccessFactory {
    private final static String PROP_TYPE = "type";

    private final static String[] ALL_PROPERTIES = {
        PROP_TYPE,
    };

    // -------------------------------------------------- ObjectFactory Methods

    /**
     * <p>Create and return a new <code>BasicDataSource</code> instance.  If no
     * instance can be created, return <code>null</code> instead.</p>
     *
     * @param obj The possibly null object containing location or
     *  reference information that can be used in creating an object
     * @param name The name of this object relative to <code>nameCtx</code>
     * @param nameCtx The context relative to which the <code>name</code>
     *  parameter is specified, or <code>null</code> if <code>name</code>
     *  is relative to the default initial context
     * @param environment The possibly null environment that is used in
     *  creating this object
     *
     * @exception Exception if an exception occurs creating the instance
     */
    public Object getObjectInstance(Object obj, Name name, Context nameCtx, Hashtable environment) throws Exception {

        if ((obj == null) || !(obj instanceof Reference)) {
            return null;
        }
        Reference ref = (Reference) obj;
        if (!"com.tmx.beng.base.medaccess.MediumAccess".equals(ref.getClassName())) {
            return null;
        }

        Properties properties = new Properties();
        for (int i = 0 ; i < ALL_PROPERTIES.length ; i++) {
            String propertyName = ALL_PROPERTIES[i];
            RefAddr ra = ref.get(propertyName);
            if (ra != null) {
                String propertyValue = ra.getContent().toString();
                properties.setProperty(propertyName, propertyValue);
            }
        }

        return createAccess(properties);
    }

    /**
     * Creates and configures a BasicDataSource instance based on the
     * given properties.
     */
    public static MediumAccess createAccess(Properties properties) throws Exception {
        String value = null;
        MediumAccess access = null;

        value = properties.getProperty(PROP_TYPE);
        if (value != null) {
            if("ussd".equals(value)){
                access = new UssdGateMediumAccess();
            }else if("https".equals(value)){
                access = new KyivstarGateMediumAccess();
            }
        }
        else{
            throw new Exception("err.beng.unknown_access_type " + value);
        }

        // Return the configured Access instance
        return access;
    }

    /**
     * <p>Parse properties from the string. Format of the string must be [propertyName=property;]*<p>
     * @param propText
     * @return Properties
     * @throws Exception
     */
    private static Properties getProperties(String propText) throws Exception {
      Properties p = new Properties();
      if (propText != null) {
        p.load(new ByteArrayInputStream(propText.replace(';', '\n').getBytes()));
      }
      return p;
    }

}
