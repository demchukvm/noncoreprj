package com.tmx.beng.base;

import com.tmx.beng.base.memory.InMemoryBean;

import java.util.List;
import java.util.ArrayList;
import java.util.Vector;
import java.util.Date;


/**

 */
public class TransactionImpl extends InMemoryBean implements Transaction{
    private String transactionId;
    private String cliTransactionNum;    
    List boundedTasks = null;
    boolean finished = false;
    Date creationTime = null;

    public TransactionImpl(){
        creationTime = new Date();
        boundedTasks = new ArrayList();
    }

    public List getTasks() {
        return boundedTasks;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void addTask(Task task) {
/*        if(boundedTasks.size() != 0){
            task.setCallBack(((Task)boundedTasks.get(0)).getCallBack());
        }*/
        boundedTasks.add(task);
    }


    public Date getRegistrationDate() {
        return this.creationTime;
    }

    public void setId(String id) {
        this.transactionId = id;
    }

    public void close(){
        boundedTasks = null;
    }

    public void finish() {
        finished = true;
    }


    public String getCliTransactionNum() {
        return cliTransactionNum;
    }

    public void setCliTransactionNum(String cliTransactionNum) {
        this.cliTransactionNum = cliTransactionNum;
    }

}
