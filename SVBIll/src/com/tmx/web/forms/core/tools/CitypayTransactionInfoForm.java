package com.tmx.web.forms.core.tools;

import com.tmx.web.controls.Button;
import com.tmx.web.controls.EditBox;
import com.tmx.web.base.BasicActionForm;

import java.util.HashMap;

public class CitypayTransactionInfoForm extends BasicActionForm
{
    private Button getStatus = null;
    private Button getStatusLifeExc = null;
    private EditBox billingNumber = null;
    private String citypayStatusInfo = "";
    private String citypayForLifeExcStatusInfo = "";

    public void init()
    {
        getStatus = new Button();
        getStatus.setLabelKey("ctrl.button.label.view");
        getStatus.setAction("/citypayTransactionInfoAction");
        getStatus.setCommand("getStatus");

        getStatusLifeExc = new Button();
        getStatusLifeExc.setLabelKey("ctrl.button.label.view");
        getStatusLifeExc.setAction("/citypayTransactionInfoAction");
        getStatusLifeExc.setCommand("getStatusLifeExc");

        billingNumber = new EditBox();
        billingNumber.setReadonly(false);

/*        citypayStatusInfo = new EditBox();
        citypayStatusInfo.setReadonly(true);*/
    }

    public Button getGetStatus() {
        return getStatus;
    }

    public void setGetStatus(Button getStatus) {
        this.getStatus = getStatus;
    }

    public EditBox getBillingNumber() {
        return billingNumber;
    }

    public void setBillingNumber(EditBox billingNumber) {
        this.billingNumber = billingNumber;
    }

/*    public EditBox getCitypayStatusInfo() {
        return citypayStatusInfo;
    }

    public void setCitypayStatusInfo(EditBox citypayStatusInfo) {
        this.citypayStatusInfo = citypayStatusInfo;
    }*/

    public String getCitypayStatusInfo() {
        return citypayStatusInfo;
    }

    public void setCitypayStatusInfo(String citypayStatusInfo) {
        this.citypayStatusInfo = citypayStatusInfo;
    }

    public String getCitypayForLifeExcStatusInfo() {
        return citypayForLifeExcStatusInfo;
    }

    public void setCitypayForLifeExcStatusInfo(String citypayForLifeExcStatusInfo) {
        this.citypayForLifeExcStatusInfo = citypayForLifeExcStatusInfo;
    }

    public Button getGetStatusLifeExc() {
        return getStatusLifeExc;
    }

    public void setGetStatusLifeExc(Button getStatusLifeExc) {
        this.getStatusLifeExc = getStatusLifeExc;
    }
}
