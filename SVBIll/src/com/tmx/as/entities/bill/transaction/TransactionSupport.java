package com.tmx.as.entities.bill.transaction;

import com.tmx.as.base.BasicEntity;
import com.tmx.as.entities.general.user.User;

import java.io.Serializable;

/**

 */
public class TransactionSupport extends BasicEntity{
    private Long transactionSupportId;
    private String description;
    private Long limitedByRestrictionId;
    private String limitedByRestrictionType;
    private String paymentReason;
    
    private User userActor;//user

    //-------------Properties
    public Long getTransactionSupportId() {
        return transactionSupportId;
    }

    public void setTransactionSupportId(Long transactionSupportId) {
        this.transactionSupportId = transactionSupportId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getLimitedByRestrictionId() {
        return limitedByRestrictionId;
    }

    public void setLimitedByRestrictionId(Long limitedByRestrictionId) {
        this.limitedByRestrictionId = limitedByRestrictionId;
    }

    public String getLimitedByRestrictionType() {
        return limitedByRestrictionType;
    }

    public void setLimitedByRestrictionType(String limitedByRestrictionType) {
        this.limitedByRestrictionType = limitedByRestrictionType;
    }

    public Serializable getId() {
        return getTransactionSupportId();
    }

    public int getValid() {
        return valid;
    }

    public void setValid(int valid) {
        this.valid = valid;
    }

    public User getUserActor() {
        return userActor;
    }

    public void setUserActor(User userActor) {
        this.userActor = userActor;
    }

    public String getPaymentReason() {
        return paymentReason;
    }

    public void setPaymentReason(String paymentReason) {
        this.paymentReason = paymentReason;
    }
}
