package com.tmx.engines.reportengine.aggrtable;

import com.tmx.engines.reportengine.aggrtable.validation.ValidationRule;
import com.tmx.engines.reportengine.aggrtable.validation.DataValidationError;
import java.util.*;

/**
 */
public class Data {
    /** Map of facts: [string of int factIdx (order in table)][Object[][] data].
     * Then data is organized like:
     * [rowIdx][columnIdx]
     *
     *  */
    private Map facts = null;
    /** Map of facts: [Integer factIdx (order in table)][String factName] */
    TreeMap factsNames = null;

    /** Count of data rows */
    public int getRowsCount(){
        if(facts == null)
            return 0;
        else if(facts.get("0") == null)
            return 0;
        else{
            //use first fact data to evaluate rows count
            Wrapped2DArray factData = ((Wrapped2DArray)facts.get("0"));
            return factData.getRowsCount();
        }
    }

    /** Count of data cols */
    public int getColsCount(){
        if(facts == null)
            return 0;
        else if(facts.get("0") == null)
            return 0;
        else{
            //use first fact data to evaluate rows count
            Wrapped2DArray factData = ((Wrapped2DArray)facts.get("0"));
            return factData.getColsCount();
        }
    }

    /** Returns list of DataElemant */
    public List getRow(int factIdx, int rowNumber) throws com.tmx.engines.reportengine.base.ReportException {
        String factIdxStr = String.valueOf(factIdx);
        Wrapped2DArray factData = (Wrapped2DArray)facts.get(factIdxStr);
        if(factData == null)
            throw new com.tmx.engines.reportengine.base.ReportException("err.report_engine.aggr_table.uknown_fact_idx", new String[]{factIdxStr});
        return Arrays.asList(factData.getRow(rowNumber));
    }

    /** returns fact names map */
    public Map getFactNames(){
        return factsNames;
    }

    public int getFactCount(){
        if(factsNames == null)
            return 0;
        return factsNames.keySet().size();
    }

    //---------package local methods
    /** @param mappingRules map of AggregatedTable.MappingRule
     *  @param tableSize    table size */
    Data(Map mappingRules, int tableSize){
        facts = new HashMap();
        factsNames = new TreeMap(new IntIdxComparator());
        for(Iterator iter = mappingRules.values().iterator(); iter.hasNext();){
            AggregatedTable.MappingRule rule = (AggregatedTable.MappingRule)iter.next();
            if(AggregatedTable.MappingRule.Type.DATA.equals(rule.getType())){
                Wrapped2DArray data = new Wrapped2DArray(tableSize, tableSize);
                facts.put(String.valueOf(rule.getDestIdx()), data);
                factsNames.put(rule.getDestIdx(), rule.getName());
            }
        }
    }

    //put data into fact matrix. Initially this is diagonal matrix.
    void put(int factIdx, int rowIdx, Object srcValue) throws com.tmx.engines.reportengine.base.ReportException {
        Wrapped2DArray data = (Wrapped2DArray)facts.get(String.valueOf(factIdx));
        if(data.get(rowIdx, rowIdx) != null)
            throw new com.tmx.engines.reportengine.base.ReportException("err.report_engine.aggr_table.trying_to_overwrite_data", new String[]{String.valueOf(factIdx), String.valueOf(rowIdx), String.valueOf(data.get(rowIdx, rowIdx)), String.valueOf(srcValue)});
        data.set(rowIdx, rowIdx, new DataElement(srcValue));
    }

    /** aggregate 2 given columns of all facts: right column aggregated into left.
     * After aggregation the columns counts are decremented
     * @param leftColIdx index of left aggregated column.
     * @param rightColIdx index of left aggregated column. */
    void aggregateColumns(int leftColIdx, int rightColIdx) throws com.tmx.engines.reportengine.base.ReportException {
        for(Iterator iter = facts.keySet().iterator(); iter.hasNext();){
            String factIdx = (String)iter.next();
            Wrapped2DArray currentFactData = (Wrapped2DArray)facts.get(factIdx);
            //-----------------------------------------
            //aggregate
            for(int rowIdx = 0; rowIdx<currentFactData.getRowsCount(); rowIdx++){
                if(currentFactData.get(rowIdx, leftColIdx) != null && currentFactData.get(rowIdx, rightColIdx) != null){
                    throw new com.tmx.engines.reportengine.base.ReportException("err.report_engine.aggr_table.aggregation_conflict", new String[]{String.valueOf(rowIdx), String.valueOf(leftColIdx), String.valueOf(rightColIdx)});
                }
                else if(currentFactData.get(rowIdx, leftColIdx) != null){
                    //actualy do nothig. Data already int the its right cell
                    //currentFactData.set(rowIdx, leftColIdx, currentFactData.get(rowIdx, leftColIdx));
                }
                else if(currentFactData.get(rowIdx, rightColIdx) != null){
                    currentFactData.move(rowIdx, rightColIdx, rowIdx, leftColIdx);
                }
            }            
            currentFactData.deleteColumn(rightColIdx);//remove aggregated column
            //----------------------------------------
//            //crete new array with decremented columns
//            Wrapped2DArray newFactData = new Wrapped2DArray(currentFactData.getRowsCount(), currentFactData.getColsCount()-1);//data is rectangular matrix, so use 0 (or any) row to get cols count
//            //copy values
//            int destColIdx = 0;
//            for(int colIdx = 0; colIdx<currentFactData.getColsCount(); colIdx++){
//                if(colIdx == leftColIdx){
//                    //aggregate
//                    for(int rowIdx = 0; rowIdx<currentFactData.getRowsCount(); rowIdx++){
//                        if(currentFactData.get(rowIdx, leftColIdx) != null && currentFactData.get(rowIdx, rightColIdx) != null){
//                            throw new com.tmx.engines.reportengine.base.ReportException("err.report_engine.aggr_table.aggregation_conflict", new String[]{String.valueOf(rowIdx), String.valueOf(colIdx)});
//                        }
//                        else if(currentFactData.get(rowIdx, leftColIdx) != null){
//                            newFactData.set(rowIdx, destColIdx, currentFactData.get(rowIdx, leftColIdx));
//                        }
//                        else if(currentFactData.get(rowIdx, rightColIdx) != null){
//                            newFactData.set(rowIdx, destColIdx, currentFactData.get(rowIdx, rightColIdx));
//                        }
//                    }
//                    destColIdx++;
//                }
//                else if(colIdx == rightColIdx){
//                    //ignore column
//                }
//                else{
//                    //just copy column
//                    for(int rowIdx = 0; rowIdx<currentFactData.getRowsCount(); rowIdx++){
//                        newFactData.set(rowIdx, destColIdx, currentFactData.get(rowIdx, colIdx));
//                    }
//                    destColIdx++;
//                }
//            }
//            //save aggregated data
//            facts.put(factIdx, newFactData);
        }
    }

    /** aggregate neighbour columns of all facts: high is identified by given parameter.
     * After aggregation rows counts is decremented
     * @param highRowIdx index of high aggregated tow. */
    void aggregateRows(int highRowIdx, int lowRowIdx) throws com.tmx.engines.reportengine.base.ReportException {
        for(Iterator iter = facts.keySet().iterator(); iter.hasNext();){
            String factIdx = (String)iter.next();
            Wrapped2DArray currentFactData = (Wrapped2DArray)facts.get(factIdx);
            //------------------------------------------------------
            //aggregate
            for(int colIdx = 0; colIdx<currentFactData.getColsCount(); colIdx++){
                if(currentFactData.get(highRowIdx, colIdx) != null && currentFactData.get(lowRowIdx, colIdx) != null){
                    throw new com.tmx.engines.reportengine.base.ReportException("err.report_engine.aggr_table.aggregation_conflict", new String[]{String.valueOf(highRowIdx), String.valueOf(lowRowIdx), String.valueOf(colIdx)});
                }
                else if(currentFactData.get(highRowIdx, colIdx) != null){
                    //actualy do nothig. Data already int the its right cell
                    //currentFactData.set(highRowIdx, colIdx, currentFactData.get(highRowIdx, colIdx));
                }
                else if(currentFactData.get(lowRowIdx, colIdx) != null){
                    currentFactData.move(lowRowIdx, colIdx, highRowIdx, colIdx);
                }
            }
            currentFactData.deleteRow(lowRowIdx);//remove aggregated row
            //------------------------------------------------------
//            //crete new array with decremented rows
//            Wrapped2DArray newFactData = new Wrapped2DArray(currentFactData.getRowsCount()-1, currentFactData.getColsCount());//data is rectangular matrix, so use 0 (or any) column to get rows count
//            //copy values
//            int destRowIdx = 0;
//            for(int rowIdx = 0; rowIdx<currentFactData.getRowsCount(); rowIdx++){
//                if(rowIdx == highRowIdx){
//                    //aggregate
//                    for(int colIdx = 0; colIdx<currentFactData.getColsCount(); colIdx++){
//                        if(currentFactData.get(highRowIdx, colIdx) != null && currentFactData.get(lowRowIdx, colIdx) != null){
//                            throw new com.tmx.engines.reportengine.base.ReportException("err.report_engine.aggr_table.aggregation_conflict", new String[]{String.valueOf(rowIdx), String.valueOf(colIdx)});
//                        }
//                        else if(currentFactData.get(highRowIdx, colIdx) != null){
//                            newFactData.set(destRowIdx, colIdx, currentFactData.get(highRowIdx, colIdx));
//                        }
//                        else if(currentFactData.get(lowRowIdx, colIdx) != null){
//                            newFactData.set(destRowIdx, colIdx, currentFactData.get(lowRowIdx, colIdx));
//                        }
//                    }
//                    destRowIdx++;
//                }
//                else if(rowIdx == lowRowIdx){
//                    //ignore row
//                }
//                else{
//                    //just copy row
//                    for(int colIdx = 0; colIdx < currentFactData.getColsCount(); colIdx++){
//                        newFactData.set(destRowIdx, colIdx, currentFactData.get(rowIdx, colIdx));
//                    }
//                    destRowIdx++;
//                }
//            }
//            //save aggregated data
//            facts.put(factIdx, newFactData);
        }
    }

    /** Sort all fact datas, using header indexes before sorting */
    void sort(Integer[] sortedColIdx, Integer[] sortedRowIdx){
        for(Iterator iter = facts.keySet().iterator(); iter.hasNext();){
            String factId = (String)iter.next();
            Wrapped2DArray data = (Wrapped2DArray)facts.get(factId);
            if(data.getColsCount() == 0)
                return;//nothing to sort
            Wrapped2DArray sortedData = new Wrapped2DArray(data.getRowsCount(), data.getColsCount());

            for(int colIdx=0; colIdx<data.getColsCount(); colIdx++){
                for(int rowIdx=0; rowIdx<data.getRowsCount(); rowIdx++){
                    sortedData.set(rowIdx, colIdx, data.get(sortedRowIdx[rowIdx].intValue(), sortedColIdx[colIdx].intValue()));
                    //sortedData[sortedRowIdx[rowIdx].intValue()][sortedColIdx[colIdx].intValue()] = data[rowIdx][colIdx];
                }
            }
            facts.put(factId, sortedData);
        }
    }

    /** Return data array of requested Fact Function
     * Uses only to evalue AggrFunctions.
     *  */
    Wrapped2DArray getFactData(int factIdx){
        return (Wrapped2DArray)facts.get(String.valueOf(factIdx));
    }

    private String getFactName(int factIdx){
        return (String)factsNames.get(new Integer(factIdx));
    }

    /** Validate all data cells according to validation rules, specified for table.
     * @param validationRules List of fact's data validation rules ordered by runNumber
     */
    void validate(ValidationRule[] validationRules){
        if(validationRules == null || validationRules.length <= 0)
            return;//no validation rules
        
        //get data cols and rows count
        Wrapped2DArray data = getFactData(0);
        if(data == null)
            return;//nothing to validate
        int colCnt = data.getColsCount();
        int rowCnt = data.getRowsCount();
        int factCnt = getFactCount();

        //prepare fact id resolver
        Map factIdResolver = new HashMap();
        for(Iterator iter = factsNames.keySet().iterator(); iter.hasNext();){
            Integer factIdx = (Integer)iter.next();
            factIdResolver.put(factsNames.get(factIdx), factIdx);
        }

        for(int colIdx=0; colIdx < colCnt; colIdx++){
            for(int rowIdx=0; rowIdx < rowCnt; rowIdx++){
                //1. collect all fact values
                Map metacubePoint = new HashMap();
                for(int factIdx=0; factIdx < factCnt; factIdx++)
                    metacubePoint.put(getFactName(factIdx), getFactData(factIdx).get(rowIdx, colIdx));

                for(int ruleId = 0; ruleId < validationRules.length; ruleId++){
                    DataValidationError error = validationRules[ruleId].validate(metacubePoint);
                    if(error != null){
                        DataElement dataElement = ((DataElement)getFactData(((Integer)(factIdResolver.get(validationRules[ruleId].getOwnerFactName()))).intValue()).get(rowIdx, colIdx));
                        if(dataElement == null){
                            dataElement = new DataElement(null);
                            getFactData(((Integer)(factIdResolver.get(validationRules[ruleId].getOwnerFactName()))).intValue()).set(rowIdx, colIdx, dataElement);
                        }
                        dataElement.validationError = error;//assign validation error
                    }
                }
            }
        }
    }
}
