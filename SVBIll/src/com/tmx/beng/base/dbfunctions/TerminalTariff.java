package com.tmx.beng.base.dbfunctions;

import com.tmx.beng.base.cache.InvokeFunctionException;
import com.tmx.beng.base.BillingMessage;
import com.tmx.beng.base.Reservation;
import com.tmx.as.entities.bill.transaction.TransactionType;

import java.util.Map;
import java.util.List;
import java.util.ArrayList;

/**

 */
public class TerminalTariff extends BasicTariffFunction {
    /** Transaction cost */
    public static final String TX_COST = "tx_cost";
    /** Terminal discount */
    public static final String DISCOUNT_PERCENT = "discount_percent";

    boolean isTariffApplicable(Map actualParams) {
        return BillingMessage.O_REFILL_PAYMENT.equals(task.getProcesedMessage().getOperationName());
    }

    void applyTariff(Map actualParams, Reservation reservation) throws InvokeFunctionException {
        try{
            double txCost = Double.parseDouble((String)actualParams.get(TX_COST));
            double discontPercent = Double.parseDouble((String)actualParams.get(DISCOUNT_PERCENT));

            double amount = Double.parseDouble(task.getProcesedMessage().getAttributeString(BillingMessage.AMOUNT));
            double tariffedAmount = (amount+txCost)*discontPercent;
            reservation.reserve(null, tariffedAmount, null, TransactionType.CODE_CREDIT);
        }
        catch(Throwable e){
            throw new InvokeFunctionException("err.terminal_tariff_calc_error", e);
        }
    }

    public ValidationError[] validate(Map actualParams) {
        List resultErrors = new ArrayList();

        resultErrors.addAll(ParameterValidator.validateParamNames(actualParams, new String[] {TX_COST, DISCOUNT_PERCENT}));
        resultErrors.addAll(ParameterValidator.validateTxCost(actualParams, new String[] {TX_COST}));
        resultErrors.addAll(ParameterValidator.validateDiscountPercent(actualParams, new String[] {DISCOUNT_PERCENT}));

        return (ValidationError[]) resultErrors.toArray(new ValidationError[resultErrors.size()]);
    }

    public TariffParameter[] getDefaultParams() {
    	TariffParameter paramCost = new TariffParameter();
    	paramCost.setName(TX_COST);
    	paramCost.setValue("0");
        paramCost.setOrderNum(new Long(1));
        paramCost.setMandatory(Boolean.TRUE);

        TariffParameter paramDisPer = new TariffParameter();
    	paramDisPer.setName(DISCOUNT_PERCENT);
    	paramDisPer.setValue("1");
        paramDisPer.setOrderNum(new Long(2));
        paramDisPer.setMandatory(Boolean.FALSE);
        return new TariffParameter[] {paramCost, paramDisPer};
    }
}
