package com.tmx.as.resolver.functions;

import com.tmx.as.resolver.FunctionSyntaxException;

public interface Function {
    public void set(int paramIndex,String paramName,Object paramValue) throws FunctionSyntaxException;
    public Object returnValue() throws FunctionSyntaxException;
}

