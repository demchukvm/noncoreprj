package com.tmx.as.entities.mobipay.kyivstar;

import com.tmx.as.base.BasicEntity;
import com.tmx.as.entities.mobipay.kyivstar.MobipayKyivstar;

import java.io.Serializable;

/**
 */
public class MobipayKyivstarAbonentInfo extends BasicEntity {
    private Long abonentInfoId;
    private String name;
    private Double balance;
    private MobipayKyivstar mobipayKyivstar;

    public MobipayKyivstar getMobipayKyivstar() {
        return mobipayKyivstar;
    }

    public void setMobipayKyivstar(MobipayKyivstar mobipayKyivstar) {
        this.mobipayKyivstar = mobipayKyivstar;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getBalance() {
        return balance;
    }

    public void setBalance(Double balance) {
        this.balance = balance;
    }

    public Long getAbonentInfoId() {
        return abonentInfoId;
    }

    public void setAbonentInfoId(Long abonentInfoId) {
        this.abonentInfoId = abonentInfoId;
    }

    public Serializable getId() {
        return getAbonentInfoId();
    }

    public int getValid() {
        return valid;
    }

    public void setValid(int valid) {
        this.valid = valid;
    }
}
