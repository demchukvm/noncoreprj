package com.tmx.beng.iso8583Gate.pdu.substructure;

import com.tmx.beng.iso8583Gate.pdu.RuntimeDataElement;

/**

 */
public interface RuntimeSubDataElement {
    public RuntimeDataElement getOwnerRDE();
    public void setOwnerRDE(RuntimeDataElement ownerRDE);    
    public void addValue(String name, String value);
    public String getValue(String name);
}
