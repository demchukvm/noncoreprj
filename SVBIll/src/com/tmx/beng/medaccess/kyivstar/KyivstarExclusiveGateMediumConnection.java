package com.tmx.beng.medaccess.kyivstar;

import com.tmx.beng.base.BillingMessage;
import com.tmx.beng.httpsgate.HttpsGateException;
import com.tmx.beng.httpsgate.kyivstar.KyivstarExclusiveGate;
import com.tmx.beng.medaccess.MediumConnection;

/**
    Medium connection to work with Mobipay Kyivstar Exclusive gate
 */
public class KyivstarExclusiveGateMediumConnection implements MediumConnection {
    private KyivstarExclusiveGate kyivstarGate = null;

    KyivstarExclusiveGateMediumConnection() throws HttpsGateException {
        kyivstarGate = KyivstarExclusiveGate.getInstance();
    }

    public void send(BillingMessage msg) {
        kyivstarGate.enqueueBillMessage(msg);
    }
}
