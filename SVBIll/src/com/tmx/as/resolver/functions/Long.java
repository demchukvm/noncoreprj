package com.tmx.as.resolver.functions;

import com.tmx.as.resolver.FunctionSyntaxException;

public class Long implements Function {
    private java.lang.Long value;
    public void set(int paramIndex, String paramName, Object paramValue) throws FunctionSyntaxException {
        value=new java.lang.Long((String)paramValue);
    }

    public Object returnValue() throws FunctionSyntaxException {
        return value;
    }
}
