package com.tmx.beng.access;

import com.tmx.as.exceptions.DatabaseException;
import com.tmx.beng.base.BillException;
import com.tmx.beng.base.MediumAccessPool;
import com.tmx.util.cache.CacheException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;

/**
 * For RMI connection, for utils action on separated web server!
 *
 * @author Andrey Nagorniy
 */
public interface GateConnection {

    public Set<String> getMediumAccessPoolKeys() throws BillException;

    public Set<String> getRemovedElemetsKeys() throws BillException;

    public void removeElements(Set<String> keys) throws BillException;

    public void restoreElements (Set<String> keys) throws BillException;

    public HashMap getMediumMap() throws BillException;

    public HashMap getMediumAccessName() throws BillException;

    public void changeMediumResolver(String productName, String gateName);

    public ArrayList getTerminalList(String inChannelName, String outChannelName) throws CacheException;

    public void loadBeelineExc() throws DatabaseException;

    public void loadKyivstarExc() throws DatabaseException;

    public void loadLifeExc() throws DatabaseException;

    public void loadMtsExc() throws DatabaseException;
    
}
