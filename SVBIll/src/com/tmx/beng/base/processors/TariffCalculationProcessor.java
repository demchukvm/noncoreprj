package com.tmx.beng.base.processors;

import com.tmx.as.entities.bill.operator.Operator;
import com.tmx.as.entities.bill.seller.Seller;
import com.tmx.as.entities.bill.terminal.Terminal;
import com.tmx.as.entities.bill.terminal_group.TerminalGroup;
import com.tmx.as.entities.bill.transaction.TransactionType;
import com.tmx.beng.base.*;
import com.tmx.beng.base.cache.DBCache;
import com.tmx.beng.base.cache.FunctionWrapper;
import com.tmx.beng.base.cache.InvokeFunctionException;
import com.tmx.beng.base.cache.DBCacheException;
import com.tmx.beng.base.dbfunctions.BasicTariffFunction;
import com.tmx.beng.base.dbfunctions.LinkedTerminalSellerTariff;

import java.util.*;

/**
 */
public class TariffCalculationProcessor extends TaskProcessor {

    public Task process0(Task task, DBCache dbCache, BillingMessageWritable msg) throws BillException, DBCacheException {
        logger.debug("Calculating tariff...");
        long strTime = System.currentTimeMillis();
        try{
            System.out.println("TariffCalculationProcessor.process0:: time: " + (System.currentTimeMillis() - strTime));
            // terminal tariff
            final String terminalSn = msg.getAttributeString(BillingMessage.TERMINAL_SN);
            if (dbCache.getTerminalBalance(terminalSn, null) == null) {
                throw new BillException("err.no_primaryBalance_for_terminal " + terminalSn);
            }
            final String terminalPrimaryBalanceCode = dbCache.getTerminalBalance(terminalSn, null).getCode();
            FunctionWrapper[] functions = dbCache.getActiveTerminalTariffs(terminalSn);
            FunctionWrapper[] linkedFunctions = new FunctionWrapper[1];
            boolean isHaveLinkedTariffs = false;
            //loking for LinkedTariff
            for (int i = 0; i < functions.length; i++) {
                FunctionWrapper fw = functions[i];
                if (fw.getObjectInstance() instanceof LinkedTerminalSellerTariff) {         
                    linkedFunctions[0] = functions[i]; 
                    isHaveLinkedTariffs = true;
                }
            }

            if(functions != null && !isHaveLinkedTariffs){
                calculateTariffs0(functions, task, msg, Terminal.class, terminalSn, terminalPrimaryBalanceCode);
            }
            else if (!isHaveLinkedTariffs) {
                // terminal group tariff
                //todo use terminal group code and terminal group primary balance
                functions = dbCache.getActiveTerminalGroupsTariffs(msg.getAttributeString(terminalSn));
                if(functions == null)
                    throw new BillException("err.tariff_calc_proc.tariff_function_not_found_for_terminal_or_group", new String[]{terminalSn});
                calculateTariffs0(functions, task, msg, TerminalGroup.class, terminalSn, terminalPrimaryBalanceCode);
            }

            // seller tariff
            String sellerCode = msg.getAttributeString(BillingMessage.SELLER_CODE);
            List sellersParentsChainCodes = dbCache.getSellerWrapperParentsChainCodes(sellerCode);
            for(Iterator sellersIter = sellersParentsChainCodes.iterator(); sellersIter.hasNext();){
                final String nextSellerCode = (String)sellersIter.next();
                final String nextSellerPrimaryBalanceCode = dbCache.getSellerBalance(nextSellerCode, null).getCode();
                functions = dbCache.getActiveSellerTariffs(nextSellerCode);
                if(functions == null)
                    throw new BillException("err.tariff_calc_proc.tariff_function_not_found_for_seller", new String[]{nextSellerCode});
                calculateTariffs0(functions, task, msg, Seller.class, nextSellerCode, nextSellerPrimaryBalanceCode);
            }

            // operator tariff
            final String operatorCode = msg.getAttributeString(BillingMessage.SERVICE_CODE);
            final String operatorPrimaryBalanceCode = dbCache.getOperatorBalance(operatorCode, null).getCode();
            functions = dbCache.getActiveOperatorTariffs(operatorCode);
            if(functions == null)
                throw new BillException("err.tariff_calc_proc.tariff_function_not_found_for_operator", new String[]{operatorCode});
            calculateTariffs0(functions, task, msg, Operator.class, operatorCode, operatorPrimaryBalanceCode);

            if (isHaveLinkedTariffs) {
                calculateTariffs0(linkedFunctions, task, msg, Terminal.class, terminalSn, terminalPrimaryBalanceCode);
            }
        }
        catch (DBCacheException e) {
            task.getProcesedMessage().setStatusCode(StatusDictionary.TARIFF_ERROR);
            logger.error(e.getLocalizedMessage(), e);

            throw new DBCacheException(e.getTraceDump());//"err.tariff_calc_proc.unexpected_error", e);

        } catch (BillException e) {
            logger.error(e.getLocalizedMessage(), e);
            throw e;
        } catch (NullPointerException e) {
            logger.error(e.getLocalizedMessage(), e);
            throw new BillException("err.tariff_calc_proc.null_pointer", e);
        } catch(Throwable e){
            task.getProcesedMessage().setStatusCode(StatusDictionary.TARIFF_ERROR);
            logger.error(e.getLocalizedMessage(), e);
            throw new BillException("err.tariff_calc_proc.unexpected_error", e);           
        }
        return task;
    }

    private void calculateTariffs0(FunctionWrapper[] tariffs,
                                   Task task,
                                   BillingMessageWritable msg,
                                   Class tariffOwnerType,
                                   String tariffOwnerCode,
                                   String tariffOwnerPrimaryBalanceCode) throws BillException{
        boolean tariffWasApplied = false;
        for (int i = 0; i < tariffs.length; i++) {
            if (tariffs[i] != null) {
                calculateTariff0(tariffs[i], task, msg, tariffOwnerType, tariffOwnerCode,
                                            tariffOwnerPrimaryBalanceCode);
            }
            boolean thisTariffApplied = isTariffApplied(msg);
            if (thisTariffApplied && tariffWasApplied)
                throw new BillException("err.tariff_calc_proc.tariff_already_applied",
                        new String[]{tariffs[i].getObjectInstance().getClass().getName(), (String)msg.getAttribute(BillingMessage.SERVICE_CODE)});
            tariffWasApplied = tariffWasApplied || thisTariffApplied;
            resetTariffApplicationStatus(msg);
        }       
        if (!tariffWasApplied)
            throw new BillException("err.tariff_calc_proc.no_tariff_applied:", new String[]{tariffOwnerCode, (String)msg.getAttribute(BillingMessage.SERVICE_CODE)});
    }

    private void calculateTariff0(FunctionWrapper functionWrapper,
                                  Task task,
                                  BillingMessageWritable msg,
                                  Class tariffOwnerType,
                                  String tariffOwnerCode,
                                  String tariffOwnerPrimaryBalanceCode) throws BillException {
        try{
            ((BasicTariffFunction)functionWrapper.getObjectInstance()).init(task);
            Map runtimeParams = new HashMap();
            runtimeParams.put(BasicTariffFunction.TARIFF_OWNER_TYPE, tariffOwnerType);
            runtimeParams.put(BasicTariffFunction.TARIFF_OWNER_CODE, tariffOwnerCode);
            runtimeParams.put(BasicTariffFunction.TARIFF_OWNER_PRIMARY_BALANCE_CODE, tariffOwnerPrimaryBalanceCode);
            functionWrapper.invoke(runtimeParams);
        }
        catch(InvokeFunctionException e){
            e.printStackTrace();
            throw new BillException("err.tariff_calc_proc.teriff_calculation_failed " + "tariff owner " + tariffOwnerCode, e);
        }
    }

    private boolean isTariffApplied(BillingMessage msg){
        Object status = msg.getAttribute(BasicTariffFunction.TARIFF_APPLIED);
        return status != null && Boolean.TRUE.equals(status);
    }

    private void resetTariffApplicationStatus(BillingMessageWritable msg){
        msg.setAttribute(BasicTariffFunction.TARIFF_APPLIED, Boolean.FALSE);
    }
}