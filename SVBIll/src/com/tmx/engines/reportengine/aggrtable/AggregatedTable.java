package com.tmx.engines.reportengine.aggrtable;

import com.tmx.engines.reportengine.base.*;
import com.tmx.engines.reportengine.aggrtable.beans.*;
import com.tmx.engines.reportengine.aggrtable.validation.ValidationRuleFactory;
import com.tmx.engines.reportengine.aggrtable.validation.ValidationRule;
import com.tmx.util.InitException;
import java.util.*;

/**
 */
public class AggregatedTable {
    //report config
    RootDocument configRootDoc = null;
    //mapping rules
    Map mappingRules = null;
    //Aggr function metadatas
    Set aggrFunctionMetadatas = null;
    //Fact function data validation rules
    ValidationRule[] dataValidationRules = null;
    static final String RESOURCE_BUNDLE = "aggr_table";

    private HeaderSet columnHeaderSet = null;
    private HeaderSet rowHeaderSet = null;
    private Data data = null;
    private AggrFunctionSet columnAggrFunctionSet = null;
    private AggrFunctionSet rowAggrFunctionSet = null;
    /** use this flag to identify is data was loaded */
    private boolean isLoaded = false;

    //package local constructor. Use AggrTableService to get AggregatedTable  instance
    AggregatedTable(){
    }

    public HeaderSet getColumnHeaderSet(){
        return columnHeaderSet;
    }

    public HeaderSet getRowHeaderSet(){
        return rowHeaderSet;
    }

    public AggrFunctionSet getColumnAggrFunctionSet() {
        return columnAggrFunctionSet;
    }

    public AggrFunctionSet getRowAggrFunctionSet() {
        return rowAggrFunctionSet;
    }

    public int getFirstCellColspan(){
        //add possible correction for fact header
        return rowHeaderSet.getRowsCount() + (("row".equals(configRootDoc.getRoot().getLookAndFeel().getFactHeader().getPosition())) ? 1 : 0);
    }

    public int getFirstCellRowspan(){
        //add possible correction for fact header
        return columnHeaderSet.getRowsCount() + (("column".equals(configRootDoc.getRoot().getLookAndFeel().getFactHeader().getPosition())) ? 1 : 0);
    }

    public Data getData(){
        return data;
    }

    public boolean isLoaded() {
        return isLoaded;
    }


    public void init(AggrTableService.AggrTableConfig config) throws InitException, ReportException{
        //copy config into internal definition
        configRootDoc = (RootDocument)config.getRootDoc();


        //1. wraps mapping rules from config into internal class MappingRule
        MappingRules.Rule[] mappRules = configRootDoc.getRoot().getMappingRules().getRuleArray();
        if(mappRules == null)
            throw new ReportException("err.report_engine.aggr_table.null_mapping_rules");
        Set rulesSet = new HashSet();
        for(int i =0; i <mappRules.length; i++){
            MappingRule mRule = new MappingRule();
            mRule.setSrcLabelIdx(new Integer(mappRules[i].getSrcLabelIdx()));
            if(mappRules[i].isSetSrcLabelOrderIdx()){
                mRule.setSrcLabelOrderIdx(new Integer(mappRules[i].getSrcLabelOrderIdx()));
            }
            mRule.setName(mappRules[i].getName());
            mRule.setType(mappRules[i].getType());
            mRule.setDestIdx(new Integer(mappRules[i].getDestIdx()));
            rulesSet.add(mRule);
        }

        mappingRules = validate(rulesSet);


        //2. use aggr function rules from config to initialize AggrFunctions
        AggregateFunctions.Function[] functions = configRootDoc.getRoot().getAggregateFunctions().getFunctionArray();
        if(functions != null){
            int columnLevelsCount = getLevelCount(rulesSet, MappingRule.Type.COLUMN_HEADER);
            int rowLevelsCount = getLevelCount(rulesSet, MappingRule.Type.ROW_HEADER);
            aggrFunctionMetadatas = AggrFunction.init(functions, columnLevelsCount, rowLevelsCount);
        }

        //3. Prepare data validation rules based on config
        DataValidation.ValidationRule[] validationRules = configRootDoc.getRoot().getDataValidation() != null ? configRootDoc.getRoot().getDataValidation().getValidationRuleArray() : null;
        dataValidationRules = ValidationRuleFactory.instantiateRules(validationRules);
    }


    /** @param table List of Object[] - list of data and headers.
     *
     * Examplle:
     *
     * [src_idx=0,type=verticalHeader,name='Years',dest_idx=0];
     * [src_idx=1,type=verticalHeader,name='Quartals',dest_idx=1];
     * [src_idx=2,type=horizontalHeader,name='Roulettes',dest_idx=0];
     * [src_idx=3,type=horizontalHeader,name='Terminals',dest_idx=1];
     * [src_idx=4,type=data,name='KeyIn',dest_idx=1];
     * [src_idx=5,type=data,name='KeyOut',dest_idx=2]
     *
     * idx=0..n - index of column in given table List. Should not be duplicated.
     * type=verticalHeader|horizontalHeader|data
     * name=any_string - Name of given column to be used in consolidated table
     * dest_idx=0..m - for header - level of heder in headers aggregation. 0 is most general level;
     *                 for data - order of data columns in aggregaed table. 0 is most left column.
     * */
    public void reload(List table) throws ReportException {
long time = _printElapsedTime(System.currentTimeMillis(), "start");
        //initialize headers and data containers
        HeaderSet localColumnHeaderSet = new HeaderSet(mappingRules, MappingRule.Type.COLUMN_HEADER, this);
        HeaderSet localRowHeaderSet = new HeaderSet(mappingRules, MappingRule.Type.ROW_HEADER, this);
        Data localData = new Data(mappingRules, table.size());
        AggrFunctionSet localColumnAggrFunctionSet = new AggrFunctionSet(aggrFunctionMetadatas, AggrFunction.Metadata.Position.COLUMN);
        AggrFunctionSet localRowAggrFunctionSet = new AggrFunctionSet(aggrFunctionMetadatas, AggrFunction.Metadata.Position.ROW);
time = _printElapsedTime(time, "initialize headers and data containers");

        //1. fill aggregated table
        for(int rowIdx =0; rowIdx<table.size(); rowIdx++){
            Object[] row = (Object[])table.get(rowIdx);
            for(int colIdx=0; colIdx <row.length; colIdx++){
                MappingRule rule = (MappingRule)mappingRules.get(String.valueOf(colIdx));
                if(rule == null){
                    continue;/** ignore this column it doesn't mapped*/
                }
                else if(MappingRule.Type.COLUMN_HEADER.equals(rule.getType())){
                    localColumnHeaderSet.put(rule, rowIdx, row);
                }
                else if(MappingRule.Type.ROW_HEADER.equals(rule.getType())){
                    localRowHeaderSet.put(rule, rowIdx, row);
                }
                else if(MappingRule.Type.DATA.equals(rule.getType())){
                    localData.put(rule.getDestIdx().intValue(), rowIdx, row[colIdx]);
                }
                else{
                    /** ignore */
                }
            }
        }
time = _printElapsedTime(time, "1. fill aggregated table");
        //2.1 aggregate by rows
        localRowHeaderSet.aggregate(localData);
time = _printElapsedTime(time, "2.1 aggregate by rows");
        //2.2 aggregate by columns
        localColumnHeaderSet.aggregate(localData);
time = _printElapsedTime(time, "2.2 aggregate by columns");
        //3. validate data
        localData.validate(dataValidationRules);
time = _printElapsedTime(time, "3. validate data");
        //4. sort headers
        sort(localColumnHeaderSet, localRowHeaderSet, localData);
time = _printElapsedTime(time, "4. sort headers");
        //5.1 aggregate headers
        localColumnHeaderSet.aggregateHeaders();
time = _printElapsedTime(time, "5.1 aggregate headers");
        //5.2 aggregate headers
        localRowHeaderSet.aggregateHeaders();
time = _printElapsedTime(time, "5.2 aggregate headers");
        //6.1 evaluate aggr functions
        localColumnAggrFunctionSet.evaluateFunctions(localColumnHeaderSet, localData);
time = _printElapsedTime(time, "6.1 evaluate aggr functions");
        //6.2 evaluate aggr functions
        localRowAggrFunctionSet.evaluateFunctions(localRowHeaderSet, localData);
time = _printElapsedTime(time, "6.2 evaluate aggr functions");
        //7. apply look and feel
        applyLookAndFeel(localColumnHeaderSet, localRowHeaderSet, localData);
time = _printElapsedTime(time, "7. apply look and feel");
time = _printElapsedTime(time, "finish");        
        //set loaded data into table
        columnHeaderSet = localColumnHeaderSet;
        rowHeaderSet = localRowHeaderSet;
        data = localData;
        columnAggrFunctionSet = localColumnAggrFunctionSet;
        rowAggrFunctionSet = localRowAggrFunctionSet;
        //data was successfully reloaded
        isLoaded = true;
    }

    public LookAndFeel getLookAndFeel(){
        return configRootDoc.getRoot().getLookAndFeel();
    }

//RM: obselete code
//
//    private Set parseMappingRules(String mappingRules) throws ReportException {
//        if (mappingRules == null)
//            throw new ReportException("err.report_engine.aggr_table.null_mapping_rules");
//        Set rules = new HashSet();
//
//        try {
//            final Pattern rulePattern = Pattern.compile("\\[src_idx=([0-9]+),type=("+MappingRule.Type.COLUMN_HEADER.toString()+"|"+MappingRule.Type.ROW_HEADER.toString()+"|"+MappingRule.Type.DATA.toString()+"),name='(.+)',dest_idx=([0-9]+)\\]");
//            String[] ruleString = mappingRules.split(";[ ]*[\\n]*");
//            for (int i = 0; i < ruleString.length; i++) {
//                //parse rule
//                Matcher matcher = rulePattern.matcher(ruleString[i]);
//                if (matcher.matches()) {
//                    MappingRule rule = new MappingRule();
//                    rule.setSrcLabelIdx(Integer.valueOf(matcher.group(1)));
//                    rule.setType(matcher.group(2));
//                    rule.setName(matcher.group(3));
//                    rule.setDestIdx(Integer.valueOf(matcher.group(4)));
//
//                    rules.add(rule);
//                }
//                else{
//                    throw new ReportException("err.report_engine.aggr_table.mapping_rules_pattern_not_match", new String[]{String.valueOf(i), ruleString[i]});
//                }
//            }
//        }
//        catch(ReportException e){
//            throw e;
//        }
//        catch (Throwable e) {
//            throw new ReportException("err.report_engine.aggr_table.mapping_rules_parsing_failed", e);
//        }
//
//        return rules;
//    }


    private Map validate(Set mappingRules) throws ReportException {
        Map mappRules = new HashMap();
        //treeset to check consistency of dest_idx'es
        TreeSet columnHeaderDestIdxs = new TreeSet(new IntIdxComparator());
        TreeSet rowHeaderDestIdxs = new TreeSet(new IntIdxComparator());
        TreeSet dataDestIdxs = new TreeSet(new IntIdxComparator());

        if(mappingRules == null)
            throw new ReportException("err.report_engine.aggr_table.null_mapping_rules");

        for(Iterator iter = mappingRules.iterator(); iter.hasNext();){
            MappingRule rule = (MappingRule)iter.next();
            if(mappRules.containsKey(rule.getSrcLabelIdx()))
                throw new ReportException("err.report_engine.aggr_table.duplicate_src_indexes_occured");

            //save in map under string keys
            mappRules.put(String.valueOf(rule.getSrcLabelIdx().intValue()), rule);

            if(MappingRule.Type.COLUMN_HEADER.equals(rule.getType())){
                columnHeaderDestIdxs.add(rule.getDestIdx());
            }
            else if(MappingRule.Type.ROW_HEADER.equals(rule.getType())){
                rowHeaderDestIdxs.add(rule.getDestIdx());
            }
            else if(MappingRule.Type.DATA.equals(rule.getType())){
                dataDestIdxs.add(rule.getDestIdx());
            }
            else
                throw new ReportException("err.report_engine.aggr_table.unknown_column_type", new String[]{rule.getType().toString()});
        }
        checkIdxListConsistence(columnHeaderDestIdxs);
        checkIdxListConsistence(rowHeaderDestIdxs);
        checkIdxListConsistence(dataDestIdxs);

        return mappRules;
    }

    /** Evaluates from mappingRules count of levels in header (of specified type).
     *  Result is used in aggreagated functions validation. */
    private int getLevelCount (Set mappingRules, MappingRule.Type type){
        int level = -1;
        for(Iterator iter = mappingRules.iterator(); iter.hasNext();){
            MappingRule rule = (MappingRule)iter.next();
            if(rule.getType().equals(type) && rule.getDestIdx().intValue() > level)
                level = rule.getDestIdx().intValue();
        }
        return level;
    }

    /** all objects in geven set should be Integer from 0 to N without "holes" */
    static void checkIdxListConsistence(TreeSet idxTreeSet) throws ReportException{
        int i = 0;
        for(Iterator iter = idxTreeSet.iterator(); iter.hasNext();){
            Integer idx = (Integer)iter.next();
            if(idx.intValue() != i)
                throw new ReportException("err.report_engine.aggr_table.idx_inconsistent", new String[]{String.valueOf(i)});
            i++;
        }
    }

    /** sort table by headers.
     * Call it after data aggregation and before headers aggregation */
    private void sort(HeaderSet localColumnSet, HeaderSet localRowSet, Data localData){
        localData.sort(localColumnSet.sortHeaderSet(), localRowSet.sortHeaderSet());
    }

    private void applyLookAndFeel(HeaderSet localColumnHeaderSet, HeaderSet localRowHeaderSet, Data localData) throws ReportException{
        //apply fact header position
        String position = configRootDoc.getRoot().getLookAndFeel().getFactHeader().getPosition();
        HeaderSet headerSet = null;
        if("column".equals(position)){
            headerSet = localColumnHeaderSet;
        }
        else if("row".equals(position)){
            headerSet = localRowHeaderSet;
        }
        else{
            throw new ReportException("err.report_engine.aggr_table.unknown_fact_header_position", new String[]{position});
        }

        int factCount = localData.getFactCount();
        for(int rowIdx = 0; rowIdx < headerSet.getRowsCount(); rowIdx++){
            for(Iterator iter = headerSet.getRowHeaders(rowIdx).iterator(); iter.hasNext();){
                Header header = (Header)iter.next();
                header.setSpan(header.getSpan() * factCount);
            }
        }
    }

    static class MappingRule{
        private Integer srcLabelIdx = null;
        private Integer srcLabelOrderIdx = null;
        /** only header */
        private Type type = null;
        /** colun name */
        private String name = null;
        private Integer destIdx = null;

        Integer getSrcLabelIdx() {
            return srcLabelIdx;
        }

        void setSrcLabelIdx(Integer srcLabelIdx) {
            this.srcLabelIdx = srcLabelIdx;
        }

        Integer getSrcLabelOrderIdx() {
            return srcLabelOrderIdx;
        }

        void setSrcLabelOrderIdx(Integer srcLabelOrderIdx) {
            this.srcLabelOrderIdx = srcLabelOrderIdx;
        }

        Type getType() {
            return type;
        }

        void setType(Type type) {
            this.type = type;
        }

        private void setType(String typeName) {
            this.type = Type.evaluateType(typeName);
        }

        String getName() {
            return name;
        }

        void setName(String name) {
            this.name = name;
        }

        Integer getDestIdx() {
            return destIdx;
        }

        void setDestIdx(Integer destIdx) {
            this.destIdx = destIdx;
        }


        /** incapsulates types:
         * verticalHeader | horizontalHeader | data */
        static class Type{
            /** these three types are supported */
            static final Type COLUMN_HEADER = new Type("columnHeader");
            static final Type ROW_HEADER = new Type("rowHeader");
            static final Type DATA = new Type("data");

            private String type = null;

            private Type(String type){
                this.type = type;
            }

            public String toString(){
                return type;
            }

            private static Type evaluateType(String typeName){
                if(COLUMN_HEADER.toString().equals(typeName)){
                    return COLUMN_HEADER;
                }
                else if(ROW_HEADER.toString().equals(typeName)){
                    return Type.ROW_HEADER;
                }
                else if(DATA.toString().equals(typeName)){
                    return Type.DATA;
                }
                else {
                    //crete new unsupported type
                    return new Type(typeName);
                }
            }
        }

    }


    private long _printElapsedTime(long prevTime, String phaseName){
        long currTime = System.currentTimeMillis();
        System.out.println(phaseName + " = " + (currTime - prevTime));
        return currTime;
    }
}
