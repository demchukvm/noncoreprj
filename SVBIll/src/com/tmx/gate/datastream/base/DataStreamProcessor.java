package com.tmx.gate.datastream.base;

import com.tmx.as.scheduler.ProcessingThread;
import org.apache.log4j.Logger;

import java.util.Properties;

/**
This is the most abstract datastream processor. Contains common errors handling.
 */
public abstract class DataStreamProcessor extends ProcessingThread {
    /** Logger */
    protected Logger logger = Logger.getLogger("gate.DataStreamProcessor");

    protected abstract void executeDataStreamProcessing() throws DataStreamException;

    protected Properties getProperties(String processorName){
        return GateContext.getInstance().getDatastreamProcessorConfig(processorName);
    }

    protected Properties getOwnProperties(){
        return GateContext.getInstance().getDatastreamProcessorConfig(getName());
    }

    protected void executeSpecificTask() {
//        logger = Logger.getLogger(GateContext.LOGCLASS_GATE + "." + getName());
        logger.info(getName() + " started...");

        try{
            executeDataStreamProcessing();
        }
        catch(DataStreamException e){
            logger.error(e.getTraceDump());
        }
        logger.info(getName() + " finished.");
    }

}
