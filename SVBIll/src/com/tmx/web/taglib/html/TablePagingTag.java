package com.tmx.web.taglib.html;

import org.apache.struts.taglib.TagUtils;
import org.apache.struts.taglib.nested.NestedPropertyHelper;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.JspException;

import com.tmx.web.taglib.base.TableBasicTag;

public class TablePagingTag extends TableBasicTag {

    protected int maxBookmarkCount = 5;//default value
    private String hrefStyleClass;
    private String selectedHrefStyleClass;

    private String leftScrollArrow;
    private String leftScrollArrowSelected;
    private String rightScrollArrow;
    private String rightScrollArrowSelected;

    //------------------------------Properties

    public int getMaxBookmarkCount() {
        return maxBookmarkCount;
    }

    public void setMaxBookmarkCount(int maxBookmarkCount) {
        this.maxBookmarkCount = (maxBookmarkCount <= 0) ? this.maxBookmarkCount : maxBookmarkCount;
    }

    public String getHrefStyleClass() {
        return hrefStyleClass != null ? hrefStyleClass : getDefaultProperty("tag.tablePaging.style.hrefStyleClass");
    }

    public void setHrefStyleClass(String hrefStyleClass) {
        this.hrefStyleClass = hrefStyleClass;
    }

    public String getSelectedHrefStyleClass() {
        return selectedHrefStyleClass != null ? selectedHrefStyleClass : getDefaultProperty("tag.tablePaging.style.selectedHrefStyleClass");
    }

    public void setSelectedHrefStyleClass(String selectedHrefStyleClass) {
        this.selectedHrefStyleClass = selectedHrefStyleClass;
    }


    public String getLeftScrollArrow() {
        return (leftScrollArrow != null) ? leftScrollArrow : getDefaultProperty("tag.tablePagingTag.label.leftScrollArrow");
    }

    public String getLeftScrollArrowSelected() {
        return (leftScrollArrowSelected != null) ? leftScrollArrowSelected : getDefaultProperty("tag.tablePagingTag.label.leftScrollArrowSelected");
    }

    public String getRightScrollArrow() {
        return (rightScrollArrow != null) ? rightScrollArrow : getDefaultProperty("tag.tablePagingTag.label.rightScrollArrow");
    }

    public String getRightScrollArrowSelected() {
        return (rightScrollArrowSelected != null) ? rightScrollArrowSelected : getDefaultProperty("tag.tablePagingTag.label.rightScrollArrowSelected");
    }

    public void setLeftScrollArrow(String leftScrollArrow) {
        this.leftScrollArrow = leftScrollArrow;
    }

    public void setLeftScrollArrowSelected(String leftScrollArrowSelected) {
        this.leftScrollArrowSelected = leftScrollArrowSelected;
    }

    public void setRightScrollArrow(String rightScrollArrow) {
        this.rightScrollArrow = rightScrollArrow;
    }

    public void setRightScrollArrowSelected(String rightScrollArrowSelected) {
        this.rightScrollArrowSelected = rightScrollArrowSelected;
    }


    //------------------------------Parsing events

    public int doStartTmxTag() throws JspException {
        String tablePagingHTML = renderTablePagingHTML();
        TagUtils.getInstance().write(pageContext, tablePagingHTML);
        return (EVAL_BODY_TAG);
    }

    //------------------------------Rendering methods

    private String renderTablePagingHTML() throws JspException {
        StringBuffer resultHTML = new StringBuffer();
        //render page bookmarks
        resultHTML.append(getLocalizedLabel("tag.tablePaging.label.pages", null));
        resultHTML.append(": [");

        int realBookmarkCount = 1;
        if(getTable().getResultSize() == null || getTable().getResultSize().intValue() == 0){
            realBookmarkCount = 1;
        } else{
            float res = (float)getTable().getResultSize().intValue() / (float)getTable().getQuery().getPageSize();
            realBookmarkCount = (int)Math.floor(res);
            if(res > realBookmarkCount)
                realBookmarkCount++;
        }
        int bookmarkCount = (realBookmarkCount > maxBookmarkCount) ? maxBookmarkCount : realBookmarkCount;

        //render page bookmarks
        int currentPageNumber = getCurrentPage();

        //left arrow
        if (currentPageNumber == 1) {
            resultHTML.append(getArrowTag(getLeftScrollArrow(), getLeftScrollArrowSelected()));
            resultHTML.append("&nbsp;&nbsp;");
        } else {
            resultHTML.append(getArrowLink(currentPageNumber - 1, getArrowTag(getLeftScrollArrow(), getLeftScrollArrowSelected())));
            resultHTML.append("&nbsp;&nbsp;");
        }

        //page numbers
        //calculete first page in pager number
        int firstPageNumber = currentPageNumber - bookmarkCount/2;
        if(firstPageNumber <= 0)
            firstPageNumber = 1;
        else if(firstPageNumber + bookmarkCount > realBookmarkCount)
            firstPageNumber = realBookmarkCount - bookmarkCount + 1;

        for (int i = firstPageNumber; i < firstPageNumber + bookmarkCount; i++) {
            if (currentPageNumber == i)
                resultHTML.append(renderSelectedBookmark(i));
            else
                resultHTML.append(getBookmarkLink(i, String.valueOf(i)));
            resultHTML.append("&nbsp;&nbsp;");
        }

        //right arrow
        if (currentPageNumber < realBookmarkCount) {
            resultHTML.append(getArrowLink(currentPageNumber + 1, getArrowTag(getRightScrollArrow(), getRightScrollArrowSelected())));
        } else {
            resultHTML.append(getArrowTag(getRightScrollArrow(), getRightScrollArrowSelected()));
        }

        resultHTML.append("]");

        return resultHTML.toString();
    }

    protected String getBookmarkLink(int pageNumber, String linkContent) throws JspException {
        StringBuffer bookmark = new StringBuffer();

        bookmark.append("<a href=\"#\" ");
        bookmark.append(" class=\"");
        bookmark.append(getHrefStyleClass());
        bookmark.append("\" ");
        bookmark.append("onClick=\"document.forms[0].action='");
        bookmark.append(getRefreshActionUrl());
        bookmark.append("?");
        bookmark.append("command="+getRefreshActionCommand()+"&");
        bookmark.append(buildParameterString(pageNumber));
        bookmark.append("';document.forms[0].submit()\">");
        bookmark.append(linkContent);
        bookmark.append("</a>");
        return bookmark.toString();
    }

    private String getArrowLink(int pageNumber, String linkContent) throws JspException {
        StringBuffer bookmark = new StringBuffer();

        bookmark.append("<a href=\"#\" ");
        bookmark.append(" class=\"");
        bookmark.append(getHrefStyleClass());
        bookmark.append("\" ");
        bookmark.append("onClick=\"document.forms[0].action='");
        bookmark.append(getRefreshActionUrl());
        bookmark.append("?");
        bookmark.append("command="+getRefreshActionCommand()+"&");
        bookmark.append(buildParameterString(pageNumber));
        bookmark.append("';document.forms[0].submit()\">");
        bookmark.append(linkContent);
        bookmark.append("</a>");
        return bookmark.toString();
    }

    /** render scroll arrow (left or right) */
    protected String getArrowTag(String scrollArrow, String scrollArrowHovered) {
        return scrollArrow;
    }

    protected String renderSelectedBookmark(int number) {
        StringBuffer bookmark = new StringBuffer();
        bookmark.append("<span class=\"");
        bookmark.append(getSelectedHrefStyleClass());
        bookmark.append("\">");
        bookmark.append(number);
        bookmark.append("</span>");
        return bookmark.toString();
    }


    //------------------------------Bean interaction methods
    protected int getCurrentPage() throws JspException {
        int currentPageNumber = 1;

        HttpServletRequest request = (HttpServletRequest) pageContext.getRequest();
        String originalName = (getName() != null) ? getName() : NestedPropertyHelper.getCurrentName(request, this);
        //Read pageNumber from Query from Table from Bean form
        String originalProperty = getTableName() + ".query.pageNumber";
        String nestedProperty = NestedPropertyHelper.getAdjustedProperty(request, originalProperty);

        Object value = TagUtils.getInstance().lookup(pageContext, originalName, nestedProperty, null);
        if (value != null && (value instanceof Integer))
            currentPageNumber = ((Integer) value).intValue();
        return currentPageNumber;
    }

    //------------------------------Business logic

    /**
     * Build parameter String
     */
    protected String buildParameterString(int pageNumber) {
        StringBuffer parameterString = new StringBuffer();
        parameterString.append(getTableName());
        parameterString.append(".appliedParameters");
        parameterString.append("=");
        parameterString.append("query(page_number=");
        parameterString.append(pageNumber);
        parameterString.append(")");
        return parameterString.toString();
    }


}
