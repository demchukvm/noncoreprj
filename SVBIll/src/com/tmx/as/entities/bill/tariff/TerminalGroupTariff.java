package com.tmx.as.entities.bill.tariff;

import com.tmx.as.entities.bill.balance.TerminalGroupBalance;
import com.tmx.as.entities.bill.terminal_group.TerminalGroup;

/**

 */
public class TerminalGroupTariff extends AbstractTariff{
    private TerminalGroup terminalGroup;
    private Long terminalGroupId;
    private Long terminalGroupBalanceId;
    private TerminalGroupBalance terminalGroupBalance;

    //--------Properties

    public TerminalGroup getTerminalGroup() {
        return terminalGroup;
    }

    public void setTerminalGroup(TerminalGroup terminalGroup) {
        this.terminalGroup = terminalGroup;
    }

    public Long getTerminalGroupId() {
        return terminalGroupId;
    }

    public void setTerminalGroupId(Long terminalGroupId) {
        this.terminalGroupId = terminalGroupId;
    }

    public Long getTerminalGroupBalanceId() {
        return terminalGroupBalanceId;
    }

    public void setTerminalGroupBalanceId(Long terminalGroupBalanceId) {
        this.terminalGroupBalanceId = terminalGroupBalanceId;
    }

    public TerminalGroupBalance getTerminalGroupBalance() {
        return terminalGroupBalance;
    }

    public void setTerminalGroupBalance(TerminalGroupBalance terminalGroupBalance) {
        this.terminalGroupBalance = terminalGroupBalance;
    }
}
