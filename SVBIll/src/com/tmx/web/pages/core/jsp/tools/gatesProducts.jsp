<%@ page import="com.tmx.web.forms.core.tools.GatesProductsForm" %>
<%@ page import="java.util.HashMap" %>
<%@ page import="java.util.Set" %>
<%@ taglib prefix="nested" uri="http://jakarta.apache.org/struts/tags-nested" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/tld/tmx-controls.tld" prefix="tmx_ctrl" %>

<%--<html:form action="/GatesProductsAction">--%>

<table border="0" cellpadding="3" cellspacing="0" style="border: 1px solid black;">
    <tr align="left" bgcolor="silver" style="background:silver;" onclick="rst();">
        <th width="15"></th>
        <th></th>
        <th>Товар</th>
        <th></th>
        <th>Шлюз</th>
    </tr>
<%
    HashMap mediumMap = ((GatesProductsForm) pageContext.getSession().getAttribute("GatesProductsForm")).getMediumMap();
    Set set = mediumMap.keySet();   // товар - шлюз
    int i = 0;
    for(Object s : set)
    {
%>
    <tr align="left" id="<%=i%>">
        <td width="10" bgcolor="silver" onclick="rst();"></td>
        <td>
           <input type="radio" name="productName" value="<%=s%>" onclick="clk();" />
        </td>
        <td><%=s%></td>
        <td width="10"></td>
        <td><%=mediumMap.get(s)%></td>
    </tr>
    <tr><td colspan="15"><span></span></td></tr> <!-- для всплывающей части -->
<%
        i++;
    }
%>


</table>


<script type="text/javascript">
    function clk()
    {
        span = document.getElementsByTagName('span');
        for(i=0;i<span.length;i++)
        {
            if(span[i].firstChild != null)
            {
                node1=span[i].firstChild;
                node2=span[i].lastChild;
                node1.parentNode.removeChild(node1);
                node1.parentNode.removeChild(node2);

                tr = document.getElementById(i);
                //tr.style['background']='white';
                tr.background='white';
            }
        }
        input = document.getElementsByName('productName');
        for(i=0;i<input.length;i++)
        {
            if(input[i].checked)
            {
                span[i].innerHTML='<div>\
                                    <select name="gateName">\
                                        <option>avancel_gate_1</option>\
                                        <option>avancel_gate_2</option>\
                                        <option>avancel_gate_3</option>\
                                        <option>avancel_gate_4</option>\
                                        <option>beeline_gate</option>\
                                        <option>citypay_gate</option>\
                                        <option>dacard_gate</option>\
                                        <option>kyivstar_gate</option>\
                                        <option>kyivstar_bonus_commission_gate</option>\
                                        <option>kyivstar_bonus_no_commission_gate</option>\
                                        <option>kyivstar_exclusive_gate</option>\
                                        <option>life_gate</option>\
                                        <option>test_gate</option>\
                                        <option>umc_gate</option>\
                                        <option>ussd_gate</option>\
                                        <option>ussd_gate_reserve</option>\
                                    </select>\
                                    <input type="submit" value="ok" />\
                                </div>';
                tr = document.getElementById(i);
                //tr.style['background']='yellow';
                tr.background='yellow';
            }
        }
    }

    function rst()
    {
        span = document.getElementsByTagName('span');
        for(i=0;i<span.length;i++)
        {
            if(span[i].firstChild != null)
            {
                node1=span[i].firstChild;
                node2=span[i].lastChild;
                node1.parentNode.removeChild(node1);
                node1.parentNode.removeChild(node2);

                tr = document.getElementById(i);
                //tr.style['background']='white';
                tr.background='white';
            }
        }
    }
</script>