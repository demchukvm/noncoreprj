package com.tmx.beng.base.channels;

import com.tmx.beng.base.BillingMessage;
import com.tmx.beng.base.BillException;

/**
    Route to the specified by id out channel.
 */
public class ToSpecifiedChannel extends AbstractChannel{

    public OutChannel process(BillingMessage msg) throws BillException {
        final String OUT_CHANNEL_ID = "routeToChannelId";
        int outChannelId = 0;
        try{
            if(getParamValue(OUT_CHANNEL_ID) == null)
                throw new BillException("err.tospecchannel.null_param_routeToChannelId");
            outChannelId = Integer.parseInt(getParamValue(OUT_CHANNEL_ID));
        }
        catch(NumberFormatException e){
            throw new BillException("err.tospecchannel.not_int_routeToChannelId");
        }
        return getOutChannel(outChannelId);
    }
}
