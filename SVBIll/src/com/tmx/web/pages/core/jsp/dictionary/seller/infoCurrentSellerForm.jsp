<%@ page language="java" pageEncoding="UTF-8" contentType="text/html; charset=utf-8" isELIgnored="false" %>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-nested" prefix="nested" %>
<%@ taglib uri="http://tmx.com/taglib/tmx-controls" prefix="tmx_ctrl" %>
<%@ taglib uri="http://tmx.com/taglib/tmx-logic" prefix="tmx_logic" %>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html" %>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean" %>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>


<html:form action="/core/dictionary/seller/infoCurrentSeller">
<table border="0" width="100%">

    <!-- BUTTONS -->
    <%--<tr>--%>
        <%--<td style="text-align: left">--%>
            <%--<tmx_ctrl:button property="onApplyButton"/>--%>
            <%--<tmx_ctrl:button property="onSaveButton"/>--%>
            <%--<tmx_ctrl:button property="onResetButton" onClick="javascript:document.forms[0].reset()"/>--%>
            <%--<tmx_ctrl:button property="onCancelButton"/>--%>
            <%----%>
        <%--</td>--%>
    <%--</tr>--%>
    <tr>
        <td valign="center" align="center" class="tabForm">
        <table width="100%" border="0">
            <%--<!-- Form header -->--%>
            <%--<tr>--%>
                <%--<td colspan="2">--%>
                    <%--<h4 class="dataLabel">--%>
                        <%--<bean:message bundle="core.labels" key="core.page.dictionary.kiosk.kioskinfoform.titlegeneral"/>--%>
                    <%--</h4>--%>
                <%--</td>--%>
            <%--</tr>--%>
            <tr>

                <td class="dataLabel" width="15%">
                    <tmx_ctrl:writeEntityLabel attrName="sellerId"
                                               entityClassName="com.tmx.as.entities.bill.seller.Seller"/>:
                </td>
                <td class="dataField" width="35%">
                    <tmx_ctrl:editBox property="editBoxSellerId"/>
                </td>

                <td class="dataLabel" width="15%">
                    <tmx_ctrl:writeEntityLabel attrName="parentSeller"
                                               entityClassName="com.tmx.as.entities.bill.seller.Seller"/>:
                </td>
                <td class="dataField" width="35%">
                    <tmx_ctrl:selectBox property="scrollBoxParentSellers"/>
                </td>

            </tr>

            <tr>
                <td class="dataLabel">
                    <tmx_ctrl:writeEntityLabel attrName="name"
                                               entityClassName="com.tmx.as.entities.bill.seller.Seller"/>:
                </td>
                <td class="dataField">
                    <tmx_ctrl:editBox property="editBoxName"/>
                </td>

                <td class="dataLabel">
                    <tmx_ctrl:writeEntityLabel attrName="code"
                                               entityClassName="com.tmx.as.entities.bill.seller.Seller"/>:
                </td>
                <td class="dataField" width="35%">
                       <tmx_ctrl:editBox property="editBoxCode" />
                </td>
            </tr>

            <tr>
                <td class="dataLabel" width="15%">
                       <tmx_ctrl:writeEntityLabel attrName="registrationDate"
                                                  entityClassName="com.tmx.as.entities.bill.seller.Seller"/>:
                </td>
                <td class="dataField" width="35%">
                    <tmx_ctrl:timeSelector property="timeSelectorRegistrationDate" />
                </td>

                <td class="dataLabel">
                    <tmx_ctrl:writeEntityLabel attrName="description"
                                               entityClassName="com.tmx.as.entities.bill.seller.Seller"/>:
                </td>
                <td class="dataField">
                    <tmx_ctrl:textArea property="textAreaDescription"/>
                </td>
            </tr>

            <tr>
                <td class="dataLabel" width="15%">

                </td>
                <td class="dataField" width="35%">

                </td>

                <td class="dataLabel">
                    <tmx_ctrl:writeEntityLabel attrName="active"
                                               entityClassName="com.tmx.as.entities.bill.seller.Seller"/>:
                </td>
                <td class="dataField">
                    <tmx_ctrl:checkBox property="checkBoxActive"/>
                </td>
            </tr>

        </table>
        </td>

    </tr>



    <%--<!-- BUTTONS -->--%>
    <%--<tr>--%>
        <%--<td style="text-align: left">--%>
            <%--<tmx_ctrl:button property="onApplyButton"/>--%>
            <%--<tmx_ctrl:button property="onSaveButton"/>--%>
            <%--<tmx_ctrl:button property="onResetButton" onClick="javascript:document.forms[0].reset()"/>--%>
            <%--<tmx_ctrl:button property="onCancelButton"/>--%>
        <%--</td>--%>
    <%--</tr>--%>
   

</table>
</html:form>