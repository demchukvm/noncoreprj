package com.tmx.beng.httpsgate.avancel;

import org.apache.log4j.Logger;
import com.tmx.beng.httpsgate.HttpsGateException;
import com.tmx.util.InitException;

/**

 */
public class AvancelGate2 extends BasicAvancelGate{
    protected static final String GATE_NAME = "avancel_gate_2";
    protected static Logger logger = Logger.getLogger("avancel_gate_2." + GATE_NAME);
    private static AvancelGate2 avancelGate = null;


    public static AvancelGate2 getInstance() throws HttpsGateException {
        if(avancelGate == null){
            logger.error("Attempt to obtain not inited AvancelGate2");
            throw new HttpsGateException("err.AvancelGate2.null_instance");
        }
        return avancelGate;
    }

    public static void init()throws InitException {
        logger.info("Start AvancelGate2 initialization");
        avancelGate = new AvancelGate2();
        avancelGate.init0();
        logger.info("AvancelGate2 initialized");
    }

    public static void shutdown(final long timeout) {
        logger.info("Start AvancelGate2 Shutdown");
        avancelGate.shutdown0(timeout);
        logger.info("AvancelGate2 shutdown");
    }

    protected String getGateName(){
        return GATE_NAME;
    }

}
