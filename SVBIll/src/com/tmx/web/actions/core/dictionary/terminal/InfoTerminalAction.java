package com.tmx.web.actions.core.dictionary.terminal;

import com.tmx.as.blogic.TerminalManager;
import com.tmx.as.blogic.BlogicException;
import com.tmx.as.blogic.tariff.TerminalTariffManager;
import com.tmx.as.blogic.tariff.AbstractTariffManager;
import com.tmx.as.blogic.tariff.SellerTariffManager;
import com.tmx.as.blogic.restriction.TerminalRestrictionManager;
import com.tmx.as.entities.bill.terminal.Terminal;
import com.tmx.as.entities.bill.restriction.TerminalRestriction;
import com.tmx.as.entities.bill.function.FunctionInstance;
import com.tmx.as.entities.bill.function.FunctionType;
import com.tmx.as.entities.bill.function.ActualFunctionParameter;
import com.tmx.as.entities.bill.tariff.TerminalTariff;
import com.tmx.as.entities.bill.tariff.SellerTariff;
import com.tmx.as.entities.bill.seller.Seller;
import com.tmx.as.base.EntityManager;
import com.tmx.as.exceptions.DatabaseException;
import com.tmx.web.actions.core.AbstractSvbillAction;
import com.tmx.web.base.Roadmap;
import com.tmx.web.base.WebException;
import com.tmx.web.forms.core.base.AbstractFormFactory;
import com.tmx.web.forms.core.dictionary.terminal.InfoTerminalForm;
import com.tmx.web.controls.CheckBox;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.*;


public class InfoTerminalAction extends AbstractSvbillAction {

    private static final String FORWARD_MANAGE_TERMINALS_ACTION = "manageTerminalsAction";
    private static final String FORWARD_INFO_TERMINAL_PAGE = "infoTerminalPage";
    private static final String FORWARD_MANAGE_DELETE_ACTION = "manageDeleteAction";


    public ActionForward preloadForCreate(ActionMapping mapping,
                                          ActionForm form,
                                          HttpServletRequest request,
                                          HttpServletResponse response) throws Throwable {
        InfoTerminalForm infoTerminalForm = (InfoTerminalForm) verifyForm(form, InfoTerminalForm.class);
        infoTerminalForm.reset();
        //enable attaching
        if (infoTerminalForm.getEditBoxTerminalId().getValue() == null
                    || infoTerminalForm.getEditBoxTerminalId().getValue().equals("")) {
            infoTerminalForm.getCheckBoxIsAttachRestriction().setSelected(true);
            infoTerminalForm.getCheckBoxIsAttachTariffOnline().setSelected(true);
            infoTerminalForm.getCheckBoxIsAttachTariffVouchers().setSelected(true);
        }

        infoTerminalForm.bindData(getLoggedInUser(request));
        String cancelAction = (String) getRoadmap().getParameterValue(this.getName(), "cancelActionFrominfoTerminalForm", true);
        infoTerminalForm.getOnCancelButton().setAction(cancelAction);

        return mapping.findForward(FORWARD_INFO_TERMINAL_PAGE);
    }

/*
    Method looks for existent tariffs for terminal and disables needed check boxes
     */
private void disableCheckBoxForExistingTariffs(InfoTerminalForm form, Long terminalId) throws DatabaseException, WebException {
    //create set of tariffed operators
    TerminalTariffManager terminalTariffManager = new TerminalTariffManager();
    List<TerminalTariff> tariffs = terminalTariffManager.getAllTariff(terminalId);
    for (TerminalTariff tariff : tariffs) {
        if (tariff.getActive()) {
            if (tariff.getFunctionInstance().getFunctionType().getImplClass().indexOf("TerminalTariffBuyVoucherDiscount") >= 0) {
                form.getCheckBoxIsAttachTariffVouchers().setReadonly(true);
            }
            if (tariff.getFunctionInstance().getFunctionType().getImplClass().equals("com.tmx.beng.base.dbfunctions.TerminalTariff")) {
                form.getCheckBoxIsAttachTariffOnline().setReadonly(true);
            }
        }
    }
}


    public ActionForward preloadForUpdate(ActionMapping mapping,
                                          ActionForm form,
                                          HttpServletRequest request,
                                          HttpServletResponse response) throws Throwable {

        InfoTerminalForm infoTerminalForm = (InfoTerminalForm) verifyForm(form, InfoTerminalForm.class);
        infoTerminalForm.reset();
        //disable attaching
        infoTerminalForm.getCheckBoxIsAttachRestriction().setSelected(false);
        infoTerminalForm.getCheckBoxIsAttachTariffOnline().setSelected(false);
        infoTerminalForm.getCheckBoxIsAttachTariffVouchers().setSelected(false);

        Terminal terminal = new TerminalManager().getTerminal( infoTerminalForm.getSelectedTerminalId() );
        
        String cancelAction = (String) getRoadmap().getParameterValue(this.getName(),
                                                "cancelActionFrominfoTerminalForm", true);
        infoTerminalForm.getOnCancelButton().setAction(cancelAction);
        
        infoTerminalForm.bindData( terminal, getLoggedInUser(request) );

        disableCheckBoxForExistingTariffs(infoTerminalForm, Long.parseLong(infoTerminalForm.getEditBoxTerminalId().getValue()));


        AbstractFormFactory.getInstance(getLoggedInUser(request)).setUpForm(infoTerminalForm);

        return mapping.findForward(FORWARD_INFO_TERMINAL_PAGE);
    }

    public ActionForward save(ActionMapping mapping,
                              ActionForm form,
                              HttpServletRequest request,
                              HttpServletResponse response) throws Throwable {
        InfoTerminalForm infoTerminalForm = (InfoTerminalForm) verifyForm(form, InfoTerminalForm.class);
        infoTerminalForm.validate();

        Terminal terminal = saveTerminal(infoTerminalForm);

        if (terminal != null && infoTerminalForm.getCheckBoxIsAttachRestriction().isSelected()) {
            attachRestriction(terminal);
        }
        if (terminal != null ) {
            attachTariffs(terminal, infoTerminalForm.getCheckBoxIsAttachTariffOnline().isSelected(),
                                                infoTerminalForm.getCheckBoxIsAttachTariffVouchers().isSelected());
        }

        return mapping.findForward(FORWARD_MANAGE_TERMINALS_ACTION);

//        return addBalances(mapping); // WTF???????????????????????

    }

    private void attachTariffs(Terminal terminal, boolean isTariffOnline, boolean isTariffVoucher) throws DatabaseException, BlogicException {
        GregorianCalendar calendar = new GregorianCalendar();
        calendar.set(Calendar.HOUR, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        if (isTariffOnline) {
            FunctionInstance functionInstance = (FunctionInstance)(new EntityManager()).RETRIEVE(
                                                    FunctionInstance.class, 5484646l);//TODO: remove from hard code

            TerminalTariff terminalTariff = new TerminalTariff();
            terminalTariff.setName(terminal.getSerialNumber() + " generic on-line tariff");
            terminalTariff.setFunctionInstance(functionInstance);
            terminalTariff.setActivationDate(calendar.getTime());
            terminalTariff.setTerminal(terminal);
            terminalTariff.setTerminalId(terminal.getTerminalId());

            (new TerminalTariffManager()).saveTariff(terminalTariff);
        }
        if (isTariffVoucher) {
            FunctionInstance functionInstance = (FunctionInstance)(new EntityManager()).RETRIEVE(
                                                    FunctionInstance.class, 5484656l);//TODO: remove from hard code

            TerminalTariff terminalTariff = new TerminalTariff();
            terminalTariff.setName(terminal.getSerialNumber() + " generic vouchers tariff");
            terminalTariff.setFunctionInstance(functionInstance);
            terminalTariff.setActivationDate(calendar.getTime());
            terminalTariff.setTerminal(terminal);
            terminalTariff.setTerminalId(terminal.getTerminalId());

            (new TerminalTariffManager()).saveTariff(terminalTariff);
        }
    }

    private void attachRestriction(Terminal terminal) throws DatabaseException, BlogicException {
        //            FunctionType functionType = new FunctionType();
//            functionType.setFunctionTypeId(40l);
//            functionType.setName("terminal_min_balance_limit");
//
//            FunctionInstance functionInstance = new FunctionInstance();
//            functionInstance.setFunctionType(functionType);
//            functionInstance.setFunctionInstanceId(instanceId);
//            functionInstance.setFunctionParameters(new HashSet(funcParametrsTable.getData()));
        FunctionInstance functionInstance = (FunctionInstance)(new EntityManager()).RETRIEVE(FunctionInstance.class,
                                                                    8333868l);//TODO: remove from hard code

        TerminalRestriction tr = new TerminalRestriction();
        tr.setName("terminal " + terminal.getSerialNumber() + " balance >= 0 ");
        tr.setFunctionInstance(functionInstance);
        tr.setTerminal(terminal);
        tr.setTerminalId(terminal.getTerminalId());

        (new TerminalRestrictionManager()).saveRestriction(tr);
    }

    public ActionForward addBalances(ActionMapping mapping) throws Throwable {
         new TerminalManager().nonPermanentUpdateTerminals();
        return mapping.findForward(FORWARD_MANAGE_TERMINALS_ACTION);
    }

    public ActionForward apply(ActionMapping mapping,
                              ActionForm form,
                              HttpServletRequest request,
                              HttpServletResponse response) throws Throwable {

        InfoTerminalForm infoTerminalForm = (InfoTerminalForm) verifyForm(form, InfoTerminalForm.class);
        infoTerminalForm.validate();

        saveTerminal( infoTerminalForm );

        return mapping.findForward(FORWARD_INFO_TERMINAL_PAGE);
    }

    public ActionForward preloadForDelete(ActionMapping mapping,
                                          ActionForm form,
                                          HttpServletRequest request,
                                          HttpServletResponse response) throws Throwable {
        Roadmap roadmap=getRoadmap();
        String forwardOnCancel = (String) getRoadmap().getParameterValue(this.getName(),
                                                "cancelActionOnDelete", true);

        roadmap.collectParameter("/core/message/confirmation", "leftMenuName", "dictionaries");
        roadmap.setPageForward("confirmation","onOk","local:"+this.getName()+"#"+FORWARD_MANAGE_DELETE_ACTION);
        roadmap.setPageForward("confirmation","onCancel","local:"+this.getName()+"#"+ forwardOnCancel);
        return mapping.findForward("confirmation");
    }

    public ActionForward delete(ActionMapping mapping,
                              ActionForm form,
                              HttpServletRequest request,
                              HttpServletResponse response) throws Throwable {

        InfoTerminalForm infoTerminalForm = (InfoTerminalForm) verifyForm(form, InfoTerminalForm.class);

        TerminalManager terminalManager = new TerminalManager();
        terminalManager.deleteTerminal( infoTerminalForm.getSelectedTerminalId() );

        String forwardToManageForm = (String) getRoadmap().getParameterValue(this.getName(),
                                                "cancelActionOnDelete", true);

        return mapping.findForward(forwardToManageForm);
    }

    public ActionForward defaultAction(ActionMapping mapping,
                                       ActionForm form,
                                       HttpServletRequest request,
                                       HttpServletResponse response,
                                       String methodName) throws Throwable {
      //  InfoTerminalForm infoTerminalForm = (InfoTerminalForm) form;


        return preloadForCreate(mapping, form, request, response);
    }

    private Terminal saveTerminal( InfoTerminalForm infoTerminalForm ) throws Throwable {
        TerminalManager terminalManager = new TerminalManager();
        Terminal terminal = infoTerminalForm.populateFromControls();
        terminalManager.saveTerminal( terminal, 0 );

        return terminal;
    }

}
