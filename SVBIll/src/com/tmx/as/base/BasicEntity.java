package com.tmx.as.base;

import com.tmx.util.StringUtil;
import com.tmx.as.modules.EntityResourcesModule;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Set;
import java.util.List;
import java.util.Locale;
import java.io.Serializable;

public abstract class BasicEntity implements Entity {

    /**
     * Set default properties' values for entity
     */
    public void setDefaults() {
    }

    public abstract Serializable getId();

    public int valid = 1;

    public abstract int getValid();

    public abstract void setValid(int valid);

    /** RM: Not used yet */
    public boolean isTransient(){
        Serializable id = getId();
        if(id == null) return true;

        if(id.getClass().equals(Long.class) &&
           ((Long)id).longValue() == 0L){
            return true;
        }

        return false;
    }


    /** RM: Not used yet */
    public void pruneEmptyTableAttributes(){
        Field[] fields = this.getClass().getDeclaredFields();
        for(int i = 0; i < fields.length; i++){
            Method getter = null;
            Method setter = null;
            try{
                String field = fields[i].getName();
                String getterName = StringUtil.getGetterName(field);
                getter = this.getClass().getDeclaredMethod(getterName, null);
                String setterName = StringUtil.getSetterName(field);
                setter = this.getClass().getDeclaredMethod(setterName, new Class[]{fields[i].getType()});
            }
            catch(NoSuchMethodException e){
                e.printStackTrace();
            }

            try{
                Object field = getter.invoke(this, null);
                if((field instanceof Set) && (field != null) && (((Set)field).size() == 0)){
                    setter.invoke(this, new Class[]{null});
                }

                if((field instanceof List) && (field != null) && (((List)field).size() == 0)){
                    setter.invoke(this, new Class[]{null});
                }

            }
            catch(Exception e){
                e.printStackTrace();
            }
        }
    }


    public String getLabel(String attrName, Locale locale){
        return new EntityResourcesModule().getEntityResources().getEntityLabel(this.getClass(), attrName, locale);
    }


}
