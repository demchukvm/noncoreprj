package com.tmx.beng.iso8583Gate.pdu.substructure;

import com.tmx.beng.iso8583Gate.pdu.RuntimeDataElement;

/**
 */
public abstract class BasicRuntimeSubDE implements RuntimeSubDataElement{
    protected RuntimeDataElement ownerRDE;


    public RuntimeDataElement getOwnerRDE() {
        return ownerRDE;
    }

    public void setOwnerRDE(RuntimeDataElement ownerRDE) {
        this.ownerRDE = ownerRDE;
    }
}
