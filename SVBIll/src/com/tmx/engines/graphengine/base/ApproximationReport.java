package com.tmx.engines.graphengine.base;

import java.util.List;
import java.util.ArrayList;

/**
 * Used to provide user information about approximation results per each data series.
 */
public class ApproximationReport {
    List blockReports = new ArrayList();

    public BlockReport addBlockReport(int idx){
        BlockReport bReport = new BlockReport();
        blockReports.add(idx, bReport);
        return bReport;
    }

    public BlockReport getBlockReport(int idx){
        return (BlockReport)blockReports.get(idx);
    }

    public int getBlockReportCount(){
        return blockReports.size();
    }

    public class BlockReport{
        /** Is current data block enabled. */
        private boolean blockEnabled = false;
        private String name = null;
        private boolean approxUsed = false;
        private int initialPointsCount = -1;
        private int approxedPointsCount = -1;

        private BlockReport(){
            /** hidden from public usage */
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public boolean isApproxUsed() {
            return approxUsed;
        }

        public void setApproxUsed(boolean approxUsed) {
            this.approxUsed = approxUsed;
        }

        public int getInitialPointsCount() {
            return initialPointsCount;
        }

        public void setInitialPointsCount(int initialPointsCount) {
            this.initialPointsCount = initialPointsCount;
        }

        public int getApproxedPointsCount() {
            return approxedPointsCount;
        }

        public void setApproxedPointsCount(int approxedPointsCount) {
            this.approxedPointsCount = approxedPointsCount;
        }

        public boolean isBlockEnabled() {
            return blockEnabled;
        }

        public void setBlockEnabled(boolean blockEnabled) {
            this.blockEnabled = blockEnabled;
        }

    }

}


