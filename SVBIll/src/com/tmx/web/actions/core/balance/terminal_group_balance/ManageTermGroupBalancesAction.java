package com.tmx.web.actions.core.balance.terminal_group_balance;

import com.tmx.web.actions.core.balance.AbstractManageBalancesAction;
import com.tmx.as.exceptions.DatabaseException;
import org.apache.struts.action.ActionForm;


public class ManageTermGroupBalancesAction extends AbstractManageBalancesAction {

    protected String getManageForwardPage() {
        return "manageTermGroupBalancesPage";  
    }

    protected ActionForm setFilters(ActionForm form) throws DatabaseException {
        return form;  //To change body of implemented methods use File | Settings | File Templates.
    }
}
