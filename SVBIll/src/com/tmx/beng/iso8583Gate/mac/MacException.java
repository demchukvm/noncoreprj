package com.tmx.beng.iso8583Gate.mac;

import com.tmx.util.StructurizedException;
import java.util.Locale;

/**

 */
public class MacException extends StructurizedException {

    public MacException(String msg, String[] params, Throwable e, Locale locale) {
        super(msg, params, e, locale);
    }

    public MacException(String msg, String[] params) {
        super(msg, params);
    }

    public MacException(String msg, String[] params, Locale locale) {
        super(msg, params, locale);
    }

    public MacException(String msg, Throwable e, Locale locale) {
        super(msg, e, locale);
    }

    public MacException(String msg, Throwable e) {
        super(msg, e);
    }

    public MacException(String msg, Locale locale) {
        super(msg, locale);
    }

    public MacException(String msg) {
        super(msg);
    }

    public MacException(String msg, String[] params, Throwable e) {
        super(msg, params, e);
    }
}
