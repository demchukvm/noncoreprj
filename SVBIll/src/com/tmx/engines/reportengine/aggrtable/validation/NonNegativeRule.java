package com.tmx.engines.reportengine.aggrtable.validation;

import com.tmx.engines.reportengine.aggrtable.DataElement;

/**
 */
public class NonNegativeRule extends SimpleValidationRule{

    public DataValidationError validate(DataElement data) {
        DataValidationError error = null;
        Double value = convertToDouble(data);
        if(value != null && value.doubleValue() < 0)
            error = new DataValidationError("negative_value", new String[]{data.getValue()});

        return error;
    }
}
