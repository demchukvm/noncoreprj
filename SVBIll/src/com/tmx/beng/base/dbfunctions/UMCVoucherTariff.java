package com.tmx.beng.base.dbfunctions;

import java.util.*;


public class UMCVoucherTariff extends VoucherTariff {

    private static final String serviceCode = "UMC";

    protected List<String> getServiceCodes() {
        return Arrays.asList(serviceCode);
    }
}
