package com.tmx.as.base.criterion;

import java.util.Hashtable;

/** 
   CriterianWrapper used to wrap underlaying sql criterias implementations. 
   <p/>
   CriterianWrapper usage:
   <p/>
   <img src="doc-files/CriterionWrapper11.png">

*/
public class  CriterionWrapper implements Cloneable{
    private String type;
    private Hashtable params =new Hashtable();

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void addParam(String name,Object value){
        params.put(name,value);
    }

    public Object getParam(String name){
       return params.get(name);
    }

    /** Creates new CriterionWrapper[] array with size have increased for one position.
     * Then put given criterion into the last cell of new array. */
    public static CriterionWrapper[] appendToCriterions(CriterionWrapper[] criterions, CriterionWrapper criterion){
        if(criterions == null)
            criterions = new CriterionWrapper[0];
        if(criterion == null)
            return criterions;
        CriterionWrapper[] newCriterions = new CriterionWrapper[criterions.length + 1];
        System.arraycopy(criterions, 0, newCriterions, 0, criterions.length);
        newCriterions[newCriterions.length-1] = criterion;
        return newCriterions;
    }
    
    public Object clone() throws CloneNotSupportedException {
       return super.clone();
    }
}
