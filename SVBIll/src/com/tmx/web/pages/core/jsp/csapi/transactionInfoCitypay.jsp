<%@ taglib prefix="nested" uri="http://jakarta.apache.org/struts/tags-nested" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/tld/tmx-controls.tld" prefix="tmx_ctrl" %>

<html:html>
<head>
</head>
    <html:form action="/transactionInfoCitypayAction">

        <div width="100%" align="left">
            CitypayBalance
            <tmx_ctrl:editBox property="editBoxCitypayBalance"/>
            <tmx_ctrl:button property="onRefreshBalance"/>
        </div>

        <%--
        <table border=0 align="center">
            <tr>
                <td class="dataLabel">BillTransaction</td>
                <td class="dataLabel">
                    <tmx_ctrl:editBox property="editBoxBillTransaction" />
                </td>
            </tr>
            <tr>
                <td>ClientTransaction</td>
                <td class="dataLabel">
                    <tmx_ctrl:editBox property="editBoxCliTransaction"/>
                </td>
            </tr>
            <tr>
                <td>StatusCode</td>
                <td class="dataLabel">
                    <tmx_ctrl:editBox property="editBoxStatusCode"/>
                </td>
            </tr>
            <tr>
                <td>StatusMessage</td>
                <td class="dataLabel">
                    <tmx_ctrl:editBox property="editBoxStatusMessage"/>
                </td>
            </tr>
            <tr>
                <td>TransactionTime</td>
                <td class="dataLabel">
                    <tmx_ctrl:editBox property="editBoxTransactionTime"/>
                </td>
            </tr>
            <tr>
                <td>ClientTime</td>
                <td class="dataLabel">
                    <tmx_ctrl:editBox property="editBoxCientTime"/>
                </td>
            </tr>
            <tr>
                <td>AccountNumber</td>
                <td class="dataLabel">
                    <tmx_ctrl:editBox property="editBoxAccountNumber"/>
                </td>
            </tr>
            <tr>
                <td>Amount</td>
                <td class="dataLabel">
                    <tmx_ctrl:editBox property="editBoxAmount"/>
                </td>
            </tr>
            <tr>
                <td>Seller</td>
                <td class="dataLabel">
                    <tmx_ctrl:editBox property="editBoxSellerName"/>
                </td>
            </tr>
            <tr>
                <td>Terminal</td>
                <td class="dataLabel">
                    <tmx_ctrl:editBox property="editBoxTerminalNumber"/>
                </td>
            </tr>
            <tr>
                <td>Operator</td>
                <td class="dataLabel">
                    <tmx_ctrl:editBox property="editBoxOperatorName"/>
                </td>
            </tr>
            <tr>
                <td colspan="2" align="right">
                    <tmx_ctrl:button property="onCancelButton"/>
                    <tmx_ctrl:button property="onInfoButton"/>
                </td>
            </tr>
        </table>
        --%>

    </html:form>
<body>
</body>
</html:html>