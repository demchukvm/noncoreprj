package com.tmx.as.entities.iso8583.key;

import com.tmx.as.base.BasicEntity;

import java.io.Serializable;
import java.util.Date;

/**
  ISO Keys entity 
 */
public class Key extends BasicEntity {
    private Long keyId;
    private String terminalId;
    private Date currentMasterKeyUpdTime;
    private Integer currentMasterKeyIndex;
    private String currentMacGenKey;
    private String currentMacGenKeyEncrypted;
    private Date currentMacGenKeyUpdTime;
    private String newMacGenKey;
    private Date newMacGenKeyCreatTime;
    private String macGenKeyEncryptAlg;

    public Long getKeyId() {
        return keyId;
    }

    public void setKeyId(Long keyId) {
        this.keyId = keyId;
    }

    public String getTerminalId() {
        return terminalId;
    }

    public void setTerminalId(String terminalId) {
        this.terminalId = terminalId;
    }

    public Date getCurrentMasterKeyUpdTime() {
        return currentMasterKeyUpdTime;
    }

    public void setCurrentMasterKeyUpdTime(Date currentMasterKeyUpdTime) {
        this.currentMasterKeyUpdTime = currentMasterKeyUpdTime;
    }

    public Integer getCurrentMasterKeyIndex() {
        return currentMasterKeyIndex;
    }

    public void setCurrentMasterKeyIndex(Integer currentMasterKeyIndex) {
        this.currentMasterKeyIndex = currentMasterKeyIndex;
    }

    public String getCurrentMacGenKey() {
        return currentMacGenKey;
    }

    public void setCurrentMacGenKey(String currentMacGenKey) {
        this.currentMacGenKey = currentMacGenKey;
    }

    public String getCurrentMacGenKeyEncrypted() {
        return currentMacGenKeyEncrypted;
    }

    public void setCurrentMacGenKeyEncrypted(String currentMacGenKeyEncrypted) {
        this.currentMacGenKeyEncrypted = currentMacGenKeyEncrypted;
    }

    /** first 2 groups of 8 bytes of encrypted key */

    public String getCurrentMacGenKeyEncrypted24b(){
        return currentMacGenKeyEncrypted == null ? null : currentMacGenKeyEncrypted.substring(0, 16 * 2);
    }


    public Date getCurrentMacGenKeyUpdTime() {
        return currentMacGenKeyUpdTime;
    }

    public void setCurrentMacGenKeyUpdTime(Date currentMacGenKeyUpdTime) {
        this.currentMacGenKeyUpdTime = currentMacGenKeyUpdTime;
    }

    public String getNewMacGenKey() {
        return newMacGenKey;
    }

    public void setNewMacGenKey(String newMacGenKey) {
        this.newMacGenKey = newMacGenKey;
    }

    public Date getNewMacGenKeyCreatTime() {
        return newMacGenKeyCreatTime;
    }

    public void setNewMacGenKeyCreatTime(Date newMacGenKeyCreatTime) {
        this.newMacGenKeyCreatTime = newMacGenKeyCreatTime;
    }

    public String getMacGenKeyEncryptAlg() {
        return macGenKeyEncryptAlg;
    }

    public void setMacGenKeyEncryptAlg(String macGenKeyEncryptAlg) {
        this.macGenKeyEncryptAlg = macGenKeyEncryptAlg;
    }

    public Serializable getId() {
        return getKeyId();
    }

    public int getValid() {
        return valid;
    }

    public void setValid(int valid) {
        this.valid = valid;
    }
}