package com.tmx.web.forms.core.balance.seller_balance;

import com.tmx.as.entities.bill.seller.Seller;
import com.tmx.as.entities.general.user.User;
import com.tmx.web.controls.ScrollBox;
import com.tmx.web.controls.table.Parameter;
import com.tmx.web.controls.table.Table;
import com.tmx.web.controls.table.ControlManagerTable;
import com.tmx.web.forms.core.balance.AbstractManageBalancesForm;
import com.tmx.web.forms.core.base.AbstractFormFactory;

public class ManageSellerBalancesForm extends AbstractManageBalancesForm {

    protected void bindSpecificData(User loggedInUser) throws Throwable {
//        ScrollBox scrollBoxSellers = (ScrollBox)getBalancesTable().getParameter("by_seller_id", "seller_id").getControl();
//        if(scrollBoxSellers!=null){
//            scrollBoxSellers.bind(new SellerManager().getNestedSellersInSeller(loggedInUser.getSellerOwner()));
//        }
        AbstractFormFactory.getInstance(loggedInUser).setUpForm(this);


    }

    protected void init(){
        super.init();
        initTable();
        initButtonActions();
        initFilters();
    }

    private void initTable() {
        ControlManagerTable table = new ControlManagerTable("query(query_type=retrieve_all," +
                "entity_class=com.tmx.as.entities.bill.balance.SellerBalance," +
                "table_attr=seller," +
                "order(name=id,type=asc)," +
                "page_size=15" +
                ")");
        table.setRefreshActionName("/core/balance/seller_balance/manageSellerBalances");
        setBalancesTable(table);
    }

    private void initButtonActions() {
        String action = "/core/balance/seller_balance/infoSellerBalance";
        getOnCreateButton().setAction(action);
        getOnEditButton().setAction(action);
        getOnDeleteButton().setAction(action);
    }

    private void initFilters(){
        /*ScrollBox scrollBoxSellers = new ScrollBox() {
            protected String getKey(Object dataListElement) {
                return ((Seller)dataListElement).getSellerId().toString();
            }

            protected String getValue(Object dataListElement) {
                return ((Seller)dataListElement).getName();
            }
        };*/

        scrollBoxSellerOwner.setSelectedKey("________NULL_KEY_________");
        getBalancesTable().addParameterToFilter("by_seller_id", new Parameter("seller_id", scrollBoxSellerOwner));
       //  getBalancesTable().addParameterToFilter("by_seller_id", new Parameter("seller_id", scrollBoxSellers));
        getBalancesTable().setCriterionFactory(getCriterionFactory());
    }

}
