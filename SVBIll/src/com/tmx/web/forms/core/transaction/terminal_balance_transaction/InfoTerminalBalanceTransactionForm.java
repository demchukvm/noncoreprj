package com.tmx.web.forms.core.transaction.terminal_balance_transaction;

import com.tmx.web.forms.core.transaction.AbstractInfoTransactionForm;
import com.tmx.web.controls.EditBox;
import com.tmx.web.controls.TextArea;
import com.tmx.as.entities.bill.transaction.TerminalBalanceTransaction;
import com.tmx.as.entities.bill.terminal.Terminal;


public class InfoTerminalBalanceTransactionForm extends AbstractInfoTransactionForm {

    private EditBox editBoxTerminalSN;// terminal serial number
    private EditBox editBoxTermRegDate;//terminal registartion date
    private TextArea textAreaTerminalBalancePaymentReason;
    private TextArea textAreaTerminalBalanceDescription;

    protected void init() {
        //initialze and register controls
        super.init();

        editBoxTerminalSN = new EditBox();
        editBoxTerminalSN.setReadonly(true);

        editBoxTermRegDate = new EditBox();
        editBoxTermRegDate.setReadonly(true);

        textAreaTerminalBalancePaymentReason = new TextArea();
        textAreaTerminalBalancePaymentReason.setReadonly(true);

        textAreaTerminalBalanceDescription= new TextArea();
        textAreaTerminalBalanceDescription.setReadonly(true);

        onCancelButton.setAction("/core/transactions/terminal_transaction/infoTerminalTransaction");
        onCancelButton.setCommand("preloadForCancel");
    }


    public void bindData(TerminalBalanceTransaction terminalBalanceTransaction) {
        super.bindData(terminalBalanceTransaction);

        //for terminal
        Terminal terminal = terminalBalanceTransaction.getTerminal();
        if(terminal != null){
            editBoxTerminalSN.setValue(terminal.getSerialNumber());
            editBoxTermRegDate.setValue(terminal.getRegistrationDate().toString());
        }
        if (terminalBalanceTransaction.getTransactionSupport() != null) { //it`s possible that TS_TransactionSupportId == null in DB
            textAreaTerminalBalancePaymentReason.setValue(terminalBalanceTransaction.getTransactionSupport().getPaymentReason());
            textAreaTerminalBalanceDescription.setValue(terminalBalanceTransaction.getTransactionSupport().getDescription());
            editBoxExecutedUser.setValue(terminalBalanceTransaction.getTransactionSupport().getUserActor().getLogin());
        }
        
    }

    public EditBox getEditBoxTerminalSN() {
        return editBoxTerminalSN;
    }

    public void setEditBoxTerminalSN(EditBox editBoxTerminalSN) {
        this.editBoxTerminalSN = editBoxTerminalSN;
    }

    public EditBox getEditBoxTermRegDate() {
        return editBoxTermRegDate;
    }

    public void setEditBoxTermRegDate(EditBox editBoxTermRegDate) {
        this.editBoxTermRegDate = editBoxTermRegDate;
    }

    public TextArea getTextAreaTerminalBalancePaymentReason() {
        return textAreaTerminalBalancePaymentReason;
    }

    public void setTextAreaTerminalBalancePaymentReason(TextArea textAreaTerminalBalancePaymentReason) {
        this.textAreaTerminalBalancePaymentReason = textAreaTerminalBalancePaymentReason;
    }

    public TextArea getTextAreaTerminalBalanceDescription() {
        return textAreaTerminalBalanceDescription;
    }

    public void setTextAreaTerminalBalanceDescription(TextArea textAreaTerminalBalanceDescription) {
        this.textAreaTerminalBalanceDescription = textAreaTerminalBalanceDescription;
    }
}
