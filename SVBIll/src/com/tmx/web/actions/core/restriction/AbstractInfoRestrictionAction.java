package com.tmx.web.actions.core.restriction;

import com.tmx.web.base.*;
import com.tmx.web.forms.core.restriction.AbstractInfoRestrictionForm;
import com.tmx.web.forms.core.restriction.AbstractManageRestrictionsForm;
import com.tmx.web.forms.core.restriction.seller_restriction.ManageSellerRestrictionsForm;
import com.tmx.web.actions.core.AbstractSvbillAction;
import com.tmx.web.controls.table.Parameter;
import com.tmx.web.controls.ScrollBox;

import com.tmx.as.entities.bill.restriction.AbstractRestriction;
import com.tmx.as.entities.bill.function.FunctionType;
import com.tmx.as.entities.general.user.User;
import com.tmx.as.exceptions.DatabaseException;
import com.tmx.as.blogic.FunctionManager;
import com.tmx.as.blogic.BlogicException;
import com.tmx.as.blogic.restriction.AbstractRestrictionManager;
import com.tmx.util.InitException;
import com.tmx.beng.base.BillException;

import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForm;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.util.List;
import java.util.Collections;
import java.util.ArrayList;

abstract public class AbstractInfoRestrictionAction extends AbstractSvbillAction {

    abstract protected void preloadForCreate(AbstractInfoRestrictionForm form, AbstractRestriction restriction, User loggedInUser) throws DatabaseException, WebException;

    abstract protected void preloadForUpdate(AbstractInfoRestrictionForm form, AbstractRestriction restriction, User loggedInUser)
            throws DatabaseException, WebException;

    abstract protected void preloadForUpdate(AbstractInfoRestrictionForm form, AbstractRestriction restriction)
            throws DatabaseException;

    abstract protected AbstractRestrictionManager getRestrictionManager();

    public ActionForward preloadForDelete(ActionMapping mapping,
                                          ActionForm form,
                                          HttpServletRequest request,
                                          HttpServletResponse response) throws Throwable {
        Roadmap roadmap = getRoadmap();
        roadmap.collectParameter("/core/message/confirmation", "leftMenuName", "restrictions");
        roadmap.setPageForward("confirmation", "onOk", "local:" + this.getName() + "#" + "manageDeleteAction");
        roadmap.setPageForward("confirmation", "onCancel", "local:" + this.getName() + "#" + "manageRestricionsAction");
        return mapping.findForward("confirmation");
    }

    public ActionForward delete(ActionMapping mapping,
                                ActionForm form,
                                HttpServletRequest request,
                                HttpServletResponse response) throws Throwable {
        AbstractInfoRestrictionForm restForm = (AbstractInfoRestrictionForm) verifyForm(form, AbstractInfoRestrictionForm.class);
        getRestrictionManager().deleteRestriction(restForm.getRestrictionId());
        return mapping.findForward("manageRestricionsAction");
    }

    public ActionForward preloadForCreate(ActionMapping mapping,
                                          ActionForm form,
                                          HttpServletRequest request,
                                          HttpServletResponse response) throws Throwable {
        AbstractInfoRestrictionForm restForm = (AbstractInfoRestrictionForm) verifyForm(form, AbstractInfoRestrictionForm.class);
        restForm.bindData(getLoggedInUser(request));
        restForm.reset();
        preloadForCreate(restForm, null, getLoggedInUser(request));
        return mapping.findForward("infoRestrictionPage");
    }

    public ActionForward preloadForUpdate(ActionMapping mapping,
                                          ActionForm form,
                                          HttpServletRequest request,
                                          HttpServletResponse response) throws Throwable {
        AbstractInfoRestrictionForm restForm = (AbstractInfoRestrictionForm) verifyForm(form, AbstractInfoRestrictionForm.class);
        AbstractRestriction rest = getRestrictionManager().getRestriction(restForm.getRestrictionId());
        restForm.setFuncInstanceId(rest.getFunctionInstance().getFunctionInstanceId());
        restForm.bindData(rest, getLoggedInUser(request));
        Long id = rest.getFunctionInstance().getFunctionInstanceId();
        restForm.getFuncParametrsTable().addParameterToFilter("by_function_instance_id", new Parameter("id", id));
        restForm.getFuncParametrsTable().refresh();
        preloadForUpdate(restForm, rest, getLoggedInUser(request));
        List params = new FunctionManager().getActualFunctionParams(getFuncTypeId(restForm));
        if (params != null) {
            restForm.getDefaultFuncParametrsTable().setData(params);
            if (restForm.getFuncParametrsTable().getData().size() == 0) {
                restForm.getFuncParametrsTable().setData(params);
            }
        }
        validateActualFuncParams(restForm);
        return mapping.findForward("infoRestrictionPage");
    }

    public ActionForward preloadForUpdateIR(ActionMapping mapping,
                                            ActionForm form,
                                            HttpServletRequest request,
                                            HttpServletResponse response) throws Throwable {
        AbstractInfoRestrictionForm restForm = (AbstractInfoRestrictionForm) verifyForm(form, AbstractInfoRestrictionForm.class);
        setOnApplyButtonAction("/core/restriction/seller_restriction/sellerRestrictionParameter");
        setOnCancelButtonAction("/core/restriction/seller_restriction/infoSellerRestriction");
        setOnSaveButtonAction("/core/restriction/seller_restriction/sellerRestrictionParameter");
        AbstractRestriction rest = getRestrictionManager().getRestriction(restForm.getIncomingRestrictionId());
        Long id = rest.getFunctionInstance().getFunctionInstanceId();
        restForm.setFuncInstanceId(id);
        restForm.bindData(rest, getLoggedInUser(request));

        restForm.getFuncParametrsTable().addParameterToFilter("by_function_instance_id", new Parameter("id", id));
        restForm.getFuncParametrsTable().refresh();

        preloadForUpdate(restForm, rest, getLoggedInUser(request));
//        preloadForUpdate(restForm, rest);
        List params = new FunctionManager().getActualFunctionParams(getFuncTypeId(restForm));
        restForm.getDefaultFuncParametrsTable().setData(params);
        validateActualFuncParams(restForm);
        return mapping.findForward("infoRestrictionPage");
    }

    protected void setOnSaveButtonAction(String action) {
        HttpServletRequest request = RequestEnvironment.currentReqContext().getRequest();
        if (request != null)
            SessionEnvironment.setAttr(request.getSession(true), "onSave", action);
    }

    protected void setOnApplyButtonAction(String action) {
        HttpServletRequest request = RequestEnvironment.currentReqContext().getRequest();
        if (request != null)
            SessionEnvironment.setAttr(request.getSession(true), "onApply", action);
    }

    protected void setOnCancelButtonAction(String action) {
        HttpServletRequest request = RequestEnvironment.currentReqContext().getRequest();
        if (request != null)
            SessionEnvironment.setAttr(request.getSession(true), "onCancel", action);
    }


    public ActionForward save(ActionMapping mapping,
                              ActionForm form,
                              HttpServletRequest request,
                              HttpServletResponse response) throws Throwable {
        AbstractInfoRestrictionForm restForm = (AbstractInfoRestrictionForm) form;
        restForm.validate();
        AbstractRestriction restriction = restForm.populateFromControls();
        validateActualFuncParams(restForm);
        getRestrictionManager().saveRestriction(restriction);
        restForm.setRestrictionId(restriction.getRestrictionId());
//        restForm.getEditBoxRestrictionId().setValue(restriction.getRestrictionId().toString());
        preloadForUpdate(mapping, form, request, response);
        restriction = restForm.populateFromControls();
        getRestrictionManager().saveRestriction(restriction);
        return mapping.findForward("manageRestricionsAction");
    }

    public ActionForward apply(ActionMapping mapping,
                               ActionForm form,
                               HttpServletRequest request,
                               HttpServletResponse response) throws Throwable {
        save(mapping, form, request, response);
        return mapping.findForward("infoRestrictionPage");
    }

    public ActionForward refreshFuncParametrsParamTable(ActionMapping mapping,
                                                        ActionForm form,
                                                        HttpServletRequest request,
                                                        HttpServletResponse response) throws Throwable {
        AbstractInfoRestrictionForm restrictionForm = (AbstractInfoRestrictionForm) form;
        return mapping.findForward("infoRestrictionPage");
    }

    public ActionForward refreshDefaultParams(ActionMapping mapping,
                                              ActionForm form,
                                              HttpServletRequest request,
                                              HttpServletResponse response) throws Throwable {
        AbstractInfoRestrictionForm restrictionForm = (AbstractInfoRestrictionForm) verifyForm(form, AbstractInfoRestrictionForm.class);
        restrictionForm.validate(Collections.singletonList(restrictionForm.getBrowseBoxFunctionTypes()));
        List params = new FunctionManager().getActualFunctionParams(getFuncTypeId(restrictionForm));
//        restrictionForm.getDefaultFuncParametrsTable().setData(new ArrayList(params));
        restrictionForm.getDefaultFuncParametrsTable().setData(params);
        FunctionType functionType = new FunctionManager().getFunctionType(getFuncTypeId(restrictionForm));
        restrictionForm.getTextAreaFuncDescription().setValue(functionType.getDescription());
        restrictionForm.getFuncParametrsTable().setData(params);
        validateActualFuncParams(restrictionForm);
        return mapping.findForward("infoRestrictionPage");
    }

    public ActionForward createParam(ActionMapping mapping,
                                     ActionForm form,
                                     HttpServletRequest request,
                                     HttpServletResponse response) throws Throwable {
        AbstractInfoRestrictionForm restrictionForm = (AbstractInfoRestrictionForm) form;
        restrictionForm.validate(Collections.singletonList(restrictionForm.getBrowseBoxFunctionTypes()));
        return mapping.findForward("createParam");
    }

    public ActionForward updateParam(ActionMapping mapping,
                                     ActionForm form,
                                     HttpServletRequest request,
                                     HttpServletResponse response) throws Throwable {
        AbstractInfoRestrictionForm restrictionForm = (AbstractInfoRestrictionForm) form;
        restrictionForm.validate(Collections.singletonList(restrictionForm.getBrowseBoxFunctionTypes()));
        return mapping.findForward("updateParam");
    }

    public ActionForward deleteParam(ActionMapping mapping,
                                     ActionForm form,
                                     HttpServletRequest request,
                                     HttpServletResponse response) throws Throwable {
        AbstractInfoRestrictionForm restrictionForm = (AbstractInfoRestrictionForm) form;
        restrictionForm.validate(Collections.singletonList(restrictionForm.getBrowseBoxFunctionTypes()));
        return mapping.findForward("deleteParam");
    }

    public ActionForward update(ActionMapping mapping,
                                ActionForm form,
                                HttpServletRequest request,
                                HttpServletResponse response) throws Throwable {
        AbstractInfoRestrictionForm restrictionForm = (AbstractInfoRestrictionForm) form;
        restrictionForm.validate(Collections.singletonList(restrictionForm.getBrowseBoxFunctionTypes()));
        validateActualFuncParams(restrictionForm);
        return mapping.findForward("infoRestrictionPage");
    }


    private Long getFuncTypeId(AbstractInfoRestrictionForm restForm) {
        if (restForm.getBrowseBoxFunctionTypes().getKey() != null && !"".equals(restForm.getBrowseBoxFunctionTypes().getKey()))
            return new Long(restForm.getBrowseBoxFunctionTypes().getKey());
        return null;
    }

    //on scrollBoxFunctionTypes change
    public ActionForward defaultAction(ActionMapping mapping,
                                       ActionForm form,
                                       HttpServletRequest request,
                                       HttpServletResponse response,
                                       String methodName) throws Throwable {
//        AbstractInfoRestrictionForm restForm = (AbstractInfoRestrictionForm) verifyForm(form, AbstractInfoRestrictionForm.class);

//        getRoadmap().collectParameter("/core/restriction/seller_restriction/sellerRestrictionParameter", "infoAction", getName());

//        restForm.bindFunctionTypeDescription();
        return mapping.findForward("infoRestrictionPage");
    }

    private void validateActualFuncParams(AbstractInfoRestrictionForm restForm)
            throws ValidationException, BlogicException, InitException, BillException {
        List errors = new FunctionManager().validateActualFunctionParams(getFuncTypeId(restForm), restForm.getFuncParametrsTable().getData());
        //if params not success
        if (errors != null && !errors.isEmpty()) {
            restForm.getParamErrorTable().setData(errors);
            throw new ValidationException(null);
        } else
            restForm.getParamErrorTable().clean();
    }

}
