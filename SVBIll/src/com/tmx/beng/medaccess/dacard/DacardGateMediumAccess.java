package com.tmx.beng.medaccess.dacard;

import com.tmx.beng.medaccess.MediumConnection;
import com.tmx.beng.medaccess.GateException;
import com.tmx.beng.medaccess.MediumAccess;

/**
 */
public class DacardGateMediumAccess implements MediumAccess {

    public void init() {
    }

    public MediumConnection getConnection() throws GateException {
        return new DacardGateMediumConnection();
    }
}
