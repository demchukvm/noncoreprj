package com.tmx.beng.medaccess.umc;

import com.tmx.beng.medaccess.MediumAccess;
import com.tmx.beng.medaccess.MediumConnection;
import com.tmx.beng.medaccess.GateException;

/**
  Medium Access to obtain UMC medium connection
 */
public class UmcGateMediumAccess implements MediumAccess {

    public void init() {
    }

    public MediumConnection getConnection() throws GateException {
        return new UmcGateMediumConnection();
    }
}
