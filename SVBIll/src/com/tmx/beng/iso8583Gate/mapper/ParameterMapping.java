package com.tmx.beng.iso8583Gate.mapper;

import java.util.Map;
import java.util.HashMap;

/**

 */
public class ParameterMapping {
    /** originalName - > parameter */
    private Map paramMap = null;
    /** mappedName -> parameter */
    private Map reverseParamMap = null;
    private String protocolVersion = null;

    public ParameterMapping(){
        paramMap = new HashMap();
        reverseParamMap = new HashMap();
    }

    public String getProtocolVersion() {
        return protocolVersion;
    }

    public void setProtocolVersion(String protocolVersion) {
        this.protocolVersion = protocolVersion;
    }

    public void addParam(Param param){
        paramMap.put(param.getOriginalName(), param);
        reverseParamMap.put(param.getMappedName(), param);
    }

    public Param getParamByOriginalName(String originalName){
        return (Param)paramMap.get(originalName);
    }

    public Param getParamByMappedName(String mappedName){
        return (Param)reverseParamMap.get(mappedName);
    }
}
