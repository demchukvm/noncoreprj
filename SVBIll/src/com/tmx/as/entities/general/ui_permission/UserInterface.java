package com.tmx.as.entities.general.ui_permission;

import com.tmx.as.base.BasicEntity;
import java.io.Serializable;

/**
 */
public class UserInterface extends BasicEntity {
    private Long userInterfaceId;
    private String name;
    private UserInterface parent;
    private UserInterfaceType type;


    public Long getUserInterfaceId() {
        return userInterfaceId;
    }

    public void setUserInterfaceId(Long userInterfaceId) {
        this.userInterfaceId = userInterfaceId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public UserInterface getParent() {
        return parent;
    }

    public void setParent(UserInterface parent) {
        this.parent = parent;
    }

    public UserInterfaceType getType() {
        return type;
    }

    public void setType(UserInterfaceType type) {
        this.type = type;
    }

    public Serializable getId() {
        return getUserInterfaceId();
    }

    public int getValid() {
        return valid;
    }

    public void setValid(int valid) {
        this.valid = valid;
    }
}