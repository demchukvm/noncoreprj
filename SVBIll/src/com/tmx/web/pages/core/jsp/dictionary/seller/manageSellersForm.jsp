<%@ page language="java" pageEncoding="UTF-8" contentType="text/html; charset=utf-8" isELIgnored="false" %>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-nested" prefix="nested" %>
<%@ taglib uri="http://tmx.com/taglib/tmx-controls" prefix="tmx_ctrl" %>
<%@ taglib uri="http://tmx.com/taglib/tmx-logic" prefix="tmx_logic" %>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html" %>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean" %>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>

<html:form action="/core/dictionary/seller/manageSellers">
<table width="100%" border="0">
    <!-- BUTTONS -->
    <tr>
        <td style="text-align: left">
            <tmx_ctrl:button property="onCreateButton"/>
            <tmx_ctrl:button property="onEditButton"/>
            <tmx_ctrl:button property="onDeleteButton"/>
        </td>
    </tr>
    <!-- FILTER SET -->
    <tr>
        <td valign="top" width="100%" style="text-align: left">
            <tmx_ctrl:panelFilterSet tableName="sellersTable">
            <table>
                <tr>
                    <td class="dataLabel">
                        <tmx_ctrl:writeEntityLabel
                                attrName="name"
                                entityClassName="com.tmx.as.entities.bill.seller.Seller"/>:
                    </td>
                    <td class="dataField">
                        <tmx_ctrl:editBox property="editBoxNameFilter" />
                    </td>

                    <td class="dataLabel">
                        <tmx_ctrl:writeEntityLabel
                                attrName="code"
                                entityClassName="com.tmx.as.entities.bill.seller.Seller"/>:
                    </td>
                    <td class="dataField">
                        <tmx_ctrl:editBox property="editBoxCodeFilter" />
                    </td>

                    <td class="dataLabel">
                        <tmx_ctrl:writeEntityLabel
                                attrName="parentSeller"
                                entityClassName="com.tmx.as.entities.bill.seller.Seller"/>:
                    </td>
                    <td class="dataField">
                        <tmx_ctrl:selectBox property="scrollBoxParentSeller" />
                    </td>

                    <td class="dataLabel">
                        <tmx_ctrl:writeEntityLabel
                                attrName="active"
                                entityClassName="com.tmx.as.entities.bill.seller.Seller"/>:
                    </td>

                    <td class="dataField">
                        <tmx_ctrl:checkBox property="sellersTable.filterSet.filter(by_active).filterEnabler" />
                    </td>

                    <td class="dataLabel">
                        <tmx_ctrl:writeEntityLabel
                                attrName="root"
                                entityClassName="com.tmx.as.entities.bill.seller.Seller"/>:
                    </td>

                    <td class="dataField">
                        <tmx_ctrl:checkBox property="sellersTable.filterSet.filter(by_rootSeller).filterEnabler" />
                    </td>
                </tr>
            </table>
        </tmx_ctrl:panelFilterSet>
        </td>
    </tr>
    <!-- TABLE -->
    <tr>
        <td width="100%">
            <table class='one_outer_cell_table' cellspacing='0' cellpadding='0' width="100%">
                <tr>
                    <td>
                        <table cellspacing='1' cellpadding='1' width="100%">
                            <!-- Table header -->
                            <tr>

                                <th class="column_header_cell">
                                    <bean:message bundle="core.labels" key="core.common.th.choose"/>
                                </th>
                                
                                <th class="column_header_cell" width="110">
                                    <tmx_ctrl:imgTableOrdering tableName="sellersTable"
                                                               orderByFieldName="code"
                                                               labelBundle="as:com.tmx.as.entities.bill.seller.Seller"
                                                               labelKey="code"/>
                                </th>

                                <th class="column_header_cell" width="110">
                                    <tmx_ctrl:imgTableOrdering tableName="sellersTable"
                                                               orderByFieldName="name"
                                                               labelBundle="as:com.tmx.as.entities.bill.seller.Seller"
                                                               labelKey="name"/>
                                </th>

                                <th class="column_header_cell" width="110">
                                    <tmx_ctrl:imgTableOrdering tableName="sellersTable"
                                                               orderByFieldName="parentSeller"
                                                               labelBundle="as:com.tmx.as.entities.bill.seller.Seller"
                                                               labelKey="parentSeller"/>
                                </th>

                                <th class="column_header_cell" width="110">
                                    <tmx_ctrl:imgTableOrdering tableName="sellersTable"
                                                               orderByFieldName="active"
                                                               labelBundle="as:com.tmx.as.entities.bill.seller.Seller"
                                                               labelKey="active"/>
                                </th>

                                <th class="column_header_cell" width="110">
                                    <tmx_ctrl:imgTableOrdering tableName="sellersTable"
                                                               orderByFieldName="root"
                                                               labelBundle="as:com.tmx.as.entities.bill.seller.Seller"
                                                               labelKey="root"/>
                                </th>

                                <th class="column_header_cell" width="110">
                                    <tmx_ctrl:imgTableOrdering tableName="sellersTable"
                                                               orderByFieldName="registrationDate"
                                                               labelBundle="as:com.tmx.as.entities.bill.seller.Seller"
                                                               labelKey="registrationDate"/>
                                </th>
                            </tr>
                            <nested:iterate id="seller" property="sellersTable.data" type="com.tmx.as.entities.bill.seller.Seller">
                                <tr>
                                    <td class='column_data_cell'>
                                        <input type="radio" name="selectedSellerId" value="<c:out value="${seller.id}" />"
                                               checked="false">
                                    </td>
                                    <td class='column_data_cell'>
                                        <c:out value="${seller.code}" />
                                    </td>
                                    <td class='column_data_cell'>
                                        <c:out value="${seller.name}" />
                                    </td>
                                    <td class='column_data_cell'>
                                        <c:out value="${seller.parentSeller.name}" />
                                    </td>
                                    <td class='column_data_cell'>
                                        <tmx_ctrl:checkSign name="seller" property="active"/>
                                    </td>
                                    <td class="column_data_cell">
                                        <tmx_ctrl:checkSign name="seller" property="root"/>
                                    </td>
                                    <td class='column_data_cell'>
                                        <fmt:formatDate value="${seller.registrationDate}" pattern="yyyy/MM/dd HH:mm:ss"/>
                                    </td>
                                </tr>
                            </nested:iterate>
                            <!-- Empty row -->
                            <nested:define id="tableSize" property="sellersTable.resultSize" type="java.lang.Integer"/>
                            <c:if test="${tableSize <= 0}">
                                <tr>
                                    <td class='column_data_cell' colspan="7" align="center">
                                        <bean:message bundle="core.labels" key="core.common.th.empty_table" />
                                    </td>
                                </tr>
                            </c:if>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td style="text-align: right">
            <tmx_ctrl:imgTablePaging maxBookmarkCount="10" tableName="sellersTable"/>
        </td>
    </tr>
    <!-- BUTTONS -->
    <tr>
        <td style="text-align: left">
            <tmx_ctrl:button property="onCreateButton"/>
            <tmx_ctrl:button property="onEditButton"/>
            <tmx_ctrl:button property="onDeleteButton"/>
        </td>
    </tr>
</table>
</html:form>