package com.tmx.beng.csapi.java.base;

import com.tmx.beng.access.Connection;
import com.tmx.beng.base.BillingMessage;

import java.util.Date;


public class GenVoucherReq extends DefaultClientReq<GenVoucherResp>{
    
    GenVoucherReq(Connection connect) {
        super(connect);
        requestMsg.setOperationName(BillingMessage.O_GEN_VOUCHER);
    }

    protected GenVoucherResp handleResponseMessage(BillingMessage responseMsg) {
        GenVoucherResp resp = new GenVoucherResp();
        resp.setStatusCode(responseMsg.getAttributeString(BillingMessage.STATUS_CODE));
        final String waitTime = responseMsg.getAttributeString(BillingMessage.WAIT_TIME);
        resp.setWaitTime(waitTime != null ? Long.parseLong(waitTime) : null);
        resp.setStatusMessage(responseMsg.getAttributeString(BillingMessage.STATUS_MESSAGE));
        resp.setNominal(responseMsg.getAttributeString(BillingMessage.VOUCHER_NOMINAL));
        resp.setCode(responseMsg.getAttributeString(BillingMessage.VOUCHER_CODE));
        resp.setSecretCode(responseMsg.getAttributeString(BillingMessage.VOUCHER_SECRET_KEY));
        resp.setBestBeforeDate((Date)responseMsg.getAttribute(BillingMessage.VOUCHER_BEST_BEFORE_DATE));
        resp.setPrice(resp.getNominal() != null ? Double.parseDouble(resp.getNominal()) : null);
        return resp;        
    }

    public void setServiceCode(String serviceCode) {
        requestMsg.setAttributeString(BillingMessage.SERVICE_CODE, serviceCode);
    }

    public void setVoucherNominal(String voucherNominal) {
        requestMsg.setAttributeString(BillingMessage.VOUCHER_NOMINAL, voucherNominal);
    }

}
