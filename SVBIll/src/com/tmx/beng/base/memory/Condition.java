package com.tmx.beng.base.memory;

/**

 */
public interface Condition {
    public boolean evaluate(InMemoryObject obj);
}
