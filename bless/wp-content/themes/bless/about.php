<?php
/*
  Template Name: About
 */
?>
<?php get_header();?>

<div class="about_currblock">

		<img src="<?php bloginfo('template_url');?>/images/about_shape<?php if(isset($_GET['lang']) & $_GET['lang'] == 'en'){echo '-eng';}?>.gif" alt="">

	<div class="left">
		<p style="font-size:12px;"><?php echo get_field('text_1');?></p>
	</div>
	
	<div class="right">
		<p><?php echo get_field('text_2');?></p>
	</div>

</div><!--.about_currblock-->


<div class="about_text">

	<div class="left">
	
	<img src="<?php bloginfo('template_url');?>/images/rules_shape<?php if(isset($_GET['lang']) & $_GET['lang'] == 'en'){echo '-eng';}?>.gif" alt="Принципи нашей работы" class="rules">
	
		<img src="<?php bloginfo('template_url');?>/images/rules_text_shape<?php if(isset($_GET['lang']) & $_GET['lang'] == 'en'){echo '-eng';}?>.gif" alt="Профессионализм, открытость, практичность" class="rules_text">

<p><?php echo get_field('text_3');?></p>

<p><?php echo get_field('text_4');?></p>
	</div>
	
	<div class="right">
		<img src="<?php bloginfo('template_url');?>/images/bless_ds812.png" alt="">
		
		<p><?php echo get_field('text_5');?></p>
	</div>

</div>

<?php get_footer(); ?>