package com.tmx.beng.iso8583Gate.mapper;

import com.tmx.beng.base.BillingMessageWritable;
import com.tmx.beng.base.BillingMessage;
import com.tmx.beng.iso8583Gate.pdu.RuntimeMessage;
import com.tmx.beng.iso8583Gate.pdu.IncorrectFormatException;
import com.tmx.as.entities.iso8583.key.Key;
import com.tmx.as.entities.iso8583.transaction.Iso8583Transaction;
import com.tmx.as.entities.iso8583.collation.Collation;
import com.tmx.as.exceptions.DatabaseException;

/**
 */
public interface BillingMapper {
    /** response code errors */
    public static final String ERR_TX_OK = "00";
    public static final String ERR_TX_NOTFOUND = "05";
    public static final String ERR_TX_UNKNOWN_ERROR = "06";
    public static final String ERR_TX_COLLATION_FAILED = "95";
    /** this error doesn't returned to terminal. it validated in basic processor and
     * trigger response suppression. */
    public static final String ERR_SUPRESS_RESPONSE = "-1";

    public Iso8583Transaction prepareTransaction(RuntimeMessage reqMsg) throws IncorrectFormatException;

    public Iso8583Transaction preSaveTransaction(RuntimeMessage reqMsg) throws IncorrectFormatException, DatabaseException;

    public void postSaveTransaction(Iso8583Transaction tx, BillingMessage respBM) throws IncorrectFormatException, DatabaseException;

    public Iso8583Transaction retrieveTransaction(RuntimeMessage reqMsg) throws IncorrectFormatException, DatabaseException;

    public void updateTransaction(Iso8583Transaction tx, BillingMessage respBM) throws DatabaseException;

    public String genCliTransactionNum(RuntimeMessage runMsg) throws IncorrectFormatException;

    public void assignRetrievalRefNum(Iso8583Transaction tx) throws DatabaseException;

    public void assignAuthIdResponse(Iso8583Transaction tx) throws DatabaseException;

    public void prepare0200Msg(RuntimeMessage reqMsg, BillingMessageWritable reqBM) throws IncorrectFormatException;

    public void prepare0210Msg(RuntimeMessage respMsg, BillingMessage respBM, Iso8583Transaction tx);

    public void prepare0100Msg(RuntimeMessage reqMsg, BillingMessageWritable reqBM) throws IncorrectFormatException;

    public void prepare0110Msg(RuntimeMessage respMsg, Iso8583Transaction tx);

    public void prepare0211Msg(RuntimeMessage respMsg, Iso8583Transaction tx);

    public void prepare0330Msg(RuntimeMessage respMsg, Iso8583Transaction tx);

    public void prepare0400Msg(RuntimeMessage reqMsg, BillingMessageWritable reqBM) throws IncorrectFormatException;

    public void prepare0410Msg(RuntimeMessage respMsg, BillingMessage respBM, Iso8583Transaction tx);

    public void prepare0510Msg(RuntimeMessage respMsg, Collation collation);

    public void prepare0530Msg(RuntimeMessage respMsg, Collation collation);

    public void prepare0810_960000Msg(RuntimeMessage respMsg, Key key);

}
