CREATE OR REPLACE FUNCTION SVBILL.CHANGE_BALLANCE(
   operator_id$	  		   SVBILL.OP_OPERATOR.OP_OPERATORID%TYPE,
   operator_amount$	  	   SVBILL.OB_OPERATOR_BALANCE.OB_AMOUNT%TYPE,
   seller_id$	  		   SVBILL.SL_SELLER.SL_SELLERID%TYPE,
   seller_amount$	  	   SVBILL.SB_SELLER_BALANCE.SB_AMOUNT%TYPE,
   terminal_id$	  		   SVBILL.TR_TERMINAL.TR_TERMINALID%TYPE,
   terminal_amount$	  	   SVBILL.TB_TERMINAL_BALANCE.TB_AMOUNT%TYPE
)RETURN NUMBER IS
   CURSOR operator_cur IS
      SELECT  
	  		  OB_AMOUNT,
	  		  OB_OPERATORBALANCEID
      FROM
	  	  	  OP_OPERATOR op,
			  OB_OPERATOR_BALANCE ob
	  WHERE
	  	   	  op.OB_PRIMARYBALANCEID = ob.OB_OPERATORBALANCEID AND
			  op.OP_OPERATORID = operator_id$
	  FOR UPDATE;			  			    
   CURSOR seller_cur IS
      SELECT  
	  		  SB_AMOUNT,
	  		  SB_SELLERBALANCEID
      FROM
	  	  	  SL_SELLER sl,
			  SB_SELLER_BALANCE sb
	  WHERE
	  	   	  sl.SB_PRIMARYBALANCEID = sb.sB_sellerBALANCEID AND
			  sl.sl_sellerID = seller_id$
	  FOR UPDATE;			  			    
   CURSOR terminal_cur IS
      SELECT  
	  		  TB_AMOUNT,
	  		  TB_TERMINALBALANCEID
      FROM
	  	  	  TR_TERMINAL t,
			  TB_TERMINAL_BALANCE tb
	  WHERE
	  	   	  t.TB_PRIMARYBALANCEID = tb.TB_TERMINALBALANCEID AND
			  t.TR_TERMINALID = terminal_id$
	  FOR UPDATE;			  			    

   op_r$	  	   operator_cur%ROWTYPE;
   sl_r$	  	   seller_cur%ROWTYPE;
   t_r$	  	   	   terminal_cur%ROWTYPE;
BEGIN
-- operator
	 OPEN operator_cur;
	 FETCH operator_cur INTO op_r$;
	 op_r$.OB_AMOUNT := op_r$.OB_AMOUNT - operator_amount$;
	 
	 Update OB_OPERATOR_BALANCE 
	 set OB_AMOUNT = op_r$.OB_AMOUNT 
	 where OB_OPERATORBALANCEID = op_r$.OB_OPERATORBALANCEID;
	 close operator_cur;
	 
-- seller
	 OPEN seller_cur;
	 FETCH seller_cur INTO sl_r$;
	 sl_r$.SB_AMOUNT := sl_r$.sb_AMOUNT - seller_amount$;
	 
	 Update sB_seller_BALANCE 
	 set sB_AMOUNT = sl_r$.sB_AMOUNT 
	 where sB_sellerBALANCEID = sl_r$.sB_sellerBALANCEID;
	 close seller_cur;
	 return 0;	 

-- terminal
	 OPEN terminal_cur;
	 FETCH terminal_cur INTO t_r$;
	 t_r$.TB_AMOUNT := t_r$.tb_AMOUNT - terminal_amount$;
	 
	 Update tB_terminal_BALANCE 
	 set tB_AMOUNT = t_r$.tB_AMOUNT 
	 where tB_terminalBALANCEID = t_r$.tB_terminalBALANCEID;
	 close terminal_cur;
	 return 0;	 
EXCEPTION
     WHEN OTHERS THEN
	 	  if operator_cur%isopen then 
		  close operator_cur;
		  end if;
	 	  if seller_cur%isopen then 
		  close seller_cur;
		  end if;
	 	  if terminal_cur%isopen then 
		  close terminal_cur;
		  end if;
       RAISE;
	   
END CHANGE_BALLANCE;



/
