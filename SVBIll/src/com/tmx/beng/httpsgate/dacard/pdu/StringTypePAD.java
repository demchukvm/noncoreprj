package com.tmx.beng.httpsgate.dacard.pdu;

import java.io.*;
import java.nio.charset.Charset;

/**

 */
public class StringTypePAD extends AbstractTypePAD<String> implements TypePAD<String>{
    final static Charset charset = Charset.forName("windows-1251");

    public void serialize(OutputStream os, String value) throws DacardPadException{
        try{
            os.write(value.getBytes(charset));
        }
        catch (IOException e){
            throw new DacardPadException("Failed to serialize String", e); 
        }
    }

    public String parse(InputStream is, short dataSize) throws DacardPadException{
        try{
            final byte[] data = new byte[dataSize];
            is.read(data, 0, dataSize);
            return new String(data, charset);
        }
        catch (IOException e){
            throw new DacardPadException("Failed to parse String from input stream", e);
        }
    }

    protected short resolveNumberSizeInBytes(String value) throws DacardPadException {
        return (short)value.getBytes().length; //lenght can't be greater than max value of short type
    }
}
