package com.tmx.web.taglib.html.ctrl;

import com.tmx.util.Configuration;

import javax.servlet.jsp.JspException;
import java.util.Date;
import java.text.SimpleDateFormat;

import org.apache.struts.taglib.TagUtils;

/**
 * Write text tag
 */
public class WriteTextTag extends AbstractControlTag {
    private String format = null;

    //-----------------------Properties

    public String getFormat() {
        return format;
    }

    public void setFormat(String format) {
        this.format = format;
    }

    //------------------------------Render methods
    protected int controlStartTag() throws JspException {
        StringBuffer resultHTML = new StringBuffer();
        Object textHolder = getPropertyObject();
        if(textHolder == null){
            resultHTML.append("");
        }
        else if(textHolder instanceof String){
            resultHTML.append(textHolder);
        }
        else if(textHolder instanceof Date){
            String dateFormat = null;
            final String defaultDateFormat = "dd.MM.yyyy HH:mm:ss";
            final String serverDateFormat = Configuration.getInstance().getProperty("datetime_format");
            if(getFormat() != null)
                dateFormat = getFormat();
            else if(serverDateFormat != null)
                dateFormat = serverDateFormat;
            else
                dateFormat = defaultDateFormat;

            SimpleDateFormat formatter = new SimpleDateFormat(dateFormat, getLocale());
            resultHTML.append(formatter.format((Date)textHolder));
        }
        else {
            resultHTML.append(textHolder.toString());
        }
        TagUtils.getInstance().write(pageContext, resultHTML.toString());
        return SKIP_BODY;
    }


}
