package com.tmx.web.actions.core;

import com.tmx.as.entities.bill.seller.Seller;
import com.tmx.as.entities.general.user.User;
import com.tmx.web.base.ApplicationEnvironment;
import com.tmx.web.base.BasicDispatchedAction;
import com.tmx.web.base.SecurityService;
import com.tmx.web.forms.core.base.AbstractFormFactory;

import javax.servlet.http.HttpServletRequest;


abstract public class AbstractSvbillAction extends BasicDispatchedAction {

    public static String TO_MANAGE_FORM_PARAM = "to_manage_form";
    
    protected User getLoggedInUser(HttpServletRequest request) throws com.tmx.as.exceptions.SecurityException {
        SecurityService securityService = (SecurityService) ApplicationEnvironment.getService(SecurityService.class);
        return securityService.getLoggedInUser(request.getSession());
    }
    protected Seller getAssociatedSeller(HttpServletRequest request) throws com.tmx.as.exceptions.SecurityException {
        SecurityService securityService = (SecurityService) ApplicationEnvironment.getService(SecurityService.class);
        return securityService.getLoggedInUser(request.getSession()).getSellerOwner();
    }
    protected Long getAssociatedSellerId(HttpServletRequest request) throws com.tmx.as.exceptions.SecurityException {
        SecurityService securityService = (SecurityService) ApplicationEnvironment.getService(SecurityService.class);
        return securityService.getLoggedInUser(request.getSession()).getSellerOwner().getSellerId();
    }

}
