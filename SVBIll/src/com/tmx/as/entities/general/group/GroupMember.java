package com.tmx.as.entities.general.group;

import com.tmx.as.base.BasicEntity;
import com.tmx.as.entities.general.user.User;

import java.io.Serializable;

/**
 *
 */
public class GroupMember extends BasicEntity {
    private Long groupMemberId;
    private User user;
    private Group group;

    public Long getGroupMemberId() {
        return groupMemberId;
    }

    public void setGroupMemberId(Long groupMemberId) {
        this.groupMemberId = groupMemberId;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Group getGroup() {
        return group;
    }

    public void setGroup(Group group) {
        this.group = group;
    }

    public Serializable getId() {
        return getGroupMemberId();
    }

    public int getValid() {
        return valid;
    }

    public void setValid(int valid) {
        this.valid = valid;
    }
}
