package com.tmx.as.base;

import com.tmx.as.resolver.ResolverException;

public class QueryParameterWrapper implements UnifiedPropertySetter{
    private String name;
    private Object value;
    private String type;
    private int index = -1;
    /** parameter direction used for stored procedures. Default direction is IN */
    private Direction direction = Direction.IN;
    /** JDBC type of parameter according to java.sql.Types */
    private int jdbcType = -1;
    private int jdbcScale = -1;
    /** User defined type name supported in some dbms (Oracle for example) */
    private String userDefinedType;

    public QueryParameterWrapper(){
    }

    /** for named params */
    public QueryParameterWrapper(String paramName, Object paramValue){
        this.setName(paramName);
        this.setValue(paramValue);
    }

    /** for named params */
    public QueryParameterWrapper(String paramName, Object paramValue, String userDefinedTypeName){
        this.setName(paramName);
        this.setValue(paramValue);
        this.setUserDefinedType(userDefinedTypeName);
    }

    /** for indexed params */
    public QueryParameterWrapper(int idx, Object paramValue){
        this.setIndex(idx);
        this.setValue(paramValue);
    }

    /** for indexed params */
    public QueryParameterWrapper(int idx, Object paramValue, String userDefinedTypeName){
        this.setIndex(idx);
        this.setValue(paramValue);
        this.setUserDefinedType(userDefinedTypeName);
    }

    public static QueryParameterWrapper createOutParameter(int idx, int jdbcType, int jdbcScale){
        QueryParameterWrapper param = new QueryParameterWrapper();
        param.setIndex(idx);
        param.setDirection(Direction.OUT);
        param.jdbcType = jdbcType;
        param.jdbcScale = jdbcScale;
        return param;
    }

    public static QueryParameterWrapper createOutParameter(int idx, int jdbcType){
        return createOutParameter(idx, jdbcType, -1);
    }

    public static QueryParameterWrapper createInParameter(int idx, int jdbcType, int jdbcScale){
        QueryParameterWrapper param = new QueryParameterWrapper();
        param.setIndex(idx);
        param.setDirection(Direction.IN);
        param.jdbcType = jdbcType;
        param.jdbcScale = jdbcScale;
        return param;
    }

    public static QueryParameterWrapper createInParameter(int idx, String userDefinedTypeName){
        QueryParameterWrapper param = new QueryParameterWrapper();
        param.setIndex(idx);
        param.setDirection(Direction.IN);
        param.userDefinedType = userDefinedTypeName;
        return param;
    }

    public static QueryParameterWrapper createInParameter(int idx, int jdbcType){
        return createInParameter(idx, jdbcType, -1);
    }

    public static QueryParameterWrapper createParameter(int idx, int jdbcType, int jdbcScale, Direction direction){
        QueryParameterWrapper param = new QueryParameterWrapper();
        param.setIndex(idx);
        param.setDirection(direction);
        param.jdbcType = jdbcType;
        param.jdbcScale = jdbcScale;
        return param;
    }

    public static QueryParameterWrapper createParameter(int idx, int jdbcType, Direction direction){
        return createParameter(idx, jdbcType, -1, direction);
    }

    public static QueryParameterWrapper createParameter(int idx, String userDefinedTypeName, Direction direction){
        QueryParameterWrapper param = new QueryParameterWrapper();
        param.setIndex(idx);
        param.setDirection(direction);
        param.userDefinedType = userDefinedTypeName;
        return param;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }

    public String getUserDefinedType() {
        return userDefinedType;
    }

    public void setUserDefinedType(String userDefinedType) {
        this.userDefinedType = userDefinedType;
    }

    public Direction getDirection() {
        return direction;
    }

    public void setDirection(Direction direction) {
        this.direction = direction;
    }

    public int getJdbcType() {
        return jdbcType;
    }

    public void setJdbcType(int jdbcType) {
        this.jdbcType = jdbcType;
    }

    public int getJdbcScale() {
        return jdbcScale;
    }

    public void setJdbcScale(int jdbcScale) {
        this.jdbcScale = jdbcScale;
    }

    /**
     * Publish given query parameters array into the string
     *
     * @param queryParams      array of table attrs to publish into String
     * @param formatString enble formating result string
     */
    public static String queryParamsToString(QueryParameterWrapper[] queryParams, boolean formatString) {
        StringBuffer queryParamsString = new StringBuffer();
        //Pepare formatting strings
        String retSymb = (formatString) ? "\r" : "";

        if(queryParams != null){
            for(int i = 0; i < queryParams.length; i++){
                queryParamsString.append("[param name='"+queryParams[i].getName()+"' index='"+queryParams[i].getIndex()+"' value='"+((queryParams[i].getValue() != null) ? queryParams[i].getValue().toString() : null)+"' class='"+((queryParams[i].getValue() != null) ? queryParams[i].getValue().getClass().getName() : null) +"' direction='"+queryParams[i].getDirection().toString()+"']"+ retSymb);
            }
        }
        //Return NULL instead of empty String. It is more obviosly for publishing
        String result = queryParamsString.toString().trim();
        return (result.length() > 0) ? result : "[no query params were fetched]";
    }


    /** Creates new QueryParameterWrapper[] array with size have increased for one position.
     * Then put given query parameter into the last cell of new array. */
    public static QueryParameterWrapper[] appendToQueryParameters(QueryParameterWrapper[] queryParams, QueryParameterWrapper queryParam){
        if(queryParams == null)
            queryParams = new QueryParameterWrapper[0];
        if(queryParam == null)
            return queryParams;
        QueryParameterWrapper[] newQueryParams = new QueryParameterWrapper[queryParams.length + 1];
        System.arraycopy(queryParams, 0, newQueryParams, 0, queryParams.length);
        newQueryParams[newQueryParams.length-1] = queryParam;
        return newQueryParams;
    }

    public void set(String fieldName, String fieldValue) throws ResolverException {
        if (fieldName.toLowerCase().equals("name")) {
            name = fieldValue;
            return;
        }
        if (fieldName.toLowerCase().equals("value")) {
            value = fieldValue;
            if(type != null)//to be independed from the "type" and "value" order in parsing string
                value = TypeWrapperUtil.parameterClassCasting((String)value, type);//it is really String before class casting
            return;
        }
        if (fieldName.toLowerCase().equals("type")) {
            type = fieldValue.toLowerCase();
            if(value != null)//to be independed from the "type" and "value" order in parsing string
                value = TypeWrapperUtil.parameterClassCasting((String)value, type);//it is really String before class casting
            return;
        }
        //RM: throw exception if field not found
        throw new ResolverException("err.resolver.unknown_parameter_for", new String[]{this.getClass().getName(), fieldName, fieldValue.toString()});
    }


    public static class Direction {
        public static Direction IN = new Direction("IN");
        public static Direction OUT = new Direction("OUT");
        private String type = null;

        private Direction(String type){
            this.type = type;
        }

        public String toString(){
            return type;
        }
    }

}
