<?php

$query = new WP_Query(array('post_type' => 'services', 'showposts' => -1));
$i = 1;
while ($query->have_posts()) : $query->the_post();
    echo '<tr>';
    echo '<td>'. $i .'</td>';
    echo '<td>' . get_the_title() . '</td>';
    echo '<td>г.'. get_field('city') . ', ' . get_field('contact') .'</td>';
    
    
    $web = get_field('web');
    
    if(!empty($web)){
        $web_link = '<a target="_blank" href="'.$web.'">'.$web.'</a>';
    }
    
    echo '<td>'. $web_link .'</td>';
    echo '</tr>';
    
    $i++;
    unset($web_link);
endwhile;

wp_reset_postdata();
?>



