package com.tmx.as.blogic;

import com.tmx.as.base.EntityManager;
import com.tmx.as.base.FilterWrapper;
import com.tmx.as.base.SecurityService;
import com.tmx.as.base.criterion.Restrictions;
import com.tmx.as.base.criterion.CriterionWrapper;

import com.tmx.as.entities.bill.seller.Seller;
import com.tmx.as.entities.general.group.Group;
import com.tmx.as.entities.general.organization.Organization;
import com.tmx.as.entities.general.role.Role;
import com.tmx.as.entities.general.user.User;
import com.tmx.as.entities.bill.messages.Messages;

import com.tmx.as.exceptions.DatabaseException;
import com.tmx.as.exceptions.SecurityException;
import com.tmx.web.controls.CriterionFactory;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;


public class UserManager extends EntityManager {

    public User getUser(Long userId) throws DatabaseException {
        return (User) RETRIEVE(User.class, userId, new String[]{ "role", "organization", "sellerOwner"});
    }

    public void saveUser(User savedUser) throws DatabaseException {
        SAVE(savedUser);
    }

    public void createUserSettings(User user) throws DatabaseException {
        SAVE(user);
    }

    /**
     * Used then user self update self data
     */
    public void selfChangeUser(User user) throws DatabaseException{
        SAVE(user, new String[]{"login", "password", "firstName", "lastName"});
    }

    /**
     * Delete existed user
     */
    public void deleteUser(Long userId) throws DatabaseException {
        DELETE(User.class, userId);
    }

    public List getUsers() throws DatabaseException {
        FilterWrapper filter = new FilterWrapper("by_active");
        return RETRIEVE_ALL(User.class, new FilterWrapper[]{filter});
    }

    public List getUsersBy(Long organizationId) throws DatabaseException {
        FilterWrapper byActivefilter = new FilterWrapper("by_active");
        FilterWrapper byOrganization = new FilterWrapper("by_organization");
        byOrganization.setParameter("organizationId", organizationId);

        //Preload with role
        return RETRIEVE_ALL(User.class, new FilterWrapper[]{byActivefilter, byOrganization}, new String[]{"role"});
    }

    public String getUsersQueryByOrganization(Long organizationId) throws DatabaseException {
        return getUsersQueryByOrganization(organizationId, null);
    }

    public String getUsersQueryByOrganization(Long organizationId, Long excGroupId) throws DatabaseException {
        StringBuffer query = new StringBuffer();
        query.append("query(query_type=retrieve_all,");
        query.append("entity_class=com.tmx.as.entities.general.user.User,");
        query.append("table_attr=role,");
        query.append("order(name=userId,type=asc),");
        query.append("filter(name=by_active),");
        query.append("filter(name=by_not_admin),");
        query.append("filter(name=by_organization,");
        query.append("parameter(name=organizationId,value=");
        query.append(organizationId);
        query.append(",type=long)),");
        query.append("page_size=10");
        query.append(")");
        return query.toString();
    }

    public List getGroups(Long organizationId) throws DatabaseException {
        FilterWrapper byOrganization = new FilterWrapper("by_organization");
        byOrganization.setParameter("organizationId", organizationId);
        return RETRIEVE_ALL(Group.class, new FilterWrapper[]{byOrganization});
    }

    public List getAllRoles() throws DatabaseException {
        return RETRIEVE_ALL(Role.class, null);
    }

    public List getAllOrganizations() throws DatabaseException {
        return RETRIEVE_ALL(Organization.class, null);
    }

    public List getAllSellers() throws DatabaseException {
        return RETRIEVE_ALL(Seller.class, null);
    }

    //Proximan
    public List getAllUsers() throws DatabaseException {

        CriterionWrapper[] wrappers = new CriterionWrapper[] {
                Restrictions.eq("active", 1), Restrictions.eq("valid", 1)};

        List users = RETRIEVE_ALL(User.class, wrappers, null);
        Collections.sort(users, new Comparator<User>()
        {
          public int compare(User u1, User u2) {
            return u1.getLogin().compareTo(u2.getLogin());
          }
        });

        return users;
    }

    public List getAllMessages(final String userLogin) throws DatabaseException
    {
        // адрессованные мне или all не мои
        CriterionWrapper[] wrappers = new CriterionWrapper[] {
            Restrictions.eq("valid", 1),
            Restrictions.in("observers", new Object[] {userLogin, "all"}),
            Restrictions.ne("login", userLogin)
        };

        // все свои
        CriterionWrapper[] wrappers2 = new CriterionWrapper[] {
            Restrictions.eq("valid", 1),
            Restrictions.eq("login", userLogin)
        };

        List<Messages> messages = RETRIEVE_ALL(Messages.class, wrappers, null);
        List<Messages> messages2 = RETRIEVE_ALL(Messages.class, wrappers2, null);

        messages.addAll(messages2);


        Collections.sort(messages, new Comparator<Messages>()
        {
          public int compare(Messages m1, Messages m2) {
            return m1.getTitle().compareTo(m2.getTitle());
          }
        });

        for(Messages m : messages)
        {
            if(m.getTitle().length() > 40)
            {
                String title = m.getTitle().substring(0, 40) + "...";
                m.setTitle(title);
            }
        }

        return messages;
    }
    //

    public List getAllRolesWithoutSUPERUSER() throws DatabaseException {
        CriterionWrapper[] wrappers = new CriterionWrapper[] {Restrictions.not(Restrictions.eq("key", "SUPERUSER"))};
        return RETRIEVE_ALL(Role.class, wrappers, null);
    }

    public User login(String login, String password) throws SecurityException, DatabaseException {
        try {
            FilterWrapper byLoginFilter = new FilterWrapper("by_exactlogin");
            byLoginFilter.setParameter("login", login);
            
            //Retrieve user and preload its role, organization,
            User user = (User) RETRIEVE(User.class, new FilterWrapper[]{byLoginFilter}, 
                                                                        new String[]{"role", "organization",
                                                                        "sellerOwner",
                                                                        "sellerOwner.physicalAddress.city",
//                                                                        "sellerOwner.physicalAddress.country",
                                                                        "sellerOwner.physicalAddress.district",
                                                                        "sellerOwner.physicalAddress.region",
//                                                                        "sellerOwner.legalAddress.country",
                                                                        "sellerOwner.legalAddress.district",
                                                                        "sellerOwner.legalAddress.region",
                                                                        "sellerOwner.legalAddress.city",
                                                                        "sellerOwner.director", "sellerOwner.accountant"});
            if (user == null)
                throw new SecurityException("err.user_manager.incorrect_login");
            if (!user.getPassword().equals(password))
                throw new SecurityException("err.user_manager.incorrect_password");
            if (user.getActive() != 1)
                throw new SecurityException("err.user_manager.inactive_user");
            if(user.getRole() == null)
                throw new SecurityException("err.user_has_null_role", new String[]{ user.getLogin()} );

            //Reload UI permissions
            SecurityService.getInstance().reloadUIPermissionsForRole(user.getRole().getKey());
            return user;
        } catch (Exception e) {
            throw new DatabaseException("err.user_manager.unknown_error", e);
        }
    }

}
