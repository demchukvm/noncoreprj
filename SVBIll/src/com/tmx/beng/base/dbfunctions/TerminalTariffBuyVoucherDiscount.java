package com.tmx.beng.base.dbfunctions;

import com.tmx.beng.base.cache.InvokeFunctionException;
import com.tmx.beng.base.BillingMessage;
import com.tmx.beng.base.Reservation;
import com.tmx.as.entities.bill.transaction.TransactionType;

import java.util.*;

/**
 */
public class TerminalTariffBuyVoucherDiscount extends BasicTariffFunction{
    public static final String DISCOUNT_PERCENT = "discount_percent";

    boolean isTariffApplicable(Map actualParams) {
        return BillingMessage.O_BUY_VOUCHER.equals(task.getProcesedMessage().getOperationName());
    }


    void applyTariff(Map actualParams, Reservation reservation) throws InvokeFunctionException {
        try{
           double discontPercent = Double.parseDouble((String) actualParams.get(DISCOUNT_PERCENT));
           double amount = Double.parseDouble(task.getProcesedMessage().getAttributeString(BillingMessage.AMOUNT));
           double tariffedAmount = amount * discontPercent;
           reservation.reserve(null, tariffedAmount, null, TransactionType.CODE_CREDIT);
        }
        catch(Throwable e){
            throw new InvokeFunctionException("err.terminal_tariff_calc_error", e);
        }
    }


    public ValidationError[] validate(Map actualParams) {
        List resultErrors = new ArrayList();

        resultErrors.addAll(ParameterValidator.validateParamNames(actualParams, new String[] {DISCOUNT_PERCENT}));
        resultErrors.addAll(ParameterValidator.validateDiscountPercent(actualParams, new String[] {DISCOUNT_PERCENT}));

        return (ValidationError[]) resultErrors.toArray(new ValidationError[resultErrors.size()]);
    }

    public TariffParameter[] getDefaultParams() {
        TariffParameter paramDisPer = new TariffParameter();
    	paramDisPer.setName(DISCOUNT_PERCENT);
    	paramDisPer.setValue("0.97");
        paramDisPer.setOrderNum(new Long(1));
        paramDisPer.setMandatory(Boolean.TRUE);
        return new TariffParameter[] {paramDisPer};
    }
}
