<?
class CustomerBallList extends ActionsController
{
    
	function create_ball(){
		
        $smarty = &Core::getSmarty();
        Users::_initUserInfo();
        set_query('safemode=','',true);
        
        if($_POST['do']=='save')
        
        {
            $customer = new Customer();
            $customer = &$customer;
            $customer->loadByID(intval($_GET['userID']));
            
            $customer->create_transaction_ball($_POST['ball'], $_POST['description']);
            Redirect('index.php?userID='.$_GET['userID'].'&did='.$_GET['did']);

            
        }
      

        $smarty->assign('UserInfoFile', 'backend/user_ball_add.html');    
        
	}
    function main()
    {
        
        $smarty = &Core::getSmarty();
        Users::_initUserInfo();
        set_query('safemode=','',true);
       
        $gridEntry = new Grid();

        $gridEntry->query_select_rows = 'SELECT * FROM ?#CUSTOMER_BALLS_TABLE WHERE customer_id='.intval($_GET['userID']);
        $gridEntry->query_total_rows_num = 'SELECT COUNT(*)  FROM ?#CUSTOMER_BALLS_TABLE WHERE customer_id='.intval($_GET['userID']);

        $gridEntry->registerHeader(translate("ball_id"), 'ball_id', true, 'desc');  
        $gridEntry->registerHeader(translate("ball"), 'ball', false, 'desc');
        $gridEntry->registerHeader(translate("date_transaction"), 'date_transaction');
        $gridEntry->registerHeader(translate("description"));


        $gridEntry->show_rows_num_select = false;


        $gridEntry->prepare();


        $smarty->assign('UserInfoFile', 'backend/user_balls.html');    
    }
};

    
    
  ActionsController::exec('CustomerBallList');  
 ?>   