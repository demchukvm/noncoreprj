package com.tmx.web.controls;

import java.util.Map;
import java.util.HashMap;

/**
    Simple container to hold statuses of actions execution
    and provide them to listener tags for publishing on pages.
 */
public class ActionExecStatus extends BasicControl{
    private Map statuses;

    public void initialize() {
        statuses = new HashMap();
    }

    public void setNewStatus(String listener, String statusKey){
        statuses.clear();
        statuses.put(listener, statusKey);
    }

    public String getStatusKey(String listener){
        return (String)statuses.get(listener);
    }
    
    protected void safeReset() {
        statuses.clear();
    }
}
