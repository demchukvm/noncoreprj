package com.tmx.web.actions.core.tools;

import com.tmx.web.base.BasicAction;
import com.tmx.web.forms.core.tools.TestForm;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForm;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.tmx.web.base.BasicDispatchedAction;
import org.apache.struts.upload.FormFile;

import java.io.File;
import java.io.FileOutputStream;
import java.io.PrintWriter;

public class TestAction extends Action//BasicDispatchedAction
{
    private final String FORWARD = "test";


    protected ActionForward defaultAction(ActionMapping mapping,
                                          ActionForm form,
                                          HttpServletRequest request,
                                          HttpServletResponse response,
                                          String methodName) throws Throwable {

        PrintWriter out = response.getWriter();
        try {
            



            out.println("<script type=\"text/javascript\">");
            out.println("var width = 800;");
            out.println("var height = 600;");
            out.println("var left = (screen.width - width) / 2;");
            out.println("var top = (screen.height - height) / 2;");
            out.println("window.open('svbill/TestAction.do', 'upload', 'width='+width+',height='+height+',top='+top+',left='+left+',channelmode=no,directories=no,location=no,menubar=no,resizable=yes,scrollbars=yes,status=yes,titlebar=yes,toolbar=no', 'false');");
            out.println("</script>");



        } finally {
            out.close();
        }


        return mapping.findForward(FORWARD);
    }


    public ActionForward execute(ActionMapping mapping,
                                 ActionForm form,
                                 HttpServletRequest request,
                                 HttpServletResponse response) throws Exception {
        System.out.println(request.getMethod());

        TestForm fileUploadForm = (TestForm)form;
        if(fileUploadForm.getFile() != null)
        {
            FormFile file = fileUploadForm.getFile();

            //Get the servers upload directory real path name
            String filePath =
                   getServlet().getServletContext().getRealPath("/") +"upload";

            //create the upload folder if not exists
            File folder = new File(filePath);
            if(!folder.exists()){
                folder.mkdir();
            }

            String fileName = file.getFileName();

            if(!("").equals(fileName)){

                System.out.println("Server path:" +filePath);
                File newFile = new File(filePath, fileName);

                if(!newFile.exists()){
                  FileOutputStream fos = new FileOutputStream(newFile);
                  fos.write(file.getFileData());
                  fos.flush();
                  fos.close();
                }

                request.setAttribute("uploadedFilePath",newFile.getAbsoluteFile());
                request.setAttribute("uploadedFileName",newFile.getName());
            }
        }
		return mapping.findForward(FORWARD);
    }

}


/*
  protected String renderOnClickScript(BasicControl ctrl) throws WebException {
        StringBuffer resultHTML = new StringBuffer();
        BrowseBox browseBox = (BrowseBox)ctrl;

        //Add script entry from onClick attr or default for button
        if(browseBox.isReadonly()){
            resultHTML.append("     //do nothing, because browseBox is disabled. \r");
        }
        else{
            //script to evaluate popup window left and top attributes
            resultHTML.append("\tvar width = ").append(getWinFeatureWidth()).append(";\r").
                       append("\tvar height = ").append(getWinFeatureHeight()).append(";\r").
                       append("\tvar left = (screen.width - width) / 2;\r").
                       append("\tvar top = (screen.height - height) / 2;\r");

            StringBuffer url = new StringBuffer(evaluateAction(browseBox));
            url.append("?").append(evaluateCommand(browseBox));
            //add callback parameters names
            url.append(KEY_ID_PARAM_NAME).append("=").append(getKeyHtmlHandlerId()).append("&").
                append(VALUE_ID_PARAM_NAME).append("=").append(getValueHtmlHandlerId()).append("&").
                append(BROWSE_PARAM_NAME).append("=true");//to identify browse request

            String name = "browse_window";
            StringBuffer features = new StringBuffer();
            features.append("width='+width+',").
                     append("height='+height+',").
                     append("top='+top+',").
                     append("left='+left+',").
                     append("channelmode=").append(getWinFeatureChannelmode()).append(",").
                     append("directories=").append(getWinFeatureDirectories()).append(",").
                     append("location=").append(getWinFeatureLocation()).append(",").
                     append("menubar=").append(getWinFeatureMenubar()).append(",").
                     append("resizable=").append(getWinFeatureResizable()).append(",").
                     append("scrollbars=").append(getWinFeatureScrollbars()).append(",").
                     append("status=").append(getWinFeatureStatus()).append(",").
                     append("titlebar=").append(getWinFeatureTitlebar()).append(",").
                     append("toolbar=").append(getWinFeatureToolbar());


            String replace = "false";
            resultHTML.append("\t").
                       append(getOpenedWindowsRefName()).append("=").
                       append("window.open(").append("'").append(url).append("', ").
                                              append("'").append(name).append("', ").
                                              append("'").append(features).append("', ").
                                              append("'").append(replace).append("');\r");
        }
        return resultHTML.toString();
    }
*/