package com.tmx.as.blogic;

import com.tmx.as.base.EntityManager;
import com.tmx.as.base.FilterWrapper;
import com.tmx.as.base.QueryParameterWrapper;
import com.tmx.as.base.criterion.CriterionWrapper;
import com.tmx.as.base.criterion.Restrictions;
import com.tmx.as.entities.bill.voucher.Voucher;
import com.tmx.as.entities.bill.voucher.BatchBuyVoucher;
import com.tmx.as.exceptions.DatabaseException;
import com.tmx.beng.base.BillingMessage;
import com.tmx.beng.base.BillException;
import com.tmx.beng.csapi.java.base.*;
import com.tmx.util.InitException;
import org.apache.log4j.Logger;

import java.util.*;
import java.math.BigDecimal;
import java.io.PrintStream;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.text.DecimalFormat;
import java.text.DateFormat;
import com.tmx.beng.csapi.xml.v1.base.ClientPAD;

public class VoucherManager extends EntityManager {

    //Logger logger = Logger.getLogger("gen."+getClass().getName());

    public Voucher getVoucherById(Long id) throws DatabaseException {
        FilterWrapper filterById = new FilterWrapper("by_id");
        filterById.setParameter("id", id);

        return (Voucher) RETRIEVE(Voucher.class, new FilterWrapper[] {filterById}, null);
    }

    public List <Voucher> getAllVouchers() throws DatabaseException {
        return RETRIEVE_ALL(Voucher.class, null);
    }

    public List getVouchersSumSeparetedByNominal(List nominals, List<String> statuses, Date timeFrom, Date timeTo,
                                                 List billingNumbers, Integer isFilterByBillTransNumSet) throws DatabaseException {
        List res = new ArrayList();

        if (billingNumbers.size() == 0) {//empty list
            billingNumbers.add("empty_list"); //redunant just for Hibernate
        }

        for (Object nominal : nominals) {
             QueryParameterWrapper[] parameterWrapper = new QueryParameterWrapper[] {
                                      new QueryParameterWrapper("nominal", nominal),
                                      new QueryParameterWrapper("status", statuses),
                                      new QueryParameterWrapper("timeFrom", timeFrom),
                                      new QueryParameterWrapper("timeTo", timeTo),
                                      new QueryParameterWrapper("isFilterByBillTransNumSet", isFilterByBillTransNumSet),
                                      new QueryParameterWrapper("billingNumbers", billingNumbers)
                                    };
            List sum = EXECUTE_QUERY(Voucher.class, "selectSum", parameterWrapper);
            res.add((sum.get(0) == null) ? 0 : ((BigDecimal)sum.get(0)).doubleValue());
        }

        return res;
    }

    public List<Double> getVouchersQuantitySeparetedByNominal(List nominals,
                                                              List<String> statuses,
                                                              Date timeFrom,
                                                              Date timeTo,
                                                              List billingNumbers,
                                                              Integer isFilterByBillTransNumSet) throws DatabaseException {
        List<Double> res = new ArrayList<Double>();

        if (billingNumbers.size() == 0) {//empty list
            billingNumbers.add("empty_list"); //redunant just for Hibernate
        }

        for (Object nominal : nominals) {
            QueryParameterWrapper[] parameterWrapper = new QueryParameterWrapper[] {
                                  new QueryParameterWrapper("nominal", nominal),
                                  new QueryParameterWrapper("status", statuses),
                                  new QueryParameterWrapper("timeFrom", timeFrom),
                                  new QueryParameterWrapper("timeTo", timeTo),
                                  new QueryParameterWrapper("isFilterByBillTransNumSet", isFilterByBillTransNumSet),
                                  new QueryParameterWrapper("billingNumbers", billingNumbers)
            };
            List quant = EXECUTE_QUERY(Voucher.class, "selectCount", parameterWrapper);
            res.add((quant.get(0) == null) ? 0 : ((BigDecimal)quant.get(0)).doubleValue());
        }

        return res;
    }

    public List<List<Voucher>> getSeparetedByNominalVouchers() throws DatabaseException {
        List<List<Voucher>> res = new ArrayList<List<Voucher>>();

        for (Object nominal : getNominals()) {
            CriterionWrapper criterion[] = new CriterionWrapper[]
                                                {Restrictions.eq("nominal", nominal)};
            res.add((List<Voucher>)RETRIEVE_ALL(Voucher.class, criterion, null));
        }

        return res;
    }

    public List<String> getNominals() throws DatabaseException {
        QueryParameterWrapper[] parameterWrapper = new QueryParameterWrapper[] {
                                  new QueryParameterWrapper("isFilterByOperatorSet", 1),
                                  new QueryParameterWrapper("operatorId", 1L)};
        return getNominals(parameterWrapper);
    }
    public List<String> getNominals(QueryParameterWrapper[] parameters) throws DatabaseException {
        return EXECUTE_QUERY(Voucher.class, "selectNominals", parameters);
    }

    public List<String> getNominalsAndPrices(QueryParameterWrapper[] parameters) throws DatabaseException {
        return EXECUTE_QUERY(Voucher.class, "selectNominalsAndPrices", parameters);
    }

    public List<Object []> getNominalsOperators() throws DatabaseException {
        QueryParameterWrapper[] parameterWrapper = new QueryParameterWrapper[] {
                                  new QueryParameterWrapper("isFilterByOperatorSet", 1),
                                  new QueryParameterWrapper("operatorId", 1L)};
        return getNominalsOperators(parameterWrapper);
    }
    public List<Object []> getNominalsOperators(QueryParameterWrapper[] parameters) throws DatabaseException {
        return EXECUTE_QUERY(Voucher.class, "selectNominalsOperators", parameters);
    }

    public List getRemains() throws DatabaseException {
        return EXECUTE_QUERY(Voucher.class, "remains", null);
    }

    public List getAllValidVouchersByNominal(String nominal) throws DatabaseException {
        QueryParameterWrapper[] parameters = new QueryParameterWrapper[] {
                new QueryParameterWrapper("nominal", nominal)};

        return EXECUTE_QUERY(Voucher.class, "allValidVouchersByNominal", parameters);
    }

    public boolean updateVoucherToCutStatus(Long voucherId)
    {
        boolean flag = false;

        Voucher voucher = null;
        try {
            voucher = (Voucher) RETRIEVE(Voucher.class, voucherId);
            voucher.setStatus("CUT");
            try {
                SAVE(voucher);
                flag = true;
            } catch (DatabaseException e) {
                flag = false;
            }
        } catch (DatabaseException e) {
            flag = false;
        } finally {
            return flag;
        }
    }       

    public List<String> getNominalsByOperatorId(Long id) throws DatabaseException {
        QueryParameterWrapper[] parameterWrapper = new QueryParameterWrapper[] {
                                  new QueryParameterWrapper("isFilterByOperatorSet", 0),
                                  new QueryParameterWrapper("operatorId", id)};

        return getNominals(parameterWrapper);
    }

    public List<String> getNominalsAndPricesByOperatorId(Long id) throws DatabaseException {
        QueryParameterWrapper[] parameterWrapper = new QueryParameterWrapper[] {
                                  new QueryParameterWrapper("isFilterByOperatorSet", 0),
                                  new QueryParameterWrapper("operatorId", id)};

        return getNominalsAndPrices(parameterWrapper);
    }

    private List getStatuses() throws DatabaseException {
        return EXECUTE_QUERY(Voucher.class, "selectStatuses", null);
    }

    public List getBillingNumbersBySeller(Long sellerId, String strTerminalId) throws DatabaseException {
        Long terminalId = 1l;
        
        int isFilterByTerminalSet = 1;
        if (strTerminalId != null) {
            if(!strTerminalId.equals("")) {
                terminalId = new Long(strTerminalId);
                isFilterByTerminalSet = 0;
            }
        }

        QueryParameterWrapper[] parameterWrapper = new QueryParameterWrapper[] {
              new QueryParameterWrapper("sellerId", sellerId),
              new QueryParameterWrapper("isFilterByTerminalSet", isFilterByTerminalSet),
              new QueryParameterWrapper("terminalid", terminalId)
            };
        List billingNumbers = EXECUTE_QUERY(Voucher.class, "soldInBillTransactionNum", parameterWrapper);
        return billingNumbers;
    }

    public List getBillingNumbers() throws DatabaseException {
            return EXECUTE_QUERY(Voucher.class, "selectBillingNumbers", null);
    }

    /** Buy batch of life vouches.
     * @param login login
     * @param password password
     * @param sleepBetween interval to sleep between voucher requests, milliseconds.
     * @param vouchers list of returned voucher
     * @throws BillException on billing exception.
     * @throws InitException on getting connection error */
    public void buyVouchersBatch(BatchBuyVoucher buyOperation,        // послать запрос на пополнение
                                 String login,
                                 String password,
                                 long sleepBetween,
                                 List<BuyVoucherResp> vouchers) throws BillException, InitException {

        BillingClientApi api = new BillingClientApi();
        //Connection conn = api.openBillingConnection();
        for(int i=0; i<buyOperation.getVoucherCount(); i++){

            if(BillingMessage.O_BUY_VOUCHER.equals(buyOperation.getOperationType())){   // Avancel
                BuyVoucherReq req = api.createBuyVoucherMsg();
                req.setClientTime(new Date());
                final String transactionCode = String.valueOf(System.currentTimeMillis());
                req.setCliTransactionNum(transactionCode);
                req.setVoucherNominal("LifeVoucher"+buyOperation.getVoucherNominal());//.toString());
                req.setServiceCode(buyOperation.getServiceCode());

                req.setProcessingCode(buyOperation.getProcessingCode());
                req.setSellerCode(buyOperation.getSellerCode());
                req.setTerminalSN(buyOperation.getTerminalSn());
                req.setLogin(login);
                req.setPasswordMD5(password);
                
                BuyVoucherResp resp = req.send();
                vouchers.add(resp);
            }
            // For LifeExc voucher generation
            else if(BillingMessage.O_REFILL_PAYMENT.equals(buyOperation.getOperationType())){
                RefillPaymentReq req = api.createRefillPaymentMsg();
                req.setClientTime(new Date());
                final String transactionCode = String.valueOf(System.currentTimeMillis());
                //req.setCliTransactionNum(transactionCode);
                req.setCliTransactionNum("GenLife_"+(i+1)+" : "+transactionCode);
                req.setAmount(buyOperation.getVoucherNominal().toString());
                req.setMsisdn("0"); // This is only supports by Life gate. It treats the REFILL_PAYMENT with msisdn = 0 like BUY_VOUCHER operation.
                req.setServiceCode(buyOperation.getServiceCode());
                req.setProcessingCode(buyOperation.getProcessingCode());
                req.setSellerCode(buyOperation.getSellerCode());
                req.setTerminalSN(buyOperation.getTerminalSn());
                req.setLogin(login);
                req.setPasswordMD5(password);
                BuyVoucherResp resp = req.send();
                vouchers.add(resp);
            }
                        
            // sleep after each reqeust to reduce loading level of SVBill
            try{
                if(sleepBetween > 0)
                    Thread.sleep(sleepBetween);
            }
            catch (Exception e){
                //ignore error
            }
        }
    }

    /*
    public void sendVouchersBatch(String gateNum,
                                 BatchBuyVoucher buyOperation,
                                 int vouchersCount, 
                                 String login,
                                 String password,
                                 long sleepBetween,
                                 List<GenVoucherSendResp> send) throws BillException, InitException {

        BillingClientApi api = new BillingClientApi();

        for(int i=0; i<buyOperation.getVoucherCount(); i++)
        {
//
//            if(BillingMessage.O_GEN_VOUCHER_SEND.equals(buyOperation.getOperationType()))
//            {   // изменить нумерацию
            GenVoucherSendReq req = api.createGenVoucherSendMsg();
            req.setClientTime(new Date());
            final String transactionCode = String.valueOf(System.currentTimeMillis());
            req.setCliTransactionNum("GenLife_A"+(Integer.parseInt(gateNum)+1)+"."+(i+1)+" : "+transactionCode);
            req.setAmount(buyOperation.getVoucherNominal().toString());
            req.setMsisdn("0");
            req.setServiceCode(buyOperation.getServiceCode());
            req.setProcessingCode(buyOperation.getProcessingCode());
            req.setSellerCode(buyOperation.getSellerCode());
            req.setTerminalSN(buyOperation.getTerminalSn());
            req.setGateNum(gateNum);
            
            req.setLogin(login);
            req.setPasswordMD5(password);
            GenVoucherSendResp resp = req.send();
            send.add(resp);
//            }

            try{
                if(sleepBetween > 0)
                    Thread.sleep(sleepBetween);
            }
            catch (Exception e){
                //ignore error
            }
        }
    }

    public void statVouchersBatch(String gateNum,
                                 BatchBuyVoucher buyOperation,
                                 long sleepBetween,
                                 List<GenVoucherSendResp> send,
                                 List<GenVoucherStatResp> stat) throws BillException, InitException {

        BillingClientApi api = new BillingClientApi();

        for(GenVoucherSendResp s : send)
        {
            GenVoucherStatReq req = api.createGenVoucherStatMsg();
            req.setTransactionId(s.getTransactionId());
            req.setRequestId(s.getRequestId());
            req.setGateNum(gateNum);
            GenVoucherStatResp resp = req.send();
            stat.add(resp);

            try{
                if(sleepBetween > 0)
                    Thread.sleep(sleepBetween);
            }
            catch (Exception e){
                //ignore error
            }
        }

    }
    */

    
    /** Dump vouchers to file and return URL to it */
    public void dumpVouchersIntoFile(BatchBuyVoucher buyOperation, List<BuyVoucherResp> vouchers) throws BlogicException{
        String fileUrl = null;
        if("LifeExc".equals(buyOperation.getServiceCode())){
            fileUrl = dumpLifeVouchersIntoFile(buyOperation, vouchers);
        }
        else {
            throw new BlogicException("Unknown service code", new String[]{buyOperation.getServiceCode()});
        }
        final EntityManager em = new EntityManager();
        buyOperation.setDumpFileUrl(fileUrl);

        try{
            em.SAVE(buyOperation);
        }
        catch (DatabaseException e){
            throw new BlogicException("Failed to save BatchBuyVoucher entity after dumping the vouchers file", e);
        }
    }

    
    private String dumpLifeVouchersIntoFile(BatchBuyVoucher buyOperation, List<BuyVoucherResp> vouchers) throws BlogicException{
        try{
            final String fileUrl = composeVouchersFileName(buyOperation);
            final PrintStream out = new PrintStream(new FileOutputStream(fileUrl));
            final SimpleDateFormat dateFormat = new SimpleDateFormat("ddMMyyyy");
            final DecimalFormat numberFormat = new DecimalFormat("000000000");
            for (BuyVoucherResp v : vouchers) {
                if(v.getSecretCode() != null && v.getCode() != null && v.getPrice() != null && v.getBestBeforeDate() != null){
                    final StringBuffer row = new StringBuffer();
                    row.append(formatString(v.getSecretCode(), 14, " "));
                    row.append(formatString(v.getCode(), 9, " "));
                    row.append(numberFormat.format(v.getPrice() / 100)); //convert to UAH 
                    row.append("S0");
                    row.append(dateFormat.format(v.getBestBeforeDate()));
                    out.println(row.toString());
                }
            }
            return fileUrl;
        }
        catch (Exception e){
            throw new BlogicException("Failed to dump Life vouchers", e);
        }
    }

    private String composeVouchersFileName(BatchBuyVoucher buyOperation) {
        final DateFormat format = new SimpleDateFormat("yyyy.MM.dd_HH.mm.ss");
        return new StringBuffer().
                append(System.getProperty("tmx.home")).
                append("/backup/vouchers/").
                append(format.format(buyOperation.getTransactionTime())).
                append("_").
                append(buyOperation.getServiceCode()).
                append("_").
                append(buyOperation.getVoucherNominal()).
                append("_").
                append(buyOperation.getVoucherCount()).
                append(".csv").
                toString();
    }

    private String formatString(String source, int maxLength, String gapChar){
        final String cuttedString = source.length() >= maxLength ? source.substring(0, maxLength) : source;
        final StringBuffer formattedString = new StringBuffer(cuttedString);
        for(int i=cuttedString.length(); i<maxLength; i++)
            formattedString.append(gapChar.substring(0, 1));
        return formattedString.toString();
    }

}
