<?php /* Smarty version 2.6.26, created on 2013-02-21 17:27:27
         compiled from backend/product_option_configuration.tpl.html */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'escape', 'backend/product_option_configuration.tpl.html', 4, false),array('modifier', 'set_query_html', 'backend/product_option_configuration.tpl.html', 55, false),array('function', 'cycle', 'backend/product_option_configuration.tpl.html', 68, false),)), $this); ?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<head>
		<title><?php echo 'Варианты выбора для '; ?>
 <?php echo ((is_array($_tmp=$this->_tpl_vars['OptionInfo']['name'])) ? $this->_run_mod_handler('escape', true, $_tmp, 'html') : smarty_modifier_escape($_tmp, 'html')); ?>
</title>
		<meta http-equiv="Content-Type" content="text/html; charset=<?php echo 'utf-8'; ?>
" />
		<link rel="stylesheet" href="<?php echo @URL_CSS; ?>
/admin.css" type="text/css" />
		<script type="text/javascript" src="<?php echo @URL_JS; ?>
/functions.js"></script>
		<script type="text/javascript" src="<?php echo @URL_JS; ?>
/behavior.js"></script>
		<script type="text/javascript" src="<?php echo @URL_JS; ?>
/admin.js"></script>
	</head>
	<body style="text-align:center;">
	<script type="text/javascript">
	<?php if ($_GET['saved']): ?>
	<?php echo '
	function closeFrame(){
	
		var p = window.top;
		var max = 15;
		while(!p.optionsSettingsManager && p.top && max--){
			
			p = p.top;
		}
		if(!p.optionsSettingsManager){
			
			p = window.parent;
			max = 15;
			while(!p.optionsSettingsManager && p.parent && max--){
				
				p = p.parent;
			}
		}
		
		if(!p.optionsSettingsManager)return;
		var optionsSettingsManager = p.optionsSettingsManager;
		
		optionsSettingsManager.setOptionValuesNum(option_values_num);
		optionsSettingsManager.setProductID(productID);
		optionsSettingsManager.hideSettings();
	}
	'; ?>

	var option_values_num = '<?php echo $this->_tpl_vars['checked_variants_num']; ?>
';
	var productID = '<?php echo $_GET['productID']; ?>
';
	closeFrame();
	<?php endif; ?>
	<?php echo '
	Behaviour.addLoadEvent(function(){checkGroupBoxState(getLayer(\'chck_groupbox\'));});
	'; ?>

	</script>
		
	<h1><?php echo 'Варианты выбора для '; ?>
 "<?php echo ((is_array($_tmp=$this->_tpl_vars['OptionInfo']['name'])) ? $this->_run_mod_handler('escape', true, $_tmp, 'html') : smarty_modifier_escape($_tmp, 'html')); ?>
"</h1>
		
	<?php echo $this->_tpl_vars['MessageBlock']; ?>

		
	<?php if ($this->_tpl_vars['OptionVariantsNumber'] != 0): ?>
	<form action="<?php echo ((is_array($_tmp='')) ? $this->_run_mod_handler('set_query_html', true, $_tmp) : smarty_modifier_set_query_html($_tmp)); ?>
" method="post" name="option_value_configurator_form">
	<input type="hidden" name="action" value="save_values" />
	<input type="hidden" name="optionID" value="<?php echo $this->_tpl_vars['OptionInfo']['optionID']; ?>
" />
	<input type="hidden" name="productID" value="<?php echo $this->_tpl_vars['productID']; ?>
" />
		
	<table cellspacing="0" cellpadding="0" align="center" class="grid">
		<tr class="gridsheader"> 
			<td><input type="checkbox" class="groupcheckbox" rel="checkbox" id="chck_groupbox" /></td>
			<td><?php echo 'По умолчанию'; ?>
</td>
			<td><?php echo 'Возможные значения'; ?>
</td>
			<td><?php echo 'Наценка к стоимости продукта, если выбрана эта опция'; ?>
</td>
		</tr>
	<?php unset($this->_sections['j']);
$this->_sections['j']['name'] = 'j';
$this->_sections['j']['loop'] = is_array($_loop=$this->_tpl_vars['OptionVariants']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['j']['show'] = true;
$this->_sections['j']['max'] = $this->_sections['j']['loop'];
$this->_sections['j']['step'] = 1;
$this->_sections['j']['start'] = $this->_sections['j']['step'] > 0 ? 0 : $this->_sections['j']['loop']-1;
if ($this->_sections['j']['show']) {
    $this->_sections['j']['total'] = $this->_sections['j']['loop'];
    if ($this->_sections['j']['total'] == 0)
        $this->_sections['j']['show'] = false;
} else
    $this->_sections['j']['total'] = 0;
if ($this->_sections['j']['show']):

            for ($this->_sections['j']['index'] = $this->_sections['j']['start'], $this->_sections['j']['iteration'] = 1;
                 $this->_sections['j']['iteration'] <= $this->_sections['j']['total'];
                 $this->_sections['j']['index'] += $this->_sections['j']['step'], $this->_sections['j']['iteration']++):
$this->_sections['j']['rownum'] = $this->_sections['j']['iteration'];
$this->_sections['j']['index_prev'] = $this->_sections['j']['index'] - $this->_sections['j']['step'];
$this->_sections['j']['index_next'] = $this->_sections['j']['index'] + $this->_sections['j']['step'];
$this->_sections['j']['first']      = ($this->_sections['j']['iteration'] == 1);
$this->_sections['j']['last']       = ($this->_sections['j']['iteration'] == $this->_sections['j']['total']);
?>
		<tr class="<?php echo smarty_function_cycle(array('values' => 'gridline,gridline1'), $this);?>
"> 
			<td align="center"><input name="switchOn_<?php echo $this->_tpl_vars['OptionVariants'][$this->_sections['j']['index']]['variantID']; ?>
" class="checkbox" <?php if ($this->_tpl_vars['OptionVariants'][$this->_sections['j']['index']]['checked']): ?>checked<?php endif; ?> rel="chck_groupbox" type="checkbox" /></td>
			<td align="center"> 
				<input name="default_radiobutton" type="radio" value="<?php echo $this->_tpl_vars['OptionVariants'][$this->_sections['j']['index']]['variantID']; ?>
" <?php if ($this->_tpl_vars['variantID_default'] == $this->_tpl_vars['OptionVariants'][$this->_sections['j']['index']]['variantID']): ?> checked="checked"<?php endif; ?> id="default_radiobutton_<?php echo $this->_tpl_vars['OptionVariants'][$this->_sections['j']['index']]['variantID']; ?>
" />
			</td>
			<td align="center"><label for="default_radiobutton_<?php echo $this->_tpl_vars['OptionVariants'][$this->_sections['j']['index']]['variantID']; ?>
"><?php echo ((is_array($_tmp=$this->_tpl_vars['OptionVariants'][$this->_sections['j']['index']]['option_value'])) ? $this->_run_mod_handler('escape', true, $_tmp, 'html') : smarty_modifier_escape($_tmp, 'html')); ?>
</label></td>
			<td align="center">
				<input name="price_surplus_<?php echo $this->_tpl_vars['OptionVariants'][$this->_sections['j']['index']]['variantID']; ?>
" type="text" value="<?php echo $this->_tpl_vars['OptionVariants'][$this->_sections['j']['index']]['price_surplus']; ?>
" size="10" />
			</td>
		</tr>
	<?php endfor; endif; ?>
		<tr class="<?php echo smarty_function_cycle(array('values' => 'gridline,gridline1'), $this);?>
"> 
			<td align="center">&nbsp;</td>
			<td align="center"> 
				<input name="default_radiobutton" type="radio" value="0" <?php if ($this->_tpl_vars['variantID_default'] == 0): ?> checked="checked"<?php endif; ?> id="default_radiobutton_0"/>
			</td>
			<td align="center"><label for="default_radiobutton_0"><?php echo 'Не определено'; ?>
</label></td>
			<td align="center">&nbsp;</td>
		</tr>
	</table>
	<p><?php echo 'Предложить выбрать значение этой опции'; ?>
	<input name="option_show_times" type="text" value="<?php echo $this->_tpl_vars['option_show_times']; ?>
" size="6" /> <?php echo 'раз(а) для этого продукта'; ?>
</p>

	<p><input name="SAVE" type="submit" value="<?php echo 'Сохранить'; ?>
" /></p>
	
	</form>
	<?php else: ?>
		<?php echo 'У этой характеристики нет предустановленных вариантов значений'; ?>
...
	<?php endif; ?>		
	</body>
</html>