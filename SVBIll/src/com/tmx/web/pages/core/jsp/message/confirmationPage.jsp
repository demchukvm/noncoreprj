<%@ page import="org.apache.struts.tiles.ComponentContext" %>
<%@ page import="java.util.Iterator" %>
<%@ page language="java" pageEncoding="UTF-8" contentType="text/html; charset=utf-8" isELIgnored="false" %>
<%@ taglib uri="/WEB-INF/tld/tmx-controls.tld" prefix="tmx_ctrl" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<html:form action="/core/message/confirmation">
<table width="100%" border="0">
    <tr>
        <td valign="top" width="100%">
            <!-- CONTENT -->
            <form action="" method="post">
                <br><br><br><br>
                <table width="350" cellspacing="0" cellpadding="0" border="0" align="center">
                    <tr>
                        <td align="center">

                            <table cellspacing='0' cellpadding='0' width="301">
                                <table cellspacing='1' cellpadding='1' width="300" class="column_header_cell_">
                                    <tr>
                                        <td>
                                            <bean:message bundle="core.labels" key="core.page.message.deleteconfirmation.msg.warning"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="300" bgcolor="#ffffff">
                                            <br>
                                            <br>

                                            <div style="width:300px; padding-left:2px;">
                                                <bean:message bundle="core.labels" key="core.page.message.deleteconfirmation.msg.confirm_delete"/>
                                            </div>
                                            <br>
                                            <br>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <tmx_ctrl:button name="core.message.confirmationForm" property="onOkButton"/>
                                            <tmx_ctrl:button name="core.message.confirmationForm" property="onCancelButton"/>
                                        </td>
                                    </tr>
                                </table>
                            </table>
                        </td>
                    </tr>
                </table>
            </form>
        </td>
    </tr>
</table>
</html:form>