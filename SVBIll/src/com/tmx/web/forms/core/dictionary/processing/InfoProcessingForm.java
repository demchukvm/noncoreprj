package com.tmx.web.forms.core.dictionary.processing;

import com.tmx.web.base.BasicActionForm;
import com.tmx.web.controls.*;
import com.tmx.as.entities.bill.processing.Processing;

import java.util.Date;
import java.text.SimpleDateFormat;

public class InfoProcessingForm extends BasicActionForm {
    //ID
    private Long selectedProcessingId;
    private EditBox editBoxProcessingId;
    private EditBox editBoxCode;

    private EditBox editBoxName;
    
    /*@deprecated*/
    private EditBox editBoxRegistrationDate;
    private TimeSelector timeSelectorRegistrationDate;


    private TextArea textAreaDescription;

    private EditBox editBoxPassword;
    private EditBox editBoxPassword2;

    private EditBox editBoxLogin;

    //Button----------------------------------
    private Button onApplyButton;
    private Button onSaveButton;
    private Button onResetButton;
    private Button onCancelButton;



    protected void init() {
        initEditBoxes();
        initTimeSelectors();
        initTextAreas();
        initButtons();
    }

    public void bindData(Processing processing) {
        reset();
        if(processing!=null){
              //deprecated
    //        if( processing.getRegistrationDate() != null ){
    //            Date registartionDate = processing.getRegistrationDate();
    //            SimpleDateFormat dataFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
    //            editBoxRegistrationDate.setValue( dataFormat.format(registartionDate) );
    //        }

            timeSelectorRegistrationDate.setTime(processing.getRegistrationDate());

            editBoxProcessingId.setValue(processing.getProcessingId().toString());

            textAreaDescription.setValue(processing.getDescription());
            editBoxName.setValue(processing.getName());
            editBoxCode.setValue(processing.getCode());
            editBoxLogin.setValue(processing.getLogin());
            editBoxPassword.setValue(processing.getPassword());
            editBoxPassword2.setValue(processing.getPassword());
        }
    }

    public void bindData() {
        reset();
    }

    public Processing populateFromControls() {
        Processing processing = new Processing();

        processing.setProcessingId(PopulateUtil.getLongValue(editBoxProcessingId));
        processing.setDescription(textAreaDescription.getValue());

        processing.setRegistrationDate(new Date());

        processing.setName(editBoxName.getValue());
        processing.setCode(editBoxCode.getValue());

        processing.setLogin(editBoxLogin.getValue());

        processing.setPassword(editBoxPassword.getValue());

        return processing;
    }

    private void initButtons() {
        onSaveButton = new Button();
        onSaveButton.setLabelKey("ctrl.button.label.save");
        onSaveButton.setAction("/core/dictionary/processing/infoProcessing");
        onSaveButton.setCommand("save");

        onResetButton = new Button();
        onResetButton.setLabelKey("ctrl.button.label.reset");

        onCancelButton = new Button();
        onCancelButton.setLabelKey("ctrl.button.label.cancel");
        onCancelButton.setAction("/core/dictionary/processing/manageProcessings");

        onApplyButton = new Button();
        onApplyButton.setLabelKey("ctrl.button.label.apply");
        onApplyButton.setAction("/core/dictionary/processing/infoProcessing");
        onApplyButton.setCommand("apply");
    }

    private void initTextAreas() {
        textAreaDescription = new TextArea();
    }

    private void initTimeSelectors() {
        timeSelectorRegistrationDate = new TimeSelector();
        timeSelectorRegistrationDate.setReadonly(true);
    }

    private void initEditBoxes() {
        editBoxProcessingId = new EditBox();
        editBoxProcessingId.setReadonly(true);

        editBoxName = new EditBox();
        editBoxName.setMandatory(true);

        editBoxCode = new EditBox();
        editBoxCode.setMandatory(true);

        editBoxRegistrationDate = new EditBox();
        editBoxRegistrationDate.setReadonly(true);

        editBoxPassword = new EditBox();
        editBoxPassword.addValidationRule(EditBox.createValidationRule_MIN_FIELD_LENGTH(8));
        editBoxPassword.setMandatory(true);

        editBoxPassword2 = new EditBox();
        editBoxPassword2.addValidationRule(EditBox.createValidationRule_PASSWD_FIELDS_MATCH(editBoxPassword));
        editBoxPassword2.addValidationRule(EditBox.createValidationRule_MIN_FIELD_LENGTH(8));
        editBoxPassword2.setMandatory(true);

        editBoxLogin = new EditBox();
    }

    public Long getSelectedProcessingId() {
        return selectedProcessingId;
    }

    public void setSelectedProcessingId(Long selectedProcessingId) {
        this.selectedProcessingId = selectedProcessingId;
    }

    public EditBox getEditBoxProcessingId() {
        return editBoxProcessingId;
    }

    public void setEditBoxProcessingId(EditBox editBoxProcessingId) {
        this.editBoxProcessingId = editBoxProcessingId;
    }

    public EditBox getEditBoxName() {
        return editBoxName;
    }

    public void setEditBoxName(EditBox editBoxName) {
        this.editBoxName = editBoxName;
    }

    public EditBox getEditBoxCode() {
        return editBoxCode;
    }

    public void setEditBoxCode(EditBox editBoxCode) {
        this.editBoxCode = editBoxCode;
    }

    public EditBox getEditBoxRegistrationDate() {
        return editBoxRegistrationDate;
    }

    public void setEditBoxRegistrationDate(EditBox editBoxRegistrationDate) {
        this.editBoxRegistrationDate = editBoxRegistrationDate;
    }

    public TextArea getTextAreaDescription() {
        return textAreaDescription;
    }

    public void setTextAreaDescription(TextArea textAreaDescription) {
        this.textAreaDescription = textAreaDescription;
    }

    public Button getOnApplyButton() {
        return onApplyButton;
    }

    public void setOnApplyButton(Button onApplyButton) {
        this.onApplyButton = onApplyButton;
    }

    public Button getOnSaveButton() {
        return onSaveButton;
    }

    public void setOnSaveButton(Button onSaveButton) {
        this.onSaveButton = onSaveButton;
    }

    public Button getOnResetButton() {
        return onResetButton;
    }

    public void setOnResetButton(Button onResetButton) {
        this.onResetButton = onResetButton;
    }

    public Button getOnCancelButton() {
        return onCancelButton;
    }

    public void setOnCancelButton(Button onCancelButton) {
        this.onCancelButton = onCancelButton;
    }

    public EditBox getEditBoxPassword() {
        return editBoxPassword;
    }

    public void setEditBoxPassword(EditBox editBoxPassword) {
        this.editBoxPassword = editBoxPassword;
    }

    public EditBox getEditBoxPassword2() {
        return editBoxPassword2;
    }

    public void setEditBoxPassword2(EditBox editBoxPassword2) {
        this.editBoxPassword2 = editBoxPassword2;
    }

    public EditBox getEditBoxLogin() {
        return editBoxLogin;
    }

    public void setEditBoxLogin(EditBox editBoxLogin) {
        this.editBoxLogin = editBoxLogin;
    }

    public TimeSelector getTimeSelectorRegistrationDate() {
        return timeSelectorRegistrationDate;
    }

    public void setTimeSelectorRegistrationDate(TimeSelector timeSelectorRegistrationDate) {
        this.timeSelectorRegistrationDate = timeSelectorRegistrationDate;
    }
}
