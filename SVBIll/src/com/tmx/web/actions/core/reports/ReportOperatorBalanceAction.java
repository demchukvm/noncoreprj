package com.tmx.web.actions.core.reports;

import com.tmx.as.entities.bill.operator.Operator;
import com.tmx.web.actions.core.AbstractSvbillAction;
import com.tmx.web.forms.core.reports.ReportOperatorBalanceForm;
import com.tmx.web.forms.core.transaction.seller_balance_transaction.ManageSellerBalanceTransactionsForm;
import com.tmx.web.controls.ScrollBox;
import com.tmx.web.controls.table.FilterSet;
import com.tmx.as.blogic.TransactionManager;
import com.tmx.as.blogic.OperatorManager;
import com.tmx.as.exceptions.DatabaseException;
import com.tmx.as.base.FilterWrapper;
import com.tmx.as.resolver.ResolverException;
import com.tmx.as.resolver.FunctionSyntaxException;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForm;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class ReportOperatorBalanceAction extends AbstractSvbillAction {
    private static final String FORWARD = "reportOperatorBalanceForm";

    public ActionForward defaultAction(ActionMapping mapping,
                                       ActionForm form,
                                       HttpServletRequest request,
                                       HttpServletResponse response,
                                       String methodName) throws Throwable {
        ReportOperatorBalanceForm reportOperatorBalanceForm = (ReportOperatorBalanceForm)form;
        reportOperatorBalanceForm.getTable().setRefreshActionName(getName());
        FilterSet filterSet = reportOperatorBalanceForm.getTable().getFilterSet();

        initScrollBoxes(reportOperatorBalanceForm, filterSet);
        setUpCalculationResult(reportOperatorBalanceForm);
        return mapping.findForward(FORWARD);
    }

    private void initScrollBoxes(ReportOperatorBalanceForm reportOperatorBalanceForm, FilterSet filterSet) throws DatabaseException, ResolverException, FunctionSyntaxException {//bind scroll box for filter
        ScrollBox scrollBoxTransType = (ScrollBox)filterSet.getFilter("by_transaction_type").getParameter("type").getControl();
        scrollBoxTransType.bind(new TransactionManager().getAllTransactionType());

        //bind scroll box for filter
        ScrollBox scrollBoxOperators = (ScrollBox)filterSet.getFilter("by_operator").getParameter("operator").getControl();

        List operators = new OperatorManager().getAllOperators();
        Collections.sort(operators, new Comparator<Operator>()
        {
          public int compare(Operator op1, Operator op2) {
            return op1.getName().compareTo(op2.getName());
          }
        });
        //scrollBoxOperators.bind(new OperatorManager().getAllOperators());
        scrollBoxOperators.bind(operators);
        reportOperatorBalanceForm.getTable().refresh();
    }

    private void setUpCalculationResult(ReportOperatorBalanceForm form)
                            throws ClassNotFoundException, DatabaseException {
        FilterWrapper[] filterWrappers = null;
        try {
            filterWrappers = form.getTable().getQuery().getFilters();
        } catch(Exception ex) {
            ex.printStackTrace();
        }
        String entityClassName = form.getTable().getQuery().getEntityClass();
        TransactionManager.CalculationResult calcResult
                = new TransactionManager().calculateTransactions(filterWrappers, entityClassName);
        form.getEditBoxAllTransaction().setValue(calcResult.getTransactionCaunt().toString());
        form.getEditBoxSuccessTransaction().setValue(calcResult.getTransactionSuccessCount().toString());
        form.getEditBoxAllTransAmount().setValue(calcResult.getTransactionAmount().toString());
        form.getEditBoxSuccessTransAmount().setValue(calcResult.getTransactionSuccessAmount().toString());
    }
}

