package com.tmx.as.entities.general.system;

import com.tmx.as.base.BasicEntity;
import java.io.Serializable;

/**
System event. Example   SHUTDOWN, STARTUP
 */
public class SystemEvent extends BasicEntity{
    private Long systemEventId;
    private String eventName;

    public Long getSystemEventId() {
        return systemEventId;
    }

    public void setSystemEventId(Long systemEventId) {
        this.systemEventId = systemEventId;
    }

    public String getEventName() {
        return eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    public Serializable getId() {
        return getSystemEventId();
    }

    public int getValid() {
        return valid;
    }

    public void setValid(int valid) {
        this.valid = valid;
    }
}
