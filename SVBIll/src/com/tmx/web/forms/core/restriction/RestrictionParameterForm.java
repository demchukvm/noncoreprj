package com.tmx.web.forms.core.restriction;

import com.tmx.web.base.BasicActionForm;
import com.tmx.web.controls.EditBox;
import com.tmx.web.controls.Button;
import com.tmx.web.controls.PopulateUtil;
import com.tmx.web.forms.core.tariff.TransportParameterForm;
import com.tmx.as.entities.bill.function.ActualFunctionParameter;
import com.tmx.as.entities.bill.function.FunctionInstance;

public class RestrictionParameterForm extends TransportParameterForm {

    private Long parameterId;
    private Long functionInstanceId;

    private EditBox editBoxParameterId;
    private EditBox editBoxOrderNum;
    private EditBox editBoxType;
    private EditBox editBoxParamName;
    private EditBox editBoxValue;

    //buttons
    private Button onApplyButton;
    private Button onSaveButton;
    private Button onResetButton;
    private Button onCancelButton;

    protected void init(){
        initEditBox();
        initButtons();
    }

    private void initButtons(){
        onSaveButton = new Button();
        onSaveButton.setLabelKey("ctrl.button.label.save");
        onSaveButton.setCommand("save");
        onSaveButton.setAction(getOnSaveButtonAction());
//        onSaveButton.setAction("/core/restriction/seller_restriction/sellerRestrictionParameter");

        onResetButton = new Button();
        onResetButton.setLabelKey("ctrl.button.label.reset");

        onCancelButton = new Button();
        onCancelButton.setLabelKey("ctrl.button.label.cancel");
        onCancelButton.setCommand("update");
        onCancelButton.setAction(getOnCancelButtonAction());
//        onCancelButton.setAction("/core/restriction/seller_restriction/infoSellerRestriction");

        onApplyButton = new Button();
        onApplyButton.setLabelKey("ctrl.button.label.apply");
        onApplyButton.setCommand("apply");
        onApplyButton.setAction(getOnApplyButtonAction());
//        onApplyButton.setAction("/core/restriction/seller_restriction/sellerRestrictionParameter");
    }

    private void initEditBox(){
        editBoxParameterId = new EditBox();
        editBoxParameterId.setReadonly(true);

        editBoxOrderNum = new EditBox();

        editBoxType = new EditBox();

        editBoxParamName = new EditBox();
        editBoxParamName.setMandatory(true);

        editBoxValue = new EditBox();
        editBoxValue.setMandatory(true);
    }

    public void bindData(ActualFunctionParameter functionParameter){
        editBoxParameterId.setValue(PopulateUtil.getString(functionParameter.getActualFunctionParameterId()));
        editBoxOrderNum.setValue(PopulateUtil.getString(functionParameter.getOrderNum()));
        editBoxType.setValue(functionParameter.getType());
        editBoxParamName.setValue(functionParameter.getName());
        editBoxValue.setValue(functionParameter.getValue());
    }

    public ActualFunctionParameter populateFromControl(){
        ActualFunctionParameter param = new ActualFunctionParameter();

        FunctionInstance instance = new FunctionInstance();//always present
        instance.setFunctionInstanceId(functionInstanceId);
        param.setFunctionInstance(instance);

        if(parameterId != null)//if update
            param.setActualFunctionParameterId(parameterId);
        param.setOrderNum(PopulateUtil.getLongValue(editBoxOrderNum));
        param.setType(PopulateUtil.getStringValue(editBoxType));
        param.setName(PopulateUtil.getStringValue(editBoxParamName));
        param.setValue(PopulateUtil.getStringValue(editBoxValue));

        return param;
    }

    public Long getParameterId() {
        return parameterId;
    }

    public void setParameterId(Long parameterId) {
        this.parameterId = parameterId;
    }

    public Long getFunctionInstanceId() {
        return functionInstanceId;
    }

    public void setFunctionInstanceId(Long functionInstanceId) {
        this.functionInstanceId = functionInstanceId;
    }

    public EditBox getEditBoxOrderNum() {
        return editBoxOrderNum;
    }

    public void setEditBoxOrderNum(EditBox editBoxOrderNum) {
        this.editBoxOrderNum = editBoxOrderNum;
    }

    public EditBox getEditBoxType() {
        return editBoxType;
    }

    public void setEditBoxType(EditBox editBoxType) {
        this.editBoxType = editBoxType;
    }

    public EditBox getEditBoxParamName() {
        return editBoxParamName;
    }

    public void setEditBoxParamName(EditBox editBoxParamName) {
        this.editBoxParamName = editBoxParamName;
    }

    public EditBox getEditBoxValue() {
        return editBoxValue;
    }

    public void setEditBoxValue(EditBox editBoxValue) {
        this.editBoxValue = editBoxValue;
    }

    public Button getOnApplyButton() {
        return onApplyButton;
    }

    public void setOnApplyButton(Button onApplyButton) {
        this.onApplyButton = onApplyButton;
    }

    public Button getOnSaveButton() {
        return onSaveButton;
    }

    public void setOnSaveButton(Button onSaveButton) {
        this.onSaveButton = onSaveButton;
    }

    public Button getOnResetButton() {
        return onResetButton;
    }

    public void setOnResetButton(Button onResetButton) {
        this.onResetButton = onResetButton;
    }

    public Button getOnCancelButton() {
        return onCancelButton;
    }

    public void setOnCancelButton(Button onCancelButton) {
        this.onCancelButton = onCancelButton;
    }

    public EditBox getEditBoxParameterId() {
        return editBoxParameterId;
    }

    public void setEditBoxParameterId(EditBox editBoxParameterId) {
        this.editBoxParameterId = editBoxParameterId;
    }
}
