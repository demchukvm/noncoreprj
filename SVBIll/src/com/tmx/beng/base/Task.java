package com.tmx.beng.base;

import com.tmx.util.queue.ActionQueueElement;
import com.tmx.beng.base.memory.InMemoryObject;
import com.tmx.beng.base.memory.InMemoryException;
import com.tmx.beng.base.cache.DBCache;

/**

 */
public class Task extends ActionQueueElement implements InMemoryObject {
    private String taskId;
    private GlobalContext globalContext = null;
//    private TaskContext taskContext = null;
    /** Message to be processed in context of this task */
    private BillingMessageWritable procesedMessage = null;
    /** Callback to call and notify on synchronous call. Nullable. */
    private CallBack callBack = null;
    /** Auth **/
    private boolean authenticated = false;

    private DBCache sessionDBCache;

    public Task(BillingMessageWritable message){
        this.procesedMessage = message;
    }

    public String getId() {
        return taskId;
    }


    public void setId(String taskId) {
        this.taskId = taskId;
    }

    public BillingMessageWritable getProcesedMessage() {
        return procesedMessage;
    }

    public void setCallBack(CallBack callBack){
        this.callBack = callBack;
    }


    public CallBack getCallBack() {
        return callBack;
    }

    public GlobalContext getGlobalContext() {
        return globalContext;
    }

    public void setGlobalContext(GlobalContext globalContext) {
        this.globalContext = globalContext;
        sessionDBCache = globalContext.getDbCache();
    }

    public DBCache getSessionDBCache() {
        return sessionDBCache;
    }

    public Transaction getTransaction(){
        return (Transaction)getGlobalContext().getTransactionPool().lookup(getProcesedMessage().getAttributeString(BillingMessage.REQUEST_ID));
    }

    public void run(){
        if(procesedMessage != null){
            Pipeline pipeline = Pipeline.getInstance().getPipeline(procesedMessage.getOperationName(), globalContext);
            pipeline.runTask(this);
        }
    }

    public boolean isAuthenticated() {
        return authenticated;
    }

    public void setAuthenticated(boolean authenticated) {
        this.authenticated = authenticated;
    }


    public Object getValue(String propertyName) {
        return null;//todo use aggregation to extend InMemoryBean 
    }
}
