package com.tmx.as.entities.network.territory;

import com.tmx.as.base.BasicEntity;

import java.io.Serializable;

/**

 */
public class City extends BasicEntity {
    private Long cityId;
    private String shortName;
    private String name;
    private Region region;
//    private Country countryId;


    public Long getCityId() {
        return cityId;
    }

    public void setCityId(Long cityId) {
        this.cityId = cityId;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Region getRegion() {
        return region;
    }

    public void setRegion(Region region) {
        this.region = region;
    }

//    public Country getCountryId() {
//        return countryId;
//    }
//
//    public void setCountryId(Country country) {
//        this.countryId = country;
//    }

    public Serializable getId() {
        return getCityId();
    }

    public int getValid() {
        return this.valid;
    }

    public void setValid(int valid) {
        this.valid = valid;
    }
}
