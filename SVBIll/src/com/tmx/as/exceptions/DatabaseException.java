package com.tmx.as.exceptions;

import com.tmx.util.StructurizedException;
import java.util.Locale;

public class DatabaseException extends StructurizedException {

    public DatabaseException(String errKey, Throwable cause){
        super(errKey, cause);
    }

    public DatabaseException(String errKey, Throwable cause, Locale locale){
        super(errKey, cause, locale);
    }

    public DatabaseException(String errKey, String[] params, Throwable cause){
        super(errKey, params, cause);
    }

    public DatabaseException(String errKey){
        super(errKey);
    }

    public DatabaseException(String errKey, String[] params){
        super(errKey, params);
    }

    public DatabaseException(String errKey, String[] params, Throwable cause, Locale locale){
        super(errKey, params, cause, locale);
    }
}
