package com.tmx.as.blogic.restriction;

import com.tmx.as.entities.bill.restriction.AbstractRestriction;
import com.tmx.as.entities.bill.restriction.TerminalRestriction;
import com.tmx.as.exceptions.DatabaseException;
import com.tmx.as.blogic.BlogicException;


public class TerminalRestrictionManager extends AbstractRestrictionManager{
    public void saveRestriction(AbstractRestriction rest) throws DatabaseException, BlogicException {
        TerminalRestriction terminalRestriction = (TerminalRestriction) rest;
        try {
            BEGIN(rest);
            SAVE(terminalRestriction.getFunctionInstance());
            if(terminalRestriction.getTerminal() == null)
               throw new BlogicException("err.seller_tariff_manager.save_tariff.absent_terminal_in_terminal_restriction");
            if(terminalRestriction.getTerminal().getTerminalId() == null)
                throw new BlogicException("err.seller_restriction_manager.save_restriction.absent_terminal_in_terminal_restriction");
            SAVE(terminalRestriction);
            COMMIT(rest);
        } catch (DatabaseException e) {
            ROLLBACK(rest);
            throw e;
        }
    }

    public TerminalRestriction getRestriction(Long restId) throws DatabaseException, BlogicException {
        return (TerminalRestriction) RETRIEVE(TerminalRestriction.class, restId, new String[]{"terminal","functionInstance.functionType"});
    }

    public void deleteRestriction(Long restId) throws DatabaseException, BlogicException {
        DELETE(TerminalRestriction.class, restId);
    }
}
