package com.tmx.beng.base.cache;

import com.tmx.as.base.EntityManager;
import com.tmx.as.base.Reconfigurable;
import com.tmx.as.blogic.VoucherManager;
import com.tmx.as.entities.bill.balance.AbstractBalance;
import com.tmx.as.entities.bill.balance.OperatorBalance;
import com.tmx.as.entities.bill.balance.SellerBalance;
import com.tmx.as.entities.bill.balance.TerminalBalance;
import com.tmx.as.entities.bill.operator.Operator;
import com.tmx.as.entities.bill.processing.Processing;
import com.tmx.as.entities.bill.seller.Seller;
import com.tmx.as.entities.bill.tariff.AbstractTariff;
import com.tmx.as.entities.bill.terminal.Terminal;
import com.tmx.as.entities.bill.transaction.OperationType;
import com.tmx.as.entities.bill.transaction.TransactionType;
import com.tmx.as.exceptions.DatabaseException;
import com.tmx.util.InitException;
import com.tmx.util.MD5;
import org.apache.log4j.Logger;

import java.util.*;
import java.math.BigDecimal;

/**
 */
public class DBCache implements Reconfigurable {
    private static Logger logger = Logger.getLogger("bill.DBCache");
    private Sellers sellers = null;
    private Operators operators = null;
    private Terminals terminals = null;
    private Processings processings = null;
    private TerminalGroups terminalGroups = null;
    private OperationTypes operationTypes = null;
    private VoucherNominals voucherNominals = null;
    private VoucherNominalsAndPrices voucherNominalsAndPrices = null;

    public final int ROUND_SCALE = 3;

    /** call this method to on-line in memory tables reload */
    public void reload() throws InitException {
        //local caches
        Sellers localSellers;
        Operators localOperators;
        Terminals localTerminals;          
        Processings localProcessings;
        TerminalGroups localTerminalGroups;
        OperationTypes localOperationTypes;
        VoucherNominals localVoucherNominals;
        VoucherNominalsAndPrices localVoucherNominalsAndPrices;
        try{
            localProcessings = new Processings();
            localSellers = new Sellers(localProcessings);
            localTerminalGroups = new TerminalGroups(localSellers);
            localTerminals = new Terminals(localTerminalGroups, localSellers);
            localOperators = new Operators();
            localOperationTypes = new OperationTypes();
            localVoucherNominals = new VoucherNominals(localOperators);
            localVoucherNominalsAndPrices = new VoucherNominalsAndPrices(localOperators);
        }
        catch(Throwable e){
            logger.error(e.getLocalizedMessage(), e);
            throw new InitException("err.dbcache.inmemory_tables_reload_failed", e);
        }
        synchronized(this){
            terminals = localTerminals;
            sellers = localSellers;
            operators = localOperators;
            processings = localProcessings;
            terminalGroups = localTerminalGroups;
            operationTypes = localOperationTypes;
            voucherNominals = localVoucherNominals;
            voucherNominalsAndPrices = localVoucherNominalsAndPrices;
        }
    }

    /**
     * Check Conformity of processing seller and terminal by these codes
     * @param processingCode
     * @param sellerCode
     * @param terminalSN
     * @return conformity
     */
    public boolean checkConformity(String processingCode, String sellerCode, String terminalSN) {
        ProcessingWrapper processingWrapper = null;
        SellerWrapper sellerWrapper = null;
        TerminalWrapper terminalWrapper = null;

        try{
            processingWrapper = getProcessingWrapper(processingCode);
            sellerWrapper = getSellerWrapper(sellerCode);
            terminalWrapper = getTerminalWrapper(terminalSN);
        }
        catch(DBCacheException e){
            logger.error("Processing, Seller or Terminal not found. ProcessingCode: "+processingCode+", Seller code: "+sellerCode+", Terminal SN: "+terminalSN);
            return false;
        }
        return processingWrapper.sellerId_seller.get(sellerWrapper.seller.getSellerId()) != null
                && sellerWrapper.terminalId_terminal.get(terminalWrapper.terminal.getTerminalId()) != null;
    }

    /**
     *  Authenticate terminal by SerialNumber login and password MD5         
     * @param terminalSN
//     * @param login
     * @param takenPswd pasword hash
     * @return status
     * @throws DBCacheException Data Base Cache exeprion
     */
    public boolean authenticateTerminal(String terminalSN, /*String login,*/ String takenPswd) throws DBCacheException{
        Terminal terminal = getTerminalWrapper(terminalSN).terminal;
        if(terminal == null || terminal.getBlocked() || terminal.getPrimaryBalance().getBlocked()) {
            return false;
        }

        String storedPswd = "";
        if(terminal.getPassword() != null) {
            storedPswd = MD5.getHashString(terminal.getPassword());
        } else {
//            if(System.getProperty("IS_MIDLET_PROTECTED") == null ||
//                    System.getProperty("IS_MIDLET_PROTECTED").equals("Yeas")) {
//                throw new DBCacheException("err.dbc.failed_to_retrive_terminal_pswd", new String[] {terminalSN});
//            }
            logger.info("Authenticate Terminal: midlet is unprotected");
            //TODO: change to return false or throwing exception. No longer then 1 month.
            return true;
        }

//        String l = terminal.getLogin() == null ? "" : terminal.getLogin();
        return (storedPswd.equalsIgnoreCase(takenPswd));// && l.equalsIgnoreCase(login));
    }


     /**
      *  Authenticate processing by Code login and password MD5
      * @param processingCode processing code
      * @param login login
      * @param passwordMD5 password under MD5 cypher
      * @return is authenticated
      */
     public boolean authenticateProcessing(String processingCode, String login, String passwordMD5) {
         Processing processing = null;
         try{
            processing = getProcessingWrapper(processingCode).processing;
            if(processing == null)
                 return false;
         }
         catch(DBCacheException e){
             logger.error("Processing not found processingCode: "+processingCode);                  
             return false;
         }

         String password = "";
         if(processing.getPassword() != null)
             password = MD5.getHashString(processing.getPassword());

         String l = processing.getLogin() == null ? "" : processing.getLogin();
         return (password.equalsIgnoreCase(passwordMD5) && l.equalsIgnoreCase(login));
     }


    /**
     * Check is terminal allowed
     * @param terminalSn terminal serial number
     * @return allow is terminal allowed
     */
    public boolean isTerminalAllowed(String terminalSn){
        Terminal terminal = terminals.getTerminal(terminalSn);
        return terminal != null && !((terminal.getBlocked() == null) || (terminal.getBlocked()));
    }

    /**
     *
     * @param terminalSN
     * @param sellerCode
     * @throws DBCacheException
     */
    public synchronized void registerDefaultTerminal(String terminalSN, String sellerCode) throws DBCacheException {
        // terminal registered
        if(terminals.getTerminal(terminalSN) != null)
            return;

        SellerWrapper sellerWrapper = sellers.getSellerWrapper(sellerCode);
        if(sellerWrapper == null)
            throw new DBCacheException("err.dbcache.seller_uncknown", new String[]{sellerCode});

        TerminalGroupWrapper terminalGroupWrapper =
                terminalGroups.getTerminalGroupWrapper(sellerWrapper.seller.getDefaultTerminalGroupId());
        if(terminalGroupWrapper == null)
            throw new DBCacheException("err.dbcache.no_default_terminal_group_for_seller", new String[]{sellerCode});

        EntityManager entityManager = new EntityManager();
        try{
            entityManager.BEGIN("svbill");
            Terminal terminal = new Terminal();
            terminal.setRegistrationDate(new Date());
            terminal.setSellerOwner(sellerWrapper.seller);
            terminal.setSellerOwnerId(sellerWrapper.seller.getSellerId());
            terminal.setSerialNumber(terminalSN);
            terminal.setTerminalGroup(terminalGroupWrapper.terminalGroup);
            terminal.setTerminalGroupId(terminalGroupWrapper.terminalGroup.getTerminalGroupId());
            entityManager.SAVE(terminal);

            TerminalBalance terminalBalance = new TerminalBalance();
            terminalBalance.setAmount(new Double(0));
            terminalBalance.setRegistrationDate(new Date());
            terminalBalance.setTerminal(terminal);
            entityManager.SAVE(terminalBalance);

            terminal.setPrimaryBalance(terminalBalance);
            entityManager.SAVE(terminal);

            entityManager.COMMIT("svbill");
            TerminalWrapper terminalWrapper = new TerminalWrapper(terminal);
            terminalWrapper.balanceId_balance.put(terminalBalance.getBalanceId(), terminalBalance);
            terminals.terminalId_terminal.put(terminal.getTerminalId(), terminalWrapper);
            terminals.terminalSN_terminal.put(terminal.getSerialNumber(), terminalWrapper);
            sellerWrapper.terminalId_terminal.put(terminal.getTerminalId(), terminalWrapper);
            terminalGroupWrapper.terminalId_terminal.put(terminal.getTerminalId(), terminalWrapper);

        }
        catch(DatabaseException e){
            try{
                entityManager.ROLLBACK("svbill");
            }
            catch(DatabaseException de){
                logger.error(e);
            }
            throw new DBCacheException(e.getLocalizedMessage(), e);
        }
    }

    /**
     * Check is seller allowed. 
     * @param code seller code
     * @return allowed
     */
    public boolean isSellerAllowed(String code){
        Seller seller = sellers.getSeller(code);
        return seller != null && !seller.getPrimaryBalance().getBlocked();
    }

    /**
     * Check is operator allowed
     * @param code operator code
     * @return allowed status
     */
    public boolean isOperatorAllowed(String code){
        return operators.getOperator(code) != null;
    }
    /**
     * Check is operator is blocked
     * @param code operator code
     * @return is blocked
     */
    public boolean isOperatorBlocked(String code){
        return operators.getOperator(code).getPrimaryBalance().getBlocked();
    }

    /**
     * Check is processing allowed
     * @param code
     * @return allow status
     */
    public boolean isProcessingAllowed(String code){
        return processings.getProcessing(code) != null;
    }

    /**
     * Check is terminal balance allowed
     * @param code  terminal code
     * @return allow
     */
//    public boolean isTerminalPrimaryBallanceAllowed(String code){
//        Terminal terminal = terminals.getTerminal(code);
//        if(terminal != null){
//            TerminalBalance terminalBalance = terminal.getPrimaryBalance();
//            if(terminalBalance != null && !terminalBalance.isBlocked())
//                return true;
//        }
//        return false;
//    }


    /**
     * Check is terminal group primary balance allowed
     * @param terminalSn  terminal Sn
     * @return allow
     */
//    public boolean isTerminalGroupPrimaryBallanceAllowed(String terminalSn){
//        Terminal terminal = terminals.getTerminal(terminalSn);
//        if(terminal != null){
//            TerminalGroup terminalGroup = terminalGroups.getTerminalGroup(terminal.getTerminalGroupId());
//            if(terminalGroup != null){
//                TerminalGroupBalance terminalGroupBalance = terminalGroup.getPrimaryBalance();
//                if(terminalGroupBalance != null && !terminalGroupBalance.isBlocked())
//                    return true;
//            }
//        }
//        return false;
//    }


    /**
     * Check is seller primary balance allowed
     * @param sellerCode  seller code
     * @return allow
     */
//    public boolean isSellerPrimaryBallanceAllowed(String sellerCode){
//        Seller seller = sellers.getSeller(sellerCode);
//        if(seller != null){
//            SellerBalance sellerBalance = seller.getPrimaryBalance();
//            if(sellerBalance != null && !sellerBalance.isBlocked())
//                return true;
//        }
//        return false;
//    }


    /**
     * Check is operator primary balance allowed
     * @param operatorCode  operator code
     * @return allow
     */
//    public boolean isOperatorPrimaryBallanceAllowed(String operatorCode){
//        Operator operator = operators.getOperator(operatorCode);
//        if(operator != null){
//            OperatorBalance operatorBalance = operator.getPrimaryBalance();
//            if(operatorBalance != null && !operatorBalance.isBlocked())
//                return true;
//        }
//        return false;
//    }


    /**
     * Get seller primary balance amount
     * @param sellerCode seller code
     * @return balance amount
     * @throws DBCacheException If SellerWrapper not found in DBCache
     */
    public Double getSellerPrimaryBalanceAmount(String sellerCode) throws DBCacheException{
        return getSellerWrapper(sellerCode).seller.getPrimaryBalance().getAmount();
    }
    /**
     * Get seller primary balance amount
     * @param sellerId seller ID
     * @return balance amount
     * @throws DBCacheException If SellerWrapper not found in DBCache
     */
    public Double getSellerPrimaryBalanceAmount(Long sellerId) throws DBCacheException{
        return getSellerWrapper(sellerId).seller.getPrimaryBalance().getAmount();
    }

    /**
     * Get terminal primary balance amount
     * @param terminalSN terminal serial number
     * @return balance amount
     * @throws DBCacheException If TerminalWrapper not found in DBCache 
     */
    public Double getTerminalPrimaryBalanceAmount(String terminalSN) throws DBCacheException{
        return getTerminalWrapper(terminalSN).terminal.getPrimaryBalance().getAmount();
    }

    /**
     * Get Operator primary balance amount
     * @param operatorCode operator code
     * @return balance amount
     * @throws DBCacheException If OperatorWrapper not found in DBCache
     */
    public Double getOperatorPrimaryBalanceAmount(String operatorCode) throws DBCacheException{
        return getOperatorWrapper(operatorCode).operator.getPrimaryBalance().getAmount();
    }

    /**
     * Change seller balance. If balance not found by balanceCode then primary seller balance is changed.
     * @param sellerCode seller code
     * @param balanceCode balance code
     * @param value value to change balance on
     * @throws DBCacheException If SellerWrapper not found in DBCache
     */
    public synchronized void changeSellerBalance(String sellerCode, String balanceCode, double value, String transTypeCode) throws DBCacheException{
        SellerBalance sellerBalance = getSellerBalance(sellerCode, balanceCode);
        changeBalance(sellerBalance, value, transTypeCode);
//        setNewSellerBalanceAmount(sellerCode, sellerBalance);
    }

    /**
     * change operator balance. If balance not found by balanceCode then primary operator balance is changed.
     * @param operatorCode operator code.
     * @param balanceCode balance code 
     * @param value value to change balance on
     * @throws DBCacheException If operator wrapper not found in DBCache
     */
    public synchronized void changeOperatorBalance(String operatorCode, String balanceCode, double value, String transTypeCode) throws DBCacheException{
        OperatorBalance operatorBalance = getOperatorBalance(operatorCode, balanceCode);
        changeBalance(operatorBalance, value, transTypeCode);
    }

    /**
     * Change terminal balance. If balance not found by balanceCode then primary terminal balance is changed.
     * @param terminalSN terminal SN
     * @param balanceCode balance code
     * @param value value to change balance on 
     * @throws DBCacheException If terminal wrapper not found in DBCache
     */
    public synchronized void changeTerminalBalance(String terminalSN, String balanceCode, double value, String transTypeCode) throws DBCacheException{
        TerminalBalance terminalBalance = getTerminalBalance(terminalSN, balanceCode);
        changeBalance(terminalBalance, value, transTypeCode);
//        setNewTerminalBalanceAmount(terminalSN, terminalBalance);
    }

    private void setNewTerminalBalanceAmount(String terminalSN, TerminalBalance terminalBalance) throws DBCacheException {
        TerminalWrapper wrapper = getTerminalWrapper(terminalSN);
        BigDecimal amountBD = new BigDecimal(terminalBalance.getAmount());
        amountBD = amountBD.setScale(ROUND_SCALE, BigDecimal.ROUND_HALF_UP);
        wrapper.terminal.getPrimaryBalance().setAmount(amountBD.doubleValue());        
    }

    private void changeBalance(AbstractBalance balance, double value, String transTypeCode){
        if(TransactionType.CODE_CREDIT.equals(transTypeCode))
            balance.setAmount(balance.getAmount() - value);
        else if(TransactionType.CODE_DEBET.equals(transTypeCode))
            balance.setAmount(balance.getAmount() + value);
        else
            throw new IllegalArgumentException("undefined transaction type code - " + transTypeCode);
    }

    /**
     * getTerminal restrictions
     * @param terminalSN
     * @return restriction list
     * @exception DBCacheException if TerminalGroupWrapper not found by sellerCode
     */
    public List getTerminalRestrictions(String terminalSN) throws DBCacheException{
        return getTerminalWrapper(terminalSN).restriction;
    }

    /**
     * getTerminalGroup restriction list
     * @param terminalSN terminal serial number
     * @return restrictions
     * @exception DBCacheException if TerminalGroupWrapper not found by sellerCode
     */
    public List getTerminalGroupRestrictions(String terminalSN) throws DBCacheException{
        return getTerminalGroupWrapper(terminalSN).restriction;
    }

    /**
     * get Seller restrictions
     * @param sellerCode seller code
     * @return restriction list
     * @exception DBCacheException if SellerWrapper not found by sellerCode
     */
    public List getSellerRestrictions(String sellerCode) throws DBCacheException{
        return getSellerWrapper(sellerCode).restriction;
    }

    /**
     * get Processing restrictions
     * @param processingCode processing code
     * @return restriction list
     * @exception DBCacheException if processing wrapper not found by sellerCode
     */
    public List getProcessingRestrictions(String processingCode) throws DBCacheException{
        return getProcessingWrapper(processingCode).restriction;
    }

    /**
     * get Operator restrictions
     * @param operatorCode operator code
     * @return restrictions list
     * @exception DBCacheException if operator wrapper not found by sellerCode
     */
    public List getOperatorRestrictions(String operatorCode) throws DBCacheException{
        return getOperatorWrapper(operatorCode).restriction;
    }

    /**
     * get Terminal tariff eval function by name
     * @param terminalSN    terminal SN
     * @param tariffName    tariff name
     * @return tariff eval function
     */
//     public FunctionWrapper getTerminalTariff(String terminalSN, String tariffName) throws DBCacheException{
//        return (FunctionWrapper)getTerminalWrapper(terminalSN).
//                tariffName_tariffFunctionInstance.get(tariffName);
//    }


    /**
     * get active Terminal tariffs eval functions
     * @param terminalSN    operator code
     * @return tariff eval function list
     */
     public FunctionWrapper[] getActiveTerminalTariffs(String terminalSN) throws DBCacheException{
        TerminalWrapper wrapper = getTerminalWrapper(terminalSN);
        return collectActiveTariffs(wrapper.tariffId_tariffEntity.values(), wrapper.tariffId_tariffFunctionInstance);
    }


    /**
     * get Terminal tariff eval function by name
     * @param terminalSN    terminal SN
     * @param tariffName    tariff name
     * @return tariff eval function
     * @exception DBCacheException if TerminalGroupWrapper not found by terminal SN
     */
//     public FunctionWrapper getTerminalGroupTariff(String terminalSN, String tariffName) throws DBCacheException{
//        return (FunctionWrapper)getTerminalGroupWrapper(terminalSN).
//                tariffName_tariffFunctionInstance.get(tariffName);
//    }


    /**
     * get active Terminal groups tariffs eval functions
     * @param terminalSN    operator code
     * @return tariff eval function array
     * @exception DBCacheException if TerminalWrapper not found by terminal SN
     */
     public FunctionWrapper[] getActiveTerminalGroupsTariffs(String terminalSN) throws DBCacheException{
        TerminalGroupWrapper terminalGroupWrapper = getTerminalGroupWrapper(terminalSN);
        return collectActiveTariffs(terminalGroupWrapper.tariffId_tariffEntity.values(), terminalGroupWrapper.tariffId_tariffFunctionInstance);
    }

    /**
     * get Seller tariff eval function by name
     * @param sellerCode    seller code
     * @param tariffName    tariff name
     * @return tariff eval function
     */
//     public FunctionWrapper getSellerTariff(String sellerCode, String tariffName) throws DBCacheException{
//        return (FunctionWrapper)getSellerWrapper(sellerCode).
//                tariffName_tariffFunctionInstance.get(tariffName);
//    }


    /**
     * get active Seller tariffs eval functions
     * @param sellerCode    seller code
     * @return tariff eval function list
     */
     public FunctionWrapper[] getActiveSellerTariffs(String sellerCode) throws DBCacheException{
        SellerWrapper wrapper = getSellerWrapper(sellerCode);
        return collectActiveTariffs(wrapper.tariffId_tariffEntity.values(), wrapper.tariffId_tariffFunctionInstance);
    }

    /**
     * get Operator tariff eval function by name
     * @param operatorCode    seller code
     * @param tariffName    tariff name
     * @return tariff eval function
     */
//     public FunctionWrapper getOperatorTariff(String operatorCode, String tariffName) throws DBCacheException{
//        return (FunctionWrapper)getOperatorWrapper(operatorCode).
//                tariffName_tariffFunctionInstance.get(tariffName);
//    }

    /**
     * get active Operator tariffs eval functions
     * @param operatorCode    operator code
     * @return tariff eval function array
     * @throws DBCacheException on getOperatorWrapper() from DBCache error
     */
     public FunctionWrapper[] getActiveOperatorTariffs(String operatorCode) throws DBCacheException{
        OperatorWrapper wrapper = getOperatorWrapper(operatorCode);
        return collectActiveTariffs(wrapper.tariffId_tariffEntity.values(), wrapper.tariffId_tariffFunctionInstance);
    }

    /**
     *
     * @param sellerCode seller code
     * @return seller
     * @throws DBCacheException SellerWrapper not found 
     */
    public Seller getSeller(String sellerCode) throws DBCacheException{
        return getSellerWrapper(sellerCode).seller;
    }

    public Seller getSellerById(Long sellerId) throws DBCacheException{
        return sellers.getSeller(sellerId);
    }

    /**
     *
     * @param operatorCode operator code
     * @return Operator
     * @throws DBCacheException if operator not found
     */
    public Operator getOperator(String operatorCode) throws DBCacheException{
        return getOperatorWrapper(operatorCode).operator;
    }

    /**
     *
     * @param terminalSN terminal SN
     * @return Terminal
     * @throws DBCacheException if TerminalWrapper not found
     */
    public Terminal getTerminal(String terminalSN) throws DBCacheException{
        return getTerminalWrapper(terminalSN).terminal;
    }

    public Processing getProcessingById(Long processingId) throws DBCacheException{
        return processings.getProcessing(processingId);
    }


    /** builds chian of sellers related as parent-child from given seller up to tree root.
     * @param sellerCode seller code to start building the chain up to the tree.
     * @return the list of sellers codes */
    public List getSellerWrapperParentsChainCodes(String sellerCode){
        List sellersChainCodes = new ArrayList();
        for(Iterator iter = sellers.getSellerWrapperParentsChain(sellerCode).iterator(); iter.hasNext();){
            SellerWrapper sellerWrapper = (SellerWrapper)iter.next();
            sellersChainCodes.add(sellerWrapper.seller.getCode());
        }
        return sellersChainCodes;
    }

    /**
     * Returns seller balance id by balanceCode or primary seller balance id if balance by code not found.
     * @param sellerCode seller code
     * @param balanceCode balance code
     * @return SellerBalance
     * @throws DBCacheException If operator not found.
     */
    public SellerBalance getSellerBalance(String sellerCode, String balanceCode) throws DBCacheException{
        SellerWrapper wrapper = getSellerWrapper(sellerCode);
        Long balanceId = balanceCode != null ? (Long)wrapper.balanceCode_balanceId.get(balanceCode) : null;
        if(balanceId == null)
            balanceId = wrapper.seller.getPrimaryBalance().getBalanceId();
        return (SellerBalance)wrapper.balanceId_balance.get(balanceId);
    }
    /**
     *Change balance amount in DBCache 
     * @param sellerCode
     * @param balance
     * @throws DBCacheException
     */
    public void setNewSellerBalanceAmount(String sellerCode, SellerBalance balance) throws DBCacheException{
        SellerWrapper wrapper = getSellerWrapper(sellerCode);
        BigDecimal amountBD = new BigDecimal(balance.getAmount());
        amountBD = amountBD.setScale(ROUND_SCALE, BigDecimal.ROUND_HALF_UP);
        wrapper.seller.getPrimaryBalance().setAmount(amountBD.doubleValue());

    }


    /**
     * Returns operator balance by balanceCode or primary operator balance if balance by code not found.
     * @param operatorCode operator code
     * @param balanceCode balance code
     * @return OperatorBalance
     * @throws DBCacheException If operator not found.
     */
    public OperatorBalance getOperatorBalance(String operatorCode, String balanceCode) throws DBCacheException{
        OperatorWrapper wrapper = getOperatorWrapper(operatorCode);
        Long balanceId = balanceCode != null ? (Long)wrapper.balanceCode_balanceId.get(balanceCode) : null;
        if(balanceId == null)
            balanceId = wrapper.operator.getPrimaryBalance().getBalanceId();
        return (OperatorBalance)wrapper.balanceId_balance.get(balanceId);
    }


    /**
     * Returns terminal balance id by balanceCode or primary terminal balance id if balance by code not found.
     * @param terminalSn terminal serial number
     * @param balanceCode balance code
     * @return TerminalBalance
     * @throws DBCacheException If terminal not found.
     */
    public TerminalBalance getTerminalBalance(String terminalSn, String balanceCode) throws DBCacheException{
        TerminalWrapper wrapper = getTerminalWrapper(terminalSn);
        Long balanceId = balanceCode != null ? (Long)wrapper.balanceCode_balanceId.get(balanceCode) : null;
        if(balanceId == null)
            balanceId = wrapper.terminal.getPrimaryBalance().getBalanceId();
        return (TerminalBalance)wrapper.balanceId_balance.get(balanceId);
    }

    public OperationType getOperationType(String operationName){
        return operationTypes.getOperationType(operationName); 
    }

    public List<String> getVoucherAllNominals(String serviceCode) {
         return voucherNominals.getVoucherNominals(serviceCode);       
    }

    public List<String> getVoucherAllNominalsAndPrices(String serviceCode) {
         return voucherNominalsAndPrices.getVoucherNominalsAndPrices(serviceCode);
    }

    //------------Private methods

    /**
     * collect active tarrifs from given tariffs entities collection and return list
     * of corresponding tariff functions.
     * @param tariffEntities collection of tariffs entiites
     * @param tariffFunctions map of tariff functions
     * @return array of functions of active tariff
     *  */
    private FunctionWrapper[] collectActiveTariffs(Collection tariffEntities, Map tariffFunctions){
        List activeTariffs = new ArrayList();
        for(Iterator iter = tariffEntities.iterator(); iter.hasNext();){
            AbstractTariff tariff = (AbstractTariff)iter.next();
            long now = System.currentTimeMillis();

            if((tariff.getActivationDate() == null || tariff.getActivationDate().getTime() <= now) &&
               (tariff.getDeactivationDate() == null || tariff.getDeactivationDate().getTime() >= now))
                activeTariffs.add(tariffFunctions.get(tariff.getId()));
        }
        return (FunctionWrapper[])activeTariffs.toArray(new FunctionWrapper[0]);
    }

    private TerminalWrapper getTerminalWrapper(String terminalSN) throws DBCacheException{
        TerminalWrapper wrapper = terminals.getTerminalWrapper(terminalSN);
        if(wrapper == null)
            throw new DBCacheException("err.dbcache.terminal_not_found", new String[]{terminalSN});
        return wrapper;
    }

    private TerminalGroupWrapper getTerminalGroupWrapper(String terminalSN) throws DBCacheException{
        TerminalWrapper wrapper = getTerminalWrapper(terminalSN);
        TerminalGroupWrapper terminalGroupWrapper = terminalGroups.getTerminalGroupWrapper(
        wrapper.terminal.getTerminalGroupId());
        if(terminalGroupWrapper == null)
            throw new DBCacheException("err.dbcache.terminalgroup_not_found", new String[]{terminalSN});
        return terminalGroupWrapper;
    }

    private SellerWrapper getSellerWrapper(String sellerCode) throws DBCacheException{
        SellerWrapper wrapper = sellers.getSellerWrapper(sellerCode);
        if(wrapper == null)
            throw new DBCacheException("err.dbcache.seller_not_found", new String[]{sellerCode});
        return wrapper;
    }

    private SellerWrapper getSellerWrapper(Long sellerId) throws DBCacheException{
        SellerWrapper wrapper = sellers.getSellerWrapper(sellerId);
        if(wrapper == null)
            throw new DBCacheException("err.dbcache.seller_not_found", new String[]{"ID ".concat(sellerId.toString())});
        return wrapper;
    }

    private OperatorWrapper getOperatorWrapper(String operatorCode) throws DBCacheException{
        OperatorWrapper wrapper = operators.getOperatorWrapper(operatorCode);
        if(wrapper == null)
            throw new DBCacheException("err.dbcache.operator_not_found", new String[]{operatorCode});
        return wrapper;
    }

    private ProcessingWrapper getProcessingWrapper(String processingCode) throws DBCacheException{
        ProcessingWrapper wrapper = processings.getProcessingWrapper(processingCode);
        if(wrapper == null)
            throw new DBCacheException("err.dbcache.processing_not_found", new String[]{processingCode});
        return wrapper;
    }

    public void dump() throws DBCacheException {
        String result = "\n BEGIN DUMP \n operators \n";
        Set codesSet = operators.operatorCode_opeartor.keySet();
        for(Iterator iterator = codesSet.iterator();iterator.hasNext();){
            String operatorCode = (String) iterator.next();
            OperatorBalance ob = getOperatorWrapper(operatorCode).operator.getPrimaryBalance();
            if (ob != null)
                result += "code:" + operatorCode + "; pbc:" + ob.getBalanceId() + "; amount:" + getOperatorPrimaryBalanceAmount(operatorCode) + "\n";
            else
                result += "code:" + operatorCode + "; pb: null;" + "\n";
        }

        result += "sellers \n";
        codesSet = sellers.sellerCode_seller.keySet();
        for(Iterator iterator = codesSet.iterator();iterator.hasNext();){
            String sellerCode = (String) iterator.next();
            SellerBalance sb = getSellerWrapper(sellerCode).seller.getPrimaryBalance();
            if (sb != null)
                result += "code:" + sellerCode + "; pbc:" + sb.getBalanceId() + "; amount:" + getSellerPrimaryBalanceAmount(sellerCode) + "\n";
            else
                result += "code:" + sellerCode + "; pb: null;" + "\n";
        }

        result += "terminals \n";
        codesSet = terminals.terminalSN_terminal.keySet();
        for(Iterator iterator = codesSet.iterator();iterator.hasNext();){
            String sn = (String) iterator.next();
            TerminalBalance tb = getTerminalWrapper(sn).terminal.getPrimaryBalance();
            if (tb != null)
                result += "sn:" + sn + "; pbc:" + tb.getBalanceId() + "; amount:" + getTerminalPrimaryBalanceAmount(sn) + "\n";
            else
                result += "sn:" + sn + "; pb: null;" + "\n";
        }
        result += "END DUMP";
        logger.info(result);
    }

}

class VoucherNominals {
    private Map<String, List<String>> serviceCode_nominals = new Hashtable<String, List<String>>();

    VoucherNominals(Operators operators) throws DatabaseException {
        VoucherManager vm = new VoucherManager();
        Collection<OperatorWrapper> wrappers = (Collection<OperatorWrapper>) operators.operatorCode_opeartor.values();
        for (OperatorWrapper wrapper : wrappers) {
            serviceCode_nominals.put(wrapper.operator.getCode(), vm.getNominalsByOperatorId(wrapper.operator.getOperatorId()));
        }
    }

    List<String> getVoucherNominals(String serviceCode) {
        return serviceCode_nominals.get(serviceCode);
    }
}

class VoucherNominalsAndPrices {
    private Map<String, List<String>> serviceCode_nominals_prices = new Hashtable<String, List<String>>();

    VoucherNominalsAndPrices(Operators operators) throws DatabaseException {
        VoucherManager vm = new VoucherManager();
        Collection<OperatorWrapper> wrappers = (Collection<OperatorWrapper>) operators.operatorCode_opeartor.values();
        for (OperatorWrapper wrapper : wrappers) {
            serviceCode_nominals_prices.put(wrapper.operator.getCode(), vm.getNominalsAndPricesByOperatorId(wrapper.operator.getOperatorId()));
        }
    }

    List<String> getVoucherNominalsAndPrices(String serviceCode) {
        return serviceCode_nominals_prices.get(serviceCode);
    }
}


