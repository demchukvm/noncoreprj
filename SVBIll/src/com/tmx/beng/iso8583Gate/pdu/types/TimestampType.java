package com.tmx.beng.iso8583Gate.pdu.types;

import com.tmx.beng.iso8583Gate.pdu.RuntimeDataElement;
import com.tmx.beng.iso8583Gate.pdu.IsoParseException;
import com.tmx.beng.iso8583Gate.pdu.IsoSerializeException;
import com.tmx.beng.iso8583Gate.pdu.IncorrectFormatException;
import com.tmx.beng.iso8583Gate.pdu.util.HexConverter;

import java.io.InputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.util.Map;
import java.util.HashMap;
import java.util.Date;
import java.text.SimpleDateFormat;
import java.text.ParseException;

/**
 */
public class TimestampType extends Type {
    /** RM: ISO has no 't' type for time. It uses 'n' instead.
     * We introduce dedicated 't' fromat for time to distinguish it from numeric types */
    {
        name = "t";
    }

    /** ISO8583 to Java time patterms reflection */
    private static Map patternReflection;
    private HexConverter hexConverter = new HexConverter();

    static{
        patternReflection = new HashMap();
        patternReflection.put("YY", "yy");
        patternReflection.put("MM", "MM");
        patternReflection.put("DD", "dd");
        patternReflection.put("hh", "HH");
        patternReflection.put("mm", "mm");
        patternReflection.put("ss", "ss");
    }

    public void parse(InputStream is, int length, RuntimeDataElement rde) throws IsoParseException {
        String isoPattern = rde.getFormat().getExtendedFormat();
        String javaPattern = convertIsoToJavaTimePattern(isoPattern);
        String timestampStr = null;
        try{
            timestampStr = hexConverter.getStringFromHex(is, length);
            rde.setValue(new SimpleDateFormat(javaPattern).parse(timestampStr));
        }
        catch(IOException e){
            throw new IsoParseException("err.timestamp_type.parsing_failed", new String[]{isoPattern, javaPattern, timestampStr}, e);
        }
        catch(ParseException e){
            throw new IsoParseException("err.timestamp_type.parsing_failed", new String[]{isoPattern, javaPattern, timestampStr}, e);
        }

    }

    public SerializedValue serialize(boolean fixedLength, int bcdLengthLimit, RuntimeDataElement rde) throws IsoSerializeException{
        try{
            Date date = rde.getDate();
            String timeFormat = convertIsoToJavaTimePattern(rde.getFormat().getExtendedFormat());
            String formattedDate = new SimpleDateFormat(timeFormat).format(date);

            SerializedValue sv = new SerializedValue();
            sv.data = hexConverter.getHexFromString(formattedDate);
            sv.length = hexConverter.getHexSize(sv.data.length);
            if(sv.length > bcdLengthLimit)
                throw new IncorrectFormatException("err.timestamp_type.time_array_size_more_then_maxlength", new String[]{String.valueOf(sv.length), String.valueOf(bcdLengthLimit)});

            if(fixedLength && (sv.data.length < hexConverter.getByteSize(bcdLengthLimit)))
                sv.data = expandFixedLengthValue(sv.data, bcdLengthLimit);

            return sv;
        }
        catch (IncorrectFormatException e) {
            throw new IsoSerializeException("err.timestamp_type.serialization_failed", e);
        }
    }

    protected byte[] expandFixedLengthValue0(byte[] value, int fixedByteLength) throws IncorrectFormatException{
        //fill tail of byte array by 0 to match fixed length
        byte[] expandedData = new byte[fixedByteLength];
        System.arraycopy(value, 0, expandedData, 0, value.length);
        return expandedData;
    }

    public String convertIsoToJavaTimePattern(String isoPattern){
        StringBuffer javaPattern = new StringBuffer();
        StringBuffer isoPatternComponent = new StringBuffer();
        for(int i=0; i<isoPattern.length(); i++){
            String nextSymbol = isoPattern.substring(i, i+1);
            if(isoPatternComponent.length() == 0 || isoPatternComponent.substring(0,1).equals(nextSymbol)){
                isoPatternComponent.append(nextSymbol);//collect pattern component of the same chars
            }
            else{
                String javaPatternComponent = (String)patternReflection.get(isoPatternComponent.toString());
                //add resolved java pattern component or original symbol from ISO string if one not found.
                //there are delimiters possible 
                javaPattern.append(javaPatternComponent != null ? javaPatternComponent : isoPatternComponent.toString());
                isoPatternComponent = new StringBuffer();
                isoPatternComponent.append(nextSymbol);//collect pattern component of the new chars
            }
        }
        //process tail pattern component
        String javaPatternComponent = (String)patternReflection.get(isoPatternComponent.toString());
        javaPattern.append(javaPatternComponent != null ? javaPatternComponent : isoPatternComponent.toString());
        return javaPattern.toString();
    }
}
