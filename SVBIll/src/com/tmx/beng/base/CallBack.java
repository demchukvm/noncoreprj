package com.tmx.beng.base;

/**

 */
public interface CallBack {
    public void sendBilledMessage(BillingMessage message);
    public BillingMessage receiveBilledMessage(long waitTimeOut, String operationName) throws InterruptedException;    
}
