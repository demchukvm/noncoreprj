function set_visible(id1, id2, state)
{
	var a = document.getElementById(id1);
	var b = document.getElementById(id2);
	if(state == 'open')
	{
		a.style.display = 'block';
		b.style.display = 'none';
		set_position(id1);
	}
	else
	{
		a.style.display = 'none'
		b.style.display = 'block';
	}
}