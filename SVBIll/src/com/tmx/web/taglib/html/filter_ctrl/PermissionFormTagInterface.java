package com.tmx.web.taglib.html.filter_ctrl;

import java.util.List;

public interface PermissionFormTagInterface {
    public Long getFormPermission(String form);
    public List getPermissionTypes();
    public boolean isActive();
//    class Bean{
//        private Long id;
//        private String value;
//        public Long getId() {
//            return id;
//        }
//        public void setId(Long id) {
//            this.id = id;
//        }
//        public String getValue() {
//            return value;
//        }
//        public void setValue(String value) {
//            this.value = value;
//        }
//    }
}
