package com.tmx.web.controls;

import java.util.List;
import java.util.Iterator;

/**
 *
 */
public abstract class Panel extends BasicControl{
    private ControlContainer ctrlContainer = null;

    /** You can use this method in successor to initialize it */
    public abstract void init();

    /** Call initialization method of sucessor and
     * then initialize contaianer. Method should be final to
     * prevent possible overriding of mandatory container initialization code. */
    public final void initialize(){
        /** Initialize successor first. Some controls could be initialized
         * and then will be registered on container initialization. */
        init();
        ctrlContainer = new ControlContainer(this);
        /** Initalize container */
        ctrlContainer.init(getOwner(), this);
    }

    public boolean isContainer() {
        return true;
    }

    public ControlContainer getContainer() {
        return ctrlContainer;
    }
}