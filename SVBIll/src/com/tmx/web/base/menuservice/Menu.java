package com.tmx.web.base.menuservice;

import com.tmx.web.base.menuservice.beans.MenuContainer;
import com.tmx.web.base.menuservice.beans.MenuItem;
import com.tmx.web.base.menuservice.beans.RootDocument;
import com.tmx.web.base.ApplicationEnvironment;
import com.tmx.util.i18n.MessageResources;
import com.tmx.as.entities.general.ui_permission.UserInterfaceType;
import com.tmx.as.entities.general.ui_permission.PermissionType;
import org.w3c.dom.Document;
import org.apache.struts.config.ModuleConfig;
import org.apache.struts.config.ActionConfig;
import org.apache.struts.config.ForwardConfig;

/**
 */
public class Menu {
    //report config
    MenuService.MenuConfig config = null;
    private final String DELIMITER = ".";

    //package local constructor. Use MenuService to get Menu  instance
    Menu(){
    }

    public void init(MenuService.MenuConfig config) throws MenuException{
        //copy config into internal definition
        this.config = config;
        preprocess();
    }


    public String serialize() throws MenuException{
        String docString = null;
        try {
            //docString = StringUtil.removeReturnSymbols(rootDoc.toString());
            docString = config.getRootDoc().toString();
        }
        catch (Exception e) {
            //RM put into log
            e.printStackTrace();
            throw new MenuException("err.menu.xml_doc_serialization_failed", e);
        }
        return docString;
    }

    public Document getDocument(){
        return config.getRootDoc().getRoot().getDomNode().getOwnerDocument();
    }

    /** Evaluates the start-up item to load the page, that item is pointing on*/
    public String getFirstItemURL(){
        String startUpUrl = null;
        MenuContainer[] menuContainers = config.getRootDoc().getRoot().getMenuContainerArray();
        if(menuContainers != null){
            for(int i=0; i<menuContainers.length; i++){
                if((startUpUrl = getFirstItemURL0(menuContainers[i])) != null)
                    return startUpUrl;
            }
        }
        return null; //any appropriate mItem have found.
    }


    /** For recursive traversal the menu structure. */
    private String getFirstItemURL0(MenuContainer mContainer){
        if(mContainer == null || mContainer.getReadonly())
            return null;
        if(mContainer.getMenuItemArray() != null){
            MenuItem[] mItems = mContainer.getMenuItemArray();
            for(int i=0; i<mItems.length; i++){
                if(!mItems[i].getReadonly() && mItems[i].getUrl() != null)
                    return mItems[i].getUrl();
            }
        }
        //If there are any mItems or all of them are readonly, then traverse enclosed mContainers
        if(mContainer.getMenuContainerArray() != null){
            MenuContainer[] enclosedMContainer = mContainer.getMenuContainerArray();
            String url = null;
            for(int i=0; i<enclosedMContainer.length; i++){
                if((url = getFirstItemURL0(enclosedMContainer[i])) != null)
                    return url;
            }
        }
        //If there are no Items whithin entire containers, then return null
        return null;
    }


    /** Run over menu and localizeItem all labels using 'name' attributes. */
    private void preprocess() throws MenuException{
        MessageResources messageResources = MessageResources.getInstance();
        MenuContainer[] mContainers = config.getRootDoc().getRoot().getMenuContainerArray();
        if(mContainers != null){
            for(int i=0; i<mContainers.length; i++){
                preprocessContainer0(mContainers[i], null, messageResources);
            }
        }
    }


    private void preprocessContainer0(MenuContainer mContainer, MenuContainer parentMContainer, MessageResources messageResources) throws MenuException{
        //preprocess this container
        evaluateContainerAttrs(mContainer, parentMContainer);
        localizeContainer(mContainer, messageResources);

        //recursively preprocess sub menus
        MenuContainer[] mContainers = mContainer.getMenuContainerArray();
        if(mContainers != null){
            for(int i=0; i<mContainers.length; i++){
                preprocessContainer0(mContainers[i], mContainer, messageResources);
            }
        }

        //preprocess items of this menu
        MenuItem[] mItems = mContainer.getMenuItemArray();
        if(mItems != null){
            for(int i=0; i<mItems.length; i++){
                // validate
                validateItem(mItems[i]);
                // evaluate attrs
                evaluateItemAttrs(mItems[i], mContainer);
                // localizeItem
                localizeItem(mItems[i], messageResources);
            }
        }
    }

    private void validateItem(MenuItem mItem) throws MenuException{
        int mutualExclusiveAttrsCount = 0;//failed if more then one
        if(mItem.getForward() != null)
            mutualExclusiveAttrsCount++;
        if(mItem.getAction() != null)
            mutualExclusiveAttrsCount++;
        if(mItem.getUrl() != null)
            mutualExclusiveAttrsCount++;
        if(mItem.getJavascript() != null)
            mutualExclusiveAttrsCount++;
        if(mutualExclusiveAttrsCount > 1)
            throw new MenuException("err.menu.mutual_exlusive_attrs_occured", new String[]{mItem.getName()});
    }

    private void localizeItem(MenuItem mItem, MessageResources messageResources) {
        String localizedLabel = messageResources.getOwnedLocalizedMessage(this.getClass(), "menu_labels", config.getLocale(), mItem.getFullName(), null);
        mItem.setLabel(localizedLabel);
    }

    private void localizeContainer(MenuContainer mContainer, MessageResources messageResources) {
        String localizedLabel = messageResources.getOwnedLocalizedMessage(this.getClass(), "menu_labels", config.getLocale(), mContainer.getFullName(), null);
        mContainer.setLabel(localizedLabel);
    }

    private void evaluateContainerAttrs(MenuContainer mContainer, MenuContainer parentMContainer){
        //Evaluate full name
        String fullNamePrefix = "";
        if(parentMContainer != null){
            if(parentMContainer.getFullName() != null)
                fullNamePrefix = parentMContainer.getFullName() + DELIMITER;
            else
                fullNamePrefix = parentMContainer.getName() + DELIMITER;
        }
        mContainer.setFullName(fullNamePrefix + mContainer.getName());

        //Evaluate level
        int level = 0;
        if(parentMContainer != null)
            level = parentMContainer.getLevel() + 1;
        mContainer.setLevel(level);
    }

    private void evaluateItemAttrs(MenuItem mItem, MenuContainer mContainer) throws MenuException{
        //Evaluate level
        mItem.setLevel(mContainer.getLevel());
        //Evaluate full name
        mItem.setFullName(mContainer.getFullName() + DELIMITER + mItem.getName());
        //Evaluate actions and forwards
        if(mItem.getAction() != null){
            mItem.setUrl(getActionURL(mItem.getAction()));
            //mItem.setAction(null);
        }
        if(mItem.getForward() != null){
            mItem.setUrl(getForwardURL(mItem.getForward()));
            mItem.setForward(null);
        }
    }


    private String getActionURL(String actionName) throws MenuException{
        //Get URL by forward name
        ModuleConfig mc = ApplicationEnvironment.getModuleConfig();
        ActionConfig actionConfig = mc.findActionConfig(actionName);
        if(actionConfig == null)
            throw new MenuException("err.menu.action_not_registered", new String[]{actionName});

        return ApplicationEnvironment.getAppDeploymentName() + actionConfig.getPath() + ApplicationEnvironment.getActionExtension();
    }

    private String getForwardURL(String forwardName) throws MenuException{
        //Get URL by forward name
        ModuleConfig mc = ApplicationEnvironment.getModuleConfig();
        ForwardConfig forwardConfig = mc.findForwardConfig(forwardName);
        if(forwardConfig == null)
            throw new MenuException("err.menu.forward_not_registered", new String[]{forwardName});

        return ApplicationEnvironment.getAppDeploymentName() + forwardConfig.getPath();
    }

    void applySecurityPolicy(String roleName) throws MenuException{
        try{
           // Get logged in user's role
           com.tmx.as.base.SecurityService asSecurityService = com.tmx.as.base.SecurityService.getInstance();

           //Check permission for entire menu
           RootDocument.Root root = config.getRootDoc().getRoot();
           String permissionType = asSecurityService.getPermissionType(roleName, UserInterfaceType.MENU, config.getTemplateName() + DELIMITER + config.getTemplateName());
           if(PermissionType.FORBIDDEN.equals(permissionType)){
               //remove all menu containers from menu
               int menuContainersCount = root.sizeOfMenuContainerArray();
               for(int i=0; i<menuContainersCount; i++){
                   root.removeMenuContainer(0);//after remove all ellements shift to zero on one position.
               }
           }
           else if(PermissionType.READONLY.equals(permissionType)){
               for(int i=0; i<root.sizeOfMenuContainerArray(); i++){
                   root.getMenuContainerArray(i).setReadonly(true);
               }
           }

           //chek permission for included menu's containers
           int idxCorrection = 0;
           for(int i=0; i<root.sizeOfMenuContainerArray(); i++){
              idxCorrection = applySecurityPolicyForMenuContainer(root.getMenuContainerArray(i), root, i, asSecurityService, roleName);
              i = i + idxCorrection;
           }
       }
       catch(MenuException e){
           throw e;
       }
       catch(Throwable e){
           throw new MenuException("err.menu.failed_to_apply_security_policy_for_menu", e);
       }
    }

    /** @return int correction for iterator over MenuContainers.
     * If MenuContainer will be removed, then method returns "-1".  In other case it returns "0". */
    private int applySecurityPolicyForMenuContainer(MenuContainer menuContainer, Object parent, int containerIdx, com.tmx.as.base.SecurityService asSecurityService, String roleName) throws MenuException{
        try{
           String permissionType = asSecurityService.getPermissionType(roleName, UserInterfaceType.MENU_CONTAINER, config.getTemplateName() + DELIMITER + menuContainer.getFullName());
           if(PermissionType.FORBIDDEN.equals(permissionType)){
               //remove this menu containers from menu
               if(parent instanceof RootDocument.Root){
                   ((RootDocument.Root)parent).removeMenuContainer(containerIdx);
                   return -1;
               }
               else if(parent instanceof MenuContainer){
                   ((MenuContainer)parent).removeMenuContainer(containerIdx);
                   return -1;
               }
               else{
                   throw new MenuException("err.menu.unsupported_menu_container_type", new String[]{((parent != null) ? parent.getClass().getName() : null)});
               }
           }
           else if(PermissionType.READONLY.equals(permissionType) ||
                   parent != null && parent instanceof MenuContainer && ((MenuContainer)parent).getReadonly()){
               menuContainer.setReadonly(true);
           }

           int idxCorrection = 0;
           //chek permission for included menu's containers
           for(int i=0; i<menuContainer.sizeOfMenuContainerArray(); i++){
              idxCorrection = applySecurityPolicyForMenuContainer(menuContainer.getMenuContainerArray(i), menuContainer, i, asSecurityService, roleName);
              i = i + idxCorrection;
           }

           //chek permission for included menu's items
           for(int i=0; i<menuContainer.sizeOfMenuItemArray(); i++){
              idxCorrection = applySecurityPolicyForMenuItem(menuContainer.getMenuItemArray(i), menuContainer, i, asSecurityService, roleName);
              i = i + idxCorrection;
           }

        }
        catch(MenuException e){
            throw e;
        }
        catch(Throwable e){
            throw new MenuException("err.menu.failed_to_apply_security_policy_for_menu_container", e);
        }
        return 0;
    }

    /** @return int correction for iterator over MenuItems.
     * If MenuItem will be removed, then method returns "-1".  In other case it returns "0". */
    private int applySecurityPolicyForMenuItem(MenuItem mItem, MenuContainer parentMenuContainer, int itemIdx, com.tmx.as.base.SecurityService asSecurityService, String roleName) throws MenuException{
        try{
            String actionFormPermissionType=null;
            if(mItem.getAction()!=null){
                ModuleConfig mc = ApplicationEnvironment.getModuleConfig();
                String beanName=mc.findActionConfig(mItem.getAction()).getName();
                if(beanName!=null)
                actionFormPermissionType=asSecurityService.getPermissionType(roleName, UserInterfaceType.FORM, beanName);
            }
            String permissionType = asSecurityService.getPermissionType(roleName, UserInterfaceType.MENU_ITEM, config.getTemplateName() + DELIMITER + mItem.getFullName());

            if(PermissionType.FORBIDDEN.equals(permissionType)||
              (actionFormPermissionType!=null&&PermissionType.FORBIDDEN.equals(actionFormPermissionType))){
                //remove this menu containers from menu
                parentMenuContainer.removeMenuItem(itemIdx);
                return -1;
            }
            else if(PermissionType.READONLY.equals(permissionType) ||
                   (parentMenuContainer != null && parentMenuContainer.getReadonly())){
                mItem.setReadonly(true);
            }
        }
        catch(Throwable e){
            throw new MenuException("err.menu.failed_to_apply_security_policy_for_menu_item", e);
        }
        return 0;
    }
}

