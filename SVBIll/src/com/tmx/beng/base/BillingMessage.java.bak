package com.tmx.beng.base;

import java.io.Serializable;

/**
 */
public interface BillingMessage extends Serializable{
    public final String O_REFILL_PAYMENT = "request.refillPayment"; 
    public final String O_REFILL_PAYMENT_RESULT = "response.refillPayment";
    public final String O_REFILL_PAYMENT_STATUS = "request.refillPaymentStatus";
    public final String O_CANCEL_REFILL_PAYMENT = "request.cancelRefillPayment";
    public final String O_CANCEL_REFILL_STATUS = "response.cancelRefillPayment";
    public final String O_BUY_VOUCHER = "request.buyVoucher";
    public final String O_RETURN_VOUCHER = "request.returnVoucher";
    public final String O_REGISTER_TERMINAL = "request.registerTerminal";
    public final String O_RELOAD_DB_CACHE = "request.reloadDbCache";
    public final String O_VALIDATE_FUNCTION_PARAMS = "request.validateFunctionParams";
    public final String O_RETRIEVE_DEFAULT_FUNCTION_PARAMS = "request.retrieveDefaultFunctionParams";
    public final String O_CHAHGE_SELLER_BALLANCE = "request.changeSellerBalance";
    public final String O_CHAHGE_OPERATOR_BALLANCE = "request.changeOperatorBalance";
    public final String O_CHAHGE_TERMINAL_BALLANCE = "request.changeTerminalBalance";
	public final String O_DB_CACHE_DUMP = "request.DBCacheDump";
	public final String O_PROCESSING_RESOLVER_TEST = "request.processingResolverTest";

    //Operation attributes
    public final String BILL_TRANSACTION_NUM = "billTransactionNum";
    public final String REQUEST_ID = "requestId";
    public final String CLI_TRANSACTION_NUM = "cliTransactionNum";
    public final String SERVICE_CODE = "serviceCode";
    public final String AMOUNT = "amount";
    public final String TRANSACTION_DESCRIPTION = "transaction description";
    public final String USER_ACTOR_ID = "user actor id";
    public final String ACCOUNT_NUMBER = "accountNumber";
    public final String PAY_ACCOUNT_NUMBER = "payAccountNumber";
    public final String VOUCHER = "voucher";
    public final String VOUCHER_NOMINAL = "voucherNominal";
    public final String VOUCHER_CODE = "voucherCode";
    public final String VOUCHER_SECRET_KEY = "voucherSecretKey";
    public final String VOUCHER_BEST_BEFORE_DATE = "voucherBestBeforeDate";
    public final String VOUCHER_RENEVAL_TIME = "voucherRenewalTime";
    public final String STATUS_CODE = "statusCode";
    public final String STATUS_CODE_STACK = "statusCodeStack";
    public final String STATUS_MESSAGE = "statusMessage";
    public final String WAIT_TIME = "waitTime";
    public final String REGISTRATION_TIME = "registrationTime";
    public final String CLIENT_TIME = "clientTime";    
    public final String RECEIPT_NUM = "receiptNum";
    public final String PAY_ID = "pay_id";
    public final String REGION_NUMBER = "regionNumber";
    public final String TRADE_POINT = "tradePoint";
    public final String TRANSACTION_TYPE = "transactionType";
    public final String FUNCTION_INSTANCE_ID = "functionInstanceId";
    public final String FUNCTION_TYPE_ID = "functionTypeId";
    public final String ACTUAL_PARAMS_LIST = "params";
    public final String BALANCE_CODE = "balanceCode";
    public final String ERROR_LIST = "errorList";

    public final String CHANGE_SELLER_BALANCE_TYPE = "sellerType";
    public final String CHANGE_OPERATOR_BALANCE_TYPE = "operatorType";
    public final String CHANGE_TERMINAL_BALANCE_TYPE = "terminalType";
	public final String TRANSACTION_TYPE_ID = "transaction_type_id";
    public final String PAYMENT_REASON = "payment_reason";

    public final String CLIENTCERTIFICATE = "clientCertificate";
    public final String CLIENTCERTIFICATEKEY = "clientCertificateKey";

    //Auth part
    public final String TERMINAL_SN = "terminalSN";
    public final String SELLER_CODE = "sellerCode";
    public final String PROCESSING_CODE = "processingCode";
    public final String LOGIN = "login";
    public final String PASSWORD = "password";

    // Inner attributes
    public final String BALANCE_RESERVATION_SET = "balanceReservationSet";
    public final String ENTITY_LIST = "entityList";

    // Registartinon
    public final String TERMINAL_SIGNATURE ="terminalSignature";

    // Transactions
    public final String CLIENT_TRANSACTION = "transaction.client";
    public final String OPERATOR_TRANSACTION = "transaction.operator";
    public final String TERMINAL_TRANSACTION = "transaction.terminal";
    public final String MOBIPAY_KYIVSTAR = "transaction.mobipay.kyivstar";

    public static final String IS_OPERATOR_AMOUNT_SET = "is_o_amount_set";
    //Need for graduated tarrife
    public static final String GRADUATED_AMOUNT_FOR_OPERATOR = "tarrifed_amount";

    // Test
    

    /** Return billing message operation name
     * @return operation name */
    public String getOperationName();
    /** Return message string attribute value by name
     * @param name attribute name
     * @return attribute value */
    public String getAttributeString(String name);
    /** Return message attribute value by name
     * @param name attribute name
     * @return attribute value */
    public Object getAttribute(String name);
    /** Return message attributes names as array of String
     * @return array of attributes names */
    public String[] getAttributesNames();
}
