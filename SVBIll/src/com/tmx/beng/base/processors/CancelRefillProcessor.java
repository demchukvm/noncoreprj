package com.tmx.beng.base.processors;

import com.tmx.beng.base.*;
import com.tmx.beng.base.cache.DBCache;
import com.tmx.as.entities.bill.transaction.TerminalBalanceTransaction;
import com.tmx.as.entities.bill.transaction.ClientTransaction;
import com.tmx.as.entities.bill.transaction.OperatorBalanceTransaction;
import com.tmx.as.base.EntityManager;
import com.tmx.as.exceptions.DatabaseException;

/**
 Join to opened transaction.
 Used when
 (1) process responds from operators and
 (2) status requests from client.
 */
public class CancelRefillProcessor extends TaskProcessor {

    public Task process0(Task task, DBCache dbCache, BillingMessageWritable msg) throws BillException {
        logger.debug("Cancel refill processor...");
        int status = Integer.parseInt(msg.getAttributeString(BillingMessageWritable.STATUS_CODE));
        // correct status
        if(status == StatusDictionary.STATUS_OK){
            EntityManager entityManager = new EntityManager();
            try{
                TerminalBalanceTransaction terminalTransaction = (TerminalBalanceTransaction) msg.getAttribute(
                        BillingMessage.TERMINAL_TRANSACTION);
                if(terminalTransaction != null){
                    terminalTransaction.setStatus(Integer.toString(StatusDictionary.TRANSACTION_CANCELED));
                    entityManager.SAVE(terminalTransaction);
                }
                ClientTransaction clientTransaction = (ClientTransaction) msg.getAttribute(
                        BillingMessage.CLIENT_TRANSACTION);
                if(clientTransaction != null){
                    clientTransaction.setStatus(Integer.toString(StatusDictionary.TRANSACTION_CANCELED));
                    entityManager.SAVE(clientTransaction);
                }
                OperatorBalanceTransaction operatorTransaction = (OperatorBalanceTransaction) msg.getAttribute(
                        BillingMessage.OPERATOR_TRANSACTION);
                if(operatorTransaction != null){
                    operatorTransaction.setStatus(Integer.toString(StatusDictionary.TRANSACTION_CANCELED));
                    entityManager.SAVE(operatorTransaction);
                }
            }catch(DatabaseException e){
                logger.error(e.getLocalizedMessage());
            }
        }

        return task;
    }
}
