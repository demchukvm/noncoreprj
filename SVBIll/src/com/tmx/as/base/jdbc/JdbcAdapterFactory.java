package com.tmx.as.base.jdbc;

import com.tmx.util.InitException;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;
import java.sql.SQLException;

/**
 Evaluate current dataSource from given entity class, then
 instantiate corresponding JDBC adapter.
 @pattern singleton
 */
public class JdbcAdapterFactory {
    private static JdbcAdapterFactory factory;
    private Map adapters = new HashMap();
    private JdbcAdapter defaultAdapter = null;

    /** hide constructor */
    private JdbcAdapterFactory(){}

    public static JdbcAdapterFactory getInstance(){
        if(factory == null)
            factory = new JdbcAdapterFactory();
        return factory;
    }


    /** Init factory to support given datasource
     * @param dataSourceName datasource name
     * @param sessionFactoryId session factory Id 
     * @throws com.tmx.util.InitException on adapter instantiation fail 
     * */
    public void init(String dataSourceName, String sessionFactoryId) throws InitException{
        //initialize default adapter
        if(defaultAdapter == null)
            defaultAdapter = new VoidJdbcAdapter();

        //initalize adapter for given datasource
        DataSource ds = obtainDataSource(dataSourceName);
        Class adapterClass = null;
        try{
            adapterClass = resolveAdapterClass(
                ds.getConnection().getMetaData().getDriverName(),
                ds.getConnection().getMetaData().getDriverVersion());

            //check JdbcAdapter & JdbcAdapterSpi implementation 
            if(!JdbcAdapter.class.isAssignableFrom(adapterClass) ||
               !JdbcAdapterSpi.class.isAssignableFrom(adapterClass))
                throw new InitException("err.jdbcadapterfactory.jdbcadapter_is_not_implements_required_interface", new String[]{adapterClass.getName(), JdbcAdapter.class.getName(), JdbcAdapterSpi.class.getName()});

            adapters.put(sessionFactoryId, adapterClass.newInstance());
        }
        catch(SQLException e){
            throw new InitException("err.jdbcadapterfactory.failed_to_obtain_datasource_metadata", new String[]{dataSourceName}, e);
        }
        catch(InstantiationException e){
            throw new InitException("err.jdbcadapterfactory.failed_to_instantiate_jdbcadapter", new String[]{dataSourceName, adapterClass.getName()}, e);
        }
        catch(IllegalAccessException e){
            throw new InitException("err.jdbcadapterfactory.failed_to_instantiate_jdbcadapter", new String[]{dataSourceName, adapterClass.getName()}, e);
        }
    }

    public JdbcAdapter obtainJdbcAdapter(String sessionFactoryId){
        JdbcAdapter adapter = (JdbcAdapter)adapters.get(sessionFactoryId);
        return adapter != null ? adapter : defaultAdapter;
    }

    private DataSource obtainDataSource(String dataSourceName) throws InitException{
        InitialContext ctx = null;
        DataSource dataSource = null;
        try {
            System.setProperty("java.naming.factory.url.pkgs", ""); //=org.apache.naming
            ctx = new InitialContext();
            dataSource = (DataSource) ctx.lookup(dataSourceName);
            if (dataSource == null)
                throw new InitException("err.jdbcadapterfactory.null_datasource_obtained");
        }
        catch (NamingException e) {
            throw new InitException("err.jdbcadapterfactory.failed_to_obtain_datasource", new String[]{dataSourceName}, e);
        }
        finally {
            if (ctx != null) {
                try {
                    ctx.close();
                }
                catch (NamingException e) {
                    //log.info( "Unable to release initial context", ne );
                }
            }
        }
        return dataSource;
    }

    /** Extend this method to support new jdbc drivers
     *
     * @param driverName driver name
     * @param driverVersion driver version
     * @return JdbcAdapter implementation class
     * */
    private Class resolveAdapterClass(String driverName, String driverVersion){
        if(driverName.equals("Oracle JDBC driver")){
            return OracleJdbcAdapter.class;
        }
        return VoidJdbcAdapter.class;
    }
}
