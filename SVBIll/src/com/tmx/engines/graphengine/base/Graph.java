package com.tmx.engines.graphengine.base;

import com.tmx.util.InitException;
import com.tmx.engines.graphengine.beans.RootDocument;

/**
 */
public interface Graph {
    /** initialize graph by config. Package-private for calling only from GraphService */
    void init(GraphService.GraphConfig config, RootDocument rootDoc) throws GraphException;
    /** Validate graph. Use this method before call serialize()
     * to get valid xml. */
    public void validate() throws GraphException;
    /** Serialise graph XML to string */
    public String serialize() throws GraphException;
    /** Get root element of graph */
    public RootDocument getRootDocument();
}
