package com.tmx.beng.base.processors;

import com.tmx.beng.base.*;
import com.tmx.beng.base.cache.DBCache;

/**
 Close transaction task 
 */
public class FinishTransactionProcessor extends TaskProcessor {

    public Task process0(Task task, DBCache dbCache, BillingMessageWritable msg){
        logger.debug("Finishing transaction...");
        task.getTransaction().finish();
        return task;
    }
}
