package com.tmx.beng.httpsgate;

import com.tmx.util.StructurizedException;

import java.util.Locale;

public class BadResponseException extends StructurizedException {

    public BadResponseException(String errKey){
        super(errKey);
    }

    public BadResponseException(String errKey, Locale locale){
        super(errKey, locale);
    }

    public BadResponseException(String errKey, String[] params){
        super(errKey, params);
    }

    public BadResponseException(String errKey, String[] params, Throwable cause){
        super(errKey, params, cause);
    }

    public BadResponseException(String errKey, String[] params, Locale locale){
        super(errKey, params, locale);
    }

    public BadResponseException(String errKey, Throwable cause){
        super(errKey, cause);
    }

}
