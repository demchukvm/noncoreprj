package com.tmx.web.controls;

import com.tmx.engines.reportengine.util.TimePeriod;
import java.util.*;
import java.math.BigDecimal;

/**
 Control to handle time periods on the web.
*/
public class TimePeriodSelector extends BasicControl {
    /** Map of [key: TimePeriod.getType().getName()][value: TimePeriod].
     * Used for instant access. */
    private Map timePeriodsMap = null;
    /** Ordered list of time periods used for publishing */
    private List orderedTimePeriods = null;
    /** Type name of current period. Used for data population */
    private String timePeriod = null;
    /** fromDate used for data population */
    private String fromDate = null;
    /** toDate used for data population */
    private String toDate = null;
    /** Period suffix to render in HTML. Value should be the same as field name */
    public static String TIME_PERIOD_SUFFIX = "timePeriod";
    /** From date suffix to render in HTML. Value should be the same as field name */
    public static String FROM_DATE_SUFFIX = "fromDate";
    /** To date suffix to render in HTML. Value should be the same as field name */
    public static String TO_DATE_SUFFIX = "toDate";

    /** Control initialization */
    public void initialize(){
        buildTimePeriods(getLocale(), fromDate, toDate);
        timePeriod = TimePeriod.Type.getDefault().getName();
        fromDate = getCurrentTimePeriod().getTimeFromString();
        toDate = getCurrentTimePeriod().getTimeToString();
    }


    /** Converts populated fields <code>fromDate</code>, <code>toDate</code>, <code>timePeriod</code> into internal structure. */
    public void onBeforeAction(WebEvent event) {
        //if custom time period has been selected then store its from and to dates
        if(TimePeriod.Type.CUSTOM.getName().equals(timePeriod)){
            TimePeriod customTimePeriod = (TimePeriod)timePeriodsMap.get(TimePeriod.Type.CUSTOM.getName());
            customTimePeriod.setTimeFromString(fromDate);
            customTimePeriod.setTimeToString(toDate);
        }
    }

    //------------------Methods to interact with rendering tags
    public List getOrderedTimePeriods() {
        return orderedTimePeriods;
    }

    /** Returns map of TimePeriods */


    //------------------Methods to interact with bean auto-population

    public void setTimePeriod(String timePeriod) {
        this.timePeriod = timePeriod;
    }

    public void setFromDate(String fromDate) {
        this.fromDate = fromDate;
    }

    public void setToDate(String toDate) {
        this.toDate = toDate;
    }

    //-----------------Methods to be used from Actions

    /** Get current time period or default if current is null. */
    public TimePeriod getCurrentTimePeriod(){
        return (TimePeriod)timePeriodsMap.get((timePeriod != null) ? timePeriod : TimePeriod.Type.getDefault().getName());
    }

    /** Set custom time period as current */
    public void selectCustomTimePeriod(Date from, Date to){
        selectTimePeriod(TimePeriod.Type.CUSTOM);
        TimePeriod customTimePeriod = getCurrentTimePeriod();
        customTimePeriod.setTimeFrom(from);
        customTimePeriod.setTimeTo(to);
        //updates string representations of current form and to dates
        fromDate = customTimePeriod.getTimeFromString();
        toDate = customTimePeriod.getTimeToString();
    }

    /**  Set given time period as Custom */
    public void selectTimePeriod(TimePeriod.Type selectedPeriodType){
        this.timePeriod = selectedPeriodType.getName();
    }

    //------------------Private methods

    private void buildTimePeriods(Locale locale, TimePeriod custom){

        timePeriodsMap = new HashMap();
        orderedTimePeriods = new ArrayList();

        GregorianCalendar cal = new GregorianCalendar(locale);
        cal.setTimeInMillis(System.currentTimeMillis());

        int year = cal.get( Calendar.YEAR );
        int month = cal.get( Calendar.MONTH ) + 1;
        int day = cal.get( Calendar.DAY_OF_MONTH );
        int hour24 = cal.get( Calendar.HOUR_OF_DAY );
        int hour12 = cal.get( Calendar.HOUR );
        int amIs0OrPmIs1 = cal.get( Calendar.AM_PM );
        int minute = cal.get( Calendar.MINUTE );
        int second = cal.get( Calendar.SECOND );
        int week = cal.get( Calendar.WEEK_OF_MONTH );
        int dayOfWeek = cal.get( Calendar.DAY_OF_WEEK );
        if (dayOfWeek ==1) {
            dayOfWeek =6;
        } else {
            dayOfWeek =dayOfWeek-2;
        }

        GregorianCalendar from = new GregorianCalendar(locale);
        GregorianCalendar to = new GregorianCalendar(locale);
        TimePeriod tp = null;

        //TODAY
        from.set(year, month-1, day, 0, 0);
        to.set(year, month-1, day, hour24, minute);
        tp = new TimePeriod(TimePeriod.Type.TODAY, from.getTime(), to.getTime(), getLocalizedLabel("ctrl.timePeriodSelector.label.today", locale));
        timePeriodsMap.put(tp.getType().getName(), tp);
        orderedTimePeriods.add(tp);

        //YESTERDAY
        from.set(year, month-1, day-1, 0, 0);
        to.set(year, month-1, day-1, 23, 59);
        tp = new TimePeriod(TimePeriod.Type.YESTERDAY, from.getTime(), to.getTime(), getLocalizedLabel("ctrl.timePeriodSelector.label.yesterday", locale));
        timePeriodsMap.put(tp.getType().getName(), tp);
        orderedTimePeriods.add(tp);

        //THIS WEEK
        from.set(year, month-1, day-dayOfWeek, 0, 0);
        to.set(year, month-1, day, hour24, minute);
        tp = new TimePeriod(TimePeriod.Type.THIS_WEEK, from.getTime(), to.getTime(), getLocalizedLabel("ctrl.timePeriodSelector.label.this_week", locale));
        timePeriodsMap.put(tp.getType().getName(), tp);
        orderedTimePeriods.add(tp);

        //PREV WEEK
        from.set(year, month-1, day-dayOfWeek-7, 0, 0);
        to.set(year, month-1, day-dayOfWeek-1, 23, 59);
        tp = new TimePeriod(TimePeriod.Type.PREV_WEEK, from.getTime(), to.getTime(), getLocalizedLabel("ctrl.timePeriodSelector.label.prev_week", locale));
        timePeriodsMap.put(tp.getType().getName(), tp);
        orderedTimePeriods.add(tp);

        //THIS MONTH
        from.set(year, month-1, 1, 0, 0);
        to.set(year, month-1, day, hour24, minute);
        tp = new TimePeriod(TimePeriod.Type.THIS_MONTH, from.getTime(), to.getTime(), getLocalizedLabel("ctrl.timePeriodSelector.label.this_month", locale));
        timePeriodsMap.put(tp.getType().getName(), tp);
        orderedTimePeriods.add(tp);

        //PREV MOUTH
        from.set(year, month-2, 1, 0, 0);
        to.set(year, month-2, 1, 0, 0);
        int maxDayInMmonth = to.getActualMaximum(Calendar.DAY_OF_MONTH);
        to.set(year, month-2, maxDayInMmonth,  23, 59);
        tp = new TimePeriod(TimePeriod.Type.PREV_MONTH, from.getTime(), to.getTime(), getLocalizedLabel("ctrl.timePeriodSelector.label.prev_month", locale));
        timePeriodsMap.put(tp.getType().getName(), tp);
        orderedTimePeriods.add(tp);

        //THIS QUARTAL
        BigDecimal d = new BigDecimal(month);
        int quartal = d.divide(new BigDecimal(3d), BigDecimal.ROUND_UP).intValue();
        from.set(year, (quartal*3)-3, 1, 0, 0);
        d = new BigDecimal(month);
        quartal = d.divide(new BigDecimal(3d), BigDecimal.ROUND_UP).intValue();
        to.set(year, month-1, day, hour24, minute);
        tp = new TimePeriod(TimePeriod.Type.THIS_QUARTAL, from.getTime(), to.getTime(), getLocalizedLabel("ctrl.timePeriodSelector.label.this_quartal", locale));
        timePeriodsMap.put(tp.getType().getName(), tp);
        orderedTimePeriods.add(tp);

        //PREV QUARTAL
        from.set(year, (quartal*3)-6, 1, 0, 0);
        to.set(year, (quartal*3)-4, 1, 0, 0);
        maxDayInMmonth = to.getActualMaximum(Calendar.DAY_OF_MONTH);
        to.set(year, (quartal*3)-4, maxDayInMmonth, 23, 59);
        tp = new TimePeriod(TimePeriod.Type.PREV_QUARTAL, from.getTime(), to.getTime(), getLocalizedLabel("ctrl.timePeriodSelector.label.prev_quartal", locale));
        timePeriodsMap.put(tp.getType().getName(), tp);
        orderedTimePeriods.add(tp);

        //THIS YEAR
        from.set(year, 0, 1, 0, 0);
        to.set(year, month-1, day, hour24, minute);
        tp = new TimePeriod(TimePeriod.Type.THIS_YEAR, from.getTime(), to.getTime(), getLocalizedLabel("ctrl.timePeriodSelector.label.this_year", locale));
        timePeriodsMap.put(tp.getType().getName(), tp);
        orderedTimePeriods.add(tp);

        //PREV YEAR
        from.set(year-1, 0, 1, 0, 0);
        to.set(year-1, 11, 1, 0, 0);
        maxDayInMmonth = to.getActualMaximum(Calendar.DAY_OF_MONTH);
        to.set(year-1, 11, maxDayInMmonth, 23, 59);
        tp = new TimePeriod(TimePeriod.Type.PREV_YEAR, from.getTime(), to.getTime(), getLocalizedLabel("ctrl.timePeriodSelector.label.prev_year", locale));
        timePeriodsMap.put(tp.getType().getName(), tp);
        orderedTimePeriods.add(tp);

        //CUSTOM
        Date date_ = null;
        if(custom == null){
            custom = new TimePeriod(TimePeriod.Type.CUSTOM, new Date(), new Date(), getLocalizedLabel("ctrl.timePeriodSelector.label.custom", locale));
        }
        else{
            custom.setLabel(getLocalizedLabel("ctrl.timePeriodSelector.label.custom", locale));
        }
        timePeriodsMap.put(custom.getType().getName(), custom);
        orderedTimePeriods.add(custom);
    }

    private void buildTimePeriods(Locale locale, String from, String to){
        TimePeriod timePeriod = new TimePeriod(TimePeriod.Type.CUSTOM, null, null, getLocalizedLabel("ctrl.timePeriodSelector.label.custom", locale));
        timePeriod.setTimeFromString(from);
        timePeriod.setTimeToString(to);
        buildTimePeriods(locale, timePeriod);
    }

}
