package com.tmx.engines.reportengine.aggrtable;

import com.tmx.engines.reportengine.aggrtable.beans.AggregateFunctions;
import com.tmx.engines.reportengine.aggrtable.validation.DataValidationError;
import com.tmx.engines.reportengine.base.ReportException;
import com.tmx.util.i18n.MessageResources;

import java.util.*;
import java.math.BigDecimal;

/**
 *
 */
public class AggrFunction {
    /** Aggregation function metadata */
    private Metadata metadata = null;
    /** Map to store evaluated [String factIdx][List values]  */
    private Map values = null;
    private final String AGGR_FUNCTION_PREFIX = "aggregation_function.";

    //------------Public methods
    public Integer getId(){
        return metadata.getId();
    }

    public String getName(Locale locale){
        return MessageResources.getInstance().getOwnedLocalizedMessage(this.getClass(), AggregatedTable.RESOURCE_BUNDLE, locale, AGGR_FUNCTION_PREFIX + metadata.getType().toString(), null);
    }

    /** Return count of values of given aggr function.
     * All facts in this table have the same count of aggr funct values.
     * So calculate based on first one */
    public int getValuesCount(){
        List vals = (List)values.get("0");//0 is string index of first fact.
        return (vals != null) ? vals.size() : 0;
    }

    /** Return count of facts in this table */
    public int getFactsCount(){
        return values.keySet().size();
    }

    /** Get aggr functon value by Fact idx and value idx */
    public AggrFunctionValue getValue(int factIdx, int valsIdx){
        List vals = ((List)values.get(String.valueOf(factIdx)));
        return (vals != null) ? (AggrFunctionValue)vals.get(valsIdx) : null;
    }

    //-----------Constructor
    AggrFunction(Metadata metadata){
        this.metadata = metadata;
        this.values = new HashMap();
    }

    /** Evaluate function values  */
    void evaluate(HeaderSet headerSet, Data data) throws ReportException{
        List rowHeaders = headerSet.getRowHeaders(metadata.level.intValue());
        //traverse all facts
        for(int factIdx = 0; factIdx < data.getFactCount(); factIdx++){
            //travers all headers on corresponded level

            //This ranges both for Column and Row AggrFunction position. To work with Row
            //position we need to transpose them.
            int startColIdx = 0;
            int endColIdx = 0;
            int startRowIdx = 0;
            int endRowIdx = 0;
            if(Metadata.Position.COLUMN.equals(metadata.getPosition()))
                endRowIdx = data.getRowsCount() - 1;
            else if(Metadata.Position.ROW.equals(metadata.getPosition())){
                endRowIdx = data.getColsCount() - 1;
            }

            for(Iterator iter = rowHeaders.iterator(); iter.hasNext();){
                Header header = (Header)iter.next();
                AggrFunctionValue aggrValue = new AggrFunctionValue();
                aggrValue.setSpan(header.getSpan());

                startColIdx = endColIdx;
                endColIdx = endColIdx + header.getSpan();

                List collectedData = null;
                if(Metadata.Position.COLUMN.equals(metadata.getPosition()))
                    collectedData = collectData(data.getFactData(factIdx), startColIdx, endColIdx-1, startRowIdx, endRowIdx);
                else if(Metadata.Position.ROW.equals(metadata.getPosition())){
                    //transpose ranges
                    collectedData = collectData(data.getFactData(factIdx), startRowIdx, endRowIdx, startColIdx, endColIdx-1);
                }
                aggrValue.setValue(evaluate0(collectedData));
                addValue(factIdx, aggrValue);
            }
        }
    }

    static Set init(AggregateFunctions.Function[] functions, int columnLevelsCount, int rowLevelsCount) throws ReportException{
        Set aggrFunctions = new HashSet();
        if(functions == null)
            return aggrFunctions;

        Set afRules = new HashSet();
        for(int i=0; i<functions.length; i++){
            Metadata afRule = new Metadata();
            afRule.setId(new Integer(functions[i].getId()));
            afRule.setLevel(new Integer(functions[i].getLevel()));
            afRule.setOrderNum(new Integer(functions[i].getOrderNum()));
            afRule.setPosition(functions[i].getPosition());
            afRule.setType(functions[i].getType());
            afRules.add(afRule);
        }
        aggrFunctions = validate(afRules, columnLevelsCount, rowLevelsCount);
        return aggrFunctions;
    }

    /** Collects Objects from specified array range into List */
    private List collectData(Wrapped2DArray srcData, int startColIdx, int endColIdx, int startRowIdx, int endRowIdx){
        List resultList = new ArrayList();
        for(int rowIdx = startRowIdx; rowIdx <= endRowIdx; rowIdx++)
            for(int colIdx = startColIdx; colIdx <= endColIdx; colIdx++){
                if(srcData.get(rowIdx, colIdx) != null && ((DataElement)srcData.get(rowIdx, colIdx)).getValidationError() == null)
                    resultList.add(srcData.get(rowIdx, colIdx));//collect all data cell values except containing validation errors
            }

        return resultList;
    }

    private double evaluate0(List argValues) throws ReportException{
        double resultValue;
        if(Metadata.Type.SUM.equals(metadata.getType())){
            resultValue = 0;
            for(Iterator iter = argValues.iterator(); iter.hasNext();){
                Double argValue = convertToDouble(iter.next());
                resultValue = resultValue + ((argValue != null) ? argValue.doubleValue() : 0);
            }

        }
        else if(Metadata.Type.MAX.equals(metadata.getType())){
            resultValue = Double.MIN_VALUE;
            for(Iterator iter = argValues.iterator(); iter.hasNext();){
                Double argValue = convertToDouble(iter.next());
                if(argValue != null && argValue.doubleValue() > resultValue)
                    resultValue = argValue.doubleValue();
            }
            if(resultValue == Double.MIN_VALUE)
                resultValue = 0;
        }
        else if(Metadata.Type.MIN.equals(metadata.getType())){
            resultValue = Double.MAX_VALUE;
            for(Iterator iter = argValues.iterator(); iter.hasNext();){
                Double argValue = convertToDouble(iter.next());
                if(argValue != null && argValue.doubleValue() < resultValue)
                    resultValue = argValue.doubleValue();
            }
            if(resultValue == Double.MAX_VALUE)
                resultValue = 0;
        }
        else if(Metadata.Type.AVE.equals(metadata.getType())){
            resultValue = 0;
            int notNullValsCount = 0;
            for(Iterator iter = argValues.iterator(); iter.hasNext();){
                Double argValue = convertToDouble(iter.next());
                if(argValue != null){
                    notNullValsCount++;//calculate count of not null values
                    resultValue = resultValue + argValue.doubleValue();
                }
            }
            resultValue = (notNullValsCount > 0) ? resultValue / notNullValsCount : 0;
        }
        else{
            throw new ReportException("err.report_engine.aggr_function.uknown_function_type", new String[]{(metadata.getType() != null) ? metadata.getType().toString() : null});
        }
        return resultValue;
    }

    /** Convert srcValue into double */
    private Double convertToDouble(Object srcValue) throws ReportException{
        double resultValue = 0;
        if(srcValue == null)
            return null;
        Object originalValue = ((DataElement)srcValue).getOriginalValue();
        if(originalValue == null)
            return null;
        if(originalValue instanceof Long){
            resultValue = ((Long)originalValue).doubleValue();
        }
        else if(originalValue instanceof Integer){
            resultValue = ((Integer)originalValue).doubleValue();
        }
        else if(originalValue instanceof Float){
            resultValue = ((Float)originalValue).doubleValue();
        }
        else if(originalValue instanceof Double){
            resultValue = ((Double)originalValue).doubleValue();
        }
        else if(originalValue instanceof Short){
            resultValue = ((Short)originalValue).doubleValue();
        }
        else if(originalValue instanceof BigDecimal){
            resultValue = ((BigDecimal)originalValue).doubleValue();
        }
        else{
            throw new ReportException("err.report_engine.aggr_function.uknown_value_type", new String[]{(srcValue != null) ? srcValue.getClass().getName() : null});
        }
        return new Double(resultValue);
    }

    /** Add aggregated value to specified fact list */
    private void addValue(int factIdx, AggrFunctionValue aggrValue){
        String factKeyStr = String.valueOf(factIdx);
        List vals = (List)values.get(factKeyStr);
        if(vals == null){
            vals = new ArrayList();
            values.put(factKeyStr, vals);
        }
        vals.add(aggrValue);
    }

    private static Set validate(Set aggrFunctionsRules, int columnLevelsCount, int rowLevelsCount) throws ReportException{
        //treeset to check consistency of indeces
        TreeSet indexSet = new TreeSet(new IntIdxComparator());
        //treeset to check consistency of orders
        TreeSet columnOrdersSet = new TreeSet(new IntIdxComparator());
        TreeSet rowOrdersSet = new TreeSet(new IntIdxComparator());

        if(aggrFunctionsRules == null)
            throw new ReportException("err.report_engine.aggr_table.null_aggrfunction_rules");

        for(Iterator iter = aggrFunctionsRules.iterator(); iter.hasNext();){
            Metadata rule = (Metadata)iter.next();

            indexSet.add(rule.getId());
            if(Metadata.Position.COLUMN.equals(rule.getPosition())){
                columnOrdersSet.add(rule.getOrderNum());
            }
            else if(Metadata.Position.ROW.equals(rule.getPosition())){
                rowOrdersSet.add(rule.getOrderNum());
            }
            else
                throw new ReportException("err.report_engine.aggr_table.unknown_column_position", new String[]{rule.getPosition().toString()});

            //check function type
            Metadata.Type type = rule.getType();
            if(!(Metadata.Type.SUM.equals(type) ||
                 Metadata.Type.MAX.equals(type) ||
                 Metadata.Type.MIN.equals(type) ||
                 Metadata.Type.AVE.equals(type)))

              throw new ReportException("err.report_engine.aggr_table.unknown_func_type", new String[]{(rule.getType() != null) ? rule.getType().toString() : null});

            //check is requested level exists
            if(rule.getPosition().equals(Metadata.Position.COLUMN) &&
              (rule.getLevel().intValue() < 0 || rule.getLevel().intValue() > columnLevelsCount))
              throw new ReportException("err.report_engine.aggr_table.level_out_of_bounds", new String[]{(rule.getType() != null) ? rule.getType().toString() : null, (rule.getLevel() != null) ? rule.getLevel().toString() : null});

            if(rule.getPosition().equals(Metadata.Position.ROW) &&
              (rule.getLevel().intValue() < 0 || rule.getLevel().intValue() > rowLevelsCount))
              throw new ReportException("err.report_engine.aggr_table.level_out_of_bounds", new String[]{(rule.getType() != null) ? rule.getType().toString() : null, (rule.getLevel() != null) ? rule.getLevel().toString() : null});

        }
        AggregatedTable.checkIdxListConsistence(indexSet);
        AggregatedTable.checkIdxListConsistence(columnOrdersSet);
        AggregatedTable.checkIdxListConsistence(rowOrdersSet);

        return aggrFunctionsRules;
    }

    static class Metadata {
        private Integer id = null;
        private Integer orderNum = null;
        private Type type = null;
        private Position position = null;
        private Integer level = null;

        //----------Properties
        Integer getId() {
            return id;
        }

        void setId(Integer id) {
            this.id = id;
        }

        Integer getOrderNum() {
            return orderNum;
        }

        void setOrderNum(Integer orderNum) {
            this.orderNum = orderNum;
        }

        Type getType() {
            return type;
        }

        void setType(String type) {
            this.type = Type.evaluateType(type);
        }

        Position getPosition() {
            return position;
        }

        void setPosition(String position) {
            this.position = Position.evaluatePosition(position);
        }

        Integer getLevel() {
            return level;
        }

        void setLevel(Integer level) {
            this.level = level;
        }

        /** incapsulates types:
         * sum | min | max | ave */
        static class Type {
            /** only these types are supported */
            static final Type SUM = new Type("sum");
            static final Type MIN = new Type("min");
            static final Type AVE = new Type("ave");
            static final Type MAX = new Type("max");

            private String type = null;

            private Type(String type){
                this.type = type;
            }

            public String toString(){
                return type;
            }

            private static Type evaluateType(String typeName){
                if(SUM.toString().equals(typeName)){
                    return SUM;
                }
                else if(MIN.toString().equals(typeName)){
                    return Type.MIN;
                }
                else if(MAX.toString().equals(typeName)){
                    return Type.MAX;
                }
                else if(AVE.toString().equals(typeName)){
                    return Type.AVE;
                }
                else {
                    //crete new unsupported type
                    return new Type(typeName);
                }
            }
        }

        /** incapsulates types:
         * column | row */
        static class Position {
            /** only these types are supported */
            static final Position COLUMN = new Position("column");
            static final Position ROW = new Position("row");

            private String position = null;

            private Position(String position){
                this.position = position;
            }

            public String toString(){
                return position;
            }

            private static Position evaluatePosition(String positionName){
                if(COLUMN.toString().equals(positionName)){
                    return Position.COLUMN;
                }
                else if(ROW.toString().equals(positionName)){
                    return Position.ROW;
                }
                else {
                    //crete new unsupported type
                    return new Position(positionName);
                }
            }

        }
    }
}
