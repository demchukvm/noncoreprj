package com.tmx.as.entities.iso8583.transaction;

import com.tmx.as.base.BasicEntity;
import com.tmx.as.entities.iso8583.collation.Collation;

import java.io.Serializable;
import java.util.Date;

/**
 */
public class Iso8583Transaction extends BasicEntity {
    private Long transactionId;
    private Long messageTypeId;
    private Long processingCode;
    private Long systemsTraceNo;
    private Long retrievalRefNo;
    private String authIdResponse;
    private String responseCode;
    private String terminalId;
    private String cardAcqId;
    private String issuerTransportData;
    private String cliTransactionNum;
    private String serviceCode;
    private String contractNumber;
    private String msisdn;
    private Long amount;
    private String payId;
    private String billTransactionNum;
    private String receiptNum;
    private String billStatusCode;
    private String billStatusDescription;
    private Date clientTime;
    private Date preProcessingTime;
    private Date postProcessingTime;
    private String voucherNominal;
    private String voucherCode;
    private String voucherSecretCode;
    private String voucherBestBeforeTime;
    private String voucherRenewalTime;
    private Long batchNumber;
    private boolean reversed;
    private Collation collation;


    public Long getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(Long transactionId) {
        this.transactionId = transactionId;
    }

    public Long getMessageTypeId() {
        return messageTypeId;
    }

    public void setMessageTypeId(Long messageTypeId) {
        this.messageTypeId = messageTypeId;
    }

    public Long getProcessingCode() {
        return processingCode;
    }

    public void setProcessingCode(Long processingCode) {
        this.processingCode = processingCode;
    }

    public Long getSystemsTraceNo() {
        return systemsTraceNo;
    }

    public void setSystemsTraceNo(Long systemsTraceNo) {
        this.systemsTraceNo = systemsTraceNo;
    }

    public Long getRetrievalRefNo() {
        return retrievalRefNo;
    }

    public void setRetrievalRefNo(Long retrievalRefNo) {
        this.retrievalRefNo = retrievalRefNo;
    }

    public String getAuthIdResponse() {
        return authIdResponse;
    }

    public void setAuthIdResponse(String authIdResponse) {
        this.authIdResponse = authIdResponse;
    }

    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    public String getTerminalId() {
        return terminalId;
    }

    public void setTerminalId(String terminalId) {
        this.terminalId = terminalId;
    }

    public String getCardAcqId() {
        return cardAcqId;
    }

    public void setCardAcqId(String cardAcqId) {
        this.cardAcqId = cardAcqId;
    }

    public String getIssuerTransportData() {
        return issuerTransportData;
    }

    public void setIssuerTransportData(String issuerTransportData) {
        this.issuerTransportData = issuerTransportData;
    }

    public String getCliTransactionNum() {
        return cliTransactionNum;
    }

    public void setCliTransactionNum(String cliTransactionNum) {
        this.cliTransactionNum = cliTransactionNum;
    }

    public String getContractNumber() {
        return contractNumber;
    }

    public void setContractNumber(String contractNumber) {
        this.contractNumber = contractNumber;
    }

    public String getMsisdn() {
        return msisdn;
    }

    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }

    public String getPayId() {
        return payId;
    }

    public void setPayId(String payId) {
        this.payId = payId;
    }

    public String getBillTransactionNum() {
        return billTransactionNum;
    }

    public void setBillTransactionNum(String billTransactionNum) {
        this.billTransactionNum = billTransactionNum;
    }

    public String getReceiptNum() {
        return receiptNum;
    }

    public void setReceiptNum(String receiptNum) {
        this.receiptNum = receiptNum;
    }

    public Long getAmount() {
        return amount;
    }

    public void setAmount(Long amount) {
        this.amount = amount;
    }


    public String getBillStatusCode() {
        return billStatusCode;
    }

    public void setBillStatusCode(String billStatusCode) {
        this.billStatusCode = billStatusCode;
    }

    public String getBillStatusDescription() {
        return billStatusDescription;
    }

    public void setBillStatusDescription(String billStatusDescription) {
        this.billStatusDescription = billStatusDescription;
    }

    public Date getClientTime() {
        return clientTime;
    }

    public void setClientTime(Date clientTime) {
        this.clientTime = clientTime;
    }

    public Date getPreProcessingTime() {
        return preProcessingTime;
    }

    public void setPreProcessingTime(Date preProcessingTime) {
        this.preProcessingTime = preProcessingTime;
    }

    public Date getPostProcessingTime() {
        return postProcessingTime;
    }

    public void setPostProcessingTime(Date postProcessingTime) {
        this.postProcessingTime = postProcessingTime;
    }

    public String getVoucherCode() {
        return voucherCode;
    }

    public void setVoucherCode(String voucherCode) {
        this.voucherCode = voucherCode;
    }

    public String getVoucherSecretCode() {
        return voucherSecretCode;
    }

    public void setVoucherSecretCode(String voucherSecretCode) {
        this.voucherSecretCode = voucherSecretCode;
    }

    public String getVoucherBestBeforeTime() {
        return voucherBestBeforeTime;
    }

    public void setVoucherBestBeforeTime(String voucherBestBeforeTime) {
        this.voucherBestBeforeTime = voucherBestBeforeTime;
    }

    public String getVoucherRenewalTime() {
        return voucherRenewalTime;
    }

    public void setVoucherRenewalTime(String voucherRenewalTime) {
        this.voucherRenewalTime = voucherRenewalTime;
    }

    public String getServiceCode() {
        return serviceCode;
    }

    public void setServiceCode(String serviceCode) {
        this.serviceCode = serviceCode;
    }

    public String getVoucherNominal() {
        return voucherNominal;
    }

    public void setVoucherNominal(String voucherNominal) {
        this.voucherNominal = voucherNominal;
    }

    public Long getBatchNumber() {
        return batchNumber;
    }

    public void setBatchNumber(Long batchNumber) {
        this.batchNumber = batchNumber;
    }

    public boolean getReversed() {
        return reversed;
    }

    public void setReversed(boolean reversed) {
        this.reversed = reversed;
    }

    public Collation getCollation() {
        return collation;
    }

    public void setCollation(Collation collation) {
        this.collation = collation;
    }

    public Serializable getId() {
        return getTransactionId();
    }

    public int getValid() {
        return valid;
    }

    public void setValid(int valid) {
        this.valid = valid;
    }
}
