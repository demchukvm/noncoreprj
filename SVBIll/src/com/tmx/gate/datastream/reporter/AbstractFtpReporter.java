package com.tmx.gate.datastream.reporter;

import com.tmx.as.base.EntityManager;
import com.tmx.as.base.FilterWrapper;
import com.tmx.as.base.QueryParameterWrapper;
import com.tmx.as.entities.bill.operator.Operator;
import com.tmx.as.entities.mobipay.report.Report;
import com.tmx.gate.datastream.base.DataStreamException;
import com.tmx.gate.datastream.base.DataStreamProcessorFtp;
import com.tmx.util.FileUtil;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;

/** FINSTAT datastream loader */
public abstract class AbstractFtpReporter extends DataStreamProcessorFtp {
    private static final SimpleDateFormat startDateFormat = new SimpleDateFormat("yyyy-MM-dd");
    private static final SimpleDateFormat startupTimeFormat = new SimpleDateFormat("HH:mm");

    private static final String START_DATE = "startDate";
    private static final String STARTUP_TIME = "startupTime";
    private static final String DAYS_INTERVAL = "daysInterval";
    private static final String OPERATOR_CODE = "operatorCode";

    protected abstract void sendReport(Operator operator, Date date)throws DataStreamException;

    protected void executeDataStreamProcessing() throws DataStreamException {
        EntityManager entityManager = new EntityManager();

        try{

            Properties properties = getOwnProperties();
            Long daysInterval = new Long(properties.getProperty(DAYS_INTERVAL));

            // get operator
            FilterWrapper by_code = new FilterWrapper("by_code");
            by_code.setParameter("code", properties.getProperty(OPERATOR_CODE));
            Operator operator = (Operator)entityManager.RETRIEVE(Operator.class, new FilterWrapper[]{ by_code }, null);
            if(operator == null){
                throw new DataStreamException("operator not found:", new String[]{properties.getProperty(OPERATOR_CODE)});
            }

            // get start date
            Date startDate = startDateFormat.parse(properties.getProperty(START_DATE));

            List list = entityManager.EXECUTE_QUERY(
                    Report.class,
                    "get_sent_reports",
                    new QueryParameterWrapper[]{
                            new QueryParameterWrapper("number_of_days", daysInterval),
                            new QueryParameterWrapper("from_date", startDate),
                            new QueryParameterWrapper("operator", operator.getOperatorId())
                    });
            // set start date
            GregorianCalendar startDateCalendar = new GregorianCalendar();
            startDateCalendar.setTime(startDate);


            // fill hashset
            Set reportsSet = new HashSet();
            for(Iterator it = list.iterator(); it.hasNext();){
                reportsSet.add(new Long(((Date)it.next()).getTime()));
            }


            // check past
            GregorianCalendar date = new GregorianCalendar();
            GregorianCalendar today = (GregorianCalendar)date.clone();
            GregorianCalendar date_full = (GregorianCalendar)date.clone();
            date.set(GregorianCalendar.HOUR_OF_DAY, 0);
            date.set(GregorianCalendar.MINUTE, 0);
            date.set(GregorianCalendar.SECOND, 0);
            date.set(GregorianCalendar.MILLISECOND, 0);

            // set startup time
            GregorianCalendar startupTime = new GregorianCalendar();
            startupTime.setTime(startupTimeFormat.parse(properties.getProperty(STARTUP_TIME)));
            startupTime.set(date_full.get(GregorianCalendar.YEAR),
                    date_full.get(GregorianCalendar.MONTH),
                    date_full.get(GregorianCalendar.DAY_OF_MONTH));
            // check today
            int i = daysInterval.intValue();
            if(today.getTimeInMillis() < startupTime.getTimeInMillis()){
                date.add(GregorianCalendar.DAY_OF_MONTH, -1);
                date_full.add(GregorianCalendar.DAY_OF_MONTH, -1);
                i--;
            }
            for(; i > 0; i--){
                if(startDateCalendar.getTimeInMillis() < date.getTimeInMillis()){
                    if(!reportsSet.contains(new Long(date.getTimeInMillis()))){

                        // need to sent report for this day
                        sendReport(operator, date.getTime());

                    }
                    date.add(GregorianCalendar.DAY_OF_MONTH, -1);
                    date_full.add(GregorianCalendar.DAY_OF_MONTH, -1);
                }else{
                    break;
                }
            }


        }catch(DataStreamException e){
            throw e;
        }catch(Throwable e){
            throw new DataStreamException("err.error", e);
        }finally{
            closeConnection();
        }
    }

    protected void backupSentReport(File tmpReportFile, String backupDir) throws DataStreamException{
        final String backupFilePath = backupDir + "/" + tmpReportFile.getName();
        try{
            logger.info("Creating local backup copy of file " + tmpReportFile.getName());
            FileUtil.createNestedDirectory(backupDir);
            FileUtil.copyFile(tmpReportFile, new File(backupFilePath));
        }
        catch(IOException e){
            throw new DataStreamException("Failed to create local copy of uploaded report file ", new String[]{backupFilePath}, e);
        }
    }


}
