package com.tmx.beng.base.dbfunctions;

import com.tmx.as.entities.bill.transaction.TransactionType;
import com.tmx.beng.base.BillingMessage;
import com.tmx.beng.base.Reservation;
import com.tmx.beng.base.cache.InvokeFunctionException;

import java.util.Map;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * The same as SellerTariff but this tariff params are sensitive
 * to operators. To identify parameter dependency for given operator (service code) the complex dot-noteded
 * names are used.
 * <p/>
 * Name format:
 * [service_code].[param_name]
 * <p/>
 * Example:
 * UMCPoP.tx_cost
 */
public class SellerTariffOperatorSensitive extends BasicTariffFunction {
    /**
     * Transaction cost
     */
    public static final String TX_COST = "tx_cost";

    boolean isTariffApplicable(Map actualParams) {
        return BillingMessage.O_REFILL_PAYMENT.equals(task.getProcesedMessage().getOperationName());
    }

    /**
     * Terminal discount
     */
    public static final String DISCOUNT_PERCENT = "discount_percent";
    protected static String SEPARATOR = ".";

    public void applyTariff(Map actualParams, Reservation reservation) throws InvokeFunctionException {
        //get service code
        String serviceCode = task.getProcesedMessage().getAttributeString(BillingMessage.SERVICE_CODE);
        double txCost;
        double discontPercent;
        double amount;

        try {
            txCost = Double.parseDouble((String) actualParams.get(serviceCode + SEPARATOR + TX_COST));
            discontPercent = Double.parseDouble((String) actualParams.get(serviceCode + SEPARATOR + DISCOUNT_PERCENT));
            amount = Double.parseDouble(task.getProcesedMessage().getAttributeString(BillingMessage.AMOUNT));
        }
        catch (Throwable e) {
            throw new InvokeFunctionException("err.seller_tariff_operator_sensitive.param_error", e);
        }
        final double tariffedAmount = (amount + txCost) * discontPercent;
        reservation.reserve(null, tariffedAmount, null, TransactionType.CODE_CREDIT);
    }

    public ValidationError[] validate(Map actualParams) {
        List result = ParameterValidator.validateDiscountPercent(actualParams);
        return (ValidationError[]) result.toArray(new ValidationError[result.size()]);
    }

    public TariffParameter[] getDefaultParams() {
        return new TariffParameter[0];
    }

}
