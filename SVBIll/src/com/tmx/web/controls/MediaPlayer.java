package com.tmx.web.controls;

/**

 */
public class MediaPlayer extends BasicControl{
    private String contentUrl;

    public void initialize() {
    }

    //properties
    public String getContentUrl() {
        return contentUrl;
    }

    public void setContentUrl(String contentUrl) {
        this.contentUrl = contentUrl;
    }
    
}
