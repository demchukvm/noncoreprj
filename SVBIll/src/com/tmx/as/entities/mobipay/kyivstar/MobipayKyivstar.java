package com.tmx.as.entities.mobipay.kyivstar;

import com.tmx.as.base.BasicEntity;
import com.tmx.as.entities.mobipay.report.Report;

import java.io.Serializable;
import java.util.Date;

/**
 */
public class MobipayKyivstar extends BasicEntity {
    private Long mobipayKyivstarId;
    private String msisdn;
    private String payAccount;
    private Double amount;
    private Long payId;
    private Integer receiptNum;
    private Date commitDate;
    private Integer sourceType;
    private String bankPayId;
    private String branch;
    private String tradePoint;
    private Date bankOperDay;
    private Integer statusCode;
    private String statusMessage;
    private Report report;
    private String gateName;


    public Report getReport() {
        return report;
    }

    public void setReport(Report report) {
        this.report = report;
    }

    public Integer getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }

    public void setStatusCodeInt(int statusCode) {
        this.statusCode = new Integer(statusCode);
    }

    public String getStatusMessage() {
        return statusMessage;
    }

    public void setStatusMessage(String statusMessage) {
        if(statusMessage != null && statusMessage.length() > 255){
            statusMessage = statusMessage.substring(0, 255);
        }
        this.statusMessage = statusMessage;
    }

    public String getMsisdn() {
        return msisdn;
    }

    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }

    public String getPayAccount() {
        return payAccount;
    }

    public void setPayAccount(String payAccount) {
        this.payAccount = payAccount;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Long getPayId() {
        return payId;
    }

    public void setPayId(Long payId) {
        this.payId = payId;
    }

    public Integer getReceiptNum() {
        return receiptNum;
    }

    public void setReceiptNum(Integer receiptNum) {
        this.receiptNum = receiptNum;
    }

    public Date getCommitDate() {
        return commitDate;
    }

    public void setCommitDate(Date commitDate) {
        this.commitDate = commitDate;
    }

    public Integer getSourceType() {
        return sourceType;
    }

    public void setSourceType(Integer sourceType) {
        this.sourceType = sourceType;
    }

    public String getBankPayId() {
        return bankPayId;
    }

    public void setBankPayId(String bankPayId) {
        this.bankPayId = bankPayId;
    }

    public String getBranch() {
        return branch;
    }

    public void setBranch(String branch) {
        this.branch = branch;
    }

    public String getTradePoint() {
        return tradePoint;
    }

    public void setTradePoint(String tradePoint) {
        this.tradePoint = tradePoint;
    }

    public Date getBankOperDay() {
        return bankOperDay;
    }

    public void setBankOperDay(Date bankOperDay) {
        this.bankOperDay = bankOperDay;
    }

    public Long getMobipayKyivstarId() {
        return mobipayKyivstarId;
    }

    public void setMobipayKyivstarId(Long mobipayKyivstarId) {
        this.mobipayKyivstarId = mobipayKyivstarId;
    }

    public String getGateName() {
        return gateName;
    }

    public void setGateName(String gateName) {
        this.gateName = gateName;
    }

    public Serializable getId() {
        return getMobipayKyivstarId();
    }

    public int getValid() {
        return valid;
    }

    public void setValid(int valid) {
        this.valid = valid;
    }
}
