package com.tmx.as.resolver;

import com.tmx.util.StructurizedException;

import java.util.Locale;


public class ResolverException extends StructurizedException {

    public ResolverException(String errKey){
        super(errKey);
    }

    public ResolverException(String errKey, Locale locale){
        super(errKey, locale);
    }

    public ResolverException(String errKey, String[] params){
        super(errKey, params);
    }

    public ResolverException(String errKey, String[] params, Throwable cause){
        super(errKey, params, cause);
    }

    public ResolverException(String errKey, String[] params, Locale locale){
        super(errKey, params, locale);
    }

    public ResolverException(String errKey, Throwable cause){
        super(errKey, cause);
    }

}
