package com.tmx.web.taglib.html.filter_ctrl;

import com.tmx.web.controls.CheckBox;

import javax.servlet.jsp.JspException;

/**

 */
public class CheckBoxFilterTag extends AbstractFilterTag{
    private String parameterName = null;

    public String getParameterName() {
        return parameterName;
    }

    public void setParameterName(String parameterName) {
        this.parameterName = parameterName;
    }

    protected String renderControlHTML() throws JspException{
//        super.renderControlHTML();
        StringBuffer sb = new StringBuffer();
        String path = getProperty() + "." + getFilter().getParameter(parameterName).getPath() + "." + CheckBox.SELECTED_SUFFIX;
        String name = getSeparatorReplacedName(path);
        CheckBox cb = (CheckBox)getFilter().getParameter(parameterName).getControl();

        sb.append("<input type='checkbox' value='").append(cb.isSelected())
                .append("' class='").append(getStyleClass()).append("' ")
                .append((cb.isSelected()) ? "checked" : "").append(" onclick='")
                .append(name).append("_onclick(this)'/>");
        sb.append("<input type='hidden' value='").append(cb.isSelected()).append("' name='")
                .append(path).append("'/>");

        sb.append("<script language='JavaScript'>");
        sb.append("function ").append(name).append("_onclick(obj){")
                .append("var hidden = document.getElementById('").append(path).append("');")
                .append("if(obj.value=='true' || obj.value=='on'){")
                .append("hidden.value='false';")
                .append("obj.value='false';")
                .append("}else{")
                .append("hidden.value='true';")
                .append("obj.value='true';}")
                .append("}</script>");
        return sb.toString();
    }

}
