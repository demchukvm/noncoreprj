CREATE OR REPLACE TYPE NUMBER_ARRAY_TYPE AS TABLE OF NUMBER;
/
CREATE OR REPLACE TYPE FLOAT_ARRAY_TYPE AS TABLE OF FLOAT;
/
CREATE OR REPLACE TYPE VARCHAR_ARRAY_TYPE AS TABLE OF VARCHAR(50);
/
CREATE OR REPLACE FUNCTION Change_Balances(
   operator_code$         VARCHAR_ARRAY_TYPE,
   operator_balance_code$ VARCHAR_ARRAY_TYPE,
   operator_amount$       FLOAT_ARRAY_TYPE, 
   seller_code$           VARCHAR_ARRAY_TYPE,
   seller_balance_code$   VARCHAR_ARRAY_TYPE,
   seller_amount$         FLOAT_ARRAY_TYPE,
   terminal_sn$           VARCHAR_ARRAY_TYPE,
   terminal_balance_code$ VARCHAR_ARRAY_TYPE,
   terminal_amount$       FLOAT_ARRAY_TYPE
)RETURN NUMBER IS

   CURSOR operator_cur(operator_code$$ svbill.OP_OPERATOR.OP_Code%TYPE,
                       operator_balance_code$$ svbill.OB_OPERATOR_BALANCE.OB_CODE%TYPE) IS
      SELECT  
      NVL(ob.OB_AMOUNT, 0) AS OB_AMOUNT,
      ob.OB_OPERATORBALANCEID
      FROM
      OB_OPERATOR_BALANCE ob,
	  OP_OPERATOR op
   WHERE
     op.OP_OperatorId = ob.OP_OperatorId AND
     op.OP_Code = operator_code$$ AND
	 ob.OB_CODE = operator_balance_code$$ AND
	 op.OP_Valid = 1 AND
	 ob.OB_VALID = 1 AND
     ob.OB_BLOCKED <> 1     
   FOR UPDATE OF ob.OB_AMOUNT;

   CURSOR seller_cur(seller_code$$ svbill.SL_SELLER.SL_Code%TYPE,
                     seller_balance_code$$ svbill.SB_SELLER_BALANCE.SB_CODE%TYPE) IS
      SELECT  
      NVL(sb.SB_AMOUNT, 0) AS SB_AMOUNT,
      sb.SB_SELLERBALANCEID
      FROM
      SB_SELLER_BALANCE sb,
	  SL_SELLER sl
   WHERE
     sl.SL_SellerId = sb.SL_SellerId AND
     sl.SL_Code = seller_code$$ AND
	 sb.SB_Code = seller_balance_code$$ AND
	 sl.SL_Valid = 1 AND
	 sb.SB_VALID = 1 AND
     sb.SB_BLOCKED <> 1     
   FOR UPDATE OF sb.SB_AMOUNT;

   CURSOR terminal_cur(terminal_sn$$ svbill.TR_TERMINAL.TR_SERIALNUMBER%TYPE,
   		  	           terminal_balance_code$$ svbill.TB_TERMINAL_BALANCE.TB_CODE%TYPE) IS
      SELECT  
      NVL(tb.TB_AMOUNT, 0) AS TB_AMOUNT,
      tb.TB_TERMINALBALANCEID
      FROM
      TB_TERMINAL_BALANCE tb,
	  TR_TERMINAL tr
   WHERE
     tr.TR_TerminalId = tb.TR_TerminalId AND
     tr.TR_SerialNumber = terminal_sn$$ AND
	 tb.TB_CODE = terminal_balance_code$$ AND
	 tr.TR_VALID = 1 AND
	 tb.TB_VALID = 1 AND
     tb.TB_BLOCKED <> 1     
   FOR UPDATE OF tb.TB_AMOUNT;

   op_r$       operator_cur%ROWTYPE;
   sl_r$       seller_cur%ROWTYPE;
   tr_r$       terminal_cur%ROWTYPE;
   
   exc_no_balance$   EXCEPTION;
   PRAGMA EXCEPTION_INIT(exc_no_balance$, -20000);   
BEGIN

  -- change operator balances 
  FOR array_id$ IN 1 .. operator_balance_code$.COUNT LOOP
     OPEN operator_cur(operator_code$(array_id$), operator_balance_code$(array_id$));
     FETCH operator_cur INTO op_r$;
     IF operator_cur%NOTFOUND THEN
        RAISE_APPLICATION_ERROR(-20000, 'Operator balance not found or blocked (OP_CODE=' || operator_code$(array_id$) || ', OB_CODE=' || operator_balance_code$(array_id$) ||').');  
     END IF;
     op_r$.OB_AMOUNT := op_r$.OB_AMOUNT - operator_amount$(array_id$);
     UPDATE OB_OPERATOR_BALANCE 
        SET OB_AMOUNT = op_r$.OB_AMOUNT 
     WHERE CURRENT OF operator_cur;
     CLOSE operator_cur;
  END LOOP;
   
  -- change sellers balances 
  FOR array_id$ IN 1 .. seller_balance_code$.COUNT LOOP
     OPEN seller_cur(seller_code$(array_id$), seller_balance_code$(array_id$));
     FETCH seller_cur INTO sl_r$;
     IF seller_cur%NOTFOUND THEN
        RAISE_APPLICATION_ERROR(-20000, 'Seller balance not found or blocked (SL_CODE=' || seller_code$(array_id$) || ', SB_CODE=' || seller_balance_code$(array_id$) ||').');  
     END IF;
     sl_r$.SB_AMOUNT := sl_r$.SB_AMOUNT - seller_amount$(array_id$);
     UPDATE SB_SELLER_BALANCE 
        SET SB_AMOUNT = sl_r$.SB_AMOUNT 
     WHERE CURRENT OF seller_cur;
     CLOSE seller_cur;
  END LOOP;

  -- change terminal balances 
  FOR array_id$ IN 1 .. terminal_balance_code$.COUNT LOOP
     OPEN terminal_cur(terminal_sn$(array_id$), terminal_balance_code$(array_id$));
     FETCH terminal_cur INTO tr_r$;
     IF terminal_cur%NOTFOUND THEN
        RAISE_APPLICATION_ERROR(-20000, 'Terminal balance not found or blocked (TR_SERIALNUMBER ' || terminal_sn$(array_id$) || ', TB_CODE = ' || terminal_balance_code$(array_id$) ||').');  
     END IF;
     tr_r$.TB_AMOUNT := tr_r$.TB_AMOUNT - terminal_amount$(array_id$);
     UPDATE TB_TERMINAL_BALANCE 
        SET TB_AMOUNT = tr_r$.TB_AMOUNT 
     WHERE CURRENT OF terminal_cur;
     CLOSE terminal_cur;
  END LOOP;
  
  COMMIT; --commit and unlock data

  RETURN 0;  
EXCEPTION
     WHEN OTHERS THEN
       ROLLBACK; --rollback and unlock data 
     IF operator_cur%isopen THEN 
       CLOSE operator_cur;
    END IF;
     IF seller_cur%isopen THEN 
       CLOSE seller_cur;
    END IF;
     IF terminal_cur%isopen THEN 
       CLOSE terminal_cur;
    END IF;  
       RAISE_APPLICATION_ERROR(-20000, SQLERRM);     
END;
/
