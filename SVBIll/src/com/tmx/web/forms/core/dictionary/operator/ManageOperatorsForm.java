package com.tmx.web.forms.core.dictionary.operator;

import com.tmx.web.controls.Button;
import com.tmx.web.controls.ScrollBox;
import com.tmx.web.controls.EditBox;
import com.tmx.web.controls.CriterionFactory;
import com.tmx.web.controls.table.ControlManagerTable;
import com.tmx.web.base.BasicActionForm;
import com.tmx.web.forms.core.base.AbstractFormFactory;
import com.tmx.as.entities.bill.operator.Operator;
import com.tmx.as.blogic.OperatorManager;
import com.tmx.as.exceptions.DatabaseException;
import com.tmx.as.base.criterion.CriterionWrapper;
import com.tmx.as.base.criterion.Restrictions;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: Demchuk
 * Date: 18.01.2008
 * Time: 15:02:30
 */
public class ManageOperatorsForm extends BasicActionForm {
    private static final String INFO_ACTION = "/core/dictionary/operator/infoOperator";
    private static final String MANAGE_ACTION = "/core/dictionary/operator/manageOperators";

    private Button onCreateButton;
    private Button onEditButton;
    private Button onDeleteButton;

    private ScrollBox operatorsScrollBox;

    private ControlManagerTable operatorsTable;

    protected void init() {
        initButtons();
        initTable();
        initFilters();
    }

    protected void initButtons() {
        onCreateButton = new Button();
        onCreateButton.setLabelKey("ctrl.button.label.create");
        onCreateButton.setAction(INFO_ACTION);
        onCreateButton.setCommand("preloadForCreate");

        onEditButton = new Button();
        onEditButton.setLabelKey("ctrl.button.label.edit");
        onEditButton.setAction(INFO_ACTION);
        onEditButton.setCommand("preloadForUpdate");

        onDeleteButton = new Button();
        onDeleteButton.setLabelKey("ctrl.button.label.delete");
        onDeleteButton.setAction(INFO_ACTION);
        onDeleteButton.setCommand("preloadForDelete");


    }

    private void initFilters() {
            getOperatorsTable().addParameterToFilter("by_id", "id", initOperatorsScrollBoxFilter());
    }

    private ScrollBox initOperatorsScrollBoxFilter(){
            operatorsScrollBox = new ScrollBox(){
                protected String getKey(Object dataListElement) {
                    return ((Operator)dataListElement).getId().toString();
                }
                protected String getValue(Object dataListElement) {
                    return ((Operator)dataListElement).getName();
                }
            };
            operatorsScrollBox.setNullable(false);
            return operatorsScrollBox ;
    }

    private void initTable() {
        operatorsTable = new ControlManagerTable("query(query_type=retrieve_all," +
            "entity_class=com.tmx.as.entities.bill.operator.Operator," +
            "page_size=15" +
            ")");
        operatorsTable.setRefreshActionName(MANAGE_ACTION);
        operatorsTable.addDependedControl(onEditButton).addDependedControl(onDeleteButton);
    }

    public void bindData() throws DatabaseException {
        getOperatorsScrollBox().setNullable(true);

        List operators = new OperatorManager().getAllOperators();
        Collections.sort(operators, new Comparator<Operator>()
        {
          public int compare(Operator op1, Operator op2) {
            return op1.getName().compareTo(op2.getName());
          }
        });

        //getOperatorsScrollBox().bind(new OperatorManager().getAllOperators());//TODO: move to Action
        getOperatorsScrollBox().bind(operators);//TODO: move to Action
        getOperatorsTable().addParameterToFilter("by_operator_id", "operator_id", getOperatorsScrollBox());
    }

    public Button getOnCreateButton() {
        return onCreateButton;
    }

    public void setOnCreateButton(Button onCreateButton) {
        this.onCreateButton = onCreateButton;
    }

    public Button getOnEditButton() {
        return onEditButton;
    }

    public void setOnEditButton(Button onEditButton) {
        this.onEditButton = onEditButton;
    }

    public Button getOnDeleteButton() {
        return onDeleteButton;
    }

    public void setOnDeleteButton(Button onDeleteButton) {
        this.onDeleteButton = onDeleteButton;
    }

    public ControlManagerTable getOperatorsTable() {
        return operatorsTable;
    }

    public void setOperatorsTable(ControlManagerTable operatorsTable) {
        this.operatorsTable = operatorsTable;
    }

    public ScrollBox getOperatorsScrollBox() {
        return operatorsScrollBox;
    }

    public void setOperatorsScrollBox(ScrollBox operatorsScrollBox) {
        this.operatorsScrollBox = operatorsScrollBox;
    }

}

