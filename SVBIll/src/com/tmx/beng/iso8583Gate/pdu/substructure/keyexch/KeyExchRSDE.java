package com.tmx.beng.iso8583Gate.pdu.substructure.keyexch;

import com.tmx.beng.iso8583Gate.pdu.substructure.BasicRuntimeSubDE;

import java.util.Map;
import java.util.HashMap;

/**

 */
public class KeyExchRSDE extends BasicRuntimeSubDE {
    private Map keys = new HashMap();

    public void addValue(String name, String value) {
        if(KeyExchSDEF.KMAC.equals(name) || KeyExchSDEF.KPP.equals(name))
            keys.put(name, value);
    }

    public String getValue(String name) {
        return (String)keys.get(name);
    }
}
