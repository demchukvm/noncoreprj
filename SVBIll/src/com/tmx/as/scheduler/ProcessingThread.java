package com.tmx.as.scheduler;

import org.apache.log4j.*;
import com.tmx.util.InitException;

public abstract class ProcessingThread extends Thread {
    /** Configuration */
    private Config config = null;
    /** Logger */
    protected Logger logger = Logger.getLogger("scheduler.procThread");


    protected abstract void executeSpecificTask();
      
    /** RM: this is strange method is used only in the <code> SchedulerContext.startProcThread() </code> */
    public ProcessingThread getNewInstance() throws InitException{
        return config.instantiate();
    }

    /**
     * Return the ProcessingThread's schedule
     */
    public Schedule getSchedule() {
        Schedule schedule = new Schedule(config.getName());
        schedule.safeLoadFromDB();
        return schedule;
    }

    public void run() {
        setName(config.getName());
        logger.debug("Started processing thread '" + getName() + "'");
        executeSpecificTask();
        logger.debug("Successful finished processing thread '" + getName() + "'");
    }

    public static Config createConfig(){
        return new Config();
    }

    protected Config getConfig() {
        return config;
    }

    private void setConfig(Config config) {
        this.config = config;
    }

    public static class Config {
        private String name;
        private Class implClass;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }


        public Class getImplClass() {
            return implClass;
        }

        public void setImplClass(Class implClass) {
            this.implClass = implClass;
        }

        public ProcessingThread instantiate() throws InitException{
            try{
                ProcessingThread pThread = (ProcessingThread)getImplClass().newInstance();
                pThread.setConfig(this);
                return pThread;
            }
            catch(Exception e){
                throw new InitException("err.scheduler.failed_to_instantiate_processing_thread", new String[]{getName(), (getImplClass() == null) ? null : getImplClass().getName()}, e);
            }
        }

    }

}