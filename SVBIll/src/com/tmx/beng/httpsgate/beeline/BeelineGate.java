package com.tmx.beng.httpsgate.beeline;

import com.tmx.beng.base.BasicGate;
import com.tmx.beng.base.BillingMessage;
import com.tmx.beng.base.BillingMessageImpl;
import com.tmx.beng.base.StatusDictionary;
import com.tmx.beng.access.Access;
import com.tmx.beng.httpsgate.HttpsGateException;
import com.tmx.beng.httpsgate.HttpsRequestException;
import com.tmx.beng.httpsgate.EasySSLProtocolSocketFactory;
import com.tmx.beng.httpsgate.BadResponseException;
import com.tmx.util.queue.ActionQueue;
import com.tmx.util.InitException;
import com.tmx.gate.datastream.base.GateContext;
import com.tmx.as.base.EntityManager;
import com.tmx.as.base.FilterWrapper;
import com.tmx.as.entities.mobipay.beeline.Beeline;
import com.tmx.as.entities.mobipay.region.Region;
import org.apache.log4j.Logger;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.NameValuePair;
import org.apache.commons.httpclient.HttpMethod;
import org.apache.commons.httpclient.protocol.Protocol;
import org.apache.commons.httpclient.methods.GetMethod;

import java.io.IOException;

/**
 * Beeline outgoing https gate 
 */
public class BeelineGate extends BasicGate {
    // properties names constants
    private static final String HTTPS_GATE_NAME = "beeline_gate";

    private static Logger logger = Logger.getLogger("beeline_gate." + HTTPS_GATE_NAME);
    private static BeelineGate beelineGate = null;
    private Access access = null;

    // queue
    private ActionQueue messageQueue = null;

    // properties names constants
    private static final String PROP_URL = "url";
    private static final String PROP_USERNAME = "username";
    private static final String PROP_PASSWORD = "password";
    private static final String PROP_SOURCE_TYPE = "source_type";
    private static final String PROP_CURRENCY = "currency";
    private static final String PROP_BRANCH = "branch";
    private static final String PROP_INIT_HANDLER_COUNT = "handlers.initial";
    private static final String PROP_MAX_HANDLER_COUNT = "handlers.max";


    public static BeelineGate getInstance()throws HttpsGateException {
        if(beelineGate == null){
            logger.error("Attempt to obtain not inited billing engine");
            throw new HttpsGateException("err.https_gate.null_instance");
        }
        return beelineGate;
    }

    public static void init()throws InitException{
        logger.info("Start Beeline HTTPS Gate initialization");
        beelineGate = new BeelineGate();
        beelineGate.init0();
        int initHandlerCount = Integer.parseInt(beelineGate.properties.getProperty(PROP_INIT_HANDLER_COUNT, "1"));
        int maxHandlerCount = Integer.parseInt(beelineGate.properties.getProperty(PROP_MAX_HANDLER_COUNT, "5"));
        beelineGate.messageQueue = new ActionQueue(initHandlerCount, maxHandlerCount);
        beelineGate.messageQueue = new ActionQueue(1,5);
        logger.info("Beeline HTTPS Gate initialized");
    }

    public static void shutdown(final long timeout) throws InitException {
        logger.info("Start Beeline Gate Shutdown");
        if(beelineGate.messageQueue.actionCount() != 0){
            Runtime.getRuntime().addShutdownHook(new Thread(){
                public void run(){
                    try{
                        long lt = System.currentTimeMillis();
                        while(beelineGate.messageQueue.actionCount() != 0 &&
                                ((System.currentTimeMillis() - lt) < timeout)){
                            sleep(1000);
                        }
                    }catch(Throwable e){
                        e.printStackTrace();
                    }
                }
            });
        }
        logger.info("Beeline Gate shutdown");
    }

    private void init0()throws InitException {
        properties = GateContext.getInstance().getDatastreamProcessorConfig(HTTPS_GATE_NAME);
        access = obtainBillingEngineAccess();
    }

    public void enqueueBillMessage(BillingMessage billMessage){
        BeelineHttpsGateTask task = new BeelineHttpsGateTask(billMessage);
        messageQueue.enqueue(task);
    }

    public void runBillMessage(BillingMessage billingMessage) throws HttpsGateException{

        BillingMessageImpl respMessage = new BillingMessageImpl();
        respMessage.setAttributeString(BillingMessage.REQUEST_ID,
                billingMessage.getAttributeString(BillingMessage.REQUEST_ID));
        try{
            // refill payment
            if(BillingMessage.O_REFILL_PAYMENT.equals(billingMessage.getOperationName())){
                refillPayment(billingMessage, respMessage);
            }
            else if(BillingMessage.O_REFILL_PAYMENT_REDIRECT.equals(billingMessage.getOperationName())){
                refillPayment(billingMessage, respMessage);
            }
            // cancel refill
            else if(BillingMessage.O_CANCEL_REFILL_PAYMENT.equals(billingMessage.getOperationName())){
                cancelPayment(billingMessage, respMessage);
            }
            // unsupported operation
            else{
                respMessage.setStatusCode(StatusDictionary.SUBMIT_TRANSACTION_ERROR);
                throw new HttpsGateException("Unsupported command",
                           new String[]{billingMessage.getOperationName()});
            }
        }
        catch(Throwable e){
            if(respMessage.getAttributeString(BillingMessage.STATUS_CODE) == null){
                respMessage.setStatusCode(StatusDictionary.UNKNOWN_ERROR);
            }
            respMessage.setAttributeString(BillingMessage.STATUS_MESSAGE, e.getLocalizedMessage());
        }
        finally{
            // send response
            try{
                access.obtainConnection().processAsync(respMessage);
            }catch(Throwable e){
                logger.error(e.getLocalizedMessage(), e);
            }
        }

    }

    /**
     * Cancel(Annulment) pay operation
     * @param billingMessage
     * @param respMessage
     */
    private void cancelPayment(BillingMessage billingMessage, BillingMessageImpl respMessage){

        EntityManager entityManager = new EntityManager();

        try{
            respMessage.setOperationName(BillingMessage.O_CANCEL_REFILL_STATUS);

            // find transaction
            FilterWrapper byBillNumFW = new FilterWrapper("mobipay_by_billnum");
            byBillNumFW.setParameter("billnum",
                    billingMessage.getAttributeString(BillingMessage.BILL_TRANSACTION_NUM));

            Beeline beeline = (Beeline) entityManager.RETRIEVE(
                    Beeline.class,
                    new FilterWrapper[]{  byBillNumFW },
                    null
            );
            if(beeline == null){ // if error
                respMessage.setStatusCode(StatusDictionary.TRANSACTION_NOT_FOUND);
                throw new HttpsGateException("transaction not found",
                        new String[]{billingMessage.getAttributeString(BillingMessage.BILL_TRANSACTION_NUM)});
            }

            // cancel transaction
            BeelineResponse  payResponse = paymentAnnulment(
                    properties.getProperty(PROP_URL),
                    properties.getProperty(PROP_USERNAME),
                    properties.getProperty(PROP_PASSWORD),
                    beeline.getPayId().longValue(),
                    beeline.getMsisdn(),
                    beeline.getReceiptNum().longValue(),
                    beeline.getAmount().doubleValue()

            );

            if(payResponse.getStatusCode().longValue() != 80){ // if error
                respMessage.setStatusCode(StatusDictionary.BEGIN_TRANSACTION_ERROR);
                respMessage.setAttribute(
                        BillingMessage.STATUS_MESSAGE,
                        obtainOperatorI18nMessage(
                                billingMessage.getAttributeString(BillingMessage.SERVICE_CODE),
                                MSG_TYPE_ERROR,
                                payResponse.getStatusCode(),
                                null)
                );
                throw new HttpsGateException("Incorrect status code",
                        new String[]{String.valueOf(payResponse.getStatusCode())});
            }
            respMessage.setStatusCode(StatusDictionary.STATUS_OK);

            // change mobipay status
            beeline.setStatusCode(new Integer(StatusDictionary.TRANSACTION_CANCELED));
            entityManager.SAVE(beeline);

            // copy transactions to response message
            respMessage.setAttribute(BillingMessage.TERMINAL_TRANSACTION,
                    billingMessage.getAttribute(BillingMessage.TERMINAL_TRANSACTION));
            respMessage.setAttribute(BillingMessage.OPERATOR_TRANSACTION,
                    billingMessage.getAttribute(BillingMessage.OPERATOR_TRANSACTION));
            respMessage.setAttribute(BillingMessage.CLIENT_TRANSACTION,
                    billingMessage.getAttribute(BillingMessage.CLIENT_TRANSACTION));

        }catch(Throwable e){
            if(respMessage.getAttributeString(BillingMessage.STATUS_CODE) == null){
                respMessage.setStatusCode(StatusDictionary.UNKNOWN_ERROR);
            }
            if(respMessage.getAttribute(BillingMessage.STATUS_MESSAGE) == null){
                respMessage.setAttributeString(BillingMessage.STATUS_MESSAGE, e.getLocalizedMessage());
            }
        }
    }

    /**
     *  Refill payment
     * @param billingMessage
     * @param respMessage
     */
    private void refillPayment(BillingMessage billingMessage, BillingMessageImpl respMessage){
        EntityManager entityManager = new EntityManager();

        Beeline beeline = new Beeline();
        beeline.setStatusCodeInt(StatusDictionary.STATUS_OK);
        BeelineResponse payResponse;

        try{
            respMessage.setOperationName(BillingMessage.O_REFILL_PAYMENT_RESULT);
            beeline.setBankPayId(billingMessage.getAttributeString(BillingMessage.BILL_TRANSACTION_NUM));
            beeline.setMsisdn(billingMessage.getAttributeString(BillingMessage.ACCOUNT_NUMBER));
            beeline.setAmount(new Double(billingMessage.getAttributeString(BillingMessage.AMOUNT)));
            beeline.setCurrency(new Integer(properties.getProperty(PROP_CURRENCY)));
            beeline.setBranch(properties.getProperty(PROP_BRANCH));
            if(billingMessage.getAttributeString(BillingMessage.TRADE_POINT) == null){
                beeline.setTradePoint(
                        billingMessage.getAttributeString(BillingMessage.CLI_TRANSACTION_NUM)
                                .substring(0, 8));
            }else{
                beeline.setTradePoint(billingMessage.getAttributeString(BillingMessage.TRADE_POINT));
            }

            // GET BRANCH
            String regionNumber = billingMessage.getAttributeString(BillingMessage.REGION_NUMBER);
            if(regionNumber != null){
                FilterWrapper by_id = new FilterWrapper("by_id");
                by_id.setParameter("id", new Long(regionNumber));
                Region region = (Region)entityManager.RETRIEVE(Region.class, new FilterWrapper[]{ by_id }, null);
                if(region == null){
                    respMessage.setStatusCode(StatusDictionary.REGION_NOT_FOUND);
                    throw new HttpsGateException("uncknown region code",
                            new String[]{ regionNumber });
                }
                beeline.setBranch(region.getBeelineRegionCode());
            }
            int sourceType = Integer.parseInt(properties.getProperty(PROP_SOURCE_TYPE));

            // begin transaction
            payResponse = beginTransaction(
                    properties.getProperty(PROP_URL),
                    properties.getProperty(PROP_USERNAME),
                    properties.getProperty(PROP_PASSWORD),
                    beeline.getMsisdn(),
                    beeline.getAmount().doubleValue(),
                    beeline.getTradePoint(),
                    beeline.getBranch(),
                    sourceType);

            if(payResponse.getStatusCode().longValue() != 21){ // if error
                respMessage.setStatusCode(StatusDictionary.BEGIN_TRANSACTION_ERROR);
                respMessage.setAttribute(
                        BillingMessage.STATUS_MESSAGE,
                        obtainOperatorI18nMessage(
                                billingMessage.getAttributeString(BillingMessage.SERVICE_CODE),
                                MSG_TYPE_ERROR,
                                payResponse.getStatusCode(),
                                null)
                );
                throw new HttpsGateException("Incorrect status code",
                        new String[]{String.valueOf(payResponse.getStatusCode())});
            }
            beeline.setPayId(payResponse.getPayId());


            // submit transaction
            payResponse = submitPaymentTransaction(
                    properties.getProperty(PROP_URL),
                    properties.getProperty(PROP_USERNAME),
                    properties.getProperty(PROP_PASSWORD),
                    beeline.getPayId().longValue());
            if(payResponse.getStatusCode().longValue() != 22){ // if error
                cancelPaymentTransaction(
                        properties.getProperty(PROP_URL),
                        properties.getProperty(PROP_USERNAME),
                        properties.getProperty(PROP_PASSWORD),
                        beeline.getPayId().longValue());
                respMessage.setStatusCode(StatusDictionary.SUBMIT_TRANSACTION_ERROR);
                respMessage.setAttribute(
                        BillingMessage.STATUS_MESSAGE,
                        obtainOperatorI18nMessage(
                                billingMessage.getAttributeString(BillingMessage.SERVICE_CODE),
                                MSG_TYPE_ERROR,
                                payResponse.getStatusCode(),
                                null)
                );
                throw new HttpsGateException("Incorrect status code",
                        new String[]{String.valueOf(payResponse.getStatusCode())});
            }
            beeline.setCommitDate(payResponse.getTimeStamp());
            beeline.setReceiptNum(new Integer(payResponse.getReceipt()));
            respMessage.setAttribute(BillingMessage.RECEIPT_NUM, beeline.getReceiptNum());
            respMessage.setAttribute(BillingMessage.PAY_ID, beeline.getPayId());
            respMessage.setStatusCode(StatusDictionary.STATUS_OK);
            
            // makeRequest exception
        }catch(HttpsRequestException e){
            e.printStackTrace();
            logger.error(e.getLocalizedMessage(), e);

            // transaction is began
            if(beeline.getPayId() != null){
                try{

                    // try to get transaction info
                    payResponse = queryTransactionStatus(
                            properties.getProperty(PROP_URL),
                            properties.getProperty(PROP_USERNAME),
                            properties.getProperty(PROP_PASSWORD),
                            beeline.getPayId().longValue());
                    if(payResponse.getStatusCode().longValue() != 70){ // if error
                        logger.info("Can't get transaction status " + payResponse.getStatusCode());

                        // try to cancel transaction
                        payResponse = cancelPaymentTransaction(
                                properties.getProperty(PROP_URL),
                                properties.getProperty(PROP_USERNAME),
                                properties.getProperty(PROP_PASSWORD),
                                beeline.getPayId().longValue());
                        if(payResponse.getPayStatus().longValue() == 23 || payResponse.getPayStatus().longValue() == -82){ // ok
                            logger.info("Transaction cancelled");
                            respMessage.setStatusCode(StatusDictionary.SUBMIT_TRANSACTION_ERROR);
                        }else{
                            logger.info("Cancel transaction error " + payResponse.getPayStatus());
                            respMessage.setStatusCode(StatusDictionary.UNKNOWN_ERROR);
                        }
                    }else{
                        switch(payResponse.getPayStatus().intValue()){
                            case 111:
                            {   // transaction is done successfully
                                beeline.setReceiptNum(new Integer(payResponse.getReceipt()));
                                beeline.setCommitDate(payResponse.getTimeStamp());
                                respMessage.setAttribute(BillingMessage.RECEIPT_NUM, beeline.getReceiptNum());
                                respMessage.setAttribute(BillingMessage.PAY_ID, beeline.getPayId());
                                respMessage.setStatusCode(StatusDictionary.STATUS_OK);
                            }
                                break;
                            case 115:
                            {   // transaction is cancelled
                                beeline.setCommitDate(payResponse.getTimeStamp());
                                respMessage.setStatusCode(StatusDictionary.TRANSACTION_CANCELED);
                            }
                                break;
                            default:
                            {  // invalid transaction try to cancel
                                payResponse = cancelPaymentTransaction(
                                        properties.getProperty(PROP_URL),
                                        properties.getProperty(PROP_USERNAME),
                                        properties.getProperty(PROP_PASSWORD),
                                        beeline.getPayId().longValue());
                                if(payResponse.getPayStatus().longValue() == 23 || payResponse.getPayStatus().longValue() == -82){ // ok
                                    logger.info("Transaction cancelled");
                                    respMessage.setStatusCode(StatusDictionary.SUBMIT_TRANSACTION_ERROR);
                                }else{
                                    logger.info("Cancel transaction error " + payResponse.getPayStatus());
                                    respMessage.setStatusCode(StatusDictionary.UNKNOWN_ERROR);
                                }
                            }
                                break;

                        }
                    }

                }catch(Throwable te){
                    te.printStackTrace();
                    logger.error(te.getLocalizedMessage(), te);
                    if(respMessage.getAttributeString(BillingMessage.STATUS_CODE) == null){
                        respMessage.setStatusCode(StatusDictionary.UNKNOWN_ERROR);
                    }
                    respMessage.setAttributeString(BillingMessage.STATUS_MESSAGE, te.getLocalizedMessage());
                }
            }else{
                respMessage.setStatusCode(StatusDictionary.BEGIN_TRANSACTION_ERROR);
            }

            // other exceptions
        }catch(Throwable e){
            e.printStackTrace();
            logger.error(e.getLocalizedMessage(), e);
            if(respMessage.getAttributeString(BillingMessage.STATUS_CODE) == null){
                respMessage.setStatusCode(StatusDictionary.UNKNOWN_ERROR);
            }
            if(respMessage.getAttribute(BillingMessage.STATUS_MESSAGE) == null){
                respMessage.setAttributeString(BillingMessage.STATUS_MESSAGE, e.getLocalizedMessage());
            }
        }finally{
            // save beeline
            try{
                beeline.setStatusCode(new Integer(respMessage.getAttributeString(BillingMessage.STATUS_CODE)));
                if(respMessage.getAttribute(BillingMessage.STATUS_MESSAGE) != null){
                    beeline.setStatusMessage(respMessage.getAttributeString(BillingMessage.STATUS_MESSAGE));
                }
                entityManager.SAVE(beeline);
            }catch(Throwable e){
                logger.error(e.getLocalizedMessage(), e);
            }
        }
    }

    private BeelineResponse beginTransaction(String url,
                                              String userName,
                                              String password,
                                              String msisdn,
                                              double payAmount,
                                              String tradePoint,
                                              String branch,
                                              int sourceType)
            throws HttpsRequestException{
        GetMethod get = new GetMethod(url);

        get.setQueryString(new NameValuePair[]{
                new NameValuePair("ACT", "0"),
                new NameValuePair("USERNAME", userName),
                new NameValuePair("PASSWORD", password),
                new NameValuePair("MSISDN", msisdn),
                new NameValuePair("PAY_AMOUNT", Double.toString(payAmount)),
                new NameValuePair("BRANCH", branch),
                new NameValuePair("SOURCE_TYPE", Integer.toString(sourceType)),
                new NameValuePair("TRADE_POINT", tradePoint)
        });
        return makeRequest(get);
    }

    private BeelineResponse queryTransactionStatus(String url,
                                                    String userName,
                                                    String password,
                                                    long payId)
            throws HttpsRequestException{
        GetMethod get = new GetMethod(url);

        get.setQueryString(new NameValuePair[]{
                new NameValuePair("ACT", "3"),
                new NameValuePair("USERNAME", userName),
                new NameValuePair("PASSWORD", password),
                new NameValuePair("PAY_ID", Long.toString(payId))
        });

        return makeRequest(get);
    }


    private BeelineResponse submitPaymentTransaction(String url,
                                                      String userName,
                                                      String password,
                                                      long payId)
            throws HttpsRequestException{
        GetMethod get = new GetMethod(url);

        get.setQueryString(new NameValuePair[]{
                new NameValuePair("ACT", "1"),
                new NameValuePair("USERNAME", userName),
                new NameValuePair("PASSWORD", password),
                new NameValuePair("PAY_ID", Long.toString(payId))
        });

        return makeRequest(get);
    }

    private BeelineResponse cancelPaymentTransaction(String url,
                                                      String userName,
                                                      String password,
                                                      long payId)
            throws HttpsRequestException{
        GetMethod get = new GetMethod(url);

        get.setQueryString(new NameValuePair[]{
                new NameValuePair("ACT", "2"),
                new NameValuePair("USERNAME", userName),
                new NameValuePair("PASSWORD", password),
                new NameValuePair("PAY_ID", Long.toString(payId))
        });

        return makeRequest(get);
    }

    private BeelineResponse changePassword(String url,
                                            String userName,
                                            String password,
                                            String newPassword)
            throws HttpsRequestException{
        GetMethod get = new GetMethod(url);

        get.setQueryString(new NameValuePair[]{
                new NameValuePair("ACT", "5"),
                new NameValuePair("USERNAME", userName),
                new NameValuePair("PASSWORD", password),
                new NameValuePair("NEWPASSWORD", newPassword)
        });
        return makeRequest(get);
    }

    private BeelineResponse paymentAnnulment(String url,
                                              String userName,
                                              String password,
                                              long payId,
                                              String msisdn,
                                              long receiptNum,
                                              double payAmount)
            throws HttpsRequestException{
        GetMethod get = new GetMethod(url);

        get.setQueryString(new NameValuePair[]{
                new NameValuePair("ACT", "6"),
                new NameValuePair("USERNAME", userName),
                new NameValuePair("PASSWORD", password),
                new NameValuePair("MSISDN", msisdn),
                new NameValuePair("PAY_AMOUNT", Double.toString(payAmount)),
                new NameValuePair("PAY_ID", Long.toString(payId)),
                new NameValuePair("RECEIPT_NUM", Long.toString(receiptNum))
        });
        return makeRequest(get);
    }


    static int cnt = 0;

    private BeelineResponse makeRequest(HttpMethod method)throws HttpsRequestException {
        BeelineResponse response = null;
        try{
            HttpClient httpClient = new HttpClient();
            logger.info(method.getQueryString()
                    .replaceFirst("PASSWORD=([^&]+)", "PASSWORD=*")
                    .replaceFirst("NEWPASSWORD=([^&]+)", "NEWPASSWORD=*"));

            Protocol.registerProtocol("https", new Protocol("https", new EasySSLProtocolSocketFactory(), 443));
            httpClient.executeMethod(method);

            if(method.getStatusCode() != 200){
                throw new HttpsRequestException("Incorrect response code from server " + method.getStatusCode());
            }
            String strResp = method.getResponseBodyAsString();

            logger.info(strResp);

            response = BeelineResponse.parseResponse(strResp);
        }catch(BadResponseException e){
            logger.error(e);
            throw new HttpsRequestException("XML exception " + e, e);
        }catch(IOException e){
            logger.error(e);
            throw new HttpsRequestException("IO exception " + e, e);
        }finally{
            method.releaseConnection();
        }

        return response;
    }

}
