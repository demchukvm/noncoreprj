package com.tmx.beng.httpsgate.dacard.pdu;

import java.io.OutputStream;
import java.io.InputStream;
import java.io.IOException;

/**

 */
public class NumericTypePAD extends AbstractTypePAD<Number> implements TypePAD<Number>{

    public void serialize(OutputStream os, Number value) throws DacardPadException{
        final int numberSizeInBytes = resolveNumberSizeInBytes(value); 
        try{
            if(numberSizeInBytes == 1){
                os.write(value.byteValue());
            }
            else if (numberSizeInBytes == 2){
                //convert and write 2-bytes from big-endian to little-endian.
                final short val = value.shortValue();
                os.write(ByteUtil.convertShortIntoLittleEndian(val));
            }
            else if (numberSizeInBytes == 4){
                //convert and write 4-bytes from big-endian to little-endian.
                final int val = value.intValue();
                os.write(ByteUtil.convertIntIntoLittleEndian(val));
            }
            else if (numberSizeInBytes == 8){
                //convert and write 8-bytes from big-endian to little-endian.
                final long val = value.longValue();
                os.write(ByteUtil.convertLongIntoLittleEndian(val));
            }
            else
                throw new DacardPadException("Number value is out of 8-bytes range", new String[]{Double.toString(value.doubleValue())});
        }
        catch (IOException e){
            throw new DacardPadException("Failed to serialize Numeric type", e);
        }
    }

    public Number parse(InputStream is, short dataSize) throws DacardPadException{
        final byte[] buffer = new byte[dataSize];
        try{
            is.read(buffer);
            if(dataSize == 1)
                return buffer[0];
            else if(dataSize == 2)
                return ByteUtil.convertLittleEndianToShort(buffer);
            else if(dataSize == 4)
                return ByteUtil.convertLittleEndianToInt(buffer);
            else if(dataSize == 8)
                return ByteUtil.convertLittleEndianToLong(buffer);
            else
                throw new DacardPadException("Parsed Number size in unknown", new String[]{String.valueOf(dataSize)});
        }
        catch (IOException e){
            throw new DacardPadException("Failed to parse Number", e);
        }
    }

    protected short resolveNumberSizeInBytes(Number value) throws DacardPadException {
        if (Math.abs(value.doubleValue()) <= Byte.MAX_VALUE)
            return 1;
        else if (Math.abs(value.doubleValue()) <= Short.MAX_VALUE)
            return 2;
        else if (Math.abs(value.doubleValue()) <= Integer.MAX_VALUE)
            return 4;
        else if (Math.abs(value.doubleValue()) <= Long.MAX_VALUE)
            return 8;
        else
            throw new DacardPadException("Number value is out of 8-bytes range", new String[]{Double.toString(value.doubleValue())});
    }
}
