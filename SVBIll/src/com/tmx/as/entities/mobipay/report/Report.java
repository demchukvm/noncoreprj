package com.tmx.as.entities.mobipay.report;

import com.tmx.as.base.BasicEntity;
import com.tmx.as.entities.bill.operator.Operator;

import java.io.Serializable;
import java.util.Date;

/**
 */
public class Report extends BasicEntity {
    private Long reportId;
    private Date reportDate;
    private String fileName;
    private Operator operator;
    private String reportedGateName;

    public Operator getOperator() {
        return operator;
    }

    public void setOperator(Operator operator) {
        this.operator = operator;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public Long getReportId() {
        return reportId;
    }

    public void setReportId(Long reportId) {
        this.reportId = reportId;
    }

    public Date getReportDate() {
        return reportDate;
    }

    public void setReportDate(Date reportDate) {
        this.reportDate = reportDate;
    }

    public String getReportedGateName() {
        return reportedGateName;
    }

    public void setReportedGateName(String reportedGateName) {
        this.reportedGateName = reportedGateName;
    }

    public Serializable getId() {
        return getReportId();
    }

    public int getValid() {
        return valid;
    }

    public void setValid(int valid) {
        this.valid = valid;
    }
}
