package com.tmx.web.taglib.html;

import com.tmx.web.base.ApplicationEnvironment;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.taglib.TagUtils;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.SimpleTagSupport;
import java.io.IOException;

/**
 * <p>Rewrites URL. If <b>action</b> or <b>forward</b> is given, corresponding
 * URL will be taken for rewriting, else <b>href</b> will be rewritten.
 * 'Rewriting' means that internal URL will be prepended with context path (if
 * it does not already contain it), supplied locale suffix
 * (if needed). Also, if <b>anchor</b> is specified, any existing anchor will
 * be replaced (if it does not exist, it will be appended). Some parameters
 * may be added to query string.
 * </p>
 * <p>
 * Allowed attributes are:
 * <ul>
 * <li>
 * <b>href</b> - URL to rewrite
 * </li>
 * </ul>
 * </p>
 * <p>
 * This tag may also accept parameters through &lt;atleap:param&gt; tag. They
 * are added to resulting URL.
 * </p>
 * <p>
 * Only one of <b>action</b>, <b>forward</b> or <b>href</b> must be
 * specified.
 * </p>
 * <p>
 * Here's an example:
 * <pre>
 * &lt;atleap:rewriteUrl action="login" var="loginUrl" scope="session"&gt;
 *     &lt;atleap:param name="error" value="true"&gt;
 * &lt;/atleap:rewriteUrl&gt;
 * </pre>
 * This code will compute URL for 'login' action and save it to session scope
 * variable named 'loginUrl'. URL will contain a parameter with name 'error'
 * and value 'true'.
 * </p>
 * <p><a href="RewriteUrlTag.java.html"><i>View Source</i></a></p>
 *
 * @jsp.tag name="rewriteUrl"
 * body-content="scriptless"
 */
public class RewriteUrlTag extends SimpleTagSupport {

	protected transient final Log log = LogFactory.getLog(RewriteUrlTag.class);

	/**
	 * HREF to rewrite
	 */
	protected String href;


	/**
     * Returns href that will be added to address
     *
     * @return href
	 * @jsp.attribute required="false"
	 * rtexprvalue="true"
	 * type="java.lang.String"
	 * description="HREF to rewrite"
	 */
	public String getHref() {
		return href;
	}

    /**
     * Sets href that will be added to address
     *
     * @param href href to set
     */
	public void setHref(String href) {
		this.href = href;
	}



    /**
     * Processes the tag
     *
     * @throws JspException
     * @throws IOException
     */
	public void doTag() throws JspException, IOException {
    	PageContext pageContext = (PageContext) getJspContext();
        String result = ApplicationEnvironment.getAppDeploymentName() + getHref();
        TagUtils tagUtils = TagUtils.getInstance();
    	tagUtils.write(pageContext, result);
	}

}
