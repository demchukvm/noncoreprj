package com.tmx.beng.httpsgate.avancel;

import org.apache.log4j.Logger;

import java.net.ServerSocket;
import java.net.Socket;
import java.io.BufferedReader;
import java.io.PrintWriter;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

import com.tmx.beng.medaccess.avancel.AvancelGateMediumConnection1;
import com.tmx.beng.medaccess.avancel.AvancelGateMediumAccess1;
import com.tmx.beng.base.BillingMessageWritable;
import com.tmx.beng.base.BillingMessageImpl;
import com.tmx.beng.base.BillingMessage;

/**

 */
public class AvancelGateTest extends Thread implements Runnable {
    private static Logger logger = Logger.getLogger("avancel_gate.AvancelGateTest");

    public void run() {
        ServerSocket serverSocket = null;
        Socket socket = null;
        BufferedReader reader = null;
        PrintWriter writer = null;
        boolean doExit = false;

        try {
            AvancelGateMediumConnection1 gateConn = (AvancelGateMediumConnection1)new AvancelGateMediumAccess1().getConnection();
            serverSocket = new ServerSocket(61777);
            socket = serverSocket.accept();
            writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream()));
            writer.print("test>");
            writer.flush();
            reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            while (!doExit) {
                final String command = reader.readLine();
                //writer.println(command);
                writer.print("test>");
                writer.flush();
                if ("auth".equalsIgnoreCase(command)) {
                    BillingMessageWritable msg = new BillingMessageImpl();
                    msg.setOperationName("TEST_AUTH");
                    gateConn.send(msg);
                } else if ("pay".equalsIgnoreCase(command)) {
                    BillingMessageWritable msg = new BillingMessageImpl();
                    msg.setOperationName(BillingMessage.O_REFILL_PAYMENT);
                    msg.setAttributeString(BillingMessage.BILL_TRANSACTION_NUM, String.valueOf(System.currentTimeMillis()));
                    //msg.setAttributeString(BillingMessage.ACCOUNT_NUMBER, "380504161655");
                    //msg.setAttributeString(BillingMessage.PAY_ACCOUNT_NUMBER, "1.11490120"); //MTS connect
                    //msg.setAttributeString(BillingMessage.PAY_ACCOUNT_NUMBER, "1.11789441"); //MTS connect
                    msg.setAttributeString(BillingMessage.PAY_ACCOUNT_NUMBER, "1.11490120"); //MTS phone
                    msg.setAttributeString(BillingMessage.AMOUNT, "1.00");
                    //msg.setAttributeString(BillingMessage.SERVICE_CODE, "UMCPoP");
                    msg.setAttributeString(BillingMessage.SERVICE_CODE, "UMCContractNum");
                    msg.setAttributeString(BillingMessage.TERMINAL_SN, "555666777");
                    gateConn.send(msg);
                } else if ("voucher".equalsIgnoreCase(command)) {
                    BillingMessageWritable msg = new BillingMessageImpl();
                    msg.setOperationName(BillingMessage.O_BUY_VOUCHER);
                    msg.setAttributeString(BillingMessage.BILL_TRANSACTION_NUM, String.valueOf(System.currentTimeMillis()));
                    msg.setAttributeString(BillingMessage.VOUCHER_NOMINAL, "TestVoucher0");
                    msg.setAttributeString(BillingMessage.SERVICE_CODE, "Test");
                    msg.setAttributeString(BillingMessage.TERMINAL_SN, "555666777");
                    gateConn.send(msg);
                } else if ("getstatus".equalsIgnoreCase(command)) {
                    BillingMessageWritable msg = new BillingMessageImpl();
                    msg.setOperationName(BillingMessage.O_REFILL_PAYMENT_STATUS);
                    msg.setAttributeString(BillingMessage.BILL_TRANSACTION_NUM, "1237451697305");
                    gateConn.send(msg);
                } else if ("getlast".equalsIgnoreCase(command)) {
                    BillingMessageWritable msg = new BillingMessageImpl();
                    msg.setOperationName(BillingMessage.O_BUY_VOUCHER_STATUS);
                    msg.setAttributeString(BillingMessage.BILL_TRANSACTION_NUM, "1237451676976");
                    gateConn.send(msg);
                } else if ("exit".equalsIgnoreCase(command)) {
                    doExit = true;
                } else {
                    writer.println("Unknown command. Use: [auth, pay, voucher, getstatus, getlast, exit] ");
                    writer.flush();
                }
            }
        }
        catch (Throwable e) {
            logger.error("Failed to execute test command", e);
            try {
                if (reader != null) reader.close();
            } catch (Exception ex) {
            }
            try {
                if (writer != null) writer.close();
            } catch (Exception ex) {
            }
            try {
                if (socket != null) socket.close();
            } catch (Exception ex) {
            }
            try {
                if (serverSocket != null) serverSocket.close();
            } catch (Exception ex) {
            }
        }

    }
}

