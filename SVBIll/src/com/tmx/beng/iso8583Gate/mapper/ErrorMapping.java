package com.tmx.beng.iso8583Gate.mapper;

import java.util.Map;
import java.util.HashMap;

/**
 */
public class ErrorMapping {
    private String protocolVersion = null;
    private Map errorMap = null;

    public ErrorMapping(){
        errorMap = new HashMap();
    }

    public void addError(Error error){
        errorMap.put(error.getBillingCode(), error);
    }

    public String getProtocolVersion() {
        return protocolVersion;
    }

    public void setProtocolVersion(String protocolVersion) {
        this.protocolVersion = protocolVersion;
    }

    public Error getError(String billCode){
        Error error = null;
        boolean isUnknownCode = false;
        if(billCode == null)
            isUnknownCode = true;
        else
            error = (Error) errorMap.get(billCode);

        if(error == null || isUnknownCode){
            //if unknown error then generate suppressive response
            error = new Error();
            error.setBillingCode(billCode);
            error.setSuppressResponse(true);
        }
        return error;
    }
}
