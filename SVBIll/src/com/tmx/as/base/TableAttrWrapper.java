package com.tmx.as.base;

import org.hibernate.FetchMode;

import java.util.Iterator;

/**
 * RM: Now the String[] of table attrs names is used instead of this class.
 * The default fetch mode is still JOIN. It is set in EntityManager
 *
 * This is is the wrapper class to hold table attribute name and assigned Fetch Mode.
 * The default fetcg mode is JOIN.
 */
public class TableAttrWrapper {
    private FetchMode fetchMode = FetchMode.JOIN;
    private String tableAttributeName = null;

    /**
     * To hide default constructor
     */
    private TableAttrWrapper() {
    }

    /**
     * Create the wrapper for given table attribute with default FetchMode = JOIN
     */
    public TableAttrWrapper(String tableAttrName) {
        tableAttributeName = tableAttrName;
    }

    public FetchMode getFetchMode() {
        return fetchMode;
    }

    public void setFetchMode(FetchMode fetchMode) {
        this.fetchMode = fetchMode;
    }

    public String getTableAttributeName() {
        return tableAttributeName;
    }

    public void setTableAttributeName(String tableAttributeName) {
        this.tableAttributeName = tableAttributeName;
    }


    /**
     * Publish geven table attrs array into the string
     *
     * @param tableAttrs      array of table attrs to publish into String
     * @param formatString enble formating result string
     */
    public static String tableAttrsToString(String[] tableAttrs, boolean formatString) {
        StringBuffer tableAttrsString = new StringBuffer();
        //Pepare formatting strings
        String retSymb = (formatString) ? "\r" : "";

        if(tableAttrs != null){
            for(int i = 0; i < tableAttrs.length; i++){
                tableAttrsString.append("[table_attr_name='"+tableAttrs[i]+"']"+ retSymb);
            }
        }
        //Return NULL instead of empty String. It is more obviosly for publishing
        String result = tableAttrsString.toString().trim();
        return (result.length() > 0) ? result : "[no table attrs were fetched]";
    }

/*    public static String tableAttrsToString(TableAttrWrapper[] tableAttrs, boolean formatString) {
        StringBuffer tableAttrsString = new StringBuffer();
        //Pepare formatting strings
        String retSymb = (formatString) ? "\r" : "";

        if(tableAttrs != null){
            for(int i = 0; i < tableAttrs.length; i++){
                tableAttrsString.append("[table_attr_name='"+tableAttrs[i].getTableAttributeName()+"' ");
                tableAttrsString.append("fetch_mode='"+tableAttrs[i].getFetchMode().toString()+"']"+ retSymb);
            }
        }
        //Return NULL instead of empty String. It is more obviosly for publishing
        String result = tableAttrsString.toString().trim();
        return (result.length() > 0) ? result : "[no table attrs fetched]";
    }*/


    /** Creates new TableAttrWrapper[] array with size have increased for one position.
     * Then put given table attr into the last cell of new array. */
    public static String[] appendToTableAttrs(String[] tableAttrs, String tableAttr){
        if(tableAttrs == null)
            tableAttrs = new String[0];
        if(tableAttr == null)
            return tableAttrs;
        String[] newTableAttrs = new String[tableAttrs.length + 1];
        System.arraycopy(tableAttrs, 0, newTableAttrs, 0, tableAttrs.length);
        newTableAttrs[newTableAttrs.length-1] = tableAttr;
        return newTableAttrs;
    }
/*    public static TableAttrWrapper[] appendToTableAttrs(TableAttrWrapper[] tableAttrs, TableAttrWrapper tableAttr){
        if(tableAttrs == null)
            tableAttrs = new TableAttrWrapper[0];
        if(tableAttr == null)
            return tableAttrs;
        TableAttrWrapper[] newTableAttrs = new TableAttrWrapper[tableAttrs.length + 1];
        System.arraycopy(tableAttrs, 0, newTableAttrs, 0, tableAttrs.length);
        newTableAttrs[newTableAttrs.length-1] = tableAttr;
        return newTableAttrs;
    }*/
}

