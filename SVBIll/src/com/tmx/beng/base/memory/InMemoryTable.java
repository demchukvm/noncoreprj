package com.tmx.beng.base.memory;

import java.util.*;

/**

 */
public class InMemoryTable {
    /** Timestamp used in last id calculation */
    private long lastTimestamp = 0;
    /** Id to destinguish ids created in the same timestamp */
    private long subTimestampId = 0;
    protected Map rows = null;

    public InMemoryTable(){
        rows = Collections.synchronizedMap(new HashMap()); 
    }

    protected synchronized String nextId(){
        long currentTimestamp = System.currentTimeMillis();
        if(currentTimestamp == lastTimestamp){
            //handle duplication by issuing next subTimesmapId
            subTimestampId++;
        }
        else{
            lastTimestamp = currentTimestamp;
            subTimestampId = 0;//reset           
        }
        return String.valueOf(lastTimestamp) + String.valueOf(subTimestampId);
    }

    public InMemoryObject lookup(String id){
        if(id == null)
            return null;
        else
            return (InMemoryObject)rows.get(id);
    }

    public List lookup(Condition cond){
        List retrieved = new ArrayList();
        for(Iterator rowsIter = rows.values().iterator(); rowsIter.hasNext();){
            InMemoryObject obj = (InMemoryObject)rowsIter.next();
            if(cond.evaluate(obj))
                retrieved.add(obj);
        }
        return retrieved;
    }

    public int size() {
        return rows.size();
    }

    protected String insert(InMemoryObject inMemoryObject){
        String rowId = nextId();
        inMemoryObject.setId(rowId);
        rows.put(rowId, inMemoryObject);
        return rowId;
    }

    protected void delete(String id){
        rows.remove(id);
    }
}
