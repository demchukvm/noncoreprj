package com.tmx.web.forms.core.restriction.seller_restriction;

import com.tmx.as.entities.bill.seller.Seller;
import com.tmx.as.entities.bill.restriction.SellerRestriction;
import com.tmx.as.entities.general.user.User;
import com.tmx.as.exceptions.DatabaseException;
import com.tmx.as.resolver.FunctionSyntaxException;
import com.tmx.as.resolver.ResolverException;
import com.tmx.as.base.criterion.CriterionWrapper;
import com.tmx.as.base.criterion.Restrictions;
import com.tmx.as.blogic.BlogicException;
import com.tmx.as.blogic.restriction.SellerRestrictionManager;
import com.tmx.web.controls.ScrollBox;
import com.tmx.web.controls.CriterionFactory;
import com.tmx.web.controls.PopulateUtil;
import com.tmx.web.controls.table.Parameter;
import com.tmx.web.controls.table.ControlManagerTable;
import com.tmx.web.forms.core.restriction.AbstractManageRestrictionsForm;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

                                                             
public class ManageSellerRestrictionsForm extends AbstractManageRestrictionsForm {

    private ControlManagerTable incomingRestrictionsTable = new ControlManagerTable();

    private List<Seller> sellers;
    private List<Seller> subSellers;

    public void copySelectedResrtiction() throws BlogicException, DatabaseException {
        new SellerRestrictionManager().copyRestriction(getIncomingRestrictionId(), getSelectedSubSellerId());
    }

    public void copyAllRestrictions(User loggeInUser) throws BlogicException, DatabaseException{// copy all from incoming tariffs to sub sellers tariffs
        new SellerRestrictionManager().copyAllRestrictions(getSelectedSellerIdIncomingTable(), getSelectedSubSellerId());
    }

    protected void init() {
        super.init();
        initRestrictionsTable();
        initIncomingRestrictionsTable();
        initButtons();
        initFilters();
        initRestrictionsTableFilters();

    }

    private void initIncomingRestrictionsTable() {
        incomingRestrictionsTable.loadQuery("query(query_type = retrieve_all," +
                "entity_class=com.tmx.as.entities.bill.restriction.SellerRestriction," +
                "table_attr=functionInstance.functionType," +
                "table_attr=functionInstance.functionParameters," +
                "table_attr=seller," +
                "order(name=restrictionId,type=asc)," +
                "page_size=15" +
                ")");
        incomingRestrictionsTable.setRefreshActionName("/core/restriction/seller_restriction/manageSellerRestrictions");
        incomingRestrictionsTable.setRefreshCommandName("refreshIncomingRestrictionsTable");
    }

    private void initRestrictionsTable() {
        getRestrictionsTable().loadQuery("query(query_type = retrieve_all," +
                "entity_class = com.tmx.as.entities.bill.restriction.SellerRestriction," +
                "table_attr = functionInstance.functionType," +
                "table_attr = functionInstance.functionParameters," +
                "table_attr = seller," +
                "order(name = restrictionId, type = asc)," +
                "page_size = 15" +
                ")");
        getRestrictionsTable().setRefreshActionName("/core/restriction/seller_restriction/manageSellerRestrictions");
    }

    public void bindSellers(List<Seller> sellers)  {
        getSellersScrollBox().bind(sellers);
        setSellers(sellers);
    }

    public void bindRestrictionNames(List<SellerRestriction> RestrName)  {
        getRestrScrollBox().bind((List)RestrName);
    }

    public void bindSubSellers(List<Seller> subSellers) {
        Collections.sort(subSellers, new Comparator<Seller>()
        {
          public int compare(Seller sb1, Seller sb2) {
            return sb1.getCode().compareTo(sb2.getCode());
          }
        });


        getSubSellersScrollBox().bind(subSellers);
        setSubSellers(subSellers);
    }

    public ScrollBox<Seller> getSellersScrollBox() {
        return (ScrollBox<Seller>) getIncomingRestrictionsTable().getParameterControl("by_seller_id", "seller_id");
    }

    public ScrollBox<Seller> getRestrScrollBox() {
        return (ScrollBox<Seller>) getIncomingRestrictionsTable().getParameterControl("by_restriction_id", "restriction_id");
    }

    public ScrollBox<Seller> getSubSellersScrollBox() {
        return (ScrollBox<Seller>) getRestrictionsTable().getParameterControl("by_seller_id", "seller_id");
    }

    private void initFilters() {
        getRestrictionsTable().addParameterToFilter("by_seller_id", new Parameter("seller_id", initSellersScrollBoxFilter(true)));
        getIncomingRestrictionsTable().addParameterToFilter("by_seller_id", new Parameter("seller_id", initSellersScrollBoxFilter(true)));
        getIncomingRestrictionsTable().addParameterToFilter("by_restriction_id", new Parameter("restriction_id", initRestrictNameScrollBox(true)));
    }

    private ScrollBox initSellersScrollBoxFilter(boolean nullable) {
        ScrollBox<Seller> sellersScrollBox = new ScrollBox<Seller>() {
            protected String getKey(Seller seller) {
                return seller.getSellerId().toString();
            }

            protected String getValue(Seller seller) {
                return seller.getName();
            }
        };
        sellersScrollBox.setNullable(nullable);
        return sellersScrollBox;
    }

    private ScrollBox initRestrictNameScrollBox(boolean nullable) {
        ScrollBox<SellerRestriction> sellersRestrictNameScrollBox = new ScrollBox<SellerRestriction>() {

            protected String getKey(SellerRestriction sellerRestrName) {
                return  sellerRestrName.getRestrictionId().toString();
            }

            protected String getValue(SellerRestriction sellerRestrName) {
                return sellerRestrName.getName();
            }
        };
        sellersRestrictNameScrollBox.setNullable(nullable);
        return sellersRestrictNameScrollBox;
    }

    private void initRestrictionsTableFilters() {
        CriterionFactory factory = new CriterionFactory() {
            public CriterionWrapper createCriterion() {
                if (subSellers == null || subSellers.isEmpty())
                    return Restrictions.in("seller", getNonExistentSellers());
                return Restrictions.in("seller", subSellers);
            }
        };
        getRestrictionsTable().setCriterionFactory(factory);
    }

    private Seller[] getNonExistentSellers() {
        Seller seller = new Seller();
        seller.setSellerId((long) -1);
        return new Seller[]{seller};
    }

    private void initButtons() {
        getOnCreateButton().setAction("/core/restriction/seller_restriction/manageSellerRestrictions");
        getOnEditButton().setAction("/core/restriction/seller_restriction/infoSellerRestriction");
        getOnDeleteButton().setAction("/core/restriction/seller_restriction/infoSellerRestriction");

        getOnCopyButton().setAction("/core/restriction/seller_restriction/manageSellerRestrictions");
        getOnCopyAllButton().setAction("/core/restriction/seller_restriction/manageSellerRestrictions");
        getOnDeleteIRButton().setAction("/core/restriction/seller_restriction/manageSellerRestrictions");
        getOnCreateIRButton().setAction("/core/restriction/seller_restriction/manageSellerRestrictions");
        getOnEditIRButton().setAction("/core/restriction/seller_restriction/infoSellerRestriction");
    }

    public ControlManagerTable getIncomingRestrictionsTable() {
        return incomingRestrictionsTable;
    }

    public List getSellers() {
        return sellers;
    }

    public void setSellers(List<Seller> sellers) {
        this.sellers = sellers;
    }

    public List getSubSellers() {
        return subSellers;
    }

    public void setSubSellers(List<Seller> subSellers) {
        this.subSellers = subSellers;
    }

    public String getSelectedIncomingSellerName() {
        return ((ScrollBox) getIncomingRestrictionsTable().getParameterControl("by_seller_id", "seller_id")).getSelectedEntry().getValue();
    }

    public String getSelectedCustomizableSellerName() {
        return ((ScrollBox) getRestrictionsTable().getParameterControl("by_seller_id", "seller_id")).getSelectedEntry().getValue();
    }

    public Long getSelectedSubSellerId() {
        return PopulateUtil.getLongKey((ScrollBox) getRestrictionsTable().getParameterControl("by_seller_id", "seller_id"));
    }
    public Long getSelectedSellerIdIncomingTable() {
        return PopulateUtil.getLongKey( (ScrollBox)getIncomingRestrictionsTable().getParameterControl("by_seller_id", "seller_id") );
    }

    public ControlManagerTable setUpCategory(final String ... categories) throws FunctionSyntaxException, DatabaseException, ResolverException {
        ControlManagerTable table = new ControlManagerTable("query(query_type=retrieve_all," +
                "entity_class=com.tmx.as.entities.bill.function.FunctionType," +
                "order(name=id, type=asc)," +
                "page_size=15" +
                ")");

        table.setCriterionFactory(new CriterionFactory() {
            public CriterionWrapper createCriterion() {
                return Restrictions.in("category", categories);
            }
        });
        table.refresh();
        return table;
    }

}
