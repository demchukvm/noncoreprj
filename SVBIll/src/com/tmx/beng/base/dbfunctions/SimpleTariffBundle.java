package com.tmx.beng.base.dbfunctions;

import com.tmx.beng.base.BillingMessageWritable;
import com.tmx.beng.base.GlobalContext;
import com.tmx.beng.base.Reservation;
import com.tmx.beng.base.cache.FunctionWrapper;
import com.tmx.beng.base.cache.InvokeFunctionException;

import java.util.HashMap;
import java.util.Map;

/**
    Tariff bundle has the same interface as pure tariff does, but
    it could use another tariffs on calculation choosing them by
    actual prameters assigned with tariff bundle's funciton in DB.

    This simple tariff bundle executes tariff's implementations according to actual DB params.
    This params should have following format:

    Parameter name: [requsted operation].[service code].implClass
    Parameter value: [impl class full name]

    Parameter name: [requsted operation].[service code].implMethod
    Parameter value: [tariff method name]

    All actual params received by tariff bundle will be passed to executed submissed tariff. 
 */
public class SimpleTariffBundle extends BasicTariffFunction {
    private static final String SEPARATOR = ".";
    private static final String WILDCARD = "*";
    /** to refer on tariff name */
    private static final String TARIFF_NAME = "tariffName";
    /** to refer on tariff type (TERMINAL, TERMINAL_GROUP, SELLER, OPERATOR) */
    private static final String TARIFF_TYPE = "tariffType";
    private static final String TERMINAL = "TERMINAL";
    private static final String TERMINAL_GROUP = "TERMINAL_GROUP";
    private static final String SELLER = "SELLER";
    private static final String OPERATOR = "OPERATOR";

    boolean isTariffApplicable(Map actualParams) {
        return true;
    }

    void applyTariff(Map actualParams, Reservation reservation) throws InvokeFunctionException {
        //get service code
/*        final String operation = billingMessage.getOperationName();
        String serviceCode = billingMessage.getAttributeString(BillingMessage.SERVICE_CODE);

        //get tariff type
        String tariffType = (String) actualParams.get(TARIFF_TYPE);

        //get tariff name
        String tariffName = (String) actualParams.get(operation + SEPARATOR + serviceCode + SEPARATOR + TARIFF_NAME);
        if (tariffName == null)
            tariffName = (String) actualParams.get(operation + SEPARATOR + WILDCARD + SEPARATOR + TARIFF_NAME);
        if (tariffName == null)
            throw new InvokeFunctionException("err.simple_tariff_bundle.no_tariff_found", new String[]{tariffName});

        FunctionWrapper functionWrapper = null;
        DBCache dbCache = globalContext.getDbCache();
        if (TERMINAL.equalsIgnoreCase(tariffType)) {
            // terminal tariff
            String terminalSn = billingMessage.getAttributeString(BillingMessage.TERMINAL_SN);
            try{
                functionWrapper = dbCache.getTerminalTariff(terminalSn, tariffName);
            }
            catch(DBCacheException e){
                throw new InvokeFunctionException("err.simple_tariff_bundle.tariff_function_not_found_for_terminal", new String[]{terminalSn, tariffName}, e);
            }
            calculateTariff0(functionWrapper, globalContext, billingMessage, Terminal.class, terminalSn);
        } else if (TERMINAL_GROUP.equalsIgnoreCase(tariffType)) {
            // terminal group tariff
            String terminalSn = billingMessage.getAttributeString(BillingMessage.TERMINAL_SN);
            try{
                functionWrapper = dbCache.getTerminalGroupTariff(terminalSn, tariffName);
            }
            catch(DBCacheException e){
                throw new InvokeFunctionException("err.simple_tariff_bundle.tariff_function_not_found_for_terminal_group", new String[]{terminalSn, tariffName});
            }
            calculateTariff0(functionWrapper, globalContext, billingMessage, TerminalGroup.class, terminalSn);
        } else if (SELLER.equalsIgnoreCase(tariffType)) {
            // seller tariff
            String sellerCode = billingMessage.getAttributeString(BillingMessage.SELLER_CODE);
            List sellersParentsChainCodes = dbCache.getSellerWrapperParentsChainCodes(sellerCode);
            for (Iterator sellersIter = sellersParentsChainCodes.iterator(); sellersIter.hasNext();) {
                String nextSellerCode = (String) sellersIter.next();
                try{
                    functionWrapper = dbCache.getSellerTariff(nextSellerCode, tariffName);
                }
                catch(DBCacheException e){
                    throw new InvokeFunctionException("err.simple_tariff_bundle.tariff_function_not_found_for_seller", new String[]{nextSellerCode, tariffName});
                }
                calculateTariff0(functionWrapper, globalContext, billingMessage, Seller.class, nextSellerCode);
            }
        } else if (OPERATOR.equalsIgnoreCase(tariffType)) {
            // operator tariff
            String operatorCode = billingMessage.getAttributeString(BillingMessage.SERVICE_CODE);
            try{
                functionWrapper = dbCache.getOperatorTariff(operatorCode, tariffName);
            }
            catch(DBCacheException e){
                throw new InvokeFunctionException("err.simple_tariff_bundle.tariff_function_not_found_for_operator", new String[]{operatorCode, tariffName});
            }
            calculateTariff0(functionWrapper, globalContext, billingMessage, Operator.class, operatorCode);
        }*/
    }


    private void calculateTariff0(FunctionWrapper functionWrapper,
                                  GlobalContext globalContext,
                                  BillingMessageWritable msg,
                                  Class tariffOwnerType,
                                  String tariffOwnerCode,
                                  String tariffOwnerPrimaryBalanceCode) throws InvokeFunctionException {
//        try{
//            ((BasicTariffFunction)functionWrapper.getObjectInstance()).init(globalContext, msg);
//            Map runtimeParams = new HashMap();
//            runtimeParams.put(BasicTariffFunction.TARIFF_OWNER_TYPE, tariffOwnerType);
//            runtimeParams.put(BasicTariffFunction.TARIFF_OWNER_CODE, tariffOwnerCode);
//            runtimeParams.put(BasicTariffFunction.TARIFF_OWNER_PRIMARY_BALANCE_CODE, tariffOwnerPrimaryBalanceCode);
//            functionWrapper.invoke();
//        }
//        catch(InvokeFunctionException e){
//            throw new InvokeFunctionException("err.simple_tariff_bundle.tariff_calculation_failed", e);
//        }
    }
}
