package com.tmx.web.taglib.html.ctrl;

import com.tmx.web.controls.Button;
import com.tmx.web.controls.BasicControl;
import com.tmx.web.base.WebException;

/**
 * To render button based on html div element
 */
public class ButtonArtificialTag extends ButtonTag{
    //-------------------------Properties

    public String getStyleClass() {
        return (styleClass != null) ? styleClass : getDefaultProperty("tag.button_artificial.style.styleClass");
    }

    public String getDisabledStyleClass() {
        return (disabledStyleClass != null) ? disabledStyleClass : getDefaultProperty("tag.button_artificial.style.disabledStyleClass");
    }

    public String getHoveredStyleClass() {
        return (hoveredStyleClass != null) ? hoveredStyleClass : getDefaultProperty("tag.button_artificial.style.hoveredStyleClass");
    }


    protected void renderButton(StringBuffer resultHTML, Button button) {
        String type = " type=\"button\" ";
        String name = " name=\""+getProperty()+"\" ";
        String styleClass = " class=\""+(button.isReadonly() ? getDisabledStyleClass() : getStyleClass())+"\" ";

        resultHTML.append("<table cellpadding=\"0\" cellspacing=\"0\" border=\"0\"><tr>");
        resultHTML.append("<td nowrap" +
                type + name + styleClass +
//                getOnClickAttr() + getOnMouseOutAttr() + getOnMouseOverAttr() + ">\r");
                getOnEventAttrsString() + ">\r");                

        resultHTML.append("&nbsp;"+button.getLocalizedLabel() + "&nbsp;\r");

        resultHTML.append("</td>\r");
        resultHTML.append("</tr></table>\r");
    }

    protected String renderOnMouseOutScript(BasicControl ctrl) throws WebException {
       if(ctrl.isReadonly())
            return "     //do nothing, because button is disabled. \r";

        return " "+CTRL_REF+".className=\""+getStyleClass()+"\"; \n";
    }

    protected String renderOnMouseOverScript(BasicControl ctrl) throws WebException {
        if(ctrl.isReadonly())
            return "     //do nothing, because button is disabled. \r";

        return " "+CTRL_REF+".className=\""+getHoveredStyleClass()+"\"; \n";
    }

}
