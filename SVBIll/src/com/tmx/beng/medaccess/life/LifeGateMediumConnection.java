package com.tmx.beng.medaccess.life;

import com.tmx.beng.httpsgate.HttpsGateException;
import com.tmx.beng.httpsgate.life.BasicLifeGate;
import com.tmx.beng.httpsgate.life.LifeGate;
import com.tmx.beng.base.BillingMessage;
import com.tmx.beng.medaccess.MediumConnection;

/**
  Medium connection to work with Beeline gate
 */
public class LifeGateMediumConnection implements MediumConnection {
    private LifeGate lifeGate = null;

    LifeGateMediumConnection() throws HttpsGateException {
        lifeGate = LifeGate.getInstance();
    }

    public void send(BillingMessage msg) {
        lifeGate.enqueueBillMessage(msg);
    }
}