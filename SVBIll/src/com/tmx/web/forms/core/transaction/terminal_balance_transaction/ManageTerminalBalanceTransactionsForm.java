package com.tmx.web.forms.core.transaction.terminal_balance_transaction;

import com.tmx.as.base.criterion.CriterionWrapper;
import com.tmx.as.base.criterion.Restrictions;
import com.tmx.web.base.BasicActionForm;
import com.tmx.web.controls.*;
import com.tmx.web.controls.table.Table;
import com.tmx.web.controls.table.Filter;
import com.tmx.web.controls.table.Parameter;
import com.tmx.web.controls.table.ControlManagerTable;
import com.tmx.util.i18n.DictionaryWrapper;
import com.tmx.beng.base.StatusDictionary;
import com.tmx.as.entities.bill.transaction.TransactionType;

import java.util.GregorianCalendar;
import java.util.Calendar;


public class ManageTerminalBalanceTransactionsForm extends BasicActionForm {

    private ControlManagerTable terminalTransactionsTable;
    private DictionaryWrapper transactionStatusDictionary;

    private EditBox editBoxAllTransaction;
    private EditBox editBoxSuccessTransaction;

    private EditBox editBoxAllTransAmount;
    private EditBox editBoxSuccessTransAmount;



    protected void init() {

        editBoxAllTransaction = new EditBox();
        editBoxAllTransaction.setReadonly(true);

        editBoxSuccessTransaction = new EditBox();
        editBoxSuccessTransaction.setReadonly(true);

        editBoxAllTransAmount = new EditBox();
        editBoxAllTransAmount.setReadonly(true);

        editBoxSuccessTransAmount = new EditBox();
        editBoxSuccessTransAmount.setReadonly(true);

        initTables();
        initFilters();

        transactionStatusDictionary = new DictionaryWrapper(getLocale(),
                StatusDictionary.class, StatusDictionary.STATUS_MSGS_BUNDLE);

    }

    private void initFilters() {
        terminalTransactionsTable.addParameterToFilter("by_client_amount_from", "from_amount", new EditBox());

        terminalTransactionsTable.addParameterToFilter("by_client_amount_to","to_amount", new EditBox());

        terminalTransactionsTable.addParameterToFilter("by_transaction_type", "type", new ScrollBox(true){
                                protected String getKey(Object dataListElement) {
                                    return ((TransactionType)dataListElement).getId().toString();
                                }

                                protected String getValue(Object dataListElement) {
                                    return ((TransactionType)dataListElement).getName();
                                }
                            });

        //selectFrom filter
        Filter fromDateFilter = new Filter();
        TimeSelector timeFromSelector = new TimeSelector();
        GregorianCalendar prototype = new GregorianCalendar();
        GregorianCalendar calendar = new GregorianCalendar(
                                        prototype.get(Calendar.YEAR),
                                        prototype.get(Calendar.MONTH),
                                        prototype.get(Calendar.DATE),
                                        0,
                                        0
        );
        timeFromSelector.setTime(calendar.getTime());//set current day
        fromDateFilter.addParameter("date", new Parameter(timeFromSelector));
        terminalTransactionsTable.getFilterSet().addFilter("by_FromDate", fromDateFilter);
//        terminalTransactionsTable.addParameterToFilter("by_FromDate", "date", new TimeSelector());
        //selectTo filter
        terminalTransactionsTable.addParameterToFilter("by_ToDate", "date", new TimeSelector());
        //for billing transaction number
        terminalTransactionsTable.addParameterToFilter("by_billnum_like", "billnum", new EditBox());
        
        //for client transaction number
        terminalTransactionsTable.addParameterToFilter("by_clinum_like", "clinum", new EditBox());


        BrowseBox browseBoxTerminals = new BrowseBox("/core/balance/terminal_balance/browseTerminalsAction");
//        browseBoxTerminals.setKey("0");//imposible value
       // terminalTransactionsTable.addParameterToFilter("by_terminal", "terminal", browseBoxTerminals);
        terminalTransactionsTable.addParameterToFilter("by_terminal_id", new Parameter("terminal_id", browseBoxTerminals));






    }

    private void initTables() {
        terminalTransactionsTable = new ControlManagerTable("query(query_type=retrieve_all," +
                "entity_class=com.tmx.as.entities.bill.transaction.TerminalBalanceTransaction," +
                "table_attr=terminal," +
                "table_attr=terminalBalance," +
                "order(name=transactionTime,type=desc)," +
                "page_size=15" +
                ")");
        terminalTransactionsTable.setRefreshActionName("/core/transactions/terminal_transaction/manageTerminalTransactions");
    }

    //--------Properties

    public ControlManagerTable getTerminalTransactionsTable() {
        return terminalTransactionsTable;
    }

    public void setTerminalTransactionsTable(ControlManagerTable terminalTransactionsTable) {
        this.terminalTransactionsTable = terminalTransactionsTable;
    }

    public DictionaryWrapper getTransactionStatusDictionary() {
        return transactionStatusDictionary;
    }

    public void setTransactionStatusDictionary(DictionaryWrapper transactionStatusDictionary) {
        this.transactionStatusDictionary = transactionStatusDictionary;
    }

    public EditBox getEditBoxAllTransaction() {
        return editBoxAllTransaction;
    }

    public void setEditBoxAllTransaction(EditBox editBoxAllTransaction) {
        this.editBoxAllTransaction = editBoxAllTransaction;
    }

    public EditBox getEditBoxSuccessTransaction() {
        return editBoxSuccessTransaction;
    }

    public void setEditBoxSuccessTransaction(EditBox editBoxSuccessTransaction) {
        this.editBoxSuccessTransaction = editBoxSuccessTransaction;
    }

    public EditBox getEditBoxAllTransAmount() {
        return editBoxAllTransAmount;
    }

    public void setEditBoxAllTransAmount(EditBox editBoxAllTransAmount) {
        this.editBoxAllTransAmount = editBoxAllTransAmount;
    }

    public EditBox getEditBoxSuccessTransAmount() {
        return editBoxSuccessTransAmount;
    }

    public void setEditBoxSuccessTransAmount(EditBox editBoxSuccessTransAmount) {
        this.editBoxSuccessTransAmount = editBoxSuccessTransAmount;
    }


}
