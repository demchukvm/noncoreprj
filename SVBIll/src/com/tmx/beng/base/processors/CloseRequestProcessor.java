package com.tmx.beng.base.processors;

import com.tmx.beng.base.Task;
import com.tmx.beng.base.BillingMessageWritable;
import com.tmx.beng.base.BillingMessage;
import com.tmx.beng.base.cache.DBCache;
import com.tmx.as.base.EntityManager;
import com.tmx.as.base.FilterWrapper;
import com.tmx.as.entities.bill.request.Request;
import com.tmx.as.exceptions.DatabaseException;

import java.util.Date;

/**
 Close transaction task
 */
public class CloseRequestProcessor extends TaskProcessor {

    public Task process0(Task task, DBCache dbCache, BillingMessageWritable msg){
        logger.debug("Closing request...");
        task.getGlobalContext().getTransactionPool().closeTransaction(
                task.getProcesedMessage().getAttributeString(BillingMessage.REQUEST_ID));

        try{
            FilterWrapper by_reqnum = new FilterWrapper("by_reqnum");
            by_reqnum.setParameter("reqnum", msg.getAttributeString(BillingMessage.REQUEST_ID));
            EntityManager entityManager = new EntityManager();
            Request request = (Request)entityManager.RETRIEVE(Request.class, new FilterWrapper[]{ by_reqnum }, null);
            if(request != null){
                request.setEndTime(new Date());
                request.setStatus(Request.SESSION_CLOSED);
                entityManager.SAVE(request);
            }
        }catch(DatabaseException e){
            logger.error(e.getLocalizedMessage(), e);
        }
        return task;
    }
}
