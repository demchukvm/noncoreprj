package com.tmx.beng.iso8583Gate.processors;

import com.tmx.beng.iso8583Gate.pdu.RuntimeMessage;

/**
 */
public class CancelSellServiceProcessor extends BasicProcessor{

    public int getMessageTypeId() {
        return 210;
    }

    public int getProcessingCode() {
        return 20000;
    }

    public int[] getDEToCopy() {
        return new int[]{4, 11, 24, 37, 38 ,39, 41};
    }

    public void processSpecificTask(RuntimeMessage reqMsg, RuntimeMessage respMsg) {
    }
}

