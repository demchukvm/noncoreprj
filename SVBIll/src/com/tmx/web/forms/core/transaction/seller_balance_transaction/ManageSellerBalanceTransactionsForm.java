package com.tmx.web.forms.core.transaction.seller_balance_transaction;

import com.tmx.web.base.BasicActionForm;
import com.tmx.web.controls.*;
import com.tmx.web.controls.table.Table;
import com.tmx.web.controls.table.Filter;
import com.tmx.web.controls.table.Parameter;
import com.tmx.web.controls.table.ControlManagerTable;
import com.tmx.util.i18n.DictionaryWrapper;
import com.tmx.beng.base.StatusDictionary;
import com.tmx.as.entities.bill.seller.Seller;
import com.tmx.as.entities.bill.operator.Operator;
import com.tmx.as.entities.bill.transaction.TransactionType;
import com.tmx.as.entities.bill.balance.SellerBalance;

import java.util.GregorianCalendar;
import java.util.Calendar;


public class ManageSellerBalanceTransactionsForm extends BasicActionForm {

    private ControlManagerTable sellerTransactionsTable;
    private DictionaryWrapper transactionStatusDictionary;

    private EditBox editBoxAllTransaction;
    private EditBox editBoxSuccessTransaction;

    private EditBox editBoxAllTransAmount;
    private EditBox editBoxSuccessTransAmount;

    private EditBox editBoxFeeForTx;

 
    protected void init() {

        editBoxAllTransaction = new EditBox();
        editBoxAllTransaction.setReadonly(true);

        editBoxSuccessTransaction = new EditBox();
        editBoxSuccessTransaction.setReadonly(true);

        editBoxAllTransAmount = new EditBox();
        editBoxAllTransAmount.setReadonly(true);

        editBoxSuccessTransAmount = new EditBox();
        editBoxSuccessTransAmount.setReadonly(true);

        
        sellerTransactionsTable = new ControlManagerTable("query(query_type=retrieve_all," +
                "entity_class=com.tmx.as.entities.bill.transaction.SellerBalanceTransaction," +
                "table_attr=seller," +
                "table_attr=sellerBalance," +
                "order(name=transactionTime,type=desc)," +
                "page_size=15" +
                ")");

        sellerTransactionsTable.setRefreshActionName("/core/transactions/seller_transaction/manageSellerTransactions");

        Filter amountFilterFrom = new Filter("by_client_amount_from", new Parameter("from_amount", new EditBox()));
        sellerTransactionsTable.addFilter(amountFilterFrom);

        Filter amountFilterTo = new Filter();
        amountFilterTo.addParameter("to_amount", new Parameter(new EditBox()));
        sellerTransactionsTable.getFilterSet().addFilter("by_client_amount_to", amountFilterTo);

        /*ScrollBox scrollBoxSellers = new ScrollBox(){
            protected String getKey(Object dataListElement) {
                return ((Seller)dataListElement).getSellerId().toString();
            }

            protected String getValue(Object dataListElement) {
                return ((Seller)dataListElement).getName();
            }
        };*/


        //////////////////////
        sellerTransactionsTable.addParameterToFilter("by_seller", "seller", new ScrollBox(false) {
            protected String getKey(Object dataListElement) {
                return ((Seller) dataListElement).getSellerId().toString();
            }

            protected String getValue(Object dataListElement) {
                return ((Seller) dataListElement).getName();
            }
        });
        /////////////////////////

//        scrollBoxSellers.setNullable(false); //TODO for sub sellers posibility
        /*Filter sellerFilter = new Filter();
        sellerFilter.addParameter("seller", new Parameter(scrollBoxSellers));
        sellerTransactionsTable .getFilterSet().addFilter("by_seller", sellerFilter);*/



        //filter by Seller balance
        ScrollBox scrollBoxSellerBalance = new ScrollBox(){
            protected String getKey(Object dataListElement) {
                return ((SellerBalance)dataListElement).getBalanceId().toString();
            }

            protected String getValue(Object dataListElement) {
                return ((SellerBalance)dataListElement).getCode();
            }
        };
        Filter sellerBalanceFilter = new Filter();
        sellerBalanceFilter.addParameter("sellerBalance", new Parameter(scrollBoxSellerBalance));
        sellerTransactionsTable .getFilterSet().addFilter("by_seller_balance", sellerBalanceFilter);

        //filter by operator
        ScrollBox scrollBoxOperators = new ScrollBox(){
            protected String getKey(Object dataListElement) {
                return ((Operator)dataListElement).getOperatorId().toString();
            }

            protected String getValue(Object dataListElement) {
                return ((Operator)dataListElement).getName();
            }
        };

        scrollBoxOperators.setNullable(true);
        Filter operatorFilter = new Filter();
        operatorFilter.addParameter("operator", new Parameter(scrollBoxOperators));
        sellerTransactionsTable .getFilterSet().addFilter("by_operator", operatorFilter);

        //filter by trans type
        ScrollBox scrollBoxTransTypes = new ScrollBox(){
            protected String getKey(Object dataListElement) {
                return ((TransactionType)dataListElement).getId().toString();
            }

            protected String getValue(Object dataListElement) {
                return ((TransactionType)dataListElement).getName();
            }
        };
        scrollBoxTransTypes.setNullable(true);
        Filter transTypeFilter = new Filter();
        transTypeFilter.addParameter("type", new Parameter(scrollBoxTransTypes));
        sellerTransactionsTable .getFilterSet().addFilter("by_transaction_type", transTypeFilter);

        //selectFrom filter
        Filter fromDateFilter = new Filter();
        TimeSelector timeFromSelector = new TimeSelector();
        GregorianCalendar prototype = new GregorianCalendar();
        GregorianCalendar calendar = new GregorianCalendar(
                                        prototype.get(Calendar.YEAR),
                                        prototype.get(Calendar.MONTH),
                                        prototype.get(Calendar.DATE),
                                        0,
                                        0
        );
        timeFromSelector.setTime(calendar.getTime());//set current day
        fromDateFilter.addParameter("date", new Parameter(timeFromSelector));
        sellerTransactionsTable .getFilterSet().addFilter("by_FromDate", fromDateFilter);

        //selectTo filter
        Filter toDateFilter = new Filter();
        TimeSelector timeToSelector = new TimeSelector();
        toDateFilter.addParameter("date", new Parameter(timeToSelector));
        sellerTransactionsTable .getFilterSet().addFilter("by_ToDate", toDateFilter);

        //for billing transaction number
        Filter billTransactionFilter = new Filter();
        billTransactionFilter.addParameter("billnum", new Parameter(new EditBox()));
        sellerTransactionsTable .getFilterSet().addFilter("by_billnum_like", billTransactionFilter);

        //for client transaction number
        Filter cliTransactionFilter = new Filter();
        cliTransactionFilter.addParameter("clinum", new Parameter(new EditBox()));
        sellerTransactionsTable .getFilterSet().addFilter("by_clinum_like", cliTransactionFilter);


        transactionStatusDictionary = new DictionaryWrapper(getLocale(),
                StatusDictionary.class, StatusDictionary.STATUS_MSGS_BUNDLE);

    }

    //--------Properties

    public ControlManagerTable getSellerTransactionsTable() {
        return sellerTransactionsTable;
    }

    public void setSellerTransactionsTable(ControlManagerTable sellerTransactionsTable) {
        this.sellerTransactionsTable = sellerTransactionsTable;
    }

    public DictionaryWrapper getTransactionStatusDictionary() {
        return transactionStatusDictionary;
    }

    public void setTransactionStatusDictionary(DictionaryWrapper transactionStatusDictionary) {
        this.transactionStatusDictionary = transactionStatusDictionary;
    }

    public EditBox getEditBoxAllTransaction() {
        return editBoxAllTransaction;
    }

    public void setEditBoxAllTransaction(EditBox editBoxAllTransaction) {
        this.editBoxAllTransaction = editBoxAllTransaction;
    }

    public EditBox getEditBoxSuccessTransaction() {
        return editBoxSuccessTransaction;
    }

    public void setEditBoxSuccessTransaction(EditBox editBoxSuccessTransaction) {
        this.editBoxSuccessTransaction = editBoxSuccessTransaction;
    }

    public EditBox getEditBoxAllTransAmount() {
        return editBoxAllTransAmount;
    }

    public void setEditBoxAllTransAmount(EditBox editBoxAllTransAmount) {
        this.editBoxAllTransAmount = editBoxAllTransAmount;
    }

    public EditBox getEditBoxSuccessTransAmount() {
        return editBoxSuccessTransAmount;
    }

    public void setEditBoxSuccessTransAmount(EditBox editBoxSuccessTransAmount) {
        this.editBoxSuccessTransAmount = editBoxSuccessTransAmount;
    }



}
