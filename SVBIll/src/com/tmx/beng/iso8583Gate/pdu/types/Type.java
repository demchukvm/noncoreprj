package com.tmx.beng.iso8583Gate.pdu.types;

import com.tmx.beng.iso8583Gate.pdu.RuntimeDataElement;
import com.tmx.beng.iso8583Gate.pdu.IsoParseException;
import com.tmx.beng.iso8583Gate.pdu.IsoSerializeException;
import com.tmx.beng.iso8583Gate.pdu.IncorrectFormatException;

import java.io.InputStream;
import java.io.OutputStream;

/**
 */
public abstract class Type {
    public String name;

    /** type name */
    public String getName(){
        return name;
    }

    public abstract void parse(InputStream is, int length, RuntimeDataElement rde) throws IsoParseException;

    public abstract SerializedValue serialize(boolean fixedLength, int lengthLimit, RuntimeDataElement rde) throws IsoSerializeException;

    protected abstract byte[] expandFixedLengthValue0(byte[] value, int fixedByteLength) throws IncorrectFormatException;
 
    /** If given value length less then required fixed length then expand this value up to fixed length*/
    protected byte[] expandFixedLengthValue(byte[] value, int fixedByteLength) throws IncorrectFormatException{
        if(value.length > fixedByteLength)
            throw new IncorrectFormatException("err.type.failed_to_expand_value.fixed_length_less_then_value_length", new String[]{String.valueOf(value.length), String.valueOf(fixedByteLength)});

        return expandFixedLengthValue0(value, fixedByteLength);
    }


    /** Handle serialized RuntimeDataElement byte array and size */
    public class SerializedValue {
        /** Serialized data */
        public byte[] data;
        /** Length of serialized data in type related units (not in bytes) */
        public int length;
    }
}
