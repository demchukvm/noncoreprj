package com.tmx.web.taglib.html;

import org.apache.struts.taglib.TagUtils;
import org.w3c.dom.Document;
import javax.servlet.jsp.JspException;
import javax.servlet.http.HttpServletRequest;

import com.tmx.web.base.menuservice.MenuService;
import com.tmx.web.base.menuservice.Menu;
import com.tmx.web.base.xslpublisher.XslPublishService;
import com.tmx.util.XMLUtil;
import com.tmx.util.StructurizedException;

/**
Publish menu
 */
public class MenuTag extends com.tmx.web.taglib.base.LocalizableTag {
    /** The published menu name */
    private String menuName;
    /** The XSL style sheet name to render menu */
    private String styleName;
    /** Optional tag property. Used to explicitly define from page which menu element should
     * be rendered as selected then given page is rendered */
    private String selectedElement;

    //--------------------Properties

    public String getMenuName() {
        return menuName;
    }

    public void setMenuName(String menuName) {
        this.menuName = menuName;
    }

    public String getStyleName() {
        return styleName;
    }

    public void setStyleName(String styleName) {
        this.styleName = styleName;
    }

    public String getSelectedElement() {
        return selectedElement;
    }

    public void setSelectedElement(String selectedElement) {
        this.selectedElement = selectedElement;
    }

    //------------------------------Parsing events
    public int doStartTmxTag() throws JspException {
        String menuHTML = renderMenuHTML();
        TagUtils.getInstance().write(pageContext, menuHTML);
        return (EVAL_BODY_TAG);
    }

    //------------------------------Rendering methods
    private String renderMenuHTML() throws JspException {
        String resultHTML = "";
        MenuService menuService = MenuService.getInstance();
        XslPublishService publihService = XslPublishService.getInstance();
        try{
            //explicitly set selected menu element if defined
            if(getSelectedElement() != null)
                menuService.setSelectedMenuElement(getMenuName(), getSelectedElement(), (HttpServletRequest)pageContext.getRequest());

            //render menu
            Menu menu = menuService.initializeMenu(getMenuName(), (HttpServletRequest)pageContext.getRequest());
            Document renderedMenu = publihService.publishDocument(menu.getDocument(), getStyleName());
            resultHTML = XMLUtil.serializeDOM(renderedMenu);
        }
        catch(StructurizedException e){
            resultHTML = e.getLocalizedMessage(getLocale());
        }
        catch(Throwable e){
            resultHTML = e.getLocalizedMessage();
        }
        return resultHTML;
    }

}
