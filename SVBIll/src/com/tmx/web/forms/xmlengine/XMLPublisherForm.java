package com.tmx.web.forms.xmlengine;


public class XMLPublisherForm extends BasicXMLPublisherForm {
    private Long selectedCourseId = null;

    public Long getSelectedCourseId() {
        return selectedCourseId;
    }

    public void setSelectedCourseId(Long selectedCourseId) {
        this.selectedCourseId = selectedCourseId;
    }

}
