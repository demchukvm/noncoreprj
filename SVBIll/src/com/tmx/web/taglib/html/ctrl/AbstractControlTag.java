package com.tmx.web.taglib.html.ctrl;

import com.tmx.web.base.*;
import com.tmx.web.taglib.base.AbstractSupportTag;
import com.tmx.web.controls.BasicControl;
import com.tmx.web.controls.ValidationRule;
import com.tmx.as.entities.general.ui_permission.UserInterfaceType;
import com.tmx.as.entities.general.ui_permission.PermissionType;
import com.tmx.as.exceptions.*;
import javax.servlet.jsp.JspException;
import javax.servlet.http.HttpServletRequest;
import org.apache.struts.taglib.TagUtils;
import java.util.List;
import org.apache.xerces.impl.dv.util.Base64;

/**
Basic tag to render all controls
 */
public abstract class AbstractControlTag extends AbstractSupportTag {
    protected String width = null;
    protected String height = null;

    private String onClick = null;
    private String onBlur = null;
    private String onMouseOver = null;
    private String onMouseOut = null;
    private String onDbClick = null;
    private String onChange = null;
    private String onFocus = null;
    private String onKeyDown = null;
    private String onKeyPress = null;
    private String onKeyUp = null;
    private String onMouseDown = null;
    private String onMouseMove = null;
    private String onMouseUp = null;

    private String permissioned = "true";

    //...........
    private String styleClass = null; //???????  RM
    private String validationErrorStyleClass = null;
    private String mandatoryFlagStyleClass = null;

    //----------Event Constants
    protected final String ON_CLICK_SUFFIX = "onClick";
    protected final String ON_BLUR_SUFFIX = "onBlur";
    protected final String ON_MOUSE_OVER_SUFFIX = "onMouseOver";
    protected final String ON_MOUSE_OUT_SUFFIX = "onMouseOut";
    protected final String ON_CHANGE_SUFFIX = "onChange";
    protected final String ON_DBCLICK_SUFFIX = "onDbClick";
    protected final String ON_FOCUS_SUFFIX = "onFocus";
    protected final String ON_KEY_DOWN_SUFFIX = "onKeyDown";
    protected final String ON_KEY_PRESS_SUFFIX = "onKeyPress";
    protected final String ON_KEY_UP_SUFFIX = "onKeyUp";
    protected final String ON_MOUSE_DOWN_SUFFIX = "onMouseDown";
    protected final String ON_MOUSE_MOVE_SUFFIX = "onMouseMove";
    protected final String ON_MOUSE_UP_SUFFIX = "onMouseUp";

    //-----------Navigation constants
    protected final String COMMAND = "command";

    //-----------Control reference in the script entry
    protected String CTRL_REF = "ctrl";

    //------------------------------Properties

    public String getWidth() {
        return (width != null) ? width : getDefaultProperty("tag.abstractControl.width");
    }

    public void setWidth(String width) {
        this.width = width;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

   
    public void setOnBlur(String onBlur) {
        this.onBlur = onBlur;
    }

    public String getOnBlur() throws WebException {
        return (onBlur != null) ? preprocessScriptEntry(onBlur) : preprocessScriptEntry(safeGetControl().getOnBlur());
    }

    public String getOnClick() throws WebException {
        return (onClick != null) ? preprocessScriptEntry(onClick) : preprocessScriptEntry(safeGetControl().getOnClick());
    }

    public void setOnClick(String onClick) {
        this.onClick = onClick;
    }

    public String getOnChange() throws WebException {
        return (onChange != null) ? preprocessScriptEntry(onChange) : preprocessScriptEntry(safeGetControl().getOnChange());
    }

    public void setOnChange(String onChange) {
        this.onChange = onChange;
    }

    public String getOnMouseOver() throws WebException{
        return (onMouseOver != null) ? preprocessScriptEntry(onMouseOver) : preprocessScriptEntry(safeGetControl().getOnMouseOver());
    }

    public void setOnMouseOver(String onMouseOver) {
        this.onMouseOver = onMouseOver;
    }

    public String getOnMouseOut() throws WebException{
        return (onMouseOut != null) ? preprocessScriptEntry(onMouseOut) : preprocessScriptEntry(safeGetControl().getOnMouseOut());
    }

    public void setOnMouseOut(String onMouseOut) {
        this.onMouseOut = onMouseOut;
    }

    public String getOnDbClick() throws WebException{
        return (onDbClick != null) ? preprocessScriptEntry(onDbClick) : preprocessScriptEntry(safeGetControl().getOnDbClick());
    }

    public void setOnDbClick(String onDbClick) {
        this.onDbClick = onDbClick;
    }

    public String getOnFocus() throws WebException{
        return (onFocus != null) ? preprocessScriptEntry(onFocus) : preprocessScriptEntry(safeGetControl().getOnFocus());
    }

    public void setOnFocus(String onFocus) {
        this.onFocus = onFocus;
    }

    public String getOnKeyDown() throws WebException{
        return (onKeyDown != null) ? preprocessScriptEntry(onKeyDown) : preprocessScriptEntry(safeGetControl().getOnKeyDown());
    }

    public void setOnKeyDown(String onKeyDown) {
        this.onKeyDown = onKeyDown;
    }

    public String getOnKeyPress() throws WebException{
        return (onKeyPress != null) ? preprocessScriptEntry(onKeyPress) : preprocessScriptEntry(safeGetControl().getOnKeyPress());
    }

    public void setOnKeyPress(String onKeyPress) {
        this.onKeyPress = onKeyPress;
    }

    public String getOnKeyUp() throws WebException{
        return (onKeyUp != null) ? preprocessScriptEntry(onKeyUp) : preprocessScriptEntry(safeGetControl().getOnKeyUp());
    }

    public void setOnKeyUp(String onKeyUp) {
        this.onKeyUp = onKeyUp;
    }

    public String getOnMouseDown() throws WebException{
        return (onMouseDown != null) ? preprocessScriptEntry(onMouseDown) : preprocessScriptEntry(safeGetControl().getOnMouseDown());
    }

    public void setOnMouseDown(String onMouseDown) {
        this.onMouseDown = onMouseDown;
    }

    public String getOnMouseMove() throws WebException{
        return (onMouseMove != null) ? preprocessScriptEntry(onMouseMove) : preprocessScriptEntry(safeGetControl().getOnMouseMove());
    }

    public void setOnMouseMove(String onMouseMove) {
        this.onMouseMove = onMouseMove;
    }

    public String getOnMouseUp() throws WebException{
        return (onMouseUp != null) ? preprocessScriptEntry(onMouseUp) : preprocessScriptEntry(safeGetControl().getOnMouseUp());
    }

    public void setOnMouseUp(String onMouseUp) {
        this.onMouseUp = onMouseUp;
    }
    //-----------------End handlers set from particular tag usage


    /** Preproecess script entries by adding tailing semicolon */
    private String preprocessScriptEntry(String entry){
        if(entry == null)
            return null;
        return entry.endsWith(";") ? (entry+"\r") : (entry + ";\r");
    }

    public String getStyleClass() {
        return styleClass;
    }

    public void setStyleClass(String styleClass) {
        this.styleClass = styleClass;
    }


    public String getValidationErrorStyleClass() {
        return (validationErrorStyleClass != null) ? validationErrorStyleClass : getDefaultProperty("tag.abstractControl.validationErrorStyleClass");
    }

    public void setValidationErrorStyleClass(String validationErrorStyleClass) {
        this.validationErrorStyleClass = validationErrorStyleClass;
    }


    public String getMandatoryFlagStyleClass() {
        return (mandatoryFlagStyleClass != null) ? mandatoryFlagStyleClass : getDefaultProperty("tag.abstractControl.mandatoryFlagStyleClass");
    }

    public void setMandatoryFlagStyleClass(String mandatoryFlagStyleClass) {
        this.mandatoryFlagStyleClass = mandatoryFlagStyleClass;
    }//----------------------------------Protected methods
    protected String renderOnBlurScript(BasicControl ctrl) throws WebException {
        return null;
    }

    protected String renderOnChangeScript(BasicControl ctrl) throws WebException{
        return null;
    }

    protected String renderOnClickScript(BasicControl ctrl) throws WebException{
        return null;
    }

    protected String renderOnDbClickScript(BasicControl ctrl) throws WebException{
        return null;
    }

    protected String renderOnFocusScript(BasicControl ctrl) throws WebException{
        return null;
    }

    protected String renderOnKeyDownScript(BasicControl ctrl) throws WebException{
        return null;
    }

    protected String renderOnKeyPressScript(BasicControl ctrl) throws WebException{
        return null;
    }

    protected String renderOnKeyUpScript(BasicControl ctrl) throws WebException{
        return null;
    }

    protected String renderOnMouseDownScript(BasicControl ctrl) throws WebException{
        return null;
    }

    protected String renderOnMouseMoveScript(BasicControl ctrl) throws WebException{
        return null;
    }

    protected String renderOnMouseOutScript(BasicControl ctrl) throws WebException{
        return null;
    }

    protected String renderOnMouseOverScript(BasicControl ctrl) throws WebException{
        return null;
    }

    protected String renderOnMouseUpScript(BasicControl ctrl) throws WebException{
        return null;
    }


    //wrap methods use to set priority of parent methods over successor's one
    private String renderOnBlurScript0(BasicControl ctrl) throws WebException {
        return (isEventHandlersAllowed(ctrl) && getOnBlur() != null) ? getOnBlur() : renderOnBlurScript(ctrl);
    }

    private String renderOnChangeScript0(BasicControl ctrl) throws WebException{
        return (isEventHandlersAllowed(ctrl) && getOnChange() != null) ? getOnChange() : renderOnChangeScript(ctrl);
    }

    private String renderOnClickScript0(BasicControl ctrl) throws WebException{
        return (isEventHandlersAllowed(ctrl) && getOnClick() != null) ? getOnClick() : renderOnClickScript(ctrl);
    }

    private String renderOnDbClickScript0(BasicControl ctrl) throws WebException{
        return (isEventHandlersAllowed(ctrl) && getOnDbClick() != null) ? getOnDbClick() : renderOnDbClickScript(ctrl);
    }

    private String renderOnFocusScript0(BasicControl ctrl) throws WebException{
        return (isEventHandlersAllowed(ctrl) && getOnFocus() != null) ? getOnFocus() : renderOnFocusScript(ctrl);
    }

    private String renderOnKeyDownScript0(BasicControl ctrl) throws WebException{
        return (isEventHandlersAllowed(ctrl) && getOnKeyDown() != null) ? getOnKeyDown() : renderOnKeyDownScript(ctrl);
    }

    private String renderOnKeyPressScript0(BasicControl ctrl) throws WebException{
        return (isEventHandlersAllowed(ctrl) && getOnKeyPress() != null) ? getOnKeyPress() : renderOnKeyPressScript(ctrl);
    }

    private String renderOnKeyUpScript0(BasicControl ctrl) throws WebException{
        return (isEventHandlersAllowed(ctrl) && getOnKeyUp() != null) ? getOnKeyUp() : renderOnKeyUpScript(ctrl);
    }

    private String renderOnMouseDownScript0(BasicControl ctrl) throws WebException{
        return (isEventHandlersAllowed(ctrl) && getOnMouseDown() != null) ? getOnMouseDown() : renderOnMouseDownScript(ctrl);
    }

    private String renderOnMouseMoveScript0(BasicControl ctrl) throws WebException{
        return (isEventHandlersAllowed(ctrl) && getOnMouseMove() != null) ? getOnMouseMove() : renderOnMouseMoveScript(ctrl);
    }

    private String renderOnMouseOutScript0(BasicControl ctrl) throws WebException{
        return (isEventHandlersAllowed(ctrl) && getOnMouseOut() != null) ? getOnMouseOut() : renderOnMouseOutScript(ctrl);
    }

    private String renderOnMouseOverScript0(BasicControl ctrl) throws WebException{
        return (isEventHandlersAllowed(ctrl) && getOnMouseOver() != null) ? getOnMouseOver() : renderOnMouseOverScript(ctrl);
    }

    private String renderOnMouseUpScript0(BasicControl ctrl) throws WebException{
        return (isEventHandlersAllowed(ctrl) && getOnMouseUp() != null) ? getOnMouseUp() : renderOnMouseUpScript(ctrl);
    }


    //--------------Attributes
    protected String getOnClickAttr(){
         return " onClick=\""+getSeparatorReplacedName(getProperty())+"_"+ON_CLICK_SUFFIX +"(this);\" ";
    }

    protected String getOnMouseOverAttr(){
         return " onMouseOver=\""+getSeparatorReplacedName(getProperty())+"_"+ON_MOUSE_OVER_SUFFIX +"(this);\" ";
    }

    protected String getOnMouseOutAttr(){
         return " onMouseOut=\""+getSeparatorReplacedName(getProperty())+"_"+ON_MOUSE_OUT_SUFFIX +"(this);\" ";
    }

    protected String getOnBlurAttr(){
         return " onBlur=\""+getSeparatorReplacedName(getProperty())+"_"+ON_BLUR_SUFFIX +"(this);\" ";
    }

    protected String getOnDbClickAttr(){
         return " onDbClick=\""+getSeparatorReplacedName(getProperty())+"_"+ON_DBCLICK_SUFFIX +"(this);\" ";
    }

    protected String getOnChangeAttr(){
         return " onChange=\""+getSeparatorReplacedName(getProperty())+"_"+ON_CHANGE_SUFFIX +"(this);\" ";
    }

    protected String getOnFocusAttr(){
         return " onFocus=\""+getSeparatorReplacedName(getProperty())+"_"+ON_FOCUS_SUFFIX +"(this);\" ";
    }

    protected String getOnKeyDownAttr(){
         return " onKeyDown=\""+getSeparatorReplacedName(getProperty())+"_"+ON_KEY_DOWN_SUFFIX +"(this);\" ";
    }

    protected String getOnKeyPressAttr(){
         return " onKeyPress=\""+getSeparatorReplacedName(getProperty())+"_"+ON_KEY_PRESS_SUFFIX +"(this);\" ";
    }

    protected String getOnKeyUpAttr(){
         return " onKeyUp=\""+getSeparatorReplacedName(getProperty())+"_"+ON_KEY_UP_SUFFIX +"(this);\" ";
    }

    protected String getMouseDownAttr(){
         return " onMouseDown=\""+getSeparatorReplacedName(getProperty())+"_"+ON_MOUSE_DOWN_SUFFIX +"(this);\" ";
    }

    protected String getMouseMoveAttr(){
         return " onMouseMove=\""+getSeparatorReplacedName(getProperty())+"_"+ON_MOUSE_MOVE_SUFFIX +"(this);\" ";
    }

    protected String getMouseUpAttr(){
         return " onMouseUp=\""+getSeparatorReplacedName(getProperty())+"_"+ON_MOUSE_UP_SUFFIX +"(this);\" ";
    }


    /** Creates string onEvent attrs. If corresponding onEventHandler is null then onEvent attribute will not
     * appended to this string.
     *  */
    protected String getOnEventAttrsString(){
        String onAttrs = "";
        BasicControl ctrl = null;
        try{
            ctrl = getControl();
            onAttrs += (renderOnClickScript0(ctrl) != null) ? getOnClickAttr() : "";
            onAttrs += (renderOnMouseOverScript0(ctrl) != null) ? getOnMouseOverAttr() : "";
            onAttrs += (renderOnMouseOutScript0(ctrl) != null) ? getOnMouseOutAttr() : "";
            onAttrs += (renderOnBlurScript0(ctrl) != null) ? getOnBlurAttr() : "";
            onAttrs += (renderOnDbClickScript0(ctrl) != null) ? getOnDbClickAttr() : "";
            onAttrs += (renderOnChangeScript0(ctrl) != null) ? getOnChangeAttr() : "";
            onAttrs += (renderOnFocusScript0(ctrl) != null) ? getOnFocusAttr() : "";
            onAttrs += (renderOnKeyDownScript0(ctrl) != null) ? getOnKeyDownAttr() : "";
            onAttrs += (renderOnKeyPressScript0(ctrl) != null) ? getOnKeyPressAttr() : "";
            onAttrs += (renderOnKeyUpScript0(ctrl) != null) ? getOnKeyUpAttr() : "";
            onAttrs += (renderOnMouseDownScript0(ctrl) != null) ? getMouseDownAttr() : "";
            onAttrs += (renderOnMouseMoveScript0(ctrl) != null) ? getMouseMoveAttr() : "";
            onAttrs += (renderOnMouseUpScript0(ctrl) != null) ? getMouseUpAttr() : "";

            //etc...
        }
        catch(Throwable e){
            return "RENDER EXCEPTION : " + e.toString();
        }
        return onAttrs;
    }


    //------------------------------Parsing events
//    public int doStartTmxTag() throws JspException {
//        BasicControl ctrl = getControl();
//        String permission = getPermission(ctrl);
//        String renderedHtml;
//        if(PermissionType.PERMITTED.equals(permission))
//            renderedHtml = renderControl(ctrl);
//        else if(PermissionType.READONLY.equals(permission)){
//            ctrl.setReadonly(true);
//            renderedHtml = renderControl(ctrl);
//        }
//        else {
//            renderedHtml = renderForbiddenMessage();
//        }
//        TagUtils.getInstance().write(pageContext, renderedHtml);
//        return EVAL_BODY_TAG;
//    }

    public int doStartTmxTag() throws JspException {
        BasicControl ctrl = getControl();
        TagUtils.getInstance().write(pageContext, "<!--permission name: "+ctrl.getFullPath()+" -->\r\n");// write pervission name
        String permission = getPermission(ctrl);
        if(permission.equals(PermissionType.READONLY))ctrl.setReadonly(true);// set readonly to control
        if(permission.equals(PermissionType.PERMITTED)||permission.equals(PermissionType.READONLY)){
            return controlStartTag();
        } else {
            TagUtils.getInstance().write(pageContext, renderForbiddenMessage());
            return SKIP_BODY;
        }
    }

    public int doEndTmxTag() throws JspException {
        BasicControl ctrl = getControl();
        String permission = getPermission(ctrl);
        if(permission.equals(PermissionType.PERMITTED)||permission.equals(PermissionType.READONLY)){
            int index = controlEndTag();
            TagUtils.getInstance().write(pageContext, renderControl(ctrl));
            TagUtils.getInstance().write(pageContext, getPermissionSetupRenderer());
            return index;
        }else
            return EVAL_PAGE;
    }

    protected abstract int controlStartTag() throws JspException;

    protected int controlEndTag() throws JspException{
        return EVAL_PAGE;
    }
    
    private String getPermissionSetupRenderer() throws JspException {
        PermissionControlTagInterface permissionControlTagInterface=(PermissionControlTagInterface)((HttpServletRequest)pageContext.getRequest()).getSession().getAttribute("core.uipermsetup.toolBarPermissionForm");
        if(Boolean.valueOf(getPermissioned()).booleanValue()&&permissionControlTagInterface!=null&&permissionControlTagInterface.isActive()){
            BasicControl ctrl = getControl();
            String ctrlName=ctrl.getFullPath();
            StringBuffer pControl= new StringBuffer("<select class=\"permissionControlSelectBox dropdownbox\" name=\"applyPermissionsFormControl(")
                    .append(Base64.encode(ctrlName.getBytes())).append(")\">\n");
            List permissionTypes=permissionControlTagInterface.getPermissionTypes();
            Long selected=permissionControlTagInterface.getFormControlPermission(ctrlName);
            for (int i = 0; i < permissionTypes.size(); i++) {
                Object[] bean=(Object[])permissionTypes.get(i);
                pControl.append("<option value=\"").append(bean[0]).append("\"")
                        .append(selected.equals(bean[0]) ? "selected" : "")
                        .append(">")
                        .append(bean[1])
                        .append("</option>\n");
            }
            pControl.append("</select>");
            return pControl.toString();
        } else return "";
    }

    private String renderControl(BasicControl ctrl) throws JspException {
        StringBuffer resultHTML = new StringBuffer();
        resultHTML.append(renderMandatoryFlagHTML(ctrl));
        resultHTML.append(renderValidationErrors(ctrl));
        resultHTML.append(renderSupportScript(ctrl));
        return resultHTML.toString();
    }

    private String renderForbiddenMessage() throws JspException {
        StringBuffer resultHTML = new StringBuffer();
        resultHTML.append("<div class=\"forbidden_ctrl\">").
                   append(getLocalizedLabel("tag.abstractControl.forbidden", null)).
                   append("</div>");
        return resultHTML.toString();
    }

    private String getPermission(BasicControl ctrl) {
        String permission = PermissionType.FORBIDDEN;
        String roleName = ((SecurityService) ApplicationEnvironment.getService(SecurityService.class)).getLoggedInUserRoleName(((HttpServletRequest)pageContext.getRequest()).getSession());
        com.tmx.as.base.SecurityService securityService = com.tmx.as.base.SecurityService.getInstance();
        try{
            permission = securityService.getPermissionType(roleName, UserInterfaceType.FORM_CONTROL, ctrl.getFullPath());
        }
        catch(DatabaseException e){
            /** Ignore with FORBIDDEN permission */
        }
        catch(com.tmx.as.exceptions.SecurityException e){
            /** Ignore with FORBIDDEN permission */
        }
        return permission;
    }


    protected TagHtmlRenderingHelper getTagRenderingHelper() throws JspException {
        //get form
        BasicActionForm form = PageEnvironment.getCurrentForm(pageContext);
        return (TagHtmlRenderingHelper)form.getTagRenderingHelper();
    }

    //-----------------------------Rendering methods

    private String renderSupportScript(BasicControl ctrl) {
        StringBuffer resultHTML = new StringBuffer();
        try{
            String scriptEntry = null;
            resultHTML.append("    <script type=\"text/javascript\">");

            scriptEntry = renderOnBlurScript0(ctrl);
            if(scriptEntry != null){
                resultHTML.append("\r");
                resultHTML.append("     function "+getSeparatorReplacedName(getProperty())+"_"+ON_BLUR_SUFFIX +"("+CTRL_REF+"){\r");
                resultHTML.append(scriptEntry);
                resultHTML.append("     }\r");
                resultHTML.append("\r");
            }

            scriptEntry = renderOnChangeScript0(ctrl);
            if(scriptEntry != null){
                resultHTML.append("\r");
                resultHTML.append("     function "+getSeparatorReplacedName(getProperty())+"_"+ON_CHANGE_SUFFIX +"("+CTRL_REF+"){\r");
                resultHTML.append(scriptEntry);
                resultHTML.append("     }\r");
                resultHTML.append("\r");
            }

            scriptEntry = renderOnClickScript0(ctrl);
            if(scriptEntry != null){
                resultHTML.append("\r");
                resultHTML.append("     function "+getSeparatorReplacedName(getProperty())+"_"+ON_CLICK_SUFFIX +"("+CTRL_REF+"){\r");
                resultHTML.append(scriptEntry);
                resultHTML.append("     }\r");
                resultHTML.append("\r");
            }

            scriptEntry = renderOnDbClickScript0(ctrl);
            if(scriptEntry != null){
                resultHTML.append("\r");
                resultHTML.append("     function "+getSeparatorReplacedName(getProperty())+"_"+ON_DBCLICK_SUFFIX +"("+CTRL_REF+"){\r");
                resultHTML.append(scriptEntry);
                resultHTML.append("     }\r");
                resultHTML.append("\r");
            }


            scriptEntry = renderOnFocusScript0(ctrl);
            if(scriptEntry != null){
                resultHTML.append("\r");
                resultHTML.append("     function "+getSeparatorReplacedName(getProperty())+"_"+ON_FOCUS_SUFFIX +"("+CTRL_REF+"){\r");
                resultHTML.append(scriptEntry);
                resultHTML.append("     }\r");
                resultHTML.append("\r");
            }

            scriptEntry = renderOnKeyDownScript0(ctrl);
            if(scriptEntry != null){
                resultHTML.append("\r");
                resultHTML.append("     function "+getSeparatorReplacedName(getProperty())+"_"+ON_KEY_DOWN_SUFFIX +"("+CTRL_REF+"){\r");
                resultHTML.append(scriptEntry);
                resultHTML.append("     }\r");
                resultHTML.append("\r");
            }

            scriptEntry = renderOnKeyPressScript0(ctrl);
            if(scriptEntry != null){
                resultHTML.append("\r");
                resultHTML.append("     function "+getSeparatorReplacedName(getProperty())+"_"+ON_KEY_PRESS_SUFFIX +"("+CTRL_REF+"){\r");
                resultHTML.append(scriptEntry);
                resultHTML.append("     }\r");
                resultHTML.append("\r");
            }

            scriptEntry = renderOnKeyUpScript0(ctrl);
            if(scriptEntry != null){
                resultHTML.append("\r");
                resultHTML.append("     function "+getSeparatorReplacedName(getProperty())+"_"+ON_KEY_UP_SUFFIX +"("+CTRL_REF+"){\r");
                resultHTML.append(scriptEntry);
                resultHTML.append("     }\r");
                resultHTML.append("\r");
            }

            scriptEntry = renderOnMouseDownScript0(ctrl);
            if(scriptEntry != null){
                resultHTML.append("\r");
                resultHTML.append("     function "+getSeparatorReplacedName(getProperty())+"_"+ON_MOUSE_DOWN_SUFFIX +"("+CTRL_REF+"){\r");
                resultHTML.append(scriptEntry);
                resultHTML.append("     }\r");
                resultHTML.append("\r");
            }

            scriptEntry = renderOnMouseMoveScript0(ctrl);
            if(scriptEntry != null){
                resultHTML.append("\r");
                resultHTML.append("     function "+getSeparatorReplacedName(getProperty())+"_"+ON_MOUSE_MOVE_SUFFIX +"("+CTRL_REF+"){\r");
                resultHTML.append(scriptEntry);
                resultHTML.append("     }\r");
                resultHTML.append("\r");
            }

            scriptEntry = renderOnMouseOutScript0(ctrl);
            if(scriptEntry != null){
                resultHTML.append("\r");
                resultHTML.append("     function "+getSeparatorReplacedName(getProperty())+"_"+ON_MOUSE_OUT_SUFFIX +"("+CTRL_REF+"){\r");
                resultHTML.append(scriptEntry);
                resultHTML.append("     }\r");
                resultHTML.append("\r");
            }

            scriptEntry = renderOnMouseOverScript0(ctrl);
            if(scriptEntry != null){
                resultHTML.append("\r");
                resultHTML.append("     function "+getSeparatorReplacedName(getProperty())+"_"+ON_MOUSE_OVER_SUFFIX +"("+CTRL_REF+"){\r");
                resultHTML.append(scriptEntry);
                resultHTML.append("     }\r");
                resultHTML.append("\r");
            }

            scriptEntry = renderOnMouseUpScript0(ctrl);
            if(scriptEntry != null){
                resultHTML.append("\r");
                resultHTML.append("     function "+getSeparatorReplacedName(getProperty())+"_"+ON_MOUSE_UP_SUFFIX +"("+CTRL_REF+"){\r");
                resultHTML.append(scriptEntry);
                resultHTML.append("     }\r");
                resultHTML.append("\r");
            }
            resultHTML.append("    </script>\r");

        }
        catch(WebException e){
            return "Failed to render control's support script: "+e.toString();
        }

        return resultHTML.toString();
    }

    protected BasicControl getControl() throws JspException{
        Object object = getPropertyObject();
        return (object != null && object instanceof BasicControl) ?
               (BasicControl)object : null;
    }

    private BasicControl safeGetControl() throws WebException{
        try{
            return getControl();
        }
        catch(JspException e){
            throw new WebException("err.failed_to_get_control", e);
        }
    }

    /** Use in the successors to identify should event handlers be rendered or no */
    protected boolean isEventHandlersAllowed(BasicControl ctrl) {
        return ctrl != null && !ctrl.isReadonly();
    }

    protected Roadmap getRoadmap(){
        return SessionEnvironment.allocateRoadmap(pageContext.getSession());
    }

    //------------------------------Abstract methods
    //protected abstract String renderControlHTML() throws JspException;


    //------------------------------Validation rendering
    private String renderValidationErrors(BasicControl ctrl){
        StringBuffer resultHTML = new StringBuffer();
        //Render error messages on validation fail
        if(ctrl != null && !ctrl.isValidationPassed()){
            List validationRules = ctrl.getValidationRules();
            for(int i=0; i<validationRules.size(); i++){
                ValidationRule rule = ((ValidationRule)validationRules.get(i));
                if(!rule.isValidationPassed()){
                    resultHTML.append("<div class='"+getValidationErrorStyleClass()+"'>");
                    resultHTML.append(rule.publishErrorMessage(getLocale()));
                    resultHTML.append("</div>");
                }
            }
        }
        return resultHTML.toString();
    }


    //---------------------------Render mandatory flag
    private String renderMandatoryFlagHTML(BasicControl ctrl){
        StringBuffer resultHTML = new StringBuffer();
        if(ctrl != null && ctrl.isMandatory()){
            resultHTML.append("<span class='"+getMandatoryFlagStyleClass()+"'>");
            resultHTML.append("*");
            resultHTML.append("</span>");
        }
        return resultHTML.toString();
    }

    protected String renderCssStyleAttribute(){
        //extend this method to support another style elements
        String width = getWidth() != null ? " width : " + getWidth() + ";" : null;
        String style = "";
        if(width != null){
            style = " style=\"" + width + "\" ";
        }
        return style;
    }

    public String getPermissioned() {
        return permissioned;
    }

    public void setPermissioned(String permissioned) {
        this.permissioned = permissioned;
    }
}
