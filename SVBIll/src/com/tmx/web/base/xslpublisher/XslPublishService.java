package com.tmx.web.base.xslpublisher;

import com.tmx.as.base.Reconfigurable;
import com.tmx.util.InitException;
import com.tmx.util.Configuration;
import com.tmx.util.XMLUtil;
import com.tmx.util.cache.FileCache;
import com.tmx.util.cache.CacheException;
import org.w3c.dom.*;
import org.apache.log4j.Logger;
import org.apache.commons.digester.Digester;
import javax.xml.transform.*;
import javax.xml.transform.stream.*;
import javax.xml.transform.dom.*;
import java.io.InputStream;
import java.io.File;
import java.io.IOException;


/**
 */
public class XslPublishService implements Reconfigurable {
    /** Only single instance available */
    private static XslPublishService publisherService;
    private XslPublishService.Config serviceConfig = null;
    public static String LOGCLASS_PUBLISHER = "web";
    /** Logger */
    private static Logger logger = Logger.getLogger(LOGCLASS_PUBLISHER + ".XslPublishService");
    private StyleTemplateCache cache = new StyleTemplateCache();

    /** to hide default constructor */
    private XslPublishService(){
    }

    /** Return gate context instance */
    public static XslPublishService getInstance(){
        if(publisherService == null)
            publisherService = new XslPublishService();

        return publisherService;
    }

    public void reload() throws InitException {
        try {
            logger.debug("Start initialization of the XslPublishService");
            String menuServiceConfigFile = Configuration.getInstance().getProperty("xslpublish_service.config_file");
            if (menuServiceConfigFile == null)
                throw new InitException("err.xslpublish_service.config_file_is_not_specified");

            File configFile = new File(menuServiceConfigFile);
            Digester digester = new Digester();
            XslPublishService.Config localConfig = new XslPublishService.Config();
            digester.push(localConfig);
            addRules(digester);
            digester.parse(configFile);
            serviceConfig = (XslPublishService.Config)digester.getRoot();
            logger.debug("Successful completed initialization of the Xsl Publish service");
        }
        catch (InitException e) {
            //rethrow
            throw e;
        }
        catch (Exception e) {
            throw new InitException("err.xslpublish_service.reload_failed", e);
        }
    }


    public Document publishDocument(Document doc, String styleName) throws PublishException{
        styleName = (styleName.endsWith(".xsl")) ? styleName.substring(0, styleName.length() - ".xsl".length()) : styleName;
        styleName = styleName.replace('.', File.separatorChar);
        String stylePath = serviceConfig.getRepository() + File.separator + styleName + ".xsl";
        return publishDocument0(doc, stylePath);
    }


    //--------private
    private Document publishDocument0(Document doc, String realStyleFilePath) throws PublishException{
        Transformer transformer = null;
        try{
            transformer = (Transformer)cache.get(realStyleFilePath);
        }
        catch(CacheException e){
            throw new PublishException("err.xslpublish_service.failed_to_get_xsl_style", new String[]{realStyleFilePath}, e);
        }

//DEBUG
//try{
//    System.out.println(XMLUtil.serializeDOM(doc));
//}
//catch(IOException e){
//    System.out.print("DEBUG ERROR:"+e.toString());
//}
//DEBUG
        long l = System.currentTimeMillis();
        DOMResult result = null;
        try {
            result = new DOMResult();
            Source source = new DOMSource(doc);
            transformer.transform(source, result);

        }
        catch (TransformerException e) {
            throw new PublishException("err.xslpublish_service.transformation_exception", new String[]{realStyleFilePath}, e);
        }
        finally {
            logger.debug("Processed with: "+realStyleFilePath+"; time="+(System.currentTimeMillis() - l));
        }
        return (Document)result.getNode();
    }

//    class StyleResolver implements URIResolver {
//
//        /**
//         * Called by the processor when it encounters
//         * an xsl:include, xsl:import, or document() function.
//         * hrefs started with "dict/{name}.xsl" prefix are specially treated -
//         * according to users's locale, file "dict/{name}_{user locale}.xsl" will
//         * be returned.
//         * If no desired dictionary would be found, "dict/{name}.xsl" will be get.
//         *
//         * @param href An href attribute, which may be relative or absolute.
//         * @param base The base URI in effect when the href attribute
//         * was encountered.
//         *
//         * @return A Source object, or null if the href cannot be resolved,
//         * and the processor should try to resolve the URI itself.
//         *
//         * @throws TransformerException if an error occurs when trying to
//         * resolve the URI.
//         */
//        public Source resolve(String href, String base) throws TransformerException {
//            InputStream is = null;
//
//            if (userLocale != null && (href.startsWith("dict/"))) {
//                int i = href.indexOf(".");
//                String newHref = href;
//                if (i > 0) {
//                    newHref = href.substring(0, i) + "_" + userLocale.getLanguage().toLowerCase();
//                    if (i+1 < href.length())
//                        newHref += "." + href.substring(i+1, href.length());
//                }
//                try {
//                    is = load(newHref);
//                } catch (Exception e) {
//                    ; // suppress
//                }
//            }else{
//                if (userLocale != null && href.indexOf("/dict/") > 0){
//                    int i = href.lastIndexOf(".");
//                    String newHref = href;
//                    if (i > 0) {
//                        newHref = href.substring(0, i) + "_" + userLocale.getLanguage().toLowerCase();
//                        if (i+1 < href.length())
//                            newHref += "." + href.substring(i+1, href.length());
//                    }
//                    try {
//                        is = load(newHref);
//                    } catch (Exception e) {
//                        ; // suppress
//                    }
//                }
//            }
//            if (is == null)
//                is = load(href);
//
//            return (is == null)? null : new StreamSource(is);
//        }
//
//        private InputStream load(String href) {
//            href = ((traxDir.length() > 0)? traxDir : "style://") + href;
//            InputStream is = services.getResourceManager().getResourceAsStream(href);
//            return is;
//        }
//    }


    //---------------private methods
    private void addRules(Digester d){
        d.addSetProperties("xslPublisherService");
    }

    public class Config{
        private String repository = null;

        //--------properties

        public String getRepository() {
            return repository;
        }

        public void setRepository(String repository) throws Exception{
            this.repository = Configuration.getInstance().substituteVariablesInString(repository);
        }
    }

    public class LnFConfig{

        LnFConfig(){
        }

    }

    private class StyleTemplateCache extends FileCache {

        private StyleTemplateCache() {
            maxSize = 100;
            cleanCount = 10;
            cleanType = CleanAlgorithm.Type.CLEAN_N_SELDOM_ACCESSED;
        }

        protected Object getRealObjectFromFile(InputStream is, String templateFilePath) throws CacheException {
            URIResolver resolver;
            Transformer transformer = null;
            TransformerFactory transFactory = TransformerFactory.newInstance();
            if (is == null)
                throw new CacheException("err.xslpublish_service.failed_to_get_style", new String[]{templateFilePath});
            try{
                transformer = transFactory.newTransformer(new StreamSource(is));
            }
            catch(TransformerConfigurationException e){
                throw new CacheException("err.xslpublish_service.failed_to_configure_transformer", new String[]{templateFilePath});
            }
//            transformer.setURIResolver(resolver);
            return transformer;
        }
    }

}
