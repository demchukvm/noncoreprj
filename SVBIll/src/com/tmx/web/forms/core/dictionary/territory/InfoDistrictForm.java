package com.tmx.web.forms.core.dictionary.territory;

import com.tmx.web.base.BasicActionForm;
import com.tmx.web.controls.EditBox;
import com.tmx.web.controls.ScrollBox;
import com.tmx.web.controls.Button;
import com.tmx.web.controls.PopulateUtil;
import com.tmx.as.entities.network.territory.Country;
import com.tmx.as.entities.network.territory.District;
import com.tmx.as.blogic.AddressManager;
import com.tmx.as.exceptions.DatabaseException;

public class InfoDistrictForm extends BasicActionForm {
    private Long selectedId = null;
    private EditBox editBoxDistrictId = new EditBox();
    private EditBox editBoxShortName = new EditBox();
    private EditBox editBoxName = new EditBox();
    private ScrollBox scrollBoxCountry = new ScrollBox(){
        protected String getKey(Object dataListElement) {
            return ((Country)dataListElement).getId().toString();
        }
        protected String getValue(Object dataListElement) {
            return ((Country)dataListElement).getName();
        }
    };

    private Button onSaveButton = new Button();
    private Button onResetButton = new Button();
    private Button onCancelButton = new Button();


    protected void init() {
            editBoxDistrictId.setReadonly(true);
            editBoxShortName.setMandatory(true);
            editBoxName.setMandatory(true);
            //set up buttons
            onSaveButton = new Button();
            onSaveButton.setLabelKey("ctrl.button.label.save");
            onSaveButton.setAction("/core/dictionary/territory/infoDistrict");
            onSaveButton.setCommand("update");

            onResetButton = new Button();
            onResetButton.setLabelKey("ctrl.button.label.reset");

            onCancelButton = new Button();
            onCancelButton.setLabelKey("ctrl.button.label.cancel");
            onCancelButton.setAction("/core/dictionary/territory/infoDistrict");
        }


        public void bindData(District district) {
            if (district.getId() != null) {
                editBoxDistrictId.setValue(district.getId().toString());
                editBoxShortName.setValue(district.getShortName());
                editBoxName.setValue(district.getName());
                scrollBoxCountry.setSelectedKey(district.getCountry().getId().toString());
            }
        }

        public District populateFromControls(District district) throws DatabaseException {
            district.setDistrictId(PopulateUtil.getLongValue(editBoxDistrictId));
            district.setShortName(editBoxShortName.getValue());
            Country country = new AddressManager().getCountry(
                                            new Long(scrollBoxCountry.getSelectedKey()));
            district.setCountry(country);
            district.setName(editBoxName.getValue());

            return district;
        }

//   getters - setters
    public Long getSelectedId() {
        return selectedId;
    }

    public void setSelectedId(Long selectedId) {
        this.selectedId = selectedId;
    }

    public EditBox getEditBoxDistrictId() {
        return editBoxDistrictId;
    }

    public void setEditBoxDistrictId(EditBox editBoxDistrictId) {
        this.editBoxDistrictId = editBoxDistrictId;
    }

    public EditBox getEditBoxShortName() {
        return editBoxShortName;
    }

    public void setEditBoxShortName(EditBox editBoxShortName) {
        this.editBoxShortName = editBoxShortName;
    }

    public EditBox getEditBoxName() {
        return editBoxName;
    }

    public void setEditBoxName(EditBox editBoxName) {
        this.editBoxName = editBoxName;
    }

    public ScrollBox getScrollBoxCountry() {
        return scrollBoxCountry;
    }

    public void setScrollBoxCountry(ScrollBox scrollBoxCountry) {
        this.scrollBoxCountry = scrollBoxCountry;
    }

    public Button getOnSaveButton() {
        return onSaveButton;
    }

    public void setOnSaveButton(Button onSaveButton) {
        this.onSaveButton = onSaveButton;
    }

    public Button getOnResetButton() {
        return onResetButton;
    }

    public void setOnResetButton(Button onResetButton) {
        this.onResetButton = onResetButton;
    }

    public Button getOnCancelButton() {
        return onCancelButton;
    }

    public void setOnCancelButton(Button onCancelButton) {
        this.onCancelButton = onCancelButton;
    }
}

