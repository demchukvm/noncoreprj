package com.tmx.beng.httpsgate.dacard.pdu;

import com.tmx.util.HexConverter;

import javax.crypto.spec.DESKeySpec;
import javax.crypto.SecretKeyFactory;
import javax.crypto.SecretKey;
import javax.crypto.Cipher;
import java.io.OutputStream;
import java.io.InputStream;
import java.io.IOException;
import java.io.ByteArrayOutputStream;
import java.security.spec.KeySpec;
import java.util.Arrays;

/**
 This class is used to calculate, validate, serialize and parse MAC sign for Dacard message. 
 */
public class MacSign {
    private final static int MAC_SIZE = 8;
    private byte[] sign = new byte[MAC_SIZE];
    private byte[] sessionKey;


    MacSign(byte[] sessionKey){
        this.sessionKey = sessionKey;
    }

    /** Calculates MAC sign for given byte array data.
     * Updates the property <code>sign</code> on with the calculated MAC bytes.
     * @param data data byte array.
     * @throws DacardCypherException on calculation error. */
    void calculateMac(byte[] data) throws DacardCypherException{
        sign = evaluateMac(data, sessionKey);
    }

    /** Validates if MAC corresponds the given data array.
     * @param data given source data;
     * @throws DacardCypherException if validation failed. */
    void validateMac(byte[] data) throws DacardCypherException{
        byte[] evaluatedMac = evaluateMac(data, sessionKey);
        if(!Arrays.equals(evaluatedMac, sign))
            throw new DacardCypherException("MACs don't matched", new String[]{new HexConverter().getStringFromHex(evaluatedMac), new HexConverter().getStringFromHex(sign)});   
    }

    /** Returns MAC size in bytes.
     * @return MAC sign size */
    int getSize(){
        return sign.length;
    }

    /** Serialize MAC to given output stream.
     * @param os output stream to write to.
     * @throws DacardPadException on serialization exception. */
    void serialize(OutputStream os) throws DacardPadException{
        try{
            os.write(sign);
        }
        catch (IOException e){
            throw new DacardPadException("Failed to serialize MAC sign", e);
        }
    }

    /** Read and parse MAC from transaction input stream.
     * Returns the amount of bytes read.
     * @param is input source to get data from.
     * @return amount of read bytes.
     * @throws DacardPadException on parsing error. */
    int parse(InputStream is) throws DacardPadException{
        byte[] buffer = new byte[MAC_SIZE];
        try{
            is.read(buffer);
        }
        catch (IOException e){
            throw new DacardPadException("Failed to parse MAC sign", e);
        }
        sign = buffer;
        //..
        return MAC_SIZE;
    }

    private byte[] evaluateMac(byte[] data, byte[] rawKey) throws DacardCypherException{
        try{
            final KeySpec keyspec = new DESKeySpec(rawKey);
            final SecretKeyFactory keyfactory = SecretKeyFactory.getInstance("DES");
            final SecretKey secretKey = keyfactory.generateSecret(keyspec);
            final Cipher cipher = Cipher.getInstance("DES/ECB/NoPadding");
            final int BLOCK_SIZE = MAC_SIZE;
            cipher.init(Cipher.ENCRYPT_MODE, secretKey);

            //provide padding by 0x00 up to the end of last encoded block. Block length is 8 bytes.
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            baos.write(data);
            final int paddingBlockLength = (data.length % BLOCK_SIZE == 0) ? BLOCK_SIZE /** last block completelly should be 0 */ : BLOCK_SIZE - data.length % BLOCK_SIZE;
            byte[] paddingBlock = new byte[paddingBlockLength];
            Arrays.fill(paddingBlock, (byte)0);
            baos.write(paddingBlock);
            // do xor of 8-bytes blocks
            final byte[] rawData = baos.toByteArray();
            final byte[] accum = new byte[BLOCK_SIZE];
            Arrays.fill(accum, (byte)0);
            for(int blockId = 0; blockId < rawData.length / BLOCK_SIZE; blockId++){
                for(int i=0; i<BLOCK_SIZE; i++)
                    accum[i] = (byte)(accum[i] ^ rawData[blockId*BLOCK_SIZE + i]);
            }
            return cipher.doFinal(accum);
        }
        catch(Exception e){
            throw new DacardCypherException("Failed to evaluate MAC", e);
        }
    }
}
