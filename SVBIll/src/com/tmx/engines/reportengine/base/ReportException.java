package com.tmx.engines.reportengine.base;

import com.tmx.util.StructurizedException;

import java.util.Locale;

/**
 */
public class ReportException extends StructurizedException {

    public ReportException(String errKey){
        super(errKey);
    }

    public ReportException(String errKey, Locale locale){
        super(errKey, locale);
    }

    public ReportException(String errKey, String[] params){
        super(errKey, params);
    }

    public ReportException(String errKey, String[] params, Throwable cause){
        super(errKey, params, cause);
    }

    public ReportException(String errKey, String[] params, Locale locale){
        super(errKey, params, locale);
    }

    public ReportException(String errKey, Throwable cause){
        super(errKey, cause);
    }
}
