package com.tmx.as.blogic;

import com.tmx.as.base.EntityManager;
import com.tmx.as.base.OrderWrapper;
import com.tmx.as.entities.bill.processing.Processing;
import com.tmx.as.exceptions.DatabaseException;

import java.util.List;


public class ProcessingManager extends EntityManager {

    public List<Processing> getAllProcessings() throws DatabaseException {
        return RETRIEVE_ALL(Processing.class, null, new OrderWrapper[]{new OrderWrapper("name", OrderWrapper.ASC)});
    }

    public Processing getProcessing(Long processingId) throws DatabaseException {
        return (Processing) RETRIEVE( Processing.class, processingId, null );
    }

    public Processing getProcessing(String id) throws DatabaseException {
        Long processingId = Long.valueOf( id );
        return getProcessing( processingId );
    }

    //DELETE------------------------------------------------------------
    public void deleteProcessing(Long processingId) throws DatabaseException {
        DELETE(Processing.class, processingId );
    }

    public void deleteProcessing(String id) throws DatabaseException {
        Long processingId = Long.valueOf( id );
        deleteProcessing( processingId );
    }

    public void deleteProcessing(Processing processing) throws DatabaseException {
        deleteProcessing( processing.getProcessingId() );
    }

    //SAVE-----------------------------------------------------------
    public void saveProcessing(Processing processing) throws DatabaseException {
        SAVE(processing);
    }


}
