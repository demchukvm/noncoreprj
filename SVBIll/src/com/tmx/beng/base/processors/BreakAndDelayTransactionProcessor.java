package com.tmx.beng.base.processors;

import com.tmx.beng.base.Task;
import com.tmx.beng.base.BillingMessageWritable;
import com.tmx.beng.base.BillException;
import com.tmx.beng.base.BillingMessage;
import com.tmx.beng.base.cache.DBCache;
import com.tmx.as.entities.bill.transaction.DelayedTransactionPart;
import com.tmx.as.entities.bill.operator.Operator;

import java.util.Date;

/**
    According to business purposes to obtain more gain
    <code>ClientTransaction</code> could be breaked to the set of
    delayed transactions with a smaller amount. But the sum of
    amounts of these transaction is equal to orignial breaked transaction. 
 */
public class BreakAndDelayTransactionProcessor extends TaskProcessor{

    protected Task process0(Task task, DBCache dbCache, BillingMessageWritable msg) throws BillException {
        logger.debug("Break and delay transaction processor...");
        //Now supports only 'refill_payment' operations
        if(BillingMessage.O_REFILL_PAYMENT.equals(msg.getOperationName())){

        }
        return task;
    }

    /** prepare delayed transacton in format as incoming client transaction has  */
/*    protected DelayedTransactionPart populateDelayedTransactionPart(DelayedTransactionPart tx, BillingMessage msg){
        msg.getAttributeString(BillingMessage.TERMINAL_SN);
        msg.getAttributeString(BillingMessage.SELLER_CODE);
        msg.getAttributeString(BillingMessage.PROCESSING_CODE);
        msg.getAttributeString(BillingMessage.LOGIN);
        msg.getAttributeString(msg.getAttributeString(BillingMessage.PASSWORD);


        tx.setAccountNumber(msg.getAttributeString(BillingMessage.ACCOUNT_NUMBER));
        tx.setClientTime((Date)msg.getAttribute(BillingMessage.CLIENT_TIME));
        tx.setOperator((Operator)msg.getAttribute(BillingMessage.SERVICE_CODE));
    }*/
}
