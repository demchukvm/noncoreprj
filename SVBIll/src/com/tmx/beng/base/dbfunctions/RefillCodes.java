package com.tmx.beng.base.dbfunctions;

import java.util.Map;
import java.util.HashMap;

public class RefillCodes {

    public final String KYIVSTYAR_DISCOUNT = "Kyivstar discount";
    public final String MTS_DISCOUNT = "MTS discount";
    public final String BEELINE_DISCOUNT = "Beeline discount";

    /** Service code to tariff parameter name mapping  */
    private static Map serviceCodeParamMapping;

    {
        serviceCodeParamMapping = new HashMap();
        serviceCodeParamMapping.put("KyivstarPoP", KYIVSTYAR_DISCOUNT);
        serviceCodeParamMapping.put("ACE_BASE_DJuice", KYIVSTYAR_DISCOUNT);
        serviceCodeParamMapping.put("UMCPoP", MTS_DISCOUNT);
        serviceCodeParamMapping.put("UMC", MTS_DISCOUNT);
        serviceCodeParamMapping.put("Beeline", BEELINE_DISCOUNT);
    }

    public Map getServiceCodeParamMapping() {
        return serviceCodeParamMapping;
    }

}
