package com.tmx.web.actions.core.csapi;

import com.tmx.as.base.EntityManager;
import com.tmx.as.blogic.BlogicException;
import com.tmx.as.entities.bill.balance.SellerBalance;
import com.tmx.as.entities.bill.voucher.Voucher;
import com.tmx.as.exceptions.DatabaseException;
import com.tmx.beng.httpsgate.kyivstar.KyivstarBonusNoCommissionGate;
import com.tmx.beng.httpsgate.kyivstar.KyivstarExclusiveGate;
import com.tmx.beng.httpsgate.kyivstar.KyivstarGate;
import com.tmx.beng.httpsgate.umc.UmcGate;
import jxl.Workbook;
import jxl.write.Label;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;


// нужно этот класс запускать при старте биллинга

public class MonthDataSaver extends Thread
{
    MonthDataSaver()
    {
        super("MonthDataSaver Thread");
        //System.out.println("MonthDataSaver Thread started");
        start();
    }

    public void run()
    {
        // бесконечный поток
        while(true)
        {
            Calendar calendar = Calendar.getInstance();
           // System.out.println(calendar.get(Calendar.HOUR) + ":" + calendar.get(Calendar.MINUTE) + ":" + calendar.get(Calendar.SECOND));
            int currentMonth = calendar.get(Calendar.MONTH);  // текущий месяц (January = 0)
            int currentDay = calendar.get(Calendar.DAY_OF_MONTH); // текущий день месяца
            int monthDayCount = calendar.getActualMaximum(Calendar.DAY_OF_MONTH); // количество дней мецяса

            if(currentDay == monthDayCount)
            {
                /*if(calendar.get(Calendar.HOUR) == 23 &&
                    calendar.get(Calendar.MINUTE) == 59 &&
                    calendar.get(Calendar.SECOND) == 59)*/
                if(calendar.get(Calendar.HOUR) == 12 &&
                    calendar.get(Calendar.MINUTE) == 50 &&
                    calendar.get(Calendar.SECOND) == 0)
                {

                    try {
                        List sellerBalance = getSellerBalance();
                        List voucherRemains = getVoucherRemains();
                        List providerBalance = new ArrayList();

                        String obj[][] = {
                                {"Kyivstar", getProviderBalance("Kyivstar")},
                                {"KS_BonusNoComission", getProviderBalance("KS_BonusNoComission")},
                                {"KS_FixConnection", getProviderBalance("KS_FixConnection")},
                                {"MTS", getProviderBalance("MTS")}
                        };

                        providerBalance.add(obj[0]);
                        providerBalance.add(obj[1]);
                        providerBalance.add(obj[2]);
                        providerBalance.add(obj[3]);

                        try {
                            System.out.println("SAVE");
                            saveDataToFile(sellerBalance, voucherRemains, providerBalance);
                        } catch (BlogicException e) {
                            e.printStackTrace();
                        }

                        try {
                            sleep(90000);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }

                    } catch (DatabaseException e) {
                        e.printStackTrace();
                    }
                    // достаем из бд данные
                    // формируем файл и сохраняем его где-то
                }
            }
        }

    }

    private List getSellerBalance() throws DatabaseException
    {
        return new EntityManager().EXECUTE_QUERY(SellerBalance.class, "monthDataSaver", null);
    }

    private List getVoucherRemains() throws DatabaseException
    {
        return new EntityManager().EXECUTE_QUERY(Voucher.class, "remains", null);
    }

    private String getProviderBalance(String providerName)
    {
        String val = null;
        if(providerName.equals("Kyivstar"))
        {
            try{
                val = KyivstarGate.getInstance().getBalance("KyivstarPoP").toString();
            }catch(Throwable e){
                val = e.getLocalizedMessage();
            }
        }
        if(providerName.equals("KS_BonusNoComission"))
        {
            try{
                val = KyivstarBonusNoCommissionGate.getInstance().getBalance("KyivstarPoP").toString();
            }catch(Throwable e){
                val = e.getLocalizedMessage();
            }
        }
        if(providerName.equals("KS_FixConnection"))
        {
            try{
                val = KyivstarExclusiveGate.getInstance().getBalance("KyivstarPoP").toString();
            }catch(Throwable e){
                val = e.getLocalizedMessage();
            }
        }
        if(providerName.equals("MTS"))
        {
            try {
                val = UmcGate.getInstance().getBalance("UMCPoP").toString();
            } catch (Throwable e) {
                val = e.getLocalizedMessage();
            }
        }
        return val;
    }

    private void saveDataToFile(List sellerBalance, List voucherRemains, List providerBalance) throws BlogicException
    {
        try {
            String xlsFileUrl = composeFileName(".xls");
            File xlsFile = new File(xlsFileUrl);
            WritableWorkbook workbook = null;
            workbook = Workbook.createWorkbook(xlsFile);
            WritableSheet sheet1 = workbook.createSheet("Балансы диллеров", 0);
            WritableSheet sheet2 = workbook.createSheet("Остатки ваучеров", 1);
            WritableSheet sheet3 = workbook.createSheet("Балансы шлюзов", 2);

            for(int i = 0; i < sellerBalance.size(); i++)
            {
                Object[] rowObject = (Object[]) sellerBalance.get(i);
                if(rowObject[1] != null)
                {
                    for (int j = 0; j < rowObject.length; j++)
                    {
                        String cell = (rowObject[j] != null) ? rowObject[j].toString() : "";
                        Label label = new Label(j, i, cell);
                        sheet1.addCell(label);
                    }
                }
            }

            for(int k = 0; k < voucherRemains.size(); k++)
            {
                Object[] rowObject = (Object[]) voucherRemains.get(k);
                if(rowObject[1] != null)
                {
                    for (int j = 0; j < rowObject.length; j++)
                    {
                        String cell = (rowObject[j] != null) ? rowObject[j].toString() : "";
                        Label label = new Label(j, k, cell);
                        sheet2.addCell(label);
                    }
                }
            }

            for(int l = 0; l < providerBalance.size(); l++)
            {
                Object[] rowObject = (Object[]) providerBalance.get(l);
                if(rowObject[1] != null)
                {
                    for (int j = 0; j < rowObject.length; j++)
                    {
                        String cell = (rowObject[j] != null) ? rowObject[j].toString() : "";
                        Label label = new Label(j, l, cell);
                        sheet3.addCell(label);
                    }
                }
            }

            workbook.write();
            workbook.close();

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private String composeFileName(String fileExtention)
    {
        final DateFormat format = new SimpleDateFormat("yyyy.MM.dd");
        Date date = new Date();
        return new StringBuffer().
                append(System.getProperty("tmx.home")).
                append("/support/month_data/").
                append("monthData_").
                append(format.format(date)).
                append(fileExtention).
                toString();
    }
}

class MonthDataSaverDemo
{
    public static void main(String args[])
    {
        //MonthDataSaver monthDataSaver = new MonthDataSaver();
        Date date = new Date();
        date.setYear(110);
        date.setMonth(12);
        date.setDate(30);

        date.setHours(23);
        date.setMinutes(59);
        date.setSeconds(0);
        long lastStartUpTime = date.getTime();
        System.out.println(lastStartUpTime);
        //long lastStartUpTime = Long.parseLong("1293487139640");
                                               //1292318488990
        System.out.println(new SimpleDateFormat("yyyy.dd.MM_HH:mm:ss").format(new Date(lastStartUpTime)));
        //lastStartUpTime = Long.parseLong("600000") / (60 * 1000);
        //System.out.println(lastStartUpTime);

/*        SimpleDateFormat formatter = new SimpleDateFormat("yyyy.dd.MM_HH:mm:ss");
        ParsePosition pos = new ParsePosition(0);
        java.util.Date newDate = formatter.parse("1292248800000", pos);  */
    //    long newLastStartUpTimeMs = newDate.getTime();
     //   System.out.println(newLastStartUpTimeMs);
        /*Date d = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat("ddMMHHmmss");
        String ff = formatter.format(d);
        System.out.println(ff);*/
        //long currDate = new java.util.Date().getTime();
        //System.out.println(currDate);
        

        //long msToStart = Long.parseLong("1292313600000") + Long.parseLong("600000") - currDate;
        //double currStartUpCountDouble = (currDate - Long.parseLong("1292313600000")) / Long.parseLong("600000");
    }
}