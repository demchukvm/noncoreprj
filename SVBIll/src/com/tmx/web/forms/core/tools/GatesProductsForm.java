package com.tmx.web.forms.core.tools;

import com.tmx.web.base.BasicActionForm;
import java.util.HashMap;

public class GatesProductsForm extends BasicActionForm
{
    HashMap mediumMap = new HashMap();
    private String gateName;
    private String productName;

    public void init()
    {
        gateName = "";
        productName = "";
    }

    public HashMap getMediumMap() {
        return mediumMap;
    }

    public void setMediumMap(HashMap mediumMap) {
        this.mediumMap = mediumMap;
    }

    public String getGateName() {
        return gateName;
    }

    public void setGateName(String gateName) {
        this.gateName = gateName;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }
}
