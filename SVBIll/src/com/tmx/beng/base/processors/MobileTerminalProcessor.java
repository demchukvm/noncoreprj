package com.tmx.beng.base.processors;

import com.tmx.as.base.EntityManager;
import com.tmx.as.base.QueryParameterWrapper;
import com.tmx.as.base.criterion.CriterionWrapper;
import com.tmx.as.base.criterion.Restrictions;
import com.tmx.as.blogic.TerminalManager;
import com.tmx.as.blogic.balance.SellerBalanceManager;
import com.tmx.as.blogic.balance.TerminalBalanceManager;
import com.tmx.as.entities.bill.balance.SellerBalance;
import com.tmx.as.entities.bill.balance.TerminalBalance;
import com.tmx.as.entities.bill.seller.Seller;
import com.tmx.as.entities.bill.tariff.SellerTariff;
import com.tmx.as.entities.bill.terminal.Terminal;
import com.tmx.as.entities.bill.transaction.ClientTransaction;
import com.tmx.as.exceptions.DatabaseException;
import com.tmx.beng.base.*;
import com.tmx.beng.base.cache.DBCache;
import com.tmx.util.MD5;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Date;


public class MobileTerminalProcessor extends TaskProcessor {

    public Task process0(Task task, DBCache dbCache, BillingMessageWritable msg) throws BillException
    {
        logger.info("MobileTerminalProcessor." + msg.getOperationName());

        //if(msg.getAttributeString(BillingMessage.IS_MOBILE_JAVA).equals("true"))
        if(msg.getAttributeString(BillingMessage.IS_MOBILE_JAVA) != null)
        {
            BillingMessageImpl resp = null;



            if(msg.getOperationName().equals(BillingMessage.O_REFILL_PAYMENT))
            {

                if(transactionTimeout(msg))
                {
                    //msg.setAttributeString(BillingMessage.IS_MOBILE_JAVA, "true");
                    msg.setStatusCode(StatusDictionary.ACCOUNT_TIMEOUT);
                    throw new BillException("err.account_timeout");
                }

            }
            else
            {

                if(msg.getOperationName().equals(BillingMessage.O_ACTIVATE_TERMINAL))
                    resp = activateMobileTerminal(msg);
                else if(msg.getOperationName().equals(BillingMessage.O_SERVICE_PARAMETERS))
                    resp = servicesParameters(msg);
                else if(msg.getOperationName().equals(BillingMessage.O_BALANCES))
                    resp = balances(msg);
                else if(msg.getOperationName().equals(BillingMessage.O_REPORT_FULL))
                    resp = reportFull(msg);
                else if(msg.getOperationName().equals(BillingMessage.O_REPORT_SHORT))
                    resp = reportShort(msg);


                resp.setAttributeString(BillingMessage.IS_MOBILE_JAVA, "true");


                CallBack callBack = task.getCallBack();
                if(callBack != null)
                    callBack.sendBilledMessage(resp);
            
            }


            //msg.setOperationName(BillingMessage.O_REFILL_PAYMENT_RESULT);

        }

        return task;
    }


    private Boolean transactionTimeout(BillingMessageWritable msg) throws BillException {
        logger.info("Check timeout...");
        //BillingMessageImpl resp = new BillingMessageImpl();
        boolean flag = false;

        try {
            EntityManager entityManager = new EntityManager();
            Terminal terminal = new TerminalManager().getTerminalBySN(msg.getAttributeString(BillingMessage.TERMINAL_SN));


//            DateFormat df = new SimpleDateFormat("yyyyMMdd");
//                                                                    //2011.01.04 00:42:23
//            Date from = df.parse(msg.getAttributeString(BillingMessage.FROM));   // начальная дата 00:00:00
//            Date to = df.parse(msg.getAttributeString(BillingMessage.TO)); // конечная дата - устанавливаем в 23:59:59
//            to.setHours(23);
//            to.setMinutes(59);
//            to.setSeconds(59);
            
            Date currentA = new Date();
            currentA.setHours(0);
            currentA.setMinutes(0);
            currentA.setSeconds(0);

            Date currentB = new Date();
            currentB.setHours(23);
            currentB.setMinutes(59);
            currentB.setSeconds(59);

            Date currentC = new Date(); // current transaction time

            String account = "";
            if(msg.getAttributeString(BillingMessage.ACCOUNT_NUMBER) != null)
                account = msg.getAttributeString(BillingMessage.ACCOUNT_NUMBER).substring(1,msg.getAttributeString(BillingMessage.ACCOUNT_NUMBER).length());
            else
                account = msg.getAttributeString(BillingMessage.PAY_ACCOUNT_NUMBER);

            CriterionWrapper [] criterions = null;
            criterions = new CriterionWrapper[]{
                    Restrictions.eq("terminal.terminalId", terminal.getTerminalId()),
                    Restrictions.between("transactionTime", currentA, currentB),
                    Restrictions.eq("accountNumber", account)
                };


            List<ClientTransaction> transList = new EntityManager().RETRIEVE_ALL(ClientTransaction.class, criterions, null);

            if(!transList.isEmpty())
            {
                int i = transList.size()-1;
                ClientTransaction clientTransaction = transList.get(i);
                Date date = clientTransaction.getTransactionTime();
                long A = date.getTime(); // last transaction time
                long B = currentC.getTime();    // current transaction time
                
                if(B - A < 600000)
                {
                    flag = true;

                }
            }


        } catch (DatabaseException e) {
            logger.error("DatabaseException", e);
            e.printStackTrace();
        }

        return flag;
        //return resp;
    }


    private BillingMessageImpl activateMobileTerminal(BillingMessageWritable msg)
    {
        logger.info("Activate mobile terminal...");

        BillingMessageImpl resp = new BillingMessageImpl();

        try {
            EntityManager entityManager = new EntityManager();

            CriterionWrapper criterions[] = new CriterionWrapper[] {
                    Restrictions.eq("serialNumber", msg.getAttributeString(BillingMessage.TERMINAL_SN))
            };

            Terminal terminal = (Terminal) entityManager.RETRIEVE(Terminal.class, null, criterions, null);

            if(terminal != null)
            {
                String realTerminalPswd = MD5.getHashString(terminal.getPassword());
                String rqstTerminalPswd = msg.getAttributeString(BillingMessage.TERMINAL_PSWD);

                if(realTerminalPswd.equals(rqstTerminalPswd))
                {
                    Seller seller = (Seller) entityManager.RETRIEVE(Seller.class, terminal.getSellerOwnerId());

                    if(seller != null)
                    {
                        resp.setAttributeString(BillingMessage.SELLER_CODE, seller.getCode());

                        String[] service = {
                            "Beeline",
                            "CDMACom",
                            "InterTelecomCom",
                            "KS1-25",
                            "KS25-99",
                            "KS100+",
                            "Life",
                            "UMCPoP"
                        };

                        for(int i=0; i<service.length; i++)
                        {
                            QueryParameterWrapper[] qpw = {
                                new QueryParameterWrapper("sellerId", seller.getSellerId()),
                                new QueryParameterWrapper("serviceCode", service[i])
                            };

                            List list = new EntityManager().EXECUTE_QUERY(SellerTariff.class, "fee", qpw);

                            if(list.size() > 0)
                            {
                                String fee = list.get(0).toString();
                                if(service[i].equals("Beeline"))
                                    resp.setAttributeString(BillingMessage.FEE_BEELINE, fee);
                                else if(service[i].equals("CDMACom"))
                                    resp.setAttributeString(BillingMessage.FEE_CDMA, fee);
                                else if(service[i].equals("InterTelecomCom"))
                                    resp.setAttributeString(BillingMessage.FEE_INTERTELECOM, fee);
                                else if(service[i].equals("KS1-25"))
                                    resp.setAttributeString(BillingMessage.FEE_KS0124, fee);
                                else if(service[i].equals("KS25-99"))
                                    resp.setAttributeString(BillingMessage.FEE_KS2599, fee);
                                else if(service[i].equals("KS100+"))
                                    resp.setAttributeString(BillingMessage.FEE_KS100, fee);
                                else if(service[i].equals("Life"))
                                    resp.setAttributeString(BillingMessage.FEE_LIFE, fee);
                                else if(service[i].equals("UMCPoP"))
                                    resp.setAttributeString(BillingMessage.FEE_UMCPOP, fee);
                            }
                            else
                            {
                                if(service[i].equals("Beeline"))
                                    resp.setAttributeString(BillingMessage.FEE_BEELINE, "empty");
                                else if(service[i].equals("CDMACom"))
                                    resp.setAttributeString(BillingMessage.FEE_CDMA, "empty");
                                else if(service[i].equals("InterTelecomCom"))
                                    resp.setAttributeString(BillingMessage.FEE_INTERTELECOM, "empty");
                                else if(service[i].equals("KS1-25"))
                                    resp.setAttributeString(BillingMessage.FEE_KS0124, "empty");
                                else if(service[i].equals("KS25-99"))
                                    resp.setAttributeString(BillingMessage.FEE_KS2599, "empty");
                                else if(service[i].equals("KS100+"))
                                    resp.setAttributeString(BillingMessage.FEE_KS100, "empty");
                                else if(service[i].equals("Life"))
                                    resp.setAttributeString(BillingMessage.FEE_LIFE, "empty");
                                else if(service[i].equals("UMCPoP"))
                                    resp.setAttributeString(BillingMessage.FEE_UMCPOP, "empty");
                            }
                        }
                    }
                    else logger.info("SellerCode not found");
                }
                else logger.info("TerminalPswd is wrong");
            }
            else logger.info("TerminalSn not found");

        } catch (DatabaseException e) {
            logger.error("DatabaseException", e);
            e.printStackTrace();
        } catch (Exception ee) {
            logger.error("Exception", ee);
            ee.printStackTrace();
        }

        resp.setOperationName(BillingMessage.O_ACTIVATE_TERMINAL_RESULT);

        return resp;
    }

    private BillingMessageImpl servicesParameters(BillingMessageWritable msg)
    {
        logger.info("Get service parameters for mobile terminal...");

        BillingMessageImpl resp = new BillingMessageImpl();

        try {
            EntityManager entityManager = new EntityManager();

            CriterionWrapper criterions[] = new CriterionWrapper[] {
                    Restrictions.eq("serialNumber", msg.getAttributeString(BillingMessage.TERMINAL_SN))
            };

            Terminal terminal = (Terminal) entityManager.RETRIEVE(Terminal.class, null, criterions, null);

            if(terminal != null)
            {
                String realTerminalPswd = MD5.getHashString(terminal.getPassword());
                String rqstTerminalPswd = msg.getAttributeString(BillingMessage.TERMINAL_PSWD);

                if(realTerminalPswd.equals(rqstTerminalPswd))
                {
                    Seller seller = (Seller) entityManager.RETRIEVE(Seller.class, terminal.getSellerOwnerId());

                    if(seller != null)
                    {
                        String[] service = {
                            "Beeline",
                            "CDMACom",
                            "InterTelecomCom",
                            "KS1-25",
                            "KS25-99",
                            "KS100+",
                            "Life",
                            "UMCPoP"
                        };

                        for(int i=0; i<service.length; i++)
                        {
                            QueryParameterWrapper[] qpw = {
                                new QueryParameterWrapper("sellerId", seller.getSellerId()),
                                new QueryParameterWrapper("serviceCode", service[i])
                            };

                            List list = new EntityManager().EXECUTE_QUERY(SellerTariff.class, "fee", qpw);

                            if(list.size() > 0)
                            {
                                String fee = list.get(0).toString();
                                if(service[i].equals("Beeline"))
                                    resp.setAttributeString(BillingMessage.FEE_BEELINE, fee);
                                else if(service[i].equals("CDMACom"))
                                    resp.setAttributeString(BillingMessage.FEE_CDMA, fee);
                                else if(service[i].equals("InterTelecomCom"))
                                    resp.setAttributeString(BillingMessage.FEE_INTERTELECOM, fee);
                                else if(service[i].equals("KS1-25"))
                                    resp.setAttributeString(BillingMessage.FEE_KS0124, fee);
                                else if(service[i].equals("KS25-99"))
                                    resp.setAttributeString(BillingMessage.FEE_KS2599, fee);
                                else if(service[i].equals("KS100+"))
                                    resp.setAttributeString(BillingMessage.FEE_KS100, fee);
                                else if(service[i].equals("Life"))
                                    resp.setAttributeString(BillingMessage.FEE_LIFE, fee);
                                else if(service[i].equals("UMCPoP"))
                                    resp.setAttributeString(BillingMessage.FEE_UMCPOP, fee);
                            }
                            else
                            {
                                if(service[i].equals("Beeline"))
                                    resp.setAttributeString(BillingMessage.FEE_BEELINE, "empty");
                                else if(service[i].equals("CDMACom"))
                                    resp.setAttributeString(BillingMessage.FEE_CDMA, "empty");
                                else if(service[i].equals("InterTelecomCom"))
                                    resp.setAttributeString(BillingMessage.FEE_INTERTELECOM, "empty");
                                else if(service[i].equals("KS1-25"))
                                    resp.setAttributeString(BillingMessage.FEE_KS0124, "empty");
                                else if(service[i].equals("KS25-99"))
                                    resp.setAttributeString(BillingMessage.FEE_KS2599, "empty");
                                else if(service[i].equals("KS100+"))
                                    resp.setAttributeString(BillingMessage.FEE_KS100, "empty");
                                else if(service[i].equals("Life"))
                                    resp.setAttributeString(BillingMessage.FEE_LIFE, "empty");
                                else if(service[i].equals("UMCPoP"))
                                    resp.setAttributeString(BillingMessage.FEE_UMCPOP, "empty");
                            }
                        }
                    }
                    else logger.info("SellerCode not found");
                }
                else logger.info("TerminalPswd is wrong");
            }
            else logger.info("TerminalSn not found");

        } catch (DatabaseException e) {
            logger.error("DatabaseException", e);
            e.printStackTrace();
        } catch (Exception ee) {
            logger.error("Exception", ee);
            ee.printStackTrace();
        }

        resp.setOperationName(BillingMessage.O_SERVICE_PARAMETERS_RESULT);

        return resp;
    }

    private BillingMessageImpl balances(BillingMessageWritable msg)
    {
        BillingMessageImpl resp = new BillingMessageImpl();

        String sBalance = "";
        String tBalance = "";

        EntityManager entityManager = null;
        Terminal terminal = null;
        Seller seller = null;

        try {
            entityManager = new EntityManager();
            CriterionWrapper criterions[] = new CriterionWrapper[] {
                    Restrictions.eq("serialNumber", msg.getAttributeString(BillingMessage.TERMINAL_SN))
            };
            terminal = (Terminal) entityManager.RETRIEVE(Terminal.class, null, criterions, null);
            TerminalBalance terminalBalance = (TerminalBalance)(new TerminalBalanceManager()).getBalance(terminal.getPrimaryBalance().getBalanceId());
            tBalance = terminalBalance.getAmount().toString();


        } catch (DatabaseException e) {
            logger.error("Can't retrieve seller balance " + e.getMessage());
            tBalance = "empty";
        }

        try {
            seller = (Seller) entityManager.RETRIEVE(Seller.class, terminal.getSellerOwnerId());

            SellerBalance sellerBalance = (SellerBalance)(new SellerBalanceManager()).getBalance(seller.getPrimaryBalance().getBalanceId());
            sBalance = sellerBalance.getAmount().toString();

        } catch (DatabaseException e) {
            logger.error("Can't retrieve seller balance " + e.getMessage());
            sBalance = "empty";
        }

        resp.setAttributeString(BillingMessage.SELLER_BALANCE, sBalance);
        resp.setAttributeString(BillingMessage.TERMINAL_BALANCE, tBalance);

        resp.setOperationName(BillingMessage.O_BALANCES_RESULT);

        return resp;
    }

    private BillingMessageImpl reportFull(BillingMessageWritable msg)
    {
        BillingMessageImpl resp = new BillingMessageImpl();

        String transactionsList = "";

        try {

            EntityManager entityManager = new EntityManager();
            CriterionWrapper criterions1[] = new CriterionWrapper[] {
                    Restrictions.eq("serialNumber", msg.getAttributeString(BillingMessage.TERMINAL_SN))
            };
            Terminal terminal = (Terminal) entityManager.RETRIEVE(Terminal.class, null, criterions1, null);


            DateFormat df = new SimpleDateFormat("yyyyMMdd");

            Date from = df.parse(msg.getAttributeString(BillingMessage.FROM));   // начальная дата 00:00:00
            Date to = df.parse(msg.getAttributeString(BillingMessage.TO)); // конечная дата - устанавливаем в 23:59:59
            to.setHours(23);
            to.setMinutes(59);
            to.setSeconds(59);

            CriterionWrapper [] criterions2 = null;
            if(msg.getAttributeString(BillingMessage.ACCOUNT).equals(""))
            {
                criterions2 = new CriterionWrapper[]{
                        Restrictions.eq("terminal.terminalId", terminal.getTerminalId()),
                        Restrictions.between("transactionTime", from, to)
                };
            }
            else
            {
                criterions2 = new CriterionWrapper[]{
                        Restrictions.eq("terminal.terminalId", terminal.getTerminalId()),
                        Restrictions.between("transactionTime", from, to),
                        Restrictions.eq("accountNumber", msg.getAttributeString(BillingMessage.ACCOUNT))
                };   
            }

            List<ClientTransaction> transList = new EntityManager().RETRIEVE_ALL(ClientTransaction.class, criterions2, null);
            //  transactionsList отсортировать по времени проведения, самые последние в начале списка

            if(!transList.isEmpty())
            {

                for(ClientTransaction t : transList)
                {
                    String status = t.getStatus();

                    if(status.equals("0"))
                    {
                        status = "[+]";
                    }
                    else if(status.equals("1"))
                    {
                        status = "[?]";
                    }
                    else
                    {
                        status = "[-]";
                    }

                    String account = t.getAccountNumber();
                    String amount = t.getAmount().toString();
                    String tDate = "";

                    if(msg.getAttributeString(BillingMessage.ACCOUNT).equals(""))
                    {
                        tDate = t.getTransactionTime().toString().substring(5, 16); // время MM-DD TT:MM  2011-03-30 00:43
                    }
                    else
                    {
                        tDate = t.getTransactionTime().toString().substring(11, 16); // время TT:MM  2011-03-30 00:43
                    }


                    transactionsList += status + " " + account + " " + amount + " " + tDate + "&";
                }
            }
            else
            {
                transactionsList = "empty";
            }
        } catch (DatabaseException e1) {
            e1.printStackTrace();
        } catch (ParseException e2) {
            e2.printStackTrace();
        }

        resp.setAttributeString(BillingMessage.TRANSACTION_LIST, transactionsList);
        resp.setOperationName(BillingMessage.O_REPORT_FULL_RESULT);

        return resp;
    }

    private BillingMessageImpl reportShort(BillingMessageWritable msg)
    {
        BillingMessageImpl resp = new BillingMessageImpl();

        int countOk = 0;
        int countError = 0;
        int countUnknown = 0;

        int countBeeline = 0;
        int countCDMA = 0;
        int countInterTelecom = 0;
        int countKS0124 = 0;
        int countKS2599 = 0;
        int countKS100 = 0;
        int countLife = 0;
        int countUMCPoP = 0;

        int countTotal = 0;

        double sumBeeline = 0;
        double sumCDMA = 0;
        double sumInterTelecom = 0;
        double sumKS0124 = 0;
        double sumKS2599 = 0;
        double sumKS100 = 0;
        double sumLife = 0;
        double sumUMCPoP = 0;

        double sumTotal = 0;

        try {

            EntityManager entityManager = new EntityManager();
            CriterionWrapper criterions1[] = new CriterionWrapper[] {
                    Restrictions.eq("serialNumber", msg.getAttributeString(BillingMessage.TERMINAL_SN))
            };
            Terminal terminal = (Terminal) entityManager.RETRIEVE(Terminal.class, null, criterions1, null);

            DateFormat df = new SimpleDateFormat("yyyyMMdd");

            Date from = df.parse(msg.getAttributeString(BillingMessage.FROM));   // начальная дата 00:00:00
            Date to = df.parse(msg.getAttributeString(BillingMessage.TO)); // конечная дата - устанавливаем в 23:59:59
            to.setHours(23);
            to.setMinutes(59);
            to.setSeconds(59);

            CriterionWrapper [] criterions = new CriterionWrapper[]{
                    Restrictions.eq("terminal.terminalId", terminal.getTerminalId()),
                    Restrictions.between("transactionTime", from, to)
            };


            List<ClientTransaction> transList = new EntityManager().RETRIEVE_ALL(ClientTransaction.class, criterions, null);



            if(!transList.isEmpty())
            {
                for(ClientTransaction t : transList)
                {

                    String status = t.getStatus();

                    if(status.equals("0"))
                    {
                        countOk++;

                        if(t.getServiceCode().equals("Beeline"))
                        {
                            countBeeline++;
                            sumBeeline = t.getAmount();
                        }
                        else if(t.getServiceCode().equals("CDMACom"))
                        {
                            countCDMA++;
                            sumCDMA += t.getAmount();
                        }
                        else if(t.getServiceCode().equals("InterTelecomCom"))
                        {
                            countInterTelecom++;
                            sumInterTelecom += t.getAmount();
                        }
                        else if(t.getServiceCode().equals("KS1-25"))
                        {
                            countKS0124++;
                            sumKS0124 += t.getAmount();
                        }
                        else if(t.getServiceCode().equals("KS25-99"))
                        {
                            countKS2599++;
                            sumKS2599 += t.getAmount();
                        }
                        else if(t.getServiceCode().equals("KS100+"))
                        {
                            countKS100++;
                            sumKS100 += t.getAmount();
                        }
                        else if(t.getServiceCode().equals("Life"))
                        {
                            countLife++;
                            sumLife += t.getAmount();
                        }
                        else if(t.getServiceCode().equals("UMCPoP"))
                        {
                            countUMCPoP++;
                            sumUMCPoP += t.getAmount();
                        }
                    }
                    else if(status.equals("1"))
                    {
                        countUnknown++;
                    }
                    else
                    {
                        countError++;
                    }
                }

                countTotal = countBeeline + countCDMA + countInterTelecom + countKS0124 + countKS2599 + countKS100 + countLife + countUMCPoP;
                sumTotal = sumBeeline + sumCDMA + sumInterTelecom + sumKS0124 + sumKS2599 + sumKS100 + sumLife + sumUMCPoP;
            }

        } catch (DatabaseException e1) {
            e1.printStackTrace();
        } catch (ParseException e2) {
            e2.printStackTrace();
        }

        resp.setAttributeString(BillingMessage.STATUS_OK, countOk + "");
        resp.setAttributeString(BillingMessage.STATUS_ERROR, countError + "");
        resp.setAttributeString(BillingMessage.STATUS_UNKNOWN, countUnknown + "");
        resp.setAttributeString(BillingMessage.COUNT_BEELINE, countBeeline + "");
        resp.setAttributeString(BillingMessage.COUNT_CDMA, countCDMA + "");
        resp.setAttributeString(BillingMessage.COUNT_INTERTELECOM, countInterTelecom + "");
        resp.setAttributeString(BillingMessage.COUNT_KS0124, countKS0124 + "");
        resp.setAttributeString(BillingMessage.COUNT_KS2599, countKS2599 + "");
        resp.setAttributeString(BillingMessage.COUNT_KS100, countKS100 + "");
        resp.setAttributeString(BillingMessage.COUNT_LIFE, countLife + "");
        resp.setAttributeString(BillingMessage.COUNT_UMCPOP, countUMCPoP + "");
        resp.setAttributeString(BillingMessage.SUM_BEELINE, sumBeeline + "");
        resp.setAttributeString(BillingMessage.SUM_CDMA, sumCDMA + "");
        resp.setAttributeString(BillingMessage.SUM_INTERTELECOM, sumInterTelecom + "");
        resp.setAttributeString(BillingMessage.SUM_KS0124, sumKS0124 + "");
        resp.setAttributeString(BillingMessage.SUM_KS2599, sumKS2599 + "");
        resp.setAttributeString(BillingMessage.SUM_KS100, sumKS100 + "");
        resp.setAttributeString(BillingMessage.SUM_LIFE, sumLife + "");
        resp.setAttributeString(BillingMessage.SUM_UMCPOP, sumUMCPoP + "");
        resp.setAttributeString(BillingMessage.COUNT_TOTAL, countTotal + "");
        resp.setAttributeString(BillingMessage.SUM_TOTAL, sumTotal + "");
        resp.setOperationName(BillingMessage.O_REPORT_SHORT_RESULT);

        return resp;
    }
}
