package com.tmx.as.base.jdbc;

import java.sql.Connection;
import java.sql.SQLException;

/**

 */
public abstract class BasicJdbcAdapter implements JdbcAdapter, JdbcAdapterSpi{

    public Object adoptParameter(Object param, String typeName, Connection jdbcConn) throws SQLException {
        if(param instanceof Object[]){
            return adoptArray((Object[])param, typeName, jdbcConn);
        }
        //throw out without modification
        return param;
    }
}
