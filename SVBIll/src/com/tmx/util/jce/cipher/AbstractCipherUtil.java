package com.tmx.util.jce.cipher;

import com.tmx.util.jce.JceException;

import javax.crypto.*;
import java.security.NoSuchAlgorithmException;
import java.io.*;

/**
 * This class defines methods for encrypting and decrypting using the abstract
 * algorithm and for generating, reading and writing abstract keys.
 */
public abstract class AbstractCipherUtil implements CipherUtil{

    public abstract String getCypherAlgoritmName();

    /**
     * Generate a secret encryption/decryption key
     */
    public SecretKey generateKey() throws JceException {
        try{
            // Get a key generator for Triple DES (a.k.a DESede)
            KeyGenerator keygen = KeyGenerator.getInstance(getCypherAlgoritmName());
            // Use it to generate a key
            return keygen.generateKey();
        }
        catch(NoSuchAlgorithmException e){
            throw new JceException("err."+getCypherAlgoritmName()+"_util.generate_key_failed", e);
        }
    }

    /**
     * Read a secret key from the specified file
     */
    public SecretKey readKey(File f) throws JceException {
        // Read the raw bytes from the keyfile
        try{
            return readKey(new FileInputStream(f));
        }
        catch(IOException e){
            throw new JceException("err."+getCypherAlgoritmName()+"_util.read_key_failed", e);
        }
        catch(JceException e){
            throw e;
        }

    }

    public SecretKey readKey(InputStream is) throws JceException{
        DataInputStream in = new DataInputStream(is);
        byte[] rawkey = null;
        try{
            rawkey = new byte[is.available()];
            in.readFully(rawkey);
            in.close();
        }
        catch(IOException e){
            throw new JceException("err."+getCypherAlgoritmName()+"_util.read_key_failed", e);
        }
        return readKey(rawkey);
    }

    /**
     * Use the specified key to encrypt bytes from the input stream
     * and write them to the output stream. This method uses CipherOutputStream
     * to perform the encryption and write bytes at the same time.
     */
    public void encrypt(SecretKey key, InputStream in, OutputStream out) throws JceException {
        try{
            // Create and initialize the encryption engine
            Cipher cipher = Cipher.getInstance(getCypherAlgoritmName());
            cipher.init(Cipher.ENCRYPT_MODE, key);

            // Create a special output stream to do the work for us
            CipherOutputStream cos = new CipherOutputStream(out, cipher);

            // Read from the input and write to the encrypting output stream
            byte[] buffer = new byte[2048];
            int bytesRead;
            while ((bytesRead = in.read(buffer)) != -1)
                cos.write(buffer, 0, bytesRead);

            cos.close();

            // For extra security, don't leave any plaintext hanging around memory.
            java.util.Arrays.fill(buffer, (byte) 0);
        }
        catch(Exception e){
            throw new JceException("err."+getCypherAlgoritmName()+"_util.encrypt_failed", e);
        }
    }

    /**
     * Use the specified key to decrypt bytes ready from the input
     * stream and write them to the output stream. This method uses uses Cipher
     * directly to show how it can be done without CipherInputStream and
     * CipherOutputStream.
     */
    public void decrypt(SecretKey key, InputStream in, OutputStream out) throws JceException {
        try{
            // Create and initialize the decryption engine
            Cipher cipher = Cipher.getInstance(getCypherAlgoritmName());
            cipher.init(Cipher.DECRYPT_MODE, key);

            // Read bytes, decrypt, and write them out.
            byte[] buffer = new byte[2048];
            int bytesRead;
            while ((bytesRead = in.read(buffer)) != -1)
                out.write(cipher.update(buffer, 0, bytesRead));


            // Write out the final bunch of decrypted bytes
            out.write(cipher.doFinal());
            out.flush();
        }
        catch(Exception e){
            throw new JceException("err."+getCypherAlgoritmName()+"_util.decrypt_failed", e);
        }
    }



}
