<%@ page language="java" pageEncoding="UTF-8" contentType="text/html; charset=utf-8"
         errorPage="/core/jsp/message/jsperror.jsp" isELIgnored="false" %>
<%@ taglib uri="/WEB-INF/tld/struts-nested.tld" prefix="nested" %>
<%@ taglib uri="/WEB-INF/tld/tmx-controls.tld" prefix="tmx_ctrl" %>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>


<html:form action="/core/transactions/client_transaction/infoClientTransaction">
<table border="0" width="100%">

    <!-- BUTTONS -->
    <tr>
        <td style="text-align: left">
            <tmx_ctrl:button property="onCancelButton"/>
        </td>
    </tr>
    <tr>
        <td valign="center" align="center" class="tabForm">
        <table width="100%" border="0">
            <%--<!-- Form header -->--%>
            <tr>
                <td colspan="2">
                    <h4 class="dataLabel">
                        <bean:message bundle="core.labels" key="core.transaction.clienttransaction.infoclienttransactionform.titleGeneral"/>
                    </h4>
                </td>
            </tr>

            <!--BEGIN for abstract info transaction form    -->
            <tr>
                <td class="dataLabel" width="15%">
                    <tmx_ctrl:writeEntityLabel attrName="transactionId"
                                               entityClassName="com.tmx.as.entities.bill.transaction.ClientTransaction"/>:
                </td>
                <td class="dataField" width="35%">
                    <tmx_ctrl:editBox property="editBoxTransactionId"/>
                </td>

                <td class="dataLabel" width="15%">
                    <tmx_ctrl:writeEntityLabel attrName="{@name}"
                                               entityClassName="com.tmx.as.entities.bill.transaction.TransactionType"/>:
                </td>
                <td class="dataField" width="35%">
                    <tmx_ctrl:editBox property="editBoxType"/>
                </td>
            </tr>

            <tr>
                <td class="dataLabel">
                    <tmx_ctrl:writeEntityLabel attrName="transactionGUID"
                                               entityClassName="com.tmx.as.entities.bill.transaction.ClientTransaction"/>:
                </td>
                <td class="dataField">
                    <tmx_ctrl:editBox property="editBoxTransactionGUID"/>
                </td>

                <td class="dataField" >
                    <tmx_ctrl:writeEntityLabel attrName="operationType"
                                               entityClassName="com.tmx.as.entities.bill.transaction.ClientTransaction"/>:
                </td>
                <td class="dataField" >
                       <tmx_ctrl:editBox property="editBoxOperationType" />
                </td>
            </tr>

            <tr>
                <td class="dataLabel" >
                    <tmx_ctrl:writeEntityLabel attrName="cliTransactionNum"
                                               entityClassName="com.tmx.as.entities.bill.transaction.ClientTransaction"/>:
                </td>
                <td class="dataField">
                       <tmx_ctrl:textArea property="textAreaClientTransactionNum"/>
                </td>

                <td class="dataLabel" >
                    <tmx_ctrl:writeEntityLabel attrName="status"
                                               entityClassName="com.tmx.as.entities.bill.transaction.ClientTransaction"/>:
                </td>
                <td class="dataField" >
                       <tmx_ctrl:editBox property="editBoxStatus" />
                </td>
            </tr>

            <tr>
                <td class="dataLabel">
                    <tmx_ctrl:writeEntityLabel attrName="transactionTime"
                                               entityClassName="com.tmx.as.entities.bill.transaction.ClientTransaction"/>:
                </td>
                <td class="dataField">
                    <tmx_ctrl:timeSelector property="timeSelectorTransactionTime" />
                </td>

                <td class="dataField" >
                    <tmx_ctrl:writeEntityLabel attrName="statusDescription"
                                               entityClassName="com.tmx.as.entities.bill.transaction.ClientTransaction"/>:
                </td>
                <td class="dataField" >
                    <tmx_ctrl:textArea property="textAreaStatusDescription" />
                </td>
            </tr>

            <tr>
                <td class="dataLabel" width="15%">
                    <tmx_ctrl:writeEntityLabel attrName="clientTime"
                                               entityClassName="com.tmx.as.entities.bill.transaction.ClientTransaction"/>:
                </td>
                <td class="dataField" width="35%">
                       <tmx_ctrl:timeSelector property="timeSelectorClientTime" />
                </td>

                <td class="dataField" >
                    <tmx_ctrl:writeEntityLabel attrName="code"
                                               entityClassName="com.tmx.as.entities.bill.transaction.TransactionError"/>:
                </td>
                <td class="dataField" >
                    <tmx_ctrl:editBox property="editBoxTransErrCode" />
                </td>
            </tr>
            <tr>
                <td class="dataField" >
                    <tmx_ctrl:writeEntityLabel attrName="accountNumber"
                                               entityClassName="com.tmx.as.entities.bill.transaction.ClientTransaction"/>:
                </td>
                <td class="dataField" >
                    <tmx_ctrl:editBox property="editBoxAccountNumber" />
                </td>
                <td class="dataLabel" width="15%">
                </td>
                <td class="dataField" width="35%">
                </td>
            </tr>

            <tr>
                <td class="dataLabel">
                    <tmx_ctrl:writeEntityLabel attrName="amount"
                                               entityClassName="com.tmx.as.entities.bill.transaction.ClientTransaction"/>:
                </td>
                <td class="dataField">
                    <tmx_ctrl:editBox property="editBoxAmount"/>
                </td>

                 <td class="dataField" >
                    <tmx_ctrl:writeEntityLabel attrName="message"
                                               entityClassName="com.tmx.as.entities.bill.transaction.TransactionError"/>:
                </td>
                <td class="dataField">
                    <tmx_ctrl:textArea property="textAreaTransErrMessage"/>
            </tr>
        </table>
        </td>

    </tr>

    <!--operator-->
    <tr>
        <td valign="center" align="center" class="tabForm">
        <table width="100%" border="0">
            <%--<!-- Form header -->--%>
            <tr>
                <td colspan="2">
                    <h4 class="dataLabel">
                        <bean:message bundle="core.labels" key="core.transaction.clienttransaction.infoclienttransactionform.titleOperator"/>
                    </h4>
                </td>
            </tr>

            <tr>
                <td class="dataLabel" width="15%">
                       <tmx_ctrl:writeEntityLabel attrName="{@name}"
                                                  entityClassName="com.tmx.as.entities.bill.operator.Operator"/>:
                </td>
                <td class="dataField" width="35%">
                    <tmx_ctrl:textArea property="textAreaOperatorName"/>
                </td>

                <td class="dataLabel" width="15%">
                    <tmx_ctrl:writeEntityLabel attrName="{@name}"
                                               entityClassName="com.tmx.as.entities.bill.balance.OperatorBalance"/>:
                </td>
                <td class="dataField" width="35%">
                    <tmx_ctrl:textArea property="textAreaOperatorBalanceName"/>
                </td>
            </tr>


        </table>
        </td>

    </tr>

    <!--seller-->
    <tr>
        <td valign="center" align="center" class="tabForm">
        <table width="100%" border="0">
            <%--<!-- Form header -->--%>
            <tr>
                <td colspan="2">
                    <h4 class="dataLabel">
                        <bean:message bundle="core.labels" key="core.transaction.clienttransaction.infoclienttransactionform.titleSeller"/>
                    </h4>
                </td>
            </tr>

            <tr>
                <td class="dataLabel" width="15%">
                       <tmx_ctrl:writeEntityLabel attrName="{@name}"
                                                  entityClassName="com.tmx.as.entities.bill.seller.Seller"/>:
                </td>
                <td class="dataField" width="35%">
                    <tmx_ctrl:textArea property="textAreaSellerName"/>
                </td>

                <td class="dataLabel" width="15%">
                    <tmx_ctrl:writeEntityLabel attrName="{@name}"
                                               entityClassName="com.tmx.as.entities.bill.balance.SellerBalance"/>:
                </td>
                <td class="dataField" width="35%">
                    <tmx_ctrl:textArea property="textAreaSellerBalanceName" width="200px"/>
                </td>
            </tr>


        </table>
        </td>

    </tr>

    <!--terminal-->
    <tr>
        <td valign="center" align="center" class="tabForm">
        <table width="100%" border="0">
            <%--<!-- Form header -->--%>
            <tr>
                <td colspan="2">
                    <h4 class="dataLabel">
                        <bean:message bundle="core.labels" key="core.transaction.clienttransaction.infoclienttransactionform.titleTerminal"/>
                    </h4>
                </td>
            </tr>

            <tr>
                <td class="dataLabel" width="15%">
                    <tmx_ctrl:writeEntityLabel attrName="serialNumber"
                                               entityClassName="com.tmx.as.entities.bill.terminal.Terminal"/>:
                </td>
                <td class="dataField" width="35%">
                    <tmx_ctrl:editBox property="editBoxTerminalSN" width="200px"/>
                </td>

                <td class="dataLabel" width="15%">
                    <tmx_ctrl:writeEntityLabel attrName="registrationDate"
                                               entityClassName="com.tmx.as.entities.bill.terminal.Terminal"/>:
                </td>
                <td class="dataField" width="35%">
                    <tmx_ctrl:timeSelector property="timeSelectorTermRegDate"/>
                </td>
            </tr>


        </table>
        </td>
    </tr>

    <!--voucher data-->
    <nested:define id="editBoxVoucherId" property="editBoxVoucherId" type="com.tmx.web.controls.EditBox"/>
    <%if(editBoxVoucherId.getValue() != null ){%>
        <tr>
            <td valign="center" align="center" class="tabForm">
                <table width="100%" border="0">
            <%--<!-- Tab header -->--%>
                    <tr>
                        <td colspan="3">
                            <h4 class="dataLabel">
                                    <bean:message bundle="core.labels" key="core.transaction.clienttransaction.infoclienttransactionform.titleVoucher"/>
                            </h4>
                        </td>
                    </tr>
                    <tr>
                        <td class="dataLabel" width="15%">
                            <tmx_ctrl:writeEntityLabel attrName="VoucherId"
                                                       entityClassName="com.tmx.as.entities.bill.voucher.Voucher"/>:
                        </td>
                        <td class="dataField" width="35%">
                            <tmx_ctrl:editBox property="editBoxVoucherId"/>
                        </td>

                        <td class="dataLabel" width="15%">
                            <tmx_ctrl:writeEntityLabel attrName="code"
                                                       entityClassName="com.tmx.as.entities.bill.voucher.Voucher"/>:
                        </td>
                        <td class="dataField" width="35%">
                            <tmx_ctrl:editBox property="editBoxVoucherCode"/>
                        </td>
                    </tr>
                    <tr>
                        <td class="dataLabel" width="15%">
                            <tmx_ctrl:writeEntityLabel attrName="secretCode"
                                                       entityClassName="com.tmx.as.entities.bill.voucher.Voucher"/>:
                        </td>
                        <td class="dataField" width="35%">
                            <tmx_ctrl:editBox property="editBoxVoucherSecretCode"/>
                        </td>

                        <td class="dataLabel" width="15%">
                            <tmx_ctrl:writeEntityLabel attrName="nominal"
                                                       entityClassName="com.tmx.as.entities.bill.voucher.Voucher"/>:
                        </td>
                        <td class="dataField" width="35%">
                            <tmx_ctrl:editBox property="editBoxVoucherNominal"/>
                        </td>
                    </tr>
                    <tr>
                        <td class="dataLabel" width="15%">
                            <tmx_ctrl:writeEntityLabel attrName="status"
                                                       entityClassName="com.tmx.as.entities.bill.voucher.Voucher"/>:
                        </td>
                        <td class="dataField" width="35%">
                            <tmx_ctrl:editBox property="editBoxVoucherStatus"/>
                        </td>
                            
                        <td class="dataLabel" width="15%">
                            <tmx_ctrl:writeEntityLabel attrName="soldTime"
                                                       entityClassName="com.tmx.as.entities.bill.voucher.Voucher"/>:
                        </td>
                        <td class="dataField" width="35%">
                            <tmx_ctrl:editBox property="editBoxVoucherSoldTime"/>
                        </td>
                    </tr>

                </table>
            </td>
        </tr>
    <%}%>

    <%--<!-- BUTTONS -->--%>
    <tr>
        <td style="text-align: left">
            <tmx_ctrl:button property="onCancelButton"/>
        </td>
    </tr>
</table>
</html:form>