package com.tmx.as.entities.network.territory;

import com.tmx.as.base.BasicEntity;

import java.io.Serializable;

/**

 */
public class Country extends BasicEntity {
    private Long countryId;
    private String shortName;
    private String name;


    public Long getCountryId() {
        return countryId;
    }

    public void setCountryId(Long countryId) {
        this.countryId = countryId;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Serializable getId() {
        return getCountryId();
    }

    public int getValid() {
        return this.valid;
    }

    public void setValid(int valid) {
        this.valid = valid;
    }
}
