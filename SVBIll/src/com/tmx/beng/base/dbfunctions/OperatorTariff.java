package com.tmx.beng.base.dbfunctions;

import com.tmx.beng.base.cache.InvokeFunctionException;
import com.tmx.beng.base.BillingMessage;
import com.tmx.beng.base.Reservation;
import com.tmx.as.entities.bill.transaction.TransactionType;

import java.util.Map;
import java.util.List;

/**
    Operator tariff.
    This tariff do debit of operator balance with transaction amount increased on transaction cost.
 */
public class OperatorTariff extends BasicTariffFunction {

    boolean isTariffApplicable(Map actualParams) {
        return BillingMessage.O_REFILL_PAYMENT.equals(task.getProcesedMessage().getOperationName());
    }

    public ValidationError[] validate(Map actualParams) {
        List result = ParameterValidator.validateDiscountPercent(actualParams);
        return (ValidationError[]) result.toArray(new ValidationError[result.size()]);            
    }

    //
    public TariffParameter[] getDefaultParams() {
        return new TariffParameter[0];  
    }

    void applyTariff(Map actualParams, Reservation reservation) throws InvokeFunctionException {
        try{
            final String serviceCode = task.getProcesedMessage().getAttributeString(BillingMessage.SERVICE_CODE);
            final String operatorTariff = (String)actualParams.get(serviceCode);
            if(operatorTariff == null)
                throw new InvokeFunctionException("Tariff is not set for operator " + serviceCode);
            Double tariff = new Double(operatorTariff);
            Double amount = new Double(task.getProcesedMessage().getAttributeString(BillingMessage.AMOUNT));
            reservation.reserve(null, amount.doubleValue() + tariff.doubleValue(), null, TransactionType.CODE_CREDIT);
        }
        catch(Throwable e){
            throw new InvokeFunctionException(e.getLocalizedMessage(), e);
        }
    }
}
