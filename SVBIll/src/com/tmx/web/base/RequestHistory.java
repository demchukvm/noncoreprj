package com.tmx.web.base;

import com.tmx.util.StringUtil;


/**
 * Used to support navigation along requests's history
 */
public class RequestHistory {
    /** The oldest request URL is in the 0 position, the newest ain the last position. */
    String[] requestHistory = new String[0];
    int currentPosition = -1;

    public boolean hasPrev(){
        return (currentPosition > 0);
    }

    public boolean hasNext(){
        return (currentPosition < requestHistory.length - 1);
    }

    public String next(){
        if(hasNext()){
            currentPosition++;
            return requestHistory[currentPosition];
        }
        else
            return null;
    }

    public String prev(){
        if(hasPrev()){
            currentPosition--;
            return requestHistory[currentPosition];
        }
        else{
            return null;
        }
    }

    /** You should call this method every time then new request is caming */
    public void handleNewRequest(String newRequest){
        if(newRequest.indexOf("history=prev") >0){
            prev();
            return;
        }

        if(newRequest.indexOf("history=next") >0){
            next();
            return;
        }

        if(newRequest.indexOf("history=current") >0){
            return;
        }

        if(currentPosition == -1){
            requestHistory = new String[1];
            requestHistory[0] = newRequest;
            currentPosition++;
            return;
        }

        if(hasNext()){
            if(requestHistory[currentPosition+1].equals(newRequest)){
                currentPosition++;
                return;
            }
            else{
                String[] tmpHistory = new String[currentPosition+2];//allocate array with length (currentPosition+1) + one position for new request
                System.arraycopy(requestHistory, 0, tmpHistory, 0, currentPosition+1);
                tmpHistory[currentPosition+1] = newRequest;
                requestHistory = tmpHistory;
                currentPosition++;
                return;
            }
        }
        else{
            String[] tmpHistory = new String[requestHistory.length+1];
            System.arraycopy(requestHistory, 0, tmpHistory, 0, requestHistory.length);
            tmpHistory[tmpHistory.length-1] = newRequest;
            requestHistory = tmpHistory;
            currentPosition++;
            return;
        }
    }

    public void clear(){
        requestHistory = new String[0];
        currentPosition = -1;
    }

    public String getCurrent(){
        return (currentPosition == -1) ? null : (StringUtil.addUrlParam(requestHistory[currentPosition], "history=current"));
    }

    public String getNext(){
        return (hasNext()) ? (StringUtil.addUrlParam(requestHistory[currentPosition+1], "history=next")) : null;
    }

    public String getPrev(){
        return (hasPrev()) ? (StringUtil.addUrlParam(requestHistory[currentPosition-1], "history=prev")) : null;
    }

}
