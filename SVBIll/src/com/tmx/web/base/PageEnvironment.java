package com.tmx.web.base;

import org.apache.struts.taglib.html.Constants;
import javax.servlet.jsp.PageContext;

/**
 */
public class PageEnvironment {

    /** Returns current action form. It was previosly stored in the
     * page context by html:form tag (FormTag.initFormBean()). */
    public static BasicActionForm getCurrentForm(PageContext pageContext){
        return (BasicActionForm)pageContext.getAttribute(Constants.BEAN_KEY, PageContext.REQUEST_SCOPE);
    }
}
