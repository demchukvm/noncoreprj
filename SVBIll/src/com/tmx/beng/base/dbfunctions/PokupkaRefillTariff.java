package com.tmx.beng.base.dbfunctions;

import com.tmx.beng.base.BillingMessage;
import com.tmx.beng.base.Reservation;
import com.tmx.beng.base.cache.InvokeFunctionException;
import com.tmx.as.entities.bill.transaction.TransactionType;

import java.util.Map;
import java.util.List;
import java.util.ArrayList;

public class PokupkaRefillTariff extends BasicTariffFunction {
    boolean isTariffApplicable(Map actualParams) {
        return BillingMessage.O_REFILL_PAYMENT.equals(task.getProcesedMessage().getOperationName());
    }

    void applyTariff(Map actualParams, Reservation reservation) throws InvokeFunctionException {
        try{
            final String serviceCode = task.getProcesedMessage().getAttributeString(BillingMessage.SERVICE_CODE);
            RefillCodes refillCodes = new RefillCodes();
            final String paramName = (String)(refillCodes.getServiceCodeParamMapping().get(serviceCode));
            if(paramName == null)
                throw new InvokeFunctionException("err.tariff.unsupported_service_code", new String[]{});

            double discontPercent = Double.parseDouble((String)actualParams.get(paramName));
            double amount = Double.parseDouble(task.getProcesedMessage().getAttributeString(BillingMessage.AMOUNT));
            double tarrifedAmount = amount * (100 - discontPercent) / 100;

            //prepare reservation on primary tariff
            reservation.reserve(null, tarrifedAmount, null, TransactionType.CODE_CREDIT);
        }
        catch(Throwable e){
            throw new InvokeFunctionException("err.seller_tariff.calc_error", e);
        }
    }

    public ValidationError[] validate(Map actualParams) {
        List resultErrors = new ArrayList();
        RefillCodes refillCodes = new RefillCodes();

        List<String> paramNames = new ArrayList<String>();
        paramNames.add(refillCodes.BEELINE_DISCOUNT);
        paramNames.add(refillCodes.KYIVSTYAR_DISCOUNT);
        paramNames.add(refillCodes.MTS_DISCOUNT);

        resultErrors.addAll(ParameterValidator.validateParamCount(actualParams, paramNames));

        return (ValidationError[]) resultErrors.toArray(new ValidationError[resultErrors.size()]);
    }

    public BasicFunction.TariffParameter[] getDefaultParams() {
        RefillCodes refillCodes = new RefillCodes();

        BasicFunction.TariffParameter paramBelineDiscount = new BasicFunction.TariffParameter();
        paramBelineDiscount.setName(refillCodes.BEELINE_DISCOUNT);
        paramBelineDiscount.setValue("2.30");
        paramBelineDiscount.setOrderNum(new Long(3));
        paramBelineDiscount.setMandatory(Boolean.FALSE);

    	BasicFunction.TariffParameter paramKyivstarDiscount = new BasicFunction.TariffParameter();
    	paramKyivstarDiscount.setName(refillCodes.KYIVSTYAR_DISCOUNT);
    	paramKyivstarDiscount.setValue("3.30");
        paramKyivstarDiscount.setOrderNum(new Long(1));
        paramKyivstarDiscount.setMandatory(Boolean.FALSE);

    	BasicFunction.TariffParameter paramMTSDiscount = new BasicFunction.TariffParameter();
    	paramMTSDiscount.setName(refillCodes.MTS_DISCOUNT);
    	paramMTSDiscount.setValue("3.30");
        paramMTSDiscount.setOrderNum(new Long(2));
        paramMTSDiscount.setMandatory(Boolean.FALSE);


        return new BasicFunction.TariffParameter[] {paramKyivstarDiscount, paramMTSDiscount, paramBelineDiscount};
    }
}
