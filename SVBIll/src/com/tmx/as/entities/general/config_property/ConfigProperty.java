package com.tmx.as.entities.general.config_property;

import com.tmx.as.base.BasicEntity;

import java.io.Serializable;

/** Entity to work with properties stored in the DB */
public class ConfigProperty extends BasicEntity {
    private Long configPropertyId;
    private String key;
    private String value;

    public Long getConfigPropertyId() {
        return configPropertyId;
    }

    public void setConfigPropertyId(Long configPropertyId) {
        this.configPropertyId = configPropertyId;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Serializable getId() {
        return getConfigPropertyId();
    }

    public int getValid() {
        return valid;
    }

    public void setValid(int valid) {
        this.valid = valid;
    }

}
