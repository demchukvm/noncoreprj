package com.tmx.web.actions.core.dictionary.news;

import com.tmx.as.base.criterion.CriterionWrapper;
import com.tmx.as.base.criterion.Restrictions;
import com.tmx.web.actions.core.AbstractSvbillAction;
import com.tmx.web.controls.CriterionFactory;
import com.tmx.web.forms.core.dictionary.news.NewsForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForm;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.tmx.web.base.BasicDispatchedAction;

import java.util.List;

public class NewsAction extends AbstractSvbillAction {

    private final String FORWARD = "news";

    protected ActionForward defaultAction(ActionMapping mapping,
                                          ActionForm form,
                                          HttpServletRequest request,
                                          HttpServletResponse response,
                                          String methodName) throws Throwable {

        NewsForm newsForm = (NewsForm) form;

        //final String userId = getLoggedInUser(request).getUserId().toString();
        final String userLogin = getLoggedInUser(request).getLogin();

        CriterionFactory criterionFactory = new CriterionFactory() {
            public CriterionWrapper createCriterion() {
                return Restrictions.in("observers", new Object[] {userLogin, "all"});
            }
        };

        newsForm.getNewsMessagesTable().setCriterionFactory(criterionFactory);

        /*
        высчитывать даты

         */


        newsForm.getNewsMessagesTable().refresh();

        return mapping.findForward(FORWARD);
    }


}
