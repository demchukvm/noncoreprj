package com.tmx.as.blogic.balance;

import com.tmx.as.entities.bill.balance.AbstractBalance;
import com.tmx.as.entities.bill.balance.SellerBalance;
import com.tmx.as.entities.bill.restriction.AbstractRestriction;
import com.tmx.as.entities.bill.restriction.SellerRestriction;
import com.tmx.as.entities.bill.transaction.SellerBalanceTransaction;
import com.tmx.as.entities.general.user.User;
import com.tmx.as.exceptions.DatabaseException;
import com.tmx.as.blogic.SellerManager;
import com.tmx.as.blogic.BlogicException;
import com.tmx.as.base.criterion.CriterionWrapper;
import com.tmx.as.base.criterion.Restrictions;
import com.tmx.as.base.FilterWrapper;
import com.tmx.as.base.ProjectionWrapper;
import com.tmx.as.base.OrderWrapper;
import com.tmx.beng.csapi.java.base.BillingClientApi;
import com.tmx.util.InitException;
import com.tmx.web.controls.table.FilterSet;

import java.util.List;


public class SellerBalanceManager extends AbstractBalanceManager{

    public SellerBalanceManager() {
        super();
    }

    public void deleteBalance(Long balanceId) throws DatabaseException{
        DELETE(SellerBalance.class, balanceId);
    }

    public void deleteRestriction(Long restrictionId) throws DatabaseException {
        DELETE(SellerRestriction.class, restrictionId);
    }

    public AbstractBalance getBalance(Long balanceId) throws DatabaseException {
        return (AbstractBalance)RETRIEVE(SellerBalance.class, balanceId, new String[]{"seller"});
    }

    public AbstractBalance getBalanceByCode(String balanceCode) throws DatabaseException {
        CriterionWrapper wrappers[] = new CriterionWrapper[] {
                                        Restrictions.eq("code", balanceCode)};
        return (SellerBalance)RETRIEVE(SellerBalance.class, null, wrappers, null);
    }

    public void saveBalance(AbstractBalance balance) throws DatabaseException{
        SAVE(balance);
    }

    public List getAllOwner(User loggedInUser) throws DatabaseException {
        return new SellerManager().getNestedSellersInSeller(loggedInUser);
    }

    public AbstractRestriction getRestriction(Long restrictionId) throws DatabaseException {
        String[] attr = {"seller","functionInstance.functionType"};
        return (AbstractRestriction) RETRIEVE(SellerRestriction.class, restrictionId, attr);
    }

    public void saveRestriction(AbstractRestriction restriction) throws DatabaseException {
        SAVE(restriction.getFunctionInstance(), new String[]{"functionType"});
        SAVE(restriction);
    }

    public void changeBalance(BalanceModificationConfig config) throws BlogicException, InitException, DatabaseException {
        config.validateConfiguration();

        SellerBalance balance = (SellerBalance) getBalance(config.getBalanceId());

        BillingClientApi.WebSellerRefillMessage message = billingClient.createSellerRefillMessage();
        message.setSellerCode( balance.getSeller().getCode() );
        message.setTransDescription( config.getDescription() );
        message.setUserId( config.getUserActorId().toString() );
        message.setTransactionType( config.getType().getCode() );
        message.setAmount( config.getAmount().toString() );
        message.setPaymentReason( config.getPaymentReason() );
        message.setBalanceCode( balance.getCode() );

        message.send();

    }

    public List getSummaryAmount(FilterWrapper [] filters, CriterionWrapper [] criterionWrappers) throws DatabaseException {
        ProjectionWrapper projectionSumAmount = new ProjectionWrapper("amount", "amount", ProjectionWrapper.SUM);
        ProjectionWrapper[] projectionsSumAmount = new ProjectionWrapper[]{projectionSumAmount};

        return  RETRIEVE_ALL(SellerBalanceTransaction.class, filters, criterionWrappers, null,
                            null, projectionsSumAmount, -1, -1);

    }

}
