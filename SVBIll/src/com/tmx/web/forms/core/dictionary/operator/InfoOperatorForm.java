package com.tmx.web.forms.core.dictionary.operator;

import com.tmx.web.base.BasicActionForm;
import com.tmx.web.controls.*;
import com.tmx.as.entities.bill.processing.Processing;
import com.tmx.as.entities.bill.bank.BankAccount;
import com.tmx.as.entities.bill.bank.Bank;
import com.tmx.as.entities.bill.contract.Contract;
import com.tmx.as.entities.bill.address.Address;
import com.tmx.as.entities.bill.person.Person;
import com.tmx.as.entities.bill.operator.Operator;
import com.tmx.as.entities.general.user.User;
import com.tmx.as.exceptions.DatabaseException;
import com.tmx.as.blogic.ProcessingManager;

import java.util.Date;

/**  ~
 * Created by IntelliJ IDEA.
 * User: Demchuk
 * Date: 24.01.2008
 * Time: 15:20:23
 * To change this template use File | Settings | File Templates.
 */
public class InfoOperatorForm extends BasicActionForm {
    private Long physAddressId;
    private Long legalAddressId;
    private Long bankId;
    private Long directorId;
    private Long accountantId;
    private Long bankAccountId;
    private Long contractId;

    private EditBox editBoxOperatorId;

    private EditBox editBoxName;
    private EditBox editBoxCode;

    private EditBox editBoxZipCodePhys;
    private EditBox editBoxAddressPhys;
    private EditBox editBoxCityPhys;
    private EditBox editBoxBuildingPhys;
    private EditBox editBoxOfficePhys;

    private EditBox editBoxZipCodeLegal;
    private EditBox editBoxAddressLegal;
    private EditBox editBoxCityLegal;
    private EditBox editBoxBuildingLegal;
    private EditBox editBoxOfficeLegal;

    //bank
    private EditBox editBoxBankName;
    private EditBox editBoxBankMfo;
    private EditBox editBoxBankAccountNumber;

    //add information
    private TimeSelector timeSelectorSignatureDate;
    private EditBox editBoxSymbol;

    //codes
    private EditBox editBoxCertificateNumber;
    private EditBox editBoxInn;
    private EditBox editBoxOkpo;

    private EditBox editBoxDirectorEmail;
    private EditBox editBoxDirectorPhone;

    private EditBox editBoxAccountantEmail;
    private EditBox editBoxAccountantPhone;

    private TimeSelector timeSelectorRegistrationDate;
    private TextArea textAreaDescription;

    private ScrollBox scrollBoxParentSellers;
    private ScrollBox scrollBoxProcessings;

    private CheckBox checkBoxActive;

    private Button onApplyButton;
    private Button onSaveButton;
    private Button onResetButton;
    private Button onCancelButton;

    //we must populated seller befor populate Contract
    private Long selectedOperatorId;

    protected void init() {
        initIds();
        initEditBoxes();
        initTextAreas();
        initTimeSelectors();
        initButtons();
    }

    //TODO:AN move to control(EditBox)
    private void initIds(){
        physAddressId = null;
        legalAddressId = null;
        bankId = null;
        directorId = null;
        accountantId = null;
        bankAccountId = null;
        contractId = null;
    }

    private void initButtons() {
        onSaveButton = new Button();
        onSaveButton.setLabelKey("ctrl.button.label.save");
        onSaveButton.setAction("/core/dictionary/operator/infoOperator");
        onSaveButton.setCommand("save");

        onResetButton = new Button();
        onResetButton.setLabelKey("ctrl.button.label.reset");

        onCancelButton = new Button();
        onCancelButton.setLabelKey("ctrl.button.label.cancel");
        onCancelButton.setAction("/core/dictionary/operator/manageOperators");

        onApplyButton = new Button();
        onApplyButton.setLabelKey("ctrl.button.label.apply");
        onApplyButton.setAction("/core/dictionary/operator/infoOperator");
        onApplyButton.setCommand("apply");
    }

    private void initTimeSelectors() {
        timeSelectorRegistrationDate = new TimeSelector();
        timeSelectorRegistrationDate.setReadonly(true);
    }

    private void initTextAreas() {
        textAreaDescription = new TextArea();
    }

    private void initEditBoxes() {
        editBoxOperatorId = new EditBox();
        editBoxName = new EditBox();
        editBoxName.setMandatory(true);
        editBoxCode = new EditBox();
        editBoxCode.setMandatory(true);

        editBoxZipCodePhys = new EditBox();
        editBoxAddressPhys = new EditBox();
        editBoxCityPhys = new EditBox();
        editBoxBuildingPhys = new EditBox();
        editBoxOfficePhys = new EditBox();

        editBoxZipCodeLegal = new EditBox();
        editBoxAddressLegal = new EditBox();
        editBoxCityLegal = new EditBox();
        editBoxBuildingLegal = new EditBox();
        editBoxOfficeLegal = new EditBox();
    }


    public void bindData(Operator operator, User loggedInUser) throws DatabaseException {
        timeSelectorRegistrationDate.setTime( operator.getRegistrationDate() );
        editBoxOperatorId.setValue(operator.getOperatorId() == null ? "" : operator.getOperatorId().toString());
        textAreaDescription.setValue(operator.getDescription());
        editBoxName.setValue(operator.getName());
        editBoxCode.setValue(operator.getCode());
        timeSelectorRegistrationDate.setTime(operator.getRegistrationDate());

    }


    public Operator populateOperator() {
        Operator operator = new Operator();
        operator.setOperatorId(PopulateUtil.getLongValue(editBoxOperatorId));
        operator.setDescription(textAreaDescription.getValue());
        operator.setRegistrationDate(new Date());
        operator.setName(editBoxName.getValue());
        operator.setCode(editBoxCode.getValue());
        return operator;
    }


    public Long getSelectedOperatorId() {
        return selectedOperatorId;
    }

    public void setSelectedOperatorId(Long selectedOperatorId) {
        this.selectedOperatorId = selectedOperatorId;
    }

    public EditBox getEditBoxOperatorId() {
        return editBoxOperatorId;
    }

    public void setEditBoxOperatorId(EditBox editBoxOperatorId) {
        this.editBoxOperatorId = editBoxOperatorId;
    }

    public EditBox getEditBoxName() {
        return editBoxName;
    }

    public void setEditBoxName(EditBox editBoxName) {
        this.editBoxName = editBoxName;
    }

    public EditBox getEditBoxCode() {
        return editBoxCode;
    }

    public void setEditBoxCode(EditBox editBoxCode) {
        this.editBoxCode = editBoxCode;
    }

    public TextArea getTextAreaDescription() {
        return textAreaDescription;
    }

    public void setTextAreaDescription(TextArea textAreaDescription) {
        this.textAreaDescription = textAreaDescription;
    }

    public Button getOnApplyButton() {
        return onApplyButton;
    }

    public void setOnApplyButton(Button onApplyButton) {
        this.onApplyButton = onApplyButton;
    }

    public Button getOnSaveButton() {
        return onSaveButton;
    }

    public void setOnSaveButton(Button onSaveButton) {
        this.onSaveButton = onSaveButton;
    }

    public Button getOnResetButton() {
        return onResetButton;
    }

    public void setOnResetButton(Button onResetButton) {
        this.onResetButton = onResetButton;
    }

    public Button getOnCancelButton() {
        return onCancelButton;
    }

    public void setOnCancelButton(Button onCancelButton) {
        this.onCancelButton = onCancelButton;
    }

    public TimeSelector getTimeSelectorRegistrationDate() {
        return timeSelectorRegistrationDate;
    }

    public void setTimeSelectorRegistrationDate(TimeSelector timeSelectorRegistrationDate) {
        this.timeSelectorRegistrationDate = timeSelectorRegistrationDate;
    }

    public ScrollBox getScrollBoxParentSellers() {
        return scrollBoxParentSellers;
    }

    public void setScrollBoxParentSellers(ScrollBox scrollBoxParentSellers) {
        this.scrollBoxParentSellers = scrollBoxParentSellers;
    }

    public CheckBox getCheckBoxActive() {
        return checkBoxActive;
    }

    public void setCheckBoxActive(CheckBox checkBoxActive) {
        this.checkBoxActive = checkBoxActive;
    }

    public EditBox getEditBoxZipCodePhys() {
        return editBoxZipCodePhys;
    }

    public void setEditBoxZipCodePhys(EditBox editBoxZipCodePhys) {
        this.editBoxZipCodePhys = editBoxZipCodePhys;
    }

    public EditBox getEditBoxAddressPhys() {
        return editBoxAddressPhys;
    }

    public void setEditBoxAddressPhys(EditBox editBoxAddressPhys) {
        this.editBoxAddressPhys = editBoxAddressPhys;
    }

    public EditBox getEditBoxCityPhys() {
        return editBoxCityPhys;
    }

    public void setEditBoxCityPhys(EditBox editBoxCityPhys) {
        this.editBoxCityPhys = editBoxCityPhys;
    }

    public EditBox getEditBoxBuildingPhys() {
        return editBoxBuildingPhys;
    }

    public void setEditBoxBuildingPhys(EditBox editBoxBuildingPhys) {
        this.editBoxBuildingPhys = editBoxBuildingPhys;
    }

    public EditBox getEditBoxOfficePhys() {
        return editBoxOfficePhys;
    }

    public void setEditBoxOfficePhys(EditBox editBoxOfficePhys) {
        this.editBoxOfficePhys = editBoxOfficePhys;
    }

    public EditBox getEditBoxZipCodeLegal() {
        return editBoxZipCodeLegal;
    }

    public void setEditBoxZipCodeLegal(EditBox editBoxZipCodeLegal) {
        this.editBoxZipCodeLegal = editBoxZipCodeLegal;
    }

    public EditBox getEditBoxAddressLegal() {
        return editBoxAddressLegal;
    }

    public void setEditBoxAddressLegal(EditBox editBoxAddressLegal) {
        this.editBoxAddressLegal = editBoxAddressLegal;
    }

    public EditBox getEditBoxCityLegal() {
        return editBoxCityLegal;
    }

    public void setEditBoxCityLegal(EditBox editBoxCityLegal) {
        this.editBoxCityLegal = editBoxCityLegal;
    }

    public EditBox getEditBoxBuildingLegal() {
        return editBoxBuildingLegal;
    }

    public void setEditBoxBuildingLegal(EditBox editBoxBuildingLegal) {
        this.editBoxBuildingLegal = editBoxBuildingLegal;
    }

    public EditBox getEditBoxOfficeLegal() {
        return editBoxOfficeLegal;
    }

    public void setEditBoxOfficeLegal(EditBox editBoxOfficeLegal) {
        this.editBoxOfficeLegal = editBoxOfficeLegal;
    }

    public Long getPhysAddressId() {
        return physAddressId;
    }

    public void setPhysAddressId(Long physAddressId) {
        this.physAddressId = physAddressId;
    }

    public Long getLegalAddressId() {
        return legalAddressId;
    }

    public void setLegalAddressId(Long legalAddressId) {
        this.legalAddressId = legalAddressId;
    }

    public EditBox getEditBoxBankName() {
        return editBoxBankName;
    }

    public void setEditBoxBankName(EditBox editBoxBankName) {
        this.editBoxBankName = editBoxBankName;
    }

    public EditBox getEditBoxBankMfo() {
        return editBoxBankMfo;
    }

    public void setEditBoxBankMfo(EditBox editBoxBankMfo) {
        this.editBoxBankMfo = editBoxBankMfo;
    }

    public EditBox getEditBoxBankAccountNumber() {
        return editBoxBankAccountNumber;
    }

    public void setEditBoxBankAccountNumber(EditBox editBoxBankAccountNumber) {
        this.editBoxBankAccountNumber = editBoxBankAccountNumber;
    }

    public TimeSelector getTimeSelectorSignatureDate() {
        return timeSelectorSignatureDate;
    }

    public void setTimeSelectorSignatureDate(TimeSelector timeSelectorSignatureDate) {
        this.timeSelectorSignatureDate = timeSelectorSignatureDate;
    }

    public EditBox getEditBoxSymbol() {
        return editBoxSymbol;
    }

    public void setEditBoxSymbol(EditBox editBoxSymbol) {
        this.editBoxSymbol = editBoxSymbol;
    }

    public EditBox getEditBoxCertificateNumber() {
        return editBoxCertificateNumber;
    }

    public void setEditBoxCertificateNumber(EditBox editBoxCertificateNumber) {
        this.editBoxCertificateNumber = editBoxCertificateNumber;
    }

    public EditBox getEditBoxInn() {
        return editBoxInn;
    }

    public void setEditBoxInn(EditBox editBoxInn) {
        this.editBoxInn = editBoxInn;
    }

    public EditBox getEditBoxOkpo() {
        return editBoxOkpo;
    }

    public void setEditBoxOkpo(EditBox editBoxOkpo) {
        this.editBoxOkpo = editBoxOkpo;
    }

    public EditBox getEditBoxDirectorEmail() {
        return editBoxDirectorEmail;
    }

    public void setEditBoxDirectorEmail(EditBox editBoxDirectorEmail) {
        this.editBoxDirectorEmail = editBoxDirectorEmail;
    }

    public EditBox getEditBoxDirectorPhone() {
        return editBoxDirectorPhone;
    }

    public void setEditBoxDirectorPhone(EditBox editBoxDirectorPhone) {
        this.editBoxDirectorPhone = editBoxDirectorPhone;
    }

    public EditBox getEditBoxAccountantEmail() {
        return editBoxAccountantEmail;
    }

    public void setEditBoxAccountantEmail(EditBox editBoxAccountantEmail) {
        this.editBoxAccountantEmail = editBoxAccountantEmail;
    }

    public EditBox getEditBoxAccountantPhone() {
        return editBoxAccountantPhone;
    }

    public void setEditBoxAccountantPhone(EditBox editBoxAccountantPhone) {
        this.editBoxAccountantPhone = editBoxAccountantPhone;
    }

    public Long getBankId() {
        return bankId;
    }

    public void setBankId(Long bankId) {
        this.bankId = bankId;
    }

    public Long getDirectorId() {
        return directorId;
    }

    public void setDirectorId(Long directorId) {
        this.directorId = directorId;
    }

    public Long getAccountantId() {
        return accountantId;
    }

    public void setAccountantId(Long accountantId) {
        this.accountantId = accountantId;
    }

    public Long getBankAccountId() {
        return bankAccountId;
    }

    public void setBankAccountId(Long bankAccountId) {
        this.bankAccountId = bankAccountId;
    }

     public ScrollBox getScrollBoxProcessings() {
        return scrollBoxProcessings;
    }

    public void setScrollBoxProcessings(ScrollBox scrollBoxProcessings) {
        this.scrollBoxProcessings = scrollBoxProcessings;
    }

    public Long getContractId() {
        return contractId;
    }

    public void setContractId(Long contractId) {
        this.contractId = contractId;
    }

}
