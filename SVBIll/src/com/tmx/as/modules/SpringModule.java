package com.tmx.as.modules;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;

import java.util.Properties;

import com.tmx.util.Configuration;


public class SpringModule extends AbstractModule{

    private static ApplicationContext ac;

    public void start(Properties props) throws ModuleException {
        Configuration cfg = Configuration.getInstance();        
        String springCfg = cfg.getProperty("spring.config_file");
        ac = new FileSystemXmlApplicationContext(new String[]{springCfg});
    }

    public void shutdownGraceful(Properties props) throws ModuleException {

    }

    public void shutdownAbort(Properties props) throws ModuleException {

    }

    public static ApplicationContext getApplicationContext() {
        return ac;
    }

}
