<%@ page import="com.tmx.web.forms.core.dictionary.user.UserInfoForm" %>
<%@ page language="java" pageEncoding="UTF-8" contentType="text/html; charset=utf-8"
         errorPage="/core/jsp/message/jsperror.jsp" isELIgnored="false" %>
<%@ taglib uri="/WEB-INF/tld/struts-nested.tld" prefix="nested" %>
<%@ taglib uri="/WEB-INF/tld/tmx-controls.tld" prefix="tmx_ctrl" %>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>


<html:form action="/core/dictionary/user/userInfo">
<table border="0" width="100%">
    <!-- BUTTONS -->
    <tr>
        <td style="text-align: left">
            <tmx_ctrl:button property="onSaveButton"/>
            <tmx_ctrl:button property="onResetButton" onClick="javascript:document.forms[0].reset()"/>
            <tmx_ctrl:button property="onCancelButton"/>
        </td>
    </tr>
    <tr>
        <td valign="center" align="center" class="tabForm">
        <table width="100%" border="0">
            <!-- Form header -->
            <tr>
                <td colspan="2">
                    <h4 class="dataLabel">
                        <bean:message bundle="core.labels" key="core.page.dictionary.user.userinfoform.title"/>
                    </h4>
                </td>
            </tr>
            <tr>
                <!-- ID -->
                <td class="dataLabel" width="15%">
                    <tmx_ctrl:writeEntityLabel attrName="userId"
                                               entityClassName="com.tmx.as.entities.general.user.User"/>:
                </td>
                <td class="dataField" width="35%">
                    <tmx_ctrl:editBox property="editBoxUserId"/>
                </td>
                <!-- LOGIN -->
                <td class="dataLabel" width="15%">
                    <tmx_ctrl:writeEntityLabel attrName="login"
                                               entityClassName="com.tmx.as.entities.general.user.User"/>:
                </td>
                <td class="dataField" width="35%">
                    <tmx_ctrl:editBox property="editBoxLogin"/>
                </td>
            </tr>

            <tr>
                <!-- LASTNAME -->
                <td class="dataLabel">
                    <tmx_ctrl:writeEntityLabel attrName="lastName"
                                               entityClassName="com.tmx.as.entities.general.user.User"/>:
                </td>
                <td class="dataField">
                    <tmx_ctrl:editBox property="editBoxLastName"/>
                </td>
                <!-- PASSWORD -->
                <td class="dataLabel">
                    <tmx_ctrl:writeEntityLabel attrName="password"
                                               entityClassName="com.tmx.as.entities.general.user.User"/>:
                </td>
                <td class="dataField">
                    <tmx_ctrl:passwordEditBox property="editBoxPassword"/>
                </td>
            </tr>

            <tr>
                <!-- FIRSTNAME -->
                <td class="dataLabel">
                    <tmx_ctrl:writeEntityLabel attrName="firstName"
                                               entityClassName="com.tmx.as.entities.general.user.User"/>:
                </td>
                <td class="dataField">
                    <tmx_ctrl:editBox property="editBoxFirstName"/>
                </td>
                <!-- PASSWORD 2 -->
                <td class="dataLabel">
                    <bean:message bundle="core.labels" key="core.page.dictionary.user.userinfoform.label.password2"/>:
                </td>
                <td class="dataField">
                    <tmx_ctrl:passwordEditBox property="editBoxPassword2"/>
                </td>
            </tr>

            <tr>
                <!-- EMAIL -->
                <td class="dataLabel">
                    <tmx_ctrl:writeEntityLabel attrName="email"
                                               entityClassName="com.tmx.as.entities.general.user.User"/>:
                </td>
                <td class="dataField">
                    <tmx_ctrl:editBox property="editBoxEmail"/>
                </td>
                <!-- ACTIVE -->                
                <td class="dataLabel">
                    <tmx_ctrl:writeEntityLabel attrName="active"
                                               entityClassName="com.tmx.as.entities.general.user.User"/>:
                </td>
                <td class="dataField">
                    <%--<tmx_ctrl:checkBox property="checkBoxActive"/>--%>
                    <%
                        boolean flag = ((UserInfoForm) pageContext.getSession().getAttribute("core.dictionary.user.userInfoForm")).isCheckBoxActive();
                        if(flag == true)
                        {
                    %>
                        <input type="checkbox" name="_checkBoxActive" checked="checked"/>
                    <%
                        }
                        else
                        {
                    %>
                        <input type="checkbox" name="_checkBoxActive" />
                    <%
                        }
                    %>
                </td>
            </tr>

            <tr>
                <!-- ROLE -->
                <td class="dataLabel">
                    <tmx_ctrl:writeEntityLabel attrName="{@name}"
                                               entityClassName="com.tmx.as.entities.general.role.Role"/>:
                </td>
                <td class="dataField">
                    <tmx_ctrl:selectBox property="selectRoles"/>
                </td>
                <!-- ORGANIZATION -->
                <td class="dataLabel">
                    <tmx_ctrl:writeEntityLabel attrName="{@name}"
                                               entityClassName="com.tmx.as.entities.general.organization.Organization"/>:
                </td>
                <td class="dataField">
                    <tmx_ctrl:selectBox property="selectOrganizations"/>
                </td>
            </tr>

            <tr>
                <!-- SELLER -->
                <td class="dataLabel">
                    <tmx_ctrl:writeEntityLabel attrName="{@name}"
                                               entityClassName="com.tmx.as.entities.bill.seller.Seller"/>:
                </td>
                <td class="dataField">
                    <tmx_ctrl:selectBox property="scrollBoxSellers"/>
                </td>

                <td class="dataLabel">
                    <tmx_ctrl:writeEntityLabel attrName="creationDate"
                                               entityClassName="com.tmx.as.entities.general.user.User"/>:
                </td>
                <td class="dataField">
                    <tmx_ctrl:timeSelector property="timeSelectorCreateTime"/>
                </td>
            </tr>

        </table>
        </td>
    </tr>
    <!-- BUTTONS -->
    <tr>
        <td style="text-align: left">
            <tmx_ctrl:button property="onSaveButton"/>
            <tmx_ctrl:button property="onResetButton" onClick="javascript:document.forms[0].reset()"/>
            <tmx_ctrl:button property="onCancelButton"/>
        </td>
    </tr>
</table>
</html:form>