package com.tmx.as.blogic.tariff;

import com.tmx.as.blogic.BlogicException;
import com.tmx.as.entities.bill.tariff.AbstractTariff;
import com.tmx.as.exceptions.DatabaseException;
import com.tmx.as.base.EntityManager;

/**
 * Template method design pattern.
 */
abstract public class AbstractTariffManager extends EntityManager {

    //contains basic checking(for input parameters)
    public AbstractTariff getTariff(Long tariffId) throws DatabaseException, BlogicException{
        return getSpecificTariff(tariffId);
    }

    public void saveTariff(AbstractTariff tariff) throws DatabaseException, BlogicException{
        SAVE(tariff.getFunctionInstance());
        saveSpecificTariff(tariff);
    }

    public void deleteTariff(Long tariffId) throws DatabaseException, BlogicException{
        deleteSpecificTariff(tariffId);
    }

    //template methods
    abstract protected AbstractTariff getSpecificTariff(Long tariffId) throws DatabaseException, BlogicException;
    abstract protected void saveSpecificTariff(AbstractTariff tariff) throws DatabaseException, BlogicException;
    abstract protected void deleteSpecificTariff(Long tariffId) throws DatabaseException, BlogicException;

    /**
     * Copy tariff to new owner
     * @param tariffId -
     * @param destOwnerId - destination owner (Seller, Terminal)
     * @throws DatabaseException -
     * @throws BlogicException -
     */
    public void copyTariff(Long tariffId, Long destOwnerId) throws DatabaseException, BlogicException{
        if(tariffId == null || destOwnerId == null)
            throw new BlogicException("err.abstract_tariff_manager.copy_tariff.incorrect_method_parameter");   

        copySpecificTariff(tariffId, destOwnerId);
    }

    public void copyAllTariffs(Long srcOwnerId, Long destOwnerId) throws DatabaseException, BlogicException{
        if(srcOwnerId == null || destOwnerId == null)
            throw new BlogicException("err.abstract_tariff_manager.copy_all_tariff.incorrect_method_parameter");   

        copyAllSpecificTariffs(srcOwnerId, destOwnerId);
    }
    
    abstract protected void copySpecificTariff(Long tariffId, Long destOwnerId) throws DatabaseException, BlogicException;
    abstract protected void copyAllSpecificTariffs(Long srcOwnerId, Long destOwnerId) throws DatabaseException, BlogicException;

}
