package com.tmx.as.modules;

import com.tmx.as.jsse.JsseSimpleService;
import com.tmx.util.InitException;
import com.tmx.beng.iso8583Gate.ISO8583Service;

import java.util.Properties;

/**
 * Created by IntelliJ IDEA.
 * User: shake
 * Date: 25.06.2007
 * Time: 15:56:19
 */
public class ISO8583ServiceModule extends AbstractModule{

    public void start(Properties props)throws ModuleException{
        try{
            ISO8583Service isoService = ISO8583Service.getInstance();
            isoService.reload();
            isoService.start();
        }catch(InitException e){
            throw new ModuleException(e.getLocalizedMessage());
        }
    }

    public void shutdownGraceful(Properties props)throws ModuleException{
        ISO8583Service.getInstance().shutdown();
    }

    public void shutdownAbort(Properties props)throws ModuleException{
        ISO8583Service.getInstance().shutdown();
    }
}
