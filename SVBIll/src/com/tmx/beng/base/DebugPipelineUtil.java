package com.tmx.beng.base;

/**
 */
public class DebugPipelineUtil {

    public static StringBuffer dumpBillingMessage(BillingMessage msg, StringBuffer dump){
        dump.append("\n---BILL_MSG_DUMP---\n").
                append("\tOperation name: ").append(msg.getOperationName()).append("\n");
        String[] msgAttrsNames = msg.getAttributesNames();
        for(int i=0; i<msgAttrsNames.length; i++){
            dump.append("\tAttr[").append(msgAttrsNames[i]).append("]=").append(msg.getAttributeString(msgAttrsNames[i])).append("\n");
        }
        dump.append("---END_DUMP---");
        return dump;
    }
}

