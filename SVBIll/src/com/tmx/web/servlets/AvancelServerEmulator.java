package com.tmx.web.servlets;


import com.tmx.util.InitException;
import com.tmx.util.Configuration;
import com.tmx.util.HexConverter;
import com.tmx.util.StructurizedException;
import com.tmx.util.jce.Base64;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.io.*;
import java.security.Key;
import java.security.PublicKey;
import java.security.KeyStore;
import java.security.KeyFactory;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Arrays;
import java.nio.charset.Charset;
import java.net.URLEncoder;
import java.net.URLDecoder;

import org.apache.log4j.Logger;

/**

 */
public class AvancelServerEmulator extends HttpServlet {
    private static Encoder encoder;
    private final static Charset charset = Charset.forName("UTF-8");
    private final static Logger logger = Logger.getLogger("avancel_gate.AvancelServerEmulator");
    private enum Operation {AUTHORIZATION, REFILL, GETSTATUS, GETLASTRESPONSE, UNKNOWN}

    public void init() throws ServletException {
        try{
            encoder = new Encoder();
            encoder.init();
        }
        catch (InitException e){
            throw new ServletException("Failed to init servlet", e);
        }

    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try{
            //parse raw query
            Parser parser = new Parser();
            final String query = request.getQueryString();
            final String salesPointId = parser.fetchSalesPointIdFromRequest(query);
            logger.info("SalesPointID: " + salesPointId);
            final String encryptedReqData = parser.fetchDataFromRequest(query);
            logger.info("reqeust EDATA: " + encryptedReqData);
            final String decryptedReqData = encoder.decode(encryptedReqData);
            logger.info("request DATA: " + decryptedReqData);

            //identify operation
            final Operation operation = parser.identifyQueryOperation(decryptedReqData);
            final Logic logic = new Logic();
            final String serverGuid = parser.fetchServerGuid(decryptedReqData);
            final String reqGuid = parser.fetchReqGuid(decryptedReqData);
            String responseStr;
            switch (operation){
                case AUTHORIZATION: responseStr = logic.doAuthorization(decryptedReqData); break;
                case REFILL: responseStr = logic.doRefill(decryptedReqData); break;
                case GETSTATUS: responseStr = logic.doGetStatus(decryptedReqData); break;
                case GETLASTRESPONSE: responseStr = logic.doGetLastResponse(decryptedReqData); break;
                default: responseStr = "";
            }

            logger.info("response DATA: " + responseStr);
            final String encryptedRespData = encoder.encode(responseStr, serverGuid, reqGuid);
            logger.info("response EDATA: " + encryptedRespData);
            ServletOutputStream os = response.getOutputStream();
            os.print(encryptedRespData);
            logger.info("response EDATA [bytes]: " + new HexConverter().getStringFromHex(encryptedRespData.getBytes(charset))); 
            os.flush();
        }
        catch (Throwable e){
            logger.error(e);
            e.printStackTrace();
        }
    }


    public class Encoder{
        private Key privateKey;
        private PublicKey publicKey;

        private void init() throws InitException {
            //loadSecurityFromKeystore(); not supported. @see #loadSecurityFromKeystore()
            loadSecurityFromFiles();
        }

        private void loadSecurityFromKeystore() throws InitException {
            //load keys from keystore
            try{
                final String keyStorePath = Configuration.getInstance().substituteVariablesInString("${tmx.home}/repository/security/avancel/test/avancel_unittest_server.jks.keystore");
                final String keypass = "12345678";
                final String keyAlias = "avancel_test_privkey";
                final String certificateAlias = "client_certificate";
                KeyStore ks = KeyStore.getInstance("JKS", "SUN");
                ks.load(new FileInputStream(keyStorePath), keypass.toCharArray());
                if(!ks.isKeyEntry(keyAlias))
                    throw new InitException("Private key not found for Avancel gete by alias: " + keyAlias + " in keystore: " + keyStorePath);
                if(!ks.isCertificateEntry(certificateAlias))
                    throw new InitException("Certificate not found for Avancel gete by alias: " + keyAlias + " in keystore: " + keyStorePath);
                privateKey = ks.getKey(keyAlias, keypass.toCharArray());//java keystore supports only one private key inside and it should has the same password what keystore has.
                publicKey = ks.getCertificate(certificateAlias).getPublicKey();
            }
            catch(Exception e){
                throw new InitException("Failed to load keys by Avancel encoder.", e);
            }
        }

        /** Load client private and server public keys from files. */
        private void loadSecurityFromFiles() throws InitException{
            try{
                final String pubKeyFile = Configuration.getInstance().substituteVariablesInString("${tmx.home}/repository/security/avancel/test/with_files/client_public_key.der");
                final String privKeyFile = Configuration.getInstance().substituteVariablesInString("${tmx.home}/repository/security/avancel/test/with_files/server_private_key.der");

                // read public key DER file
                DataInputStream dis = new DataInputStream(new FileInputStream(pubKeyFile));
                byte[] pubKeyBytes = new byte[dis.available()];
                dis.readFully(pubKeyBytes);
                dis.close();

                // read private key DER file
                dis = new DataInputStream(new FileInputStream(privKeyFile));
                byte[] privKeyBytes = new byte[dis.available()];
                dis.readFully(privKeyBytes);
                dis.close();

                KeyFactory keyFactory = KeyFactory.getInstance("RSA");

                // decode private key
                PKCS8EncodedKeySpec privSpec = new PKCS8EncodedKeySpec(privKeyBytes);
                privateKey = keyFactory.generatePrivate(privSpec);

                // decode public key
                X509EncodedKeySpec pubSpec = new X509EncodedKeySpec(pubKeyBytes);
                publicKey = keyFactory.generatePublic(pubSpec);
            }
            catch(Exception e){
                throw new InitException("Failed to load keys by Avancel encoder from files.", e);
            }
        }


        /** Perform encoding outging data according to formula:
         * EDATA = Base64(Rj(RSA1(DATA))),
         * where EDATA = encrypted DATA.
         * @param data string data to be encrypted.
         * @param guid1 string used like byte source to create IV parameter for AES algorithm.
         * @param guid2 string used in concatenation with guid1 (guid1 + guid2) like byte source to create Key for AES algorithm.
         * @return encrypted data.
         * @throws EncoderException on encoding exception. */
        private String encode(String data, String guid1, String guid2) throws EncoderException {
            try{
                final HexConverter hexConverter = new HexConverter();
                logger.debug("Data [string]: " + data);
                final byte[] dataBytes = data.getBytes(charset);
                logger.debug("Data [bytes]: " + hexConverter.getStringFromHex(dataBytes));

                //1. Encrypt by RSA
                final byte[] rsaEncrypted = encryptRsa(dataBytes);
                logger.debug("RSA(Data): " + hexConverter.getStringFromHex(rsaEncrypted));

                //2. Encrypt by AES
                final byte[] aesEncrypted = encryptAes(rsaEncrypted, guid1, guid2);
                logger.debug("AES(RSA(Data)): " + hexConverter.getStringFromHex(aesEncrypted));

                //3. Encrypt by Base64 modified for safe usage in URL
                final String base64Encrypted = encryptBase64(aesEncrypted);
                logger.debug("Base64(AES(RSA(Data))): " + base64Encrypted);

                return base64Encrypted;
            }
            catch (Exception e){
                final String msg = "Failed to encrypt data: [" + data + "]";
                logger.error(msg, e);
                throw new EncoderException(msg, e);
            }
        }

        /** Perform decoding of incoming data according to formula:
         * EDATA = Base64(RSA2(DATA)),
         * where EDATA = encrypted DATA.
         * @param edata data to be decrypted.
         * @return decrypted data.
         * @throws EncoderException on decryption exception. */
        private String decode(String edata) throws EncoderException{
            try{
                HexConverter hexConverter = new HexConverter();
                logger.debug("Base64(AES(RSA(Data))): " + edata);

                //1. Decrypt by Base64 modified for URL
                final byte[] base64Decrypted = decryptUrlAndBase64(edata);
                logger.debug("AES(RSA(Data)): " + hexConverter.getStringFromHex(base64Decrypted));

                //2. Decrypt by RSA
                final byte[] rsaDecrypted = decryptRsa(base64Decrypted);
                logger.debug("Data [bytes]: " + hexConverter.getStringFromHex(rsaDecrypted));

                final String dataString = new String(rsaDecrypted, charset);
                logger.debug("Data [string]: " + dataString + "\n");

                return dataString;
            }
            catch (Exception e){
                final String msg = "Failed to decrypt data: [" + edata + "]";
                logger.error(msg, e);
                throw new EncoderException(msg, e);
            }
        }

        private byte[] encryptRsa(byte[] data) throws EncoderException{
            try{
                /** Encrypt by RSA. Remember: size of data to be encrypted plus some padding data should be less then key size.
                 * To encode data larger than key the data splitting on blocks used.
                 * Avancel uses 2048 bit keys with had to be splitted into blocks with lenght = (KEY_LEN/8 - 42).
                 * Where KEY_LEN = 2048. */
                Cipher cipherRsa = Cipher.getInstance("RSA/ECB/OAEPWithSHA1AndMGF1Padding");
                cipherRsa.init(Cipher.ENCRYPT_MODE, publicKey);
                final int KEY_LEN = 2048;
                final int DATA_BLOCK_SIZE = KEY_LEN/8 - 42;
                byte[] dataBlock;
                final HexConverter hexConverter = new HexConverter();
                ByteArrayInputStream bais = new ByteArrayInputStream(data);
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                int encryptedBytes = 0;
                while(encryptedBytes < data.length){
                    if(data.length - encryptedBytes <= DATA_BLOCK_SIZE){
                        dataBlock = new byte[data.length - encryptedBytes];
                        encryptedBytes = encryptedBytes + bais.read(dataBlock); //last block
                    }
                    else{
                        dataBlock = new byte[DATA_BLOCK_SIZE];
                        encryptedBytes = encryptedBytes + bais.read(dataBlock); //not last block
                    }
                    final byte[] encryptedRsaBlock = cipherRsa.doFinal(dataBlock);
                    baos.write(encryptedRsaBlock);
                    logger.debug("RSA(DataBlock): " + dataBlock.length+ "-bytes size block: [" + hexConverter.getStringFromHex(encryptedRsaBlock) + "]");
                }
                return baos.toByteArray();
            }
            catch (Exception e){
                final String msg = "Failed to encrypt data by RSA";
                logger.error(msg, e);
                throw new EncoderException(msg, e);
            }
        }

        private byte[] decryptRsa(byte[] edata) throws EncoderException{
            /** Decrypt by RSA. Remember: size of data to be encrypted plus some padding data should be less then key size.
             * To encode data larger than key the data splitting on blocks was used.
             * Avancel uses 2048 bit keys with had to be splitted into blocks. Length of blocks to decode = (KEY_LEN/8).
             * Where KEY_LEN = 2048. */
            try {
                Cipher cipherRsa = Cipher.getInstance("RSA/ECB/OAEPWithSHA1AndMGF1Padding");
                cipherRsa.init(Cipher.DECRYPT_MODE, privateKey);
                final int KEY_LEN = 2048;
                final int DATA_BLOCK_SIZE = KEY_LEN / 8;
                byte[] dataBlock;
                ByteArrayInputStream bais = new ByteArrayInputStream(edata);
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                int decryptedBytes = 0;
                final HexConverter hexConverter = new HexConverter();
                while (decryptedBytes < edata.length) {
                    if (edata.length - decryptedBytes <= DATA_BLOCK_SIZE) {
                        dataBlock = new byte[edata.length - decryptedBytes];
                        decryptedBytes = decryptedBytes + bais.read(dataBlock); //last block
                    } else {
                        dataBlock = new byte[DATA_BLOCK_SIZE];
                        decryptedBytes = decryptedBytes + bais.read(dataBlock); //not last block
                    }
                    final byte[] decryptedRsaBlock = cipherRsa.doFinal(dataBlock);
                    baos.write(decryptedRsaBlock);
                    logger.debug("RSA(DataBlock): " + dataBlock.length + "-bytes size block: [" + hexConverter.getStringFromHex(decryptedRsaBlock) + "]");
                }
                return baos.toByteArray();
            }
            catch (Exception e) {
                final String msg = "Failed to decrypt data by RSA";
                logger.error(msg, e);
                throw new EncoderException(msg, e);
            }
        }

        private String encryptBase64(byte[] data) throws EncoderException{
            return Base64.encodeBytes(data, Base64.DONT_BREAK_LINES);
        }

        private byte[] decryptUrlAndBase64(String edata) throws EncoderException{
            try{
                return Base64.decode(URLDecoder.decode(edata, charset.name()), Base64.DONT_BREAK_LINES);
            }
            catch (UnsupportedEncodingException e){
                throw new EncoderException("Failed to decrypt Base64(URL(Data))", e);
            }
        }
        private byte[] encryptAes(byte[] edata, String guid1, String guid2) throws EncoderException{
            try{
                final HexConverter hexConverter = new HexConverter();

                //convert guid1 into byte array
                final byte[] bytes128ForIV = getHexGuidFromString(guid1);
                if (bytes128ForIV.length != 16)
                    throw new EncoderException("Byte array used as source for Initialization Vector (IV) is not 128 bit (16 byte) length: " + hexConverter.getStringFromHex(bytes128ForIV));
                //logger.debug("IV: " + hexConverter.getStringFromHex(bytes128ForIV));

                //convert guid2 into byte array
                final byte[] guid2bytes = getHexGuidFromString(guid2);
                final byte[] bytes256ForKey = new byte[32];
                System.arraycopy(bytes128ForIV, 0, bytes256ForKey, 0, 16);
                System.arraycopy(guid2bytes, 0, bytes256ForKey, 16, 16);
                if (bytes256ForKey.length != 32)
                    throw new EncoderException("Byte array used as source for Key is not 128 bit (32 byte) length: " + hexConverter.getStringFromHex(bytes256ForKey));
                //logger.debug("Key: " + hexConverter.getStringFromHex(bytes256ForKey));

                Cipher cipherAes = Cipher.getInstance("AES/CBC/NoPadding");
                IvParameterSpec ivSpec = new IvParameterSpec(bytes128ForIV);
                SecretKeySpec keySpec = new SecretKeySpec(bytes256ForKey, "AES");
                cipherAes.init(Cipher.ENCRYPT_MODE, keySpec, ivSpec);

                //provide padding by 0x00 up to the end of last encoded block. Block length is 128 bit.
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                baos.write(edata);
                final int paddingBlockLength = (edata.length % 16 == 0) ? 16 /** last block completelly should be 0 */ : 16 - edata.length % 16;
                byte[] paddingBlock = new byte[paddingBlockLength];
                Arrays.fill(paddingBlock, (byte)0);
                baos.write(paddingBlock);
                return cipherAes.doFinal(baos.toByteArray());
            }
            catch (Exception e){
                final String msg = "Failed to encrypt data by AES";
                logger.error(msg, e);
                throw new EncoderException(msg, e);
            }
        }

        private byte[] decryptAes(byte[] edata, String guid1, String guid2) throws EncoderException{
            try {
                final HexConverter hexConverter = new HexConverter();

                //convert guid1 into byte array
                final byte[] bytes128ForIV = getHexGuidFromString(guid1);
                if (bytes128ForIV.length != 16)
                    throw new EncoderException("Byte array used as source for Initialization Vector (IV) is not 128 bit (16 byte) length: " + hexConverter.getStringFromHex(bytes128ForIV));
                //logger.debug("IV: " + hexConverter.getStringFromHex(bytes128ForIV));

                //convert guid2 into byte array
                final byte[] guid2bytes = getHexGuidFromString(guid2);
                final byte[] bytes256ForKey = new byte[32];
                System.arraycopy(bytes128ForIV, 0, bytes256ForKey, 0, 16);
                System.arraycopy(guid2bytes, 0, bytes256ForKey, 16, 16);
                if (bytes256ForKey.length != 32)
                    throw new EncoderException("Byte array used as source for Key is not 128 bit (32 byte) length: " + hexConverter.getStringFromHex(bytes256ForKey));
                //logger.debug("Key: " + hexConverter.getStringFromHex(bytes256ForKey));

                Cipher cipherAes = Cipher.getInstance("AES/CBC/NoPadding");
                IvParameterSpec ivSpec = new IvParameterSpec(bytes128ForIV);
                SecretKeySpec keySpec = new SecretKeySpec(bytes256ForKey, "AES");
                cipherAes.init(Cipher.DECRYPT_MODE, keySpec, ivSpec);

                final byte[] dataWithPadding = cipherAes.doFinal(edata);

                //remove padding of 0x00 applied to the end of last encoded block. Block length is 128 bit (16 bytes).
                int paddingLength = 0;
                for(int i=0; i<16; i++){
                    if(dataWithPadding[dataWithPadding.length - i - 1] == 0)
                        paddingLength++;
                    else
                        break;
                }
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                baos.write(dataWithPadding, 0, dataWithPadding.length - paddingLength);

                return baos.toByteArray();
            }
            catch (Exception e) {
                final String msg = "Failed to decrypt data by AES";
                logger.error(msg, e);
                throw new EncoderException(msg, e);
            }
        }

        /** Converts GUID into byte array. First three parts of GUID are  */
        private byte[] getHexGuidFromString(String guid) throws EncoderException{
            try{
                final String[] guidParts = guid.split("-");
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                HexConverter hexConverter = new HexConverter();
                baos.write(hexConverter.reverseBytes(hexConverter.getHexFromString(guidParts[0])));
                baos.write(hexConverter.reverseBytes(hexConverter.getHexFromString(guidParts[1])));
                baos.write(hexConverter.reverseBytes(hexConverter.getHexFromString(guidParts[2])));
                baos.write(hexConverter.getHexFromString(guidParts[3]));
                baos.write(hexConverter.getHexFromString(guidParts[4]));
                baos.flush();
                return baos.toByteArray();
            }
            catch (Exception e){
                throw new EncoderException("Failed to convert string GUID to byte array: " + guid, e);
            }
        }

        private class EncoderException extends StructurizedException {
            public EncoderException(String msg, Throwable cause){
                super(msg, cause);
            }

            public EncoderException(String msg){
                super(msg);
            }
        }
    }


    private class Parser{

        public String fetchSalesPointIdFromRequest(String data){
            return data.substring(0, data.indexOf("&"));
        }

        public String fetchDataFromRequest(String data){
            return data.substring(data.indexOf("&")+1);
        }

        private Operation identifyQueryOperation(String query){
            final String[] params = query.split("&");
            final Integer paramsCount = params.length;
            switch (paramsCount) {
                case 4 : return Operation.AUTHORIZATION;
                case 8 : return Operation.REFILL;
                case 5 : {
                    try{
                        Long.parseLong(params[3]); //GUID or number transaction id
                        return Operation.GETSTATUS;
                    }
                    catch (Exception e){
                        return Operation.GETLASTRESPONSE; //GUID
                    }
                }
            }
            return Operation.UNKNOWN;
        }

        private String fetchServerGuid(String query){
            return query.split("&")[0];
        }

        private String fetchReqGuid(String query){
            final String[] params = query.split("&");
            final Operation operation = identifyQueryOperation(query);
            switch (operation){
                case AUTHORIZATION: return params[3];
                case REFILL: return params[7];
                case GETSTATUS: return params[4];
                case GETLASTRESPONSE: return params[4];
                default: return "";
            }
        }
    }

    private class Logic {
        //private final String etalonServerGuid = "AAAAAAAA-AAAAAAAAAAAA-AAAA-AAAAAAAA";
        private final String etalonServerGuid = "73AA65ED-3CF9-40D6-BE17-BC21B8603442";
        //private final String etalonServerGuidResp = "BBBBBBBB-BBBBBBBBBBBB-BBBB-BBBBBBBB";
        private final String etalonServerGuidResp = "0F206FF1-00B1-4E2E-857F-B7F20DACB459";
        private final String etalonLogin = "testLogin";
        private final String etalonPassword = "testPassword";
        private final String DELIMITER = "&";

        private String doAuthorization(String s){
            String[] params = s.split(DELIMITER);
            final String serverGuid = params[0];
            final String login = params[1];
            final String password = params[2];
            final String reqGuid = params[3];

            logger.info("Server GUID: " + serverGuid);
            logger.info("Login: " + login);
            logger.info("Password: " + password);
            logger.info("reqGuid: " + reqGuid);

            final String statusByte = new String(new byte[]{0x66});
            final String storeListing = "art1,len1=nom11,nom12&art2,len2=nom21,nom22";

            return new StringBuffer(statusByte).append(DELIMITER).append(etalonServerGuidResp).append(DELIMITER).append(storeListing).toString();
        }

        private String doRefill(String s){
            String[] params = s.split(DELIMITER);
            final String serverGuid = params[0];
            final String login = params[1];
            final String password = params[2];
            final String msisdn = params[3];
            final String nominal = params[4];
            final String serviceId = params[5];
            final String terminalId = params[6];
            final String reqGuid = params[7];

            logger.info("Server GUID: " + serverGuid);
            logger.info("Login: " + login);
            logger.info("Password: " + password);
            logger.info("Msisdn: " + msisdn);
            logger.info("Nominal: " + nominal);
            logger.info("ServiceId: " + serviceId);
            logger.info("TerminalId: " + terminalId);
            logger.info("reqGuid: " + reqGuid);

            final String statusByte = new String(new byte[]{0x06});
            final String transactionId = String.valueOf(System.currentTimeMillis());
            final String serviceName = String.valueOf("UMC service");//human readable
            final String voucherSerialNumber = "12345678901234567890";
            final String voucherRefillCode = "0987654321";
            final String voucherExpirationDate = "3.09";
            final String instruction = "instruction first row" + new String(new  byte[]{0x0D}) + " instruction second row"; //0x0D = newline
            final String salesPointInfo = "info first row" + new String(new  byte[]{0x0D}) + " info second row"; //0x0D = newline

            return new StringBuffer(statusByte).
                    append(DELIMITER).append(transactionId).append(DELIMITER).
                    append(serviceName).append(DELIMITER).append(voucherSerialNumber).append(DELIMITER).
                    append(voucherRefillCode).append(DELIMITER).append(voucherExpirationDate).append(DELIMITER).
                    append(instruction).append(DELIMITER).append(salesPointInfo).toString();

        }

        private String doGetStatus(String s){
            String[] params = s.split(DELIMITER);
            final String serverGuid = params[0];
            final String login = params[1];
            final String password = params[2];
            final String transactionId = params[3];
            final String reqGuid = params[4];

            logger.info("Server GUID: " + serverGuid);
            logger.info("Login: " + login);
            logger.info("Password: " + password);
            logger.info("TransactionId: " + transactionId);
            logger.info("reqGuid: " + reqGuid);

            final String statusByte = new String(new byte[]{0x16});
            final String addressedTransactionStatus = "1";

            return new StringBuffer(statusByte).append(DELIMITER).append(addressedTransactionStatus).toString();
        }

        private String doGetLastResponse(String s){
            String[] params = s.split(DELIMITER);
            final String serverGuid = params[0];
            final String login = params[1];
            final String password = params[2];
            final String addressedTransactionGuid = params[3];
            final String reqGuid = params[4];

            logger.info("Server GUID: " + serverGuid);
            logger.info("Login: " + login);
            logger.info("Password: " + password);
            logger.info("TransactionGUID: " + addressedTransactionGuid);
            logger.info("reqGuid: " + reqGuid);

            final String statusByte = new String(new byte[]{0x27});
            final String transactionId = String.valueOf(System.currentTimeMillis());
            final String serviceName = String.valueOf("UMC service");//human readable
            final String voucherSerialNumber = "12345678901234567890";
            final String voucherRefillCode = "0987654321";
            final String voucherExpirationDate = "3.09";
            final String instruction = "instruction first row" + new String(new  byte[]{0x0D}) + " instruction second row"; //0x0D = newline
            final String salesPointInfo = "info first row" + new String(new  byte[]{0x0D}) + " info second row"; //0x0D = newline

            return new StringBuffer(statusByte).
                    append(DELIMITER).append(transactionId).append(DELIMITER).
                    append(serviceName).append(DELIMITER).append(voucherSerialNumber).append(DELIMITER).
                    append(voucherRefillCode).append(DELIMITER).append(voucherExpirationDate).append(DELIMITER).
                    append(instruction).append(DELIMITER).append(salesPointInfo).toString();

        }

    }
}
