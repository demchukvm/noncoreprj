package com.tmx.web.taglib.html.ctrl;

import com.tmx.web.controls.TextArea;
import javax.servlet.jsp.JspException;

import org.apache.struts.taglib.TagUtils;

/**
 * Text area rendering tag
 */
public class TextAreaTag extends AbstractControlTag{
    //CSS styles properties
    private String styleClass = null;
    private String readonlyStyleClass = null;
    private int cols = -1;
    private int rows = -1;

    //------------------------------Properties
    public String getStyleClass() {
        return (styleClass != null) ? styleClass : getDefaultProperty("tag.textArea.style.styleClass");
    }

    public void setStyleClass(String styleClass) {
        this.styleClass = styleClass;
    }

    public String getReadonlyStyleClass() {
        return (readonlyStyleClass != null) ? readonlyStyleClass : getDefaultProperty("tag.textArea.style.styleClassReadonly");
    }

    public void setReadonlyStyleClass(String readonlyStyleClass) {
        this.readonlyStyleClass = readonlyStyleClass;
    }

    public int getCols() {
        return (cols > 0) ? cols : Integer.parseInt(getDefaultProperty("tag.textArea.cols"));
    }

    public void setCols(int cols) {
        this.cols = cols;
    }

    public int getRows() {
        return (rows > 0) ? rows : Integer.parseInt(getDefaultProperty("tag.textArea.rows"));
    }

    public void setRows(int rows) {
        this.rows = rows;
    }

    //------------------------------Abstract methods
    protected int controlStartTag() throws JspException {
        StringBuffer resultHTML = new StringBuffer();
        TextArea ta = getTextArea();
        if(ta == null){
            TagUtils.getInstance().write(pageContext,"Textarea controll is null");
            return SKIP_BODY;
        }

        //Prepare tag attributes
        String name = " name=\""+getProperty()+"."+ TextArea.VALUE_SUFFIX+"\" ";
        String readonly = (ta.isReadonly()) ? " readonly=\"true\" " : " ";
        String styleClass = (ta.isReadonly()) ? " class=\""+getReadonlyStyleClass()+"\"" : "class=\""+getStyleClass()+"\" ";
        String rows = " rows=\""+getRows()+"\" ";
        String cols = " cols=\""+getCols()+"\" ";
        String type = " type=\"text\" ";

        //Render HTML
        resultHTML.append("\r");
        resultHTML.append("<textarea " + name + type + rows + cols + styleClass + readonly+">\r");
        resultHTML.append(ta.getValue() != null ? ta.getValue() : "");
        resultHTML.append("</textarea>\r");

        TagUtils.getInstance().write(pageContext,resultHTML.toString());
        return SKIP_BODY;
    }


   private TextArea getTextArea() throws JspException{
        Object object = getPropertyObject();
        return (object != null && object instanceof TextArea) ?
               (TextArea)object : null;
   }
}


