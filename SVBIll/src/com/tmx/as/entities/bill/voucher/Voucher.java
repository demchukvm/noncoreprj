package com.tmx.as.entities.bill.voucher;

import com.tmx.as.base.BasicEntity;
import com.tmx.as.entities.bill.balance.SellerBalance;
import com.tmx.as.entities.bill.transaction.SellerBalanceTransaction;
import com.tmx.as.entities.bill.operator.Operator;

import java.io.Serializable;
import java.util.Date;

/**
 */
public class Voucher extends BasicEntity {
    //constants
    public static final String STATUS_NEW = "NEW";
    public static final String STATUS_RESERVED = "RES";
    public static final String STATUS_SOLD = "SLD";
    public static final String STATUS_RETURNED = "RET";

    private Long voucherId;                                              
    private String code;    
    private String secretCode;
    private String nominal;
    private Double price;
    private Date bestBeforeDate;
    private Integer renevalTime;
    private String status;
    private Date loadingTime;
    private Date reservationTime;
    private Date soldTime;
    private Date returnTime;
    private String soldInBillTransactionNum;
    private Long voucherStatus;
    private SellerBalanceTransaction sellerBalanceRef;
    private Operator operator;


    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getNominal() {
        return nominal;
    }

    public void setNominal(String nominal) {
        this.nominal = nominal;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getBestBeforeDate() {
        return bestBeforeDate;
    }

    public void setBestBeforeDate(Date bestBeforeDate) {
        this.bestBeforeDate = bestBeforeDate;
    }

    public String getSecretCode() {
        return secretCode;
    }

    public void setSecretCode(String secretCode) {
        this.secretCode = secretCode;
    }

    public Long getVoucherId() {
        return voucherId;
    }

    public void setVoucherId(Long voucherId) {
        this.voucherId = voucherId;
    }

    public Serializable getId() {
        return getVoucherId();
    }

    public int getValid() {
        return valid;
    }

    public void setValid(int valid) {
        this.valid = valid;
    }

    public Integer getRenevalTime() {
        return renevalTime;
    }

    public void setRenevalTime(Integer renevalTime) {
        this.renevalTime = renevalTime;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Date getLoadingTime() {
        return loadingTime;
    }

    public void setLoadingTime(Date loadingTime) {
        this.loadingTime = loadingTime;
    }

    public Date getReservationTime() {
        return reservationTime;
    }

    public void setReservationTime(Date reservationTime) {
        this.reservationTime = reservationTime;
    }

    public Date getSoldTime() {
        return soldTime;
    }

    public void setSoldTime(Date soldTime) {
        this.soldTime = soldTime;
    }

    public Date getReturnTime() {
        return returnTime;
    }

    public void setReturnTime(Date returnTime) {
        this.returnTime = returnTime;
    }

    public String getSoldInBillTransactionNum() {
        return soldInBillTransactionNum;
    }

    public void setSoldInBillTransactionNum(String soldInBillTransactionNum) {
        this.soldInBillTransactionNum = soldInBillTransactionNum;
    }

    public Long getVoucherStatus() {
        return voucherStatus;
    }

    public void setVoucherStatus(Long voucherStatus) {
        this.voucherStatus = voucherStatus;
    }

    public SellerBalanceTransaction getSellerBalanceRef() {
        return sellerBalanceRef;
    }

    public void setSellerBalanceRef(SellerBalanceTransaction sellerBalanceRef) {
        this.sellerBalanceRef = sellerBalanceRef;
    }

    public Operator getOperator() {
        return operator;
    }

    public void setOperator(Operator operator) {
        this.operator = operator;
    }
}
