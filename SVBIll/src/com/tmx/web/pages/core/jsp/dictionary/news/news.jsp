<%@ taglib prefix="nested" uri="http://jakarta.apache.org/struts/tags-nested" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/tld/tmx-controls.tld" prefix="tmx_ctrl" %>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>

<%--<html:form action="/NewsAction">--%>

<nested:iterate id="messages"
                property="newsMessagesTable.data"
                type="com.tmx.as.entities.bill.messages.Messages">

    <div style="width:800px; padding:20px;border: 0px solid red;">

        <div style="border: 0px solid green; padding:2px;">
            <%  if(!messages.getObservers().equals("all"))
                {
            %>
            <div style="border: 0px solid black; background-color:white; padding:2px;text-align:right;font-size:20px;font-weight:bold;color:red;">
            <%
                }
                else
                {
            %>
            <div style="border: 0px solid black; background-color:white; padding:2px;text-align:right;font-size:20px;font-weight:bold;color:black;">
            <%
                }
            %>
                <c:if test="${messages.title != null}">
                    <c:out value="${messages.title}" />
                </c:if>
            </div>

            <div style="height: 2px;"></div>

            <div style="border: 1px dotted black; padding:5px;text-align:justify;">

                <c:if test="${messages.text != null}">
                    <% messages.setText(messages.getText().replace("[b]","<b>")
                                                        .replace("[/b]","</b>")
                                                        .replace("[u]","<u>")
                                                        .replace("[/u]","</u>")
                                                        .replace("[i]","<i>")
                                                        .replace("[/i]","</i>")
                                                        .replace("[left]","<div align=left style=\"padding:0px;margin:0px;\">")
                                                        .replace("[/left]","</div>")
                                                        .replace("[right]","<div align=right style=\"padding:0px;margin:0px;\">")
                                                        .replace("[/right]","</div>")
                                                        .replace("[center]","<div align=center style=\"padding:0px;margin:0px;\">")
                                                        .replace("[/center]","</div>")
                                                        .replace("[justify]","<div align=justify style=\"padding:0px;margin:0px;\">")
                                                        .replace("[/justify]","</div>")
                                                        .replace("[br]","<br/>"));
                    %>
                    <%--<c:out value="${ --%>
                       <!--messages.text-->
                    <%= messages.getText() %>
                       <%-- }" /> --%>
                </c:if>
            </div>

            <div style="height: 2px;"></div>

            <div style="border: 0px solid black; background-color:white; padding:2px;text-align:right;font-style:italic;">
                <c:if test="${messages.autor != null}">
                    <span style="color:blue;">Автор: <c:out value="${messages.autor}" /></span>
                </c:if>
                <br/>
                <c:if test="${messages.dateStart != null}">
                    <span style="color:green;"><fmt:formatDate value="${messages.dateStart}" pattern="yyyy/MM/dd HH:mm:ss" /></span>
                </c:if>
            </div>

        </div>

    </div> 

</nested:iterate>

<!-- If no data - empty row -->
<nested:define id="tableSize" property="newsMessagesTable.resultSize" type="java.lang.Integer"/>
<c:if test="${tableSize <= 0}">
	<div style="border: 0px solid black; background-color:white; padding:2px;text-align:center;font-size:20px;font-weight:bold;">
        <br/><br/><br/><br/><br/><br/><br/><br/><br/><bean:message bundle="core.labels" key="core.common.th.empty_news" />
	</div>
</c:if>

<c:if test="${tableSize > 5}">
    <tmx_ctrl:imgTablePaging maxBookmarkCount="10" tableName="newsMessagesTable"/>
</c:if>

<!--
<script type="text/javascript">
    
    document.body.innerHTML = document.body.innerHTML.replace("[b]","<b>")
                                                    .replace("[/b]","</b>")
                                                    .replace("[u]","<u>")
                                                    .replace("[/u]","</u>")
                                                    .replace("[i]","<i>")
                                                    .replace("[/i]","</i>")
                                                    .replace("[left]","<div align=left style='padding:0px;margin:0px;'>")
                                                    .replace("[/left]","</div>")
                                                    .replace("[right]","<div align=right style='padding:0px;margin:0px;'>")
                                                    .replace("[/right]","</div>")
                                                    .replace("[center]","<div align=center style='padding:0px;margin:0px;'>")
                                                    .replace("[/center]","</div>")
                                                    .replace("[justify]","<div align=justify style='padding:0px;margin:0px;'>")
                                                    .replace("[/justify]","</div>")
                                                    .replace("[small]","<small>")
                                                    .replace("[/small]","</small>")
                                                    .replace("[big]","<big>")
                                                    .replace("[/big]","</big>")
                                                    .replace("[red]","<p style='padding:0px;margin:0px;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;")
                                                    .replace("[/red]","<p/>")
                                                    .replace("[hr]","<hr/>")
                                                    .replace("[br]","<br/>");
</script>

-->