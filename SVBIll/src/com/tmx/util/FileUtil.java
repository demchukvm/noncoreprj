package com.tmx.util;

import java.io.*;
import java.util.Date;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

/**
 * Created by IntelliJ IDEA.
 * User: galyamov
 * Date: 25/4/2006
 * Time: 14:47:49
 * To change this template use File | Settings | File Templates.
 */
public class FileUtil {

    public static void appendToFile(String fileName, String text) {
        try {
            BufferedWriter out = new BufferedWriter(new FileWriter(fileName, true));
            out.write(StringUtil.getFormattedDate(new Date(), null) + " " + text + "\n");
            out.close();
        } catch (IOException e) { }
    }


    /** return true if file path refers inside jar */
    public static boolean isFilepathInsideJar(String path){
        return Pattern.compile("[^.]+\\.(jar|zip)!.+").matcher(path).matches();
    }

    public static InputStream loadFromJar(String filePathInsideJar) throws IOException{
        //Matcher matcher = Pattern.compile("[file:/]*([^.]+\\.(?:jar|zip))!/(.+)").matcher(filePathInsideJar);
        //RM: RFC #0000284 - leave starting slash in file path. This is the root slash on Linux
	Matcher matcher = Pattern.compile("[file:]*([^.]+\\.(?:jar|zip))!/(.+)").matcher(filePathInsideJar);
        InputStream is = null;
        if(matcher.matches()){
            String pathToJar = "/"+matcher.group(1);
            String pathInsideJar = matcher.group(2);
            ZipFile zipFile = new ZipFile(pathToJar);
            ZipEntry zipEntry = zipFile.getEntry(StringUtil.normalize(pathInsideJar));
            if(zipEntry == null)
                throw new IOException("ZipEntry '"+pathInsideJar+"' not found in '"+pathToJar+"'");
            is = zipFile.getInputStream(zipEntry);
        }
        return is;

    }

    public static void copyFile(File srcFile, File destFile) throws IOException{
        final int BUFFER_SIZE = 10000;
        byte[] buf = new byte[BUFFER_SIZE];
        InputStream is = null;
        OutputStream os = null;
        try{
            is = new FileInputStream(srcFile);
            os = new FileOutputStream(destFile);
            int readBytes = is.read(buf);
            for(; readBytes >= 0; readBytes = is.read(buf))
                os.write(buf, 0, readBytes);
        }
        finally{
            try{if(is != null)is.close();}catch(IOException e){}
            try{if(os != null)os.close();}catch(IOException e){}
        }
    }

    /** Checks if direcotry exists, otherwise �reates a chain of subsequent directories
     * based on given directory path.
     * @return created directory */
    public static File createNestedDirectory(String path) throws IOException{
        File dir = new File(StringUtil.normalize(path));
        dir.mkdirs();
        return dir;
    }

    //returns all files nested into directory
    public static ArrayList getInnerFiles(File directory) {
        ArrayList result = new ArrayList();
        addInnerFilesToList(result, directory);
        return result;
    }
    
    private static void addInnerFilesToList(ArrayList list, File file) {
        File[] fileList = file.listFiles();
        if (fileList != null) {
            for(int i = 0; i < fileList.length; i++) {
                if(fileList[i].isDirectory())
                    addInnerFilesToList(list, fileList[i]);
                if(fileList[i].isFile()) list.add(fileList[i]);
            }
        }
    }
    
}
