<?php /* Smarty version 2.6.26, created on 2013-02-20 19:16:16
         compiled from head.html */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'default', 'head.html', 1, false),array('modifier', 'escape', 'head.html', 1, false),)), $this); ?>
<title><?php echo ((is_array($_tmp=((is_array($_tmp=@$this->_tpl_vars['page_title'])) ? $this->_run_mod_handler('default', true, $_tmp, @CONF_DEFAULT_TITLE) : smarty_modifier_default($_tmp, @CONF_DEFAULT_TITLE)))) ? $this->_run_mod_handler('escape', true, $_tmp, 'html') : smarty_modifier_escape($_tmp, 'html')); ?>
</title>
<?php echo $this->_tpl_vars['page_meta_tags']; ?>

<script type="text/javascript" src="<?php echo @URL_JS; ?>
/niftycube.js"></script>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script> 
<script type="text/javascript" src="<?php echo @URL_JS; ?>
/jquery.flexslider-min.js"></script>

<script>
<?php echo '

$(document).ready(function($){


$(window).load(function() {
  $(\'.flexslider\').flexslider({
    animation: "fade",
	directionNav: false
  });
});

// Catalog
$(\'.catalog_sw h3\').click(function(){
	$(\'.cat_ws_cnt\').toggle(500);
});




var ddmenuitem = 0;
function jsddm_open()
{  jsddm_close();
   ddmenuitem = $(this).find(\'.filters ul li ul li\').css(\'display\', \'block\');
}
function jsddm_close()
{  
 if(ddmenuitem) ddmenuitem.css(\'display\', \'none\');
}
$(document).ready(function()
{  
   $(\'.filters ul li\').bind(\'click\', jsddm_open)
   $(\'.filters ul li\').click(function(){
   if ($(this).attr(\'class\') != \'current\'){
   $(\'.filters ul li\').removeClass(\'current\');
   $(this).addClass(\'current\');
   }
 });
});


});

'; ?>

</script>