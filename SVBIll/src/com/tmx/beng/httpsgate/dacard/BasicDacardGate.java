package com.tmx.beng.httpsgate.dacard;

import com.tmx.beng.base.*;
import com.tmx.beng.access.Access;
import com.tmx.beng.httpsgate.*;
import com.tmx.beng.httpsgate.dacard.pdu.*;
import com.tmx.util.queue.ActionQueue;
import com.tmx.util.InitException;
import com.tmx.util.HexConverter;
import com.tmx.util.Configuration;
import com.tmx.gate.datastream.base.GateContext;
import com.tmx.as.entities.mobipay.dacard.DacardGateTransaction;
import com.tmx.as.entities.mobipay.dacard.DacardGateStan;
import com.tmx.as.entities.mobipay.dacard.DacardGateReconciliation;
import com.tmx.as.base.EntityManager;
import com.tmx.as.base.criterion.CriterionWrapper;
import com.tmx.as.base.criterion.Restrictions;
import com.tmx.as.exceptions.DatabaseException;
import org.apache.log4j.Logger;
import org.apache.commons.digester.Digester;

import javax.crypto.spec.DESKeySpec;
import javax.crypto.SecretKeyFactory;
import javax.crypto.SecretKey;
import javax.crypto.Cipher;
import java.util.*;
import java.util.regex.Pattern;
import java.util.regex.Matcher;
import java.text.SimpleDateFormat;
import java.net.Socket;
import java.net.SocketAddress;
import java.net.InetSocketAddress;
import java.io.*;
import java.security.spec.KeySpec;


/**
 * Dacard outgoing gate
 */
public abstract class BasicDacardGate extends BasicGate {
    // properties names constants
    protected Logger logger = Logger.getLogger(getGateName());
    private Access access = null;
    private ActionQueue messageQueue = null;
    private StanManager stanManager;
    private ReconciliationManager reconMgr;
    private TariffEvaluator tariffEvaluator;
    private CodeMapping codeMapping;
    private ConnectionPool connPool;
    private Security security;

    // properties names constants
    private static final String PROP_SERVER_HOST = "serverHost";
    private static final String PROP_SERVER_PORT = "serverPort";
    private static final String PROP_INIT_HANDLER_COUNT = "handlers.initial";
    private static final String PROP_MAX_HANDLER_COUNT = "handlers.max";
    private static final String PROP_TERMINAL_ID = "terminalId";    
    private static final String PROP_TEST_MODE = "enableTestMode";
    private static final String PROP_MAX_TCP_CONNECTIONS = "maxTcpConnections";
    private static final String PROP_MAX_ALLOCATION_TIME = "maxAllocationTime";
    private static final String PROP_ENCODED_SESS_KEY = "encodedSessionKey";
    private static final String PROP_CHECK_REPLENISHMENT_MAX_ATTEMPTS = "checkReplenisment.maxAttempts";
    private static final String PROP_CHECK_REPLENISHMENT_SLEEP_INTERVAL = "checkReplenisment.sleepInterval";
    private static final String PROP_CHECK_REPLENISHMENT_SLEEP_INTERVAL_MULTIPLICATOR = "checkReplenisment.sleepInterval.multiplicator";



    protected abstract String getGateName();


    protected void init0()throws InitException {
        properties = GateContext.getInstance().getDatastreamProcessorConfig(getGateName());
        access = obtainBillingEngineAccess();

        //initialize codes mapping
        codeMapping = new CodeMapping();
        codeMapping.init();

        //initialize security
        security = new Security();
        security.init();

        //initialize connection pool
        connPool = new SimpleConnectionPool();
        connPool.init();

        //initilize reconciliation manager                                      
        reconMgr = new ReconciliationManager();
        reconMgr.init();

        //initialize tariff evaluator
        tariffEvaluator = new TariffEvaluator();
        tariffEvaluator.init();

        //create incoming queue
        final int initHandlerCount = Integer.parseInt(properties.getProperty(PROP_INIT_HANDLER_COUNT, "1"));
        final int maxHandlerCount = Integer.parseInt(properties.getProperty(PROP_MAX_HANDLER_COUNT, "5"));
        messageQueue = new ActionQueue(initHandlerCount, maxHandlerCount);

        //initialize stanManager
        stanManager = new StanManager();
        stanManager.init();        

        //for testing start this console.
        if(properties.getProperty(PROP_TEST_MODE) != null &&
           "true".equalsIgnoreCase(properties.getProperty(PROP_TEST_MODE).trim())){
            DacardGateTest testConsole = new DacardGateTest();
            testConsole.start();
        }
    }

    protected void shutdown0(final long timeout){
        if(messageQueue.actionCount() != 0){
            Runtime.getRuntime().addShutdownHook(new Thread(){
                public void run(){
                    try{
                        long lt = System.currentTimeMillis();
                        while(messageQueue.actionCount() != 0 && ((System.currentTimeMillis() - lt) < timeout)){
                            sleep(1000);
                        }
                        reconMgr.shutdown();
                        connPool.shutdown();
                    }
                    catch(Throwable e){
                        e.printStackTrace();
                    }
                }
            });
        }
    }


    public void enqueueBillMessage(BillingMessage billMessage){
        DacardGateTask task = new DacardGateTask(billMessage, this);
        messageQueue.enqueue(task);
    }

    public void runBillMessage(BillingMessage billingMessage) throws DacardGateException{

        BillingMessageImpl respMessage = new BillingMessageImpl();
        respMessage.setAttributeString(BillingMessage.REQUEST_ID,
                billingMessage.getAttributeString(BillingMessage.REQUEST_ID));
        try{
            // refill payment                
            if(BillingMessage.O_REFILL_PAYMENT.equals(billingMessage.getOperationName())){
                refillPayment(billingMessage, respMessage);
            }
            else if(BillingMessage.O_REFILL_PAYMENT_REDIRECT.equals(billingMessage.getOperationName())){
                refillPayment(billingMessage, respMessage);
            }
            // get status for refill payment
            else if(BillingMessage.O_REFILL_PAYMENT_STATUS.equals(billingMessage.getOperationName())){
//                getTransactionStatus(billingMessage, respMessage);
            }
            else{
                respMessage.setStatusCode(StatusDictionary.SUBMIT_TRANSACTION_ERROR);
                throw new HttpsGateException("Unsupported command",
                           new String[]{billingMessage.getOperationName()});
            }
        }catch(Throwable e){
            e.printStackTrace();
            logger.error(e.getLocalizedMessage(), e);
            if(respMessage.getAttributeString(BillingMessage.STATUS_CODE) == null){
                respMessage.setStatusCode(StatusDictionary.UNKNOWN_ERROR);
            }
            respMessage.setAttributeString(BillingMessage.STATUS_MESSAGE, e.getLocalizedMessage());
        }finally{
            // send response
            try{
                access.obtainConnection().processAsync(respMessage);
            }catch(Throwable e){
                logger.error(e.getLocalizedMessage(), e);
            }
        }

    }


    /**
     *  Refill payment by msisdn.
     * @param billingMessage billing message from billing;
     * @param respMessage response message to billing.
     */
    private void refillPayment(BillingMessage billingMessage, BillingMessageImpl respMessage){
        EntityManager entityManager = new EntityManager();
        DacardGateTransaction dacardGateTransaction = new DacardGateTransaction();
        try{
            respMessage.setOperationName(BillingMessage.O_REFILL_PAYMENT_RESULT);

            //1. populate and pre-save Dacard transaction
            dacardGateTransaction.setBillTransactionNum(billingMessage.getAttributeString(BillingMessage.BILL_TRANSACTION_NUM));
            dacardGateTransaction.setInvoiceRrn(dacardGateTransaction.getBillTransactionNum());
            dacardGateTransaction.setAmount(Reference.DacardValueConverter.convertAmount(new Double(billingMessage.getAttributeString(BillingMessage.AMOUNT))));
            dacardGateTransaction.setMessageType(Reference.MessageTypeCode.MT_ONLINE_REPLENISHMENT.code);
            if (billingMessage.getAttributeString(BillingMessage.ACCOUNT_NUMBER) != null) {
                //TODO: remove from hard code ["80" +]    
                dacardGateTransaction.setInvoiceInfo(Reference.DacardValueConverter.formatInvoiceNumber(billingMessage.getAttributeString(BillingMessage.ACCOUNT_NUMBER)));
            } else if (billingMessage.getAttributeString(BillingMessage.PAY_ACCOUNT_NUMBER) != null) {
                dacardGateTransaction.setInvoiceInfo(billingMessage.getAttributeString(BillingMessage.PAY_ACCOUNT_NUMBER));
            }
            dacardGateTransaction.setTerminalId(properties.getProperty(PROP_TERMINAL_ID));
            dacardGateTransaction.setMsgCount(stanManager.allocateNewStan());
            dacardGateTransaction.setLocalDate(new Date());
            dacardGateTransaction.setLocalDateString(Reference.DacardValueConverter.formatDate(dacardGateTransaction.getLocalDate()));
            dacardGateTransaction.setRrn(Reference.DacardValueConverter.formatRRN(
                    dacardGateTransaction.getTerminalId(),
                    dacardGateTransaction.getMsgCount(),
                    dacardGateTransaction.getLocalDateString()
            ));
            dacardGateTransaction.setArticleId(codeMapping.getDacardArticleId(
                    BillingMessage.O_REFILL_PAYMENT, 
                    billingMessage.getAttributeString(BillingMessage.SERVICE_CODE),
                    billingMessage.getAttributeString(BillingMessage.VOUCHER_NOMINAL)
            ));
            dacardGateTransaction.setFee1(tariffEvaluator.evaluateFee(dacardGateTransaction.getAmount(), dacardGateTransaction.getArticleId(), 1));
            dacardGateTransaction.setFee2(tariffEvaluator.evaluateFee(dacardGateTransaction.getAmount(), dacardGateTransaction.getArticleId(), 2));
            dacardGateTransaction.setReconIndicator(reconMgr.allocateReconIndicator());
            dacardGateTransaction.setGateName(getGateName());
            //pre-save transaction
            entityManager.SAVE(dacardGateTransaction);

            // 2. send MT_ONLINE_REPLENISHMENT message
            DacardMessage dmResp = null;
            boolean isStatusFinalized = false;
            try{
                logger.info("Do onlineReplenishment reqeust...");
                dmResp = sendMessageOnlineReplenishment(dacardGateTransaction);
                isStatusFinalized = !Reference.ResponseCodeResolver.isStatusNotFinal(dacardGateTransaction.getErrorCode());
            }
            catch (DacardGateException e){
                logger.error("onlineReplenishment reqeust failed.", e);
                isStatusFinalized = false;
            }

            //3. send MT_CHECK_REPLENISHMENT message until the final status will be occued or maximum attempts reached.
            if(!isStatusFinalized){
                logger.error("Transaction status is not final. Start sequence of checkReplenishment reqeusts...");
                int checkReplenishmentMaxAttempt = Integer.parseInt(properties.getProperty(PROP_CHECK_REPLENISHMENT_MAX_ATTEMPTS, "5"));
                long checkReplenishmentSleepInterval = Integer.parseInt(properties.getProperty(PROP_CHECK_REPLENISHMENT_SLEEP_INTERVAL, "30000"));
                double checkReplenishmentMultiplicator = Double.parseDouble(properties.getProperty(PROP_CHECK_REPLENISHMENT_SLEEP_INTERVAL_MULTIPLICATOR, "1.1"));
                checkReplenishmentMultiplicator = checkReplenishmentMultiplicator <= 0 ? checkReplenishmentMultiplicator = 1.1 : checkReplenishmentMultiplicator;

                for(int i=0; i<checkReplenishmentMaxAttempt && !isStatusFinalized; i++){
                    //sleep before reqeust
                    try {
                        if(i > 0)
                            checkReplenishmentSleepInterval = (long)(checkReplenishmentSleepInterval * checkReplenishmentMultiplicator);
                        Thread.sleep(checkReplenishmentSleepInterval);
                    }
                    catch (InterruptedException ei) {
                        logger.error("Failed to sleep between checkReplenishment request attempts.", ei);
                    }

                    //execute check replenishment reqeust
                    try{
                        logger.info("Do checkReplenishment reqeust attempt #"+i+"...");
                        dmResp = sendMessageCheckReplenishment(dacardGateTransaction);
                    }
                    catch (DacardGateException e){
                        logger.error("checkReplenishment reqeust failed at "+i+" attempt.", e);
                        continue;
                    }

                    //check is message finalized
                    isStatusFinalized = !Reference.ResponseCodeResolver.isStatusNotFinal(dacardGateTransaction.getErrorCode());
                }
                if(!isStatusFinalized)
                    logger.error("Transaction status is leaved not finilized");
                if(dmResp == null)
                    throw new DacardGateException("Still no dacard response at the end of the transaction");
            }

            //4. populate response message for billing 
            if(dacardGateTransaction.getErrorCode() == Reference.ErrorCode.SUCCESS.code){
                respMessage.setStatusCode(StatusDictionary.STATUS_OK);
            }
            else {
                respMessage.setStatusCode(StatusDictionary.PARTNER_ERROR);
                respMessage.setAttribute(
                        BillingMessage.STATUS_MESSAGE,
                        obtainOperatorI18nMessage(
                                billingMessage.getAttributeString(BillingMessage.SERVICE_CODE),
                                MSG_TYPE_ERROR,
                                new Long(dacardGateTransaction.getErrorCode()),
                                null)
                );
                throw new DacardGateException("err.dacard_gate.online_replenishment_error",
                        new String[]{String.valueOf(dacardGateTransaction.getErrorCode())});
            }
        }
        catch(Throwable e){
            logger.error(e.getLocalizedMessage(), e);
            if(respMessage.getAttributeString(BillingMessage.STATUS_CODE) == null)
                respMessage.setStatusCode(StatusDictionary.UNKNOWN_ERROR);
            if(respMessage.getAttribute(BillingMessage.STATUS_MESSAGE) == null)
                respMessage.setAttributeString(BillingMessage.STATUS_MESSAGE, e.getLocalizedMessage());
        }
        finally{
            // save dacard gate transaction
            try{
                dacardGateTransaction.setStatusCode(new Integer(respMessage.getAttributeString(BillingMessage.STATUS_CODE)));
                if(respMessage.getAttribute(BillingMessage.STATUS_MESSAGE) != null)
                    dacardGateTransaction.setStatusMessage(respMessage.getAttributeString(BillingMessage.STATUS_MESSAGE));
                entityManager.SAVE(dacardGateTransaction);
            }
            catch(Throwable e){
                logger.error(e.getLocalizedMessage(), e);
            }
        }
    }

    private DacardMessage sendMessageOnlineReplenishment(DacardGateTransaction dgt) throws DacardGateException{
        final DacardMessage dm = new DacardPAD().createMessage(security.getSessionKey(), logger);
        dm.addDataElement(DataElementName.TAG_MSGTYPE, DataElementType.ID_TYPE_NUMERIC, Reference.MessageTypeCode.MT_ONLINE_REPLENISHMENT.code);
        dm.addDataElement(DataElementName.TAG_TERMINALID, DataElementType.ID_TYPE_STRING, dgt.getTerminalId());
        dm.addDataElement(DataElementName.TAG_MSGCOUNT, DataElementType.ID_TYPE_NUMERIC, dgt.getMsgCount());
        dm.addDataElement(DataElementName.TAG_LOCALDATE, DataElementType.ID_TYPE_STRING, dgt.getLocalDateString());
        dm.addDataElement(DataElementName.TAG_RRN, DataElementType.ID_TYPE_STRING, dgt.getRrn());
        dm.addDataElement(DataElementName.TAG_ARTICLEID, DataElementType.ID_TYPE_NUMERIC, dgt.getArticleId());
        dm.addDataElement(DataElementName.TAG_AMOUNT, DataElementType.ID_TYPE_NUMERIC, dgt.getAmount());
        dm.addDataElement(DataElementName.TAG_FEE1, DataElementType.ID_TYPE_NUMERIC, dgt.getFee1());
        dm.addDataElement(DataElementName.TAG_FEE2, DataElementType.ID_TYPE_NUMERIC, dgt.getFee2());
        dm.addDataElement(DataElementName.TAG_RECONINDICATOR, DataElementType.ID_TYPE_NUMERIC, dgt.getReconIndicator());
        dm.addDataElement(DataElementName.TAG_INVOICE_INFO, DataElementType.ID_TYPE_STRING, dgt.getInvoiceInfo());
        dm.addDataElement(DataElementName.TAG_INVOICE_RRN, DataElementType.ID_TYPE_STRING, dgt.getInvoiceRrn());
        final DacardMessage dmResp = executeDacardRequest(dm);
        //�opy respone message to transaction entity
        dgt.setFee((String)(dmResp.getDataElement(DataElementName.TAG_FEE) != null ? dmResp.getDataElement(DataElementName.TAG_FEE).getValue() : null));
        dgt.setPrn1Name((String)(dmResp.getDataElement(DataElementName.TAG_PRN_1_NAME) != null ? dmResp.getDataElement(DataElementName.TAG_PRN_1_NAME).getValue() : null));
        dgt.setPrn1Value((String)(dmResp.getDataElement(DataElementName.TAG_PRN_1_VALUE) != null ? dmResp.getDataElement(DataElementName.TAG_PRN_1_VALUE).getValue() : null));
        dgt.setErrorCode(dmResp.getDataElement(DataElementName.TAG_ERRORCODE) != null ? ((Number)dmResp.getDataElement(DataElementName.TAG_ERRORCODE).getValue()).intValue() : null);
        dgt.setErrorDescription((String)(dmResp.getDataElement(DataElementName.TAG_ERRORDESC) != null ? dmResp.getDataElement(DataElementName.TAG_ERRORDESC).getValue() : null));
        return dmResp;                   
    }


    private DacardMessage sendMessageCheckReplenishment(DacardGateTransaction dgt) throws DacardGateException {
        final DacardMessage dm = new DacardPAD().createMessage(security.getSessionKey(), logger);
        dm.addDataElement(DataElementName.TAG_MSGTYPE, DataElementType.ID_TYPE_NUMERIC, Reference.MessageTypeCode.MT_CHECK_REPLENISHMENT_STATE.code);
        dm.addDataElement(DataElementName.TAG_TERMINALID, DataElementType.ID_TYPE_STRING, dgt.getTerminalId());
        Integer msgCount = stanManager.allocateNewStan();
        dm.addDataElement(DataElementName.TAG_MSGCOUNT, DataElementType.ID_TYPE_NUMERIC, msgCount);
        dm.addDataElement(DataElementName.TAG_LOCALDATE, DataElementType.ID_TYPE_STRING, dgt.getLocalDateString());

        String currentDateString = Reference.DacardValueConverter.formatDate(new Date());
        dm.addDataElement(DataElementName.TAG_RRN, DataElementType.ID_TYPE_STRING, Reference.DacardValueConverter.formatRRN(
                                                                                                dgt.getTerminalId(),
                                                                                                msgCount,
                                                                                                currentDateString));
        
        dm.addDataElement(DataElementName.TAG_RECONINDICATOR, DataElementType.ID_TYPE_NUMERIC, dgt.getReconIndicator());
        dm.addDataElement(DataElementName.TAG_ENABLE_EXT_INFO, DataElementType.ID_TYPE_NUMERIC, 1);
        dm.addDataElement(DataElementName.TAG_INVOICE_RRN, DataElementType.ID_TYPE_STRING, dgt.getInvoiceRrn());
        final DacardMessage dmResp = executeDacardRequest(dm);
        //Copy respone message to transaction entity
        dgt.setPrn1Name((String)(dmResp.getDataElement(DataElementName.TAG_PRN_1_NAME) != null ? dmResp.getDataElement(DataElementName.TAG_PRN_1_NAME).getValue() : null));
        dgt.setPrn1Value((String)(dmResp.getDataElement(DataElementName.TAG_PRN_1_VALUE) != null ? dmResp.getDataElement(DataElementName.TAG_PRN_1_VALUE).getValue() : null));
        dgt.setErrorCode(dmResp.getDataElement(DataElementName.TAG_ORIGINAL_AC) != null ? ((Number)dmResp.getDataElement(DataElementName.TAG_ORIGINAL_AC).getValue()).intValue() : null);
        dgt.setCheckReplenishmentErrorCode(dmResp.getDataElement(DataElementName.TAG_ERRORCODE) != null ? ((Number)dmResp.getDataElement(DataElementName.TAG_ERRORCODE).getValue()).intValue() : null);
        dgt.setErrorDescription((String)(dmResp.getDataElement(DataElementName.TAG_ERRORDESC) != null ? dmResp.getDataElement(DataElementName.TAG_ERRORDESC).getValue() : null));
        return dmResp;
    }


    private void reconciliation(DacardGateReconciliation dgr){
        //...
    }

    private DacardMessage executeDacardRequest(DacardMessage dm)throws DacardGateException {
        Socket socket = null;
        try{
            socket = connPool.allocateSocket();
            OutputStream os;
            InputStream is;
            try{
                os = socket.getOutputStream();
                is = socket.getInputStream();
            }
            catch (IOException e){
                throw new DacardGateException("Failed to work with socket", e);
            }
            final DacardPAD pad = new DacardPAD();
            //write request message
            pad.serializeMessage(os, dm);

            try{
                os.flush();
            }
            catch (IOException e){
                throw new DacardGateException("Failed to flush output stream", e);
            }

            //read answer message
            DacardMessage respDm = pad.parseMessage(is, security.getSessionKey(), logger);
            return respDm;
        }
        finally {
            if(socket != null)
                connPool.returnSocket(socket);
            socket = null;
        }

    }

    /** Class operates with STAN code. Reset it every day to 0 and increament it thread safe for each STAN allcoation. */
    private class StanManager {
        private int MAX_STAN_VALUE = 999999;
        private DacardGateStan stan;

        private void init() throws InitException{
            try{
                EntityManager em = new EntityManager();

                Calendar cal = Calendar.getInstance();
                cal.setTime(new Date());
                cal.set(Calendar.HOUR_OF_DAY, 0); cal.set(Calendar.MINUTE, 0); cal.set(Calendar.SECOND, 0); cal.set(Calendar.MILLISECOND, 0);
                final String terminalId = properties.getProperty(PROP_TERMINAL_ID); 
                CriterionWrapper wrappers[] = new CriterionWrapper[] {Restrictions.eq("day", cal.getTime()), Restrictions.eq("dacardTerminalId", terminalId)};
                stan = (DacardGateStan)em.RETRIEVE(DacardGateStan.class, null, wrappers, null);

                if(stan == null){
                    stan = new DacardGateStan();
                    stan.setDay(cal.getTime());
                    stan.setStan(0);
                    stan.setDacardTerminalId(terminalId);
                }

            }
            catch (DatabaseException e){
                throw new InitException("Failed to initialize STAN", e);
            }
        }

        private synchronized int allocateNewStan() throws DacardGateException{
            if(stan == null)
                throw new IllegalStateException("StanManager is not inited yet");

            final int currentDay = new GregorianCalendar().get(Calendar.DAY_OF_YEAR);

            GregorianCalendar cal = new GregorianCalendar();
            cal.setTime(stan.getDay());
            final int lastStanAllocDay = cal.get(Calendar.DAY_OF_YEAR);

            if(currentDay != lastStanAllocDay){
                cal.setTime(new Date()); // reset STAN each day
                cal.set(Calendar.HOUR_OF_DAY, 0); cal.set(Calendar.MINUTE, 0); cal.set(Calendar.SECOND, 0); cal.set(Calendar.MILLISECOND, 0);
                stan = new DacardGateStan();
                stan.setDay(cal.getTime());
                stan.setStan(1);
                stan.setAllocationTime(new Date());
                stan.setDacardTerminalId(properties.getProperty(PROP_TERMINAL_ID));
            }
            else{
                if(stan.getStan() >= MAX_STAN_VALUE)
                    throw new DacardGateException("Stan exceeds allowed limit " + MAX_STAN_VALUE + " in day");
                stan.setStan(stan.getStan() + 1); // increment STAN
                stan.setAllocationTime(new Date());
            }
            try{
                new EntityManager().SAVE(stan);
            }
            catch (DatabaseException e){
                throw new DacardGateException("Failed to update Stan", e);
            }
            return stan.getStan();
        }
    }

    /** Reconciliation manager manages the state of the gate,  */
    private class ReconciliationManager extends Thread implements Runnable {
        //private GateState gateState = GateState.RECON_MANAGER_NOT_INITED;
        /** Last reconciled day */
        //private Date lastReconciledDay;
        /** Current reconciliation record */
        private DacardGateReconciliation currentReconciliation;
        /** Shutdown reconciliation thread */
        private boolean doShutdown;


        /** Initialize Reconciliation Manager
         * @throws InitException on initialization exception. */
        private void init() throws InitException {
            currentReconciliation = null;// reset before init
            DacardGateReconciliation lastReconcilation = null;

            //build calendar with today
            final GregorianCalendar todayDayCal = new GregorianCalendar();
            todayDayCal.setTime(new Date());
            todayDayCal.set(Calendar.HOUR_OF_DAY, 0); todayDayCal.set(Calendar.MINUTE, 0); todayDayCal.set(Calendar.SECOND, 0); todayDayCal.set(Calendar.MILLISECOND, 0);

            try{
                final EntityManager em = new EntityManager();
                final List vals = em.EXECUTE_QUERY(DacardGateReconciliation.class, "getLastReconciliationDate", null);

                if(vals != null && vals.size() == 1 && vals.get(0) != null){
                    //build calendar with last reconciliation date
                    lastReconcilation = (DacardGateReconciliation)vals.get(0);
                    final GregorianCalendar lastReconCal = new GregorianCalendar();
                    lastReconCal.setTime(lastReconcilation.getReconDate());

                    //set current reconciliation = last reconciliation if dates are the same
                    if (lastReconCal.compareTo(todayDayCal) == 0){
                        currentReconciliation = lastReconcilation;
                    }
                }

                if(currentReconciliation == null){
                    currentReconciliation = createNewReconciliation(lastReconcilation, todayDayCal);
                    em.SAVE(currentReconciliation);
                }
           }
            catch (DatabaseException e){
                e.printStackTrace();
                logger.error(e);
                throw new InitException("Failed to initialize ReconciliationManager", e);
            }

            //start reconciliation scheuler
            doShutdown = false;
            start();
        }

        /** Shutdown Reconciliation Manager. */
        private void shutdown(){
            doShutdown = true;
        }

        /**
         * Reconciliation scheduler main life-cycle.
         */
        public void run() {
            final int sleepInterval = 600000; //60 minutes
            while (!doShutdown) {
                try {
//                    //check if previous day reconciled
//
//                    //obtain previos day's date
//                    final GregorianCalendar prevDaycal = new GregorianCalendar();
//                    prevDaycal.setTime(new Date());
//                    prevDaycal.set(Calendar.HOUR_OF_DAY, 0); prevDaycal.set(Calendar.MINUTE, 0); prevDaycal.set(Calendar.SECOND, 0); prevDaycal.set(Calendar.MILLISECOND, 0);
//                    prevDaycal.add(Calendar.DAY_OF_MONTH, -1);
//
//                    //prepare last reconciled day's date
//                    final GregorianCalendar lastReconciledCal = new GregorianCalendar();
//                    lastReconciledCal.setTime(lastReconciledDay != null ? lastReconciledDay : new Date(0));//if no last reconciliation date, then take most old date
//                    lastReconciledCal.set(Calendar.HOUR_OF_DAY, 0); lastReconciledCal.set(Calendar.MINUTE, 0); lastReconciledCal.set(Calendar.SECOND, 0); lastReconciledCal.set(Calendar.MILLISECOND, 0);
//
//                    //compare dates
//                    if (lastReconciledCal.compareTo(prevDaycal) < 0) {
//                        // previsious day needs reconciliation
//                        try{
//                            doReconciliation(prevDaycal.getTime());
//                        }
//                        catch (HttpsGateException e){
//                            logger.error("Previous day reconciliation failed", e);
//                        }
//                    }

                    //sleep awhile
                    try {
                        Thread.sleep(sleepInterval);
                    }
                    catch (Exception e) {
                        logger.error("Failed to sleep scheduler", e);
                    }
                }
                catch (Throwable e) {
                    logger.error("Reconciliation Manager life cycle exception (ignored)", e);
                }
            }
        }

        /** Do reconciliation for given day
         * @param day the day reconciliation is done for.
         * @throws HttpsGateException on reconciliation fault. */
        private void doReconciliation(Date day) throws HttpsGateException{
//            try{
//                //1. update all COMPLETED transactions inside given day to be reconciled
//                EntityManager em = new EntityManager();
//
//                Calendar cal = Calendar.getInstance();
//                cal.setTime(day);
//                cal.set(Calendar.HOUR_OF_DAY, 0); cal.set(Calendar.MINUTE, 0); cal.set(Calendar.SECOND, 0); cal.set(Calendar.MILLISECOND, 0);
//                QueryParameterWrapper qp1 = new QueryParameterWrapper("startTime", cal.getTime());
//
//                cal.set(Calendar.HOUR_OF_DAY, 23); cal.set(Calendar.MINUTE, 59); cal.set(Calendar.SECOND, 59); cal.set(Calendar.MILLISECOND, 999);
//                QueryParameterWrapper qp2 = new QueryParameterWrapper("endTime", cal.getTime());
//
//                QueryParameterWrapper qp3 = new QueryParameterWrapper("gateName", getGateName());
//
//                final int markedTransactionsCount = em.EXECUTE_QUERY_UPDATE(DacardGateReconciliation.class, "markTransactionsForReconciliation", new QueryParameterWrapper[]{qp1, qp2, qp3});
//
//                //2. get reconciliation data and populate reconciliation
//                DacardGateReconciliation reconciliation = new DacardGateReconciliation();
//                reconciliation.setStartTime(new Date());
//                reconciliation.setReconDate(day);
//                reconciliation.setStatus("IN_PROGRESS");
//                reconciliation.setAttemptsCount(0);
//
//                List vals = em.EXECUTE_QUERY(DacardGateReconciliation.class, "getReconciliationValues", new QueryParameterWrapper[]{qp1, qp2, qp3});
//                if(vals != null && vals.size() == 1 && vals.get(0) != null){
//                    Object[] row = (Object[])vals.get(0);
//                    reconciliation.setOc791TotalAmount((row[0] != null) ? (Integer)row[0] : 0);
//                    reconciliation.setOc791OperationCount((row[1] != null) ? (Integer)row[1] : 0);
//                }
//                else{
//                    reconciliation.setOc791TotalAmount(0);
//                    reconciliation.setOc791OperationCount(0);
//                }
//
//                //3. pre-save reconciliation
//                em.SAVE(reconciliation);
//
//                //4. execute reconciliation with i-OTM
//                reconciliation(reconciliation);
//
//                //5. mark transaction as reconciled
//                QueryParameterWrapper qp4 = new QueryParameterWrapper("reconciliationId", reconciliation.getDacardGateReconciliationId());
//                final int unmarkedTransactionsCount = em.EXECUTE_QUERY_UPDATE(DacardGateReconciliation.class, "unmarkTransactionsForReconciliation", new QueryParameterWrapper[]{qp1, qp2, qp3, qp4});
//                if(markedTransactionsCount != unmarkedTransactionsCount)
//                    logger.error("Count of marked and unmarked during the reconciliation transactions are different. Probably some transactions have been added during reconciliation into the reconcilied period.");
//
//                //6. post save reconciliation
//                reconciliation.setEndTime(new Date());
//                reconciliation.setStatus("COMPLETED");
//                em.SAVE(reconciliation);
//
//                //7. Save last reconciled day
//                lastReconciledDay = day;
//            }
//            catch (DatabaseException e){
//                throw new HttpsGateException("Database exception during the reconciliation:"+e);
//            }
        }

        private synchronized int allocateReconIndicator() throws DacardGateException{
            // check if day was changes since last allocation

            //build calendar with today
            final GregorianCalendar todayDayCal = new GregorianCalendar();
            todayDayCal.setTime(new Date());
            todayDayCal.set(Calendar.HOUR_OF_DAY, 0); todayDayCal.set(Calendar.MINUTE, 0); todayDayCal.set(Calendar.SECOND, 0); todayDayCal.set(Calendar.MILLISECOND, 0);

            //build calendar with current reconciliation day
            final GregorianCalendar currentReconCal = new GregorianCalendar();
            currentReconCal.setTime(currentReconciliation.getReconDate());
            currentReconCal.set(Calendar.HOUR_OF_DAY, 0); currentReconCal.set(Calendar.MINUTE, 0); currentReconCal.set(Calendar.SECOND, 0); currentReconCal.set(Calendar.MILLISECOND, 0);

            if(todayDayCal.after(currentReconCal)){
                //create new reconciliation enity. Old one became obselete
                currentReconciliation = createNewReconciliation(currentReconciliation, todayDayCal);
                try{
                    new EntityManager().SAVE(currentReconciliation);
                }
                catch (DatabaseException e){
                    throw new DacardGateException("Failed to save new DacardGateReconciliation", e);
                }
            }
            return currentReconciliation.getReconciliationIndicator();
        }

        private DacardGateReconciliation createNewReconciliation(DacardGateReconciliation prevReconciliation, Calendar todayCal) {
            final int MIN_RECON_INDICATOR = 1;
            final int MAX_RECON_INDICATOR = 999;
            DacardGateReconciliation newReconciliation = new DacardGateReconciliation();
            newReconciliation.setReconciliationIndicator(prevReconciliation != null && prevReconciliation.getReconciliationIndicator() != null ? prevReconciliation.getReconciliationIndicator() + 1 : MIN_RECON_INDICATOR);
            if (newReconciliation.getReconciliationIndicator() > MAX_RECON_INDICATOR)
                newReconciliation.setReconciliationIndicator(MIN_RECON_INDICATOR);
            newReconciliation.setReconDate(todayCal.getTime());
            return newReconciliation;
        }
    }

    /** Evaluates and applies fees for payment based on configured tariffs. */
    private class TariffEvaluator{
        private Map<Integer,Tariff> tariffs;
        /** This field used only during the parsing XML config */
        private Tariff beingAddedTariff;

        private void init() throws InitException{
            //load tariffs from "dacard_tariffs.xml"
            try{
                logger.debug("Start initialization of the TariffEvaluator");
                tariffs = new HashMap<Integer, Tariff>();
                String dacardTariffsFile = Configuration.getInstance().getProperty("dacard.tariffs_file");
                if (dacardTariffsFile == null)
                    throw new InitException("err.dacard_gate.tariffs_file_is_not_specified_in_config");

                File configFile = new File(dacardTariffsFile);
                Digester d = new Digester();
                d.push(this);
                //add digester rules
                d.addCallMethod("dacardTariffs/tariff", "addTariff", 1);
                d.addCallParam("dacardTariffs/tariff", 0, "articleId");
                d.addCallMethod("dacardTariffs/tariff/fee", "addFee", 4);
                d.addCallParam("dacardTariffs/tariff/fee", 0, "id");
                d.addCallParam("dacardTariffs/tariff/fee", 1, "calcType");
                d.addCallParam("dacardTariffs/tariff/fee", 2, "value");
                d.addCallParam("dacardTariffs/tariff/fee", 3, "alternativeValue");
                d.parse(configFile);
                logger.debug("Successful completed initialization of the TariffEvaluator");
            }
            catch (Exception e){
                throw new InitException("Failed to init TariffEvaluator", e);
            }
        }

        /** Method used during the xml config parsing only */
        public void addTariff(String articleId) throws DacardGateException{
            if(articleId == null || articleId.trim().length() <= 0)
                throw new DacardGateException("Mandatory attribute /dacardTariffs/tariff@articleId not found");
            tariffs.put(Integer.parseInt(articleId), beingAddedTariff);
            beingAddedTariff = null;
        }

        /** Method used during the xml config parsing only */
        public void addFee(String id, String calcType, String value, String alternativeValue) throws DacardGateException{
            if(calcType == null || calcType.trim().length() <= 0)
                throw new DacardGateException("Mandatory attribute /dacardTariffs/tariff/fee@calcType not found or empty");
            if(id == null || id.trim().length() <= 0)
                throw new DacardGateException("Mandatory attribute /dacardTariffs/tariff/fee@id not found or empty");

            final int calcTypeInt = Integer.parseInt(calcType);
            if(id != null){
                Fee fee = null;
                if(calcTypeInt == 0)
                    fee = new ZeroFee();
                else if(calcTypeInt == 1)
                    fee = new ExactlyGivenFee();
                else if(calcTypeInt == 2)
                    fee = new PercentRoundDownFee();
                else if(calcTypeInt == 3)
                    fee = new PercentRoundMathFee();
                else if(calcTypeInt == 4)
                    fee = new PercentRoundUpFee();

                if(fee == null){                      
                    throw new DacardGateException("Unknown fee calcTypeInt: "+calcType);
                }
                else{
                    fee.id = Integer.parseInt(id);
                    fee.calcType = calcTypeInt;
                    fee.value = (value != null && value.trim().length() > 0) ? Integer.parseInt(value) : 0;
                    fee.alternativeValue = (alternativeValue != null && alternativeValue.trim().length() > 0) ? Integer.parseInt(alternativeValue) : 0;
                    if(beingAddedTariff == null)
                        beingAddedTariff = new Tariff();
                    beingAddedTariff.addFee(fee);
                }
            }
        }


        /** Evaluates "fee1". It is always 0 in our model with Dacard.
         * @param amount of transaction;
         * @param articleId ID of article;
         * @param feeId ID of fee to be evaluated.
         * @return value of evaluated "fee1".
         * @throws DacardGateTransaction on tariff evaluation error. Lack/incorrect tariff or params */
        private int evaluateFee(Integer amount, Integer articleId, Integer feeId) throws DacardGateException{
            amount = amount != null ? amount : 0;
            if(articleId == null)
                throw new DacardGateException("Given articleId is null");
            if(feeId == null)
                throw new DacardGateException("Geven feeId is null");
            //take a Tariff by id
            final Tariff tariff = tariffs.get(articleId);
            if(tariff == null)
                throw new DacardGateException("No tariff found by given articleId", new String[]{String.valueOf(articleId)});
            final Fee fee = tariff.getFee(feeId);
            if(fee == null)
                throw new DacardGateException("No fee found by given id", new String[]{String.valueOf(feeId)});
            return fee.evaluate(amount);
        }

        private class Tariff{
            private Map<Integer,Fee> fees = new HashMap<Integer,Fee>();

            private void addFee(Fee fee){
                fees.put(fee.id,  fee);
            }

            private Fee getFee(int feeId){
                return fees.get(feeId);
            }
        }

        private abstract class Fee{
            protected int id;
            protected Integer calcType;
            protected Integer value;
            protected Integer alternativeValue;

            protected abstract int evaluate(int amount);
        }

        /** No fee applied */
        private class ZeroFee extends Fee{

            protected int evaluate(int amount) {
                return 0;
            }

        }

        /** Exactly given (predefined) amount of fee. */
        private class ExactlyGivenFee extends Fee{

            protected int evaluate(int amount) {
                return value;
            }

        }

        /** Take a percent. Then round the fractial amount down to closer integer value
         * under the amount being rounded. */
        private class PercentRoundDownFee extends Fee {

            protected int evaluate(int amount) {
                //convert commission percent into fractial (percent holded as multiplied on 1000)
                double feeAmount = (double)amount*(double)value/100000;
                //round down the raw fee amount
                feeAmount = Math.floor(feeAmount);
                if(feeAmount < alternativeValue)
                    feeAmount = alternativeValue;
                return (int)feeAmount;
            }

        }

        /** Take a percent. Round the fractial amount mathematically. */
        private class PercentRoundMathFee extends Fee {

            protected int evaluate(int amount) {
                //convert commission percent into fractial (percent holded as multiplied on 1000)
                double feeAmount = (double)amount*(double)value/100000;
                //round down the raw fee amount
                feeAmount = Math.round(feeAmount);
                if(feeAmount < alternativeValue)
                    feeAmount = alternativeValue;
                return (int)feeAmount;
            }

        }

        /** Take a percent. Round the fractial amount up to closer integer value
         * above the amount being rounded. */
        private class PercentRoundUpFee extends Fee {

            protected int evaluate(int amount) {
                //convert commission percent into fractial (percent holded as multiplied on 1000)
                double feeAmount = (double)amount*(double)value/100000;
                //round up the raw fee amount
                double cuttedAmount = Math.floor(feeAmount);
                if(feeAmount > cuttedAmount)
                    feeAmount = cuttedAmount + 1; //round up
                if(feeAmount < alternativeValue)
                    feeAmount = alternativeValue;
                return (int)feeAmount;
            }

        }
    }

    /** Provides mapping between our billing and Dacard server codes.
     * Mapping allows to fetch Dacard's articles ids by codes of our billing. */
    private class CodeMapping{
        /** Articles mapping:
         * triple key (svbillOperationCode, svbillServiceCode, svbillNominal) -> dacardArticleId */
        private Map<String,Integer> articleMapping;

        private void init() throws InitException{
            articleMapping = new HashMap<String,Integer>();
            try{
                logger.debug("Start initialization of the Dacard mapping");
                String dacardMappingFile = Configuration.getInstance().getProperty("dacard.mapping_file");
                if (dacardMappingFile == null)
                    throw new InitException("err.dacard_gate.mapping_file_is_not_specified_in_config");

                File configFile = new File(dacardMappingFile);
                Digester d = new Digester();
                d.push(this);
                //add digester rules
                d.addCallMethod("dacardMapping/article", "addArticleMapping", 4);
                d.addCallParam("dacardMapping/article", 0, "svbillOperationType");
                d.addCallParam("dacardMapping/article", 1, "svbillServiceCode");
                d.addCallParam("dacardMapping/article", 2, "svbillNominal");
                d.addCallParam("dacardMapping/article", 3, "dacardArticleId");
                d.parse(configFile);
                logger.debug("Successful completed initialization of the Dacard mapping");
            }
            catch (Exception e){
                throw new InitException("Failed to init Dacard gate mapping", e);
            }
        }

        /** Method used only during the parsing the xml data */
        public void addArticleMapping(String svbillOperationType, String svbillServiceCode, String svbillNominal, String dacardArticleId){
            if(dacardArticleId != null && dacardArticleId.trim().length() > 0)
                articleMapping.put(composeTripleKey(svbillOperationType, svbillServiceCode, svbillNominal), new Integer(dacardArticleId));
        }

        /** Resolve Dacard articleId by billing operation, service code and nominal code.
         * @param billingOperation billing operation;
         * @param billingServiceCode billing service code;
         * @param billingNominal billing nominal code;
         * @return Dacard nominal code. */
        private Integer getDacardArticleId(String billingOperation, String billingServiceCode, String billingNominal){
            return articleMapping.get(composeTripleKey(billingOperation, billingServiceCode, billingNominal));
        }

        private String composeTripleKey(String billingOperation, String billingServiceCode, String billingNominal){
            final String DELIMITER = "---!!!DELIMITER!!!---";
            final String NULL = "---!!!NULL!!!---";
            billingOperation = billingOperation == null || billingOperation.trim().length() <= 0 ? NULL : billingOperation;
            billingServiceCode = billingServiceCode == null || billingServiceCode.trim().length() <= 0 ? NULL : billingServiceCode;
            billingNominal = billingNominal == null || billingNominal.trim().length() <= 0 ? NULL : billingNominal;

            return new StringBuffer().
                    append(billingOperation).append(DELIMITER).
                    append(billingServiceCode).append(DELIMITER).
                    append(billingNominal).
                    toString();
        }

    }


    private interface ConnectionPool{
        void init() throws InitException;
        void shutdown();
        Socket allocateSocket() throws DacardGateException;
        void returnSocket(Socket socket);
    }

    /** This is rather connection factory, but not a connection pool. It just creates new sockets each time. */
    private class SimpleConnectionPool implements ConnectionPool{
        private SocketAddress serverSocketAddress;

        public void init() throws InitException{
            final String host = properties.getProperty(PROP_SERVER_HOST);
            final int port = Integer.parseInt(properties.getProperty(PROP_SERVER_PORT));
            serverSocketAddress = new InetSocketAddress(host, port);
        }

        public void shutdown(){
        }

        /**
         * Allocate socket from the pool
         */
        public synchronized Socket allocateSocket() throws DacardGateException {
            try {
                final Socket s = new Socket();
                s.setKeepAlive(true);
                s.connect(serverSocketAddress);
                return s;
            }
            catch (Exception e) {
                throw new DacardGateException("Failed to open new TCP socket to Dacard server", e);
            }
        }

        /** Return allocated socket to the pool. */
        public synchronized void returnSocket(Socket socket){
            try{ socket.getInputStream().close(); }catch (IOException e){/** ignore */}
            try{ socket.getOutputStream().close(); }catch (IOException e){/** ignore */}
            try{ socket.close(); }catch (IOException e){/** ignore */}
        }

    }

    /** Connection pool is used to manage TCP connections to Dacard server.
     * There are two problems why it doesn't work with Dacard sercer properly:
     * 1) Dacard closes sockets very quickly, so there is no sense to hold clients sockets opened on our side.
     * 2) When new client on our side take used socket from pool and begin to read data from its input stream,
     *      it reads the old garbage data of zeros. (this issue could be fixed...).
     * so for now use SimpleConnectionPool. */
    private class HoldingConnectionPool implements ConnectionPool{
        private SocketAddress serverSocketAddress;
        private int maxTcpConnections;
        private List<Socket> availableSockets;
        private Map<Socket,Long> allocatedSockets;
        private int maxAllocationTime;

        public void init() throws InitException{
            final String host = properties.getProperty(PROP_SERVER_HOST);
            final int port = Integer.parseInt(properties.getProperty(PROP_SERVER_PORT));
            serverSocketAddress = new InetSocketAddress(host, port);
            maxTcpConnections = Integer.parseInt(properties.getProperty(PROP_MAX_TCP_CONNECTIONS, "30"));
            availableSockets = new LinkedList<Socket>();
            allocatedSockets = new HashMap<Socket,Long>();
            maxAllocationTime = Integer.parseInt(properties.getProperty(PROP_MAX_ALLOCATION_TIME, "60")) * 1000;
        }

        public void shutdown(){
            for(Socket s : availableSockets)
                shutdownSocket(s);
            availableSockets = null;

            for(Socket s : allocatedSockets.keySet())
                shutdownSocket(s);
            allocatedSockets = null;
        }

        /** Allocate socket from the pool */
        public synchronized Socket allocateSocket() throws DacardGateException{
            if(availableSockets.size() + allocatedSockets.size() >= maxTcpConnections){
                //check is there over-held socket among allocated.
                final Map<Socket,Long> nonOverHeldSockets = new HashMap<Socket,Long>();
                final Set<Socket> overHeldSockets = new HashSet<Socket>();
                final long currentTime = System.currentTimeMillis();
                for(Socket socket : allocatedSockets.keySet()){
                    final Long socketAllocationTime = allocatedSockets.get(socket);
                    //separate overheld and non-overheld sockets
                    if(socketAllocationTime + maxAllocationTime >= currentTime)
                        nonOverHeldSockets.put(socket, socketAllocationTime);
                    else
                        overHeldSockets.add(socket);
                }
                allocatedSockets = nonOverHeldSockets;

                //shutdown over-held sockets
                for(Socket socket : overHeldSockets)
                    shutdownSocket(socket);

                //check if we shutdown some sockets to reduce amount of allocated one.
                if(overHeldSockets.size() <= 0)
                    throw new DacardGateException("Max TCP connections limit exceeded. You can increase it in gate.xml", new String[]{String.valueOf(maxTcpConnections)});
            }
            Socket socket;
            if(availableSockets.size() > 0){
                // allocate available opened socket and check it before return
                socket = availableSockets.remove(0);//allocate socket from the head of the list.
                if(!isSocketOpen(socket)){
                    shutdownSocket(socket);
                    socket = openNewSocket();
                }

            }
            else {
                // produce and open new socket, then allocate it
                socket = openNewSocket();
            }
            allocatedSockets.put(socket, System.currentTimeMillis());
            return socket;
        }


        /** Return allocated socket to the pool. */
        public synchronized void returnSocket(Socket socket){
            availableSockets.add(socket); // add returned socket to the end of the list. Thus we handle available sockets list like FIFO
            allocatedSockets.remove(socket);
        }

        private Socket openNewSocket() throws DacardGateException{
            try{
                final Socket s = new Socket();
                s.setKeepAlive(true);
                s.connect(serverSocketAddress);
                return s;
            }
            catch (Exception e){
                throw new DacardGateException("Failed to open new TCP socket to Dacard server", e);
            }
        }

        private void shutdownSocket(Socket socket) {
            try{ socket.getInputStream().close(); }catch (IOException e){/** ignore */}
            try{ socket.getOutputStream().close(); }catch (IOException e){/** ignore */}
            try{ socket.close(); }catch (IOException e){/** ignore */}
        }

        private boolean isSocketOpen(Socket socket){
            if (socket.isClosed() || socket.isInputShutdown() || socket.isOutputShutdown() || !socket.isConnected())
                return false;

            try{
                byte[] testPacket = new byte[]{0, 0, 0, 0 ,0, 0, 0, 0};
                final OutputStream os = socket.getOutputStream();
                os.write(testPacket);
                os.flush();
                socket.getInputStream().read(testPacket);
            }
            catch (IOException e){
                return false;
            }
            return true;
        }
    }

    /** Handles security keys */
    private class Security{
        private byte[] sessionKey;
        private byte[] masterKey;

        private void init() throws InitException{
            /** Hardcoded master key */
            //AB 6E 8A B5 AE 7F 4F D9 - given by Oleh Mykytuk
            masterKey = new byte[]{(byte)0xAB, (byte)0x6E, (byte)0x8A, (byte)0xB5, (byte)0xAE, (byte)0x7F, (byte)0x4F, (byte)0xD9};
            //....
            final String encodedSessionKey = properties.getProperty(PROP_ENCODED_SESS_KEY);
            if(encodedSessionKey == null)
                throw new InitException("Session key not found in gate.xml");   

            try{
                sessionKey = decodeSessionKey(masterKey, new HexConverter().getHexFromString(encodedSessionKey));
            }
            catch (DacardCypherException e){
                throw new InitException("Failed to decrypt session key", e);
            }
        }

        private byte[] getSessionKey(){
            return sessionKey;
        }

        private void setSessionKey(byte[] key){
            this.sessionKey = key;
        }

        private byte[] getMasterKey(){
            return masterKey;
        }

        private byte[] decodeSessionKey(byte[] masterKey, byte[] encodedSessionKey) throws DacardCypherException {
            try {
                final KeySpec keyspec = new DESKeySpec(masterKey);
                final SecretKeyFactory keyfactory = SecretKeyFactory.getInstance("DES");
                final SecretKey secretKey = keyfactory.generateSecret(keyspec);
                final Cipher cipher = Cipher.getInstance("DES/ECB/NoPadding");
                final int BLOCK_SIZE = 8;
                cipher.init(Cipher.DECRYPT_MODE, secretKey);

                //provide padding by 0x00 up to the end of last encoded block. Block length is 8 bytes.
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                baos.write(encodedSessionKey);
                final int paddingBlockLength = (encodedSessionKey.length % BLOCK_SIZE == 0) ? BLOCK_SIZE /** last block completelly should be 0 */ : BLOCK_SIZE - encodedSessionKey.length % BLOCK_SIZE;
                byte[] paddingBlock = new byte[paddingBlockLength];
                Arrays.fill(paddingBlock, (byte) 0);
                baos.write(paddingBlock);
                byte[] decodedData = cipher.doFinal(baos.toByteArray());
                byte[] sessionKey = new byte[BLOCK_SIZE];
                System.arraycopy(decodedData, 0, sessionKey, 0, BLOCK_SIZE);
                return sessionKey;
            }
            catch (Exception e) {
                throw new DacardCypherException("Failed to decrypt session key", e);
            }
        }
    }

   /** Dacard protocol reference. */
    private static class Reference {

        /** Message Type Codes */
       private enum MessageTypeCode {
            MT_DBLOAD((byte)1),
            MT_RECV((byte)2),
            MT_CONF((byte)3),
            MT_REVER((byte)4),
            MT_SYNCTIME((byte)5),
            MT_EOD((byte)6),
            MT_TPLOAD((byte)7),
            MT_TP_ASK_FOR_NEW_VERSION((byte)8),
            MT_TP_GET_NEW_VERSION((byte)9),
            MT_ONLINE_REPLENISHMENT((byte)10),
            MT_CHECK_REPLENISHMENT_STATE((byte)11),
            MT_REGISTERNEWTERMINAL((byte)12),
            MT_UPDATETERMINALINFO((byte)13),
            MT_TEST((byte)14),
            MT_VALIDATE_INVOICE_INFO((byte)15),
            MT_SUBST_TERMINAL((byte)16),
            MT_SUBST_CONFIRM((byte)17),
            MT_SUBST_FAILED((byte)18),
            MT_SUBST_REVERSE((byte)19),
            MT_CLOSE_SESSION((byte)20),
            MT_GET_PURCHASE_STATUS((byte)21);

            private byte code;

            MessageTypeCode(byte code){
                this.code = code;
            }

        }

        /** Error codes */
       private enum ErrorCode {
            SUCCESS(0),
            INCORRECT_AMOUNT(110),
            INCORRECT_NUMBER(111),
            LOADING_FINISHED(114),
            FUNCTION_ISNT_SUPPORTED(115),
            FORBIDDEN_OPERATION(120),
            LIMIT_EXCEEDED(121),
            TERMINAL_DUPLICATED(125),
            INCORRECT_COMMISSION(130),
            ERROR_CODE_NOT_USED_1(302),
            BLOCKED_ACCOUNT(305),
            UNKNOWN_FILE(309),
            OPERATION_NOT_COMPLITED(311),
            OPERATION_CANCELED_BY_TERM(312),
            OPERATION_CANCELED_BY_ADMIN(313),
            OPERATION_BLOCKED_BY_OPERATOR(314),
            ERROR_STATE(315),
            DATA_FORMAT_EROR(904),
            NO_PRODUCTS(905),
            SERVER_ERROR(909),
            DUPLICATED_TRANSACTION(913),
            NO_ORIGINAL_OPERATION(914),
            CRYPTO_ERROR(916),
            IN_PROGRESS(923),
            ERROR_CODE_NOT_USED_2(940),
            NEXT_PARTNER_ERROR(941),
            HIGH_TIME_DEVIATION(947);

            private int code;

            private ErrorCode(int code){
                this.code = code;
            }
        }

       private static class DacardValueConverter {
            private static final SimpleDateFormat dateFormat = new SimpleDateFormat("yyMMddHHmmss");
            private static final Pattern datePattern = Pattern.compile("(\\d{4})-(\\d{2})-(\\d{2})T(\\d{2}):(\\d{2}):(\\d{2})");

            /** Return date in format "yyMMddHHmmss" (Example: 2001.12.31 23:50:59 = �011231235059�)
             * @param date to be formatted into string.
             * @return formatted date like a string. */
            private static String formatDate(Date date){
                return date != null ? dateFormat.format(date) : "";
            }

            /** Return date parsed from string by format "yyyy-MM-ddTHH:mm:ss" (Example: "2007-06-29T11:30:20")
             * @param formattedDate string to be parsed into date.
             * @return parsed date. */
            private static Date parseDate(String formattedDate){
                Date date = null;
                if(formattedDate == null)
                    return null;
                Matcher matcher = datePattern.matcher(formattedDate);
                if(matcher.matches()){
                    Calendar cal = Calendar.getInstance();
                    cal.clear();
                    cal.set(Calendar.YEAR, Integer.parseInt(matcher.group(1)));
                    cal.set(Calendar.MONTH, Integer.parseInt(matcher.group(2)));
                    cal.set(Calendar.DAY_OF_MONTH, Integer.parseInt(matcher.group(3)));
                    cal.set(Calendar.HOUR_OF_DAY, Integer.parseInt(matcher.group(4)));
                    cal.set(Calendar.MINUTE, Integer.parseInt(matcher.group(5)));
                    cal.set(Calendar.SECOND, Integer.parseInt(matcher.group(6)));
                    date = cal.getTime();
                }
                return date;
            }


            /** Multiply double amount by 100 and return integer
             * @param value double to be multiplied and converted into int.
             * @return multiplied and converted double like a int. */
            private static Integer convertAmount(Double value){
                return value == null ? 0 : (int)(value * 100);
            }

            /** Cut string to fit 8 chars
             * @param value string value to be cutted into string.
             * @return cutted string. */
            private static String formatTerminalId(String value){
                return value == null ? "00000000" : (value.length() <= 8) ? value : value.substring(0, 8);
            }


            private static String formatInvoiceNumber(String value){
                if (value.length() == 9) {
                    value = "80".concat(value);
                }
                return !value.startsWith("8") ? value : "3".concat(value);
            }

            /** 2 last symbols of TerminalID + 5 last digits of LocalDate + 5 last digits of STAN.
             * @param terminalId terminalId;
             * @param msgCount STAN;
             * @param localDate localDate;
             * @return 12 digits RRN */
            private static String formatRRN(String terminalId, int msgCount, String localDate){
                String rrn = "";
                int length = terminalId.length();
                rrn = terminalId.substring(length-2, length);
                length = localDate.length();
                rrn = rrn + localDate.substring(length-5, length);
                final String msgCountStr = "00000" + msgCount; //add zeros before  
                length = msgCountStr.length();
                rrn = rrn + msgCountStr.substring(length-5, length);
                return rrn;
            }
        }

       private static class ResponseCodeResolver{
           private static final Set<Integer> notFinalStatus;

           static {
               notFinalStatus = new HashSet<Integer>();
               notFinalStatus.add(ErrorCode.IN_PROGRESS.code);
           }


           private static boolean isStatusNotFinal(Integer errorCode){
               return notFinalStatus.contains(errorCode); 
           }
       }

    }


}

