package com.tmx.web.actions.core.ui_permission;

import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForm;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.tmx.web.base.BasicDispatchedAction;
import com.tmx.web.forms.core.ui_permission.ManageRoleForm;

public class ManageRoleAction extends BasicDispatchedAction {
    private static final String FORWARD = "page";
    public ActionForward defaultAction(ActionMapping mapping,
                                       ActionForm form,
                                       HttpServletRequest request,
                                       HttpServletResponse response,
                                       String methodName) throws Throwable {
        ManageRoleForm manageUserInterfaceForm = (ManageRoleForm) verifyForm(form, ManageRoleForm.class);
        manageUserInterfaceForm.getTable().setRefreshActionName(this.getName());
        manageUserInterfaceForm.getTable().refresh();
        // set Readonly button
        if(manageUserInterfaceForm.getTable().getResultSize().intValue()==0){
            manageUserInterfaceForm.getOnDeleteButton().setReadonly(true);
            manageUserInterfaceForm.getOnEditButton().setReadonly(true);
        }else{
            manageUserInterfaceForm.getOnDeleteButton().setReadonly(false);
            manageUserInterfaceForm.getOnEditButton().setReadonly(false);
        }
        //initStructRelations(manageUserInterfaceForm);
        return mapping.findForward(FORWARD);
    }
}

