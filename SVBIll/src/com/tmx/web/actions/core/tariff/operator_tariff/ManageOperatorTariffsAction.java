package com.tmx.web.actions.core.tariff.operator_tariff;

import com.tmx.web.actions.core.tariff.AbstractManageTariffsAction;
import com.tmx.web.forms.core.tariff.operator_tariff.ManageOperatorTariffsForm;
import com.tmx.web.controls.ScrollBox;
import com.tmx.as.blogic.tariff.AbstractTariffManager;
import com.tmx.as.blogic.tariff.OperatorTariffManager;
import com.tmx.as.blogic.OperatorManager;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForm;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class ManageOperatorTariffsAction extends AbstractManageTariffsAction {

    public static final String PARAMETER_OWNER_NAME = "operatorId";
    
    protected AbstractTariffManager getTariffManager() {
        return new OperatorTariffManager();  
    }

    //roadmap setuping
    public ActionForward defaultAction(ActionMapping mapping,
                                       ActionForm form,
                                       HttpServletRequest request,
                                       HttpServletResponse response,
                                       String methodName) throws Throwable {
        ManageOperatorTariffsForm tariffsForm = (ManageOperatorTariffsForm) form;
        tariffsForm.bindOperators(new OperatorManager().getAllOperators());
        getRoadmap().collectParameter(DEFAULT_VALID_ACTION, "onCancelAction", "/core/tariff/operator_tariff/manageOperatorTariffs");
        getRoadmap().collectParameter(DEFAULT_VALID_ACTION, "onSaveAction", "manageTariffsAction");
        return super.defaultAction(mapping, form, request, response, methodName);
    }

    public ActionForward onCreateTariffAction(ActionMapping mapping,
                                       ActionForm form,
                                       HttpServletRequest request,
                                       HttpServletResponse response) throws Throwable {
        ManageOperatorTariffsForm operatorTariffsForm = (ManageOperatorTariffsForm) form;
        String selectedKey = ((ScrollBox) operatorTariffsForm.getTariffsTable().getParameterControl("by_operator_id", "operator_id")).getSelectedKey();
        getRoadmap().collectParameter("/core/tariff/operator_tariff/infoOperatorTariff", PARAMETER_OWNER_NAME, selectedKey);
        return mapping.findForward("infoTariffAction");
    }
}
