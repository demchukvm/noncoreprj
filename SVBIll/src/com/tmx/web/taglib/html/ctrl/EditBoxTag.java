package com.tmx.web.taglib.html.ctrl;

import com.tmx.web.controls.EditBox;

import javax.servlet.jsp.JspException;

import org.apache.struts.taglib.TagUtils;

/**
 * Render edit box tag
 */
public class EditBoxTag extends AbstractControlTag {
    //CSS styles properties
    private String styleClass = null;
    private String readonlyStyleClass = null;

    //------------------------------Properties
    public String getStyleClass() {
        return (styleClass != null) ? styleClass : getDefaultProperty("tag.editBox.style.styleClass");
    }

    public void setStyleClass(String styleClass) {
        this.styleClass = styleClass;
    }

    public String getReadonlyStyleClass() {
        return (readonlyStyleClass != null) ? readonlyStyleClass : getDefaultProperty("tag.editBox.style.styleClassReadonly");
    }

    public void setReadonlyStyleClass(String readonlyStyleClass) {
        this.readonlyStyleClass = readonlyStyleClass;
    }

    //------------------------------Protected methods
    protected int controlStartTag() throws JspException {
        StringBuffer resultHTML = new StringBuffer();
        EditBox eb = getEditBox();
        if(eb == null){
            TagUtils.getInstance().write(pageContext,"EditBox '"+getProperty()+"' is not found");
            return SKIP_BODY;
        }

        String readonly = (eb.isReadonly()) ? " readonly=\"true\" " : "";
        String value = " value=\""+((eb.getValue() == null) ? "" : eb.getValue()) +"\" ";
        String styleClass = " class=\""+((eb.isReadonly()) ? getReadonlyStyleClass() : getStyleClass())+"\" ";
        String type = " type=\"text\" ";
        String name = "name=\""+getProperty()+"."+EditBox.VALUE_SUFFIX+"\"";
        //Render HTML
        resultHTML.append("\r");
        resultHTML.append("<input " + name + type + value + styleClass + readonly + renderCssStyleAttribute() + "/>\r");

        TagUtils.getInstance().write(pageContext,resultHTML.toString());
        return SKIP_BODY;
    }


   protected EditBox getEditBox() throws JspException{
        Object object = getPropertyObject();
        return (object != null && object instanceof EditBox) ?
               (EditBox)object : null;
   }
}

