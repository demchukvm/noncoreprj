package com.tmx.beng.base.dbfunctions;

import com.tmx.beng.base.Reservation;
import com.tmx.beng.base.BillingMessage;
import com.tmx.beng.base.cache.InvokeFunctionException;
import com.tmx.as.entities.bill.transaction.TransactionType;

import java.util.Map;
import java.util.ArrayList;
import java.util.List;
import java.math.BigDecimal;

public class SuperPuperExternalComissionTariff extends SellerOperatorTariff
{
    public static final String SELLER_DELTA = "seller_delta";
    public static final String OPERATOR_LIMIT = "operator_limit";
    public static final String OPERATOR_L_LIMIT = "operator_l_limit";
    public static final String FEE = "fee";
    public static final String Y = "y";

    void applyTariff(Map actualParams, Reservation reservation) throws InvokeFunctionException
    {
        try {
            String serviceCodeInMessage = task.getProcesedMessage().getAttributeString(BillingMessage.SERVICE_CODE);
            if (actualParams.get(SERVICE_CODE).equals(serviceCodeInMessage))
            {
                double y = Double.parseDouble((String)actualParams.get(Y));
                double txCost = Double.parseDouble((String)actualParams.get(TX_COST_OP));
                double discontPercent = Double.parseDouble((String)actualParams.get(DISCOUNT_PERCENT_OP));
                double sellerDelta = Double.parseDouble((String)actualParams.get(SELLER_DELTA));
                double operatorHLimit = Double.parseDouble((String)actualParams.get(OPERATOR_LIMIT));
                double operatorLLimit = 0.00;


                if (actualParams.get(OPERATOR_L_LIMIT) != null && !actualParams.get(OPERATOR_L_LIMIT).equals(""))
                {
                    operatorLLimit = Double.parseDouble((String)actualParams.get(OPERATOR_L_LIMIT));
                }
                double fee = 0.00;
                if (actualParams.get(FEE) != null)
                {
                    fee = Double.parseDouble((String)actualParams.get(FEE));
                }

                double totalAmount = Double.parseDouble(task.getProcesedMessage().getAttributeString(BillingMessage.TOTAL_AMOUNT));
                if (totalAmount < operatorHLimit && totalAmount >= operatorLLimit)
                {
                    //TODO: stuped way. FIX IT!!!                                                                               
                    if(task.getProcesedMessage().getAttributeString(BillingMessage.IS_OPERATOR_AMOUNT_SET) == null)
                    {
                        double amount = totalAmount - fee;
                        BigDecimal amountBD = new BigDecimal(amount);
                        amountBD = amountBD.setScale(ROUND_SCALE, BigDecimal.ROUND_HALF_UP);
                        task.getProcesedMessage().setAttributeString(BillingMessage.AMOUNT, ((Double)amountBD.doubleValue()).toString());
                        task.getProcesedMessage().setAttributeString(BillingMessage.IS_OPERATOR_AMOUNT_SET, "set");

                    }
                    //double tarrifedAmount = (amount + sellerDelta);
                    double tarrifedAmount = (totalAmount-fee+sellerDelta-txCost-y)*discontPercent;        // списываем с баланса



                    //save to DB
                    BigDecimal tarrifedAmountBD = new BigDecimal(tarrifedAmount);
                    tarrifedAmountBD = tarrifedAmountBD.setScale(ROUND_SCALE, BigDecimal.ROUND_HALF_UP);
                    reservation.reserve(null, tarrifedAmountBD.doubleValue(), fee, null, TransactionType.CODE_CREDIT);

                    task.getProcesedMessage().setAttributeString(BillingMessage.Y, ((Double)y).toString());
                }
                else
                {
                    throw new InvokeFunctionException("err.graduated_refill_tariff.amount_out_of_range");
                }
            }
        } catch(Throwable e) {
            throw new InvokeFunctionException("err.graduated_refill_tariff.calc_error", e);
        }
    }

    public ValidationError[] validate(Map actualParams)
    {
        List resultErrors = new ArrayList();
        resultErrors.addAll(ParameterValidator.validateTxCost(actualParams, new String[] {TX_COST_OP}));
        resultErrors.addAll(ParameterValidator.validateDiscountPercent(actualParams, new String[] {DISCOUNT_PERCENT_OP}));
        return (ValidationError[]) resultErrors.toArray(new ValidationError[resultErrors.size()]);
    }

    public TariffParameter[] getDefaultParams()
    {
        TariffParameter paramOperatorCode = new TariffParameter();
    	paramOperatorCode.setName(SERVICE_CODE);
    	paramOperatorCode.setValue("UMCPoP");
        paramOperatorCode.setOrderNum(1l);
        paramOperatorCode.setMandatory(Boolean.TRUE);

        TariffParameter paramDisPer = new TariffParameter();
    	paramDisPer.setName(DISCOUNT_PERCENT_OP);
    	paramDisPer.setValue("1");
        paramDisPer.setOrderNum(2l);
        paramDisPer.setMandatory(Boolean.TRUE);

        TariffParameter paramCost = new TariffParameter();
    	paramCost.setName(TX_COST_OP);
    	paramCost.setValue("0");
        paramCost.setOrderNum(3l);
        paramCost.setMandatory(Boolean.TRUE);

        TariffParameter paramSellerDelta = new TariffParameter();
    	paramSellerDelta.setName(SELLER_DELTA);
    	paramSellerDelta.setValue("0.3");
        paramSellerDelta.setOrderNum(4l);
        paramSellerDelta.setMandatory(Boolean.TRUE);

        TariffParameter paramOperatorLimit = new TariffParameter();
    	paramOperatorLimit.setName(OPERATOR_LIMIT);
    	paramOperatorLimit.setValue("6000");
        paramOperatorLimit.setOrderNum(5l);
        paramOperatorLimit.setMandatory(Boolean.TRUE);

        TariffParameter paramOperatorLLimit = new TariffParameter();
    	paramOperatorLLimit.setName(OPERATOR_L_LIMIT);
    	paramOperatorLLimit.setValue("0.00");
        paramOperatorLLimit.setOrderNum(6l);
        paramOperatorLLimit.setMandatory(Boolean.TRUE);

        TariffParameter paramFee = new TariffParameter();
    	paramFee.setName(FEE);
    	paramFee.setValue("1");
        paramFee.setOrderNum(7l);
        paramFee.setMandatory(Boolean.TRUE);

        TariffParameter y = new TariffParameter();
    	paramFee.setName(Y);
    	paramFee.setValue("0");
        paramFee.setOrderNum(7l);
        paramFee.setMandatory(Boolean.TRUE);

        return new TariffParameter[] {
                paramDisPer,
                paramOperatorCode,
                paramCost,
                paramSellerDelta, //SOME PROBLEM HERE
                paramOperatorLimit,
                paramOperatorLLimit,
                paramFee
        };
    }
}