package com.tmx.beng.httpsgate.kyivstar;

import org.apache.log4j.Logger;
import com.tmx.util.InitException;
import com.tmx.beng.httpsgate.HttpsGateException;

/**
 *
 * Implementation of Kyivstar gate used for exclusive terminals
 */
public class KyivstarExclusiveGate extends BasicKyivstarGate {
    public static final String GATE_NAME = "kyivstar_exclusive_gate";
    protected static Logger logger = Logger.getLogger(GATE_NAME);
    private static KyivstarExclusiveGate gate = null;

    public static KyivstarExclusiveGate getInstance()throws HttpsGateException {
        if(gate == null){
            logger.error("Attempt to obtain not inited billing engine");
            throw new HttpsGateException("err.KyivstarExclusiveGate.null_instance");
        }
        return gate;
    }

    public static void init()throws InitException{
        logger.info("Start KyivstarExclusiveGate initialization");
        gate = new KyivstarExclusiveGate();
        gate.init0();
        logger.info("KyivstarExclusiveGate initialized");
    }

    public static void shutdown(final long timeout) throws InitException {
        logger.info("Start KyivstarExclusiveGate Shutdown");
        gate.shutdown0(timeout);
        logger.info("KyivstarExclusiveGate shutdown");
    }

    protected String getGateName(){
        return GATE_NAME;
    }
}
