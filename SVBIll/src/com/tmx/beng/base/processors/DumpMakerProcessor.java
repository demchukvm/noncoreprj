package com.tmx.beng.base.processors;

import com.tmx.beng.base.*;
import com.tmx.beng.base.cache.DBCache;
import com.tmx.beng.base.cache.DBCacheException;


public class DumpMakerProcessor extends TaskProcessor{

    protected Task process0(Task task, DBCache dbCache, BillingMessageWritable msg) throws BillException {

        try {
            dbCache.dump();
        } catch (DBCacheException e) {
            throw new BillException(e.getLocalizedMessage(), e);
        }

        CallBack callBack = task.getCallBack();
        if(callBack != null){
            callBack.sendBilledMessage( new BillingMessageImpl() );
        }
        return task;

    }

}
