package com.tmx.web.taglib.html.ctrl;

import com.tmx.web.controls.BrowseBox;
import com.tmx.web.controls.BasicControl;
import com.tmx.web.base.WebException;
import com.tmx.web.base.ApplicationEnvironment;

import javax.servlet.jsp.JspException;

import org.apache.struts.config.ModuleConfig;
import org.apache.struts.config.ActionConfig;
import org.apache.struts.taglib.TagUtils;

import java.util.GregorianCalendar;

/**
    Renders button to open browse window and edit with hidden to handle returned value.
    Tag is assigned to BrowseBox control.
 
    @See com.tmx.web.controls.BrowseBox,
    @See com.tmx.web.taglib.html.ctrl.BrowseCallBackTag 
 */
public class BrowseBoxTag extends AbstractControlTag{
    //constants to cooperates with BrowsePickTag
    public static final String PICK_FUNCTION_PREFIX = "_pick";
    public static final String KEY_ID_PARAM_NAME = "keyId";
    public static final String VALUE_ID_PARAM_NAME = "valueId";
    public static final String BROWSE_PARAM_NAME = "browse";
    //CSS styles properties
    private String editBoxStyleClass;
    private String editBoxDisabledStyleClass;
    private String buttonStyleClass;
    private String buttonDisabledStyleClass;
    private String winFeatureWidth;
    private String winFeatureHeight;
    private String winFeatureChannelmode;
    private String winFeatureDirectories;
    private String winFeatureLocation;
    private String winFeatureMenubar;
    private String winFeatureResizable;
    private String winFeatureScrollbars;
    private String winFeatureStatus;
    private String winFeatureTitlebar;
    private String winFeatureToolbar;
    private String winFeatureTop;
    private String winFeatureLeft;

    //---------Properties
    public String getEditBoxStyleClass() {
        return (editBoxStyleClass != null) ? editBoxStyleClass : getDefaultProperty("tag.browseBox.style.editBoxStyleClass");
    }

    public void setEditBoxStyleClass(String editBoxStyleClass) {
        this.editBoxStyleClass = editBoxStyleClass;
    }

    public String getEditBoxDisabledStyleClass() {
        return (editBoxDisabledStyleClass != null) ? editBoxDisabledStyleClass : getDefaultProperty("tag.browseBox.style.editBoxDisabledStyleClass");
    }

    public void setEditBoxDisabledStyleClass(String editBoxDisabledStyleClass) {
        this.editBoxDisabledStyleClass = editBoxDisabledStyleClass;
    }

    public String getButtonStyleClass() {
        return (buttonStyleClass != null) ? buttonStyleClass : getDefaultProperty("tag.browseBox.style.buttonStyleClass");
    }

    public void setButtonStyleClass(String buttonStyleClass) {
        this.buttonStyleClass = buttonStyleClass;
    }

    public String getButtonDisabledStyleClass() {
        return (buttonDisabledStyleClass != null) ? buttonDisabledStyleClass : getDefaultProperty("tag.browseBox.style.buttonDisabledStyleClass");
    }

    public void setButtonDisabledStyleClass(String buttonDisabledStyleClass) {
        this.buttonDisabledStyleClass = buttonDisabledStyleClass;
    }

    public String getWinFeatureWidth() {
        return (winFeatureWidth != null) ? winFeatureWidth : getDefaultProperty("tag.browseBox.winFeatureWidth");
    }

    public void setWinFeatureWidth(String winFeatureWidth) {
        this.winFeatureWidth = winFeatureWidth;
    }

    public String getWinFeatureHeight() {
        return (winFeatureHeight != null) ? winFeatureHeight : getDefaultProperty("tag.browseBox.winFeatureHeight");
    }

    public void setWinFeatureHeight(String winFeatureHeight) {
        this.winFeatureHeight = winFeatureHeight;
    }


    public String getWinFeatureChannelmode() {
        return (winFeatureChannelmode != null) ? winFeatureChannelmode : getDefaultProperty("tag.browseBox.winFeatureChannelmode");
    }

    public void setWinFeatureChannelmode(String winFeatureChannelmode) {
        this.winFeatureChannelmode = winFeatureChannelmode;
    }

    public String getWinFeatureDirectories() {
        return (winFeatureDirectories != null) ? winFeatureDirectories : getDefaultProperty("tag.browseBox.winFeatureDirectories");
    }

    public void setWinFeatureDirectories(String winFeatureDirectories) {
        this.winFeatureDirectories = winFeatureDirectories;
    }

    public String getWinFeatureLocation() {
        return (winFeatureLocation != null) ? winFeatureLocation : getDefaultProperty("tag.browseBox.winFeatureLocation");
    }

    public void setWinFeatureLocation(String winFeatureLocation) {
        this.winFeatureLocation = winFeatureLocation;
    }

    public String getWinFeatureMenubar() {
        return (winFeatureMenubar != null) ? winFeatureMenubar : getDefaultProperty("tag.browseBox.winFeatureMenubar");
    }

    public void setWinFeatureMenubar(String winFeatureMenubar) {
        this.winFeatureMenubar = winFeatureMenubar;
    }

    public String getWinFeatureResizable() {
        return (winFeatureResizable != null) ? winFeatureResizable : getDefaultProperty("tag.browseBox.winFeatureResizable");
    }

    public void setWinFeatureResizable(String winFeatureResizable) {
        this.winFeatureResizable = winFeatureResizable;
    }

    public String getWinFeatureScrollbars() {
        return (winFeatureScrollbars != null) ? winFeatureScrollbars : getDefaultProperty("tag.browseBox.winFeatureScrollbars");
    }

    public void setWinFeatureScrollbars(String winFeatureScrollbars) {
        this.winFeatureScrollbars = winFeatureScrollbars;
    }

    public String getWinFeatureStatus() {
        return (winFeatureStatus != null) ? winFeatureStatus : getDefaultProperty("tag.browseBox.winFeatureStatus");
    }

    public void setWinFeatureStatus(String winFeatureStatus) {
        this.winFeatureStatus = winFeatureStatus;
    }

    public String getWinFeatureTitlebar() {
        return (winFeatureTitlebar != null) ? winFeatureTitlebar : getDefaultProperty("tag.browseBox.winFeatureTitlebar");
    }

    public void setWinFeatureTitlebar(String winFeatureTitlebar) {
        this.winFeatureTitlebar = winFeatureTitlebar;
    }

    public String getWinFeatureToolbar() {
        return (winFeatureToolbar != null) ? winFeatureToolbar : getDefaultProperty("tag.browseBox.winFeatureToolbar");
    }

    public void setWinFeatureToolbar(String winFeatureToolbar) {
        this.winFeatureToolbar = winFeatureToolbar;
    }

    public String getWinFeatureTop() {
        return (winFeatureTop != null) ? winFeatureTop : getDefaultProperty("tag.browseBox.winFeatureTop");
    }

    public void setWinFeatureTop(String winFeatureTop) {
        this.winFeatureTop = winFeatureTop;
    }

    public String getWinFeatureLeft() {
        return (winFeatureLeft != null) ? winFeatureLeft : getDefaultProperty("tag.browseBox.winFeatureLeft");
    }

    public void setWinFeatureLeft(String winFeatureLeft) {
        this.winFeatureLeft = winFeatureLeft;
    }

    public int controlStartTag() throws JspException {
        StringBuffer resultHTML = new StringBuffer();
        BrowseBox browseBox = getBrowseBox();
        if(browseBox == null){
            TagUtils.getInstance().write(pageContext,"BrowseBox '"+getProperty()+"' is not found");
            return SKIP_BODY;
        }    
        //Render edit box - 'Value holder'
        String readonly = " readonly=\"true\" ";//to prevent manual value input
        String value = " value=\""+((browseBox.getValue() == null) ? "" : browseBox.getValue()) +"\" ";
        String styleClass = " class=\""+((browseBox.isReadonly()) ? getEditBoxDisabledStyleClass() : getEditBoxStyleClass())+"\" ";
        String type = " type=\"text\" ";
        String name = "name=\""+ getValueHtmlHandlerId()+"\"";
        String valueId = "id=\""+ getValueHtmlHandlerId() +"\"";
        resultHTML.append("\r");
        resultHTML.append("<input " + valueId + name + type + value + styleClass + readonly + renderCssStyleAttribute() + "/>\r");

        //Render 'choose' button
        type = " type=\"button\" ";
        name = " name=\""+getProperty()+"."+BrowseBox.BUTTON_SUFFIX+"\"";
        value = " value=\""+getLocalizedLabel("tag.browseBox.label.choose", null)+"\" ";
        styleClass = " class=\""+(browseBox.isReadonly() ? getButtonDisabledStyleClass() : getButtonStyleClass())+"\" ";
        readonly = (browseBox.isReadonly()) ? " disabled " : "";
        resultHTML.append("\r");
        resultHTML.append("<input " + type + name + value + styleClass + readonly + getOnEventAttrsString() + "/>\r");

        //Render 'reset' button
        type = " type=\"button\" ";
        value = " value=\""+getLocalizedLabel("tag.browseBox.label.reset", null)+"\" ";
        styleClass = " class=\""+(browseBox.isReadonly() ? getButtonDisabledStyleClass() : getButtonStyleClass())+"\" ";
        readonly = (browseBox.isReadonly()) ? " disabled " : "";
        String onClick = " onClick=\"" + getResetFunctionName() +"();\"";
        resultHTML.append("\r");
        resultHTML.append("<input " + type + value + styleClass + readonly + onClick + "/>\r");

        //Render hidden - 'Key holder'
        resultHTML.append("\r");
        type = " type=\"hidden\" ";
        name = " name=\""+getKeyHtmlHandlerId()+"\"";
        String keyId = " id=\""+ getKeyHtmlHandlerId()+"\"";
        value = " value=\""+((browseBox.getKey() == null) ? "" : browseBox.getKey()) +"\" ";
        resultHTML.append("\r");
        resultHTML.append("<input " + keyId + name + type + value + "/>\r");

        renderSupportScript(resultHTML);

        TagUtils.getInstance().write(pageContext,resultHTML.toString());
        return SKIP_BODY;
    }

    protected String renderOnClickScript(BasicControl ctrl) throws WebException {
        StringBuffer resultHTML = new StringBuffer();
        BrowseBox browseBox = (BrowseBox)ctrl;

        //Add script entry from onClick attr or default for button
        if(browseBox.isReadonly()){
            resultHTML.append("     //do nothing, because browseBox is disabled. \r");
        }
        else{
            //script to evaluate popup window left and top attributes
            resultHTML.append("\tvar width = ").append(getWinFeatureWidth()).append(";\r").
                       append("\tvar height = ").append(getWinFeatureHeight()).append(";\r").
                       append("\tvar left = (screen.width - width) / 2;\r").
                       append("\tvar top = (screen.height - height) / 2;\r");

            StringBuffer url = new StringBuffer(evaluateAction(browseBox));
            url.append("?").append(evaluateCommand(browseBox));
            //add callback parameters names
            url.append(KEY_ID_PARAM_NAME).append("=").append(getKeyHtmlHandlerId()).append("&").
                append(VALUE_ID_PARAM_NAME).append("=").append(getValueHtmlHandlerId()).append("&").
                append(BROWSE_PARAM_NAME).append("=true");//to identify browse request

            String name = "browse_window";
            StringBuffer features = new StringBuffer();
            features.append("width='+width+',").
                     append("height='+height+',").
                     append("top='+top+',").
                     append("left='+left+',").                    
                     append("channelmode=").append(getWinFeatureChannelmode()).append(",").
                     append("directories=").append(getWinFeatureDirectories()).append(",").
                     append("location=").append(getWinFeatureLocation()).append(",").
                     append("menubar=").append(getWinFeatureMenubar()).append(",").
                     append("resizable=").append(getWinFeatureResizable()).append(",").
                     append("scrollbars=").append(getWinFeatureScrollbars()).append(",").
                     append("status=").append(getWinFeatureStatus()).append(",").
                     append("titlebar=").append(getWinFeatureTitlebar()).append(",").
                     append("toolbar=").append(getWinFeatureToolbar());


            String replace = "false";
            resultHTML.append("\t").
                       append(getOpenedWindowsRefName()).append("=").
                       append("window.open(").append("'").append(url).append("', ").
                                              append("'").append(name).append("', ").
                                              append("'").append(features).append("', ").
                                              append("'").append(replace).append("');\r");
        }
        return resultHTML.toString();
    }

    /** This method requires global.js function registerSingleFunction(...) */
    private void renderSupportScript(StringBuffer resultHTML){
        //prepare funciton to unload window opener
        resultHTML.append("<script type=\"text/javascript\">").
           append("<!--\r").
           append("     \r").
           append("     function ").append(getResetFunctionName()).append("()").append("{\r").
           append("         var keyHandler = document.getElementById(\""+ getKeyHtmlHandlerId() +"\");\r").
           append("         var valueHandler = document.getElementById(\""+ getValueHtmlHandlerId() +"\");\r").
           append("         if(keyHandler != null) keyHandler.value = '';\r").
           append("         if(valueHandler != null) valueHandler.value = '';\r").
           append("     }\r").
           append("     \r").
           append("     function ").append(getUnloadBrowseFunctionName()).append("()").append("{\r").
           append("         if (window."+getOpenedWindowsRefName()+" && window."+getOpenedWindowsRefName()+".open && !window."+getOpenedWindowsRefName()+".closed)\n").
           append("             window."+getOpenedWindowsRefName()+".opener = null;\r").
           append("     }\r").
           append("\r").
           append("registerSingleFunction('window.unload', function() {alert('prepare to unload win opener')});\r").
           append("registerSingleFunction('window.unload', "+getUnloadBrowseFunctionName()+");\r").
           append("-->\r").
           append("</script>\r");     
    }

    private String evaluateAction(BrowseBox browseBox) throws WebException{
        ModuleConfig mc = ApplicationEnvironment.getModuleConfig();
        ActionConfig actionConfig = mc.findActionConfig(browseBox.getAction());
        if(actionConfig == null)
            throw new WebException("err.browsebox_tag.unregistered_action", new String[]{browseBox.getAction()});
        return ApplicationEnvironment.getAppDeploymentName() + actionConfig.getPath() + ApplicationEnvironment.getActionExtension();
    }

    private String evaluateCommand(BrowseBox browseBox){
        return (browseBox.getCommand() != null) ? "command="+browseBox.getCommand() + "&" : ""; 
    }

    private String getKeyHtmlHandlerId(){
        return getProperty()+"."+BrowseBox.KEY_SUFFIX;
    }

    private String getValueHtmlHandlerId(){
        return getProperty()+"."+BrowseBox.VALUE_SUFFIX;
    }

    private String getOpenedWindowsRefName(){
        return getSeparatorReplacedName(getProperty()) + "_openedBrowseWindow";        
    }

    private String getResetFunctionName(){
        return getSeparatorReplacedName(getProperty()) + "_reset";        
    }

    private String getUnloadBrowseFunctionName(){
        return getSeparatorReplacedName(getProperty()) + "_unload_browse";
    }

    protected BrowseBox getBrowseBox() throws JspException {
        Object object = getPropertyObject();
        return (object != null && object instanceof BrowseBox) ?
                (BrowseBox) object : null;
    }    
}
