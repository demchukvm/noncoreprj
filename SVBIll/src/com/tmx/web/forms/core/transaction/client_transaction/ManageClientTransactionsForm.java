package com.tmx.web.forms.core.transaction.client_transaction;

import com.tmx.as.entities.bill.operator.Operator;
import com.tmx.as.entities.bill.seller.Seller;
import com.tmx.as.entities.bill.transaction.TransactionType;
import com.tmx.as.entities.bill.terminal.EquipmentType;
import com.tmx.as.base.criterion.CriterionWrapper;
import com.tmx.as.base.criterion.Restrictions;

import com.tmx.beng.base.StatusDictionary;
import com.tmx.util.i18n.DictionaryWrapper;

import com.tmx.web.base.BasicActionForm;
import com.tmx.web.controls.*;
import com.tmx.web.controls.table.Filter;
import com.tmx.web.controls.table.Parameter;
import com.tmx.web.controls.table.ControlManagerTable;

import java.util.Calendar;
import java.util.GregorianCalendar;

public class ManageClientTransactionsForm extends BasicActionForm {

    private ControlManagerTable clientTransactionsTable;
    private DictionaryWrapper transactionStatusDictionary;

    private EditBox editBoxAllTransaction;
    private EditBox editBoxSuccessTransaction;
    private EditBox editBoxErrorTransaction;

    private EditBox editBoxAllTransAmount;
    private EditBox editBoxSuccessTransAmount;
    private EditBox editBoxErrorTransAmount;


    private EditBox editBoxAccountNumber;
    private EditBox editBoxVoucherCode;
    private EditBox editBoxTerminalSerialNumber;


    private ScrollBox scrollBoxTransTypes,
                        scrollBoxStatuses,
                        scrollBoxTerminalTypes;

    //private CheckBox chekBoxErrorTransactions = new CheckBox();
    private boolean chekBoxErrorTransactions;
    private boolean _chekBoxErrorTransactions;

    private CheckBox chekBoxIsUsingFullString = new CheckBox();

   // private String check;

    protected void init() {
        editBoxTerminalSerialNumber = new EditBox();

        editBoxAllTransaction = new EditBox();
        editBoxAllTransaction.setReadonly(true);

        editBoxSuccessTransaction = new EditBox();
        editBoxSuccessTransaction.setReadonly(true);

        editBoxAllTransAmount = new EditBox();
        editBoxAllTransAmount.setReadonly(true);

        editBoxSuccessTransAmount = new EditBox();
        editBoxSuccessTransAmount.setReadonly(true);

        editBoxErrorTransaction = new EditBox();
        editBoxErrorTransaction.setReadonly(true);

        editBoxErrorTransAmount = new EditBox();
        editBoxErrorTransAmount.setReadonly(true);

        editBoxAccountNumber = new EditBox();
        editBoxVoucherCode = new EditBox();

        clientTransactionsTable = new ControlManagerTable("query(query_type=retrieve_all," +
                "entity_class=com.tmx.as.entities.bill.transaction.ClientTransaction," +
                "table_attr=operator," +
                "table_attr=operatorBalance," +
                "table_attr=seller," +
                "table_attr=sellerBalance," +
                "table_attr=terminal," +
                "table_attr=voucher," +
                "table_attr=type," +
                "order(name=transactionTime,type=desc)," +
                "page_size=15" +
                ")");
        clientTransactionsTable.setRefreshActionName("/core/transactions/client_transaction/manageClientTransactions");
        clientTransactionsTable.setRefreshCommandName("refreshTable");

        scrollBoxStatuses = new ScrollBox<StatusDictionary.StatusObject>() {
            protected String getKey(StatusDictionary.StatusObject dataListElement) {
                return dataListElement.getKey().toString();
            }

            protected String getValue(StatusDictionary.StatusObject dataListElement) {
                return dataListElement.getValue();
            }
        };
        scrollBoxStatuses.setNullable(true);

        scrollBoxTerminalTypes = new ScrollBox<EquipmentType>() {

            protected String getKey(EquipmentType dataListElement) {
                return dataListElement.getEquipmentTypeId().toString();
            }

            protected String getValue(EquipmentType dataListElement) {
                return dataListElement.getName();
            }
        };
        scrollBoxTerminalTypes.setNullable(true);


        Filter amountFilterFrom = new Filter();
        amountFilterFrom.addParameter("from_amount", new Parameter(new EditBox()));
        clientTransactionsTable.getFilterSet().addFilter("by_client_amount_from", amountFilterFrom);

        Filter amountFilterTo = new Filter();
        amountFilterTo.addParameter("to_amount", new Parameter(new EditBox()));
        clientTransactionsTable.getFilterSet().addFilter("by_client_amount_to", amountFilterTo);

        ScrollBox scrollBoxOperators = new ScrollBox() {
            protected String getKey(Object dataListElement) {
                return ((Operator) dataListElement).getOperatorId().toString();
            }

            protected String getValue(Object dataListElement) {
                return ((Operator) dataListElement).getName();
            }
        };
        scrollBoxOperators.setNullable(true);
        Filter operatorFilter = new Filter();
        operatorFilter.addParameter("operator", new Parameter(scrollBoxOperators));
        clientTransactionsTable.getFilterSet().addFilter("by_operator", operatorFilter);

        scrollBoxTransTypes = new ScrollBox() {
            protected String getKey(Object dataListElement) {
                return ((TransactionType) dataListElement).getId().toString();
            }

            protected String getValue(Object dataListElement) {
                return ((TransactionType) dataListElement).getName();
            }
        };
        scrollBoxTransTypes.setNullable(true);

        Filter transTypeFilter = new Filter();
        transTypeFilter.addParameter("type", new Parameter(scrollBoxTransTypes));
        clientTransactionsTable.getFilterSet().addFilter("by_transaction_type", transTypeFilter);

        //chekBoxErrorTransactions = new CheckBox();
        _chekBoxErrorTransactions = chekBoxErrorTransactions;

        //if (chekBoxErrorTransactions.isSelected()) {
        if (chekBoxErrorTransactions == true) {
            Filter errorTransaction = new Filter("error_transactions");
            clientTransactionsTable.getFilterSet().addFilter(errorTransaction);
        }

        CriterionFactory criterion = new CriterionFactory() {

            public CriterionWrapper createCriterion() {
                //if (chekBoxErrorTransactions.isSelected()) {
                if (chekBoxErrorTransactions == true) {
                    return Restrictions.not(Restrictions.eq("status", "1"));
                } else {
                    return null;
                }
            }
        };
        clientTransactionsTable.setCriterionFactory(criterion);

        chekBoxErrorTransactions = false;

        //selectFrom filter              
        Filter fromDateFilter = new Filter();
        TimeSelector timeFromSelector = new TimeSelector();

        GregorianCalendar prototype = new GregorianCalendar();
        GregorianCalendar calendar = new GregorianCalendar(
                                        prototype.get(Calendar.YEAR),
                                        prototype.get(Calendar.MONTH),
                                        prototype.get(Calendar.DATE),
                                        0,
                                        0
        );
        timeFromSelector.setTime(calendar.getTime());
        fromDateFilter.addParameter("date", new Parameter(timeFromSelector));
        clientTransactionsTable.getFilterSet().addFilter("by_FromDate", fromDateFilter);

        //selectTo filter
        Filter toDateFilter = new Filter();
        TimeSelector timeToSelector = new TimeSelector();
        toDateFilter.addParameter("date", new Parameter(timeToSelector));
        clientTransactionsTable.getFilterSet().addFilter("by_ToDate", toDateFilter);

        //for billing transaction number
        Filter billTransactionFilter = new Filter();
        billTransactionFilter.addParameter("billnum", new Parameter(new EditBox()));
        clientTransactionsTable.getFilterSet().addFilter("by_billnum_like", billTransactionFilter);

        //for client transaction number
        Filter cliTransactionFilter = new Filter();
        cliTransactionFilter.addParameter("clinum", new Parameter(new EditBox()));
        clientTransactionsTable.getFilterSet().addFilter("by_clinum_like", cliTransactionFilter);

        clientTransactionsTable.addParameterToFilter("by_seller", "seller", new ScrollBox(false) {
            protected String getKey(Object dataListElement) {
                return ((Seller) dataListElement).getSellerId().toString();
            }

            protected String getValue(Object dataListElement) {
                return ((Seller) dataListElement).getName();
            }
        });

//        CriterionFactory criterionFactory = new CriterionFactory() {
//
//            public CriterionWrapper createCriterion() {
//                CriterionWrapper resultCriterion = Restrictions.isNotNull("tansactionId");//redunant
//
//                if (editBoxAccountNumber.getValue() != null && !editBoxAccountNumber.getValue().equals("")) {
//                    resultCriterion = Restrictions.and(resultCriterion,
//                            Restrictions.ilike("accountNumber", "%" + editBoxAccountNumber.getValue() + "%"));
//                }
//
//                if (editBoxTerminalSerialNumber.getValue() != null && !editBoxTerminalSerialNumber.getValue().equals("")) {
//                    resultCriterion = Restrictions.and(resultCriterion,
//                            Restrictions.ilike("terminal.serialNumber", "%" + editBoxTerminalSerialNumber.getValue() + "%"));
//                }
//
//                if (editBoxVoucherCode.getValue() != null && !editBoxVoucherCode.getValue().equals("")) {
//                    resultCriterion = Restrictions.and(resultCriterion,
//                            Restrictions.ilike("voucher.code", "%" + editBoxVoucherCode.getValue() + "%"));
//                }
//
////                if (clientTransactionsTable.getCriterionFactory() != null) {
////                    resultCriterion = Restrictions.and(resultCriterion, clientTransactionsTable.getCriterionFactory().createCriterion());
////                }
//
//                return resultCriterion;
//            }
//        };
//        clientTransactionsTable.setCriterionFactory(criterionFactory);

        transactionStatusDictionary = new DictionaryWrapper(getLocale(),
                StatusDictionary.class, StatusDictionary.STATUS_MSGS_BUNDLE);

        chekBoxIsUsingFullString = new CheckBox();
    }

    //--------Properties

    public ControlManagerTable getClientTransactionsTable() {
        return clientTransactionsTable;
    }

    public void setClientTransactionsTable(ControlManagerTable clientTransactionsTable) {
        this.clientTransactionsTable = clientTransactionsTable;
    }


    public DictionaryWrapper getTransactionStatusDictionary() {
        return transactionStatusDictionary;
    }

    public void setTransactionStatusDictionary(DictionaryWrapper transactionStatusDictionary) {
        this.transactionStatusDictionary = transactionStatusDictionary;
    }

    public EditBox getEditBoxAllTransaction() {
        return editBoxAllTransaction;
    }

    public void setEditBoxAllTransaction(EditBox editBoxAllTransaction) {
        this.editBoxAllTransaction = editBoxAllTransaction;
    }

    public EditBox getEditBoxSuccessTransaction() {
        return editBoxSuccessTransaction;
    }

    public void setEditBoxSuccessTransaction(EditBox editBoxSuccessTransaction) {
        this.editBoxSuccessTransaction = editBoxSuccessTransaction;
    }

    public EditBox getEditBoxAllTransAmount() {
        return editBoxAllTransAmount;
    }

    public void setEditBoxAllTransAmount(EditBox editBoxAllTransAmount) {
        this.editBoxAllTransAmount = editBoxAllTransAmount;
    }

    public EditBox getEditBoxSuccessTransAmount() {
        return editBoxSuccessTransAmount;
    }

    public void setEditBoxSuccessTransAmount(EditBox editBoxSuccessTransAmount) {
        this.editBoxSuccessTransAmount = editBoxSuccessTransAmount;
    }

    public EditBox getEditBoxAccountNumber() {
        return editBoxAccountNumber;
    }

    public void setEditBoxAccountNumber(EditBox editBoxAccountNumber) {
        this.editBoxAccountNumber = editBoxAccountNumber;
    }

    public EditBox getEditBoxTerminalSerialNumber() {
        return editBoxTerminalSerialNumber;
    }

    public void setEditBoxTerminalSerialNumber(EditBox editBoxTerminalSerialNumber) {
        this.editBoxTerminalSerialNumber = editBoxTerminalSerialNumber;
    }

    public EditBox getEditBoxVoucherCode() {
        return editBoxVoucherCode;
    }

    public void setEditBoxVoucherCode(EditBox editBoxVoucherCode) {
        this.editBoxVoucherCode = editBoxVoucherCode;
    }

    public ScrollBox getScrollBoxTransTypes() {
        return scrollBoxTransTypes;
    }

    public void setScrollBoxTransTypes(ScrollBox scrollBoxTransTypes) {
        this.scrollBoxTransTypes = scrollBoxTransTypes;
    }

    public ScrollBox getScrollBoxStatuses() {
        return scrollBoxStatuses;
    }

    public void setScrollBoxStatuses(ScrollBox scrollBoxStatuses) {
        this.scrollBoxStatuses = scrollBoxStatuses;
    }

    public EditBox getEditBoxErrorTransaction() {
        return editBoxErrorTransaction;
    }

    public void setEditBoxErrorTransaction(EditBox editBoxErrorTransaction) {
        this.editBoxErrorTransaction = editBoxErrorTransaction;
    }

    public EditBox getEditBoxErrorTransAmount() {
        return editBoxErrorTransAmount;
    }

    public void setEditBoxErrorTransAmount(EditBox editBoxErrorTransAmount) {
        this.editBoxErrorTransAmount = editBoxErrorTransAmount;
    }

/*    public CheckBox getChekBoxErrorTransactions() {
        return chekBoxErrorTransactions;
    }

    public void setChekBoxErrorTransactions(CheckBox chekBoxErrorTransactions) {
        this.chekBoxErrorTransactions = chekBoxErrorTransactions;
    }*/

    public ScrollBox getScrollBoxTerminalTypes() {
        return scrollBoxTerminalTypes;
    }

    public void setScrollBoxTerminalTypes(ScrollBox scrollBoxTerminalTypes) {
        this.scrollBoxTerminalTypes = scrollBoxTerminalTypes;
    }

    public CheckBox getChekBoxIsUsingFullString() {
        return chekBoxIsUsingFullString;
    }

    public void setChekBoxIsUsingFullString(CheckBox chekBoxIsUsingFullString) {
        this.chekBoxIsUsingFullString = chekBoxIsUsingFullString;
    }

    /*public String getCheck() {
        return check;
    }

    public void setCheck(String check) {
        this.check = check;
    }*/

    public boolean isChekBoxErrorTransactions() {
        return chekBoxErrorTransactions;
    }

    public void setChekBoxErrorTransactions(boolean chekBoxErrorTransactions) {
        this.chekBoxErrorTransactions = chekBoxErrorTransactions;
    }

    public boolean is_chekBoxErrorTransactions() {
        return _chekBoxErrorTransactions;
    }

    public void set_chekBoxErrorTransactions(boolean _chekBoxErrorTransactions) {
        this._chekBoxErrorTransactions = _chekBoxErrorTransactions;
    }
}
