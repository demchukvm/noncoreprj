package com.tmx.web.forms.core.transaction.seller_balance_transaction;

import com.tmx.as.entities.bill.seller.Seller;
import com.tmx.as.entities.bill.transaction.SellerBalanceTransaction;
import com.tmx.as.entities.bill.transaction.TransactionSupport;
import com.tmx.as.exceptions.DatabaseException;
import com.tmx.as.blogic.TransactionManager;

import com.tmx.web.controls.EditBox;
import com.tmx.web.controls.TextArea;
import com.tmx.web.controls.Button;
import com.tmx.web.forms.core.transaction.AbstractInfoTransactionForm;

public class InfoSellerBalanceTransactionForm extends AbstractInfoTransactionForm {
    private EditBox editBoxSellerName;
    private EditBox editBoxSellerBalanceName;

    private TextArea textAreaSellerBalancePaymentReason;
    private TextArea textAreaSellerBalanceDescription;

    private Button onSaveButton;


    protected void init() {
        //initialze and register controls
        super.init();

        editBoxSellerName = new EditBox();
        editBoxSellerName.setReadonly(true);

        editBoxSellerBalanceName = new EditBox();
        editBoxSellerBalanceName.setReadonly(true);

        textAreaSellerBalancePaymentReason = new TextArea();
        //textAreaSellerBalancePaymentReason.setReadonly(true);

        textAreaSellerBalanceDescription = new TextArea();
//        textAreaSellerBalanceDescription.setReadonly(true);

        editBoxExecutedUser.setReadonly(false);

//        onCancelButton.setAction("/core/transactions/seller_transaction/manageSellerTransactions");
        onCancelButton.setAction("/core/transactions/seller_transaction/infoSellerTransaction");
        onCancelButton.setCommand("preloadForCancel");

        onSaveButton = new Button();
        onSaveButton.setAction("/core/transactions/seller_transaction/infoSellerTransaction");
        onSaveButton.setLabelKey("ctrl.button.label.save");
        onSaveButton.setCommand("preloadForSave");
    }


    

    public void bindData(SellerBalanceTransaction sellerBalanceTransaction) throws DatabaseException {
        super.bindData(sellerBalanceTransaction);

        //for seller
        Seller seller = sellerBalanceTransaction.getSeller();
        if(seller != null){
            editBoxSellerName.setValue(seller.getName());
            if(seller.getPrimaryBalance() != null) {
                editBoxSellerBalanceName.setValue(seller.getPrimaryBalance().getName());
            }
//            System.out.println(new TransactionManager().getSellerBalanceTransaction(sellerBalanceTransaction.getTansactionId()));
            if (sellerBalanceTransaction.getTransactionSupport() != null) { //it`s possible that TS_TransactionSupportId == null in DB
                textAreaSellerBalancePaymentReason.setValue(sellerBalanceTransaction.getTransactionSupport().getPaymentReason());
                textAreaSellerBalanceDescription.setValue(sellerBalanceTransaction.getTransactionSupport().getDescription());
                editBoxExecutedUser.setValue(sellerBalanceTransaction.getTransactionSupport().getUserActor().getLogin());
            }
        }
    }

    public TransactionSupport populateTransactionSupport() throws DatabaseException {
      TransactionManager transactionManager = new TransactionManager();

        SellerBalanceTransaction sellerBalanceTransaction =
                transactionManager.getSellerBalanceTransaction(
                        new Long(getEditBoxTransactionId().getValue()));
        TransactionSupport transactionSupport = sellerBalanceTransaction.getTransactionSupport();
        transactionSupport.setPaymentReason(textAreaSellerBalancePaymentReason.getValue());
        transactionSupport.setDescription(textAreaSellerBalanceDescription.getValue());
//        transactionSupport.setUserActor(editBoxExecutedUser.getValue());
        return transactionSupport;
    }

    public EditBox getEditBoxSellerName() {
        return editBoxSellerName;
    }

    public void setEditBoxSellerName(EditBox editBoxSellerName) {
        this.editBoxSellerName = editBoxSellerName;
    }

    public EditBox getEditBoxSellerBalanceName() {
        return editBoxSellerBalanceName;
    }

    public void setEditBoxSellerBalanceName(EditBox editBoxSellerBalanceName) {
        this.editBoxSellerBalanceName = editBoxSellerBalanceName;
    }

    public TextArea getTextAreaSellerBalancePaymentReason() {
        return textAreaSellerBalancePaymentReason;
    }

    public void setTextAreaSellerBalancePaymentReason(TextArea textAreaSellerBalancePaymentReason) {
        this.textAreaSellerBalancePaymentReason = textAreaSellerBalancePaymentReason;
    }

    public TextArea getTextAreaSellerBalanceDescription() {
        return textAreaSellerBalanceDescription;
    }

    public void setTextAreaSellerBalanceDescription(TextArea textAreaSellerBalanceDescription) {
        this.textAreaSellerBalanceDescription = textAreaSellerBalanceDescription;
    }

    public Button getOnSaveButton() {
        return onSaveButton;
    }

    public void setOnSaveButton(Button onSaveButton) {
        this.onSaveButton = onSaveButton;
    }
}
