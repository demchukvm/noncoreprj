<%@ page language="java" pageEncoding="UTF-8" contentType="text/html; charset=utf-8" isELIgnored="false" %>
<!--errorPage="/core/jsp/message/jsperror.jsp"-->
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean" %>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c" %>
<%@ taglib uri="http://tmx.com/taglib/tmx-logic" prefix="tmx_logic" %>
<%@ taglib uri="http://tmx.com/taglib/tmx-controls" prefix="tmx_ctrl" %>


<tiles:useAttribute id="leftLayoutBlocks" name="leftLayoutBlocksList" classname="java.util.List" ignore="true"/>
<tiles:useAttribute id="leftPageBlocks" name="leftPageBlocksList" classname="java.util.List" ignore="true"/>
<tiles:useAttribute id="rightLayoutBlocks" name="rightLayoutBlocksList" classname="java.util.List" ignore="true"/>
<tiles:useAttribute id="rightPageBlocks" name="rightPageBlocksList" classname="java.util.List" ignore="true"/>

<table height="100%" width="100%" border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td colspan="2" align="center" nowrap bgcolor="#FFFFCC">
            <tmx_ctrl:menu menuName="main" styleName="html_mainmenu"/>
        </td>
    </tr>

    <%--<tiles:insert page="${requestScope['tile.loggedInLayout.toolBarPlaceHolder']}" ignore="true"/>--%>
    <tr>
		<!--<td style="height: 700px; width: 2px;">

			&nbsp;
		</td>-->
		
		
		
		
        <!--<td valign="top" style="width:0%;border: red 1px solid;>-->
		<td  id="leftColumn" class="leftColumn" valign="top" style="width:120px; ">
            <c:if test="${requestScope['tile.loggedInLayout.leftBlocks'] != null}">
                <tmx_logic:iterator items="${requestScope['tile.loggedInLayout.leftBlocks']}" var="pageBlock">
                    <tiles:insert page="${pageBlock}"/>
                </tmx_logic:iterator>
            </c:if>
        </td>
		
		
		
		
		
        <td width='100%' id="rightColumn" style="padding-right: 5px; padding-left: 5px; vertical-align: top; text-align: left; border: 0px solid green;">
            <c:if test="${requestScope['tile.loggedInLayout.content'] != null}">
                <!-- Form Title -->
                <c:if test="${requestScope['tile.abstractLayout.titleKey'] != null}">
                    <table width='100%' cellpadding='0' cellspacing='0' border='0' class='moduleTitle'>
                        <tr>
                            <td valign='top'>
                                <h2><bean:message bundle="core.labels" key="${requestScope['tile.abstractLayout.titleKey']}" /></h2>
                            </td>
                        <tr>
                    </table>
                </c:if>
                <!-- Form Body -->
                <tiles:insert page="${requestScope['tile.loggedInLayout.content']}" ignore="true"/>
            </c:if>
        </td>
		
		
		<script type="text/javascript">
			var element = document.getElementById('rightColumn');
			element.style.width = screen.width - 102 + 'px';
			var element2 = document.getElementById('leftColumn');
			element2.style.height = screen.height - 350 + 'px';
			
			
			
			
			//alert(document.documentElement.clientHeight);
			//var element3 = document.getElementById('cell');
			//alert(element3.height);
		</script>
		
    </tr>
	
	
	
    <tr>
    	<td colspan="2" height="5">
			<%--<img src="<tmx_ctrl:rewriteUrl href="/core/images/common/blank.gif"/>" width="1" height="5" border="0">--%>
		</td>
    </tr>
	<tr>
    	
		<td colspan="2"align='center' class='copyRight' style="border-top: #ccc 1px solid;margin-top:5px;">
            &copy; 2003-2007 <a href="http://www.tmx.com.ua" style="text-decoration:none;color:#006600;font-weight:900;">Technomatix Inc.</a> All Rights Reserved<br>
			&copy; 2008-pres. <a href="http://www.svcard.com.ua" style="text-decoration:none;color:#006600;font-weight:900;">SVCard.</a> All Rights Reserved
        </td>
		</td>
    </tr>
		
    
	
</table>