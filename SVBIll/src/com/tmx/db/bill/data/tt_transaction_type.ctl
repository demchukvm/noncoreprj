LOAD DATA
CHARACTERSET CL8MSWIN1251
INFILE *
INSERT INTO TABLE svbill.tt_transaction_type
Fields terminated by ";" Optionally enclosed by '"'
(
  TT_CODE,
  TT_NAME,
  TT_DESCRIPTION,
  TT_VALID NULLIF (TT_VALID="NULL")
)
BEGINDATA
"1";"CREDIT";"Decrease balance";1
"2";"DEBET";"Increase balance";1