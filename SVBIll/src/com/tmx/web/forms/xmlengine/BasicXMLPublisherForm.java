package com.tmx.web.forms.xmlengine;

import com.tmx.web.base.BasicActionForm;

public class BasicXMLPublisherForm extends BasicActionForm {
    private String xmlDocSerialized = null;

    public String getXmlDocSerialized() {
        return xmlDocSerialized;
    }

    public void setXmlDocSerialized(String xmlDocSerialized) {
        this.xmlDocSerialized = xmlDocSerialized;
    }

}
