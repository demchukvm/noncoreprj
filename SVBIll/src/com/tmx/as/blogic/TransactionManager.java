package com.tmx.as.blogic;

import com.tmx.as.base.*;
import com.tmx.as.base.criterion.CriterionWrapper;
import com.tmx.as.base.criterion.Restrictions;
import com.tmx.as.entities.bill.operator.Operator;
import com.tmx.as.entities.bill.transaction.*;
import com.tmx.as.entities.bill.seller.Seller;
import com.tmx.as.entities.bill.balance.SellerBalance;
import com.tmx.as.exceptions.DatabaseException;
import com.tmx.web.forms.core.balance.seller_balance.InfoSellerBalanceForm;

import java.util.*;


public class TransactionManager extends EntityManager {

    private ProjectionWrapper projectionSumAmount = new ProjectionWrapper("amount", "amount", ProjectionWrapper.SUM);
    private ProjectionWrapper[] projectionsSumAmount = new ProjectionWrapper[]{projectionSumAmount};

    private ProjectionWrapper projectionSumFee = new ProjectionWrapper("fee", "feeSum", ProjectionWrapper.SUM);
//    private ProjectionWrapper projectionCountFee = new ProjectionWrapper("fee", "feeCount", ProjectionWrapper.COUNT);
    private ProjectionWrapper[] projectionsFee = new ProjectionWrapper[]{projectionSumFee};

    public static String SELLER_TRANSACTIONS = "sellerTransactions";
    public static String OPERATOR_TRANSACTIONS = "operatorTransactions";
    public static String TERMINAL_TRANSACTIONS = "terminalTransactions";

    public List getAllOperators() throws DatabaseException {
        return RETRIEVE_ALL(Operator.class, null);
    }

    public HashMap getProblemTransactions() throws DatabaseException {
        HashMap<Long, List> operatorTxs = new HashMap<Long, List>();

        List<Long> operatorIds = new ArrayList<Long>();
        operatorIds.add(3l);//KyivstarPoP
        operatorIds.add(7590328l);//KyivstarBonusNoComission
        operatorIds.add(5l);//MTS

        GregorianCalendar dateTo = new GregorianCalendar();
        dateTo.set(Calendar.HOUR, 0);
        dateTo.set(Calendar.MINUTE, 0);
        GregorianCalendar dateFrom = new GregorianCalendar();
        dateFrom.add(Calendar.DAY_OF_MONTH, -1);
        dateFrom.set(Calendar.HOUR, 0);
        dateFrom.set(Calendar.MINUTE, 0);

/*Class entityClass, FilterWrapper[] filters, CriterionWrapper[] criterions, OrderWrapper[] orders,
String[] tableAttrs, ProjectionWrapper[] projections, int pageNumber, int pageSize*/
        for (Long operatorId : operatorIds) {
            CriterionWrapper criterions[] = new CriterionWrapper[] {
                Restrictions.eq("operator.operatorId", operatorId),
                Restrictions.between("transactionTime", dateFrom.getTime(), dateTo.getTime()),
                Restrictions.not(Restrictions.eq("status", "0"))};
            List txs = RETRIEVE_ALL(ClientTransaction.class, null, criterions,
                                    null, null, null, -1, -1);
            operatorTxs.put(operatorId,  txs);

        }
        return operatorTxs;
    }

//    public TransactionSupport getTransactionSupport(Long userId) throws DatabaseException {
//        FilterWrapper transactionSupportByUser = new FilterWrapper("by_user_actor_id");
//        transactionSupportByUser.setParameter("user_actor_id", userId);
//        return (TransactionSupport) RETRIEVE(TransactionSupport.class, transactionSupportByUser);
//    }

    //client
    public ClientTransaction getClientTransaction(Long clientTransactionId) throws DatabaseException {
        String[] attributes = new String[]{"operator.primaryBalance", "seller.primaryBalance",
                "terminal.primaryBalance", "type", "error", "operationType"};
        return (ClientTransaction) RETRIEVE( ClientTransaction.class, clientTransactionId, attributes );
    }

    public ClientTransaction getClientTransaction(String id) throws DatabaseException {
        Long clientTransactionId = Long.valueOf( id );
        return getClientTransaction( clientTransactionId );
    }

    public List<ClientTransaction> getClientTransactionByGUID(String number) throws DatabaseException {
        CriterionWrapper[] wrappers = new CriterionWrapper[] {
                    Restrictions.eq("transactionGUID", number)};

        String[] attributes = new String[]{"operator.primaryBalance", "seller.primaryBalance",
                                                                        "terminal.primaryBalance",
                                                                        "type", "error",
                                                                        "operationType"};
        List<ClientTransaction> clientTransactions = RETRIEVE_ALL(ClientTransaction.class, wrappers, attributes);
        if (clientTransactions.size() == 0) {
            throw new DatabaseException("Client transaction with this number is absent in DB");
        }
        return  clientTransactions;
    }
    public List<SellerBalanceTransaction> getSellerTransactionByGUID(String number) throws DatabaseException {
        CriterionWrapper[] wrappers = new CriterionWrapper[] {
                    Restrictions.eq("transactionGUID", number)};

        List<SellerBalanceTransaction> sellerBalanceTransactions = RETRIEVE_ALL(SellerBalanceTransaction.class, wrappers, null);
        if (sellerBalanceTransactions.size() == 0) {
            throw new DatabaseException("Seller transaction with this number is absent in DB");
        }
        return  sellerBalanceTransactions;
    }
    public List<TerminalBalanceTransaction> getTerminalTransactionByGUID(String number) throws DatabaseException {
        CriterionWrapper[] wrappers = new CriterionWrapper[] {
                    Restrictions.eq("transactionGUID", number)};

        List<TerminalBalanceTransaction> terminalBalanceTransactions = RETRIEVE_ALL(TerminalBalanceTransaction.class, wrappers, null);
        if (terminalBalanceTransactions.size() == 0) {
            throw new DatabaseException("Seller transaction with this number is absent in DB");
        }
        return  terminalBalanceTransactions;
    }
    public List<OperatorBalanceTransaction> getOperatorTransactionByGUID(String number) throws DatabaseException {
        CriterionWrapper[] wrappers = new CriterionWrapper[] {
                    Restrictions.eq("transactionGUID", number)};

        List<OperatorBalanceTransaction> operatorBalanceTransactions = RETRIEVE_ALL(OperatorBalanceTransaction.class, wrappers, null);
        if (operatorBalanceTransactions.size() == 0) {
            throw new DatabaseException("Seller transaction with this number is absent in DB");
        }
        return  operatorBalanceTransactions;
    }

    /**
     * Retrive all transactions (Client, Seller, Terminal, Operator) by billing GUID
     * @param number billing GUID
     * @return list of transactions
     *  */
    public List getAllTransactionsByGUID(String number) {
        List transactions = new ArrayList();
        List clientTransaction = null;
        try {
            clientTransaction = getClientTransactionByGUID(number);
        } catch (DatabaseException e) {
            logger.debug(e.getMessage());
        }
        List sellerTransaction = null;
        try {
            sellerTransaction = getSellerTransactionByGUID(number);
        } catch (DatabaseException e) {
            logger.debug(e.getMessage());
        }
        List terimalTransaction = null;
        try {
            terimalTransaction = getTerminalTransactionByGUID(number);
        } catch (DatabaseException e) {
            logger.debug(e.getMessage());
        }
        List operatorTransaction = null;
        try {
            operatorTransaction = getOperatorTransactionByGUID(number);
        } catch (DatabaseException e) {
            logger.debug(e.getMessage());
        }
        if (clientTransaction != null) {
            transactions.addAll(clientTransaction);
        }
        if (sellerTransaction != null) {
            transactions.addAll(sellerTransaction);
        }
        if (terimalTransaction != null) {
            transactions.addAll(terimalTransaction);
        }
        if (operatorTransaction != null) {
            transactions.addAll(operatorTransaction);
        }

        return  transactions;
    }

    public ClientTransaction getClientTransactionByClientNum(String clientTransactionNum) throws DatabaseException {
        CriterionWrapper[] wrappers = new CriterionWrapper[] {
                            Restrictions.eq("cliTransactionNum", clientTransactionNum)};
        String[] attributes = new String[]{"voucher"};
        List<ClientTransaction> clientTransactions = RETRIEVE_ALL( ClientTransaction.class, wrappers, attributes );

        if (clientTransactions.size() == 0) {
            throw new DatabaseException("Client transaction with this number is absent in DB");
        }
        if (clientTransactions.size() > 1) {
            logger.error("More than one client transaction with cliTransactionNum " + clientTransactionNum);
        }
        return  clientTransactions.get(0);
    }

    public Map getAllTransactionByGuid(String transactionGuid) throws DatabaseException {
        CriterionWrapper[] wrappers = new CriterionWrapper[] {
                            Restrictions.eq("transactionGUID", transactionGuid)};
        String[] sellerParams = new String[]{ "seller.primaryBalance", "sellerBalance"};
        List<SellerBalanceTransaction> stx =  RETRIEVE_ALL(SellerBalanceTransaction.class,
                                                wrappers, sellerParams);

        String[] operatorParams = new String[]{ "operator.primaryBalance", "operatorBalance"};
        List<OperatorBalanceTransaction> otx =  RETRIEVE_ALL(OperatorBalanceTransaction.class,
                                                    wrappers, operatorParams);

        String[] terminalParams = new String[]{ "terminal.primaryBalance", "terminalBalance"};
        List<TerminalBalanceTransaction> ttx =  RETRIEVE_ALL(TerminalBalanceTransaction.class,
                                                    wrappers, terminalParams);

        Map<String, List> transactions = new HashMap<String, List>();
        transactions.put(SELLER_TRANSACTIONS, stx);
        transactions.put(OPERATOR_TRANSACTIONS, otx);
        transactions.put(TERMINAL_TRANSACTIONS, ttx);

        return transactions;
    }

    //operator
    public OperatorBalanceTransaction getOperatorBalanceTransaction(Long operatorTransactionId) throws DatabaseException {
        String[] attributes = new String[]{"operator.primaryBalance", "operatorBalance",
                                                                      "type", "error",
                                                                      "transactionSupport",
                                                                      "operationType",
                                                                      "transactionSupport.userActor"};
        return (OperatorBalanceTransaction) RETRIEVE(OperatorBalanceTransaction.class, operatorTransactionId, attributes);
    }

    public OperatorBalanceTransaction getOperatorBalanceTransaction(String id) throws DatabaseException {
        Long operatorTransactionId = Long.valueOf(id);
        return getOperatorBalanceTransaction(operatorTransactionId);
    }

    //seller
    public SellerBalanceTransaction getSellerBalanceTransaction(Long sellerTransactionId) throws DatabaseException {
        String[] attributes = new String[]{"seller.primaryBalance", "sellerBalance",
                                                                    "type", "error",
                                                                    "transactionSupport",
                                                                    "operationType",
                                                                    "transactionSupport.userActor",
                                                                    "clientTransaction.operator"};
        return (SellerBalanceTransaction) RETRIEVE(SellerBalanceTransaction.class, sellerTransactionId, attributes);
    }

    public SellerBalanceTransaction getSellerBalanceTransaction(String id) throws DatabaseException {
        Long sellerTransactionId = Long.valueOf(id);
        return getSellerBalanceTransaction(sellerTransactionId);
    }

    //terminal
    public TerminalBalanceTransaction getTerminalBalanceTransaction(Long terminalTransactionId) throws DatabaseException {
        String[] attributes = new String[]{"terminal.primaryBalance","terminalBalance",
                                                                    "type", "error",
                                                                    "operationType",
                                                                    "transactionSupport",
                                                                    "transactionSupport.userActor",
                                                                    "clientTransaction.operator"};
        return (TerminalBalanceTransaction) RETRIEVE(TerminalBalanceTransaction.class, terminalTransactionId, attributes);
    }

    public TerminalBalanceTransaction getTerminalBalanceTransaction(String id) throws DatabaseException {
        Long terminalTransactionId = Long.valueOf( id );
        return getTerminalBalanceTransaction(terminalTransactionId);
    }

    public List getAllTransactionType() throws DatabaseException {
        return RETRIEVE_ALL(TransactionType.class, null);
    }

    public CalculationResult calculateTransactions(FilterWrapper[] wrappers, String transClassName)
                                throws ClassNotFoundException, DatabaseException {
            return calculateTransactions(wrappers, transClassName, null);
    }
    // for table dependency
    public CalculationResult calculateTransactions(FilterWrapper[] wrappers, String transClassName, CriterionWrapper[] criterionWrappers) throws ClassNotFoundException, DatabaseException {
        Class transClass = Class.forName( transClassName );

        if( transClass.equals(ClientTransaction.class) || transClass.equals(SellerBalanceTransaction.class) || transClass.equals(OperatorBalanceTransaction.class) || transClass.equals(TerminalBalanceTransaction.class))
            return calculateTransactions(wrappers, transClass, criterionWrappers);

        throw new IllegalArgumentException("Class isn't transaction realizations");
    }

    private CalculationResult calculateTransactions(FilterWrapper[] wrappers, Class transClass, CriterionWrapper [] criterionWrapper) throws DatabaseException {
        if( wrappers == null)
            wrappers = new FilterWrapper[0];

        CalculationResult result = new CalculationResult();
        int allCount = RETRIEVE_ALL_COUNT(transClass, wrappers, criterionWrapper);
        result.setTransactionCaunt( new Integer(allCount) );

        FilterWrapper wrapper = new FilterWrapper("by_client_status");
        wrapper.setParameter("status", "0");
        FilterWrapper[] successWrappers = new FilterWrapper[wrappers.length + 1];
        successWrappers[wrappers.length] = wrapper;
        System.arraycopy(wrappers, 0, successWrappers, 0, wrappers.length);

        int successCount = RETRIEVE_ALL_COUNT(transClass, successWrappers, criterionWrapper);
        result.setTransactionSuccessCount( new Integer(successCount) );

        result.setTransactionAmount(calculateAmount(wrappers, transClass, criterionWrapper));
        result.setTransactionSuccessAmount(calculateAmount(successWrappers, transClass, criterionWrapper));//success transaction

        //calculate fee for TX
        if (transClass.equals(SellerBalanceTransaction.class)) {
            ProjectionWrapper projectionFeeTx = new ProjectionWrapper("fee", "fee", ProjectionWrapper.SUM);
            ProjectionWrapper[] projectionFeeTxs = new ProjectionWrapper[]{projectionFeeTx};

            List fees = RETRIEVE_ALL(transClass, successWrappers, criterionWrapper, null, null, projectionFeeTxs, -1, -1);
            Double fee = (Double)fees.get(0);
            result.setFeeForTx(fee != null ? fee : 0);
        }
        return result;
    }

    private Double calculateAmount(FilterWrapper[] wrappers, Class transClass, CriterionWrapper [] criterionWrapper) throws DatabaseException {
        List<CriterionWrapper> debetCriterionWrappers = new ArrayList<CriterionWrapper>();
        CriterionWrapper debetCriterionWrapper   = Restrictions.eq("type.code", TransactionType.CODE_DEBET);
        debetCriterionWrappers.add(debetCriterionWrapper);

        List<CriterionWrapper> creditCriterionWrappers = new ArrayList<CriterionWrapper>();
        CriterionWrapper creditCriterionWrapper  = Restrictions.eq("type.code", TransactionType.CODE_CREDIT);
        creditCriterionWrappers.add(creditCriterionWrapper);

        //if another restrictions set
        if (criterionWrapper != null) {
            if(criterionWrapper.length > 0) {
                debetCriterionWrappers.addAll(Arrays.asList(criterionWrapper));
                creditCriterionWrappers.addAll(Arrays.asList(criterionWrapper));
            }
        }

        List resultListDebet = RETRIEVE_ALL(transClass, wrappers,
                debetCriterionWrappers.toArray(new CriterionWrapper[debetCriterionWrappers.size()]), null, null, projectionsSumAmount, -1, -1);
        List resultListCredit = RETRIEVE_ALL(transClass, wrappers,
                creditCriterionWrappers.toArray(new CriterionWrapper[creditCriterionWrappers.size()]), null, null, projectionsSumAmount, -1, -1);

        //        all transaction
        if(resultListDebet.size() != 1 && resultListCredit.size() != 1) {
            throw new IllegalStateException("Transaction list size isn't one");
        }
        Double debetAmount = (resultListDebet.get(0) != null) ? (Double) resultListDebet.get(0) : 0;
        Double creditAmount = (resultListCredit.get(0)!= null) ? (Double) resultListCredit.get(0) : 0;

        return debetAmount - creditAmount;
    }

    /*Get cash calculation for InfoSellerBalanceForm*/
    public CashCalculation getCashCalculation(InfoSellerBalanceForm form) throws DatabaseException {
        Long balanceId = form.getBalanceId();
        Long sellerId = new Long(form.getScrollBoxBalanceOwner().getSelectedKey());
        //date filtering
        GregorianCalendar dateFrom = new GregorianCalendar();
        dateFrom.setTime(form.getTimeSelectorTransFrom().getTime());

        GregorianCalendar dateTo = new GregorianCalendar();
        dateTo.setTime(form.getTimeSelectorTransTo().getTime());

        return getCashCalculation(balanceId, sellerId, dateFrom.getTime(), dateTo.getTime(), true, false);
    }
    /*Get cash calculation for POS terminal*/
    public CashCalculation getCashCalculation(Long balanceId,
                                              Long sellerId,
                                              Date dateFrom,
                                              Date dateTo) throws DatabaseException {
        return getCashCalculation(balanceId, sellerId, dateFrom, dateTo, false, true);
    }

    public CashCalculation getCashCalculation(Long balanceId,
                                              Long sellerId,
                                              Date dateFrom,
                                              Date dateTo,
                                              boolean isCalculateSpareCash,
                                              boolean isSumTxAmount) throws DatabaseException {
        List<Long> operationTypes = Arrays.asList(1l,2l);//only payments operations <on-line, buy vouchers>
        CashCalculation cashCalculation = new CashCalculation();
        SellerBalance sellerBalance = (SellerBalance)RETRIEVE(SellerBalance.class, balanceId, null);
        //Full profit = sum of fees for all txs, subsellers including
        List<Seller> sellers;
        sellers = (new SellerManager()).getSellersInSeller(sellerId, true);

        CriterionWrapper[] criterions = new CriterionWrapper[] {
                Restrictions.between("transactionTime", dateFrom, dateTo),
                Restrictions.in("seller", sellers),/*for seller & subsellers*/
                Restrictions.in("operationType.operationTypeId", operationTypes),
                Restrictions.eq("sellerBalance", sellerBalance),
                Restrictions.eq("status", "0")};
        //Get seller profit
        List<Double> resultListFee = RETRIEVE_ALL(SellerBalanceTransaction.class, null,
                                                            criterions, null, null, projectionsFee, -1, -1);
        if (resultListFee.get(0) != null) {
            cashCalculation.setFullProfit((resultListFee.get(0)));
        }

        //Spare cash. [parent_seller_balance - (all_sub_sellers_balances + all_parent_seller`s_terminal_balances)]
        QueryParameterWrapper[] parameterWrapper = new QueryParameterWrapper[]{
                                                        new QueryParameterWrapper("parentSeller", sellerId)};
        //Get spare cash
        if (isCalculateSpareCash) {
            List<Double> spareCash = EXECUTE_QUERY(SellerBalanceTransaction.class,
                                      "spareCash", parameterWrapper);
            if (spareCash != null) {
                cashCalculation.setSpareCash((spareCash.get(0) == null) ? 0 : spareCash.get(0));
            }
        }

        if (isSumTxAmount) {
            //Retrive all txs for given period for terminal seller owner
            //reinit criterions. retrive txs only for terminal seller owner, without subsellers
            criterions = new CriterionWrapper[] {
                Restrictions.between("transactionTime", dateFrom, dateTo),
                Restrictions.eq("seller.sellerId", sellerId),/*only for given seller*/
                Restrictions.eq("status", "0"),
                Restrictions.in("operationType.operationTypeId", operationTypes),
                Restrictions.eq("sellerBalance", sellerBalance),
            };
            List resultListSumAmount = RETRIEVE_ALL(SellerBalanceTransaction.class, null,
                                                            criterions, null, null, projectionsSumAmount, -1, -1);
            //Set sum of all txs
            if (resultListSumAmount.size() > 0 && resultListSumAmount.get(0) != null) {
                cashCalculation.setAmountTx((Double)resultListSumAmount.get(0));
            }

            //get root seller balance
            Seller rootSeller = (new SellerManager()).getNestedSellerForSeller(sellerId);
            if (rootSeller == null) {
                throw new DatabaseException("err.failed to retrive root seller for seller " + sellerId);
            }
            Long rootBalanceId = rootSeller.getPrimaryBalance().getBalanceId();
            if (rootSeller == null) {
                throw new DatabaseException("err.failed to retrive root seller balance for seller " + sellerId);
            }
            //reinit criterions. set root seller balance
            criterions = new CriterionWrapper[] {
                Restrictions.between("transactionTime", dateFrom, dateTo),
                Restrictions.eq("seller.sellerId", sellerId),/*only for given seller*/
                Restrictions.eq("status", "0"),
                Restrictions.in("operationType.operationTypeId", operationTypes),
                Restrictions.eq("sellerBalance.balanceId", rootBalanceId),};
            //Retrive all txs for given period for root seller of terminal seller owner
            List resultListCreditAmount = RETRIEVE_ALL(SellerBalanceTransaction.class, null,
                                                            criterions, null, null, projectionsSumAmount, -1, -1);
            //Set money taken from root seller
            if (resultListCreditAmount.size() > 0 && resultListCreditAmount.get(0) != null) {
                cashCalculation.setTakenFromRootSeller((Double)resultListCreditAmount.get(0));
            }
        }

        return cashCalculation;
        
    }

    /*Class contains fin. activities for dealer */
    public class CashCalculation {
        private Double fullProfit = 0d;//profit from terminals and subdealers
        private Double spareCash = 0d;//free money. Primary balance - sum of subsellers balances - sum of terminal balances
        private Double balance = 0d;//balance
        private Double takenFromRootSeller = 0d;//taken money from root seller (credit)
        private Double amountTx = 0d;//amount of succes tx

        public Double getFullProfit() {
            return fullProfit;
        }                

        public void setFullProfit(Double fulProfit) {
            this.fullProfit = fulProfit;
        }

        public Double getSpareCash() {
            return spareCash;
        }

        public void setSpareCash(Double spareCash) {
            this.spareCash = spareCash;
        }

        public Double getBalance() {
            return balance;
        }

        public void setBalance(Double balance) {
            this.balance = balance;
        }

        public Double getTakenFromRootSeller() {
            return takenFromRootSeller;
        }

        public void setTakenFromRootSeller(Double takenFromRootSeller) {
            this.takenFromRootSeller = takenFromRootSeller;
        }

        public Double getAmountTx() {
            return amountTx;
        }

        public void setAmountTx(Double amountTx) {
            this.amountTx = amountTx;
        }
    }

    public class CalculationResult{
        private Integer transactionCaunt;
        private Double transactionAmount;
        private Integer transactionSuccessCount;
        private Double transactionSuccessAmount;
        private Double feeForTx;

        public Integer getTransactionCaunt() {
            return transactionCaunt;
        }

        public void setTransactionCaunt(Integer transactionCaunt) {
            this.transactionCaunt = transactionCaunt;
        }

        public Double getTransactionAmount() {
            return transactionAmount;
        }

        public void setTransactionAmount(Double transactionAmount) {
            this.transactionAmount = transactionAmount;
        }

        public Integer getTransactionSuccessCount() {
            return transactionSuccessCount;
        }

        public void setTransactionSuccessCount(Integer transactionSuccessCount) {
            this.transactionSuccessCount = transactionSuccessCount;
        }

        public Double getTransactionSuccessAmount() {
            return transactionSuccessAmount;
        }

        public void setTransactionSuccessAmount(Double transactionSuccessAmount) {
            this.transactionSuccessAmount = transactionSuccessAmount;
        }

        public Double getFeeForTx() {
            return feeForTx;
        }

        public void setFeeForTx(Double feeForTx) {
            this.feeForTx = feeForTx;
        }
    }

    public TransCalculationResult calculateBallanceTransactions(String entityClass, String queryName, QueryParameterWrapper[] parameterWrapper) throws DatabaseException, ClassNotFoundException {
        TransCalculationResult result = new TransCalculationResult();
        Object[] calculations = (Object[]) EXECUTE_QUERY(Class.forName(entityClass), queryName, parameterWrapper).get(0);
        result.setNominalTransSum((Float)calculations[0]);
        result.setTransferSum((Float)calculations[1]);
        result.setAwardSum((Float)calculations[2]);
        return result;
    }
    public IncreaseBallanceResult calculateBallanceIncrease(String entityClass, String queryName, QueryParameterWrapper[] parameterWrapper) throws DatabaseException, ClassNotFoundException {
        IncreaseBallanceResult result = new IncreaseBallanceResult();
        List calculations = EXECUTE_QUERY(Class.forName(entityClass), queryName, parameterWrapper);
        result.setIncreaseBallanceSum((Float)calculations.get(0));
        return result;
    }

    public CurrentAmountResult calculateCurrentAmount(String entityClass, String queryName, QueryParameterWrapper[] parameterWrapper) throws DatabaseException, ClassNotFoundException {
        CurrentAmountResult result = new CurrentAmountResult();
        List calculations = EXECUTE_QUERY(Class.forName(entityClass), queryName, parameterWrapper);
        result.setCurrentTransaction((Float)calculations.get(0));
        return result;
    }

    public DependTransCalcResult calculateAllCount(String entityClass, String queryName, QueryParameterWrapper[] parameterWrapper) throws DatabaseException, ClassNotFoundException {
        DependTransCalcResult result = new DependTransCalcResult();
        List calculations = EXECUTE_QUERY(Class.forName(entityClass), queryName, parameterWrapper);
        result.setAllTransCount((Long)calculations.get(0));
        return result;
    }

    public DependTransCalcResult calculateDebet(String entityClass, String queryName, QueryParameterWrapper[] parameterWrapper) throws DatabaseException, ClassNotFoundException {
        DependTransCalcResult result = new DependTransCalcResult();
        Object[] calculations = (Object[]) EXECUTE_QUERY(Class.forName(entityClass), queryName, parameterWrapper).get(0);
        result.setDebetTransSum((Float)calculations[0]);
        result.setDebetTransCount((Long)calculations[1]);
        return result;
    }

    public DependTransCalcResult calculateCreditCount(String entityClass, String queryName, QueryParameterWrapper[] parameterWrapper) throws DatabaseException, ClassNotFoundException {
        DependTransCalcResult result = new DependTransCalcResult();
        List calculations = EXECUTE_QUERY(Class.forName(entityClass), queryName, parameterWrapper);
        result.setCreditTransCount((Long)calculations.get(0));
        return result;
    }

    public DependTransCalcResult calculateCreditSum(String entityClass, String queryName, QueryParameterWrapper[] parameterWrapper) throws DatabaseException, ClassNotFoundException {
        DependTransCalcResult result = new DependTransCalcResult();
        List calculations = EXECUTE_QUERY(Class.forName(entityClass), queryName, parameterWrapper);
        result.setCreditNomTransSum((Float)calculations.get(0));
        return result;
    }

    public DependTransCalcResult calculateAward(String entityClass, String queryName, QueryParameterWrapper[] parameterWrapper) throws DatabaseException, ClassNotFoundException {
        DependTransCalcResult result = new DependTransCalcResult();
//        List calculations = EXECUTE_QUERY(Class.forName(entityClass), queryName, parameterWrapper);
//        result.setAwardTransSum((Float)calculations.get(0));
        result.setAwardTransSum(0f);
        return result;
    }

    public List<Object[]> getVoucherByTransactionGUID(String guid) throws DatabaseException {
        QueryParameterWrapper[] wrappers = new QueryParameterWrapper[] {new QueryParameterWrapper("transactionGUID", guid)};
        return EXECUTE_QUERY(ClientTransaction.class, "selectVoucherByTransaction", wrappers);
    }

    public class TransCalculationResult{

        private Float nominalTransSum;
        private Float transferSum;
        private Float awardSum;

        public Float getAwardSum() {
            return awardSum;
        }

        public void setAwardSum(Float awardSum) {
            this.awardSum = awardSum;
        }


        public Float getNominalTransSum() {
            return nominalTransSum;
        }

        public void setNominalTransSum(Float nominalTransSum) {
            this.nominalTransSum = nominalTransSum;
        }

        public Float getTransferSum() {
            return transferSum;
        }

        public void setTransferSum(Float transferSum) {
            this.transferSum = transferSum;
        }
    }
    public class IncreaseBallanceResult{
        private Float increaseBallanceSum;

        public Float getIncreaseBallanceSum() {
            return increaseBallanceSum;
        }

        public void setIncreaseBallanceSum(Float increaseBallanceSum) {
            this.increaseBallanceSum = increaseBallanceSum;
        }
    }

    public class CurrentAmountResult {
        private Float currentTransaction;

        public Float getCurrentTransaction() {
            return currentTransaction;
        }

        public void setCurrentTransaction(Float currentTransaction) {
            this.currentTransaction = currentTransaction;
        }
    }
    public class DependTransCalcResult {

        private Long allTransCount;
        private Long debetTransCount;
        private Long creditTransCount;

        private Float debetTransSum;
        private Float creditNomTransSum;
        private Float creditErrNomTransSum;
        private Float AwardTransSum;

        public Long getAllTransCount() {
            return allTransCount;
        }

        public void setAllTransCount(Long allTransCount) {
            this.allTransCount = allTransCount;
        }

        public Long getDebetTransCount() {
            return debetTransCount;
        }

        public void setDebetTransCount(Long debetTransCount) {
            this.debetTransCount = debetTransCount;
        }

        public Long getCreditTransCount() {
            return creditTransCount;
        }

        public void setCreditTransCount(Long creditOkTransCount) {
            this.creditTransCount = creditOkTransCount;
        }

        public Float getDebetTransSum() {
            return debetTransSum;
        }

        public void setDebetTransSum(Float debetTransSum) {
            this.debetTransSum = debetTransSum;
        }

        public Float getCreditNomTransSum() {
            return creditNomTransSum;
        }

        public void setCreditNomTransSum(Float creditOkNomTransSum) {
            this.creditNomTransSum = creditOkNomTransSum;
        }

        public Float getCreditErrNomTransSum() {
            return creditErrNomTransSum;
        }

        public void setCreditErrNomTransSum(Float creditErrNomTransSum) {
            this.creditErrNomTransSum = creditErrNomTransSum;
        }

        public Float getAwardTransSum() {
            return AwardTransSum;
        }

        public void setAwardTransSum(Float awardOkTransSum) {
            AwardTransSum = awardOkTransSum;
        }

    }
}
