package com.tmx.beng.base.cache;

import com.tmx.util.WeakList;
import org.apache.log4j.Logger;

public class DBCacheStack {

    private static Logger logger = Logger.getLogger("bill.DBCache");
    private WeakList weakList = new WeakList();
    private DBCache lastDBCache; //strog reference

    public DBCacheStack(DBCache dbCache){
        if( dbCache == null)
            throw new IllegalArgumentException("dbCache be absent");
        
        lastDBCache = dbCache;
        weakList.add(dbCache);
    }

    synchronized public void dump(long reloadingTime, int taskPoolSize, int transactionPoolSize, double reloadCounter){
        logger.debug( "stack size: " + new Integer(weakList.size())  + " reloading time: " +
                        (System.currentTimeMillis() - reloadingTime) +
                        " taskPoolSize: " + taskPoolSize +
                        " transactionPoolSize: " + transactionPoolSize +
                        " reloadings amount: " + reloadCounter);
    }

    synchronized public DBCache peek(){
        return lastDBCache;
//        return (DBCache)weakList.get(weakList.size()-1);
    }

    synchronized public void push(DBCache dbCache){
        lastDBCache = dbCache;

        weakList.add(dbCache);
    }

    public int size() {
        return weakList.size();
    }
}
