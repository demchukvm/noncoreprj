package com.tmx.beng.iso8583Gate.pdu.substructure.keyexch;

import com.tmx.beng.iso8583Gate.pdu.substructure.BasicSubDEFormat;
import com.tmx.beng.iso8583Gate.pdu.substructure.RuntimeSubDataElement;
import com.tmx.beng.iso8583Gate.pdu.RuntimeDataElement;
import com.tmx.beng.iso8583Gate.pdu.IsoParseException;
import com.tmx.beng.iso8583Gate.pdu.IsoSerializeException;
import com.tmx.beng.iso8583Gate.pdu.util.HexConverter;
import com.tmx.beng.iso8583Gate.pdu.types.BinaryType;
import com.tmx.beng.iso8583Gate.pdu.types.Type;
import com.tmx.beng.iso8583Gate.pdu.length_types.LLLVARLengthType;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Arrays;

/**

 */
public class KeyExchSDEF extends BasicSubDEFormat {
    public static final String KMAC = "KMAC";
    public static final String KPP = "KPP";
    private final int KEY_BIT_LENGTH = 128;

    public void parse(RuntimeDataElement rde) throws IsoParseException {
        RuntimeSubDataElement runtimeSDE = prepare(rde);
        byte[] sourceBytes = rde.getString().getBytes();
        ByteArrayInputStream sourceBais = new ByteArrayInputStream(sourceBytes);
        LLLVARLengthType lengthType = new LLLVARLengthType();
        int actualLength = lengthType.parseLength(sourceBais, 3);
        BinaryType bType = new BinaryType();
        runtimeSDE.addValue(KMAC, new String(bType.parse(sourceBais, KEY_BIT_LENGTH)));
        runtimeSDE.addValue(KPP, new String(bType.parse(sourceBais, KEY_BIT_LENGTH)));
    }

    public void serialize(RuntimeDataElement rde) throws IsoSerializeException {
                RuntimeSubDataElement runtimeSDE = rde.getSubstructureDataElement();
        if(!(runtimeSDE instanceof KeyExchRSDE))
            throw new IsoSerializeException("err.keyexch_sdef.incorrect_rsde_type", new String[]{runtimeSDE.getClass().toString()});

        KeyExchRSDE rsde = (KeyExchRSDE)runtimeSDE;
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        //LLLVARLengthType lengthType = new LLLVARLengthType();
        BinaryType bType = new BinaryType();

        try{                                                       
            //byte[] lengthData = lengthType.serializeLength(evalLength());
            //baos.write(lengthData);
            Type.SerializedValue serializedEL = bType.serialize(true, KEY_BIT_LENGTH, serializeKey(rsde.getValue(KMAC)));
            baos.write(serializedEL.data);
            serializedEL = bType.serialize(true, KEY_BIT_LENGTH, serializeKey(rsde.getValue(KPP)));
            baos.write(serializedEL.data);

        }
        catch(IOException e){
            throw new IsoSerializeException("err.collation_sdef.serialization_failed", e);
        }
        rde.setValue(new String(baos.toByteArray()));
    }

    public RuntimeSubDataElement getRuntimeSubDataElementInstance() {
        return new KeyExchRSDE();
    }

    public int evalLength(){
        return KEY_BIT_LENGTH + KEY_BIT_LENGTH;
    }

    /** Should be 128 bit key in HEX string.
     *  For null key zero-filled value will be generated */
    private byte[] serializeKey(String keyString) throws IsoSerializeException{
        byte[] key = null;           
        if(keyString != null){
            key = new HexConverter().getHexFromString(keyString);
            if(key.length != KEY_BIT_LENGTH / 8)
                throw new IsoSerializeException("Key length is not match to required: "+KEY_BIT_LENGTH + " bit");
        }
        else{
            key = new byte[KEY_BIT_LENGTH / 8];
            Arrays.fill(key, (byte)0);
        }
        return key;
    }
}
