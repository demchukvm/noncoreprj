package com.tmx.beng.medaccess.ussd;

import com.tmx.beng.medaccess.MediumAccess;
import com.tmx.beng.medaccess.MediumConnection;
import com.tmx.beng.medaccess.GateException;

/**
 * TODO: rewrite!!!
 * @author Andrey Nagorniy
 */
public class UssdGateReserveMediumAccess implements MediumAccess {

    public void init() {
    }

    public MediumConnection getConnection() throws GateException {
        return new UssdGateReserveMediumConnection();
    }

}
