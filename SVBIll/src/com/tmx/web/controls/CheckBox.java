package com.tmx.web.controls;

/**
 */
public class CheckBox extends BasicControl{
    private Long id = new Long(-1);
    private String label = "";
    private boolean selected = false;
    private boolean firstSelected = false;
    /** Constants */
    public static String SELECTED_SUFFIX = "selected";//should be exactly the same as value property name


    public CheckBox(){
    }

    public CheckBox(Long id, String label, boolean selected){
		setId(id);
        setLabel(label);
        setSelected(selected);
        notifyUpdate();
    }

    public void initialize() {
        /** do nothing */
    }

    public void safeReset() {
        selected = firstSelected;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        if(!isUpdated()){
            firstSelected = selected;
            notifyUpdate();
        }
        this.selected = selected;
    }
}
