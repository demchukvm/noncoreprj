<?php
/**
* Основные параметры WordPress.
*
* Этот файл содержит следующие параметры: настройки MySQL, префикс таблиц,
* секретные ключи, язык WordPress и ABSPATH. Дополнительную информацию можно найти
* на странице {@link http://codex.wordpress.org/Editing_wp-config.php Editing
* wp-config.php} Кодекса. Настройки MySQL можно узнать у хостинг-провайдера.
*
* Этот файл используется сценарием создания wp-config.php в процессе установки.
* Необязательно использовать веб-интерфейс, можно скопировать этот файл
* с именем "wp-config.php" и заполнить значения.
*
* @package WordPress
*/

// ** Параметры MySQL: Эту информацию можно получить у вашего хостинг-провайдера ** //
/** Имя базы данных для WordPress */
define('DB_NAME', 'bless');

/** Имя пользователя MySQL */
define('DB_USER', 'bless_u');

/** Пароль к базе данных MySQL */
define('DB_PASSWORD', 'bless_p');

/** Имя сервера MySQL */
define('DB_HOST', 'localhost');

/** Кодировка базы данных для создания таблиц. */
define('DB_CHARSET', 'utf8');

/** Схема сопоставления. Не меняйте, если не уверены. */
define('DB_COLLATE', '');

/**#@+
* Уникальные ключи и соли для аутентификации.
*
* Смените значение каждой константы на уникальную фразу.
* Можно сгенерировать их с помощью {@link https://api.wordpress.org/secret-key/1.1/salt/ сервиса ключей на WordPress.org}
* Можно изменить их, чтобы сделать существующие файлы cookies недействительными. Пользователям потребуется снова авторизоваться.
*
* @since 2.6.0
*/
define('AUTH_KEY',         'J8UM>/V66S_S@%@%HOPT86EH//8_,1IS@%S&QYNLAR1J:C46BEBA%:..K//E.@../');
define('SECURE_AUTH_KEY',  'K//S2QZ<W9SU..FHZG7Q@JRKE:JPYX!:8//T!I..@D7K17B8%//T05<M7E7P?CDC.');
define('LOGGED_IN_KEY',    'Y..>%>H,,:UQ.9SHG9@XU6V9OR/S9XARHV9U5CI_QQYU96?B5A!D5AMO9!NUP6//S');
define('NONCE_KEY',        '4XB>5XP/%K5UGVDNYR.%Z1ZS.20LU7QL7A9AMS3NT*M*WNN_&9M/*O0H9*Y7J@OS,');
define('AUTH_SALT',        'O_P><3C?IPF?7V&V!H9D1UW40IV>C5WQBFK?..40BJ..IN!XIW//:!&KY..//7VO.');
define('SECURE_AUTH_SALT', '%<N%A_?0M!SNNG:,ZLO*?TK/ON?E0W2G.@ZK?A5D,VOWZL6SYV//H..FN:_.*@WU_');
define('LOGGED_IN_SALT',   '2L0H7XEZO77GA/*EL?5&GIO3.<4ZSWA/X.._AY*//2Y?_GFF96MKDVW<A*/?LFO*K');
define('NONCE_SALT',       '3RDF4ADRK>S3IOX,P//.UKIF:UKN<4ADYE:AS2FLF,N@&JY0<4GJA%Q9QS5/<TG?D');

/**#@-*/

/**
* Префикс таблиц в базе данных WordPress.
*
* Можно установить несколько блогов в одну базу данных, если вы будете использовать
* разные префиксы. Пожалуйста, указывайте только цифры, буквы и знак подчеркивания.
*/
$table_prefix  = 'wp_';

/**
* Язык локализации WordPress, по умолчанию английский.
*
* Измените этот параметр, чтобы настроить локализацию. Соответствующий MO-файл
* для выбранного языка должен быть установлен в wp-content/languages. Например,
* чтобы включить поддержку русского языка, скопируйте ru_RU.mo в wp-content/languages
* и присвойте WPLANG значение 'ru_RU'.
*/
define('WPLANG', 'ru_RU');

/**
* Для разработчиков: Режим отладки WordPress.
*
* Измените это значение на true, чтобы включить отображение уведомлений при разработке.
* Настоятельно рекомендуется, чтобы разработчики плагинов и тем использовали WP_DEBUG
* в своём рабочем окружении.
*/
define('WP_DEBUG', false);

/* Это всё, дальше не редактируем. Успехов! */

/** Абсолютный путь к директории WordPress. */
if ( !defined('ABSPATH') )
define('ABSPATH', dirname(__FILE__) . '/');

/** Инициализирует переменные WordPress и подключает файлы. */
require_once(ABSPATH . 'wp-settings.php');
