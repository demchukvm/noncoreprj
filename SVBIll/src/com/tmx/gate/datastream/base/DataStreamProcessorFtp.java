package com.tmx.gate.datastream.base;

import java.util.Properties;
import java.util.Map;
import java.util.HashMap;

import org.ftp4che.FTPConnection;
import org.ftp4che.FTPConnectionFactory;

/**
 * Abstract datastream processor. Contains methods to execute basic FTP operations:
 * openConnection(), closeConnection() and etc.
 */
public abstract class DataStreamProcessorFtp extends DataStreamProcessor {
    /**
     * FTP manager provides API to work with FTP
     */
//    private FTPManager ftpManager = new FTPManager();
    Map ftpConnections = new HashMap();

    /**
     * Prepare and open FTP connection using parameters of current Data Stream.
     */
    protected void openConnection(String processorName) throws DataStreamException {
        Properties props = null;
        FTPConnection connection = null;
        try {
            props = getProperties(processorName);
            connection = FTPConnectionFactory.getInstance(props);

            //Set up connection
            connection.connect();
            ftpConnections.put(processorName, connection);
        }
        catch (Throwable e) {
            try {
                logger.error("Failed to open connection to " + createConnectionString(props));
                if(connection != null)
                    connection.disconnect();
            }
            catch (Throwable ex) {
                logger.warn("err.gate.ftp_datastream_processor.failed_to_close_connection");
            }
            throw new DataStreamException("err.gate.ftp.ftp_datastream_processor.failed_to_open_connection_for", new String[]{getName()}, e);
        }
    }

    /** Open own connection. It is identified in the gate.xml file by this processor thread name.
     * @throws DataStreamException*/
    protected void openConnection() throws DataStreamException {
        openConnection(getName());
    }


    /**
     * Close FTP connection
     * @param processorName
     */
    protected void closeConnection(String processorName) {
        try {
            FTPConnection connection = (FTPConnection) ftpConnections.get(processorName);
            if(connection != null){
                connection.disconnect();
                ftpConnections.remove(processorName);
            }
        }catch (Throwable e) {
            logger.warn("err.gate.ftp_datastream_processor.failed_to_close_connection");
        }
    }

    /** Close own connection. It is identified in the gate.xml file by this processor thread name. */
    protected void closeConnection() {
        closeConnection(getName());
    }

    protected FTPConnection getFTPConnection(String processorName){
        return (FTPConnection) ftpConnections.get(processorName);
    }

    protected FTPConnection getFTPConnection(){
        return getFTPConnection(getName());
    }


    private String createConnectionString(Properties props){
        if(props == null)
            return "properties is null";
        else
            return props.getProperty("connection.type") + "://" +
                props.getProperty("user.login") + ":" +
                props.getProperty("user.password") + "@" +
                props.getProperty("connection.host") + ":" +
                props.getProperty("connection.port") + 
                props.getProperty("directory") +
               " (PARAMS: "+
               "passiveMode="+props.getProperty("connection.passive") +
               ")";
    }

}
