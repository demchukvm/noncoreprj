package com.tmx.web.forms.core.ui_permission;

import com.tmx.web.base.BasicActionForm;
import com.tmx.web.controls.EditBox;
import com.tmx.web.controls.Button;
import com.tmx.web.controls.PopulateUtil;
import com.tmx.as.entities.general.role.Role;

public class InfoRoleForm extends BasicActionForm {
    // work
    private Long selectedRoleId;
    private final static String action="/core/ui_permission/infoRole";
    private Role content;
    // info
    private EditBox roleId;
    private EditBox key;
    private EditBox roleName;
    // buttons
    private Button buttonApply;
    private Button buttonSave;
    private Button buttonCancel;
    private Button buttonReset;
    protected void init() {
        // info
        roleId=new EditBox();roleId.setReadonly(true);
        key=new EditBox();key.setMandatory(true);
        roleName=new EditBox();roleName.setMandatory(true);
        // buttons
        buttonApply=new Button();
        buttonApply.setLabelKey("ctrl.button.label.apply");
        buttonApply.setAction(action);
        buttonApply.setCommand("apply");

        buttonSave=new Button();
        buttonSave.setLabelKey("ctrl.button.label.save");
        buttonSave.setAction(action);
        buttonSave.setCommand("save");

        buttonCancel=new Button();
        buttonCancel.setLabelKey("ctrl.button.label.cancel");
        buttonCancel.setAction("/core/ui_permission/manageRole");

        buttonReset =new Button();
        buttonReset.setLabelKey("ctrl.button.label.reset");
    }

    public void bindData(Role value) {
        content=value;
        roleId.setValue(value.getId() == null ? "" : value.getId().toString());
        key.setValue(value.getKey());
        roleName.setValue(value.getRoleName());
    }

    public Role populateFromControls() {
        content.setRoleId(PopulateUtil.getLongValue(roleId));
        content.setKey(key.getValue());
        content.setRoleName(roleName.getValue());
        return content;
    }

    public Button getButtonApply() {
        return buttonApply;
    }

    public void setButtonApply(Button buttonApply) {
        this.buttonApply = buttonApply;
    }

    public Button getButtonSave() {
        return buttonSave;
    }

    public void setButtonSave(Button buttonSave) {
        this.buttonSave = buttonSave;
    }

    public Button getButtonCancel() {
        return buttonCancel;
    }

    public void setButtonCancel(Button buttonCancel) {
        this.buttonCancel = buttonCancel;
    }

    public Button getButtonReset() {
        return buttonReset;
    }

    public void setButtonReset(Button buttonReset) {
        this.buttonReset = buttonReset;
    }

    public Long getSelectedRoleId() {
        return selectedRoleId;
    }

    public void setSelectedRoleId(Long selectedRoleId) {
        this.selectedRoleId = selectedRoleId;
    }

    public Role getContent() {
        return content;
    }

    public void setContent(Role content) {
        this.content = content;
    }

    public EditBox getRoleId() {
        return roleId;
    }

    public void setRoleId(EditBox roleId) {
        this.roleId = roleId;
    }

    public EditBox getKey() {
        return key;
    }

    public void setKey(EditBox key) {
        this.key = key;
    }

    public EditBox getRoleName() {
        return roleName;
    }

    public void setRoleName(EditBox roleName) {
        this.roleName = roleName;
    }
}
