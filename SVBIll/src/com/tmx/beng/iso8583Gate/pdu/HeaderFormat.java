package com.tmx.beng.iso8583Gate.pdu;

import java.io.InputStream;
import java.io.IOException;
import java.io.ByteArrayOutputStream;
import java.util.Iterator;
import java.util.List;

/**

 */
public class HeaderFormat {
    private final int HEADER_LENGTH = 7;
    private final int TPDU_LENGTH = 5;
    private final int ISO8583_MSG_HEADERID = 96;    

    public RuntimeHeader parse(InputStream is) throws IsoParseException {
        try{
            RuntimeHeader rh = new RuntimeHeader(this);
            byte[] header = new byte[HEADER_LENGTH];

            ParsingCollector collector = new ParsingCollector();
            while(!collector.isCollected())
                collector.push(is.read());                    

            header = collector.getHeaderSource();
            //is.read(header);
            rh.setLength(header[0] << 8 | header[1]);
            rh.setId(header[2]);
            rh.setDestinationAddress(header[3] << 8 | header[4]);
            rh.setOriginatorAddress(header[5] << 8 | header[6]);
            return rh;
        }
        catch(IOException e){
            throw new IsoParseException("err.header_format.parsing_failed", e);
        }
    }

    public byte[] serialize(RuntimeHeader rh) throws IsoSerializeException{
        byte[] header = new byte[7];
        //length
        header[0] = (byte) ((rh.getLength() >> 8) & 0xFF);
        header[1] = (byte) (rh.getLength() & 0xFF);
        //id
        header[2] = (byte) (rh.getId() & 0xFF);
        //destination address
        header[3] = (byte) ((rh.getDestinationAddress() >> 8) & 0xFF);
        header[4] = (byte) (rh.getDestinationAddress() & 0xFF);
        //originator address
        header[5] = (byte) ((rh.getOriginatorAddress() >> 8) & 0xFF);
        header[6] = (byte) (rh.getOriginatorAddress() & 0xFF);
        return header;
    }

    public RuntimeHeader create(List applicationDataByteChanks){
        //evaluate message size
        int msgLength = 0;
        for(Iterator iter = applicationDataByteChanks.iterator(); iter.hasNext();)
            msgLength = msgLength + ((byte[])iter.next()).length;

        msgLength = msgLength + TPDU_LENGTH;

        RuntimeHeader rh = new RuntimeHeader(this);
        rh.setLength(msgLength);
        return rh;
    }

    /** ParsingCollector skips gap bytes before iso8583 message header */
    private class ParsingCollector {
        private boolean msgIdFound = false;
        private ByteArrayOutputStream baos = new ByteArrayOutputStream();
        private int bytesToCollect;

        /** push the next byte of inputstream into the collector.
         * If complete header source is collected there then method returns true. */
        private boolean push(int b) throws IsoParseException{
            //b == -1 -> enf of inputstream
            if(b < 0)
                throw new IsoParseException("err.header_format.parsing_collector.header_not_found_in_gap");
            baos.write(b);
            if(msgIdFound)
                bytesToCollect--;
            if(isCollected())
                return true;

            if(!msgIdFound && b == ISO8583_MSG_HEADERID){
                msgIdFound = true;
                bytesToCollect = TPDU_LENGTH-1;//msgIdByte from tpdu just collected
            }
            
            return false;
        }

        private boolean isCollected(){
            return msgIdFound && bytesToCollect == 0;
        }

        private byte[] getHeaderSource(){
            //extract header bytes from collected gap
            byte[] headerData = new byte[HEADER_LENGTH];
            byte[] gap = baos.toByteArray();
            System.arraycopy(gap, gap.length-HEADER_LENGTH, headerData, 0, HEADER_LENGTH);
            return headerData;
        }
    }
   
}
