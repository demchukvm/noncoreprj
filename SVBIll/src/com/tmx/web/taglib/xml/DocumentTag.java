package com.tmx.web.taglib.xml;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;
import org.apache.struts.taglib.TagUtils;

/**
 * Renders an Document element with appropriate attributes
 */
public class DocumentTag extends TagSupport {
  

    // ------------------------------------------------------------- Properties


    /**
     * Process the start of this tag.
     *
     * @exception JspException if a JSP exception has occurred
     */
    public int doStartTag() throws JspException {
        TagUtils.getInstance().write(this.pageContext, this.renderDocumentStartElement());
        return EVAL_BODY_INCLUDE;
    }

    /**
     * Renders an &lt;html&gt; element with appropriate language attributes.
     * @since Struts 1.2
     */
    protected String renderDocumentStartElement() {
        StringBuffer sb = new StringBuffer("<document");
        sb.append(">");
        return sb.toString();
    }


    /**
     * Process the end of this tag.
     *
     * @exception JspException if a JSP exception has occurred
     */
    public int doEndTag() throws JspException {

        TagUtils.getInstance().write(pageContext, "</document>");

        // Evaluate the remainder of this page
        return (EVAL_PAGE);

    }

    /**
     * Release any acquired resources.
     */
    public void release() {
    }

}
