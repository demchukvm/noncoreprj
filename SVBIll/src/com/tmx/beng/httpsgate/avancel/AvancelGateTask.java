package com.tmx.beng.httpsgate.avancel;

import com.tmx.util.queue.ActionQueueElement;
import com.tmx.beng.base.BillingMessage;
import com.tmx.beng.httpsgate.HttpsGateException;
import org.apache.log4j.Logger;

public class AvancelGateTask extends ActionQueueElement {

    private BillingMessage procesedMessage = null;
    private BasicAvancelGate gate;

    public AvancelGateTask(BillingMessage billingMessage, BasicAvancelGate gate){
        this.procesedMessage = billingMessage;
        this.gate = gate;
    }

    public void run() {
        try{
            gate.runBillMessage(procesedMessage);
        }
        catch(HttpsGateException e){
            gate.logger.error(e.getLocalizedMessage(), e);
        }
    }
}

