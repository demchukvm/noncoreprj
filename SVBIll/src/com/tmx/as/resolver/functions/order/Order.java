package com.tmx.as.resolver.functions.order;

import com.tmx.as.base.OrderWrapper;
import com.tmx.as.resolver.functions.Function;
import com.tmx.as.resolver.FunctionSyntaxException;
import com.tmx.as.resolver.ResolverException;

public class Order implements Function {
    OrderWrapper order=new OrderWrapper();
    public void set(int paramIndex, String paramName, Object paramValue) throws FunctionSyntaxException {
        try {
            order.set(paramName,(String)paramValue);
        } catch (ResolverException e) {
            throw new FunctionSyntaxException("projection error", e);
        }
    }

    public Object returnValue() throws FunctionSyntaxException {
        return order;
    }
}
