package com.tmx.beng.csapi.java.base;

import com.tmx.as.entities.bill.function.ActualFunctionParameter;

import java.util.List;
import java.util.Date;


public class ClientMessages {
    public static interface ValidateParamsMsg {
        void setActualFunctionParams(List params);
        void setFunctionTypeId(Long functionTypeId);
    }

    //DEFAULT PARAMS MSG--------------------------------
    public static interface DefaultParamsMsg extends BillingMessage<DefaultParamsResp> {
    	void setFunctionTypeId(Long functionTypeId);
    }
    public static interface DefaultParamsResp {
        List<ActualFunctionParameter> getDefaultParams();
    }
    //--------------------------------------------------

    public static interface BuyVoucherMsg extends BasicMessage {
    	void setServiceCode(String serviceCode);
        void setVoucherNominal(String voucherNominal);
    }

    public static interface BasicMessage  {
        void setTerminalSN(String terminalSN);
        void setSellerCode(String sellerCode);
        void setProcessingCode(String processingCode);
        void setLogin(String login);
        void setPassword(String password);
        void setClientTime(Date date);
        void setPasswordMD5(String passwordMD5);
    }

    
}
