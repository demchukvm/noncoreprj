package com.tmx.web.forms.core.balance.operator_balance;

import com.tmx.as.entities.bill.balance.AbstractBalance;
import com.tmx.as.entities.bill.balance.OperatorBalance;
import com.tmx.as.entities.bill.operator.Operator;
import com.tmx.web.controls.PopulateUtil;
import com.tmx.web.controls.ScrollBox;
import com.tmx.web.controls.table.ControlManagerTable;
import com.tmx.web.forms.core.balance.AbstractInfoBalanceForm;


public class InfoOperatorBalanceForm extends AbstractInfoBalanceForm {

    private Long operatorId;

    protected void initButtonsAction() {
        onApplyButton.setAction("/core/balance/operator_balance/infoOperatorBalance");
        onApplyTransButton.setAction("/core/balance/operator_balance/infoOperatorBalance");
        onCancelButton.setAction("/core/balance/operator_balance/manageOperatorBalances");
        onSaveButton.setAction("/core/balance/operator_balance/infoOperatorBalance");
        onChangeBalanceButton.setAction("/core/balance/operator_balance/changeOperatorBalance");
        onCreateRestButton.setAction("/core/balance/operator_balance/operatorRestriction");
        onEditRestButton.setAction("/core/balance/operator_balance/operatorRestriction");
        onDeleteRestButton.setAction("/core/balance/operator_balance/operatorRestriction");
    }

    protected ScrollBox initScrollBoxBalanceOwner() {
        return scrollBoxBalanceOwner = new ScrollBox(){
            protected String getKey(Object dataListElement) {
                return ((Operator)dataListElement).getId().toString();
            }
            protected String getValue(Object dataListElement) {
                return ((Operator)dataListElement).getName();
            }
        };
    }

    protected ControlManagerTable initTransactionsTable() {
        ControlManagerTable transactionsTable = new ControlManagerTable("query(query_type=retrieve_all," +
                "entity_class=com.tmx.as.entities.bill.transaction.OperatorBalanceTransaction," +
                "table_attr=operator," +
                "table_attr=operatorBalance," +
                "table_attr=clientTransaction," +
                "order(name=transactionTime,type=desc)," +
                "page_size=15" +
                ")");
        transactionsTable.setRefreshActionName("/core/balance/operator_balance/infoOperatorBalance");
        transactionsTable.setRefreshCommandName("refreshTransactionsTable");
        return transactionsTable;
    }

    protected void bindData(AbstractBalance balance) {}

    public AbstractBalance populateFromControls() {
        Long operatorId = PopulateUtil.getLongKey(scrollBoxBalanceOwner);
        OperatorBalance balance = new OperatorBalance();
        Operator operator = new Operator();
        operator.setOperatorId(operatorId);
        balance.setOperator(operator);
        return balance;
    }

    public Long getOperatorId() {
        return operatorId;
    }

    public void setOperatorId(Long operatorId) {
        this.operatorId = operatorId;
    }
}
