LOAD DATA
INFILE *
APPEND INTO TABLE svbill.vc_voucher
Fields terminated by ";" Optionally enclosed by '"'
TRAILING NULLCOLS
(
  VC_SECRETCODE position(01:14),
  VC_CODE position(15:23),
  VC_PRICE position(24:32),
  VC_BESTBEFOREDATE position(35:42) "to_date(:VC_BESTBEFOREDATE, 'DD.MM.YYYY')",
  VC_NOMINAL constant 'LifeVoucher35',
  VC_VOUCHERID "voucher_seq.nextval",
  VC_STATUS constant 'NEW',
  VC_LOADINGTIME "sysdate", 
  VC_VALID constant '1'
  OP_OperatorId constant 2397778
)