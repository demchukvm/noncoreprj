package com.tmx.web.base;

import com.tmx.as.entities.general.user.User;
import com.tmx.as.exceptions.SecurityException;
import com.tmx.as.base.FilterWrapper;
import org.apache.log4j.Logger;

import java.util.Map;
import java.util.HashMap;
import java.util.Set;
import java.util.HashSet;

/**
 * 1) Provides the simple direct access from JSP pages to services and other logic.
 * To use this method from Struts nested tags define getters without input params.
 * */
public class UserContextForm extends BasicActionForm{
    private RequestHistory requestHistory = new RequestHistory();
    private GlobalFiltersHelper globalFiltersHelper = new GlobalFiltersHelper();

    /** to hold tracking parameters */
    private Logger logger = Logger.getLogger("web.UserContextForm");


    /** Get URL for current user menu.
     * Should be absolutely safe method to avoid exceptions handling
     * in JSP where it will be used. */
    public String getMenuURL(){
        return ApplicationEnvironment.getAppDeploymentName() + "/management/menu.jsp";
    }

    public String getDefaultHeaderURL(){
        return ApplicationEnvironment.getAppDeploymentName() + "/management/header.jsp";
    }

    public String getDefaultWorkplaceURL(){
        return ApplicationEnvironment.getAppDeploymentName() + "/management/start.jsp";
    }

    public User getCurrentUser(){
        try{
            return ((SecurityService) ApplicationEnvironment.getService(SecurityService.class)).getLoggedInUser(RequestEnvironment.currentReqContext().getRequest().getSession());
        }
        catch(SecurityException e){
            return null;
        }
    }

    public RequestHistory getRequestHistory() {
        return requestHistory;
    }


    public GlobalFiltersHelper getGlobalFiltersHelper() {
        return globalFiltersHelper;
    }
}