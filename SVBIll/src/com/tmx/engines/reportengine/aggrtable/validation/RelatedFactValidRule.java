package com.tmx.engines.reportengine.aggrtable.validation;

import com.tmx.engines.reportengine.aggrtable.DataElement;

import java.util.Map;
import java.util.Iterator;

/**
 */
public class RelatedFactValidRule extends BasicValidationRule{

    public DataValidationError validate(Map metacubePoint){
        boolean cellAlreadyHasError = false;
        DataValidationError validError = null;

        for(Iterator iter = metacubePoint.keySet().iterator(); iter.hasNext();){
            String factName = (String)iter.next();
            if(factName.equals(getOwnerFactName())){
                //check if point already erroneous then do not overwrite error
                DataElement data = (DataElement)metacubePoint.get(factName);
                if(data != null && data.getValidationError() != null)
                    cellAlreadyHasError = true;
            }
            else{
                //ignore ownered data
                DataElement data = (DataElement)metacubePoint.get(factName);
                if(data != null && data.getValidationError() != null)
                    validError = new DataValidationError("related_factfunc_data_erroneous", new String[]{factName});
            }
        }
        if(cellAlreadyHasError)
            return null;
        else
            return validError;
    }
}

