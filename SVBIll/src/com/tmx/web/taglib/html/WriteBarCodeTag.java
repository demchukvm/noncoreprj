package com.tmx.web.taglib.html;

import org.apache.struts.taglib.TagUtils;
import org.apache.struts.taglib.nested.NestedPropertyHelper;

import javax.servlet.jsp.JspException;
import javax.servlet.http.HttpServletRequest;

import com.tmx.web.base.ApplicationEnvironment;


public class WriteBarCodeTag extends com.tmx.web.taglib.base.LocalizableTag{

    public int doStartTmxTag() throws JspException {
        //get barcode from badge entity
        HttpServletRequest request = (HttpServletRequest) pageContext.getRequest();
        String originalName = (getName() != null) ? getName() : NestedPropertyHelper.getCurrentName(request, this);
        String originalProperty = getProperty();
        String nestedProperty = NestedPropertyHelper.getAdjustedProperty(request, originalProperty);

        Object barCode = TagUtils.getInstance().lookup(pageContext, originalName, nestedProperty, null);

        if( barCode != null )
            TagUtils.getInstance().write(pageContext, getBarCodeHTML( (String)barCode ) );            
        else
            TagUtils.getInstance().write( pageContext, "" );

        return (EVAL_BODY_TAG);
    }

    private String getBarCodeHTML( String barCode ){
        String code = '*' + barCode + '*';

        String barCodeHTML = "<table border=0 cellspacing=0 cellpadding=0>" +
                                "<tr>";

        String bcstr = "";

        for (int i = 0; i < code.toCharArray().length; i++) {
              String bc = "";
		      if (code.charAt(i) == ' ') { bc = "100011101011101";}
		      if (code.charAt(i) == '$') { bc = "100010001000101";}
		      if (code.charAt(i) == '%') { bc = "101000100010001";}
		      if (code.charAt(i) == '*') { bc = "100010111011101";}
		      if (code.charAt(i) == '+') { bc = "100010100010001";}
		      if (code.charAt(i) == '-') { bc = "100010101110111";}
		      if (code.charAt(i) == '.') { bc = "111000101011101";}
		      if (code.charAt(i) == '/') { bc = "100010001010001";}
		      if (code.charAt(i) == '0') { bc = "101000111011101";}
		      if (code.charAt(i) == '1') { bc = "111010001010111";}
		      if (code.charAt(i) == '2') { bc = "101110001010111";}
		      if (code.charAt(i) == '3') { bc = "111011100010101";}
		      if (code.charAt(i) == '4') { bc = "101000111010111";}
		      if (code.charAt(i) == '5') { bc = "111010001110101";}
		      if (code.charAt(i) == '6') { bc = "101110001110101";}
		      if (code.charAt(i) == '7') { bc = "101000101110111";}
		      if (code.charAt(i) == '8') { bc = "111010001011101";}
		      if (code.charAt(i) == '9') { bc = "101110001011101";}
		      if (code.charAt(i) == 'A') { bc = "111010100010111";}
		      if (code.charAt(i) == 'B') { bc = "101110100010111";}
		      if (code.charAt(i) == 'C') { bc = "111011101000101";}
		      if (code.charAt(i) == 'D') { bc = "101011100010111";}
		      if (code.charAt(i) == 'E') { bc = "111010111000101";}
		      if (code.charAt(i) == 'F') { bc = "101110111000101";}
		      if (code.charAt(i) == 'G') { bc = "101010001110111";}
		      if (code.charAt(i) == 'H') { bc = "111010100011101";}
		      if (code.charAt(i) == 'I') { bc = "101110100011101";}
		      if (code.charAt(i) == 'J') { bc = "101011100011101";}
		      if (code.charAt(i) == 'K') { bc = "111010101000111";}
		      if (code.charAt(i) == 'L') { bc = "101110101000111";}
		      if (code.charAt(i) == 'M') { bc = "111011101010001";}
		      if (code.charAt(i) == 'N') { bc = "101011101000111";}
		      if (code.charAt(i) == 'O') { bc = "111010111010001";}
		      if (code.charAt(i) == 'P') { bc = "101110111010001";}
		      if (code.charAt(i) == 'Q') { bc = "101010111000111";}
		      if (code.charAt(i) == 'R') { bc = "111010101110001";}
		      if (code.charAt(i) == 'S') { bc = "101110101110001";}
		      if (code.charAt(i) == 'T') { bc = "101011101110001";}
		      if (code.charAt(i) == 'U') { bc = "111000101010111";}
		      if (code.charAt(i) == 'V') { bc = "100011101010111";}
		      if (code.charAt(i) == 'W') { bc = "111000111010101";}
		      if (code.charAt(i) == 'X') { bc = "100010111010111";}
		      if (code.charAt(i) == 'Y') { bc = "111000101110101";}
		      if (code.charAt(i) == 'Z') { bc = "100011101110101";}

              if ( !"".equals( bcstr ) )
                  bcstr += '0';

              bcstr += bc;
		}

        for (int i = 0; i < bcstr.toCharArray().length; i++) {
		      barCodeHTML += "<td>"+
                                "<img src='"+getPath()+ bcstr.charAt(i) + ".png' alt='" + bcstr.charAt(i) + "'>" +
		                     "</td>";
		}

        barCodeHTML += "</tr>" +
                 "</table>";

        return barCodeHTML;
    }

    private String getPath() {
        return ApplicationEnvironment.getAppDeploymentName() + getDefaultProperty("tag.barCode.img.stripe");
    }
    

}
