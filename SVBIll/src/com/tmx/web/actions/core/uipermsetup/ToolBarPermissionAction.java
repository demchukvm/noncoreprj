package com.tmx.web.actions.core.uipermsetup;

import com.tmx.web.base.BasicDispatchedAction;
import com.tmx.web.forms.core.uipermsetup.ToolBarPermissionForm;
import com.tmx.as.blogic.UserManager;
import com.tmx.as.blogic.PermissionManager;
import com.tmx.as.entities.general.ui_permission.Permission;
import com.tmx.as.entities.general.ui_permission.UserInterfaceType;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForm;
import org.apache.xerces.impl.dv.util.Base64;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Iterator;
import java.util.Hashtable;
import java.util.Enumeration;

public class ToolBarPermissionAction extends BasicDispatchedAction {
    public ActionForward setup(ActionMapping mapping,
                                       ActionForm form,
                                       HttpServletRequest request,
                                       HttpServletResponse response) throws Throwable {
        ToolBarPermissionForm toolBarPermissionForm = (ToolBarPermissionForm) form;
        List<Permission> rolePerms = new PermissionManager().getRolePermissions(new Long(toolBarPermissionForm.getRoles().getSelectedKey()));
        Hashtable permissionsForm = new Hashtable();
        Hashtable permissionsMenu = new Hashtable();
        Hashtable permissionsMenuContainer = new Hashtable();
        Hashtable permissionsMenuItem = new Hashtable();
        Hashtable permissionsFormControl = new Hashtable();
        for (Permission perm : rolePerms) {
            if (perm.getUserInterface().getType().getName().equalsIgnoreCase(UserInterfaceType.FORM)) {
                permissionsForm.put(perm.getUserInterface().getName(), perm.getPermissionType().getPermissionTypeId());
                
            } else if (perm.getUserInterface().getType().getName().equalsIgnoreCase(UserInterfaceType.MENU)) {
                permissionsMenu.put(perm.getUserInterface().getName(), perm.getPermissionType().getPermissionTypeId());

            } else if (perm.getUserInterface().getType().getName().equalsIgnoreCase(UserInterfaceType.MENU_CONTAINER)) {
                permissionsMenuContainer.put(perm.getUserInterface().getName(), perm.getPermissionType().getPermissionTypeId());

            } else if (perm.getUserInterface().getType().getName().equalsIgnoreCase(UserInterfaceType.MENU_ITEM)) {
                permissionsMenuItem.put(perm.getUserInterface().getName(), perm.getPermissionType().getPermissionTypeId());

            } else if (perm.getUserInterface().getType().getName().equalsIgnoreCase(UserInterfaceType.FORM_CONTROL)) {
                permissionsFormControl.put(perm.getUserInterface().getName(), perm.getPermissionType().getPermissionTypeId());

            }
        }
        toolBarPermissionForm.setPermissionEntities(rolePerms);
        toolBarPermissionForm.setPermissionsForm(permissionsForm);
        toolBarPermissionForm.setPermissionsMenu(permissionsMenu);
        toolBarPermissionForm.setPermissionsMenuContainer(permissionsMenuContainer);
        toolBarPermissionForm.setPermissionsMenuItem(permissionsMenuItem);
        toolBarPermissionForm.setPermissionsFormControl(permissionsFormControl);
        // set permission type objects
        toolBarPermissionForm.setPermissionTypeObjects(new PermissionManager().getPermissionTypes());
        return new ActionForward(toolBarPermissionForm.getForwardUrl(),true);
    }

    public ActionForward apply(ActionMapping mapping,
                                       ActionForm form,
                                       HttpServletRequest request,
                                       HttpServletResponse response) throws Throwable {
        ToolBarPermissionForm toolBarPermissionForm = (ToolBarPermissionForm) verifyForm(form, ToolBarPermissionForm.class);
        // base64 decode appli maps
        decodeHashtable(toolBarPermissionForm.getApplyPermissionsForm());
        decodeHashtable(toolBarPermissionForm.getApplyPermissionsFormControl());
        decodeHashtable(toolBarPermissionForm.getApplyPermissionsMenu());
        decodeHashtable(toolBarPermissionForm.getApplyPermissionsMenuContainer());
        decodeHashtable(toolBarPermissionForm.getApplyPermissionsMenuItem());

        new PermissionManager().saveDifferentPermissionsMapElements(
                toolBarPermissionForm.getApplyPermissionsForm(),
                toolBarPermissionForm.getPermissionsForm(),
                toolBarPermissionForm.getPermissionEntities(),
                UserInterfaceType.FORM_ID,
                new Long(toolBarPermissionForm.getRoles().getSelectedKey())
        );
        new PermissionManager().saveDifferentPermissionsMapElements(
                toolBarPermissionForm.getApplyPermissionsFormControl(),
                toolBarPermissionForm.getPermissionsFormControl(),
                toolBarPermissionForm.getPermissionEntities(),
                UserInterfaceType.FORM_CONTROL_ID,
                new Long(toolBarPermissionForm.getRoles().getSelectedKey())
        );
        new PermissionManager().saveDifferentPermissionsMapElements(
                toolBarPermissionForm.getApplyPermissionsMenu(),
                toolBarPermissionForm.getPermissionsMenu(),
                toolBarPermissionForm.getPermissionEntities(),
                UserInterfaceType.MENU_ID,
                new Long(toolBarPermissionForm.getRoles().getSelectedKey())
        );
        new PermissionManager().saveDifferentPermissionsMapElements(
                toolBarPermissionForm.getApplyPermissionsMenuContainer(),
                toolBarPermissionForm.getPermissionsMenuContainer(),
                toolBarPermissionForm.getPermissionEntities(),
                UserInterfaceType.MENU_CONTAINER_ID,
                new Long(toolBarPermissionForm.getRoles().getSelectedKey())
        );
        new PermissionManager().saveDifferentPermissionsMapElements(
                toolBarPermissionForm.getApplyPermissionsMenuItem(),
                toolBarPermissionForm.getPermissionsMenuItem(),
                toolBarPermissionForm.getPermissionEntities(),
                UserInterfaceType.MENU_ITEM_ID,
                new Long(toolBarPermissionForm.getRoles().getSelectedKey())
        );
        return new ActionForward(toolBarPermissionForm.getForwardUrl(),true);
    }

    private void decodeHashtable(Hashtable hashtable64) {
        Enumeration enumeration=hashtable64.keys();
        Hashtable hashtable=new Hashtable();
        while (enumeration.hasMoreElements()) {
            String key=(String)enumeration.nextElement();
            hashtable.put(new String(Base64.decode(key)),hashtable64.get(key));
        }
        hashtable64.clear();
        hashtable64.putAll(hashtable);
    }

    public ActionForward interactiveActivate(ActionMapping mapping,
                                       ActionForm form,
                                       HttpServletRequest request,
                                       HttpServletResponse response) throws Throwable {
        ToolBarPermissionForm toolBarPermissionForm = (ToolBarPermissionForm) verifyForm(form, ToolBarPermissionForm.class);
        toolBarPermissionForm.setActive(true);
        update(mapping,form,request,response);
        setup(mapping,form,request,response);
        return new ActionForward(toolBarPermissionForm.getForwardUrl(),true);
    }

    public ActionForward interactiveDectivate(ActionMapping mapping,
                                       ActionForm form,
                                       HttpServletRequest request,
                                       HttpServletResponse response) throws Throwable {
        ToolBarPermissionForm toolBarPermissionForm = (ToolBarPermissionForm) verifyForm(form, ToolBarPermissionForm.class);
        toolBarPermissionForm.setActive(false);
        toolBarPermissionForm.setPermissionsForm(null);
        toolBarPermissionForm.setPermissionsFormControl(null);
        toolBarPermissionForm.setPermissionsMenu(null);
        toolBarPermissionForm.setPermissionsMenuContainer(null);
        toolBarPermissionForm.setPermissionsMenuItem(null);
        return new ActionForward(toolBarPermissionForm.getForwardUrl(),true);
    }

    public ActionForward update(ActionMapping mapping,
                                       ActionForm form,
                                       HttpServletRequest request,
                                       HttpServletResponse response) throws Throwable {
        ToolBarPermissionForm toolBarPermissionForm = (ToolBarPermissionForm) verifyForm(form, ToolBarPermissionForm.class);
        List roles=new UserManager().getAllRolesWithoutSUPERUSER();
        toolBarPermissionForm.getRoles().bind(roles);
        return new ActionForward(toolBarPermissionForm.getForwardUrl(),true);
    }
}
