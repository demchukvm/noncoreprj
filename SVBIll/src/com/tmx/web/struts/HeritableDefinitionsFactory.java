package com.tmx.web.struts;

import org.apache.struts.tiles.NoSuchDefinitionException;
import org.apache.struts.tiles.xmlDefinition.DefinitionsFactory;
import org.apache.struts.tiles.xmlDefinition.XmlDefinition;
import org.apache.struts.tiles.xmlDefinition.XmlDefinitionsSet;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * <p>A factory for definitions.
 * This factory allows to retrieve definitions by their keys.</p>
 * <p><a href="HeritableDefinitionsFactory.java.html"><i>View Source</i></a></p>
 * <p/>
 */
public class HeritableDefinitionsFactory extends DefinitionsFactory {

	/**
	 * Constructor.
	 * Creates a factory initialized with definitions from {@link org.apache.struts.tiles.xmlDefinition.XmlDefinitionsSet}.
	 *
	 * @param xmlDefinitions Resolved definition from XmlDefinitionSet.
	 * @throws org.apache.struts.tiles.NoSuchDefinitionException
	 *          If an error occurs while resolving inheritance
	 */
	public HeritableDefinitionsFactory(XmlDefinitionsSet xmlDefinitions)
	        throws NoSuchDefinitionException {
		super(xmlDefinitions);

		definitions = new HashMap();
		// Walk thru xml set and copy each definitions.
		Iterator i = xmlDefinitions.getDefinitions().values().iterator();
		while ( i.hasNext() ) {
			XmlDefinition xmlDefinition = (XmlDefinition) i.next();
			putDefinition(new HeritableComponentDefinition(xmlDefinition));
		}  // end loop

	}

    /**
     * Gets a mapping from definition names to definitions that can be
     * manufactured by this factory
     *
     * @return mapping from definition names to definitions
     */
	public Map getDefinitions() {
		return definitions;
	}

}
