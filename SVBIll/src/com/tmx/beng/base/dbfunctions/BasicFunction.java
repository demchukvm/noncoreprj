package com.tmx.beng.base.dbfunctions;

import com.tmx.beng.base.Task;
import com.tmx.beng.base.cache.InvokeFunctionException;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**

 */
abstract public class BasicFunction {
    
    protected Task task;

    /**Scale for round calculation**/
    public static final int ROUND_SCALE = 3;

    public void init(Task task){
        this.task = task;
    }

    abstract void invoke(Map actualParams) throws InvokeFunctionException;

    public ValidationError[] validate(Map actualParams){return null;}

    public TariffParameter[] getDefaultParams(){return null;}
    
    public static class TariffParameter {
    	private String name;
    	private String value;
        private Long orderNum;
        private Boolean mandatory;

        public TariffParameter() {
        }

        public TariffParameter(String name, String value, Long orderNum, Boolean mandatory) {
            this.name = name;
            this.value = value;
            this.orderNum = orderNum;
            this.mandatory = mandatory;
        }

        public TariffParameter(String name, String value, Boolean mandatory) {
            this.name = name;
            this.value = value;
            this.mandatory = mandatory;
        }

        public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		public String getValue() {
			return value;
		}
		public void setValue(String value) {
			this.value = value;
		}

        public Long getOrderNum() {
            return orderNum;
        }

        public void setOrderNum(Long orderNum) {
            this.orderNum = orderNum;
        }

        public Boolean getMandatory() {
            return mandatory;
        }

        public void setMandatory(Boolean mandatory) {
            this.mandatory = mandatory;
        }
    }

}
