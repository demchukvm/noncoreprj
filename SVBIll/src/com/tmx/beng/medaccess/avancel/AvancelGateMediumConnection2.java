package com.tmx.beng.medaccess.avancel;

import com.tmx.beng.httpsgate.HttpsGateException;
import com.tmx.beng.httpsgate.avancel.AvancelGate2;
import com.tmx.beng.base.BillingMessage;
import com.tmx.beng.medaccess.MediumConnection;

/**
 */
public class AvancelGateMediumConnection2 implements MediumConnection {
    private AvancelGate2 avancelGate = null;

    AvancelGateMediumConnection2() throws HttpsGateException {
        avancelGate = AvancelGate2.getInstance();
    }

    public void send(BillingMessage msg) {
        avancelGate.enqueueBillMessage(msg);
    }
}
