package com.tmx.beng.iso8583Gate.mapper;

import java.util.Map;
import java.util.HashMap;

/**
    Extended mapping
 */
public class ExtMapping {
    /** by protocolVersion */
    private Map crossParamMappings;

    public ExtMapping() {
        crossParamMappings = new HashMap();
    }

    public void addCrossParamMapping(CrossParameterMapping evm) {
        crossParamMappings.put(evm.getProtocolVersion(), evm);
    }

    public CrossParameterMapping getCrossParamMapping(String protocolVersion) {
        return (CrossParameterMapping) crossParamMappings.get(protocolVersion);
    }
}
