package com.tmx.web.controls;

import com.tmx.util.StringUtil;
import com.tmx.web.base.BasicActionForm;

import java.util.*;
import java.lang.reflect.Method;

/**
 * Container to manage controls.
 */
public class ControlContainer {
    /** control owner this is the object that contains controls
     * as fields. And this contianer is used by this owner to
     * manage controls */
    private Object controlOwner = null;
    //----------Properrties to manage owner's controls
    /** List of enclosed controls */
    private List controls = new ArrayList();
    /** Map of controls names [BasicControl ctrl][String name].
     * The name of control is the same as it defined in owner form */
//    private Map controlsNames = new HashMap();

    /** Default constructor */
    public ControlContainer(Object controlOwner){
        if(controlOwner == null)
            throw new NullPointerException("Given controlOwner is null");
        this.controlOwner = controlOwner;
    }

    /** initialize container */
    public void init(BasicActionForm owner, BasicControl parent){
        //initialize container
        registerControls(owner, parent);
    }

    /** Returns form's owned controls. They were previously
     * registered by init() method. */
    public List getOwnedControlsByClass(Class controlsClass){
        List selectedControls = new ArrayList();
        if(controls == null){
            return selectedControls;
        }
        else{
            for(Iterator iter = controls.iterator(); iter.hasNext();){
                BasicControl ctrl = (BasicControl)iter.next();
                if(ctrl.getClass().equals(controlsClass)){
                    selectedControls.add(ctrl);
                }
                if(ctrl.isContainer()){
                    selectedControls.addAll(ctrl.getContainer().getOwnedControlsByClass(controlsClass));
                }
            }
        }
        return selectedControls;
    }

    /** Returns all child controls that refer this container and its childs recursively as parent */
    public List getAllChildControls(){
        List allControls = new ArrayList();
        for(Iterator iter = controls.iterator(); iter.hasNext();){
            BasicControl control = (BasicControl)iter.next();
            allControls.add(control);
            if(control.isContainer())
                allControls.addAll(control.getContainer().getAllChildControls());
        }
        return allControls;
    }

    /** Returns child controls that refer this container as parent */
    public List getDirectlyChildControls(){
        return controls;
    }

    /** Traverse the owner getters and builds
     * map [BasicControl ctrl][String prtopertyName]. */
    private void registerControls(BasicActionForm owner, BasicControl parent){
        Method methods[] = controlOwner.getClass().getMethods();

        for (Method method : methods) {
            //look for property getters to obtain BasicControls
            if (method.getName().startsWith("get") && method.getParameterTypes().length==0) {
                try {
                    Object obj = method.invoke(controlOwner);
                    if (obj != null && obj instanceof BasicControl) {

                        //ignore parent
                        if (controlOwner instanceof BasicControl && ((BasicControl) controlOwner).getParent() == obj)
                            continue;

                        controls.add(obj);
                        String name = StringUtil.getFieldName(method.getName());
                        ((BasicControl) obj).init(name, owner, parent);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

}