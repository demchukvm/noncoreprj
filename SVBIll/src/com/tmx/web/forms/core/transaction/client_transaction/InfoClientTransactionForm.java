package com.tmx.web.forms.core.transaction.client_transaction;

import com.tmx.as.entities.bill.operator.Operator;
import com.tmx.as.entities.bill.seller.Seller;
import com.tmx.as.entities.bill.terminal.Terminal;
import com.tmx.as.entities.bill.transaction.ClientTransaction;

import com.tmx.web.controls.EditBox;
import com.tmx.web.controls.TimeSelector;
import com.tmx.web.controls.TextArea;
import com.tmx.web.forms.core.transaction.AbstractInfoTransactionForm;

import java.util.List;


public class InfoClientTransactionForm extends AbstractInfoTransactionForm {

    private TextArea textAreaOperatorName;
    private TextArea textAreaOperatorBalanceName;
    private String selectedBillTransactionNum;

    //seller
    private TextArea textAreaSellerName;
    private TextArea textAreaSellerBalanceName;

    //terminal
    private EditBox editBoxTerminalSN;// terminal serial number
    private TimeSelector timeSelectorTermRegDate;//terminal registartion date

    private EditBox editBoxVoucherCode,
                    editBoxVoucherId,
                    editBoxVoucherSecretCode,
                    editBoxVoucherNominal,
                    editBoxVoucherStatus,
                    editBoxVoucherSoldTime;

    protected void init() {
        //initialze and register controls
        super.init();

        textAreaOperatorName = new TextArea();
        textAreaOperatorName.setReadonly(true);

        textAreaOperatorBalanceName = new TextArea();
        textAreaOperatorBalanceName.setReadonly(true);

        textAreaSellerName = new TextArea();
        textAreaSellerName.setReadonly(true);

        textAreaSellerBalanceName = new TextArea();
        textAreaSellerBalanceName.setReadonly(true);

        editBoxTerminalSN = new EditBox();
        editBoxTerminalSN.setReadonly(true);

        timeSelectorTermRegDate = new TimeSelector();
        timeSelectorTermRegDate.setReadonly(true);

        initEditBoxes();

        onCancelButton.setAction("/core/transactions/client_transaction/manageClientTransactions");
    }

    private void initEditBoxes() {
        editBoxVoucherCode = new EditBox();
        editBoxVoucherCode.setReadonly(true);

        editBoxVoucherId = new EditBox();
        editBoxVoucherId.setReadonly(true);

        editBoxVoucherSecretCode = new EditBox();
        editBoxVoucherSecretCode.setReadonly(true);

        editBoxVoucherNominal = new EditBox();
        editBoxVoucherNominal.setReadonly(true);

        editBoxVoucherStatus = new EditBox();
        editBoxVoucherStatus.setReadonly(true);

        editBoxVoucherSoldTime = new EditBox();
        editBoxVoucherSoldTime.setReadonly(true);
    }


    public void bindData(ClientTransaction clientTransaction) {
        super.bindData(clientTransaction);

        //for operator
        Operator operator = clientTransaction.getOperator();
        if(operator != null){
            textAreaOperatorName.setValue(operator.getName());
            if(operator.getPrimaryBalance() != null)
                textAreaOperatorBalanceName.setValue(operator.getPrimaryBalance().getName());
        }

        //for seller
        Seller seller = clientTransaction.getSeller();
        if(seller != null){
            textAreaSellerName.setValue(seller.getName());
            if(seller.getPrimaryBalance() != null)
                textAreaSellerBalanceName.setValue(seller.getPrimaryBalance().getName());
        }

        //for terminal
        Terminal terminal = clientTransaction.getTerminal();
        if(terminal != null){
            editBoxTerminalSN.setValue(terminal.getSerialNumber());
            timeSelectorTermRegDate.setTime(terminal.getRegistrationDate());
        }

        //for native
        editBoxAccountNumber.setValue(clientTransaction.getAccountNumber());
    }

    public void bindVoucherData(Object[] voucherData) {
        if (voucherData[0] != null) {
            editBoxVoucherId.setValue(voucherData[0].toString());
        }
        if (voucherData[1] != null) {
            editBoxVoucherCode.setValue(voucherData[1].toString());
        }
        if (voucherData[2] != null) {
            editBoxVoucherSecretCode.setValue(voucherData[2].toString());
        }
        if (voucherData[4] != null) {
            editBoxVoucherNominal.setValue(voucherData[4].toString());
        }
        if (voucherData[7] != null) {
            editBoxVoucherStatus.setValue(voucherData[7].toString());
        }
        if (voucherData[10] != null) {
            editBoxVoucherSoldTime.setValue(voucherData[10].toString());
        }
    }

    public TextArea getTextAreaOperatorName() {
        return textAreaOperatorName;
    }

    public void setTextAreaOperatorName(TextArea textAreaOperatorName) {
        this.textAreaOperatorName = textAreaOperatorName;
    }

    public TextArea getTextAreaOperatorBalanceName() {
        return textAreaOperatorBalanceName;
    }

    public void setTextAreaOperatorBalanceName(TextArea textAreaOperatorBalanceName) {
        this.textAreaOperatorBalanceName = textAreaOperatorBalanceName;
    }

    public TextArea getTextAreaSellerName() {
        return textAreaSellerName;
    }

    public void setTextAreaSellerName(TextArea textAreaSellerName) {
        this.textAreaSellerName = textAreaSellerName;
    }

    public TextArea getTextAreaSellerBalanceName() {
        return textAreaSellerBalanceName;
    }

    public void setTextAreaSellerBalanceName(TextArea textAreaSellerBalanceName) {
        this.textAreaSellerBalanceName = textAreaSellerBalanceName;
    }

    public EditBox getEditBoxTerminalSN() {
        return editBoxTerminalSN;
    }

    public void setEditBoxTerminalSN(EditBox editBoxTerminalSN) {
        this.editBoxTerminalSN = editBoxTerminalSN;
    }

    public TimeSelector getTimeSelectorTermRegDate() {
        return timeSelectorTermRegDate;
    }

    public void setTimeSelectorTermRegDate(TimeSelector timeSelectorTermRegDate) {
        this.timeSelectorTermRegDate = timeSelectorTermRegDate;
    }

    public String getSelectedBillTransactionNum() {
        return selectedBillTransactionNum;
    }

    public void setSelectedBillTransactionNum(String selectedBillTransactionNum) {
        this.selectedBillTransactionNum = selectedBillTransactionNum;
    }

    public EditBox getEditBoxVoucherCode() {
        return editBoxVoucherCode;
    }

    public void setEditBoxVoucherCode(EditBox editBoxVoucherCode) {
        this.editBoxVoucherCode = editBoxVoucherCode;
    }

    public EditBox getEditBoxVoucherId() {
        return editBoxVoucherId;
    }

    public void setEditBoxVoucherId(EditBox editBoxVoucherId) {
        this.editBoxVoucherId = editBoxVoucherId;
    }

    public EditBox getEditBoxVoucherSecretCode() {
        return editBoxVoucherSecretCode;
    }

    public void setEditBoxVoucherSecretCode(EditBox editBoxVoucherSecretCode) {
        this.editBoxVoucherSecretCode = editBoxVoucherSecretCode;
    }

    public EditBox getEditBoxVoucherNominal() {
        return editBoxVoucherNominal;
    }

    public void setEditBoxVoucherNominal(EditBox editBoxVoucherNominal) {
        this.editBoxVoucherNominal = editBoxVoucherNominal;
    }

    public EditBox getEditBoxVoucherStatus() {
        return editBoxVoucherStatus;
    }

    public void setEditBoxVoucherStatus(EditBox editBoxVoucherStatus) {
        this.editBoxVoucherStatus = editBoxVoucherStatus;
    }

    public EditBox getEditBoxVoucherSoldTime() {
        return editBoxVoucherSoldTime;
    }

    public void setEditBoxVoucherSoldTime(EditBox editBoxVoucherSoldTime) {
        this.editBoxVoucherSoldTime = editBoxVoucherSoldTime;
    }

}
