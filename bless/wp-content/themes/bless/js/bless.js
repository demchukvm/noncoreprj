$(document).ready(function () {

    //var url = 'http://bless.silencatech.com/wp-content/themes/bless/js/ajax.php';
    var url = 'http://bless.ua/wp-content/themes/bless/js/ajax.php';
    $('#send_form_button').click(function () {


        var form_name = $('#form_name').val()?$('#form_name').val():'';
        var form_mail = $('#form_mail').val()?$('#form_mail').val():'';
        var form_phone = $('#form_phone').val()?$('#form_phone').val():'';
        var form_message = $('#form_message').val()?$('#form_message').val():'';
        var form_city = $('#partner_city').val()?$('#partner_city').val():'';
        var form_site = $('#form_website').val()?$('#form_website').val():'';
        var form_org = $('#form_org').val()?$('#form_org').val():'';
		var form_type = $('#form_type').val()?$('#form_type').val():'';

        if (form_name == '') {
            $('.name').css('border-color', 'red');
        }

        if (form_mail == '') {
            $('.mail').css('border-color', 'red');
        }

        if (form_phone == '') {
            $('.phone').css('border-color', 'red');
        }

        if (form_message == '') {
            $('.message').css('border-color', 'red');
        }

        if (form_name !== '' & form_mail !== '' & form_phone !== '' & form_message !== '') {
            $.ajax({
                type:"POST",
                data:"action=feedback&name=" + form_name + "&mail= " + form_mail + "&phone=" + form_phone + "&city=" + form_city + "&site=" + form_site + "&org=" + form_org + "&type=" + form_type + "&message=" + form_message,
                url:url,
                success:function (data) {
                    $('.feedback_block h2').html(data);
                    $('input').attr('value', '');
                    $('#form_message').attr('value', '');
                }
            });
        }

    });


    $('#form_message').click(function () {
        $('.message').css('border-color', '#04A9E1');
    });

    $('.name').click(function () {
        $('.name').css('border-color', '#04A9E1');
    });

    $('.mail').click(function () {
        $('.mail').css('border-color', '#04A9E1');
    });

    $('.phone').click(function () {
        $('.phone').css('border-color', '#04A9E1');
    });

    $('.city').click(function () {
        $('.city').css('border-color', '#04A9E1');
    });

    $('a.select').click(function () {

        var open = $('#hiddencity').css('display');

        if (open == 'none') {
            $('#hiddencity').css('display', 'block');
            $('.city span').html('');
        }
        if (open == 'block') {
            $('#hiddencity').css('display', 'none');
        }


    })


    $('.hiddencity').click(function () {
        var city = $(this).html();
        $('#hiddencity').css('display', 'none');
        $('.city span').html(city);
    });

    $('#searchservice').click(function () {
        var city = $('.city span').html();

//        if(city == ''){
//            alert('Выберите город');
//        }


        $('.table_parm table tbody tr').each(function () {
            var html = $(this).html();
            $(this).removeAttr("style");


            if (html.indexOf(city) == -1 & html.indexOf("Адрес") == -1) {
                $(this).css('display', 'none');
            }
        });

        if (city == 'Все города') {
            $('.table_parm table tbody tr').each(function () {
                $(this).removeAttr("style");
                $('.extratr').remove();
            });
        }


        var i = 0;

        $('.table_parm table tbody tr').each(function () {
            if ($(this).css('display') !== 'none') {
                i++;
            }


        });

        if (i == 1) {
            $('table tbody').append('<tr class="extratr" style="background-color:#DEF2F4;"><td colspan="4">Ничего не найлено</td></tr>')
        }


    })

    // Alexandr Oleynik
    $('body').click(function () {
        $('#hiddencity').hide(400);
    });

    $('a.select').click(function (event) {
        event.stopPropagation();
    });


// Alexandr Oleynik

    $('a.buy').click(function () {
        $('.hid_form').css('display', 'none');
        $('a.buy').css('display', 'block');
        $(this).css('display', 'none');
        $(this).next().css('display', 'block');
    });
    /**/



    $('.buyphone').click(function () {

        var form_name = $(this)./*prev()*/parent('form').find('.name input').val();
        var form_city = $(this)./*prev()*/parent('form').find('.city input').val();
        var form_email = $(this)./*prev()*/parent('form').find('.mail input').val();
        var form_phone = $(this)./*prev()*/parent('form').find('.phone input').val();
		var form_model = $(this)./*prev()*/parent('form').find('input[name=\'phone_model\']').val();

        var product_id = $(this).attr('id');

        if (form_name == '') {
            $(this).prev().find('.name').css('border-color', 'red');
        }

        if (form_city == '') {
            $(this).prev().find('.city').css('border-color', 'red');
        }

        if (form_email == '') {
            $(this).prev().find('.mail').css('border-color', 'red');
        }
        if (form_phone == '') {
            $(this).prev().find('.phone').css('border-color', 'red');
        }


        if (form_name !== '' & form_city !== '' & form_email !== '' & form_phone !== '') {
            $.ajax({
                type:"POST",
                data:"action=order&name=" + form_name + "&city= " + form_city + "&mail=" + form_email + "&phone=" + form_phone + "&model=" + form_model,
                url:url,
                success:function (data) {
					var res = $.parseJSON(data);
					if(res.result == "OK") {
						$('#product' + product_id).find('.info a.buy').hide();
						$('#product' + product_id).find('.info .buyinfo').css('display','block').html('Спасибо за заказ!');
						
						$('.hid_form').css('display','none');
						$('a.buy').css('display','block');
					} else {
						$('.hid_form').css('display','block');
						$('a.buy').css('display','none');
						alert(res.description);
					}
                }
            });

            

//            setTimeout(function(){
//                location.reload();
//            }, 3000);
        }

    });


});

