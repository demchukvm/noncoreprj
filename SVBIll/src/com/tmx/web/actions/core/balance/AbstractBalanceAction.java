package com.tmx.web.actions.core.balance;

import com.tmx.as.blogic.balance.AbstractBalanceManager;

import com.tmx.web.actions.core.AbstractSvbillAction;
import com.tmx.util.InitException;
import com.tmx.beng.base.BillException;

/**
 * ------Fuctory method design pattern with Template method design pattern------------
 * In this class we throw fuctory method between sub actions and sub balance managers
 * @see com.tmx.as.blogic.balance.AbstractBalanceManager
 */
abstract public class AbstractBalanceAction extends AbstractSvbillAction {

    /**
     * sub non abstract actions must override this method for taking target manager
     * @return target balance manager
     */
    abstract protected AbstractBalanceManager getBalanceManager() throws InitException, BillException;

}
