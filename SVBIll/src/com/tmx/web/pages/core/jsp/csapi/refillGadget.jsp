<%@ page import="com.tmx.web.forms.core.csapi.RefillGadgetForm" %>
<%@ taglib prefix="nested" uri="http://jakarta.apache.org/struts/tags-nested" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/tld/tmx-controls.tld" prefix="tmx_ctrl" %>

<html:form action="/RefillGadgetAction">

<table border="0">
    <tr>

		<td style="border:black 1px dotted;" valign="top">
			<table style="background-color:white;">
				<tr><td style="font-size:9px;">0</td><td style="font-size:9px;">STATUS_OK</td></tr>
				<tr><td style="font-size:9px;">1</td><td style="font-size:9px;">TRANSACTION_OPENED</td></tr>
				<tr><td style="font-size:9px;">2</td><td style="font-size:9px;">SYNC_BILL_WAIT_INTERRUPED</td></tr>
				<tr><td style="font-size:9px;">3</td><td style="font-size:9px;">BILLING_RETURNS_NULL_MESSAGE</td></tr>
				<tr><td style="font-size:9px;">4</td><td style="font-size:9px;">JOINED_REQUEST_NOT_FOUND</td></tr>
				<tr><td style="font-size:9px;">5</td><td style="font-size:9px;">WAIT_FOR_BILLING_TIME_OUT</td></tr>
				<tr><td style="font-size:9px;">6</td><td style="font-size:9px;">AUTHENTICATION_ERROR</td></tr>
				<tr><td style="font-size:9px;">7</td><td style="font-size:9px;">UNCKNOWN_COMMAND</td></tr>
				<tr><td style="font-size:9px;">8</td><td style="font-size:9px;">OPEN_TRANSACTION_ERROR</td></tr>
				<tr><td style="font-size:9px;">9</td><td style="font-size:9px;">MEDIUM_CONNECT_ERROR</td></tr>
				<tr><td style="font-size:9px;">10</td><td style="font-size:9px;">LIMITS_ERROR</td></tr>
				<tr><td style="font-size:9px;">11</td><td style="font-size:9px;">TARIFF_ERROR</td></tr>
				<tr><td style="font-size:9px;">12</td><td style="font-size:9px;">RESERVATION_ERROR</td></tr>
				<tr><td style="font-size:9px;">13</td><td style="font-size:9px;">COPY_CALLBACK_ERROR</td></tr>
				<tr><td style="font-size:9px;">14</td><td style="font-size:9px;">CANCEL_RESERVATION_ERROR</td></tr>
				<tr><td style="font-size:9px;">15</td><td style="font-size:9px;">SAVE_TO_DB_ERROR</td></tr>
				<tr><td style="font-size:9px;">16</td><td style="font-size:9px;">PAYMENT_PROPS_CHECK_ERROR</td></tr>
				<tr><td style="font-size:9px;">17</td><td style="font-size:9px;">UNKNOWN_STATUS</td></tr>
				<tr><td style="font-size:9px;">18</td><td style="font-size:9px;">IDENTIFY_STATUS_ERROR</td></tr>
				<tr><td style="font-size:9px;">19</td><td style="font-size:9px;">VALIDATE_CLIENT_TRANSACTION_ERROR</td></tr>
				<tr><td style="font-size:9px;">20</td><td style="font-size:9px;">TIME_RECONCIL_ERROR</td></tr>
				<tr><td style="font-size:9px;">21</td><td style="font-size:9px;">TERMINAL_AUTOREGISTRATION_ERROR</td></tr>
				<tr><td style="font-size:9px;">22</td><td style="font-size:9px;">FIND_TRANSACTION_ERROR</td></tr>
				<tr><td style="font-size:9px;">23</td><td style="font-size:9px;">NO_VOUCHER_FOUND</td></tr>
				<tr><td style="font-size:9px;">50</td><td style="font-size:9px;">TRANSACTION_CANCELED</td></tr>
				<tr><td style="font-size:9px;">51</td><td style="font-size:9px;">TEST_ERROR</td></tr>
				<tr><td style="font-size:9px;">52</td><td style="font-size:9px;">ACCOUNT_TIMEOUT</td></tr>
				<tr><td style="font-size:9px;">200</td><td style="font-size:9px;">UNKNOWN_ERROR</td></tr>
				<tr><td style="font-size:9px;">201</td><td style="font-size:9px;">BEGIN_TRANSACTION_ERROR</td></tr>
				<tr><td style="font-size:9px;">202</td><td style="font-size:9px;">QUERY_PAYMENT_ERROR</td></tr>
				<tr><td style="font-size:9px;">203</td><td style="font-size:9px;">SUBMIT_TRANSACTION_ERROR</td></tr>
				<tr><td style="font-size:9px;">204</td><td style="font-size:9px;">UNSUPPORTED_COMMAND</td></tr>
				<tr><td style="font-size:9px;">205</td><td style="font-size:9px;">TRANSACTION_NOT_FOUND</td></tr>
				<tr><td style="font-size:9px;">206</td><td style="font-size:9px;">REGION_NOT_FOUND</td></tr>
				<tr><td style="font-size:9px;">250</td><td style="font-size:9px;">CANCEL_TRANSACTION_ERROR</td></tr>
				<tr><td style="font-size:9px;">900</td><td style="font-size:9px;">INCORRECT_AMOUNT</td></tr>
			</table>
		</td>

		<td width="5px"></td>

        <td valign="top">




			<table border="0" style="border:black 1px dotted;background-color:white;">
                <tr>
                    <td><b>Product:</b></td>
                    <td><tmx_ctrl:selectBox name="RefillGadgetForm" property="sbProducts" /></td>
                    <td><b>TradePoint:</b></td>
                    <td><tmx_ctrl:selectBox name="RefillGadgetForm" property="sbTerminals" /></td>
                </tr>
				<tr>
                    <td><b>Account:</b></td>
                    <td><tmx_ctrl:editBox name="RefillGadgetForm" property="ebAccount" /></td>
                    <td><b>Amount:</b></td>
                    <td><tmx_ctrl:editBox name="RefillGadgetForm" property="ebAmount" /></td>
                </tr>
                <tr>
				<td colspan="2" align="right"></td>
                    <td colspan="2" align="right">
                        <br>
                        <tmx_ctrl:button property="bRefill" /><%--<tmx_ctrl:button property="bAllThisMonth" />--%>
                    </td>
                </tr>
            </table>
                       

			<br>
			<b>Response:</b>&nbsp;&nbsp;&nbsp;<br>
			<span style="background-color:white;">
			<%= ((RefillGadgetForm) pageContext.getSession().getAttribute("RefillGadgetForm")).getMessage() %>
			<%= ((RefillGadgetForm) pageContext.getSession().getAttribute("RefillGadgetForm")).getResponseXML() %>
			</span>
        </td>

		<td width="5px"></td>


		<td valign="top" style="border:black 0px dotted;">

			<div style="float:left;text-align:left;">
				<b>Refill Story:</b><br>
				<div style="border:black 1px dotted;background-color:black;color:red;">
					<b>
					REQUEST : <font color="white">product + refill_type + account + amount + trade_point</font><br>
					RESPONSE : <font color="white">status??????support_message??????date</font>
					</b>
				</div>
				<%= ((RefillGadgetForm) pageContext.getSession().getAttribute("RefillGadgetForm")).getRefillStory() %>
			</div>
		</td>
    </tr>
</table>


</html:form>



