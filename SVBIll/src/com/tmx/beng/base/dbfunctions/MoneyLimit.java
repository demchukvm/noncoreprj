package com.tmx.beng.base.dbfunctions;

import com.tmx.beng.base.BillingMessage;
import com.tmx.beng.base.BillingMessageWritable;
import com.tmx.beng.base.cache.InvokeFunctionException;

import java.util.Map;

/**

 */
public class MoneyLimit extends BasicLimitFunction {
    public static final String MAX_AMOUNT = "max_amount";
    public static final String MIN_LIMIT = "min_limit";

    public void checkLimit(Map actualParams) throws InvokeFunctionException {
        try{
            BillingMessageWritable billingMessage = task.getProcesedMessage();
            // check max amount limit
            Double amount = new Double(billingMessage.getAttributeString(BillingMessage.AMOUNT));
            Double max_amount = new Double((String)actualParams.get(MAX_AMOUNT));
            if(max_amount.compareTo(amount) < 0){
                throw new InvokeFunctionException("err.bill.dbfunctions.max_terminal_amount_exceed");
            }

            // check min terminal balance
            Double min_limit = new Double((String)actualParams.get(MIN_LIMIT));
            Double terminal_amount = task.getSessionDBCache().getTerminalPrimaryBalanceAmount(
                    billingMessage.getAttributeString(BillingMessage.TERMINAL_SN));
            if(((terminal_amount.doubleValue() - amount.doubleValue()) < 0) ||
                    (min_limit.doubleValue()  > (terminal_amount.doubleValue() - amount.doubleValue()))){
                throw new InvokeFunctionException("err.bill.dbfunctions.min_terminal_limit_exceed");
            }

        }catch(Throwable e){
            throw new InvokeFunctionException(e.getLocalizedMessage(), e);
        }
    }
}
