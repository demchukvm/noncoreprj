package com.tmx.as.entities.general.ui_permission;

import com.tmx.as.base.BasicEntity;
import java.io.Serializable;

/**
 */
public class PermissionType extends BasicEntity {
    /** List of supported permisson types  */
    public static String PERMITTED = "PERMITTED";
    public static String FORBIDDEN = "FORBIDDEN";
    public static String READONLY = "READONLY";
    private Long permissionTypeId;
    private String name;
    private String shortName;

    public Long getPermissionTypeId() {
        return permissionTypeId;
    }

    public void setPermissionTypeId(Long permissionTypeId) {
        this.permissionTypeId = permissionTypeId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public Serializable getId() {
        return getPermissionTypeId();
    }

    public int getValid() {
        return valid;
    }

    public void setValid(int valid) {
        this.valid = valid;
    }

}
