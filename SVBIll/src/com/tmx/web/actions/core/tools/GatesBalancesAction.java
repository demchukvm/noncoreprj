package com.tmx.web.actions.core.tools;

import com.tmx.beng.httpsgate.avancel.AvancelGate1;
import com.tmx.beng.httpsgate.citypay.CitypayGateForLifeExc;
import com.tmx.beng.httpsgate.cyberplat.CyberplatGate;
import com.tmx.beng.httpsgate.kyivstar.KyivstarBonusNoCommissionGate;
import com.tmx.beng.httpsgate.kyivstar.KyivstarExclusiveGate;
import com.tmx.beng.httpsgate.kyivstar.KyivstarGate;
import com.tmx.beng.httpsgate.umc.UmcGate;
import com.tmx.web.forms.core.tools.GatesBalancesForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForm;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.tmx.web.base.BasicDispatchedAction;
import com.tmx.beng.httpsgate.citypay.CitypayGate;



public class GatesBalancesAction extends BasicDispatchedAction
{

    private final String FORWARD = "gatesBalances";

    private String smile;
    private String value;

    protected ActionForward defaultAction(ActionMapping mapping,
                                          ActionForm form,
                                          HttpServletRequest request,
                                          HttpServletResponse response,
                                          String methodName) throws Throwable {

        return refreshBalance(mapping, form, request, response);
    }

    public ActionForward refreshBalance(ActionMapping mapping,
                                        ActionForm form,
                                        HttpServletRequest request,
                                        HttpServletResponse response) throws Throwable {

        GatesBalancesForm gatesBalancesForm = (GatesBalancesForm) form;

        // Avancel
        try{
            value = AvancelGate1.getInstance().getBalance();
            formatBalance(value);
            changeSmile();
        }catch(Throwable e){
            value = "gate error";
            smile = "/core/images/common/smiles/smile_5.gif";
        }
        gatesBalancesForm.getEditBoxAvancelBalance().setValue(value);
        gatesBalancesForm.setAvancelSmile(smile);

        // CityPay
        try {
            value = CitypayGate.getInstance().getBalance().toString();
            formatBalance(value);
            changeSmile();
        } catch (Throwable e) {
            value = "gate error";
            smile = "/core/images/common/smiles/smile_5.gif";
        }
        gatesBalancesForm.getEditBoxCitypayBalance().setValue(value);
        gatesBalancesForm.setCitypaySmile(smile);

        // CityPay (life)
        try {
            value = CitypayGateForLifeExc.getInstance().getBalance().toString();
            formatBalance(value);
            changeSmile();
        } catch (Throwable e) {
            value = "gate error";
            smile = "/core/images/common/smiles/smile_5.gif";
        }
        gatesBalancesForm.getEditBoxCitypayForLifeBalance().setValue(value);
        gatesBalancesForm.setCitypayForLifeSmile(smile);

        // CyberPlat // есть в протоколе, но на деле не реализовано
        /*
        try {
            value = CyberplatGate.getInstance().getBalance().toString();
            formatBalance(value);
            changeSmile();
        } catch (Throwable e) {
            value = "gate error";
            smile = "/core/images/common/smiles/smile_5.gif";
        }
        gatesBalancesForm.getEditBoxCyberplatBalance().setValue(value);
        gatesBalancesForm.setCyberplatSmile(smile);
        */

        // KS
        try{
            value = KyivstarGate.getInstance().getBalance("KyivstarPoP").toString();
            formatBalance(value);
            changeSmile();
        }catch(Throwable e){
            value = "gate error";
            smile = "/core/images/common/smiles/smile_5.gif";
        }
        gatesBalancesForm.getEditBoxKSBalance().setValue(value);
        gatesBalancesForm.setKsSmile(smile);

        // KS BNC
        try{
            value = KyivstarBonusNoCommissionGate.getInstance().getBalance("KyivstarPoP").toString();
            formatBalance(value);
            changeSmile();
        }catch(Throwable e){
            value = "gate error";
            smile = "/core/images/common/smiles/smile_5.gif";
        }
        gatesBalancesForm.getEditBoxKSBNCBalance().setValue(value);
        gatesBalancesForm.setKsbncSmile(smile);

        // KS FC
        try{
            value = KyivstarExclusiveGate.getInstance().getBalance("KyivstarPoP").toString();
            formatBalance(value);
            changeSmile();
        }catch(Throwable e){
            value = "gate error";
            smile = "/core/images/common/smiles/smile_5.gif";
        }
        gatesBalancesForm.getEditBoxKSFCBalance().setValue(value);
        gatesBalancesForm.setKsfcSmile(smile);

        // MTS
        try {
            value = UmcGate.getInstance().getBalance("UMCPoP").toString();
            formatBalance(value);
            changeSmile();
        } catch (Throwable e) {
            value = "gate error";
            smile = "/core/images/common/smiles/smile_5.gif";
        }
        gatesBalancesForm.getEditBoxMTSBalance().setValue(value);
        gatesBalancesForm.setMtsSmile(smile);

        return mapping.findForward(FORWARD);
    }

    private void formatBalance(String balance)
    {
        if(balance.substring(balance.indexOf(".")).length() < 3) balance += "0";
		if(balance.substring(balance.indexOf(".")).length() > 3) balance = balance.substring(0, balance.indexOf(".")+3);
        value = balance;
    }

    private void changeSmile()
    {
        Float f = Float.parseFloat(value);
        if(f<=1000) {smile="/core/images/common/smiles/smile_1.gif";}
        else if(f>1000 && f<=5000) {smile="/core/images/common/smiles/smile_2.gif";}
        else if(f>5000 && f<=100000) {smile="/core/images/common/smiles/smile_3.gif";}
        else if(f>100000) {smile="/core/images/common/smiles/smile_4.gif";}
    }

}
