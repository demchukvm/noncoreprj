package com.tmx.web.taglib.html.ctrl;

import com.tmx.web.controls.MultipleSelectBox;

import javax.servlet.jsp.JspException;

import org.apache.struts.taglib.TagUtils;

/**
 * Render ScrollBox control as HTML select element
 */
public class MultipleSelectBoxTag extends AbstractControlTag {
    //CSS styles properties
    private String selectBoxStyleClass = null;
    private String readonlySelectBoxStyleClass = null;

    //------------------------------Properties

    public String getSelectBoxStyleClass() {
        return (selectBoxStyleClass != null) ? selectBoxStyleClass : getDefaultProperty("tag.selectBox.style.selectBoxStyleClass");
    }

    public void setSelectBoxStyleClass(String selectBoxStyleClass) {
        this.selectBoxStyleClass = selectBoxStyleClass;
    }

    public String getReadonlySelectBoxStyleClass() {
        return (readonlySelectBoxStyleClass != null) ? readonlySelectBoxStyleClass : getDefaultProperty("tag.selectBox.style.readonlySelectBoxStyleClass");
    }

    public void setReadonlySelectBoxStyleClass(String readonlySelectBoxStyleClass) {
        this.readonlySelectBoxStyleClass = readonlySelectBoxStyleClass;
    }

    //------------------------------Abstract methods
     protected int controlStartTag() throws JspException {
        StringBuffer resultHTML = new StringBuffer();
        MultipleSelectBox sb = getMultipleSelectBox();
        if(sb == null){
            TagUtils.getInstance().write(pageContext, "MultipleSelectBox '"+getProperty()+"' is not found");
            return SKIP_BODY;
        }

        if(!sb.isBounded()){
            TagUtils.getInstance().write(pageContext, "Data is not bound to MultipleSelectBox '"+getProperty()+"'");
            return SKIP_BODY;
        }

        //Render HTML
        resultHTML.append("\r");
        String name = " name=\""+getProperty() +"."+MultipleSelectBox.SELECTEDKEY_SUFFIX + "\" ";
        String styleClass = " class=\"" + (sb.isReadonly() ? getReadonlySelectBoxStyleClass() : getSelectBoxStyleClass()) + "\" ";
        String readonly = (sb.isReadonly()) ? " disabled" : "";

        StringBuffer style = new StringBuffer(" style=\"{");
        if(getWidth() != null)
            style.append(" width: "+getWidth()+"; ");
        style.append("}\"");


        resultHTML.append("<select multiple "+ name + styleClass + style + readonly + getOnEventAttrsString() + ">\r");
//        if(sb.isDisableItem())resultHTML.append("<option value=\""+MultipleSelectBox.DISABLE_ITEM_KEY+"\" "+(!sb.isSelected()&&sb.isDisableItemSelected()?"selected":"")+">"+getLocalizedLabel("tag.MultipleSelectBox.disable",null)+"</option>");
//        if(sb.isAllItem())resultHTML.append("<option value=\""+MultipleSelectBox.ALL_ITEM_KEY+"\" "+(!sb.isSelected()&&sb.isAllItemSelected()?"selected":"")+">"+getLocalizedLabel("tag.MultipleSelectBox.all",null)+"</option>");
//        if(sb.isNoneItem())resultHTML.append("<option value=\""+MultipleSelectBox.NONE_ITEM_KEY+"\" "+(!sb.isSelected()&&sb.isNoneItemSelected()?"selected":"")+">"+getLocalizedLabel("tag.MultipleSelectBox.none",null)+"</option>");
        if(sb.isDisableItem())resultHTML.append("<option value=\""+MultipleSelectBox.DISABLE_ITEM_KEY+"\" >"+getLocalizedLabel("tag.MultipleSelectBox.disable",null)+"</option>");
        if(sb.isAllItem())resultHTML.append("<option value=\""+MultipleSelectBox.ALL_ITEM_KEY+"\" >"+getLocalizedLabel("tag.MultipleSelectBox.all",null)+"</option>");
        if(sb.isNoneItem())resultHTML.append("<option value=\""+MultipleSelectBox.NONE_ITEM_KEY+"\" >"+getLocalizedLabel("tag.MultipleSelectBox.none",null)+"</option>");
        for(int i=0;i<sb.getSize();i++){
            String attrSelectedHTML = sb.isSelected(i)?"selected" : "";
            resultHTML.append("    <option value="+i+ " " + attrSelectedHTML + ">\r");
            resultHTML.append(sb.getValue(i));
            resultHTML.append("    </option>\r");
        }
        resultHTML.append("</select>\r");
        resultHTML.append("\r");

        TagUtils.getInstance().write(pageContext, resultHTML.toString());
        return SKIP_BODY;
    }


    private MultipleSelectBox getMultipleSelectBox() throws JspException{
        Object object = getPropertyObject();
        return (object != null && object instanceof MultipleSelectBox) ? (MultipleSelectBox)object : null;
    }
    
}