package com.tmx.beng.base.dbfunctions;

import com.tmx.beng.base.BillingMessage;
import com.tmx.beng.base.Reservation;
import com.tmx.beng.base.ReservationSet;
import com.tmx.beng.base.cache.InvokeFunctionException;
import com.tmx.beng.base.cache.DBCacheException;
import com.tmx.beng.base.cache.DBCache;
import com.tmx.as.entities.bill.transaction.TransactionType;
import com.tmx.as.entities.bill.terminal.Terminal;
import com.tmx.as.entities.bill.seller.Seller;

import java.util.Map;
import java.util.HashMap;
import java.util.Iterator;
import java.math.BigDecimal;

public class SellerOperatorTariff extends BasicTariffFunction {
    public static final String SERVICE_CODE = "operator_code";
    /**
     * Transaction cost
     */
    public static final String TX_COST_OP = "tx_cost_op";
    /**
     * Terminal discount
     */
    public static final String DISCOUNT_PERCENT_OP = "discount_percent_op";

    boolean isTariffApplicable(Map actualParams) {
        String serviceCodeInMessage = task.getProcesedMessage().getAttributeString(BillingMessage.SERVICE_CODE);
        if (BillingMessage.O_REFILL_PAYMENT.equals(task.getProcesedMessage().getOperationName()) &&
                actualParams.get(SERVICE_CODE).equals(serviceCodeInMessage))
            return true;
        else
            return false;
    }

    void applyTariff(Map actualParams, Reservation reservation) throws InvokeFunctionException {
        try {
            String serviceCodeInMessage = task.getProcesedMessage().getAttributeString(BillingMessage.SERVICE_CODE);
            if (actualParams.get(SERVICE_CODE).equals(serviceCodeInMessage)) {
                double txCost = Double.parseDouble((String) actualParams.get(TX_COST_OP));
                double discontPercent = Double.parseDouble((String) actualParams.get(DISCOUNT_PERCENT_OP));

                double amount = Double.parseDouble(task.getProcesedMessage().getAttributeString(BillingMessage.AMOUNT));

                //round value to 2 signs after dot
                BigDecimal tarrifedAmountBD = new BigDecimal((amount + txCost) * discontPercent);
                tarrifedAmountBD = tarrifedAmountBD.setScale(ROUND_SCALE, BigDecimal.ROUND_HALF_UP);

                //prepare reservation on primary tariff
                reservation.reserve(null, tarrifedAmountBD.doubleValue(), null, TransactionType.CODE_CREDIT);
            }
        }
        catch (Throwable e) {
            throw new InvokeFunctionException("err.seller_tariff.calc_error", e);
        }
    }

    public ValidationError[] validate(Map actualParams) {
        return new ValidationError[0];
    }

    public TariffParameter[] getDefaultParams() {
        TariffParameter paramOperatorCode = new TariffParameter();
    	paramOperatorCode.setName(SERVICE_CODE);
    	paramOperatorCode.setValue("UMCPoP");
        paramOperatorCode.setOrderNum(1l);
        paramOperatorCode.setMandatory(Boolean.TRUE);

        TariffParameter paramDisPer = new TariffParameter();
    	paramDisPer.setName(DISCOUNT_PERCENT_OP);
    	paramDisPer.setValue("0.98");
        paramDisPer.setOrderNum(2l);
        paramDisPer.setMandatory(Boolean.TRUE);

        TariffParameter paramCost = new TariffParameter();
    	paramCost.setName(TX_COST_OP);
    	paramCost.setValue("0");
        paramCost.setOrderNum(3l);
        paramCost.setMandatory(Boolean.TRUE);

        return new TariffParameter[] {paramOperatorCode, paramDisPer, paramCost};
    }
}
