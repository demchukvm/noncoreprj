package com.tmx.web.base;

import org.apache.struts.config.ModuleConfig;
import org.apache.struts.config.ForwardConfig;
import org.apache.struts.config.ActionConfig;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.HashSet;

/** One instance of Roadmap could be assigned with one http session.
 * Using it you could organize the chains of actions and pages. Then previos actions are
 * specify forwards for next actions and pages in chain. */
public class Roadmap {

    private Map actionForwards = new HashMap();
    private Map actionCommands = new HashMap();
    private Map paramsScheduled = new HashMap();
    private Map paramsCollected = new HashMap();
    private Map pageForwards = new HashMap();
    private Map pageCommands = new HashMap();
    private Map pageActions = new HashMap();
    private final String DEFAULT = "DEFAULT";

    /** Clean the roadmap */
    public void clean(){
        actionForwards = new HashMap();
        actionCommands = new HashMap();
        paramsScheduled = new HashMap();
        paramsCollected = new HashMap();
        pageForwards = new HashMap();
        pageCommands = new HashMap();
        pageActions = new HashMap();
    }


    /**
     * Set the forward with given name to specifed action.
     * @param   targetActionName  name of the action the current forward be addressed to
     *                            as it specified in struts-config.xml.
     * @param   forwardRole       you could assign more then one forward to action.
     *                            And target action could use them according to its internal logic.
     * @param   forwardName       the name of forward as it specified in struts-config.xml
     * */
    public void setActionForward(String targetActionName,
                                 String forwardRole,
                                 String forwardName) throws WebException{

        validateActionName(targetActionName);
        validateForwardName(forwardName);

        Map forwardsForAction = (Map)actionForwards.get(targetActionName);
        if(forwardsForAction == null)
            forwardsForAction = new HashMap();

        forwardsForAction.put(forwardRole, forwardName);
        actionForwards.put(targetActionName, forwardsForAction);
    }

    /** The same with <code>setActionForward()</code>, but uses constant
     * <code>DEFAULT</code> to set default forward.
    */
    public void setDefaultActionForward(String targetActionName,
                                        String forwardName) throws WebException{
        setActionForward(targetActionName, DEFAULT, forwardName);
    }

    /** Forward name validation against the struts-config.xml
     * is performed in the SET methods. */
    public String getDefaultActionForwardName(String actionName) throws WebException{
        return getActionForwardName(actionName, DEFAULT);
    }


    /** Return the forward by role registered in this roadmap for given action's name */
    public String getActionForwardName(String actionName, String forwardRole) throws WebException{
        Map forwardsForAction = (Map)actionForwards.get(actionName);
        if(forwardsForAction == null)
            throw new WebException("err.roadmap.no_forwards_found_for_action", new String[]{actionName});
        String forward = (String)forwardsForAction.get(forwardRole);
        if(forward == null)
            throw new WebException("err.roadmap.forward_not_found_for_action", new String[]{actionName, forwardRole});

        return forward;
    }


    /** Set the command name and value to the given action.
     * Command could be used inside the action to navigate along
     * business script algorithm */
    public void setActionCommand(String targetActionName,
                                 String commandName,
                                 String commandValue) throws WebException{

        validateActionName(targetActionName);

        Map commandsForAction = (Map)actionCommands.get(targetActionName);
        if(commandsForAction == null)
            commandsForAction = new HashMap();

        commandsForAction.put(commandName, commandValue);
        actionCommands.put(targetActionName, commandsForAction);
    }

    /** The same with <code>setActionCommand()</code>, but uses constant
     * <code>DEFAULT</code> to set default command.
    */
    public void setDefaultActionCommand(String targetActionName,
                                        String commandName) throws WebException{
        setActionCommand(targetActionName, DEFAULT, commandName);
    }


    /** Action name validation against the struts-config.xml
     * is performed in the SET methods. */
    public String getDefaultActionCommandName(String actionName) throws WebException{
        return getActionCommandName(actionName, DEFAULT);
    }


    /** Return the command value by command name registered in this roadmap for given action's name */
    public String getActionCommandName(String actionName, String commandName) throws WebException{
        Map commandsForAction = (Map)actionCommands.get(actionName);
        if(commandsForAction == null)
//RM:            throw new WebException("err.roadmap.no_commands_found_for_action", new String[]{actionName});
            return null;
        String command = (String)commandsForAction.get(commandName);
        if(command == null)
//RM:            throw new WebException("err.roadmap.command_not_found_for_action", new String[]{actionName, commandName});
            return null;
        return command;
    }


    /**
     * Set the forward with given name to specifed page.
     * @param   targetPageName    name of the page the current forward be addressed to
     *                            as it specified in struts-config.xml.
     * @param   forwardRole       you could assign more then one forward to page.
     *                            And target page could use them according to its internal logic.
     * @param   forwardName       the name of forward as it specified in struts-config.xml
     * */
    public void setPageForward(String targetPageName,
                               String forwardRole,
                               String forwardName) throws WebException{

        validateForwardName(targetPageName);
        validateForwardName(forwardName);

        Map forwardsForPage = (Map)pageForwards.get(targetPageName);
        if(forwardsForPage == null)
            forwardsForPage = new HashMap();

        forwardsForPage.put(forwardRole, forwardName);
        pageForwards.put(targetPageName, forwardsForPage);
    }

    /**
     * Set the forward with given name to specifed page.
     * @param   targetPageName    name of the page the current forward be addressed to
     *                            as it specified in struts-config.xml.
     * @param   commandRole       you could assign more then one command to page.
     *                            And target page could use them according to its internal logic.
     * @param   commandName       the name of command to be sent on given forwardName *
     * */
    public void setPageCommand(String targetPageName,
                               String commandRole,
                               String commandName) throws WebException{

        validateForwardName(targetPageName);

        Map commandsForPage = (Map)pageCommands.get(targetPageName);
        if(commandsForPage == null)
            commandsForPage = new HashMap();

        commandsForPage.put(commandRole, commandName);
        pageCommands.put(targetPageName, commandsForPage);
    }

    /** The same with <code>setPageForward()</code>, but uses constant
     * <code>DEFAULT</code> to set default forward. */
    public void setDefaultPageForward(String targetPageName,
                                      String forwardName) throws WebException{
        setPageForward(targetPageName, DEFAULT, forwardName);
    }

    public String getPageForwardURL(String pageName, String forwardRole) throws WebException{
        Map forwardsForPage = (Map) pageForwards.get(pageName);
        if (forwardsForPage == null)
            throw new WebException("err.roadmap.no_forwards_found_for_page", new String[]{pageName});
        String forwardName = (String) forwardsForPage.get(forwardRole);
        if (forwardName == null)
            throw new WebException("err.roadmap.forward_not_found_for_page", new String[]{pageName, forwardRole});
        //Get URL by forward name
        ModuleConfig mc = ApplicationEnvironment.getModuleConfig();
        ForwardConfig forwardConfig = findForwardConfig(mc, forwardName);
        if(forwardConfig == null)
            throw new WebException("err.roadmap.forward_not_registered", new String[]{forwardName});

        return ApplicationEnvironment.getAppDeploymentName() + forwardConfig.getPath();
    }

    /** Forward name validation against the struts-config.xml
     * is performed in the SET methods. */
    public String getDefaultPageForwardURL(String pageName) throws WebException{
        return getPageForwardURL(pageName, DEFAULT);
    }


    public String getPageForwardName(String pageName, String forwardRole) throws WebException{
        Map forwardsForPage = (Map) pageForwards.get(pageName);
        if (forwardsForPage == null)
            throw new WebException("err.roadmap.no_forwards_found_for_page", new String[]{pageName});
        String forward = (String) forwardsForPage.get(forwardRole);
        if (forward == null)
            throw new WebException("err.roadmap.forward_not_found_for_page", new String[]{pageName, forwardRole});
        return forward;
    }

    public String getPageCommand(String pageName, String commandRole) throws WebException{
        Map commandsForPage = (Map) pageCommands.get(pageName);
        if (commandsForPage == null)
            return null;
        String command = (String) commandsForPage.get(commandRole);
        if (command == null)
            return null;
        return command;
    }

    /** Forward name validation against the struts-config.xml
     * is performed in the SET methods. */
    public String getDefaultPageForwardName(String pageName) throws WebException{
        return getPageForwardName(pageName, DEFAULT);
    }

    /**
     * Set the action with given name to specifed page.
     * @param   targetPageName    name of the page the current forward be addressed to
     *                            as it specified in struts-config.xml.
     * @param   actionRole        you could assign more then one action to page.
     *                            And target page could use them according to its internal logic.
     * @param   actionName        the name of action as it specified in struts-config.xml
     * */
    public void setPageAction(String targetPageName,
                              String actionRole,
                              String actionName) throws WebException{

        validateForwardName(targetPageName);
        validateActionName(actionName);

        Map actionsForPage = (Map)pageActions.get(targetPageName);
        if(actionsForPage == null)
            actionsForPage = new HashMap();

        actionsForPage.put(actionRole, actionName);
        pageActions.put(targetPageName, actionsForPage);
    }

    /** The same with <code>setPageAction()</code>, but uses constant
     * <code>DEFAULT</code> to set default action. */
    public void setDefaultPageAction(String targetPageName,
                                     String actionName) throws WebException{
        setPageAction(targetPageName, DEFAULT, actionName);
    }
                                              
    public String getPageActionURL(String pageName, String actionRole) throws WebException{
        Map actionsForPage = (Map) pageActions.get(pageName);
        if (actionsForPage == null)
            throw new WebException("err.roadmap.no_actions_found_for_page", new String[]{pageName});
        String actionName = (String) actionsForPage.get(actionRole);
        if (actionName == null)
            throw new WebException("err.roadmap.action_not_found_for_page", new String[]{pageName, actionRole});
        //Get URL by forward name
        ModuleConfig mc = ApplicationEnvironment.getModuleConfig();
        ActionConfig actionConfig = mc.findActionConfig(actionName);
        if(actionConfig == null)
            throw new WebException("err.roadmap.action_not_registered", new String[]{actionName});

        return ApplicationEnvironment.getAppDeploymentName() + actionConfig.getPath() + ApplicationEnvironment.getActionExtension();
    }

    public String getPageUrl(String pageName, String role) throws WebException {
        Map actionForPage = (Map) pageActions.get(pageName);
        Map forwardForPage = (Map) pageForwards.get(pageName);

        if (actionForPage == null) {
            if (forwardForPage == null)
                throw new WebException("err.roadmap.no_actions_and_forwards_found_for_page", new String[]{pageName});
            String forwardName = (String) forwardForPage.get(role);
            if (forwardName == null)
                throw new WebException("err.roadmap.no_actions_and_forwards_found_for_page", new String[]{pageName, role});

            ModuleConfig mc = ApplicationEnvironment.getModuleConfig();
            ForwardConfig forwardConfig = findForwardConfig(mc, forwardName);
            if(forwardConfig == null)
                throw new WebException("err.roadmap.forward_not_registered", new String[]{forwardName});

            return ApplicationEnvironment.getAppDeploymentName() + forwardConfig.getPath();
        } else {
            String actionName = (String) actionForPage.get(role);

            if (forwardForPage != null) {
                String forwardName = (String) forwardForPage.get(role);
                if (( forwardName != null)&&(actionName != null) )
                    throw new WebException("err.roadmap.both_action_and_forward_found_for_page", new String[]{pageName, role});
            }
            if (actionName == null)
                throw new WebException("err.roadmap.no_actions_and_forwards_found_for_page", new String[]{pageName, role});
            ModuleConfig mc = ApplicationEnvironment.getModuleConfig();
            ActionConfig actionConfig = mc.findActionConfig(actionName);
            if(actionConfig == null)
                throw new WebException("err.roadmap.action_not_registered", new String[]{actionName});
            return ApplicationEnvironment.getAppDeploymentName() + actionConfig.getPath() + ApplicationEnvironment.getActionExtension();
        }
    }

    /** Action name validation against the struts-config.xml
     * is performed in the SET methods. */
    public String getDefaultPageActionURL(String pageName) throws WebException{
        return getPageActionURL(pageName, DEFAULT);
    }


    /** Schedule the parameter to be collected by the geven action */
    public void scheduleParameterToCollect(String collectInActionName, String collectForActionName, String parameterName) throws WebException{
        validateActionName(collectInActionName);
        validateActionName(collectForActionName);

        Map params = (Map)paramsScheduled.get(collectInActionName);
        if(params == null)
            params = new HashMap();

        //One parameter could be collected for several actions. Put them into set
        Set actions = (Set)params.get(parameterName);
        if(actions == null)
            actions = new HashSet();

        actions.add(collectForActionName);
        params.put(parameterName, actions);
        paramsScheduled.put(collectInActionName, params);
    }

    /** Set the parameter into Roadmap */
    public void collectParameter(String collectForActionName, String parameterName, Object parameterValue) throws WebException{
        validateActionName(collectForActionName);

        Map collectedParamsForAction = (Map)paramsCollected.get(collectForActionName);
        if(collectedParamsForAction == null)
            collectedParamsForAction = new HashMap();

        collectedParamsForAction.put(parameterName, parameterValue);
        paramsCollected.put(collectForActionName, collectedParamsForAction);
    }

    /** Returns the map of scheduled parameters for given action.
     * The values are assigned with parameters are the Set of <code>collectForActionName</code> */
    public Map getScheduledParameters(String collectInActionName) throws WebException{
        validateActionName(collectInActionName);
        Map scheduledParamsForAction = (Map)paramsScheduled.get(collectInActionName);
        return scheduledParamsForAction;
    }

    /** Get the paramer value */
    public Object getParameterValue(String collectForActionName, String parameterName, boolean required) throws WebException{
        validateActionName(collectForActionName);
        Map collectedParamsForAction = (Map)paramsCollected.get(collectForActionName);

        if(collectedParamsForAction == null)
            if(required)
                throw new WebException("err.roadmap.required_parameter_is_null", new String[]{collectForActionName, parameterName});
            else
                return null;

        Object paramValue = collectedParamsForAction.get(parameterName);
        if(paramValue == null && required)
                throw new WebException("err.roadmap.required_parameter_is_null", new String[]{collectForActionName, parameterName});
        return paramValue;
    }

    private void validateActionName(String actionName) throws WebException{
        ModuleConfig mc = ApplicationEnvironment.getModuleConfig();
        if(actionName == null || mc.findActionConfig(actionName) == null)
            throw new WebException("err.roadmap.action_not_registered", new String[]{actionName});
    }


    private void validateForwardName(String forwardName) throws WebException{
        ModuleConfig mc = ApplicationEnvironment.getModuleConfig();
        if(forwardName == null || findForwardConfig(mc, forwardName) == null)
            throw new WebException("err.roadmap.forward_not_registered", new String[]{forwardName});
    }


    private ForwardConfig findForwardConfig(ModuleConfig mc, String complexForwardName){
        ForwardHelper forwardHelper = new ForwardHelper(complexForwardName);
        ForwardConfig forwardConfig = null;
        if(!forwardHelper.local)
            forwardConfig = mc.findForwardConfig(forwardHelper.forwardName);
        else{
            ActionConfig actionConfig = mc.findActionConfig(forwardHelper.actionOwnerName);
            if(actionConfig != null)
                forwardConfig = actionConfig.findForwardConfig(forwardHelper.forwardName);
        }
        return forwardConfig;
    }

    /** Help to work with forwards */
    private class ForwardHelper{
        private boolean local = false;
        private String actionOwnerName;
        private String forwardName;

        /**
         * Complex forward name has format:
         *
         * 1) 'global_forward_name'
         * 2) 'global:global_forward_name'
         * 3) 'local:action_owner_name#local_forward_name'
         * */
        private ForwardHelper(String complexForwardName){
            final String GLOBAL_SUFFIX = "global:";
            final String LOCAL_SUFFIX = "local:";
            final String SPLITTER = "#";
            if(complexForwardName.indexOf(GLOBAL_SUFFIX) == 0){
                forwardName = complexForwardName.substring(GLOBAL_SUFFIX.length());
                local = false;
            }
            else if(complexForwardName.indexOf(LOCAL_SUFFIX) == 0){
                String[] names = complexForwardName.substring(LOCAL_SUFFIX.length()).split(SPLITTER);
                local = true;
                actionOwnerName = names[0];
                forwardName = names[1];
            }
            else{
                forwardName = complexForwardName;
                local = false;
            }
        }
    }


}
