package com.tmx.beng.iso8583Gate.keygen;

import com.tmx.util.InitException;
import com.tmx.beng.iso8583Gate.pdu.util.HexConverter;

import java.io.File;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.util.Arrays;

/**
    Util to generate master keys XML-file for HKLM.
 */
public class KeyGenUtil {
    private int trmCount = 10;//default
    private int keyCollectionSize = 11;//default
    private String trmNamePrefix = "SV";
    private int trmNameIdxLength = 5;
    private byte[] hklmKey;
    private String workDirectoryPath;
    private PrintStream log = System.out;

    public static void main(String[] args) throws InitException, Exception{
        KeyGenUtil kgu = new KeyGenUtil();
        kgu.init();
        kgu.generate();
    }

    private void init() throws InitException {
        log.print("Initialization...");

        //required props
        workDirectoryPath = System.getProperty("workDir");
        if(workDirectoryPath == null)
            throw new InitException("System property 'workDir' is not specified");

        String hklmKeyStr = System.getProperty("hklmKey");
        if(hklmKeyStr == null)
            throw new InitException("System property 'hklmKey' is not specified");
        hklmKey = new HexConverter().getHexFromString(hklmKeyStr);

        //optional props
        if(System.getProperty("trmCount") != null)
            trmCount = Integer.parseInt(System.getProperty("trmCount"));

        if(System.getProperty("trmNamePrefix") != null)
            trmNamePrefix = System.getProperty("trmNamePrefix");

        if(System.getProperty("keyCollectionSize") != null)
            keyCollectionSize = Integer.parseInt(System.getProperty("keyCollectionSize"));

        if(System.getProperty("trmNameIdxLength") != null)
            trmNameIdxLength = Integer.parseInt(System.getProperty("trmNameIdxLength"));

        log.println(" successful");
    }

    private void generate() throws Exception{
        PrintStream reportPs = new PrintStream(new FileOutputStream(workDirectoryPath + File.separator + "report.csv"));
        reportPs.println(KeyProfile.toCsvHeaderString());
        for(int i=0; i<trmCount; i++){
            String terminalName = evaluateTerminalName(i);
            log.print("Generating keyprofile for terminal "+terminalName+"...");
            KeyProfile keyProfile = new KeyProfile();
            keyProfile.generate(terminalName, hklmKey, keyCollectionSize);

            PrintStream keyProfilePs = new PrintStream(new FileOutputStream(workDirectoryPath + File.separator + terminalName+".xml"));
            keyProfilePs.println(keyProfile.toXmlString());
            keyProfilePs.flush();
            keyProfilePs.close();

            reportPs.print(keyProfile.toCsvString());
            log.println(" successful");
        }
        reportPs.flush();
        reportPs.close();
    }

    private String evaluateTerminalName(int idx){
        return trmNamePrefix+evaluateNameIdx(idx);
    }

    private String evaluateNameIdx(int idx){
        String nameIdx = String.valueOf(idx);
        int zeroPrefixLength;
        if((zeroPrefixLength =  trmNameIdxLength - nameIdx.length()) > 0){
            char[] zeroPrefix = new char[zeroPrefixLength];
            Arrays.fill(zeroPrefix, "0".charAt(0));
            return new String(zeroPrefix) + nameIdx;
        }
        else
            return nameIdx;
    }


}
