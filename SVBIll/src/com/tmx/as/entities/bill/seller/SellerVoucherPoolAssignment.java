package com.tmx.as.entities.bill.seller;

import com.tmx.as.base.BasicEntity;
import com.tmx.as.entities.bill.voucher_pool.VoucherPool;

import java.io.Serializable;

/**
 * Created by IntelliJ IDEA.
 * User: informix
 * Date: 24.04.2007
 * Time: 15:02:43
 * To change this template use File | Settings | File Templates.
 */
public class SellerVoucherPoolAssignment extends BasicEntity{
    private Long sellerVoucherPoolAssignmentId;
    private VoucherPool voucherPoolId;
    private Seller sellerId;

    public Serializable getId() {
        return getSellerVoucherPoolAssignmentId();
    }

    private Long getSellerVoucherPoolAssignmentId() {
       return sellerVoucherPoolAssignmentId;
    }

    public int getValid() {
        return valid;
    }

    public void setValid(int valid) {
        this.valid=valid;
    }

    public VoucherPool getVoucherPoolId() {
        return voucherPoolId;
    }

    public void setVoucherPoolId(VoucherPool voucherPoolId) {
        this.voucherPoolId = voucherPoolId;
    }

    public void setSellerVoucherPoolAssignmentId(Long sellerVoucherPoolAssignmentId) {
        this.sellerVoucherPoolAssignmentId = sellerVoucherPoolAssignmentId;
    }

    public Seller getSellerId() {
        return sellerId;
    }

    public void setSellerId(Seller sellerId) {
        this.sellerId = sellerId;
    }
}
