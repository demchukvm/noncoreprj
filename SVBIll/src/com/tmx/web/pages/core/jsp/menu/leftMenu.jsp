<%@ page language="java" pageEncoding="UTF-8" contentType="text/html; charset=utf-8" errorPage="/core/jsp/message/jsperror.jsp" isELIgnored="false" %>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://tmx.com/taglib/tmx-controls" prefix="tmx_ctrl" %>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c" %>
<%--<%@ include file="news_scripts.css" %>--%>

<!--<script type="text/javascript" src="news_scripts.js" />-->


<c:if test="${requestScope['tile.loggedInLayout.leftMenuName'] != null}">
    <tmx_ctrl:menu menuName="${requestScope['tile.loggedInLayout.leftMenuName']}" styleName="html_leftmenu_v3"/>
</c:if>

<%--
<div style="position: absolute; left:0px; top:300px; display: block;" id="first" onClick="set_visible('second', 'first', 'open')">
    <img src="<tmx_ctrl:rewriteUrl href="/core/images/common/news.png"/>" border="0"/>
</div>

<div id="second" style="border-style: dotted;display: none; position: absolute; width:500px;">
	<div style="width:100%;background: #fc3;">
		Новости
		<span style="position: absolute; left:95%;" onClick="set_visible('second', 'first', 'close')">x</span>
	</div>
	<div style="background: white;">
	Если бы корабли оставляли следы на волнах,
	Мы бы пустились за ними вплавь,
	Если бы лекарством от всех болезней были песни,
	Мы бы спасли весь мир.
	Жаль, но всегда есть эти \"если\",
	И мы не летаем во сне, как в детстве.
	З друзьями реже бываем вместе,
	Та все не так как прежде.
	Если бы корабли оставляли следы на волнах,
	Мы бы пустились за ними вплавь,
	Если бы лекарством от всех болезней были песни,
	Мы бы спасли весь мир.
	Жаль, но всегда есть эти \"если\",
	И мы не летаем во сне, как в детстве.
	З друзьями реже бываем вместе,
	Та все не так как прежде.
	</div>
	<div style="width:100%;background: green;">
		бла-бла-бла
	</div>
</div>      --%>