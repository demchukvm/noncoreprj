<%@ page language="java" pageEncoding="UTF-8" contentType="text/html; charset=utf-8"
         errorPage="/core/jsp/message/jsperror.jsp" isELIgnored="false" %>
<%@ taglib uri="/WEB-INF/tld/struts-nested.tld" prefix="nested" %>
<%@ taglib uri="/WEB-INF/tld/tmx-controls.tld" prefix="tmx_ctrl" %>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>


<html:form action="/core/balance/terminal_group_balance/changeTerminalGroupBalance">
<table border="0" width="100%">

    <bean:define id="form" name="core.balance.terminal_group_balance.changeTermGroupBalanceForm" type="com.tmx.web.forms.core.balance.terminal_group_balance.ChangeTermGroupBalanceForm"/>
    <input type="hidden" name="balanceId"
           value="${form.balanceId}"/>

    <!-- BUTTONS -->
    <tr>
        <td style="text-align: left">
            <tmx_ctrl:button property="onCancelButton"/>
            <tmx_ctrl:button property="onChangeButton"/>
        </td>
    </tr>
    <tr>
        <td valign="center" align="center" class="tabForm">
        <table width="100%" border="0">

            <%--<!--BEGIN for abstract info transaction form    -->--%>
            <tr>
                <td class="dataLabel" width="15%">
                    <tmx_ctrl:writeEntityLabel attrName="amount"
                                               entityClassName="com.tmx.as.entities.bill.balance.SellerBalance"/>:
                </td>
                <td class="dataField" width="35%">
                    <tmx_ctrl:editBox property="amountEditBox"/>
                </td>

                <td class="dataLabel" width="15%">
                    <tmx_ctrl:writeEntityLabel attrName="{@name}"
                                               entityClassName="com.tmx.as.entities.bill.transaction.TransactionType"/>:
                </td>
                <td class="dataField" width="35%">
                     <tmx_ctrl:selectBox property="transTypeScrollBox" width="230px"/>
                </td>

            </tr>

            <tr>
                <td class="dataLabel" width="15%">
                    <tmx_ctrl:writeEntityLabel attrName="description"
                                               entityClassName="com.tmx.as.entities.bill.balance.SellerBalance"/>:
                </td>
                <td class="dataField" width="35%">
                    <tmx_ctrl:textArea property="descriptionTextArea"/>
                </td>
            </tr>

        </table>
        </td>

    </tr>
   
</table>
</html:form>