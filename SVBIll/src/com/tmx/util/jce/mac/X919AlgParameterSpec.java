package com.tmx.util.jce.mac;

import javax.crypto.Cipher;
import java.security.spec.AlgorithmParameterSpec;

/**

 */
public class X919AlgParameterSpec  implements AlgorithmParameterSpec {
    private Cipher desCipher;
    private Cipher desEdeCipher; //Triple DES

    public X919AlgParameterSpec(Cipher tripleDesCipher, Cipher desCipher){
        this.desCipher = desCipher;
        this.desEdeCipher = tripleDesCipher;
    }

    public Cipher getDesCipher() {
        return desCipher;
    }

    public Cipher getDesEdeCipher() {
        return desEdeCipher;
    }
}
