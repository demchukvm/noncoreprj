package com.tmx.beng.base.cache;

import com.tmx.as.scheduler.ProcessingThread;
import com.tmx.beng.base.*;
import com.tmx.beng.access.Access;
import com.tmx.beng.access.Connection;
import com.tmx.util.InitException;

import javax.naming.InitialContext;
import javax.naming.NamingException;


public class DBCacheReloader extends ProcessingThread {

    protected void executeSpecificTask() {
        try {
            Access access = getBillingEngineAccess();
            Connection connection= access.obtainConnection();
            BillingMessageImpl billMsg = new BillingMessageImpl();
            billMsg.setOperationName(BillingMessage.O_RELOAD_DB_CACHE);
            connection.processAsync(billMsg);
        } catch (InitException e) {
            logger.error("err_in_dbcache_reloading", e);
        } catch (BillException e) {
            logger.error("err_in_dbcache_reloading", e);  
        }
    }

    private Access getBillingEngineAccess() throws InitException {
        InitialContext ctx = null;
        try{
            System.setProperty("java.naming.factory.url.pkgs", ""); //=org.apache.naming
            ctx = new InitialContext();//System.getProperties()
            Access newAccess = (Access)ctx.lookup("java:comp/services/billingEngine");
            if(newAccess == null)
                throw new InitException("err.csapi.null_basic_access_obtained");
            return newAccess;
        }
        catch(NamingException e){
            throw new InitException("err.csapi.failed_to_obtain_billing_engine_access", e);
        }
        finally{
            closeInitialContext(ctx);
        }
    }

    private void closeInitialContext(InitialContext ctx){
        if(ctx != null){
            try{
                ctx.close();
            }
            catch(NamingException e) {
                logger.info( "Unable to release initial context", e );
            }
        }
    }
    
}
