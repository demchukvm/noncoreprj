package com.tmx.beng.base.processors;

import com.tmx.beng.base.*;
import com.tmx.beng.base.cache.DBCache;
import com.tmx.beng.base.cache.DBCacheException;

/**
 * Register message
 */
public class AutoRegistrationProcessor extends TaskProcessor {

    public Task process0(Task task, DBCache dbCache, BillingMessageWritable msg) throws BillException {
        logger.debug("Terminal Autoregistration...");

        try{
            // get terminal allowed
            if(!dbCache.isTerminalAllowed(msg.getAttributeString(BillingMessage.TERMINAL_SN))){
                dbCache.registerDefaultTerminal(
                        msg.getAttributeString(BillingMessage.TERMINAL_SN),
                        msg.getAttributeString(BillingMessage.SELLER_CODE)
                );
            }
        }catch(DBCacheException e){
            msg.setStatusCode(StatusDictionary.TERMINAL_AUTOREGISTRATION_ERROR);
            throw new BillException(e.getMessage(), e);
        }

        return task;
    }

}
