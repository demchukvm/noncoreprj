package com.tmx.beng.httpsgate.cyberplat;

import com.tmx.beng.httpsgate.HttpsGateException;
import com.tmx.util.InitException;
import org.apache.log4j.Logger;

public class CyberplatGate extends BasicCyberplatGate
{
    protected static final String GATE_NAME = "cyberplat_gate";
    protected static Logger logger = Logger.getLogger("cyberplat_gate." + GATE_NAME);
    private static CyberplatGate cyberplatGate = null;

//    static
//    {
//        System.load("D:\\SVBILL\\repository\\security\\cyberplat\\win32\\thread-safe\\jnipriv.dll");
//        //System.loadLibrary("jnipriv");
//    }

    public static CyberplatGate getInstance() throws HttpsGateException
    {
        if(cyberplatGate == null)
        {
            logger.error("Attempt to obtain not inited CyberplatGate");
            throw new HttpsGateException("err.CyberplatGate.null_instance");
        }
        return cyberplatGate;
    }

    public static void init()throws InitException
    {
        logger.info("Start CyberplatGate initialization");
        cyberplatGate = new CyberplatGate();
        cyberplatGate.init0();
        logger.info("CyberplatGate initialized");
    }

    public static void shutdown(final long timeout)
    {
        logger.info("Start CyberplatGate Shutdown");
        cyberplatGate.shutdown0(timeout);
        logger.info("CyberplatGate shutdown");
    }

    protected String getGateName()
    {
        return GATE_NAME;
    }

}
