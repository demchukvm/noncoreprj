package com.tmx.as.entities.bill.request;

import com.tmx.as.base.BasicEntity;

import java.util.Date;
import java.io.Serializable;

/**
 */
public class Request extends BasicEntity {
    public static Long SESSION_OPENED   = new Long(1);
    public static Long SESSION_CLOSED   = new Long(2);  

    private Long requestId;
    private String requestNum;
    private String billTransactionNum;
    private String cliTransactionNum;
    private Date beginTime;
    private Date endTime;
    private Long status;

    public Long getStatus() {
        return status;
    }

    public String getRequestNum() {
        return requestNum;
    }

    public void setRequestNum(String requestNum) {
        this.requestNum = requestNum;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

    public Long getRequestId() {
        return requestId;
    }

    public void setRequestId(Long requestId) {
        this.requestId = requestId;
    }

    public String getBillTransactionNum() {
        return billTransactionNum;
    }

    public void setBillTransactionNum(String billTransactionNum) {
        this.billTransactionNum = billTransactionNum;
    }

    public String getCliTransactionNum() {
        return cliTransactionNum;
    }

    public void setCliTransactionNum(String cliTransactionNum) {
        this.cliTransactionNum = cliTransactionNum;
    }

    public Date getBeginTime() {
        return beginTime;
    }

    public void setBeginTime(Date beginTime) {
        this.beginTime = beginTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public Serializable getId() {
        return getRequestId();
    }

    public int getValid() {
        return valid;
    }

    public void setValid(int valid) {
        this.valid = valid;
    }
}
