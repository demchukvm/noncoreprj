package com.tmx.beng.httpsgate.dacard.pdu;

import com.tmx.util.StructurizedException;
import com.tmx.beng.httpsgate.dacard.DacardGateException;

import java.util.Locale;

/**
This exception could occurs on parsing or composing the Dacard message.
 */
public class DacardPadException extends DacardGateException {

    public DacardPadException(String msg, String[] params, Throwable e, Locale locale) {
        super(msg, params, e, locale);
    }

    public DacardPadException(String msg, String[] params) {
        super(msg, params);
    }

    public DacardPadException(String msg, String[] params, Locale locale) {
        super(msg, params, locale);
    }

    public DacardPadException(String msg, Throwable e, Locale locale) {
        super(msg, e, locale);
    }

    public DacardPadException(String msg, Throwable e) {
        super(msg, e);
    }

    public DacardPadException(String msg, Locale locale) {
        super(msg, locale);
    }

    public DacardPadException(String msg) {
        super(msg);
    }

    public DacardPadException(String msg, String[] params, Throwable e) {
        super(msg, params, e);
    }
}
