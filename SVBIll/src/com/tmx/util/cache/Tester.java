package com.tmx.util.cache;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.lang.reflect.Array;
import java.util.Arrays;

/**
 * Created by IntelliJ IDEA.
 * User: Roma
 * Date: 12.08.2006
 * Time: 0:09:30
 * To change this template use File | Settings | File Templates.
 */
public class Tester {

    public static void main(String[] args) throws Throwable{
        new Tester().test();
    }

    public void test() throws Throwable{
        //1. crete test files
//        createTestFiles();

        Cache myCache = new MyFileCache();
        byte[] val = null;


        for(int i=0; i<15; i++){
            Thread.sleep(100);
            val = (byte[])myCache.get("D:\\test\\testfile"+i+".txt");
            val = null;
        }

        for(int i=0; i<50; i++){
            Thread.sleep(100);
            val = (byte[])myCache.get("D:\\test\\testfile"+i+".txt");
            val = null;
        }
    }

    private class MyFileCache extends FileCache{

        private MyFileCache(){
            maxSize = 20;
            cleanCount = 5;
            cleanType = CleanAlgorithm.Type.CLEAN_N_SELDOM_ACCESSED;
        }

        protected Object getRealObjectFromFile(InputStream is, String filePath) throws CacheException{
            byte[] data = null;
            try{
                data = new byte[is.available()];
                is.read(data, 0, is.available());
            }
            catch(Exception e){
                throw new CacheException("err.cache.failed_to_get_real_object !!!insert KEY!!!", e);
            }
            return data;
        }
    }


    private void createTestFiles() throws Throwable{
        for(int i=0; i < 50; i++){
            byte[] data = new byte[1000];
            byte d = 1;
            Arrays.fill(data, d);
            File file = new File("D:\\test\\testfile"+i+".txt");
            FileOutputStream fos = new FileOutputStream(file);
            fos.write(data);
            fos.flush();
            fos.close();
        }
    }
}
