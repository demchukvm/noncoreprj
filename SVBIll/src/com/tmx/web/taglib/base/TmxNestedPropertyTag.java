package com.tmx.web.taglib.base;

import org.apache.struts.taglib.nested.NestedPropertyTag;

import javax.servlet.jsp.JspException;

public abstract class TmxNestedPropertyTag extends NestedPropertyTag {

    abstract public int doStartTmxTag() throws JspException;

    public int doEndTmxTag() throws JspException {
        return EVAL_PAGE;    
    }

    final public int doStartTag() throws JspException {
        super.doStartTag();
        return  doStartTmxTag();
    }


    final public int doEndTag() throws JspException {
        super.doEndTag();
        return  doEndTmxTag();
    }
}
