<!--

This page renders errors occured on Action level

-->
<%@ page language="java" pageEncoding="UTF-8" contentType="text/html; charset=utf-8"
         errorPage="/core/jsp/message/jsperror.jsp" isELIgnored="false" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/tmx-controls.tld" prefix="tmx_ctrl" %>

<br/><br/>
<table border="0" width="100%" height="100%">
    <tr>
        <td valign="center" align="center">

            <table border="0" class="tabError">
                <tr>
                    <td align="center">
                        <!-- Print errors -->
                        <tmx_ctrl:writeError
                                errorMsgStyleClass="error_msg_div"
                                errorCauseStyleClass="error_cause_div"
                                errorStackStyleClass="error_stack_div"
                                errorLabelStyleClass="error_label_div"
                                imgRollout="/core/images/gray/ctrl/drill_out.gif"
                                imgRollin="/core/images/gray/ctrl/drill_in.gif"/>

                        <br/>
                        <input type="button" class="button" onclick="javascript:history.back()"
                            value="<bean:message bundle="core.labels" key="core.common.button.ok"/>">
                        <br/>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<br/><br/>

