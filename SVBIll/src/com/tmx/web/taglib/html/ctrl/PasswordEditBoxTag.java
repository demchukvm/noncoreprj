package com.tmx.web.taglib.html.ctrl;

import com.tmx.web.controls.EditBox;

import javax.servlet.jsp.JspException;

import org.apache.struts.taglib.TagUtils;

/**
 * Hides rendered chars
 */
public class PasswordEditBoxTag extends EditBoxTag{

    protected int controlStartTag() throws JspException {
        StringBuffer resultHTML = new StringBuffer();
        EditBox eb = getEditBox();
        if (eb == null) {
            TagUtils.getInstance().write(pageContext, "EditBox '"+getProperty()+"' is not found");
            return SKIP_BODY;
        }

        String readonly = (eb.isReadonly()) ? " readonly=\"true\" " : "";
        String value = " value=\""+((eb.getValue() == null) ? "" : eb.getValue()) +"\" ";
        String styleClass = " class=\""+((eb.isReadonly()) ? getReadonlyStyleClass() : getStyleClass())+"\" ";
        String type = " type=\"password\" ";
        String name = "name=\""+getProperty()+"."+EditBox.VALUE_SUFFIX+"\"";
        //Render HTML
        resultHTML.append("\r");
        resultHTML.append("<input " + name + type + value + styleClass + readonly +"/>\r");

        TagUtils.getInstance().write(pageContext,resultHTML.toString());
        return SKIP_BODY;
    }

}
