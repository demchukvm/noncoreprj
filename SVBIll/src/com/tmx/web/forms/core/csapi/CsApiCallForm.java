package com.tmx.web.forms.core.csapi;

import com.tmx.beng.access.Access;
import com.tmx.web.forms.xmlengine.BasicXMLPublisherForm;


/**
 */
public class CsApiCallForm extends BasicXMLPublisherForm {
    private Access billingEngineAccess = null;
    private String requestXml = null;
    private String desRequestXml = null;


    //------------------Properties
    
    public String getRequestXml() {
        return requestXml;
    }

    public void setRequestXml(String requestXml) {
        this.requestXml = requestXml;
    }


    public Access getBillingEngineAccess() {
        return billingEngineAccess;
    }

    public void setBillingEngineAccess(Access billingEngineAccess) {
        this.billingEngineAccess = billingEngineAccess;
    }

    public String getDesRequestXml() {
        return desRequestXml;
    }

    public void setDesRequestXml(String desRequestXml) {
        this.desRequestXml = desRequestXml;
    }
}
