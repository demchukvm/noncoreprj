package com.tmx.beng.httpsgate.globalmoney;

import org.apache.log4j.Logger;
import com.tmx.beng.httpsgate.HttpsGateException;
import com.tmx.util.InitException;

/**
 */
public class GlobalMoneyGate extends BasicGlobalMoneyGate {
    public static final String GATE_NAME = "globalmoney_gate";
    protected static Logger logger = Logger.getLogger(GATE_NAME);
    private static GlobalMoneyGate gate = null;

    public static GlobalMoneyGate getInstance()throws HttpsGateException {
        if(gate == null){
            logger.error("Attempt to obtain not inited billing engine");
            throw new HttpsGateException("err.GlobalMoneyGate.null_instance");
        }
        return gate;
    }

    public static void init()throws InitException {
        logger.info("Start GlobalMoneyGate initialization");
        gate = new GlobalMoneyGate();
        gate.init0();
        logger.info("GlobalMoneyGate initialized");
    }

    public static void shutdown(final long timeout) throws InitException {
        logger.info("Start GlobalMoneyGate Shutdown");
        gate.shutdown0(timeout);
        logger.info("GlobalMoneyGate shutdown");
    }

    protected String getGateName(){
        return GATE_NAME;
    }
}

