package com.tmx.as.entities.network.territory;

import com.tmx.as.base.BasicEntity;

import java.io.Serializable;

public class District extends BasicEntity {
    private Long districtId;
    private String name;
    private String shortName;
    private Country country;

    public Long getDistrictId() {
        return districtId;
    }

    public void setDistrictId(Long districtId) {
        this.districtId = districtId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    public Serializable getId() {
        return getDistrictId();
    }

    public int getValid() {
        return valid;
    }

    public void setValid(int valid) {
        this.valid = valid;
    }
}
