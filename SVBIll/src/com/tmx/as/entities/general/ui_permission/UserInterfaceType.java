package com.tmx.as.entities.general.ui_permission;

import com.tmx.as.base.BasicEntity;
import java.io.Serializable;

/**
 */
public class UserInterfaceType extends BasicEntity {
    /** Suported user interface types */
    public static final String FORM = "FORM";
    public static final String MENU = "MENU";
    public static final String MENU_CONTAINER = "MENU_CONTAINER";
    public static final String MENU_ITEM = "MENU_ITEM";
    public static final String FORM_CONTROL = "FORM_CONTROL";

    public static final Long FORM_ID = new Long(1);
    public static final Long MENU_ID = new Long(2);
    public static final Long MENU_CONTAINER_ID = new Long(3);
    public static final Long MENU_ITEM_ID = new Long(4);
    public static final Long FORM_CONTROL_ID = new Long(5);
    
    private Long userInterfaceTypeId;
    private String name;
    private String shortName;

    public Long getUserInterfaceTypeId() {
        return userInterfaceTypeId;
    }

    public void setUserInterfaceTypeId(Long userInterfaceTypeId) {
        this.userInterfaceTypeId = userInterfaceTypeId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public Serializable getId() {
        return getUserInterfaceTypeId();
    }

    public int getValid() {
        return valid;
    }

    public void setValid(int valid) {
        this.valid = valid;
    }
}
