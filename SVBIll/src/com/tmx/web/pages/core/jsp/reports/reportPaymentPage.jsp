<%@ page language="java" pageEncoding="UTF-8" contentType="text/html; charset=utf-8" isELIgnored="false" %>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-nested" prefix="nested" %>
<%@ taglib uri="http://tmx.com/taglib/tmx-controls" prefix="tmx_ctrl" %>
<%@ taglib uri="http://tmx.com/taglib/tmx-logic" prefix="tmx_logic" %>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html" %>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean" %>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>

<html:form action="/core/report/reportPayment">
<table width="100%" border="0">
    <!-- FILTER SET -->
    <tr>
        <td valign="top" width="100%" align="left">
            <tmx_ctrl:panelFilterSet tableName="table">
            <table>
                <tr>
                    <td class="dataLabel">
                        <bean:message bundle="core.labels" key="core.report.reportPayment.period"/>&nbsp;<bean:message bundle="core.labels" key="core.monitor.timeFrom"/>:
                    </td>
                    <td class="dataField">
                        <tmx_ctrl:timeSelector property="table.filterSet.filter(by_FromDate).parameter(date).control"/>
                    </td>

                    <td class="dataLabel">
                        <bean:message bundle="core.labels" key="core.monitor.timeTo"/>:
                    </td>
                    <td class="dataField">
                        <tmx_ctrl:timeSelector property="table.filterSet.filter(by_ToDate).parameter(date).control"/>
                    </td>
                </tr>
                <tr>
                    <td class="dataLabel">
                        <bean:message bundle="core.labels" key="core.report.reportPayment.diler"/>:
                    </td>
                    <td class="dataField">
                        <tmx_ctrl:selectBox property="sellersScrollBox"/>
                    </td>

                    <td class="dataLabel">
                        <bean:message bundle="core.labels" key="core.report.reportPayment.subdiler"/>:
                    </td>
                    <td class="dataField">
                        <tmx_ctrl:selectBox property="subSellersScrollBox"/>
                    </td>
                </tr>
                <tr>
                    <td class="dataLabel">
                        <bean:message bundle="core.labels" key="core.report.reportPayment.paymentSize"/>&nbsp;
                        <bean:message bundle="core.labels" key="core.report.reportPayment.from"/>:
                    </td>
                    <td class="dataField">
                        <tmx_ctrl:editBox property="table.filterSet.filter(by_client_amount_from).parameter(from_amount).control"/>
                    </td>

                    <td class="dataLabel">
                        <bean:message bundle="core.labels" key="core.monitor.timeTo"/>:
                    </td>
                    <td class="dataField">
                        <tmx_ctrl:editBox property="table.filterSet.filter(by_client_amount_to).parameter(to_amount).control"/>
                    </td>
                </tr>

                <tr>
                    <td class="dataField">
                    <tmx_ctrl:writeEntityLabel
                        attrName="{@name}"
                        entityClassName="com.tmx.as.entities.bill.transaction.TransactionType"/>:
                    </td>
                    <td class="dataField">
                    <tmx_ctrl:selectBox
                        property="table.filterSet.filter(by_transaction_type).parameter(type).control"/>
                    </td>
                    
                    <td class="dataField">
                            <tmx_ctrl:writeEntityLabel
                                attrName="paymentReason"
                                entityClassName="com.tmx.as.entities.bill.transaction.TransactionSupport"/>:
                    </td>
                    <td class="dataField">
                     <tmx_ctrl:editBox property="editBoxPaymentReason"/>
                    </td>
                </tr>
            </table>
        </tmx_ctrl:panelFilterSet>
        </td>
    </tr>
    <!-- TABLE -->
    <tr>
        <td>
            <table class='one_outer_cell_table' cellspacing='0' cellpadding='0' width="100%">
                <tr>
                    <td>
                        <table cellspacing='1' cellpadding='1' width="100%">
                            <!-- Table header -->
                            <tr align="center">
                                <th class="column_header_cell">
                                    <tmx_ctrl:imgTableOrdering tableName="table"
                                                               orderByFieldName="transactionTime"
                                                               labelBundle="as:com.tmx.as.entities.bill.transaction.ClientTransaction"/>
                                </th>
                                <th class="column_header_cell">
                                    <bean:message bundle="core.labels" key="core.report.reportPayment.diler"/>
                                </th>
                                <%--<th class="column_header_cell">--%>
                                    <%--<bean:message bundle="core.labels" key="core.report.reportPayment.subdiler"/>--%>
                                <%--</th>--%>
                                <th class="column_header_cell">
                                        <tmx_ctrl:imgTableOrdering tableName="table"
                                                                   orderByFieldName="amount"
                                                                   labelBundle="as:com.tmx.as.entities.bill.transaction.SellerBalanceTransaction"/>
                                </th>
                                <th class="column_header_cell">
                                    <tmx_ctrl:writeEntityLabel attrName="paymentReason"
                                               entityClassName="com.tmx.as.entities.bill.transaction.TransactionSupport"/>
                                </th>
                                <th class="column_header_cell">
                                    <tmx_ctrl:writeEntityLabel attrName="login"
                                               entityClassName="com.tmx.as.entities.general.user.User"/>
                                </th>
                                <th class="column_header_cell">
                                    <tmx_ctrl:writeEntityLabel attrName="description"
                                               entityClassName="com.tmx.as.entities.bill.transaction.TransactionSupport"/>
                                </th>
                                <th class="column_header_cell">
                                        <tmx_ctrl:imgTableOrdering tableName="table"
                                                                   orderByFieldName="type.name"
                                                                   labelBundle="as:com.tmx.as.entities.bill.transaction.SellerBalanceTransaction"/>
                                </th>
                            </tr>
                            <!-- Table data -->
                            <nested:iterate id="el" property="table.data" type="com.tmx.as.entities.bill.transaction.SellerBalanceTransaction">
                                <tr>
                                    <td class='column_data_cell'>
                                        <fmt:formatDate value="${el.transactionTime}" pattern="yyyy/MM/dd HH:mm:ss"/>
                                    </td>
                                    <td class='column_data_cell'>
                                        <c:if test="${el.seller!=null}">
                                            <tmx_ctrl:write name="el" property="seller.name" ignore="true"/>
                                        </c:if>
                                        <%--<c:if test="${el.seller.parentSeller!=null}">--%>
                                            <%--<tmx_ctrl:write name="el" property="seller.parentSeller.name" ignore="true"/>--%>
                                        <%--</c:if>--%>
                                    </td>
                                    <%--<td class='column_data_cell'>--%>
                                        <%--<c:if test="${el.seller.parentSeller!=null}">--%>
                                            <%--<tmx_ctrl:write name="el" property="seller.name" ignore="true"/>--%>
                                        <%--</c:if>--%>
                                    <%--</td>--%>
                                    <td class='column_data_cell'>
                                        <div align="right">
                                            <tmx_ctrl:write name="el" property="amount" ignore="true" format="###.00"/>
                                        </div>
                                    </td>
                                    <td class='column_data_cell'>
                                        <tmx_ctrl:write name="el" property="transactionSupport.paymentReason" ignore="true"/>
                                    </td>
                                    <td class='column_data_cell'>
                                        <tmx_ctrl:write name="el" property="transactionSupport.userActor.login" ignore="true"/>
                                    </td>
                                    <td class='column_data_cell'>
                                        <tmx_ctrl:write name="el" property="transactionSupport.description" ignore="true"/>
                                    </td>
                                    <td class='column_data_cell'>
                                        <tmx_ctrl:write name="el" property="type.name" ignore="true"/>
                                    </td>
                                </tr>
                            </nested:iterate>
                            <!-- Empty row -->
                            <nested:define id="tableSize" property="table.resultSize" type="java.lang.Integer"/>
                            <c:if test="${tableSize <= 0}">
                                <tr>
                                    <td class='column_data_cell' colspan="8" align="center">
                                        <bean:message bundle="core.labels" key="core.common.th.empty_table" />
                                    </td>
                                </tr>
                            </c:if>

                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>

    <tr>
        <td valign="center" align="center" class="tabForm">
        <table width="100%" border="0">
            <%--<!-- Tab header -->--%>
            <tr>
                <td colspan="2">
                    <h4 class="dataLabel">
                        <bean:message bundle="core.labels" key="core.report.reportPayment.summaryAmount"/>
                    </h4>
                </td>
            </tr>
           <tr>
                <td class="dataLabel" width="15%">
                       <tmx_ctrl:writeEntityLabel attrName="summaryAmount"
                                                  entityClassName="com.tmx.as.entities.bill.balance.SellerBalance"/>:
                </td>
                <td class="dataField" width="35%">
                    <tmx_ctrl:editBox property="editBoxSummaryAmount"/>
                </td>

                <td class="dataLabel" width="15%">
                    <%--<tmx_ctrl:writeEntityLabel attrName="description"--%>
                                               <%--entityClassName="com.tmx.as.entities.bill.transaction.TransactionSupport"/>:--%>
                </td>
                <td class="dataField" width="35%">
                </td>
            </tr>
        </table>
        </td>
    </tr>

    <tr>
        <td style="text-align: right">
            <tmx_ctrl:imgTablePaging tableName="table"/>
        </td>
    </tr>
</table>
</html:form>