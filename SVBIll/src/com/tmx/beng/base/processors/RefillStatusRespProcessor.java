package com.tmx.beng.base.processors;

import com.tmx.beng.base.*;
import com.tmx.beng.base.cache.DBCache;
import org.apache.log4j.Logger;

import java.util.Date;
import java.text.SimpleDateFormat;

/**
Creates refill status response message and sends it via callback. 
 */
public class RefillStatusRespProcessor extends TaskProcessor {

    public Task process0(Task task, DBCache dbCache, BillingMessageWritable msg) {
        logger.debug("Refill status response...");

        BillingMessageImpl resp = new BillingMessageImpl();
        resp.setOperationName(msg.getOperationName());
        resp.setAttributeString(BillingMessage.BILL_TRANSACTION_NUM,
                msg.getAttributeString(BillingMessage.BILL_TRANSACTION_NUM));
        if(msg.getAttribute(BillingMessage.RECEIPT_NUM) != null){
            resp.setAttribute(BillingMessage.RECEIPT_NUM, msg.getAttribute(BillingMessage.RECEIPT_NUM));
        }
        if(msg.getAttribute(BillingMessage.PAY_ID) != null){
            resp.setAttribute(BillingMessage.PAY_ID, msg.getAttribute(BillingMessage.PAY_ID));
        }
        if(msg.getAttribute(BillingMessage.AMOUNT) != null){
            resp.setAttribute(BillingMessage.AMOUNT, msg.getAttribute(BillingMessage.AMOUNT));
        }

        //to support Life gate integration
        if(msg.getAttribute(BillingMessage.VOUCHER_CODE) != null)
            resp.setAttribute(BillingMessage.VOUCHER_CODE, msg.getAttribute(BillingMessage.VOUCHER_CODE));

        if(msg.getAttribute(BillingMessage.VOUCHER_NOMINAL) != null)
            resp.setAttribute(BillingMessage.VOUCHER_NOMINAL, msg.getAttribute(BillingMessage.VOUCHER_NOMINAL));        

        if(msg.getAttribute(BillingMessage.VOUCHER_SECRET_KEY) != null)
            resp.setAttribute(BillingMessage.VOUCHER_SECRET_KEY, msg.getAttribute(BillingMessage.VOUCHER_SECRET_KEY));

        if(msg.getAttribute(BillingMessage.VOUCHER_BEST_BEFORE_DATE) != null)
            //resp.setAttribute(BillingMessage.VOUCHER_BEST_BEFORE_DATE, new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format((Date)msg.getAttribute(BillingMessage.VOUCHER_BEST_BEFORE_DATE)));
            resp.setAttribute(BillingMessage.VOUCHER_BEST_BEFORE_DATE, (Date)msg.getAttribute(BillingMessage.VOUCHER_BEST_BEFORE_DATE));


        resp.setAttributeString(BillingMessage.STATUS_CODE, msg.getAttributeString(BillingMessage.STATUS_CODE));
        resp.setAttributeString(BillingMessage.STATUS_MESSAGE, msg.getAttributeString(BillingMessage.STATUS_MESSAGE));
        resp.setAttributeString(BillingMessage.WAIT_TIME, String.valueOf(task.getGlobalContext().getReporter().getWaitTimeEstimate()));

        //todo fix that
        Date d = task.getTransaction() != null ?
                task.getTransaction().getRegistrationDate() :
                (Date)msg.getAttribute(BillingMessage.REGISTRATION_TIME);

        resp.setAttribute(BillingMessage.REGISTRATION_TIME, d);

        

        CallBack callBack = task.getCallBack();
        if(callBack != null){
            callBack.sendBilledMessage(resp);

            // Proximan 05.11.2010
            if(resp.getAttribute(BillingMessage.VOUCHER_CODE) != null)
                genlife_logger.info("Response From LifeGate: \n\t"
                        + resp.getAttributeString(BillingMessage.STATUS_CODE) + "\n\t"
                        + resp.getAttributeString(BillingMessage.VOUCHER_NOMINAL) + "\n\t"
                        + resp.getAttributeString(BillingMessage.VOUCHER_CODE) + "\n\t"
                        + resp.getAttributeString(BillingMessage.VOUCHER_SECRET_KEY) + "\n\t"
                        + resp.getAttributeString(BillingMessage.VOUCHER_BEST_BEFORE_DATE));
            //
        }

        return task;
    }

    // Proximan 04.11.2010
    Logger genlife_logger = Logger.getLogger("genlife."+getClass().getName());
    //
}
