package com.tmx.web.base.menuservice;

import com.tmx.as.base.Reconfigurable;
import com.tmx.as.entities.general.user.User;
import com.tmx.as.entities.general.role.Role;
import com.tmx.util.InitException;
import com.tmx.util.Configuration;
import com.tmx.util.cache.FileCache;
import com.tmx.util.cache.CacheException;
import com.tmx.as.exceptions.SecurityException;
import com.tmx.web.base.menuservice.beans.*;
import com.tmx.web.base.*;
import org.apache.log4j.Logger;
import org.apache.commons.digester.Digester;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlException;

import javax.servlet.http.HttpServletRequest;
import java.util.*;
import java.io.File;
import java.io.InputStream;

/**
 */
public class MenuService implements Reconfigurable {
    /** Only single instance available */
    private static MenuService menuService;
    private MenuService.Config serviceConfig = null;
    public static String LOGCLASS_REPORT = "web";
    /** Logger */
    private static Logger logger = Logger.getLogger(LOGCLASS_REPORT + ".MenuService");
    private MenuTemplateCache cache = new MenuTemplateCache();
    private final String SELECTED_MENU_ELEMENT = "selectedMenuElement";
    private final String SELECTED_MENU_NAME = "selectedMenuName";
    private final String STORED_MENU_REQUESTS = "storedMenuRequests";
    /** Map of stored in user's session menu requests [String selectedMenuName][String selectedMenuItem]  */
    private Map storedMenuRequests = null;

    /** to hide default constructor */
    private MenuService(){
    }

    /** Return gate context instance */
    public static MenuService getInstance(){
        if(menuService == null)
            menuService = new MenuService();

        return menuService;
    }

    /** Reload configuration */
    public void reload() throws InitException{
        try {
            logger.debug("Start initialization of the MenuService");
            String menuServiceConfigFile = Configuration.getInstance().getProperty("menu_service.config_file");
            if (menuServiceConfigFile == null)
                throw new InitException("err.menu_service.config_file_is_not_specified");

            File configFile = new File(menuServiceConfigFile);
            Digester digester = new Digester();
            MenuService.Config localConfig = new MenuService.Config();
            digester.push(localConfig);
            addRules(digester);
            digester.parse(configFile);
            serviceConfig = (MenuService.Config)digester.getRoot();
            logger.debug("Successful completed initialization of the Menu service");

        }
        catch (InitException e) {
            //rethrow
            throw e;
        }
        catch (Exception e) {
            throw new InitException("err.menu_service.reload_failed", e);
        }
    }


    public Menu initializeMenu(String templateName, HttpServletRequest request) throws MenuException, SecurityException{
        templateName = (templateName.endsWith(".xml")) ? templateName.substring(0, templateName.length() - ".xml".length()) : templateName;
        templateName = templateName.replace('.', File.separatorChar);
        String templatePath = serviceConfig.getRepository() + File.separator + templateName + ".xml";
        return initializeMenu0(templatePath, templateName, request);
    }

    public String getSelectedMenuElement(String menuName, HttpServletRequest request){
        final String unknownFullName = "";
        if(request == null)
            return unknownFullName;
        Map storedMenuRequests = (Map)SessionEnvironment.getAttr(request.getSession(), STORED_MENU_REQUESTS);
        if(storedMenuRequests == null)
            return unknownFullName;
        return (String)storedMenuRequests.get(menuName);
    }

    /** Collect all menu requests in session. */
    public void listenMenuRequests(HttpServletRequest request){
        String selectedMenuName = request.getParameter(SELECTED_MENU_NAME);
        if(selectedMenuName != null){
            Map storedMenuRequests = (Map)SessionEnvironment.getAttr(request.getSession(), STORED_MENU_REQUESTS);
            if(storedMenuRequests == null){
                storedMenuRequests = new HashMap();
                SessionEnvironment.setAttr(request.getSession(), STORED_MENU_REQUESTS, storedMenuRequests);
            }
            storedMenuRequests.put(selectedMenuName, request.getParameter(SELECTED_MENU_ELEMENT));
        }
    }

    /** Explicitly set selected menu element. Used from writeMenuTag on tag rendering */
    public void setSelectedMenuElement(String selectedMenuName, String selectedMenuElementName, HttpServletRequest request){
        if(request != null){
            Map storedMenuRequests = (Map)SessionEnvironment.getAttr(request.getSession(), STORED_MENU_REQUESTS);
            if(storedMenuRequests == null){
                storedMenuRequests = new HashMap();
                SessionEnvironment.setAttr(request.getSession(), STORED_MENU_REQUESTS, storedMenuRequests);
            }
            storedMenuRequests.put(selectedMenuName, selectedMenuElementName);
        }
    }


    //--------private
    private Menu initializeMenu0(String realTemplateFilePath, String templateName, HttpServletRequest request) throws MenuException, SecurityException{
        RootDocument rootDoc = null;
        try{
            rootDoc = (RootDocument)cache.get(realTemplateFilePath);
            /*This config could be changed according to user's settings. To avoid impact
            of such updates on common template we will use the copy.
            Copy impleneted as re-parsing. */
            rootDoc = RootDocument.Factory.parse(rootDoc.getRoot().getDomNode());
        }
        catch(CacheException e){
            throw new MenuException("err.menu_service.failed_to_get_menu_template", new String[]{realTemplateFilePath}, e);
        }
        catch(XmlException e){
            throw new MenuException("err.menu_service.failed_to_prepare_copy_of_menu_template_via_parsing", new String[]{realTemplateFilePath}, e);
        }

        //Prepare menu config and initialize menu
        Menu menu = new Menu();
        //prepare runtime context
        prepareRuntimeContext(rootDoc, templateName, request);

        menu.init(new MenuConfig(templateName, rootDoc, SessionEnvironment.getLocale(request.getSession(true))));

        //apply security realms
        applySecurityPolicy(menu, request);
        return menu;
    }

    private void applySecurityPolicy(Menu menu, HttpServletRequest request) throws MenuException, SecurityException{
        SecurityService webSecurityService = (SecurityService)ApplicationEnvironment.getService(SecurityService.class);
        User user = webSecurityService.getLoggedInUser(request.getSession());
        Role role = user.getRole();
        if(role == null)
            throw new SecurityException("err.menu_service.user_without_role_found");
        menu.applySecurityPolicy(role.getKey());
    }

    private void prepareRuntimeContext(RootDocument rootDoc, String templateName, HttpServletRequest request){
        //add runtime context
        RuntimeContext runtimeContext = rootDoc.getRoot().getRuntimeContext();
        if(runtimeContext == null)
            runtimeContext = rootDoc.getRoot().addNewRuntimeContext();
        runtimeContext.setMenuName(templateName);

        RuntimeContext.SelectedElement selectedElement = runtimeContext.getSelectedElement();
        if(selectedElement == null)
            selectedElement = runtimeContext.addNewSelectedElement();

        selectedElement.setFullName(getSelectedMenuElement(templateName, request));
        selectedElement.setHiddenElementName(SELECTED_MENU_ELEMENT);
        selectedElement.setHiddenMenuName(SELECTED_MENU_NAME);

        RuntimeContext.WebApplication webApplication = runtimeContext.getWebApplication();
        if(webApplication == null)
            webApplication = runtimeContext.addNewWebApplication();
        webApplication.setDeploymentName(ApplicationEnvironment.getAppDeploymentName());
    }

    //---------------private methods
    private void addRules(Digester d){
        d.addSetProperties("menuService");
    }

    public class Config{
        private String repository = null;

        //--------properties

        public String getRepository() {
            return repository;
        }

        public void setRepository(String repository) throws Exception{
            this.repository = Configuration.getInstance().substituteVariablesInString(repository);
        }
    }

    //wrapper for RootDocument
    public class MenuConfig{
        private RootDocument rootDoc = null;
        private Locale locale = null;
        private String templateName = null;

        MenuConfig(String menuTemplateName, RootDocument rootDoc, Locale locale){
            this.templateName = menuTemplateName;
            this.rootDoc = rootDoc;
            this.locale = locale;
        }

        public RootDocument getRootDoc() {
            return rootDoc;
        }

        public Locale getLocale() {
            return locale;
        }

        public String getTemplateName() {
            return templateName;
        }
    }

    private class MenuTemplateCache extends FileCache {

        private MenuTemplateCache() {
            maxSize = 100;
            cleanCount = 10;
            cleanType = CleanAlgorithm.Type.CLEAN_N_SELDOM_ACCESSED;
        }

        protected Object getRealObjectFromFile(InputStream is, String templateFilePath) throws CacheException {
            RootDocument rootDoc = null;
            try{
                // Bind the instance to the generated XMLBeans types.
                rootDoc = RootDocument.Factory.parse(is);
            }
            catch(Throwable e){
                throw new CacheException("err.menu_service.failed_to_parse_menu_template", new String[]{templateFilePath}, e);
            }

            //validate
            ArrayList errors = new ArrayList();
            XmlOptions opts = new XmlOptions();
            opts.setErrorListener(errors);
            if(!rootDoc.validate(opts)){
                StringBuffer errorBuffer = new StringBuffer("Validation failed of XML "+templateFilePath+":\n");
                Iterator iter = errors.iterator();
                while(iter.hasNext())
                    errorBuffer.append("   >> " + iter.next() + "\n");
                logger.error(errorBuffer.toString());
                throw new CacheException("err.menu_service.template_validation_failed", new String[]{templateFilePath});
            }
            return rootDoc;
        }
    }

}
