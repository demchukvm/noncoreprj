/*//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                        L O C A L     V A R I A N T
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////*/
/*
package com.tmx.web.actions.core.tools;

import com.tmx.beng.base.MediumResolver;
import com.tmx.web.forms.core.tools.GatesProductsForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForm;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.tmx.web.base.BasicDispatchedAction;

public class GatesProductsAction extends BasicDispatchedAction
{
    private final String FORWARD = "gatesProducts";

    protected ActionForward defaultAction(ActionMapping mapping,
                                          ActionForm form,
                                          HttpServletRequest request,
                                          HttpServletResponse response,
                                          String methodName) throws Throwable {

        GatesProductsForm gatesProductsForm = (GatesProductsForm) form;

        if(gatesProductsForm.getMediumMap().isEmpty())  // init mediumMap
        {
            return initMap(mapping, form, request, response);
        }
        else
        {
            if(!gatesProductsForm.getGateName().equals(""))                 // change gate
            {
                return gateChange(mapping, form, request, response);
            }
            else
            {
                return mapping.findForward(FORWARD);  // do nothing
            }
        }
    }

    private ActionForward initMap(ActionMapping mapping,
                                     ActionForm form,
                                     HttpServletRequest request,
                                     HttpServletResponse response) throws Throwable
    {
        GatesProductsForm gatesProductsForm = (GatesProductsForm) form;

        gatesProductsForm.setMediumMap(MediumResolver.getInstance().getMediumMap());
        if(gatesProductsForm.getMediumMap().isEmpty() || gatesProductsForm.getMediumMap() == null)
            gatesProductsForm.setMediumMap(MediumResolver.getInstance().getMediumAccessName());

        return mapping.findForward(FORWARD);   // do nothing
    }


    private ActionForward gateChange(ActionMapping mapping,
                                     ActionForm form,
                                     HttpServletRequest request,
                                     HttpServletResponse response) throws Throwable {

        GatesProductsForm gatesProductsForm = (GatesProductsForm) form;

        MediumResolver.getInstance().changeMediumResolver(gatesProductsForm.getProductName(), gatesProductsForm.getGateName());
        gatesProductsForm.setMediumMap(MediumResolver.getInstance().getMediumAccessName());

        gatesProductsForm.setProductName("");
        gatesProductsForm.setGateName("");

        return mapping.findForward(FORWARD);
    }

}
*/

/*//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                        S E R V E R     V A R I A N T
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////*/

package com.tmx.web.actions.core.tools;

import com.tmx.as.modules.SpringModule;
import com.tmx.beng.access.GateConnection;
import com.tmx.web.forms.core.tools.GatesProductsForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForm;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.tmx.web.base.BasicDispatchedAction;

public class GatesProductsAction extends BasicDispatchedAction
{
    private final String FORWARD = "gatesProducts";

    protected ActionForward defaultAction(ActionMapping mapping,
                                          ActionForm form,
                                          HttpServletRequest request,
                                          HttpServletResponse response,
                                          String methodName) throws Throwable {

        GatesProductsForm gatesProductsForm = (GatesProductsForm) form;

        if(gatesProductsForm.getMediumMap().isEmpty())  // init mediumMap
        {
            return initMap(mapping, form, request, response);
        }
        else
        {
            if(!gatesProductsForm.getGateName().equals(""))                 // change gate
            {
                return gateChange(mapping, form, request, response);
            }
            else
            {
                return mapping.findForward(FORWARD);  // do nothing
            }
        }
    }

    private ActionForward initMap(ActionMapping mapping,
                                     ActionForm form,
                                     HttpServletRequest request,
                                     HttpServletResponse response) throws Throwable
    {
        GatesProductsForm gatesProductsForm = (GatesProductsForm) form;

        GateConnection conn = (GateConnection) SpringModule.getApplicationContext().getBean("gateConnection");
        gatesProductsForm.setMediumMap(conn.getMediumMap());
        if(gatesProductsForm.getMediumMap().isEmpty() || gatesProductsForm.getMediumMap() == null)
            gatesProductsForm.setMediumMap(conn.getMediumAccessName());

        return mapping.findForward(FORWARD);   // do nothing
    }

    private ActionForward gateChange(ActionMapping mapping,
                                     ActionForm form,
                                     HttpServletRequest request,
                                     HttpServletResponse response) throws Throwable {

        GatesProductsForm gatesProductsForm = (GatesProductsForm) form;

        GateConnection conn = (GateConnection) SpringModule.getApplicationContext().getBean("gateConnection");
        conn.changeMediumResolver(gatesProductsForm.getProductName(), gatesProductsForm.getGateName());
        gatesProductsForm.setMediumMap(conn.getMediumAccessName());

        gatesProductsForm.setProductName("");
        gatesProductsForm.setGateName("");

        return mapping.findForward(FORWARD);
    }

}
