package com.tmx.gate.datastream.reporter;

import com.tmx.gate.datastream.base.DataStreamProcessorEmail;
import com.tmx.gate.datastream.base.DataStreamException;
import com.tmx.as.exceptions.DatabaseException;

import javax.mail.*;
import java.util.Properties;

/**
   Abstract reporter to receive email via POP3 protocol.
 */
public abstract class AbstractPop3Reporter extends DataStreamProcessorEmail {

    //abstract methods
    public abstract String getReporterName();
    public abstract void receiveReport(Message msg) throws DataStreamException;

    protected void executeDataStreamProcessing() throws DataStreamException {
        Store store = null;
        Folder folder = null;
        try{
            Properties props = getProperties(getReporterName());
            Session session = Session.getDefaultInstance(props, null);
            store = session.getStore("pop3");
            store.connect(
                    props.getProperty("connection.host"),
                    props.getProperty("user.login"),
                    props.getProperty("user.password"));

            //get default folder
            folder = store.getDefaultFolder();
            if(folder == null)
                throw new DataStreamException("No default folder");
            //get INBOX
            folder = folder.getFolder("INBOX");
            if(folder == null)
                throw new DataStreamException("No POP3 INBOX");
            //open the folder for read only
            folder.open(Folder.READ_WRITE);
            //get the message wrappers and process them
            Message[] msgs = folder.getMessages();
            for (int i = 0; i < msgs.length; i++){
                receiveReport(msgs[i]);
                msgs[i].setFlag(Flags.Flag.DELETED, true);
            }
        }
        catch(MessagingException e){
            throw new DataStreamException("Failed to receive email", e);
        }
        finally{
            try{
                if (folder!=null) folder.close(true);
                if (store!=null) store.close();
            }
            catch (Exception e) {
                logger.error("pop3 store close failed", e);
            }
        }
    }
}
