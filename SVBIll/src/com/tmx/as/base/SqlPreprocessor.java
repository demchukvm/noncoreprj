package com.tmx.as.base;

import com.tmx.as.exceptions.DatabaseException;
import java.util.*;

import org.hibernate.dialect.Dialect;
import org.hibernate.engine.SessionFactoryImplementor;
import org.hibernate.engine.FilterDefinition;
import org.hibernate.sql.Template;
import org.hibernate.cfg.Configuration;

/**

 */
public class SqlPreprocessor {
    private List wrappedFiltersParams = null;
    // Inject defined filters instead of specified placeholders in SQL.
    // Placeholders are implemented as SQL comments :
    // Placeholder in the SQL: /** w1 */
    // Then placeholder name: w1
    public String injectQueryFilters(String query, Class entityClass, FilterWrapper[] filters) throws DatabaseException {
        init();
        if(filters == null || filters.length <= 0)
            return query;

        Configuration cfg = HibernateSessionFactoryProvider.getConfiguration(entityClass);
        validateFilters(cfg, filters);
        Dialect dialect = ((SessionFactoryImplementor)HibernateSessionFactoryProvider.getSessionFactory(entityClass)).getDialect();
        Map filtersMetadata = cfg.getClassMapping(entityClass.getName()).getFilterMap();

        Map byPlaceholderGroup = new HashMap();
        StringBuffer simpleFiltersSql = null;
        for(int i=0; i<filters.length; i++){
            String filterSql = (String)filtersMetadata.get(filters[i].getName());
            if(filters[i].getPlaceholder() != null){
                StringBuffer filtersSql = (StringBuffer)byPlaceholderGroup.get(filters[i].getPlaceholder());
                filtersSql = appendFilterSql(filtersSql, filterSql, dialect, filters[i]);
                byPlaceholderGroup.put(filters[i].getPlaceholder(), filtersSql);
            }
            else{
                simpleFiltersSql = appendFilterSql(simpleFiltersSql, filterSql, dialect, filters[i]);
            }
        }

        for(Iterator iter = byPlaceholderGroup.keySet().iterator(); iter.hasNext();){
            String placeholder = (String)iter.next();
            //String wrappedPlaceholder = "-- " + placeholder + " -- \n";
            //query = query.replaceFirst("(?i)--\\ *"+placeholder+"\\ *--", wrappedPlaceholder + byPlaceholderGroup.get(placeholder).toString());
            String wrappedPlaceholder = "'" + placeholder + "'='" + placeholder +"' and ";
            query = query.replaceFirst("(?i)'"+placeholder+"\\ *'='\\ *"+placeholder+"'[\\ \\n]*and", wrappedPlaceholder + byPlaceholderGroup.get(placeholder).toString());
        }

        final String whereLexem = "where[\\ *|\\r|\\n]";
        int whereClauseCount = query.split(whereLexem).length - 1;
        if(whereClauseCount > 1 && simpleFiltersSql != null && simpleFiltersSql.length() > 0){
            throw new DatabaseException("err.filter_injector.simple_filter_used_with_subquery", new String[]{query, simpleFiltersSql.toString()}); 
        }
        else if(whereClauseCount == 1 && simpleFiltersSql != null && simpleFiltersSql.length() >0){
            query = query.replaceFirst("(?i)"+whereLexem, " where \n "+simpleFiltersSql.toString());
        }
        else if (whereClauseCount < 1){
            throw new DatabaseException("err.filter_injector.any_where_clause_not_found", new String[]{query});            
        }

        return query;
    }

    private StringBuffer appendFilterSql(StringBuffer filterSetSql, String filterSql, Dialect dialect, FilterWrapper filterWrapper) throws DatabaseException{
        if(filterSql == null || filterSql.trim().length() <=0)
            return filterSetSql;

        final String FILTER_CONJUNCTION = " and ";//" and \n";
        final String HEAD_PARENTHESIS = " (";
        final String TAIL_PARENTHESIS = ") ";
        if(filterSetSql == null || filterSetSql.length() <= 0)
            filterSetSql = new StringBuffer();


        List wrappedFilterParams = new ArrayList();
        for(Iterator iter = filterWrapper.getParamNames(); iter.hasNext();){
            String paramName = (String)iter.next();
            Object paramValue = filterWrapper.getParamValue(paramName);
            //convert to string
//            String strParamValue = paramValue != null ? paramValue.toString() : null;
//            if(strParamValue == null)
//                throw new DatabaseException("err.filter_injector.filter_parameter_value_is_null");
//            strParamValue = "'" + strParamValue + "'";
//            filterSql = filterSql.replaceFirst(":"+paramName, strParamValue);

            //replcace filter parameter with qury parameter
            QueryParameterWrapper wrappedFilterParam = new QueryParameterWrapper(generateWrappedFilterParameterName(filterWrapper.getName(), paramName), paramValue);
            wrappedFilterParams.add(wrappedFilterParam);
            filterSql = filterSql.replaceFirst(":"+paramName, ":"+wrappedFilterParam.getName());
        }

        final String aliasPlaceHolder = "___ALIAS_PLACEHOLDER___";
        filterSetSql.append(HEAD_PARENTHESIS).
                append(
                    Template.renderWhereStringTemplate(filterSql, aliasPlaceHolder, dialect).
                        replaceAll(aliasPlaceHolder+"\\.", "")//replace all alias placeholders and following dots = $FILTER_PLACEHOLDER$
                ).
                append(TAIL_PARENTHESIS).
                append(FILTER_CONJUNCTION);

        //assign wrapped filters params to property
        this.wrappedFiltersParams.addAll(wrappedFilterParams);

        return filterSetSql;
    }

    public QueryParameterWrapper[] getWrappedFiltersParams() {
        return (QueryParameterWrapper[])wrappedFiltersParams.toArray(new QueryParameterWrapper[0]);
    }

    private String generateWrappedFilterParameterName(String filterName, String paramName){
        return (filterName + "__" + paramName).replaceAll("\\.", "__");//do not use dot - it is incorrect interpreted on hql query building
    }

    private void init(){
        this.wrappedFiltersParams = new ArrayList();
    }

    private void validateFilters(Configuration cfg, FilterWrapper[] filters) throws DatabaseException{
        Map filtersDefinition = cfg.getFilterDefinitions();
        for(int i=0; i < filters.length; i++){
            FilterDefinition filterDefinition = (FilterDefinition)filtersDefinition.get(filters[i].getName());
            if(filterDefinition == null)
                throw new DatabaseException("err.filter_injector.filter_definition_not_found", new String[]{filters[i].getName()});
            Set filterParams = filterDefinition.getParameterNames();
            for(Iterator iter = filters[i].getParamNames(); iter.hasNext();){
                String paramName = (String)iter.next();
                if(!filterParams.contains(paramName))
                    throw new DatabaseException("err.filter_injector.filter_parameter_not_found", new String[]{filters[i].getName(), paramName});
            }
        }
    }

    //this implementation supports only hql queries but doesn't sql. But it is possible to implement.
    public String injectHqlQueryOrders(String query, OrderWrapper[] orders) throws DatabaseException {
        if(orders == null || orders.length <= 0)
            return query;

        StringBuffer ordersSql = new StringBuffer();
        for(int i=0; i<orders.length; i++){
            if(i > 0)
                ordersSql.append(", ");
            appendOrderSql(ordersSql, orders[i]);
        }

        final String orderLexem = "order\\ +?by[\\ *|\\r|\\n]";
        String[] clauseParts = query.split(orderLexem);
        int orderClauseCount = clauseParts.length - 1;
        StringBuffer queryBuffer = new StringBuffer();
        if(orderClauseCount >= 1){
            //to inject dynamic order before static
            //proceed with last one
            //for(int i=0; i<clauseParts.length - 1; i++)
            //    queryBuffer.append(clauseParts[i]).append(" order by ");
            //queryBuffer.append(ordersSql).append(", ").append(clauseParts[orderClauseCount]);
            //to inject dynamic order after static
            queryBuffer.append(query.trim()).append(", ").append(ordersSql);
        }
        else{// if <=0
            queryBuffer.append(query.trim()).append(" order by ").append(ordersSql);
        }
        return queryBuffer.toString();
    }

    private void appendOrderSql(StringBuffer ordersSql, OrderWrapper orderWrapper){
        String alias = orderWrapper.getAlias() != null ? orderWrapper.getAlias() + ".": ""; 
        ordersSql.append(alias).append(orderWrapper.getOrderByFieldName()).append(" ").append(orderWrapper.getOrderType().toString());
    }
}
