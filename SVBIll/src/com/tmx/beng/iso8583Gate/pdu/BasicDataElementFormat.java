package com.tmx.beng.iso8583Gate.pdu;

/**
Parent for common and particular data element formats
 */
public class BasicDataElementFormat {
    public final static String DEF_BITMAP = "BitMap";
    public final static String DEF_MESSAGETYPEID = "MessageTypeId";
    public final static String DEF_PROCESSINGCODE = "ProcessingCode";
    public final static String DEF_RESPONSECODE = "ResponseCode";

    protected int bitmapNumber;
    protected String name;

    //-----------Properties
    public int getBitmapNumber() {
        return bitmapNumber;
    }

    public String getName() {
        return name;
    }
    

}
