package com.tmx.web.actions.core.dictionary.territory;

import com.tmx.web.base.BasicDispatchedAction;
import com.tmx.web.base.Roadmap;
import com.tmx.web.forms.core.dictionary.territory.InfoDistrictForm;
import com.tmx.as.base.EntityManager;
import com.tmx.as.entities.network.territory.Country;
import com.tmx.as.entities.network.territory.Region;
import com.tmx.as.entities.network.territory.District;
import com.tmx.as.blogic.AddressManager;

import java.util.List;

import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForm;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class InfoDistrictAction extends BasicDispatchedAction {

    private static final String FORWARD_MANAGE_ACTION = "manageAction";
    private static final String FORWARD_INFO_PAGE = "infoPage";
    private static final String FORWARD_MANAGE_DELETE_ACTION = "manageDeleteAction";

    public void bindData(EntityManager entityManager, InfoDistrictForm form) throws Throwable{
        List countries = entityManager.RETRIEVE_ALL(Country.class, null);
        form.getScrollBoxCountry().bind(countries);
    }

    public ActionForward preloadForCreate(ActionMapping mapping,
                                          ActionForm form,
                                          HttpServletRequest request,
                                          HttpServletResponse response) throws Throwable {
        InfoDistrictForm districtForm = (InfoDistrictForm) form;
        bindData(new EntityManager(), districtForm);
        districtForm.bindData(new District());

        return mapping.findForward(FORWARD_INFO_PAGE);
    }

    public ActionForward preloadForUpdate(ActionMapping mapping,
                                          ActionForm form,
                                          HttpServletRequest request,
                                          HttpServletResponse response) throws Throwable {
        InfoDistrictForm districtForm = (InfoDistrictForm) form;

        EntityManager entityManager = new EntityManager();

        bindData(entityManager, districtForm);
        District district = (District) new AddressManager().RETRIEVE(District.class, districtForm.getSelectedId(), new String[]{"country"});
        districtForm.bindData(district);

        return mapping.findForward(FORWARD_INFO_PAGE);
    }

    public ActionForward update(ActionMapping mapping,
                                ActionForm form,
                                HttpServletRequest request,
                                HttpServletResponse response) throws Throwable {
        validateTransactionToken(request);
        InfoDistrictForm districtForm = (InfoDistrictForm) form;
        districtForm.validate(mapping.findForward(FORWARD_INFO_PAGE));

        District district = districtForm.populateFromControls(new District());
        new EntityManager().SAVE(district);

        return mapping.findForward(FORWARD_MANAGE_ACTION);
    }

    public ActionForward preloadForDelete(ActionMapping mapping,
                                          ActionForm form,
                                          HttpServletRequest request,
                                          HttpServletResponse response) throws Throwable {
        Roadmap roadmap = getRoadmap();
        roadmap.collectParameter("/core/message/confirmation", "leftMenuName", "dictionaries");
        roadmap.setPageForward("confirmation","onOk","local:" + this.getName() + "#" + FORWARD_MANAGE_DELETE_ACTION);
        roadmap.setPageForward("confirmation","onCancel","local:" + this.getName() + "#" + FORWARD_MANAGE_ACTION);
        
        return mapping.findForward("confirmation");
    }

    public ActionForward delete(ActionMapping mapping,
                                ActionForm form,
                                HttpServletRequest request,
                                HttpServletResponse response) throws Throwable {
        InfoDistrictForm districtForm = (InfoDistrictForm) form;
        new EntityManager().DELETE(District.class, districtForm.getSelectedId());

        return mapping.findForward(FORWARD_MANAGE_ACTION);
    }

    public ActionForward defaultAction(ActionMapping mapping,
                                       ActionForm form,
                                       HttpServletRequest request,
                                       HttpServletResponse response,
                                       String methodName) throws Throwable {

        return preloadForCreate(mapping, form, request, response);
    }
}