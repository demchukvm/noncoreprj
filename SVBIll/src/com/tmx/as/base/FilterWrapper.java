package com.tmx.as.base;

import com.tmx.as.resolver.ResolverException;

import java.util.HashMap;
import java.util.Iterator;

/** Use to create and fill Filters before the DB session will be opened. */
public class FilterWrapper implements Cloneable {
    private String name;
    /** used to inject this filter's sql code instead of the dedicated placeholder in
     * the sql (hql) query (optional) */
    private String placeholder = null;
    private HashMap params = new HashMap();

    /** To hide this constructor */
/* RM:  sometimes we need to create OrderWrapper instance before defining
    private FilterWrapper(){
    }*/

    public FilterWrapper(){
    }

    public FilterWrapper(String name){
        this.name = name;
    }

    public String getName(){
        return this.name;
    }

    public String getPlaceholder() {
        return placeholder;
    }

    public void setPlaceholder(String placeholder) {
        this.placeholder = placeholder;
    }

    public void setParameter(String name, Object value){
        params.put(name, value);
    }

    public void setParameter(FilterParameterWrapper parameter){
        setParameter(parameter.getParameterName(), parameter.getParameterValue());
    }

    public Iterator getParamNames(){
        return params.keySet().iterator();
    }

    public Object getParamValue(String paramName){
        return params.get(paramName);
    }

    /** Publish geven filters array into the string
     * @param filters array of filters to publish into String
     * @param formatString enble formating result string */
    public static String filtersToString(FilterWrapper[] filters, boolean formatString){
        StringBuffer filtersString = new StringBuffer();
        //Pepare formatting strings
        String retSymb = (formatString) ? "\r" : "";
        String tabSymb = (formatString) ? "\t" : "";

        if(filters != null){
            for(int i = 0; i < filters.length; i++){
                filtersString.append("[name='").append(filters[i].getName()).append("'");
                if(filters[i].getPlaceholder() != null)
                    filtersString.append(", placeholder='").append(filters[i].getPlaceholder()).append("'");
                Iterator paramNames = filters[i].params.keySet().iterator();
                if(paramNames == null){
                    filtersString.append("]");
                }
                else{
                    while(paramNames.hasNext()){
                        String paramName = (String)paramNames.next();
                        Object paramValue = filters[i].params.get(paramName);
                        String paramType = null;
                        if(paramValue != null){
                            paramType = paramValue.getClass().getName();
                            paramValue = paramValue.toString();
                        }
                        filtersString.append(retSymb).append(tabSymb).append(" (param='").append(paramName).append("' ");
                        filtersString.append("value='").append(paramValue).append("' ");
                        filtersString.append("type='").append(paramType).append("')");
                    }
                }
                filtersString.append("] ").append(retSymb);
            }
        }
        //Return NULL instead of empty String. It is more obviosly for publishing
        String result = filtersString.toString().trim();
        return (result.length() > 0) ? result : "[no filters]";
    }

    /** Creates new FilterWrapper[] array with size have increased for one position.
     * Then put given filter into the last cell of new array. */
    public static FilterWrapper[] appendToFilters(FilterWrapper[] filters, FilterWrapper filter){
        if(filters == null)
            filters = new FilterWrapper[0];
        if(filter == null)
            return filters;
        FilterWrapper[] newFilters = new FilterWrapper[filters.length + 1];
        System.arraycopy(filters, 0, newFilters, 0, filters.length);
        newFilters[newFilters.length-1] = filter;
        return newFilters;
    }

    public void set(String fieldName, String fieldValue) throws ResolverException {
        if(fieldName.toLowerCase().equals("name")){
            name = fieldValue;
            return;
        }
        else if (fieldName.toLowerCase().equals("placeholder")) {
            placeholder = fieldValue;
            return;
        }
        //RM: throw exception if field not found
        throw new ResolverException("err.resolver.unknown_parameter_for", new String[]{this.getClass().getName(), fieldName, fieldValue.toString()});
    }
}
