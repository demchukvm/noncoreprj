<%@ page language="java" pageEncoding="UTF-8" contentType="text/html; charset=utf-8" isELIgnored="false" %>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-nested" prefix="nested" %>
<%@ taglib uri="http://tmx.com/taglib/tmx-controls" prefix="tmx_ctrl" %>
<%@ taglib uri="http://tmx.com/taglib/tmx-logic" prefix="tmx_logic" %>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html" %>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean" %>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>

<html:form action="/core/report/reportOperatorBalanceAction">
    <table width="100%" border="0">
        <!--FILTER SET-->
        <tr>
            <td valign="top" width="100%" align="left">
                <tmx_ctrl:panelFilterSet tableName="table">
                 <table>
                    <tr>
                        <td class="dataLabel">
                            <bean:message bundle="core.labels" key="core.report.reportPayment.period"/>&nbsp;<bean:message bundle="core.labels" key="core.monitor.timeFrom"/>:
                        </td>
                        <td class="dataField">
                            <tmx_ctrl:timeSelector property="table.filterSet.filter(by_FromDate).parameter(date).control"/>
                        </td>

                        <td class="dataLabel">
                            <bean:message bundle="core.labels" key="core.monitor.timeTo"/>:
                        </td>
                        <td class="dataField">
                            <tmx_ctrl:timeSelector property="table.filterSet.filter(by_ToDate).parameter(date).control"/>
                        </td>
                    </tr>
                    <tr>
                        <td class="dataLabel">
                            <bean:message bundle="core.labels" key="core.report.reportPayment.paymentSize"/>&nbsp;
                            <bean:message bundle="core.labels" key="core.report.reportPayment.from"/>:
                        </td>
                        <td class="dataField">
                            <tmx_ctrl:editBox property="table.filterSet.filter(by_client_amount_from).parameter(from_amount).control"/>
                        </td>

                        <td class="dataLabel">
                            <bean:message bundle="core.labels" key="core.monitor.timeTo"/>:
                        </td>
                        <td class="dataField">
                            <tmx_ctrl:editBox property="table.filterSet.filter(by_client_amount_to).parameter(to_amount).control"/>
                        </td>
                    </tr>
                    <tr>
                       <td class="dataField">
                            <tmx_ctrl:writeEntityLabel
                                attrName="{@name}"
                                entityClassName="com.tmx.as.entities.bill.operator.Operator"/>:
                        </td>
                        <td class="dataField">
                            <tmx_ctrl:selectBox
                                    property="table.filterSet.filter(by_operator).parameter(operator).control"/>
                        </td>

                        <td class="dataField">
                            <tmx_ctrl:writeEntityLabel
                                attrName="{@name}"
                                entityClassName="com.tmx.as.entities.bill.transaction.TransactionType"/>:
                        </td>
                        <td class="dataField">
                         <tmx_ctrl:selectBox
                                property="table.filterSet.filter(by_transaction_type).parameter(type).control"/>
                        </td>

                        <td class="dataField">
                            <tmx_ctrl:writeEntityLabel
                                attrName="paymentReason"
                                entityClassName="com.tmx.as.entities.bill.transaction.TransactionSupport"/>:
                        </td>
                        <td class="dataField">
                         <tmx_ctrl:editBox property="editBoxPaymentReason"/>
                        </td>
                     </tr>
                 </table>
                </tmx_ctrl:panelFilterSet>
            </td>
        </tr>
        <!--TABLE-->
        <tr>
            <td valign="top" width="100%" align="left">
                 <table class="one_outer_cell_table" cellspacing="0" cellpadding="0" width="100%">
                     <tr>
                         <td>
                             <table cellspacing="1" cellpadding="1" width="100%">
                                 <!--Table header-->
                                 <tr align="center">
                                     <th class="column_header_cell">
                                        <tmx_ctrl:imgTableOrdering tableName="table"
                                                                   orderByFieldName="transactionTime"
                                                                   labelBundle="as:com.tmx.as.entities.bill.transaction.ClientTransaction"/>
                                    </th>
                                     <th class="column_header_cell">
                                        <tmx_ctrl:imgTableOrdering tableName="table"
                                                                   orderByFieldName="operator.name"
                                                                   labelBundle="as:com.tmx.as.entities.bill.operator.Operator"/>
                                    </th>
                                     <th class="column_header_cell">
                                        <tmx_ctrl:imgTableOrdering tableName="table"
                                                                   orderByFieldName="amount"
                                                                   labelBundle="as:com.tmx.as.entities.bill.transaction.OperatorBalanceTransaction"/>
                                    </th>
                                     <th class="column_header_cell">
                                        <tmx_ctrl:imgTableOrdering tableName="table"
                                                                   orderByFieldName="transactionSupport.paymentReason"
                                                                   labelBundle="as:com.tmx.as.entities.bill.transaction.TransactionSupport"/>
                                    </th>
                                    <th class="column_header_cell">
                                        <tmx_ctrl:imgTableOrdering tableName="table"
                                                                   orderByFieldName="login"
                                                                   labelBundle="as:com.tmx.as.entities.general.user.User"/>
                                    </th>
                                    <th class="column_header_cell">
                                        <tmx_ctrl:imgTableOrdering tableName="table"
                                                                   orderByFieldName="transactionSupport.description"
                                                                   labelBundle="as:com.tmx.as.entities.bill.transaction.TransactionSupport"/>
                                    </th>
                                    <th class="column_header_cell">
                                        <tmx_ctrl:imgTableOrdering tableName="table"
                                                                   orderByFieldName="type.name"
                                                                   labelBundle="as:com.tmx.as.entities.bill.transaction.TransactionType"/>
                                    </th>
                                </tr>
                                 <!-- Table data -->
                                <nested:iterate id="el" property="table.data" type="com.tmx.as.entities.bill.transaction.OperatorBalanceTransaction">
                                    <tr>
                                        <td class='column_data_cell'>
                                            <fmt:formatDate value="${el.transactionTime}" pattern="yyyy/MM/dd HH:mm:ss"/>
                                        </td>
                                        <td class='column_data_cell'>
                                            <tmx_ctrl:write name="el" property="operator.name"ignore="true"/>
                                        </td>
                                        <td class='column_data_cell'  align="right">
                                            <tmx_ctrl:write name="el" property="amount" ignore="true" format="0.00" />
                                        </td>
                                        <td class='column_data_cell'>
                                            <tmx_ctrl:write name="el" property="transactionSupport.paymentReason" ignore="true"/>
                                        </td>
                                        <td class='column_data_cell'>
                                            <tmx_ctrl:write name="el" property="transactionSupport.userActor.login" ignore="true"/>
                                        </td>
                                        <td class='column_data_cell'>
                                            <tmx_ctrl:write name="el" property="transactionSupport.description" ignore="true"/>
                                        </td>
                                        <td class='column_data_cell'>
                                            <tmx_ctrl:write name="el" property="type.name" ignore="true"/>
                                        </td>
                                    </tr>
                                </nested:iterate>
                                <!-- Empty row -->
                                <nested:define id="tableSize" property="table.resultSize" type="java.lang.Integer"/>
                                <c:if test="${tableSize <= 0}">
                                    <tr>
                                        <td class='column_data_cell' colspan="7" align="center">
                                            <bean:message bundle="core.labels" key="core.common.th.empty_table" />
                                        </td>
                                    </tr>
                                </c:if>

                             </table>
                         </td>
         </tr>
    </table>
    <tr>
        <td style="text-align: right">
            <tmx_ctrl:imgTablePaging tableName="table"/>
        </td>
    </tr>

    <tr>
        <td valign="center" align="center" class="tabForm">
            <table width="100%" border="0">
            <%--<!-- Form header -->--%>
                <tr>
                    <td colspan="2">
                        <h4 class="dataLabel">
                            <bean:message bundle="core.labels" key="core.transaction.clienttransaction.infoclienttransactionform.titleAmount"/>
                        </h4>
                    </td>
                </tr>
                <tr>
                    <td class="dataLabel">
                        <tmx_ctrl:writeEntityLabel
                                attrName="successTransactions"
                                entityClassName="com.tmx.as.entities.bill.transaction.ClientTransaction"/>:
                    </td>
                    <td class="dataField">
                        <tmx_ctrl:editBox
                                property="editBoxSuccessTransaction" />
                    </td>


                    <td class="dataLabel">
                        <tmx_ctrl:writeEntityLabel
                                attrName="allTransactions"
                                entityClassName="com.tmx.as.entities.bill.transaction.ClientTransaction"/>:
                    </td>
                    <td class="dataField">
                        <tmx_ctrl:editBox
                                property="editBoxAllTransaction"/>
                    </td>
                </tr>

                <tr>
                    <td class="dataLabel">
                        <tmx_ctrl:writeEntityLabel
                                attrName="successTransactionAmount"
                                entityClassName="com.tmx.as.entities.bill.transaction.ClientTransaction"/>:
                    </td>
                    <td class="dataField">
                        <tmx_ctrl:editBox
                                property="editBoxSuccessTransAmount"/>
                    </td>


                    <td class="dataLabel">
                        <tmx_ctrl:writeEntityLabel
                                attrName="allTransactionAmount"
                                entityClassName="com.tmx.as.entities.bill.transaction.ClientTransaction"/>:
                    </td>
                    <td class="dataField">
                        <tmx_ctrl:editBox
                                property="editBoxAllTransAmount"/>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    </table>
</html:form>