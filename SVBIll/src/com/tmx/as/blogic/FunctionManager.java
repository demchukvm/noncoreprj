package com.tmx.as.blogic;

import com.tmx.as.entities.bill.function.ActualFunctionParameter;
import com.tmx.as.entities.bill.function.FunctionType;
import com.tmx.as.entities.bill.terminal.Terminal;
import com.tmx.as.exceptions.DatabaseException;
import com.tmx.as.base.EntityManager;
import com.tmx.as.base.FilterWrapper;
import com.tmx.as.base.criterion.CriterionWrapper;
import com.tmx.as.base.criterion.Restrictions;
import com.tmx.util.StructurizedException;
import com.tmx.beng.base.StatusDictionary;
import com.tmx.beng.csapi.java.base.BillingClientApi;
import com.tmx.beng.csapi.java.base.ClientMessages;
import com.tmx.beng.csapi.java.base.DefaultParamsReq;
import com.tmx.beng.csapi.java.base.ValidateParamReq;

import java.util.List;
import java.util.ArrayList;


public class FunctionManager extends EntityManager{

    public void saveParameter(ActualFunctionParameter parameter) throws DatabaseException {
        SAVE(parameter);
    }

    public ActualFunctionParameter getParameter(Long parameterId) throws DatabaseException{
        return (ActualFunctionParameter)RETRIEVE(ActualFunctionParameter.class, parameterId, null);
    }

    public List getParameters(Long functionInstanceID) throws DatabaseException{
        CriterionWrapper[] criterions = new CriterionWrapper[] {
                        Restrictions.eq("functionInstanceId", functionInstanceID)};
        return  RETRIEVE_ALL(ActualFunctionParameter.class, criterions, null);
    }

    public void deleteParameter(Long parameterId) throws DatabaseException{
        DELETE(ActualFunctionParameter.class, parameterId);
    }

    public List getAllFunctionType() throws DatabaseException{
        return new EntityManager().RETRIEVE_ALL(FunctionType.class, null);
    }

    public List getAllFunctionTypes() throws DatabaseException {
        return new EntityManager().RETRIEVE_ALL(FunctionType.class);
    }

    public FunctionType getFunctionType(Long functionTypeid) throws DatabaseException {
        return (FunctionType) new EntityManager().RETRIEVE(FunctionType.class, functionTypeid, null);
    }

    public FunctionType getFunctionType(String functionTypeId) throws DatabaseException {
        Long id = Long.valueOf(functionTypeId);
        return (FunctionType) new EntityManager().RETRIEVE(FunctionType.class, id, null);
    }

    public List getActualFunctionParams(Long functionTypeId) throws BlogicException {
        try {
            DefaultParamsReq msg = new BillingClientApi().createDefaultParamsMsg();
            msg.setFunctionTypeId(functionTypeId);
            return msg.send().getDefaultParams();
        } catch (StructurizedException e) {
            throw new BlogicException("", e);
        }
    }

    public List validateActualFunctionParams(Long functionTypeId, List actualFunctionParams) throws BlogicException {
        try {
            BillingClientApi manager = new BillingClientApi();
            ValidateParamReq msg = manager.createValidateParamsMsg();
            msg.setFunctionTypeId(functionTypeId);
            msg.setActualFunctionParams(actualFunctionParams);
            return msg.send().getValidationErrors();
        } catch (StructurizedException e) {
            throw new BlogicException("", e);
        }
    }

    //TODO
    public List getFuncCategories(){
        ArrayList categories = new ArrayList();
        categories.add(FunctionType.CATEGORY_LIMIT);
        categories.add(FunctionType.CATEGORY_TARIFF);
        return categories;
    }

    public static class CheckResponse{
        private String message;
        private boolean status;

        public CheckResponse(String message, boolean status) {
            this.message = message;
            this.status = status;
        }

        public CheckResponse(String message, int code) {
            this.message = message;
            this.status = (code == StatusDictionary.STATUS_OK);
        }

        public String getMessage() {
            return message;
        }

        public boolean isStatus() {
            return status;
        }
    }

}
