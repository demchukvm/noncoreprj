package com.tmx.as.blogic.tariff;

import com.tmx.as.blogic.BlogicException;
import com.tmx.as.entities.bill.tariff.AbstractTariff;
import com.tmx.as.entities.bill.tariff.TerminalTariff;
import com.tmx.as.exceptions.DatabaseException;
import com.tmx.as.base.criterion.CriterionWrapper;
import com.tmx.as.base.criterion.Restrictions;

import java.util.List;


public class TerminalTariffManager extends AbstractTariffManager{

    //template methods
    protected AbstractTariff getSpecificTariff(Long tariffId) throws DatabaseException, BlogicException {
        return (AbstractTariff) RETRIEVE(TerminalTariff.class, tariffId, new String[]{"terminal","functionInstance.functionType"});
    }

    protected void saveSpecificTariff(AbstractTariff tariff) throws DatabaseException, BlogicException {
        TerminalTariff terminalTariff = (TerminalTariff) tariff;
        if(terminalTariff.getTerminal() == null)
            throw new BlogicException("err.terminal_tariff_manager.save_tariff.absent_terminal_in_terminal_tariff");
        if(terminalTariff.getTerminal().getTerminalId() == null)
            throw new BlogicException("err.terminal_tariff_manager.save_tariff.absent_terminal_id_in_terminal");
        SAVE(terminalTariff);
    }

    protected void deleteSpecificTariff(Long tariffId) throws DatabaseException, BlogicException {
        DELETE(TerminalTariff.class, tariffId);
    }

    protected void copySpecificTariff(Long tariffId, Long destOwnerId) throws DatabaseException, BlogicException {
        
    }

    protected void copyAllSpecificTariffs(Long srcOwnerId, Long destOwnerId) throws DatabaseException, BlogicException {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public List<TerminalTariff> getAllTariff(Long terminalId) throws DatabaseException {
        CriterionWrapper criterions[] = new CriterionWrapper[] {
                Restrictions.eq("terminalId", terminalId)};
        return RETRIEVE_ALL(TerminalTariff.class, criterions, new String[] {"functionInstance"});
    }

}
