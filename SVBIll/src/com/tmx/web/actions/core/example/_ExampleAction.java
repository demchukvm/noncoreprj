package com.tmx.web.actions.core.example;

import com.tmx.web.base.BasicAction;
import com.tmx.web.forms.core.example._ExampleForm;
import com.tmx.web.controls.CheckBox;
import com.tmx.web.controls.ScrollBox;
import com.tmx.web.controls.RadioButtonGroup;
import com.tmx.as.base.EntityManager;
import com.tmx.as.entities.general.role.Role;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForm;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Exapmle action to show how to use tmx controls
 */
public class _ExampleAction extends BasicAction {

    public ActionForward executeSpecificAction(ActionMapping mapping,
                                               ActionForm form,
                                               HttpServletRequest request,
                                               HttpServletResponse response)  throws Throwable {

        _ExampleForm exampleForm = (_ExampleForm)form;


        //Use checkboxes
        CheckBox cb = exampleForm.getCheckBox1();
        if(cb.isSelected()){
            System.out.println(cb.getLabel()+" is selected");
        }
        else{
            System.out.println(cb.getLabel()+" is not selected");
        }

        cb = exampleForm.getMyPanel().getCheckBox2();
        if(cb.isSelected()){
            System.out.println(cb.getLabel()+" is selected");
        }
        else{
            System.out.println(cb.getLabel()+" is not selected");
        }

        cb = exampleForm.getMyPanel().getMyPanel2().getCheckBox3();
        if(cb.isSelected()){
            System.out.println(cb.getLabel()+" is selected");
        }
        else{
            System.out.println(cb.getLabel()+" is not selected");
        }

//        cb.setSelected(true);
//        cb.setLabel("ffffffff");


        //Use scrollbox
        ScrollBox sbRoles1 = exampleForm.getSelectRoles1();
        ScrollBox sbRoles3 = exampleForm.getMyPanel().getMyPanel2().getSelectRoles3();
        EntityManager em = new EntityManager();

        if(!sbRoles1.isBounded() || !sbRoles3.isBounded()){
            em.EXPLICIT_CONNECT("general");
            sbRoles1.bind(em.RETRIEVE_ALL(Role.class, null));
            sbRoles3.bind(em.RETRIEVE_ALL(Role.class, null));
            em.EXPLICIT_DISCONNECT("general");
        }

        System.out.println("Selected option 1: key="+sbRoles1.getSelectedEntry().getKey()+", value="+sbRoles1.getSelectedEntry().getValue());
        System.out.println("Selected option 3: key="+sbRoles3.getSelectedEntry().getKey()+", value="+sbRoles3.getSelectedEntry().getValue());


        //Use radio button groups
        RadioButtonGroup radioRoles1 = exampleForm.getRadioRoles1();
        RadioButtonGroup radioRoles3 = exampleForm.getMyPanel().getMyPanel2().getRadioRoles3();

        if(!radioRoles1.isBounded() || !radioRoles3.isBounded()){
            em.EXPLICIT_CONNECT("general");
            radioRoles1.bind(em.RETRIEVE_ALL(Role.class, null));
            radioRoles3.bind(em.RETRIEVE_ALL(Role.class, null));
            em.EXPLICIT_DISCONNECT("general");
        }

        System.out.println("Selected radio 1: key="+radioRoles1.getSelectedEntry().getKey()+", value="+radioRoles1.getSelectedEntry().getValue());
        System.out.println("Selected radio 3: key="+radioRoles3.getSelectedEntry().getKey()+", value="+radioRoles3.getSelectedEntry().getValue());


        return mapping.findForward("_examplePage");
    }


}
