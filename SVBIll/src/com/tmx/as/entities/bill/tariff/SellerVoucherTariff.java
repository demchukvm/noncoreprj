package com.tmx.as.entities.bill.tariff;


import com.tmx.as.entities.bill.seller.SellerVoucherPoolAssignment;
import com.tmx.as.entities.bill.balance.SellerBalance;
import com.tmx.as.entities.bill.voucher.VoucherType;

/**

 */
public class SellerVoucherTariff extends AbstractTariff{
    private SellerVoucherPoolAssignment sellerVoucherPoolAssignmentId;
    private SellerBalance sellerBalance;
    private VoucherType voucherTypeId;

    public SellerBalance getSellerBalance() {
        return sellerBalance;
    }

    public void setSellerBalance(SellerBalance sellerBalance) {
        this.sellerBalance = sellerBalance;
    }

    public SellerVoucherPoolAssignment getSellerVoucherPoolAssignment() {
        return sellerVoucherPoolAssignmentId;
    }

    public void setSellerVoucherPoolAssignment(SellerVoucherPoolAssignment sellerVoucherPoolAssignment) {
        this.sellerVoucherPoolAssignmentId = sellerVoucherPoolAssignment;
    }

    public VoucherType getVoucherTypeId() {
        return voucherTypeId;
    }

    public void setVoucherTypeId(VoucherType voucherTypeId) {
        this.voucherTypeId = voucherTypeId;
    }
}
