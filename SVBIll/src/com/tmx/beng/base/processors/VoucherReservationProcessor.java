package com.tmx.beng.base.processors;

import com.tmx.beng.base.*;
import com.tmx.beng.base.cache.DBCache;
import com.tmx.as.base.EntityManager;
import com.tmx.as.base.FilterWrapper;
import com.tmx.as.base.OrderWrapper;
import com.tmx.as.entities.bill.voucher.Voucher;
import com.tmx.as.exceptions.DatabaseException;
import java.util.List;
import java.util.Date;

/**

 */
public class VoucherReservationProcessor extends TaskProcessor {

    protected Task process0(Task task, DBCache dbCache, BillingMessageWritable msg) throws BillException {
        logger.debug("Voucher reservation processor...");

        Voucher voucher = null;
        String voucherNominal = msg.getAttributeString(BillingMessage.VOUCHER_NOMINAL);
        FilterWrapper byStatusFW = new FilterWrapper("by_voucher_status");
        byStatusFW.setParameter("status", Voucher.STATUS_NEW);
        FilterWrapper byNominalFW = new FilterWrapper("by_voucher_nominal");
        byNominalFW.setParameter("nominal", voucherNominal);
        OrderWrapper byExpirationDateOW = new OrderWrapper("bestBeforeDate", OrderWrapper.ASC);
                            
        try{
            EntityManager em = new EntityManager();
            List singleVoucher = em.RETRIEVE_ALL(Voucher.class,
                new FilterWrapper[]{byStatusFW, byNominalFW},
                new OrderWrapper[]{byExpirationDateOW},
                new String[]{/** "voucherType" */},
                1,
                1);
            if(singleVoucher.size() <= 0){
                msg.setStatusCode(StatusDictionary.NO_VOUCHER_FOUND);
                logger.error("Not found new voucer");
                throw new BillException("err.voucher_reservation_processor.no_voucher_found", new String[]{voucherNominal});
            }

            voucher = (Voucher)singleVoucher.get(0);
            voucher.setStatus(Voucher.STATUS_RESERVED);
            voucher.setReservationTime(new Date());
            em.SAVE(voucher, new String[]{"status", "reservationTime"});
        }
        catch(DatabaseException e){
            throw new BillException("err.failed_to_retrieve_voucher", e);
        }

        //Publish voucher nominal as transaction amount to be handled by Reservation and CheckLimit processors
        msg.setAttributeString(BillingMessage.AMOUNT, voucher.getPrice().toString());
        //publish voucher to be handled by SellVoucherProcessor
        msg.setAttribute(BillingMessage.VOUCHER, voucher);
        
        return task;
    }
}
