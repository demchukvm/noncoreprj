LOAD DATA
INFILE *
APPEND INTO TABLE svbill.ft_function_type
Fields terminated by ";" Optionally enclosed by '"'
(
  FT_FUNCTIONTYPEID NULLIF (FT_FUNCTIONTYPEID="NULL"),
  FT_NAME,
  FT_FUNCTIONSIGNATURE,
  FT_IMPLCLASS,
  FT_IMPLMETHOD,
  FT_VALID NULLIF (FT_VALID="NULL")
)
BEGINDATA
1;"transaction_min_max_limit";"";"com.tmx.beng.base.dbfunctions.TransactionMinMaxLimit";"invoke";1
2;"operator tariff";"";"com.tmx.beng.base.dbfunctions.OperatorTariff";"invoke";1
3;"terminal tariff";"";"com.tmx.beng.base.dbfunctions.TerminalTariff";"invoke";1
4;"seller tariff";"";"com.tmx.beng.base.dbfunctions.SellerTariff";"invoke";1
5;"seller_balance_min_max_limit";"";"com.tmx.beng.base.dbfunctions.SellerBalanceMinMaxLimit";"invoke";1
6;"transaction_positive_and_fractional_limit";"";"com.tmx.beng.base.dbfunctions.TransactionPositiveAndFractionalLimit";"invoke";1
7;"transaction_non_fractional_limit";"";"com.tmx.beng.base.dbfunctions.TransactionNonFractionalLimit";"invoke";1
8;"seller_tariff_operator_sensitive";"";"com.tmx.beng.base.dbfunctions.SellerTariffOperatorSensitive";"invoke";1
