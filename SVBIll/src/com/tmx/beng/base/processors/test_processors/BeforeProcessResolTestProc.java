package com.tmx.beng.base.processors.test_processors;

import com.tmx.beng.base.processors.TaskProcessor;
import com.tmx.beng.base.Task;
import com.tmx.beng.base.BillingMessageWritable;
import com.tmx.beng.base.BillException;
import com.tmx.beng.base.BillingMessage;
import com.tmx.beng.base.cache.DBCache;


public class BeforeProcessResolTestProc extends TaskProcessor{
    protected Task process0(Task task, DBCache dbCache, BillingMessageWritable msg) throws BillException {
        
        return task;
    }
}
