package com.tmx.as.resolver;

import com.tmx.util.StructurizedException;
import com.tmx.as.base.EntityManager;
import com.tmx.as.base.Entity;
import com.tmx.as.base.FilterWrapper;
import com.tmx.as.exceptions.DatabaseException;

import java.util.List;


public class QueryResolver {

    /**
     * Execute specified queryString Retrieve required data from database
     */
    public QueryResult executeQuery(String queryString) throws ResolverException, FunctionSyntaxException, DatabaseException {
        if (queryString == null)
            throw new ResolverException("err.query_string_is_null");

        Query query=new FunctionProcessor().parseQueryText(queryString);

        return executeQuery(query);
    }

    /**
     * Execute specified query Retrieve required data from database.
     */
    public QueryResult executeQuery(Query query) throws ResolverException, DatabaseException{
        if (query == null)
            throw new ResolverException("err.query_is_null");

        if(query.getQueryType().equals(Query.QueryType.RETRIEVE_ALL)){
            return new QueryResult(query, executeRetrieveAll(query), executeRetrieveAllCount(query));
        }

        if(query.getQueryType().equals(Query.QueryType.RETRIEVE)){
            Entity entity = executeRetrieve(query);
            return new QueryResult(query, executeRetrieve(query), entity != null ? 1 : 0);
        }

        if(query.getQueryType().equals(Query.QueryType.EXECUTE_QUERY)){
            /* This implementation doesn't evaluates a total count of data should be retrieved
            without pagination. Only count of rows in current pageset is returned.*/ 
            List result = executeExecuteQuery(query);
            return new QueryResult(query, result, result.size());
        }

        //RM: write here your code to support new types of queries

        throw new ResolverException("err.query_evaluator.unknown_query_type", new String[]{query.getQueryType()});
    }

    /** Take parameters from <code>parametersQuery</code> and apply it to
     * the <code>targetQuery</code>. */
    public Query applyQueryParameters(Query targetQuery, Query parametersQuery){
        if(parametersQuery.getPageNumber() > 0)
            targetQuery.setPageNumber(parametersQuery.getPageNumber());
        if(parametersQuery.getPageSize() > 0)
            targetQuery.setPageSize(parametersQuery.getPageSize());

        /**
         *  There are three levels where ordering could be defined:
         *  1) in the HQL query definition in the *.hbm.xml file
         *  2) in the highl level query language, aka ("query(name=retrieve_all....")
         *  3) in the UI ordering tags.
         *
         *  We should provide a simple and full solution how to combine the orders from these three levels
         *  in one query to cover all requirements.
         *
         *  For now solutin is:
         *  1) ordering from level 1 is never overrided by another and always used as primary orders (other orders
         *  injected after them). Use it to define steady for user actions ordering.
         *  2) ordering from lev.2 injected after orders from lev.1 and replaced by orders lev.3 if last one is occured.
         *  Use this orders to define how data will be ordered first time then user execute query without applying its
         *  own orderings using tags.
         *  3) orders from lev.3 replace the orders from lev.2. It is important to have ability to clean and apply
         *  new orders using ordering tag. 
         */
        if(parametersQuery.getOrders() != null && parametersQuery.getOrders().length > 0)
            targetQuery.setOrders(parametersQuery.getOrders());

        //combine filters from target and parameters queries
        FilterWrapper[] parametersFilters = parametersQuery.getFilters();
        FilterWrapper[] targetFilters = targetQuery.getFilters();
        if(parametersFilters != null && parametersFilters.length > 0){
            if(targetFilters != null && targetFilters.length > 0){
                FilterWrapper[] combinedFilters = new FilterWrapper[parametersFilters.length + targetFilters.length];
                System.arraycopy(targetFilters, 0, combinedFilters, 0, targetFilters.length);
                System.arraycopy(parametersFilters, 0, combinedFilters, targetFilters.length, parametersFilters.length);
                targetQuery.setFilters(combinedFilters);
            }
            else{
                targetQuery.setFilters(parametersFilters);
            }
        }

        if(parametersQuery.getTableAttrs() != null && parametersQuery.getTableAttrs().length > 0)
            targetQuery.setTableAttrs(parametersQuery.getTableAttrs());
        return targetQuery;
    }

    private List executeRetrieveAll(Query query) throws DatabaseException, ResolverException {
        Class entityClass = null;
        try {
            entityClass = Class.forName(query.getEntityClass());
        }
        catch (ClassNotFoundException e) {
            throw new ResolverException("err.specified_entity_class_is_not_found", new String[]{query.getEntityClass()});
        }

        return new EntityManager().RETRIEVE_ALL(entityClass, query.getFilters(), query.getCriterions(), query.getOrders(), query.getTableAttrs(),
                query.getProjectionWrappers(), query.getPageNumber(), query.getPageSize());
    }

    private Entity executeRetrieve(Query query) throws DatabaseException, ResolverException {
        Class entityClass = null;
        try {
            entityClass = Class.forName(query.getEntityClass());
        }
        catch (ClassNotFoundException e) {
            throw new ResolverException("err.specified_entity_class_is_not_found", new String[]{query.getEntityClass()});
        }

        return new EntityManager().RETRIEVE(entityClass, query.getFilters(), query.getTableAttrs());
    }

    private List executeExecuteQuery(Query query) throws DatabaseException, ResolverException {
        Class entityClass = null;
        try {
            entityClass = Class.forName(query.getEntityClass());
        }
        catch (ClassNotFoundException e) {
            throw new ResolverException("err.specified_entity_class_is_not_found", new String[]{query.getEntityClass()});
        }

        return new EntityManager().EXECUTE_QUERY(entityClass, query.getQueryName(), query.getQueryParameters(), query.getFilters(), query.getOrders(), query.getPageNumber(), query.getPageSize());
    }


    private int executeRetrieveAllCount(Query query) throws DatabaseException, ResolverException {
        Class entityClass = null;
        try {
            entityClass = Class.forName(query.getEntityClass());
        }
        catch (ClassNotFoundException e) {
            throw new ResolverException("err.specified_entity_class_is_not_found", new String[]{query.getEntityClass()});
        }

        return new EntityManager().RETRIEVE_ALL_COUNT(entityClass, query.getFilters(), query.getCriterions());
    }
}
