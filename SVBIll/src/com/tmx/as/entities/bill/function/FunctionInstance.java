package com.tmx.as.entities.bill.function;

import com.tmx.as.base.BasicEntity;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

/**
 */
public class FunctionInstance extends BasicEntity implements Cloneable, Serializable{
    private Long functionInstanceId;
    private FunctionType functionType;
    private Set functionParameters; 
//    private String creTStrin;

    //---------------Properties
    public Long getFunctionInstanceId() {
        return functionInstanceId;
    }

    public void setFunctionInstanceId(Long functionInstanceId) {
        this.functionInstanceId = functionInstanceId;
    }

    public FunctionType getFunctionType() {
        return functionType;
    }

    public void setFunctionType(FunctionType functionType) {
        this.functionType = functionType;
    }

    public Serializable getId() {
        return getFunctionInstanceId();
    }

    public int getValid() {
        return valid;
    }

    public void setValid(int valid) {
        this.valid = valid;
    }

    public Set getFunctionParameters() {
        return functionParameters;
    }

    public void setFunctionParameters(Set functionParameters) {
        this.functionParameters = functionParameters;
    }

    public Object clone() throws CloneNotSupportedException {
        FunctionInstance functionInstance = (FunctionInstance) super.clone();
        functionInstance.setFunctionInstanceId(null);
        
        if(functionInstance.getFunctionParameters() != null){
            HashSet hashSet = new HashSet();
            Iterator iterator = functionInstance.getFunctionParameters().iterator();
            while (iterator.hasNext()){
                ActualFunctionParameter param = (ActualFunctionParameter) iterator.next();
                hashSet.add( param.clone() );
            }
            functionInstance.setFunctionParameters(hashSet);
        }
        return functionInstance;
    }
}
