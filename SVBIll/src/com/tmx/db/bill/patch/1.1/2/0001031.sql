--RFC: 0001031
--Note: Implement subsellers

--Create sequnce and unique constraint for sl_seller
@%TMX_HOME%\src\com\tmx\db\bill\table\sl_seller.sql
--Create sequnce and unique constraint for sb_seller_balance
@%TMX_HOME%\src\com\tmx\db\bill\table\sb_seller_balance.sql
--Create sequnce and unique constraint for tr_terminal
@%TMX_HOME%\src\com\tmx\db\bill\table\tr_terminal.sql
--Create sequnce and unique constraint for tb_terminal_balance
@%TMX_HOME%\src\com\tmx\db\bill\table\tb_terminal_balance.sql
--Create sequnce and unique constraint for op_operator
@%TMX_HOME%\src\com\tmx\db\bill\table\op_operator.sql
--Create sequnce and unique constraint for ob_operator_balance
@%TMX_HOME%\src\com\tmx\db\bill\table\ob_operator_balance.sql
--Create sequnce and unique constraint for pr_processing
@%TMX_HOME%\src\com\tmx\db\bill\table\pr_processing.sql
--Recreate change balance procedure
@%TMX_HOME%\src\com\tmx\db\bill\plsql\change_balances.sql
@%TMX_HOME%\src\com\tmx\db\bill\plsql\test_change_balances.sql

--Rename incorrect table column
ALTER TABLE ST_SELLER_TRANSACTION RENAME COLUMN sl_valid TO st_valid;

exit
