package com.tmx.engines.reportengine.aggrtable;

import com.tmx.as.base.Reconfigurable;
import com.tmx.util.InitException;
import com.tmx.util.Configuration;
import com.tmx.util.cache.FileCache;
import com.tmx.util.cache.CacheException;
import com.tmx.engines.reportengine.base.ReportException;
import com.tmx.engines.reportengine.aggrtable.beans.RootDocument;
import org.apache.log4j.Logger;
import org.apache.commons.digester.Digester;
import org.apache.xmlbeans.XmlOptions;

import java.util.Locale;
import java.util.ArrayList;
import java.util.Iterator;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;

/**
 */
public class AggrTableService implements Reconfigurable {
    /** Only single instance available */
    private static AggrTableService aggrTableService;
    private AggrTableService.Config serviceConfig = null;
    public static String LOGCLASS_REPORT = "report";
    /** Logger */
    private static Logger logger = Logger.getLogger(LOGCLASS_REPORT + ".AggrTableService");
    private AggrTableTemplateCache cache = new AggrTableTemplateCache();

    /** to hide default constructor */
    private AggrTableService(){
    }

    /** Return gate context instance */
    public static AggrTableService getInstance(){
        if(aggrTableService == null)
            aggrTableService = new AggrTableService();

        return aggrTableService;
    }

    /** Reload configuration */
    public void reload() throws InitException{
        try {
            logger.debug("Start initialization of the AggrTableService");
            String aggrTableConfigFile = Configuration.getInstance().getProperty("aggrtable_service.config_file");
            if (aggrTableConfigFile == null)
                throw new InitException("err.aggrtable_service.config_file_is_not_specified");

            File configFile = new File(aggrTableConfigFile);
            Digester digester = new Digester();
            AggrTableService.Config localConfig = new AggrTableService.Config();
            digester.push(localConfig);
            addRules(digester);
            digester.parse(configFile);
            serviceConfig = (AggrTableService.Config)digester.getRoot();
            logger.debug("Successful completed initialization of the AggrTable service");

        }
        catch (InitException e) {
            //rethrow
            throw e;
        }
        catch (Exception e) {
            throw new InitException("err.aggrtable_service.reload_failed", e);
        }
    }


    public AggregatedTable initializeAggrTable(String templateName) throws ReportException, InitException{
        templateName = (templateName.endsWith(".xml")) ? templateName.substring(0, templateName.length() - ".xml".length()) : templateName;
        templateName = templateName.replace('.', File.separatorChar);
        String templatePath = serviceConfig.getRepository() + File.separator + templateName + ".xml";
        return initializeAggrTable0(templatePath);
    }


    //--------private
    private AggregatedTable initializeAggrTable0(String realTemplateFilePath) throws ReportException, InitException{
        RootDocument rootDoc = null;
        try{
            rootDoc = (RootDocument)cache.get(realTemplateFilePath);
        }
        catch(CacheException e){
            throw new ReportException("err.aggrtable_service.failed_to_get_aggrtable_template", new String[]{realTemplateFilePath}, e);
        }

        //Prepare aggr table config and initialize aggr table
        AggregatedTable table = new AggregatedTable();
        table.init(new AggrTableConfig(rootDoc));
        return table;
    }


    //---------------private methods
    private void addRules(Digester d){
        d.addSetProperties("aggrTableService");
        d.addSetNestedProperties("aggrTableService/params");
    }

    public class Config{
        private String repository = null;
        private int maxRowCount;
        private int maxHeaderCount;

        //--------properties

        public String getRepository() {
            return repository;
        }

        public void setRepository(String repository) throws Exception{
            this.repository = Configuration.getInstance().substituteVariablesInString(repository);
        }

        public int getMaxRowCount() {
            return maxRowCount;
        }

        public void setMaxRowCount(int maxRowCount) {
            this.maxRowCount = maxRowCount;
        }

        public int getMaxHeaderCount() {
            return maxHeaderCount;
        }

        public void setMaxHeaderCount(int maxHeaderCount) {
            this.maxHeaderCount = maxHeaderCount;
        }
    }

    //wrapper for RootDocument
    public class AggrTableConfig{
        private RootDocument rootDoc = null;

        AggrTableConfig(RootDocument rootDoc){
            this.rootDoc = rootDoc;
        }

        public RootDocument getRootDoc() {
            return rootDoc;
        }
    }

    private class AggrTableTemplateCache extends FileCache {

        private AggrTableTemplateCache() {
            maxSize = 100;
            cleanCount = 10;
            cleanType = CleanAlgorithm.Type.CLEAN_N_SELDOM_ACCESSED;
        }

        protected Object getRealObjectFromFile(InputStream is, String templateFilePath) throws CacheException {
            RootDocument rootDoc = null;
            try{
                // Bind the instance to the generated XMLBeans types.
                rootDoc = RootDocument.Factory.parse(is);
            }
            catch(Throwable e){
                throw new CacheException("err.aggrtable_service.failed_to_parse_aggrtable_template", new String[]{templateFilePath}, e);
            }

            //validate
            ArrayList errors = new ArrayList();
            XmlOptions opts = new XmlOptions();
            opts.setErrorListener(errors);
            if(!rootDoc.validate(opts)){
                StringBuffer errorBuffer = new StringBuffer("Validation failed of XML "+templateFilePath+":\n");
                Iterator iter = errors.iterator();
                while(iter.hasNext())
                    errorBuffer.append("   >> " + iter.next() + "\n");
                logger.error(errorBuffer.toString());
                throw new CacheException("err.aggrtable_service.template_validation_failed", new String[]{templateFilePath});
            }

            return rootDoc;
        }
    }

}
