package com.tmx.web.forms.core.balance.seller_balance;

import com.tmx.web.forms.core.restriction.RestrictionParameterForm;

@Deprecated
public class SellerParameterForm extends RestrictionParameterForm {


    protected void init(){
        super.init();
        initButtons();
    }

    private void initButtons(){
        getOnSaveButton().setAction("/core/balance/seller_balance/sellerParameter");
        getOnApplyButton().setAction("/core/balance/seller_balance/sellerParameter");
        getOnCancelButton().setAction("/core/balance/seller_balance/sellerBalanceRestriction");        
    }


}
