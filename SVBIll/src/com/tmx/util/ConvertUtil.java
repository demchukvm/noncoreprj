package com.tmx.util;

import java.util.*;
import java.lang.reflect.Array;

/**
 * Provides methods to manipulate over data sets
 */
public class ConvertUtil {


    /**
     * Converts collection, specified in argument to the instance of <code>java.util.List</code>. Supported types
     * include: <ul type="disc">
     * <li><code>java.util.Collection, java.util.Set, java.util.SortedSet, java.util.List</code> - result list will contain all elements from specified collection, set or list</li>
     * <li><code>java.util.Enumeration</code> - result list will contain all elements from this enumeration in the same order</li>
     * <li><code>java.util.Iterator</code> - result list will contain all elements from collection, iterated by this iterator in the same order</li>
     * <li><code>java.util.Map, java.util.SortedMap</code> - result list will contain all entries (instances of <code>java.util.Map$Entry</code>)</li>
     * <li><code>java.lang.String</code> - result list will contain all characters, each one wrapped in <code>java.lang.Character</code></li>
     * <li><code>java.lang.Object[]</code> - result list will be dynamic equivalent for specified array</li>
     * <li>any primitive array - result list will contain elements from array, each wrapped in instance of equivalent class
     * (e.g. <code>java.lang.Integer</code> is object equivalent to <code>int</code>,
     * <code>java.lang.Boolean</code> is object equivalent to <code>boolean</code>, etc.)</li>
     * </ul>
     *
     * @param collection Collection to convert to list
     * @return List, containing all elements from collection according to rules, specified above
     */
    public static List convertCollectionToList(Object collection) {

        if (collection == null) {
            return null;
        }

        List list = null;

        if (collection instanceof Collection) {
            list = new ArrayList((Collection) collection);
        } else if (collection instanceof Enumeration) {
            list = new ArrayList();
            Enumeration e = (Enumeration) collection;
            while (e.hasMoreElements()) {
                list.add(e.nextElement());
            }
        } else if (collection instanceof Iterator) {
            list = new ArrayList();
            Iterator i = (Iterator) collection;
            while (i.hasNext()) {
                list.add(i.next());
            }
        } else if (collection instanceof Map) {
            list = new ArrayList(((Map) collection).entrySet());
        } else if (collection instanceof String) {
            list = Arrays.asList(convertPrimitivesToObjects(((String) collection).toCharArray()));
        } else if (collection instanceof Object[]) {
            list = Arrays.asList((Object[]) collection);
        } else if (collection.getClass().isArray()) {
            list = Arrays.asList(convertPrimitivesToObjects(collection));
        } else {
            // type is not supported
            throw new IllegalArgumentException("Class '" + collection.getClass().getName() + "' is not convertable to java.util.List");
        }

        return list;
    }

    /**
     * Converts primitive array to array of objects. Each element in returned array will have run-time class
     * equivalent to its primitive type (e.g. <code>java.lang.Integer</code> is object equivalent to <code>int</code>,
     * <code>java.lang.Boolean</code> is object equivalent to <code>boolean</code>, etc.)
     *
     * @param primitiveArray Array of primitives which needs to be converted to objects
     * @return Array of object, each element is object equivalent to corresponding primitive value
     * @throws IllegalArgumentException if specified argument is not a primitive array
     */
    public static Object[] convertPrimitivesToObjects(Object primitiveArray) {
        if (primitiveArray == null) {
            return null;
        }

        if (!primitiveArray.getClass().isArray()) {
            throw new IllegalArgumentException("Specified object is not array");
        }

        if (primitiveArray instanceof Object[]) {
            throw new IllegalArgumentException("Specified object is not primitive array");
        }

        int length = Array.getLength(primitiveArray);
        Object[] result = new Object[length];
        for (int i = 0; i < length; i++) {
            result[i] = Array.get(primitiveArray, i);
        }

        return result;
    }

}
