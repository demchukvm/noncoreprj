package com.tmx.gate.datastream.base;

import com.tmx.as.base.Reconfigurable;
import com.tmx.util.InitException;
import com.tmx.util.Configuration;
import com.tmx.util.XMLUtil;

import java.util.HashMap;
import java.util.Map;
import java.util.Locale;
import java.util.Properties;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Element;
import com.sun.org.apache.xpath.internal.XPathAPI;
import org.apache.log4j.Logger;

/** Context to hold configs and manage datastreams processors */
public class GateContext implements Reconfigurable {
    /** Only single instance available */
    private static GateContext gateContext;

    private Map datastreamProcessorsConfigs = new HashMap();

    public static String LOGCLASS_GATE = "gate";
    /** Logger */
    private static Logger logger = Logger.getLogger(LOGCLASS_GATE + ".GateContext");


    /** to hide default constructor */
    private GateContext(){
    }

    /** Return gate context instance */
    public static GateContext getInstance(){
        if(gateContext == null)
            gateContext = new GateContext();

        return gateContext;
    }


    /** reload configuration */
    public void reload() throws InitException{
        try {
            logger.debug("Start initialization of the Gate Context");
            String gateConfigFile = Configuration.getInstance().getProperty("gate.config_file");
            if (gateConfigFile == null)
                throw new InitException("err.init.gate.config_file_is_not_specified", Locale.getDefault());

            //Load config from XML
            Document doc = XMLUtil.getDocumentFromFile(gateConfigFile);

            String xpathDatastreamProcessors = "/gate/datastreamProcessors/processor";
            NodeList processorsNodes = XPathAPI.selectNodeList(doc, xpathDatastreamProcessors);
            Map localDatastreamProcessorsConfigs = new HashMap();
            for (int i = 0; i < processorsNodes.getLength(); i++) {
                Node processorNode = processorsNodes.item(i);
                if (processorNode.getNodeType() == Node.ATTRIBUTE_NODE)
                    continue;

                Properties processorProperties = new Properties();
                String processorName = ((Element) processorNode).getAttribute("name");
                if (processorName == null)
                    throw new InitException("err.gate.init.processor_name_is_null_in_config");
                if (localDatastreamProcessorsConfigs.containsKey(processorName))
                    throw new InitException("err.gate.init.duplicate_processor_name_in_config");
                localDatastreamProcessorsConfigs.put(processorName, processorProperties);

                String xpathParams = "/gate/datastreamProcessors/processor[@name='"+processorName+"']/param";
                NodeList paramNodes = XPathAPI.selectNodeList(doc, xpathParams);
                for (int j = 0; j < paramNodes.getLength(); j++) {
                    Node paramNode = paramNodes.item(j);
                    if (paramNode.getNodeType() == Node.ATTRIBUTE_NODE)
                        continue;
                    String paramName = ((Element) paramNode).getAttribute("name");
                    if(paramName == null || "".equals(paramName.trim()))
                        throw new InitException("err.gate.init.empty_parameter_name_in_processor_config", new String[]{processorName});
                    if(processorProperties.containsKey(paramName))
                        throw new InitException("err.gate.init.duplicate_parameter_name_in_processor_config", new String[]{processorName});
                    String paramValue = ((Element) paramNode).getAttribute("value");
                    processorProperties.put(paramName, paramValue);
                }
            }
            logger.debug("New config will be set synchronized...");
            setDatastreamsProcessorsConfigs(localDatastreamProcessorsConfigs);
            logger.debug("Successful completed initialization of the Gate Context");
        }
        catch (InitException e) {
            //rethrow
            throw e;
        }
        catch (Exception e) {
            throw new InitException("err.init.scheduler.reload_failed", e, Locale.getDefault());
        }
    }

    /** synchronous set new config */
    private synchronized void setDatastreamsProcessorsConfigs(Map newConfigs){
        datastreamProcessorsConfigs = newConfigs;
    }

    /** Return null config for datastream processor thread by processor name.
     * Return null if given processor doesn't have it.  */
    public synchronized Properties getDatastreamProcessorConfig(String processorName){
        Properties props = null;
        if(datastreamProcessorsConfigs != null)
            props = (Properties)datastreamProcessorsConfigs.get(processorName);

        return props;
    }
}
