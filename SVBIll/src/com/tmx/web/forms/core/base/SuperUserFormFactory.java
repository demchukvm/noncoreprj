package com.tmx.web.forms.core.base;

import com.tmx.as.blogic.SellerManager;
import com.tmx.as.blogic.balance.SellerBalanceManager;
import com.tmx.as.entities.bill.seller.Seller;
import com.tmx.as.entities.bill.balance.SellerBalance;
import com.tmx.as.entities.general.user.User;
import com.tmx.as.exceptions.DatabaseException;
import com.tmx.as.base.criterion.CriterionWrapper;
import com.tmx.as.base.criterion.Restrictions;
import com.tmx.as.base.EntityManager;

import com.tmx.web.controls.BrowseBox;
import com.tmx.web.controls.ScrollBox;
import com.tmx.web.controls.CriterionFactory;

import com.tmx.web.forms.core.balance.seller_balance.ManageSellerBalancesForm;
import com.tmx.web.forms.core.balance.terminal_balance.BrowseTerminalsForm;
import com.tmx.web.forms.core.balance.terminal_balance.ManageTerminalBalancesForm;
import com.tmx.web.forms.core.balance.AbstractInfoBalanceForm;
import com.tmx.web.forms.core.dictionary.seller.ManageSellersForm;
import com.tmx.web.forms.core.dictionary.seller.InfoSellerForm;
import com.tmx.web.forms.core.dictionary.terminal.InfoTerminalForm;
import com.tmx.web.forms.core.dictionary.user.UserInfoForm;
import com.tmx.web.forms.core.dictionary.terminal.ManageTerminalsForm;
import com.tmx.web.forms.core.restriction.seller_restriction.ManageSellerRestrictionsForm;
import com.tmx.web.forms.core.restriction.seller_restriction.InfoSellerRestrictionForm;
import com.tmx.web.forms.core.restriction.terminal_restriction.ManageTerminalRestrictionsForm;
import com.tmx.web.forms.core.tariff.seller_tariff.ManageSellerTariffsForm;
import com.tmx.web.forms.core.tariff.seller_tariff.ManageSimpleSellerTariffsForm;
import com.tmx.web.forms.core.tariff.seller_tariff.InfoSellerTariffForm;
import com.tmx.web.forms.core.tariff.terminal_tariff.ManageTerminalTariffsForm;
import com.tmx.web.forms.core.transaction.client_transaction.ManageClientTransactionsForm;
import com.tmx.web.forms.core.transaction.seller_balance_transaction.ManageSellerBalanceTransactionsForm;
import com.tmx.web.forms.core.transaction.terminal_balance_transaction.ManageTerminalBalanceTransactionsForm;
import com.tmx.web.forms.core.reports.ReportPaymentForm;
import com.tmx.web.forms.core.reports.ReportVouchersForm;
import com.tmx.web.forms.core.reports.ReportNetworksForm;

import java.util.Comparator;
import java.util.List;
import java.util.Collections;
import java.util.ArrayList;

//
public class SuperUserFormFactory extends AbstractFormFactory {

    public void setUpForm(UserInfoForm userInfoForm) throws DatabaseException {
        userInfoForm.getScrollBoxSellers().bind(new SellerManager().getAllSellers());
    }

    public void setUpForm(ManageSellersForm manageSellersForm) throws DatabaseException {
        manageSellersForm.getSellersTable().loadQuery("query(query_type=retrieve_all," +
            "entity_class=com.tmx.as.entities.bill.seller.Seller," +
            "table_attr=physicalAddress," +
            "table_attr=legalAddress," +
            "table_attr=parentSeller," +
            "order(name=id,type=asc)," +
            "page_size=15" +
            ")");
        manageSellersForm.getScrollBoxParentSeller().bind(new SellerManager().getAllSellers());
        manageSellersForm.getScrollBoxParentSeller().setNullable(true);
        manageSellersForm.getScrollBoxParentSeller().setSelectedKey(ScrollBox.NULL_KEY);

    }

    public void setUpForm(ManageSellerBalanceTransactionsForm form) throws DatabaseException {
        ScrollBox scrollBox = (ScrollBox) form.getSellerTransactionsTable().getParameterControl("by_seller", "seller");
        final List<Seller> sellers = (new SellerManager()).getAllSellers(new String[] {"primaryBalance"});
        if(scrollBox != null && !scrollBox.isUpdated()) {
            scrollBox.setNullable(true);
            //scrollBox.bind(new SellerManager().getAllSellers());
            scrollBox.bind(sellers);
            scrollBox.setSelectedKey(ScrollBox.NULL_KEY);
        }

        final List<SellerBalance> sellerBalances = new ArrayList<SellerBalance>();
        for(Seller seller : sellers) {
            if (seller.getPrimaryBalance() != null) {
                sellerBalances.add(seller.getPrimaryBalance());
            }
        }

        Collections.sort(sellerBalances, new Comparator<SellerBalance>()
        {
          public int compare(SellerBalance sb1, SellerBalance sb2) {
            return sb1.getCode().compareTo(sb2.getCode());
          }
        });

        final ScrollBox<Seller> scrollBoxSellers = (ScrollBox<Seller>) form.getSellerTransactionsTable().getParameterControl("by_seller", "seller");
        scrollBoxSellers.setNullable(true);
        //scrollBoxSellers.bind(sellers);
        scrollBox.bind(new SellerManager().getAllSellers());

        final ScrollBox<SellerBalance> scrollBoxSellerBalance = (ScrollBox<SellerBalance>) form.getSellerTransactionsTable().getParameterControl("by_seller_balance", "sellerBalance");
        scrollBoxSellerBalance.setNullable(true);

        form.getSellerTransactionsTable().setCriterionFactory(new CriterionFactory() {
            public CriterionWrapper createCriterion() {
                if (scrollBoxSellerBalance.getSelectedKey() == null || scrollBoxSellerBalance.isNullKeySelected()) {
                    return Restrictions.in("sellerBalance", sellerBalances);
                } else {
                    SellerBalance sellerBalance = null;
                    //retrive needed balance
                    try {sellerBalance = (SellerBalance)(new SellerBalanceManager()).getBalance(new Long(scrollBoxSellerBalance.getSelectedKey()));
                    } catch (DatabaseException e) {e.printStackTrace();}

                    return Restrictions.eq("sellerBalance", sellerBalance);
                }
            }
        });
        scrollBoxSellerBalance.bind(sellerBalances);
    }

    public void setUpForm(ManageTerminalBalanceTransactionsForm form) throws DatabaseException {
        BrowseBox browseBox = (BrowseBox) form.getTerminalTransactionsTable().getParameterControl("by_terminal_id", "terminal_id");
        if(browseBox!=null && !browseBox.isUpdated()){//if !browseBox.isUpdated() is absent then defaultAction always set browseBox key in null
            browseBox.setKey(null);
        }
    }

    public void setUpForm(ManageClientTransactionsForm form) throws DatabaseException {
        final ManageClientTransactionsForm manageClientTransactionsForm = form;
        
        ScrollBox scrollBox = (ScrollBox) form.getClientTransactionsTable().getParameterControl("by_seller", "seller");

        if ( !scrollBox.isBounded() )//(A.N)Rube part, I can't setNullable(true)  in init() method!
            scrollBox.setNullable(true);

        CriterionFactory criterionFactory = new CriterionFactory() {

            public CriterionWrapper createCriterion() {
                CriterionWrapper resultCriterion = Restrictions.isNotNull("tansactionId");//redunant

                if (manageClientTransactionsForm.getEditBoxAccountNumber().getValue() != null && !manageClientTransactionsForm.getEditBoxAccountNumber().getValue().equals("")) {
                    resultCriterion = Restrictions.and(resultCriterion,
                            Restrictions.ilike("accountNumber", "%" + manageClientTransactionsForm.getEditBoxAccountNumber().getValue() + "%"));
                }

                if (manageClientTransactionsForm.getEditBoxTerminalSerialNumber().getValue() != null && !manageClientTransactionsForm.getEditBoxTerminalSerialNumber().getValue().equals("")) {
                    resultCriterion = Restrictions.and(resultCriterion,
                            Restrictions.ilike("terminal.serialNumber", "%" + manageClientTransactionsForm.getEditBoxTerminalSerialNumber().getValue() + "%"));
                }

                 if (!manageClientTransactionsForm.getScrollBoxTerminalTypes().isNullKeySelected()) {
                    resultCriterion = Restrictions.and(resultCriterion,
                            Restrictions.eq("terminal.equipmentType.equipmentTypeId", new Long(manageClientTransactionsForm.getScrollBoxTerminalTypes().getSelectedKey())));
                }

                if (manageClientTransactionsForm.getEditBoxVoucherCode().getValue() != null && !manageClientTransactionsForm.getEditBoxVoucherCode().getValue().equals("")) {
                    resultCriterion = Restrictions.and(resultCriterion,
                            Restrictions.ilike("voucher.code", "%" + manageClientTransactionsForm.getEditBoxVoucherCode().getValue() + "%"));
                }

                if (!manageClientTransactionsForm.getScrollBoxTransTypes().isNullKeySelected()) {
                    resultCriterion = Restrictions.and(resultCriterion,
                            Restrictions.eq("type.code", manageClientTransactionsForm.getScrollBoxTransTypes().getSelectedKey()));
                }

                if (!manageClientTransactionsForm.getScrollBoxStatuses().isNullKeySelected()) {//by status
                    resultCriterion = Restrictions.and(resultCriterion,
                            Restrictions.eq("status", manageClientTransactionsForm.getScrollBoxStatuses().getSelectedKey()));
                    //manageClientTransactionsForm.getChekBoxErrorTransactions().setSelected(false);
                    manageClientTransactionsForm.setChekBoxErrorTransactions(false);
                } else {
                    //if (manageClientTransactionsForm.getChekBoxErrorTransactions().isSelected()) {// all error transactions
                    manageClientTransactionsForm.set_chekBoxErrorTransactions(manageClientTransactionsForm.isChekBoxErrorTransactions());
                    if (manageClientTransactionsForm.isChekBoxErrorTransactions() == true) {
                        resultCriterion = Restrictions.and(resultCriterion,
                            Restrictions.not(Restrictions.eq("status", "0")));
                        manageClientTransactionsForm.setChekBoxErrorTransactions(false);
                    }
                }

               return resultCriterion;
            }
        };
        manageClientTransactionsForm.getClientTransactionsTable().setCriterionFactory(criterionFactory);

        scrollBox.bind(new SellerManager().getAllSellers());
    }

    public void setUpForm(ManageSellerBalancesForm form) throws DatabaseException {
        ScrollBox scrollBox = (ScrollBox) form.getBalancesTable().getParameterControl("by_seller_id", "seller_id");
        scrollBox.bind(new SellerManager().getAllSellers());

        scrollBox.setNullable(true);
        if(scrollBox != null){
            scrollBox.setNullable(true);
            if (!scrollBox.isUpdated()) {
                scrollBox.setSelectedKey(ScrollBox.NULL_KEY);
            }
        }
    }

    public void setUpForm(AbstractInfoBalanceForm form) throws DatabaseException {
        form.getOnChangeBalanceButton().setReadonly(false);
    }

    public void setUpForm(ManageTerminalBalancesForm form, User user) throws DatabaseException {
        BrowseBox browseBox = (BrowseBox) form.getBalancesTable().getParameterControl("by_terminal_id", "terminal_id");
        if(browseBox != null && !browseBox.isUpdated()){
            browseBox.setKey(null);
        }
        ScrollBox scrollBox = form.getScrollBoxSellerOwner();
        scrollBox.setNullable(true);
        scrollBox.bind(new SellerManager().getAllSellers());

        if(scrollBox != null && !scrollBox.isUpdated()){
            scrollBox.setSelectedKey(ScrollBox.NULL_KEY);
        }

        Seller seller = null;
        if (form.getScrollBoxSellerOwner().getSelectedKey() != null) {
                    if(!form.getScrollBoxSellerOwner().isNullKeySelected()) {
                        Long sellerId = new Long(form.getScrollBoxSellerOwner().getSelectedKey());
                        seller = (Seller)(new EntityManager()).RETRIEVE(Seller.class, sellerId);
                    }
        }
        final Seller finalSeller = seller;
        final ManageTerminalBalancesForm finalForm = form;
        CriterionFactory criterionFactory = new CriterionFactory() {

            public CriterionWrapper createCriterion() {
                //initialize criterion wrapper
                CriterionWrapper resultCriterion = Restrictions.isNotNull("balanceId");

                 //add restrictions
                if (finalForm.getEditBoxBalanceName().getValue() != null && !finalForm.getEditBoxBalanceName().getValue().equals("")) {
                    resultCriterion = Restrictions.and(resultCriterion,
                            Restrictions.ilike("name", "%" + finalForm.getEditBoxBalanceName().getValue() + "%"));
                }
                if (finalForm.getEditBoxBalanceCode().getValue() != null && !finalForm.getEditBoxBalanceCode().getValue().equals("")) {
                    resultCriterion = Restrictions.and(resultCriterion,
                            Restrictions.ilike("code", "%" + finalForm.getEditBoxBalanceCode().getValue() + "%"));
                }
                if (finalSeller != null) {
                        resultCriterion = Restrictions.and(resultCriterion,
                                Restrictions.eq("terminal.sellerOwner", finalSeller));

                    }
                return resultCriterion;
            }
        };

//
        form.getBalancesTable().setCriterionFactory(criterionFactory);
    }

    public void setUpForm(ManageSellerTariffsForm form) throws DatabaseException {
        ScrollBox scrollBox = (ScrollBox) form.getTariffsTable().getParameterControl("by_seller_id", "seller_id");
        if(scrollBox != null && !scrollBox.isUpdated()){
            scrollBox.setNullable(true);
            scrollBox.bind(new SellerManager().getAllSellers());
            scrollBox.setSelectedKey(ScrollBox.NULL_KEY);
        }
    }

    public void setUpForm(ManageTerminalTariffsForm form) throws DatabaseException {
        BrowseBox browseBox = (BrowseBox) form.getTariffsTable().getParameterControl("by_terminal_id", "terminal_id");
        if(!browseBox.isUpdated()){
            browseBox.setKey(null);
        }
    }

    public void setUpForm(ManageSellerRestrictionsForm form) throws DatabaseException {
        form.getOnDeleteButton().setReadonly(false);
        form.getOnEditButton().setReadonly(false);
        if(!form.getSellersScrollBox().isUpdated()){
            form.bindSellers(new SellerManager().getAllSellers());
        }
    }

    public void setUpForm(ManageTerminalRestrictionsForm form) throws DatabaseException {
        BrowseBox browseBox = (BrowseBox) form.getRestrictionsTable().getParameterControl("by_terminal_id", "terminal_id");
        if(!browseBox.isUpdated()){
            browseBox.setKey(null);
        }
    }

    public void setUpForm(BrowseTerminalsForm form) throws DatabaseException {
        ScrollBox scrollBox = (ScrollBox) form.getBrowseTable().getParameterControl("by_seller_id", "seller_id");
        if(scrollBox != null && !scrollBox.isUpdated()){
            scrollBox.setNullable(true);
            scrollBox.bind(new SellerManager().getAllSellers());
            scrollBox.setSelectedKey(ScrollBox.NULL_KEY);
        }
    }

    public void setUpForm(ManageSimpleSellerTariffsForm form) throws DatabaseException {
        ScrollBox scrollBox = (ScrollBox) form.getIncomingTriffsTable().getParameterControl("by_seller_id", "seller_id");
        scrollBox.setNullable(true);
        if(!scrollBox.isUpdated()){
            form.bindSellers(new SellerManager().getAllSellers());
        }
        form.bindSubSellers(new SellerManager().getNestedSellersInSeller(form.getSelectedSellerIdIncomingTable(), false));
        form.getOnCreateButton().setReadonly(false);
    }

    public void setUpForm(ManageTerminalsForm form) throws DatabaseException {
        form.getScrollBoxSellerOwner().setNullable(true);
        form.getScrollBoxSellerOwner().bind(new SellerManager().getAllSellers());
        form.getScrollBoxSellerOwner().setNullable(true);
//        form.getTerminalsTable().addParameterToFilter( "by_seller_id", "seller_id", form.getScrollBoxSellerOwner() );
    }

    public void setUpForm(InfoTerminalForm form) throws DatabaseException
    {

    }

    public void setUpForm(ReportPaymentForm form) throws DatabaseException {
        SellerManager sellerManager = new SellerManager();

        List subSellerList = Collections.EMPTY_LIST;
        if (form.getSellersScrollBox().getSelectedKey() != null &&
                !form.getSellersScrollBox().isNullKeySelected()) {
            subSellerList = sellerManager.getSellersInSeller(new Long(form.getSellersScrollBox().getSelectedKey()), true);
        }

        form.bindSellersScrollBox(sellerManager.getAllSellers());
        form.bindSubSellersScrollBox(subSellerList);
        form.bindFilters(sellerManager.getRootSellers(), subSellerList);
    }

    public void setUpForm(InfoSellerForm form) throws DatabaseException {
        form.getScrollBoxParentSellers().setNullable(true);
        List<Seller> sellers = new SellerManager().getAllSellers();//getNestedSellersInSeller(loggedInUser.getSellerOwner().getSellerId(), false);
        sellers.add(loggedInUser.getSellerOwner());
        form.getScrollBoxParentSellers().bind(sellers);

        form.getEditBoxKSDiscountPercent().setReadonly(false);
        form.getEditBoxMTSDiscountPercent().setReadonly(false);
        form.getEditBoxLifeDiscountPercent().setReadonly(false);
        form.getEditBoxBeelineDiscountPercent().setReadonly(false);
        form.getEditBoxSellerDelta().setReadonly(false);

        form.getCheckBoxIsAttachKSTariff().setReadonly(false);
        form.getCheckBoxIsAttachMTSTariff().setReadonly(false);
        form.getCheckBoxIsAttachLifeTariff().setReadonly(false);
        form.getCheckBoxIsAttachBeelineTariff().setReadonly(false);

        //form.getScrollBoxParentSellers().setSelectedKey(loggedInUser.getSellerOwner().getSellerId().toString());
        form.getScrollBoxParentSellers().setSelectedKey(ScrollBox.NULL_KEY);
    }

    public void setUpForm(InfoSellerRestrictionForm form) throws DatabaseException {
        form.getScrollBoxSeller().bind(new SellerManager().getAllSellers());
    }

    public void setUpForm(InfoSellerTariffForm form) throws DatabaseException {
        form.getScrollBoxSellers().bind(new SellerManager().getAllSellers());
    }

    public void setUpForm(ReportVouchersForm form) throws DatabaseException {
        form.getScrollBoxSeller().setNullable(true);
        form.getScrollBoxSeller().bind(new SellerManager().getAllSellers());
        form.getScrollBoxSeller().setSelectedKey(ScrollBox.NULL_KEY);//protect from autopopulation
    }

    public void setUpForm(ReportNetworksForm form) throws DatabaseException {
        SellerManager manager = new SellerManager();
        List sellers = manager.getRootSellers();
        Collections.sort(sellers);
        form.getMultipleSelectBoxSellerBalances().bind(sellers);
        form.getMultipleSelectBoxSellerBalances().selectAll();
        form.getMultipleSelectBoxSubSellerBalances().bind(manager.getAllSellers());
    }
}