package com.tmx.gate.datastream.reporter;

import com.tmx.as.entities.mobipay.umc.UmcReport;
import com.tmx.as.entities.mobipay.umc.UmcReportRow;
import com.tmx.as.base.EntityManager;
import com.tmx.as.exceptions.DatabaseException;
import com.tmx.gate.datastream.base.DataStreamException;

import javax.mail.Message;
import javax.mail.Part;
import javax.mail.Multipart;
import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;
import java.io.InputStream;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;
import java.util.Date;
import java.text.SimpleDateFormat;
import java.text.ParseException;

import org.apache.log4j.Logger;

/**
    UMC POP3 reporter
 */
public class UmcPop3Reporter extends AbstractPop3Reporter{
    private final static String REPORTER_NAME = "UMC_POP3_REPORTER";
    private final static String DELIMITER = "\\|";
    private final static String DATE_FORMAT = "yyyy/MM/dd HH:mm:ss";

    public UmcPop3Reporter(){
        logger = Logger.getLogger("umc_gate_reporter.Reporter");
    }

    //abstract methods
    public String getReporterName() {
        return REPORTER_NAME;
    }

    public void receiveReport(Message msg) throws DataStreamException {
        try {
            UmcReport report = new UmcReport();
            // Get the header information
            String from = ((InternetAddress) msg.getFrom()[0]).getPersonal();
            if (from == null)
                from = ((InternetAddress) msg.getFrom()[0]).getAddress();

            report.setEmailFrom(from);
            String subject = msg.getSubject();
            report.setEmailSubject(subject);
            report.setReceiveTime(new Date());
            logger.info("Message received: [from="+from+", subject="+subject+"]");
            //get the message part (i.e. the message itself)
            Part messagePart = msg;
            Object content = messagePart.getContent();
            //or its first body part if it is a multipart message
            if (content instanceof Multipart) {
                messagePart = ((Multipart)content).getBodyPart(0);
                logger.error("Multipart Message is not supported for UMC report");
            }
            //get the content type
            String contentType = messagePart.getContentType();

            //if the content is plain text, we can print it
            if (contentType.startsWith("text/plain") || contentType.startsWith("text/html")) {
                InputStream is = messagePart.getInputStream();
                BufferedReader reader = new BufferedReader(new InputStreamReader(is));
                String thisLine = reader.readLine();
                SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT);
                while (thisLine != null) {
                    String[] fields = thisLine.split(DELIMITER);
                    if(fields.length == 8){
                        //data header
                        report.setFileName(fields[0]);
                        report.setFileCreationDate(dateFormat.parse(fields[1]));
                        report.setTransactionsCount(Long.valueOf(fields[2]));
                        report.setTransactionsAmountSum(Double.valueOf(fields[3]));
                        report.setSuccessTransactionsCount(Long.valueOf(fields[4]));
                        report.setSuccessTransactionsAmountSum(Double.valueOf(fields[5]));
                        report.setFailedTransactionsCount(Long.valueOf(fields[6]));
                        report.setFailedTransactionsAmountSum(Double.valueOf(fields[7]));
                    }
                    else if(fields.length == 7){
                        //data row
                        UmcReportRow reportRow = new UmcReportRow();
                        reportRow.setDillerTransactionId(fields[0]);
                        reportRow.setOperatorTransactionId(fields[1]);
                        reportRow.setResult(Integer.valueOf(fields[2]));
                        reportRow.setErrorCode(fields[3]);
                        reportRow.setTransactionTime(dateFormat.parse(fields[4]));
                        reportRow.setTransactionAmount(Double.valueOf(fields[5]));
                        reportRow.setMsisdn(fields[6]);
                        reportRow.setReport(report);
                        report.getReportRows().add(reportRow);
                    }
                    thisLine = reader.readLine();
                }
                //save report
                new EntityManager().SAVE(report);
            }
        }
        catch (MessagingException e) {
            logger.error("Message get failed: ", e);
            throw new DataStreamException("err.umc_pop3_reporter.message_get_failed", e);
        }
        catch (IOException e) {
            logger.error("Message read failed: ", e);
            throw new DataStreamException("err.umc_pop3_reporter.message_read_failed", e);
        }
        catch (ParseException e) {
            logger.error("Message parsing failed: ", e);
            throw new DataStreamException("err.umc_pop3_reporter.message_parsing_failed", e);
        }
        catch(DatabaseException e){
            logger.error("Message saving failed: ", e);
            throw new DataStreamException("err.umc_pop3_reporter.message_saving_failed", e);
        }
    }

}
