package com.tmx.beng.base.memory;

/**

 */
public interface InMemoryObject {
    public void setId(String id);
    public Object getValue(String propertyName);
}
