LOAD DATA
INFILE *
APPEND INTO TABLE svbill.vc_voucher
Fields terminated by ";" Optionally enclosed by '"'
TRAILING NULLCOLS
(
  VC_PRICE_ FILLER,
  VC_BESTBEFOREDATE "to_date(:VC_BESTBEFOREDATE, 'DD.MM.YYYY')",
  VC_RENEVALTIME,
  VC_SECRETCODE,
  VC_CODE,
  VC_PRICE constant '10',
  VC_NOMINAL constant 'UmcVoucher10',
  VC_VOUCHERID "voucher_seq.nextval",
  VC_STATUS constant 'NEW',
  VC_LOADINGTIME "sysdate", 
  VC_VALID constant '1'
  OP_OperatorId constant 1
)