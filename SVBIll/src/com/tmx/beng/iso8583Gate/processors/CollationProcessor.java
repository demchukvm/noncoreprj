package com.tmx.beng.iso8583Gate.processors;

import com.tmx.beng.iso8583Gate.pdu.RuntimeMessage;
import com.tmx.beng.iso8583Gate.pdu.IncorrectFormatException;
import com.tmx.beng.iso8583Gate.pdu.substructure.RuntimeSubDataElement;
import com.tmx.beng.iso8583Gate.pdu.substructure.collation.BasicCollationSDEF;
import com.tmx.beng.iso8583Gate.mapper.BillingMapper;
import com.tmx.beng.iso8583Gate.mapper.BillingMapperMetadata;
import com.tmx.beng.base.BillException;
import com.tmx.as.entities.iso8583.transaction.Iso8583Transaction;
import com.tmx.as.entities.iso8583.collation.Collation;
import com.tmx.as.exceptions.DatabaseException;
import com.tmx.as.base.EntityManager;
import com.tmx.as.base.FilterWrapper;
import com.tmx.util.InitException;

import java.util.List;
import java.util.Iterator;
import java.util.Date;
import java.util.ArrayList;

/**
 */
public class CollationProcessor extends AbstractBatchProcessor{

    public int getMessageTypeId() {
        return 510;
    }

    public int getProcessingCode() {
        return 920000;
    }

    public int[] getDEToCopy() {
        return new int[]{11, 24, 41, 42, 61};
    }

    public void processSpecificTask(RuntimeMessage reqMsg, RuntimeMessage respMsg)
        throws ProcessingException, BillException, IncorrectFormatException, InitException, DatabaseException {

        logger.debug("Collation");
        BillingMapper billingMapper = BillingMapperMetadata.getInstance().getBillMapper(reqMsg);

        //prepare and pre-save iso transaction in iso datasource
        Iso8583Transaction tx = billingMapper.preSaveTransaction(reqMsg);
        billingMapper.assignRetrievalRefNum(tx);
        billingMapper.assignAuthIdResponse(tx);

        Collation collation = prepareCollation(tx);
        List finTransactions = retrieveTransactionsForCollation(reqMsg);

        calculateBillingCollation(collation, finTransactions);
        calculateTerminalCollation(collation, reqMsg);

        //compare billing and terminal transactions
        EntityManager em = new EntityManager();

        if(collation.getBillingSalesAmountCollation().longValue() == collation.getTerminalSalesAmountCollation().longValue() &&
           collation.getBillingSalesCountCollation().longValue() == collation.getTerminalSalesCountCollation().longValue()){

            collation.setSuccessful(true);
            collation.setEndCollationTime(new Date());
            em.SAVE(collation);  
            updateTransactions(finTransactions, collation);
            closeCollationAttempts(tx, collation);
        }
        else{
            collation.setSuccessful(false);
            em.SAVE(collation);
        }

        assignBatchTransactionToCollation(tx, collation);
        billingMapper.prepare0510Msg(respMsg, collation);
    }


    private Collation prepareCollation(Iso8583Transaction tx){
        Collation collation = new Collation();
        collation.setTerminalId(tx.getTerminalId());
        collation.setMerchantId(tx.getCardAcqId());
        collation.setNumberOfBatch(tx.getBatchNumber());
        collation.setStartCollationTime(new Date());
        collation.setStartCollationTransaction(tx);
        return collation;
    }

    private List retrieveTransactionsForCollation(RuntimeMessage reqMsg) throws DatabaseException{
        FilterWrapper byMessageTypeFW = new FilterWrapper("by_fin_tx");
        FilterWrapper byTerminalIdFW = new FilterWrapper("by_isoterminalid");
        byTerminalIdFW.setParameter("terminalid", reqMsg.getRuntimeDataElement(41).getString());
        FilterWrapper byMerchantIdFW = new FilterWrapper("by_merchantid");
        byMerchantIdFW.setParameter("merchantid", reqMsg.getRuntimeDataElement(42).getString());
        FilterWrapper byNotReversedFW = new FilterWrapper("by_not_reversed");
        FilterWrapper bySuccessfulFW = new FilterWrapper("by_successfultx");
        FilterWrapper byBatchNumFW = new FilterWrapper("by_batchnum");
        byBatchNumFW.setParameter("batchnum", new Long(reqMsg.getRuntimeDataElement(61).getString()));

        List txs = new EntityManager().RETRIEVE_ALL(
                Iso8583Transaction.class,
                new FilterWrapper[]{byMessageTypeFW, byTerminalIdFW, byMerchantIdFW, byNotReversedFW, bySuccessfulFW, byBatchNumFW},
                new String[]{"collation"});

        List notClosedTxs = new ArrayList();
        for(Iterator iter = txs.iterator(); iter.hasNext();){
            Iso8583Transaction tx = (Iso8583Transaction)iter.next();
            if(tx.getCollation() == null || !tx.getCollation().getClosed())
                notClosedTxs.add(tx);
        }
        return notClosedTxs;
    }

    private void calculateBillingCollation(Collation collation, List finTransactions){
        long billingSalesCount = 0;
        long billingSalesAmount = 0;
        for(Iterator iter = finTransactions.iterator(); iter.hasNext();){
            Iso8583Transaction commitedTx = (Iso8583Transaction)iter.next();
            billingSalesCount++;
            billingSalesAmount = billingSalesAmount + (commitedTx.getAmount() != null ? commitedTx.getAmount().longValue() : 0);
        }
        collation.setBillingSalesAmountCollation(new Long(billingSalesAmount));
        collation.setBillingSalesCountCollation(new Long(billingSalesCount));
    }

    protected void calculateTerminalCollation(Collation collation, RuntimeMessage reqMsg){
        RuntimeSubDataElement rsde = reqMsg.getRuntimeDataElement(63).getSubstructureDataElement();
        String tmp = rsde.getValue(BasicCollationSDEF.SALES_COUNT);
        long terminalSalesCount = tmp != null ? Long.parseLong(tmp) : 0;
        tmp = rsde.getValue(BasicCollationSDEF.SALES_AMOUNT);
        long terminalSalesAmount = tmp != null ? Long.parseLong(tmp) : 0;

        collation.setTerminalSalesAmountCollation(new Long(terminalSalesAmount));
        collation.setTerminalSalesCountCollation(new Long(terminalSalesCount));
    }

    private void updateTransactions(List finTransactions, Collation collation) throws DatabaseException{
        EntityManager em = new EntityManager();
        for(Iterator iter = finTransactions.iterator(); iter.hasNext();){
            Iso8583Transaction tx = (Iso8583Transaction)iter.next();
            tx.setCollation(collation);
            em.SAVE(tx, new String[]{"collation"});
        }
    }

}
