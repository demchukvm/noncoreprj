package com.tmx.gate.datastream.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;

/**
 */
public class EncoderUtil {
    private static final String COMPRESSED_FILE_EXTENTION = ".crOx";

    public static void main(String[] args){
        StringBuffer helpMessage = new StringBuffer("Use arguments: (compress|decompress) (<from folder name>) (<to folder name>)");
        helpMessage.append("Example: 'compress d:/src c:/dest'.");

        if(args != null && args.length == 3){
            if("compress".equals(args[0])){
                EncoderUtil.encodeFilesBunlde(args[1], args[2]);
            }
            else if("decompress".equals(args[0])){
                EncoderUtil.decodeFilesBunlde(args[1], args[2]);
            }
            else{
                System.out.println(helpMessage.toString());
            }
        }
        else{
            System.out.println(helpMessage.toString());
        }
    }

    public static void decodeFilesBunlde(String fromFolder, String toFolder){
        File folder = new File(fromFolder);
        if(folder.exists() && folder.isDirectory()){
            Encoder encoder = new Encoder();
            String[] folderEntries = folder.list();
            for(int i=0; i<folderEntries.length; i++){
                if(folderEntries[i] != null && folderEntries[i].endsWith(COMPRESSED_FILE_EXTENTION)){
                    File encodedFile = new File(fromFolder + File.separator + folderEntries[i]);
                    if(encodedFile == null || !encodedFile.exists() || !encodedFile.isFile())
                        continue;//silent ignore
                    try{
                        FileInputStream fis = new FileInputStream(encodedFile);
                        byte[] data = new byte[fis.available()];
                        fis.read(data, 0, fis.available());
                        data = encoder.CheckAndDecompress(data);
                        //store file
                        String decompressedFileName = folderEntries[i].substring(0, folderEntries[i].length() - COMPRESSED_FILE_EXTENTION.length());
                        FileOutputStream fos = new FileOutputStream(toFolder + File.separator + decompressedFileName);
                        fos.write(data);
                        fos.flush();
                    }
                    catch(Throwable e){
                        System.out.println("Failed to decompress file '"+folderEntries[i]+"': " + e.toString());
                    }
                    System.out.println("Successfully decompressed file '"+folderEntries[i]+"'");
                }
                else{
                    System.out.println("File '"+folderEntries[i]+"' have been ignored, because it hasn't '"+COMPRESSED_FILE_EXTENTION+"' extension.");
                }
            }
        }
    }

    /** RM: do not supports last crox version */
    public static void encodeFilesBunlde(String fromFolder, String toFolder){
        File folder = new File(fromFolder);
        if(folder.exists() && folder.isDirectory()){
            Encoder encoder = new Encoder();
            String[] folderEntries = folder.list();
            for(int i=0; i<folderEntries.length; i++){
                if(folderEntries[i] != null){
                    File srcFile = new File(fromFolder + File.separator + folderEntries[i]);
                    if(srcFile == null || !srcFile.exists() || !srcFile.isFile())
                        continue;//silent ignore
                    try{
                        FileInputStream fis = new FileInputStream(srcFile);
                        byte[] data = new byte[fis.available()];
                        fis.read(data, 0, fis.available());
                        data = encoder.Compress(data);
                        //store file
                        String compressedFileName = folderEntries[i] + COMPRESSED_FILE_EXTENTION;
                        FileOutputStream fos = new FileOutputStream(toFolder + File.separator + compressedFileName);
                        fos.write(data);
                        fos.flush();
                    }
                    catch(Throwable e){
                        System.out.println("Failed to compress file '"+folderEntries[i]+"': " + e.toString());
                    }
                    System.out.println("Successfully compressed file '"+folderEntries[i]+"'");
                }
            }
        }
    }
}
