<%@ page language="java" pageEncoding="UTF-8" contentType="text/html; charset=utf-8" isELIgnored="false" %>
<%@ taglib uri="/WEB-INF/tld/struts-nested.tld" prefix="nested" %>
<%@ taglib uri="/WEB-INF/tld/tmx-controls.tld" prefix="tmx_ctrl" %>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>

<html:form action="/core/settings/personal/personalInfo">
<table border="0" width="100%">
    <!-- BUTTONS -->
    <tr>
        <td style="text-align: left">
            <tmx_ctrl:button property="onSaveButton"/>
            <tmx_ctrl:button property="onResetButton" onClick="javascript:document.forms[0].reset()"/>
        </td>
    </tr>
    <tr>
        <!--Alarm Type-->
        <td valign="center" align="left" class="tabForm">
            <table width="100%" border="0">
                <!-- Form header -->
                <tr>
                    <td colspan="2">
                        <h4 class="dataLabel">
                            <bean:message bundle="core.labels" key="core.page.settings.personal.personalinfoform.title"/>
                        </h4>
                    </td>
                </tr>
                <tr>
                    <td class="dataLabel" colspan="2">&nbsp;</td>
                </tr>
                <tr>
                    <!-- ID -->
                    <td class="dataLabel">
                        <tmx_ctrl:writeEntityLabel attrName="userId"
                                                   entityClassName="com.tmx.as.entities.general.user.User"/>:
                    </td>
                    <td class="dataField">
                        <tmx_ctrl:editBox property="userId"/>
                    </td>
                    <!-- FIRSTNAME -->
                    <td class="dataLabel">
                        <tmx_ctrl:writeEntityLabel attrName="firstName"
                                                   entityClassName="com.tmx.as.entities.general.user.User"/>:
                    </td>
                    <td class="dataField">
                        <tmx_ctrl:editBox property="firstName"/>
                    </td>
                </tr>
                <tr>
                    <!-- LOGIN -->
                    <td class="dataLabel">
                        <tmx_ctrl:writeEntityLabel attrName="login"
                                                   entityClassName="com.tmx.as.entities.general.user.User"/>:
                    </td>
                    <td class="dataField">
                        <tmx_ctrl:editBox property="login"/>
                    </td>
                    <!-- LASTNAME -->
                    <td class="dataLabel">
                        <tmx_ctrl:writeEntityLabel attrName="lastName"
                                                   entityClassName="com.tmx.as.entities.general.user.User"/>:
                    </td>
                    <td class="dataField">
                        <tmx_ctrl:editBox property="lastName"/>
                    </td>
                </tr>
                <tr>
                    <!-- PASSWORD -->
                    <td class="dataLabel">
                        <tmx_ctrl:writeEntityLabel attrName="password"
                                                   entityClassName="com.tmx.as.entities.general.user.User"/>:
                    </td>
                    <td class="dataField">
                        <tmx_ctrl:passwordEditBox property="password"/>
                    </td>
                    <!-- ROLE -->
                    <td class="dataLabel">
                        <tmx_ctrl:writeEntityLabel attrName="role"
                                                   entityClassName="com.tmx.as.entities.general.user.User"/>:
                    </td>
                    <td class="dataField">
                        <tmx_ctrl:selectBox property="selectRoles"/>
                    </td>
                </tr>
                <tr>
                    <!-- PASSWORD 2 -->
                    <td class="dataLabel">
                        <bean:message bundle="core.labels" key="core.page.dictionary.user.userinfoform.label.password2"/>:
                    </td>
                    <td class="dataField">
                        <tmx_ctrl:passwordEditBox property="password2"/>
                    </td>
                    <!-- ORGANIZATION -->
                    <td class="dataLabel">
                        <tmx_ctrl:writeEntityLabel attrName="organization"
                                                   entityClassName="com.tmx.as.entities.general.user.User"/>:
                    </td>
                    <td class="dataField">
                        <tmx_ctrl:selectBox property="selectOrganisations"/>
                    </td>
                </tr>

                <tr>
                    <!-- SELLER -->
                    <td class="dataLabel">
                        <tmx_ctrl:writeEntityLabel attrName="{@name}"
                                                   entityClassName="com.tmx.as.entities.bill.seller.Seller"/>:
                    </td>
                    <td class="dataField">
                        <tmx_ctrl:selectBox property="selectSellers"/>
                    </td>

                    <td class="dataLabel">

                    </td>
                    <td class="dataField">

                    </td>
                </tr>


            </table>
        </td>
    </tr>
    <!-- BUTTONS -->
    <tr>
        <td style="text-align: left">
            <tmx_ctrl:button property="onSaveButton"/>
            <tmx_ctrl:button property="onResetButton" onClick="javascript:document.forms[0].reset()"/>
        </td>
    </tr>    
</table>
</html:form>