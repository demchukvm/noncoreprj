package com.tmx.engines.xmlengine;

import java.util.Locale;


public class DocumentHeader {
    private String serverUrl = null;
    private String appContextName = null;
    private String prevPage = null;
    private String currentPage = null;
    private String nextPage = null;
    private String pageName = null;
    private Locale locale = null;

    public String getServerUrl() {
        return serverUrl;
    }

    public void setServerUrl(String serverUrl) {
        this.serverUrl = serverUrl;
    }

    public String getAppContextName() {
        return appContextName;
    }

    public void setAppContextName(String appContextName) {
        this.appContextName = appContextName;
    }

    public String getPrevPage() {
        return prevPage;
    }

    public void setPrevPage(String prevPage) {
        this.prevPage = prevPage;
    }

    public String getNextPage() {
        return nextPage;
    }

    public void setNextPage(String nextPage) {
        this.nextPage = nextPage;
    }

    public String getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(String currentPage) {
        this.currentPage = currentPage;
    }

    public String getPageName() {
        return pageName;
    }

    public void setPageName(String pageName) {
        this.pageName = pageName;
    }

    public String getFullAppUrl(){
        return getServerUrl() + getAppContextName();
    }

    public Locale getLocale() {
        return locale;
    }

    public void setLocale(Locale locale) {
        this.locale = locale;
    }
}
