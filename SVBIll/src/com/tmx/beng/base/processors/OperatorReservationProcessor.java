package com.tmx.beng.base.processors;

import com.tmx.as.base.ProcedureWrapper;
import com.tmx.as.base.QueryParameterWrapper;
import com.tmx.as.entities.bill.operator.Operator;
import com.tmx.as.entities.bill.transaction.OperatorBalanceTransaction;
import com.tmx.as.entities.bill.transaction.TransactionType;

import com.tmx.beng.base.*;
import com.tmx.beng.base.cache.DBCache;
import com.tmx.beng.base.cache.DBCacheException;

import java.sql.Types;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;


public class OperatorReservationProcessor extends DefaultReservationProcessor{


    protected List prepareOperatorTransactions(BillingMessageWritable msg, DBCache dbCache, TransConfig config)
            throws DBCacheException {
        OperatorBalanceTransaction transaction = new OperatorBalanceTransaction();
        //setup
        transaction.setTransactionTime(config.transRegTime);
        transaction.setClientTime(config.clientTime);

        Reservation reservation = config.reservationSet.getReservation(Operator.class, config.operatorCode);
        Iterator iter = reservation.reservesIterator();
        Reservation.Reserve reserve = (Reservation.Reserve) iter.next();

        if (iter.hasNext())
            throw new IllegalStateException("reserves isn't one");

        transaction.setAmount(new Double(reserve.getAmount()));

        transaction.setOperator(dbCache.getOperator(config.operatorCode));
        transaction.setOperatorBalance(dbCache.getOperatorBalance(config.operatorCode, reserve.getBalanceCode()));

        transaction.setTransactionSupport(config.transactionSupport);
        transaction.setTransactionGUID(config.transactionGUID);

        transaction.setCliTransactionNum(config.cliTransactionNum);
        transaction.setType(new TransactionType(reserve.getTransTypeCode()));
        transaction.setOperationType(config.operationsType);

        dbCache.changeOperatorBalance(config.operatorCode, reserve.getBalanceCode(), reserve.getAmount(),
                reserve.getTransTypeCode());

        //TODO:(AN) ?????
        msg.setStatusCode(StatusDictionary.STATUS_OK);

        List result = new ArrayList();
        result.add(transaction);
        return result;
    }

    protected List prepareSellerTransactions(BillingMessageWritable msg, DBCache dbCache, TransConfig config)
            throws DBCacheException {
        return Collections.EMPTY_LIST;
    }

    protected List prepareTerminalTransactions(BillingMessageWritable msg, DBCache dbCache, TransConfig config)
            throws DBCacheException {
        return Collections.EMPTY_LIST;
    }

    protected List prepareClientTransaction(BillingMessageWritable msg, DBCache dbCache, TransConfig config)
            throws DBCacheException {
        return Collections.EMPTY_LIST;
    }

    protected List prepareChangeBalancesProcedure(BillingMessageWritable msg, DBCache dbCache, TransConfig config)
            throws DBCacheException {
        Reservation reservation = config.reservationSet.getReservation(Operator.class, config.operatorCode);
        Iterator iter = reservation.reservesIterator();
        Reservation.Reserve reserve = (Reservation.Reserve) iter.next();

        if (iter.hasNext())
            throw new IllegalStateException("reserves isn't one");

        ProcedureWrapper wrapper = new ProcedureWrapper(
                new QueryParameterWrapper[]{
                        QueryParameterWrapper.createOutParameter(1, Types.INTEGER),
                        new QueryParameterWrapper(2, dbCache.getOperatorBalance(config.operatorCode, reserve.getBalanceCode()).getBalanceId()),
                        new QueryParameterWrapper(3,
                                new Double(getSignedAmount(reserve.getTransTypeCode(), reserve.getAmount()))),
                },
                Operator.class,
                "change_operator_balance");

        List result = new ArrayList();
        result.add(wrapper);

        return result;
    }

    public Task rollback(Task task, Throwable e) {
        logger.info("Rollback operator change...");
        try {

            BillingMessageWritable msg = task.getProcesedMessage();
            DBCache dbCache = task.getSessionDBCache();
            ReservationSet reservationSet = (ReservationSet) msg.getAttribute(BillingMessage.BALANCE_RESERVATION_SET);

            String serviceCode = msg.getAttributeString(BillingMessage.SERVICE_CODE);
            Reservation reservation = reservationSet.getReservation(Operator.class, serviceCode);

            for (Iterator iter = reservation.reservesIterator(); iter.hasNext();) {
                Reservation.Reserve reserve = (Reservation.Reserve) iter.next();
                if (reserve.isApplied())
                    dbCache.changeOperatorBalance(serviceCode, reserve.getBalanceCode(), -reserve.getAmount(),
                            reserve.getTransTypeCode());
                reservation.cancelReserve(reserve.getId());
            }

        }
        catch (Throwable te) {
            logger.error(te.getLocalizedMessage(), te);
        }

        return super.rollback(task, e);
    }
}
