package com.tmx.as.scheduler;


import com.tmx.as.base.EntityManager;
import com.tmx.as.base.FilterWrapper;
import com.tmx.as.entities.general.config_property.ConfigProperty;
import com.tmx.as.exceptions.DatabaseException;

import java.util.*;
import java.text.*;

import org.apache.log4j.Logger;


/**
 * schedule for processing threads (one schedule per one thread)
 */
public class Schedule {
    /** Thread name */
    private String threadName;
    /** Map of [String confPropName][ConfigProeprtiy confProp]  */
    Map configProperties = new HashMap();
    Logger logger = Logger.getLogger("scheduler.Schedule");

    /**
     * Simply constructor
     */
    public Schedule(String threadName) {
        this.threadName = threadName;
    }


    /**
     * Load schedule from DB
     */
    public void loadFromDB() throws DatabaseException{

        FilterWrapper byKey = new FilterWrapper("by_key");
        byKey.setParameter("key", getKeyPrefix() + "%"); //'%' is for 'like' in SQL
        List configPropertiesList = (List)new EntityManager().RETRIEVE_ALL(ConfigProperty.class, new FilterWrapper[]{byKey});

        configProperties.clear();
        for(Iterator iter = configPropertiesList.iterator(); iter.hasNext();){
            ConfigProperty confProperty = (ConfigProperty)iter.next();
            configProperties.put(confProperty.getKey(), confProperty);
        }
    }


    /** Safely load schedule from database. On error default values will be used */
    public void safeLoadFromDB(){
        try{
            loadFromDB();
        }
        catch(Throwable e){
            logger.info("Failed to load schedule from DB");
        }
    }

    /**
     * Show is the thread is blocked or not
     */
    public boolean getBlocked() {
        return "true".equals(getConfigProperty("Blocked").getValue());
    }


    /**
     * Set blocked.
     */
    public void setBlocked(boolean newBlocked) throws DatabaseException{
        ConfigProperty confProperty = getConfigProperty("Blocked");
        if (newBlocked)
            setProperty(confProperty, "true");
        else
            setProperty(confProperty, "false");
    }

    /**
     * The start date of thread (ms)
     */
    public long getStartDate() {
        long startDate = new Long(0).longValue();//default value
        try{
            startDate = Long.parseLong(getConfigProperty("StartDate").getValue());
        }
        catch(Exception e){/** ignore */}
        return startDate;
    }

    /**
     * Return planned start date in specified format
     */
    public String getFormatedStartDate(String pattern) {
        return new SimpleDateFormat(pattern).format(new Date(getStartDate()));
    }

    /**
     * Set start date (ms).
     */
    public void setStartDate(long newStartDate) throws DatabaseException{
        setProperty(getConfigProperty("StartDate"), String.valueOf(newStartDate));
    }

    /**
     * Set start date from string date using specified pattern
     */
    public void setFormatedStartDate(String newStartDate, String pattern) throws DatabaseException{
        SimpleDateFormat formatter = new SimpleDateFormat(pattern);
        ParsePosition pos = new ParsePosition(0);
        Date newDate = formatter.parse(newStartDate, pos);
        long newStartDateMs = newDate.getTime();
        setStartDate(newStartDateMs);
    }

    /**
     * The interval of thread's activity (ms)
     */
    public long getInterval() {
        long interval = new Long(10000).longValue();//default value
        try{
            interval = Long.parseLong(getConfigProperty("Interval").getValue());
        }
        catch(Exception e){/** ignore */}
        return interval;
    }

    /**
     * Return interval in minutes
     */
    public long getIntervalMinutes() {
        return (getInterval() / (60 * 1000));
    }


    /**
     * Set intrval (ms).
     */
    public void setInterval(long newInterval) throws DatabaseException{
        setProperty(getConfigProperty("Interval"), String.valueOf(newInterval));
    }

    /**
     * Set intrval in minutes.
     */
    public void setIntervalMinutes(long newIntervalMin) throws DatabaseException{
        setInterval(newIntervalMin * 60 * 1000);
    }


    /**
     * The time of thread's last start up (ms)
     */
    public long getLastStartUpTime() {
        long lastStartUpTime = new Long(0).longValue();//default value
        try{
            lastStartUpTime = Long.parseLong(getConfigProperty("LastStartUpTime").getValue());
        }
        catch(Exception e){/** ignore */}
        return lastStartUpTime;
    }


    /**
     * Return last start up date in specified format
     */
    public String getFormatedLastStartUpTime(String pattern) {
        return new SimpleDateFormat(pattern).format(new Date(getLastStartUpTime()));
    }

    /**
     * Set last start up Date (ms)
     */
    public void setLastStartUpTime(long newLastStartUpTime) throws DatabaseException{
        setProperty(getConfigProperty("LastStartUpTime"), String.valueOf(newLastStartUpTime));
    }

    /** Set last start up Date from string date using specified pattern */
    public void setFormatedLastStartUpTime(String newLastStartUpTime, String pattern) throws DatabaseException{
        SimpleDateFormat formatter = new SimpleDateFormat(pattern);
        ParsePosition pos = new ParsePosition(0);
        java.util.Date newDate = formatter.parse(newLastStartUpTime, pos);
        long newLastStartUpTimeMs = newDate.getTime();
        setLastStartUpTime(newLastStartUpTimeMs);
    }

    //------------------------ pivate methods

    /** RM: do not use separete select per each getProeprty()
     * read specified configuration property value by key from database
    private String getProperty(String key) throws DatabaseException {
        FilterWrapper byKey = new FilterWrapper("by_key");
        byKey.setParameter("key", key);
        ConfigProperty configProperty = (ConfigProperty)new EntityManager().RETRIEVE(ConfigProperty.class, new FilterWrapper[]{byKey}, null);
        return (configProperty == null) ? null : configProperty.getValue();
    }
    */


    private void setProperty(ConfigProperty configProperty, String newValue) throws DatabaseException{
        configProperty.setValue(newValue);
        new EntityManager().SAVE(configProperty);
    }


    private String getKeyPrefix(){
        return "ProcessingThread." + threadName;
    }

    private ConfigProperty getConfigProperty(String suffix){
        String key = getKeyPrefix() + "." + suffix;
        ConfigProperty confProperty = (ConfigProperty)configProperties.get(key);
        if(confProperty == null){
            confProperty = new ConfigProperty();
            confProperty.setKey(key);
        }
        return confProperty;
    }


}