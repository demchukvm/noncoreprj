package com.tmx.as.resolver;

/**
 * Holds executed Query and retrieved List or Entity (on future maybe
 * something else) as Object.
 * */
public class QueryResult {
    private Query executedQuery = null;
    private Object retrievedResult = null;
    private Integer resultSize = null;

    /** private constructor to hide default constructor */
    private QueryResult(){
    }

    public QueryResult(Query executedQuery, Object retrievedResult){
        this.executedQuery = executedQuery;
        this.retrievedResult = retrievedResult;
    }

    public QueryResult(Query executedQuery, Object retrievedResult, int resultSize){
        this.executedQuery = executedQuery;
        this.retrievedResult = retrievedResult;
        this.resultSize = new Integer(resultSize);
    }

    public Query getExecutedQuery() {
        return executedQuery;
    }

    public Object getRetrievedResult() {
        return retrievedResult;
    }

    public Integer getResultSize(){
        return resultSize;
    }
}
