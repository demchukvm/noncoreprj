package com.tmx.beng.iso8583Gate.processors;

import com.tmx.beng.iso8583Gate.processors.ISOProcessor;
import com.tmx.beng.iso8583Gate.pdu.RuntimeMessage;
import com.tmx.beng.iso8583Gate.pdu.RuntimeDataElement;
import com.tmx.beng.iso8583Gate.pdu.IncorrectFormatException;
import com.tmx.beng.iso8583Gate.pdu.DataElementFormat;


/**
 */
public class ProcessorProvider {

    /** provide iso processor specific for this message  */
    public ISOProcessor provideProcessor(RuntimeMessage msg) throws ProcessingException{
        RuntimeDataElement messageTypeIdRDE = msg.getRuntimeDataElement(DataElementFormat.DEF_MESSAGETYPEID);
        RuntimeDataElement processingCodeRDE = msg.getRuntimeDataElement(DataElementFormat.DEF_PROCESSINGCODE);
        try{
            if(messageTypeIdRDE.getLong() == 100 && processingCodeRDE.getLong() == 310000){
                return new GetSellStatusProcessor();
            }
            else if(messageTypeIdRDE.getLong() == 200 && processingCodeRDE.getLong() == 0){
                return new SellServiceProcessor();
            }
//test plese wait
//            else if(messageTypeIdRDE.getLong() == 200 && processingCodeRDE.getLong() == 0){
//                return new PleaseWaitProcessor();
//            }
            else if(messageTypeIdRDE.getLong() == 200 && processingCodeRDE.getLong() == 20000){
                return new CancelSellServiceProcessor();
            }
            else if(messageTypeIdRDE.getLong() == 201 && processingCodeRDE.getLong() == 0){
                return new RepeatSellServiceProcessor();
            }
            else if(messageTypeIdRDE.getLong() == 400){
                return new UniversalReversalProcessor();
            }
            else if(messageTypeIdRDE.getLong() == 500 && processingCodeRDE.getLong() == 920000){
                return new CollationProcessor();
            }
            else if(messageTypeIdRDE.getLong() == 320){
                return new BatchUploadProcessor();
            }
            else if(messageTypeIdRDE.getLong() == 520 && processingCodeRDE.getLong() == 930000){
                return new EndBatchUploadProcessor();
            }
            else if(messageTypeIdRDE.getLong() == 800 && processingCodeRDE.getLong() == 960000){
                return new AuthKeysUpdateProcessor();
            }
            else if(messageTypeIdRDE.getLong() == 800 && processingCodeRDE.getLong() == 990000){
                return new TestConnectionProcessor();
            }
            throw new ProcessingException("err.processor_provider.no_processor", new String[]{String.valueOf(messageTypeIdRDE.getLong()), String.valueOf(processingCodeRDE.getLong())});
        }
        catch(IncorrectFormatException e){
            throw new ProcessingException("err.processor_provider.failed_to_read_message", new String[]{msg.dumpMessage()}, e);
        }

    }
}
