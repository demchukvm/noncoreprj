package com.tmx.web.taglib.common;

import org.apache.struts.taglib.TagUtils;
import javax.servlet.jsp.JspException;
import com.tmx.util.Configuration;
import com.tmx.web.taglib.base.LocalizableTag;

/** Writes value of speciifed parameter from configuration */
public class WriteConfigParamTag extends LocalizableTag {
    private String paramName = null;

    //--------------------Properties

    public String getParamName() {
        return paramName;
    }

    public void setParamName(String paramName) {
        this.paramName = paramName;
    }
    //------------------------------Parsing events

    public int doStartTmxTag() throws JspException {
        String paramValueHTML = renderParamValueHTML();
        TagUtils.getInstance().write(pageContext, paramValueHTML);
        return (EVAL_BODY_TAG);
    }

    //------------------------------Rendering methods

    private String renderParamValueHTML(){
        return Configuration.getInstance().getProperty(getParamName());
    }
}
