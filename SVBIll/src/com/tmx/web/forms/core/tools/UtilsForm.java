package com.tmx.web.forms.core.tools;              

import com.tmx.web.base.BasicActionForm;
import com.tmx.web.controls.*;
import com.tmx.as.entities.general.user.User;
import com.tmx.as.entities.bill.voucher.BatchBuyVoucher;
import com.tmx.as.entities.bill.seller.Seller;
import com.tmx.as.entities.bill.processing.Processing;
import com.tmx.as.exceptions.DatabaseException;
import com.tmx.beng.csapi.java.base.BuyVoucherResp;
import com.tmx.beng.base.BillingMessage;

import java.util.List;
import java.util.LinkedList;

/**
 */
public class UtilsForm extends BasicActionForm {
    private EditBox ebBillingTransactionNumbers;

    private Button btnGetStatus;
    private Button btnRemoveMediumAccesses;
    private Button btnRestroreMediumAccesses;
    private Button btnRestoreDefault;

    private MultipleSelectBox multipleSelectBoxMediumAccesses;
    private MultipleSelectBox multipleSelectBoxRemovedElements;

    protected void init() {
        initEditBoxes();
        initButtons();
        initSelectBoxes();
        clean();
    }

    private void initSelectBoxes() {
        multipleSelectBoxMediumAccesses = new MultipleSelectBox() {
            protected Object getKey(Object dataListElement) {
                return dataListElement;
            }

            protected String getValue(Object dataListElement) {
                return (String)dataListElement;
            }
        };
        multipleSelectBoxRemovedElements = new MultipleSelectBox() {
            protected Object getKey(Object dataListElement) {
                return dataListElement;
            }

            protected String getValue(Object dataListElement) {
                return (String)dataListElement;
            }
        };
    }

    private void initButtons() {
        btnGetStatus = new Button();
        btnGetStatus.setAction("/core/tools/utils");
        btnGetStatus.setCommand("getStatus");

        btnRemoveMediumAccesses = new Button();
        btnRemoveMediumAccesses.setAction("/core/tools/utils");
        btnRemoveMediumAccesses.setLabelKey("ctrl.button.label.blocked");
        btnRemoveMediumAccesses.setCommand("removeMediumAccesses");

        btnRestroreMediumAccesses = new Button();
        btnRestroreMediumAccesses.setAction("/core/tools/utils");
        btnRestroreMediumAccesses.setLabelKey("ctrl.button.label.unblocked");
        btnRestroreMediumAccesses.setCommand("restroreMediumAccesses");

        btnRestoreDefault = new Button();
        btnRestoreDefault.setAction("/core/tools/utils");
        btnRestoreDefault.setLabelKey("ctrl.button.label.unblockedAll");
        btnRestoreDefault.setCommand("restoreDefault");
    }

    private void initEditBoxes() {
        ebBillingTransactionNumbers = new EditBox();
    }

    public void bindData() throws DatabaseException {

    }

    public void clean(){
        ebBillingTransactionNumbers.setValue(null);
    }

    public void setDefaults(){

    }


    public EditBox getEbBillingTransactionNumbers() {
        return ebBillingTransactionNumbers;
    }

    public void setEbBillingTransactionNumbers(EditBox ebBillingTransactionNumbers) {
        this.ebBillingTransactionNumbers = ebBillingTransactionNumbers;
    }

    public Button getBtnGetStatus() {
        return btnGetStatus;
    }

    public void setBtnGetStatus(Button btnGetStatus) {
        this.btnGetStatus = btnGetStatus;
    }

    public MultipleSelectBox getMultipleSelectBoxMediumAccesses() {
        return multipleSelectBoxMediumAccesses;
    }

    public void setMultipleSelectBoxMediumAccesses(MultipleSelectBox multipleSelectBoxMediumAccesses) {
        this.multipleSelectBoxMediumAccesses = multipleSelectBoxMediumAccesses;
    }

    public MultipleSelectBox getMultipleSelectBoxRemovedElements() {
        return multipleSelectBoxRemovedElements;
    }

    public void setMultipleSelectBoxRemovedElements(MultipleSelectBox multipleSelectBoxRemovedElements) {
        this.multipleSelectBoxRemovedElements = multipleSelectBoxRemovedElements;
    }

    public Button getBtnRemoveMediumAccesses() {
        return btnRemoveMediumAccesses;
    }

    public void setBtnRemoveMediumAccesses(Button btnRemoveMediumAccesses) {
        this.btnRemoveMediumAccesses = btnRemoveMediumAccesses;
    }

    public Button getBtnRestoreDefault() {
        return btnRestoreDefault;
    }

    public void setBtnRestoreDefault(Button btnRestoreDefault) {
        this.btnRestoreDefault = btnRestoreDefault;
    }

    public Button getBtnRestroreMediumAccesses() {
        return btnRestroreMediumAccesses;
    }

    public void setBtnRestroreMediumAccesses(Button btnRestroreMediumAccesses) {
        this.btnRestroreMediumAccesses = btnRestroreMediumAccesses;
    }
}