package com.tmx.beng.base.cache;

import com.tmx.as.base.EntityManager;
import com.tmx.as.entities.bill.transaction.OperationType;
import com.tmx.as.exceptions.DatabaseException;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Iterator;


public class OperationTypes {

    Map typeName_type = new HashMap(); 

    OperationTypes() throws DatabaseException {
        List operTypes = new EntityManager().RETRIEVE_ALL(OperationType.class);
        for (Iterator iterator = operTypes.iterator(); iterator.hasNext();) {
            OperationType type = (OperationType) iterator.next();
            typeName_type.put(type.getName(), type);
        }
    }

    public OperationType getOperationType(String name){
        return (OperationType) typeName_type.get(name);
    }

}
