package com.tmx.beng.base.processors;

import com.tmx.as.base.EntityManager;
import com.tmx.as.base.FilterWrapper;
import com.tmx.as.base.ProcedureWrapper;
import com.tmx.as.base.QueryParameterWrapper;
import com.tmx.as.entities.bill.balance.OperatorBalance;
import com.tmx.as.entities.bill.balance.SellerBalance;
import com.tmx.as.entities.bill.balance.TerminalBalance;
import com.tmx.as.entities.bill.operator.Operator;
import com.tmx.as.entities.bill.seller.Seller;
import com.tmx.as.entities.bill.terminal.Terminal;
import com.tmx.as.entities.bill.transaction.*;
import com.tmx.beng.base.*;
import com.tmx.beng.base.cache.DBCache;
import com.tmx.beng.base.cache.DBCacheException;

import java.util.*;
import java.sql.Types;

/**
 */
public class ReservationProcessor extends TaskProcessor {
    private final String SCHEMA_NAME = "svbill";

    public Task process0(Task task, DBCache dbCache, BillingMessageWritable msg)throws BillException{
        logger.debug("Reservation...");
//        EntityManager entityManager = new EntityManager();
//        try{
//            //get credit transaction type
//            EntityManager em = new EntityManager();
//            FilterWrapper byTypeCode = new FilterWrapper("by_ttypename");
////            byTypeCode.setParameter("name", TransactionType.CREDIT.toString());
//            TransactionType ttCredit = (TransactionType)em.RETRIEVE(TransactionType.class, new FilterWrapper[]{byTypeCode}, null);
//
//            List transactions = new Vector();
//            Date transRegTime = (Date)msg.getAttribute(BillingMessage.REGISTRATION_TIME);
//            Date clientTime = (Date)msg.getAttribute(BillingMessage.CLIENT_TIME);
//            String status = Integer.toString(StatusDictionary.TRANSACTION_OPENED);
//
//            ReservationSet reservationSet = (ReservationSet)msg.getAttribute(BillingMessage.BALANCE_RESERVATION_SET);
//
//            // operator transaction
//            String operatorCode = msg.getAttributeString(BillingMessage.SERVICE_CODE);
//            List operatorBalanceTransactions = prepareOperatorTransactions(
//                    msg,
//                    dbCache,
//                    operatorCode,
//                    transRegTime,
//                    clientTime,
//                    reservationSet,
//                    ttCredit);
//            transactions.addAll(operatorBalanceTransactions);
//
//
//            // sellers transaction
//            String sellerCode = msg.getAttributeString(BillingMessage.SELLER_CODE);
//            List sellersParentsChainCodes = dbCache.getSellerWrapperParentsChainCodes(sellerCode);
//            for(Iterator sellersIter = sellersParentsChainCodes.iterator(); sellersIter.hasNext();){
//                String nextSellerCode = (String)sellersIter.next();
//                List sellerBalanceTransactions = prepareSellerTransactions(
//                        msg,
//                        dbCache,
//                        nextSellerCode,
//                        transRegTime,
//                        clientTime,
//                        reservationSet,
//                        ttCredit);
//                transactions.addAll(sellerBalanceTransactions);
//            }
//
//
//            // terminal transaction
//            String terminalSn = msg.getAttributeString(BillingMessage.TERMINAL_SN);
//            List terminalBalanceTransactions = prepareTerminalTransactions(
//                    msg,
//                    dbCache,
//                    terminalSn,
//                    transRegTime,
//                    clientTime,
//                    reservationSet,
//                    ttCredit);
//            transactions.addAll(terminalBalanceTransactions);
//
//
//            //client transaction
//            transactions.add(
//                    prepareClientTransaction(msg,
//                            dbCache,
//                            transRegTime,
//                            clientTime,
//                            ttCredit));
//
//            entityManager.BEGIN(SCHEMA_NAME);
//            for(Iterator it = transactions.iterator(); it.hasNext(); ){
//                Object obj = it.next();
//                if(obj instanceof AbstractTransaction){
//                    AbstractTransaction tx = (AbstractTransaction)obj;
//                    tx.setStatus(status);
//                    entityManager.SAVE(tx);
//                }
//            }
//            entityManager.COMMIT(SCHEMA_NAME);
//
//            // change balance procedure
//            ProcedureWrapper procedureWrapper = prepareChangeBalancesProcedure(
//                    dbCache,
//                    operatorCode,
//                    sellerCode,
//                    terminalSn,
//                    reservationSet
//            );
//            transactions.add(procedureWrapper);
//
//            // save
//            msg.setAttribute(BillingMessage.ENTITY_LIST, transactions);
//
//        }
//        catch(Throwable e){
//            task.getProcesedMessage().setStatusCode(StatusDictionary.RESERVATION_ERROR);
//            logger.error(e.getLocalizedMessage(), e);
//            if(e instanceof BillException)
//                throw (BillException)e;
//            throw new BillException(e.getLocalizedMessage(), e);
//        }
        return task;
    }

//    public Task rollback(Task task, Throwable e) {
//        logger.info("Rollback Reservation...");
//        try{
//            // operator transaction
//            BillingMessageWritable msg = task.getProcesedMessage();
//            DBCache dbCache = task.getSessionDBCache();
//            ReservationSet reservationSet = (ReservationSet)msg.getAttribute(BillingMessage.BALANCE_RESERVATION_SET);
//
//            // cancel operator reservations
//            String operatorCode = msg.getAttributeString(BillingMessage.SERVICE_CODE);
//            Reservation reservation = reservationSet.getReservation(Operator.class, operatorCode);
//            for(Iterator iter = reservation.reservesIterator(); iter.hasNext();){
//                Reservation.Reserve reserve = (Reservation.Reserve)iter.next();
//                if(reserve.isApplied())
//                    dbCache.changeOperatorBalance(
//                            operatorCode,
//                            reserve.getBalanceCode(),
//                            -reserve.getAmount());
//                    reservation.cancelReserve(reserve.getId());
//            }
//
//            // cancel seller reservations
//            String sellerCode = msg.getAttributeString(BillingMessage.SELLER_CODE);
//            List sellersParentsChainCodes = dbCache.getSellerWrapperParentsChainCodes(sellerCode);
//            for(Iterator sellersIter = sellersParentsChainCodes.iterator(); sellersIter.hasNext();){
//                String nextSellerCode = (String)sellersIter.next();
//                reservation = reservationSet.getReservation(Seller.class, nextSellerCode);
//                for(Iterator iter = reservation.reservesIterator(); iter.hasNext();){
//                    Reservation.Reserve reserve = (Reservation.Reserve)iter.next();
//                    if(reserve.isApplied())
//                        dbCache.changeSellerBalance(
//                                nextSellerCode,
//                                reserve.getBalanceCode(),
//                                -reserve.getAmount());
//                        reservation.cancelReserve(reserve.getId());
//                }
//            }
//
//            // cancel terminal reservations
//            String terminalSn = msg.getAttributeString(BillingMessage.TERMINAL_SN);
//            reservation = reservationSet.getReservation(Terminal.class, terminalSn);
//            for(Iterator iter = reservation.reservesIterator(); iter.hasNext();){
//                Reservation.Reserve reserve = (Reservation.Reserve)iter.next();
//                if(reserve.isApplied())
//                    dbCache.changeTerminalBalance(
//                            terminalSn,
//                            reserve.getBalanceCode(),
//                            -reserve.getAmount());
//                    reservation.cancelReserve(reserve.getId());
//            }
//        }
//        catch(Throwable te){
//            logger.error("Rollback reservation failed: " + te.getLocalizedMessage(), te);
//        }
//        return super.rollback(task, e);
//    }
//
//    private List prepareOperatorTransactions(BillingMessageWritable msg,
//                                             DBCache dbCache,
//                                             String operatorCode,
//                                             Date transRegTime,
//                                             Date clientTime,
//                                             ReservationSet reservationSet,
//                                             TransactionType ttCredit) throws DBCacheException {
//
//        Reservation reservation = reservationSet.getReservation(Operator.class, operatorCode);
//        List transactions = new ArrayList();
//        for(Iterator iter = reservation.reservesIterator(); iter.hasNext();){
//            Reservation.Reserve reserve = (Reservation.Reserve)iter.next();
//            // operator transaction
//            OperatorBalanceTransaction tx = new OperatorBalanceTransaction();
//            //time
//            tx.setTransactionTime(transRegTime);
//            tx.setClientTime(clientTime);
//            //amount
//            double amount = reserve.getAmount();
//            dbCache.changeOperatorBalance(operatorCode, reserve.getBalanceCode(), amount);
//            reserve.markApplied();
//            tx.setAmount(new Double(amount));
//            //operator
//            Operator operator = new Operator();
//            operator.setOperatorId(dbCache.getOperator(operatorCode).getOperatorId());
//            tx.setOperator(operator);
//            //operator balance
//            OperatorBalance operatorBalance = new OperatorBalance();
//            operatorBalance.setBalanceId(dbCache.getOperatorBalance(operatorCode, reserve.getBalanceCode()).getBalanceId());
//            tx.setOperatorBalance(operatorBalance);
//            //BillTransactionNumber
//            tx.setTransactionGUID(msg.getAttributeString(BillingMessage.BILL_TRANSACTION_NUM));
//            //CliTransactionNumber
//            tx.setCliTransactionNum(msg.getAttributeString(BillingMessage.CLI_TRANSACTION_NUM));
//            //transaction type
//            tx.setType(ttCredit);
//
//            transactions.add(tx);
//        }
//        return transactions;
//    }
//
//
//    private List prepareSellerTransactions(BillingMessageWritable msg,
//                                           DBCache dbCache,
//                                           String sellerCode,
//                                           Date transRegTime,
//                                           Date clientTime,
//                                           ReservationSet reservationSet,
//                                           TransactionType ttCredit) throws DBCacheException {
//
//        Reservation reservation = reservationSet.getReservation(Seller.class, sellerCode);
//        List transactions = new ArrayList();
//        for(Iterator iter = reservation.reservesIterator(); iter.hasNext();){
//            Reservation.Reserve reserve = (Reservation.Reserve)iter.next();
//            SellerBalanceTransaction tx = new SellerBalanceTransaction();
//            //time
//            tx.setTransactionTime(transRegTime);
//            tx.setClientTime(clientTime);
//            //amount
//            double amount = reserve.getAmount();
//            dbCache.changeSellerBalance(sellerCode, reserve.getBalanceCode(), amount);
//            reserve.markApplied();
//            tx.setAmount(new Double(reserve.getAmount()));
//            //seller
//            Seller seller = new Seller();
//            seller.setSellerId(dbCache.getSeller(msg.getAttributeString(BillingMessage.SELLER_CODE)).getSellerId());
//            tx.setSeller(seller);
//            //seller balance
//            SellerBalance sellerBalance = new SellerBalance();
//            sellerBalance.setBalanceId(dbCache.getSellerBalance(sellerCode, reserve.getBalanceCode()).getBalanceId());
//            tx.setSellerBalance(sellerBalance);
//            //BillTransactionNumber
//            tx.setTransactionGUID(msg.getAttributeString(BillingMessage.BILL_TRANSACTION_NUM));
//            //CliTransactionNumber
//            tx.setCliTransactionNum(msg.getAttributeString(BillingMessage.CLI_TRANSACTION_NUM));
//            //transaction type
//            tx.setType(ttCredit);
//
//            transactions.add(tx);
//        }
//        return transactions;
//    }
//
//
//    private List prepareTerminalTransactions(BillingMessageWritable msg,
//                                             DBCache dbCache,
//                                             String terminalSn,
//                                             Date transRegTime,
//                                             Date clientTime,
//                                             ReservationSet reservationSet,
//                                             TransactionType ttCredit) throws DBCacheException {
//
//        Reservation reservation = reservationSet.getReservation(Terminal.class, terminalSn);
//        List transactions = new ArrayList();
//        for(Iterator iter = reservation.reservesIterator(); iter.hasNext();){
//            Reservation.Reserve reserve = (Reservation.Reserve)iter.next();
//            TerminalBalanceTransaction tx = new TerminalBalanceTransaction();
//            //time
//            tx.setTransactionTime(transRegTime);
//            tx.setClientTime(clientTime);
//            //amount
//            double amount = reserve.getAmount();
//            dbCache.changeTerminalBalance(terminalSn, reserve.getBalanceCode(), amount);
//            reserve.markApplied();
//            tx.setAmount(new Double(amount));
//            //terminal
//            Terminal terminal = new Terminal();
//            terminal.setTerminalId(dbCache.getTerminal(terminalSn).getTerminalId());
//            tx.setTerminal(terminal);
//            //terminal balance
//            TerminalBalance terminalBalance = new TerminalBalance();
//            terminalBalance.setBalanceId(dbCache.getTerminalBalance(terminalSn, reserve.getBalanceCode()).getBalanceId());
//            tx.setTerminalBalance(terminalBalance);
//            //BillTransactionNumber
//            tx.setTransactionGUID(msg.getAttributeString(BillingMessage.BILL_TRANSACTION_NUM));
//            //CliTransactionNumber
//            tx.setCliTransactionNum(msg.getAttributeString(BillingMessage.CLI_TRANSACTION_NUM));
//            //transaction type
//            tx.setType(ttCredit);
//
//            transactions.add(tx);
//        }
//        return transactions;
//    }
//
//
//    private ClientTransaction prepareClientTransaction(BillingMessageWritable msg,
//                                                       DBCache dbCache,
//                                                       Date transRegTime,
//                                                       Date clientTime,
//                                                       TransactionType ttCredit) throws DBCacheException {
//
//
//        ClientTransaction clientTransaction = new ClientTransaction();
//        //time
//        clientTransaction.setTransactionTime(transRegTime);
//        clientTransaction.setClientTime(clientTime);
//        //amount
//        Double amount = Double.valueOf(msg.getAttributeString(BillingMessage.AMOUNT));
//        clientTransaction.setAmount(amount);
//        //terminal
//        Terminal terminal = new Terminal();
//        terminal.setTerminalId(dbCache.getTerminal(msg.getAttributeString(BillingMessage.TERMINAL_SN)).getTerminalId());
//        clientTransaction.setTerminal(terminal);
//        //seller
//        Seller seller = new Seller();
//        seller.setSellerId(dbCache.getSeller(msg.getAttributeString(BillingMessage.SELLER_CODE)).getSellerId());
//        clientTransaction.setSeller(seller);
//        //operator
//        Operator operator = new Operator();
//        operator.setOperatorId(dbCache.getOperator(msg.getAttributeString(BillingMessage.SERVICE_CODE)).getOperatorId());
//        clientTransaction.setOperator(operator);
//        //BillTransactionNumber
//        clientTransaction.setTransactionGUID(msg.getAttributeString(BillingMessage.BILL_TRANSACTION_NUM));
//        //CliTransactionNumber
//        clientTransaction.setCliTransactionNum(msg.getAttributeString(BillingMessage.CLI_TRANSACTION_NUM));
//        clientTransaction.setAccountNumber(msg.getAttributeString(BillingMessage.ACCOUNT_NUMBER));
//        if(clientTransaction.getAccountNumber() == null)
//            clientTransaction.setAccountNumber(msg.getAttributeString(BillingMessage.PAY_ACCOUNT_NUMBER));
//        //clientTransaction.setContractNumber(msg.getAttributeString(BillingMessage....));
//        clientTransaction.setVoucherNominal(msg.getAttributeString(BillingMessage.VOUCHER_NOMINAL));
//        clientTransaction.setOperationName(msg.getOperationName());
//        clientTransaction.setServiceCode(msg.getAttributeString(BillingMessage.SERVICE_CODE));
//        //transaction type
//        clientTransaction.setType(ttCredit);
//        return clientTransaction;
//    }
//
//
//    private ProcedureWrapper prepareChangeBalancesProcedure(DBCache dbCache,
//                                                            String operatorCode,
//                                                            String sellerCode,
//                                                            String terminalSn,
//                                                            ReservationSet reservationSet){
//        final String VARCHAR_ARRAY_TYPE = "VARCHAR_ARRAY_TYPE";
//        final String FLOAT_ARRAY_TYPE = "FLOAT_ARRAY_TYPE";
//
//        //prepare parameters to pass into SPL procedure
//        //operator
//        Reservation reservation = reservationSet.getReservation(Operator.class, operatorCode);
//        List balancesOwnersCodes = new ArrayList();//list of String
//        List balancesCodes = new ArrayList();//list of String
//        List balancesAmounts = new ArrayList();//list of Float
//        for(Iterator iter = reservation.reservesIterator(); iter.hasNext();){
//            Reservation.Reserve reserve = (Reservation.Reserve)iter.next();
//            balancesOwnersCodes.add(operatorCode);
//            balancesCodes.add(reserve.getBalanceCode());
//            balancesAmounts.add(new Float(reserve.getAmount()));//double -> float!
//        }
//        String[] operatorCodesArray = (String[])balancesOwnersCodes.toArray(new String[0]);
//        String[] operatorBalancesCodesArray = (String[])balancesCodes.toArray(new String[0]);
//        Float[] operatorBalancesAmountsArray = (Float[])balancesAmounts.toArray(new Float[0]);
//
//        //seller
//        List sellersParentsChainCodes = dbCache.getSellerWrapperParentsChainCodes(sellerCode);
//        balancesOwnersCodes = new ArrayList();//list of String
//        balancesCodes = new ArrayList();//list of String
//        balancesAmounts = new ArrayList();//list of Float
//        for(Iterator sellersIter = sellersParentsChainCodes.iterator(); sellersIter.hasNext();){
//            String nextSellerCode = (String)sellersIter.next();
//            reservation = reservationSet.getReservation(Seller.class, nextSellerCode);
//            for(Iterator iter = reservation.reservesIterator(); iter.hasNext();){
//                Reservation.Reserve reserve = (Reservation.Reserve)iter.next();
//                balancesOwnersCodes.add(nextSellerCode);
//                balancesCodes.add(reserve.getBalanceCode());
//                balancesAmounts.add(new Float(reserve.getAmount()));//double -> float!
//            }
//        }
//        String[] sellerCodesArray = (String[])balancesOwnersCodes.toArray(new String[0]);
//        String[] sellerBalancesCodesArray = (String[])balancesCodes.toArray(new String[0]);
//        Float[] sellerBalancesAmountsArray = (Float[])balancesAmounts.toArray(new Float[0]);
//
//        //seller
//        reservation = reservationSet.getReservation(Terminal.class, terminalSn);
//        balancesOwnersCodes = new ArrayList();//list of String
//        balancesCodes = new ArrayList();//list of String
//        balancesAmounts = new ArrayList();//list of Float
//        for(Iterator iter = reservation.reservesIterator(); iter.hasNext();){
//            Reservation.Reserve reserve = (Reservation.Reserve)iter.next();
//            balancesOwnersCodes.add(terminalSn);
//            balancesCodes.add(reserve.getBalanceCode());
//            balancesAmounts.add(new Float(reserve.getAmount()));//double -> float!
//        }
//        String[] termianlSnArray = (String[])balancesOwnersCodes.toArray(new String[0]);
//        String[] termianlBalancesIdsArray = (String[])balancesCodes.toArray(new String[0]);
//        Float[] termianlBalancesAmountsArray = (Float[])balancesAmounts.toArray(new Float[0]);
//
//
//        return new ProcedureWrapper(
//                new QueryParameterWrapper[]{
//                        QueryParameterWrapper.createOutParameter(1, Types.INTEGER),
//
//                        new QueryParameterWrapper(2, operatorCodesArray, VARCHAR_ARRAY_TYPE),
//                        new QueryParameterWrapper(3, operatorBalancesCodesArray, VARCHAR_ARRAY_TYPE),
//                        new QueryParameterWrapper(4, operatorBalancesAmountsArray, FLOAT_ARRAY_TYPE),
//
//                        new QueryParameterWrapper(5, sellerCodesArray, VARCHAR_ARRAY_TYPE),
//                        new QueryParameterWrapper(6, sellerBalancesCodesArray, VARCHAR_ARRAY_TYPE),
//                        new QueryParameterWrapper(7, sellerBalancesAmountsArray, FLOAT_ARRAY_TYPE),
//
//                        new QueryParameterWrapper(8, termianlSnArray, VARCHAR_ARRAY_TYPE),
//                        new QueryParameterWrapper(9, termianlBalancesIdsArray, VARCHAR_ARRAY_TYPE),
//                        new QueryParameterWrapper(10, termianlBalancesAmountsArray, FLOAT_ARRAY_TYPE)
//                },
//                Operator.class,
//                "change_balances");
//    }

}
