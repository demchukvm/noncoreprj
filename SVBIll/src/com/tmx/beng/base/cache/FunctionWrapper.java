package com.tmx.beng.base.cache;

import com.tmx.as.base.EntityManager;
import com.tmx.as.base.FilterWrapper;
import com.tmx.as.entities.bill.function.ActualFunctionParameter;
import com.tmx.as.entities.bill.function.FunctionInstance;
import com.tmx.as.entities.bill.function.FunctionType;
import com.tmx.as.exceptions.DatabaseException;

import com.tmx.beng.base.dbfunctions.BasicFunction;
import com.tmx.beng.base.dbfunctions.ValidationError;

import com.tmx.util.InitException;

import java.lang.reflect.Method;
import java.util.*;

/**
 */
public class FunctionWrapper {

    private Map functionParams = new HashMap();
    private Object functionObj;//BasicFunction by default
    private Method functionMethod;//invocation method(invoke - by default)
    private final static String CHECK_PARAMS_METHOD_NAME = "validate";
    private final static String RETRIEVE_PARAMS_METHOD_NAME = "getDefaultParams";


    public FunctionWrapper(FunctionInstance functionInstance)throws DatabaseException, InitException{
        //if(functionInstance == null) return;
        addFunction(functionInstance);

        // retreive params
        EntityManager entitityManager = new EntityManager();
        FilterWrapper by_function_id = new FilterWrapper("by_function_instance_id");
        by_function_id.setParameter("id", functionInstance.getFunctionInstanceId());
        List list = entitityManager.RETRIEVE_ALL(ActualFunctionParameter.class, new FilterWrapper[]{
                by_function_id
        });

        for(Iterator it = list.iterator(); it.hasNext();){
            ActualFunctionParameter param = (ActualFunctionParameter)it.next();
            addFunctionParam(param);
        }

    }
    
    public FunctionWrapper(FunctionType functionType) throws InitException{
        try {
            functionObj = Class.forName(functionType.getImplClass()).newInstance();
        } catch (Throwable e) {
            throw new InitException(e.getLocalizedMessage(), e);
        }
    }

    private void addFunction(FunctionInstance functionInstance) throws InitException {
        try{
            functionObj = Class.forName(functionInstance.getFunctionType().getImplClass()).newInstance();
            functionMethod = functionObj.getClass().getMethod(
                    functionInstance.getFunctionType().getImplMethod(),
                    new Class[]{ Map.class });
        }catch(Throwable e){
            throw new InitException(e.getLocalizedMessage(), e);
        }
    }

    private void addFunctionParam(ActualFunctionParameter param){
        functionParams.put(param.getName(), param.getValue());
    }

    public void invoke() throws InvokeFunctionException{
        try{
            functionMethod.invoke(functionObj, new Object[]{ functionParams });
        }catch(Throwable e){
            throw new InvokeFunctionException(e.getCause().getLocalizedMessage(), e.getCause());
        }
    }

    public void invoke(Map addFuncParameters, Object... params) throws InvokeFunctionException{
        try{
            addFuncParameters.putAll(functionParams);
            functionMethod.invoke(functionObj, new Object[]{addFuncParameters});
        }catch(Throwable e){
            throw new InvokeFunctionException(e.getCause().getLocalizedMessage(), e.getCause());
        }
    }

    public ValidationError[] validateFunctionParams(Map funcParams) throws InitException, InvokeFunctionException {
        try {
            Method testFunctionMethod = functionObj.getClass().getMethod(CHECK_PARAMS_METHOD_NAME, new Class[]{ Map.class });
            return (ValidationError[])testFunctionMethod.invoke(functionObj, new Object[] {funcParams});
        }catch (Throwable e){
            throw new InvokeFunctionException(e.getCause().getLocalizedMessage(), e.getCause());
        }
    }

    public ActualFunctionParameter[] getDefaultFunctionParam() throws InvokeFunctionException {
        try {
            Method testFunctionMethod = functionObj.getClass().getMethod(RETRIEVE_PARAMS_METHOD_NAME, new Class[]{});
            BasicFunction.TariffParameter[] params = (BasicFunction.TariffParameter[]) testFunctionMethod.invoke(functionObj);
            ActualFunctionParameter[] result = new ActualFunctionParameter[params.length];
            for (int i = 0; i < params.length; i++) {
				ActualFunctionParameter param = new ActualFunctionParameter();
				param.setName(params[i].getName());
				param.setValue(params[i].getValue());
                param.setOrderNum(params[i].getOrderNum());
                param.setMandatory(params[i].getMandatory());
                result[i] = param;
			}
            return result;
        }catch (Throwable e){
            throw new InvokeFunctionException(e.getCause().getLocalizedMessage(), e.getCause());
        }
    }


    /** Invoke enclosed function instance with defined in DB actual params and given runtime params.
     * Runtime params overrides actual params assigned in DB.
     * @param runtimeParams additional runtime params.
     * @throws InvokeFunctionException exception on function execution */
    public void invoke(Map runtimeParams) throws InvokeFunctionException{
        if(runtimeParams != null && runtimeParams.size() > 0){
            functionParams.putAll(runtimeParams);
            invoke();
        }
    }

    public Object getObjectInstance(){
        return this.functionObj;
    }

}
