package com.tmx.web.base;

import org.apache.struts.config.ModuleConfig;
import org.apache.struts.util.ModuleUtils;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;

import com.tmx.util.InitException;
import com.tmx.web.base.menuservice.MenuService;
import com.tmx.web.base.xslpublisher.XslPublishService;

/**
 * Holds objects in application scope context
 */
public class ApplicationEnvironment {
    private static ServletContext context = null;

    public static void init(ServletContext ctx) throws InitException {
        context = ctx;

        //Init and store services
        publishService(ResourceService.class);//initialize it first to load resource messages
        publishService(SecurityService.class);
        publishService(UserInterfaceService.class);
        //TODO RM: introduce common approach to initialize all services on the web.   
        MenuService.getInstance().reload();
        XslPublishService.getInstance().reload();
    }


    /**
     * Get servlet dispatcher for given applicatio root related URL.
     */
    public static RequestDispatcher getRequestDispatcher(String url) {
        if (context == null)
            return null;
        return context.getRequestDispatcher(url);
    }

    /**
     * Get init parameter from ServletContext
     */
    public static String getInitParameter(String varName, String defaultValue) {
        if (context == null)
            return defaultValue;
        String initParam = context.getInitParameter(varName);
        return (initParam != null) ? initParam : defaultValue;
    }

    /**
     * Create singleton instance, initialize it and publish inside application scope.
     */
    public static void publishService(Class serviceClass) throws InitException {
        if (serviceClass == null)
            throw new InitException("err.app_env.null_class_specified");
        try {
            Object serviceObj = serviceClass.newInstance();
            if (!(serviceObj instanceof Service))
                throw new InitException("err.app_env.not_service_class", new String[]{Service.class.getName()});
            Service service = (Service) serviceObj;
            service.init(context);
            setAttr(serviceClass.getName(), service);
        }
        catch (Exception e) {
            throw new InitException("err.app_env.service_instantiation_failed", e);
        }
    }

    public static String getWebPath() {
        return context.getRealPath("/");
    }

    /** Return the Application deployment name. It is defined
     * as "app_base" parameter in web.xml.
     * Default value is "/lms" */
    public static String getAppDeploymentName() {
        return getInitParameter("app_base", null);
    }

    /** Return the Application deployment name. It is defined
     * as "action_extenison" parameter in web.xml.
     * Default value is ".do" */
    public static String getActionExtension(){
        return getInitParameter("action_extenison", ".do");
    }

    public static String getActionUrl(String actionName){
        return getAppDeploymentName() + "/" + ((actionName == null || actionName.length() <= 0) ? "#" : (actionName + getActionExtension()));
    }



    public static ResourceService getResourceService() {
        return (ResourceService) getService(ResourceService.class);
    }

    public static Service getService(Class serviceClass) {
        if (serviceClass == null)
            return null;
        Object service = getAttr(serviceClass.getName(), null);
        if (!(service instanceof Service))
            return null;
        return (Service) service;
    }


    private static Object getAttr(String attrName, String defaultValue) {
        if (context == null || attrName == null)
            return defaultValue;
        Object value = context.getAttribute(attrName);
        return (value != null) ? value : defaultValue;
    }


    private static void setAttr(String attrName, Object attrValue) {
        if (context == null || attrName == null || attrValue == null)
            return;
        context.setAttribute(attrName, attrValue);
    }

    public static boolean removeAttr(String attrName) {
        if (context == null || attrName == null)
            return false;

        if (context.getAttribute(attrName) != null) {
            context.setAttribute(attrName, null);
            return true;
        }

        return false;
    }

    public static ModuleConfig getModuleConfig() {
        return ModuleUtils.getInstance().getModuleConfig("", context);
    }

}
