package com.tmx.beng.iso8583Gate.processors;

import com.tmx.as.entities.iso8583.collation.Collation;
import com.tmx.as.entities.iso8583.transaction.Iso8583Transaction;
import com.tmx.as.exceptions.DatabaseException;
import com.tmx.as.base.EntityManager;
import com.tmx.as.base.FilterWrapper;
import com.tmx.as.base.OrderWrapper;
import com.tmx.beng.iso8583Gate.pdu.RuntimeMessage;
import com.tmx.beng.iso8583Gate.pdu.substructure.RuntimeSubDataElement;
import com.tmx.beng.iso8583Gate.pdu.substructure.collation.BasicCollationSDEF;

import java.util.List;
import java.util.ArrayList;

/**
    Parent for batch processors
 */
public abstract class AbstractBatchProcessor extends BasicProcessor{

    protected List retrieveAllCollationAttempts(Iso8583Transaction batchTx) throws DatabaseException {
        EntityManager em = new EntityManager();
        FilterWrapper byTerminalIdFW = new FilterWrapper("by_isoterminalid");
        byTerminalIdFW.setParameter("terminalid", batchTx.getTerminalId());
        FilterWrapper byMerchantIdFW = new FilterWrapper("by_merchantid");
        byMerchantIdFW.setParameter("merchantid", batchTx.getCardAcqId());
        FilterWrapper byBatchNumFW = new FilterWrapper("by_batchnum");
        byBatchNumFW.setParameter("batchnum", batchTx.getBatchNumber());

        return em.RETRIEVE_ALL(
                Collation.class,
                new FilterWrapper[]{byTerminalIdFW, byMerchantIdFW, byBatchNumFW},
                new OrderWrapper[]{new OrderWrapper("startCollationTime", OrderWrapper.DESC)});
    }

    protected Collation retrieveLastCollationAttempt(Iso8583Transaction batchTx) throws DatabaseException{
        EntityManager em = new EntityManager();
        FilterWrapper byTerminalIdFW = new FilterWrapper("by_isoterminalid");
        byTerminalIdFW.setParameter("terminalid", batchTx.getTerminalId());
        FilterWrapper byMerchantIdFW = new FilterWrapper("by_merchantid");
        byMerchantIdFW.setParameter("merchantid", batchTx.getCardAcqId());
        FilterWrapper bySuccessfulFW = new FilterWrapper("by_successful");
        bySuccessfulFW.setParameter("successful", Boolean.FALSE);
        FilterWrapper byClosedFW = new FilterWrapper("by_closed");
        byClosedFW.setParameter("closed", Boolean.FALSE);
        FilterWrapper byBatchNumFW = new FilterWrapper("by_batchnum");
        byBatchNumFW.setParameter("batchnum", batchTx.getBatchNumber());

        List collationAttempts = em.RETRIEVE_ALL(
                Collation.class,
                new FilterWrapper[]{byTerminalIdFW, byMerchantIdFW, bySuccessfulFW, byClosedFW, byBatchNumFW},
                new OrderWrapper[]{new OrderWrapper("startCollationTime", OrderWrapper.DESC)});

        if(collationAttempts != null && collationAttempts.size() > 0)
            return (Collation)collationAttempts.get(0);
        else
            return null;
    }

    protected void closeCollationAttempts(Iso8583Transaction tx, Collation lastCollation) throws DatabaseException{
        List prevCollationAttempts = retrieveAllCollationAttempts(tx);
        if(prevCollationAttempts == null)
            prevCollationAttempts = new ArrayList();

        prevCollationAttempts.add(lastCollation);

        EntityManager em = new EntityManager();
        for(int i=0; i<prevCollationAttempts.size(); i++){
            Collation collation = (Collation)prevCollationAttempts.get(i);
            collation.setClosed(true);
            em.SAVE(collation);
        }
    }

    protected void assignBatchTransactionToCollation(Iso8583Transaction tx, Collation collation) throws DatabaseException{
        EntityManager em = new EntityManager();
        tx.setCollation(collation);
        em.SAVE(tx);
    }

}
