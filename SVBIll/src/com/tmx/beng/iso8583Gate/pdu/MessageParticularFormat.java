package com.tmx.beng.iso8583Gate.pdu;

import com.tmx.beng.iso8583Gate.pdu.particularmsgformat.beans.Iso8583ParticularMsgFormatDocument;
import com.tmx.beng.iso8583Gate.ISO8583Service;
import com.tmx.util.InitException;

import java.util.Map;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.HashMap;
import java.io.FileInputStream;
import java.io.File;
import java.io.IOException;

import org.apache.log4j.Logger;
import org.apache.xmlbeans.XmlOptions;

/**
*  Message Particular Format (message related).
*/
public class MessageParticularFormat {
    private Iso8583ParticularMsgFormatDocument particularMsgFormatDoc;
    private Map dataElementsFormatsByBitmapNum;
    private Map dataElementsFormatsByName;
    private Logger logger = ISO8583Service.getLogger(this.getClass());
    private static final String ID_DELIMITER = ".";


    public void load(String path, String msgFormatFile) throws InitException {
        Iso8583ParticularMsgFormatDocument localParticularMsgFormatDoc;
        FileInputStream fis = null;
        final String messagePath = path + File.separator + msgFormatFile;
        try {

            fis = new FileInputStream(messagePath);
            // Bind the instance to the generated XMLBeans formats.
            localParticularMsgFormatDoc = Iso8583ParticularMsgFormatDocument.Factory.parse(fis);
        }
        catch (Throwable e) {
            throw new InitException("err.iso8583particularmsgformat.failed_to_parse_template", new String[]{messagePath}, e);
        }
        finally {
            try {
                if (fis != null)
                    fis.close();
            }
            catch (IOException e) {/** ignore */}
        }

        //validate
        ArrayList errors = new ArrayList();
        XmlOptions opts = new XmlOptions();
        opts.setErrorListener(errors);
        if (!localParticularMsgFormatDoc.validate(opts)) {
            StringBuffer errorBuffer = new StringBuffer("Validation failed of XML " + messagePath + ":\n");
            Iterator iter = errors.iterator();
            while (iter.hasNext())
                errorBuffer.append("   >> " + iter.next() + "\n");
            logger.error(errorBuffer.toString());
            throw new InitException("err.iso8583particularmsgformat.template_validation_failed", new String[]{messagePath});
        }

        Map localDataElementsFormatsByBitmap = new HashMap();
        Map localDataElementsFormatsByName = new HashMap();
        com.tmx.beng.iso8583Gate.pdu.particularmsgformat.beans.DataElement[] dataElement = localParticularMsgFormatDoc.getIso8583ParticularMsgFormat().getApplicationData().getDataElementArray();
        for (int i = 0; i < dataElement.length; i++){
            ParticularDataElementFormat pdef = new ParticularDataElementFormat(
                    dataElement[i].getBitmapNumber(),
                    dataElement[i].getName(),
                    dataElement[i].getFixedValue(),
                    dataElement[i].getOccuranceType(),
                    dataElement[i].getSubStructureFormatImpl()
                );


            localDataElementsFormatsByBitmap.put(String.valueOf(pdef.getBitmapNumber()), pdef);
            localDataElementsFormatsByName.put(pdef.getName(), pdef);
        }


        //assign to properties
        particularMsgFormatDoc = localParticularMsgFormatDoc;
        dataElementsFormatsByBitmapNum = localDataElementsFormatsByBitmap;
        dataElementsFormatsByName = localDataElementsFormatsByName;
    }

    public String resolveFormatId() {
        return resolveFormatId(
                getDataElementFormat(DataElementFormat.DEF_MESSAGETYPEID).getFixedValue(),
                getDataElementFormat(DataElementFormat.DEF_PROCESSINGCODE).getFixedValue());
    }

    /** Particular Message Format Id this is the couple of messegeTypeId and processingCode.
     * If processingCode is null then only messageTypeId used. */
    public static String resolveFormatId(String messageTypeId, String processingCode){
        return (processingCode == null) ? messageTypeId : (messageTypeId + ID_DELIMITER + processingCode);
    }


    public ParticularDataElementFormat getDataElementFormat(int bitmapNumber){
        return (ParticularDataElementFormat) dataElementsFormatsByBitmapNum.get(String.valueOf(bitmapNumber));
    }

    public ParticularDataElementFormat getDataElementFormat(String name){
        return (ParticularDataElementFormat) dataElementsFormatsByName.get(name);
    }
}
