package com.tmx.beng.medaccess.beeline;

import com.tmx.beng.httpsgate.HttpsGateException;
import com.tmx.beng.httpsgate.beeline.BeelineGate;
import com.tmx.beng.base.BillingMessage;
import com.tmx.beng.medaccess.MediumConnection;

/**
  Medium connection to work with Beeline gate 
 */
public class BeelineGateMediumConnection implements MediumConnection {
    private BeelineGate beelineGate = null;

    BeelineGateMediumConnection() throws HttpsGateException {
        beelineGate = BeelineGate.getInstance();
    }

    public void send(BillingMessage msg) {
        beelineGate.enqueueBillMessage(msg);
    }
}
