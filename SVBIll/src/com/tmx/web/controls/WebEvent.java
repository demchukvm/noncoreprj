package com.tmx.web.controls;

import org.apache.struts.action.ActionMapping;

import javax.servlet.http.HttpServletRequest;
import java.util.EventObject;

/**
WebEvent to be generated and passed into handler methods
 */
public class WebEvent extends EventObject {
    private HttpServletRequest request = null;
    private ActionMapping mapping = null;

    /** Constructor */
    public WebEvent(Object source, ActionMapping mapping, HttpServletRequest request){
        super(source);
        this.mapping = mapping;
        this.request = request;
    }

    // ------------- Properties
    public HttpServletRequest getRequest() {
        return request;
    }

    public ActionMapping getMapping() {
        return mapping;
    }
}
