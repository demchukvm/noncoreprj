package com.tmx.web.actions.trans_mgmt;

import com.tmx.as.modules.SpringModule;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForm;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import com.tmx.web.forms.trans_mgmt.TransactionInfoMtsForm;
import com.tmx.web.base.BasicDispatchedAction;
import com.tmx.beng.httpsgate.umc.UmcGate;
import com.tmx.beng.base.BillingMessageImpl;
import com.tmx.beng.base.BillingMessage;
import com.tmx.beng.access.Access;
import com.tmx.beng.access.Connection;
import com.tmx.as.entities.bill.transaction.ClientTransaction;
import com.tmx.as.base.EntityManager;
import com.tmx.as.base.FilterWrapper;
import com.tmx.util.InitException;


import javax.swing.*;
import javax.swing.text.Element;
import javax.swing.text.html.parser.ParserDelegator;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.swing.filechooser.FileFilter;
import org.w3c.dom.Document;
import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.net.*;
import java.util.ArrayList;
import javax.swing.text.html.HTMLEditorKit.ParserCallback;
import javax.swing.text.MutableAttributeSet;
import javax.swing.text.html.HTML.Attribute;
import javax.swing.text.html.HTML.Tag;

import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Collections;
import java.util.StringTokenizer;

public class TransactionInfoMtsAction extends BasicDispatchedAction {

    private final String FORWARD = "transactionInfoMts";

    protected ActionForward defaultAction(ActionMapping mapping,
                                          ActionForm form,
                                          HttpServletRequest request,
                                          HttpServletResponse response,
                                          String methodName) throws Throwable {
        return refreshBalance(mapping, form, request, response);
    }

    public ActionForward refreshBalance(ActionMapping mapping,
                                        ActionForm form,
                                        HttpServletRequest request,
                                        HttpServletResponse response) throws Throwable {
        TransactionInfoMtsForm transactionInfoForm = (TransactionInfoMtsForm) form;

        try {
            transactionInfoForm.getEditBoxMtsBalance().setValue(UmcGate.getInstance().getBalance("UMCPoP").toString());
        } catch (Throwable e) {
            transactionInfoForm.getEditBoxMtsBalance().setValue(e.getLocalizedMessage());
        }
        return mapping.findForward(FORWARD);
    }

    public ActionForward info(ActionMapping mapping,
                              ActionForm form,
                              HttpServletRequest request,
                              HttpServletResponse response) throws Throwable {
        TransactionInfoMtsForm transactionInfoForm = (TransactionInfoMtsForm) form;
        transactionInfoForm.clear();
        ClientTransaction clientTransaction = null;
        EntityManager entityManager = new EntityManager();

        if (transactionInfoForm.getEditBoxBillTransaction() != null && transactionInfoForm.getEditBoxBillTransaction().getValue().length() > 0) {
            FilterWrapper byBillNumFW = new FilterWrapper("by_billnum");
            byBillNumFW.setParameter("billnum", transactionInfoForm.getEditBoxBillTransaction().getValue());
            clientTransaction = (ClientTransaction) entityManager.RETRIEVE(
                    ClientTransaction.class,
                    new FilterWrapper[]{byBillNumFW},
                    new String[]{"operator", "terminal", "seller"}
            );
        } else if (transactionInfoForm.getEditBoxCliTransaction() != null && transactionInfoForm.getEditBoxCliTransaction().getValue().length() > 0) {
            FilterWrapper byCliNumFW = new FilterWrapper("by_clinum");
            byCliNumFW.setParameter("clinum", transactionInfoForm.getEditBoxCliTransaction().getValue());
            clientTransaction = (ClientTransaction) entityManager.RETRIEVE(
                    ClientTransaction.class,
                    new FilterWrapper[]{byCliNumFW},
                    new String[]{"operator", "terminal", "seller"}
            );
        }
        if (clientTransaction != null) {
            transactionInfoForm.getEditBoxBillTransaction().setValue(clientTransaction.getTransactionGUID());
            transactionInfoForm.getEditBoxCliTransaction().setValue(clientTransaction.getCliTransactionNum());
            transactionInfoForm.getEditBoxAmount().setValue(clientTransaction.getAmount().toString());
            transactionInfoForm.getEditBoxStatusCode().setValue(clientTransaction.getStatus());
            if (clientTransaction.getTransactionTime() != null) {
                transactionInfoForm.getEditBoxTransactionTime().setValue(
                        new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(clientTransaction.getTransactionTime()));
            }
            if (clientTransaction.getClientTime() != null) {
                transactionInfoForm.getEditBoxCientTime().setValue(
                        new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(clientTransaction.getClientTime()));
            }
            if (clientTransaction.getAccountNumber() != null) {
                transactionInfoForm.getEditBoxAccountNumber().setValue(clientTransaction.getAccountNumber());
            }
            transactionInfoForm.getEditBoxSellerName().setValue(clientTransaction.getSeller().getName());
            transactionInfoForm.getEditBoxTerminalNumber().setValue(clientTransaction.getTerminal().getSerialNumber());
            transactionInfoForm.getEditBoxOperatorName().setValue(clientTransaction.getOperator().getName());
        } else {
            transactionInfoForm.getEditBoxStatusMessage().setValue("Transaction not found");
        }
        return mapping.findForward(FORWARD);
    }

    public ActionForward cancelTransaction(ActionMapping mapping,
                                           ActionForm form,
                                           HttpServletRequest request,
                                           HttpServletResponse response) throws Throwable {

        TransactionInfoMtsForm transactionInfoForm = (TransactionInfoMtsForm) form;
        BillingMessageImpl msg = new BillingMessageImpl();
        boolean skip = false;
        if (transactionInfoForm.getEditBoxBillTransaction() != null && transactionInfoForm.getEditBoxBillTransaction().getValue().length() > 0) {
            msg.setAttributeString(BillingMessage.BILL_TRANSACTION_NUM, transactionInfoForm.getEditBoxBillTransaction().getValue());
        } else if (transactionInfoForm.getEditBoxCliTransaction() != null && transactionInfoForm.getEditBoxCliTransaction().getValue().length() > 0) {
            msg.setAttributeString(BillingMessage.CLI_TRANSACTION_NUM, transactionInfoForm.getEditBoxCliTransaction().getValue());
        } else {
            skip = true;
        }

        if (!skip) {
            msg.setOperationName(BillingMessage.O_CANCEL_REFILL_PAYMENT);
            msg.setAttributeString(BillingMessage.SERVICE_CODE, transactionInfoForm.getEditBoxOperatorName().getValue());

//            Access access = obtainBillingEngineAccess(transactionInfoForm);
            Connection conn = (Connection) SpringModule.getApplicationContext().getBean("billingConnection");
            BillingMessage resp = conn.processSync(msg);

            transactionInfoForm.clear();
            transactionInfoForm.getEditBoxStatusCode().setValue(resp.getAttributeString(BillingMessage.STATUS_CODE));
            transactionInfoForm.getEditBoxStatusMessage().setValue(resp.getAttributeString(BillingMessage.STATUS_MESSAGE));
        } else {
            transactionInfoForm.getEditBoxStatusMessage().setValue("Transaction code expected");
        }
        return mapping.findForward("transactionInfo");
    }

    private Access obtainBillingEngineAccess(TransactionInfoMtsForm transactionInfoForm) throws InitException {
        if (transactionInfoForm.getBillingEngineAccess() == null) {
            InitialContext ctx = null;
            try {
                System.setProperty("java.naming.factory.url.pkgs", "");//=org.apache.naming
                ctx = new InitialContext();//System.getProperties()
                Access newAccess = (Access) ctx.lookup("java:comp/services/billingEngine");
                if (newAccess == null)
                    throw new InitException("err.csapi.null_basic_access_obtained");

                transactionInfoForm.setBillingEngineAccess(newAccess);
            }
            catch (NamingException e) {
                throw new InitException("err.csapi.failed_to_obtain_billing_engine_access", e);
            }
            finally {
                if (ctx != null) {
                    try {
                        ctx.close();
                    }
                    catch (NamingException e) {
                        //log.info( "Unable to release initial context", ne );
                    }
                }
            }
        }
        return transactionInfoForm.getBillingEngineAccess();
    }








    
    public ActionForward getResult(ActionMapping mapping,
                              ActionForm form,
                              HttpServletRequest request,
                              HttpServletResponse response) throws Throwable {
        TransactionInfoMtsForm f = (TransactionInfoMtsForm) form;

        f.setV1("#1 - 0");
        f.setV2("#2 - 0");
        f.setV3("#3 - 0");
        f.setV4("#4 - 0");
        f.setV5("#5 - 0");
        f.setV6("#6 - 0");


        String firstUrl = f.getEbFirstPageUrl().getValue().trim();
        int totalPages = Integer.parseInt(f.getEbTotalPages().getValue().trim());
        int firstValidMessage = Integer.parseInt(f.getEbFirstValidMessage().getValue().trim());

        String nodeElement = parsePage(firstUrl);

        ArrayList<String> nodeList = new ArrayList<String>();

        for(int i=1; i<totalPages; i++)
        {
            String Url = firstUrl+"&start="+i+"0";
            String node = parsePage(Url);
            if(node != null)
            {
                nodeList.add(node);
            }
        }

        ArrayList<String> values_1 = new ArrayList<String>();
        ArrayList<String> values_2 = new ArrayList<String>();
        ArrayList<String> values_3 = new ArrayList<String>();
        ArrayList<String> values_4 = new ArrayList<String>();
        ArrayList<String> values_5 = new ArrayList<String>();
        ArrayList<String> values_6 = new ArrayList<String>();

        String v = "";
        StringTokenizer st = new StringTokenizer(nodeElement, "~~~", false);
        int a = 0;
        while(st.hasMoreTokens())
        {
            String str = st.nextToken();
            if(a>(firstValidMessage-2)) v += str+",";

            a++;
        }

        String totalStr = v;
        for(String nE : nodeList)
        {
            totalStr += nE;
        }
        totalStr = totalStr.replace("~~~", "");

        st = new StringTokenizer(totalStr, ",", false);
        while(st.hasMoreTokens())
        {
            String s = st.nextToken();
            if(s.equals("1")) values_1.add(s);
            else if(s.equals("2")) values_2.add(s);
            else if(s.equals("3")) values_3.add(s);
            else if(s.equals("4")) values_4.add(s);
            else if(s.equals("5")) values_5.add(s);
            else if(s.equals("6")) values_6.add(s);

        }

        System.out.println("#1 - "+values_1.size());
        System.out.println("#2 - "+values_2.size());
        System.out.println("#3 - "+values_3.size());
        System.out.println("#4 - "+values_4.size());
        System.out.println("#5 - "+values_5.size());
        System.out.println("#6 - "+values_6.size());

        f.setV1("#1 - "+values_1.size());
        f.setV2("#2 - "+values_2.size());
        f.setV3("#3 - "+values_3.size());
        f.setV4("#4 - "+values_4.size());
        f.setV5("#5 - "+values_5.size());
        f.setV6("#6 - "+values_6.size());

                
        return mapping.findForward(FORWARD);
    }

    private String parsePage(String link) throws IOException
	{
		URL pageUrl = new URL(link);
		BufferedReader webPageStream = new BufferedReader(new InputStreamReader(pageUrl.openStream(), "utf-8"));

		ParserDelegator htmlParser = new ParserDelegator();
        MyParseCallback callback = new MyParseCallback(link);
		htmlParser.parse(webPageStream, callback, true);

		return callback.getElements();
	}
}


class NodeElement
{
    private String message;
	private String position;
	private String countVotes;

	public NodeElement(String message)
	{
		//this.position = position;
		//this.countVotes = countVotes;
        this.message = message;
	}

    public String getPosition()
    {
        return position;
    }

    public String getCountVotes()
    {
        return countVotes;
    }

    public String getMessage()
    {
        return message;
    }
}


class MyParseCallback extends ParserCallback
{
    private ArrayList<String> message = new ArrayList<String>();
	private ArrayList<String> position = new ArrayList<String>();			// номера участников
	private ArrayList<String> countVotes = new ArrayList<String>();			// количество голосов

	private boolean flag1 = false;
    private boolean flag2 = false;
    private boolean endflag = false;
    private boolean content = false;

//	private String link;
 //   private String startComment;

    public MyParseCallback(String link)
	{
//		this.link = link;
    }

	public void handleStartTag(Tag t, MutableAttributeSet a, int pos)
	{
		if(t==Tag.DIV && a.containsAttribute(Attribute.CLASS, "content"))
		{
			flag1 = true;
            endflag = false;
            content = true;
		}
	}

    public void handleSimpleTag(Tag t, MutableAttributeSet a, int pos)
    {
        if(t==Tag.BR && endflag!=true)
        {
            flag2 = true;
        }
    }

    public void handleEndTag(Tag t, int pos)
    {
        if(t == Tag.DIV && content==true)
        {
            endflag = true;
            message.add("~~~");
            content=false;
        }
    }

	public void handleText(char[] data, int pos)
	{
		if((flag1 || flag2) && content == true)
		{
			String htmlText = new String(data);
			message.add(htmlText);
			flag1=false;
            flag2=false;
		}
        
	}

    public String getElements()
	{
		purge();
        String nodeList = new String();
		for(String str : message)
		{
            nodeList += str;
		}

		return nodeList.trim();
	}


	private void purge()
	{
        for(int k=0; k<message.size(); k++)
        {
            char[] chrs = message.get(k).toCharArray();
            for(int i=0; i<chrs.length; i++)
            {
                if(!Character.isDigit(chrs[i]) && chrs[i]!='~')
                {
                    chrs[i] = ',';
                }
            }

            String str = new String(chrs);
		    message.set(k, str.trim()+",");
        }

	}


}
