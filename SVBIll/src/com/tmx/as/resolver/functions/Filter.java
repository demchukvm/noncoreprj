package com.tmx.as.resolver.functions;

import com.tmx.as.base.FilterWrapper;
import com.tmx.as.base.FilterParameterWrapper;
import com.tmx.as.resolver.FunctionSyntaxException;
import com.tmx.as.resolver.ResolverException;

public class Filter implements Function{
    FilterWrapper filterWrapper=new FilterWrapper();
    public void set(int paramIndex, String paramName, Object paramValue) throws FunctionSyntaxException {
        if(paramName.equalsIgnoreCase("parameter")){
            FilterParameterWrapper filterParameterWrapper=(FilterParameterWrapper)paramValue;
            filterWrapper.setParameter(filterParameterWrapper);
            return;
        }
        try {
            filterWrapper.set(paramName,(String)paramValue);
        } catch (ResolverException e) {
            throw new FunctionSyntaxException("projection error", e);
        }
    }

    public Object returnValue() throws FunctionSyntaxException {
        return filterWrapper;
    }
}
