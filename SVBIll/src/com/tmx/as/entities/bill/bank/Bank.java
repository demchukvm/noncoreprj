package com.tmx.as.entities.bill.bank;

import com.tmx.as.base.BasicEntity;

import java.io.Serializable;


public class Bank extends BasicEntity {
    private Long bankId;
    private String name;
    private String mfo;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMfo() {
        return mfo;
    }

    public void setMfo(String mfo) {
        this.mfo = mfo;
    }

    public Long getBankId() {
        return bankId;
    }

    public void setBankId(Long bankId) {
        this.bankId = bankId;
    }

    public Serializable getId() {
        return bankId;
    }

    public int getValid() {
        return valid;
    }

    public void setValid(int valid) {
        this.valid = valid;    
    }
}
