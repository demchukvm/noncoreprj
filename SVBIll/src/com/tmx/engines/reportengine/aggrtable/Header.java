package com.tmx.engines.reportengine.aggrtable;

/**
 */
public class Header {
    private String label = null;
    private int span = 1;
    /** Value to order this header among another siblings */
    private Object labelOrder = null;

    public String getLabel() {
        return label;
    }

    public int getSpan() {
        return span;
    }

    public Object getLabelOrder() {
        return labelOrder;
    }

    //----------package local methods

    void setLabel(String label) {
        this.label = label;
    }

    void setSpan(int span) {
        this.span = span;
    }

    void setLabelOrder(Object labelOrder) {
        this.labelOrder = labelOrder;
    }

    /** compare headers names only */
    public boolean equals(Header h){
        return this.getLabel().equals(h.getLabel());
    }
}
