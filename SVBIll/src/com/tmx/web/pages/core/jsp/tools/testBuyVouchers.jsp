<%@ page language="java" pageEncoding="UTF-8" contentType="text/html; charset=utf-8"
         errorPage="/core/jsp/message/jsperror.jsp" isELIgnored="false" %>
<%@ taglib uri="/WEB-INF/tld/struts-nested.tld" prefix="nested" %>
<%@ taglib uri="/WEB-INF/tld/tmx-controls.tld" prefix="tmx_ctrl" %>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>

<%--<html:form action="/TestBuyVouchersAction">--%>

<table border="0" width="100%">

    <tr>
        <td style="text-align: left">
            <tmx_ctrl:button property="bBuy"/>
            <tmx_ctrl:button property="bStatus"/>
            <tmx_ctrl:button property="bDownload"/>
            <tmx_ctrl:button property="bReset"/>
        </td>
    </tr>

    <tr>
        <td valign="center" align="center" class="tabForm">
            <table width="100%" border="0">
                <tr>
                    <td class="dataLabel">Номинал:</td>
                    <td class="dataField"><tmx_ctrl:selectBox property="sbVoucherNominal"/></td>
                    <td class="dataLabel">Количество:</td>
                    <td class="dataField"><tmx_ctrl:editBox property="ebVoucherCount"/></td>
                    <td class="dataLabel">Дилер:</td>
                    <td class="dataField"><tmx_ctrl:editBox property="ebSellerCode"/></td>
                </tr>
                <tr>
                    <td class="dataLabel">Поставщик:</td>
                    <td class="dataField"><tmx_ctrl:selectBox property="sbDistributor"/></td>
                    <td class="dataLabel">Сервис:</td>
                    <%--<td class="dataField"><tmx_ctrl:editBox property="ebServiceCode"/></td>--%>
                    <td class="dataField"><tmx_ctrl:selectBox property="sbServiceCode"/></td>
                    <td class="dataLabel">Серийный номер терминала:</td>
                    <td class="dataField"><tmx_ctrl:selectBox property="sbTerminalSn"/></td>
                </tr>
            </table>
        </td>
    </tr>

    <tr>
        <td valign="center" align="center" class="tabForm">
            <table width="100%" border="0">
                <tr>
                    <td class="dataLabel">Готово:</td>
                    <td class="dataField"><tmx_ctrl:editBox property="ebCompliteCount"/></td>
                    <td class="dataLabel">Статус:</td>
                    <td class="dataField"><tmx_ctrl:editBox property="ebStatus"/></td>
                    <%--<td class="dataLabel">Описание статусов:</td>--%>
                    <%--<td class="dataField"></td>--%>
                </tr>
            </table>
        </td>
    </tr>

    <%--<tr>--%>
        <%--<td style="text-align: left">--%>
            <%--<tmx_ctrl:button property="bBuy"/>--%>
            <%--<tmx_ctrl:button property="bStatus"/>--%>
            <%--<tmx_ctrl:button property="bDownload"/>--%>
            <%--<tmx_ctrl:button property="bReset"/>--%>
        <%--</td>--%>
    <%--</tr>--%>

    <tr>
        <td style="text-align: center;"><hr/></td>
    </tr>

    <tr>
        <td valign="center" align="center" class="tabForm">
            <table width="100%" border="0">
                <tr>
                    <td class="dataLabel" colspan="5">Купленные ваучеры</td>
                </tr>
                <tr>
                    <td class="dataLabel">
                        <bean:message bundle="core.labels" key="core.report.reportPayment.period"/>
                        &nbsp;
                        <bean:message bundle="core.labels" key="core.monitor.timeFrom"/>:
                    </td>
                    <td class="dataField">
                        <tmx_ctrl:timeSelector property="from"/>
                    </td>
                    <td class="dataLabel">
                        <bean:message bundle="core.labels" key="core.monitor.timeTo"/>:
                    </td>
                    <td class="dataField">
                        <tmx_ctrl:timeSelector property="to"/>
                    </td>
                    <td class="dataField"><tmx_ctrl:button property="bGetVouchers"/></td>
                </tr>
            </table>
        </td>
    </tr>

    <tr>
        <td>
            <tmx_ctrl:grid property="grid"/>
        </td>
    </tr>

</table>
