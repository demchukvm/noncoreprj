package com.tmx.beng.iso8583Gate.keygen;

import com.tmx.beng.iso8583Gate.pdu.util.HexConverter;

import javax.crypto.spec.DESedeKeySpec;
import javax.crypto.SecretKeyFactory;
import javax.crypto.SecretKey;
import javax.crypto.Cipher;
import java.util.Arrays;
import java.util.Random;
import java.security.spec.KeySpec;

/**
    Key used to generate XML-file with master keys.
    This implements 'tecb' algorithm. 

 Creation rules:

 compositekey (equal to 'keypart value' also)
 � checksum (equeal to 'keypart checksum' also)

 (X - key = 16, XL - left part = 8, XR - right part = 8 )

 res = XOR(A,B) (size = 8 byte)
 res = 3DES(source, key) (source size = res siz = 8 bytes, key size = 16 bytes)

 masterHKLMkey = M (16 bytes)
 masterTERMINALkey = T (16 bytes)

 nulldata = N (8 zero-bytes)

 checksum = S (8 bytes, only first 3 si used)
 compositekey = C (16 bytes)

 interkey = I (16 bytes)

 1. If encryptionalgorithm="cbc"

 CL = 3DES( TL , M )
 CR = 3DES( XOR( CL , TR ) , M )

 S = 3DES( N , T )


 2. If encryptionalgorithm="tecb"

 CL = 3DES( TL , M )
 CR = 3DES( TR , M )

 IL = TL
 IR =  XOR( CL , TR )
 S = 3DES( N , I )


 */
public class Key {
    private final String type = "2";
    private String name;
    private final String usage = "8";
    private byte[] rawkey;
    private byte[] encryptedkey;
    private String compositekey;
    private final String keylength = "32";
    private final String protocol = "cat33";
    private final String baudrate = "2400";
    private final String databits = "7";
    private final String stopbits = "1";
    private final String parity = "even";
    private final String verifychecksum = "yes";
    private final String variantindex = "";
    private String checksum;
    private final String status = "1";
    private String index;//HEX string
    private final String keyportparas = "2";
    private final String keyprotocol = "0";
    private final String keybaudrate = "0";
    private final String encryptionalgorithm = "tecb";

    private String terminalName;
    private final HexConverter hc = new HexConverter();
    private final String DES_EDE_ALG = "DESede";
    private final int KEY_BLOCK_SIZE = 8;//bytes
    private static final Random random = new Random(System.currentTimeMillis());
    private static final String CSV_DELIMITER = ";";

    public void generate(String terminalName, byte[] hklmKey, int idx) throws Exception{
        this.terminalName = terminalName;
        generateMasterKey(convertKey(hklmKey));
        evaluateCheckSum(rawkey);
        index = hc.getStringFromHex(new byte[]{(byte)idx});//encode to HEX
        index = index.substring(index.length()-1);//get last nibble
        name = evaluateKeyName();
    }

    public String toXmlString(){
        return new StringBuffer("<key").
                append(" type=\"").append(type).append("\"").
                append(" name=\"").append(name).append("\"").
                append(" usage=\"").append(usage).append("\"").
                append(" compositekey=\"").append(compositekey).append("\"").
                append(" keylength=\"").append(keylength).append("\"").
                append(" protocol=\"").append(protocol).append("\"").
                append(" baudrate=\"").append(baudrate).append("\"").
                append(" databits=\"").append(databits).append("\"").
                append(" stopbits=\"").append(stopbits).append("\"").
                append(" parity=\"").append(parity).append("\"").
                append(" verifychecksum=\"").append(verifychecksum).append("\"").
                append(" variantindex=\"").append(variantindex).append("\"").
                append(" checksum=\"").append(checksum).append("\"").
                append(" status=\"").append(status).append("\"").
                append(" index=\"").append(index).append("\"").
                append(" keyportparas=\"").append(keyportparas).append("\"").
                append(" keyprotocol=\"").append(keyprotocol).append("\"").
                append(" keybaudrate=\"").append(keybaudrate).append("\"").
                append(" encryptionalgorithm=\"").append(encryptionalgorithm).append("\"").
                append(">").
                append("<keyparts>").
                append("<keypart").
                append(" value=\"").append(compositekey).append("\"").
                append(" checksum=\"").append(checksum).append("\"").
                append("/>").
                append("</keyparts>").
                append("</key>").
                toString();
    }

    public String toCsvString(){
        return terminalName + CSV_DELIMITER +
              name + CSV_DELIMITER +
              hc.getStringFromHex(rawkey) + CSV_DELIMITER +
              compositekey + CSV_DELIMITER +
              checksum + CSV_DELIMITER;
    }

    public static String toCsvHeaderString(){
        return "TERMINAL_NAME" + CSV_DELIMITER +
              "KEY_NAME" + CSV_DELIMITER +
              "RAW_MASTER_KEY" + CSV_DELIMITER +
              "ENCRYPTED_MASTER_KEY" + CSV_DELIMITER +
              "CHECKSUM" + CSV_DELIMITER;
    }

    /** Genereate raw key and encode it's 16 bytes by ESP algorithm:
     *  8 bytes unsder desede + 8 bytes under desede. */
    private void generateMasterKey(byte[] hklmKey) throws Exception {
        rawkey = generateRawKey();
        Cipher cipher = initCipher(hklmKey);

        byte[] dataBlock = new byte[KEY_BLOCK_SIZE];
        encryptedkey = new byte[KEY_BLOCK_SIZE * 2];
        //encode first 8 bytes
        System.arraycopy(rawkey, 0, dataBlock, 0, KEY_BLOCK_SIZE);
        System.arraycopy(cipher.doFinal(dataBlock), 0, encryptedkey, 0, KEY_BLOCK_SIZE);
        //encode second 8 bytes
        System.arraycopy(rawkey, KEY_BLOCK_SIZE, dataBlock, 0, KEY_BLOCK_SIZE);
        System.arraycopy(cipher.doFinal(dataBlock), 0, encryptedkey, KEY_BLOCK_SIZE, KEY_BLOCK_SIZE);

        compositekey = hc.getStringFromHex(encryptedkey);
    }

    /** Encrypt 8 zero bytes by raw key and return first 3 bytes */
    private void evaluateCheckSum(byte[] rawKey) throws Exception{
        byte[] rawInterKey = new byte[KEY_BLOCK_SIZE*2];
        System.arraycopy(rawKey, 0, rawInterKey, 0, KEY_BLOCK_SIZE); //IL = TL
        for(int i=0; i<KEY_BLOCK_SIZE; i++) //IR = XOR(CL, TR)
            rawInterKey[KEY_BLOCK_SIZE+i] = (byte)(encryptedkey[i] ^ rawKey[KEY_BLOCK_SIZE+i]);

        byte[] etalonDump = new byte[8];
        Arrays.fill(etalonDump, (byte)0);
        Cipher cipher = initCipher(convertKey(rawInterKey));
        checksum = hc.getStringFromHex(cipher.doFinal(etalonDump)).substring(0, 3 * 2);//2 symbols per byte
    }

    private Cipher initCipher(byte[] rawDesEdeKey) throws Exception{
        KeySpec keyspec = new DESedeKeySpec(rawDesEdeKey);
        SecretKeyFactory keyfactory = SecretKeyFactory.getInstance(DES_EDE_ALG);
        SecretKey secretKey = keyfactory.generateSecret(keyspec);
        Cipher cipher = Cipher.getInstance(DES_EDE_ALG);
        cipher.init(Cipher.ENCRYPT_MODE, secretKey);
        return cipher;
    }

    /** Convert 16 byte key into 24 byte key by adding first 8 bytes to key's tail. */
    private byte[] convertKey(byte[] raw16BytesKey) throws Exception{
        if(raw16BytesKey.length != KEY_BLOCK_SIZE*2)
            throw new Exception("Incorrect key length. Use 16-byte length key");

        //convert 16 byte key to 24 byte key
        byte[] raw24BytesKey = new byte[KEY_BLOCK_SIZE*3];
        System.arraycopy(raw16BytesKey, 0, raw24BytesKey, 0, KEY_BLOCK_SIZE);
        System.arraycopy(raw16BytesKey, KEY_BLOCK_SIZE, raw24BytesKey, KEY_BLOCK_SIZE, KEY_BLOCK_SIZE);
        System.arraycopy(raw16BytesKey, 0, raw24BytesKey, KEY_BLOCK_SIZE*2, KEY_BLOCK_SIZE);
        return raw24BytesKey;
    }

    private String evaluateKeyName(){
        return terminalName+"_"+index;
    }

    private byte[] generateRawKey(){
        byte[] rawKey = new byte[24];
        byte[] keyPart = new byte[8];
        random.nextBytes(keyPart);
        System.arraycopy(keyPart, 0, rawKey, 0, keyPart.length);
        random.nextBytes(keyPart);
        System.arraycopy(keyPart, 0, rawKey, keyPart.length, keyPart.length);
        System.arraycopy(rawKey, 0, rawKey, keyPart.length*2, keyPart.length);
        return rawKey;
    }

}