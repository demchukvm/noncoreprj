package com.tmx.web.forms.core.reports;

import com.tmx.as.entities.bill.terminal.EquipmentType;
import com.tmx.as.entities.general.user.User;
import com.tmx.web.controls.*;
import com.tmx.web.controls.grid.Grid;
import com.tmx.web.controls.grid.GridException;
import com.tmx.web.base.BasicActionForm;
import com.tmx.as.entities.bill.seller.Seller;
import com.tmx.as.entities.bill.transaction.SellerBalanceTransaction;
import com.tmx.as.entities.bill.terminal.Terminal;
import com.tmx.as.exceptions.DatabaseException;
import com.tmx.as.base.QueryParameterWrapper;
import com.tmx.as.base.EntityManager;
import com.tmx.as.blogic.TerminalManager;
import com.tmx.util.i18n.MessageResources;

import java.util.*;

/**
 * Created by Proximan
  * Date: May 2010
  */
public class ReportNetworksForm extends BasicActionForm {

    private boolean roleFlag;

    private TimeSelector from;
    private TimeSelector to;
    private Grid grid;
    private Button buttonSubmit;
    private Button buttonUploadReport;
    private MultipleSelectBox multipleSelectBoxSellerBalances;
    private MultipleSelectBox multipleSelectBoxSubSellerBalances;
    //private CheckBox checkBoxDescription;
    //private CheckBox checkBoxGroupByTerminals;
    private boolean checkBoxDescription;
    private boolean checkBoxGroupByTerminals;
    private boolean _checkBoxDescription;
    private boolean _checkBoxGroupByTerminals;

    private ScrollBox scrollBoxEquipmentType;
    private EditBox editBoxTerminals;

    private String descr;


    protected void init() {
        initButtons();
        initBoxes();
        initTimeSelectors();

        



        grid = new Grid() {

            public void bind(List data) throws GridException
            {
                clear();

                _checkBoxDescription = checkBoxDescription;
                _checkBoxGroupByTerminals = checkBoxGroupByTerminals;

                //if(checkBoxGroupByTerminals.isSelected())  // отчет с терминалами
                if(checkBoxGroupByTerminals == true)  // отчет с терминалами
                {
              //      checkBoxGroupByTerminals.setSelected(true);

                    List<String> operatorsList = getOperatorsListAsc(data);
                    List<String> sellersList = getSellersListAsc(data);
                    List<SellerData> sellersDataList = getSellersDataListObjects(data);
                    HashMap <String, TreeSet<String>> sellers_terminal = getTerminalListOnSellerAsc(sellersDataList, sellersList);
                    
                    int totalColumnCount = operatorsList.size() * 2 + 2;
                    int totalRowCount = 2;
                    for(String seller : sellersList)
                    {
                       totalRowCount += sellers_terminal.get(seller).size();
                    }

                    int operColumnCount = 2;
                    int dataRowNumber = 2;
                    int dataRowNumberTerm = 2;

                    if(ifDataHas(sellers_terminal, sellersList))
                    {
                        setCell(0, 0, getLable("sellers"), "column_header_cell", "font-weight: bold", 1, 2);
                        setCell(0, 1, getLable("terminals"), "column_header_cell", "font-weight: bold", 1, 2);

                        for(int i = 0; i < operatorsList.size(); i++)
                        {
                            String operatorName = operatorsList.get(i).toString();
                            int operatorColumnIndex = 2 + i * operColumnCount;

                            setCell(0, operatorColumnIndex, operatorName, "column_header_cell", "font-weight: bold", operColumnCount, 1);

                            String sellerSumHeader = new StringBuilder().append("<div title=\"").append(getLable("sellerSumHelp")).append("\" >").append(getLable("sum")).toString();

                            setCell(1, operatorColumnIndex, sellerSumHeader, "column_header_cell", "font-weight: bold");
                            setCell(1, operatorColumnIndex + 1, getLable("countSum"), "column_header_cell", "font-weight: bold");
                        }
                        setCell(0, totalColumnCount, getLable("sum"), "column_header_cell", "font-weight: bold", 1, 2);
                        setCell(0, totalColumnCount + 1, getLable("sumCount"), "column_header_cell", "font-weight: bold", 1, 2);

                        Object[] terminals = null;
                        int selTermRowCount = 0;
                        for (int i = 0; i < sellersList.size(); i++)
                        {
                            String sellerName = sellersList.get(i).toString();
                            int rowspan = sellers_terminal.get(sellerName).size();

                            setCell(dataRowNumber + selTermRowCount, 0, sellerName, "column_data_cell", 1, rowspan);

                            terminals = sellers_terminal.get(sellerName).toArray();

                            for (int j = 2; j < terminals.length+2; j++)
                            {
                                String terminalName = terminals[j-2].toString();
                                setCell(j+selTermRowCount, 1, terminalName, "column_data_cell");
                                int colCount = 0;
                                for(Object operator : operatorsList)
                                {
                                    int operatorColumnIndex = 2 + colCount * operColumnCount;
                                    if(sellerHasTheOperator(sellerName, operator.toString(), terminalName, data))
                                    {
                                        setCell(dataRowNumberTerm, operatorColumnIndex, totalTermCellAmount(sellerName, operator.toString(), terminalName, data)+"", "column_data_cell");
                                        setCell(dataRowNumberTerm, operatorColumnIndex + 1, totalTermCellCount(sellerName, operator.toString(), terminalName, data)+"", "column_data_cell");
                                    }
                                    else
                                    {
                                        setCell(dataRowNumberTerm, operatorColumnIndex, "0.0", "column_data_cell");
                                        setCell(dataRowNumberTerm, operatorColumnIndex + 1, "0", "column_data_cell");
                                    }
                                    colCount++;
                                }

                                setCell(dataRowNumberTerm, totalColumnCount, calcGeneralSumForSellerOnTerminal(sellerName, terminalName, data)+"", "column_data_cell", "font-weight: bold");
                                setCell(dataRowNumberTerm, totalColumnCount + 1, calcGeneralCountForSellerOnTerminal(sellerName, terminalName, data)+"", "column_data_cell", "font-weight: bold");

                                dataRowNumberTerm++;
                            }
                            selTermRowCount += rowspan;
                        }

                        setCell(totalRowCount, 0, getLable("amount"), "column_data_cell", "font-weight: bold", 2, 1);      // Итого:

                        for (int j = 0; j < operatorsList.size(); j++)
                        {
                            String operatorName = operatorsList.get(j);
                            int operatorColumnIndex = 2 + j * operColumnCount;

                            setCell(totalRowCount, operatorColumnIndex, calculateGeneralSumForOperator(sellersDataList, operatorName)+"", "column_data_cell", "font-weight: bold");
                            setCell(totalRowCount, operatorColumnIndex + 1, calculateGeneralOperationCountForOperator(data, operatorName)+"", "column_data_cell", "font-weight: bold");
                        }
               //         System.out.println(terminals);
               //         setCell(totalRowCount, totalColumnCount, calculateNetworkSum(data)+"", "column_data_cell", "font-weight: bold"); // !!!!!
                //        setCell(totalRowCount, totalColumnCount + 1, calculateNetworkOperationCount(data)+"", "column_data_cell", "font-weight: bold"); //!!!!!
                        setCell(totalRowCount, totalColumnCount, calculateNetworkSum(data, terminals)+"", "column_data_cell", "font-weight: bold"); // !!!!!
                        setCell(totalRowCount, totalColumnCount + 1, calculateNetworkOperationCount(data, terminals)+"", "column_data_cell", "font-weight: bold"); //!!!!!


                    }
                    else
                    {
                        setCell(0, 0, getLable("noData"), "column_header_cell", "font-weight: bold");
                    }
                }

                else // отчет без терминалов
                {
               //     checkBoxGroupByTerminals.setSelected(false);

                    int generalColCount = 0;
                    int generalRowCount = 0;
                    int operColumnCount = 2;
                    int dataRowNumber = 1;

                    List operatorsList = getOperatorsListAsc(data);
                    List sellerList = getSellersListAsc(data);
                    List<SellerData> sellersDataList = getSellersDataListObjects(data);

                    HashMap <String, TreeSet<String>> sellers_terminal = getTerminalListOnSellerAsc(sellersDataList, sellerList);

                    if(ifDataHas(sellers_terminal, sellerList))
                    {
                        setCell(0, 0, getLable("sellers"), "column_header_cell", "font-weight: bold", 1, 2);
                        generalColCount = operatorsList.size() * 2 + 1;
                        generalRowCount = sellerList.size() + 2;

                        for (int i = 0; i < operatorsList.size(); i++)
                        {
                            String operatorName = operatorsList.get(i).toString();

                            int operatorColumnIndex = 1 + i * operColumnCount;

                            setCell(0, operatorColumnIndex, operatorName, "column_header_cell", "font-weight: bold", operColumnCount, 1);

                            String sellerSumHeader = new StringBuilder().append("<div title=\"").append(getLable("sellerSumHelp")).append("\" >").append(getLable("sum")).toString();
                            setCell(1, operatorColumnIndex, sellerSumHeader, "column_header_cell", "font-weight: bold");

                            setCell(1, operatorColumnIndex + 1, getLable("countSum"), "column_header_cell", "font-weight: bold");
                        }

                        setCell(0, generalColCount, getLable("sum"), "column_header_cell", "font-weight: bold", 1, 2);
                        setCell(0, generalColCount + 1, getLable("sumCount"), "column_header_cell", "font-weight: bold", 1, 2);

                        for (int i = 0; i < sellerList.size(); i++)
                        {
                            String sellerName = sellerList.get(i).toString();
                            dataRowNumber++;
                            setCell(dataRowNumber, 0, sellerName, "column_data_cell");

                            for (int j = 0; j < operatorsList.size(); j++)
                            {
                                String operatorName = operatorsList.get(j).toString();
                                int operatorColumnIndex = 1 + j * operColumnCount;

                                if (getSellerDataForOperator(sellersDataList, operatorName, sellerName) != null)
                                {
                              //      SellerData sellerData = getSellerDataForOperator(sellersDataList, operatorName, sellerName);

                                    setCell(dataRowNumber, operatorColumnIndex, totalSellerAmount(sellersDataList, sellers_terminal, sellerName, operatorName)+"", "column_data_cell");
                                    setCell(dataRowNumber, operatorColumnIndex + 1, totalSellerCount(sellersDataList, sellers_terminal, sellerName, operatorName)+"", "column_data_cell");
                                }
                                else
                                {
                                    setCell(dataRowNumber, operatorColumnIndex, "0.0", "column_data_cell");
                                    setCell(dataRowNumber, operatorColumnIndex + 1, "0", "column_data_cell");
                                }
                            }

                            setCell(dataRowNumber, generalColCount, calculateGeneralSumForSeller(data, sellerName)+"", "column_data_cell", "font-weight: bold");
                            setCell(dataRowNumber, generalColCount + 1, calculateGeneralOperationCountForSeller(data, sellerName)+"", "column_data_cell", "font-weight: bold");
                        }

                        setCell(generalRowCount, 0, getLable("amount"), "column_data_cell", "font-weight: bold", 1, 1);

                        for (int j = 0; j < operatorsList.size(); j++)
                        {
                            String operatorName = operatorsList.get(j).toString();
                            int operatorColumnIndex = 1 + j * operColumnCount;

                            setCell(generalRowCount, operatorColumnIndex, calculateGeneralSumForOperator(sellersDataList, operatorName)+"", "column_data_cell", "font-weight: bold");
                            setCell(generalRowCount, operatorColumnIndex + 1, calculateGeneralOperationCountForOperator(data, operatorName)+"", "column_data_cell", "font-weight: bold");
                        }

                        setCell(generalRowCount, generalColCount, calculateNetworkSum(data)+"", "column_data_cell", "font-weight: bold");
                        setCell(generalRowCount, generalColCount + 1, calculateNetworkOperationCount(data)+"", "column_data_cell", "font-weight: bold");
                    }
                    else
                    {
                        setCell(0, 0, getLable("noData"), "column_header_cell", "font-weight: bold");
                    }
                }

                checkBoxDescription = false;
                checkBoxGroupByTerminals = false;
            }

            boolean ifDataHas(HashMap <String, TreeSet<String>> sellers_terminal, List<String> sellersList)
            {
                boolean flag = false;
                for(String name : sellersList)
                {
                    TreeSet<String> ts = sellers_terminal.get(name);
                    if(!ts.isEmpty())
                    {
                        flag = true;
                        break;
                    }
                }
                return flag;

            }

            boolean sellerHasTheOperator(String sellerName, String operatorName, String terminalName, List data)
            {
                boolean flag = false;
                for(Object dt : data)
                {
                    Object[] ret = (Object[]) dt;
                    if(ret[1].equals(sellerName) && ret[3].equals(operatorName) && ret[6].equals(terminalName))
                    {
                        flag = true;
                        break;
                    }
                }
                return flag;
            }

            private double totalTermCellAmount(String sellerName, String operatorName, String terminalName, List data)
            {
                double totalTAmount = 0;
                for(Object dt : data)
                {
                    Object[] ret = (Object[]) dt;
                    if(ret[1].equals(sellerName) && ret[3].equals(operatorName) && ret[6].equals(terminalName))
                    {
                        totalTAmount += Double.parseDouble(ret[4].toString());
                    }
                }
                return totalTAmount;
            }

            private double totalSellerAmount(List<SellerData> sellersDataList, HashMap <String, TreeSet<String>> sellers_terminal, String sellerName, String operatorName)
            {
                double totalSAmount = 0;

                TreeSet<String> ts = sellers_terminal.get(sellerName);
                for(String term : ts)
                {
                    for(SellerData sellerData : sellersDataList)
                    {
                        if(term.equals(sellerData.getTerminalNum()) && sellerName.equals(sellerData.getSellerName()) && operatorName.equals(sellerData.getOperatorName()))
                        {
                            totalSAmount += sellerData.getSt_amount();
                        }
                    }
                }

                return totalSAmount;
            }

            private int totalTermCellCount(String sellerName, String operatorName, String terminalName, List data)
            {
                int totalTCount = 0;
                for(Object dt : data)
                {
                    Object[] ret = (Object[]) dt;
                    if(ret[1].equals(sellerName) && ret[3].equals(operatorName) && ret[6].equals(terminalName))
                    {
                        totalTCount += (int) Double.parseDouble(ret[5].toString());
                    }
                }
                return totalTCount;
            }

            private double totalSellerCount(List<SellerData> sellersDataList, HashMap <String, TreeSet<String>> sellers_terminal, String sellerName, String operatorName)
            {
                double totalSCount = 0;

                TreeSet<String> ts = sellers_terminal.get(sellerName);
                for(String term : ts)
                {
                    for(SellerData sellerData : sellersDataList)
                    {
                        if(term.equals(sellerData.getTerminalNum()) && sellerName.equals(sellerData.getSellerName()) && operatorName.equals(sellerData.getOperatorName()))
                        {
                            totalSCount += sellerData.getSt_count();
                        }
                    }
                }
                return totalSCount;
            }


            private String getLable(String key) {
                return MessageResources.getInstance().getOwnedLocalizedMessage(this.getClass(), "labels", getLocale(), key, null);
            }
        };
    }


    private List getTerminalListAsc(List data) {
        List list = new ArrayList();
        for (int i = 0; i < data.size(); i++) {
            Object[] ret = (Object[]) data.get(i);
            if (isInList(list, ret[6].toString()))
                list.add(ret[6].toString());
        }
        return list;
    }


    private HashMap <String, TreeSet<String>> getTerminalListOnSellerAsc(List<SellerData> sellersDataList, List sellerList)
    {
        HashMap <String, TreeSet<String>> hs = new HashMap <String, TreeSet<String>> ();
        for(Object sellerName : sellerList)
        {
            TreeSet<String> selList = new TreeSet<String>();
            for(SellerData sellerData : sellersDataList)
            {
                if(sellerName.equals(sellerData.getSellerName()))
                {
                    selList.add(sellerData.getTerminalNum());
                }
            }
            hs.put(sellerName.toString(), selList);
        }

        return hs;
    }

    private String getTerminals(HashMap <String, ArrayList<String>> hm, String seller)
    {
        String term = "";
        ArrayList<String> al = hm.get(seller);
        for(String sel : al)
        {
            term += sel + "<br>";
        }
        return term;
    }

    private void initButtons() {
        String action = "/core/report/reportNetworks";

        buttonSubmit = new Button();
        buttonSubmit.setLabelKey("ctrl.button.label.apply");

        buttonUploadReport = new Button();
        buttonUploadReport.setLabelKey("ctrl.button.label.download");

        buttonSubmit.setAction(action);
        buttonSubmit.setCommand("refreshGrid");

        buttonUploadReport.setAction(action);
        buttonUploadReport.setCommand("uploadReport");
    }

    private void initTimeSelectors() {
        GregorianCalendar calendar = new GregorianCalendar();
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.HOUR, 0);
        calendar.set(Calendar.SECOND, 0);

        from = new TimeSelector();
        from.setTime(calendar.getTime());
        to = new TimeSelector();
        to = new TimeSelector();
    }

    private void initBoxes() {

        editBoxTerminals = new EditBox();

        /*checkBoxDescription = new CheckBox();
        checkBoxDescription.setSelected(true);*/

        /*checkBoxGroupByTerminals = new CheckBox();
        checkBoxGroupByTerminals.setSelected(true);*/


        multipleSelectBoxSellerBalances = new MultipleSelectBox() {

            protected Object getKey(Object dataListElement) {
                return ((Seller) dataListElement).getPrimaryBalance().getBalanceId();
            }

            protected String getValue(Object dataListElement) {
                return ((Seller) dataListElement).getCode();
            }
        };

        multipleSelectBoxSubSellerBalances = new MultipleSelectBox() {

            protected Object getKey(Object dataListElement) {
                return ((Seller) dataListElement).getPrimaryBalance().getBalanceId();
            }

            protected String getValue(Object dataListElement) {
                return ((Seller) dataListElement).getCode();
            }
        };

        //
        scrollBoxEquipmentType = new ScrollBox<EquipmentType>() {

            protected String getKey(EquipmentType dataListElement) {
                return dataListElement.getEquipmentTypeId().toString();
            }

            protected String getValue(EquipmentType dataListElement) {
                return dataListElement.getName();
            }
        };
        scrollBoxEquipmentType.setNullable(true);
        //
    }


    private double calculateNetworkSum(List data) {
        double counter = 0;
        for (int i = 0; i < data.size(); i++) {
            Object[] ret = (Object[]) data.get(i);
            counter += Double.parseDouble(ret[4].toString());
        }
        return counter;
    }

    private double calculateNetworkSum(List data, Object[] terminal) {
        double counter = 0;
        for (Object term : terminal)
        {
            for (int i = 0; i < data.size(); i++)
            {

                Object[] ret = (Object[]) data.get(i);
                if(ret[6].equals(term))
                    counter += Double.parseDouble(ret[4].toString());
            }
        }
        return counter;
    }

    private int calculateNetworkOperationCount(List data) {
        int counter = 0;
        for (int i = 0; i < data.size(); i++) {
            Object[] ret = (Object[]) data.get(i);
            counter += (int) Double.parseDouble(ret[5].toString());
        }
        return counter;
    }

    private int calculateNetworkOperationCount(List data, Object[] terminal) {
        int counter = 0;
        for (Object term : terminal)
        {
            for (int i = 0; i < data.size(); i++) {
                Object[] ret = (Object[]) data.get(i);
                if(ret[6].equals(term))
                    counter += Double.parseDouble(ret[5].toString());
            }
        }
        return counter;
    }

    private double calculateGeneralSumForOperator(List<SellerData> sellerDataList, String operatorName) {
        double counter = 0;

        for (SellerData sellerData : sellerDataList)
        {
            if(sellerData.getOperatorName().equals(operatorName))
            {
                counter += sellerData.getSt_amount();
            }
        }
        return counter;
    }

    private int calculateGeneralOperationCountForOperator(List data, String operatorName) {
        int counter = 0;
        for (int i = 0; i < data.size(); i++) {
            Object[] ret = (Object[]) data.get(i);
            if (ret[3].equals(operatorName))
                counter += (int) Double.parseDouble(ret[5].toString());
        }
        return counter;
    }

    private double calculateGeneralSumForSeller(List data, String sellerName) {
        double counter = 0;
        for (int i = 0; i < data.size(); i++) {
            Object[] ret = (Object[]) data.get(i);
            if (ret[1].equals(sellerName))
                counter += Double.parseDouble(ret[4].toString());
        }
        return counter;
    }

    private double calcGeneralSumForSellerOnTerminal(String sellerName, String terminalName, List data)
    {
        double counter = 0;
        for (int i = 0; i < data.size(); i++) {
            Object[] ret = (Object[]) data.get(i);
            if (ret[1].equals(sellerName) && ret[6].equals(terminalName))
                counter += Double.parseDouble(ret[4].toString());
        }
        return counter;
    }

    private int calculateGeneralOperationCountForSeller(List data, String sellerName) {
        int counter = 0;
        for (int i = 0; i < data.size(); i++) {
            Object[] ret = (Object[]) data.get(i);
            if (ret[1].equals(sellerName))
                counter += Double.parseDouble(ret[5].toString());
        }
        return counter;
    }

    private int calcGeneralCountForSellerOnTerminal(String sellerName, String terminalName, List data)
    {
        int counter = 0;
        for (int i = 0; i < data.size(); i++) {
            Object[] ret = (Object[]) data.get(i);
            if (ret[1].equals(sellerName) && ret[6].equals(terminalName))
                counter += (int) Double.parseDouble(ret[5].toString());
        }
        return counter;
    }

    private SellerData getSellerDataForOperator(List<SellerData> sellerDataList, String operatorName, String sellerName) {
        for (SellerData sellerData : sellerDataList) {
            if (sellerData.getOperatorName().equals(operatorName) && sellerData.getSellerName().equals(sellerName))
                return sellerData;
        }
        return null;
    }

    private List<SellerData> getSellersDataListObjects(List data) {
        List<SellerData> sellerDataList = new ArrayList<SellerData>();
        if(scrollBoxEquipmentType.getSelectedKey().equals("________NULL_KEY_________"))
        {
            for (int i = 0; i < data.size(); i++) {
                SellerData sellerData = new SellerData();
                Object[] ret = (Object[]) data.get(i);
                sellerData.setSellerName(ret[1].toString());
                sellerData.setOperatorId(Integer.parseInt(ret[2].toString()));
                sellerData.setOperatorName(ret[3].toString());
                sellerData.setSt_amount(Double.parseDouble(ret[4].toString()));
                sellerData.setSt_count(Double.parseDouble(ret[5].toString()));
                sellerData.setTerminalNum(ret[6].toString());
                sellerData.setEquipType(ret[7].toString());
                sellerDataList.add(sellerData);
            }
        }
        else
        {
            String eq_type = scrollBoxEquipmentType.getSelectedKey();
            for (int i = 0; i < data.size(); i++) {
                Object[] ret = (Object[]) data.get(i);
                if(ret[7].equals(eq_type))
                {
                    SellerData sellerData = new SellerData();

                    sellerData.setSellerName(ret[1].toString());
                    sellerData.setOperatorId(Integer.parseInt(ret[2].toString()));
                    sellerData.setOperatorName(ret[3].toString());
                    sellerData.setSt_amount(Double.parseDouble(ret[4].toString()));
                    sellerData.setSt_count(Double.parseDouble(ret[5].toString()));
                    sellerData.setTerminalNum(ret[6].toString());
                    sellerData.setEquipType(ret[7].toString());
                    sellerDataList.add(sellerData);
                }
            }
        }
        return sellerDataList;
    }

    private List getOperatorsListAsc(List data) {
        List list = new ArrayList();
        for (int i = 0; i < data.size(); i++) {
            Object[] ret = (Object[]) data.get(i);
            if (isInList(list, ret[3].toString()))
                list.add(ret[3].toString());
        }
        Collections.sort(list);
        return list;
    }

    private boolean isInList(List data, String operator) {
        for (int i = 0; i < data.size(); i++) {
            if (data.get(i).toString().equals(operator))
                return false;
        }
        return true;
    }


    private List getSellersListAsc(List data) {
        List list = new ArrayList();
        for (int i = 0; i < data.size(); i++) {
            Object[] ret = (Object[]) data.get(i);
            if (isSellerInList(list, ret[1].toString()))
                list.add(ret[1].toString());
        }
        Collections.sort(list);
        return list;
    }

    private boolean isSellerInList(List data, String seller) {
        for (int i = 0; i < data.size(); i++) {
            if (data.get(i).toString().equals(seller))
                return false;
        }
        return true;
    }


    public void refresh() throws Throwable {
            bindGrid();
    }

    protected void bindGrid() throws GridException, DatabaseException, Exception {
        grid.bind(new EntityManager().EXECUTE_QUERY(SellerBalanceTransaction.class, "networkAmounts", prepareFilters()));
    }


    public List getSalesReport() throws DatabaseException, Exception {
        //if(checkBoxGroupByTerminals.isSelected())  // отчет с терминалами
        if(checkBoxGroupByTerminals == true)  // отчет с терминалами
        {
            return new EntityManager().EXECUTE_QUERY(SellerBalanceTransaction.class, "uploadSales2", prepareFilters());
        }
        else
        {
            return new EntityManager().EXECUTE_QUERY(SellerBalanceTransaction.class, "uploadSales1", prepareFilters());
        }
    }

    // Фильтр выборки
    private QueryParameterWrapper[] prepareFilters() throws DatabaseException, Exception {// fill selers

            List sellerBalances = Arrays.asList(multipleSelectBoxSellerBalances.getSelectedKeys());
            List subSellerBalances = Arrays.asList(multipleSelectBoxSellerBalances.getSelectedKeys());
            List balancesList = new ArrayList();
            List<Terminal> terminals = new ArrayList<Terminal>();

            if (editBoxTerminals.getValue() != null || !editBoxTerminals.getValue().equals("")) {
                List<String> terminalsCodes = new ArrayList<String>();
                String editBoxString = editBoxTerminals.getValue();
                if(editBoxString.indexOf(",") > 0) {
                    String [] terminalCodes = editBoxString.split(",");
                    terminalsCodes.addAll(Arrays.asList(terminalCodes));
                } else {
                    terminalsCodes.add(editBoxString);
                }

                terminals = (new TerminalManager()).getTerminalsBySN(terminalsCodes);
            }
            String isFilteringByTerminals = "0";
            if (terminals.size() > 0) {
                isFilteringByTerminals = "1";
            } else {
                terminals.add((new TerminalManager()).getTerminalBySN("SVB00181"));
            }

            for (Object o : multipleSelectBoxSellerBalances.getSelectedKeys()) {
                balancesList.add(o);
            }
            for (Object o : multipleSelectBoxSubSellerBalances.getSelectedKeys()) {
                balancesList.add(o);
            }
            String description = "";

            //if (checkBoxDescription.isSelected()) {
            if(roleFlag == true)
            {
                if (checkBoxDescription == true) {
                    description = "show";
                }
            }

            return new QueryParameterWrapper[]{

                    //new QueryParameterWrapper("fromFiltering", from.getTime() == null ? 0 : 1),
                    new QueryParameterWrapper("fromT", from.getTime() == null ? new Date() : from.getTime()),

                    //new QueryParameterWrapper("toFiltering", to.getTime() == null ? 0 : 1),
                    new QueryParameterWrapper("toT", to.getTime() == null ? new Date() : to.getTime()),

                    new QueryParameterWrapper("sellerBalances", balancesList),

                    new QueryParameterWrapper("terminals", terminals),

                    new QueryParameterWrapper("isFilteringByTerminals", isFilteringByTerminals),

                    new QueryParameterWrapper("descr", description)

            };
         
    }

    public String ListToString(List list)
    {
        String listString = "";

        return listString;
    }


    public TimeSelector getFrom() {
        return from;
    }

    public void setFrom(TimeSelector from) {
        this.from = from;
    }

    public TimeSelector getTo() {
        return to;
    }

    public void setTo(TimeSelector to) {
        this.to = to;
    }

    public Grid getGrid() {
        return grid;
    }

    public void setGrid(Grid grid) {
        this.grid = grid;
    }


    public Button getButtonSubmit() {
        return buttonSubmit;
    }

    public void setButtonSubmit(Button buttonSubmit) {
        this.buttonSubmit = buttonSubmit;
    }

    private class SellerData {
        private String SellerName;
        private int operatorId;
        private String operatorName;
        private double st_amount;
        private double st_count;
        private String terminalNum;
        private String equipType;


        public String getSellerName() {
            return SellerName;
        }

        public void setSellerName(String sellerName) {
            SellerName = sellerName;
        }

        public int getOperatorId() {
            return operatorId;
        }

        public void setOperatorId(int operatorId) {
            this.operatorId = operatorId;
        }

        public String getOperatorName() {
            return operatorName;
        }

        public void setOperatorName(String operatorName) {
            this.operatorName = operatorName;
        }

        public double getSt_amount() {
            return st_amount;
        }

        public void setSt_amount(double st_amount) {
            this.st_amount = st_amount;
        }

        public double getSt_count() {
            return st_count;
        }

        public void setSt_count(double st_count) {
            this.st_count = st_count;
        }

        public String getTerminalNum() {
            return terminalNum;
        }

        public void setTerminalNum(String terminalNum) {
            this.terminalNum = terminalNum;
        }

        public String getEquipType() {
            return equipType;
        }

        public void setEquipType(String equipType) {
            this.equipType = equipType;
        }
    }

    public MultipleSelectBox getMultipleSelectBoxSellerBalances() {
        return multipleSelectBoxSellerBalances;
    }

    public void setMultipleSelectBoxSellerBalances(MultipleSelectBox multipleSelectBoxSellerBalances) {
        this.multipleSelectBoxSellerBalances = multipleSelectBoxSellerBalances;
    }

    public MultipleSelectBox getMultipleSelectBoxSubSellerBalances() {
        return multipleSelectBoxSubSellerBalances;
    }

    public void setMultipleSelectBoxSubSellerBalances(MultipleSelectBox multipleSelectBoxSubSellerBalances) {
        this.multipleSelectBoxSubSellerBalances = multipleSelectBoxSubSellerBalances;
    }

    public Button getButtonUploadReport() {
        return buttonUploadReport;
    }

    public void setButtonUploadReport(Button buttonUploadReport) {
        this.buttonUploadReport = buttonUploadReport;
    }

    /*public CheckBox getCheckBoxDescription() {
        return checkBoxDescription;
    }

    public void setCheckBoxDescription(CheckBox checkBoxDescription) {
        this.checkBoxDescription = checkBoxDescription;
    }*/

    public boolean isCheckBoxDescription() {
        return checkBoxDescription;
    }

    public void setCheckBoxDescription(boolean checkBoxDescription) {
        this.checkBoxDescription = checkBoxDescription;
    }

    public ScrollBox getScrollBoxEquipmentType() {
        return scrollBoxEquipmentType;
    }

    public void setScrollBoxEquipmentType(ScrollBox scrollBoxEquipmentType) {
        this.scrollBoxEquipmentType = scrollBoxEquipmentType;
    }

    public EditBox getEditBoxTerminals() {
        return editBoxTerminals;
    }

    public void setEditBoxTerminals(EditBox editBoxTerminals) {
        this.editBoxTerminals = editBoxTerminals;
    }

    /*public CheckBox getCheckBoxGroupByTerminals() {
        return checkBoxGroupByTerminals;
    }

    public void setCheckBoxGroupByTerminals(CheckBox checkBoxGroupByTerminals) {
        this.checkBoxGroupByTerminals = checkBoxGroupByTerminals;
    }*/

    public boolean isCheckBoxGroupByTerminals() {
        return checkBoxGroupByTerminals;
    }

    public void setCheckBoxGroupByTerminals(boolean checkBoxGroupByTerminals) {
        this.checkBoxGroupByTerminals = checkBoxGroupByTerminals;
    }

    public String getDescr() {
        return descr;
    }

    public void setDescr(String descr) {
        this.descr = descr;
    }

    public boolean is_checkBoxDescription() {
        return _checkBoxDescription;
    }

    public void set_checkBoxDescription(boolean _checkBoxDescription) {
        this._checkBoxDescription = _checkBoxDescription;
    }

    public boolean is_checkBoxGroupByTerminals() {
        return _checkBoxGroupByTerminals;
    }

    public void set_checkBoxGroupByTerminals(boolean _checkBoxGroupByTerminals) {
        this._checkBoxGroupByTerminals = _checkBoxGroupByTerminals;
    }

    public boolean getRoleFlag() {
        return roleFlag;
    }

    public void setRoleFlag(boolean roleFlag) {
        this.roleFlag = roleFlag;
    }
}
