package com.tmx.web.base;

/**
  Uses
  /WEB-XML/role_behavior_mapping.xml and
  /WEB-XML/action_behavior_mapping.xml to evaluate
  role-action-command behavior.

  @See action_behavior_mapping.xsd
  @See role_behavior_mapping.xsd
  for details
 */
public abstract class BasicBehaviorizedAction extends BasicDispatchedAction{
    //RM: not implemented yet
    //do not use introspection to call default method!
}
