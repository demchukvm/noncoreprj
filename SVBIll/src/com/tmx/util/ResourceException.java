package com.tmx.util;

import java.util.Locale;

public class ResourceException extends StructurizedException {

    public ResourceException(String errKey){
        super(errKey);
    }

    public ResourceException(String errKey, Locale locale){
        super(errKey, locale);
    }

    public ResourceException(String errKey, String[] params, Locale locale){
        super(errKey, params, locale);
    }

    public ResourceException(String errKey, String[] params, Throwable e){
        super(errKey, params, e);
    }

    public ResourceException(String errKey, String[] params){
        super(errKey, params);
    }

    public ResourceException(String errKey, Throwable cause){
        super(errKey, cause);
    }
}
