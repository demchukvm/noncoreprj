package com.tmx.beng.base.channels;

import com.tmx.beng.base.BillingMessage;
import com.tmx.beng.base.BillException;

/**
 * Returns channel number 0 if operation uses account number and 1 otherwise (i.e. on msisdn). 
 */
public class ContractNumberChannel extends AbstractChannel{

    public OutChannel process(BillingMessage msg) throws BillException {
        return msg.getAttribute(BillingMessage.PAY_ACCOUNT_NUMBER) != null ? getOutChannel(0) : getOutChannel(1); 
    }


}
