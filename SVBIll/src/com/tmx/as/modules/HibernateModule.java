package com.tmx.as.modules;

import com.tmx.as.base.HibernateSessionFactoryProvider;

import java.util.Properties;

/**
 * Created by IntelliJ IDEA.
 * User: shake
 * Date: 25.06.2007
 * Time: 15:56:19
 */
public class HibernateModule extends AbstractModule{

    public void start(Properties props)throws ModuleException{
        HibernateSessionFactoryProvider.testConnections();
    }

    public void shutdownGraceful(Properties props)throws ModuleException{
    }

    public void shutdownAbort(Properties props)throws ModuleException{
    }
}
