package com.tmx.beng.base.dbfunctions;

import java.util.List;
import java.util.Map;
import java.util.ArrayList;
import java.util.Arrays;


public class ParameterValidator {

    public static List validateParamNames(Map params, String... names) {
        return validateParamNames(params, Arrays.asList(names));
    }

    public static List<ValidationError> validateParamNames(Map params, List<String> names) {
        ArrayList<ValidationError> errors = new ArrayList<ValidationError>();
        for (String name : names) {
            if (!params.containsKey(name)) {  
                errors.add(new ValidationError("no " + name + " param"));
            }    
        }
        if (params.size() > names.size()) {
            errors.add(new ValidationError("too many params"));
        }
        return errors;
    }

    public static List<ValidationError> validateParamCount(Map params, List<String> names) {
        ArrayList<ValidationError> errors = new ArrayList<ValidationError>();
        if (params.size() > names.size()) {
            errors.add(new ValidationError("too many params"));
        }
        return errors;
    }

    public static List validateDiscountPercent(Map params, String[] names) {
        return validateRange(params, names, 0, 2);
    }

    // for all names (all names = params.keySet())
    public static List validateDiscountPercent(Map params) {
        return validateRange(params, (String[]) params.keySet().toArray(new String[params.keySet().size()]), 0, 1);
    }
  
    public static List validateTxCost(Map params, String[] names) {
        return validateRange(params, names, 0, Integer.MAX_VALUE);
    }

    //only if params contains names
    public static List validateRange(Map params, String[] names, int from, int to) {
        ArrayList errors = new ArrayList();
        for (String name : names) {
            if (params.get(name) != null) {
                try {
                    double value = Double.parseDouble((String) params.get(name));
                    if (value < from || value > to) {
                        errors.add(new ValidationError("incorrect value " + name));
                    }
                } catch (NumberFormatException e) {
                    errors.add(new ValidationError(e.getMessage()));
                }
            }
        }
        return errors;
    }

}
