<?php
/*
  Template Name: Homepage
 */
?>
<?php get_header();?>
<div class="table_block_middle">

	<div class="left">
		<?php echo get_field('text_5');?>
	</div>

	<div class="right">
        <?php echo get_field('text_6');?>
	</div>

</div>

<div class="table_block_footer">

	<div class="info-left">
		<p><?php echo get_field('text_7');?></p>
	</div>

	<div class="info-right">
		<p><?php echo get_field('text_8');?></p>
	</div>

</div>

<div class="bless_edge">

	<div class="left">
		<?php if(!isset($_GET['lang'])) { ?><img src="<?php bloginfo('template_url');?>/images/bless-features-shape.gif" alt="BLESS преимущества"><?php } ?></h2>

		<ul class="list">
            <?php echo get_field('text_9');?>
		</ul>
	</div>

	<div class="right">

		<ul class="list bottom">
            <?php echo get_field('text_10');?>
		</ul>

		<span class="what">
			<?php echo get_field('text_11');?><br />
            <?php echo get_field('text_12');?>
		</span>

	</div>

</div>

<?php get_footer(); ?>