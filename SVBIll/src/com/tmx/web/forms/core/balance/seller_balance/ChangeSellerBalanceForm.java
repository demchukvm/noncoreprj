package com.tmx.web.forms.core.balance.seller_balance;

import com.tmx.web.forms.core.balance.AbstractChangeBalanceForm;
import com.tmx.web.controls.EditBox;
import com.tmx.as.entities.bill.balance.OperatorBalance;
import com.tmx.as.entities.bill.balance.SellerBalance;
import com.tmx.as.entities.bill.balance.AbstractBalance;


public class ChangeSellerBalanceForm extends AbstractChangeBalanceForm {

    protected void init() {
        super.init();

        getOnCancelButton().setAction("/core/balance/seller_balance/infoSellerBalance");
        getOnChangeButton().setAction("/core/balance/seller_balance/changeSellerBalance");
    }

    public void bindData(AbstractBalance abstractBalance) {
        super.bindData(abstractBalance);
        SellerBalance sellerBalance = (SellerBalance) abstractBalance;
        editBoxOwnerName.setValue(sellerBalance.getSeller().getName());
    }

}
