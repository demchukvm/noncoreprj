package com.tmx.as.entities.bill.transaction;

import com.tmx.as.entities.bill.balance.OperatorBalance;
import com.tmx.as.entities.bill.operator.Operator;

/**
 */
public class OperatorBalanceTransaction extends AbstractTransaction{
    private Operator operator;
    private OperatorBalance operatorBalance;
    private String cliTransactionGUID;


    //------------Operator balance
    public Operator getOperator() {
        return operator;
    }

    public void setOperator(Operator operator) {
        this.operator = operator;
    }

    public OperatorBalance getOperatorBalance() {
        return operatorBalance;
    }

    public void setOperatorBalance(OperatorBalance operatorBalance) {
        this.operatorBalance = operatorBalance;
    }

    public String getCliTransactionGUID() {
        return cliTransactionGUID;
    }

    public void setCliTransactionGUID(String cliTransactionGUID) {
        this.cliTransactionGUID = cliTransactionGUID;
    }
}
