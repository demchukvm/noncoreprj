package com.tmx.as.resolver.functions.projection;

import com.tmx.as.base.ProjectionWrapper;
import com.tmx.as.resolver.functions.Function;
import com.tmx.as.resolver.FunctionSyntaxException;
import com.tmx.as.resolver.ResolverException;

public class Min implements Function {
    private ProjectionWrapper projection=new ProjectionWrapper("",null,ProjectionWrapper.MIN);

    public void set(int paramIndex, String paramName, Object paramValue) throws FunctionSyntaxException {
        try {
            projection.set(paramName,(String)paramValue);
        } catch (ResolverException e) {
            throw new FunctionSyntaxException("projection error", e);
        }
    }

    public Object returnValue() throws FunctionSyntaxException {
        return projection;
    }
}
