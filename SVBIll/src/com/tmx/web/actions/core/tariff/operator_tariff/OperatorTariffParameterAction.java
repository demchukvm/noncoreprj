package com.tmx.web.actions.core.tariff.operator_tariff;

import com.tmx.as.blogic.balance.AbstractBalanceManager;
import com.tmx.as.blogic.balance.OperatorBalanceManager;
import com.tmx.web.actions.core.tariff.AbstractTariffParameterAction;
import com.tmx.web.base.SessionEnvironment;
import com.tmx.web.base.WebException;
import com.tmx.web.forms.core.tariff.AbstractInfoTariffForm;
import com.tmx.web.forms.core.tariff.operator_tariff.InfoOperatorTariffForm;
import com.tmx.util.InitException;
import com.tmx.beng.base.BillException;
import org.apache.struts.action.ActionMapping;

import javax.servlet.http.HttpServletRequest;


public class OperatorTariffParameterAction  extends AbstractTariffParameterAction {
    protected AbstractBalanceManager getBalanceManager() throws InitException, BillException {
        return new OperatorBalanceManager();
    }

    protected AbstractInfoTariffForm getInfoTariffForm(ActionMapping mapping, HttpServletRequest request) throws WebException {
        return (AbstractInfoTariffForm) SessionEnvironment.allocateForm(
                InfoOperatorTariffForm.class, "core.tariff.operator_tariff.infoOperatorTariffForm", mapping, request);  
    }
}
