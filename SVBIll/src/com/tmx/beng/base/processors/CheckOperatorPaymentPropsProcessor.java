package com.tmx.beng.base.processors;

import com.tmx.beng.base.*;
import com.tmx.beng.base.cache.DBCache;


public class CheckOperatorPaymentPropsProcessor extends TaskProcessor{

    public Task process0(Task task, DBCache dbCache, BillingMessageWritable msg) throws BillException {
        logger.debug("Check operator payment properties...");

        // check is service (operator) allowed
        if(!dbCache.isOperatorAllowed(task.getProcesedMessage().getAttributeString(BillingMessage.SERVICE_CODE))){
            msg.setStatusCode(StatusDictionary.PAYMENT_PROPS_CHECK_ERROR);
            throw new BillException("err.bill.check_pay_props_processor.service_not_allowed", new String[]{
                    msg.getAttributeString(BillingMessage.SERVICE_CODE)
            });
        }

        return task;
    }    


}
