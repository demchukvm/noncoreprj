package com.tmx.web.taglib.common;

import org.apache.struts.taglib.TagUtils;
import javax.servlet.jsp.JspException;

import com.tmx.as.base.EntityResources;
import com.tmx.as.modules.EntityResourcesModule;

import java.util.Locale;


public class EntityWriteLabelTag extends com.tmx.web.taglib.base.LocalizableTag {
    private String entityClassName = null;
    private String attrName = null;

    //--------------------Properties

    public String getEntityClassName() {
        return entityClassName;
    }

    public void setEntityClassName(String entityClassName) {
        this.entityClassName = entityClassName;
    }

    public String getAttrName() {
        return attrName;
    }

    public void setAttrName(String attrName) {
        this.attrName = attrName;
    }


    //------------------------------Parsing events

    public int doStartTmxTag() throws JspException {
        String labelHTML = renderLabelHTML();
        TagUtils.getInstance().write(pageContext, labelHTML);
        return EVAL_BODY_INCLUDE;
    }

    //------------------------------Rendering methods

    private String renderLabelHTML(){
        Class entityClass = null;
        try{
            entityClass = Class.forName(getEntityClassName());
        }
        catch(ClassNotFoundException e){
            return "";
        }
        EntityResources entityResources = EntityResourcesModule.getEntityResources();
        Locale currentLocale = getLocale();
        return entityResources.getEntityLabel(entityClass, getAttrName(), currentLocale);
    }

}
