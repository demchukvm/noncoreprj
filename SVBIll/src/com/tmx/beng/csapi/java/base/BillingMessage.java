package com.tmx.beng.csapi.java.base;


public interface BillingMessage <T> {
    T sendSync();
}

