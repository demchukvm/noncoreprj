<%@ page language="java" pageEncoding="UTF-8" contentType="text/html; charset=utf-8" isELIgnored="false" %>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-nested" prefix="nested" %>
<%@ taglib uri="http://tmx.com/taglib/tmx-controls" prefix="tmx_ctrl" %>
<%@ taglib uri="http://tmx.com/taglib/tmx-logic" prefix="tmx_logic" %>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html" %>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean" %>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>

<html:form action="/core/transactions/operator_transaction/manageOperatorTransactions">
<table width="100%" border="0">
    
    <!-- FILTER SET -->
    <tr>
        <td valign="top" width="100%" style="text-align: left">
            <tmx_ctrl:panelFilterSet tableName="operatorTransactionsTable">
            <table>
                <tr>
                    <td class="dataLabel">
                        <tmx_ctrl:writeEntityLabel
                                attrName="transactionFromTime"
                                entityClassName="com.tmx.as.entities.bill.transaction.ClientTransaction"/>:
                    </td>
                    <td class="dataField">
                        <tmx_ctrl:timeSelector
                                property="operatorTransactionsTable.filterSet.filter(by_FromDate).parameter(date).control"/>
                    </td>

                    <td class="dataLabel">
                        <tmx_ctrl:writeEntityLabel
                                attrName="transactionToTime"
                                entityClassName="com.tmx.as.entities.bill.transaction.ClientTransaction"/>:
                    </td>
                    <td class="dataField">
                        <tmx_ctrl:timeSelector
                                property="operatorTransactionsTable.filterSet.filter(by_ToDate).parameter(date).control"/>
                    </td>
                </tr>

                <tr>
                    <td class="dataLabel">
                        <tmx_ctrl:writeEntityLabel
                                attrName="amountFrom"
                                entityClassName="com.tmx.as.entities.bill.transaction.ClientTransaction"/>:
                    </td>
                    <td class="dataField">
                        <tmx_ctrl:editBox
                                property="operatorTransactionsTable.filterSet.filter(by_client_amount_from).parameter(from_amount).control"/>
                    </td>

                    <td class="dataLabel">
                        <tmx_ctrl:writeEntityLabel
                                attrName="amountTo"
                                entityClassName="com.tmx.as.entities.bill.transaction.ClientTransaction"/>:
                    </td>
                    <td class="dataField">
                        <tmx_ctrl:editBox
                                property="operatorTransactionsTable.filterSet.filter(by_client_amount_to).parameter(to_amount).control"/>
                    </td>
                </tr>

                <tr>
                    <td class="dataLabel">
                        <tmx_ctrl:writeEntityLabel
                                attrName="transactionGUID"
                                entityClassName="com.tmx.as.entities.bill.transaction.ClientTransaction"/>:
                    </td>
                    <td class="dataField">
                        <tmx_ctrl:editBox
                                property="operatorTransactionsTable.filterSet.filter(by_billnum_like).parameter(billnum).control"/>
                    </td>

                    <td class="dataLabel">
                        <tmx_ctrl:writeEntityLabel
                                attrName="cliTransactionNum"
                                entityClassName="com.tmx.as.entities.bill.transaction.ClientTransaction"/>:
                    </td>
                    <td class="dataField">
                        <tmx_ctrl:editBox
                                property="operatorTransactionsTable.filterSet.filter(by_clinum_like).parameter(clinum).control"/>
                    </td>
                </tr>

                <tr>
                    <td class="dataField">
                        <tmx_ctrl:writeEntityLabel
                                attrName="{@name}"
                                entityClassName="com.tmx.as.entities.bill.transaction.TransactionType"/>:
                    </td>
                    <td class="dataField">
                        <tmx_ctrl:selectBox
                                property="operatorTransactionsTable.filterSet.filter(by_transaction_type).parameter(type).control"/>
                    </td>

                    <td class="dataLabel">
                        <tmx_ctrl:writeEntityLabel
                                attrName="{@name}"
                                entityClassName="com.tmx.as.entities.bill.operator.Operator"/>:
                    </td>
                    <td class="dataField">
                        <tmx_ctrl:selectBox
                                property="operatorTransactionsTable.filterSet.filter(by_operator).parameter(operator).control"/>
                    </td>

                </tr>

            </table>
        </tmx_ctrl:panelFilterSet>
        </td>
    </tr>
    <!-- TABLE -->
    <tr>
        <td width="100%">
            <table class='one_outer_cell_table' cellspacing='0' cellpadding='0' width="100%">
                <tr>
                    <td>
                        <table cellspacing='1' cellpadding='1' width="100%">
                            <!-- Table header -->
                            <tr>

                                <th class="column_header_cell" width="110">
                                    <tmx_ctrl:imgTableOrdering tableName="operatorTransactionsTable"
                                                               orderByFieldName="transactionTime"
                                                               labelBundle="as:com.tmx.as.entities.bill.transaction.ClientTransaction"
                                                               labelKey="transactionTime"/>
                                </th>

                                <th class="column_header_cell" width="110">
                                    <tmx_ctrl:imgTableOrdering tableName="operatorTransactionsTable"
                                                               orderByFieldName="cliTransactionNum"
                                                               labelBundle="as:com.tmx.as.entities.bill.transaction.ClientTransaction"
                                                               labelKey="cliTransactionNum"/>
                                </th>

                                <th class="column_header_cell" width="110">
                                    <tmx_ctrl:imgTableOrdering tableName="operatorTransactionsTable"
                                                               orderByFieldName="transactionGUID"
                                                               labelBundle="as:com.tmx.as.entities.bill.transaction.ClientTransaction"
                                                               labelKey="transactionGUID"/>
                                </th>

                                <th class="column_header_cell" width="110">
                                    <tmx_ctrl:imgTableOrdering tableName="operatorTransactionsTable"
                                                               orderByFieldName="operator.name"
                                                               labelBundle="as:com.tmx.as.entities.bill.operator.Operator"
                                                               labelKey="{@name}"/>
                                </th>

                                <th class="column_header_cell" width="110">
                                    <tmx_ctrl:imgTableOrdering tableName="operatorTransactionsTable"
                                                               orderByFieldName="amount"
                                                               labelBundle="as:com.tmx.as.entities.bill.transaction.ClientTransaction"
                                                               labelKey="amount"/>
                                </th>

                                <th class="column_header_cell" width="110">
                                    <tmx_ctrl:imgTableOrdering tableName="operatorTransactionsTable"
                                                               orderByFieldName="status"
                                                               labelBundle="as:com.tmx.as.entities.bill.transaction.ClientTransaction"
                                                               labelKey="status"/>
                                </th>

                            </tr>
                            <nested:iterate id="operatorTransaction" property="operatorTransactionsTable.data" type="com.tmx.as.entities.bill.transaction.OperatorBalanceTransaction">
                                <tr>
                                    <td class='column_data_cell'>
                                            <fmt:formatDate value="${operatorTransaction.transactionTime}" pattern="yyyy/MM/dd HH:mm:ss"/>
                                    </td>
                                    <td class='column_data_cell'>
                                            <c:out value="${operatorTransaction.cliTransactionNum}"/>
                                    </td>
                                    <td class='column_data_cell'>
                                        <html:link action="/core/transactions/operator_transaction/infoOperatorTransaction"
                                                   paramId="selectedTransactionId" paramName="operatorTransaction" paramProperty="tansactionId">
                                            <c:out value="${operatorTransaction.transactionGUID}" />
                                        </html:link>
                                    </td>
                                    <td class='column_data_cell'>
                                        <c:if test="${operatorTransaction.operator != null}">
                                            <c:out value="${operatorTransaction.operator.name}" />
                                        </c:if>
                                    </td>

                                    <td class='column_data_cell'>
                                        <fmt:formatNumber value="${operatorTransaction.amount}" currencySymbol="" type="currency"/>
                                    </td>
                                    <td class='column_data_cell'>
                                        <tmx_ctrl:write
                                                name="core.transactions.operator_transaction.manageOperatorTransactionsForm"
                                                property="transactionStatusDictionary.value(status.${operatorTransaction.status})" ignore="true" length="13"/>
                                    </td>
                                </tr>
                            </nested:iterate>
                            <!-- Empty row -->
                            <nested:define id="tableSize" property="operatorTransactionsTable.resultSize" type="java.lang.Integer"/>
                            <c:if test="${tableSize <= 0}">
                                <tr>
                                    <td class='column_data_cell' colspan="6" align="center">
                                        <bean:message bundle="core.labels" key="core.common.th.empty_table" />
                                    </td>
                                </tr>
                            </c:if>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td style="text-align: right">
            <tmx_ctrl:imgTablePaging maxBookmarkCount="10" tableName="operatorTransactionsTable"/>
        </td>
    </tr>
    <tr>
        <td valign="center" align="center" class="tabForm">
            <table width="100%" border="0">
            <%--<!-- Form header -->--%>
                <tr>
                    <td colspan="2">
                        <h4 class="dataLabel">
                            <bean:message bundle="core.labels" key="core.transaction.clienttransaction.infoclienttransactionform.titleAmount"/>
                        </h4>
                    </td>
                </tr>
                <tr>
                    <td class="dataLabel">
                        <tmx_ctrl:writeEntityLabel
                                attrName="successTransactions"
                                entityClassName="com.tmx.as.entities.bill.transaction.ClientTransaction"/>:
                    </td>
                    <td class="dataField">
                        <tmx_ctrl:editBox
                                property="editBoxSuccessTransaction"/>
                    </td>


                    <td class="dataLabel">
                        <tmx_ctrl:writeEntityLabel
                                attrName="allTransactions"
                                entityClassName="com.tmx.as.entities.bill.transaction.ClientTransaction"/>:
                    </td>
                    <td class="dataField">
                        <tmx_ctrl:editBox
                                property="editBoxAllTransaction"/>
                    </td>
                </tr>

                <tr>
                    <td class="dataLabel">
                        <tmx_ctrl:writeEntityLabel
                                attrName="successTransactionAmount"
                                entityClassName="com.tmx.as.entities.bill.transaction.ClientTransaction"/>:
                    </td>
                    <td class="dataField">
                        <tmx_ctrl:editBox
                                property="editBoxSuccessTransAmount"/>
                    </td>


                    <td class="dataLabel">
                        <tmx_ctrl:writeEntityLabel
                                attrName="allTransactionAmount"
                                entityClassName="com.tmx.as.entities.bill.transaction.ClientTransaction"/>:
                    </td>
                    <td class="dataField">
                        <tmx_ctrl:editBox
                                property="editBoxAllTransAmount"/>
                    </td>
                </tr>
            </table>
        </td>
    </tr>

</table>
</html:form>