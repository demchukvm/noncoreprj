package com.tmx.beng.iso8583Gate.test;

import com.tmx.beng.iso8583Gate.pdu.*;
import com.tmx.beng.iso8583Gate.ISO8583Service;
import com.tmx.util.InitException;
import java.io.InputStream;
import java.io.IOException;
import java.net.Socket;

import org.apache.log4j.Logger;

import javax.net.ssl.SSLSocketFactory;

/**

 */
public class ISO8583Client {
    private RuntimeMessage requestMessage;
    private Socket socket = null;
    private String version = null;
    private ISO8583Service isoService = null;
    private Logger logger = ISO8583Service.getLogger(getClass());


    public ISO8583Client(String version){
        this.version = version;
    }

    public void reload() throws InitException {
        isoService = ISO8583Service.getInstance();
        isoService.reloadConfig();
    }

    public void connect(String host, int port, boolean enableSsl) throws IOException{
        if(enableSsl)
            socket = SSLSocketFactory.getDefault().createSocket(host, port);
        else
            socket = new Socket(host, port);
    }

    public void disonnect() {
        //close connection
        try {
            socket.getInputStream().close();
        } catch (IOException e) {/** ignore */}
        try {
            socket.getOutputStream().close();
        } catch (IOException e) {/** ignore */}
        try {
            socket.close();
        } catch (IOException e) {/** ignore */}
    }

    public RuntimeMessage[] send(RuntimeMessage[] reqRuntimeMsg){
        RuntimeMessage[] respRuntimeMsg = new RuntimeMessage[reqRuntimeMsg.length];
        for (int i = 0; i < reqRuntimeMsg.length; i++) {
            try {
                ISO8583PAD pad = new ISO8583PAD();

                //Serialize request ISOMessage into socket
                logger.debug("Start request message serialization...");
                pad.serialize(reqRuntimeMsg[i], socket.getOutputStream(), version);
                socket.getOutputStream().flush();
                logger.debug("Request message serialized");


                //Read bytedata from socket and parse it to produce ISOMessage
                logger.debug("Read and parse response message...");
                InputStream is = socket.getInputStream();
                respRuntimeMsg[i] = pad.parse(is, version);
                logger.debug("Response message parsed");
            }
            catch (IOException e) {
                logger.error("Failed to handle request: " + e.toString());
            }
            catch (IncorrectFormatException e) {
                logger.error("Failed to handle request: " + e.getTraceDump());
            }
            catch (IsoParseException e) {
                logger.error("Failed to handle request: " + e.getTraceDump());
            }
            catch (IsoSerializeException e) {
                logger.error("Failed to handle request: " + e.getTraceDump());
            }
            catch (Throwable e) {
                logger.error("Failed to handle request: " + e.toString());
            }
        }
        return respRuntimeMsg;
    }

    public RuntimeMessage prepareRequestMessage() throws InitException {
        MessageCommonFormat mcf = isoService.getProtocolMetadata().getProtocolVersion(version).getMessageCommonFormat();
//        MessageCommonFormat mcf = new MessageCommonFormat();
//        mcf.load("D:\\SVBILL\\repository\\beng\\iso8583Gate\\protocols\\ssi\\v1", "message_format.xml");
        RuntimeMessage rm = new RuntimeMessage(mcf);

        DataElementFormat def;
        RuntimeDataElement rde;


        def = mcf.getDataElementFormat(DataElementFormat.DEF_MESSAGETYPEID);
        rde = new RuntimeDataElement(def);
        rde.setValue(800);
        rm.addRuntimeDataElement(rde);

        def = mcf.getDataElementFormat(3);
        rde = new RuntimeDataElement(def);
        rde.setValue(990000);
        rm.addRuntimeDataElement(rde);

        def = mcf.getDataElementFormat(24);
        rde = new RuntimeDataElement(def);
        rde.setValue(12);
        rm.addRuntimeDataElement(rde);

        def = mcf.getDataElementFormat(41);
        rde = new RuntimeDataElement(def);
        rde.setValue("TSSISAL1");
        rm.addRuntimeDataElement(rde);

        def = mcf.getDataElementFormat(42);
        rde = new RuntimeDataElement(def);
        rde.setValue("TSSISAL1       ");
        rm.addRuntimeDataElement(rde);

//        def = mcf.getDataElementFormat(DataElementFormat.DEF_BITMAP);
//        rde = new RuntimeDataElement(def);
//        rde.setValue(rm.calculateBitmap());
//        rm.addRuntimeDataElement(rde);

        return rm;
    }
}
