package com.tmx.web.controls;

import java.util.*;

/**
 * Scroll Box control. Uses DNTML.
 */
public abstract class MultipleSelectBox extends BasicControl{
    private List data = null;
    /** Key of the selected row. Set by bean auto-population. */
    private Set selectedKeys = new HashSet();
    private Set selectedIndexes = new HashSet();
    private Set initKeys = null;
    private Set initIndexes = null;
    private boolean allItem=false;
    private boolean noneItem=false;
    private boolean disableItem=false;
//    private boolean allItemSelected=false;
//    private boolean noneItemSelected=false;
//    private boolean disableItemSelected=false;
    public static int ALL_ITEM_KEY = -1;
    public static int NONE_ITEM_KEY = -2;
    public static int DISABLE_ITEM_KEY = -3;
    public static String SELECTEDKEY_SUFFIX = "selIndPop";

    //-----------Methods for usage from action
    /**
     * Bind control to data list
     * */
    public void bind(List data){
        //assign data list
        this.data = data;
        this.safeReset();
    }

    /**
     * Return true if data is bounded to scroll box
     * */
    public boolean isBounded(){
        return (data != null);
    }

    public Set getSelectedKeys(){
        return selectedKeys;
    }

    public List getSelectedEntities(){
        ArrayList list=new ArrayList();
        Iterator indaxes=selectedIndexes.iterator();
        while (indaxes.hasNext()) {
            Integer index =  (Integer)indaxes.next();
            list.add(data.get(index.intValue()));
        }
        return list;
    }

    public Set getSelectedIndexes() {
        return selectedIndexes;
    }

    public void setSelectedIndexes(Set selectedIndexes) {
        this.selectedIndexes = selectedIndexes;
    }//-----------Methods to be implemented in the particular successor
    /**
     * Returns Key fetched from given <code>dataListElement</code>
     * */
    protected abstract Object getKey(Object dataListElement);


    /**
     * Returns Value fetched from given <code>dataListElement</code>
     * */
    protected abstract String getValue(Object dataListElement);


    public void addSelectedIndex(int index){
//        if(index==ALL_ITEM_KEY){setAllItemSelected(true); return;}
//        if(index==NONE_ITEM_KEY){setNoneItemSelected(true); return;}
//        if(index==DISABLE_ITEM_KEY){setDisableItemSelected(true); return;}
        if(index==ALL_ITEM_KEY){selectAll(); return;}
        if(index==NONE_ITEM_KEY){return;}
        if(index==DISABLE_ITEM_KEY){return;}
        if(index>-1&&data.get(index)!=null){
            selectedIndexes.add(new Integer(index));
            selectedKeys.add(getKey(data.get(index)));
        }
    }

    public boolean isSelectedAll(){
        return selectedKeys.containsAll(data);
    }
    
    public void selectAll(){
        safeReset();
        int[] indexes=new int[data.size()];
        for (int i = 0; i < indexes.length; i++) indexes[i]=i;
        setSelectedIndexes(indexes);
    }

    public void addSelectedKeys(Object key){
        for (int i = 0; i < data.size(); i++) {
            if(key.equals(getKey(data.get(i)))){
                selectedKeys.add(key);
                selectedIndexes.add(new Integer(i));
                break;
            }
        }
    }

    // autopopulation
    public void setSelIndPop(String[] indexes){
        safeReset();
        for (int i = 0; i < indexes.length; i++) {
            addSelectedIndex(Integer.parseInt(indexes[i]));
        }
    }

    public void setSelectedIndexes(int[] indexes){
        safeReset();
        for (int i = 0; i < indexes.length; i++) {
            addSelectedIndex(indexes[i]);
        }
    }

    public void setSelectedKeys(Set keys){
        Iterator it=keys.iterator();
        while (it.hasNext()) {
            Object o =  it.next();
            addSelectedKeys(o);
        }
    }

    public void setSelectedKeys(Object[] keys){
        safeReset();
        for (int i = 0; i < keys.length; i++) {
            addSelectedKeys(keys[i]);
        }
    }

    /** Initialization */
    public void initialize() {
        /** Do nothing */
    }


    /**
     * @see com.tmx.web.controls.BasicControl
     */
    protected void safeReset(){
        if(initIndexes!=null&&initKeys!=null){
            selectedIndexes=initIndexes;
            selectedKeys=initKeys;
        }
        else{
            selectedIndexes=new HashSet();
            selectedKeys=new HashSet();
        }
    }

    public void addInitIndex(int index){
        if(index>-1&&data.get(index)!=null){
            initIndexes.add(new Integer(index));
            initKeys.add(getKey(data.get(index)));
        }
    }

    public void addInitKeys(Object key){
        for (int i = 0; i < data.size(); i++) {
            if(key.equals(getKey(data.get(i)))){
                initKeys.add(key);
                initIndexes.add(new Integer(i));
                break;
            }
        }
    }

    public void setInitIndexes(int[] indexes){
        for (int i = 0; i < indexes.length; i++) {
            addInitIndex(indexes[i]);
        }
    }

    public void setInitKeys(Set keys){
        Iterator it=keys.iterator();
        while (it.hasNext()) {
            Object o =  it.next();
            addInitKeys(o);
        }
    }

    public void setInitKeys(Object[] keys){
        for (int i = 0; i < keys.length; i++) {
            addInitKeys(keys[i]);
        }
    }

    // return data value from index
    public String getValue(int index){
        return getValue(data.get(index));
    }

    public int getSize(){
        return data.size();
    }

    public boolean isSelected(int index){
        return selectedIndexes.contains(new Integer(index));
    }

    public boolean isSelected(){
        if(selectedIndexes.size()==0)return false;
        else return true;
    }

    public boolean isAllItem() {
        return allItem;
    }

    public void setAllItem(boolean allItem) {
        this.allItem = allItem;
    }

    public boolean isDisableItem() {
        return disableItem;
    }

    public boolean isNoneItem() {
        return noneItem;
    }

    public void setNoneItem(boolean noneItem) {
        this.noneItem = noneItem;
    }

    public void setDisableItem(boolean disableItem) {
        this.disableItem = disableItem;
    }

//    public boolean isAllItemSelected() {
//        return allItemSelected;
//    }
//
//    public void setAllItemSelected(boolean allItemSelected) {
//        this.allItemSelected = allItemSelected;
//        if(allItemSelected){
//            this.noneItemSelected = false;
//            this.disableItemSelected = false;
//        }
//        selectAll();
//    }
//
//    public boolean isNoneItemSelected() {
//        return noneItemSelected;
//    }
//
//    public void setNoneItemSelected(boolean noneItemSelected) {
//        this.noneItemSelected = noneItemSelected;
//        if(noneItemSelected){
//            this.allItemSelected = false;
//            this.disableItemSelected = false;
//        }
//    }
//
//    public boolean isDisableItemSelected() {
//        return disableItemSelected;
//    }
//
//    public void setDisableItemSelected(boolean disableItemSelected) {
//        this.disableItemSelected = disableItemSelected;
//        if(disableItemSelected){
//            this.allItemSelected = false;
//            this.noneItemSelected = false;
//        }
//    }
}
