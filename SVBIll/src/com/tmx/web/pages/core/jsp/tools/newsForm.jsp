<%@ page import="com.tmx.web.forms.core.tools.NewsForm" %>
<%@ page language="java" pageEncoding="UTF-8" contentType="text/html; charset=utf-8"
         errorPage="/core/jsp/message/jsperror.jsp" isELIgnored="false" %>
<%@ taglib uri="/WEB-INF/tld/struts-nested.tld" prefix="nested" %>
<%@ taglib uri="/WEB-INF/tld/tmx-controls.tld" prefix="tmx_ctrl" %>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>

<%--<body onload="Init();">--%>
<%--<html:form action="/core/tools/news">--%>

<div style="width:50%;position:inherit;left:15px;float:left;">
	<p>
        <b>Title</b><br />
        <%--<input name="messageTitle" type="text" style="width:100%;background-image:url('<tmx_ctrl:rewriteUrl href="/core/images/message_editor/title.png"/>');"/>--%>
        <input name="messageTitle" type="text" style="width:100%;font-family:Times New Roman;font-size: 11pt;"/>
    </p>

	<%--<div style="width:100%;">--%>
    <p>
    	<a href="javascript:insert('[left]','[/left]');"><img src="<tmx_ctrl:rewriteUrl href="/core/images/message_editor/ico2.gif"/>" border="0" width="24" height="24" alt="Выровнять по левому краю"></a>
		<a href="javascript:insert('[center]','[/center]');"><img src="<tmx_ctrl:rewriteUrl href="/core/images/message_editor/ico1.gif"/>" border="0" width="24" height="24" alt="Выровнять по центру"></a>
		<a href="javascript:insert('[right]','[/right]');"><img src="<tmx_ctrl:rewriteUrl href="/core/images/message_editor/ico8.gif"/>" border="0" width="24" height="24" alt="Выровнять по правому краю"></a>
		<a href="javascript:insert('[justify]','[/justify]');"><img src="<tmx_ctrl:rewriteUrl href="/core/images/message_editor/ico9.gif"/>" border="0" width="24" height="24" alt="Выровнять по ширине"></a>
		<a href="javascript:insertIMAGE();"><img src="<tmx_ctrl:rewriteUrl href="/core/images/message_editor/ico11.gif"/>" border="0" width="24" height="24" alt="Вставить изображение"></a>
		<a href="javascript:insert('[b]','[/b]');"><img src="<tmx_ctrl:rewriteUrl href="/core/images/message_editor/ico5.gif"/>" border="0" width="24" height="24" alt="Жирный"></a>
		<a href="javascript:insert('[i]','[/i]');"><img src="<tmx_ctrl:rewriteUrl href="/core/images/message_editor/ico6.gif"/>" border="0" width="24" height="24" alt="Курсивный"></a>
		<a href="javascript:insert('[u]','[/u]');"><img src="<tmx_ctrl:rewriteUrl href="/core/images/message_editor/ico7.gif"/>" border="0" width="24" height="24" alt="Подчеркнутый"></a>
		<a href="javascript:insert('[red]','[/red]');"><img src="<tmx_ctrl:rewriteUrl href="/core/images/message_editor/ico10.gif"/>" border="0" width="24" height="24" alt="Абзац"></a>

    </p>
    <textarea id="elm1" name="messageText" rows="15" cols="80" style="font-family:Times New Roman;font-size: 11pt;width:100%;" onClick="storeCaret(this);pview();" ONSELECT="storeCaret(this);" ONKEYUP="storeCaret(this);"></textarea>
	<%--</div>--%>
    <br>
    <!--  В этом SPANе выводятся ошибки -->
    <SPAN></SPAN>
    <!--  В этом SPANе выводится предосмотр -->
    <%--<div align="left" style="border: 1px solid gray;width:100%;background-image:url('<tmx_ctrl:rewriteUrl href="/core/images/message_editor/preview.png"/>');">--%>
    <div align="left" style="border: 1px solid gray;width:100%;">
        <SPAN style="font-family:Times New Roman;font-size: 11pt;"></SPAN>
    </div>
    <%--<div style="visibility:hidden;display:none;">--%>
        <%--<textarea name="savedMessageText" id="elm2"></textarea>--%>
    <%--</div>--%>

    <div align="left" style="width:100%;font-family:Times New Roman;font-size: 11pt;">
    <%= ((NewsForm) pageContext.getSession().getAttribute("NewsForm")).getResponseText() %>
    </div>

</div>

<div style="position:relative;left:15px;float:left;">
    <tmx_ctrl:button property="reset" onClick="javascript:document.forms[0].reset()" /><br />
	<tmx_ctrl:button property="saveMessage"/>
</div>



<%--</html:form>--%>
<%--
</body>--%>
