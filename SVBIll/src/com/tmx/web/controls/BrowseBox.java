package com.tmx.web.controls;

/**
    BrowseBox controls provides enchanced logic to select single cose with string description.
    Control renders HTML to open pop-window with table with filter set to choose one data item
    end return its code and value into main (opener) window.

    @See com.tmx.web.taglib.html.ctrl.BrowseBoxTag
    @See com.tmx.web.taglib.html.ctrl.BrowseCallBackTag
 */
public class BrowseBox extends BasicControl{
    /** String value to be returned (and rendered) by control after user finished browse */
    private String value;
    private String firstUpdateValue = "";
    /** Key value to be returned by control after user finished browse */
    private String key;
    private String firstUpdateKey;
    /** Action name to process window openning (Required) */
    private String action;
    /** Command name to process window openning (Optional) */
    private String command;
    /** Constants */
    public static String VALUE_SUFFIX = "value";//should be exactly the same as value property name
    public static String KEY_SUFFIX = "key";//should be exactly the same as key property name
    public static String BUTTON_SUFFIX = "button";    

    public void initialize() {
        /** Do nothing */
    }

    public void setMandatory(boolean mandatory) {
        super.setMandatory(mandatory);
        if(mandatory)
            addValidationRule(createValidationRule_NOT_NULL());
    }

    //------------------Common used validation rules
    public static ValidationRule createValidationRule_NOT_NULL() {
        return new ValidationRule("not_null", null) {
            public boolean validate(BasicControl control, Object[] args) {
                return !(control instanceof BrowseBox &&
                        (((BrowseBox) control).getKey() == null ||
                        ((BrowseBox) control).getKey().trim().length() <= 0));
            }
        };
    }

    public BrowseBox(){
        command = "openBrowseWindow";
    }

    public BrowseBox(String action){
        this();
        this.action = action;
    }

    public BrowseBox(String action, String cmd){
        this(action);
        command = cmd;
    }

    protected void safeReset(){
        value = firstUpdateValue;
        key = firstUpdateKey;
    }


    //----------Properties

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        if(!isUpdated()){
            firstUpdateKey = key;
            notifyUpdate();
        }
        this.key = key;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getCommand() {
        return command;
    }

    public void setCommand(String command) {
        this.command = command;
    }
}
