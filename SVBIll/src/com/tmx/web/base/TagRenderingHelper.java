package com.tmx.web.base;

/**
 * Supports tags rendering
 */
public abstract class TagRenderingHelper {
    /** Reset helper settings. Call it on form reset to provide reseted helper for each request. */
    public abstract void reset();
}
