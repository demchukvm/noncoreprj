package com.tmx.as.entities.bill.terminal;

import com.tmx.as.base.BasicEntity;
import com.tmx.as.entities.bill.seller.Seller;
import com.tmx.as.entities.bill.address.Address;
import com.tmx.as.entities.bill.balance.TerminalBalance;
import com.tmx.as.entities.bill.terminal_group.TerminalGroup;
import com.tmx.as.entities.bill.trade_putlet.TradePutlet;

import java.util.Date;
import java.io.Serializable;

/**
 */
public class Terminal extends BasicEntity {
    private Long terminalId;
    private String serialNumber;
    private String hardwareSerialNumber;    
    private Date registrationDate;
    private Boolean blocked;
    private Boolean allowRegistrationUpdate;
    private Seller sellerOwner;
    private Long sellerOwnerId;
    private Address address;
    private String login;
    private String password;
    private TerminalBalance primaryBalance;
    private TerminalGroup terminalGroup;
    private EquipmentType equipmentType;
    private Long terminalGroupId;
    private TradePutlet tradePutlet;

    public TradePutlet getTradePutlet() {
        return tradePutlet;
    }

    public void setTradePutlet(TradePutlet tradePutlet) {
        this.tradePutlet = tradePutlet;
    }

    public Boolean getAllowRegistrationUpdate() {
        return allowRegistrationUpdate;
    }

    public void setAllowRegistrationUpdate(Boolean allowRegistrationUpdate) {
        this.allowRegistrationUpdate = allowRegistrationUpdate;
    }

    public Long getSellerOwnerId() {
        return sellerOwnerId;
    }

    public void setSellerOwnerId(Long sellerOwnerId) {
        this.sellerOwnerId = sellerOwnerId;
    }

    public Long getTerminalGroupId() {
        return terminalGroupId;
    }

    public void setTerminalGroupId(Long terminalGroupId) {
        this.terminalGroupId = terminalGroupId;
    }

    public TerminalGroup getTerminalGroup() {
        return terminalGroup;
    }

    public void setTerminalGroup(TerminalGroup terminalGroup) {
        this.terminalGroup = terminalGroup;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Long getTerminalId() {
        return terminalId;
    }

    public void setTerminalId(Long terminalId) {
        this.terminalId = terminalId;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public String getHardwareSerialNumber() {
        return hardwareSerialNumber;
    }

    public void setHardwareSerialNumber(String hardwareSerialNumber) {
        this.hardwareSerialNumber = hardwareSerialNumber;
    }

    public Date getRegistrationDate() {
        return registrationDate;
    }

    public void setRegistrationDate(Date registrationDate) {
        this.registrationDate = registrationDate;
    }

    public Boolean getBlocked() {
        return blocked;
    }

    public void setBlocked(Boolean blocked) {
        this.blocked = blocked;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public Seller getSellerOwner() {
        return sellerOwner;
    }

    public void setSellerOwner(Seller sellerOwner) {
        this.sellerOwner = sellerOwner;
    }

    public Serializable getId() {
        return getTerminalId();
    }

    public int getValid() {
        return valid;
    }

    public void setValid(int valid) {
        this.valid = valid;
    }
     
    
    public TerminalBalance getPrimaryBalance() {
        return primaryBalance;
    }

    public void setPrimaryBalance(TerminalBalance primaryBalance) {
        this.primaryBalance = primaryBalance;
    }
    
    public EquipmentType getEquipmentType() {
        return equipmentType;
    }

    public void setEquipmentType(EquipmentType equipmentType) {
        this.equipmentType = equipmentType;
    }
}
