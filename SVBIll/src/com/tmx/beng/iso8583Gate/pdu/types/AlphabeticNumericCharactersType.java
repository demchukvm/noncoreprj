package com.tmx.beng.iso8583Gate.pdu.types;

import com.tmx.beng.iso8583Gate.pdu.IncorrectFormatException;

import java.util.Arrays;

/**
 */
public class AlphabeticNumericCharactersType extends CharacterType{
    {
        name = "an";
    }

    protected byte[] expandFixedLengthValue0(byte[] value, int fixedByteLength) throws IncorrectFormatException {
        //fill head of byte array by 0x30 to match fixed length
        byte[] expandedData = new byte[fixedByteLength];
        Arrays.fill(expandedData, (byte)0x30);
        System.arraycopy(value, 0, expandedData, expandedData.length-value.length, value.length);
        return expandedData;
    }

}
