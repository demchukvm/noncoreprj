package com.tmx.engines.xmlengine;

import org.w3c.dom.Node;
import com.tmx.as.base.Entity;

/** Class to be published into custom node */
public interface NodePrototype{
    /** Render XML node from current prototype and inject it ubder given parent
     * XML node. */
    Node renderAndInjectXMLNode(Node parentNode, Entity entity);
}
