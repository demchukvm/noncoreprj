package com.tmx.beng.medaccess;

import com.tmx.beng.base.BillingMessage;

/**
    Medium connection to work with a medium gate 
 */
public interface MediumConnection {
    public void send(BillingMessage msg);
}
