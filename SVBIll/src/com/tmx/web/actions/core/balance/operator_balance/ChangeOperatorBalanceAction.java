package com.tmx.web.actions.core.balance.operator_balance;

import com.tmx.web.actions.core.balance.AbstractChangeBalanceAction;
import com.tmx.as.blogic.balance.AbstractBalanceManager;
import com.tmx.as.blogic.balance.OperatorBalanceManager;
import com.tmx.util.InitException;
import com.tmx.beng.base.BillException;


public class ChangeOperatorBalanceAction extends AbstractChangeBalanceAction {

    protected String getInfoPageForward() {
        return "updateInfoFormAction";  
    }

    protected AbstractBalanceManager getBalanceManager() throws InitException, BillException {
        return new OperatorBalanceManager();
    }
}
