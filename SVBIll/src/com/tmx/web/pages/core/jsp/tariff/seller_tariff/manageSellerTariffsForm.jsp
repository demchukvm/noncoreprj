<%@ page language="java" pageEncoding="UTF-8" contentType="text/html; charset=utf-8"
         errorPage="/core/jsp/message/jsperror.jsp" isELIgnored="false" %>
<%@ taglib uri="/WEB-INF/tld/struts-nested.tld" prefix="nested" %>
<%@ taglib uri="/WEB-INF/tld/tmx-controls.tld" prefix="tmx_ctrl" %>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>


<html:form action="/core/tariff/seller_tariff/manageSellerTariffs">
<table width="100%" border="0">
    <!-- BUTTONS -->
    <tr>
        <td style="text-align: left">
            <tmx_ctrl:button property="onCreateButton"/>
            <tmx_ctrl:button property="onEditButton"/>
            <tmx_ctrl:button property="onDeleteButton"/>
        </td>
    </tr>

    <!--table-->

    <%--<tr>--%>
        <%--<td valign="top" width="100%" align="left">--%>
            <%--<tmx_ctrl:panelFilterSet tableName="tariffsTable">--%>
            <%--<table>--%>
                <%--<tr>--%>
                    <%--<td class="dataLabel">--%>
                        <%--<tmx_ctrl:writeEntityLabel--%>
                                <%--attrName="{@name}"--%>
                                <%--entityClassName="com.tmx.as.entities.bill.tariff.SellerTariff"/>:--%>
                    <%--</td>--%>
                    <%--<td class="dataField">--%>
                        <%--<tmx_ctrl:editBox--%>
                                <%--property="tariffsTable.filterSet.filter(by_tariff_name).parameter(tariff_name).control"/>--%>
                    <%--</td>--%>

                    <%--<td class="dataLabel">--%>
                        <%--<tmx_ctrl:writeEntityLabel--%>
                                <%--attrName="{@name}"--%>
                                <%--entityClassName="com.tmx.as.entities.bill.seller.Seller"/>:--%>
                    <%--</td>--%>
                    <%--<td class="dataField">--%>
                        <%--<tmx_ctrl:selectBox--%>
                                <%--property="tariffsTable.filterSet.filter(by_seller_id).parameter(seller_id).control"/>--%>
                    <%--</td>--%>

                    <%--<td class="dataLabel">--%>
                        <%--<tmx_ctrl:writeEntityLabel--%>
                                <%--attrName="active"--%>
                                <%--entityClassName="com.tmx.as.entities.bill.tariff.SellerTariff"/>:--%>
                    <%--</td>--%>
                    <%--<td class="dataField">--%>
                        <%--<tmx_ctrl:checkBox--%>
                                <%--property="tariffsTable.filterSet.filter(by_active_tariff).filterEnabler"/>--%>
                    <%--</td>--%>
                <%--</tr>--%>
            <%--</table>--%>
        <%--</tmx_ctrl:panelFilterSet>--%>
        <%--</td>--%>
    <%--</tr>--%>

    <tr>
        <td>
        <table class='one_outer_cell_table' cellspacing='0' cellpadding='0' width="100%">
            <tr>
                <td>
                    <table cellspacing='1' cellpadding='1' width="100%">
                        <!-- Table header -->
                        <tr>
                            <th class="column_header_cell" width="110">
                                <bean:message bundle="core.labels" key="core.common.th.choose"/>
                            </th>

                            <th class="column_header_cell" width="110">
                                <tmx_ctrl:imgTableOrdering tableName="tariffsTable"
                                                           orderByFieldName="name"
                                                           labelBundle="as:com.tmx.as.entities.bill.tariff.SellerTariff"
                                                           labelKey="{@name}"/>
                            </th>

                            <th class="column_header_cell" width="110">
                                <tmx_ctrl:imgTableOrdering tableName="tariffsTable"
                                                           orderByFieldName="activationDate"
                                                           labelBundle="as:com.tmx.as.entities.bill.tariff.SellerTariff"
                                                           labelKey="activationDate"/>
                            </th>

                            <th class="column_header_cell" width="110">
                                <tmx_ctrl:imgTableOrdering tableName="tariffsTable"
                                                           orderByFieldName="deactivationDate"
                                                           labelBundle="as:com.tmx.as.entities.bill.tariff.SellerTariff"
                                                           labelKey="deactivationDate"/>
                            </th>

                            <th class="column_header_cell" width="110">
                                <tmx_ctrl:imgTableOrdering tableName="tariffsTable"
                                                           orderByFieldName="seller.name"
                                                           labelBundle="as:com.tmx.as.entities.bill.seller.Seller"
                                                           labelKey="{@name}"/>
                            </th>

                            <th class="column_header_cell" width="110">
                                <tmx_ctrl:imgTableOrdering tableName="tariffsTable"
                                                           orderByFieldName="functionInstance.functionType.name"
                                                           labelBundle="as:com.tmx.as.entities.bill.function.FunctionType"
                                                           labelKey="{@name}"/>
                            </th>
                        </tr>
                        <nested:iterate id="sellerTariff" property="tariffsTable.data" type="com.tmx.as.entities.bill.tariff.SellerTariff">
                            <tr>
                                <td class='column_data_cell'>
                                    <input type="radio" name="tariffId" value="<c:out value="${sellerTariff.id}" />"
                                           checked="false">
                                </td>
                                <td class='column_data_cell'>
                                    <c:out value="${sellerTariff.name}"/>
                                </td>
                                <td class='column_data_cell'>
                                    <fmt:formatDate value="${sellerTariff.activationDate}" pattern="yyyy/MM/dd HH:mm:ss"/>
                                </td>
                                <td class='column_data_cell'>
                                    <fmt:formatDate value="${sellerTariff.deactivationDate}" pattern="yyyy/MM/dd HH:mm:ss"/>
                                </td>
                                <td class='column_data_cell'>
                                    <c:if test="${sellerTariff.seller != null}">
                                        <c:out value="${sellerTariff.seller.name}" />
                                    </c:if>
                                </td>
                                <td class='column_data_cell'>
                                    <c:if test="${sellerTariff.functionInstance != null}">
                                        <c:if test="${sellerTariff.functionInstance.functionType != null}">
                                            <c:out value="${sellerTariff.functionInstance.functionType.name}" />
                                        </c:if>
                                    </c:if>
                                </td>
                            </tr>
                        </nested:iterate>
                        <!-- Empty row -->
                        <nested:define id="tableSize" property="tariffsTable.resultSize" type="java.lang.Integer"/>
                        <c:if test="${tableSize <= 0}">
                            <tr>
                                <td class='column_data_cell' colspan="6" align="center">
                                    <bean:message bundle="core.labels" key="core.common.th.empty_table" />
                                </td>
                            </tr>
                        </c:if>
                    </table>
                </td>
            </tr>
        </table>
        </td>
    </tr>

    <tr>
        <td style="text-align: right">
            <tmx_ctrl:imgTablePaging maxBookmarkCount="10" tableName="tariffsTable"/>
        </td>
    </tr>


    <!-- BUTTONS -->
    <tr>
        <td style="text-align: left">
            <tmx_ctrl:button property="onCreateButton"/>
            <tmx_ctrl:button property="onEditButton"/>
            <tmx_ctrl:button property="onDeleteButton"/>
        </td>
    </tr>

</table>
</html:form>