package com.tmx.as.base.criterion;

import java.util.Map;
import java.util.Collection;

public class Restrictions{
    public static CriterionWrapper allEq(Map propertyNameValues){
        CriterionWrapper criterion=new CriterionWrapper();
        criterion.setType("allEq");
        criterion.addParam("propertyNameValues",propertyNameValues);
        return criterion;
    }
    public static CriterionWrapper and(CriterionWrapper lhs, CriterionWrapper rhs){
        CriterionWrapper criterion=new CriterionWrapper();
        criterion.setType("and");
        criterion.addParam("lhs",lhs);
        criterion.addParam("rhs",rhs);
        return criterion;
    }
    public static CriterionWrapper between(String propertyName, Object lo, Object hi){
        CriterionWrapper criterion=new CriterionWrapper();
        criterion.setType("between");
        criterion.addParam("propertyName",propertyName);
        criterion.addParam("lo",lo);
        criterion.addParam("hi",hi);
        return criterion;
    }
    public static CriterionWrapper conjunction(){
        CriterionWrapper criterion=new CriterionWrapper();
        criterion.setType("conjunction");
        return criterion;
    }
    public static CriterionWrapper disjunction(){
        CriterionWrapper criterion=new CriterionWrapper();
        criterion.setType("disjunction");
        return criterion;
    }
    public static CriterionWrapper eq(String propertyName, Object value){
        CriterionWrapper criterion=new CriterionWrapper();
        criterion.setType("eq");
        criterion.addParam("propertyName",propertyName);
        criterion.addParam("value",value);
        return criterion;
    }
    public static CriterionWrapper eqProperty(String propertyName, String otherPropertyName){
        CriterionWrapper criterion=new CriterionWrapper();
        criterion.setType("eqProperty");
        criterion.addParam("propertyName",propertyName);
        criterion.addParam("otherPropertyName",otherPropertyName);
        return criterion;
    }
    public static CriterionWrapper ge(String propertyName, Object value){
        CriterionWrapper criterion=new CriterionWrapper();
        criterion.setType("ge");
        criterion.addParam("propertyName",propertyName);
        criterion.addParam("value",value);
        return criterion;
    }
    public static CriterionWrapper geProperty(String propertyName, String otherPropertyName){
        CriterionWrapper criterion=new CriterionWrapper();
        criterion.setType("geProperty");
        criterion.addParam("propertyName",propertyName);
        criterion.addParam("otherPropertyName",otherPropertyName);
        return criterion;
    }
    public static CriterionWrapper gt(String propertyName, Object value){
        CriterionWrapper criterion=new CriterionWrapper();
        criterion.setType("gt");
        criterion.addParam("propertyName",propertyName);
        criterion.addParam("value",value);
        return criterion;
    }
    public static CriterionWrapper gtProperty(String propertyName, String otherPropertyName){
        CriterionWrapper criterion=new CriterionWrapper();
        criterion.setType("gtProperty");
        criterion.addParam("propertyName",propertyName);
        criterion.addParam("otherPropertyName",otherPropertyName);
        return criterion;
    }
    public static CriterionWrapper idEq(Object value){
        CriterionWrapper criterion=new CriterionWrapper();
        criterion.setType("idEq");
        criterion.addParam("value",value);
        return criterion;
    }
    public static CriterionWrapper ilike(String propertyName, Object value){
        CriterionWrapper criterion=new CriterionWrapper();
        criterion.setType("ilike");
        criterion.addParam("propertyName",propertyName);
        criterion.addParam("value",value);
        return criterion;
    }
//    public static CriterionWrapper ilike(String propertyName, String value, MatchMode matchMode){
//        CriterionWrapper criterion=new CriterionWrapper();
//        criterion.setType("ilike");
//            criterion.addParam("propertyName",propertyName);
//            criterion.addParam("value",value);
//            criterion.addParam("matchMode",matchMode);
//        return criterion;
//    }
    public static CriterionWrapper in(String propertyName, Collection values){
        CriterionWrapper criterion=new CriterionWrapper();
        criterion.setType("in");
        criterion.addParam("propertyName",propertyName);
        criterion.addParam("values",values);
        return criterion;
    }
    public static CriterionWrapper in(String propertyName, Object[] values){
        CriterionWrapper criterion=new CriterionWrapper();
        criterion.setType("in");
        criterion.addParam("propertyName",propertyName);
        criterion.addParam("values",values);
        return criterion;
    }
    public static CriterionWrapper isEmpty(String propertyName){
        CriterionWrapper criterion=new CriterionWrapper();
        criterion.setType("isEmpty");
        criterion.addParam("propertyName",propertyName);
        return criterion;
    }
    public static CriterionWrapper isNotEmpty(String propertyName){
        CriterionWrapper criterion=new CriterionWrapper();
        criterion.setType("isNotEmpty");
        criterion.addParam("propertyName",propertyName);
        return criterion;
    }
    public static CriterionWrapper isNotNull(String propertyName){
        CriterionWrapper criterion=new CriterionWrapper();
        criterion.setType("isNotNull");
        criterion.addParam("propertyName",propertyName);
        return criterion;
    }
    public static CriterionWrapper isNull(String propertyName){
        CriterionWrapper criterion=new CriterionWrapper();
        criterion.setType("isNull");
        criterion.addParam("propertyName",propertyName);
        return criterion;
    }
    public static CriterionWrapper le(String propertyName, Object value){
        CriterionWrapper criterion=new CriterionWrapper();
        criterion.setType("le");
        criterion.addParam("propertyName",propertyName);
        criterion.addParam("value",value);
        return criterion;
    }
    public static CriterionWrapper leProperty(String propertyName, String otherPropertyName){
        CriterionWrapper criterion=new CriterionWrapper();
        criterion.setType("leProperty");
        criterion.addParam("propertyName",propertyName);
        criterion.addParam("otherPropertyName",otherPropertyName);
        return criterion;
    }
    public static CriterionWrapper like(String propertyName, Object value){
        CriterionWrapper criterion=new CriterionWrapper();
        criterion.setType("like");
        criterion.addParam("propertyName",propertyName);
        criterion.addParam("value",value);
        return criterion;
    }
//    public static CriterionWrapper like(String propertyName, String value, MatchMode matchMode){
//        CriterionWrapper criterion=new CriterionWrapper();
//        criterion.setType("like");
//        criterion.addParam("propertyName",propertyName);
//        criterion.addParam("value",value);
//        return criterion;
//    }
    public static CriterionWrapper lt(String propertyName, Object value){
        CriterionWrapper criterion=new CriterionWrapper();
        criterion.setType("lt");
        criterion.addParam("propertyName",propertyName);
        criterion.addParam("value",value);
        return criterion;
    }
    public static CriterionWrapper ltProperty(String propertyName, String otherPropertyName){
        CriterionWrapper criterion=new CriterionWrapper();
        criterion.setType("ltProperty");
        criterion.addParam("propertyName",propertyName);
        criterion.addParam("otherPropertyName",otherPropertyName);
        return criterion;
    }
    public static CriterionWrapper naturalId(){
        CriterionWrapper criterion=new CriterionWrapper();
        criterion.setType("naturalId");
        return criterion;
    }
    public static CriterionWrapper ne(String propertyName, Object value){
        CriterionWrapper criterion=new CriterionWrapper();
        criterion.setType("ne");
        criterion.addParam("propertyName",propertyName);
        criterion.addParam("value",value);
        return criterion;
    }
    public static CriterionWrapper neProperty(String propertyName, String otherPropertyName){
        CriterionWrapper criterion=new CriterionWrapper();
        criterion.setType("neProperty");
        criterion.addParam("propertyName",propertyName);
        criterion.addParam("otherPropertyName",otherPropertyName);
        return criterion;
    }
    public static CriterionWrapper not(CriterionWrapper expression){
        CriterionWrapper criterion=new CriterionWrapper();
        criterion.setType("not");
        criterion.addParam("expression",expression);
        return criterion;
    }
    public static CriterionWrapper or(CriterionWrapper lhs, CriterionWrapper rhs){
        CriterionWrapper criterion=new CriterionWrapper();
        criterion.setType("or");
        criterion.addParam("lhs",lhs);
        criterion.addParam("rhs",rhs);
        return criterion;
    }
    public static CriterionWrapper sizeEq(String propertyName, int size){
        CriterionWrapper criterion=new CriterionWrapper();
        criterion.setType("sizeEq");
        criterion.addParam("propertyName",propertyName);
        criterion.addParam("size",new Integer(size));
        return criterion;
    }
    public static CriterionWrapper sizeGe(String propertyName, int size){
        CriterionWrapper criterion=new CriterionWrapper();
        criterion.setType("sizeGe");
        criterion.addParam("propertyName",propertyName);
        criterion.addParam("size",new Integer(size));
        return criterion;
    }
    public static CriterionWrapper sizeGt(String propertyName, int size){
        CriterionWrapper criterion=new CriterionWrapper();
        criterion.setType("sizeGt");
        criterion.addParam("propertyName",propertyName);
        criterion.addParam("size",new Integer(size));
        return criterion;
    }
    public static CriterionWrapper sizeLe(String propertyName, int size){
        CriterionWrapper criterion=new CriterionWrapper();
        criterion.setType("sizeLe");
        criterion.addParam("propertyName",propertyName);
        criterion.addParam("size",new Integer(size));
        return criterion;
    }
    public static CriterionWrapper sizeLt(String propertyName, int size){
        CriterionWrapper criterion=new CriterionWrapper();
        criterion.setType("sizeLt");
        criterion.addParam("propertyName",propertyName);
        criterion.addParam("size",new Integer(size));
        return criterion;
    }
    public static CriterionWrapper sizeNe(String propertyName, int size){
        CriterionWrapper criterion=new CriterionWrapper();
        criterion.setType("sizeNe");
        criterion.addParam("propertyName",propertyName);
        criterion.addParam("size",new Integer(size));
        return criterion;
    }
    public static CriterionWrapper sqlRestriction(String sql){
        CriterionWrapper criterion=new CriterionWrapper();
        criterion.setType("sqlRestriction");
        criterion.addParam("sql",sql);
        return criterion;
    }
//    public static CriterionWrapper sqlRestriction(String sql, Object[] values, Type[] types){
//        CriterionWrapper criterion=new CriterionWrapper();
//        criterion.setType("sizeGe");
//        criterion.addParam("propertyName",propertyName);
//        criterion.addParam("size",new Integer(size));
//        return criterion;
//    }
//    public static CriterionWrapper sqlRestriction(String sql, Object value, Type type){
//        CriterionWrapper criterion=new CriterionWrapper();
//        criterion.setType("sqlRestriction");
//        criterion.addParam("sql",sql);
//        return criterion;
//    }
}
