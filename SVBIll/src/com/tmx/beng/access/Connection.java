package com.tmx.beng.access;

import com.tmx.beng.base.BillException;
import com.tmx.beng.base.BillingMessage;

/**
 *     
 */
public interface Connection {
    public BillingMessage processSync(BillingMessage billingMessage) throws BillException;
    public void processAsync(BillingMessage billingMessage) throws BillException;    
}
