package com.tmx.as.entities.bill.voucher;

import com.tmx.as.base.BasicEntity;

import java.io.Serializable;

/**
 * Created by IntelliJ IDEA.
 * User: informix
 * Date: 24.04.2007
 * Time: 15:29:05
 */
public class VoucherNominal extends BasicEntity {
    private Long voucherNominalId;
    private String nominal;
    private Integer value;
    private String description;


    public Long getVoucherNominalId() {
        return voucherNominalId;
    }

    public void setVoucherNominalId(Long voucherNominalId) {
        this.voucherNominalId = voucherNominalId;
    }

    public Serializable getId() {
        return voucherNominalId;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public int getValid() {
        return valid;
    }

    public void setValid(int valid) {
        this.valid=valid;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getNominal() {
        return nominal;
    }

    public void setNominal(String nominal) {
        this.nominal = nominal;
    }

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }
}