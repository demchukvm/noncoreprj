package com.tmx.as.entities.mobipay.umc;

import com.tmx.as.base.BasicEntity;

import java.util.Date;
import java.util.Set;
import java.util.HashSet;
import java.io.Serializable;

/**

 */
public class UmcReport extends BasicEntity {
    private Long umcReportId;
    private String emailFrom;
    private String emailSubject;
    private Date receiveTime;
    private String fileName;
    private Date fileCreationDate;
    private Long transactionsCount;
    private Double transactionsAmountSum;
    private Long successTransactionsCount;
    private Double successTransactionsAmountSum;
    private Long failedTransactionsCount;
    private Double failedTransactionsAmountSum;
    private Set reportRows;

    public UmcReport(){
        reportRows = new HashSet();
    }

    public Long getUmcReportId() {
        return umcReportId;
    }

    public void setUmcReportId(Long umcReportId) {
        this.umcReportId = umcReportId;
    }

    public String getEmailFrom() {
        return emailFrom;
    }

    public void setEmailFrom(String emailFrom) {
        this.emailFrom = emailFrom;
    }

    public String getEmailSubject() {
        return emailSubject;
    }

    public void setEmailSubject(String emailSubject) {
        this.emailSubject = emailSubject;
    }

    public Date getReceiveTime() {
        return receiveTime;
    }

    public void setReceiveTime(Date receiveTime) {
        this.receiveTime = receiveTime;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public Date getFileCreationDate() {
        return fileCreationDate;
    }

    public void setFileCreationDate(Date fileCreationDate) {
        this.fileCreationDate = fileCreationDate;
    }

    public Long getTransactionsCount() {
        return transactionsCount;
    }

    public void setTransactionsCount(Long transactionsCount) {
        this.transactionsCount = transactionsCount;
    }

    public Double getTransactionsAmountSum() {
        return transactionsAmountSum;
    }

    public void setTransactionsAmountSum(Double transactionsAmountSum) {
        this.transactionsAmountSum = transactionsAmountSum;
    }

    public Long getSuccessTransactionsCount() {
        return successTransactionsCount;
    }

    public void setSuccessTransactionsCount(Long successTransactionsCount) {
        this.successTransactionsCount = successTransactionsCount;
    }

    public Double getSuccessTransactionsAmountSum() {
        return successTransactionsAmountSum;
    }

    public void setSuccessTransactionsAmountSum(Double successTransactionsAmountSum) {
        this.successTransactionsAmountSum = successTransactionsAmountSum;
    }

    public Long getFailedTransactionsCount() {
        return failedTransactionsCount;
    }

    public void setFailedTransactionsCount(Long failedTransactionsCount) {
        this.failedTransactionsCount = failedTransactionsCount;
    }

    public Double getFailedTransactionsAmountSum() {
        return failedTransactionsAmountSum;
    }

    public void setFailedTransactionsAmountSum(Double failedTransactionsAmountSum) {
        this.failedTransactionsAmountSum = failedTransactionsAmountSum;
    }

    public Set getReportRows() {
        return reportRows;
    }

    public void setReportRows(Set reportRows) {
        this.reportRows = reportRows;
    }

    public Serializable getId() {
        return getUmcReportId();
    }

    public int getValid() {
        return valid;
    }

    public void setValid(int valid) {
        this.valid = valid;
    }
}
