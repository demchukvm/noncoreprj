package com.tmx.web.forms.core.tools;

import com.tmx.web.base.BasicActionForm;
import com.tmx.web.controls.*;
import com.tmx.as.entities.general.user.User;
import com.tmx.as.entities.bill.voucher.BatchBuyVoucher;
import com.tmx.as.entities.bill.seller.Seller;
import com.tmx.as.entities.bill.processing.Processing;
import com.tmx.as.exceptions.DatabaseException;
import com.tmx.beng.csapi.java.base.BuyVoucherResp;
import com.tmx.beng.base.BillingMessage;

import java.util.List;
import java.util.LinkedList;

/**
 */
public class BatchBuyVouchersForm extends BasicActionForm {
    private EditBox ebBatchBuyVoucherId;
    //private EditBox ebVoucherNominal;
    private ScrollBox<String[]> sbVoucherNominal;    // !!!
    private EditBox ebVoucherCount;
    private EditBox ebLogin;
    private EditBox ebPassword;
    private EditBox ebTerminalSn;
    private EditBox ebVouchersObtained;
    private EditBox ebStatus;
    private EditBox ebSleepBetweenInterval;
    private ScrollBox<Seller> sbSellerCode;
    private ScrollBox<Processing> sbProcessingCode;
    private ScrollBox<String> sbServiceCode;
    private ScrollBox<String[]> sbOperationType;
    private TimeSelector tsTransactionTime;
    private Button btBuy;
    private Button btGetStatus;
    private Button btReset;
    private Button btDownload;
    private List<BuyVoucherResp> vouchers;


    protected void init() {
        initEditBoxes();
        initTimeSelectors();
        initButtons();
        initScrollBoxes();
        clean();
    }

    private void initButtons() {
        btBuy = new Button();
        btBuy.setLabelKey("ctrl.button.label.buy");
        btBuy.setAction("/core/tools/batchBuyVoucher");
        btBuy.setCommand("buy");

        btGetStatus = new Button();
        btGetStatus.setLabelKey("ctrl.button.label.getStatus");
        btGetStatus.setAction("/core/tools/batchBuyVoucher");
        btGetStatus.setCommand("getStatus");

        btReset = new Button();
        btReset.setLabelKey("ctrl.button.label.reset");
        btReset.setAction("/core/tools/batchBuyVoucher");
        btReset.setCommand("preloadForBuy");

        btDownload = new Button();
        btDownload.setLabelKey("ctrl.button.label.download");
        btDownload.setAction("/core/tools/batchBuyVoucher");
        btDownload.setCommand("download");
    }

    private void initTimeSelectors() {
        tsTransactionTime = new TimeSelector();
        tsTransactionTime.setReadonly(true);
    }

    private void initEditBoxes() {
        ebBatchBuyVoucherId = new EditBox();
        ebBatchBuyVoucherId.setReadonly(true);

//        ebVoucherNominal = new EditBox();
//        ebVoucherNominal.setMandatory(true);
//        ebVoucherNominal.addValidationRule(EditBox.createValidationRule_INTEGER_FORMAT(getLocale()));

        ebVoucherCount = new EditBox();
        ebVoucherCount.setMandatory(true);
        ebVoucherCount.addValidationRule(EditBox.createValidationRule_INTEGER_FORMAT(getLocale()));

        ebLogin = new EditBox();
        ebLogin.setMandatory(true);

        ebPassword = new EditBox();
        ebPassword.setMandatory(true);

        ebTerminalSn = new EditBox();
        ebTerminalSn.setMandatory(true);

        ebVouchersObtained = new EditBox();
        ebVouchersObtained.setReadonly(true);

        ebStatus = new EditBox();
        ebStatus.setReadonly(true);

        ebSleepBetweenInterval = new EditBox();
        ebSleepBetweenInterval.addValidationRule(EditBox.createValidationRule_INTEGER_FORMAT(getLocale()));
    }


    private void initScrollBoxes(){
        sbSellerCode = new ScrollBox<Seller>(){
            protected String getKey(Seller seller) {
                return seller.getCode();
            }

            protected String getValue(Seller seller) {
                return seller.getName();
            }
        };
        sbSellerCode.setNullable(true);
        sbSellerCode.setMandatory(true);

        sbProcessingCode = new ScrollBox<Processing>(){
            protected String getKey(Processing processing) {
                return processing.getCode();
            }

            protected String getValue(Processing processing) {
                return processing.getName();
            }
        };
        sbProcessingCode.setNullable(true);
        sbProcessingCode.setMandatory(true);

        sbServiceCode = new ScrollBox<String>(){
            protected String getKey(String serviceDescription) {
                return serviceDescription;
            }

            protected String getValue(String serviceDescription) {
                return serviceDescription;
            }
        };
        sbServiceCode.setNullable(true);
        sbServiceCode.setMandatory(true);

        sbOperationType = new ScrollBox<String[]>(){

            protected String getKey(String[] opearationType) {
                return opearationType[0];
            }

            protected String getValue(String[] opearationType) {
                return opearationType[1];
            }
        };
        sbOperationType.setNullable(false);

        // !!!
        sbVoucherNominal = new ScrollBox<String[]>(){
            protected String getKey(String[] voucherNominal) {
                return voucherNominal[0];
            }

            protected String getValue(String[] voucherNominal) {
                return voucherNominal[1];
            }
        };
        sbVoucherNominal.setNullable(true);
        sbVoucherNominal.setMandatory(true);
        
    }

    public void bindData(BatchBuyVoucher buyOperation,
                         List<Seller> sellers,
                         List<Processing> processings,
                         User loggedInUser) throws DatabaseException {

        if(buyOperation != null){
            ebBatchBuyVoucherId.setValue(buyOperation.getBatchBuyVoucherId().toString());
            tsTransactionTime.setTime(buyOperation.getTransactionTime());
            ebStatus.setValue(buyOperation.getStatus());
        }

        if(sellers != null)
            sbSellerCode.bind(sellers);

        if(processings != null)
            sbProcessingCode.bind(processings);

        final List<String> services = new LinkedList<String>();
        //services.add("Life");
        services.add("LifeExc");
        sbServiceCode.bind(services);

        final List<String[]> operationTypes = new LinkedList<String[]>();
        //operationTypes.add(new String[]{BillingMessage.O_REFILL_PAYMENT, "nPay"}); // for nPay life_gate
        operationTypes.add(new String[]{BillingMessage.O_GEN_VOUCHER, "Avancel"}); // for Avancel

        sbOperationType.bind(operationTypes);

        final List<String[]> nominals = new LinkedList<String[]>(); // !!!
        //nominals.add(new String[]{"5", "LifeVoucher5"});
        nominals.add(new String[]{"10", "LifeVoucher10"});
        nominals.add(new String[]{"25", "LifeVoucher25"});
        nominals.add(new String[]{"35", "LifeVoucher35"});
        nominals.add(new String[]{"50", "LifeVoucher50"});
        nominals.add(new String[]{"100", "LifeVoucher100"});
        sbVoucherNominal.bind(nominals);
    }

    public void clean(){
        ebBatchBuyVoucherId.setValue(null);
       // ebVoucherNominal.setValue(null);
        ebVoucherCount.setValue(null);
        ebLogin.setValue(null);
        ebPassword.setValue(null);
        ebTerminalSn.setValue(null);
        ebVouchersObtained.setValue("0");
        ebStatus.setValue("NEW");
        ebSleepBetweenInterval.setValue("0");
        sbSellerCode.reset();
        sbProcessingCode.reset();
        sbServiceCode.reset();
        sbVoucherNominal.reset(); // !!!
        sbOperationType.reset();
        tsTransactionTime.setTime(null);
        btBuy.setReadonly(false);
        btGetStatus.setReadonly(false);
        btReset.setReadonly(false);
        btDownload.setReadonly(true);
        vouchers = null;
    }

    public void setDefaults(){
        ebLogin.setValue("SVCard");
        ebPassword.setValue("T6%4+31@mZ");
        ebTerminalSn.setValue("GENLIFE_00");
        sbSellerCode.setSelectedKey("GenLife");
        sbProcessingCode.setSelectedKey("SVCard");
        //sbServiceCode.setSelectedKey("Life");
        sbServiceCode.setSelectedKey("LifeExc");
        sbVoucherNominal.setSelectedKey("LifeVoucher5"); // !!!
        sbOperationType.setSelectedKey("BUY_VOUCHER (Avancel avancel_gate_1)");
    }

    public BatchBuyVoucher  populateBatchBuyVoucher() {
        BatchBuyVoucher buyOperation = new BatchBuyVoucher();
        buyOperation.setBatchBuyVoucherId(PopulateUtil.getLongValue(ebBatchBuyVoucherId));
       // buyOperation.setVoucherNominal(PopulateUtil.getIntValue(ebVoucherNominal));
        buyOperation.setVoucherNominal(Integer.parseInt(sbVoucherNominal.getSelectedKey())); // !!!
        buyOperation.setVoucherCount(PopulateUtil.getIntValue(ebVoucherCount));
        buyOperation.setSellerCode(sbSellerCode.getSelectedKey());
        buyOperation.setTerminalSn(ebTerminalSn.getValue());
        buyOperation.setProcessingCode(sbProcessingCode.getSelectedKey());
        buyOperation.setServiceCode(sbServiceCode.getSelectedKey());
        buyOperation.setOperationType(sbOperationType.getSelectedKey());
        return buyOperation;        
    }

    public EditBox getEbBatchBuyVoucherId() {
        return ebBatchBuyVoucherId;
    }

    public void setEbBatchBuyVoucherId(EditBox ebBatchBuyVoucherId) {
        this.ebBatchBuyVoucherId = ebBatchBuyVoucherId;
    }

//    public EditBox getEbVoucherNominal() {
//        return ebVoucherNominal;
//    }
//
//    public void setEbVoucherNominal(EditBox ebVoucherNominal) {
//        this.ebVoucherNominal = ebVoucherNominal;
//    }


    public ScrollBox<String[]> getSbVoucherNominal() {
        return sbVoucherNominal;
    }

    public void setSbVoucherNominal(ScrollBox<String[]> sbVoucherNominal) {
        this.sbVoucherNominal = sbVoucherNominal;
    }

    public EditBox getEbVoucherCount() {
        return ebVoucherCount;
    }

    public void setEbVoucherCount(EditBox ebVoucherCount) {
        this.ebVoucherCount = ebVoucherCount;
    }

    public EditBox getEbLogin() {
        return ebLogin;
    }

    public void setEbLogin(EditBox ebLogin) {
        this.ebLogin = ebLogin;
    }

    public EditBox getEbPassword() {
        return ebPassword;
    }

    public void setEbPassword(EditBox ebPassword) {
        this.ebPassword = ebPassword;
    }

    public EditBox getEbTerminalSn() {
        return ebTerminalSn;
    }

    public void setEbTerminalSn(EditBox ebTerminalSn) {
        this.ebTerminalSn = ebTerminalSn;
    }

    public EditBox getEbVouchersObtained() {
        return ebVouchersObtained;
    }

    public void setEbVouchersObtained(EditBox ebVouchersObtained) {
        this.ebVouchersObtained = ebVouchersObtained;
    }

    public EditBox getEbStatus() {
        return ebStatus;
    }

    public void setEbStatus(EditBox ebStatus) {
        this.ebStatus = ebStatus;
    }

    public ScrollBox<Seller> getSbSellerCode() {
        return sbSellerCode;
    }

    public void setSbSellerCode(ScrollBox<Seller> sbSellerCode) {
        this.sbSellerCode = sbSellerCode;
    }

    public ScrollBox<Processing> getSbProcessingCode() {
        return sbProcessingCode;
    }

    public void setSbProcessingCode(ScrollBox<Processing> sbProcessingCode) {
        this.sbProcessingCode = sbProcessingCode;
    }

    public ScrollBox<String> getSbServiceCode() {
        return sbServiceCode;
    }

    public void setSbServiceCode(ScrollBox<String> sbServiceCode) {
        this.sbServiceCode = sbServiceCode;
    }

    public ScrollBox<String[]> getSbOperationType() {
        return sbOperationType;
    }

    public void setSbOperationType(ScrollBox<String[]> sbOperationType) {
        this.sbOperationType = sbOperationType;
    }

    public TimeSelector getTsTransactionTime() {
        return tsTransactionTime;
    }

    public void setTsTransactionTime(TimeSelector tsTransactionTime) {
        this.tsTransactionTime = tsTransactionTime;
    }

    public Button getBtBuy() {
        return btBuy;
    }

    public void setBtBuy(Button btBuy) {
        this.btBuy = btBuy;
    }

    public Button getBtGetStatus() {
        return btGetStatus;
    }

    public void setBtGetStatus(Button btGetStatus) {
        this.btGetStatus = btGetStatus;
    }

    public Button getBtReset() {
        return btReset;
    }

    public void setBtReset(Button btReset) {
        this.btReset = btReset;
    }

    public Button getBtDownload() {
        return btDownload;
    }

    public void setBtDownload(Button btDownload) {
        this.btDownload = btDownload;
    }

    public EditBox getEbSleepBetweenInterval() {
        return ebSleepBetweenInterval;
    }

    public void setEbSleepBetweenInterval(EditBox ebSleepBetweenInterval) {
        this.ebSleepBetweenInterval = ebSleepBetweenInterval;
    }

    public List<BuyVoucherResp> getVouchers() {
        return vouchers;
    }

    public void setVouchers(List<BuyVoucherResp> vouchers) {
        this.vouchers = vouchers;
    }
}
