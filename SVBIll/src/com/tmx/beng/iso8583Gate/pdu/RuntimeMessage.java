package com.tmx.beng.iso8583Gate.pdu;

import com.tmx.beng.iso8583Gate.pdu.util.HexConverter;
import com.tmx.beng.iso8583Gate.ISO8583Service;

import java.util.*;
import java.io.PipedOutputStream;
import java.io.PipedInputStream;
import java.io.IOException;
import java.io.ByteArrayOutputStream;

/**
 */
public class RuntimeMessage {
    private MessageCommonFormat messageCommonFormat;
    private RuntimeHeader runtimeHeader;
    private Map runtimeDataElements;

    public RuntimeMessage(MessageCommonFormat messageCommonFormat){
        this.messageCommonFormat = messageCommonFormat;
        this.runtimeDataElements = new HashMap();
    }

    /** add or replace RuntimeDataElement with existed name */
    public void addRuntimeDataElement(RuntimeDataElement rde){
        runtimeDataElements.put(rde.getFormat().getName(), rde);
        rde.setMessage(this);
        updateBitmap(rde);
    }

    public void copyRuntimeDataElement(RuntimeMessage fromRM, int bitnum){
        RuntimeDataElement copiedRDE = fromRM.getRuntimeDataElement(bitnum);
        if(copiedRDE != null)
            addRuntimeDataElement(copiedRDE);
    }

    public RuntimeDataElement getRuntimeDataElement(String name){
        return (RuntimeDataElement)runtimeDataElements.get(name);
    }

    public RuntimeDataElement getRuntimeDataElement(int bitmapNum){
        DataElementFormat def = messageCommonFormat.getDataElementFormat(bitmapNum);
        return (RuntimeDataElement)runtimeDataElements.get(def.getName());
    }

    /** Returns ordered by position in bitnumber list of data elements */
    public List getRuntimeDataElements(){
        List rdes = new ArrayList();
        for(Iterator iter = messageCommonFormat.getDataElementFormats().iterator(); iter.hasNext();){
            RuntimeDataElement rde = getRuntimeDataElement(((DataElementFormat)iter.next()).getName());
            if(rde != null)
                rdes.add(rde);
        }
        return rdes;
    }

    public RuntimeHeader getRuntimeHeader() {
        return runtimeHeader;
    }

    public void setRuntimeHeader(RuntimeHeader runtimeHeader) {
        this.runtimeHeader = runtimeHeader;
    }

    public MessageCommonFormat getMessageCommonFormat() {
        return messageCommonFormat;
    }

    public MessageParticularFormat getMessageParticularFormat() {
        RuntimeDataElement messageTypeRDE = getRuntimeDataElement(DataElementFormat.DEF_MESSAGETYPEID);
        RuntimeDataElement processingCodeRDE = getRuntimeDataElement(DataElementFormat.DEF_PROCESSINGCODE);
        MessageParticularFormat mpf = ISO8583Service.getInstance().getProtocolMetadata().
                getProtocolVersion(messageCommonFormat.getProtocolVersion()).getMessageParticularFormat(
                messageTypeRDE != null ? messageTypeRDE.getString() : null,
                processingCodeRDE != null ? processingCodeRDE.getString() : null
            );

        if(mpf != null)
            return mpf;

        //MessageParticularFormat identified by messageTypeId only
        mpf = ISO8583Service.getInstance().getProtocolMetadata().
                getProtocolVersion(messageCommonFormat.getProtocolVersion()).getMessageParticularFormat(
                messageTypeRDE != null ? messageTypeRDE.getString() : null,
                null
            );

        return mpf;
    }


//    public byte[] calculateBitmap(){
//        byte[] bitmap = new byte[8];
//        Arrays.fill(bitmap, (byte)0x00);
//        List rdes = getRuntimeDataElements();
//        for(int i=0; i<rdes.size(); i++){
//            RuntimeDataElement rde = (RuntimeDataElement)rdes.get(i);
//            int bitmupNumber = rde.getFormat().getBitmapNumber();
//            if(bitmupNumber < 2)
//                continue;//ignore elements not covered by bitmap
//
//            bitmupNumber--; //bitmap in ISO starts with 1 not with 0 index
//            int byteNum = bitmupNumber / 8;
//            int bitNum = bitmupNumber % 8;
//            byte bit = (byte)(0x01 << (7 - bitNum));
//            if(byteNum < bitmap.length)
//                bitmap[byteNum] = (byte)(bitmap[byteNum] | bit);
//        }
//        return bitmap;
//    }

    /** Update bitmap on adding given runtime data element into message. */
    private void updateBitmap(RuntimeDataElement rde) {
        RuntimeDataElement bitmapRDE = getRuntimeDataElement(DataElementFormat.DEF_BITMAP);
        if (bitmapRDE == null) {
            bitmapRDE = new RuntimeDataElement(getMessageCommonFormat().getDataElementFormat(DataElementFormat.DEF_BITMAP));
            bitmapRDE.setValue(new byte[8]);
            addRuntimeDataElement(bitmapRDE);
        }
        byte[] bitmap = null;
        try{
            bitmap = bitmapRDE.getBinary();
        }
        catch(IncorrectFormatException e){
            throw new RuntimeException("Bitmap should be binary");
        }
        int bitmupNumber = rde.getFormat().getBitmapNumber();
        if (bitmupNumber < 2)
            return;//ignore elements not covered by bitmap

        bitmupNumber--; //bitmap in ISO starts with 1 not with 0 index
        int byteNum = bitmupNumber / 8;
        int bitNum = bitmupNumber % 8;
        byte bit = (byte) (0x01 << (7 - bitNum));
        if (byteNum < bitmap.length)
            bitmap[byteNum] = (byte) (bitmap[byteNum] | bit);

        bitmapRDE.setValue(bitmap);
    }
    
    /** for debug */
    public String dumpMessage(){
        StringBuffer msgDump = new StringBuffer();
        msgDump.append("---ISO8583 MESSAGE DUMP---\n")
                .append("\nHEADER\n");

        if(getRuntimeHeader() != null)
            msgDump.append(getRuntimeHeader().dumpHeader());

        msgDump.append("\nAPPLICATION DATA\n");

        RuntimeDataElement messageTypeRDE = getRuntimeDataElement(DataElementFormat.DEF_MESSAGETYPEID);
        msgDump.append(dumpRuntimeDataElement(messageTypeRDE));

        RuntimeDataElement bitMapRDE = getRuntimeDataElement(DataElementFormat.DEF_BITMAP);
        msgDump.append(dumpRuntimeDataElement(bitMapRDE));        
        

        for(Iterator iter = getRuntimeDataElements().iterator();iter.hasNext();){
            RuntimeDataElement nextRDE = (RuntimeDataElement)iter.next();
            if(nextRDE.getFormat().getBitmapNumber() > 0) //get only elements with bitmap number
                msgDump.append(dumpRuntimeDataElement(nextRDE));
        }
        return msgDump.toString();
    }

    private String dumpRuntimeDataElement(RuntimeDataElement rde) {
        StringBuffer rdeDump = new StringBuffer();
        DataElementFormat def = rde.getFormat();
        String serializedRde;
        try{
            serializedRde = new HexConverter().getStringFromHex(def.serialize(rde));
        }
        catch(IsoSerializeException e){
            serializedRde = "(ERROR: "+e.toString()+")";
        }
        rdeDump.append(def.getBitmapNumber()).append(" : ");
        rdeDump.append(def.getName()).append(" : ");
        rdeDump.append("[val=")
                .append(rde.getString()).append("]; [isoVal=")
                .append(serializedRde).append("]; [")
                .append(def.getType().getName()).append(", ")
                .append(def.getLengthType().getName()).append(", len=")
                .append(def.getLength()).append(", maxLen=")
                .append(def.getMaxLength()).append("]")
                .append("]\n");
        return rdeDump.toString();
    }

    public String dumpHexMessage(){
        StringBuffer msgDump = new StringBuffer();
        msgDump.append(getRuntimeHeader().dumpHexHeader());

        RuntimeDataElement messageTypeRDE = getRuntimeDataElement(DataElementFormat.DEF_MESSAGETYPEID);
        msgDump.append(dumpHexRuntimeDataElement(messageTypeRDE));

        RuntimeDataElement bitMapRDE = getRuntimeDataElement(DataElementFormat.DEF_BITMAP);
        msgDump.append(dumpHexRuntimeDataElement(bitMapRDE));


        for(Iterator iter = getRuntimeDataElements().iterator();iter.hasNext();){
            RuntimeDataElement nextRDE = (RuntimeDataElement)iter.next();
            if(nextRDE.getFormat().getBitmapNumber() > 0) //get only elements with bitmap number
                msgDump.append(dumpHexRuntimeDataElement(nextRDE));
        }
        return msgDump.toString();
    }

    private String dumpHexRuntimeDataElement(RuntimeDataElement rde){
        DataElementFormat def = rde.getFormat();
        String serializedRde;
        try{
            serializedRde = new HexConverter().getStringFromHex(def.serialize(rde));
        }
        catch(IsoSerializeException e){
            serializedRde = "(ERROR: "+e.toString()+")";
        }
        return serializedRde;        
    }

    public Map getRuntimeDataElementsNative() {
        return runtimeDataElements;
    }
}
