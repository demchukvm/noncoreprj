package com.tmx.gate.datastream.base;

import com.tmx.util.StructurizedException;

import java.util.Locale;

/** DataStreamException */
public class DataEncodingException extends StructurizedException {
    public DataEncodingException(String errKey){
        super(errKey);
    }

    public DataEncodingException(String errKey, Locale locale){
        super(errKey, locale);
    }

    public DataEncodingException(String errKey, String[] params){
        super(errKey, params);
    }

    public DataEncodingException(String errKey, String[] params, Throwable cause){
        super(errKey, params, cause);
    }

    public DataEncodingException(String errKey, String[] params, Locale locale){
        super(errKey, params, locale);
    }

    public DataEncodingException(String errKey, Throwable cause){
        super(errKey, cause);
    }
}
