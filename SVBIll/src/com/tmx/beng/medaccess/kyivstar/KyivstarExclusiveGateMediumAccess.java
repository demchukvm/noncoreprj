package com.tmx.beng.medaccess.kyivstar;

import com.tmx.beng.medaccess.MediumAccess;
import com.tmx.beng.medaccess.MediumConnection;
import com.tmx.beng.medaccess.GateException;

/**
  Medium Access to obtain Mobipay Kyivstar Exclusive medium connection
 */
public class KyivstarExclusiveGateMediumAccess implements MediumAccess {

    public void init() {
    }

    public MediumConnection getConnection() throws GateException {
        return new KyivstarExclusiveGateMediumConnection();
    }
}
