package com.tmx.beng.medaccess.avancel;

import com.tmx.beng.httpsgate.HttpsGateException;
import com.tmx.beng.httpsgate.avancel.AvancelGate3;
//import com.tmx.beng.httpsgate.avancel.AvancelGate;
import com.tmx.beng.base.BillingMessage;
import com.tmx.beng.medaccess.MediumConnection;

/**
 */
public class AvancelGateMediumConnection3 implements MediumConnection {
    private AvancelGate3 avancelGate = null;

    AvancelGateMediumConnection3() throws HttpsGateException {
        avancelGate = AvancelGate3.getInstance();
    }

    public void send(BillingMessage msg) {
        avancelGate.enqueueBillMessage(msg);
    }
}
