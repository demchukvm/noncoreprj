package com.tmx.as.entities.bill.trade_putlet;

import com.tmx.as.base.BasicEntity;
import com.tmx.as.entities.bill.address.Address;
import com.tmx.as.entities.bill.person.Person;

import java.io.Serializable;


public class TradePutlet extends BasicEntity {
    private Long tradePutletId;
    private String name;
    private String description;
    private Address address;
    private Person director;
    private Person manager;
    private TradePutletType type;

    public Serializable getId() {
        return tradePutletId;
    }

    public int getValid() {
        return valid;
    }

    public void setValid(int valid) {
        this.valid = valid;
    }

    public Long getTradePutletId() {
        return tradePutletId;
    }

    public void setTradePutletId(Long tradePutletId) {
        this.tradePutletId = tradePutletId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public TradePutletType getType() {
        return type;
    }

    public void setType(TradePutletType type) {
        this.type = type;
    }

    public Person getDirector() {
        return director;
    }

    public void setDirector(Person director) {
        this.director = director;
    }

    public Person getManager() {
        return manager;
    }

    public void setManager(Person manager) {
        this.manager = manager;
    }
}
