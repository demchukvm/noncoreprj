package com.tmx.web.taglib.html.ctrl;

import com.tmx.web.controls.grid.Grid;
import com.tmx.web.controls.grid.Cell;
import com.tmx.web.controls.grid.CellLink;

import javax.servlet.jsp.JspException;

import org.apache.struts.taglib.TagUtils;

public class GridTag extends AbstractControlTag{
    protected final int controlStartTag() throws JspException{
        Grid grid=getGrid();
        Cell[][] cells= grid.getCells();
        StringBuffer resultHTML = new StringBuffer();
        resultHTML.append("<table class='one_outer_cell_table' cellspacing='1' cellpadding='1' width=\"100%\">");
        for (int i = 0; i < cells.length; i++) {
            Cell[] colArr = cells[i];
            if(colArr==null)continue;
            resultHTML.append("<tr>");
            for (int j = 0; j < colArr.length; j++) {
                Cell cell = colArr[j];
                if(cell==null||cell instanceof CellLink)continue;
                String value=(cell.getValue()==null)?"":cell.getValue();
                String styleClass=cell.getStyleClass()==null?"":" class=\""+cell.getStyleClass()+"\"";
                String style=cell.getStyle()==null?"":" style=\""+cell.getStyle()+"\"";
                String colSpan=" colSpan="+cell.getColSpan();
                String rowSpan=" rowSpan="+cell.getRowSpan();
                resultHTML.append("<td"+styleClass+style+colSpan+rowSpan+">");
                resultHTML.append(value);
                resultHTML.append("</td>");
            }
            resultHTML.append("</tr>");
        }
        resultHTML.append("</table>");
        TagUtils.getInstance().write(pageContext,resultHTML.toString());
        return SKIP_BODY;
    }
    private Grid getGrid() throws JspException {
        Object object = getPropertyObject();
        return (object != null && object instanceof Grid) ?
               (Grid)object : null;
    }
    
}
