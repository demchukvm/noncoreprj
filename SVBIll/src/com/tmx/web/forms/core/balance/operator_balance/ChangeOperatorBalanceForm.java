package com.tmx.web.forms.core.balance.operator_balance;

import com.tmx.web.forms.core.balance.AbstractChangeBalanceForm;
import com.tmx.web.controls.EditBox;
import com.tmx.as.entities.bill.balance.OperatorBalance;
import com.tmx.as.entities.bill.balance.AbstractBalance;


public class ChangeOperatorBalanceForm extends AbstractChangeBalanceForm {

    protected void init() {
        super.init();

        getOnCancelButton().setAction("/core/balance/operator_balance/infoOperatorBalance");
        getOnChangeButton().setAction("/core/balance/operator_balance/changeOperatorBalance");
    }

    public void bindData(AbstractBalance abstractBalance) {
        super.bindData(abstractBalance);
        OperatorBalance operatorBalance = (OperatorBalance) abstractBalance;
        editBoxOwnerName.setValue(operatorBalance.getOperator().getName());
    }
}
