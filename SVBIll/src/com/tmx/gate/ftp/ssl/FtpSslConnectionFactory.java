package com.tmx.gate.ftp.ssl;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.security.KeyStore;
import java.security.SecureRandom;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;
import com.sun.net.ssl.*;
import org.apache.log4j.*;

/** FTP SSL Connection factory  */
public class FtpSslConnectionFactory {
  /** Logger */
  private Logger logger = null;

  /** Default constructor */
  public FtpSslConnectionFactory(){
    logger = Logger.getLogger("ssl.FtpSslConnectionFactory");
  }


  public SSLSocket getSslSocket(String ip, int port, String password){
    logger.info("Started to set connection to IP='"+ip+"' PORT='"+port+"'");
    char[] passwd = password.toCharArray();
    System.setProperty("javax.net.debug", "all");
    SecureRandom secureRandom = null;
    PrintWriter toServer = null;
    BufferedReader fromServer = null;

    try {
      RandomGenerator generator = new RandomGenerator();
      generator.start();
      KeyStore serverKeyStore = KeyStore.getInstance("JKS");
      serverKeyStore.load(null, passwd);
      KeyStore clientKeyStore = KeyStore.getInstance("JKS");
      clientKeyStore.load(null, passwd);
      TrustManagerFactory tmf = TrustManagerFactory.getInstance("SunX509", "SunJSSE");
      tmf.init(serverKeyStore);
      TrustManager[] tm = (new TrustManager[] { new TrustManagerImpl()});
      KeyManagerFactory kmf = KeyManagerFactory.getInstance("SunX509");
      kmf.init(clientKeyStore, passwd);
      secureRandom = generator.getSecureRandom();
      SSLContext sslContext = SSLContext.getInstance("TLS", "SunJSSE");
      sslContext.init(kmf.getKeyManagers(), tm, secureRandom);
      SSLSocketFactory factory = sslContext.getSocketFactory();

      Socket tmpSocket = new Socket(ip, port);
      logger.info("Successfully set connection with server");      
      toServer = new PrintWriter(new BufferedWriter(new OutputStreamWriter(tmpSocket.getOutputStream())), true);
      fromServer = new BufferedReader(new InputStreamReader(tmpSocket.getInputStream()));

      toServer.println("AUTH TLS");
      toServer.flush();
      //Wait for server answer
      Thread.sleep(10000);
      String s;
      while((s = fromServer.readLine()).equals("")){}
      logger.info("Successfully move server to AUTH SSL mode");

      SSLSocket cmdSocket = (SSLSocket) factory.createSocket(tmpSocket, tmpSocket.getInetAddress().getHostName(), tmpSocket.getPort(), true);
      cmdSocket.startHandshake();
      logger.info("Successfully set encrypted connection with server");
      
      return cmdSocket;
   }
    catch (Exception e){
      logger.error("Failed to set encrypted connection with server, becouse: "+e.toString());
    }
    return null;
  }



  public Socket getSslDataSocket(String ip, int port, String password){
    logger.info("Started to set encrypted data connection to IP='"+ip+"' PORT='"+port+"'");
    char[] passwd = password.toCharArray();
    System.setProperty("javax.net.debug", "all");
    SecureRandom secureRandom = null;
    PrintWriter toServer = null;
    BufferedReader fromServer = null;
    try {
      RandomGenerator generator = new RandomGenerator();
      generator.start();
      KeyStore serverKeyStore = KeyStore.getInstance("JKS");
      serverKeyStore.load(null, passwd);
      KeyStore clientKeyStore = KeyStore.getInstance("JKS");
      clientKeyStore.load(null, passwd);
      TrustManagerFactory tmf = TrustManagerFactory.getInstance("SunX509", "SunJSSE");
      tmf.init(serverKeyStore);
      TrustManager[] tm = (new TrustManager[] { new TrustManagerImpl()});
      KeyManagerFactory kmf = KeyManagerFactory.getInstance("SunX509");
      kmf.init(clientKeyStore, passwd);
      secureRandom = generator.getSecureRandom();
      SSLContext sslContext = SSLContext.getInstance("TLS", "SunJSSE");
      sslContext.init(kmf.getKeyManagers(), tm, secureRandom);
      SSLSocketFactory factory = sslContext.getSocketFactory();

      SSLSocket socket = (SSLSocket)factory.createSocket(ip, port);
      logger.info("Successfully set encrypted data connection");
      return socket;
    }
    catch (Exception e){
      logger.error("Failed to set data connection, becouse: "+e.toString());
    }
    return null;
  }


  
}