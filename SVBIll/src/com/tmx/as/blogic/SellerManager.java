package com.tmx.as.blogic;

import com.tmx.as.base.FilterWrapper;
import com.tmx.as.base.QueryParameterWrapper;
import com.tmx.as.entities.bill.seller.Seller;
import com.tmx.as.entities.bill.balance.SellerBalance;
import com.tmx.as.entities.bill.bank.BankAccount;
import com.tmx.as.base.EntityManager;
import com.tmx.as.base.OrderWrapper;
import com.tmx.as.base.criterion.CriterionWrapper;
import com.tmx.as.base.criterion.Restrictions;
import com.tmx.as.entities.bill.contract.Contract;
import com.tmx.as.entities.general.user.User;
import com.tmx.as.exceptions.DatabaseException;

import java.util.List;
import java.util.Date;
import java.util.ArrayList;
import java.util.Collections;

public class SellerManager extends EntityManager {

    public static final String PRIMARY_BALANCE_DESCRIPTION = "Primary balance";
    public static final String PRIMARY_BALANCE_PREFIX = "_primary_balance";
    public static final String PRIMARY_BALANCE_NAME = "Primary balance";

    private final String[] fullSellerAttrs = new String[]{
                                                "name", "description", "code",
                                                "physicalAddress.city",
                                                "physicalAddress.district",
                                                "physicalAddress.region",
                                                "legalAddress.district",
                                                "legalAddress.region",
                                                "legalAddress.city",
                                                /*"primaryBalance",*/ "defaultTerminalGroup",
                                                "defaultTerminalGroupId", "processing",
                                                "processingId", "active", "certificateNumber",
                                                "inn","okpo", "director", "accountant",
                                                "phone0","phone1",
                                                "mobPhone0", "mobPhone1",
                                                "mail0", "mail1", "mail2"};

    private final String[] fullSellerAttrsUpdate = new String[]{
                                                "name", "description",
                                                "code",
                                                "physicalAddress",
                                                "legalAddress",
                                                "defaultTerminalGroup",
                                                "defaultTerminalGroupId",
                                                "processing",
                                                "processingId",
                                                "active",
                                                "certificateNumber",
                                                "inn","okpo",
                                                "director",
                                                "accountant",
                                                "phone0","phone1",
                                                "mobPhone0", "mobPhone1",
                                                "mail0", "mail1", "mail2"};

    private final String[] sellerAttrs = new String[]{
                                                "physicalAddress.city",
                                                "physicalAddress.district",
                                                "physicalAddress.region",
                                                "legalAddress.district",
                                                "legalAddress.region",
                                                "legalAddress.city",
                                                "primaryBalance",
                                                "parentSeller",
                                                "defaultTerminalGroup",
                                                "processing",
                                                "director", "accountant"};

    public List<Seller> getAllSellers(String[] attr) throws DatabaseException {
        return RETRIEVE_ALL(Seller.class, null, null, attr, 0, 0);
    }
    public List<Seller> getAllSellers() throws DatabaseException {
        return RETRIEVE_ALL(Seller.class, null, new OrderWrapper[]{new OrderWrapper("name", OrderWrapper.ASC)});
    }

    public Long getSellerId(String sellerCode) throws DatabaseException
    {
        CriterionWrapper [] criterions = new CriterionWrapper[]{Restrictions.eq("code", sellerCode)};
        Seller seller = (Seller) new EntityManager().RETRIEVE(Seller.class, null, criterions, null);
        return seller.getSellerId();
    }

    /** Get sellers belonged directly to given parent seller.
     * @param parentSellerId id of parent seller
     * @return list of retrieved child sellers
     * @throws DatabaseException databse exception */
    public List getSellersInSeller(Long parentSellerId) throws DatabaseException {
        

        return getSellersInSeller(parentSellerId, false);
    }
    /** Get sellers belonged directly to given parent seller + parent seller.
     * @param parentSellerId id of parent seller
     * @return list of retrieved child sellers
     * @throws DatabaseException databse exception */
    public List getSellersInSeller(Long parentSellerId, boolean include) throws DatabaseException {
        List sellers;
        FilterWrapper byParentFW = new FilterWrapper("by_parent");
        byParentFW.setParameter("parent_id", parentSellerId);
        sellers = RETRIEVE_ALL(Seller.class, new FilterWrapper[]{byParentFW});
        if (include) {
            sellers.add(RETRIEVE(Seller.class, parentSellerId));
        }
        return sellers;
    }

    /** Get query to retrieve sellers belonged directly to given parent seller.
     * @param parentSellerId id of parent seller
     * @return query string */
    public String getSellersInSellerQuery(Long parentSellerId) {
        return "query(query_type=retrieve_all," +
                "entity_class=com.tmx.as.entities.bill.seller.Seller," +
                "order(name=sellerId,type=asc)," +
                "table_attr=parentSeller," +
                "table_attr=physicalAddress," +
                "table_attr=legalAddress," +
                "filter(name=by_parent,parameter(name=parent_id,value=" + (parentSellerId != null ? parentSellerId : 0) + ",type=long))," +
                "page_size=15" +
                ")";
    }

    public String getNestedSellersInSellerQuery(Long parentSellerId) {
        return "query(query_type=execute_query," +
                "entity_class=com.tmx.as.entities.bill.seller.Seller," +
                "query_name=retrieveNestedSellersInSeller," +
                "query_parameter(name=exceptSellerId, value=" + parentSellerId + ", type=long)," +
                "table_attr=parentSeller," +
                "table_attr=physicalAddress," +
                "table_attr=legalAddress.city," +
                "page_size=10" +
                ")";
    }


    public Seller getNestedSellerForSeller(Seller seller) throws DatabaseException {
        CriterionWrapper [] criterion = new CriterionWrapper[] {Restrictions.eq("sellerId", seller.getTreeId())};
        return (Seller)RETRIEVE(Seller.class, null, criterion, new String[]{"primaryBalance"});
    }
    /*retrive root seller for given seller (above given seller)*/
    public Seller getNestedSellerForSeller(Long sellerId) throws DatabaseException {
        return getNestedSellerForSeller(getSeller(sellerId));
    }
    /*retrive root seller for given seller (above given seller)*/
    public List<Seller> getNestedSellersInSeller(String parentSellerId, boolean include) throws DatabaseException {
        QueryParameterWrapper parentSellerIdQPW = new QueryParameterWrapper("parentSellerId", Long.parseLong(parentSellerId));
        OrderWrapper[] orderWrapper = new OrderWrapper[] { new OrderWrapper("code", OrderWrapper.ASC)};
        List<Seller> result = EXECUTE_QUERY(Seller.class, "retrieveNestedSellersInSeller", new QueryParameterWrapper[]{parentSellerIdQPW});
        if (include)
            result.add(getSeller(Long.parseLong(parentSellerId)));
        return result;
    }

    /** Get all nested sellers under given parent seller.
     * @param parentSellerId id of parent seller
     * @param include
     * @return list of retrieved child sellers
     * @throws DatabaseException databse exception */
    public List<Seller> getNestedSellersInSeller(Long parentSellerId, boolean include) throws DatabaseException {
        QueryParameterWrapper parentSellerIdQPW = new QueryParameterWrapper("parentSellerId", parentSellerId);
        List<Seller> result = EXECUTE_QUERY(Seller.class, "retrieveNestedSellersInSeller", new QueryParameterWrapper[]{parentSellerIdQPW});
        if (include)
            result.add(getSeller(parentSellerId));
        return result;
    }

    //return genestedSellersInSeller + parentSeller + balance
    public List<Seller> getNestedSellersInSeller(Seller parentSeller, boolean include, boolean withBalances) throws DatabaseException {
        QueryParameterWrapper parentSellerIdQPW = new QueryParameterWrapper("parentSellerId", parentSeller.getSellerId());
        List<Seller> result = EXECUTE_QUERY( Seller.class, "retrieveNestedSellersInSeller", new QueryParameterWrapper[]{parentSellerIdQPW});
        if (include) {
            result.add(parentSeller);                                                    
        }

        Collections.sort(result);

        if (!withBalances) {
            return result;
        } else {

            final List<Long> sellerIds = new ArrayList<Long>();
            for (Seller seller : result) {
                sellerIds.add(seller.getSellerId());
            }
            CriterionWrapper [] criterions = new CriterionWrapper [] {Restrictions.in( "sellerId",sellerIds)};
            String [] attr = {"primaryBalance"};
            //terive sellers with balances
            return RETRIEVE_ALL(Seller.class, criterions, attr);
        }
    }
    //return getestedSellersInSeller + parentSeller
    public List<Seller> getNestedSellersInSeller(Seller parentSeller, boolean include) throws DatabaseException {
        return getNestedSellersInSeller(parentSeller, include, false);
    }

    public List getNestedSellersInSeller(User user) throws DatabaseException {
        if("SUPERUSER".equals(user.getRole().getRoleName())){//TODO: remove hard code
            return getAllSellers();   
        }
        return getNestedSellersInSeller(user.getSellerOwner(), true);
    }

    public Seller getSeller(Long sellerId) throws DatabaseException {
        return (Seller) RETRIEVE(Seller.class, sellerId, sellerAttrs);
    }

    public Seller getSellerOnlyPrimaryBalance(Long sellerId) throws DatabaseException {
        String[] sellerAttrs = new String[]{"primaryBalance"};
        return (Seller) RETRIEVE(Seller.class, sellerId, sellerAttrs);
    }

    public List getBankAcount(Long sellerId) throws DatabaseException {
        FilterWrapper wrapper = new FilterWrapper("by_seller_id");
        wrapper.setParameter("seller_id", sellerId);
        FilterWrapper[] wrappers = new FilterWrapper[]{wrapper};
        return RETRIEVE_ALL(BankAccount.class, wrappers , new String[]{"bank"} );
    }
    
    public Contract getContract(Long sellerId) throws DatabaseException {
        FilterWrapper wrapper = new FilterWrapper("by_seller_id");
        wrapper.setParameter("seller_id", sellerId);
        List list = RETRIEVE_ALL( Contract.class, new FilterWrapper[]{wrapper} );
        return (Contract) list.get(0);
    }

    public List getRootSellers() throws DatabaseException{
        FilterWrapper byRootSellerFW = new FilterWrapper("by_rootSeller");

        return RETRIEVE_ALL(Seller.class, new FilterWrapper[]{byRootSellerFW});
    }

    /** Get query to retrieve all sellers except given one.
     * @param exceptSellerId retrive sellers except this one
     * @return query string
     * */
    public String getSellersExceptCurrentQuery(Long exceptSellerId){
        return "query(query_type=execute_query," +
                "entity_class=com.tmx.as.entities.bill.seller.Seller," +
                "query_name=retrieveSellersExceptCurrent," +
                "query_parameter(name=exceptSellerId, value=" + exceptSellerId + ", type=long)," +
                "page_size=10" +
                ")";
    }

    public void deleteSeller(Seller deletedSeller) throws DatabaseException{
        //delete unexpected node
        if(deletedSeller.getLeftMargin() == null || deletedSeller.getRightMargin() == null || deletedSeller.getTreeId() == null)
            throw new DatabaseException("err.kiosk_manager.group_not_in_tree", new String[]{deletedSeller.getSellerId() != null ? deletedSeller.getSellerId().toString() : null});

        try{
            BEGIN(deletedSeller);
            //delete nested groupmembers
            QueryParameterWrapper dropLeftQPW = new QueryParameterWrapper("dropLeft", deletedSeller.getLeftMargin());
            QueryParameterWrapper dropRightQPW = new QueryParameterWrapper("dropRight", deletedSeller.getRightMargin());
            QueryParameterWrapper currTreeIdQPW = new QueryParameterWrapper("currentTreeId", deletedSeller.getTreeId());
            QueryParameterWrapper[] qpw = new QueryParameterWrapper[]{dropLeftQPW, dropRightQPW, currTreeIdQPW};

            EXECUTE_QUERY_UPDATE(Seller.class, "deleteNestedSellers", qpw);
            Integer marginWidth = deletedSeller.getRightMargin().intValue() - deletedSeller.getLeftMargin().intValue() + 1;
            QueryParameterWrapper marginWidthQPW = new QueryParameterWrapper("marginWidth", marginWidth);
            qpw = new QueryParameterWrapper[]{dropLeftQPW, currTreeIdQPW, marginWidthQPW};
            EXECUTE_QUERY_UPDATE(Seller.class, "rebuiltMarginsAfterSubTreeDelete", qpw);

            COMMIT(deletedSeller);
        }
        catch(DatabaseException e){
            ROLLBACK(deletedSeller);
            throw new DatabaseException("err.sellermanager.failed_to_delete_seller", e);
        }
    }

    public void deleteSeller(Long deletedSellerId) throws DatabaseException{
        Seller deletedSeller = (Seller)RETRIEVE(Seller.class, deletedSellerId, null);
        if(deletedSeller != null)
            deleteSeller(deletedSeller);
    }

    /**
     * Save seller with primary balance
     * @param savedSeller
     * @param primaryAmount
     * @throws BlogicException
     * @throws DatabaseException
     */
    public void saveSeller(Seller savedSeller, long primaryAmount) throws BlogicException, DatabaseException {
        saveSeller(savedSeller, new Long(primaryAmount));
    }

    /**
     * save seller with correct Sellers Tree
     * @param savedSeller
     * @param primaryAmount
     * @throws BlogicException
     * @throws DatabaseException
     */
    public void saveSeller(Seller savedSeller, Long primaryAmount) throws BlogicException, DatabaseException {
        saveSeller(savedSeller, createPrimaryBalance(savedSeller, primaryAmount));
    }

    //you cannot save seller if it already exist
    //with correct Sellers tree
    public void saveSeller(Seller savedSeller, SellerBalance primaryBalance) throws BlogicException, DatabaseException {
        if ( !primaryBalance.isTransient() )//if this primaryBalance is assigned to another seller
            throw new BlogicException("err.sellermanager.balance_is_not_transient");

        if( !savedSeller.isTransient() ){//if seller with primary balance is already exist
            saveSeller(savedSeller);
            return;
        }

        try {
            BEGIN(savedSeller);
                createSeller0(savedSeller);
                primaryBalance.setSeller(savedSeller);
                SAVE(primaryBalance);
                savedSeller.setPrimaryBalance(primaryBalance);
                SAVE(savedSeller, new String[]{"sellerBalance"});
            COMMIT(savedSeller);
        } catch (DatabaseException e) {
            ROLLBACK(savedSeller);
            throw new DatabaseException("err.sellermanager.seller_creation_failed", e);
        }
    }

    /**
     * save seller without primary balance
     * @param savedSeller
     * @throws DatabaseException
     */
    public void saveSeller(Seller savedSeller) throws DatabaseException {
        if(savedSeller.isTransient()){
            try {
                BEGIN(savedSeller);
                createSeller0(savedSeller);
                COMMIT(savedSeller);
            } catch (DatabaseException e) {
                ROLLBACK(savedSeller);
                throw new DatabaseException("err.sellermanager.seller_creation_failed", e);    
            }
        } else
            updateSeller0(savedSeller);
    }
    //* save seller with correct Sellers Tree */      
    public Seller saveSeller(BankAccount account, Contract contract) throws DatabaseException, BlogicException {
        Seller seller = account.getSeller();
        try {
            BEGIN(account);
            SAVE(seller.getDirector());
            SAVE(seller.getAccountant());
            if (seller.getLegalAddress() != null) {
                SAVE(seller.getLegalAddress());
            }
            if (seller.getPhysicalAddress() != null) {
                SAVE(seller.getPhysicalAddress());
            }
            saveSeller(seller, new Long(0));
            SAVE(account.getBank());
            SAVE(account);
            SAVE(contract);
            COMMIT(account);
        } catch (DatabaseException e) {
            ROLLBACK(account);
            throw new DatabaseException("err.sellermanager.seller_creation_failed", e);  
        }
        return seller;
    }
    public void saveSeller(BankAccount account, Contract contract, Long parentSellerId) throws DatabaseException, BlogicException {
        try {
            BEGIN(account);
            SAVE(account.getSeller().getDirector());
            SAVE(account.getSeller().getAccountant());
            if (account.getSeller().getLegalAddress() != null) {
                SAVE(account.getSeller().getLegalAddress());
            }
            if (account.getSeller().getPhysicalAddress() != null) {
                SAVE(account.getSeller().getPhysicalAddress());
            }
//            saveSeller(account.getSeller(), new Long(0));
            updateSeller0(account.getSeller(), parentSellerId);
            SAVE(account.getBank());
            SAVE(account);
            SAVE(contract);
            COMMIT(account);
        } catch (DatabaseException e) {
            ROLLBACK(account);
            throw new DatabaseException("err.sellermanager.seller_creation_failed", e);
        }
    }

    /** Create seller as new root of sellers tree or as nested seller if parent is specified. */
    //with correct Sellers tree
    private void createSeller0(Seller newSeller) throws DatabaseException {
        if (newSeller.getParentSeller() == null) {
            //save as new tree root
            SAVE(newSeller);
            //use node id as tree id
            newSeller.setTreeId(newSeller.getSellerId());
            newSeller.setLeftMargin(1);
            newSeller.setRightMargin(2);
            SAVE(newSeller);
        }
        else {
            //save as nested node in tree
            //get tree id from parent node (preload all parent's attrs)
            Seller parentSeller = (Seller) RETRIEVE(Seller.class, newSeller.getParentSeller().getSellerId(), new String[]{});
            newSeller.setTreeId(parentSeller.getTreeId());
            //add node to the end of the parent's nested nodes set
            newSeller.setLeftMargin(parentSeller.getRightMargin());
            newSeller.setRightMargin(parentSeller.getRightMargin().intValue() + 1);
            //update tree before inserting
            QueryParameterWrapper addLeftQPW = new QueryParameterWrapper("addLeft", newSeller.getLeftMargin());
            QueryParameterWrapper currTreeIdQPW = new QueryParameterWrapper("currentTreeId", newSeller.getTreeId());
            QueryParameterWrapper[] qpw = new QueryParameterWrapper[]{addLeftQPW, currTreeIdQPW};
            EXECUTE_QUERY_UPDATE(Seller.class, "rebuiltMarginsBeforeAddingLeafNode", qpw);
            //add new seller by inseritng it into the tree
            SAVE(newSeller);
        }
    }

    /** On seller update you couldn't change or delete its reference on parent seller. */
    private void updateSeller0(Seller savedSeller) throws DatabaseException {
        SAVE(savedSeller, fullSellerAttrsUpdate);
    }
    /** On seller update you couldn't change or delete its reference on parent seller. */
    private void updateSeller0(Seller savedSeller, Long parentSellerId) throws DatabaseException {
        SAVE(savedSeller, fullSellerAttrsUpdate);
    }                                                                

    //primary seller balance for seller
    private SellerBalance createPrimaryBalance(Seller seller, Long amount){
        SellerBalance sellerBalance = new SellerBalance();
        sellerBalance.setBlocked(false);
        sellerBalance.setDescription(PRIMARY_BALANCE_DESCRIPTION);
        sellerBalance.setName(PRIMARY_BALANCE_NAME);
        sellerBalance.setRegistrationDate(new Date());
        sellerBalance.setCode(seller.getCode() + PRIMARY_BALANCE_PREFIX);
        sellerBalance.setAmount(amount.doubleValue());
        return sellerBalance;
    }

    public List getSellersListAsc(List data) {
        List list = new ArrayList();
        for (int i = 0; i < data.size(); i++) {
            Object[] ret = (Object[]) data.get(i);
            if (isSellerInList(list, ret[1].toString()))
                list.add(ret[1].toString());
        }
        Collections.sort(list);
        return list;
    }

    private boolean isSellerInList(List data, String seller) {
        for (int i = 0; i < data.size(); i++) {
            if (data.get(i).toString().equals(seller))
                return false;
        }
        return true;
    }

}
