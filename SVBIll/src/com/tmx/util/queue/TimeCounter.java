package com.tmx.util.queue;

import java.io.PrintStream;
import java.util.*;

/**
 * Incapsulates the facility of iterative process performance measuring.
 */
public class TimeCounter {
    private String reportPrefix;
    public static final String EMPTY = "";
    private static final String[] PARAMETER_NAMES = new String[0];
    private static final long[] REPORT_INTERVAL_ITERATIONS = new long[0];

    public TimeCounter(String parameterName, long reportIntervalIterations, long reportIntervalMillis) {
        this(new String[]{parameterName}, new long[]{reportIntervalIterations}, new long[]{reportIntervalMillis});
    }

    public TimeCounter(String[] parameterNames, long reportIntervalIterations, long reportIntervalMillis) {
        this(parameterNames, new long[]{reportIntervalIterations}, new long[]{reportIntervalMillis});
    }

    public TimeCounter(String[] parameterNames, long[] reportIntervalIterations, long[] reportIntervalMillis) {
        String dbg = System.getProperty("debug");
        dbg = dbg == null ? "no" : dbg;
        debug = dbg.equalsIgnoreCase("true") | dbg.equalsIgnoreCase("y");
        if (!production) {
            parameters = new HashMap();
            parametersIndex = new ArrayList();
            int iMax = parameterNames.length;
            for (int i = 0; i < iMax; ++i) {
                String parameterName = parameterNames[i];
                StatisticsParameter sp = new StatisticsParameter(parameterName, i);
                parameters.put(parameterName, sp);
                parametersIndex.add(sp);
            }
            setReportPeriod(reportIntervalIterations, reportIntervalMillis);
            //
            startPoints = new Hashtable();
            startPointsPool = new ObjectPool("startPointsPool", new StartPoint(0, 0));
            maximumNotFinishedActivitiesToDescribe = -1;
        }
        //
        setSilent(!debug);
    }

    public TimeCounter(String reportPrefix, PrintStream reportStream) {
        this(PARAMETER_NAMES, REPORT_INTERVAL_ITERATIONS, REPORT_INTERVAL_ITERATIONS);
        this.reportPrefix = reportPrefix;
        this.reportStream = reportStream;
    }

    public String getReportPrefix() {
        return reportPrefix;
    }

    private int maximumNotFinishedActivitiesToDescribe;

    public TimeCounter setMaximumNotFinishedActivitiesToDescribe(int maximumNotFinishedActivitiesToDescribe) {
        this.maximumNotFinishedActivitiesToDescribe = maximumNotFinishedActivitiesToDescribe;
        return this;
    }

    public TimeCounter setReportPrefix(String reportPrefix) {
        this.reportPrefix = reportPrefix;
        return this;
    }

    public int getParameterIndex(String parameterName) {
        if (!production) {
            StatisticsParameter statisticsParameter = getStatisticsParameter(parameterName);
            if (statisticsParameter == null) {
                statisticsParameter = new StatisticsParameter(parameterName, parametersIndex.size());
                parameters.put(parameterName, statisticsParameter);
                parametersIndex.add(statisticsParameter);
            }
            return statisticsParameter.getIndex();
        } else {
            return -1;
        }
    }

    private Hashtable startPoints;
    private HashMap parameters;
    private ArrayList parametersIndex;
    private boolean debug;

    public class StatisticsParameter {
        String name;
        int index;
        long objectsCreated, totalTimeUsed, lastObjectsCreated, lastReportTime;
        long[] timeThresholds, minTimeUsed, maxTimeUsed;
        long reportIntervalIterations, reportIntervalMillis;
        public static final String NOT_FINISHED = "Not finished: ";
        public static final String COLON = ": ";
        public static final String MORE = "More...";
        public static final String MIN_MAX_COUNTS = ", min/max:cnts=";
        public static final String BIGGER_THEN = "> ";
        public static final String ITR = ": itr=";
        public static final String DURATION = ", dur=";
        public static final String AVG = ", avg=";
        public static final String MSEC_IPS = " msec, ips=";
        public static final String WAIT = ", wait=";
        public static final String SLASH = "/";
        public static final String WDUR = ", wdur=[";
        public static final String COMMA = ", ";
        public static final String CLOSIG_BRACKET = "]";
        public static final String EMPTY = "";

        public StatisticsParameter() {
            timeThresholds = new long[]{-1, 100, 250, 1000, 2000, 5000};
            minTimeUsed = new long[]{Long.MAX_VALUE, 0, 0, 0, 0, 0};
            maxTimeUsed = new long[]{Long.MIN_VALUE, 0, 0, 0, 0, 0};
        }

        public void setObjectsCreated(long objectsCreated) {
            this.objectsCreated = objectsCreated;
//            if (reportIntervalIterations > 0 && this.objectsCreated % reportIntervalIterations == 0) {
//                flushStatistics();
//            }
        }

//        private void flushStatistics() {
//            this.objectsCreated = 1;
//            this.totalTimeUsed = 0;
//            this.lastObjectsCreated = 0;
//            this.lastReportTime = 0;
//        }

        public void setTotalTimeUsed(long totalTimeUsed) {
            long diff = totalTimeUsed - this.totalTimeUsed;
            for (int i = 0; i < timeThresholds.length; i++) {
                if (i == 0) {
                    minTimeUsed[i] = minTimeUsed[i] > diff ? diff : minTimeUsed[i];
                    maxTimeUsed[i] = maxTimeUsed[i] < diff ? diff : maxTimeUsed[i];
                } else {
                    long timeThreshold = timeThresholds[i];
                    if (diff > timeThreshold) {
                        maxTimeUsed[i] += 1;
                    } else if (diff <= timeThreshold) {
                        minTimeUsed[i] += 1;
                    }
                }
            }
            this.totalTimeUsed = totalTimeUsed;
//            if (this.reportIntervalMillis > 0 && this.totalTimeUsed / this.reportIntervalMillis >= 2) {
//                flushStatistics();
//            }
        }

        public void setLastObjectsCreated(long lastObjectsCreated) {
            this.lastObjectsCreated = lastObjectsCreated;
        }

        public void setLastReportTime(long lastReportTime) {
            this.lastReportTime = lastReportTime;
        }

        public void setReportIntervalIterations(long reportIntervalIterations) {
            this.reportIntervalIterations = reportIntervalIterations;
        }

        public void setReportIntervalMillis(long reportIntervalMillis) {
            this.reportIntervalMillis = reportIntervalMillis;
        }

        public StatisticsParameter(String name, int index) {
            this();
            this.name = name;
            this.index = index;
            reportIntervalMillis = 60000;
        }

        public String getName() {
            return name;
        }

        public int getIndex() {
            return index;
        }

        public long getObjectsCreated() {
            return objectsCreated;
        }

        public long getTotalTimeUsed() {
            return totalTimeUsed;
        }

        public long getLastObjectsCreated() {
            return lastObjectsCreated;
        }

        public long getLastReportTime() {
            return lastReportTime;
        }

        public long getReportIntervalIterations() {
            return reportIntervalIterations;
        }

        public long getReportIntervalMillis() {
            return reportIntervalMillis;
        }

        public void report(PrintStream ps) {
            if (getObjectsCreated() <= 0) {
                return;
            }
            setLastObjectsCreated(getObjectsCreated());
            setLastReportTime(System.currentTimeMillis());
            //
            int notFinished = 0, notFinishedTotal;
            long notFinishedTimeMIN = Long.MAX_VALUE, notFinishedTimeMAX = Long.MIN_VALUE;
            StringBuffer wdesc = new StringBuffer();
            int descElements = 0, descElementsMAX = maximumNotFinishedActivitiesToDescribe;
            synchronized (startPoints) {
                notFinishedTotal = startPoints.size();
                Iterator v = startPoints.entrySet().iterator();
                while (v.hasNext()) {
                    Map.Entry e = (Map.Entry) v.next();
                    StartPoint p = (StartPoint) e.getValue();
                    if (p.parameterIndex == getIndex()) {
                        Object key = e.getKey();
                        if (!(key instanceof String) && descElements < descElementsMAX) {
                            wdesc.append('\n');
                            if (reportPrefix != null) {
                                wdesc.append(reportPrefix);
                                wdesc.append(COLON);
                            }
                            wdesc.append(NOT_FINISHED);
                            wdesc.append(key.toString());
                            descElements++;
                        } else if (descElements == descElementsMAX) {
                            wdesc.append('\n');
                            if (reportPrefix != null) {
                                wdesc.append(reportPrefix);
                                wdesc.append(COLON);
                            }
                            wdesc.append(MORE);
                            descElements++;
                        }
                        //
                        notFinished++;
                        notFinishedTimeMIN = notFinishedTimeMIN > p.startTime ? p.startTime : notFinishedTimeMIN;
                        notFinishedTimeMAX = notFinishedTimeMAX < p.startTime ? p.startTime : notFinishedTimeMAX;
                    }
                }
            }
            //
            StringBuffer minMax = new StringBuffer(MIN_MAX_COUNTS);
            minMax.append(minTimeUsed[0]).append('/').append(maxTimeUsed[0]).append(':');
            for (int i = 1; i < timeThresholds.length; i++) {
                minMax.append(maxTimeUsed[i]).append(',');
            }
            String time = new java.sql.Timestamp(System.currentTimeMillis()).toString();
            time = time.substring(0, time.indexOf('.'));
            ps.println(
                    time +
                    (reportPrefix == null ? BIGGER_THEN : BIGGER_THEN + reportPrefix + COLON) +
                    getName() + ITR + getObjectsCreated() +
                    DURATION + getTotalTimeUsed() +
                    AVG + format(((double) getTotalTimeUsed()) / getObjectsCreated()) +
                    MSEC_IPS + format(((double) getObjectsCreated()) / getTotalTimeUsed() * 1000) +
                    minMax +
                    WAIT + notFinished + SLASH + notFinishedTotal +
                    (notFinished > 0
                    ? WDUR + (getLastReportTime() - notFinishedTimeMAX) +
                    COMMA + (getLastReportTime() - notFinishedTimeMIN) + CLOSIG_BRACKET
                    : EMPTY
                    ) +
                    wdesc
            );
            for (int i = 0; i < timeThresholds.length; i++) {
                if (i == 0) {
                    minTimeUsed[i] = Long.MAX_VALUE;
                    maxTimeUsed[i] = Long.MIN_VALUE;
                } else {
                    minTimeUsed[i] = 0;
                    maxTimeUsed[i] = 0;
                }
            }
        }
    }

    public StatisticsParameter getStatisticsParameter(String parameterName) {
        return ((StatisticsParameter) parameters.get(parameterName));
    }
    public StatisticsParameter getStatisticsParameter(int parameterIndex) {
        return ((StatisticsParameter) parametersIndex.get(parameterIndex));
    }

    public long getObjectsCreated(int parameterIndex) {
        return ((StatisticsParameter) parametersIndex.get(parameterIndex)).getObjectsCreated();
    }

    public long getObjectsCreated(String parameterName) {
        return ((StatisticsParameter) parameters.get(parameterName)).getObjectsCreated();
    }

    public long getTotalTimeUsed(int parameterIndex) {
        return ((StatisticsParameter) parametersIndex.get(parameterIndex)).getTotalTimeUsed();
    }

    public long getTotalTimeUsed(String parameterName) {
        return ((StatisticsParameter) parameters.get(parameterName)).getTotalTimeUsed();
    }

    public TimeCounter setSilent(boolean silent) {
        if (production) {
            return this;
        }
        if (silent == true) {
            removeReporter(this);
        } else {
            addReporter(this);
        }
        return this;
    }

    public void setReportPeriod(long reportIntervalIterations, long reportIntervalMillis) {
        setReportPeriod(new long[]{reportIntervalIterations}, new long[]{reportIntervalMillis});
    }

    public void setReportPeriod(long[] reportIntervalIterations, long[] reportIntervalMillis) {
        if (!production) {
            int iMax = parametersIndex.size();
            for (int i = 0; i < iMax; ++i) {
                StatisticsParameter statisticsParameter = getStatisticsParameter(i);
                statisticsParameter.setReportIntervalIterations(reportIntervalIterations.length > i ? reportIntervalIterations[i] : reportIntervalIterations[0]);
                statisticsParameter.setReportIntervalMillis(reportIntervalMillis.length > i ? reportIntervalMillis[i] : reportIntervalMillis[0]);
            }
        }
    }

    private PrintStream reportStream;

    public PrintStream getReportStream() {
        return reportStream == null ? System.out : reportStream;
    }

    public TimeCounter setReportStream(PrintStream reportStream) {
        this.reportStream = reportStream;
        return this;
    }

    public void report() {
        report(getReportStream());
    }

    public void report(PrintStream ps) {
        if (!production) {
            for (int i = 0; i < parametersIndex.size(); i++) {
                StatisticsParameter statisticsParameter = (StatisticsParameter) parametersIndex.get(i);
                statisticsParameter.report(ps);
            }
        }
    }

    private String format(double d) {
        return String.valueOf(((double) Math.round(d * 10000)) / 10000);
    }

    private ObjectPool startPointsPool;
    private class StartPoint implements ObjectPoolElement {
        long startTime;
        int parameterIndex;

        public StartPoint(long startTime, int parameterIndex) {
            this.startTime = startTime;
            this.parameterIndex = parameterIndex;
        }

        public StartPoint setStartTime(long startTime) {
            this.startTime = startTime;
            return this;
        }

        public StartPoint setParameterIndex(int parameterIndex) {
            this.parameterIndex = parameterIndex;
            return this;
        }

        public Object newPoolObjectInstance() {
            return new StartPoint(0, 0);
        }
    }

    public Object start() {
        return start(0, null);
    }

    public Object start(String parameterName) {
        return start(getParameterIndex(parameterName));
    }

    public Object start(int parameterIndex) {
        return start(parameterIndex, null);
    }

    public Object start(String parameterName, Object checkPointObject) {
        return start(getParameterIndex(parameterName), checkPointObject);
    }

    private static long _id;
    private static long getID() {
        return ++_id;
    }
    public Object start(int parameterIndex, Object checkPointObject) {
        if (!production) {
            long t = System.currentTimeMillis();
            checkPointObject = checkPointObject == null ? String.valueOf(getID()) : checkPointObject;
            StartPoint startPoint = ((StartPoint) startPointsPool.getObject());
            startPoints.put(checkPointObject,
                startPoint.setStartTime(t).setParameterIndex(parameterIndex));
            return checkPointObject;
        } else {
            return null;
        }
    }

    public void end(Object checkPointObject) {
        if (!production && checkPointObject != null) {
            StartPoint startPoint = (StartPoint) startPoints.remove(checkPointObject);
            if (startPoint != null) {
                try {startPointsPool.returnObject(startPoint);} catch (Throwable e) {e.printStackTrace();}
                StatisticsParameter statisticsParameter = getStatisticsParameter(startPoint.parameterIndex);
                statisticsParameter.setObjectsCreated(statisticsParameter.getObjectsCreated() + 1);
                long now = System.currentTimeMillis();
                long diff = (now - startPoint.startTime);
                statisticsParameter.setTotalTimeUsed(statisticsParameter.getTotalTimeUsed() + (diff < 0 ? 0 : diff));
            }
        }
    }

    public void count(String parameterName) {
        count(getParameterIndex(parameterName));
    }

    public void set(String parameterName, long value) {
        set(getParameterIndex(parameterName), value);
    }

    public void inc(String parameterName, int iter) {
        inc(getParameterIndex(parameterName), iter);
    }

    public void inc(int parameterIndex, int iter) {
        if (!production) {
            StatisticsParameter statisticsParameter = getStatisticsParameter(parameterIndex);
            statisticsParameter.setObjectsCreated(statisticsParameter.getObjectsCreated() + iter);
        }
    }

    public void count(int parameterIndex) {
        if (!production) {
            StatisticsParameter statisticsParameter = getStatisticsParameter(parameterIndex);
            statisticsParameter.setObjectsCreated(statisticsParameter.getObjectsCreated() + 1);
        }
    }

    public void set(int parameterIndex, long value) {
        if (!production) {
            StatisticsParameter statisticsParameter = getStatisticsParameter(parameterIndex);
            statisticsParameter.setObjectsCreated(value);
        }
    }

    public void cancel(Object checkPointObject) {
        if (!production) {
            try {startPointsPool.returnObject(startPoints.remove(checkPointObject));} catch (Throwable e) {e.printStackTrace();}
        }
    }

    private static HashMap timeCounters;
    private static boolean production;

    private static void addReporter(TimeCounter tc) {
        timeCounters.put(tc, new Long(System.currentTimeMillis()));
    }

    private static void removeReporter(TimeCounter tc) {
        timeCounters.remove(tc);
    }

    static {
        String dbg = System.getProperty("production");
        dbg = dbg == null ? "no" : dbg;
        production = dbg.equalsIgnoreCase("true") | dbg.equalsIgnoreCase("y");
        startReporterThread();
    }

    private static void startReporterThread() {
        new Thread() {
            {
                setName("TimeCounter Report Thread");
                setDaemon(true);
                timeCounters = new HashMap();
            }

            public void run() {
                while (true) {
                    Object[] _tc = timeCounters.keySet().toArray();
                    for (int i = 0; i < _tc.length; i++) {
                        TimeCounter tCounter = (TimeCounter) _tc[i];
                        long thisTime = System.currentTimeMillis();
                        int imx = tCounter.parametersIndex.size();
                        boolean smthChanged = false;
                        for (int p = 0; p < imx; ++p) {
                            StatisticsParameter sp = tCounter.getStatisticsParameter(p);
                            if (sp.getObjectsCreated() > 0 && (sp.getLastObjectsCreated() != sp.getObjectsCreated())
                                    && ((thisTime - sp.getLastReportTime()) >= sp.getReportIntervalMillis()
                                    || sp.getReportIntervalIterations() <= (sp.getObjectsCreated() - sp.getLastObjectsCreated()))) {
                                smthChanged = true;
                                break;
                            }
                        }
                        if (smthChanged) {
                            tCounter.getReportStream().println(EMPTY);
                            tCounter.report(tCounter.getReportStream());
                        }
                    }
                    try {
                        Thread.sleep(10000);
                    } catch (InterruptedException e) {
                        break;
                    }
                }
            }
        }.start();
    }
}
