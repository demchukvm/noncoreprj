package com.tmx.beng.iso8583Gate.processors;

import com.tmx.beng.iso8583Gate.pdu.RuntimeMessage;
import com.tmx.beng.iso8583Gate.pdu.IncorrectFormatException;
import com.tmx.beng.iso8583Gate.mapper.BillingMapper;
import com.tmx.beng.iso8583Gate.mapper.BillingMapperMetadata;
import com.tmx.beng.base.*;
import com.tmx.util.InitException;
import com.tmx.as.exceptions.DatabaseException;
import com.tmx.as.entities.iso8583.transaction.Iso8583Transaction;
import com.tmx.as.base.EntityManager;
import com.tmx.as.base.FilterWrapper;

/**

 */
public class RepeatSellServiceProcessor extends BasicProcessor{

    public int getMessageTypeId() {
        return 211;
    }

    public int getProcessingCode() {
        return 0;
    }

    public int[] getDEToCopy() {
        return new int[]{4, 11, 24, 38, 41, 42};
    }

    public void processSpecificTask(RuntimeMessage reqMsg, RuntimeMessage respMsg)
            throws ProcessingException, BillException, IncorrectFormatException, InitException, DatabaseException {

        logger.debug("Repeat sale");
        BillingMapper billingMapper = BillingMapperMetadata.getInstance().getBillMapper(reqMsg);
        BillingMessageWritable reqBM = new BillingMessageImpl();
        Iso8583Transaction isoTx = billingMapper.retrieveTransaction(reqMsg);
        try {
            //1. check is payment already processed  
            if(isoTx == null || StatusDictionary.JOINED_REQUEST_NOT_FOUND == Integer.parseInt(isoTx.getBillStatusCode())){
                //transaction not registered yet. process it
                logger.debug("Repeat sale:: prepare MSG for rep. sale..");
                logger.debug("Repeat sale:: " + isoTx);

                //prepare and save isotransaction in iso datasource
                isoTx = billingMapper.preSaveTransaction(reqMsg);
                billingMapper.assignRetrievalRefNum(isoTx);
                billingMapper.assignAuthIdResponse(isoTx);

                //prepare billing message and send it to process into billing engine
                billingMapper.prepare0200Msg(reqMsg, reqBM);

                BillingMessage respBM = getConnection().processSync(reqBM);

                //post-save iso transaction in iso datasource
                billingMapper.postSaveTransaction(isoTx, respBM);
                //prepare iso response         ;
                billingMapper.prepare0211Msg(respMsg, isoTx);
            } else {
                logger.debug("Repeat sale:: -1 ");
                //transaction found. return its state
                //billingMapper.prepare0211Msg(respMsg, isoTx);
                //If isoTx no succes
                if(isoTx.getBillStatusCode() == null || !isoTx.getBillStatusCode().equals("0")) {
                    logger.debug("Repeat sale:: 1 ");
                    //get status code from billing
                    BillingMessage respBM100 = retriveStatusFromBilling(reqMsg, billingMapper, reqBM);
                    String billingStatusCode = (String)respBM100.getAttribute(BillingMessage.STATUS_CODE);
                    if (billingStatusCode != null) {
                        //if status code determined
                        if(!billingStatusCode.equals("1")) {
                            isoTx.setBillStatusCode((String)respBM100.getAttribute(BillingMessage.STATUS_CODE));

                            billingMapper.assignRetrievalRefNum(isoTx);
                            billingMapper.assignAuthIdResponse(isoTx);
                            //post-save iso transaction in iso datasource
                            billingMapper.postSaveTransaction(isoTx, respBM100);
                           //prepare iso response
                            billingMapper.prepare0211Msg(respMsg, isoTx);
                        } else {
                            logger.debug("Repeat sale:: 2 ");
                           isoTx.setBillStatusCode("5");
                           billingMapper.prepare0211Msg(respMsg, isoTx);
                        }
                    }
                } else {
                    logger.debug("Repeat sale:: 3 ");
                    billingMapper.assignRetrievalRefNum(isoTx);
                    billingMapper.assignAuthIdResponse(isoTx);
                    //post-save iso transaction in iso datasource
                   //prepare iso response
                    billingMapper.prepare0211Msg(respMsg, isoTx);
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("err.Repeat sale" + e.getMessage() + e.getCause());
            BillingMessage respBM100 = retriveStatusFromBilling(reqMsg, billingMapper, reqBM);
            String billingStatusCode = (String)respBM100.getAttribute(BillingMessage.STATUS_CODE);
            if (billingStatusCode != null) {
                //if stsus code determined
                if(!billingStatusCode.equals("1")) {
                    isoTx.setBillStatusCode((String)respBM100.getAttribute(BillingMessage.STATUS_CODE));

                    billingMapper.assignRetrievalRefNum(isoTx);
                    billingMapper.assignAuthIdResponse(isoTx);
                    //post-save iso transaction in iso datasource
                    billingMapper.postSaveTransaction(isoTx, respBM100);
                   //prepare iso response
                    billingMapper.prepare0211Msg(respMsg, isoTx);
                } else {
                    logger.debug("Repeat sale:: 2 ");
                   isoTx.setBillStatusCode("5");
                   billingMapper.prepare0211Msg(respMsg, isoTx);
                }
            }
        }

    }

    private BillingMessage retriveStatusFromBilling(RuntimeMessage reqMsg, BillingMapper billingMapper, BillingMessageWritable reqBM) throws IncorrectFormatException, BillException {
        //RM: do not use billing to get refill status. use local iso datasource.
        // DVM: why not??
        billingMapper.prepare0100Msg(reqMsg, reqBM);

        return getConnection().processSync(reqBM);
    }
}
