package com.tmx.web.actions.core.base;

import com.tmx.web.actions.core.AbstractSvbillAction;
import com.tmx.web.forms.core.base.AbstractBrowseForm;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *  Action to handle 'browse against some entities' request from any page.
 *  You could add such browse functionality as this one has into your particular action
 *  and do not use separate action for browse.
 */
public abstract class AbstractBrowseAction extends AbstractSvbillAction {

    protected abstract String getForwardName();

    public ActionForward defaultAction(ActionMapping mapping,
                                       ActionForm form,
                                       HttpServletRequest request,
                                       HttpServletResponse response,
                                       String methodName) throws Throwable {
        AbstractBrowseForm abstractBrowseForm = (AbstractBrowseForm)verifyForm(form, AbstractBrowseForm.class);
        abstractBrowseForm.getBrowseTable().setRefreshActionName(this.getName());
        abstractBrowseForm.getBrowseTable().refresh();
        return mapping.findForward(getForwardName());
    }

    
    public ActionForward openBrowseWindow(ActionMapping mapping,
                                       ActionForm form,
                                       HttpServletRequest request,
                                       HttpServletResponse response) throws Throwable {
        return defaultAction(mapping, form, request, response, null);
    }
    
}
