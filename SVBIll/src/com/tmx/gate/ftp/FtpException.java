package com.tmx.gate.ftp;

import com.tmx.util.StructurizedException;

import java.util.Locale;


/**
 * When there are any FTP command fails in the SFtpClient,<br>
 * This exception is throwed.
 */

public class FtpException extends StructurizedException {
    
    public FtpException(String errKey){
        super(errKey);
    }

    public FtpException(String errKey, Locale locale){
        super(errKey, locale);
    }

    public FtpException(String errKey, String[] params){
        super(errKey, params);
    }

    public FtpException(String errKey, String[] params, Throwable cause){
        super(errKey, params, cause);
    }

    public FtpException(String errKey, String[] params, Locale locale){
        super(errKey, params, locale);
    }

    public FtpException(String errKey, Throwable cause){
        super(errKey, cause);
    }

}
