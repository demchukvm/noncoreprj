package com.tmx.web.forms.core.balance.operator_balance;

import com.tmx.web.forms.core.balance.AbstractManageBalancesForm;
import com.tmx.web.controls.table.ControlManagerTable;
import com.tmx.web.controls.table.Parameter;
import com.tmx.web.controls.ScrollBox;
import com.tmx.as.entities.general.user.User;
import com.tmx.as.entities.bill.operator.Operator;
import com.tmx.as.blogic.OperatorManager;
import com.tmx.as.exceptions.DatabaseException;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class ManageOperatorBalancesForm extends AbstractManageBalancesForm {

    private ScrollBox scrollBoxOperators;

    protected void init(){
        super.init();
        initBoxes();
        initTables();
        initButtonsAction();            
    }

    private void initBoxes() {
        scrollBoxOperators = new ScrollBox() {

            protected String getKey(Object dataListElement) {
                return ((Operator) dataListElement).getId().toString();
            }

            protected String getValue(Object dataListElement) {
                return ((Operator) dataListElement).getName().toString();
            }
        };
        scrollBoxOperators.setNullable(true);

    }

    public void bind() throws DatabaseException {
//        scrollBoxOperators.bind(new OperatorManager().getAllOperators());
    }

    private void initTables() {
        ControlManagerTable table = new ControlManagerTable("query(query_type=retrieve_all," +
                "entity_class=com.tmx.as.entities.bill.balance.OperatorBalance," +
                "table_attr=operator," +
                "order(name=id,type=asc)," +
                "page_size=15" +
                ")");
        table.setRefreshActionName("/core/balance/operator_balance/manageOperatorBalances");

        table.addParameterToFilter("by_operator_id", new Parameter("operator_id", scrollBoxOperators));

        table.setCriterionFactory(getCriterionFactory());

        setBalancesTable(table);
    }

    private void initButtonsAction() {
        String action = "/core/balance/operator_balance/infoOperatorBalance";
        getOnCreateButton().setAction(action);
        getOnDeleteButton().setAction(action);
        getOnEditButton().setAction(action);
    }

    protected void bindSpecificData(User loggedInUser) throws Throwable {
        List operators = new OperatorManager().getAllOperators();
        Collections.sort(operators, new Comparator<Operator>()
        {
          public int compare(Operator op1, Operator op2) {
            return op1.getName().compareTo(op2.getName());
          }
        });
        //scrollBoxOperators.bind(new OperatorManager().getAllOperators());
        scrollBoxOperators.bind(operators);
    }

    public ScrollBox getScrollBoxOperators() {
        return scrollBoxOperators;
    }

    public void setScrollBoxOperators(ScrollBox scrollBoxOperators) {
        this.scrollBoxOperators = scrollBoxOperators;
    }

}
