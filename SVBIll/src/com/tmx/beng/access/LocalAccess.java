package com.tmx.beng.access;

import com.tmx.beng.base.LocalConnectionImpl;
import com.tmx.beng.base.BillException;

/**
*  Simple Access to billing engine
*/
public class LocalAccess implements Access {

    public Connection obtainConnection() throws BillException {
        return new LocalConnectionImpl();
    }
    
}
