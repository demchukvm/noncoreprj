package com.tmx.beng.medaccess.ussd;

import com.tmx.beng.medaccess.MediumAccess;
import com.tmx.beng.medaccess.MediumConnection;
import com.tmx.beng.medaccess.GateException;

/**
   Medium Access to obtain USSD medium connection
 */
public class UssdGateMediumAccess implements MediumAccess {

    public void init() {
    }

    public MediumConnection getConnection() throws GateException {
        return new UssdGateMediumConnection();
    }
}
