package com.tmx.beng.base.processors;

import com.tmx.as.entities.bill.terminal.Terminal;
import com.tmx.as.entities.bill.transaction.TerminalBalanceTransaction;
import com.tmx.as.entities.bill.transaction.TransactionType;
import com.tmx.as.base.ProcedureWrapper;
import com.tmx.as.base.QueryParameterWrapper;
import com.tmx.as.exceptions.DatabaseException;
import com.tmx.as.blogic.balance.TerminalBalanceManager;
import com.tmx.beng.base.*;
import com.tmx.beng.base.cache.DBCache;
import com.tmx.beng.base.cache.DBCacheException;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.sql.Types;


public class TerminalReservationProcessor extends DefaultReservationProcessor{


    protected List prepareOperatorTransactions(BillingMessageWritable msg, DBCache dbCache, TransConfig config) throws DBCacheException {
        return Collections.EMPTY_LIST;
    }

    protected List prepareSellerTransactions(BillingMessageWritable msg, DBCache dbCache, TransConfig config) throws DBCacheException {
        return Collections.EMPTY_LIST;
    }

    protected List prepareTerminalTransactions(BillingMessageWritable msg, DBCache dbCache, TransConfig config) throws DBCacheException {
        Reservation reservation = config.reservationSet.getReservation(Terminal.class, config.terminalSn);
        Iterator iter = reservation.reservesIterator();
        Reservation.Reserve reserve = (Reservation.Reserve)iter.next();
        if(iter.hasNext())
            throw new IllegalStateException("reserves isn't one");

        TerminalBalanceTransaction transaction = new TerminalBalanceTransaction();
        transaction.setTransactionTime(config.transRegTime);
        transaction.setClientTime(config.clientTime);
        transaction.setAmount(new Double(reserve.getAmount()));
        transaction.setTerminal(dbCache.getTerminal(config.terminalSn));
        transaction.setTerminalBalance(dbCache.getTerminalBalance(config.terminalSn, reserve.getBalanceCode()));
        transaction.setTransactionSupport(config.transactionSupport);
        transaction.setTransactionGUID(config.transactionGUID);
        transaction.setCliTransactionNum(config.cliTransactionNum);
        transaction.setType(new TransactionType(reserve.getTransTypeCode()));
        transaction.setOperationType(config.operationsType);

        dbCache.changeTerminalBalance(config.terminalSn, reserve.getBalanceCode(), reserve.getAmount(), reserve.getTransTypeCode());

         //save pre tx balance amount
        try {
            transaction.setPreTxBalance((new TerminalBalanceManager()).getBalanceByCode(reserve.getBalanceCode()).getAmount());
        } catch (DatabaseException dbe) {
            logger.error("Can`t set current balance amount " + " " + reserve.getBalanceCode() + " " + dbe.getMessage());
        } catch (Exception e) {
            logger.error("Can`t set current balance amount " + " " + reserve.getBalanceCode() + " " + e.getMessage());
        }

        //TODO:(AN) ?????
        msg.setStatusCode(StatusDictionary.STATUS_OK);

        List result = new ArrayList();
        result.add(transaction);
        return result;
    }

    protected List prepareClientTransaction(BillingMessageWritable msg, DBCache dbCache, TransConfig config) throws DBCacheException {
        return Collections.EMPTY_LIST;
    }

    protected List prepareChangeBalancesProcedure(BillingMessageWritable msg, DBCache dbCache, TransConfig config) throws DBCacheException {
        Reservation reservation = config.reservationSet.getReservation(Terminal.class, config.terminalSn);
        Iterator iter = reservation.reservesIterator();
        Reservation.Reserve reserve = (Reservation.Reserve)iter.next();

        if(iter.hasNext())
            throw new IllegalStateException("reserves isn't one");

        ProcedureWrapper wrapper = new ProcedureWrapper(
                new QueryParameterWrapper[]{
                        QueryParameterWrapper.createOutParameter(1, Types.INTEGER),
                        new QueryParameterWrapper(2, dbCache.getTerminal(config.terminalSn).getTerminalId()),
                        new QueryParameterWrapper(3, new Double(getSignedAmount(reserve.getTransTypeCode(), reserve.getAmount()))),
                },
                Terminal.class,
                "change_terminal_balance");

        List result = new ArrayList();
        result.add(wrapper);

        return result;
    }

    public Task rollback(Task task, Throwable e) {
        logger.info("Rollback operator change...");
        try{
            BillingMessageWritable msg = task.getProcesedMessage();
            DBCache dbCache = task.getSessionDBCache();
            ReservationSet reservationSet = (ReservationSet)msg.getAttribute(BillingMessage.BALANCE_RESERVATION_SET);

            String terminalSn = msg.getAttributeString(BillingMessage.TERMINAL_SN);
            Reservation reservation = reservationSet.getReservation(Terminal.class, terminalSn);

            for(Iterator iter = reservation.reservesIterator(); iter.hasNext();){
                Reservation.Reserve reserve = (Reservation.Reserve)iter.next();
                if(reserve.isApplied())
                    dbCache.changeTerminalBalance(terminalSn, reserve.getBalanceCode(), -reserve.getAmount(), reserve.getTransTypeCode());
                    reservation.cancelReserve(reserve.getId());
            }
        }
        catch(Throwable te){
            logger.error(te.getLocalizedMessage(), te);
        }
        return super.rollback(task, e);
    }


}
