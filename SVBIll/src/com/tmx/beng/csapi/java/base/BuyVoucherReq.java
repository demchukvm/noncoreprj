package com.tmx.beng.csapi.java.base;

import com.tmx.beng.access.Connection;
import com.tmx.beng.base.BillingMessage;

import java.util.Date;


public class BuyVoucherReq extends DefaultClientReq<BuyVoucherResp> {

    BuyVoucherReq(Connection connect) {
        super(connect);
        requestMsg.setOperationName(BillingMessage.O_BUY_VOUCHER);
    }

    protected BuyVoucherResp handleResponseMessage(com.tmx.beng.base.BillingMessage responseMsg) {
        BuyVoucherResp resp = new BuyVoucherResp();
        resp.setStatusCode(responseMsg.getAttributeString(BillingMessage.STATUS_CODE));
        resp.setWaitTime(Long.parseLong(responseMsg.getAttributeString(BillingMessage.WAIT_TIME)));
        resp.setStatusMessage(responseMsg.getAttributeString(BillingMessage.STATUS_MESSAGE));
        resp.setNominal(responseMsg.getAttributeString(BillingMessage.VOUCHER_NOMINAL));
        resp.setCode(responseMsg.getAttributeString(BillingMessage.VOUCHER_CODE));
        resp.setSecretCode(responseMsg.getAttributeString(BillingMessage.VOUCHER_SECRET_KEY));
        resp.setBestBeforeDate((Date)responseMsg.getAttribute(BillingMessage.VOUCHER_BEST_BEFORE_DATE));
        resp.setPrice(resp.getNominal() != null ? Double.parseDouble(resp.getNominal()) : null);
        return resp;
    }

    public void setServiceCode(String serviceCode) {
        requestMsg.setAttributeString(BillingMessage.SERVICE_CODE, serviceCode);
    }

    public void setVoucherNominal(String voucherNominal) {
        requestMsg.setAttributeString(BillingMessage.VOUCHER_NOMINAL, voucherNominal);
    }

    // Proximan 01.11.2010
  /*  public void setGenSellerCode(String genSellerCode) {
        requestMsg.setAttributeString(BillingMessage.GEN_SELLER_CODE, genSellerCode);
    }

    public void setGenServiceCode(String genServiceCode) {
        requestMsg.setAttributeString(BillingMessage.GEN_SERVICE_CODE, genServiceCode);
    }

    public void setGenService(String genService) {
        requestMsg.setAttributeString(BillingMessage.GEN_SERVICE, genService);
    }

    public void setGenTerminalSN(String genTerminalSN) {
        requestMsg.setAttributeString(BillingMessage.GEN_TERMINAL_SN, genTerminalSN);
    }   */
    // поле с поддиллером
    // сервайс
    // тип ГЕН

}
