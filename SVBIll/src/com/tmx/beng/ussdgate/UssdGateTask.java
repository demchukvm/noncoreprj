package com.tmx.beng.ussdgate;

import com.tmx.util.queue.ActionQueueElement;
import com.tmx.beng.base.BillingMessage;
import org.apache.log4j.Logger;


public class UssdGateTask extends ActionQueueElement {
    private BillingMessage procesedMessage = null;
    private String ussdName = null;

    private static Logger logger = Logger.getLogger("gate.UssdTask");

    public UssdGateTask(BillingMessage billingMessage, String ussdName){
        this.procesedMessage = billingMessage;
        this.ussdName = ussdName;
    }

    public void run() {
        try{
            UssdGate.getInstance(ussdName).runBillMessage(procesedMessage);
        }catch(UssdGateException e){
            logger.error(e.getLocalizedMessage(), e);
        }
    }
}
