package com.tmx.beng.httpsgate.life;

import com.tmx.util.queue.ActionQueueElement;
import com.tmx.beng.base.BillingMessage;
import com.tmx.beng.httpsgate.HttpsGateException;
import com.tmx.beng.httpsgate.life.BasicLifeGate;

/**
    Gate task. Initiated from gate via medium connection.
    Processed by asynchronous thread.
 */
public class LifeGateTask extends ActionQueueElement {
    private BillingMessage procesedMessage;
    private BasicLifeGate gate;

    public LifeGateTask(BillingMessage billingMessage, BasicLifeGate gate){
        this.gate = gate;
        this.procesedMessage = billingMessage;
    }

    public void run() {
        try{
            gate.runBillMessage(procesedMessage);
        }
        catch(HttpsGateException e){
            gate.logger.error(e.getLocalizedMessage(), e);
        } catch (Exception totalE) {
            gate.logger.error(totalE);            
        }
    }
}
