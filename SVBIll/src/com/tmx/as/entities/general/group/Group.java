package com.tmx.as.entities.general.group;

import com.tmx.as.base.BasicEntity;
import com.tmx.as.entities.general.organization.Organization;

import java.util.HashSet;
import java.util.Set;
import java.io.Serializable;


public class Group extends BasicEntity {

    private Long groupId;
    private String name;
    private Organization organization;
    private Set users = new HashSet();

    public Long getGroupId() {
        return groupId;
    }

    public void setGroupId(Long groupId) {
        this.groupId = groupId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set getUsers() {
        return users;
    }

    public void setUsers(Set users) {
        this.users = users;
    }

    public Serializable getId() {
        return getGroupId();
    }

    public int getValid() {
        return valid;
    }

    public void setValid(int valid) {
        this.valid = valid;
    }

    public Organization getOrganization() {
        return organization;
    }

    public void setOrganization(Organization organization) {
        this.organization = organization;
    }

}
