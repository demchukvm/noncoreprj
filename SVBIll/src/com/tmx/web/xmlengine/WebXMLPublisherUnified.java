package com.tmx.web.xmlengine;

import com.tmx.engines.xmlengine.XMLPublisherUnified;
import com.tmx.engines.xmlengine.NodePrototype;
import com.tmx.engines.xmlengine.customTags.AbstractNode;
import com.tmx.engines.xmlengine.customTags.GroupNode;
import org.w3c.dom.Node;

/** Unified implementation of WebXMLPublisher */
public class WebXMLPublisherUnified extends XMLPublisherUnified implements WebXMLPublisher{

    public Node addLink(Node parantNode, NodePrototype nodePrototype){
        return nodePrototype.renderAndInjectXMLNode(parantNode, null);
    }

    /** Render group node. Group is the container to incapsulate  */
    public Node addGroup(Node parentNode, String groupName, String groupId){
        return new GroupNode(groupId, groupName).renderAndInjectXMLNode(parentNode, null);
    }
}
