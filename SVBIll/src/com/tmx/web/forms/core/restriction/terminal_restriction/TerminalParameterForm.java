package com.tmx.web.forms.core.restriction.terminal_restriction;

import com.tmx.web.forms.core.restriction.RestrictionParameterForm;

@Deprecated
public class TerminalParameterForm extends RestrictionParameterForm {

    protected void init(){
        super.init();
        initButtons();
    }

    private void initButtons(){
        getOnSaveButton().setAction("/core/balance/terminal_balance/terminalParameter");
        getOnApplyButton().setAction("/core/balance/terminal_balance/terminalParameter");
        getOnCancelButton().setAction("/core/balance/terminal_balance/terminalRestriction");
    }

}
