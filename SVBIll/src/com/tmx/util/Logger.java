package com.tmx.util;

/**
 * Author: Baramykov <br>
 * Date: Oct 7, 2005,
 * Time: 2:32:24 PM <br>
 * <br>
 * System logger.
 */
public class Logger {

    /**
     * Logger name
     */
    private String name = null;

    /**
     * Constructor
     * @param name logger name
     */
    public Logger(String name) {
        this.name = name;
    }

    /**
     * Log message with DEBUG level.
     * @param msg сообщение
     */
    public void debug(String msg){
        org.apache.log4j.Logger.getLogger(name).debug(msg);
    }

    /**
     * Log message with INFO level.
     * @param msg сообщение
     */
    public void info(String msg){
        org.apache.log4j.Logger.getLogger(name).info(msg);
    }

    /**
     * Log message with WARN level.
     * @param msg сообщение
     */
    public void warn(String msg){
        org.apache.log4j.Logger.getLogger(name).warn(msg);
    }

    /**
     * Log message with ERROR level.
     * @param msg сообщение
     */
    public void error(String msg){
        org.apache.log4j.Logger.getLogger(name).error(msg);
    }

    /**
     * Log message with FATAL level.
     * @param msg сообщение
     */
    public void fatal(String msg){
        org.apache.log4j.Logger.getLogger(name).fatal(msg);
    }

}
