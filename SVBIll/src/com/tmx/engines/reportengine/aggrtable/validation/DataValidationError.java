package com.tmx.engines.reportengine.aggrtable.validation;

import com.tmx.util.i18n.MessageResources;

import java.util.Locale;

/**
  
 */
public class DataValidationError {
    private final String VALIDATION_MSGS = "validation_msgs"; //resource file name
    private String messageKey = null;
    private String[] messageParams = null;

    DataValidationError(String messageKey, String[] messageParams){
        this.messageKey = messageKey;
        this.messageParams = messageParams;
    }

    public String toLocalizedString(Locale locale){
        return MessageResources.getInstance().getOwnedLocalizedMessage(this.getClass(), VALIDATION_MSGS, locale, messageKey, messageParams);
    }
}
