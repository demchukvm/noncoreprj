package com.tmx.web.actions.core.restriction.terminal_restriction;

import com.tmx.as.blogic.TerminalManager;
import com.tmx.as.blogic.restriction.AbstractRestrictionManager;
import com.tmx.as.blogic.restriction.TerminalRestrictionManager;
import com.tmx.as.entities.bill.restriction.AbstractRestriction;
import com.tmx.as.entities.bill.restriction.TerminalRestriction;
import com.tmx.as.entities.general.user.User;
import com.tmx.as.exceptions.DatabaseException;

import com.tmx.web.actions.core.restriction.AbstractInfoRestrictionAction;
import com.tmx.web.actions.core.restriction.AbstractManageRestrictionsAction;
import com.tmx.web.forms.core.restriction.AbstractInfoRestrictionForm;
import com.tmx.web.forms.core.restriction.terminal_restriction.InfoTerminalRestrictionForm;
import com.tmx.web.base.WebException;


public class InfoTerminalRestrictionAction extends AbstractInfoRestrictionAction {
    
    protected void preloadForCreate(AbstractInfoRestrictionForm form, AbstractRestriction restriction, User loggedInUser) throws DatabaseException, WebException {
        InfoTerminalRestrictionForm restrForm = (InfoTerminalRestrictionForm) form;
        String terminalId = getRoadmap().getParameterValue(getName(), AbstractManageRestrictionsAction.PARAMETER_OWNER_NAME, true).toString();
        restrForm.getBrowseBoxTerminals().setKey(terminalId);
        if (!"".equals(terminalId))
            restrForm.getBrowseBoxTerminals().setValue(new TerminalManager().getTerminal(terminalId).getSerialNumber());
// {!!!!} maybe needed in future
//        prepareRoadmap();
    }

    private void prepareRoadmap() throws WebException {
        getRoadmap().collectParameter("/core/restriction/terminal_restriction/terminalRestrictionParameter", "infoAction", getName());
        getRoadmap().collectParameter("/core/restriction/terminal_restriction/terminalRestrictionParameter", "paramAction", "/core/restriction/terminal_restriction/terminalRestrictionParameter");
    }

    protected void preloadForUpdate(AbstractInfoRestrictionForm form, AbstractRestriction restriction, User loggedInUser) throws DatabaseException, WebException {
        InfoTerminalRestrictionForm restrForm = (InfoTerminalRestrictionForm) form;
        String terminalId = ((TerminalRestriction) restriction).getTerminal().getTerminalId().toString();
        restrForm.getBrowseBoxTerminals().setKey(terminalId);
        restrForm.getBrowseBoxTerminals().setReadonly(true);
        restrForm.getBrowseBoxTerminals().setKey(((TerminalRestriction) restriction).getTerminal().getTerminalId().toString());
        restrForm.getBrowseBoxTerminals().setValue(((TerminalRestriction) restriction).getTerminal().getSerialNumber());
// {!!!!} maybe needed in future
//        prepareRoadmap();
    }

    protected AbstractRestrictionManager getRestrictionManager() {
        return new TerminalRestrictionManager();
    }

    protected void preloadForUpdate(AbstractInfoRestrictionForm form, AbstractRestriction restriction) throws DatabaseException {
        //do nothing
    }

}
