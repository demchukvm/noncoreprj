package com.tmx.beng.iso8583Gate.pdu;

import com.tmx.util.StructurizedException;
import java.util.Locale;

/**

 */
public class IsoSerializeException extends StructurizedException {

    public IsoSerializeException(String msg, String[] params, Throwable e, Locale locale) {
        super(msg, params, e, locale);
    }

    public IsoSerializeException(String msg, String[] params) {
        super(msg, params);
    }

    public IsoSerializeException(String msg, String[] params, Locale locale) {
        super(msg, params, locale);
    }

    public IsoSerializeException(String msg, Throwable e, Locale locale) {
        super(msg, e, locale);
    }

    public IsoSerializeException(String msg, Throwable e) {
        super(msg, e);
    }

    public IsoSerializeException(String msg, Locale locale) {
        super(msg, locale);
    }

    public IsoSerializeException(String msg) {
        super(msg);
    }

    public IsoSerializeException(String msg, String[] params, Throwable e) {
        super(msg, params, e);
    }
}

