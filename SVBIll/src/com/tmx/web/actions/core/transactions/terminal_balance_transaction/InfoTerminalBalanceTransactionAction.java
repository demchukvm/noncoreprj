package com.tmx.web.actions.core.transactions.terminal_balance_transaction;

import com.tmx.web.base.BasicDispatchedAction;
import com.tmx.web.forms.core.transaction.terminal_balance_transaction.InfoTerminalBalanceTransactionForm;
import com.tmx.as.blogic.TransactionManager;
import com.tmx.as.entities.bill.transaction.TerminalBalanceTransaction;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForm;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class InfoTerminalBalanceTransactionAction extends BasicDispatchedAction {
     private static final String FORWARD_INFO_TERMINAL_TRANSACTION_PAGE = "infoTerminalTransactionPage";

    public ActionForward defaultAction(ActionMapping mapping,
                                       ActionForm form,
                                       HttpServletRequest request,
                                       HttpServletResponse response,
                                       String methodName) throws Throwable {
        InfoTerminalBalanceTransactionForm infoTerminalBalanceTransactionForm =
                (InfoTerminalBalanceTransactionForm) verifyForm(form, InfoTerminalBalanceTransactionForm.class);

        infoTerminalBalanceTransactionForm.reset();
        TransactionManager transactionManager = new TransactionManager();

        TerminalBalanceTransaction terminalBalanceTransaction =
                transactionManager.getTerminalBalanceTransaction(infoTerminalBalanceTransactionForm.getSelectedTransactionId());

        infoTerminalBalanceTransactionForm.bindData(terminalBalanceTransaction);

        return mapping.findForward(FORWARD_INFO_TERMINAL_TRANSACTION_PAGE);
    }

    public ActionForward preloadForCancel(ActionMapping mapping,
                                       ActionForm form,
                                       HttpServletRequest request,
                                       HttpServletResponse response) throws Throwable {
        return mapping.findForward((String) getRoadmap().getParameterValue(this.getName(), "onCancelAction", true));
    }
}
