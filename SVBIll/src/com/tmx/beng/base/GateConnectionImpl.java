package com.tmx.beng.base;

import com.tmx.as.exceptions.DatabaseException;
import com.tmx.beng.access.GateConnection;
import com.tmx.util.cache.CacheException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;


public class GateConnectionImpl implements GateConnection {

    public Set<String> getMediumAccessPoolKeys() throws BillException {
        MediumAccessPool accessPool = BillingEngine.getInstance().getMediumAccessPool();
        return new HashSet<String>(accessPool.getMediumAccessPoolKeys());
    }

    public Set<String> getRemovedElemetsKeys() throws BillException {
        MediumAccessPool accessPool = BillingEngine.getInstance().getMediumAccessPool();
        return new HashSet<String>(accessPool.getRemovedElemetsKeys());
    }

    public void removeElements(Set<String> keys) throws BillException {
        MediumAccessPool accessPool = BillingEngine.getInstance().getMediumAccessPool();
        accessPool.removeElements(keys);
    }

    public void restoreElements(Set<String> keys) throws BillException {
        MediumAccessPool accessPool = BillingEngine.getInstance().getMediumAccessPool();
        accessPool.restoreElements(keys);
    }

    public HashMap getMediumMap() throws BillException {
        //return MediumResolver.getInstance().getMediumAccessName();
        return MediumResolver.getInstance().getMediumMap();
    }

    public HashMap getMediumAccessName() throws BillException {
        return MediumResolver.getInstance().getMediumAccessName();
    }


    public void changeMediumResolver(String productName, String gateName)
    {
        MediumResolver.getInstance().changeMediumResolver(productName, gateName);       
    }

    public ArrayList getTerminalList(String inChannelName, String outChannelName) throws CacheException {
        return ChannelResolver.getInstance().getTerminalList(inChannelName, outChannelName);
    }

    public void loadBeelineExc() throws DatabaseException {
        ChannelResolver.getInstance().loadBeelineExc();
    }

    public void loadKyivstarExc() throws DatabaseException {
        ChannelResolver.getInstance().loadKyivstarExc();
    }

    public void loadLifeExc() throws DatabaseException {
        ChannelResolver.getInstance().loadLifeExc();
    }

    public void loadMtsExc() throws DatabaseException {
        ChannelResolver.getInstance().loadMtsExc();
    }


}
