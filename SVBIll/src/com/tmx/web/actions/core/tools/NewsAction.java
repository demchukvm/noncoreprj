package com.tmx.web.actions.core.tools;

import com.tmx.as.base.EntityManager;
import com.tmx.as.base.criterion.CriterionWrapper;
import com.tmx.as.base.criterion.Restrictions;
import com.tmx.as.entities.bill.messages.Messages;
import com.tmx.web.actions.core.AbstractSvbillAction;
import com.tmx.web.forms.core.tools.NewsForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForm;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class NewsAction extends AbstractSvbillAction
{
    private static final String FORWARD_TRANSACTION_NEWS_PAGE = "transactionNewsPage";

    // тут формируем запрос
    public ActionForward defaultAction(ActionMapping mapping,
                                       ActionForm form,
                                       HttpServletRequest request,
                                       HttpServletResponse response,
                                       String methodName) throws Throwable {

        return saveMessage(mapping, form, request, response);

    }

    public ActionForward saveMessage(ActionMapping mapping,
                                        ActionForm form,
                                        HttpServletRequest request,
                                        HttpServletResponse response) throws Throwable {

        NewsForm newsForm = (NewsForm) form;
        String message = newsForm.getMessageText();
        if(!message.isEmpty() && !newsForm.getMessageTitle().isEmpty())
        {
            message = replaceNodeElements(message);
            EntityManager em = new EntityManager();
            Messages mes = new Messages();

            mes.setTitle(newsForm.getMessageTitle());
            mes.setText(message);

            em.SAVE(mes);


            CriterionWrapper criterions[] = new CriterionWrapper[] {
                Restrictions.eq("title", newsForm.getMessageTitle())};
            Messages mes2 = new Messages();

            mes2 = (Messages) em.RETRIEVE(Messages.class, null, criterions, null);
            newsForm.setResponseText(mes2.getText());
        }


        return mapping.findForward(FORWARD_TRANSACTION_NEWS_PAGE);
    }

    private String replaceNodeElements(String message)
    {
        // Replace
        message = message.replace("<","&lt;");
	    message = message.replace(">","&gt;");
	    message = message.replace("\r\n","<br>");
	    message = message.replace("(c)","&copy;");

        // Replace tags
        message = message.replace("[b]","<b>");
        message = message.replace("[/b]","</b>");
        message = message.replace("[u]","<u>");
        message = message.replace("[/u]","</u>");
        message = message.replace("[i]","<i>");
        message = message.replace("[/i]","</i>");
        message = message.replace("[left]","<div align=left>");
        message = message.replace("[/left]","</div>");
        message = message.replace("[right]","<div align=right>");
        message = message.replace("[/right]","</div>");
        message = message.replace("[center]","<div align=center>");
        message = message.replace("[/center]","</div>");
        message = message.replace("[justify]","<div align=justify>");
        message = message.replace("[/justify]","</div>");
        message = message.replace("[small]","<small>");
        message = message.replace("[/small]","</small>");
        message = message.replace("[big]","<big>");
        message = message.replace("[/big]","</big>");
        message = message.replace("[red]","&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");
        message = message.replace("[/red]","<br />");
        message = message.replace("[hr]","<hr>");

        // Replace img
        if (message.length()!=0) {
            while (message.indexOf("[img]")>=0 && message.indexOf("[/img]")>message.indexOf("[img]")) {
                String str1=message.substring(0,message.indexOf("[img]"));
                String str2=message.substring(message.indexOf("[/img]")+6,message.length());
                String substring=message.substring(message.indexOf("[img]")+5,message.indexOf("[/img]"));
                message=str1+"<img src='"+substring+"' alt='"+substring+"'>"+str2;
            }
        }
        
        return message;
    }

}