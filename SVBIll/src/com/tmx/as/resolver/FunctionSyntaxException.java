package com.tmx.as.resolver;

import com.tmx.util.StructurizedException;

import java.util.Locale;

public class FunctionSyntaxException extends StructurizedException {

    public FunctionSyntaxException(String errKey){
        super(errKey);
    }

    public FunctionSyntaxException(String errKey, Locale locale){
        super(errKey, locale);
    }

    public FunctionSyntaxException(String errKey, String[] params){
        super(errKey, params);
    }

    public FunctionSyntaxException(String errKey, String[] params, Throwable cause){
        super(errKey, params, cause);
    }

    public FunctionSyntaxException(String errKey, String[] params, Locale locale){
        super(errKey, params, locale);
    }

    public FunctionSyntaxException(String errKey, Throwable cause){
        super(errKey, cause);
    }

}
