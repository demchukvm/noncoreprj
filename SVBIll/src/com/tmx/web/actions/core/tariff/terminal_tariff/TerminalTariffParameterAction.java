package com.tmx.web.actions.core.tariff.terminal_tariff;

import com.tmx.as.blogic.balance.AbstractBalanceManager;
import com.tmx.as.blogic.balance.TerminalBalanceManager;
import com.tmx.web.actions.core.tariff.AbstractTariffParameterAction;
import com.tmx.web.base.SessionEnvironment;
import com.tmx.web.base.WebException;
import com.tmx.web.forms.core.tariff.AbstractInfoTariffForm;
import com.tmx.web.forms.core.tariff.terminal_tariff.InfoTerminalTariffForm;
import com.tmx.util.InitException;
import com.tmx.beng.base.BillException;
import org.apache.struts.action.ActionMapping;

import javax.servlet.http.HttpServletRequest;


public class TerminalTariffParameterAction extends AbstractTariffParameterAction {
    protected AbstractBalanceManager getBalanceManager() throws InitException, BillException {
        return new TerminalBalanceManager();
    }

    protected AbstractInfoTariffForm getInfoTariffForm(ActionMapping mapping, HttpServletRequest request) throws WebException {
        return (AbstractInfoTariffForm) SessionEnvironment.allocateForm(
                InfoTerminalTariffForm.class, "core.tariff.terminal_tariff.infoTerminalTariffForm", mapping, request);  
    }
}
