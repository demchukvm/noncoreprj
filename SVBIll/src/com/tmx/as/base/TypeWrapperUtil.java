package com.tmx.as.base;

import com.tmx.as.resolver.ResolverException;

import java.util.Date;

/**
 * To wrap types
 */
public class TypeWrapperUtil {

    public static Object parameterClassCasting(String currentParameterValue, String parameterType) throws ResolverException {
        Object newParameterValue = null;
        try {
            //RM: look here to define missing type
            // http://www.kickjava.com/src/org.hibernate.type.index.htm
            if (parameterType.toLowerCase().equals("long")) {
                newParameterValue = new org.hibernate.type.LongType().fromStringValue(currentParameterValue);
                return newParameterValue;
            }
            if (parameterType.toLowerCase().equals("short")) {
                newParameterValue = new org.hibernate.type.ShortType().fromStringValue(currentParameterValue);
                return newParameterValue;
            }
            if (parameterType.toLowerCase().equals("byte")) {
                newParameterValue = new org.hibernate.type.ByteType().fromStringValue(currentParameterValue);
                return newParameterValue;
            }
            if (parameterType.toLowerCase().equals("double")) {
                newParameterValue = new org.hibernate.type.DoubleType().fromStringValue(currentParameterValue);
                return newParameterValue;
            }
            if (parameterType.toLowerCase().equals("float")) {
                newParameterValue = new org.hibernate.type.FloatType().fromStringValue(currentParameterValue);
                return newParameterValue;
            }
            if (parameterType.toLowerCase().equals("integer")) {
                newParameterValue = new org.hibernate.type.IntegerType().fromStringValue(currentParameterValue);
                return newParameterValue;
            }
            if (parameterType.toLowerCase().equals("big_decimal")) {
                newParameterValue = new org.hibernate.type.BigDecimalType().fromStringValue(currentParameterValue);
                return newParameterValue;
            }
            if (parameterType.toLowerCase().equals("string")) {
                newParameterValue = new org.hibernate.type.StringType().fromStringValue(currentParameterValue);
                return newParameterValue;
            }
            if (parameterType.toLowerCase().equals("text")) {
                newParameterValue = new org.hibernate.type.TextType().fromStringValue(currentParameterValue);
                return newParameterValue;
            }
            if (parameterType.toLowerCase().equals("character")) {
                newParameterValue = new org.hibernate.type.CharacterType().fromStringValue(currentParameterValue);
                return newParameterValue;
            }
            if (parameterType.toLowerCase().equals("boolean")) {
                newParameterValue = new org.hibernate.type.BooleanType().fromStringValue(currentParameterValue);
                return newParameterValue;
            }
            if (parameterType.toLowerCase().equals("timestamp")) {
                newParameterValue = new Date(Long.parseLong(currentParameterValue));
                //newParameterValue = new org.hibernate.type.TimestampType().fromStringValue(currentParameterValue);
                return newParameterValue;
            }
            if (parameterType.toLowerCase().equals("time")) {
                newParameterValue = new org.hibernate.type.TimeType().fromStringValue(currentParameterValue);
                return newParameterValue;
            }
            if (parameterType.toLowerCase().equals("date")) {
                newParameterValue = new org.hibernate.type.DateType().fromStringValue(currentParameterValue);
                return newParameterValue;
            }
            if (parameterType.toLowerCase().equals("binary")) {
                newParameterValue = new org.hibernate.type.BinaryType().fromStringValue(currentParameterValue);
                return newParameterValue;
            }
/*
            if (parameterType.toLowerCase().equals("clob")) {
                newParameterValue = (java.sql.Clob) parameterValue;
                return newParameterValue;
            }
            if (parameterType.toLowerCase().equals("blob")) {
                newParameterValue = (java.sql.Blob) parameterValue;
                return newParameterValue;
            }
*/

            //the default value's type is "string"
            newParameterValue = new org.hibernate.type.StringType().fromStringValue(currentParameterValue);
            return newParameterValue;

        }
        catch (ClassCastException e) {
            throw new ResolverException("err.resolver.failed_to_cast_parameter_type", new String[]{parameterType, currentParameterValue}, e);
        }
    }

}
