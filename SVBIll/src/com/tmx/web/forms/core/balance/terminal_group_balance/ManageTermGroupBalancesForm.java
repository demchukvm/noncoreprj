package com.tmx.web.forms.core.balance.terminal_group_balance;

import com.tmx.web.forms.core.balance.AbstractManageBalancesForm;
import com.tmx.web.controls.table.Table;
import com.tmx.as.entities.general.user.User;


public class ManageTermGroupBalancesForm extends AbstractManageBalancesForm {

    protected void bindSpecificData(User loggedInUser) throws Throwable {

    }

    protected Table initBalancesTable() {
        Table table = new Table("query(query_type=retrieve_all," +
                "entity_class=com.tmx.as.entities.bill.balance.TerminalGroupBalance," +
                "table_attr=terminalGroup," +
                "order(name=id,type=asc)," +
                "page_size=15" +
                ")");
        table.setRefreshActionName("/core/balance/terminal_group_balance/manageTerminalGroupBalances");
        return table;
    }

    protected String initButtonsAction() {
        return "/core/balance/terminal_group_balance/infoTerminalGroupBalance";
    }
}
