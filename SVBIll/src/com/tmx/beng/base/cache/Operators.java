package com.tmx.beng.base.cache;

import com.tmx.as.base.EntityManager;
import com.tmx.as.base.FilterWrapper;
import com.tmx.as.entities.bill.balance.OperatorBalance;
import com.tmx.as.entities.bill.operator.Operator;
import com.tmx.as.entities.bill.restriction.OperatorRestriction;
import com.tmx.as.entities.bill.tariff.OperatorTariff;
import com.tmx.as.exceptions.DatabaseException;

import com.tmx.util.InitException;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 */
class Operators {
    Map operatorId_operator = new HashMap();
    Map operatorCode_amount = new HashMap();
    Map operatorCode_opeartor = new HashMap();

    Operators() throws DatabaseException, InitException {
        EntityManager entityManager = new EntityManager();

        // load operators
        Iterator it;
        OperatorWrapper wrapper;
        List list = entityManager.RETRIEVE_ALL(Operator.class, (FilterWrapper[]) null, new String[]{"primaryBalance"});
        for(it = list.iterator(); it.hasNext();){
            Operator operator = (Operator)it.next();
            if(operator.getPrimaryBalance() == null)
                throw new IllegalStateException("absent primary balance for operator - " + operator.getCode());
            wrapper = new OperatorWrapper(operator);
            operatorId_operator.put(operator.getOperatorId(), wrapper);
            operatorCode_opeartor.put(operator.getCode(), wrapper);
            operatorCode_amount.put(operator.getCode(), operator.getPrimaryBalance().getAmount());
        }

        // load operator ballance
        list  = entityManager.RETRIEVE_ALL(OperatorBalance.class, null);
        for(it = list.iterator(); it.hasNext();){
            OperatorBalance balance = (OperatorBalance)it.next();
            wrapper = (OperatorWrapper)operatorId_operator.get(balance.getOperatorId());
            if(wrapper != null && balance.getBalanceId() != null && balance.getCode() != null){
                wrapper.balanceId_balance.put(balance.getBalanceId(), balance);
                wrapper.balanceCode_balanceId.put(balance.getCode(), balance.getBalanceId());
            }
        }

        // load operator tariff
        list  = entityManager.RETRIEVE_ALL(OperatorTariff.class, (FilterWrapper[]) null, new String[]{"functionInstance"});
        for(it = list.iterator(); it.hasNext();){
            OperatorTariff tariff = (OperatorTariff)it.next();
            wrapper = (OperatorWrapper)operatorId_operator.get(tariff.getOperatorId());
            if(wrapper != null){
                wrapper.tariffId_tariffEntity.put(tariff.getTariffId(), tariff);
                FunctionWrapper functionWrapper = new FunctionWrapper(tariff.getFunctionInstance());
                wrapper.tariffId_tariffFunctionInstance.put(tariff.getTariffId(), functionWrapper);
                //cache tariff by name if name is not null
//                if(tariff.getName() != null){
//                    if(wrapper.tariffName_tariffFunctionInstance.containsKey(tariff.getName()))
//                        throw new InitException("Duplicate tariff name for operator", new String[]{tariff.getName(), wrapper.getOperator().getName()});
//                    wrapper.tariffName_tariffFunctionInstance.put(tariff.getName(), functionWrapper);
//                }
            }
        }

        // load operator restriction
        list  = entityManager.RETRIEVE_ALL(OperatorRestriction.class, (FilterWrapper[]) null, new String[]{"functionInstance"});
        for(it = list.iterator(); it.hasNext();){
            OperatorRestriction restriction = (OperatorRestriction)it.next();
            wrapper = (OperatorWrapper)operatorId_operator.get(restriction.getOperatorId());
            if(wrapper != null){
                FunctionWrapper functionWrapper = new FunctionWrapper(restriction.getFunctionInstance());
                wrapper.restriction.add(functionWrapper);
            }
        }
    }

    public Operator getOperator(Long id) {
        if(id == null)
            return null;
        OperatorWrapper wrapper = (OperatorWrapper)operatorId_operator.get(id);
        if(wrapper == null)
            return null;
        return wrapper.getOperator();
    }

    public OperatorWrapper getOperatorWrapper(String code){
        return (OperatorWrapper)operatorCode_opeartor.get(code);
    }
    
    public Operator getOperator(String code) {
        OperatorWrapper wrapper = getOperatorWrapper(code);
        if(wrapper == null) return null;
        return wrapper.getOperator();
    }
}
