package com.tmx.web.base;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

/**
Tomcat filter to apply Hibernate global filters
 */
public class ApplyGlobalFilter implements Filter {

    public void init(FilterConfig filterConfig) throws ServletException {
    }

    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) servletRequest;
        UserContextForm userContextForm = (UserContextForm)SessionEnvironment.getAttr(req.getSession(), SessionEnvironment.USER_CONTEXT_FORM);
        userContextForm.getGlobalFiltersHelper().applyGlobalFilters();
        filterChain.doFilter(servletRequest, servletResponse);
    }

    public void destroy() {
    }
}
