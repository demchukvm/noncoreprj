package com.tmx.as.entities.bill.voucher_pool;

import com.tmx.as.base.BasicEntity;
import com.tmx.as.entities.bill.seller.Seller;
import com.tmx.as.entities.bill.voucher.Voucher;

import java.io.Serializable;
import java.util.Set;

/**
 * Created by IntelliJ IDEA.
 * User: informix
 * Date: 24.04.2007
 * Time: 14:40:42
 * To change this template use File | Settings | File Templates.
 */
public class VoucherPool extends BasicEntity {
    private Long voucherPoolId;
    private Seller owner;
    private Set vouchers;



    public Set getVouchers() {
        return vouchers;
    }

    public void setVouchers(Set vouchers) {
        this.vouchers = vouchers;
    }
    public void addVoucher(Voucher voucher) {
            vouchers.add(voucher);
        }





    public int getValid() {
        return valid;
    }

    public void setValid(int valid) {
        this.valid = valid;
    }

    public Seller getOwner() {
        return owner;
    }

    public void setOwner(Seller owner) {
        this.owner = owner;
    }

    public Long getVoucherPoolId() {
        return voucherPoolId;
    }

    public void setVoucherPoolId(Long voucherPoolId) {
        this.voucherPoolId = voucherPoolId;
    }

    public Serializable getId() {
        return getVoucherPoolId();
    }

}
