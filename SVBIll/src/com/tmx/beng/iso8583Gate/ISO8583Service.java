package com.tmx.beng.iso8583Gate;

import com.tmx.util.queue.ActionQueue;
import com.tmx.util.InitException;
import com.tmx.util.Configuration;
import com.tmx.as.base.Reconfigurable;
import com.tmx.beng.iso8583Gate.base.ConnectionHandler;
import com.tmx.beng.iso8583Gate.base.ProtocolMetadata;
import com.tmx.beng.iso8583Gate.base.RetainingConnectionHandler;
import com.tmx.beng.iso8583Gate.base.beans.Iso8583ConfDocument;
import com.tmx.beng.iso8583Gate.mapper.BillingMapperMetadata;
import com.tmx.beng.base.BasicGate;
import com.tmx.beng.access.Access;

import java.net.ServerSocket;
import java.net.Socket;
import java.io.IOException;
import java.io.FileInputStream;
import java.util.*;

import org.apache.log4j.Logger;
import org.apache.xmlbeans.XmlOptions;

import javax.net.ServerSocketFactory;
import javax.net.ssl.SSLServerSocketFactory;

/**
 */
public class ISO8583Service extends BasicGate implements Reconfigurable {
    /** Only single instance available */
    private static ISO8583Service iso8583Service;
    /** Map of connection listeners: [String protocolName, ConnectionListener listener] */
    private Map connectionListeners;
    private ActionQueue connectionQueue;
    protected Logger logger = getLogger(this.getClass());
    protected Config serviceConfig = null;
    protected ProtocolMetadata protocolMetadata;
    /** billing engine access */
    private Access access;

    /** to hide default constructor */
    protected ISO8583Service() {
    }

    /** Return gate context instance */
    public static ISO8583Service getInstance(){
        if(iso8583Service == null)
            iso8583Service = new ISO8583Service();

        return iso8583Service;
    }

    public void reload() throws InitException {
        logger.info("ISO8583 service config reloading...");
        reloadConfig();
        //reload mapping
        BillingMapperMetadata.getInstance().reload();

                //get billing engine access
        access = obtainBillingEngineAccess();

        connectionQueue = new ActionQueue("ISO8583RequestsQueue", 1, serviceConfig.getMaxConnectionsHandler());

        com.tmx.beng.iso8583Gate.base.beans.ConnectionListener[] listeners = serviceConfig.getIso8583ConfDocument().getIso8583Conf().getListeners().getConnectionListenerArray();
        Map localConnectionListeners = new HashMap();
        for(int i=0; i<listeners.length; i++){
            localConnectionListeners.put(
                    listeners[i].getProtocolVersion(),
                    new ConnectionListener(
                            listeners[i].getPort(),
                            listeners[i].getMaxConnections(),
                            listeners[i].getBlocked(),
                            listeners[i].getProtocolVersion(),
                            listeners[i].getEnableSsl(),
                            listeners[i].getRetainTimeout()));
        }
        connectionListeners = localConnectionListeners;
        logger.info("ISO8583 service config reloaded");        
    }

    public void reloadConfig() throws InitException {
        String configFile = Configuration.getInstance().getProperty("iso8583service.config_file");
        if (configFile == null)
            throw new InitException("err.iso8583_service.config_file_is_not_specified", Locale.getDefault());

        //Load config from XML
        serviceConfig = new Config();
        serviceConfig.reload(configFile);

        protocolMetadata = new ProtocolMetadata();
        protocolMetadata.reload();
    }    

    public Config getConfig() {
        return serviceConfig;
    }

    public ProtocolMetadata getProtocolMetadata() {
        return protocolMetadata;
    }

    public void start() {
        for(Iterator iter = connectionListeners.values().iterator(); iter.hasNext();)
            ((ConnectionListener)iter.next()).start();
    }

    public void shutdown() {
        for(Iterator iter = connectionListeners.values().iterator(); iter.hasNext();)
            ((ConnectionListener)iter.next()).shutdown();
    }

    public static Logger getLogger(Class clentToUseIn) {
        return Logger.getLogger("iso8583Gate." + clentToUseIn);
    }

    public Access getBillingAccess(){
        return access;
    }

    private class ConnectionListener extends Thread {
        private Logger logger = ISO8583Service.getLogger(this.getClass());
        private boolean runnable = false;
        private ServerSocket serverSocket;
        private int maxConnections;
        private boolean blocked = false;
        private String version;
        private boolean enableSsl = false;
        private int retainTimeout;

        private ConnectionListener(int port, int maxConnections, boolean blocked, String version, boolean enableSsl, int retainTimeout) throws InitException {
            try {
                logger.debug("Creating connlsnr...");
                this.enableSsl = enableSsl;
                this.maxConnections = maxConnections;
                this.blocked = blocked;
                this.version = version;
                this.retainTimeout = retainTimeout;
                if(enableSsl){
                    serverSocket = SSLServerSocketFactory.getDefault().createServerSocket(port);
                }
                else{
                    serverSocket = new ServerSocket(port);
                }
                logger.debug(getSignature()+" created");
            }
            catch (IOException e) {
                logger.error("Failed to create connlsnr");
                throw new InitException("err.iso8583serivce.failed_to_create_server_socket", new String[]{String.valueOf(port)}, e);
            }
        }

        public void run() {
            if(blocked){
                logger.info(getSignature()+" Blocked");
                return;
            }
            else
                logger.info(getSignature()+" Started");


            Set sockets = new HashSet();
            runnable = true;
            while (runnable && !blocked) {
                Socket socket;
                try {
                    socket = serverSocket.accept();
                    logger.debug(getSignature() + " Client connection from " + socket.getInetAddress().getHostAddress() + " is accepted");
                    /** collect closed sockets and check count of opened sockets
                     * to prevent uncontrolled increasing of
                     * openned sockets in the conectionHandler queue waiting for
                     * free connectionHandler thread. 
                    */
                    sockets = collectClosedSockets(sockets);
                    if(sockets.size() + 1 > maxConnections){
                        //max sockets count exceeded - ignore new connection
                        //do not handle socket close by sending message to client
                        logger.error(getSignature()+" connection limit="+maxConnections+" exceeded");
                        socket.close();
                    }
                    else{
                        //handle connection
                        logger.info(getSignature() + " handling connection from " + socket.getInetAddress().getHostAddress());
                        sockets.add(socket);
                        //ConnectionHandler connHandler = new ConnectionHandler(socket, version);
                        RetainingConnectionHandler connHandler = new RetainingConnectionHandler(socket, version, retainTimeout);
                        connectionQueue.enqueue(connHandler);
                    }
                }
                catch (IOException e) {
                    logger.error(getSignature()+" Failed to accept client connection", e);
                }
		finally{
                    socket = null;
		}
            }
            logger.info(getSignature()+" Shutdown");
        }

        public void shutdown() {
            logger.info(getSignature()+" Stopping");
            runnable = false;
        }


        private Set collectClosedSockets(Set sockets){
            if(sockets == null)
                return sockets;

            Set collectedSockets = new HashSet();
            for(Iterator iter = sockets.iterator(); iter.hasNext();){
                Socket sock = (Socket)iter.next();
                if(!sock.isClosed())
                    collectedSockets.add(sock);
            }
            return collectedSockets;
        }

        private String getSignature(){
            return "connlsnr [Protocol ver="+version+", Port="+serverSocket.getLocalPort()+"]";
        }
    }

    public class Config{
        Iso8583ConfDocument configDocument;

        public void reload(String configFilePath) throws InitException{
            Iso8583ConfDocument localConfigDocument;
            FileInputStream fis = null;
            try{
                fis = new FileInputStream(configFilePath);
                // Bind the instance to the generated XMLBeans types.
                localConfigDocument = Iso8583ConfDocument.Factory.parse(fis);
            }
            catch(Throwable e){
                throw new InitException("err.iso8583service.failed_to_parse_config", new String[]{configFilePath}, e);
            }
            finally{
                try{if(fis != null)
                        fis.close();}
                catch(IOException e){}
            }

            //validate
            ArrayList errors = new ArrayList();
            XmlOptions opts = new XmlOptions();
            opts.setErrorListener(errors);
            if(!localConfigDocument.validate(opts)){
                StringBuffer errorBuffer = new StringBuffer("Validation failed of XML "+configFilePath+":\n");
                Iterator iter = errors.iterator();
                while(iter.hasNext())
                    errorBuffer.append("   >> " + iter.next() + "\n");
                logger.error(errorBuffer.toString());
                throw new InitException("err.iso8583service.config_validation_failed", new String[]{configFilePath});
            }
            //assign to property
            configDocument = localConfigDocument;
        }

        public Iso8583ConfDocument getIso8583ConfDocument(){
            return configDocument;
        }

        /** Evluate max connectionsHandlers as sum of all listeners max connections count */
        public int getMaxConnectionsHandler(){
            int allListenersMaxConnections = 0;
            com.tmx.beng.iso8583Gate.base.beans.ConnectionListener[] listeners = serviceConfig.getIso8583ConfDocument().getIso8583Conf().getListeners().getConnectionListenerArray();
            for(int i=0; i<listeners.length; i++)
                allListenersMaxConnections =+ listeners[i].getMaxConnections();
            return allListenersMaxConnections;
        }

    }
}
