package com.tmx.as.entities.mobipay.dacard;

import com.tmx.as.base.BasicEntity;

import java.util.Date;
import java.io.Serializable;

/**
 */
public class DacardGateReconciliation extends BasicEntity {
    private Long dacardGateReconciliationId;
    private Date reconDate; //yyyyMMdd
    private Integer reconciliationIndicator;
    private String stan;
    private String status;
    private String responseCode;
    private Integer attemptsCount;
    private Date startTime;
    private Date endTime;
    //set of fields for operation code #791 ('voucherSaleAndActivation')
    private Integer oc791OperationCount;
    private Integer oc791TotalAmount;
    private String oc791ResponseCode;


    public Long getDacardGateReconciliationId() {
        return dacardGateReconciliationId;
    }

    public void setDacardGateReconciliationId(Long dacardGateReconciliationId) {
        this.dacardGateReconciliationId = dacardGateReconciliationId;
    }

    public Date getReconDate() {
        return reconDate;
    }

    public void setReconDate(Date reconDate) {
        this.reconDate = reconDate;
    }

    public Integer getReconciliationIndicator() {
        return reconciliationIndicator;
    }

    public void setReconciliationIndicator(Integer reconciliationIndicator) {
        this.reconciliationIndicator = reconciliationIndicator;
    }

    public String getStan() {
        return stan;
    }

    public void setStan(String stan) {
        this.stan = stan;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    public Integer getAttemptsCount() {
        return attemptsCount;
    }

    public void setAttemptsCount(Integer attemptsCount) {
        this.attemptsCount = attemptsCount;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public Integer getOc791OperationCount() {
        return oc791OperationCount;
    }

    public void setOc791OperationCount(Integer oc791OperationCount) {
        this.oc791OperationCount = oc791OperationCount;
    }

    public Integer getOc791TotalAmount() {
        return oc791TotalAmount;
    }

    public void setOc791TotalAmount(Integer oc791TotalAmount) {
        this.oc791TotalAmount = oc791TotalAmount;
    }

    public String getOc791ResponseCode() {
        return oc791ResponseCode;
    }

    public void setOc791ResponseCode(String oc791ResponseCode) {
        this.oc791ResponseCode = oc791ResponseCode;
    }

    public Serializable getId() {
        return getDacardGateReconciliationId();
    }

    public int getValid() {
        return valid;
    }

    public void setValid(int valid) {
        this.valid = valid;
    }
}
