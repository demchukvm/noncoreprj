package com.tmx.web.actions.core.tools;

import com.tmx.beng.csapi.java.base.BillingClientApi;
import com.tmx.beng.access.Connection;
import com.tmx.beng.base.BillingMessage;
import com.tmx.beng.base.BillingMessageImpl;
import com.tmx.beng.base.BillingMessageWritable;
import com.tmx.web.base.BasicDispatchedAction;
import com.tmx.web.forms.core.tools.InfoDBCacheForm;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class InfoDBCacheAction extends BasicDispatchedAction {

     public ActionForward defaultAction(ActionMapping mapping,
                                       ActionForm form,
                                       HttpServletRequest request,
                                       HttpServletResponse response,
                                       String methodName) throws Throwable {

         InfoDBCacheForm cacheForm = (InfoDBCacheForm) form; 
         BillingMessageWritable msg = new BillingMessageImpl();
         msg.setOperationName(BillingMessage.O_DB_CACHE_DUMP);
         Connection connection = new BillingClientApi().openBillingConnection();
         connection.processSync(msg);
         cacheForm.reset();
         return mapping.findForward("DBCacheInfo");
    }    

}
