package com.tmx.web.forms.core.tariff.terminal_tariff;

import com.tmx.as.entities.bill.tariff.AbstractTariff;
import com.tmx.as.entities.bill.tariff.TerminalTariff;
import com.tmx.as.entities.bill.terminal.Terminal;
import com.tmx.as.entities.general.user.User;
import com.tmx.as.exceptions.DatabaseException;
import com.tmx.web.controls.BrowseBox;
import com.tmx.web.controls.PopulateUtil;
import com.tmx.web.forms.core.tariff.AbstractInfoTariffForm;


public class InfoTerminalTariffForm extends AbstractInfoTariffForm {
    
    //maybe mandutory???
    private BrowseBox browseBoxTerminals;

    protected void init() {
        super.init();
        initButtons();
        initBrowses();
        initParameterButtonsAction();
        initTables();
    }

    private void initTables(){
        if(getFuncParametrsTable() != null){
            getFuncParametrsTable().setRefreshActionName("/core/tariff/terminal_tariff/infoTerminalTariff");
        }
    }

    private void initBrowses(){
        browseBoxTerminals = new BrowseBox("/core/balance/terminal_balance/browseTerminalsAction");
        getBrowseBoxFunctionTypes().setAction("/core/tariff/terminal_tariff/browseTerminalFuncTypesAction");
        browseBoxTerminals.setMandatory(true);
    }


    

    private void initButtons(){
        getOnSaveButton().setAction("/core/tariff/terminal_tariff/infoTerminalTariff");
        getOnApplyButton().setAction("/core/tariff/terminal_tariff/infoTerminalTariff");
        getOnCancelButton().setAction("/core/tariff/terminal_tariff/manageTerminalTariffs");

        getOnRefreshParamButton().setAction("/core/tariff/terminal_tariff/infoTerminalTariff");
        getOnCreateParamButton().setAction("/core/tariff/terminal_tariff/infoTerminalTariff");
        getOnEditParamButton().setAction("/core/tariff/terminal_tariff/terminalTariffParameter");
        getOnDeleteParamButton().setAction("/core/tariff/terminal_tariff/terminalTariffParameter");
    }

    protected AbstractTariff populateFromSpecificControls() {
        TerminalTariff tariff = new TerminalTariff();
        tariff.setTerminal(getTerminalInstance());
        return tariff;
    }

    private Terminal getTerminalInstance() {
        Terminal terminal = new Terminal();
        terminal.setTerminalId(PopulateUtil.getLongKey(browseBoxTerminals));
        terminal.setSerialNumber(browseBoxTerminals.getValue());
        return terminal;
    }


    protected void bindSpecificData(AbstractTariff restriction, User loggedInUser) {
        TerminalTariff tariff = (TerminalTariff) restriction;
        if(tariff.getTerminal()!=null && tariff.getTerminal().getTerminalId()!=null){// if terminal is present!
            browseBoxTerminals.setKey(tariff.getTerminal().getTerminalId().toString());
            browseBoxTerminals.setValue(tariff.getTerminal().getSerialNumber());
        }
        browseBoxTerminals.setReadonly(true);
    }

    private void initParameterButtonsAction(){
        setOnApplyButtonAction("/core/tariff/terminal_tariff/terminalTariffParameter");
        setOnCancelButtonAction("/core/tariff/terminal_tariff/infoTerminalTariff");
        setOnSaveButtonAction("/core/tariff/terminal_tariff/terminalTariffParameter");
    }

    protected void bindSpecificData(User loggedInUser) throws DatabaseException {
        browseBoxTerminals.setReadonly(false);
    }

    public BrowseBox getBrowseBoxTerminals() {
        return browseBoxTerminals;
    }

    public void setBrowseBoxTerminals(BrowseBox browseBoxTerminals) {
        this.browseBoxTerminals = browseBoxTerminals;
    }
}
