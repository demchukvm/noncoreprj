package com.tmx.web.taglib.html;

import org.apache.struts.taglib.TagUtils;

import javax.servlet.jsp.JspException;
import javax.servlet.http.HttpServletRequest;

import com.tmx.util.StructurizedException;
import com.tmx.web.base.RequestEnvironment;
import com.tmx.web.base.SessionEnvironment;
import com.tmx.web.base.ApplicationEnvironment;
import com.tmx.web.taglib.base.LocalizableTag;

import java.util.Locale;
import java.util.List;
import java.util.Iterator;
import java.util.Arrays;

/**
Publish error
 */
public class WriteErrorTag extends LocalizableTag {
    /** The property error has assigned with */
    protected String property;
    private String errorMsgStyleClass = null;
    private String errorCauseStyleClass = null;
    private String errorStackStyleClass = null;
    private String errorLabelStyleClass = null;
    private String imgRollout = null;
    private String imgRollin = null;
    //---------Constants to render HTML and JS
    private final String ERROR_SWITCH_IMG = "error_switch_img";
    private final String STACK_SWITCH_IMG_PREFIX = "stack_switch_img_";
    private final String CAUSE_LABEL_PREFIX = "cause_label_";
    private final String CAUSE_BODY_PREFIX = "cause_body_";
    private final String STACK_LABEL_PREFIX = "stack_label_";
    private final String STACK_BODY_PREFIX = "stack_body_";

    //--------------------Properties
    public String getProperty() {
        return property;
    }

    public void setProperty(String property) {
        this.property = property;
    }

    public String getErrorMsgStyleClass() {
        return errorMsgStyleClass;
    }

    public void setErrorMsgStyleClass(String errorMsgStyleClass) {
        this.errorMsgStyleClass = errorMsgStyleClass;
    }

    public String getErrorCauseStyleClass() {
        return errorCauseStyleClass;
    }

    public void setErrorCauseStyleClass(String errorCauseStyleClass) {
        this.errorCauseStyleClass = errorCauseStyleClass;
    }

    public String getErrorStackStyleClass() {
        return errorStackStyleClass;
    }

    public void setErrorStackStyleClass(String errorStackStyleClass) {
        this.errorStackStyleClass = errorStackStyleClass;
    }

    public String getImgRollout() {
        return imgRollout;
    }

    public void setImgRollout(String imgRollout) {
        this.imgRollout = imgRollout;
    }

    public String getImgRollin() {
        return imgRollin;
    }

    public void setImgRollin(String imgRollin) {
        this.imgRollin = imgRollin;
    }

    public String getErrorLabelStyleClass() {
        return errorLabelStyleClass;
    }

    public void setErrorLabelStyleClass(String errorLabelStyleClass) {
        this.errorLabelStyleClass = errorLabelStyleClass;
    }

    //------------------------------Parsing events
    public int doStartTmxTag() throws JspException {
        String errorHTML = renderErrorHTML();
        TagUtils.getInstance().write(pageContext, errorHTML);
        return (EVAL_BODY_TAG);
    }

    //------------------------------Rendering methods
    private String renderErrorHTML() throws JspException {
        StringBuffer resultHTML = new StringBuffer();
        StructurizedException strException = RequestEnvironment.getError((HttpServletRequest) pageContext.getRequest(), getProperty());
        Locale locale = SessionEnvironment.getLocale(((HttpServletRequest) pageContext.getRequest()).getSession());

        //add top error
        resultHTML.append("<div class='"+getErrorLabelStyleClass()+"'>");
        resultHTML.append("<img id='"+ERROR_SWITCH_IMG+"' onclick='javascript:switchErrors(this);'/>&nbsp;");
        resultHTML.append(getLocalizedLabel("tag.writeError.label.error", null)+"</div>\r");
        resultHTML.append("<div class='"+getErrorMsgStyleClass()+"'>\r");
        resultHTML.append(strException.getLocalizedMessage(locale)+"\r");
        resultHTML.append("</div>\r");

        renderErrorStackHTML(resultHTML, strException, 0);

        //add ordered list of caused
        List causes = strException.getCauses();
        int causeIdx = 1;
        for(Iterator iter = causes.iterator(); iter.hasNext();){
            resultHTML.append("<div id='"+CAUSE_LABEL_PREFIX+causeIdx+"' class='"+getErrorLabelStyleClass()+"'>");
            resultHTML.append("<hr/>\r");
            resultHTML.append(getLocalizedLabel("tag.writeError.label.cause", null)+"</div>\r");
            resultHTML.append("<div id='"+CAUSE_BODY_PREFIX+causeIdx+"' class='"+getErrorCauseStyleClass()+"'>\r");
            Throwable cause = (Throwable)iter.next();
            if(cause instanceof StructurizedException){
                resultHTML.append(((StructurizedException)cause).getLocalizedMessage(locale)+"\r");
            }
            else{
                resultHTML.append(cause.toString()+"\r");
            }
            resultHTML.append("</div>\r");

            renderErrorStackHTML(resultHTML, cause, causeIdx);
            causeIdx++;
        }

        renderJS(resultHTML, causeIdx--);

        return resultHTML.toString();
    }

    //add error stack trace
    private void renderErrorStackHTML(StringBuffer resultHTML, Throwable e, int idx){
        resultHTML.append("<div id='"+STACK_LABEL_PREFIX+idx +"' class='"+getErrorLabelStyleClass()+"'>");
        resultHTML.append("<img id='"+STACK_SWITCH_IMG_PREFIX+idx+"' onclick='javascript:switchStackBody(this, "+idx +");'/>&nbsp;");
        resultHTML.append(getLocalizedLabel("tag.writeError.label.stack", null)+"</div>\r");
        resultHTML.append("<div id='"+STACK_BODY_PREFIX+idx +"' class='"+getErrorStackStyleClass()+"'>\r");
        String stackTrace = "";
        Iterator stackTraces = Arrays.asList(e.getStackTrace()).iterator();
        while(stackTraces.hasNext())
            stackTrace = stackTrace + ((StackTraceElement)stackTraces.next()).toString() + "<br/>\r";

        resultHTML.append(stackTrace);
        resultHTML.append("</div>\r");
    }

    //render javascript functions
    private void renderJS(StringBuffer resultHTML, int causesCount){
        resultHTML.append("<script language=\"JavaScript\">\r");
        resultHTML.append("   var errorRolloutImgSrc = '"+ApplicationEnvironment.getAppDeploymentName()+getImgRollout()+"';\r");
        resultHTML.append("   var errorRollinImgSrc = '"+ApplicationEnvironment.getAppDeploymentName()+getImgRollin()+"';\r");

        resultHTML.append("   errorImg = document.getElementById(\""+ERROR_SWITCH_IMG+"\");\r");
        resultHTML.append("   errorImg.src = errorRollinImgSrc;\r");
        resultHTML.append("   errorImg.setAttribute('state', 'rolledIn');\r");

        resultHTML.append("   switchDetailsDivs('none');\r");

        resultHTML.append("   function switchErrors(img){\r");
        resultHTML.append("     if(img.getAttribute('state') == null || img.getAttribute('state') == 'rolledIn'){\r");
        resultHTML.append("         switchDetailsDivs('block');\r");
        resultHTML.append("     }\r");
        resultHTML.append("     else{\r");
        resultHTML.append("         switchDetailsDivs('none');\r");
        resultHTML.append("     }\r");
        resultHTML.append("     switchImg(img);\r");
        resultHTML.append("   }\r");

        resultHTML.append("   function switchDetailsDivs(mode){\r");
        for(int i=0; i<causesCount; i++){
            resultHTML.append("   stackLabelDiv = document.getElementById(\""+STACK_LABEL_PREFIX+i+"\");\r");
            resultHTML.append("   stackLabelDiv.style.display=mode;\r");

            resultHTML.append("   if(mode == 'none'){\r");
            resultHTML.append("     stackBodyDiv = document.getElementById(\""+STACK_BODY_PREFIX+i+"\");\r");
            resultHTML.append("     stackBodyDiv.style.display=mode;\r");
            resultHTML.append("     errorImg = document.getElementById(\""+STACK_SWITCH_IMG_PREFIX+i+"\");\r");
            resultHTML.append("     errorImg.src = errorRollinImgSrc;\r");
            resultHTML.append("     errorImg.setAttribute('state', 'rolledIn');\r");
            resultHTML.append("   }\r");

            if(i != 0){
                resultHTML.append("   causeLabelDiv = document.getElementById(\""+CAUSE_LABEL_PREFIX+i+"\");\r");
                resultHTML.append("   causeLabelDiv.style.display=mode;\r");
                resultHTML.append("   causeBodyDiv = document.getElementById(\""+CAUSE_BODY_PREFIX+i+"\");\r");
                resultHTML.append("   causeBodyDiv.style.display=mode;\r");
            }
        }
        resultHTML.append("   }\r");

        resultHTML.append("   function switchStackBody(img, stackId){\r");

        resultHTML.append("     stackDivName = '"+STACK_BODY_PREFIX+"'+stackId;\r");
        resultHTML.append("     stackDiv = document.getElementById(\""+STACK_BODY_PREFIX+"\"+stackId);\r");
        resultHTML.append("     if(img.getAttribute('state') == null || img.getAttribute('state') == 'rolledIn'){\r");
        resultHTML.append("         stackDiv.style.display='block';\r");
        resultHTML.append("     }\r");
        resultHTML.append("     else{\r");
        resultHTML.append("         stackDiv.style.display='none';\r");
        resultHTML.append("     }\r");
        resultHTML.append("     switchImg(img);\r");
        resultHTML.append("   }\r");

        resultHTML.append("   function switchImg(img){\r");
        resultHTML.append("     if(img.getAttribute('state') == null || img.getAttribute('state') == 'rolledIn'){\r");
        resultHTML.append("         img.setAttribute('state', 'rolledOut');\r");
        resultHTML.append("         img.src = errorRolloutImgSrc;\r");
        resultHTML.append("     }\r");
        resultHTML.append("     else{\r");
        resultHTML.append("         img.setAttribute('state', 'rolledIn');\r");
        resultHTML.append("         img.src = errorRollinImgSrc;\r");
        resultHTML.append("     }\r");
        resultHTML.append("   }\r");
        resultHTML.append("</script>");
    }

}
