package com.tmx.beng.base.processors;

import com.tmx.beng.base.*;
import com.tmx.beng.base.cache.DBCache;
import com.tmx.beng.access.Access;
import com.tmx.util.InitException;

import javax.naming.InitialContext;
import javax.naming.NamingException;


public class ReloadConfigPropertiesProcessor extends TaskProcessor {

    protected Task process0(Task task, DBCache dbCache, BillingMessageWritable msg) throws BillException {
        logger.debug("Reload Config Properties processor ... ");

        try {
            long startTime = System.currentTimeMillis();
            task.getGlobalContext().reloadDBCache();
            logger.debug( "Time (ms): " + (System.currentTimeMillis() - startTime) );
        } catch (InitException e) {
            throw new BillException("err_dbcache_reloading",e);
        }

        BillingMessageWritable respMsg = new BillingMessageImpl();
        respMsg.setAttribute(BillingMessage.STATUS_CODE, new Integer(0));
        respMsg.setAttribute(BillingMessage.WAIT_TIME, String.valueOf(task.getGlobalContext().getReporter().getWaitTimeEstimate()));

        CallBack callBack = task.getCallBack();
        if(callBack != null)
            callBack.sendBilledMessage(respMsg);

        return task;
    }

}