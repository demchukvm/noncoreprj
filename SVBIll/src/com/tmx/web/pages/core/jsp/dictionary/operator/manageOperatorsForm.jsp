<%@ page language="java" pageEncoding="UTF-8" contentType="text/html; charset=utf-8" isELIgnored="false" %>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-nested" prefix="nested" %>
<%@ taglib uri="http://tmx.com/taglib/tmx-controls" prefix="tmx_ctrl" %>
<%@ taglib uri="http://tmx.com/taglib/tmx-logic" prefix="tmx_logic" %>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html" %>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean" %>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>

<html:form action="/core/dictionary/operator/manageOperators">
<table width="100%" border="0">
    <!-- BUTTONS -->
    <tr>
        <td style="text-align: left">
            <tmx_ctrl:button property="onCreateButton"/>
            <tmx_ctrl:button property="onEditButton"/>
            <tmx_ctrl:button property="onDeleteButton"/>            
        </td>
    </tr>
    <!--FILTER-->
    <tr>
        <td valign="top" width="100%" align="left">
            <tmx_ctrl:panelFilterSet tableName="operatorsTable">
            <table>
                <tr>
                    <td class="dataLabel">
                        <tmx_ctrl:writeEntityLabel
                                attrName="{@name}"
                                entityClassName="com.tmx.as.entities.bill.operator.Operator"/>
                    </td>
                    <td class="dataField">
                        <tmx_ctrl:selectBox
                                property="operatorsTable.filterSet.filter(by_id).parameter(id).control"/>
                    </td>
                </tr>
            </table>
        </tmx_ctrl:panelFilterSet>
        </td>
    </tr>
     <!-- TABLE -->
    <tr>
        <td width="100%">
            <table class='one_outer_cell_table' cellspacing='0' cellpadding='0' width="100%">
                <tr>
                    <td>
                        <table cellspacing='1' cellpadding='1' width="100%">
                            <!-- Table header -->
                            <tr>

                                <th class="column_header_cell">
                                    <bean:message bundle="core.labels" key="core.common.th.choose"/>
                                </th>

                                <th class="column_header_cell" width="110">
                                    <tmx_ctrl:imgTableOrdering tableName="operatorsTable"
                                                               orderByFieldName="code"
                                                               labelBundle="as:com.tmx.as.entities.bill.operator.Operator"
                                                               labelKey="code"/>
                                </th>

                                <th class="column_header_cell" width="110">
                                    <tmx_ctrl:imgTableOrdering tableName="operatorsTable"
                                                               orderByFieldName="name"
                                                               labelBundle="as:com.tmx.as.entities.bill.operator.Operator"
                                                               labelKey="name"/>
                                </th>

                                <th class="column_header_cell" width="110">
                                    <tmx_ctrl:imgTableOrdering tableName="operatorsTable"
                                                               orderByFieldName="description"
                                                               labelBundle="as:com.tmx.as.entities.bill.operator.Operator"
                                                               labelKey="description"/>
                                </th>

                                <th class="column_header_cell" width="110">
                                    <tmx_ctrl:imgTableOrdering tableName="operatorsTable"
                                                               orderByFieldName="registrationDate"
                                                               labelBundle="as:com.tmx.as.entities.bill.operator.Operator"
                                                               labelKey="registrationDate"/>
                                </th>
                            </tr>
                            <nested:iterate id="operator" property="operatorsTable.data" type="com.tmx.as.entities.bill.operator.Operator">
                                <tr>
                                    <td class='column_data_cell'>
                                        <input type="radio" name="selectedOperatorId" value="<c:out value="${operator.operatorId}" />"
                                               checked="false">
                                    </td>
                                    <td class='column_data_cell'>
                                        <c:out value="${operator.code}" />
                                    </td>
                                    <td class='column_data_cell'>
                                        <c:out value="${operator.name}" />
                                    </td>
                                    <td class='column_data_cell'>
                                        <c:out value="${operator.description}"/>
                                    </td>
                                    <td class='column_data_cell'>
                                        <fmt:formatDate value="${operator.registrationDate}" pattern="yyyy/MM/dd HH:mm:ss"/>
                                    </td>
                                </tr>
                            </nested:iterate>
                            <!-- Empty row -->
                            <nested:define id="tableSize" property="operatorsTable.resultSize" type="java.lang.Integer"/>
                            <c:if test="${tableSize <= 0}">
                                <tr>
                                    <td class='column_data_cell' colspan="5" align="center">
                                        <bean:message bundle="core.labels" key="core.common.th.empty_table" />
                                    </td>
                                </tr>
                            </c:if>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td style="text-align: right">
            <tmx_ctrl:imgTablePaging maxBookmarkCount="10" tableName="operatorsTable"/>
        </td>
    </tr>
    <!-- BUTTONS -->
    <tr>
        <td style="text-align: left">
            <tmx_ctrl:button property="onCreateButton"/>
            <tmx_ctrl:button property="onEditButton"/>
            <tmx_ctrl:button property="onDeleteButton"/>
        </td>
    </tr>
</table>
</html:form>