<?php
/*
  Template Name: Feedback
 */
?>
<?php get_header();?>
<div class="feedback_currblock">

	<div class="left">
		<img src="<?php bloginfo('template_url');?>/images/feedback-shape<?php if(isset($_GET['lang']) & $_GET['lang'] == 'en'){echo '-eng';}?>.gif" alt="Обратная связь">
	</div>
	
	<div class="right">
		<p><?php echo get_field('text_1');?></p>
	</div>

</div><!--.about_currblock-->


<div class="quest_text">

	<h2><?php echo get_field('text_2');?></h2>


	<div class="left">
	
		<ul>
			<li>
			<b><?php echo get_field('text_3');?></b>
			<?php echo get_field('text_4');?>
			</li>
			
			<li>
			<b><?php echo get_field('text_5');?></b>
                        <?php echo get_field('text_6');?>
			</li>
		</ul>
	</div>
	
	<div class="center">
		<ul>
			<li>
			<b><?php echo get_field('text_7');?></b><?php echo get_field('text_8');?>
			</li>
		</ul>
	</div>
	
	<div class="right">
		<ul>
			<li>
			<b><?php echo get_field('text_9');?></b><?php echo get_field('text_91');?>
			</li>
		</ul>		
		<!--<p><?php echo get_field('text_9');?></p>-->
	</div>

</div><!--.about_text-->


<div class="feedback_block">
    <h2><?php echo get_field('text_10');?></h2>

    <form method="POST">
		<input type="hidden" name="type" value="feedb" id="form_type" />
        <div class="left">
            <div class="name"><span><?php echo get_field('text_11');?></span> <input type="text" name="name" id="form_name" value=""></div>
            <div class="mail"><span><?php echo get_field('text_12');?></span> <input type="text" name="mail" id="form_mail" value=""></div>
            <div class="phone"><span><?php echo get_field('text_13');?></span> <input type="text" name="phone" id="form_phone" value=""></div>
        </div>

        <div class="right">

            <div class="message">
                <span><?php echo get_field('text_14');?></span>
                <textarea id="form_message" name="text"></textarea>
            </div>
            <div id="send_form_button" style="background: #f1b40e url('<?php bloginfo('template_url');?>/images/submit_btn<?php if(isset($_GET['lang']) & $_GET['lang'] == 'en'){echo '-eng';}?>.gif') no-repeat;"></div>
        </div>
    </form>

</div>

<?php get_footer(); ?>