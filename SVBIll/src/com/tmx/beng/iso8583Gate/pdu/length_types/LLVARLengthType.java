package com.tmx.beng.iso8583Gate.pdu.length_types;

import com.tmx.beng.iso8583Gate.pdu.IsoParseException;
import com.tmx.beng.iso8583Gate.pdu.IsoSerializeException;
import com.tmx.beng.iso8583Gate.pdu.IncorrectFormatException;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 */
public class LLVARLengthType extends LengthType {
    {
        name = "LLVAR";
    }

    public int parseLength(InputStream is, int maxLEngth) throws IsoParseException {
        try{
            byte[] length = new byte[1];
            is.read(length);
            //length could not be more then int
            return (int)bcdConverter.getLongFromBcd(length, 2/** length value in 2 nibbles */);
        }
        catch(IOException e){
            throw new IsoParseException("err.llvar_lengthtype.parsing_failed", e);
        }
        catch(IncorrectFormatException e){
            throw new IsoParseException("err.llvar_lengthtype.parsing_failed", e);
        }
    }

//    public void serializeLength(OutputStream os, int length) throws IsoSerializeException {
//        try{
//            byte[] bcdLength = bcdConverter.getUnsignedBcdFromLong(length);
//            if(bcdLength.length > 1)
//                throw new IsoSerializeException("err.llvar_lengthtype.place_is_not_enough_for_value", new String[]{String.valueOf(length)});
//            os.write(bcdLength);
//        }
//        catch(IOException e){
//            throw new IsoSerializeException("err.llvar_lengthtype.serialization_failed", new String[]{String.valueOf(length)}, e);
//        }
//    }

    public byte[] serializeLength(int length) throws IsoSerializeException {
        byte[] bcdLength = bcdConverter.getUnsignedBcdFromLong(length);
        if (bcdLength.length > 1)
            throw new IsoSerializeException("err.llvar_lengthtype.place_is_not_enough_for_value", new String[]{String.valueOf(length)});
        return bcdLength;
    }    
}
