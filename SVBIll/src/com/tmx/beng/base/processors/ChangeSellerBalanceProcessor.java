package com.tmx.beng.base.processors;

import com.tmx.as.base.ProcedureWrapper;
import com.tmx.as.base.QueryParameterWrapper;
import com.tmx.as.entities.bill.balance.SellerBalance;
import com.tmx.as.entities.bill.seller.Seller;
import com.tmx.as.entities.bill.transaction.SellerBalanceTransaction;
import com.tmx.as.entities.bill.transaction.TransactionSupport;
import com.tmx.as.entities.bill.transaction.TransactionType;
import com.tmx.as.entities.general.user.User;
import com.tmx.beng.base.*;
import com.tmx.beng.base.cache.DBCache;
import com.tmx.beng.base.cache.DBCacheException;

import java.sql.Types;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class ChangeSellerBalanceProcessor extends TaskProcessor{

    protected Task process0(Task task, DBCache dbCache, BillingMessageWritable msg) throws BillException {
//        try {
//            //create
//            SellerBalanceTransaction sellerBalanceTransaction = new SellerBalanceTransaction();
//
//            //setup
//            sellerBalanceTransaction.setTransactionTime( getTransRegTime(msg) );
//            sellerBalanceTransaction.setClientTime( getClientTime(msg) );
//            sellerBalanceTransaction.setAmount( getAmount(msg) );
//            sellerBalanceTransaction.setSeller( getSeller(msg, dbCache) );
//            sellerBalanceTransaction.setSellerBalance( getSellerBalance(msg, dbCache) );
//            sellerBalanceTransaction.setTransactionSupport( getTransactionSupport(msg) );
//            sellerBalanceTransaction.setTransactionGUID( getTransactionGUID(msg) );
//            sellerBalanceTransaction.setCliTransactionNum( getCliTransactionNum(msg) );
//            sellerBalanceTransaction.setType( getTransactionType(msg) );
//
//            dbCache.reserveSellerBalance(msg.getAttributeString(BillingMessage.SELLER_CODE), getRealAmount(msg));
//            msg.setAttributeString(BillingMessage.SELLER_TARIFFED_AMOUNT_CHANGED, "true");
//            //TODO:(AN) ?????
//            msg.setStatusCode(StatusDictionary.STATUS_OK);
//            //add to message
//            msg.setAttribute(BillingMessage.ENTITY_LIST, getEntityList(msg, sellerBalanceTransaction));
//
//        } catch (Throwable e) {
//            task.getProcesedMessage().setStatusCode(StatusDictionary.RESERVATION_ERROR);
//            logger.error(e.getLocalizedMessage(), e);
//            if(e instanceof BillException)
//                throw (BillException)e;
//            throw new BillException(e.getLocalizedMessage(), e);
//        }
        return task;
    }

//    private List getEntityList(BillingMessageWritable msg, SellerBalanceTransaction sellerBalanceTransaction) {
//        List list = new ArrayList();
//        list.add(sellerBalanceTransaction);
//        list.add(getProcedureWrapper(msg, sellerBalanceTransaction));
//        return list;
//    }
//
//    private ProcedureWrapper getProcedureWrapper(BillingMessageWritable msg, SellerBalanceTransaction sellerBalanceTransaction) {
//        return new ProcedureWrapper(
//                new QueryParameterWrapper[]{
//                        QueryParameterWrapper.createOutParameter(1, Types.INTEGER),
//                        new QueryParameterWrapper(2, sellerBalanceTransaction.getSeller().getSellerId()),
//                        new QueryParameterWrapper(3, getRealAmount(msg)),
//                },
//                Seller.class,
//                "change_seller_balance");
//    }
//
//    private TransactionType getTransactionType(BillingMessageWritable msg){
//        TransactionType type = new TransactionType();
//        type.setCode( msg.getAttributeString(BillingMessage.TRANSACTION_TYPE_ID) );
//        return type;
//    }
//
//    private String getCliTransactionNum(BillingMessageWritable msg) {
//        return msg.getAttributeString(BillingMessage.CLI_TRANSACTION_NUM);
//    }
//
//    private String getTransactionGUID(BillingMessageWritable msg) {
//        return msg.getAttributeString(BillingMessage.BILL_TRANSACTION_NUM);
//    }
//
//    private TransactionSupport getTransactionSupport(BillingMessageWritable msg) {
//        //transaction support
//        TransactionSupport transSupport = new TransactionSupport();
//        //user for transaction support
//        Long userActorId = (Long)msg.getAttribute(BillingMessage.USER_ACTOR_ID);
//        User userActor = new User();
//        userActor.setUserId(userActorId);
//        transSupport.setUserActor(userActor);
//        //-----------------------------
//        transSupport.setDescription( msg.getAttributeString(BillingMessage.TRANSACTION_DESCRIPTION) );
//        transSupport.setPaymentReason( msg.getAttributeString(BillingMessage.PAYMENT_REASON) );
//        return transSupport;
//    }
//
//    private SellerBalance getSellerBalance(BillingMessageWritable msg, DBCache dbCache) throws DBCacheException {
//        SellerBalance sellerBalance = new SellerBalance();
//        sellerBalance.setBalanceId(dbCache.getSellerBalanceId(msg.getAttributeString(BillingMessage.SELLER_CODE)));
//        return sellerBalance;
//    }
//
//    private Date getClientTime(BillingMessageWritable msg) {
//        return (Date)msg.getAttribute(BillingMessage.CLIENT_TIME);
//    }
//
//    private Date getTransRegTime(BillingMessageWritable msg) {
//        return (Date)msg.getAttribute(BillingMessage.REGISTRATION_TIME);
//    }
//
//    private Seller getSeller(BillingMessageWritable msg, DBCache dbCache) throws DBCacheException {
//        Seller seller = new Seller();
//        seller.setSellerId(dbCache.getSellerId(msg.getAttributeString(BillingMessage.SELLER_CODE)));
//        return seller;
//    }
//
//    private Double getAmount(BillingMessageWritable msg) throws DBCacheException {
//        return (Double)msg.getAttribute(BillingMessage.SELLER_TARIFFED_AMOUNT);
//    }
//
//    private Double getRealAmount(BillingMessageWritable msg){
//        Double amount = (Double)msg.getAttribute(BillingMessage.SELLER_TARIFFED_AMOUNT);
//        String transactionType = msg.getAttributeString(BillingMessage.TRANSACTION_TYPE_ID);
//        return "2".equals(transactionType) ? new Double(amount.doubleValue() * (-1)) : amount;
//    }
//
//    public Task rollback(Task task, Throwable e) {
//        logger.info("Rollback Change...");
//        try{
//            BillingMessageWritable msg = task.getProcesedMessage();
//            DBCache dbCache = task.getGlobalContext().getDbCache();
//
//            String changed = msg.getAttributeString(BillingMessage.SELLER_TARIFFED_AMOUNT_CHANGED);
//            if(changed != null && changed.equalsIgnoreCase( "true" )){
//                dbCache.reserveSellerBalance(
//                        msg.getAttributeString(BillingMessage.SELLER_CODE),
//                        new Double( -getRealAmount(msg).doubleValue() )
//                );
//            }
//        }
//        catch(Throwable te){
//            logger.error(te.getLocalizedMessage(), te);
//        }
//        return super.rollback(task, e);
//    }

}
