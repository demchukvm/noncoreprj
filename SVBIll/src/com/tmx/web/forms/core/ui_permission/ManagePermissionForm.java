package com.tmx.web.forms.core.ui_permission;

import com.tmx.web.base.BasicActionForm;
import com.tmx.web.controls.table.*;
import com.tmx.web.controls.*;
import com.tmx.as.entities.general.role.Role;
import com.tmx.as.entities.general.ui_permission.UserInterface;
import com.tmx.as.entities.general.ui_permission.PermissionType;

public class ManagePermissionForm extends BasicActionForm {
    private Table table;
    private Button onCreateButton;
    private Button onEditButton;
    private Button onDeleteButton;
    // interactive
    private Button interactiveButton;
    private CheckBox interactiveCheckBox;
    protected void init() {
        table = new Table("query(query_type=retrieve_all," +
                "entity_class=com.tmx.as.entities.general.ui_permission.Permission," +
                "table_attr=userInterface," +
                "table_attr=userInterface.type," +
                "table_attr=permissionType," +
                "table_attr=role," +
                "order(name=id,type=asc)," +
                "page_size=10" +
                ")");
        // filters
        ScrollBox<Role> scrollBoxRoles=new ScrollBox<Role>() {
            protected String getKey(Role dataListElement) {
                return dataListElement.getRoleId().toString();
            }

            protected String getValue(Role dataListElement) {
                return dataListElement.getRoleName();
            }
        };
        scrollBoxRoles.setNullable(true);
        Filter filter = new Filter();
        filter.addParameter("value", new Parameter(scrollBoxRoles));
        table.getFilterSet().addFilter
                ("com_tmx_as_entities_general_ui_permission_Permission_role", filter);
        BrowseBox browseBox=new BrowseBox();
        browseBox.setAction("/core/ui_permission/browseUserInterface");
        browseBox.setCommand("openBrowseWindow");
        filter=new Filter();
        filter.addParameter("value",new Parameter(browseBox));
        table.getFilterSet().addFilter
             ("com_tmx_as_entities_general_ui_permission_Permission_userInterface", filter);

        ScrollBox<PermissionType> scrollBoxPermTypes=new ScrollBox<PermissionType>() {
            protected String getKey(PermissionType dataListElement) {
                return dataListElement.getPermissionTypeId().toString();
            }

            protected String getValue(PermissionType dataListElement) {
                return dataListElement.getName();
            }
        };
        scrollBoxPermTypes.setNullable(true);
        filter = new Filter();
        filter.addParameter("value", new Parameter(scrollBoxPermTypes));
        table.getFilterSet().addFilter
                ("com_tmx_as_entities_general_ui_permission_Permission_permissionType", filter);
        //setup buttons
        String action="/core/ui_permission/infoPermission";
        onCreateButton = new Button();
        onCreateButton.setLabelKey("ctrl.button.label.create");
        onCreateButton.setAction(action);
        onCreateButton.setCommand("preloadForCreate");

        onEditButton = new Button();
        onEditButton.setLabelKey("ctrl.button.label.edit");
        onEditButton.setAction(action);
        onEditButton.setCommand("preloadForUpdate");

        onDeleteButton = new Button();
        onDeleteButton.setLabelKey("ctrl.button.label.delete");
        onDeleteButton.setAction(action);
        onDeleteButton.setCommand("preloadForDelete");
        // interactive
        interactiveButton=new Button();
        interactiveButton.setAction("/core/uipermsetup/toolBarPermissionAction");
        interactiveCheckBox=new CheckBox();interactiveCheckBox.setReadonly(true);
    }

    public Table getTable() {
        return table;
    }

    public void setTable(Table table) {
        this.table = table;
    }

    public Button getOnCreateButton() {
        return onCreateButton;
    }

    public void setOnCreateButton(Button onCreateButton) {
        this.onCreateButton = onCreateButton;
    }

    public Button getOnEditButton() {
        return onEditButton;
    }

    public void setOnEditButton(Button onEditButton) {
        this.onEditButton = onEditButton;
    }

    public Button getOnDeleteButton() {
        return onDeleteButton;
    }

    public void setOnDeleteButton(Button onDeleteButton) {
        this.onDeleteButton = onDeleteButton;
    }

    public Button getInteractiveButton() {
        return interactiveButton;
    }

    public void setInteractiveButton(Button interactiveButton) {
        this.interactiveButton = interactiveButton;
    }

    public CheckBox getInteractiveCheckBox() {
        return interactiveCheckBox;
    }

    public void setInteractiveCheckBox(CheckBox interactiveCheckBox) {
        this.interactiveCheckBox = interactiveCheckBox;
    }
}
