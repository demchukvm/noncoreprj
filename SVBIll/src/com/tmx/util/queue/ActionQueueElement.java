package com.tmx.util.queue;

/**
 */
public abstract class ActionQueueElement implements Runnable {
    ActionQueueElement next, prev;

    private long enqueueTime;

    public ActionQueueElement setEnqueueTime(long enqueueTime) {
        this.enqueueTime = enqueueTime;
        return this;
    }
    public long getEnqueueTime() {
        return enqueueTime;
    }

    public boolean stillWorking() {
        return true;
    }

    public boolean canBeEnqueued() {
        return true;
    }
    public boolean canBeDequeued() {
        return true;
    }
    public boolean canBeCounted() {
        return true;
    }

    abstract public void run();

    public long getExecutionTimeoutDeadlineTimestamp() {
        return 0;//no timeout
    }

    /**
     * Important: do not return this AQE to pool of reusable AQE (if any) if timeout occured!!!
     */
    public void onTimeout() {
        // will be called if timout occure
    }

    long timeLeft() {
        long executionTimeoutDeadlineTimestamp = getExecutionTimeoutDeadlineTimestamp();
        long left = executionTimeoutDeadlineTimestamp > 0
                ? executionTimeoutDeadlineTimestamp - System.currentTimeMillis()
                : 1;
        return left;
    }

    int waitingPoolProcessorVersion;
    synchronized void setWaitingPoolProcessorVersion(int v) {
        waitingPoolProcessorVersion = v;
    }

    ActionQueue.Versionable poolProcessor;
    public void setPoolProcessor(ActionQueue.Versionable poolProcessor) {
        this.poolProcessor = this.poolProcessor == null ? poolProcessor : this.poolProcessor;
    }
    public void unsetPoolProcessor(ActionQueue.Versionable poolProcessor) {
        this.poolProcessor = this.poolProcessor == poolProcessor ? null : this.poolProcessor;
    }

}
