package com.tmx.web.taglib.html;

import com.tmx.web.base.ApplicationEnvironment;
import org.apache.struts.taglib.TagUtils;

import javax.servlet.jsp.JspException;

public class ImgTablePagingTag extends TablePagingTag {

    private String startScrollArrow;
    private String endScrollArrow;
    private String startScrollArrowSelected;
    private String endScrollArrowSelected;
    private String leftScrollArrow;
    private String leftScrollArrowSelected;
    private String rightScrollArrow;
    private String rightScrollArrowSelected;
    private String bookmarkLinkStyleClass;

    //-----------------------Properties
    public String getStartScrollArrow() {
        return (startScrollArrow != null) ? startScrollArrow : ApplicationEnvironment.getAppDeploymentName() + getDefaultProperty("tag.imgTablePagingTag.img.startScrollArrow");
    }

    public String getEndScrollArrow() {
        return (endScrollArrow != null) ? endScrollArrow : ApplicationEnvironment.getAppDeploymentName() + getDefaultProperty("tag.imgTablePagingTag.img.endScrollArrow");
    }

    public String getStartScrollArrowSelected() {
        return (startScrollArrowSelected != null) ? startScrollArrowSelected : ApplicationEnvironment.getAppDeploymentName() + getDefaultProperty("tag.imgTablePagingTag.img.startScrollArrowSelected");
    }

    public String getEndScrollArrowSelected() {
        return (endScrollArrowSelected != null) ? endScrollArrowSelected : ApplicationEnvironment.getAppDeploymentName() + getDefaultProperty("tag.imgTablePagingTag.img.endScrollArrowSelected");
    }

    public String getLeftScrollArrow() {
        return (leftScrollArrow != null) ? leftScrollArrow : ApplicationEnvironment.getAppDeploymentName() + getDefaultProperty("tag.imgTablePagingTag.img.leftScrollArrow");
    }

    public String getLeftScrollArrowSelected() {
        return (leftScrollArrowSelected != null) ? leftScrollArrowSelected : ApplicationEnvironment.getAppDeploymentName() + getDefaultProperty("tag.imgTablePagingTag.img.leftScrollArrowSelected");
    }

    public String getRightScrollArrow() {
        return (rightScrollArrow != null) ? rightScrollArrow : ApplicationEnvironment.getAppDeploymentName() + getDefaultProperty("tag.imgTablePagingTag.img.rightScrollArrow");
    }

    public String getRightScrollArrowSelected() {
        return (rightScrollArrowSelected != null) ? rightScrollArrowSelected : ApplicationEnvironment.getAppDeploymentName() + getDefaultProperty("tag.imgTablePagingTag.img.rightScrollArrowSelected");
    }

    public void setStartScrollArrow(String startScrollArrow) {
        this.startScrollArrow = startScrollArrow;
    }

    public void setEndScrollArrow(String endScrollArrow) {
        this.endScrollArrow = endScrollArrow;
    }

    public void setStartScrollArrowSelected(String startScrollArrowSelected) {
        this.startScrollArrowSelected = startScrollArrowSelected;
    }

    public void setEndScrollArrowSelected(String endScrollArrowSelected) {
        this.endScrollArrowSelected = endScrollArrowSelected;
    }

    public void setLeftScrollArrow(String leftScrollArrow) {
        this.leftScrollArrow = leftScrollArrow;
    }

    public void setLeftScrollArrowSelected(String leftScrollArrowSelected) {
        this.leftScrollArrowSelected = leftScrollArrowSelected;
    }

    public void setRightScrollArrow(String rightScrollArrow) {
        this.rightScrollArrow = rightScrollArrow;
    }

    public void setRightScrollArrowSelected(String rightScrollArrowSelected) {
        this.rightScrollArrowSelected = rightScrollArrowSelected;
    }

    public String getBookmarkLinkStyleClass() {
        return (bookmarkLinkStyleClass != null) ? bookmarkLinkStyleClass : getDefaultProperty("tag.imgTablePagingTag.style.bookmarkLink");
    }

    public void setBookmarkLinkStyleClass(String bookmarkLinkStyleClass) {
        this.bookmarkLinkStyleClass = bookmarkLinkStyleClass;
    }

    //------------------------------Parsing events

    public int doStartTmxTag() throws JspException {
        String tablePagingHTML = renderTablePagingHTML();
        TagUtils.getInstance().write(pageContext, tablePagingHTML);
        return (EVAL_BODY_TAG);
    }

    //------------------------------Rendering methods

    private String renderTablePagingHTML() throws JspException {
        StringBuffer resultHTML = new StringBuffer();
        resultHTML.append("");

        int realBookmarkCount = 1;
        if(getTable().getResultSize() == null || getTable().getResultSize().intValue() == 0){
            realBookmarkCount = 1;
        } else{
            float res = (float)getTable().getResultSize().intValue() / (float)getTable().getQuery().getPageSize();
            realBookmarkCount = (int)Math.floor(res);
            if(res > realBookmarkCount)
                realBookmarkCount++;
        }
        int bookmarkCount = (realBookmarkCount > getMaxBookmarkCount()) ? getMaxBookmarkCount() : realBookmarkCount;

        //render page bookmarks
        int currentPageNumber = getCurrentPage();
        int pageSize=getTable().getQuery().getPageSize();
        int resultSize=getTable().getResultSize().intValue();

        // rows
        resultHTML.append("<span class=\"pageNumbers\">");
        resultHTML.append(getLocalizedLabel("tag.tablePaging.label.rows",null)+":&nbsp;&nbsp;");
        resultHTML.append(getPageSizeEdit(pageSize));
        resultHTML.append("</span>");
        resultHTML.append("&nbsp;&nbsp;|&nbsp;&nbsp;");

        // pages
        resultHTML.append("<span class=\"pageNumbers\">");
        resultHTML.append(getLocalizedLabel("tag.tablePaging.label.page",null)+":&nbsp;&nbsp;");
        resultHTML.append(getPageNumberEdit(currentPageNumber)).append("&nbsp;&nbsp;"+getLocalizedLabel("tag.tablePaging.label.of",null)+"&nbsp;");
        resultHTML.append(realBookmarkCount);
        resultHTML.append("</span>");
        resultHTML.append("&nbsp;&nbsp;|&nbsp;&nbsp;");

        //Start arrow
        if (currentPageNumber == 1) {
            resultHTML.append(getImageTag(getStartScrollArrow(), getStartScrollArrow())+"&nbsp;"+getLocalizedLabel("tag.tablePaging.label.start",null));
        } else {
            resultHTML.append(getImageLink(1,getImageTag(getStartScrollArrowSelected(), getStartScrollArrowSelected())+"&nbsp;"+getLocalizedLabel("tag.tablePaging.label.start",null)));
        }
        resultHTML.append("&nbsp;&nbsp;");

        //left arrow
        if (currentPageNumber == 1) {
            resultHTML.append(getImageTag(getLeftScrollArrow(), getLeftScrollArrow())+"&nbsp;"+getLocalizedLabel("tag.tablePaging.label.previous",null));
        } else {
            resultHTML.append(getImageLink(currentPageNumber - 1,getImageTag(getLeftScrollArrowSelected(), getLeftScrollArrowSelected())+"&nbsp;"+getLocalizedLabel("tag.tablePaging.label.previous",null)));
        }
        resultHTML.append("&nbsp;&nbsp;");

        // items period
        int itemStartPeriodIndex=pageSize*currentPageNumber-pageSize+1;
        int itemEndPeriodIndex;
        if(realBookmarkCount==currentPageNumber) itemEndPeriodIndex=resultSize;
        else itemEndPeriodIndex=pageSize*currentPageNumber;

        resultHTML.append("<span class=\"pageNumbers\">");
        resultHTML.append("(").append(itemStartPeriodIndex).append(" - ");
        resultHTML.append(itemEndPeriodIndex).append(" "+getLocalizedLabel("tag.tablePaging.label.of",null)+" ");
        resultHTML.append(resultSize).append(")&nbsp;&nbsp;");
        resultHTML.append("</span>");

        /*
        //page numbers
        //calculete first page in pager number
        int firstPageNumber = currentPageNumber - bookmarkCount/2;
        if(firstPageNumber <= 0)
            firstPageNumber = 1;
        else if(firstPageNumber + bookmarkCount > realBookmarkCount)
            firstPageNumber = realBookmarkCount - bookmarkCount + 1;

        for (int i = firstPageNumber; i < firstPageNumber + bookmarkCount; i++) {
            if (currentPageNumber == i)
                resultHTML.append(renderSelectedBookmark(i));
            else
                resultHTML.append(getLink(i, String.valueOf(i)));
            resultHTML.append("&nbsp;&nbsp;");
        }
        */

        //right arrow
        if (currentPageNumber < realBookmarkCount) {
            resultHTML.append(getImageLink(currentPageNumber + 1,getLocalizedLabel("tag.tablePaging.label.next",null)+"&nbsp;"+getImageTag(getRightScrollArrowSelected(), getRightScrollArrowSelected())));
        } else {
            resultHTML.append(getLocalizedLabel("tag.tablePaging.label.next",null)+"&nbsp;"+getImageTag(getRightScrollArrow(), getRightScrollArrow()));
        }
        resultHTML.append("&nbsp;&nbsp;");

        //end arrow
        if (currentPageNumber < realBookmarkCount) {
            resultHTML.append(getImageLink(realBookmarkCount,getLocalizedLabel("tag.tablePaging.label.end",null)+"&nbsp;"+getImageTag(getEndScrollArrowSelected(), getEndScrollArrowSelected())));
        } else {
            resultHTML.append(getLocalizedLabel("tag.tablePaging.label.end",null)+"&nbsp;"+getImageTag(getEndScrollArrow(), getEndScrollArrow()));
        }

        StringBuffer tableStr=new StringBuffer("<table width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\">");
        tableStr.append("<tr><td id=\"listViewPaginationButtons\" class=\"listViewPaginationTdS1\" nowrap=\"nowrap\" align=\"right\">");
        tableStr.append(resultHTML);
        tableStr.append("</td></tr>");
        tableStr.append("</table>");
        return tableStr.toString();
        /*getLeftScrollArrow()
        StringBuffer resultHTML = new StringBuffer();
        getLocalizedLabel("pagingStart",null);*/

    }
    /*private String getPagingStart(){

    } */

    //-------------------

     private String getPageSizeEdit(int pageSize) throws JspException {
        String pagingTextId="pagingText"  + getTableName()+"Paging_GetPageSizeEdit";
        String enterKeyCheck="KeyCheck" + getTableName()+"Paging_GetPageSizeEdit";
        // submit
        StringBuffer submit=new StringBuffer("document.forms[0].action='");
        submit.append(getRefreshActionUrl());
        submit.append("?");
        String refreshCommand = getRefreshActionCommand();
        if(refreshCommand != null)
            submit.append("command=").append(refreshCommand).append("&");
        submit.append(getTableName()+".query.pageSize='");
        submit.append("+document.forms[0]."+pagingTextId+".value");
        submit.append(" ;document.forms[0].submit();");

        StringBuffer sb=new StringBuffer("<input class=\"editbox\"type=\"text\" id=\"").append(pagingTextId).append("\" ");
        sb.append("style=\"width: 25px\" ").append("value=\"").append(pageSize).append("\"/>");
        sb.append("<script type=\"text/javascript\">\n");
        sb.append("\tfunction "+enterKeyCheck+"(e){\n");
        sb.append("\t\tvar KeyID = (window.event) ? event.keyCode : e.keyCode;\n");
        sb.append("\t\tif(KeyID==13) {\n");
        sb.append("\t\t\t"+submit+"\n");
        //sb.append("\t\t\talert(\"Enter\");\n");
        sb.append("\t\t}\n");
        sb.append("\t}\n");
        sb.append("\tdocument.getElementById(\""+pagingTextId+"\").onkeyup = "+enterKeyCheck+";\n");
        sb.append("</script>");
        return sb.toString();
    }

    private String getPageNumberEdit(int pageNumber) throws JspException {
        String pagingTextId="pagingText" + getTableName()+"Paging_GetPageNumberEdit";
        String enterKeyCheck="KeyCheck" + getTableName()+"Paging_GetPageNumberEdit";
        // submit
        StringBuffer submit=new StringBuffer("document.forms[0].action='");
        submit.append(getRefreshActionUrl());
        submit.append("?");
        String refreshCommand = getRefreshActionCommand();
        if(refreshCommand != null)
            submit.append("command=").append(refreshCommand);
        submit.append(buildPageNumberString("document.forms[0]."+pagingTextId+".value"));
        submit.append("';document.forms[0].submit();");

        StringBuffer sb=new StringBuffer("<input class=\"editbox\"type=\"text\" id=\"").append(pagingTextId).append("\" ");
        sb.append("style=\"width: 25px\" ").append("value=\"").append(pageNumber).append("\"/>");
        sb.append("<script type=\"text/javascript\">\n");
        sb.append("\tfunction "+enterKeyCheck+"(e){\n");
        sb.append("\t\tvar KeyID = (window.event) ? event.keyCode : e.keyCode;\n");
        sb.append("\t\tif(KeyID==13) {\n");
        sb.append("\t\t\t"+submit+"\n");
        //sb.append("\t\t\talert(\"Enter\");\n");
        sb.append("\t\t}\n");
        sb.append("\t}\n");
        sb.append("\tdocument.getElementById(\""+pagingTextId+"\").onkeyup = "+enterKeyCheck+";\n");
        sb.append("</script>");
        return sb.toString();
    }

    private String getLink(int pageNumber, String linkContent) throws JspException {
        StringBuffer result = new StringBuffer();
        result.append("<a href=\"#\" class=\""+getBookmarkLinkStyleClass()+"\" ");
        result.append("onClick=\"document.forms[0].action='");
        result.append(getRefreshActionUrl());
        result.append("?");
        String refreshCommand = getRefreshActionCommand();
        if(refreshCommand != null)
            result.append("command=").append(refreshCommand).append("&");
        result.append(buildParameterString(pageNumber));
        result.append("';document.forms[0].submit()\">");
        result.append(linkContent);
        result.append("</a>");
        return result.toString();
    }

    private String getImageLink(int pageNumber, String linkContent) throws JspException {
        StringBuffer result = new StringBuffer();
        result.append("<a class=\"listViewPaginationLinkS1\" href=\"#\" ");
        result.append("onClick=\"document.forms[0].action='");
        result.append(getRefreshActionUrl());
        result.append("?");
        String refreshCommand = getRefreshActionCommand();
        if(refreshCommand != null)
            result.append("command=").append(refreshCommand).append("&");
        result.append(buildParameterString(pageNumber));
        result.append("';document.forms[0].submit()\">");
        result.append(linkContent);
        result.append("</a>");
        return result.toString();
    }

    private String getImageTag(String scrollArrow, String scrollArrowHovered) {
        StringBuffer result = new StringBuffer();
        result.append("<img src=\"").
                append(scrollArrow).
                append("\" alt=\"\" border=\"0\" onmouseover=\"this.src='").
                append(scrollArrowHovered).
                append("'\" onmouseout=\"this.src='").
                append(scrollArrow).
                append("'\">");
        return result.toString();
    }

    protected String renderSelectedBookmark(int number) {
        StringBuffer bookmark = new StringBuffer();
        bookmark.append("<b>");
        bookmark.append(number);
        bookmark.append("</b>");
        return bookmark.toString();
    }

    protected String buildPageNumberString(String pageNumber) {
        StringBuffer parameterString = new StringBuffer();
        parameterString.append(getTableName());
        parameterString.append(".appliedParameters");
        parameterString.append("=");
        parameterString.append("query(page_number='+");
        parameterString.append(pageNumber);
        parameterString.append("+')");
        return parameterString.toString();
    }


}
