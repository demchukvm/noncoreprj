package com.tmx.web.forms.core.tools;              

import com.tmx.beng.access.Access;
import com.tmx.web.controls.*;
import com.tmx.web.forms.xmlengine.BasicXMLPublisherForm;

import java.io.UnsupportedEncodingException;


public class RefillForm extends BasicXMLPublisherForm {

    private Access billingEngineAccess = null;
    private String requestXml = null;
    private String refillType;
    private String refillNum;
    private String refillSum;
    private Button btnRefill;
    private Button btnClear;
    private String responseMessage;



    public String getRefillType() {
        return refillType;
    }

    public void setRefillType(String refillType) {
        this.refillType = refillType;
    }

    public Button getBtnRefill() {
        return btnRefill;
    }

    public void setBtnRefill(Button btnRefill) {
        this.btnRefill = btnRefill;
    }

    public Button getBtnClear() {
        return btnClear;
    }

    public void setBtnClear(Button btnClear) {
        this.btnClear = btnClear;
    }

    public String getRefillNum() {
        return refillNum;
    }

    public void setRefillNum(String refillNum) {
        this.refillNum = refillNum;
    }

    public String getRefillSum() {
        return refillSum;
    }

    public void setRefillSum(String refillSum) {
        this.refillSum = refillSum;
    }

    public String getResponseMessage() {
        return responseMessage;
    }

    public void setResponseMessage(String responseMessage) {
        this.responseMessage = responseMessage;
    }

    public Access getBillingEngineAccess() {
        return billingEngineAccess;
    }

    public void setBillingEngineAccess(Access billingEngineAccess) {
        this.billingEngineAccess = billingEngineAccess;
    }

    public String getRequestXml() {
        return requestXml;
    }

    public void setRequestXml(String requestXml) {
        this.requestXml = requestXml;
    }



    protected void init()
    {
        refillType = "";
        refillNum = "";
        refillSum = "";
        responseMessage = "";

        btnRefill = new Button();
        btnRefill.setAction("/core/tools/refill");
        btnRefill.setLabelKey("ctrl.button.label.refill");

        btnClear = new Button();
        btnClear.setAction("/core/tools/refill");
        btnClear.setCommand("clear");
        btnClear.setLabelKey("ctrl.button.label.reset");
     }

    /*public String [][] responseKS =
            {
                    {"-112", "Превышено количество попыток. Платеж отклонен"},
                    {"-111", "Критическая ошибка. Платеж не выполнен"},
                    {"-104", "Платеж отменен пользователем"},
                    {"-103", "Платеж отменен по таймауту"},
                    {"-102", "Проверка неуспешна. Платеж невозможен"},
                    {"-86", "Аннулирование платежа невозможно, т.к. платеж уже учтен в биллинге"},
                    {"-85", "Аннулирование платежа невозможно для данного абонента"},
                    {"-84", "Платеж уже был аннулирован"},
                    {"-83", "В биллинговой системе по чеку найдено более одного платежа"},
                    {"-82", "Платеж не найден в биллинговой системе"},
                    {"-81", "Время возможности аннулирования платежа истекло"},
                    {"-60", "Отказано в смене пароля"},
                    {"-59", "Сервис недоступен (внутренний сбой системы)"},
                    {"-51", "Ошибка сценария"},
                    {"-50", "Ошибка выполнения запроса в базе данных"},
                    {"-49", "Сбой биллинговой системы"},
                    {"-48", "Превышено количество попыток платежа"},
                    {"-46", "Платеж зарегистрирован, но не проведен"},
                    {"-42", "Платеж на указанную сумму невозможен для данного абонента"},
                    {"-41", "Прием платежей для абонента невозможен"},
                    {"-40", "Абонент не найден"},
                    {"-38", "Превышено количество попыток проведения платежа"},
                    {"-37", "Ошибка получения информации о квоте"},
                    {"-36", "Квота платежей отключена"},
                    {"-35", "Квота платежей исчерпана"},
                    {"-34", "Прием платежей от партнера заблокирован"},
                    {"-32", "Неизвестный тип источника платежа"},
                    {"-31", "Сумма платежа вне диапазона, заданного для источника платежа"},
                    {"-27", "Транзакция не найдена"},
                    {"-26", "Операция недопустима для данной транзакции"},
                    {"-25", "Доступ запрещен"},
                    {"-20", "Создание транзакции невозможно"},
                    {"-19", "Неверный формат суммы платежа"},
                    {"-18", "Неверный формат даты платежа"},
                    {"-17", "Не задан пароль"},
                    {"-16", "Задан недопустимый параметр"},
                    {"-15", "Не задан идентификатор транзакции"},
                    {"-14", "Валюта не поддерживается"},
                    {"-13", "Не задана сумма платежа"},
                    {"-12", "Номер получателя платежа или лицевой счет не задан или неверен"},
                    {"-11", "Действие не предусмотрено"},
                    {"-10", "Не задан номер чека"},
                    {"-3", "Неверный логин/пароль"},
                    {"-2", "Доступ с данного IP не предусмотрен"},
                    {"20", "Транзакция создана"},
                    {"21", "Платеж возможен"},
                    {"22", "Платеж подтвержден"},
                    {"23", "Платеж отменен"},
                    {"30", "Запрос успешно завершен"},
                    {"60", "Пароль успешно изменен"},
                    {"70", "Состояние транзакции определено"},
                    {"80", "Аннулирование платежа успешно проведено"},
                    {"101", "Данные корректны. Платеж принят на исполнение"},
                    {"102", "Начата проверка возможности платежа"},
                    {"110", "Платеж подтвержден. Транзакция поставлена в очередь платежей в биллинговую систему"},
                    {"111", "Платеж успешно завершен"},
                    {"112", "Некритическая ошибка. Повтор проведения платежа"},
                    {"115", "Платеж аннулирован"},
                    {"120", "Платеж находится в обработке тарификационной системой"}
            };





    public final String responseMTS =
            "<table>" +
                "<tr> <td width=\"40\" align=\"center\">100</td> <td>Сервис временно недоступен</td></tr>" +
                "<tr> <td align=\"center\">101</td> <td>Аутентификация не состоялась</td></tr>" +
                "<tr> <td align=\"center\">102</td> <td>Транзакция не может быть выполнена</td></tr>" +
                "<tr> <td align=\"center\">206</td> <td>Баланс дилера не заведен в системе. Пополнение не может быть осуществлено</td></tr>" +
                "<tr> <td align=\"center\">200</td> <td>Абонент находится в черном списке и не может осуществлять пополнения своего счета</td></tr>" +
                "<tr> <td align=\"center\">201</td> <td>Не удалось найти абонента по данному номеру</td></tr>" +
                "<tr> <td align=\"center\">202</td> <td>Баланс дилера меньше нуля</td></tr>" +
                "<tr> <td align=\"center\">203</td> <td>Абоненту с данным номером запрещено выполнять пополнение своего счета</td></tr>" +
                "<tr> <td align=\"center\">204</td> <td>Отсутствует заявка на пополнение с соответствующим идентификатором транзакции на стороне MTC</td></tr>" +
                "<tr> <td align=\"center\">205</td> <td>Сумма пополнения не соответствует заданным ограничениям</td></tr>" +
                "<tr> <td align=\"center\">208</td> <td>Неправильный тип баланса. Баланс принадлежит банку, а не дилеру</td></tr>" +
                "<tr> <td align=\"center\">301</td> <td>Неверный или отсутствует обязательный параметр msisdn (номер телефона)</td></tr>" +
                "<tr> <td align=\"center\">302</td> <td>Неверный или отсутствует обязательный параметр ‘amount’ (сумма пополнения)</td></tr>" +
                "<tr> <td align=\"center\">303</td> <td>Неверный или отсутствует обязательный параметр ‘partnerId’</td></tr>" +
                "<tr> <td align=\"center\">304</td> <td>Неверный или отсутствует обязательный параметр ‘transID’</td></tr>" +
                "<tr> <td align=\"center\">305</td> <td>Возможно пополнение только тестовых номеров</td></tr>" +
                "<tr> <td align=\"center\">310</td> <td>Транзакция с таким partnerId для данного дилера уже потверждена</td></tr>" +
                "<tr> <td align=\"center\">311</td> <td>Транзакция с таким transId уже находится в состоянии обработки</td></tr>" +
            "</table>";*/
    //private final String responseOther;
}