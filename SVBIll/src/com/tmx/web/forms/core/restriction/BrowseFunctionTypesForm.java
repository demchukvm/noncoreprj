package com.tmx.web.forms.core.restriction;

import com.tmx.web.forms.core.base.AbstractBrowseForm;
import com.tmx.web.controls.table.ControlManagerTable;
import com.tmx.web.controls.table.Parameter;
import com.tmx.web.controls.CriterionFactory;
import com.tmx.as.entities.general.user.User;
import com.tmx.as.base.criterion.CriterionWrapper;
import com.tmx.as.base.criterion.Restrictions;


/**
 * for restrictions
 */
public class BrowseFunctionTypesForm extends AbstractBrowseForm {

    public void setUpCategory(final String ... categories) {
        ControlManagerTable table = new ControlManagerTable("query(query_type=retrieve_all," +
                "entity_class=com.tmx.as.entities.bill.function.FunctionType," +
                "order(name=id, type=asc)," +
                "page_size=15" +
                ")");
        table.setCriterionFactory(new CriterionFactory() {
            public CriterionWrapper createCriterion() {
                return Restrictions.in("category", categories);  
            }
        });
        setBrowseTable(table);
    }

    public void bindData(User loggedInUser) throws Throwable {
        getBrowseTable().refresh();
    }

}
