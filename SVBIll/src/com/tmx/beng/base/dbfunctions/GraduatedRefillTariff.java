package com.tmx.beng.base.dbfunctions;

import com.tmx.beng.base.Reservation;
import com.tmx.beng.base.BillingMessage;
import com.tmx.beng.base.cache.InvokeFunctionException;
import com.tmx.as.entities.bill.transaction.TransactionType;

import java.util.Map;
import java.util.ArrayList;
import java.util.List;
import java.math.BigDecimal;

import org.apache.log4j.Logger;

public class GraduatedRefillTariff extends SellerOperatorTariff {
    /** Transaction cost */
    public static final String SELLER_DELTA = "seller_delta";
    public static final String OPERATOR_LIMIT = "operator_limit";
    public static final String FEE = "fee";



    void applyTariff(Map actualParams, Reservation reservation) throws InvokeFunctionException {
        try{
            String serviceCodeInMessage = task.getProcesedMessage().getAttributeString(BillingMessage.SERVICE_CODE);
            if (actualParams.get(SERVICE_CODE).equals(serviceCodeInMessage)) {
                double txCost = Double.parseDouble((String)actualParams.get(TX_COST_OP));
                double discontPercent = Double.parseDouble((String)actualParams.get(DISCOUNT_PERCENT_OP));
                double sellerDelta = Double.parseDouble((String)actualParams.get(SELLER_DELTA));
                double opertorLimit = Double.parseDouble((String)actualParams.get(OPERATOR_LIMIT));
                double fee = 1;
                if (actualParams.get(FEE) != null) {
                    fee = Double.parseDouble((String)actualParams.get(FEE));
                }

                double amount = Double.parseDouble(task.getProcesedMessage().getAttributeString(BillingMessage.AMOUNT));
                if (amount < opertorLimit) {
                    //TODO: stuped way. FIX IT!!!
                    if(task.getProcesedMessage().getAttributeString(BillingMessage.IS_OPERATOR_AMOUNT_SET) == null) {
                        amount = amount - fee;

                        BigDecimal amountBD = new BigDecimal(amount);
                        amountBD = amountBD.setScale(ROUND_SCALE, BigDecimal.ROUND_HALF_UP);
                        task.getProcesedMessage().setAttributeString(BillingMessage.AMOUNT, ((Double)amountBD.doubleValue()).toString());
                        task.getProcesedMessage().setAttributeString(BillingMessage.IS_OPERATOR_AMOUNT_SET, "set");
                    }
                    double tarrifedAmount = amount + sellerDelta;
                    //save to DB
                    BigDecimal tarrifedAmountBD = new BigDecimal(tarrifedAmount);
                    tarrifedAmountBD = tarrifedAmountBD.setScale(ROUND_SCALE, BigDecimal.ROUND_HALF_UP);
                    reservation.reserve(null, tarrifedAmountBD.doubleValue(), null, TransactionType.CODE_CREDIT);
                } else {
                    Double tarrifedAmount = (amount + txCost) * discontPercent;
                    //save to DB
                    BigDecimal tarrifedAmountBD = new BigDecimal(tarrifedAmount);
                    tarrifedAmountBD = tarrifedAmountBD.setScale(ROUND_SCALE, BigDecimal.ROUND_HALF_UP);
                    reservation.reserve(null, tarrifedAmountBD.doubleValue(), null, TransactionType.CODE_CREDIT);
                }

//                if (amount < opertorLimit) {
//                    discontPercent = 1;
//                } else {
//                    sellerDelta = 0;
//                }
//
//                double tarrifedAmount = (amount + txCost) * discontPercent + sellerDelta;
//                reservation.reserve(null, tarrifedAmount, null, TransactionType.CODE_CREDIT);

                //prepare reservation on primary tariff

            }
        }
        catch(Throwable e){
            throw new InvokeFunctionException("err.graduated_refill_tariff.calc_error", e);
        }
    }

    public ValidationError[] validate(Map actualParams) {
        List resultErrors = new ArrayList();

        resultErrors.addAll(ParameterValidator.validateParamNames(actualParams, new String[] {TX_COST_OP,
                                                                                    DISCOUNT_PERCENT_OP,
                                                                                    SERVICE_CODE,
                                                                                    SELLER_DELTA,
                                                                                    OPERATOR_LIMIT,
                                                                                    FEE}));
        resultErrors.addAll(ParameterValidator.validateTxCost(actualParams, new String[] {TX_COST_OP}));
        resultErrors.addAll(ParameterValidator.validateDiscountPercent(actualParams, new String[] {DISCOUNT_PERCENT_OP}));

        return (ValidationError[]) resultErrors.toArray(new ValidationError[resultErrors.size()]);
    }

    public TariffParameter[] getDefaultParams() {
        TariffParameter paramOperatorCode = new TariffParameter();
    	paramOperatorCode.setName(SERVICE_CODE);
    	paramOperatorCode.setValue("KyivstarPoP");
        paramOperatorCode.setOrderNum(1l);
        paramOperatorCode.setMandatory(Boolean.TRUE);

        TariffParameter paramDisPer = new TariffParameter();
    	paramDisPer.setName(DISCOUNT_PERCENT_OP);
    	paramDisPer.setValue("0.965");
        paramDisPer.setOrderNum(2l);
        paramDisPer.setMandatory(Boolean.TRUE);

        TariffParameter paramCost = new TariffParameter();
    	paramCost.setName(TX_COST_OP);
    	paramCost.setValue("0");
        paramCost.setOrderNum(3l);
        paramCost.setMandatory(Boolean.TRUE);

        TariffParameter paramSellerDelta = new TariffParameter();
    	paramSellerDelta.setName(SELLER_DELTA);
    	paramSellerDelta.setValue("1");
        paramSellerDelta.setOrderNum(4l);
        paramSellerDelta.setMandatory(Boolean.TRUE);

        TariffParameter paramOperatorLimit = new TariffParameter();
    	paramOperatorLimit.setName(OPERATOR_LIMIT);
    	paramOperatorLimit.setValue("19.99");
        paramOperatorLimit.setOrderNum(5l);
        paramOperatorLimit.setMandatory(Boolean.TRUE);

        TariffParameter paramFee = new TariffParameter();
    	paramFee.setName(FEE);
    	paramFee.setValue("1");
        paramFee.setOrderNum(6l);
        paramFee.setMandatory(Boolean.FALSE);
//
//        TariffParameter paramSellerDelta = new TariffParameter();
//    	paramDisPer.setName(SELLER_DELTA);
//    	paramDisPer.setValue("1");
//        paramDisPer.setOrderNum(4l);
//        paramDisPer.setMandatory(Boolean.TRUE);

        return new TariffParameter[] {paramDisPer,
                                        paramOperatorCode,
                                        paramCost,
                                        paramSellerDelta, //SOME PROBLEM HERE
                                        paramOperatorLimit,
                                        paramFee};
    }
}
