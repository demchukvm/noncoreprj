package com.tmx.as.entities.bill.transaction;

import com.tmx.as.entities.bill.balance.TerminalBalance;
import com.tmx.as.entities.bill.terminal.Terminal;

/**
 */
public class TerminalBalanceTransaction extends AbstractTransaction{
    private Terminal terminal;
    private Long terminalId;

    private TerminalBalance terminalBalance;
    private String cliTransactionGUID;
    private Double preTxBalance;


    //-----------Properties

    public Terminal getTerminal() {
        return terminal;
    }

    public void setTerminal(Terminal terminal) {
        this.terminal = terminal;
    }

    public TerminalBalance getTerminalBalance() {
        return terminalBalance;
    }

    public void setTerminalBalance(TerminalBalance terminalBalance) {
        this.terminalBalance = terminalBalance;
    }

    public String getCliTransactionGUID() {
        return cliTransactionGUID;
    }

    public void setCliTransactionGUID(String cliTransactionGUID) {
        this.cliTransactionGUID = cliTransactionGUID;
    }

    public Double getPreTxBalance() {
        return preTxBalance;
    }

    public void setPreTxBalance(Double preTxBalance) {
        this.preTxBalance = preTxBalance;
    }

    public Long getTerminalId() {
        return terminalId;
    }

    public void setTerminalId(Long terminalId) {
        this.terminalId = terminalId;
    }

}
