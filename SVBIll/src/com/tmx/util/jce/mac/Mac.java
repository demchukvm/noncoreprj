package com.tmx.util.jce.mac;

import com.tmx.util.jce.mac.redundant.MACFactory;
import javax.crypto.ShortBufferException;
import java.security.Key;
import java.security.InvalidKeyException;
import java.security.InvalidAlgorithmParameterException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.AlgorithmParameterSpec;

/**
 * TODO Redundant
 * This is temporary enigne for use while com.tmx.util.jce.Provider will not signed.
 */
public class Mac {
    private AnsiXMacSpi macSpi;
    private String algorithm;


  public static final Mac getInstance(String algorithm) throws NoSuchAlgorithmException {
      return new Mac(algorithm);
  }

    private Mac(String algorithm) throws NoSuchAlgorithmException {
        this.macSpi = MACFactory.getInstance(algorithm);
        this.algorithm = algorithm;
    }

    public final byte[] doFinal() throws IllegalStateException {
        byte[] digest = macSpi.engineDoFinal();
        reset();
        return digest;
    }

    public final byte[] doFinal(byte[] input) throws IllegalStateException {
        update(input);
        byte[] digest = macSpi.engineDoFinal();
        reset();
        return digest;
    }

    public final void doFinal(byte[] output, int outOffset) throws IllegalStateException, ShortBufferException {
        if (output.length - outOffset < getMacLength()) {
            throw new ShortBufferException();
        }
        byte[] mac = macSpi.engineDoFinal();
        System.arraycopy(mac, 0, output, outOffset, getMacLength());
        reset();
    }

    public final String getAlgorithm() {
        return algorithm;
    }

    public final int getMacLength() {
        return macSpi.engineGetMacLength();
    }

    public final void init(Key key) throws InvalidKeyException {
        try {
            init(key, null);
        }
        catch (InvalidAlgorithmParameterException iape) {
            throw new IllegalArgumentException(algorithm + " needs parameters");
        }
    }


    public final void init(Key key, AlgorithmParameterSpec params)
            throws InvalidAlgorithmParameterException, InvalidKeyException {
        macSpi.engineInit(key, params);
    }

    public final void reset() {
        macSpi.engineReset();
    }

    public final void update(byte input) throws IllegalStateException {
        macSpi.engineUpdate(input);
    }

    public final void update(byte[] input) throws IllegalStateException {
        update(input, 0, input.length);
    }

    public final void update(byte[] input, int offset, int length)
            throws IllegalStateException {
        macSpi.engineUpdate(input, offset, length);
    }

}
