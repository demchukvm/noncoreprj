package com.tmx.web.base;

import javax.servlet.*;
import java.io.IOException;

/**
 * Sets encoding before request
 */
public class CharsetEncodingFilter implements Filter {

    // default character encoding
    String defaultEncoding = "UTF-8";

    public void init(FilterConfig config) throws ServletException {
        String encoding = config.getInitParameter("encoding");
        if (encoding != null)
            defaultEncoding = encoding;
    }

    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        request.setCharacterEncoding(defaultEncoding);
        response.setCharacterEncoding(defaultEncoding);
        chain.doFilter(request, response);
    }

    public void destroy() {
        defaultEncoding = null;
    }

}