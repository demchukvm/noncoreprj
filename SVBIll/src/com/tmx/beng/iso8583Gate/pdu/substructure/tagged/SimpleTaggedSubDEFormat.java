package com.tmx.beng.iso8583Gate.pdu.substructure.tagged;

import com.tmx.beng.iso8583Gate.pdu.RuntimeDataElement;
import com.tmx.beng.iso8583Gate.pdu.IsoParseException;
import com.tmx.beng.iso8583Gate.pdu.IsoSerializeException;
import com.tmx.beng.iso8583Gate.pdu.DataElementFormat;
import com.tmx.beng.iso8583Gate.pdu.types.AlphabeticNumericSpecialCharactersType;
import com.tmx.beng.iso8583Gate.pdu.types.Type;
import com.tmx.beng.iso8583Gate.pdu.length_types.LLLVARLengthType;
import com.tmx.beng.iso8583Gate.pdu.length_types.LengthTypeFactory;
import com.tmx.beng.iso8583Gate.pdu.substructure.BasicSubDEFormat;
import com.tmx.beng.iso8583Gate.pdu.substructure.RuntimeSubDataElement;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Iterator;

/**
    Simple format with structure:
    Length  n   LLLVAR
    TagName ans Fixed = 2
    Value   an  var

    Works with String data in RDE. 
 */
public class SimpleTaggedSubDEFormat extends BasicSubDEFormat {
    final int TAG_BYTE_LENGTH = 2;

    public void parse(RuntimeDataElement rde) throws IsoParseException {
        RuntimeSubDataElement runtimeSDE = prepare(rde);
        byte[] sourceBytes = rde.getString().getBytes();
        ByteArrayInputStream sourceBais = new ByteArrayInputStream(sourceBytes);
        LLLVARLengthType lengthType = new LLLVARLengthType();
        AlphabeticNumericSpecialCharactersType ansType = new AlphabeticNumericSpecialCharactersType();
        int maxLength = getSubFieldMaxLength(rde);
        while(sourceBais.available() > 0){
            int actualLength = lengthType.parseLength(sourceBais, maxLength);
            String tag = ansType.parse(sourceBais, TAG_BYTE_LENGTH);
            String value = ansType.parse(sourceBais, actualLength-TAG_BYTE_LENGTH);
            runtimeSDE.addValue(tag, value);
        }
    }

    public void serialize(RuntimeDataElement rde) throws IsoSerializeException {
        RuntimeSubDataElement runtimeSDE = rde.getSubstructureDataElement();
        if(!(runtimeSDE instanceof RuntimeSimpleTaggedSubDE))
            throw new IsoSerializeException("err.simple_tag_sdef.incorrect_rsde_type", new String[]{runtimeSDE.getClass().toString()});

        RuntimeSimpleTaggedSubDE tagRdes = (RuntimeSimpleTaggedSubDE)runtimeSDE;
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        LLLVARLengthType lengthType = new LLLVARLengthType();
        AlphabeticNumericSpecialCharactersType ansType = new AlphabeticNumericSpecialCharactersType();
        int maxLength = getSubFieldMaxLength(runtimeSDE.getOwnerRDE());
        for(Iterator iter = tagRdes.iterator(); iter.hasNext();){
            RuntimeSimpleTaggedSubDE.ValueHandler value = (RuntimeSimpleTaggedSubDE.ValueHandler)iter.next();
            Type.SerializedValue serializedTag = ansType.serialize(false, maxLength, value.getTag());
            Type.SerializedValue serializedVal = ansType.serialize(false, maxLength, value.getValue());
            byte[] lengthData = lengthType.serializeLength(serializedTag.length + serializedVal.length);
            try{
                baos.write(lengthData);
                baos.write(serializedTag.data);
                baos.write(serializedVal.data);
            }
            catch(IOException e){
                throw new IsoSerializeException("err.simple_tagged_sdef.serialization_failed", e);
            }
        }
        rde.setValue(new String(baos.toByteArray()));
    }

    public RuntimeSubDataElement getRuntimeSubDataElementInstance() {
        return new RuntimeSimpleTaggedSubDE();
    }

    private int getSubFieldMaxLength(RuntimeDataElement rde){
        DataElementFormat def = rde.getFormat();
        if(LengthTypeFactory.FIXED.equals(def.getLengthType())){
            return def.getLength();
        }
        else{
            return def.getMaxLength();
        }
    }
}

