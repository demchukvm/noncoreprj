package com.tmx.as.entities.bill.modem_group;

import com.tmx.as.base.BasicEntity;
import com.tmx.as.entities.bill.operator.Operator;

import java.io.Serializable;

/**
 */
public class ModemGroup extends BasicEntity {
    private Long modemGroupId;
    private Operator operator;

    public Long getModemGroupId() {
        return modemGroupId;
    }

    public void setModemGroupId(Long modemGroupId) {
        this.modemGroupId = modemGroupId;
    }

    public Operator getOperator() {
        return operator;
    }

    public void setOperator(Operator operator) {
        this.operator = operator;
    }

    public Serializable getId() {
        return getModemGroupId();
    }

    public int getValid() {
        return valid;
    }

    public void setValid(int valid) {
        this.valid = valid;
    }
}
