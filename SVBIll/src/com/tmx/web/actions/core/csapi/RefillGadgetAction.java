package com.tmx.web.actions.core.csapi;

import com.tmx.as.base.QueryParameterWrapper;
import com.tmx.as.entities.general.user.User;
import com.tmx.as.modules.SpringModule;
import com.tmx.web.base.ApplicationEnvironment;
import com.tmx.web.base.SecurityService;
import com.tmx.web.controls.ScrollBox;
import com.tmx.web.forms.core.csapi.RefillGadgetForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForm;
import org.apache.log4j.Logger;
import com.tmx.beng.access.Connection;
import com.tmx.beng.csapi.xml.v1.base.ClientPAD;
import com.tmx.beng.base.BillException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import javax.servlet.http.HttpServletResponse;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class RefillGadgetAction extends CsApiCallAction
{
    Logger logger = Logger.getLogger("webcsapi."+getClass().getName());

    final String TERMINAL_SN_MTSEXC = "localssm-01";
    final String SELLER_CODE_MTSEXC = "LocalSSM";

    final String TERMINAL_SN_LIFEEXC = "LocalLife01";
    final String SELLER_CODE_LIFEEXC = "LocalLife";

    final String TERMINAL_SN_KSBNC = "local-01";
    final String SELLER_CODE_KSBNC = "LocalSVCOM";

    final String PROCESSING_CODE = "Local";
    final String LOGIN = "Local";
    final String PASSWORD = "7460dfdf78c61247fc883fd5e4553fb4";

    final String TRADE_POINT_M_1 = "UN_00004";
    final String TRADE_POINT_L_1 = "UN_00888";
    final String TRADE_POINT_K_1 = "UN_00687";
    final String TRADE_POINT_K_2 = "UN_10474";
    final String TRADE_POINT_K_3 = "UN_00586";

    private String requestXML;

    private String tradePoint;
    private String serviceCode;
    private String accountType;
    private String account;
    private String amount;

    private void clear(RefillGadgetForm form)
    {
        requestXML = "";
        form.setResponseXML("");
        form.setMessage("");

        tradePoint = "";
        serviceCode = "";
        accountType = "";
        account = "";
        amount = "";

        //form.getEbAccount().setValue("");
        //form.getEbAmount().setValue("");
        //form.getSbProducts().setSelectedKey(ScrollBox.NULL_KEY);
        //form.getSbTerminals().setSelectedKey(ScrollBox.NULL_KEY);
    }

    private void createXML(RefillGadgetForm form)
    {
        Date date = new Date();
        requestXML = new StringBuilder()
                .append("<root xmlns=\"http://tmx.com/gate/csapi/v1/base/beans\">")
                .append("<request protocol=\"1.1\">")
                .append("<auth>")
                .append(addToHeaderXML(form.getSbProducts().getSelectedKey().substring(0,form.getSbProducts().getSelectedKey().indexOf(":"))))
                .append("<processingCode>" + PROCESSING_CODE + "</processingCode>")
                .append("<login>" + LOGIN + "</login>")
                .append("<password>" + PASSWORD + "</password>")
                .append("<clientTime>2011.07.02 00:00:00</clientTime>")
                .append("</auth>")
                .append("<operations>")
                .append("<refillPayment>")
                .append("<cliTransactionNum>" + tradePoint + "_" + date.getTime() + "_m" + "</cliTransactionNum>")
                .append("<serviceCode>" + serviceCode + "</serviceCode>")
                .append("<amount>" + amount + "</amount>")
                .append("<" + accountType + ">" + account + "</" + accountType + ">")
                .append("<tradePoint>" + tradePoint + "</tradePoint>")
                .append("</refillPayment>")
                .append("</operations>")
                .append("</request>")
                .append("</root>")
                .toString();
    }

    private String addToHeaderXML(String serviceCode)
    {
        if(serviceCode.equals("KyivstarBonusNoCommission"))
        {
            return new StringBuilder()
                    .append("<terminalSn>" + TERMINAL_SN_KSBNC + "</terminalSn>")
                    .append("<sellerCode>" + SELLER_CODE_KSBNC + "</sellerCode>").toString();
        }
        else if(serviceCode.equals("LifeExc"))
        {
            return new StringBuilder()
                    .append("<terminalSn>" + TERMINAL_SN_LIFEEXC + "</terminalSn>")
                    .append("<sellerCode>" + SELLER_CODE_LIFEEXC + "</sellerCode>").toString();
        }
        else if(serviceCode.equals("MTSExc"))
        {
            return new StringBuilder()
                    .append("<terminalSn>" + TERMINAL_SN_MTSEXC + "</terminalSn>")
                    .append("<sellerCode>" + SELLER_CODE_MTSEXC + "</sellerCode>").toString();
        }
        else return "";
    }

    private void getData(RefillGadgetForm form)
    {
        tradePoint = form.getSbTerminals().getSelectedKey();

        serviceCode = form.getSbProducts().getSelectedKey().substring(0,form.getSbProducts().getSelectedKey().indexOf(":"));

        accountType = form.getSbProducts().getSelectedKey().substring(form.getSbProducts().getSelectedKey().indexOf(":")+1);

//        account = form.getEbAccount().getValue();
//        amount = form.getEbAmount().getValue().replace(",", ".");
    }

    private boolean isDataCorrect()
    {
        if(serviceCode.equals("KyivstarBonusNoCommission"))
        {
            if(tradePoint.equals(TRADE_POINT_K_1)
                    || tradePoint.equals(TRADE_POINT_K_2)
                    || tradePoint.equals(TRADE_POINT_K_3))
            { return true; }
            else
            { return false; }
        }
        else if(serviceCode.equals("LifeExc"))
        {
            if(tradePoint.equals(TRADE_POINT_L_1))
            { return true; }
            else
            { return false; }

        }
        else if(serviceCode.equals("MTSExc"))
        {
            if(tradePoint.equals(TRADE_POINT_M_1))
            { return true; }
            else
            { return false; }
        }
        else return false;
    }


    public ActionForward clientSideApiCall(ActionMapping mapping,
                                                        ActionForm form,
                                                        HttpServletRequest request,
                                                        HttpServletResponse response) throws Throwable {

        RefillGadgetForm refillGadgetForm = (RefillGadgetForm) form;

       // if()


   //     clear(refillGadgetForm);
        
//        refillGadgetForm.getEbAccount().validate();
//        refillGadgetForm.getEbAmount().validate();
//
//        refillGadgetForm.getSbProducts().validate();
//        refillGadgetForm.getSbTerminals().validate();

        getData(refillGadgetForm);
        if(isDataCorrect())
        {
            refillGadgetForm.setRefillStory(refillGadgetForm.getRefillStory() + "<b>REQUEST: </b>"+serviceCode+"+"+accountType+"+"+account+"+"+amount+"+"+tradePoint+"<br>");

            createXML(refillGadgetForm);

            ClientPAD clientPAD = new ClientPAD();

            try {

                Connection conn = (Connection) SpringModule.getApplicationContext().getBean("billingConnection");

                long t = System.currentTimeMillis();
                ClientPAD.ParsingResult resp;

                try {
                    logger.info("Request XML manual: " + requestXML);
                    resp = clientPAD.parseRequest(requestXML);
                    logger.debug("Parse time: " + (System.currentTimeMillis() - t));
                }
                catch(BillException e) {
                    logger.error("Parsing error: \n"+e.getTraceDump());
                    refillGadgetForm.setResponseXML(responseTransform(clientPAD.buildErrorResponse(ClientPAD.CSAPI_REQUEST_PARSING_ERROR, getLocale(request)), true));
                    refillGadgetForm.setRefillStory(refillGadgetForm.getRefillStory() + "<b>RESPONSE: </b>"+responseTransform(clientPAD.buildErrorResponse(ClientPAD.CSAPI_REQUEST_PARSING_ERROR, getLocale(request)), false)+"<hr>");
                    throw new HandledBillingException();
                }

                try{
                    resp.setBillingMessage(conn.processSync(resp.getBillingMessage()));
                }
                catch(BillException e) {
                    logger.error("Processing error: \n"+e.getTraceDump());
                    refillGadgetForm.setResponseXML(clientPAD.buildErrorResponse(ClientPAD.CSAPI_REQUEST_PROCESSING_ERROR, getLocale(request)));
                    refillGadgetForm.setRefillStory(refillGadgetForm.getRefillStory() + "<b>RESPONSE: </b>"+clientPAD.buildErrorResponse(ClientPAD.CSAPI_REQUEST_PROCESSING_ERROR, getLocale(request))+"<hr>");
                    throw new HandledBillingException();
                }

                refillGadgetForm.setResponseXML(responseTransform(clientPAD.safeSerializeResponse(resp.getBillingMessage(), resp.getVersion()), true));
                refillGadgetForm.setRefillStory(refillGadgetForm.getRefillStory() + "<b>RESPONSE: </b>"+clientPAD.safeSerializeResponse(resp.getBillingMessage(), resp.getVersion())+"<hr>");
                logger.info("Response XML manual: " + refillGadgetForm.getResponseXML());
            }
            catch(HandledBillingException e){
                logger.error("Error: " + e.toString());
            }
            catch(Throwable e){
                e.printStackTrace();
                logger.error("Unknown error: \n"+e.toString());
                refillGadgetForm.setResponseXML(clientPAD.buildErrorResponse(ClientPAD.CSAPI_UNKNOWN_ERROR, getLocale(request)));
                refillGadgetForm.setRefillStory(refillGadgetForm.getRefillStory() + "<b>RESPONSE: </b>"+clientPAD.buildErrorResponse(ClientPAD.CSAPI_UNKNOWN_ERROR, getLocale(request))+"<hr>");
            }
        }
        else
        {
            refillGadgetForm.setMessage("Data correct error");
        }
        clear(refillGadgetForm);


        //return mapping.findForward("refill_gadget_resp");


        return mapping.findForward("refill_gadget_resp");
    }

    protected ActionForward defaultAction(ActionMapping mapping,
                                          ActionForm form,
                                          HttpServletRequest request,
                                          HttpServletResponse response,
                                          String methodName) throws Throwable {

        User user = getLoggedInUser(request);
        if(user.getAdmin() == 1 && user.getRole().getRoleId() == 1)
            return mapping.findForward("refill_gadget_resp");
        else
            return mapping.findForward("refill_gadget_resp_err");
    }

    private class HandledBillingException extends Throwable { }

    protected User getLoggedInUser(HttpServletRequest request) throws com.tmx.as.exceptions.SecurityException {
        SecurityService securityService = (SecurityService) ApplicationEnvironment.getService(SecurityService.class);
        return securityService.getLoggedInUser(request.getSession());
    }

    public ActionForward getAllThisMonth(ActionMapping mapping,
                                                        ActionForm form,
                                                        HttpServletRequest request,
                                                        HttpServletResponse response) throws Throwable {

        GregorianCalendar dFrom = new GregorianCalendar();
        GregorianCalendar dTo = new GregorianCalendar();

        dFrom.set(Calendar.DAY_OF_MONTH, 1);
        dFrom.set(Calendar.HOUR_OF_DAY, 0);
        dFrom.set(Calendar.MINUTE, 0);
        dFrom.set(Calendar.SECOND, 0);
        dFrom.set(Calendar.MILLISECOND, 0);

        dTo.set(Calendar.MONTH, dTo.get(Calendar.MONTH)+1);
        dTo.set(Calendar.DAY_OF_MONTH, 1);
        dTo.set(Calendar.HOUR_OF_DAY, 0);
        dTo.set(Calendar.MINUTE, 0);
        dTo.set(Calendar.SECOND, 0);
        dTo.set(Calendar.MILLISECOND, 0);

        QueryParameterWrapper[] wrapper = new QueryParameterWrapper[]{
                new QueryParameterWrapper("from", dFrom.getTime()),
                new QueryParameterWrapper("to", dTo.getTime())
        };

        return mapping.findForward("refill_gadget_resp");
    }

    private String responseTransform(String oldResponse, boolean flag)
    {
        String newResponse = "";

        if(flag)
        {
            newResponse = oldResponse
                    .replace("<?xml version=\"1.0\"?>", "")
                    .replace("<root>", "")
                    .replace("<response>", "")
                    .replace("<operationsResults>", "")
                    .replace("<refillPayment>", "")
                    .replace("<statusCode>", "statusCode: ")
                    .replace("</statusCode>", "<br>")
                    .replace("<statusMessage>??????", "statusMessage: ")
                    .replace("??????</statusMessage>", "<br>")
                    .replace("<registrationTime>", "registrationTime: ")
                    .replace("</registrationTime>", "")
                    .replace("</refillPayment>", "")
                    .replace("</operationsResults>", "")
                    .replace("</response>", "")
                    .replace("</root>", "")
                    .replace("<billTransactionNum>", "billTransactionNum: ")
                    .replace("</billTransactionNum>", "<br>")
                    .replace("<receiptNum>", "receiptNum: ")
                    .replace("</receiptNum>", "<br>")
                    .replace("<payId>", "payId: ")
                    .replace("</payId>", "<br>")
                    .replace("<waitTime>", "waitTime: ")
                    .replace("</waitTime>", "");
        }
        else
        {
            newResponse = oldResponse
                    .replace("<?xml version=\"1.0\"?>", "")
                    .replace("<root>", "")
                    .replace("<response>", "")
                    .replace("<operationsResults>", "")
                    .replace("<refillPayment>", "")
                    .replace("<statusCode>", "")
                    .replace("</statusCode>", " + ")
                    .replace("<statusMessage>??????", "")
                    .replace("??????</statusMessage>", " + ")
                    .replace("<registrationTime>", "")
                    .replace("</registrationTime>", "")
                    .replace("</refillPayment>", "")
                    .replace("</operationsResults>", "")
                    .replace("</response>", "")
                    .replace("</root>", "")
                    .replace("<billTransactionNum>", "")
                    .replace("</billTransactionNum>", " + ")
                    .replace("<receiptNum>", "")
                    .replace("</receiptNum>", " + ")
                    .replace("<payId>", "")
                    .replace("</payId>", " + ")
                    .replace("<waitTime>", "")
                    .replace("</waitTime>", "");
        }

        return newResponse;
    }

}


