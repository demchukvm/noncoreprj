package com.tmx.util.jce.cipher;

import com.tmx.util.jce.JceException;
import javax.crypto.SecretKey;
import java.io.File;
import java.io.InputStream;
import java.io.OutputStream;

/**

 */
public interface CipherUtil {
    /**
     * Generate a secret encryption/decryption key
     */
    public SecretKey generateKey() throws JceException;

    /**
     * Save the specified SecretKey to the specified file
     */
    public void writeKey(SecretKey key, File f) throws JceException;

    /**
     * Read a secret key from the specified file
     */
    public SecretKey readKey(File f) throws JceException;

    public SecretKey readKey(InputStream is) throws JceException;

    public SecretKey readKey(byte[] rawKey) throws JceException;

    public void encrypt(SecretKey key, InputStream in, OutputStream out) throws JceException;

    public void decrypt(SecretKey key, InputStream in, OutputStream out) throws JceException;
}
