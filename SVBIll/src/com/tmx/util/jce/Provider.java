package com.tmx.util.jce;

import java.security.AccessController;

/**
 Steps to authentificate:
 1) Use keytool to generate a DSA keypair.
 keytool -genkey -alias <alias> -keyalg DSA -keysize 1024
    -dname "cn=<Company Name>,ou=Java Software Code Signing,o=Sun Microsystems Inc"
    -keystore <keystore file name>
    -storepass <keystore password>

 Example:
 keytool -genkey -alias tmxcert -keyalg DSA -keysize 1024 -dname "cn=Technomatix,ou=Java Software Code Signing,o=Sun Microsystems Inc" -keystore D:\SVBILL\repository\security\svbillkeystore  -storepass svbill2o

 2) Use keytool to generate a certificate signing request.
 keytool -certreq -alias <alias> -sigalg DSA 
        -file <csr file name>
        -keystore <keystore file name>
        -storepass <keystore password>

 Example:
 keytool -certreq -alias tmxcert -sigalg DSA  -file D:\SVBILL\repository\security\tmxcert.csr -keystore D:\SVBILL\repository\security\svbillkeystore -storepass svbill2o

 
 */
public final class Provider extends java.security.Provider{

    public Provider() {
        super("TmxJceProvider", 1.0, "TMX JCE Provider v1.0 implementing X9.9, X9.19 MAC");

        AccessController.doPrivileged(new java.security.PrivilegedAction() {
            public Object run() {
                put("Mac.X99", "com.tmx.util.jce.mac.AnsiX99MacSpi");
                put("Mac.X919", "com.tmx.util.jce.mac.AnsiX919MacSpi");
                return null;
            }
        });
    }
}
