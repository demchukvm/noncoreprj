package com.tmx.as.blogic.restriction;

import com.tmx.as.blogic.BlogicException;
import com.tmx.as.base.EntityManager;
import com.tmx.as.entities.bill.restriction.AbstractRestriction;
import com.tmx.as.exceptions.DatabaseException;


abstract public class AbstractRestrictionManager extends EntityManager {

    public abstract void saveRestriction(AbstractRestriction rest) throws DatabaseException, BlogicException;
    public abstract AbstractRestriction getRestriction(Long restId) throws DatabaseException, BlogicException;
    public abstract void deleteRestriction(Long restId) throws DatabaseException, BlogicException;

}
