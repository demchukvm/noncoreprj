package com.tmx.beng.base.channels;

import com.tmx.beng.base.BillingMessage;
import com.tmx.beng.base.BillException;

import java.util.List;

/**
    Resolves resulting channel based on refilled msisdn
 */
public class MsisdnMaskChannel extends AbstractChannel{
    private static final String DEFAULT_CHANNEL_ID = "defaultChannelId";
    private static final String NUMBER_PREFIX = "numberPrefix";

    public OutChannel process(BillingMessage msg) throws BillException {
        String normilizedPhoneNumber = normalizeMsisdn(msg.getAttributeString(BillingMessage.ACCOUNT_NUMBER));
        String pauAccountNumber = normalizeMsisdn(msg.getAttributeString(BillingMessage.PAY_ACCOUNT_NUMBER));
        final int outChannelId = resolveOutChannelId(normilizedPhoneNumber, pauAccountNumber);
        OutChannel outChannel = getOutChannel(outChannelId);
        if(outChannel == null)
            throw new BillException("err.msisdn_mask_channel.no_channel_found_with_id", new String[]{String.valueOf(outChannelId)});
        return outChannel;
    }
    /** convert msisdn to international format like 380 50 XXX XX XX */
   // private String normalizeMsisdn(String msisdn) throws BillException{
    protected String normalizeMsisdn(String msisdn) throws BillException{
        final int SHORT_MSISDN_LENGTH = 9;
        String normalizedMsisdn = msisdn;
        if(normalizedMsisdn != null){
            if(normalizedMsisdn.length() >= SHORT_MSISDN_LENGTH){
                normalizedMsisdn = msisdn.substring(normalizedMsisdn.length() - SHORT_MSISDN_LENGTH);
                String msisdnPrefix = getParamValue(NUMBER_PREFIX);
                if(msisdnPrefix == null)
                    throw new BillException("err.msisdn_mask_channel.no_prefix_defined_in_config");
                normalizedMsisdn = msisdnPrefix + normalizedMsisdn;
            }
        }
        return normalizedMsisdn;
    }

    //private int resolveOutChannelId(String msisdn, String accounNumber) throws BillException {
    protected int resolveOutChannelId(String msisdn, String accounNumber) throws BillException {
        if (msisdn == null || accounNumber != null) {
            return Integer.parseInt(getParamValue("accountNumber"));
        }
        String[] operatorsIds = getParamNames();
        for(int j=0; j<operatorsIds.length; j++){
            List msisdnTemplateList = getParamListValues(operatorsIds[j]);
            for(int i=0; i<msisdnTemplateList.size(); i++){
                String msisdnPrefix = (String)msisdnTemplateList.get(i);
                if(isMsisdnTemplateMatch(msisdnPrefix, msisdn))
                    return Integer.parseInt(operatorsIds[j]);
            }
        }
        //return default channel id
        String defaultChannelId = getParamValue(DEFAULT_CHANNEL_ID);
        if(defaultChannelId == null)
            throw new BillException("err.msisdn_mask_channel.no_default_channel_id_defined_in_config");
        try{
            return Integer.parseInt(defaultChannelId);
        }
        catch(NumberFormatException e){
            throw new BillException("err.msisdn_mask_channel.failed_to_parse_default_channel_id", new String[]{defaultChannelId});
        }
    }

    //private boolean isMsisdnTemplateMatch(String msisdnTemplate, String msisdn){
    protected boolean isMsisdnTemplateMatch(String msisdnTemplate, String msisdn){
        return msisdn.startsWith(msisdnTemplate);
    }
}
