package com.tmx.web.forms.core.reports;

import com.tmx.web.controls.TimeSelector;
import com.tmx.web.controls.ScrollBox;
import com.tmx.web.controls.Button;
import com.tmx.web.controls.BrowseBox;
import com.tmx.web.controls.grid.Grid;
import com.tmx.web.controls.grid.GridException;
import com.tmx.web.base.BasicActionForm;
import com.tmx.web.base.ApplicationEnvironment;
import com.tmx.util.i18n.MessageResources;
import com.tmx.as.entities.bill.voucher.Voucher;
import com.tmx.as.entities.bill.seller.Seller;
import com.tmx.as.entities.bill.operator.Operator;
import com.tmx.as.blogic.VoucherManager;
import com.tmx.as.blogic.OperatorManager;
import com.tmx.as.exceptions.DatabaseException;
import com.tmx.as.base.QueryParameterWrapper;

import javax.swing.*;
import java.util.List;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.Collections;
import java.util.regex.Pattern;
import java.util.regex.Matcher;
import java.text.DecimalFormat;
import java.text.NumberFormat;

public class ReportVouchersForm extends BasicActionForm {
    private TimeSelector timeSelRangeFrom,
                         timeSelRangeTo;
    private ScrollBox scrollBoxNominal,
                      scrollBoxSeller,
                      scrollBoxOperator;
    private BrowseBox browseBoxTerminals;
    protected Grid gridVouchersReport;
    
    private final String SLD_STATUS = "SLD";
    private final String NEW_STATUS = "NEW";
    private final String BLK_STATUS = "BLK";
    private final String RES_STATUS = "RES";

    private List<String> listNominals = new ArrayList<String>();

    private final GregorianCalendar TIME_FROM = new GregorianCalendar(1970, 1, 1, 0, 0);
    private final GregorianCalendar TIME_TO = new GregorianCalendar(3020, 1, 1, 0, 0);

    private Button buttonSubmit;

    VoucherManager voucherManager = new VoucherManager();

    String MONEY_VALUE_PATTERN = "0.00";
    DecimalFormat formatterMoney =  (DecimalFormat) DecimalFormat.getInstance();

    String QUANTITY_VALUE_PATTERN = "#####.#####";
    DecimalFormat formatterQuantity =  (DecimalFormat) DecimalFormat.getInstance();

    List<Object[]> nominalsOperators;

    private final String SEPARATOR = "::";

    protected void init() {
        initControls();
        initGrid();
    }

    private void initControls() {
        String action = "/core/report/reportVauchersAction";
        buttonSubmit = new Button();
        buttonSubmit.setLabelKey("ctrl.button.label.apply");

        buttonSubmit.setCommand("refreshVoucherReportGrid");

        scrollBoxNominal = new ScrollBox() {

            protected String getKey(Object dataListElement) {
                if(((Object[]) dataListElement)[1]!=null&&((Object[]) dataListElement)[0]!=null){
                return new StringBuilder().append(((Object[]) dataListElement)[1].toString()).
                        append(SEPARATOR).append(((Object[]) dataListElement)[0].toString()).toString();}
               else
               return ""; 

            }

            protected String getValue(Object dataListElement) {
                return ((Object[]) dataListElement)[0].toString();
            }
        };
        scrollBoxNominal.setNullable(true);

        scrollBoxSeller = new ScrollBox() {

            protected String getKey(Object dataListElement) {
                return ((Seller) dataListElement).getSellerId().toString();
            }
            protected String getValue(Object dataListElement) {
                return ((Seller) dataListElement).getName();
            }
        };
        scrollBoxSeller.setOnChange("document.forms[0].action=\"" +
                                            ApplicationEnvironment.getAppDeploymentName() +"/core/report/reportVauchersAction.do?command=" +
                                            "updateBrowseBoxTerminals\";document.forms[0].submit()");
        scrollBoxSeller.setNullable(true);

        scrollBoxOperator = new ScrollBox() {

            protected String getKey(Object dataListElement) {
                return ((Operator) dataListElement).getOperatorId().toString();
            }
            protected String getValue(Object dataListElement) {
                return ((Operator) dataListElement).getName();
            }
        };
        scrollBoxOperator.setNullable(true);

        timeSelRangeFrom = new TimeSelector();
        timeSelRangeTo = new TimeSelector();

        browseBoxTerminals = new BrowseBox("/core/balance/terminal_balance/browseTerminalsAction");
    }

    public void bindData() throws GridException, DatabaseException {
        nominalsOperators = voucherManager.getNominalsOperators();
        listNominals.clear();
        for (Object[] nominalOperator : nominalsOperators) {
            listNominals.add(nominalOperator[0].toString());
        }

        scrollBoxNominal.bind(nominalsOperators);
        scrollBoxOperator.bind(new OperatorManager().getAllOperators());


    }

//    private List<String> getNominalsFromlist() throws DatabaseException {
//        List<String> nominals = new ArrayList<String>();
//        for (Object[] nominalOperator : voucherManager.getNominalsOperators()) {
//            nominals.add(nominalOperator[0].toString());
//        }
//        return nominals;
//    }
    private List<String> getNominalsFromlist(QueryParameterWrapper[] parameterWrappers) throws DatabaseException {
        List<String> nominals = new ArrayList<String>();
        for (Object[] nominalOperator : voucherManager.getNominalsOperators(parameterWrappers)) {
            nominals.add(nominalOperator[0].toString());
        }
        return nominals;
    }

    private void initGrid() {

        formatterMoney.applyPattern(MONEY_VALUE_PATTERN);
        formatterQuantity.applyPattern(QUANTITY_VALUE_PATTERN);

        gridVouchersReport = new Grid() {
            int firstDataColumn = 4;
            public void bind(List data) throws GridException {
                List<String> nominals = new ArrayList<String>();//(List) data.get(0);

                List quantitiesNEW = (List) data.get(1);
                List sumsNEW = (List) data.get(2);

                List quantitiesNEW_RES_BLK = (List) data.get(3);
                List sumsNEW_RES_BLK = (List) data.get(4);

                List quantitiesSLD = (List) data.get(5);
                List sumsSLD = (List) data.get(6);

                List<Object[]> nominalsOperators = (List<Object[]>) data.get(7);

                for (Object[] nominalOperator : nominalsOperators) {
                    nominals.add(nominalOperator[0].toString());
                }

                clear();
     //           initHeaders(nominals, quantitiesNEW.size(), quantitiesNEW_RES_BLK.size(), nominalsOperators);
                //total sum by category
                if (quantitiesNEW.size() != 0 && quantitiesNEW_RES_BLK.size() != 0 ) {//just for sold vouchers
                    setCell(2, 1, "" + formatterQuantity.format(calculateSum(quantitiesNEW)) ,
                                        "column_data_cell");//total quantity NEW by category
                    setCell(3, 1, "" + formatterMoney.format(calculateSum(sumsNEW)),
                                        "column_data_cell");//total sum NEW by category

                    setCell(5, 1, "" + formatterQuantity.format(calculateSum(quantitiesNEW_RES_BLK)),
                                        "column_data_cell");//total quantity NEW_RES_BLK by category
                    setCell(6, 1, "" + formatterMoney.format(calculateSum(sumsNEW_RES_BLK)),
                                        "column_data_cell");//total sum NEW_RES_BLK by category
                }

                setCell(8, 1, "" + formatterQuantity.format(calculateSum(quantitiesSLD)),
                                    "column_data_cell");//total quantity SLD by category
                setCell(9, 1, "" + calculateSum(sumsSLD),
                                    "column_data_cell");//total sum SLD by category

                for (int i = 0; i < quantitiesSLD.size(); i++) {
                    if (quantitiesNEW.size() != 0 && quantitiesNEW_RES_BLK.size() != 0 ) { //just for sold vouchers
                        setCell(2, 3 + i, "" + formatterQuantity.format(quantitiesNEW.get(i)),
                                                                "column_data_cell");//quantity NEW
                        setCell(5, 3 + i, "" + formatterQuantity.format(quantitiesNEW_RES_BLK.get(i)),
                                                                "column_data_cell");//quantity NEW_RES_BLK
                    }
                    setCell(8, 3 + i, "" + formatterQuantity.format(quantitiesSLD.get(i)),
                                                            "column_data_cell");//quantity SLD
                }
                for (int i = 0; i < sumsSLD.size(); i++) {
                    if (quantitiesNEW.size() != 0 && quantitiesNEW_RES_BLK.size() != 0 ) { //just for sold vouchers
                        setCell(3, 3 + i, "" + formatterMoney.format(sumsNEW.get(i)),
                                                                "column_data_cell");//sum NEW

                        setCell(6, 3 + i, "" + formatterMoney.format(sumsNEW_RES_BLK.get(i)),
                                                                "column_data_cell");//sum NEW_RES_BLK
                    }

                    setCell(9, 3 + i, "" + formatterMoney.format(sumsSLD.get(i)),
                                                            "column_data_cell");//sum SLD
                }

            }

            private void initHeaders(List<String> nominals, int quantitiesNewSize, int quantitiesNRBSize, List<Object[]> nominalsOperators)
                                                throws GridException {
                setOperatorsHeaders(nominalsOperators);//0th row

                for (int i = 0; i < nominals.size(); i++) {
                    String titleNominal = new StringBuilder().append("<div title=\"").append(nominals.get(i)).append("\" >").toString();
                    setCell(1, 3 + i, new StringBuilder().append(titleNominal).append(nominals.get(i).replaceAll("[^0-9]", "")).toString(),
                                  "column_header_cell", "font-weight:bold");//1th row
                }

                setCell(0, 0, getLable("headerProvider"),
                                "column_header_cell", "font-weight:bold ; weigth=50%", 1, 1);//provider
                setCell(1, 0, getLable("headerNominal"),
                                "column_header_cell", "font-weight: bold ; weigth=50%", 1, 1);//nominal
                setCell(0, 2, getLable("headerTotalByCategory"),
                                "column_header_cell", "font-weight: bold ; weigth=50%", 1, 2);//total by category


                if (quantitiesNewSize != 0 && quantitiesNRBSize != 0) {
                    setCell(2, 0, getLable("rowQuantitytPrepearedVaucher"),
                                    "column_header_cell", "font-weight: bold ; weigth=50%", 1, 0);//Quantit NEW
                    setCell(3, 0, getLable("rowAmountPrepearedVaucher"),
                                    "column_header_cell", "font-weight: bold ; weigth=50%", 1, 0);//Sum NEW
                    setCell(4, 0, "&nbsp;", "column_header_cell", nominals.size() + 2, 0);//separator--

                    setCell(5, 0, getLable("rowQuantitytInStockVaucher"),
                                    "column_header_cell", "font-weight: bold ; weigth=50%", 1, 0);//Q NEW...
                    setCell(6, 0, getLable("rowAmountInStockVaucher"),
                                    "column_header_cell", "font-weight: bold ; weigth=50%", 1, 0);//S NEW...
                    setCell(7, 0, "&nbsp;", "column_header_cell", nominals.size() + 2, 0);//separator--
                }

                setCell(8, 0, getLable("rowQuantitytSoldVaucher"),
                                "column_header_cell", "font-weight: bold ; weigth=50%", 1, 0);//sum SLD
                setCell(9, 0, getLable("rowAmountSoldVaucher"),
                                "column_header_cell", "font-weight: bold ; weigth=50%", 1, 0);//amount SLD

            }

        private void setOperatorsHeaders(List<Object[]> nominalsOperators) throws GridException {
            if (nominalsOperators.size() > 0) {
                Integer operatorId = new Integer( nominalsOperators.get(0)[1].toString() );//get operator ID for first nominal
                int startPosition = 0;
                int colSpanOperator = 0;
                String operatorName = getOperatorName(operatorId);
                for (int i = 0; i < nominalsOperators.size(); i++) {
                    int tmpOperatorId = 3;
                    if (nominalsOperators.get(i)[1] != null) {
                        tmpOperatorId = new Integer( nominalsOperators.get(i)[1] .toString());
                    }

                    if (operatorId == tmpOperatorId) {
                        //calculate colspan
                        colSpanOperator++;
                    } else {
                        //set header with caclulated colspan & start position
                        setCell(0, 3 + startPosition, operatorName, "column_header_cell",
                                        "font-weight: bold", colSpanOperator, 1);//0th row
                        if (nominalsOperators.get(i)[1] != null){
                            operatorId = new Integer( nominalsOperators.get(i)[1].toString() );
                        }
                        startPosition = i+1;
                        operatorName = getOperatorName(operatorId);
                        colSpanOperator = 1;
                    }
                    //for last operator in nominalsOperators 
                    if (i == nominalsOperators.size()-1) {
                        //set header with caclulated colspan & start position
                        setCell(0, 3 + startPosition, operatorName, "column_header_cell",
                                        "font-weight: bold", colSpanOperator, 1);//0th row
                    }
                }
        }
    }

            private String getLable(String key){
                return MessageResources.getInstance().getOwnedLocalizedMessage(this.getClass(),"labels",getLocale(),key,null);
            }
        };
    }


    private String getOperatorName(int operatorId) {
        String operatorName;
        try {
            operatorName = new OperatorManager().getOperator((long) operatorId).getName();
        } catch (DatabaseException e) {
            operatorName = e.getMessage();
            e.printStackTrace();
        }

        return operatorName;
    }

    public void refresh() throws DatabaseException, GridException {
        List<List> vouchersData = composeVouchersData(null);

        gridVouchersReport.bind(vouchersData);
    }

    protected List<List> composeVouchersData(ReportSoldVouchersForm form) throws DatabaseException {
        List<List> vouchersData = new ArrayList<List>();
        List<String> filteredNominals = new ArrayList<String>();
        List filteredSumsSLD;
        List<Double> filteredQuantitiesSLD;

        List filteredSumsNEW;
        List filteredQuantitiesNEW;

        List filteredSumsNEW_RES_BLK;
        List filteredQuantitiesNEW_RES_BLK;

        List<Object[]> filteredNominalsOperators = new ArrayList<Object[]>();

        GregorianCalendar timeFrom = new GregorianCalendar();
        GregorianCalendar timeTo = new GregorianCalendar();
        timeFrom.setTime(TIME_FROM.getTime());
        timeTo.setTime(TIME_TO.getTime());

        //filter by time
        if(timeSelRangeFrom.getTime() != null) {
            timeFrom.setTime(timeSelRangeFrom.getTime());
        }
        if(timeSelRangeTo.getTime() != null) {
            timeTo.setTime(timeSelRangeTo.getTime());
        }
        //filter by nominal
        filteredNominals.addAll(listNominals);//not filtering by nominal & by operator to
        filteredNominalsOperators.addAll(nominalsOperators);//not filtering by nominal & by operator to
        if (!scrollBoxNominal.isNullKeySelected()) {
            ScrollBox.Entry scrolBoxValue = scrollBoxNominal.getSelectedEntry();
            filteredNominals.clear();
            filteredNominals.add(scrolBoxValue.getValue());//filter by nominal

            int separatorIndex = scrolBoxValue.getKey().indexOf(SEPARATOR);
            String operatorId = scrolBoxValue.getKey().substring(0, separatorIndex);
            filteredNominalsOperators.clear();
            filteredNominalsOperators.add(new Object[]{scrolBoxValue.getValue(), operatorId});
        } else {
            if (!scrollBoxOperator.isNullKeySelected()) {//filtering by Operator ID
                QueryParameterWrapper[] parameterWrapper = new QueryParameterWrapper[] {
                                  new QueryParameterWrapper("isFilterByOperatorSet", 0),
                                  new QueryParameterWrapper("operatorId", scrollBoxOperator.getSelectedKey())};

                List<Object[]> nominalsOperators = voucherManager.getNominalsOperators(parameterWrapper);

                filteredNominals.clear();
                for (Object[] nominalOperator : nominalsOperators) {
                    filteredNominals.add(nominalOperator[0].toString());
                }

                filteredNominalsOperators.clear();
                filteredNominalsOperators.addAll(nominalsOperators);
            }
        }

        //filter by seller id
        List billingNumbers = new ArrayList();
        int isFilterByBillTransNumSet = 1;//false
        if (!scrollBoxSeller.isNullKeySelected()) {
            billingNumbers = voucherManager.getBillingNumbersBySeller(new Long(scrollBoxSeller.getSelectedKey()), browseBoxTerminals.getKey());
            isFilterByBillTransNumSet = 0;//true
        }

        List<String> statuses = new ArrayList<String>();
        statuses.add(SLD_STATUS);
        filteredQuantitiesSLD = voucherManager.getVouchersQuantitySeparetedByNominal(filteredNominals, statuses,
                                                                    timeFrom.getTime(), timeTo.getTime(),
                                                                    billingNumbers, isFilterByBillTransNumSet);
        filteredSumsSLD = voucherManager.getVouchersSumSeparetedByNominal(filteredNominals, statuses,
                                                                    timeFrom.getTime(), timeTo.getTime(),
                                                                    billingNumbers, isFilterByBillTransNumSet);

        statuses.clear();
        statuses.add(NEW_STATUS);
        if(form == null) {//in futute condition will be more cool
            filteredQuantitiesNEW = voucherManager.getVouchersQuantitySeparetedByNominal(filteredNominals, statuses,
                                                                    timeFrom.getTime(), timeTo.getTime(),
                                                                    billingNumbers, isFilterByBillTransNumSet);
            filteredSumsNEW = voucherManager.getVouchersSumSeparetedByNominal(filteredNominals, statuses,
                                                                    timeFrom.getTime(), timeTo.getTime(),
                                                                    billingNumbers, isFilterByBillTransNumSet);
        } else {//just for sold vouchers
            filteredQuantitiesNEW = Collections.EMPTY_LIST;
            filteredSumsNEW = Collections.EMPTY_LIST;
        }

//        statuses.add(RES_STATUS);
        statuses.clear();
        statuses.add(BLK_STATUS);
        if (form == null) {//in futute condition will be more cool
            filteredQuantitiesNEW_RES_BLK = voucherManager.getVouchersQuantitySeparetedByNominal(filteredNominals, statuses,
                                                                    timeFrom.getTime(), timeTo.getTime(),
                                                                    billingNumbers, isFilterByBillTransNumSet);
            filteredSumsNEW_RES_BLK = voucherManager.getVouchersSumSeparetedByNominal(filteredNominals, statuses,
                                                                    timeFrom.getTime(), timeTo.getTime(),
                                                                    billingNumbers, isFilterByBillTransNumSet);
        } else {//just for sold vouchers
            filteredQuantitiesNEW_RES_BLK = Collections.EMPTY_LIST;
            filteredSumsNEW_RES_BLK = Collections.EMPTY_LIST;
        }

        vouchersData.add(filteredNominals);

        vouchersData.add(filteredQuantitiesNEW);
        vouchersData.add(filteredSumsNEW);

        vouchersData.add(filteredQuantitiesNEW_RES_BLK);
        vouchersData.add(filteredSumsNEW_RES_BLK);

        vouchersData.add(filteredQuantitiesSLD);
        vouchersData.add(filteredSumsSLD);

        vouchersData.add(filteredNominalsOperators);
        
        return vouchersData;
    }

    /**
     * Methods calculates sum of prices for each status
     * @param data
     * @return sum
     */
    private Double calculateSum(List data) {
        Double sum = (double) 0;
        for (Object element : data) {
          sum = sum + (Double) element;
        }
        return sum;
    }

    /**
     *Gets amount of vouchers for each status
     * @param data
     * @param vcStatus
     * @return amount
     */
    private int calculateAmount(List<Voucher> data, String vcStatus) {
        int sum = 0;
        for (Voucher objVoucher : data) {
            Voucher voucher = objVoucher;
            if (voucher.getStatus().indexOf(vcStatus) != -1) {
                sum++;
            }
        }
        return sum;
    }


//    GETTERS - SETTERS
    public TimeSelector getTimeSelRangeFrom() {
        return timeSelRangeFrom;
    }

    public void setTimeSelRangeFrom(TimeSelector timeSelRangeFrom) {
        this.timeSelRangeFrom = timeSelRangeFrom;
    }

    public TimeSelector getTimeSelRangeTo() {
        return timeSelRangeTo;
    }

    public void setTimeSelRangeTo(TimeSelector timeSelRangeTo) {
        this.timeSelRangeTo = timeSelRangeTo;
    }

    public Grid getGridVouchersReport() {
        return gridVouchersReport;
    }

    public void setGridVouchersReport(Grid gridVouchersReport) {
        this.gridVouchersReport = gridVouchersReport;
    }

    public ScrollBox getScrollBoxNominal() {
        return scrollBoxNominal;
    }

    public void setScrollBoxNominal(ScrollBox scrollBoxNominal) {
        this.scrollBoxNominal = scrollBoxNominal;
    }

    public List<String> getListNominals() {
        return listNominals;
    }

    public void setListNominals(List<String> listNominals) {
        this.listNominals = listNominals;
    }

    public Button getButtonSubmit() {
        return buttonSubmit;
    }

    public void setButtonSubmit(Button buttonSubmit) {
        this.buttonSubmit = buttonSubmit;
    }

    public ScrollBox getScrollBoxSeller() {
        return scrollBoxSeller;
    }

    public void setScrollBoxSeller(ScrollBox scrollBoxSeller) {
        this.scrollBoxSeller = scrollBoxSeller;
    }

    public ScrollBox getScrollBoxOperator() {
        return scrollBoxOperator;
    }

    public void setScrollBoxOperator(ScrollBox scrollBoxOperator) {
        this.scrollBoxOperator = scrollBoxOperator;
    }

    public BrowseBox getBrowseBoxTerminals() {
        return browseBoxTerminals;
    }

    public void setBrowseBoxTerminals(BrowseBox browseBoxTerminals) {
        this.browseBoxTerminals = browseBoxTerminals;
    }
}

