package com.tmx.web.actions.core.tools;

import com.tmx.beng.base.MediumResolver;
import com.tmx.beng.httpsgate.avancel.AvancelGate1;
import com.tmx.beng.httpsgate.citypay.BasicCitypayGate;
import com.tmx.beng.httpsgate.citypay.CitypayGateForLifeExc;
import com.tmx.beng.httpsgate.kyivstar.KyivstarBonusNoCommissionGate;
import com.tmx.beng.httpsgate.kyivstar.KyivstarExclusiveGate;
import com.tmx.beng.httpsgate.kyivstar.KyivstarGate;
import com.tmx.beng.httpsgate.umc.UmcGate;
import com.tmx.web.forms.core.tools.CitypayTransactionInfoForm;
import com.tmx.web.forms.core.tools.GatesBalancesForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForm;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.tmx.web.base.BasicDispatchedAction;
import com.tmx.beng.httpsgate.citypay.CitypayGate;

import java.util.HashMap;


public class CitypayTransactionInfoAction extends BasicDispatchedAction {

    private final String FORWARD = "citypayTransactionInfo";

    protected ActionForward defaultAction(ActionMapping mapping,
                                          ActionForm form,
                                          HttpServletRequest request,
                                          HttpServletResponse response,
                                          String methodName) throws Throwable {
        //return getStatus(mapping, form, request, response);
        return mapping.findForward(FORWARD);
    }

    public ActionForward getStatus(ActionMapping mapping,
                                        ActionForm form,
                                        HttpServletRequest request,
                                        HttpServletResponse response) throws Throwable {

        CitypayTransactionInfoForm f = (CitypayTransactionInfoForm) form;

        if(f.getBillingNumber().getValue() != null)
        {
            try{
                f.setCitypayStatusInfo(CitypayGate.getInstance().getStatusFromCitypay(f.getBillingNumber().getValue()));
            }catch(Throwable e){
                f.setCitypayStatusInfo(e.getLocalizedMessage());
            }

            /*
            try{
                f.setCitypayForLifeExcStatusInfo(CitypayGateForLifeExc.getInstance().getStatusFromCitypay(f.getBillingNumber().getValue()));
            }catch(Throwable e){
                f.setCitypayForLifeExcStatusInfo(e.getLocalizedMessage());
            }*/
        }
      //  f.getBillingNumber().setValue("12950187678020");
        return mapping.findForward(FORWARD);
    }

    public ActionForward getStatusLifeExc(ActionMapping mapping,
                                        ActionForm form,
                                        HttpServletRequest request,
                                        HttpServletResponse response) throws Throwable {

        CitypayTransactionInfoForm f = (CitypayTransactionInfoForm) form;

        if(f.getBillingNumber().getValue() != null)
        {
            /*
            try{
                f.setCitypayStatusInfo(CitypayGate.getInstance().getStatusFromCitypay(f.getBillingNumber().getValue()));
            }catch(Throwable e){
                f.setCitypayStatusInfo(e.getLocalizedMessage());
            } */

            try{
                f.setCitypayForLifeExcStatusInfo(CitypayGateForLifeExc.getInstance().getStatusFromCitypay(f.getBillingNumber().getValue()));
            }catch(Throwable e){
                f.setCitypayForLifeExcStatusInfo(e.getLocalizedMessage());
            }
        }
      //  f.getBillingNumber().setValue("12950187678020");
        return mapping.findForward(FORWARD);
    }

}
