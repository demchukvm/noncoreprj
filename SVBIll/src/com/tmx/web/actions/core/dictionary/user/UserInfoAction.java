package com.tmx.web.actions.core.dictionary.user;

import com.tmx.as.blogic.UserManager;
import com.tmx.as.entities.general.user.User;
import com.tmx.web.actions.core.AbstractSvbillAction;
import com.tmx.web.base.ApplicationEnvironment;
import com.tmx.web.base.Roadmap;
import com.tmx.web.base.SecurityService;
import com.tmx.web.forms.core.base.AbstractFormFactory;
import com.tmx.web.forms.core.dictionary.user.UserInfoForm;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;


public class UserInfoAction extends AbstractSvbillAction {
    
    private static final String FORWARD_MANAGE_USERS_ACTION = "manageUsersAction";
    private static final String FORWARD_USER_INFO_PAGE = "userInfoPage";

    public ActionForward preloadForUpdate(ActionMapping mapping,
                                          ActionForm form,
                                          HttpServletRequest request,
                                          HttpServletResponse response) throws Throwable {
        UserInfoForm userInfoForm = (UserInfoForm) verifyForm(form, UserInfoForm.class);

        List roles = new UserManager().getAllRoles();
        List organizatinos = new UserManager().getAllOrganizations();

        User user = new UserManager().getUser(userInfoForm.getSelectedUserId());

        userInfoForm.bindData(user, roles, organizatinos);

        userInfoForm.getSelectOrganizations().setSelectedKey(user.getOrganization().getOrganizationId().toString());

        AbstractFormFactory.getInstance(getLoggedInUser(request)).setUpForm(userInfoForm);

        userInfoForm.getScrollBoxSellers().setSelectedKey(user.getSellerOwner().getSellerId().toString());

        return mapping.findForward(FORWARD_USER_INFO_PAGE);
    }


    public ActionForward preloadForCreate(ActionMapping mapping,
                                          ActionForm form,
                                          HttpServletRequest request,
                                          HttpServletResponse response) throws Throwable {

        UserInfoForm userInfoForm = (UserInfoForm) verifyForm(form, UserInfoForm.class);

        //Preload all roles
        List roles = new UserManager().getAllRoles();
        List organizatinos = new UserManager().getAllOrganizations();
        userInfoForm.bindData(new User(), roles, organizatinos);

        AbstractFormFactory.getInstance(getLoggedInUser(request)).setUpForm(userInfoForm);

        return mapping.findForward(FORWARD_USER_INFO_PAGE);
    }


    public ActionForward save(ActionMapping mapping,
                              ActionForm form,
                              HttpServletRequest request,
                              HttpServletResponse response) throws Throwable {
        UserInfoForm userInfoForm = (UserInfoForm) verifyForm(form, UserInfoForm.class);
        userInfoForm.validate(mapping.findForward(FORWARD_USER_INFO_PAGE));

        //
        userInfoForm.setCheckBoxActive(userInfoForm.is_checkBoxActive());

        User savedUser = userInfoForm.populateFromControls(new User());
        new UserManager().saveUser(savedUser);
        return mapping.findForward(FORWARD_MANAGE_USERS_ACTION);
    }

    public ActionForward preloadForDelete(ActionMapping mapping,
                                          ActionForm form,
                                          HttpServletRequest request,
                                          HttpServletResponse response) throws Throwable {
        Roadmap roadmap=getRoadmap();
        roadmap.collectParameter("/core/message/confirmation", "leftMenuName", "dictionaries");
        roadmap.setPageAction("confirmation","onOk",getName());
        roadmap.setPageCommand("confirmation","onOk","delete");
        roadmap.setPageForward("confirmation","onCancel","local:"+this.getName()+"#"+FORWARD_MANAGE_USERS_ACTION);
        return mapping.findForward("confirmation");
    }

    public ActionForward delete(ActionMapping mapping,
                              ActionForm form,
                              HttpServletRequest request,
                              HttpServletResponse response) throws Throwable {
        UserInfoForm formT = (UserInfoForm) verifyForm(form, UserInfoForm.class);
        new UserManager().DELETE(User.class,formT.getSelectedUserId());
        return mapping.findForward(FORWARD_MANAGE_USERS_ACTION);
    }

    public ActionForward defaultAction(ActionMapping mapping,
                                       ActionForm form,
                                       HttpServletRequest request,
                                       HttpServletResponse response,
                                       String methodName) throws Throwable {

        return preloadForCreate(mapping, form, request, response);
    }

}
