package com.tmx.beng.iso8583Gate.mapper;

import java.util.regex.Pattern;

/**

 */
public class RequestMapping {
    private String terminalSnRegExp = null;
    private String sellerCode = null;
    private String processingCode = null;
    private String login = null;
    private String password = null;
    private String protocolVersion = null;
    private String useExtendedMapping = null;
    private Pattern pattern = null;


    public String getTerminalSnRegExp() {
        return terminalSnRegExp;
    }

    public void setTerminalSnRegExp(String terminalSnRegExp) {
        this.terminalSnRegExp = terminalSnRegExp;
        this.pattern = Pattern.compile(terminalSnRegExp);
    }

    public String getSellerCode() {
        return sellerCode;
    }

    public void setSellerCode(String sellerCode) {
        this.sellerCode = sellerCode;
    }

    public String getProcessingCode() {
        return processingCode;
    }

    public void setProcessingCode(String processingCode) {
        this.processingCode = processingCode;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public String getUseExtendedMapping() {
        return useExtendedMapping;
    }

    public void setUseExtendedMapping(String useExtendedMapping) {
        this.useExtendedMapping = useExtendedMapping;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Pattern getPattern() {
        return pattern;
    }

    public String getProtocolVersion() {
        return protocolVersion;
    }

    public void setProtocolVersion(String protocolVersion) {
        this.protocolVersion = protocolVersion;
    }
}
