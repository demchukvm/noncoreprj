package com.tmx.beng.httpsgate.cyberplat;

import com.tmx.beng.base.BillingMessage;
import com.tmx.beng.httpsgate.HttpsGateException;
import com.tmx.util.queue.ActionQueueElement;

public class CyberplatGateTask extends ActionQueueElement
{
    private BillingMessage procesedMessage;
    private BasicCyberplatGate gate;

    public CyberplatGateTask(BillingMessage billingMessage, BasicCyberplatGate gate)
    {
        this.gate = gate;
        this.procesedMessage = billingMessage;
    }

    public void run() {
        try{
            gate.runBillMessage(procesedMessage);
        }
        catch(HttpsGateException e){
            gate.logger.error(e.getLocalizedMessage(), e);
        }
    }
}
