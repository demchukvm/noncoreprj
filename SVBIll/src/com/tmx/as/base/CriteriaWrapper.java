package com.tmx.as.base;

import com.tmx.as.base.criterion.CriterionWrapper;

import java.util.*;

/**
 Handle criteries hierarchy to be executed by single EntityManager.RETRIEVE() or
 RETRIEVE_ALL() call.

 Tree of criteries represents a joinig model of corresponding tables in DB.
 Tree is built on from list of given table attributes, orders, projections.
 */
public class CriteriaWrapper {
    private final String DOT = ".";
    private final String UNDER_SCORE = "_";
    private final String EMPTY_NAME = "";
    private CriteriaWrapper parentCriteria;
    private Map childCriterias;
    private String name;
    private Set tableAttrs;
    private Set orderWrappers;
    private Set projectionWrappers;
    private Set criterionWrappers;
    private String alias;


    public CriteriaWrapper(){
        this.childCriterias = new HashMap();
        this.tableAttrs = new HashSet();
        this.orderWrappers = new LinkedHashSet();
        this.projectionWrappers = new HashSet();
        this.criterionWrappers=new HashSet();
    }

    public void build(String[] tableAttrs, OrderWrapper[] orders, ProjectionWrapper[] projections, CriterionWrapper[] criterions){
        //build root criteria
        name = EMPTY_NAME;
        parentCriteria = null;

        if(tableAttrs != null)
            this.tableAttrs.addAll(Arrays.asList(tableAttrs));
        if(orders != null)
            this.orderWrappers.addAll(Arrays.asList(orders));
        if(projections != null)
            this.projectionWrappers.addAll(Arrays.asList(projections));
        if(criterions != null)
            this.criterionWrappers.addAll(Arrays.asList(criterions));
        build0();
    }


    private void build0(){
        processTableAttrs();
        processOrders();
        processCriterions();
        processProjections();
        //process childs
        for(Iterator iter = childCriterias.values().iterator(); iter.hasNext();)
            ((CriteriaWrapper)iter.next()).build0();
    }


    private void processTableAttrs(){
        if(tableAttrs == null)
            return;
        Set unprocessedTableAttrs = tableAttrs;
        tableAttrs = new HashSet();//for processed table attrs
        for(Iterator iter = unprocessedTableAttrs.iterator(); iter.hasNext();){
            String tableAttr = (String)iter.next();
            String[] nameAndPath = splitCriteriaName(tableAttr);
            if(EMPTY_NAME.equals(nameAndPath[0])){
                obtainChildCriteria(nameAndPath[1]);
                this.tableAttrs.add(tableAttr);
            }else{
                CriteriaWrapper criteriaWrapper = obtainChildCriteria(nameAndPath[0]);
                criteriaWrapper.tableAttrs.add(nameAndPath[1]);
            }
        }
    }


    private void processOrders(){
        if(orderWrappers == null)
            return;
        Set unprocessedOrderWrappers = orderWrappers;
        orderWrappers = new LinkedHashSet();//for processed orders
        for(Iterator iter = unprocessedOrderWrappers.iterator(); iter.hasNext();){
            OrderWrapper orderWrapper = (OrderWrapper)iter.next();
            String[] nameAndPath = splitCriteriaName(orderWrapper.getOrderByFieldName());
            if(EMPTY_NAME.equals(nameAndPath[0]))
                this.orderWrappers.add(orderWrapper);
            else{
                CriteriaWrapper criteriaWrapper = obtainChildCriteria(nameAndPath[0]);
                criteriaWrapper.orderWrappers.add(new OrderWrapper(
                        nameAndPath[1],
                        orderWrapper.getOrderType()));
            }
        }
    }

    private void processCriterions(){
        if(criterionWrappers == null||criterionWrappers.size()==0)
            return;
        Set unprocessedCriterionWrappers = criterionWrappers;
        HashSet pathes=new HashSet();// read all pathes
        for(Iterator iter = unprocessedCriterionWrappers.iterator(); iter.hasNext();){
            CriterionWrapper criterionWrapper = (CriterionWrapper)iter.next();
            getAllCriterionWrapperPathesAndConvertToHiberMode(criterionWrapper,pathes);
        }
        // create aliases from pathesd
        for(Iterator iter = pathes.iterator(); iter.hasNext();){
            String property = (String)iter.next();
            CriteriaWrapper criteriaWrapper = obtainChildCriteria(property);
            criteriaWrapper.setAlias(property);
        }
    }

    private void getAllCriterionWrapperPathesAndConvertToHiberMode(CriterionWrapper criterionWrapper,HashSet pathes){
        String propertyName=(String)criterionWrapper.getParam("propertyName");
        if(propertyName!=null){
            String[] propertyPathAndName=getConvertHiberhatePathAndName(propertyName);
            if(!propertyPathAndName[0].equals(EMPTY_NAME)){
                pathes.add(propertyPathAndName[0]);
                criterionWrapper.addParam("propertyName",propertyPathAndName[0]+"."+propertyPathAndName[1]);
            }
        }
        String otherPropertyName=(String)criterionWrapper.getParam("otherPropertyName");
        if(otherPropertyName!=null){
            String[] propertyPathAndName=getConvertHiberhatePathAndName(otherPropertyName);
            if(!propertyPathAndName[0].equals(EMPTY_NAME)){
                pathes.add(propertyPathAndName[0]);
                criterionWrapper.addParam("otherPropertyName",propertyPathAndName[0]+"."+propertyPathAndName[1]);
            }     
        }
        CriterionWrapper lhs=(CriterionWrapper)criterionWrapper.getParam("lhs");
        if(lhs!=null)getAllCriterionWrapperPathesAndConvertToHiberMode(lhs,pathes);
        CriterionWrapper rhs=(CriterionWrapper)criterionWrapper.getParam("rhs");
        if(rhs!=null)getAllCriterionWrapperPathesAndConvertToHiberMode(rhs,pathes);
        CriterionWrapper expression=(CriterionWrapper)criterionWrapper.getParam("expression");
        if(expression!=null)getAllCriterionWrapperPathesAndConvertToHiberMode(expression,pathes);
    }

    private String[] getConvertHiberhatePathAndName(String propName){
        String[] propertyPathAndName=splitCriteriaPath(propName);
        if(propertyPathAndName.length==0)return propertyPathAndName;
        propertyPathAndName[0]=propertyPathAndName[0].replaceAll("\\.",UNDER_SCORE);
        return propertyPathAndName;
    }

    private void processProjections(){
        if(projectionWrappers == null)
            return;
        Set unprocessedProjectionWrappers = projectionWrappers;
        projectionWrappers = new HashSet();//for processed projections
        for(Iterator iter = unprocessedProjectionWrappers.iterator(); iter.hasNext();){
            ProjectionWrapper projectionWrapper = (ProjectionWrapper)iter.next();
            String[] nameAndPath = splitCriteriaName(projectionWrapper.getName());
            if(EMPTY_NAME.equals(nameAndPath[0]))
                this.projectionWrappers.add(projectionWrapper);
            else{
                CriteriaWrapper criteriaWrapper = obtainChildCriteria(nameAndPath[0]);
                criteriaWrapper.projectionWrappers.add(new ProjectionWrapper(
                        nameAndPath[1],
                        projectionWrapper.getAlias(),
                        projectionWrapper.getType()));
            }
        }
    }

    private String[] splitCriteriaPath(String properyName){
        int lastDotIdx = properyName.lastIndexOf(DOT);
        String[] nameAndPath = new String[2];
        nameAndPath[0] = (lastDotIdx < 0) ? EMPTY_NAME : properyName.substring(0, lastDotIdx);
        nameAndPath[1] = (lastDotIdx < 0) ? properyName : properyName.substring(lastDotIdx+1);
        return nameAndPath;
    }

    private String[] splitCriteriaName(String associationPath){
        int firstDotIdx = associationPath.indexOf(DOT);
        String[] nameAndPath = new String[2];
        nameAndPath[0] = (firstDotIdx < 0) ? EMPTY_NAME : associationPath.substring(0, firstDotIdx);
        nameAndPath[1] = (firstDotIdx < 0) ? associationPath : associationPath.substring(firstDotIdx+1);
        return nameAndPath;
    }

    private CriteriaWrapper obtainChildCriteria(String name){
        String[] names=name.split(UNDER_SCORE);
        if(names.length==0)names=new String[]{name};
        CriteriaWrapper childCriteria=this;
        for (int i = 0; i < names.length; i++) {
            String s = names[i];
            childCriteria=childCriteria.obtainChildCriteria0(s);
        }
        return childCriteria;
    }

    private CriteriaWrapper obtainChildCriteria0(String name){
        CriteriaWrapper criteria = (CriteriaWrapper)childCriterias.get(name);
        if(criteria == null){
            criteria = new CriteriaWrapper();
            criteria.setName(name);
            criteria.setParentCriteria(this);
            childCriterias.put(name, criteria);            
        }
        return criteria;
    }

    //------------Properties
    public CriteriaWrapper getParentCriteria() {
        return parentCriteria;
    }

    private void setParentCriteria(CriteriaWrapper parentCriteria) {
        this.parentCriteria = parentCriteria;
    }


    public String getName() {
        return name;
    }

    private void setName(String name) {
        this.name = name;
    }

    public CriteriaWrapper getChildCriteria(String name){
        return (CriteriaWrapper)childCriterias.get(name);
    }

    public Iterator getChildCriterias(){
        return childCriterias.values().iterator();
    }

    public String[] getTableAttrs(){
        if(tableAttrs == null)
            return new String[0];
        else
            return (String[])tableAttrs.toArray(new String[tableAttrs.size()]);
    }

    public OrderWrapper[] getOrders(){
        if(orderWrappers == null)
            return new OrderWrapper[0];
        else
            return (OrderWrapper[])orderWrappers.toArray(new OrderWrapper[orderWrappers.size()]);
    }

    public CriterionWrapper[] getCriterions(){
        if(criterionWrappers == null)
            return new CriterionWrapper[0];
        else
            return (CriterionWrapper[])criterionWrappers.toArray(new CriterionWrapper[criterionWrappers.size()]);
    }

    public ProjectionWrapper[] getProjections(){
        if(projectionWrappers == null)
            return new ProjectionWrapper[0];
        else
            return (ProjectionWrapper[])projectionWrappers.toArray(new ProjectionWrapper[projectionWrappers.size()]);
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }
}
