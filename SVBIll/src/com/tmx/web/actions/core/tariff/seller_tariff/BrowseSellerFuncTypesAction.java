package com.tmx.web.actions.core.tariff.seller_tariff;

import com.tmx.web.actions.core.base.AbstractBrowseAction;
import com.tmx.web.forms.core.tariff.BrowseFunctionTypesForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForm;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class BrowseSellerFuncTypesAction extends AbstractBrowseAction {

    public ActionForward defaultAction(ActionMapping mapping, ActionForm form, HttpServletRequest request,
                                       HttpServletResponse response, String methodName) throws Throwable {
        BrowseFunctionTypesForm abstractBrowseForm = (BrowseFunctionTypesForm) form;
        abstractBrowseForm.setUpCategory("SELLER_TARIFF");
        return super.defaultAction(mapping, form, request, response, methodName);
    }

    protected String getForwardName() {
        return "browseFunctionTypesPage";
    }
    
}
