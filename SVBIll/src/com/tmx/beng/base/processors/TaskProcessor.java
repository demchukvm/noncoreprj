package com.tmx.beng.base.processors;

import com.tmx.beng.base.Task;
import com.tmx.beng.base.BillException;
import com.tmx.beng.base.BillingMessageWritable;
import com.tmx.beng.base.cache.DBCache;
import com.tmx.beng.base.cache.DBCacheException;

/**
 Close transaction task
 */
abstract public class TaskProcessor extends BasicProcessor {

    public Task process(Task task) throws BillException, DBCacheException {
        return process0(task, task.getSessionDBCache(), task.getProcesedMessage());
    }

    public Task rollback(Task task, Throwable e){
        return task;
    }

    /** With prepared generally used parametes: dbCache, msg. */
    protected abstract Task process0(Task task, DBCache dbCache, BillingMessageWritable msg) throws BillException, DBCacheException;

}
