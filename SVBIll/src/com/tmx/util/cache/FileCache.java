package com.tmx.util.cache;

import com.tmx.util.Configuration;
import com.tmx.util.FileUtil;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 implements method <code>getRealObject()</code> to return desired data in memory
 readed from file. possible after consuming parsing.
 */
public abstract class FileCache extends BasicCacheImpl{
    /** @param is - is the opened input stream to get cached object from.
     *  */
    protected abstract Object getRealObjectFromFile(InputStream is, String path) throws CacheException;

    protected Object getRealObject(Object filePath) throws CacheException {
        Object realObject = null;
//System.out.println("FILE_CACHE: "+filePath);
        String filePathStr = (String) filePath;
        try {
            File file = new File(filePathStr);
            InputStream is = null;
            if (file.isFile()) {
                is = new FileInputStream(file);
            }
            else if(FileUtil.isFilepathInsideJar(filePathStr)){
                is = FileUtil.loadFromJar(filePathStr);
            }
            else
                throw new CacheException("err.cache.file_cache.bad_filepath_to_get_real_object", new String[]{filePathStr});

            if(filePathStr == null)
                throw new CacheException("err.cache.file_cache.file_path_is_null");

            if(is == null)
                throw new CacheException("err.cache.file_cache.input_stream_is_null", new String[]{filePathStr});

            //call successors method
            realObject = getRealObjectFromFile(is, filePathStr);

            try{
                is.close();
            }
            catch(IOException e){/** ignore if file was closed in successors */}
        }
        catch(CacheException e){
            throw e;//re-throw
        }
        catch (Exception e) {
            throw new CacheException("err.cache.file_cache.failed_to_get_real_object", new String[]{filePathStr}, e);
        }
        return realObject;
    }


    /**
     * @param filePath is the file system local file path (key)
    */
    protected CachedObjectSignature createSignature(Object filePath) throws CacheException{
        FileSignature signature = null;
        try{
            signature = new FileSignature();
            signature.setFilePath((String)filePath);
            signature.setLastModified(new File(signature.getFilePath()).lastModified());
        }
        catch(Exception e){
            throw new CacheException("err.cache.file_cache.failed_to_create_signature", new String[]{((filePath == null) ? null : filePath.toString())}, e);
        }
        return signature;
    }

    private class FileSignature implements CachedObjectSignature{
        private long lastModified = -1;
        private String filePath = null;

        private void setLastModified(long lastModified) {
            this.lastModified = lastModified;
        }

        private void setFilePath(String filePath) {
            this.filePath = filePath;
        }

        private String getFilePath() {
            return filePath;
        }

        public boolean isUpToDate() throws CacheException{
            File file = new File(filePath);
            return (!file.exists() || file.lastModified() > lastModified) ? false : true;
        }
    }
}
