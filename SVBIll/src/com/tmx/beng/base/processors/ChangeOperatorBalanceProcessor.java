package com.tmx.beng.base.processors;

import com.tmx.as.base.ProcedureWrapper;
import com.tmx.as.base.QueryParameterWrapper;
import com.tmx.as.entities.bill.balance.OperatorBalance;
import com.tmx.as.entities.bill.operator.Operator;
import com.tmx.as.entities.bill.transaction.OperatorBalanceTransaction;
import com.tmx.as.entities.bill.transaction.TransactionSupport;
import com.tmx.as.entities.bill.transaction.TransactionType;
import com.tmx.as.entities.general.user.User;
import com.tmx.beng.base.*;
import com.tmx.beng.base.cache.DBCache;
import com.tmx.beng.base.cache.DBCacheException;

import java.sql.Types;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class ChangeOperatorBalanceProcessor extends TaskProcessor{

    protected Task process0(Task task, DBCache dbCache, BillingMessageWritable msg) throws BillException {
//        try {
//            //create
//            OperatorBalanceTransaction operatorBalanceTransaction = new OperatorBalanceTransaction();
//            //setup
//            operatorBalanceTransaction.setTransactionTime(getTransRegTime(msg));
//            operatorBalanceTransaction.setClientTime(getClientTime(msg));
//            operatorBalanceTransaction.setAmount(getAmount(msg));
//            operatorBalanceTransaction.setOperator(getOperator(msg, dbCache));
//            operatorBalanceTransaction.setOperatorBalance(getOperatorBalance(msg, dbCache));
//            operatorBalanceTransaction.setTransactionSupport(getTransactionSupport(msg));//not supported
//            operatorBalanceTransaction.setTransactionGUID(getTransactionGUID(msg));
//            operatorBalanceTransaction.setCliTransactionNum(getCliTransactionNum(msg));
//
//
//            TransactionType type = new TransactionType();
//            type.setCode(msg.getAttributeString(BillingMessage.TRANSACTION_TYPE_ID));
//            operatorBalanceTransaction.setType(type);
//
//            dbCache.reserveOperatorBalance(msg.getAttributeString(BillingMessage.SERVICE_CODE), getRealAmount(msg));
//            msg.setAttributeString(BillingMessage.OPERATOR_TARIFFED_AMOUNT_CHANGED, "true");
//
//            //----------------------
//
//            msg.setStatusCode(StatusDictionary.STATUS_OK);
//
//            /*TODO:(AN) rube! SaveToDatabaseTransactionProcessor has hardcode:
//            task.getGlobalContext().getTransactionPool().lookupTask(
//                    msg.getAttributeString(BillingMessage.REQUEST_ID),
//                    BillingMessage.O_REFILL_PAYMENT
//                );*/
//            msg.setOperationName(BillingMessage.O_REFILL_PAYMENT);
//
//            //add to message
//            msg.setAttribute(BillingMessage.ENTITY_LIST, getEntityList(msg, operatorBalanceTransaction));
//        } catch (Throwable e) {
//            task.getProcesedMessage().setStatusCode(StatusDictionary.RESERVATION_ERROR);
//            logger.error(e.getLocalizedMessage(), e);
//            if(e instanceof BillException)
//                throw (BillException)e;
//            throw new BillException(e.getLocalizedMessage(), e);
//        }
        return task;
    }

//    private List getEntityList(BillingMessageWritable msg, OperatorBalanceTransaction operatorBalanceTransaction) {
//        List list = new ArrayList();
//        list.add(operatorBalanceTransaction);
//        list.add(getProcedureWrapper(msg, operatorBalanceTransaction));
//        return list;
//    }
//
//    private ProcedureWrapper getProcedureWrapper(BillingMessageWritable msg, OperatorBalanceTransaction operatorBalanceTransaction) {
//        return new ProcedureWrapper(
//                new QueryParameterWrapper[]{
//                        QueryParameterWrapper.createOutParameter(1, Types.INTEGER),
//                        new QueryParameterWrapper(2, operatorBalanceTransaction.getOperator().getOperatorId()),
//                        new QueryParameterWrapper(3, getRealAmount(msg)),
//                },
//                Operator.class,
//                "change_operator_balance");
//    }
//
//    private String getCliTransactionNum(BillingMessageWritable msg) {
//        return msg.getAttributeString(BillingMessage.CLI_TRANSACTION_NUM);
//    }
//
//    private String getTransactionGUID(BillingMessageWritable msg) {
//        return msg.getAttributeString(BillingMessage.BILL_TRANSACTION_NUM);
//    }
//
//    private TransactionSupport getTransactionSupport(BillingMessageWritable msg) {
//        //transaction support
//        TransactionSupport transSupport = new TransactionSupport();
//        //user for transaction support
//        Long userActorId = (Long)msg.getAttribute(BillingMessage.USER_ACTOR_ID);
//        User userActor = new User();
//        userActor.setUserId(userActorId);
//        transSupport.setUserActor(userActor);
//        //-----------------------------
//        transSupport.setPaymentReason( msg.getAttributeString(BillingMessage.PAYMENT_REASON) );
//        transSupport.setDescription(msg.getAttributeString(BillingMessage.TRANSACTION_DESCRIPTION));
//        return transSupport;
//    }
//
//    private OperatorBalance getOperatorBalance(BillingMessageWritable msg, DBCache dbCache) throws DBCacheException {
//        OperatorBalance operatorBalance = new OperatorBalance();
//        operatorBalance.setBalanceId(dbCache.getOperatorPrimaryBalanceId(msg.getAttributeString(BillingMessage.SERVICE_CODE)));
//        return operatorBalance;
//    }
//
//    private Date getClientTime(BillingMessageWritable msg) {
//        return (Date)msg.getAttribute(BillingMessage.CLIENT_TIME);
//    }
//
//    private Date getTransRegTime(BillingMessageWritable msg) {
//        return (Date)msg.getAttribute(BillingMessage.REGISTRATION_TIME);
//    }
//
//    private Operator getOperator(BillingMessageWritable msg, DBCache dbCache) throws DBCacheException {
//        Operator operator = new Operator();
//        operator.setOperatorId(dbCache.getOperatorId(msg.getAttributeString(BillingMessage.SERVICE_CODE)));
//        return operator;
//    }
//
//    private Double getAmount(BillingMessageWritable msg) {
//        return (Double)msg.getAttribute(BillingMessage.OPERATOR_TARIFFED_AMOUNT);
//    }
//
//    private Double getRealAmount(BillingMessageWritable msg){
//        String transactionType = msg.getAttributeString(BillingMessage.TRANSACTION_TYPE_ID);
//        return "2".equals(transactionType) ? new Double(getAmount(msg).doubleValue() * (-1)) : getAmount(msg);
//    }
//
//    public Task rollback(Task task, Throwable e) {
//        logger.info("Rollback Change...");
//        try{
//
//            BillingMessageWritable msg = task.getProcesedMessage();
//            DBCache dbCache = task.getGlobalContext().getDbCache();
//
//            String changed = msg.getAttributeString(BillingMessage.OPERATOR_TARIFFED_AMOUNT_CHANGED);
//            if(changed != null && changed.equalsIgnoreCase("true")){
//                dbCache.reserveOperatorBalance(
//                        msg.getAttributeString(BillingMessage.SERVICE_CODE),
//                        new Double( -getRealAmount(msg).doubleValue() )
//                );
//            }
//
//        }
//        catch(Throwable te){
//            logger.error(te.getLocalizedMessage(), te);
//        }
//
//        return super.rollback(task, e);
//    }

}
