<%@ page language="java" pageEncoding="UTF-8" contentType="text/html; charset=utf-8" isELIgnored="false" %>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean" %>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html" %>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic" %>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://tmx.com/taglib/tmx-logic" prefix="tmx_logic" %>
<%@ taglib uri="http://tmx.com/taglib/tmx-controls" prefix="tmx_ctrl" %>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-nested" prefix="nested" %>
        <!-- Setup form method -->
        <c:set var="methodNullSafe" value="${requestScope['tile.abstractLayout.nestedForm.method']}"/>
        <c:if test="${empty methodNullSafe}">
            <c:set var="methodNullSafe" value="post"/>
        </c:if>

        <!-- Setup form enctype -->
        <c:set var="enctypeNullSafe" value="${requestScope['tile.abstractLayout.nestedForm.enctype']}"/>

        <tmx_ctrl:form
                action="${requestScope['com.tmx.web.base.BasicAction.name']}"
                method="${methodNullSafe}"
                enctype="${enctypeNullSafe}">
        <tiles:insert beanName="tile.abstractLayout.content"/>
        </tmx_ctrl:form>
