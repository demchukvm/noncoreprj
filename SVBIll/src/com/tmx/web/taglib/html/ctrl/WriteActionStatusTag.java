package com.tmx.web.taglib.html.ctrl;

import com.tmx.web.controls.ActionExecStatus;
import javax.servlet.jsp.JspException;

/**

 */
public class WriteActionStatusTag extends AbstractControlTag {
    private String listener;


    public String getListener() {
        return listener;
    }

    public void setListener(String listener) {
        this.listener = listener;
    }

    public String getProperty(){
        return "actionExecStatus";//this control always has the same name for different forms
    }

    //------------------------------Protected methods
    protected String renderControlHTML() throws JspException {
        StringBuffer resultHTML = new StringBuffer();
        ActionExecStatus aes = getActionExecStatus();
        if (aes == null)
            return "ActionExecStatus '" + getProperty() + "' is not found";
        String statusKey = aes.getStatusKey(getListener());
        String statusLabel = getLocalizedStatus(statusKey);
        String styleClass = getStyleClass(statusKey);
        styleClass = styleClass != null ? " class='" + styleClass + "' " : "";
        resultHTML.append("<span").
                append(styleClass).
                append(">").
                append(statusLabel).
                append("</span>");
        return resultHTML.toString();
    }


    protected ActionExecStatus getActionExecStatus() throws JspException {
        Object object = getPropertyObject();
        return (object != null && object instanceof ActionExecStatus) ?
                (ActionExecStatus) object : null;
    }

    public String getStyleClass(String statusKey){
        return getDefaultProperty("tag.actionExecStatus.style."+statusKey);
    }

    public String getLocalizedStatus(String statusKey){
        return statusKey == null ? "" : getLocalizedLabel("tag.actionExecStatus.label."+statusKey, null);
    }

    protected int controlStartTag() throws JspException {
        return 0;  //To change body of implemented methods use File | Settings | File Templates.
    }
}
