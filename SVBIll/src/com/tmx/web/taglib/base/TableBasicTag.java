package com.tmx.web.taglib.base;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.JspException;

import com.tmx.web.base.ApplicationEnvironment;
import com.tmx.web.controls.table.Table;

/**
 * Basic class to holds some methods are shared
 * between table related tags
 */
public abstract class TableBasicTag extends AbstractSupportTag {
    /** Get action published this page. Paging requests will send on it
     * with additional params.
     * This property is optional. If it is null then gets reftesh action from table property */
    private String action = null;
    /** Optional refresh command */
    private String command = null;
    /** The name of table. Should be the same as used in form bean */
    private String tableName = null;
    /** Form bean name */
    private String name = null;//TODO:covering
    /** XSL style name for rendering */
    private String style = null;


    //-----------------------Properties
    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getCommand() {
        return command;
    }

    public void setCommand(String command) {
        this.command = command;
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStyle() {
        return style;
    }

    public void setStyle(String style) {
        this.style = style;
    }

    //-----------------------Business logic

    /** Refresh Action is used to call table refresh() */
    protected String getRefreshActionUrl() throws JspException {
        HttpServletRequest request = (HttpServletRequest) pageContext.getRequest();
        //String submitURL = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath();
        StringBuffer submitURL = new StringBuffer(request.getContextPath());
        if(getAction() != null)
            return submitURL.append(getAction()).toString();
        else {
            String actionName = getTable().getRefreshActionName();
            if(actionName != null)
                submitURL.append(actionName).append(ApplicationEnvironment.getActionExtension());
            return submitURL.toString();
        }
    }

    /** Oprional refresh command is used to call table refresh() */
    protected String getRefreshActionCommand() throws JspException {
        if(getCommand() != null)
            return getCommand();
        else
            return getTable().getRefreshCommandName();
    }

    protected Table getTable() throws JspException{
        setProperty(getTableName());//will be used in getPropertyObject()
        Object object = getPropertyObject();
        return (object != null && object instanceof Table) ?
               (Table)object : null;
    }

}
