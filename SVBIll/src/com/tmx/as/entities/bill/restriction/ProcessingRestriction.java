package com.tmx.as.entities.bill.restriction;

import com.tmx.as.entities.bill.processing.Processing;

/**
 * 
 */
public class ProcessingRestriction extends AbstractRestriction{
    private Processing processing;
    private Long processingId;

    //--------------Properties
    public Processing getProcessing() {
        return processing;
    }

    public void setProcessing(Processing processing) {
        this.processing = processing;
    }

    public Long getProcessingId() {
        return processingId;
    }

    public void setProcessingId(Long processingId) {
        this.processingId = processingId;
    }
}
