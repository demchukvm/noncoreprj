package com.tmx.as.base.criterion;

import org.hibernate.criterion.*;

import java.util.Map;
import java.util.Collection;

public class CriterionHelper {
    public static Criterion getHibernateCriterion(CriterionWrapper cw){
        if(cw.getType().equals("allEq")){
            return org.hibernate.criterion.Restrictions.allEq
                    ((Map)cw.getParam("propertyNameValues"));
        }else if(cw.getType().equals("and")){
            return org.hibernate.criterion.Restrictions.and
                    (getHibernateCriterion((CriterionWrapper)cw.getParam("lhs")),
                     getHibernateCriterion((CriterionWrapper)cw.getParam("rhs")));
        }else if(cw.getType().equals("between")){
            return org.hibernate.criterion.Restrictions.between
                    ((String) cw.getParam("propertyName"),
                     cw.getParam("lo"),
                     cw.getParam("hi"));   
        }else if(cw.getType().equals("conjunction")){
            return org.hibernate.criterion.Restrictions.conjunction();                       
        }else if(cw.getType().equals("disjunction")){
            return org.hibernate.criterion.Restrictions.conjunction();
        }else if(cw.getType().equals("eq")){
            return org.hibernate.criterion.Restrictions.eq
                    ((String) cw.getParam("propertyName"),
                     cw.getParam("value"));   
        }else if(cw.getType().equals("eqProperty")){
            return org.hibernate.criterion.Restrictions.eqProperty
                    ((String) cw.getParam("propertyName"),
                     (String) cw.getParam("otherPropertyName"));   
        }else if(cw.getType().equals("ge")){
            return org.hibernate.criterion.Restrictions.ge
                    ((String) cw.getParam("propertyName"),
                     cw.getParam("value"));
        }else if(cw.getType().equals("geProperty")){
            return org.hibernate.criterion.Restrictions.geProperty
                    ((String) cw.getParam("propertyName"),
                     (String) cw.getParam("otherPropertyName"));
        }else if(cw.getType().equals("gt")){
            return org.hibernate.criterion.Restrictions.gt
                    ((String) cw.getParam("propertyName"),
                     cw.getParam("value"));
        }else if(cw.getType().equals("gtProperty")){
            return org.hibernate.criterion.Restrictions.gtProperty
                    ((String) cw.getParam("propertyName"),
                     (String) cw.getParam("otherPropertyName"));
        }else if(cw.getType().equals("idEq")){
            return org.hibernate.criterion.Restrictions.idEq
                    (cw.getParam("value"));   
        }else if(cw.getType().equals("ilike")){
            return org.hibernate.criterion.Restrictions.ilike
                    ((String) cw.getParam("propertyName"),
                     cw.getParam("value"));
        }else if(cw.getType().equals("in")){
            return (cw.getParam("values") instanceof Collection)?
                    org.hibernate.criterion.Restrictions.in
                    ((String) cw.getParam("propertyName"),
                    (Collection)cw.getParam("values"))
                    :
                    org.hibernate.criterion.Restrictions.in
                    ((String) cw.getParam("propertyName"),
                    (Object[])cw.getParam("values"));
        }else if(cw.getType().equals("isEmpty")){
            return org.hibernate.criterion.Restrictions.isEmpty
                    ((String) cw.getParam("propertyName"));
        }else if(cw.getType().equals("isNotEmpty")){
            return org.hibernate.criterion.Restrictions.isEmpty
                    ((String) cw.getParam("propertyName"));
        }else if(cw.getType().equals("isNotNull")){
            return org.hibernate.criterion.Restrictions.isNotNull
                    ((String) cw.getParam("propertyName"));
        }else if(cw.getType().equals("isNull")){
            return org.hibernate.criterion.Restrictions.isNull
                    ((String) cw.getParam("propertyName"));
        }else if(cw.getType().equals("le")){
            return org.hibernate.criterion.Restrictions.le
                    ((String) cw.getParam("propertyName"),
                      cw.getParam("value"));
        }else if(cw.getType().equals("like")){
            return org.hibernate.criterion.Restrictions.like
                    ((String) cw.getParam("propertyName"),
                     (String) cw.getParam("otherPropertyName"));
        }else if(cw.getType().equals("lt")){
            return org.hibernate.criterion.Restrictions.lt
                    ((String) cw.getParam("propertyName"),
                     cw.getParam("value"));
        }else if(cw.getType().equals("ltProperty")){
            return org.hibernate.criterion.Restrictions.ltProperty
                    ((String) cw.getParam("propertyName"),
                     (String) cw.getParam("otherPropertyName"));
        }else if(cw.getType().equals("naturalId")){
            return org.hibernate.criterion.Restrictions.naturalId();
        }else if(cw.getType().equals("ne")){
            return org.hibernate.criterion.Restrictions.ne
                    ((String) cw.getParam("propertyName"),
                     cw.getParam("value"));
        }else if(cw.getType().equals("neProperty")){
            return org.hibernate.criterion.Restrictions.neProperty
                    ((String) cw.getParam("propertyName"),
                     (String) cw.getParam("otherPropertyName"));
        }else if(cw.getType().equals("not")){
            return org.hibernate.criterion.Restrictions.not(
                    getHibernateCriterion(
                            (CriterionWrapper) cw.getParam("expression")
                    ));
        }else if(cw.getType().equals("or")){
            return org.hibernate.criterion.Restrictions.or
                    (getHibernateCriterion((CriterionWrapper)cw.getParam("lhs")),
                     getHibernateCriterion((CriterionWrapper)cw.getParam("rhs")));
        }else if(cw.getType().equals("sizeEq")){
            return org.hibernate.criterion.Restrictions.sizeEq
                    ((String) cw.getParam("propertyName"),
                     ((Integer)cw.getParam("size")).intValue());
        }else if(cw.getType().equals("sizeGe")){
            return org.hibernate.criterion.Restrictions.sizeGe
                    ((String) cw.getParam("propertyName"),
                     ((Integer)cw.getParam("size")).intValue());
        }else if(cw.getType().equals("sizeGt")){
            return org.hibernate.criterion.Restrictions.sizeGt
                    ((String) cw.getParam("propertyName"),
                     ((Integer)cw.getParam("size")).intValue());
        }else if(cw.getType().equals("sizeLe")){
            return org.hibernate.criterion.Restrictions.sizeLe
                    ((String) cw.getParam("propertyName"),
                     ((Integer)cw.getParam("size")).intValue());
        }else if(cw.getType().equals("sizeLt")){
            return org.hibernate.criterion.Restrictions.sizeLt
                    ((String) cw.getParam("propertyName"),
                     ((Integer)cw.getParam("size")).intValue());
        }else if(cw.getType().equals("sizeNe")){
            return org.hibernate.criterion.Restrictions.sizeNe
                    ((String) cw.getParam("propertyName"),
                     ((Integer)cw.getParam("size")).intValue());
        }else if(cw.getType().equals("sqlRestriction")){
            return org.hibernate.criterion.Restrictions.sqlRestriction
                    ((String) cw.getParam("sql"));
        }
        return null;
    }
}
