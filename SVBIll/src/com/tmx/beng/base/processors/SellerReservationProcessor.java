package com.tmx.beng.base.processors;

import com.tmx.as.entities.bill.transaction.SellerBalanceTransaction;
import com.tmx.as.entities.bill.transaction.TransactionType;
import com.tmx.as.entities.bill.seller.Seller;
import com.tmx.as.base.ProcedureWrapper;
import com.tmx.as.base.QueryParameterWrapper;
import com.tmx.as.blogic.balance.SellerBalanceManager;
import com.tmx.as.exceptions.DatabaseException;

import com.tmx.beng.base.*;
import com.tmx.beng.base.cache.DBCache;
import com.tmx.beng.base.cache.DBCacheException;

import java.util.*;
import java.sql.Types;

public class SellerReservationProcessor extends DefaultReservationProcessor{

    protected List prepareOperatorTransactions(BillingMessageWritable msg, DBCache dbCache, TransConfig config) throws DBCacheException {
        return Collections.EMPTY_LIST;
    }

    protected List prepareSellerTransactions(BillingMessageWritable msg, DBCache dbCache, TransConfig config) throws DBCacheException {
        SellerBalanceTransaction transaction = new SellerBalanceTransaction();

        transaction.setTransactionTime(config.transRegTime);
        transaction.setClientTime(config.clientTime);

        Reservation reservation = config.reservationSet.getReservation(Seller.class, config.sellerCode);
        Iterator iter = reservation.reservesIterator();
        Reservation.Reserve reserve = (Reservation.Reserve) iter.next();

        if (iter.hasNext())
            throw new IllegalStateException("reserves isn't one");

        transaction.setAmount(new Double(reserve.getAmount()));

        transaction.setSeller(dbCache.getSeller(config.sellerCode));
        transaction.setSellerBalance(dbCache.getSellerBalance(config.sellerCode, reserve.getBalanceCode()));

        transaction.setTransactionSupport(config.transactionSupport);
        transaction.setTransactionGUID(config.transactionGUID);

        transaction.setCliTransactionNum(config.cliTransactionNum);
        transaction.setType(new TransactionType(reserve.getTransTypeCode()));
        transaction.setOperationType(config.operationsType);

        dbCache.changeSellerBalance(config.sellerCode,
                reserve.getBalanceCode(),
                reserve.getAmount(),
                reserve.getTransTypeCode());

        //save pre tx balance amount
        try {
            transaction.setPreTxBalance((new SellerBalanceManager()).getBalanceByCode(reserve.getBalanceCode()).getAmount());//TODO: retest needed, mb need to use reserve instead config 
        } catch (DatabaseException dbe) {
            logger.error("Can`t set current balance amount " + reserve.getBalanceCode() + " " + dbe.getMessage());
        } catch (Exception e) {
            logger.error("Can`t set current balance amount " + reserve.getBalanceCode() + " " + e.getMessage());
        }

        //TODO:(AN) ?????
        msg.setStatusCode(StatusDictionary.STATUS_OK);

        List result = new ArrayList();
        result.add(transaction);
        return result;
    }

    protected List prepareTerminalTransactions(BillingMessageWritable msg, DBCache dbCache, TransConfig config) throws DBCacheException {
        return Collections.EMPTY_LIST;
    }

    protected List prepareClientTransaction(BillingMessageWritable msg, DBCache dbCache, TransConfig config) throws DBCacheException {
        return Collections.EMPTY_LIST;
    }

    protected List prepareChangeBalancesProcedure(BillingMessageWritable msg, DBCache dbCache, TransConfig config) throws DBCacheException {
        Reservation reservation = config.reservationSet.getReservation(Seller.class, config.sellerCode);
        Iterator iter = reservation.reservesIterator();
        Reservation.Reserve reserve = (Reservation.Reserve)iter.next();

        if(iter.hasNext())
            throw new IllegalStateException("reserves count isn't one");

        ProcedureWrapper wrapper = new ProcedureWrapper(
                new QueryParameterWrapper[]{
                        QueryParameterWrapper.createOutParameter(1, Types.INTEGER),
                        //new QueryParameterWrapper(2, dbCache.getSellerBalance(config.sellerCode, reserve.getBalanceCode()).getBalanceId()),
                        new QueryParameterWrapper(2, dbCache.getSeller(config.sellerCode).getSellerId()),
                        new QueryParameterWrapper(3, new Double(getSignedAmount(reserve.getTransTypeCode(), reserve.getAmount()))),
                },
                Seller.class,
                "change_seller_balance");

        List result = new ArrayList();
        result.add(wrapper);
        return result;
    }

    public Task rollback(Task task, Throwable e) {
        logger.info("Rollback Reservation...");
        try{
            
            BillingMessageWritable msg = task.getProcesedMessage();
            DBCache dbCache = task.getSessionDBCache();
            ReservationSet reservationSet = (ReservationSet)msg.getAttribute(BillingMessage.BALANCE_RESERVATION_SET);

            // cancel seller reservations
            String sellerCode = msg.getAttributeString(BillingMessage.SELLER_CODE);
            Reservation reservation = reservationSet.getReservation(Seller.class, sellerCode);
            for(Iterator iter = reservation.reservesIterator(); iter.hasNext();){
                Reservation.Reserve reserve = (Reservation.Reserve)iter.next();
                if(reserve.isApplied())
                    dbCache.changeSellerBalance( sellerCode, reserve.getBalanceCode(), -reserve.getAmount(), reserve.getTransTypeCode() );
                    reservation.cancelReserve(reserve.getId());
            }
        }
        catch(Throwable te){
            logger.error("Rollback reservation failed: " + te.getLocalizedMessage(), te);
        }
        return task;
    }


    
}
