<%@ page language="java" pageEncoding="UTF-8" contentType="text/html; charset=utf-8" isELIgnored="false" %>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-nested" prefix="nested" %>
<%@ taglib uri="http://tmx.com/taglib/tmx-controls" prefix="tmx_ctrl" %>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html" %>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean" %>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>

<html:form action="/core/balance/terminal_group_balance/terminalGroupBrowseAction">
<tmx_ctrl:browseCallBack/>
<table width="100%" border="0">
    <!-- FILTER SET -->
    <tr>
        <td valign="top" width="100%" align="left">
            <%--<tmx_ctrl:panelFilterSet tableName="browseTable">--%>

            <%--</tmx_ctrl:panelFilterSet>--%>
        </td>
    </tr>
    <!-- TABLE -->
    <tr>
        <td width="100%">
            <table class='one_outer_cell_table' cellspacing='0' cellpadding='0' width="100%">
                <tr>
                    <td>
                        <table cellspacing='1' cellpadding='1' width="100%">
                            <!-- Table header -->
                            <tr>

                                <th class="column_header_cell" width="110">
                                    <tmx_ctrl:imgTableOrdering tableName="browseTable"
                                                               orderByFieldName="terminalGroupId"
                                                               labelBundle="as:com.tmx.as.entities.bill.terminal_group.TerminalGroup"
                                                               labelKey="terminalGroupId"/>
                                </th>

                                <th class="column_header_cell" width="110">
                                    <tmx_ctrl:imgTableOrdering tableName="browseTable"
                                                               orderByFieldName="sellerOwner.name"
                                                               labelBundle="as:com.tmx.as.entities.bill.seller.Seller"
                                                               labelKey="{@name}"/>
                                </th>

                                <th class="column_header_cell" width="110">
                                    <tmx_ctrl:imgTableOrdering tableName="browseTable"
                                                               orderByFieldName="login"
                                                               labelBundle="as:com.tmx.as.entities.bill.terminal_group.TerminalGroup"
                                                               labelKey="login"/>
                                </th>

                                <th class="column_header_cell" width="110">
                                    <tmx_ctrl:imgTableOrdering tableName="browseTable"
                                                               orderByFieldName="password"
                                                               labelBundle="as:com.tmx.as.entities.bill.terminal_group.TerminalGroup"
                                                               labelKey="password"/>
                                </th>

                            </tr>
                            <!-- Table data -->
                            <nested:iterate id="terminalGroup" property="browseTable.data" type="com.tmx.as.entities.bill.terminal_group.TerminalGroup">
                                <tr>
                                    <td class='column_data_cell'>
                                        <tmx_ctrl:browsePick pickKey="${terminalGroup.terminalGroupId}" pickValue="${terminalGroup.terminalGroupId}"/>
                                    </td>
                                    <td class='column_data_cell'>
                                        <tmx_ctrl:write name="terminalGroup" property="sellerOwner.name" length="20" ignore="true"/>
                                    </td>
                                    <td class='column_data_cell'>
                                        <tmx_ctrl:write name="terminalGroup" property="login" length="20" ignore="true"/>
                                    </td>
                                    <td class='column_data_cell'>
                                        <tmx_ctrl:write name="terminalGroup" property="password" length="20" ignore="true"/>
                                   </td>
                                </tr>
                            </nested:iterate>
                            <!-- Empty row -->
                            <nested:define id="tableSize" property="browseTable.resultSize" type="java.lang.Integer"/>
                            <c:if test="${tableSize <= 0}">
                                <tr>
                                    <td class='column_data_cell' colspan="4" align="center">
                                        <bean:message bundle="core.labels" key="core.common.th.empty_table" />
                                    </td>
                                </tr>
                            </c:if>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td style="text-align: right">
            <tmx_ctrl:imgTablePaging tableName="browseTable"/>
        </td>
    </tr>
</table>
</html:form>