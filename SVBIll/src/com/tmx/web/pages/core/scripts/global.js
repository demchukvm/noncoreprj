/* ======================================================================== */
/*                           Register onEvent functions                     */
/* ======================================================================== */
/**
 * Map of pairs: [event name][Array of functions to execute on this event]
 */
var eventsMap = new Object();
/*
* Adds instance of Function to array of functions
* which must be executed on target event
* f - function to register
* e - call on event
*/
function registerFunction(e, f) {
    var onEventFunctionList = null;
    if (this.eventsMap[e] == null) {
        onEventFunctionList = new Array();
        this.eventsMap[e] = onEventFunctionList;
    }
    else {
        onEventFunctionList = this.eventsMap[e];
    }
    onEventFunctionList[onEventFunctionList.length] = f;
}


/**
* The same as registerFunction(e, f), but registers given
* function instance only it was not registered yet.
*/
function registerSingleFunction(e, f) {
    var onEventFunctionList = null;
    if(this.eventsMap[e] == null){
        onEventFunctionList = new Array();
        this.eventsMap[e] = onEventFunctionList;
    }
    else{
        onEventFunctionList = this.eventsMap[e];
    }

    //exit if function is alredy registered
    for (var i = 0; i < onEventFunctionList.length; i++) {
       if(onEventFunctionList[i].toString() == f.toString()){
      return;
       }
    }
    onEventFunctionList[onEventFunctionList.length] = f;
}



/*
* Executes all registered on given event functions
* e - name of event
*/
function executeOnEvent(e) {
    if (this.eventsMap[e] != null) {
        var onEventFunctionList = this.eventsMap[e];
        for (var i = 0; i < onEventFunctionList.length; i++) {
            onEventFunctionList[i]();
        }
    }
}

/**
 * Assign execution of our functions lists with DOM events
 */
    //--------WINDOW.ONLOAD
    //Wrap current onevent function into our model
	if(window.onload != null){
  	   registerFunction('window.onload', window.onload);
	}
    window.onload = function() {executeOnEvent('window.onload');}

    //--------DOCUMENT.ONCLICK
    //Wrap current onevent function into our model
    if(document.onclick != null){
   	   registerFunction('document.onclick', document.onclick);
    }
    document.onclick = function() {executeOnEvent('document.onclick');}

/**
 * EXAMPLE: How to use this model:
 *
 * registerFunction('window.onload', function() {alert('win on load1')});
 * registerFunction('window.onload', function() {alert('win on load2')});
 *
 * registerFunction('document.onclick', function() {alert('doc on click1')});
 * registerFunction('document.onclick', function() {alert('doc on click2')});
 *
 * registerSingleFunction('window.onload', function() {alert('win on load single 1')});
 * registerSingleFunction('window.onload', function() {alert('win on load single 1')});
 *
 */
//---------------------------------------------------------------------------