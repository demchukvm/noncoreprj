package com.tmx.as.entities.bill.voucher;

import com.tmx.as.base.BasicEntity;

import java.io.Serializable;

/**
 */
public class VoucherType extends BasicEntity {
    private Long voucherTypeId;
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getVoucherTypeId() {
        return voucherTypeId;
    }

    public void setVoucherTypeId(Long voucherTypeId) {
        this.voucherTypeId = voucherTypeId;
    }

    public Serializable getId() {
        return getVoucherTypeId();
    }

    public int getValid() {
        return valid;
    }

    public void setValid(int valid) {
        this.valid = valid;
    }
}
