package com.tmx.web.forms.core.restriction;

import com.tmx.as.blogic.FunctionManager;
import com.tmx.as.blogic.BlogicException;
import com.tmx.as.entities.bill.function.FunctionInstance;
import com.tmx.as.entities.bill.function.FunctionType;
import com.tmx.as.entities.bill.restriction.AbstractRestriction;
import com.tmx.as.entities.bill.restriction.SellerRestriction;
import com.tmx.as.entities.general.user.User;
import com.tmx.as.exceptions.DatabaseException;
import com.tmx.web.base.BasicActionForm;
import com.tmx.web.base.WebException;
import com.tmx.web.controls.*;
import com.tmx.web.controls.table.ControlManagerTable;
import com.tmx.web.forms.core.base.AbstractFormFactory;
import com.tmx.web.forms.core.tariff.TransportParameterForm;
import com.tmx.web.actions.core.restriction.AbstractManageRestrictionsAction;
import com.tmx.util.InitException;

import java.util.HashSet;


abstract public class AbstractInfoRestrictionForm extends TransportParameterForm {

    private Long restrictionId;//restrictionId == null if form create
    private Long incomingRestrictionId;
    private Long funcInstanceId;// for AbstractParameterPage
//    private Long ownerId;//seller, terminal, operator, terminal group

    //buttons
    private Button onApplyButton;
    private Button onSaveButton;
    private Button onResetButton;
    private Button onCancelButton;
    
    //restriction buttons
    private Button onRefreshParamButton;
    private Button onCreateParamButton;
    private Button onEditParamButton;
    private Button onDeleteParamButton;

    private ControlManagerTable funcParametrsTable;
    private ControlManagerTable paramErrorTable;
    private ControlManagerTable defaultFuncParametrsTable;

    private EditBox editBoxRestrictionId;
    private EditBox editBoxName;
    private EditBox editBoxFunctionInstanceId;
    private EditBox editBoxOwner;

    private BrowseBox browseBoxFunctionTypes;

    private TextArea textAreaFuncDescription;

    abstract protected AbstractRestriction populateFromSpecificControls();
    abstract protected void bindSpecificData(AbstractRestriction restriction, User loggedInUser) throws DatabaseException;
    abstract protected void bindSpecificData(AbstractRestriction restriction);

    protected void init() {
        initEditBoxes();
        initButtons();
        initTables();
        initTextAreas();
        initBrowseBoxes();
    }

    private void initBrowseBoxes(){
        browseBoxFunctionTypes = new BrowseBox();
        browseBoxFunctionTypes.setMandatory(true);
    }

    private void initTextAreas(){
        textAreaFuncDescription = new TextArea();
        textAreaFuncDescription.setReadonly(true);
    }

    private void initTables(){
        funcParametrsTable = new ControlManagerTable("query(query_type=retrieve_all," +
                "entity_class=com.tmx.as.entities.bill.function.ActualFunctionParameter," +
                "table_attr=functionInstance.functionType," +
                "order(name=actualFunctionParameterId,type=asc)," +
                "page_size=15" +
                ")");
        funcParametrsTable.addFilter("by_function_instance_id");
        funcParametrsTable.setRefreshCommandName("refreshFuncParametrsParamTable");
        funcParametrsTable.addDependedControl(onEditParamButton).addDependedControl(onDeleteParamButton);

        defaultFuncParametrsTable = new ControlManagerTable();
        paramErrorTable = new ControlManagerTable();
//        defaultFuncParametrsTable.setRefreshCommandName("refreshFuncParametrsParamTable"); old
    }

    private void initEditBoxes(){
        editBoxRestrictionId = new EditBox();
        editBoxRestrictionId.setReadonly(true);
        editBoxName = new EditBox();
        editBoxName.setMandatory(true);

        editBoxOwner = new EditBox();
        editBoxOwner.setReadonly(true);

        editBoxFunctionInstanceId = new EditBox();
        editBoxFunctionInstanceId.setReadonly(true);
    }

    private void initButtons(){
        onSaveButton = new Button();
        onSaveButton.setLabelKey("ctrl.button.label.save");
        onSaveButton.setCommand("save");

        onResetButton = new Button();
        onResetButton.setLabelKey("ctrl.button.label.reset");

        onCancelButton = new Button();
        onCancelButton.setLabelKey("ctrl.button.label.cancel");
//        onCancelButton.setCommand("preloadForUpdate");

        onApplyButton = new Button();
        onApplyButton.setLabelKey("ctrl.button.label.apply");
        onApplyButton.setCommand("apply");

        //---restriction buttons----------------------------------
        onRefreshParamButton = new Button();
        onRefreshParamButton.setLabelKey("ctrl.button.label.refresh");        
        onRefreshParamButton.setCommand("refreshDefaultParams");

        onCreateParamButton = new Button();
        onCreateParamButton.setLabelKey("ctrl.button.label.create");
        onCreateParamButton.setCommand("createParam");

        onEditParamButton = new Button();
        onEditParamButton.setLabelKey("ctrl.button.label.edit");
        onEditParamButton.setCommand("preloadForUpdate");
        onEditParamButton.setReadonly(true);

        onDeleteParamButton = new Button();
        onDeleteParamButton.setLabelKey("ctrl.button.label.delete");
        onDeleteParamButton.setCommand("preloadForDelete");
        onDeleteParamButton.setReadonly(true);
    }

    public AbstractRestriction populateFromControls(){
        AbstractRestriction restriction = populateFromSpecificControls();

        restriction.setRestrictionId(PopulateUtil.getLongValue(editBoxRestrictionId));
        restriction.setName(PopulateUtil.getStringValue(editBoxName));

        Long instanceId = PopulateUtil.getLongValue(editBoxFunctionInstanceId);
        restriction.setFunctionInstance(populateFunctionInstance(instanceId));

        return restriction;
    }

    private FunctionInstance populateFunctionInstance(Long instanceId) {
        FunctionInstance functionInstance = new FunctionInstance();
        functionInstance.setFunctionType(populateFunctionType());
        functionInstance.setFunctionInstanceId(instanceId);
        functionInstance.setFunctionParameters(new HashSet(funcParametrsTable.getData()));
        return functionInstance;
    }

    private FunctionType populateFunctionType() {
        FunctionType functionType = new FunctionType();
        functionType.setFunctionTypeId(PopulateUtil.getLongKey(browseBoxFunctionTypes));
        functionType.setName(browseBoxFunctionTypes.getValue());
        return functionType;
    }



    public void bindData(AbstractRestriction rest, User loggedInUser) throws DatabaseException, WebException {

        bindEditBoxes(rest);
        bindBrowseBoxes(rest);
        bindValueForPopulating(rest);
        bindTextAreas(rest);
        bindSpecificData(rest, loggedInUser);
// {!!!!}
//        bindButtonActions();
    }

    public void bindData(User loggedInUser) throws DatabaseException, BlogicException, InitException, WebException {
             reset();
//             bindButtonActions();//from roadmap
         }

    //from roadmap
// {!!!!} maybe to delet method bindButtonActions() 
    private void bindButtonActions() throws WebException {
             String action = (String) getRoadmap().getParameterValue(AbstractManageRestrictionsAction.DEFAULT_VALID_ACTION,"onCancelAction", true);
             getOnCancelButton().setAction(action);
         }
    private void bindEditBoxes(AbstractRestriction rest) {
        editBoxRestrictionId.setValue(PopulateUtil.getString(rest.getRestrictionId()));
        editBoxName.setValue(rest.getName());
        editBoxFunctionInstanceId.setValue(PopulateUtil.getString(rest.getFunctionInstance().getFunctionInstanceId()));
    }

    private void bindBrowseBoxes(AbstractRestriction rest) {
        String selectedKey = PopulateUtil.getString(rest.getFunctionInstance().getFunctionType().getFunctionTypeId());
        browseBoxFunctionTypes.setKey(selectedKey);
        browseBoxFunctionTypes.setValue(rest.getFunctionInstance().getFunctionType().getName());
    }

    private void bindValueForPopulating(AbstractRestriction rest) {
        funcInstanceId = rest.getFunctionInstance().getFunctionInstanceId();
        restrictionId = rest.getRestrictionId();
    }

    private void bindTextAreas(AbstractRestriction rest) {
        textAreaFuncDescription.setValue(rest.getFunctionInstance().getFunctionType().getDescription());
    }
//    {!!!!}
//    private void bindButtonActions() throws WebException {
//        String action = (String) getRoadmap().getParameterValue("/core/restriction/seller_restriction/infoSellerRestriction","onCancelAction", true);
//        getOnCancelButton().setAction(action);
//    }

    public Long getRestrictionId() {
        return restrictionId;
    }

    public void setRestrictionId(Long restrictionId) {
        this.restrictionId = restrictionId;
    }


    public Button getOnApplyButton() {
        return onApplyButton;
    }

    public void setOnApplyButton(Button onApplyButton) {
        this.onApplyButton = onApplyButton;
    }

    public Button getOnSaveButton() {
        return onSaveButton;
    }

    public void setOnSaveButton(Button onSaveButton) {
        this.onSaveButton = onSaveButton;
    }

    public Button getOnResetButton() {
        return onResetButton;
    }

    public void setOnResetButton(Button onResetButton) {
        this.onResetButton = onResetButton;
    }

    public Button getOnCancelButton() {
        return onCancelButton;
    }

    public void setOnCancelButton(Button onCancelButton) {
        this.onCancelButton = onCancelButton;
    }

    public Button getOnCreateParamButton() {
        return onCreateParamButton;
    }

    public void setOnCreateParamButton(Button onCreateParamButton) {
        this.onCreateParamButton = onCreateParamButton;
    }

    public Button getOnEditParamButton() {
        return onEditParamButton;
    }

    public void setOnEditParamButton(Button onEditParamButton) {
        this.onEditParamButton = onEditParamButton;
    }

    public Button getOnDeleteParamButton() {
        return onDeleteParamButton;
    }

    public void setOnDeleteParamButton(Button onDeleteParamButton) {
        this.onDeleteParamButton = onDeleteParamButton;
    }
    
    public ControlManagerTable getFuncParametrsTable() {
        return funcParametrsTable;
    }

    public void setFuncParametrsTable(ControlManagerTable funcParametrsTable) {
        this.funcParametrsTable = funcParametrsTable;
    }

    public EditBox getEditBoxRestrictionId() {
        return editBoxRestrictionId;
    }

    public void setEditBoxRestrictionId(EditBox editBoxRestrictionId) {
        this.editBoxRestrictionId = editBoxRestrictionId;
    }

    public EditBox getEditBoxName() {
        return editBoxName;
    }

    public void setEditBoxName(EditBox editBoxName) {
        this.editBoxName = editBoxName;
    }

    public BrowseBox getBrowseBoxFunctionTypes() {
        return browseBoxFunctionTypes;
    }

    public void setBrowseBoxFunctionTypes(BrowseBox browseBoxFunctionTypes) {
        this.browseBoxFunctionTypes = browseBoxFunctionTypes;
    }

    public EditBox getEditBoxFunctionInstanceId() {
        return editBoxFunctionInstanceId;
    }

    public void setEditBoxFunctionInstanceId(EditBox editBoxFunctionInstanceId) {
        this.editBoxFunctionInstanceId = editBoxFunctionInstanceId;
    }

    public Long getFuncInstanceId() {
        return funcInstanceId;
    }

    public void setFuncInstanceId(Long funcInstanceId) {
        this.funcInstanceId = funcInstanceId;
    }

    public EditBox getEditBoxOwner() {
        return editBoxOwner;
    }

    public void setEditBoxOwner(EditBox editBoxOwner) {
        this.editBoxOwner = editBoxOwner;
    }

/*
    public Long getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(Long ownerId) {
        this.ownerId = ownerId;
    }
*/

    public TextArea getTextAreaFuncDescription() {
        return textAreaFuncDescription;
    }

    public void setTextAreaFuncDescription(TextArea textAreaFuncDescription) {
        this.textAreaFuncDescription = textAreaFuncDescription;
    }

    public Button getOnRefreshParamButton() {
        return onRefreshParamButton;
    }

    public void setOnRefreshParamButton(Button onRefreshParamButton) {
        this.onRefreshParamButton = onRefreshParamButton;
    }

    public ControlManagerTable getParamErrorTable() {
        return paramErrorTable;
    }

    public void setParamErrorTable(ControlManagerTable paramErrorTable) {
        this.paramErrorTable = paramErrorTable;
    }

    public ControlManagerTable getDefaultFuncParametrsTable() {
        return defaultFuncParametrsTable;
    }

    public void setDefaultFuncParametrsTable(ControlManagerTable defaultFuncParametrsTable) {
        this.defaultFuncParametrsTable = defaultFuncParametrsTable;
    }

    public Long getIncomingRestrictionId() {
        return incomingRestrictionId;
    }

    public void setIncomingRestrictionId(Long incomingRestrictionId) {
        this.incomingRestrictionId = incomingRestrictionId;
    }
}
