<?php

function smarty_function_auxpage ($params, &$smarty){
    if (trim($params['slug'])){
        $aux_page_text = db_phquery_fetch (DBRFETCH_FIRST, 'SELECT '.LanguagesManager::sql_prepareField('aux_page_text').' FROM `?#AUX_PAGES_TABLE` WHERE `aux_page_slug`=?', $params['slug']);
        $smarty->assign ('aux_page', $aux_page_text);
        $smarty->display (DIR_FTPLS.'/aux_page.html');
    }
}

?>