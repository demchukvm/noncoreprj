package com.tmx.web.base;

import org.apache.struts.Globals;
import org.apache.struts.action.ActionMapping;

import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpServletRequest;
import java.util.Locale;

/**
 * Holds objects in session context
 */
public class SessionEnvironment {
    static final String LOCALE = Globals.LOCALE_KEY;//"LOCALE"; //RM: use struts locale instead
    static final String IS_LOCALE_MANUALLY_CHANGED = "IS_LOCALE_MANUALLY_CHANGED";
    public static final String USER = "USER";
    public static final String COMMAND_FRAGMENT_MAP = "COMMAND_FRAGMENT_MAP";

    public static final String OBJECTIVE_USER = "OBJECTIVE_USER";
    public static final String USER_CONTEXT_FORM = "userContextForm";/** This name used from JSP, so it should be Bean styled */

    public static Locale getLocale(HttpSession session) {
        return (Locale) getAttr(session, LOCALE);
    }

    public static void setLocale(HttpSession session, Locale locale) {
        setAttr(session, LOCALE, locale);
    }


    public static boolean isLocaleManuallyChanged(HttpSession session){
        return (getAttr(session, IS_LOCALE_MANUALLY_CHANGED) == null || !"true".equals(getAttr(session, IS_LOCALE_MANUALLY_CHANGED))) ? false : true;
    }

    /** Set true. It is impossible to go out state "locale was manually changed".
     * You can do that only by session termination. */
    public static void setLocaleManuallyChanged(HttpSession session){
        setAttr(session, IS_LOCALE_MANUALLY_CHANGED, "true");
    }

    /**
     * Package private
     */
    public static Object getAttr(HttpSession session, String attrName) {
        if (session == null || attrName == null)
            return null;
        return session.getAttribute(attrName);
    }

    /**
     * Package private
     */
    public static void setAttr(HttpSession session, String attrName, Object attrValue) {
        if (session == null || attrName == null || attrValue == null)
            return;
        session.setAttribute(attrName, attrValue);
    }

    public static boolean removeAttr(HttpSession session, String attrName) {
        if (session == null || attrName == null)
            return false;

        if (session.getAttribute(attrName) != null) {
            session.setAttribute(attrName, null);
            return true;
        }

        return false;
    }

    /** Obtain form from session by given name or create and initialize it from mapping and request*/
    public static BasicActionForm allocateForm(Class formClass,
                                               String formName,
                                               ActionMapping mapping,
                                               HttpServletRequest request) throws WebException{

        BasicActionForm targetForm = (BasicActionForm)SessionEnvironment.getAttr(request.getSession(), formName);
        if(targetForm == null){
            try{
                targetForm = (BasicActionForm)formClass.newInstance();
                targetForm.reset(mapping, request);
            }
            catch(Exception e){
                throw new WebException("err.failed_to_instantiate_form", new String[]{((formClass != null) ? formClass.getName() : null), formName}, e);
            }
        }
        return targetForm;
    }

    /** Store form in session */
    public static void storeForm(HttpSession session, String formName, BasicActionForm form){
        setAttr(session, formName, form);
    }

    public static Roadmap allocateRoadmap(HttpSession session){
        Roadmap roadmap = (Roadmap)SessionEnvironment.getAttr(session, "roadmap");
        if(roadmap == null){
            roadmap = new Roadmap();
            SessionEnvironment.setAttr(session, "roadmap", roadmap);
        }
        return roadmap;
    }

}
