package com.tmx.web.controls.grid;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Matrix {
    private ArrayList rowArr = new ArrayList();

    private static void setToArray(ArrayList a, int index, Object o) {
        if (a.size() <= index) {
            a.addAll(a.size(), Arrays.asList(new Object[index - a.size() + 1]));
        }
        a.set(index, o);
    }

    private static Object getFromArray(ArrayList a, int index) {
        if (a.size() <= index) return null;
        return a.get(index);
    }

    public void set(int row, int col, Object value) {
        ArrayList colArr=(ArrayList) getFromArray(rowArr,row);
        if(colArr==null) setToArray(rowArr,row,colArr=new ArrayList());
        setToArray(colArr,col,value);
    }

    public Object get(int row, int col) {
        ArrayList colArr=(ArrayList) getFromArray(rowArr,row);
        if(colArr==null)return null;
        return getFromArray(colArr,col);
    }

    public void delete(int row, int col) {
        set(row,col,null);
    }

    public List toList(){
        return rowArr;
    }
}
