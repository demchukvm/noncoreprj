package com.tmx.util.i18n;

import java.util.Locale;
import java.util.List;

/**
    
 */
public class DictionaryWrapper {
    private MessageDictionary messageDictionary = MessageDictionary.getDictionaryInstance();
    private String bundleName;
    private Class ownerClass;
    private Locale locale;

    public DictionaryWrapper(Locale locale, Class ownerClass, String bundleName) {
        this.locale = locale;
        this.bundleName = bundleName;
        this.ownerClass = ownerClass;
    }

    public String getValue(String key) {
        return messageDictionary.getOwnedLocalizedMessage(ownerClass, bundleName, locale, key, null);
    }

    public String getValue(long key) {
        return getValue(String.valueOf(key));
    }

    public List getKeys() {
        return messageDictionary.getKeys(ownerClass, bundleName, locale);
    }

    public List getList(){
        return messageDictionary.getList(ownerClass, bundleName, locale);
    }
}
