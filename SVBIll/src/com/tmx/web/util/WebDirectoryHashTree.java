package com.tmx.web.util;

import com.tmx.util.*;
import com.tmx.web.base.ApplicationEnvironment;

import java.io.File;


/**
 *
 */
public class WebDirectoryHashTree extends DirectoryHashTree {

    /**
     * @param relativeRootPath
     * */
    public static TreeNode initializeRoot(String relativeRootPath) throws InitException {
        return DirectoryHashTree.initializeRoot(ApplicationEnvironment.getWebPath() + relativeRootPath);
    }


    /** @return relative path to saved file in the tree */
    public static String saveHashedFile(String relativeRootPath, String originalFileName, byte[] fileContent) throws ResourceException{
        String extension = "";
        if(originalFileName.lastIndexOf(".") >=0 )
             extension = originalFileName.substring(originalFileName.lastIndexOf("."));
        String hahsedName = MD5.getHashString(originalFileName + String.valueOf(System.currentTimeMillis())) + extension;
        return saveOriginalFile(relativeRootPath, hahsedName, fileContent);
    }

    public static String saveOriginalFile(String relativeRootPath, String originalFileName, byte[] fileContent) throws ResourceException{
        String absoluteRootPath = ApplicationEnvironment.getWebPath() + relativeRootPath;
        String absolutePath = DirectoryHashTree.saveOriginalFile(absoluteRootPath, originalFileName, fileContent);
        absolutePath = StringUtil.normalize(absolutePath);
        int startRelativePathIdx = absolutePath.indexOf(relativeRootPath);
        if(startRelativePathIdx < 0)
            throw new ResourceException("err.web_directory_hash_tree.absolute_path_does_not_contain_web_root", new String[]{absolutePath});
        return absolutePath.substring(startRelativePathIdx + relativeRootPath.length() + File.separator.length());
    }

    public static String getFileAbsoulutePath(String relativeRootPath, String relativeFilePath) {
        return ApplicationEnvironment.getWebPath() + relativeRootPath + File.separator + relativeFilePath;
    }

    public static String getFileUrl(String relativeRootPath, String relativeFilePath){
        return ApplicationEnvironment.getAppDeploymentName() + relativeRootPath + "/" + relativeFilePath;
    }
}
