package com.tmx.as.entities.bill.exclusive;

import com.tmx.as.base.BasicEntity;
import com.tmx.as.entities.general.user.User;
import java.util.Date;
import java.io.Serializable;

public class Exclusives extends BasicEntity {

    private Long exclusiveId;
    private String exclusive;

    public Serializable getId() {
        return getExclusiveId();
    }

    public int getValid() {
        return valid;
    }

    public void setValid(int valid) {
        this.valid = valid;
    }

    public Long getExclusiveId() {
        return exclusiveId;
    }

    public void setExclusiveId(Long exclusiveId) {
        this.exclusiveId = exclusiveId;
    }

    public String getExclusive() {
        return exclusive;
    }

    public void setExclusive(String exclusive) {
        this.exclusive = exclusive;
    }
}
