package com.tmx.web.forms.core.restriction.terminal_restriction;

import com.tmx.as.entities.bill.restriction.AbstractRestriction;
import com.tmx.as.entities.bill.restriction.TerminalRestriction;
import com.tmx.as.entities.bill.terminal.Terminal;
import com.tmx.as.entities.general.user.User;
import com.tmx.as.exceptions.DatabaseException;
import com.tmx.web.controls.BrowseBox;
import com.tmx.web.controls.PopulateUtil;
import com.tmx.web.forms.core.restriction.AbstractInfoRestrictionForm;


public class InfoTerminalRestrictionForm extends AbstractInfoRestrictionForm {

    private BrowseBox browseBoxTerminals;

    protected void init() {
        super.init();
        initButtons();
        initTables();
        initBrowses();
    }

    private void initBrowses(){
        browseBoxTerminals = new BrowseBox("/core/balance/terminal_balance/browseTerminalsAction");
        getBrowseBoxFunctionTypes().setAction("/core/restriction/terminal_restriction/browseTerminalFuncTypesAction");
        browseBoxTerminals.setMandatory(true);
    }

    private void initButtons(){
        getOnSaveButton().setAction("/core/restriction/terminal_restriction/infoTerminalRestriction");
        getOnApplyButton().setAction("/core/restriction/terminal_restriction/infoTerminalRestriction");
        getOnCancelButton().setAction("/core/restriction/terminal_restriction/manageTerminalRestrictions");

        getOnRefreshParamButton().setAction("/core/restriction/terminal_restriction/infoTerminalRestriction");
        getOnCreateParamButton().setAction("/core/restriction/terminal_restriction/infoTerminalRestriction");
        getOnEditParamButton().setAction("/core/restriction/terminal_restriction/terminalRestrictionParameter");
        getOnDeleteParamButton().setAction("/core/restriction/terminal_restriction/terminalRestrictionParameter");
    }
    
    private void initTables(){
        getFuncParametrsTable().setRefreshActionName("/core/restriction/terminal_restriction/infoTerminalRestriction");
        getDefaultFuncParametrsTable().setRefreshActionName("/core/restriction/terminal_restriction/infoTerminalRestriction");
    }

    protected AbstractRestriction populateFromSpecificControls() {
        TerminalRestriction restriction = new TerminalRestriction();

        Terminal terminal = new Terminal();
        terminal.setTerminalId(PopulateUtil.getLongKey(browseBoxTerminals));
        restriction.setTerminal(terminal);

        return restriction;
    }

    protected void bindSpecificData(AbstractRestriction restriction, User loggedInUser) throws DatabaseException {
        //do nohing
    }

    protected void bindSpecificData(AbstractRestriction restriction) {
        TerminalRestriction rest = (TerminalRestriction)restriction;
        browseBoxTerminals.setKey(rest.getTerminal().getSerialNumber());
        browseBoxTerminals.setValue(rest.getTerminal().getSerialNumber());
    }

    public BrowseBox getBrowseBoxTerminals() {
        return browseBoxTerminals;
    }

    public void setBrowseBoxTerminals(BrowseBox browseBoxTerminals) {
        this.browseBoxTerminals = browseBoxTerminals;
    }
}
