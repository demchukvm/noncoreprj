package com.tmx.beng.base.processors;

import com.tmx.beng.base.*;
import com.tmx.beng.base.cache.DBCache;
import com.tmx.as.base.FilterWrapper;
import com.tmx.as.base.EntityManager;
import com.tmx.as.entities.bill.transaction.ClientTransaction;
import com.tmx.as.entities.bill.transaction.OperatorBalanceTransaction;
import com.tmx.as.entities.bill.transaction.TerminalBalanceTransaction;

/**
Identify request status processor
 */
public class FindTransactionProcessor extends TaskProcessor{

    public Task process0(Task task, DBCache dbCache, BillingMessageWritable msg) throws BillException {
        logger.debug("Find transaction processor...");
        EntityManager entityManager = new EntityManager();
        try{
            ClientTransaction clientTransaction;

            if(msg.getAttributeString(BillingMessage.CLI_TRANSACTION_NUM) != null &&
                msg.getAttributeString(BillingMessage.BILL_TRANSACTION_NUM) == null){

                    String cliTransactionNum = msg.getAttributeString(BillingMessage.CLI_TRANSACTION_NUM);
                    FilterWrapper byCliNumFW = new FilterWrapper("by_clinum");
                    byCliNumFW.setParameter("clinum", cliTransactionNum);
                    clientTransaction = (ClientTransaction)entityManager.RETRIEVE(
                            ClientTransaction.class,
                            new FilterWrapper[]{byCliNumFW},
                            null
                    );

                    if (clientTransaction == null) {
                        throw new BillException("err.find_transaction_proc.transaction_not_found", new String[]{cliTransactionNum});
                    }
            }else if(msg.getAttributeString(BillingMessage.BILL_TRANSACTION_NUM) != null &&
                    msg.getAttributeString(BillingMessage.CLI_TRANSACTION_NUM) == null){

                    FilterWrapper byBillNumFW = new FilterWrapper("by_billnum");
                    byBillNumFW.setParameter("billnum", msg.getAttributeString(BillingMessage.BILL_TRANSACTION_NUM));
                    clientTransaction = (ClientTransaction)entityManager.RETRIEVE(
                            ClientTransaction.class,
                            new FilterWrapper[]{byBillNumFW},
                            null
                    );

                    if(clientTransaction == null){
                        throw new BillException("err.find_transaction_proc.transaction_not_found", new String[]{
                                msg.getAttributeString(BillingMessage.BILL_TRANSACTION_NUM)});
                    }
            }else{
                throw new BillException("err.find_transaction_proc.incorrect_trans_numbers_combination");
            }

            msg.setAttribute(BillingMessage.CLIENT_TRANSACTION, clientTransaction);
            msg.setAttributeString(BillingMessage.BILL_TRANSACTION_NUM, clientTransaction.getTransactionGUID());

            // get operator transaction
            FilterWrapper byBillNumFW = new FilterWrapper("by_billnum");
            byBillNumFW.setParameter("billnum", clientTransaction.getTransactionGUID());

            OperatorBalanceTransaction operatorTransaction = (OperatorBalanceTransaction) entityManager.RETRIEVE(
                    OperatorBalanceTransaction.class,
                    new FilterWrapper[]{  byBillNumFW },
                    null
            );
            msg.setAttribute(BillingMessage.OPERATOR_TRANSACTION, operatorTransaction);

            // get terminal transaction
            TerminalBalanceTransaction terminalTransaction = (TerminalBalanceTransaction) entityManager.RETRIEVE(
                    TerminalBalanceTransaction.class,
                    new FilterWrapper[]{  byBillNumFW },
                    null
            );
            msg.setAttribute(BillingMessage.TERMINAL_TRANSACTION, terminalTransaction);


        }catch(BillException e){
            msg.setStatus(StatusDictionary.FIND_TRANSACTION_ERROR, e.getLocalizedMessage());
            throw e;
        }catch(Throwable e){
            msg.setStatus(StatusDictionary.FIND_TRANSACTION_ERROR, e.getLocalizedMessage());
            throw new BillException("err.find_transaction_proc.uncknown_error");
        }
        return task;
    }

}
