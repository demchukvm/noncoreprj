package com.tmx.web.forms.core.balance.operator_balance;

import com.tmx.web.forms.core.restriction.RestrictionParameterForm;

@Deprecated
public class OperatorParameterForm extends RestrictionParameterForm {

     protected void init(){
        super.init();
        initButtons();
    }

    private void initButtons(){
        getOnSaveButton().setAction("/core/balance/operator_balance/operatorParameter");
        getOnApplyButton().setAction("/core/balance/operator_balance/operatorParameter");
        getOnCancelButton().setAction("/core/balance/operator_balance/operatorRestriction");
    }

}
