package com.tmx.beng.base;

import com.tmx.as.base.EntityManager;
import com.tmx.as.base.criterion.CriterionWrapper;
import com.tmx.as.base.criterion.Restrictions;
import com.tmx.as.entities.bill.gates.ProductGates;
import com.tmx.as.exceptions.DatabaseException;
import com.tmx.util.Configuration;
import com.tmx.util.InitException;
import com.tmx.util.cache.FileCache;
import com.tmx.util.cache.CacheException;
import com.tmx.beng.base.beans.*;

import java.io.InputStream;
import java.util.HashMap;
import java.util.List;

public class MediumResolver extends FileCache {
    private static MediumResolver instance = null;

    // Proximan
    private HashMap mediumMap = new HashMap();

    protected Object getRealObjectFromFile(InputStream is, String path) throws CacheException {
        /* Old Version */

        /*
        MediumResolverDocument root;

        try{
            root = MediumResolverDocument.Factory.parse(is);
        }catch(Throwable e){
            throw new CacheException("err.medium_resolver.failed_to_parse_config", new String[]{ path }, e);
        }

        MediumType[] mediumType = root.getMediumResolver().getMediumArray();
        HashMap mediumMap = new HashMap();
        for(int i = 0; i < mediumType.length; i++){
            mediumMap.put(mediumType[i].getChannelName(), mediumType[i].getAccessName());
        }

        return mediumMap;
        */

        /* New Verion */
        /*
        MediumResolverDocument root;
        try {
            root = MediumResolverDocument.Factory.parse(is);
        } catch (Throwable e) {
            throw new CacheException("err.medium_resolver.failed_to_parse_config", new String[]{ path }, e);
        }

        HashMap mediumMap = new HashMap();

        ProductName[] productNames = root.getMediumResolver().getProductArray();


        for(int i=0; i<productNames.length; i++)
        {
            ProductName.GateName[] gateNames = productNames[i].getGateNameArray();
            for(int j=0; j<gateNames.length; j++)
            {
                if(gateNames[j].getValue() == true)
                {
                    mediumMap.put(productNames[i].getName(), gateNames[j].getName());
                    break;
                }
            }
        }

        return mediumMap; */

      //  HashMap mediumMap = new HashMap();
        try {
            mediumMap = getMediumAccessName();
        } catch (BillException e) {
            e.printStackTrace();
        }
        return mediumMap;
    }

    public static MediumResolver getInstance(){
        if(instance == null){
            instance = new MediumResolver();
        }
        return instance;
    }

    private MediumResolver(){
        maxSize = 100;
        cleanCount = 10;
        cleanType = CleanAlgorithm.Type.CLEAN_N_SELDOM_ACCESSED;
    }

    // Old
    /*public String getMediumAccessName(String channelName, String configFile) throws BillException{
        HashMap mediumMap;
        try{
            mediumMap = (HashMap)get(configFile);
        }catch(CacheException e){
            throw new BillException("err.bill.failed_to_get_medium_map", new String[]{ configFile }, e);
        }

        String mediumName = (String) mediumMap.get(channelName);
        if(mediumName == null){
            throw new BillException("err.bill.failed_to_get_medium", new String[]{ channelName });
        }
        return mediumName;
    }*/

    // Proximan
    public String getMediumAccessName(String productName) throws BillException
    {
        if(mediumMap.isEmpty())
        {
            getMediumAccessName();        
        }

        String gateName = null;
        try {
            gateName = mediumMap.get(productName).toString();
            if(gateName == null) throw new InitException("Product not found");
        } catch (InitException e) {
            e.printStackTrace();
        }

        return gateName;
    }

    // Proximan
    public HashMap getMediumAccessName() throws BillException
    {
        // с конфига
        /*
        String configFile = Configuration.getInstance().getProperty("medium_resolver.config_file");
        HashMap mediumMap;
        try{
            mediumMap = (HashMap)get(configFile);
        }catch(CacheException e){
            throw new BillException("err.bill.failed_to_get_medium_map", new String[]{ configFile }, e);
        }
        return mediumMap;
        */
        mediumMap.clear();

        EntityManager em = new EntityManager();
        CriterionWrapper criterions[] = new CriterionWrapper[] {
                Restrictions.eq("valid", 1)};

        List<ProductGates> productNameList = null;
        try {
            productNameList = em.RETRIEVE_ALL(ProductGates.class, criterions, null);
        } catch (DatabaseException e) {
            e.printStackTrace();
        }

  //      HashMap mediumMap = new HashMap();
        for(ProductGates el : productNameList)
        {
            mediumMap.put(el.getProductName(), el.getGateName());
        }

        return mediumMap;
    }

    // Proximan
    public void changeMediumResolver(String productName, String gateName)
    {
        EntityManager em = new EntityManager();
        CriterionWrapper criterions[] = new CriterionWrapper[] {
                Restrictions.eq("productName", productName)};

        ProductGates productGates = null;
        try {
            productGates = (ProductGates) em.RETRIEVE(ProductGates.class, null, criterions, null);

            productGates.setGateName(gateName);

            em.SAVE(productGates);

        } catch (DatabaseException e) {
            e.printStackTrace();
        }
    }

    public HashMap getMediumMap() {
        return mediumMap;
    }

    public void setMediumMap(HashMap mediumMap) {
        this.mediumMap = mediumMap;
    }
}
