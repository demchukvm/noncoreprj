package com.tmx.beng.httpsgate.avancel;

import org.apache.log4j.Logger;
import com.tmx.beng.httpsgate.HttpsGateException;
import com.tmx.util.InitException;

/**

 */
public class AvancelGate4 extends BasicAvancelGate{
    protected static final String GATE_NAME = "avancel_gate_4";
    protected static Logger logger = Logger.getLogger("avancel_gate_4." + GATE_NAME);
    private static AvancelGate4 avancelGate = null;


    public static AvancelGate4 getInstance() throws HttpsGateException {
        if(avancelGate == null){
            logger.error("Attempt to obtain not inited AvancelGate4");
            throw new HttpsGateException("err.AvancelGate4.null_instance");
        }
        return avancelGate;
    }

    public static void init()throws InitException {
        logger.info("Start AvancelGate4 initialization");
        avancelGate = new AvancelGate4();
        avancelGate.init0();
        logger.info("AvancelGate4 initialized");
    }

    public static void shutdown(final long timeout) {
        logger.info("Start AvancelGate4 Shutdown");
        avancelGate.shutdown0(timeout);
        logger.info("AvancelGate4 shutdown");
    }

    protected String getGateName(){
        return GATE_NAME;
    }

}
