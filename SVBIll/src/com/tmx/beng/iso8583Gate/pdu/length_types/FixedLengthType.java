package com.tmx.beng.iso8583Gate.pdu.length_types;

import com.tmx.beng.iso8583Gate.pdu.IsoParseException;
import com.tmx.beng.iso8583Gate.pdu.IsoSerializeException;

import java.io.InputStream;
import java.io.OutputStream;

/**
 */
public class FixedLengthType extends LengthType {
    {
        name = "FIXED";
    }

    public int parseLength(InputStream is, int maxLEngth) throws IsoParseException {
        //FixedLengthType is not provided for read length from input stream
        throw new IsoParseException("err.fixed_lengthtype.parsing_failed");
    }

//    public void serializeLength(OutputStream os, int length) throws IsoSerializeException{
//        //do nothing - fixed length type is not inserted into packet
//    }

    public byte[] serializeLength(int length) throws IsoSerializeException{
        return new byte[]{}; 
    }
}
