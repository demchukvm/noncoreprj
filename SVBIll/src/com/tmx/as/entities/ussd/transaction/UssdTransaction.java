package com.tmx.as.entities.ussd.transaction;

import com.tmx.as.base.BasicEntity;

import java.util.Date;
import java.io.Serializable;

/**
 */
public class UssdTransaction extends BasicEntity {
    private Long ussdTransactionId;
    private String msisdn;
    private Double amount;
    private Date beginDate;
    private Date commitDate;
    private Date confirmDate;
    private String confirmTransaction;
    private String billTransactionNum;
    private String requestId;
    private String serviceCode;
    private Integer statusCode;
    private String statusMessage;

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public Date getCommitDate() {
        return commitDate;
    }

    public void setCommitDate(Date commitDate) {
        this.commitDate = commitDate;
    }

    public String getServiceCode() {
        return serviceCode;
    }

    public void setServiceCode(String serviceCode) {
        this.serviceCode = serviceCode;
    }

    public Integer getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }

    public void setStatusCodeInt(int statusCode) {
        this.statusCode = new Integer(statusCode);
    }

    public String getStatusMessage() {
        return statusMessage;
    }

    public void setStatusMessage(String statusMessage) {
        if(statusMessage != null && statusMessage.length() > 255){
            statusMessage = statusMessage.substring(0, 255);
        }
        this.statusMessage = statusMessage;
    }

    public String getMsisdn() {
        return msisdn;
    }

    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }


    public Date getBeginDate() {
        return beginDate;
    }

    public void setBeginDate(Date beginDate) {
        this.beginDate = beginDate;
    }


    public Long getUssdTransactionId() {
        return ussdTransactionId;
    }

    public void setUssdTransactionId(Long ussdTransactionId) {
        this.ussdTransactionId = ussdTransactionId;
    }

    public Date getConfirmDate() {
        return confirmDate;
    }

    public void setConfirmDate(Date confirmDate) {
        this.confirmDate = confirmDate;
    }

    public String getConfirmTransaction() {
        return confirmTransaction;
    }

    public void setConfirmTransaction(String confirmTransaction) {
        this.confirmTransaction = confirmTransaction;
    }

    public String getBillTransactionNum() {
        return billTransactionNum;
    }

    public void setBillTransactionNum(String billTransactionNum) {
        this.billTransactionNum = billTransactionNum;
    }

    public Serializable getId() {
        return getUssdTransactionId();
    }

    public int getValid() {
        return valid;
    }

    public void setValid(int valid) {
        this.valid = valid;
    }
}
