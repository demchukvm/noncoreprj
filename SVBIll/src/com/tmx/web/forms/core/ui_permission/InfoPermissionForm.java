package com.tmx.web.forms.core.ui_permission;

import com.tmx.web.base.BasicActionForm;
import com.tmx.web.controls.*;
import com.tmx.as.entities.general.ui_permission.Permission;
import com.tmx.as.entities.general.ui_permission.UserInterface;
import com.tmx.as.entities.general.ui_permission.PermissionType;
import com.tmx.as.entities.general.role.Role;

public class InfoPermissionForm extends BasicActionForm {
    // work
    private Long selectedPermissionId;
    private final static String action="/core/ui_permission/infoPermission";
    private Permission content;
    // info
    private EditBox permissionId;
    private BrowseBox userInterface;
    private ScrollBox permissionType;
    private ScrollBox role;
    // buttons
    private Button buttonApply;
    private Button buttonSave;
    private Button buttonCancel;
    private Button buttonReset;
    protected void init() {
        // info
        permissionId =new EditBox();
        permissionId.setReadonly(true);
        userInterface=new BrowseBox();userInterface.setMandatory(true);
        userInterface.setAction("/core/ui_permission/browseUserInterface");
        userInterface.setCommand("openBrowseWindow");
        permissionType=new ScrollBox() {
            protected String getKey(Object dataListElement) {
                return ((PermissionType)dataListElement).getPermissionTypeId().toString();
            }

            protected String getValue(Object dataListElement) {
                return ((PermissionType)dataListElement).getName();
            }
        };
        role=new ScrollBox() {
            protected String getKey(Object dataListElement) {
                return ((Role)dataListElement).getRoleId().toString();
            }

            protected String getValue(Object dataListElement) {
                return ((Role)dataListElement).getRoleName();
            }
        };
        // buttons
        buttonApply=new Button();
        buttonApply.setLabelKey("ctrl.button.label.apply");
        buttonApply.setAction(action);
        buttonApply.setCommand("apply");

        buttonSave=new Button();
        buttonSave.setLabelKey("ctrl.button.label.save");
        buttonSave.setAction(action);
        buttonSave.setCommand("save");

        buttonCancel=new Button();
        buttonCancel.setLabelKey("ctrl.button.label.cancel");
        buttonCancel.setAction("/core/ui_permission/managePermission");

        buttonReset =new Button();
        buttonReset.setLabelKey("ctrl.button.label.reset");
    }

    public void bindData(Permission value) {
        content=value;
        permissionId.setValue(value.getId() == null ? "" : value.getId().toString());
        userInterface.setKey(value.getUserInterface()==null?"":value.getUserInterface().getUserInterfaceId().toString());
        userInterface.setValue(value.getUserInterface()==null?"":value.getUserInterface().getName());
        if(value.getPermissionType()!=null)
            permissionType.setSelectedKey(value.getPermissionType().getPermissionTypeId().toString());
        if(value.getRole()!=null)
            role.setSelectedKey(value.getRole().getRoleId().toString());
    }

    public Permission populateFromControls() {
        content.setPermissionId(PopulateUtil.getLongValue(permissionId));
        UserInterface userInterface=new UserInterface();
        userInterface.setUserInterfaceId(PopulateUtil.getLongKey(this.userInterface));
        content.setUserInterface(userInterface);
        PermissionType permissionType=new PermissionType();
        permissionType.setPermissionTypeId(new Long(this.permissionType.getSelectedKey()));
        content.setPermissionType(permissionType);
        Role role=new Role();
        role.setRoleId(new Long(this.role.getSelectedKey()));
        content.setRole(role);
        return content;
    }

    public Button getButtonApply() {
        return buttonApply;
    }

    public void setButtonApply(Button buttonApply) {
        this.buttonApply = buttonApply;
    }

    public Button getButtonSave() {
        return buttonSave;
    }

    public void setButtonSave(Button buttonSave) {
        this.buttonSave = buttonSave;
    }

    public Button getButtonCancel() {
        return buttonCancel;
    }

    public void setButtonCancel(Button buttonCancel) {
        this.buttonCancel = buttonCancel;
    }

    public Button getButtonReset() {
        return buttonReset;
    }

    public void setButtonReset(Button buttonReset) {
        this.buttonReset = buttonReset;
    }

    public Long getSelectedPermissionId() {
        return selectedPermissionId;
    }

    public void setSelectedPermissionId(Long selectedPermissionId) {
        this.selectedPermissionId = selectedPermissionId;
    }

    public Permission getContent() {
        return content;
    }

    public void setContent(Permission content) {
        this.content = content;
    }

    public EditBox getPermissionId() {
        return permissionId;
    }

    public void setPermissionId(EditBox PermissionId) {
        this.permissionId = PermissionId;
    }

    public BrowseBox getUserInterface() {
        return userInterface;
    }

    public void setUserInterface(BrowseBox userInterface) {
        this.userInterface = userInterface;
    }

    public ScrollBox getPermissionType() {
        return permissionType;
    }

    public void setPermissionType(ScrollBox permissionType) {
        this.permissionType = permissionType;
    }

    public ScrollBox getRole() {
        return role;
    }

    public void setRole(ScrollBox role) {
        this.role = role;
    }
}


