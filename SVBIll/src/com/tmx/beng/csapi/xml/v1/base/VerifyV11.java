package com.tmx.beng.csapi.xml.v1.base;

import com.tmx.gate.csapi.v1.base.beans.*;
import com.tmx.beng.base.BillException;
import com.tmx.beng.base.BillingMessage;
import com.tmx.beng.base.BillingMessageImpl;

import java.util.regex.Matcher;

/**
 * Created by IntelliJ IDEA.
 * User: Shake
 * Date: 27.04.2007
 * Time: 11:16:02
 */
public class VerifyV11 extends AbstractVerify
{
    public void verify(RootDocument root, BillingMessageImpl billMsg) throws BillException
    {
        super.verify(root, billMsg);

        OperationsType operations = root.getRoot().getRequest().getOperations();
        if(operations.getRefillPayment() != null)
        {
            RefillPaymentReqType operation = operations.getRefillPayment();
            if(operation.getMsisdn() == null && operation.getPayAccount() == null)
                throw new BillException("err.csapi.msisdn_or_payaccount_not_found");
            if(operation.getMsisdn() != null && operation.getPayAccount() != null)
                throw new BillException("err.csapi.msisdn_and_payaccount_declared");
            if(operation.getMsisdn() != null){
                // check msisdn
                Matcher matcher = msisdnPattern.matcher(operation.getMsisdn());
                if(!matcher.matches()){
                    throw new BillException("err.csapi.incorrect_msisdn_format");
                }
                billMsg.setAttributeString(BillingMessage.ACCOUNT_NUMBER, operation.getMsisdn());
            }
            billMsg.setAttributeString(BillingMessage.PAY_ACCOUNT_NUMBER, operation.getPayAccount());
            billMsg.setAttributeString(BillingMessage.REGION_NUMBER, operation.getRegionNumber());
            billMsg.setAttributeString(BillingMessage.TRADE_POINT, operation.getTradePoint());

        }
        else if(operations.getRefillPaymentStatus() != null)
        {
            //
        }
        else if(operations.getCancelRefillPayment() != null)
        {
            CancelRefillPaymentReqType operation = operations.getCancelRefillPayment();
            billMsg.setOperationName(MSG_TYPE + operation.getDomNode().getNodeName());
            if(operation.getCliTransactionNum() == null && operation.getBillTransactionNum() == null)
                throw new BillException("err.csapi.cli_and_bill_transaction_num_not_found");
            billMsg.setAttributeString(BillingMessage.BILL_TRANSACTION_NUM, operation.getBillTransactionNum());
            billMsg.setAttributeString(BillingMessage.CLI_TRANSACTION_NUM, operation.getCliTransactionNum());
        }
        else if(operations.getBuyVoucher() != null)
        {
            //
        }
        else if(operations.getActivateMobileTerminal() != null)
        {
            //
        }
        else if(operations.getServicesParameters() != null)
        {
            //
        }
        else if(operations.getBalances() != null)
        {
            //
        }
        else if(operations.getReportFull() != null)
        {
            //
        }
        else if(operations.getReportShort() != null)
        {
            //
        }
        else
        {
            throw new BillException("err.csapi.uncknown_operation");
        }

    }

    public void serialize(RootDocument root, BillingMessage msg)
    {
        super.serialize(root, msg);

        if(BillingMessage.O_REFILL_PAYMENT.equals(msg.getOperationName()))
        {
            RefillPaymentRespType resp = root.getRoot().getResponse().getOperationsResults().getRefillPayment();
            if(msg.getAttribute(BillingMessage.RECEIPT_NUM) != null){
                resp.setReceiptNum(msg.getAttributeString(BillingMessage.RECEIPT_NUM));
            }
            if(msg.getAttribute(BillingMessage.PAY_ID) != null){
                resp.setPayId(Integer.parseInt(msg.getAttributeString(BillingMessage.PAY_ID)));
            }
        }
        else if(BillingMessage.O_REFILL_PAYMENT_RESULT.equals(msg.getOperationName()))
        {
            RefillPaymentRespType resp = root.getRoot().getResponse().getOperationsResults().getRefillPayment();
            if(msg.getAttribute(BillingMessage.RECEIPT_NUM) != null){
                resp.setReceiptNum(msg.getAttributeString(BillingMessage.RECEIPT_NUM));
            }
            if(msg.getAttribute(BillingMessage.PAY_ID) != null){
                resp.setPayId(Integer.parseInt(msg.getAttributeString(BillingMessage.PAY_ID)));
            }
        }
        else if(BillingMessage.O_REFILL_PAYMENT_STATUS.equals(msg.getOperationName()))
        {
            RefillPaymentStatusRespType resp = root.getRoot().getResponse().getOperationsResults().getRefillPaymentStatus();
            if(msg.getAttribute(BillingMessage.RECEIPT_NUM) != null){
                resp.setReceiptNum(msg.getAttributeString(BillingMessage.RECEIPT_NUM));
            }
            if(msg.getAttribute(BillingMessage.PAY_ID) != null){
                resp.setPayId(Integer.parseInt(msg.getAttributeString(BillingMessage.PAY_ID)));
            }
        }
    }
}
