package com.tmx.beng.httpsgate.dacard.pdu;

import com.tmx.util.StructurizedException;
import com.tmx.beng.httpsgate.dacard.DacardGateException;

import java.util.Locale;

/**
This exception could occurs with MAC calculation and validation, data encoding/decoding of the Dacard message.
 */
public class DacardCypherException extends DacardGateException {

    public DacardCypherException(String msg, String[] params, Throwable e, Locale locale) {
        super(msg, params, e, locale);
    }

    public DacardCypherException(String msg, String[] params) {
        super(msg, params);
    }

    public DacardCypherException(String msg, String[] params, Locale locale) {
        super(msg, params, locale);
    }

    public DacardCypherException(String msg, Throwable e, Locale locale) {
        super(msg, e, locale);
    }

    public DacardCypherException(String msg, Throwable e) {
        super(msg, e);
    }

    public DacardCypherException(String msg, Locale locale) {
        super(msg, locale);
    }

    public DacardCypherException(String msg) {
        super(msg);
    }

    public DacardCypherException(String msg, String[] params, Throwable e) {
        super(msg, params, e);
    }
}
