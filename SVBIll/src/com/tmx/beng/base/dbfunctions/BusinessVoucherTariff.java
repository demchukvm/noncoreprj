package com.tmx.beng.base.dbfunctions;

import com.tmx.as.entities.bill.transaction.TransactionType;
import com.tmx.beng.base.BillingMessage;
import com.tmx.beng.base.Reservation;
import com.tmx.beng.base.cache.InvokeFunctionException;
import java.util.Map;

public class BusinessVoucherTariff extends BasicTariffFunction {
    public static final String[] VoucherNames = {
            "BeelineVoucher5", "BeelineVoucher15", "BeelineVoucher30", "BeelineVoucher60", "BeelineVoucher90",
            "KyivstarVoucher25", "KyivstarVoucher50", "KyivstarVoucher100", "KyivstarVoucher300",
            "LifeVoucher10", "LifeVoucher25", "LifeVoucher50", "LifeVoucher100",
            "UmcVoucher10", "UmcVoucher25", "UmcVoucher50", "UmcVoucher100", "UmcVoucher200" 
    };
    public static final Double [] VoucherValues = {
            4.88, 14.63, 29.25, 58.50, 87.75,
            24.25, 48.50, 97.00, 291.00,
            9.63, 23.75, 47.50, 95.00,
            9.65, 24.13, 48.25, 96.50, 193.00
    };
    public static final Double [] BeelineVal = {};

    boolean isTariffApplicable(Map actualParams) {
        return BillingMessage.O_REFILL_PAYMENT.equals(task.getProcesedMessage().getOperationName());
    }

    void applyTariff(Map actualParams, Reservation reservation) throws InvokeFunctionException {
        try{
            final String voucherNominal = task.getProcesedMessage().getAttributeString(BillingMessage.VOUCHER_NOMINAL);
            double price = Double.parseDouble(String.valueOf(actualParams.get(voucherNominal)));
            //prepare reservation on primary tariff
            reservation.reserve(null, price, null, TransactionType.CODE_CREDIT);
        }
        catch(Throwable e){
            throw new InvokeFunctionException("err.tariff.calc_error", e);
        }
    }

    public BasicFunction.TariffParameter[] getDefaultParams() {
        BasicFunction.TariffParameter[] result = new BasicFunction.TariffParameter[18];
        for (int i=0; i<18; i++){
            BasicFunction.TariffParameter paramCost = new BasicFunction.TariffParameter();
            paramCost.setName(VoucherNames[i]);
            paramCost.setValue(VoucherValues[i].toString());
            paramCost.setOrderNum(new Long(i));
            paramCost.setMandatory(Boolean.FALSE);

            result[i] = paramCost;
        }

        return result;
    }

}
