package com.tmx.web.actions.core.balance.seller_balance;

import com.tmx.web.actions.core.balance.AbstractChangeBalanceAction;
import com.tmx.as.blogic.balance.AbstractBalanceManager;
import com.tmx.as.blogic.balance.SellerBalanceManager;
import com.tmx.util.InitException;
import com.tmx.beng.base.BillException;


public class ChangeSellerBalanceAction extends AbstractChangeBalanceAction {

    protected AbstractBalanceManager getBalanceManager() throws InitException, BillException {
        return new SellerBalanceManager();
    }

    protected String getInfoPageForward() {
        return "updateInfoFormAction";  
    }
}
