<%@ page language="java" pageEncoding="UTF-8" contentType="text/html; charset=utf-8" isELIgnored="false" %>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-nested" prefix="nested" %>
<%@ taglib uri="http://tmx.com/taglib/tmx-controls" prefix="tmx_ctrl" %>
<%@ taglib uri="http://tmx.com/taglib/tmx-logic" prefix="tmx_logic" %>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html" %>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean" %>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>

<html:form action="/core/report/reportOnlineSelles">
    <table  border="0" width="100%">
        <tr>
            <td class="tabForm" align="left">
                <table>
                    <tr>
                        <td class="dataLabel">
                            <bean:message bundle="core.labels" key="core.report.reportPayment.period"/>
                            &nbsp;
                            <bean:message bundle="core.labels" key="core.monitor.timeFrom"/>
                            :
                        </td>
                        <td class="dataField">
                            <tmx_ctrl:timeSelector property="from"/>
                        </td>

                        <td class="dataLabel">
                            <bean:message bundle="core.labels" key="core.monitor.timeTo"/>
                            :
                        </td>
                        <td class="dataField">
                            <tmx_ctrl:timeSelector property="to"/>
                        </td>

                        <td class="dataLabel">
                            <bean:message bundle="core.labels" key="core.report.reportSelles.operators"/>
                            :
                        </td>
                        <td class="dataField">
                            <tmx_ctrl:multipleSelectBox property="operators"/>
                        </td>
                        <td class="dataField">
                            <tmx_ctrl:button property="buttonSubmit"/>
                        </td>
                    </tr>
                    <tr>
                        <td class="dataLabel">
                            <bean:message bundle="core.labels" key="core.report.reportSelles.dilers"/>
                            :
                        </td>
                        <td class="dataField">
                            <tmx_ctrl:multipleSelectBox property="sellers"/>
                        </td>

                        <td class="dataLabel">
                            <bean:message bundle="core.labels" key="core.report.reportSelles.subdilers"/>
                            :
                        </td>
                        <td class="dataField">
                            <tmx_ctrl:multipleSelectBox property="subSellers"/>
                        </td>

                        <td class="dataLabel">
                            <bean:message bundle="core.labels" key="core.report.reportSelles.terms"/>
                            :
                        </td>
                        <td class="dataField">
                            <tmx_ctrl:multipleSelectBox property="terms"/>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <tmx_ctrl:grid property="grid"/>
            </td>
        </tr>
    </table>

</html:form>