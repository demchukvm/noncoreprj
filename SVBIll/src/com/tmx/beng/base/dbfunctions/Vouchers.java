package com.tmx.beng.base.dbfunctions;

public class Vouchers {
        private static final String[] voucherNames = {
                "BeelineVoucher5", "BeelineVoucher15", "BeelineVoucher30", "BeelineVoucher60", "BeelineVoucher90",
                "KyivstarVoucher25", "KyivstarVoucher50", "KyivstarVoucher100", "KyivstarVoucher300",
                "LifeVoucher10", "LifeVoucher25", "LifeVoucher50", "LifeVoucher100",
                "UmcVoucher10", "UmcVoucher25", "UmcVoucher50", "UmcVoucher100", "UmcVoucher200"
        };

        public String[] getVoucherNames() {
            return voucherNames;
        }


}
