package com.tmx.engines.xmlengine.customTags;

/**
 * Group node is used to incapsulate another nodes. 
 */
public class GroupNode extends AbstractNode {

    public GroupNode(String groupId, String groupName){
        setName(groupName);
        setId(groupId);
    }


    public String getCDATASectionValue(){
        //the <group> node doesn't contain CDATA section.
        return null;
    }

    public String getTagName() {
        return "group";
    }
}
