package com.tmx.web.actions.core.transactions.terminal_balance_transaction;

import com.tmx.as.blogic.TransactionManager;
import com.tmx.as.exceptions.DatabaseException;
import com.tmx.as.base.FilterWrapper;
import com.tmx.web.actions.core.AbstractSvbillAction;
import com.tmx.web.controls.ScrollBox;
import com.tmx.web.controls.table.FilterSet;
import com.tmx.web.forms.core.base.AbstractFormFactory;
import com.tmx.web.forms.core.transaction.terminal_balance_transaction.ManageTerminalBalanceTransactionsForm;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class ManageTerminalBalanceTransactionsAction extends AbstractSvbillAction {
    
    private static final String FORWARD_MANAGE_TERMINAL_TRANSACTIONS_PAGE = "manageTerminalTransactionsPage";

    public ActionForward defaultAction(ActionMapping mapping,
                                       ActionForm form,
                                       HttpServletRequest request,
                                       HttpServletResponse response,
                                       String methodName) throws Throwable {
        ManageTerminalBalanceTransactionsForm transactionsForm = (ManageTerminalBalanceTransactionsForm)form;
        AbstractFormFactory.getInstance(getLoggedInUser(request)).setUpForm(transactionsForm);
        transactionsForm.getTerminalTransactionsTable().refresh();
        preloadFilters(transactionsForm);
//        setUpCalculationResult(transactionsForm);
        getRoadmap().collectParameter("/core/transactions/terminal_transaction/infoTerminalTransaction", "onCancelAction", "manageTerminalTransactionsAction");
        return mapping.findForward(FORWARD_MANAGE_TERMINAL_TRANSACTIONS_PAGE);
    }

    private void setUpCalculationResult(ManageTerminalBalanceTransactionsForm form) throws ClassNotFoundException, DatabaseException {
        FilterWrapper[] filterWrappers = form.getTerminalTransactionsTable().getQuery().getFilters();
        String entityClassName = form.getTerminalTransactionsTable().getQuery().getEntityClass();
        TransactionManager.CalculationResult calcResult
                = new TransactionManager().calculateTransactions(filterWrappers, entityClassName);
        form.getEditBoxAllTransaction().setValue( calcResult.getTransactionCaunt().toString() );
        form.getEditBoxSuccessTransaction().setValue( calcResult.getTransactionSuccessCount().toString() );
        form.getEditBoxAllTransAmount().setValue( calcResult.getTransactionAmount().toString() );
        form.getEditBoxSuccessTransAmount().setValue( calcResult.getTransactionSuccessAmount().toString() );
    }

    private void preloadFilters(ManageTerminalBalanceTransactionsForm terminalTransForm) throws DatabaseException {
        FilterSet filterSet = terminalTransForm.getTerminalTransactionsTable().getFilterSet();
        ScrollBox scrollBoxTransType = (ScrollBox)filterSet.getFilter("by_transaction_type").getParameter("type").getControl();
        scrollBoxTransType.bind(new TransactionManager().getAllTransactionType());
    }

}
