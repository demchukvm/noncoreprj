package com.tmx.beng.httpsgate.cyberplat;

import com.tmx.as.base.EntityManager;
import com.tmx.as.base.criterion.CriterionWrapper;
import com.tmx.as.base.criterion.Restrictions;
import com.tmx.as.entities.bill.transaction.ClientTransaction;
import com.tmx.as.entities.bill.transaction.OperatorBalanceTransaction;
import com.tmx.as.entities.bill.transaction.SellerBalanceTransaction;
import com.tmx.as.entities.bill.transaction.TerminalBalanceTransaction;
import com.tmx.as.entities.mobipay.cyberplat.CyberplatTransaction;
import com.tmx.as.exceptions.DatabaseException;
import com.tmx.beng.access.Access;
import com.tmx.beng.base.*;
import com.tmx.beng.httpsgate.HttpsGateException;
import com.tmx.beng.httpsgate.HttpsRequestException;
import com.tmx.gate.datastream.base.GateContext;
import com.tmx.util.Configuration;
import com.tmx.util.InitException;
import com.tmx.util.queue.ActionQueue;
import org.CyberPlat.IPriv;
import org.CyberPlat.IPrivException;
import org.CyberPlat.IPrivKey;
import org.apache.commons.digester.Digester;
import org.apache.log4j.Logger;
import java.io.*;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.*;


public abstract class BasicCyberplatGate extends BasicGate
{
    protected Logger logger = Logger.getLogger(getGateName());
    private Access access = null;
    private ActionQueue messageQueue = null;
    //private CodeMapping codeMapping;

    private static final String PROP_INIT_HANDLER_COUNT = "handlers.initial";
    private static final String PROP_MAX_HANDLER_COUNT = "handlers.max";
    private static final String SECRET_KEY = "secret.key";
    private static final String SECRET_KEY_PASS = "secret.key_pass";
    private static final String PUBLIC_KEY = "pubkeys.key";
    private static final String PUBLIC_KEY_PASS = "pubkeys.key_pass";
    private static final String URL_PAY_CHECK = "url.pc";
    private static final String URL_PAY = "url.p";
    private static final String URL_PAY_STATUS = "url.ps";
    private static final String SD = "SD";
    private static final String AP = "AP";
    private static final String OP = "OP";

    private static final String ENC="windows-1251";

    private IPrivKey sec=null;
    private IPrivKey pub=null;

    protected abstract String getGateName();

//    static
//    {
//        System.load("D:\\SVBILL\\repository\\security\\cyberplat\\win32\\thread-safe\\jnipriv.dll");
//        //System.loadLibrary("jnipriv");
//    }

    protected void init0() throws InitException
    {
        properties = GateContext.getInstance().getDatastreamProcessorConfig(getGateName());
        int initHandlerCount = Integer.parseInt(properties.getProperty(PROP_INIT_HANDLER_COUNT, "1"));
        int maxHandlerCount = Integer.parseInt(properties.getProperty(PROP_MAX_HANDLER_COUNT, "5"));
        messageQueue = new ActionQueue(initHandlerCount, maxHandlerCount);
        access = obtainBillingEngineAccess();
        //codeMapping = new CodeMapping();
        //codeMapping.init();

        IPriv.setCodePage(ENC);
        try {
            sec=IPriv.openSecretKey(properties.getProperty(SECRET_KEY),properties.getProperty(SECRET_KEY_PASS));
            pub=IPriv.openPublicKey(properties.getProperty(PUBLIC_KEY),Integer.parseInt(properties.getProperty(PUBLIC_KEY_PASS)));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected void shutdown0(final long timeout)
    {
        logger.info("Start Cyberplat Gate Shutdown");
        if(messageQueue.actionCount() != 0){
            Runtime.getRuntime().addShutdownHook(new Thread(){
                public void run(){
                    try{
                        long lt = System.currentTimeMillis();
                        while(messageQueue.actionCount() != 0 &&
                                ((System.currentTimeMillis() - lt) < timeout)){
                            sleep(1000);
                        }
                    }
                    catch(Throwable e){
                        e.printStackTrace();
                    }
                }
            });
        }
        logger.info("Cyberplat Gate shutdown");
    }

    public void enqueueBillMessage(BillingMessage billMessage)
    {
        CyberplatGateTask task = new CyberplatGateTask(billMessage, this);
        messageQueue.enqueue(task);
    }

    public String getBalance()     // ???? проверить правильность работы        // переделать
    {
        // variant 1
        String balance = "";

        String req=
                "SD="+properties.getProperty(SD)+"\r\n"+
                "AP="+properties.getProperty(AP)+"\r\n"+
                "OP="+properties.getProperty(OP)+"\r\n"+
                "SESSION="+new Date().getTime()+"\r\n"+
                "NUMBER="+"380631010911"+"\r\n"+
                "AMOUNT="+"1.00"+"\r\n"+
                "REQ_TYPE=1"+"\r\n";
        try {
            String response = sendRequest(req, properties.getProperty(URL_PAY_CHECK + "_LifeExc"));


            if(response != null)
            {
                StringTokenizer stringTokenizer = new StringTokenizer(response, "\r\n", false);
                ArrayList<String> list = new ArrayList<String>();
                while(stringTokenizer.hasMoreTokens())
                {
                    list.add(stringTokenizer.nextToken());
                }

                for(String str : list)
                {
                    String[] tmp = new String[2];
                    tmp[0] = str.substring(0, str.indexOf('='));
                    tmp[1] = str.substring(str.indexOf('=')+1);

                    if(tmp[0].contains("REST")) { balance = tmp[1];}
                }
            }

        } catch (IPrivException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return balance;

        /*
        // variant 2
        CyberplatTransaction cyberplatTransaction = null;
        try {
            EntityManager entityManager = new EntityManager();

            CriterionWrapper criterions[] = new CriterionWrapper[] {
                Restrictions.eq("valid", 1)};

            cyberplatTransaction = (CyberplatTransaction) entityManager.RETRIEVE(CyberplatTransaction.class, null, criterions, new String[]{"restBeforeTransaction"});

        } catch (DatabaseException e) {
            logger.error("Error of load previousTransaction 1", e);
            e.printStackTrace();
        } catch (Exception ee) {
            logger.error("Error of load previousTransaction 2", ee);
            ee.printStackTrace();
        }

        return cyberplatTransaction.getRestBeforeTransaction();
        */
    }

    // ClientTransaction
    private void updateTransactionStatus(ClientTransaction clientTransaction, CyberplatTransaction cyberplatTransaction)
    {
        if(clientTransaction != null)
        {
            try {
                EntityManager entityManager = new EntityManager();

                String result = cyberplatTransaction.getResult();
                String error = cyberplatTransaction.getError();

                if(result.equals("3")) { clientTransaction.setStatus("1"); }
                else if(result.equals("7") && error.equals("0")) { clientTransaction.setStatus("0"); }
                else { clientTransaction.setStatus("201"); }

                entityManager.SAVE(clientTransaction);

            } catch (DatabaseException e) {
                logger.error("Error of load previousTransaction 1", e);
                e.printStackTrace();
            } catch (Exception ee) {
                logger.error("Error of load previousTransaction 2", ee);
                ee.printStackTrace();
            }
        }
    }
    // OperatorTransaction
    private void updateTransactionStatus(OperatorBalanceTransaction operatorTransaction, CyberplatTransaction cyberplatTransaction)
    {
        if(operatorTransaction != null)
        {
            try {
                EntityManager entityManager = new EntityManager();

                String result = cyberplatTransaction.getResult();
                String error = cyberplatTransaction.getError();

                if(result.equals("3")) { operatorTransaction.setStatus("1"); }
                else if(result.equals("7") && error.equals("0")) { operatorTransaction.setStatus("0"); }
                else { operatorTransaction.setStatus("201"); }

                entityManager.SAVE(operatorTransaction);

            } catch (DatabaseException e) {
                logger.error("Error of load previousTransaction 1", e);
                e.printStackTrace();
            } catch (Exception ee) {
                logger.error("Error of load previousTransaction 2", ee);
                ee.printStackTrace();
            }
        }

    }
    // SellerTransaction
    private void updateTransactionStatus(List<SellerBalanceTransaction> sellerTransactions, CyberplatTransaction cyberplatTransaction)
    {
        if(sellerTransactions != null)
        {
            for(SellerBalanceTransaction sellerTransaction : sellerTransactions)
            {
                if(sellerTransaction != null)
                {
                    try {
                        EntityManager entityManager = new EntityManager();

                        String result = cyberplatTransaction.getResult();
                        String error = cyberplatTransaction.getError();

                        if(result.equals("3")) { sellerTransaction.setStatus("1"); }
                        else if(result.equals("7") && error.equals("0")) { sellerTransaction.setStatus("0"); }
                        else { sellerTransaction.setStatus("201"); }

                        entityManager.SAVE(sellerTransaction);

                    } catch (DatabaseException e) {
                        logger.error("Error of load previousTransaction 1", e);
                        e.printStackTrace();
                    } catch (Exception ee) {
                        logger.error("Error of load previousTransaction 2", ee);
                        ee.printStackTrace();
                    }
                }
            }
        }
    }
    // TerminalTransaction
    private void updateTransactionStatus(TerminalBalanceTransaction terminalTransaction, CyberplatTransaction cyberplatTransaction)
    {
        if(terminalTransaction != null)
        {
            try {
                EntityManager entityManager = new EntityManager();

                String result = cyberplatTransaction.getResult();
                String error = cyberplatTransaction.getError();

                if(result.equals("3")) { terminalTransaction.setStatus("1"); }
                else if(result.equals("7") && error.equals("0")) { terminalTransaction.setStatus("0"); }
                else { terminalTransaction.setStatus("201"); }

                entityManager.SAVE(terminalTransaction);

            } catch (DatabaseException e) {
                logger.error("Error of load previousTransaction 1", e);
                e.printStackTrace();
            } catch (Exception ee) {
                logger.error("Error of load previousTransaction 2", ee);
                ee.printStackTrace();
            }
        }
    }

    public String getStatus(String num) throws HttpsRequestException, HttpsGateException     // доделать // переделать, доставать транзакцию и определять урл
    {
        String req = "SESSION="+num+"\r\n";

        CyberplatTransaction cyberplatTransaction = null;
        ClientTransaction clientTransaction = null;
        OperatorBalanceTransaction operatorTransaction = null;
        List<SellerBalanceTransaction> sellerTransactions = null;
        TerminalBalanceTransaction terminalTransaction = null;

        EntityManager entityManager = null;
        try {
            entityManager = new EntityManager();

            // добавить все типы транзакций
            CriterionWrapper criterions_1[] = new CriterionWrapper[]{Restrictions.eq("billTransactionNum", num)};
            CriterionWrapper criterions_2[] = new CriterionWrapper[]{Restrictions.eq("transactionGUID", num)};
            CriterionWrapper criterions_3[] = new CriterionWrapper[]{Restrictions.eq("transactionGUID", num)};
            CriterionWrapper criterions_4[] = new CriterionWrapper[]{Restrictions.eq("transactionGUID", num)};
            CriterionWrapper criterions_5[] = new CriterionWrapper[]{Restrictions.eq("transactionGUID", num)};

            cyberplatTransaction = (CyberplatTransaction) entityManager.RETRIEVE(CyberplatTransaction.class, null, criterions_1, null);
            clientTransaction = (ClientTransaction) entityManager.RETRIEVE(ClientTransaction.class, null, criterions_2, null);
            operatorTransaction = (OperatorBalanceTransaction) entityManager.RETRIEVE(OperatorBalanceTransaction.class, null, criterions_3, null);
            sellerTransactions = entityManager.RETRIEVE_ALL(SellerBalanceTransaction.class, criterions_4, null);
            terminalTransaction = (TerminalBalanceTransaction) entityManager.RETRIEVE(TerminalBalanceTransaction.class, null, criterions_5, null);

        } catch (DatabaseException e) {
            logger.error("Error of load previousTransaction 1", e);
            e.printStackTrace();
        } catch (Exception ee) {
            logger.error("Error of load previousTransaction 2", ee);
            ee.printStackTrace();
        }

        if(cyberplatTransaction != null)
        {
            cyberplatTransaction.setOperationType("pay_status");
            String response = null;
            try {
                String request = serializeRequest(cyberplatTransaction);
                response = sendRequest(request, properties.getProperty(URL_PAY_STATUS + "_" + cyberplatTransaction.getBillServiceCode()));

            } catch (IPrivException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            if(response != null)
            {
                parseResponse(cyberplatTransaction, response);

                try {
                    entityManager.SAVE(cyberplatTransaction);
                } catch (DatabaseException e) {
                    logger.error("Failed to pre-save Cyberplat transaction", e);
                }

                updateTransactionStatus(clientTransaction, cyberplatTransaction);
                updateTransactionStatus(operatorTransaction, cyberplatTransaction);
                updateTransactionStatus(sellerTransactions, cyberplatTransaction);
                updateTransactionStatus(terminalTransaction, cyberplatTransaction);
            }
            return cyberplatTransaction.getError();
        }
        else
        {
            return "Payment Not Found On Cyberplat Gate";
        }
    }

    public void runBillMessage(BillingMessage billingMessage) throws HttpsGateException
    {
        BillingMessageImpl respMessage = new BillingMessageImpl();
        respMessage.setAttributeString(BillingMessage.REQUEST_ID, billingMessage.getAttributeString(BillingMessage.REQUEST_ID));
        try{
            if(BillingMessage.O_REFILL_PAYMENT.equals(billingMessage.getOperationName()))
            {
                refillPayment(billingMessage, respMessage);
            }
            else
            {
                respMessage.setStatusCode(StatusDictionary.SUBMIT_TRANSACTION_ERROR);
                throw new HttpsGateException("Unsupported command", new String[]{billingMessage.getOperationName()});
            }
        } catch(Throwable e){
            if(respMessage.getAttributeString(BillingMessage.STATUS_CODE) == null)
                respMessage.setStatusCode(StatusDictionary.UNKNOWN_ERROR);
            respMessage.setAttributeString(BillingMessage.STATUS_MESSAGE, e.getLocalizedMessage());
        }
        finally{
            try{
                access.obtainConnection().processAsync(respMessage);
            }
            catch(Throwable e){
                logger.error(e.getLocalizedMessage(), e);
            }
        }
    }


    private void refillPayment(BillingMessage billingMessage, BillingMessageImpl respMessage) throws HttpsGateException {
        respMessage.setOperationName(BillingMessage.O_REFILL_PAYMENT_RESULT);

        EntityManager entityManager = new EntityManager();
        CyberplatTransaction cyberplatTransaction = new CyberplatTransaction();

        String account = billingMessage.getAttributeString(BillingMessage.ACCOUNT_NUMBER);

        if(billingMessage.getAttributeString(BillingMessage.SERVICE_CODE).equals("Life") ||
                billingMessage.getAttributeString(BillingMessage.SERVICE_CODE).equals("LifeExc"))
            account = "380" + account;

        if(account==null || account.isEmpty() || account.equals(""))
        {
            account = billingMessage.getAttributeString(BillingMessage.PAY_ACCOUNT_NUMBER);
            cyberplatTransaction.setBillServiceCode(billingMessage.getAttributeString(BillingMessage.SERVICE_CODE)+"_pAccount");
        }
        else
        {
            cyberplatTransaction.setBillServiceCode(billingMessage.getAttributeString(BillingMessage.SERVICE_CODE));
        }

        // account = "9999999999"; //  error
        //account = "8888888888"; //  ok

        cyberplatTransaction.setAccount(account);
        //cyberplatTransaction.setCyberplatServiceCode(codeMapping.getCyberplatServiceCode(billingMessage.getAttributeString(BillingMessage.SERVICE_CODE)));

        String amount = billingMessage.getAttributeString(BillingMessage.AMOUNT).replace(',','.');
        if(!amount.contains("."))
        {
            amount = amount + ".00";
        }
        else
        {
            String post = amount.substring(amount.indexOf(".")+1);
            if(post.length()==1)
            {
                amount = amount + "0";
            }
        }
        //cyberplatTransaction.setAmount(billingMessage.getAttributeString(BillingMessage.AMOUNT).replace(',','.'));
        cyberplatTransaction.setAmount(amount);
        cyberplatTransaction.setBillTransactionNum(billingMessage.getAttributeString(BillingMessage.BILL_TRANSACTION_NUM));
        cyberplatTransaction.setGateName(CyberplatGate.GATE_NAME);
        cyberplatTransaction.setRequestTime(new Date());
        cyberplatTransaction.setTerminalId(billingMessage.getAttributeString(BillingMessage.TERMINAL_SN));
        cyberplatTransaction.setOperationType("pay_check");

        try {
            entityManager.SAVE(cyberplatTransaction);
        } catch (DatabaseException e) {
            logger.error("Failed to pre-save Cyberplat transaction", e);
        }

        String response = null;
        String request = null;
        
        try {
            // 1. Pay_check
            request = serializeRequest(cyberplatTransaction);

            //response = sendRequest(request, properties.getProperty(URL_PAY_CHECK));
            String url_pc = properties.getProperty(URL_PAY_CHECK + "_" + cyberplatTransaction.getBillServiceCode());
            if(url_pc == null)
            {
                respMessage.setStatusCode(StatusDictionary.BEGIN_TRANSACTION_ERROR);
                throw new HttpsGateException("Url error", new String[]{billingMessage.getOperationName()});
            }
            response = sendRequest(request, url_pc);


            if(response != null)
            {
                logger.info("Response_1: pay_check: " + "BillNumber=" + cyberplatTransaction.getBillTransactionNum() + "\r\n" + response);
                parseResponse(cyberplatTransaction, response);

                try {
                    entityManager.SAVE(cyberplatTransaction);
                } catch (DatabaseException e) {
                    logger.error("Failed to pre-save Cyberplat transaction", e);
                }

                response = null;
                request = null;

                if(isOk(cyberplatTransaction))
                {
                    // 2. Pay
                    cyberplatTransaction.setOperationType("pay");

                    request = serializeRequest(cyberplatTransaction);
                    //response = sendRequest(request, properties.getProperty(URL_PAY));
                    String url = properties.getProperty(URL_PAY + "_" + cyberplatTransaction.getBillServiceCode());
                    if(url == null)
                    {
                        respMessage.setStatusCode(StatusDictionary.BEGIN_TRANSACTION_ERROR);
                        throw new HttpsGateException("Url error", new String[]{billingMessage.getOperationName()});
                    }
                    response = sendRequest(request, url);

                    if(response != null)
                    {
                        logger.info("Response_2: pay: " + "BillNumber=" + cyberplatTransaction.getBillTransactionNum() + "\r\n" + response);
                        parseResponse(cyberplatTransaction, response);

                        try {
                            entityManager.SAVE(cyberplatTransaction);
                        } catch (DatabaseException e) {
                            logger.error("Failed to pre-save Cyberplat transaction", e);
                        }

                        response = null;
                        request = null;

                        while(!isEnded(cyberplatTransaction))
                        {
                            // 3. Pay_status
                            cyberplatTransaction.setOperationType("pay_status");
                            request = serializeRequest(cyberplatTransaction);

                            //response = sendRequest(request, properties.getProperty(URL_PAY_STATUS));
                            String url_ps = properties.getProperty(URL_PAY_STATUS + "_" + cyberplatTransaction.getBillServiceCode());
                            if(url_ps == null)
                            {
                                respMessage.setStatusCode(StatusDictionary.BEGIN_TRANSACTION_ERROR);
                                throw new HttpsGateException("Url error", new String[]{billingMessage.getOperationName()});
                            }
                            response = sendRequest(request, url_ps);

                            if(response != null)
                            {
                                logger.info("Response_3: pay_status: " + "BillNumber=" + cyberplatTransaction.getBillTransactionNum() + "\r\n" + response);
                                parseResponse(cyberplatTransaction, response);
                                response = null;
                                request = null;
                            }
                            else // response3 == null
                            {
                                respMessage.setAttributeString(BillingMessage.STATUS_CODE, StatusDictionary.SUBMIT_TRANSACTION_ERROR + "");
                                cyberplatTransaction.setErrmsg("not response");
                                respMessage.setStatusCode(StatusDictionary.BEGIN_TRANSACTION_ERROR);
                                logger.info("Response_3: pay_status: " + "BillNumber=" + cyberplatTransaction.getBillTransactionNum() + " - " + "not response");
                            }
                        }

                        try {
                            entityManager.SAVE(cyberplatTransaction);
                        } catch (DatabaseException e) {
                            logger.error("Failed to pre-save Cyberplat transaction", e);
                        }

                        if(isOk(cyberplatTransaction))
                        {
                            respMessage.setStatusCode(StatusDictionary.STATUS_OK);
                        }
                        else
                        {
                            respMessage.setAttributeString(BillingMessage.STATUS_CODE, StatusDictionary.SUBMIT_TRANSACTION_ERROR + "");
                            respMessage.setStatusCode(StatusDictionary.BEGIN_TRANSACTION_ERROR);
                        }
                    }
                    else // response2 == null
                    {
                        respMessage.setAttributeString(BillingMessage.STATUS_CODE, StatusDictionary.SUBMIT_TRANSACTION_ERROR + "");
                        cyberplatTransaction.setErrmsg("not response");
                        respMessage.setStatusCode(StatusDictionary.BEGIN_TRANSACTION_ERROR);
                        logger.info("Response_2: pay: " + "BillNumber=" + cyberplatTransaction.getBillTransactionNum() + " - " + "not response");
                    }

                }
                else
                {
                    respMessage.setAttributeString(BillingMessage.STATUS_CODE, StatusDictionary.SUBMIT_TRANSACTION_ERROR + "");
                    respMessage.setStatusCode(StatusDictionary.BEGIN_TRANSACTION_ERROR);
                }
            }
            else // response1 == null
            {
                respMessage.setAttributeString(BillingMessage.STATUS_CODE, StatusDictionary.SUBMIT_TRANSACTION_ERROR + "");
                cyberplatTransaction.setErrmsg("not response");
                respMessage.setStatusCode(StatusDictionary.BEGIN_TRANSACTION_ERROR);
                logger.info("Response_1: pay_check: " + "BillNumber=" + cyberplatTransaction.getBillTransactionNum() + " - " + "not response");
            }
        } catch (IPrivException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                entityManager.SAVE(cyberplatTransaction);
            } catch (DatabaseException e) {
                logger.error("Failed to pre-save Cyberplat transaction", e);
            }
        }

    }

    private boolean isOk(CyberplatTransaction cyberplatTransaction)
    {
        if(cyberplatTransaction.getOperationType().equals("pay_status"))
        {
            if(cyberplatTransaction.getResult().equals("7") && cyberplatTransaction.getError().equals("0"))
                return true;
            else
                return false;
        }
        else // pay_check & pay
        {
            if(cyberplatTransaction.getResult().equals("0") && cyberplatTransaction.getError().equals("0"))
                return true;
            else
                return false;
        }
    }

    private boolean isEnded(CyberplatTransaction cyberplatTransaction)
    {
        if(cyberplatTransaction.getResult().equals("0"))
            return false;
        else if(1 < Integer.parseInt(cyberplatTransaction.getResult()) && Integer.parseInt(cyberplatTransaction.getResult()) < 7)
            return false;
        else 
            return true;
    }

    private String serializeRequest(CyberplatTransaction cyberplatTransaction)
    {
        String req= "";
        if(cyberplatTransaction.getOperationType().equals("pay_check"))
        {
            req=
                "SD="+properties.getProperty(SD)+"\r\n"+
                "AP="+properties.getProperty(AP)+"\r\n"+
                "OP="+properties.getProperty(OP)+"\r\n"+
                "SESSION="+cyberplatTransaction.getBillTransactionNum()+"\r\n"+
                "NUMBER="+cyberplatTransaction.getAccount()+"\r\n"+
                "AMOUNT="+cyberplatTransaction.getAmount()+"\r\n";
            logger.info("Request_1: pay_check: " + "BillNumber=" + cyberplatTransaction.getBillTransactionNum() + "\r\n" + req);
        }
        else if(cyberplatTransaction.getOperationType().equals("pay"))
        {
            req=
                "SD="+properties.getProperty(SD)+"\r\n"+
                "AP="+properties.getProperty(AP)+"\r\n"+
                "OP="+properties.getProperty(OP)+"\r\n"+
                "SESSION="+cyberplatTransaction.getBillTransactionNum()+"\r\n"+
                "NUMBER="+cyberplatTransaction.getAccount()+"\r\n"+
                "AMOUNT="+cyberplatTransaction.getAmount()+"\r\n";
            logger.info("Request_2: pay: " + "BillNumber=" + cyberplatTransaction.getBillTransactionNum() + "\r\n" + req);
        }
        else if(cyberplatTransaction.getOperationType().equals("pay_status"))
        {
            req=
                "SESSION="+cyberplatTransaction.getBillTransactionNum()+"\r\n";
            logger.info("Request_3: pay_status: " + "BillNumber=" + cyberplatTransaction.getBillTransactionNum() + "\r\n" + req);
        }


        return req;
    }


    private String sendRequest(String request, String url) throws IPrivException,IOException
    {
        /* encrypt request */
        request="inputmessage=" + URLEncoder.encode(sec.signText(request));

        /* connect to server */
        URL u=new URL(url);
        URLConnection con=u.openConnection();
        con.setDoOutput(true);
        con.connect();

        /* send request */
        con.getOutputStream().write(request.getBytes());

        /* read response */
        BufferedReader in=new BufferedReader(new InputStreamReader(con.getInputStream(),ENC));
        char[] raw_resp=new char[1024];
        int raw_resp_len=in.read(raw_resp);
        StringBuffer s=new StringBuffer();
        s.append(raw_resp,0,raw_resp_len);
        String resp=s.toString();

        /* check server signature */
        resp=pub.verifyText(resp);

        return resp;
    }

    private void parseResponse(CyberplatTransaction cyberplatTransaction, String response)
    {
        StringTokenizer stringTokenizer = new StringTokenizer(response, "\r\n", false);
        ArrayList<String> list = new ArrayList<String>();
        while(stringTokenizer.hasMoreTokens())
        {
            list.add(stringTokenizer.nextToken());
        }

        for(String str : list)
        {
            String[] tmp = new String[2];
            tmp[0] = str.substring(0, str.indexOf('='));
            tmp[1] = str.substring(str.indexOf('=')+1);

            if(tmp[0].contains("ERROR")) {cyberplatTransaction.setError(tmp[1]);}
            else if(tmp[0].contains("RESULT")) {cyberplatTransaction.setResult(tmp[1]);}
            else if(tmp[0].contains("ERRMSG")) {cyberplatTransaction.setErrmsg(tmp[1]);}
            else if(tmp[0].contains("TRANSID")) {cyberplatTransaction.setCyberplatTransactionNum(tmp[1]);}
            else if(tmp[0].contains("REST")) {cyberplatTransaction.setRestBeforeTransaction(tmp[1]);}
        }

        cyberplatTransaction.setResponseTime(new Date());
    }


    /*
    private class CodeMapping
    {
        private Map<String,String> articleMapping;

        private void init() throws InitException
        {
            articleMapping = new HashMap<String,String>();
            try {
                logger.debug("Start initialization of the Cyberplat mapping");
                String cyberplatMappingFile = Configuration.getInstance().getProperty("cyberplat.mapping_file");
                if (cyberplatMappingFile == null)
                    throw new InitException("err.cyberplat_gate.mapping_file_is_not_specified_in_config");

                File configFile = new File(cyberplatMappingFile);
                Digester d = new Digester();
                d.push(this);

                d.addCallMethod("cyberplatMapping/article", "addArticleMapping", 2);
                d.addCallParam("cyberplatMapping/article", 0, "svbillCode");
                d.addCallParam("cyberplatMapping/article", 1, "cyberplatCode");
                d.parse(configFile);

                logger.debug("Successful completed initialization of the Cyberplat mapping");
                
            } catch (Exception e){
                throw new InitException("Failed to init Cyberplat gate mapping", e);
            }
        }

        public void addArticleMapping(String billServiceCode, String cyberplatServiceCode)
        {
            articleMapping.put(billServiceCode, cyberplatServiceCode);
        }

        private String getCyberplatServiceCode(String billServiceCode)
        {
            return articleMapping.get(billServiceCode);
        }
    }
    */

}
