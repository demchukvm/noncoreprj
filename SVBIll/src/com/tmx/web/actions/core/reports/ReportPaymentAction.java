package com.tmx.web.actions.core.reports;

import com.tmx.web.forms.core.reports.ReportPaymentForm;
import com.tmx.web.forms.core.base.AbstractFormFactory;
import com.tmx.web.actions.core.AbstractSvbillAction;
import com.tmx.web.controls.table.FilterSet;
import com.tmx.web.controls.ScrollBox;
import com.tmx.web.controls.TimeSelector;
import com.tmx.as.blogic.SellerManager;
import com.tmx.as.blogic.TransactionManager;
import com.tmx.as.blogic.balance.SellerBalanceManager;
import com.tmx.as.base.FilterWrapper;
import com.tmx.as.base.criterion.CriterionWrapper;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForm;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.*;

public class ReportPaymentAction extends AbstractSvbillAction {

    private static final String FORWARD = "page";
    
    public ActionForward defaultAction(ActionMapping mapping,
                                       ActionForm form,
                                       HttpServletRequest request,
                                       HttpServletResponse response,
                                       String methodName) throws Throwable {
        ReportPaymentForm reportPaymentForm = (ReportPaymentForm) form;
        reportPaymentForm.validate(mapping.findForward(FORWARD));
        reportPaymentForm.getTable().setRefreshActionName(getName());
        FilterSet filterSet = reportPaymentForm.getTable().getFilterSet();
        ScrollBox scrollBoxTransType = (ScrollBox)filterSet.getFilter("by_transaction_type").getParameter("type").getControl();
        scrollBoxTransType.bind(new TransactionManager().getAllTransactionType());

        setUpTimeSelectors(reportPaymentForm);
        //Methods bind data to scrollBoxes and sets filters for ReportPaymentForm
        AbstractFormFactory.getInstance(getLoggedInUser(request)).setUpForm(reportPaymentForm);

        reportPaymentForm.getTable().refresh();
        
        FilterWrapper[] filters = reportPaymentForm.getTable().getQuery().getFilters();
        CriterionWrapper [] criterionWrappers = reportPaymentForm.getTable().getQuery().getCriterions(); 

        List summaryAmountList = new SellerBalanceManager().getSummaryAmount(filters, criterionWrappers);
            
        if (summaryAmountList .size() > 0) {
            if (summaryAmountList.get(0) != null) {
                reportPaymentForm.getEditBoxSummaryAmount().setValue(summaryAmountList.get(0).toString());
            } else {
                reportPaymentForm.getEditBoxSummaryAmount().setValue("0");
            }
        }
            
//        reportPaymentForm.getEditBoxSummaryAmount().setValue(
//                );   

        return mapping.findForward(FORWARD);
    }

    private void setUpTimeSelectors(ReportPaymentForm form) {
        GregorianCalendar  calendar = new GregorianCalendar();
        Date timeTo = calendar.getTime();

        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        Date timeFrom = calendar.getTime();

        FilterSet filterSet = form.getTable().getFilterSet();
        TimeSelector tsFrom = (TimeSelector)filterSet.getFilter("by_FromDate").getParameter("date").getControl();
        if (!tsFrom.isUpdated()) {
            tsFrom.setTime(timeFrom);
        }
        TimeSelector tsTo = (TimeSelector)filterSet.getFilter("by_ToDate").getParameter("date").getControl();
        if (tsTo.isUpdated()) {
            tsTo.setTime(timeTo);
        }

    }

    public ActionForward changeSellersScrollBox(ActionMapping mapping,
                                       ActionForm form,
                                       HttpServletRequest request,
                                       HttpServletResponse response) throws Throwable {
        ReportPaymentForm paymentForm = (ReportPaymentForm) form;
        paymentForm.validate(mapping.findForward(FORWARD));

        List subSellerList = Collections.EMPTY_LIST;
        if(!paymentForm.getSellersScrollBox().isNullKeySelected()) {
            subSellerList = new SellerManager().getNestedSellersInSeller(
                                              new Long(paymentForm.getSellersScrollBox().getSelectedKey()), true);
        }
        paymentForm.getSubSellersScrollBox().setSelectedKey(null);
//        paymentForm.getSubSellersScrollBox().bind(subSellerList);
        paymentForm.bindSubSellersScrollBox(subSellerList);
        
//        paymentForm.bindFilters();
//        paymentForm.getTable().refresh();
        return mapping.findForward(FORWARD);
    }

    
}
