package com.tmx.beng.base.processors;

import com.tmx.beng.base.*;
import com.tmx.beng.base.cache.DBCache;
import com.tmx.as.entities.bill.transaction.ClientTransaction;
import com.tmx.as.entities.mobipay.kyivstar.MobipayKyivstar;
import com.tmx.as.entities.mobipay.beeline.Beeline;
import com.tmx.as.base.EntityManager;
import com.tmx.as.base.FilterWrapper;
import com.tmx.as.exceptions.DatabaseException;

import java.util.Date;

/**
 * Identify request status processor more deeply than super class. Using operators gates
 */
public class IdentifyStatusExtendedProcessor extends IdentifyStatusProcessor {

    protected Status identifyTxStatus(Task task, DBCache dbCache, BillingMessageWritable msg, boolean client) throws BillException {
        FilterWrapper byTransactionNum;
        String transactionNum;
        Status status = new Status();

        if (client) {
            transactionNum = msg.getAttributeString(BillingMessage.CLI_TRANSACTION_NUM);
            byTransactionNum = new FilterWrapper("by_clinum");
            byTransactionNum.setParameter("clinum", transactionNum);
        } else {
            transactionNum = msg.getAttributeString(BillingMessage.BILL_TRANSACTION_NUM);
            byTransactionNum = new FilterWrapper("by_billnum");
            byTransactionNum.setParameter("billnum", transactionNum);
        }

        try {
            EntityManager entitityManager = new EntityManager();

            // get client transaction
            ClientTransaction cliTx = (ClientTransaction) entitityManager.
                    RETRIEVE(ClientTransaction.class, new FilterWrapper[]{byTransactionNum}, new String[]{"error"});

            if (cliTx == null) {
                msg.setStatusCode(StatusDictionary.IDENTIFY_STATUS_ERROR);
                throw new BillException("err.identify_status_proc.transaction_not_found", new String[]{transactionNum});
            }
            String stutusCodeStr = cliTx.getStatus();
            status.setStatusCode((stutusCodeStr != null) ? Integer.parseInt(stutusCodeStr) : StatusDictionary.UNKNOWN_STATUS);
            if (cliTx.getError() != null) {
                status.setStatusDescription(cliTx.getError().getMessage());
            }
            status.setRegistrationTime(cliTx.getTransactionTime());
            status.setAmount(cliTx.getAmount());
//            task.getGlobalContext().getMediumAccessPool().getMediumAccess()
//            try {
//                // try to get info from MobipayKyivstar
//                FilterWrapper by_billnum = new FilterWrapper("mobipay_by_billnum");
//                by_billnum.setParameter("billnum", cliTx.getTransactionGUID());
//                MobipayKyivstar kyivstar = (MobipayKyivstar) entitityManager
//                        .RETRIEVE(MobipayKyivstar.class, new FilterWrapper[]{by_billnum}, null);
//                if (kyivstar != null) {
//                    status.setPayId(kyivstar.getPayId());
//                    status.setReceiptNum(kyivstar.getReceiptNum());
//                } else {
//                    // try to get from beeline
//                    Beeline beeline = (Beeline) entitityManager
//                            .RETRIEVE(Beeline.class, new FilterWrapper[]{by_billnum}, null);
//                    if (beeline != null) {
//                        status.setPayId(beeline.getPayId());
//                        status.setReceiptNum(beeline.getReceiptNum());
//                    }
//                }
//            } catch (Exception e) {
//                logger.error("err.identify_status_proc.igored_error", e);
//                //ignore
//            }

        } catch (DatabaseException e) {
            msg.setStatusCode(StatusDictionary.IDENTIFY_STATUS_ERROR);
            throw new BillException("err.identify_status_proc.error", e);
        }
        return status;
    }

}