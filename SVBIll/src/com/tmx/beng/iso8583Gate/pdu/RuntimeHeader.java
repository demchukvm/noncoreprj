package com.tmx.beng.iso8583Gate.pdu;

import com.tmx.beng.iso8583Gate.pdu.util.HexConverter;

/**
 */
public class RuntimeHeader {
    private HeaderFormat format;
    private int length, id, destinationAddress, originatorAddress;

    RuntimeHeader(HeaderFormat hf){
        this.format = hf;
    }

    public HeaderFormat getFormat() {
        return format;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getDestinationAddress() {
        return destinationAddress;
    }

    public void setDestinationAddress(int destinationAddress) {
        this.destinationAddress = destinationAddress;
    }

    public int getOriginatorAddress() {
        return originatorAddress;
    }

    public void setOriginatorAddress(int originatorAddress) {
        this.originatorAddress = originatorAddress;
    }

    public String dumpHeader(){
        StringBuffer dump = new StringBuffer();
        String serializedHeader = null;
        try{
            serializedHeader = new HexConverter().getStringFromHex(format.serialize(this));
        }
        catch(IsoSerializeException e){
            serializedHeader = "(ERROR: "+e.toString()+")";
        }
        dump.append("MsgLen=").append(length).append(", ").
            append("Id=").append(id).append(", ").
            append("DestinationAddress=").append(destinationAddress).append(", ").
            append("OriginatorAddress=").append(originatorAddress).append(", ").
            append("[IsoValue=").append(serializedHeader).append("]");
        return dump.toString();
    }

    public String dumpHexHeader(){
        String serializedHeader = null;
        try{
            serializedHeader = new HexConverter().getStringFromHex(format.serialize(this));
        }
        catch(IsoSerializeException e){
            serializedHeader = "(ERROR: "+e.toString()+")";
        }
        return serializedHeader;        
    }

}
