package com.tmx.engines.reportengine.aggrtable.validation;

import com.tmx.engines.reportengine.aggrtable.DataElement;

import java.util.Map;

/**
 */
public abstract class SimpleValidationRule extends BasicValidationRule{

    public DataValidationError validate(Map metacubePoint){
        return validate((DataElement)metacubePoint.get(getOwnerFactName()));
    }

    public abstract DataValidationError validate(DataElement data);
}
