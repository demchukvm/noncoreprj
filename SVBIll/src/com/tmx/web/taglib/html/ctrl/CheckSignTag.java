package com.tmx.web.taglib.html.ctrl;

import com.tmx.web.base.ApplicationEnvironment;
import com.tmx.web.controls.CheckBox;
import com.tmx.web.taglib.base.AbstractSupportTag;

import javax.servlet.jsp.JspException;

import org.apache.struts.taglib.TagUtils;

public class CheckSignTag extends AbstractSupportTag {
    private String value=null;
    private String checkedImg=null;
    private String uncheckedImg=null;

    public int doStartTmxTag() throws JspException {
        String checked="0";
        Object object = getPropertyObject();
        if(value!=null){
            checked=value;
        }else{
            if(object!=null){
                if(object instanceof CheckBox){
                    checked=""+((CheckBox)object).isSelected();
                }else{
                    checked=object.toString();
                }            
            }
        }
        String imageName = null;
        if (
            checked.equalsIgnoreCase("true")||
            checked.equalsIgnoreCase("1")||
            checked.equalsIgnoreCase("yes")
           ) {
            imageName = getCheckedImg();
        } else {
            imageName = getUncheckedImg();
        }
        TagUtils.getInstance().write(pageContext, getImageTage(imageName));
        return EVAL_BODY_TAG;
    }

    private String getImageTage(String imageName) {
        if (imageName == null) return "";
        StringBuffer imageTag = new StringBuffer();
        imageTag.append("<img src=\"");
        imageTag.append(imageName);
        imageTag.append("\" border=\"0\">");
        return imageTag.toString();
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getCheckedImg() {
        return (checkedImg != null) ? checkedImg : ApplicationEnvironment.getAppDeploymentName() + getDefaultProperty("tag.checkSign.img.checkedImg");
    }

    public void setCheckedImg(String checkedImg) {
        this.checkedImg = (checkedImg != null && checkedImg.startsWith("/") && !checkedImg.startsWith(ApplicationEnvironment.getAppDeploymentName()) ? ApplicationEnvironment.getAppDeploymentName() : "") + checkedImg;
    }

    public String getUncheckedImg() {
        return (uncheckedImg != null) ? uncheckedImg : ApplicationEnvironment.getAppDeploymentName() + getDefaultProperty("tag.checkSign.img.uncheckedImg");
    }

    public void setUncheckedImg(String uncheckedImg) {
        this.uncheckedImg = (uncheckedImg != null && uncheckedImg.startsWith("/") && !uncheckedImg.startsWith(ApplicationEnvironment.getAppDeploymentName()) ? ApplicationEnvironment.getAppDeploymentName() : "") + uncheckedImg;
    }
}
