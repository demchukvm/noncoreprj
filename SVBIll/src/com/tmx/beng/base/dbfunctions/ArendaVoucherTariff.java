package com.tmx.beng.base.dbfunctions;

import com.tmx.as.entities.bill.transaction.TransactionType;
import com.tmx.beng.base.BillingMessage;
import com.tmx.beng.base.Reservation;
import com.tmx.beng.base.cache.InvokeFunctionException;
import java.util.Map;
public class ArendaVoucherTariff extends BasicTariffFunction {
    public static final String[] VoucherNames = {
            "BeelineVoucher5", "BeelineVoucher15", "BeelineVoucher30", "BeelineVoucher60", "BeelineVoucher90",
            "KyivstarVoucher25", "KyivstarVoucher50", "KyivstarVoucher100", "KyivstarVoucher300",
            "LifeVoucher10", "LifeVoucher25", "LifeVoucher50", "LifeVoucher100",
            "UmcVoucher10", "UmcVoucher25", "UmcVoucher50", "UmcVoucher100", "UmcVoucher200"
    };
    public static final Double [] VoucherValues = {
            4.91, 14.73, 29.46, 58.92, 88.38,
            24.30, 48.60, 97.20, 291.60,
            9.66, 23.80, 47.60, 95.20,
            9.72, 24.30, 48.60, 97.20, 194.40
    };
    public static final Double [] BeelineVal = {};

    boolean isTariffApplicable(Map actualParams) {
        return BillingMessage.O_REFILL_PAYMENT.equals(task.getProcesedMessage().getOperationName());
    }

    void applyTariff(Map actualParams, Reservation reservation) throws InvokeFunctionException {
        try{
            final String voucherNominal = task.getProcesedMessage().getAttributeString(BillingMessage.VOUCHER_NOMINAL);
            double price = Double.parseDouble(String.valueOf(actualParams.get(voucherNominal)));
            //prepare reservation on primary tariff
            reservation.reserve(null, price, null, TransactionType.CODE_CREDIT);
        }
        catch(Throwable e){
            throw new InvokeFunctionException("err.tariff.calc_error", e);
        }
    }

    public BasicFunction.TariffParameter[] getDefaultParams() {
        BasicFunction.TariffParameter[] result = new BasicFunction.TariffParameter[18];
        for (int i=0; i<18; i++){
            BasicFunction.TariffParameter paramCost = new BasicFunction.TariffParameter();
            paramCost.setName(VoucherNames[i]);
            paramCost.setValue(VoucherValues[i].toString());
            paramCost.setOrderNum(new Long(i));
            paramCost.setMandatory(Boolean.FALSE);

            result[i] = paramCost;
        }

        return result;
    }

}
