package com.tmx.web.actions.core.dictionary.seller;

import com.tmx.web.actions.core.AbstractSvbillAction;
import com.tmx.web.controls.table.Table;
import com.tmx.web.forms.core.base.AbstractFormFactory;
import com.tmx.web.forms.core.dictionary.seller.ManageSellersForm;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class ManageSellersAction extends AbstractSvbillAction {

    private static final String FORWARD_MANAGE_SELLERS_PAGE = "manageSellersPage";

    public ActionForward defaultAction(ActionMapping mapping,
                                       ActionForm form,
                                       HttpServletRequest request,
                                       HttpServletResponse response,
                                       String methodName) throws Throwable {
        ManageSellersForm manageSellerForm = (ManageSellersForm) verifyForm(form, ManageSellersForm.class);
        manageSellerForm.bindData( getLoggedInUser(request) );
        return mapping.findForward(FORWARD_MANAGE_SELLERS_PAGE);
    }

    public ActionForward refreshTable(ActionMapping mapping,
                                  ActionForm form,
                                  HttpServletRequest request,
                                  HttpServletResponse response) throws Throwable {
        ManageSellersForm manageSellerForm = (ManageSellersForm) verifyForm(form, ManageSellersForm.class);
        manageSellerForm.getSellersTable().refresh();
        return mapping.findForward(FORWARD_MANAGE_SELLERS_PAGE);
    }

}
