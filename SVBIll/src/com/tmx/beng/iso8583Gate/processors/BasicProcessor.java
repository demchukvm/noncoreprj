package com.tmx.beng.iso8583Gate.processors;

import com.tmx.beng.iso8583Gate.pdu.RuntimeMessage;
import com.tmx.beng.iso8583Gate.pdu.RuntimeDataElement;
import com.tmx.beng.iso8583Gate.pdu.DataElementFormat;
import com.tmx.beng.iso8583Gate.pdu.IncorrectFormatException;
import com.tmx.beng.iso8583Gate.ISO8583Service;
import com.tmx.beng.iso8583Gate.mac.MacException;
import com.tmx.beng.iso8583Gate.mapper.BillingMapper;
import com.tmx.beng.access.Access;
import com.tmx.beng.access.Connection;
import com.tmx.beng.base.BillException;
import com.tmx.util.StructurizedException;
import com.tmx.util.InitException;
import com.tmx.as.entities.iso8583.transaction.Iso8583Transaction;
import com.tmx.as.exceptions.DatabaseException;
import com.tmx.as.base.EntityManager;
import com.tmx.as.base.FilterWrapper;
import com.tmx.as.modules.SpringModule;

import java.util.Arrays;
import java.util.List;
import java.util.Date;

import org.apache.log4j.Logger;

/**
 */
public abstract class BasicProcessor implements ISOProcessor{
    protected Logger logger = ISO8583Service.getLogger(this.getClass());
    private Connection connection;

    public abstract void processSpecificTask(RuntimeMessage reqMsg, RuntimeMessage respMsg)
            throws ProcessingException, BillException, IncorrectFormatException, InitException, DatabaseException, MacException;

    /** Returns array of bitnums of elements to copy from request to response */
    public abstract int[] getDEToCopy();

    public abstract int getMessageTypeId();

    public abstract int getProcessingCode();

    public RuntimeMessage process(RuntimeMessage requestMsg) throws ProcessingException{
        RuntimeMessage responseMsg = prepareInitialRespMsg(requestMsg);
        //set message type id
        setMessageTypeId(responseMsg, getMessageTypeId());
        //set processing code
        setProcessingCode(responseMsg, getProcessingCode());
        //copy fields from request message
        copyDataElements(requestMsg, responseMsg, getDEToCopy());
        //set positive response before specific processing. It could be overriden below in specific processor
        setResponseCode(responseMsg, "00");
        //set local date time before specific processing. It could be overriden below in specific processor        
        setLocalDateTime(responseMsg, new Date());
        try{
            obtainBillingConnection();
            processSpecificTask(requestMsg, responseMsg);
            checkSuppressResponse(responseMsg);
        }
        catch(Throwable e){
            setResponseCode(responseMsg, BillingMapper.ERR_TX_UNKNOWN_ERROR);//error code = 06
            if(e instanceof StructurizedException)
                logger.error(((StructurizedException)e).getTraceDump());
            else
                logger.error(e.toString(), e);
            //throw new ProcessingException("err.failed_to_process_specific_task", e);
        }

        return responseMsg;
    }

    /** Creates new response message */
    private RuntimeMessage prepareInitialRespMsg(RuntimeMessage requestMessage){
        return new RuntimeMessage(requestMessage.getMessageCommonFormat());
    }

    /** copy required parameters from original message */
    private void copyDataElements(RuntimeMessage requestMessage, RuntimeMessage responseMessage, int[] bitmapToCopy){
        for(int i=0; i<bitmapToCopy.length; i++)
            responseMessage.copyRuntimeDataElement(requestMessage, bitmapToCopy[i]);
    }

    private void setMessageTypeId(RuntimeMessage msg, int messageTypeId){
        RuntimeDataElement messageCommonFormatRDE = new RuntimeDataElement(msg.getMessageCommonFormat().getDataElementFormat(DataElementFormat.DEF_MESSAGETYPEID));
        messageCommonFormatRDE.setValue(messageTypeId);
        msg.addRuntimeDataElement(messageCommonFormatRDE);
    }

    private void setProcessingCode(RuntimeMessage msg, int processingCode){
        RuntimeDataElement proccessingCodeRDE = new RuntimeDataElement(msg.getMessageCommonFormat().getDataElementFormat(DataElementFormat.DEF_PROCESSINGCODE));
        proccessingCodeRDE.setValue(processingCode);
        msg.addRuntimeDataElement(proccessingCodeRDE);
    }

    protected void setResponseCode(RuntimeMessage msg, String responseCode){
        RuntimeDataElement responseCodeRDE = new RuntimeDataElement(msg.getMessageCommonFormat().getDataElementFormat(DataElementFormat.DEF_RESPONSECODE));
        responseCodeRDE.setValue(responseCode);
        msg.addRuntimeDataElement(responseCodeRDE);
    }

    protected void setLocalDateTime(RuntimeMessage msg, Date serverTime){
        RuntimeDataElement localTimeRDE = new RuntimeDataElement(msg.getMessageCommonFormat().getDataElementFormat(12));
        localTimeRDE.setValue(serverTime);
        msg.addRuntimeDataElement(localTimeRDE);

        RuntimeDataElement localDateRDE = new RuntimeDataElement(msg.getMessageCommonFormat().getDataElementFormat(13));
        localDateRDE.setValue(serverTime);
        msg.addRuntimeDataElement(localDateRDE);
    }

    protected Connection getConnection(){
        return connection;
    }

    private void obtainBillingConnection() throws BillException {
        //connection = ISO8583Service.getInstance().getBillingAccess().obtainConnection();
        connection = (Connection) SpringModule.getApplicationContext().getBean("billingConnection");
    }

    private void checkSuppressResponse(RuntimeMessage responseMsg) throws ProcessingException{
        RuntimeDataElement rde = responseMsg.getRuntimeDataElement(39);
        if(rde == null)
            throw new ProcessingException("err.basic_processor.response_rde_is_null");
        if(BillingMapper.ERR_SUPRESS_RESPONSE.equals(rde.getString())){
            throw new ProcessingException("err.basic_processor.suppress_response");
        }
    }

}
