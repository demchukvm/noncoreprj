package com.tmx.beng.base.processors;

import com.tmx.beng.base.*;
import com.tmx.beng.base.cache.DBCache;
import com.tmx.as.entities.bill.request.Request;
import com.tmx.as.base.EntityManager;
import com.tmx.as.exceptions.DatabaseException;

import java.util.Date;

/**
 * Register message
 */
public class OpenRequestProcessor extends TaskProcessor {

    public Task process0(Task task, DBCache dbCache, BillingMessageWritable msg) throws BillException {
        logger.debug("Opening request...");

        //instantiate request id
        Transaction tx = task.getGlobalContext().getTransactionPool().openTransaction();
        msg.setAttributeString(BillingMessage.REQUEST_ID, tx.getTransactionId());
        logger.debug("OpenRequestProcessor : transaction id : " + tx.getTransactionId());
        tx.addTask(task);

        Request request = new Request();
        request.setBeginTime(new Date());
        request.setRequestNum(tx.getTransactionId());
        request.setRequestNum(msg.getAttributeString(BillingMessage.REQUEST_ID));
        request.setBillTransactionNum(msg.getAttributeString(BillingMessage.BILL_TRANSACTION_NUM));
        request.setCliTransactionNum(msg.getAttributeString(BillingMessage.CLI_TRANSACTION_NUM));
        request.setStatus(Request.SESSION_OPENED);

        try{
            new EntityManager().SAVE(request);
        }catch(DatabaseException e){
            throw new BillException("err.bill.open_request_error");
        }


        return task;
    }

    
    @Override
    public Task rollback(Task task, Throwable e) {

        logger.debug("+++++++Rollback OpenRequestProcessor++++++++");

        //remove tasks from transaction
        Transaction t = (Transaction) task.getGlobalContext().getTransactionPool().lookup(
                task.getProcesedMessage().getAttributeString(BillingMessage.REQUEST_ID));

        logger.debug("Rollback OpenRequestProcessor : " + t.getTasks().size());

        logger.debug("Rollback OpenRequestProcessor : transaction pool size : " + task.getGlobalContext().getTransactionPool().size());

        logger.debug("Rollback OpenRequestProcessor : transaction pool : " + task.getGlobalContext().getTransactionPool().toString());

        logger.debug("Rollback OpenRequestProcessor : request id : " + task.getProcesedMessage().getAttributeString(BillingMessage.REQUEST_ID));

        // close transaction
        task.getGlobalContext().getTransactionPool().closeTransaction(
                task.getProcesedMessage().getAttributeString(BillingMessage.REQUEST_ID));

        logger.debug("Rollback OpenRequestProcessor : transaction pool size : " + task.getGlobalContext().getTransactionPool().size());

        return super.rollback(task, e);
    }

}
