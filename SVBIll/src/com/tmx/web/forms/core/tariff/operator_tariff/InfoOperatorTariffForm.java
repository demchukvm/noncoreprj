package com.tmx.web.forms.core.tariff.operator_tariff;

import com.tmx.as.blogic.OperatorManager;
import com.tmx.as.entities.bill.operator.Operator;
import com.tmx.as.entities.bill.tariff.AbstractTariff;
import com.tmx.as.entities.bill.tariff.OperatorTariff;
import com.tmx.as.entities.general.user.User;
import com.tmx.as.exceptions.DatabaseException;
import com.tmx.web.controls.PopulateUtil;
import com.tmx.web.controls.ScrollBox;
import com.tmx.web.forms.core.tariff.AbstractInfoTariffForm;


public class InfoOperatorTariffForm extends AbstractInfoTariffForm {
    
    //maybe mandutory???
    private ScrollBox scrollBoxOperators;

    protected void init() {
        super.init();
        initButtons();
        initScrollBoxes();
        initParameterButtonsAction();
        initTables();
        initBrowseBoxes();
    }

    private void initTables(){
        if(getFuncParametrsTable() != null){
            getFuncParametrsTable().setRefreshActionName("/core/tariff/operator_tariff/infoOperatorTariff");
        }
    }

    private void initScrollBoxes(){
        scrollBoxOperators = new ScrollBox(){
            protected String getKey(Object dataListElement) {
                return ((Operator)dataListElement).getOperatorId().toString();
            }
            protected String getValue(Object dataListElement) {
                return ((Operator)dataListElement).getName();
            }
        };
    }

    private void initButtons(){
        getOnSaveButton().setAction("/core/tariff/operator_tariff/infoOperatorTariff");
        getOnApplyButton().setAction("/core/tariff/operator_tariff/infoOperatorTariff");
        getOnCancelButton().setAction("/core/tariff/operator_tariff/manageOperatorTariffs");

        getOnRefreshParamButton().setAction("/core/tariff/operator_tariff/infoOperatorTariff");
        getOnCreateParamButton().setAction("/core/tariff/operator_tariff/infoOperatorTariff");
        getOnEditParamButton().setAction("/core/tariff/operator_tariff/operatorTariffParameter");
        getOnDeleteParamButton().setAction("/core/tariff/operator_tariff/operatorTariffParameter");
    }

    private void initBrowseBoxes(){
        getBrowseBoxFunctionTypes().setAction("/core/tariff/operator_tariff/browseOperatorFuncTypesAction");
    }

    protected AbstractTariff populateFromSpecificControls() {
        OperatorTariff tariff = new OperatorTariff();
        tariff.setOperator(getOperatorInstance());
        return tariff;
    }

    private Operator getOperatorInstance() {
        Operator operator = new Operator();
        operator.setOperatorId(PopulateUtil.getLongKey(scrollBoxOperators));
        return operator;
    }


    protected void bindSpecificData(AbstractTariff abstractTariff, User loggedInUser) throws DatabaseException {
        OperatorTariff tariff = (OperatorTariff) abstractTariff;
        //------problem part-------------
        scrollBoxOperators.bind(new OperatorManager().getAllOperators());
        //--------------------------------
        if(tariff.getOperator()!=null && tariff.getOperator().getOperatorId()!=null)//if operator is present
            scrollBoxOperators.setSelectedKey(tariff.getOperator().getOperatorId().toString());

        scrollBoxOperators.setReadonly(true);
    }

    private void initParameterButtonsAction(){
        setOnApplyButtonAction("/core/tariff/operator_tariff/operatorTariffParameter");
        setOnCancelButtonAction("/core/tariff/operator_tariff/infoOperatorTariff");
        setOnSaveButtonAction("/core/tariff/operator_tariff/operatorTariffParameter");
    }

    protected void bindSpecificData(User loggedInUser) throws DatabaseException {
        scrollBoxOperators.bind(new OperatorManager().getAllOperators());
        scrollBoxOperators.setReadonly(false);
    }

    public ScrollBox getScrollBoxOperators() {
        return scrollBoxOperators;
    }

    public void setScrollBoxOperators(ScrollBox scrollBoxOperators) {
        this.scrollBoxOperators = scrollBoxOperators;
    }
}
