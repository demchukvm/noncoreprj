package com.tmx.util.jce.cipher;

import com.tmx.util.jce.JceException;

import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESKeySpec;
import java.io.FileOutputStream;
import java.io.File;


/**
 */
public class DESUtil extends AbstractCipherUtil{

    public String getCypherAlgoritmName(){
        return "DES";
    }

    public SecretKey readKey(byte[] rawKey) throws JceException {
        try{
            // Convert the raw bytes to a secret key like this
            DESKeySpec keyspec = new DESKeySpec(rawKey);

            SecretKeyFactory keyfactory = SecretKeyFactory.getInstance(getCypherAlgoritmName());
            return keyfactory.generateSecret(keyspec);
        }
        catch(Exception e){
            throw new JceException("err."+getCypherAlgoritmName()+"_util.read_key_failed", e);
        }
    }

    /**
     * Save the specified SecretKey to the specified file
     */
    public void writeKey(SecretKey key, File f) throws JceException {
        // Convert the secret key to an array of bytes like this
        try{
            SecretKeyFactory keyfactory = SecretKeyFactory.getInstance(getCypherAlgoritmName());
            DESKeySpec keyspec = (DESKeySpec) keyfactory.getKeySpec(key, DESKeySpec.class);
            byte[] rawkey = keyspec.getKey();

            // Write the raw key to the file
            FileOutputStream out = new FileOutputStream(f);
            out.write(rawkey);
            out.close();
        }
        catch(Exception e){
            throw new JceException("err."+getCypherAlgoritmName()+"_util.write_key_failed", e);
        }
    }

}
