package com.tmx.web.forms.graphengine;

import com.tmx.web.base.BasicActionForm;
import com.tmx.web.controls.CheckBox;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionErrors;

import javax.servlet.http.HttpServletRequest;

/**
 * Graph setup form
 */
public class GraphSetupForm extends BasicActionForm {
    private final int DEFAULT_APPROXIMATION_POINTS = 100;
    private CheckBox useApproximation = null;
    private int approximationPoints = DEFAULT_APPROXIMATION_POINTS;

    protected void init() {
        //initialze and register controls
        useApproximation = new CheckBox();
        useApproximation.setSelected(true);
        useApproximation.setLabel("");
    }

    public CheckBox getUseApproximation() {
        return useApproximation;
    }

    public void setUseApproximation(CheckBox useApproximation) {
        this.useApproximation = useApproximation;
    }

    public int getApproximationPoints() {
        return (approximationPoints <= 0) ? DEFAULT_APPROXIMATION_POINTS : approximationPoints;
    }

    public void setApproximationPoints(int approximationPoints) {
        this.approximationPoints = approximationPoints;
    }

//    public void reset(ActionMapping mapping, HttpServletRequest request){
//        super.reset(mapping, request);
//    }
}
