package com.tmx.web.forms.core.dictionary.terminal;

import com.tmx.as.blogic.SellerManager;
import com.tmx.as.blogic.TerminalManager;
import com.tmx.as.blogic.AddressManager;
import com.tmx.as.entities.bill.seller.Seller;
import com.tmx.as.entities.bill.terminal.Terminal;
import com.tmx.as.entities.bill.terminal.EquipmentType;
import com.tmx.as.entities.bill.trade_putlet.TradePutletType;
import com.tmx.as.entities.bill.trade_putlet.TradePutlet;
import com.tmx.as.entities.bill.person.Person;
import com.tmx.as.entities.bill.address.Address;
import com.tmx.as.entities.general.user.User;
import com.tmx.as.exceptions.DatabaseException;
import com.tmx.web.controls.*;
import com.tmx.util.Logger;

import java.text.SimpleDateFormat;
import java.util.Date;


public class InfoTerminalForm extends InfoTerminalFormProperties {


    public void bindData(Terminal terminal, User loggedInUser) throws DatabaseException {
        getTimeSelectorRegistrationDate().setTime( terminal.getRegistrationDate() );
        
        getCheckBoxBlocked().setSelected(terminal.getBlocked().booleanValue());

        if( terminal.getRegistrationDate() != null ){
            Date registartionDate = terminal.getRegistrationDate();
            SimpleDateFormat dataFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
            getEditBoxRegistrationDate().setValue( dataFormat.format(registartionDate) );
        }

        if(loggedInUser!=null && loggedInUser.getSellerOwner()!=null){
            getScrollBoxSellers().bind(new SellerManager().getNestedSellersInSeller(loggedInUser));
            if(terminal.getSellerOwner()!=null) {
                getScrollBoxSellers().setSelectedKey(terminal.getSellerOwnerId().toString());
            }
        }

        getScrollBoxEqTypes().bind(new TerminalManager().getAllEquipmentTypes());
        getScrollBoxTradePutletType().bind(new TerminalManager().getAllTradePutletTypes());
        
        if(terminal.getEquipmentType() != null)
            getScrollBoxEqTypes().setSelectedKey(terminal.getEquipmentType().getEquipmentTypeId().toString());
        else
            getScrollBoxEqTypes().setSelectedKey(ScrollBox.NULL_KEY);

        if(terminal.getTradePutlet()!=null){
            setTradePutletId(terminal.getTradePutlet().getTradePutletId());
            getEditBoxTradePutlet().setValue(terminal.getTradePutlet().getName());
            
            if(terminal.getTradePutlet().getType() != null)
                getScrollBoxTradePutletType().setSelectedKey(terminal.getTradePutlet().getType().getTradePutletTypeId().toString());
            else
                getScrollBoxTradePutletType().setSelectedKey(ScrollBox.NULL_KEY);

            if(terminal.getTradePutlet().getDirector() != null){
                setDirectorId(terminal.getTradePutlet().getDirector().getPersonId());
                getEditBoxDirectorEmail().setValue(terminal.getTradePutlet().getDirector().getEmail());
                getEditBoxDirectorPhone().setValue(terminal.getTradePutlet().getDirector().getPhone());
            }

            if(terminal.getTradePutlet().getManager() != null){
                setManagerId(terminal.getTradePutlet().getManager().getPersonId());
                getEditBoxManagerEmail().setValue(terminal.getTradePutlet().getManager().getEmail());
                getEditBoxManagerPhone().setValue(terminal.getTradePutlet().getManager().getPhone());
            }

            if(terminal.getTradePutlet().getAddress() != null){
                setTradePutletAddressId(terminal.getTradePutlet().getAddress().getAddressId());
                getEditBoxAddress().setValue(terminal.getTradePutlet().getAddress().getAddressLine1());
                getEditBoxBuilding().setValue(terminal.getTradePutlet().getAddress().getBuilding());
                if(terminal.getTradePutlet().getAddress().getCity() != null) {
                    getEditBoxCity().setValue(terminal.getTradePutlet().getAddress().getCity().getName());
                }
                if (terminal.getTradePutlet().getAddress().getRegion() != null) {
                    getEditBoxRegion().setValue(terminal.getTradePutlet().getAddress().getRegion().getName());
                }
            }

        }

        getEditBoxTerminalId().setValue(terminal.getTerminalId().toString());

        getEditBoxLogin().setValue(terminal.getLogin());
        getEditBoxPassword().setValue(terminal.getPassword());
        getEditBoxPassword2().setValue(terminal.getPassword());
        getEditBoxSerialNumber().setValue(terminal.getSerialNumber());
        getEditBoxHardwareSerialNumber().setValue(terminal.getHardwareSerialNumber());
    }

    public void bindData(User loggedInUser) throws DatabaseException {
        if(loggedInUser != null && loggedInUser.getSellerOwner() != null) {
            getScrollBoxSellers().bind(new SellerManager().getNestedSellersInSeller(loggedInUser));
        }

        getScrollBoxEqTypes().bind(new TerminalManager().getAllEquipmentTypes());
        getScrollBoxTradePutletType().bind(new TerminalManager().getAllTradePutletTypes());
    }


    public Terminal populateFromControls() throws DatabaseException {
        Terminal terminal = new Terminal();

        terminal.setTerminalId(PopulateUtil.getLongValue(getEditBoxTerminalId()));
        terminal.setLogin(getEditBoxLogin().getValue());
        terminal.setPassword(getEditBoxPassword().getValue());
        terminal.setBlocked(Boolean.valueOf(getCheckBoxBlocked().isSelected()));
        terminal.setRegistrationDate(new Date());
        terminal.setSerialNumber(getEditBoxSerialNumber().getValue());
        terminal.setHardwareSerialNumber(getEditBoxHardwareSerialNumber().getValue());

        terminal.setSellerOwner( getSellerOwner() );
        terminal.setEquipmentType( getEquipmentType() );
        terminal.setTradePutlet( getTradePutlet() );
//        try {
//            terminal.setPrimaryBalance( new TerminalManager().getTerminal(terminal.getTerminalId()).getPrimaryBalance());
//        } catch (Exception e) {
//            System.out.println("InfoTerminalForm.populateFromControls: " + e.getMessage());
//        }
        return terminal;
    }

    private TradePutlet getTradePutlet() throws DatabaseException {
        TradePutlet tradePutlet = new TradePutlet();

        tradePutlet.setTradePutletId(getTradePutletId());
        tradePutlet.setName(getEditBoxTradePutlet().getValue());
        tradePutlet.setType( getTradePutletType() );
        tradePutlet.setDirector( getDirector() );
        tradePutlet.setManager( getManager() );
        tradePutlet.setAddress(getAddress());

        return tradePutlet;
    }

    private Address getAddress() throws DatabaseException {
        Address address = new Address();

        address.setAddressId(getTradePutletAddressId());
        address.setAddressLine1(getEditBoxAddress().getValue());
        if (getEditBoxCity().getValue() != null && ! getEditBoxCity().getValue().equals("")) {
            address.setCity(new AddressManager().getCity(new Long(getEditBoxCity().getValue())));
        }
        address.setBuilding(getEditBoxBuilding().getValue());
        if (getEditBoxRegion().getValue() != null && ! getEditBoxRegion().getValue().equals("")) {
            address.setRegion(new AddressManager().getRegion(new Long(getEditBoxRegion().getValue())));
        }

        return address;
    }

    private TradePutletType getTradePutletType(){
        if( isNullKey(getScrollBoxTradePutletType()) )
            return null;

        TradePutletType type = new TradePutletType();
        type.setTradePutletTypeId(PopulateUtil.getLongKey(getScrollBoxTradePutletType()));
        return type;
    }

    private Person getDirector(){
        Person director = new Person();

        director.setPersonId(getDirectorId());
        director.setPhone(getEditBoxDirectorPhone().getValue());
        director.setEmail(getEditBoxDirectorEmail().getValue());

        return director;
    }

    private Person getManager(){
        Person manager = new Person();

        manager.setPersonId(getManagerId());
        manager.setPhone(getEditBoxManagerPhone().getValue());
        manager.setEmail(getEditBoxManagerEmail().getValue());

        return manager;
    }

    private Seller getSellerOwner(){
        Seller sellerOwner = new Seller();

        sellerOwner.setSellerId(PopulateUtil.getLongKey(getScrollBoxSellers()));

        return sellerOwner;
    }

    private EquipmentType getEquipmentType(){
        if( isNullKey(getScrollBoxEqTypes()) )
            return null;

        EquipmentType equipmentType = new EquipmentType();
        equipmentType.setEquipmentTypeId(PopulateUtil.getLongKey(getScrollBoxEqTypes()));
        return equipmentType;
    }

    private boolean isNullKey(ScrollBox scrollBox){
        return ScrollBox.NULL_KEY.equals( scrollBox.getSelectedKey() );
    }

}
