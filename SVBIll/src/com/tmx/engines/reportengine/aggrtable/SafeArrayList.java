package com.tmx.engines.reportengine.aggrtable;

import java.util.ArrayList;

/**
 * support random access fill of the List
 */
class SafeArrayList extends ArrayList {


    void safeSet(int index, Object element) {
        if (index >= size()) {
            for (int i = size(); i <= index - 1; i++)
                add(null);
            add(element);
        } else {
            set(index, element);
        }
    }
}
