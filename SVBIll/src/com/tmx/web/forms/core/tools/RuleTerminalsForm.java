package com.tmx.web.forms.core.tools;

import com.tmx.as.blogic.ExclusiveManager;
import com.tmx.as.blogic.SellerManager;
import com.tmx.as.entities.bill.exclusive.Exclusives;
import com.tmx.as.entities.bill.seller.Seller;
import com.tmx.as.exceptions.DatabaseException;
import com.tmx.web.base.BasicActionForm;
import com.tmx.web.controls.Button;
import com.tmx.web.controls.EditBox;
import com.tmx.web.controls.ScrollBox;
import com.tmx.web.controls.table.ControlManagerTable;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class RuleTerminalsForm extends BasicActionForm
{
    private EditBox ebCheckTerminal;
    private Button bCheck;
    private Button bClear;
    private boolean beeline;
    private boolean kyivstar;
    private boolean life;
    private boolean mts;

    private ScrollBox sbExclusiveTable;
    private Button bShow;
    private String chosenTable;
    private EditBox ebAddTerminal;
    private Button bAddTerminal;
    private Button bRemoveTerminals;
    private ControlManagerTable exclusiveTable;
    private String chosenTerminalId;

    private ScrollBox sbSellers;
    private Button bGetTerminals;
    private ControlManagerTable tradePointsTable;

    /*
    private ArrayList chosenExclusiveTable;
    private ArrayList beelineExc;
    private ArrayList kyivstarExc;
    private ArrayList lifeExc;
    private ArrayList mtsExc;
    ArrayList beeline_fc;
    ArrayList kyivstar_fc;
    */

    public void init()
    {
        initScrollBoxes();
        initButtons();
        initVariables();
        initTables();
        initEditBoxes();

        /*
        beeline_fc = new ArrayList();
        kyivstar_fc = new ArrayList();
        */
    }

    private void initScrollBoxes()
    {
        sbExclusiveTable = new ScrollBox() {
            protected String getKey(Object exc) { return ((Exclusives) exc).getExclusive(); }
            protected String getValue(Object exc) { return ((Exclusives) exc).getExclusive(); }
        };
        try {
            sbExclusiveTable.bind(new ExclusiveManager().getAllExclusives());
        } catch (DatabaseException e) {
            e.printStackTrace();
        }
        sbExclusiveTable.setNullable(true);
        sbExclusiveTable.setSelectedKey(ScrollBox.NULL_KEY);

        
        sbSellers = new ScrollBox(){
            protected String getKey(Object dataListElement) {
                return ((Seller)dataListElement).getCode();
            }
            protected String getValue(Object dataListElement) {
                return ((Seller)dataListElement).getCode();
            }
        };
        try {
            sbSellers.bind(new SellerManager().getAllSellers());
        } catch (DatabaseException e) {
            e.printStackTrace();
        }
    }

    private void initEditBoxes()
    {
        ebAddTerminal = new EditBox();

        ebCheckTerminal = new EditBox();
    }

    private void initButtons()
    {
        bShow = new Button();
        bShow.setLabelKey("ctrl.button.label.show");
        bShow.setAction("/RuleTerminalsAction");
        bShow.setCommand("showTable");

        bClear = new Button();
        bClear.setLabelKey("ctrl.button.label.reset");
        bClear.setAction("/RuleTerminalsAction");
        bClear.setCommand("defaultAction");

        bAddTerminal = new Button();
        bAddTerminal.setLabelKey("ctrl.button.label.add");
        bAddTerminal.setAction("/RuleTerminalsAction");
        bAddTerminal.setCommand("addToChosenTable");

        bRemoveTerminals = new Button();
        bRemoveTerminals.setLabelKey("ctrl.button.label.delete");
        bRemoveTerminals.setAction("/RuleTerminalsAction");
        bRemoveTerminals.setCommand("removeFromChosenTable");

        bCheck = new Button();
        bCheck.setLabelKey("ctrl.button.label.info");
        bCheck.setAction("/RuleTerminalsAction");
        bCheck.setCommand("info");

        bGetTerminals = new Button();
        bGetTerminals.setLabelKey("ctrl.button.label.apply");
        bGetTerminals.setAction("/RuleTerminalsAction");
        bGetTerminals.setCommand("defaultAction");
    }

    private void initVariables()
    {
        /*
        chosenExclusiveTable = new ArrayList();
        beelineExc = new ArrayList();
        kyivstarExc = new ArrayList();
        lifeExc = new ArrayList();
        mtsExc = new ArrayList();
        */

        chosenTable = "";

        chosenTerminalId = "";

        beeline = false;
        kyivstar = false;
        life = false;
        mts = false;
    }

    private void initTables()
    {
        exclusiveTable = new ControlManagerTable();
        exclusiveTable.setRefreshActionName("/RuleTerminalsAction");
        exclusiveTable.setRefreshCommandName("showTable");

        tradePointsTable = new ControlManagerTable();
        tradePointsTable.setRefreshActionName("/RuleTerminalsAction");
        tradePointsTable.setRefreshCommandName("defaultAction");
    }

    public EditBox getEbCheckTerminal() {
        return ebCheckTerminal;
    }

    public void setEbCheckTerminal(EditBox ebCheckTerminal) {
        this.ebCheckTerminal = ebCheckTerminal;
    }

    public Button getbCheck() {
        return bCheck;
    }

    public void setbCheck(Button bCheck) {
        this.bCheck = bCheck;
    }

    public Button getbClear() {
        return bClear;
    }

    public void setbClear(Button bClear) {
        this.bClear = bClear;
    }

    public boolean isBeeline() {
        return beeline;
    }

    public void setBeeline(boolean beeline) {
        this.beeline = beeline;
    }

    public boolean isKyivstar() {
        return kyivstar;
    }

    public void setKyivstar(boolean kyivstar) {
        this.kyivstar = kyivstar;
    }

    public boolean isLife() {
        return life;
    }

    public void setLife(boolean life) {
        this.life = life;
    }

    public boolean isMts() {
        return mts;
    }

    public void setMts(boolean mts) {
        this.mts = mts;
    }

    public ScrollBox getSbExclusiveTable() {
        return sbExclusiveTable;
    }

    public void setSbExclusiveTable(ScrollBox sbExclusiveTable) {
        this.sbExclusiveTable = sbExclusiveTable;
    }

    public Button getbShow() {
        return bShow;
    }

    public void setbShow(Button bShow) {
        this.bShow = bShow;
    }

    public String getChosenTable() {
        return chosenTable;
    }

    public void setChosenTable(String chosenTable) {
        this.chosenTable = chosenTable;
    }

    public EditBox getEbAddTerminal() {
        return ebAddTerminal;
    }

    public void setEbAddTerminal(EditBox ebAddTerminal) {
        this.ebAddTerminal = ebAddTerminal;
    }

    public Button getbAddTerminal() {
        return bAddTerminal;
    }

    public void setbAddTerminal(Button bAddTerminal) {
        this.bAddTerminal = bAddTerminal;
    }

    public Button getbRemoveTerminals() {
        return bRemoveTerminals;
    }

    public void setbRemoveTerminals(Button bRemoveTerminals) {
        this.bRemoveTerminals = bRemoveTerminals;
    }

    public ControlManagerTable getExclusiveTable() {
        return exclusiveTable;
    }

    public void setExclusiveTable(ControlManagerTable exclusiveTable) {
        this.exclusiveTable = exclusiveTable;
    }

    public String getChosenTerminalId() {
        return chosenTerminalId;
    }

    public void setChosenTerminalId(String chosenTerminalId) {
        this.chosenTerminalId = chosenTerminalId;
    }

    public ScrollBox getSbSellers() {
        return sbSellers;
    }

    public void setSbSellers(ScrollBox sbSellers) {
        this.sbSellers = sbSellers;
    }

    public Button getbGetTerminals() {
        return bGetTerminals;
    }

    public void setbGetTerminals(Button bGetTerminals) {
        this.bGetTerminals = bGetTerminals;
    }

    public ControlManagerTable getTradePointsTable() {
        return tradePointsTable;
    }

    public void setTradePointsTable(ControlManagerTable tradePointsTable) {
        this.tradePointsTable = tradePointsTable;
    }
}
