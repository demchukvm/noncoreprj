package com.tmx.beng.base.processors;

import com.tmx.as.entities.bill.seller.Seller;
import com.tmx.as.entities.bill.transaction.TransactionType;
import com.tmx.as.entities.bill.operator.Operator;
import com.tmx.as.entities.bill.terminal.Terminal;

import com.tmx.beng.base.*;
import com.tmx.beng.base.cache.DBCache;

/**
 * Rube class, only for web operations
 * tariff and restriction not used in web operation, so we mast prepare reservation set
 */
public class PrepareReservationSetProcessor extends TaskProcessor{

    protected Task process0(Task task, DBCache dbCache, BillingMessageWritable msg) throws BillException {
        logger.debug("prepare reservation set...");

        ReservationSet reservationSet = ReservationSet.createReservationSet();
        Reservation reservation = null;

        if (getSellerCode(msg) != null) {
            reservation = reservationSet.allocateReservation(Seller.class, getSellerCode(msg), null);
        }
        if (getServiceCode(msg) != null) {
            if (reservation != null) throw new BillException("err.bill.prepare_reservation_set_processor.reservation_already_exist");
            reservation = reservationSet.allocateReservation(Operator.class, getServiceCode(msg), null);

        }
        if (getTerminalSN(msg) != null){
            if (reservation != null) throw new BillException("err.bill.prepare_reservation_set_processor.reservation_already_exist");
            reservation = reservationSet.allocateReservation(Terminal.class, getTerminalSN(msg), null);
        }

        if (reservation == null) throw new BillException("err.bill.prepare_reservation_set_processor.error_msg");

        reservation.reserve(getBalanceCode(msg), getAmount(msg), null, getCheckingTransTypeCode(msg));
        msg.setAttribute(BillingMessage.BALANCE_RESERVATION_SET, reservationSet);

        return task;
    }

    private double getAmount(BillingMessageWritable msg){
        String amount = msg.getAttributeString(BillingMessage.AMOUNT);
        return Double.parseDouble(amount);
    }

    private String getCheckingTransTypeCode(BillingMessageWritable msg) throws BillException {
        String typeCode = msg.getAttributeString(BillingMessage.TRANSACTION_TYPE_ID);
        if(!TransactionType.CODE_CREDIT.equals(typeCode) && !(TransactionType.CODE_DEBET.equals(typeCode)))
            throw new BillException("err.bill.prepare_reservation_set_processor.undefined_type_code", new String[]{typeCode});
        return typeCode;  
    }

    private String getSellerCode(BillingMessageWritable msg){
        return msg.getAttributeString(BillingMessage.SELLER_CODE);
    }

    private String getServiceCode(BillingMessageWritable msg){
        return msg.getAttributeString(BillingMessage.SERVICE_CODE);
    }

    private String getTerminalSN(BillingMessageWritable msg){
        return msg.getAttributeString(BillingMessage.TERMINAL_SN);
    }

    private String getBalanceCode(BillingMessageWritable msg){
        return msg.getAttributeString(BillingMessage.BALANCE_CODE);
    }

}
