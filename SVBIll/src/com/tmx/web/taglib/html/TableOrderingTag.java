package com.tmx.web.taglib.html;

import com.tmx.as.base.OrderWrapper;
import com.tmx.web.taglib.base.TableBasicTag;
import org.apache.struts.taglib.TagUtils;
import org.apache.struts.taglib.nested.NestedPropertyHelper;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.JspException;


public class TableOrderingTag extends TableBasicTag {
    /**
     * Attribute name the table is ordered by
     */
    private String orderByFieldName = null;


    //------------------------------Properties
    public String getOrderByFieldName() {
        return orderByFieldName;
    }

    public void setOrderByFieldName(String orderByFieldName) {
        this.orderByFieldName = orderByFieldName;
    }

    //------------------------------Parsing events
    public int doStartTmxTag() throws JspException {
        String tablePagingHTML = renderTableOrderingHTML();
        TagUtils.getInstance().write(pageContext, tablePagingHTML);
        return (EVAL_BODY_TAG);
    }

    //------------------------------Rendering methods

    private String renderTableOrderingHTML() throws JspException {
        StringBuffer resultHTML = new StringBuffer();
        //render ASC DESC ordering links
        renderOrderingHref("asc", resultHTML);
        renderOrderingHref("desc", resultHTML);
        return resultHTML.toString();
    }

    private void renderOrderingHref(String orderType, StringBuffer resultHTML) throws JspException {
        //You could use images insted of symbols to render the ordering references
        String orderTypePresentation = "Unknown order type '" + orderType + "'";
        if ("asc".equals(orderType))
            orderTypePresentation = "Az";
        else if ("desc".equals(orderType))
            orderTypePresentation = "zA";

        resultHTML.append("[");
        if (isOrderAlreadyApplied(orderType)) {
            resultHTML.append(orderTypePresentation);
        } else {
            resultHTML.append("<a href=\"#\" ");
            resultHTML.append("onClick=\"document.forms[0].action='");
            resultHTML.append(getRefreshActionUrl());
            resultHTML.append("?");
            resultHTML.append(buildParameterString(orderType));
            resultHTML.append("';document.forms[0].submit()\">");
            resultHTML.append(orderTypePresentation);
            resultHTML.append("</a>");
        }
        resultHTML.append("]");
    }

    //-----------------------Bean iteration methods


    protected boolean isOrderAlreadyApplied(String orderType) throws JspException {
        HttpServletRequest request = (HttpServletRequest) pageContext.getRequest();
        String originalName = (getName() != null) ? getName() : NestedPropertyHelper.getCurrentName(request, this);
        //Read pageNumber from Query from Table from Bean form
        String originalProperty = getTableName() + ".query.orders";
        String nestedProperty = NestedPropertyHelper.getAdjustedProperty(request, originalProperty);

        Object value = TagUtils.getInstance().lookup(pageContext, originalName, nestedProperty, null);
        OrderWrapper[] orders = new OrderWrapper[0];
        if (value != null && (value instanceof OrderWrapper[]))
            orders = (OrderWrapper[]) value;

        for (int i = 0; i < orders.length; i++) {
            if (orders[i].getOrderByFieldName().equals(orderByFieldName) &&
                    orders[i].isOrderType(orderType))
                return true;
        }
        return false;
    }

    //------------------------------Business logic

    /**
     * Build parameter String
     */
    protected String buildParameterString(String orderType) {
        StringBuffer parameterString = new StringBuffer();
        parameterString.append(getTableName());
        parameterString.append(".appliedParameters");
        parameterString.append("=");
        parameterString.append("query(order(name=");
        parameterString.append(getOrderByFieldName());
        parameterString.append(",type=");
        parameterString.append(orderType);
        parameterString.append("))");
        return parameterString.toString();
    }

}