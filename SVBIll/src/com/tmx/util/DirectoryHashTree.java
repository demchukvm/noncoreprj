package com.tmx.util;

import java.util.*;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Manage tree of hash directories.
 *
 * Max count of nodes in structure is
 *
 * MaxNodesCount = n^m + n^(m-1) + ... n^2 + n^1 + n^0
 *
 * Where:
 * n - MAX_NESTED_NODES
 * m - MAX_NESTING
 */
public class DirectoryHashTree {
    private static DirectoryHashTree directoryHashTree = null;
    /** Max count of child dirs in any tree's directory */
    private static int MAX_NESTED_NODES = 100;
    /** Max count of files in any tree's directory  */
    private static int MAX_FILE_ENTRIES = 100;
    /** Max directory tree nesting */
    private static int MAX_NESTING = 10;

    /** Map of root nodes
     * [String rootNodePath][TreeNode rootNode] */
    private static Map roots = null;

    /** hide constructor */
    protected DirectoryHashTree(){
        roots = new HashMap();
    }

    public static TreeNode initializeRoot(String baseDirectory) throws InitException{
        if(directoryHashTree == null){
            directoryHashTree = new DirectoryHashTree();
        }
        TreeNode root = directoryHashTree.initRoot0(baseDirectory);
        roots.put(baseDirectory, root);
        return root;
    }

    private synchronized TreeNode initRoot0(String baseDirectory) throws InitException{
        File rootDir = new File(baseDirectory);
        // Checks if direcotry exists, otherwise creates it
        if(rootDir.exists()){
            if(!rootDir.isDirectory())
                throw new InitException("err.directory_hash_tree.failed_to_create_base_dir_file_with_same_name_exists", new String[]{baseDirectory});
        }
        else{
            if(!rootDir.mkdir())
                throw new InitException("err.directory_hash_tree.failed_to_create_base_dir", new String[]{baseDirectory});
        }

        TreeNode rootNode = new TreeNode(baseDirectory, null);
        rootNode.init();
        return rootNode;
    }

    /** Save file without hashing its name */
    public static String saveOriginalFile(String rootPath, String originalFileName, byte[] fileContent) throws ResourceException{
        TreeNode rootNode = (TreeNode)roots.get(rootPath);
        if(rootNode == null)
            throw new ResourceException("err.directory_hash_tree.root_node_not_found_by_path", new String[]{rootPath});


        String savedFilePath = null;
        FileOutputStream fos = null;
        try {
            TreeNode allocateNode = allocateTreeNode(rootNode);
            savedFilePath = allocateNode.path + File.separator + originalFileName;
            fos = new FileOutputStream(savedFilePath);
            fos.write(fileContent);
            fos.flush();
            fos.close();
            allocateNode.incrementEntriesCount();
        }
        catch (IOException e) {
            try{if(fos != null)fos.close();}catch(Exception ex){/** ignore */}
            throw new ResourceException("err.directory_hash_tree.failed_to_save_file", e);
        }
        return savedFilePath;
    }

    public static String saveHashedFile(String rootPath, String originalFileName, byte[] fileContent) throws ResourceException{
        String extension = "";
        if(originalFileName.lastIndexOf(".") >=0 )
             extension = originalFileName.substring(originalFileName.lastIndexOf("."));
        String hahsedName = MD5.getHashString(originalFileName + String.valueOf(System.currentTimeMillis())) + extension;
        return saveOriginalFile(rootPath, hahsedName, fileContent);
    }

    /** Obtains hashed directory path */
    private static TreeNode allocateTreeNode(TreeNode rootNode) throws ResourceException{
        TreeNode allocatedNode = rootNode.allocateNode();
        if(allocatedNode == null)
            throw new ResourceException("err.directory_hash_tree.tree_is_full");
        return allocatedNode;
    }

    /** Describes tree node */
    protected static class TreeNode{
        private String name = null;
        private String path = null;
        private TreeNode parent = null;
        private List nestedNodes = null;
        private int nestedFileEntriesCount = 0;
        /** index of nested node that was last allocated.
         * If == -1 then any of childs nodes has no allocation.
         * If == MAX_NESTED_NODES-1 then there is no allocation capabilities under this node */
        private int lastAllocatedNodeIdx = 0;
        private boolean limitExceeded = false;

        private TreeNode(String name, TreeNode parent){
            this.parent = parent;
            this.name = name;
            this.path = (parent != null) ? parent.path + File.separator + name : name;
            this.nestedNodes = new ArrayList();
            this.nestedFileEntriesCount = 0;
        }


        private TreeNode allocateNode() throws ResourceException{
            if(limitExceeded){
                return null;
            }
            if(getNestedFileEntriesCount() < MAX_FILE_ENTRIES){
            /*  nestedFileEntriesCount++;

                RM: increment this counter after actual file will be saved
                in allocated folder.
                */
                if(! new File(this.path).exists()){
                    throw new ResourceException("err.directory_hash_tree.directory_is_not_exist", new String[]{this.path});
                }
                return this;
            }
            for(int i=lastAllocatedNodeIdx; i<getNestedDirsCount(); i++){
                TreeNode allocatedNestedNode = ((TreeNode)nestedNodes.get(i)).allocateNode();
                if(allocatedNestedNode != null){
                    if(! new File(allocatedNestedNode.path).exists()){
                        continue;//ignore selected node
                    }
                    lastAllocatedNodeIdx = i;
                    return allocatedNestedNode;
                }
            }
            if(getNestingLevel() < MAX_NESTING && getNestedDirsCount() < MAX_NESTED_NODES){
                //create new nested Dir and returns it as last allocated
                String directoryName = String.valueOf(getNestedDirsCount());//generateDigitString(10);
                File newNestedDir = new File(path + File.separator + directoryName);

                if(!newNestedDir.mkdir()){
                    throw new ResourceException("err.directory_hash_tree.failed_to_create_dir");
                }

                TreeNode nestedNode = createNestedNode(directoryName, this);
                lastAllocatedNodeIdx = getNestedDirsCount()-1;
                return nestedNode;
            }
            limitExceeded = true;
            return null;
        }

        private int getNestedDirsCount(){
            return nestedNodes.size();
        }

        private int getNestedFileEntriesCount(){
            return nestedFileEntriesCount;
        }

        private void incrementEntriesCount(){
            nestedFileEntriesCount++;
        }

        private void init(){
            File thisDir = new File(path);
            String[] entries = thisDir.list();
            if(entries == null)
                return;

            for(int i=0; i<entries.length; i++){
                File entry = new File(path + File.separator + entries[i]);
                if(entry.isFile()){
                    nestedFileEntriesCount++;
                }
                else if(entry.isDirectory() && entry.exists()){
                    TreeNode nestedNode = createNestedNode(entries[i], this);
                    nestedNode.init();
                }
            }
            if(getNestedFileEntriesCount() >= MAX_FILE_ENTRIES &&
               getNestingLevel() >= MAX_NESTING)
               limitExceeded = true;

            else if(getNestedFileEntriesCount() >= MAX_FILE_ENTRIES &&
                    getNestedDirsCount() >= MAX_NESTED_NODES){
                for(Iterator iter = nestedNodes.iterator(); iter.hasNext();){
                    if(!((TreeNode)iter.next()).limitExceeded){
                        limitExceeded = false;
                        break;
                    }
                }
            }
        }

        private int getNestingLevel(){
            if(parent == null)
                return 1;
            else
                return parent.getNestingLevel()+1;
        }

        private static synchronized TreeNode createNestedNode(String name, TreeNode parentNode){
            TreeNode nestedNode = new TreeNode(name, parentNode);
            parentNode.nestedNodes.add(parentNode.getNestedDirsCount(), nestedNode);
            return nestedNode;
        }

        private String generateDigitString(int chars){
            if(chars <= 0)
                return null;
            String str = MD5.getHashString(String.valueOf(System.currentTimeMillis()));
            while(true){
                if(str.length() > chars)
                    return str.substring(0, chars);
                str = str + MD5.getHashString(String.valueOf(System.currentTimeMillis()) + str);
            }
        }

    }

}
