package com.tmx.as.entities.bill.transaction;

import com.tmx.as.entities.bill.balance.SellerBalance;
import com.tmx.as.entities.bill.seller.Seller;

/**
 */
public class SellerBalanceTransaction extends AbstractTransaction{
    private Seller seller;
    private SellerBalance sellerBalance;
    private String cliTransactionGUID;
    private Double preTxBalance;
    private Double fee;

    public String getCliTransactionGUID() {
        return cliTransactionGUID;
    }

    public void setCliTransactionGUID(String cliTransactionGUID) {
        this.cliTransactionGUID = cliTransactionGUID;
    }

    //-----------Properties
    public Seller getSeller() {
        return seller;
    }

    public void setSeller(Seller seller) {
        this.seller = seller;
    }

    public SellerBalance getSellerBalance() {
        return sellerBalance;
    }

    public void setSellerBalance(SellerBalance sellerBalance) {
        this.sellerBalance = sellerBalance;
    }

    public Double getPreTxBalance() {
        return preTxBalance;
    }

    public void setPreTxBalance(Double preTxBalance) {
        this.preTxBalance = preTxBalance;
    }

    public Double getFee() {
        return fee;
    }

    public void setFee(Double fee) {
        this.fee = fee;
    }
}
