package com.tmx.beng.csapi.java.base;

import com.tmx.beng.access.Connection;
import com.tmx.beng.base.*;
import com.tmx.beng.base.BillingMessage;
import com.tmx.as.entities.bill.function.ActualFunctionParameter;

import java.util.List;


public class DefaultParamsReq extends ClientRequest<DefaultParamsResp> {

    DefaultParamsReq(Connection connect) {
        super(connect);
        requestMsg.setOperationName(com.tmx.beng.base.BillingMessage.O_RETRIEVE_DEFAULT_FUNCTION_PARAMS);
    }

    public void setFunctionTypeId(Long typeId) {
        requestMsg.setAttribute(BillingMessage.FUNCTION_TYPE_ID, typeId);
    }

    protected DefaultParamsResp handleResponseMessage(BillingMessage responseMsg) {
        List<ActualFunctionParameter> params = (List<ActualFunctionParameter>) responseMsg.getAttribute(BillingMessage.ACTUAL_PARAMS_LIST);
        return new DefaultParamsResp(params);
    }
}
