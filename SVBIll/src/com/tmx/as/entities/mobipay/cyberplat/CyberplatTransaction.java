package com.tmx.as.entities.mobipay.cyberplat;

import com.tmx.as.base.BasicEntity;
import java.io.Serializable;
import java.util.Date;

public class CyberplatTransaction extends BasicEntity
{
    private Long cyberplatTransactionId;
    private Date requestTime;
    private Date responseTime;
    private String billTransactionNum;
    private String cyberplatTransactionNum;
    private String operationType;
    //private String number;
    private String account;
    private String amount;
    private String result;
    private String error;
    private String errmsg;
    private String terminalId;
    private String billServiceCode;
    private String cyberplatServiceCode;
    private String restBeforeTransaction;
    private String gateName;

    public Long getCyberplatTransactionId() {
        return cyberplatTransactionId;
    }

    public void setCyberplatTransactionId(Long cyberplatTransactionId) {
        this.cyberplatTransactionId = cyberplatTransactionId;
    }

    public Date getRequestTime() {
        return requestTime;
    }

    public void setRequestTime(Date requestTime) {
        this.requestTime = requestTime;
    }

    public Date getResponseTime() {
        return responseTime;
    }

    public void setResponseTime(Date responseTime) {
        this.responseTime = responseTime;
    }

    public String getBillTransactionNum() {
        return billTransactionNum;
    }

    public void setBillTransactionNum(String billTransactionNum) {
        this.billTransactionNum = billTransactionNum;
    }

    public String getCyberplatTransactionNum() {
        return cyberplatTransactionNum;
    }

    public void setCyberplatTransactionNum(String cyberplatTransactionNum) {
        this.cyberplatTransactionNum = cyberplatTransactionNum;
    }

    public String getOperationType() {
        return operationType;
    }

    public void setOperationType(String operationType) {
        this.operationType = operationType;
    }

//    public String getNumber() {
//        return number;
//    }
//
//    public void setNumber(String number) {
//        this.number = number;
//    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getErrmsg() {
        return errmsg;
    }

    public void setErrmsg(String errmsg) {
        this.errmsg = errmsg;
    }

    public String getTerminalId() {
        return terminalId;
    }

    public void setTerminalId(String terminalId) {
        this.terminalId = terminalId;
    }

    public String getBillServiceCode() {
        return billServiceCode;
    }

    public void setBillServiceCode(String billServiceCode) {
        this.billServiceCode = billServiceCode;
    }

    public String getCyberplatServiceCode() {
        return cyberplatServiceCode;
    }

    public void setCyberplatServiceCode(String cyberplatServiceCode) {
        this.cyberplatServiceCode = cyberplatServiceCode;
    }

    public String getRestBeforeTransaction() {
        return restBeforeTransaction;
    }

    public void setRestBeforeTransaction(String restBeforeTransaction) {
        this.restBeforeTransaction = restBeforeTransaction;
    }

    public String getGateName() {
        return gateName;
    }

    public void setGateName(String gateName) {
        this.gateName = gateName;
    }

    public Serializable getId() {
        return getCyberplatTransactionId();
    }

    public int getValid() {
        return valid;
    }

    public void setValid(int valid) {
        this.valid = valid;
    }

}
