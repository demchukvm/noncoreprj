package com.tmx.web.forms.core.csapi;

import com.tmx.as.entities.bill.terminal.EquipmentType;
import com.tmx.web.base.BasicActionForm;
import com.tmx.web.controls.Button;
import com.tmx.web.controls.EditBox;
import com.tmx.beng.access.Access;
import com.tmx.web.controls.RadioButtonGroup;
import com.tmx.web.controls.ScrollBox;
import com.tmx.web.forms.xmlengine.BasicXMLPublisherForm;
import org.apache.struts.taglib.html.RadioTag;

/**
 * Created by IntelliJ IDEA.
 * User: Proximan
 * Date: 30.06.2010
 * Time: 10:02:01
 * To change this template use File | Settings | File Templates.
 */
public class LocalRefillForm extends BasicXMLPublisherForm
{
    private EditBox telephone;          // номер телефона
    private EditBox account;            // номер лицевого
    private EditBox amount1;             // сумма 1
    private EditBox amount2;             // сумма 2
    private Button submitByTelephone;   // передать по телефону
    private Button submitByAccount;     // передать по лицевому
    private static final String ACTION = "/localRefill";

    private String operatorType = null;
    private Access billingEngineAccess = null;
    private String requestXml = null;
    private String desRequestXml = null;


    protected void init() {
        initButtons();
        initBoxes();
    }

    protected void initButtons()
    {
        submitByTelephone = new Button();
        submitByTelephone.setLabelKey("ctrl.button.label.refill");
        submitByTelephone.setAction(ACTION);
        submitByTelephone.setCommand("byTelephoneNum");

        submitByAccount = new Button();
        submitByAccount.setLabelKey("ctrl.button.label.refill");
        submitByAccount.setAction(ACTION);
        submitByAccount.setCommand("byAccountNum");             
    }

    private void initBoxes() {
        telephone = new EditBox();
        account = new EditBox();
        amount1 = new EditBox();
        amount2 = new EditBox();
    }

    public EditBox getTelephone() {
        return telephone;
    }

    public void setTelephone(EditBox telephone) {
        this.telephone = telephone;
    }

    public EditBox getAccount() {
        return account;
    }

    public void setAccount(EditBox account) {
        this.account = account;
    }

    public EditBox getAmount1() {
        return amount1;
    }

    public void setAmount1(EditBox amount1) {
        this.amount1 = amount1;
    }

    public EditBox getAmount2() {
        return amount2;
    }

    public void setAmount2(EditBox amount2) {
        this.amount2 = amount2;
    }

    public Button getSubmitByTelephone() {
        return submitByTelephone;
    }

    public void setSubmitByTelephone(Button submitByTelephone) {
        this.submitByTelephone = submitByTelephone;
    }

    public Button getSubmitByAccount() {
        return submitByAccount;
    }

    public void setSubmitByAccount(Button submitByAccount) {
        this.submitByAccount = submitByAccount;
    }

    public Access getBillingEngineAccess() {
        return billingEngineAccess;
    }

    public void setBillingEngineAccess(Access billingEngineAccess) {
        this.billingEngineAccess = billingEngineAccess;
    }

    public String getRequestXml() {
        return requestXml;
    }

    public void setRequestXml(String requestXml) {
        this.requestXml = requestXml;
    }

    public String getDesRequestXml() {
        return desRequestXml;
    }

    public void setDesRequestXml(String desRequestXml) {
        this.desRequestXml = desRequestXml;
    }

    public String getOperatorType() {
        return operatorType;
    }

    public void setOperatorType(String operatorType) {
        this.operatorType = operatorType;
    }
}


