package com.tmx.beng.base;

import com.tmx.beng.base.beans.CodeWrappingDocument;
import com.tmx.beng.base.beans.ExternalType;
import com.tmx.beng.base.beans.InternalType;
import com.tmx.util.cache.FileCache;
import com.tmx.util.cache.CacheException;

import java.util.HashMap;
import java.io.InputStream;


public class CodeWrapping extends FileCache {
    private static CodeWrapping instance = null;


    protected Object getRealObjectFromFile(InputStream is, String path) throws CacheException {
        CodeWrappingDocument root;

        try{
            root = CodeWrappingDocument.Factory.parse(is);
        }catch(Throwable e){
            throw new CacheException("err.code_wrapping.failed_to_parse_config", new String[]{ path }, e);
        }

        ExternalType[] externalType = root.getCodeWrapping().getExternalArray();
        HashMap codeMap = new HashMap();
        for(int i = 0; i < externalType.length; i++){
            InternalType[] internalType = externalType[i].getInternalArray();
            for(int j = 0; j < internalType.length; j++){
                codeMap.put(new Long(internalType[j].getCode()), new Long(externalType[i].getCode()));
            }
        }

        return codeMap;
    }

    public static CodeWrapping getInstance(){
        if(instance == null){
            instance = new CodeWrapping();
        }
        return instance;
    }

    private CodeWrapping(){
        maxSize = 100;
        cleanCount = 10;
        cleanType = CleanAlgorithm.Type.CLEAN_N_SELDOM_ACCESSED;
    }

    public Long getExternalCode(Long internalCode, String configFile)throws BillException{
        HashMap codeMap;
        try{
            codeMap = (HashMap)get(configFile);
        }catch(CacheException e){
            throw new BillException("err.bill.failed_to_get_codemap", new String[]{ configFile }, e);
        }

        Long ret = (Long)codeMap.get(internalCode);
        if(ret == null){
            ret = internalCode;
        }
        return ret;
    }

}
