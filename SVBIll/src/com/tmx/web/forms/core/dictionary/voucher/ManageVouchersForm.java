package com.tmx.web.forms.core.dictionary.voucher;

import com.tmx.util.i18n.MessageResources;
import com.tmx.web.base.BasicActionForm;
import com.tmx.web.controls.*;
import com.tmx.web.controls.grid.Grid;
import com.tmx.web.controls.grid.GridException;
import com.tmx.as.blogic.VoucherManager;
import com.tmx.as.exceptions.DatabaseException;
import com.tmx.web.forms.core.tools.TestBuyVouchersForm;
import java.util.List;

public class ManageVouchersForm extends BasicActionForm
{
    protected Grid gridVoucherRmains;

    private ScrollBox sbVoucherNominal;
    private EditBox ebVoucherCount;
    private Button bCutToPrepaid;
    private String message1 = "";

    private EditBox ebFilePath;
    private Button bLoadFile;
    private Button bAddToSvbill;
    private String message2 = "";

    private String absoluteFileName = "";

    private Button bUpload;

    protected void init()
    {
        initButtons();
        initScrollBoxes();
        initEditBoxes();
        initGrid();
    }

    private void initScrollBoxes()
    {
        sbVoucherNominal = new ScrollBox(){
            protected String getKey(Object voucherNominal){return voucherNominal.toString();}
            protected String getValue(Object voucherNominal){return voucherNominal.toString();}
        };
        sbVoucherNominal.setNullable(true);
        sbVoucherNominal.setMandatory(true);
    }

    private void initEditBoxes()
    {
        ebVoucherCount = new EditBox();
        ebVoucherCount.addValidationRule(EditBox.createValidationRule_INTEGER_FORMAT(getLocale()));
        ebVoucherCount.setMandatory(true);

        ebFilePath = new EditBox();
        ebFilePath.setReadonly(true);
        ebFilePath.setMandatory(true);
    }


    private void initButtons()
    {
        bCutToPrepaid = new Button();
        bCutToPrepaid.setAction("/core/dictionary/voucher/manageVouchers");
        bCutToPrepaid.setCommand("cutToPrepaid");
        bCutToPrepaid.setLabelKey("ctrl.button.label.cutToPrepaid");

        bLoadFile = new Button();
        bLoadFile.setAction("/core/dictionary/voucher/manageVouchers");
        bLoadFile.setCommand("loadFile");
        bLoadFile.setLabelKey("ctrl.button.label.loadFile");

        bAddToSvbill = new Button();
        bAddToSvbill.setAction("/core/dictionary/voucher/manageVouchers");
        bAddToSvbill.setCommand("addToSvbill");
        bAddToSvbill.setLabelKey("ctrl.button.label.addToSvbill");


        bUpload = new Button();
        bUpload.setAction("/core/dictionary/voucher/manageVouchers");
        bUpload.setCommand("upload");
        bUpload.setLabelKey("ctrl.button.label.addToSvbill");
    }

     private void initGrid() 
     {
        gridVoucherRmains = new Grid()
        {
            public void bind(List data) throws GridException
            {
                setCell(0, 0, getLable("nominal"), "column_header_cell", "font-weight: bold", 1, 1);
                setCell(0, 1, getLable("count"), "column_header_cell", "font-weight: bold", 1, 1);

                for (int i = 0; i < data.size(); i++) {
                    Object[] nominal_count = (Object[])data.get(i);
                    setCell(i+1, 0, "" + nominal_count[0],"column_data_cell", 1, 1);//nominal
                    setCell(i+1, 1, "" + nominal_count[1], "column_data_cell", 1, 1);//count
                }
            }

            private String getLable(String key)
            {
                return MessageResources.getInstance().getOwnedLocalizedMessage(TestBuyVouchersForm.class, "labels", getLocale(), key, null);
            }
        };
    }

    public List getVouchersRamains() throws DatabaseException {
        return (new VoucherManager()).getRemains();
    }

    public Grid getGridVoucherRmains() {
        return gridVoucherRmains;
    }

    public void setGridVoucherRmains(Grid gridVoucherRmains) {
        this.gridVoucherRmains = gridVoucherRmains;
    }

    public ScrollBox getSbVoucherNominal() {
        return sbVoucherNominal;
    }

    public void setSbVoucherNominal(ScrollBox sbVoucherNominal) {
        this.sbVoucherNominal = sbVoucherNominal;
    }

    public EditBox getEbVoucherCount() {
        return ebVoucherCount;
    }

    public void setEbVoucherCount(EditBox ebVoucherCount) {
        this.ebVoucherCount = ebVoucherCount;
    }

    public Button getbCutToPrepaid() {
        return bCutToPrepaid;
    }

    public void setbCutToPrepaid(Button bCutToPrepaid) {
        this.bCutToPrepaid = bCutToPrepaid;
    }

    public String getMessage1() {
        return message1;
    }

    public void setMessage1(String message1) {
        this.message1 = message1;
    }

    public String getMessage2() {
        return message2;
    }

    public void setMessage2(String message2) {
        this.message2 = message2;
    }

    public EditBox getEbFilePath() {
        return ebFilePath;
    }

    public void setEbFilePath(EditBox ebFilePath) {
        this.ebFilePath = ebFilePath;
    }

    public Button getbLoadFile() {
        return bLoadFile;
    }

    public void setbLoadFile(Button bLoadFile) {
        this.bLoadFile = bLoadFile;
    }

    public Button getbAddToSvbill() {
        return bAddToSvbill;
    }

    public void setbAddToSvbill(Button bAddToSvbill) {
        this.bAddToSvbill = bAddToSvbill;
    }

    public String getAbsoluteFileName() {
        return absoluteFileName;
    }

    public void setAbsoluteFileName(String absoluteFileName) {
        this.absoluteFileName = absoluteFileName;
    }

    public Button getbUpload() {
        return bUpload;
    }

    public void setbUpload(Button bUpload) {
        this.bUpload = bUpload;
    }
}
