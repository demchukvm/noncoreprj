package com.tmx.web.actions.core.ui_permission;

import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForm;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.tmx.web.base.BasicDispatchedAction;
import com.tmx.web.base.Roadmap;
import com.tmx.web.forms.core.ui_permission.InfoRoleForm;
import com.tmx.as.base.EntityManager;
import com.tmx.as.entities.general.role.Role;

public class InfoRoleAction extends BasicDispatchedAction {
    private static final String FORWARD_MANAGE_ACTION = "manageAction";
    private static final String FORWARD_INFO_PAGE = "infoPage";
    private static final String FORWARD_MANAGE_DELETE_ACTION = "manageDeleteAction";

    public ActionForward preloadForCreate(ActionMapping mapping,
                                          ActionForm form,
                                          HttpServletRequest request,
                                          HttpServletResponse response) throws Throwable {
        InfoRoleForm roleForm = (InfoRoleForm) verifyForm(form, InfoRoleForm.class);
        roleForm.bindData(new Role());
        return mapping.findForward(FORWARD_INFO_PAGE);
    }

    public ActionForward preloadForUpdate(ActionMapping mapping,
                                          ActionForm form,
                                          HttpServletRequest request,
                                          HttpServletResponse response) throws Throwable {
        InfoRoleForm userInterfaceForm = (InfoRoleForm) verifyForm(form, InfoRoleForm.class);
        userInterfaceForm.bindData((Role)new EntityManager().RETRIEVE(Role.class,userInterfaceForm.getSelectedRoleId()));
        return mapping.findForward(FORWARD_INFO_PAGE);
    }

    public ActionForward apply(ActionMapping mapping,
                              ActionForm form,
                              HttpServletRequest request,
                              HttpServletResponse response) throws Throwable {
        validateTransactionToken(request);
        InfoRoleForm roleForm = (InfoRoleForm) verifyForm(form, InfoRoleForm.class);
        roleForm.validate(mapping.findForward(FORWARD_INFO_PAGE));
        Role role=roleForm.populateFromControls();
        new EntityManager().SAVE(role);
        //render after saving
        roleForm.setSelectedRoleId(role.getRoleId());
        roleForm.getActionExecStatus().setNewStatus("apply", "applied");
        return preloadForUpdate(mapping,form,request,response);
    }

    public ActionForward save(ActionMapping mapping,
                              ActionForm form,
                              HttpServletRequest request,
                              HttpServletResponse response) throws Throwable {
        apply(mapping, form, request, response);
        return mapping.findForward(FORWARD_MANAGE_ACTION);
    }

    public ActionForward preloadForDelete(ActionMapping mapping,
                                          ActionForm form,
                                          HttpServletRequest request,
                                          HttpServletResponse response) throws Throwable {
        Roadmap roadmap=getRoadmap();
        roadmap.collectParameter("/core/message/confirmation", "leftMenuName", "ui_permission");
        roadmap.setPageForward("confirmation","onOk","local:"+this.getName()+"#"+FORWARD_MANAGE_DELETE_ACTION);
        roadmap.setPageForward("confirmation","onCancel","local:"+this.getName()+"#"+FORWARD_MANAGE_ACTION);
        return mapping.findForward("confirmation");
    }

    public ActionForward delete(ActionMapping mapping,
                              ActionForm form,
                              HttpServletRequest request,
                              HttpServletResponse response) throws Throwable {
        InfoRoleForm roleForm = (InfoRoleForm) verifyForm(form, InfoRoleForm.class);
        new EntityManager().DELETE(Role.class,roleForm.getSelectedRoleId());
        return mapping.findForward(FORWARD_MANAGE_ACTION);
    }
}
