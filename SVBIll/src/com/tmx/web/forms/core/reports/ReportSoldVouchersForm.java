package com.tmx.web.forms.core.reports;

import com.tmx.as.exceptions.DatabaseException;
import com.tmx.web.controls.grid.GridException;

import java.util.List;

public class ReportSoldVouchersForm extends ReportVouchersForm {

    protected void init() {
        super.init();        
    }

    public void refresh() throws DatabaseException, GridException {
        List<List> vouchersData = composeVouchersData(this);

        gridVouchersReport.bind(vouchersData);
    }

    public void bindData() throws GridException, DatabaseException {
        super.bindData();
    }
}
