package com.tmx.util;

import org.apache.log4j.Category;
import java.net.InetAddress;
import java.security.SecureRandom;

/**

 */
public class UID {
    private static Category m_log = Category.getInstance(UID.class.getName());
    private final String DELIMITER="-";

    public String getUID() {
        String strRetVal = "";
        String strTemp = "";
        try {
            // Get IPAddress Segment
            InetAddress addr = InetAddress.getLocalHost();
            byte[] ipaddr = addr.getAddress();
            for (int i = 0; i < ipaddr.length; i++) {
                Byte b = new Byte(ipaddr[i]);
                strTemp = Integer.toHexString(b.intValue() & 0x000000ff);
                while (strTemp.length() < 2)
                    strTemp = '0' + strTemp;
                strRetVal += strTemp;
            }
            strRetVal += DELIMITER;

            //Get CurrentTimeMillis() segment
            strTemp = Long.toHexString(System.currentTimeMillis());
            while (strTemp.length() < 12) {
                strTemp = '0' + strTemp;
            }
            String splitTmpStr = strTemp.substring(0, 4) + DELIMITER + strTemp.substring(4, 8) + DELIMITER + strTemp.substring(8, 12) + DELIMITER;
            strRetVal += splitTmpStr;

            //Get Random Segment
            SecureRandom prng = SecureRandom.getInstance("SHA1PRNG");

            strTemp = Integer.toHexString(prng.nextInt());
            while (strTemp.length() < 8)
                strTemp = '0' + strTemp;
            strRetVal += strTemp.substring(4);

            //Get IdentityHash() segment
            strTemp = Long.toHexString(System.identityHashCode(this));
            while (strTemp.length() < 8)
                strTemp = '0' + strTemp;
            strRetVal += strTemp;
        }
        catch (java.net.UnknownHostException ex) {
            m_log.error("Unknown Host Exception Caught: " + ex.getMessage());
        }
        catch (java.security.NoSuchAlgorithmException ex) {
            m_log.error("No Such Algorithm Exception Caught: " + ex.getMessage());
        }
        return strRetVal.toUpperCase();
    }


    /** main method to use this util from ant script */
    public static void main(String[] args){
        System.out.println(new UID().getUID());
    }
}