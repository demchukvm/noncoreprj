<?php /* Smarty version 2.6.26, created on 2013-02-21 17:59:06
         compiled from advanced_search_in_category.tpl.html */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'set_query_html', 'advanced_search_in_category.tpl.html', 1, false),array('modifier', 'escape', 'advanced_search_in_category.tpl.html', 11, false),)), $this); ?>
<form name='AdvancedSearchInCategory' method='get' action='<?php echo ((is_array($_tmp="?ukey=category_search")) ? $this->_run_mod_handler('set_query_html', true, $_tmp) : smarty_modifier_set_query_html($_tmp)); ?>
'>

	<?php if (! @FURL_ENABLED || ! @MOD_REWRITE_SUPPORT): ?>	
		<input name='ukey' value='category_search' type="hidden" >
	<?php endif; ?>
	
	<?php if ($this->_tpl_vars['categories_to_select']): ?>
		<input type='hidden' name='search_with_change_category_ability' value='yes' >
	<?php else: ?>
		<input type='hidden' name='search_with_change_category_ability' value='1' >
		<input name='categoryID' val<?php echo ((is_array($_tmp=$this->_tpl_vars['params'][$this->_sections['i']['index']]['name'])) ? $this->_run_mod_handler('escape', true, $_tmp, 'html') : smarty_modifier_escape($_tmp, 'html')); ?>
ue='<?php echo $this->_tpl_vars['categoryID']; ?>
' type="hidden" >
	<?php endif; ?>
	
	
	<?php if ($this->_tpl_vars['categories_to_select']): ?>
	<input name='categoryID' type='hidden' value = '<?php echo $this->_tpl_vars['categoryID']; ?>
'>

	<?php endif; ?>

	<div class="filters">

            
        	<?php unset($this->_sections['i']);
$this->_sections['i']['name'] = 'i';
$this->_sections['i']['loop'] = is_array($_loop=$this->_tpl_vars['params']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['i']['show'] = true;
$this->_sections['i']['max'] = $this->_sections['i']['loop'];
$this->_sections['i']['step'] = 1;
$this->_sections['i']['start'] = $this->_sections['i']['step'] > 0 ? 0 : $this->_sections['i']['loop']-1;
if ($this->_sections['i']['show']) {
    $this->_sections['i']['total'] = $this->_sections['i']['loop'];
    if ($this->_sections['i']['total'] == 0)
        $this->_sections['i']['show'] = false;
} else
    $this->_sections['i']['total'] = 0;
if ($this->_sections['i']['show']):

            for ($this->_sections['i']['index'] = $this->_sections['i']['start'], $this->_sections['i']['iteration'] = 1;
                 $this->_sections['i']['iteration'] <= $this->_sections['i']['total'];
                 $this->_sections['i']['index'] += $this->_sections['i']['step'], $this->_sections['i']['iteration']++):
$this->_sections['i']['rownum'] = $this->_sections['i']['iteration'];
$this->_sections['i']['index_prev'] = $this->_sections['i']['index'] - $this->_sections['i']['step'];
$this->_sections['i']['index_next'] = $this->_sections['i']['index'] + $this->_sections['i']['step'];
$this->_sections['i']['first']      = ($this->_sections['i']['iteration'] == 1);
$this->_sections['i']['last']       = ($this->_sections['i']['iteration'] == $this->_sections['i']['total']);
?>
		        		<ul>
					
					<?php unset($this->_sections['j']);
$this->_sections['j']['name'] = 'j';
$this->_sections['j']['loop'] = is_array($_loop=$this->_tpl_vars['params'][$this->_sections['i']['index']]['variants']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['j']['show'] = true;
$this->_sections['j']['max'] = $this->_sections['j']['loop'];
$this->_sections['j']['step'] = 1;
$this->_sections['j']['start'] = $this->_sections['j']['step'] > 0 ? 0 : $this->_sections['j']['loop']-1;
if ($this->_sections['j']['show']) {
    $this->_sections['j']['total'] = $this->_sections['j']['loop'];
    if ($this->_sections['j']['total'] == 0)
        $this->_sections['j']['show'] = false;
} else
    $this->_sections['j']['total'] = 0;
if ($this->_sections['j']['show']):

            for ($this->_sections['j']['index'] = $this->_sections['j']['start'], $this->_sections['j']['iteration'] = 1;
                 $this->_sections['j']['iteration'] <= $this->_sections['j']['total'];
                 $this->_sections['j']['index'] += $this->_sections['j']['step'], $this->_sections['j']['iteration']++):
$this->_sections['j']['rownum'] = $this->_sections['j']['iteration'];
$this->_sections['j']['index_prev'] = $this->_sections['j']['index'] - $this->_sections['j']['step'];
$this->_sections['j']['index_next'] = $this->_sections['j']['index'] + $this->_sections['j']['step'];
$this->_sections['j']['first']      = ($this->_sections['j']['iteration'] == 1);
$this->_sections['j']['last']       = ($this->_sections['j']['iteration'] == $this->_sections['j']['total']);
?>

					<li<?php if ($this->_tpl_vars['params'][$this->_sections['i']['index']]['variants'][$this->_sections['j']['index']]['selected']): ?> class='current'<?php endif; ?>>
                    <input style='display: none;float:left; position:relative; top: 5px;margin: 0 5px 0 10px; ' id='param_<?php echo $this->_tpl_vars['params'][$this->_sections['i']['index']]['variants'][$this->_sections['j']['index']]['variantID']; ?>
' name='param_<?php echo $this->_tpl_vars['params'][$this->_sections['i']['index']]['optionID']; ?>
_<?php echo $this->_tpl_vars['params'][$this->_sections['i']['index']]['variants'][$this->_sections['j']['index']]['variantID']; ?>
' type="checkbox" value="<?php echo $this->_tpl_vars['params'][$this->_sections['i']['index']]['variants'][$this->_sections['j']['index']]['variantID']; ?>
" <?php if ($this->_tpl_vars['params'][$this->_sections['i']['index']]['variants'][$this->_sections['j']['index']]['selected']): ?>checked='checked'<?php endif; ?>>
					<label for="param_<?php echo $this->_tpl_vars['params'][$this->_sections['i']['index']]['variants'][$this->_sections['j']['index']]['variantID']; ?>
" >
                           <a class='tag-<?php echo $this->_tpl_vars['params'][$this->_sections['i']['index']]['variants'][$this->_sections['j']['index']]['variantID']; ?>
' title='<?php echo ((is_array($_tmp=$this->_tpl_vars['params'][$this->_sections['i']['index']]['variants'][$this->_sections['j']['index']]['value'])) ? $this->_run_mod_handler('escape', true, $_tmp, 'html') : smarty_modifier_escape($_tmp, 'html')); ?>
'>
                               <?php echo ((is_array($_tmp=$this->_tpl_vars['params'][$this->_sections['i']['index']]['variants'][$this->_sections['j']['index']]['value'])) ? $this->_run_mod_handler('escape', true, $_tmp, 'html') : smarty_modifier_escape($_tmp, 'html')); ?>

                             </a>
                    </label>
                    </li>
                        
					<?php endfor; endif; ?>
					

				</ul>
					
			<?php endfor; endif; ?>
	
	</div>
	        <input type='hidden' value='1' name='search_in_subcategory' >
            <input type='hidden' value='' name='advanced_search_in_category' >
</form>

<?php echo '
<script type="text/javascript" language="javascript">



$(function() {
   $(\'.filters li input[type="checkbox"]\').change(function(){
   //   $(\'form[name="AdvancedSearchInCategory"]\').submit();
   var form_data = $(\'form[name="AdvancedSearchInCategory"]\').serialize();
   
   $.ajax({
	  type: "GET",
	  url:\'/category_search/\',
	  data: form_data,
	  error: function() {
	    $(\'#status\').text(\'Update failed. Try again.\').slideDown(\'slow\');
	  },
	  success: function(data) {
	   
	    $(\'#container\').html($(data).find(\'#container\').html());
	  },
	  
      complete: function() {
	    setTimeout(function() {
	      $(\'#status\').slideUp(\'slow\');
	    }, 3000);
	  }
	});
   
   //return false;
   });
   
});
</script>
'; ?>