package com.tmx.as.entities.bill.transaction;

import com.tmx.as.base.BasicEntity;

import java.io.Serializable;

/**
 */
public class TransactionType extends BasicEntity{
    
    private String code;
    private String name;
    private String description;

    public final static String CODE_CREDIT = "1";
    public final static String CODE_DEBET = "2";


    public TransactionType(String code){
        this.code = code;
    }

    public TransactionType() {
        
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Serializable getId() {
        return getCode();
    }

    public int getValid() {
        return valid;
    }

    public void setValid(int valid) {
        this.valid = valid;
    }

}
