package com.tmx.web.actions.core.dictionary.voucher;

import com.tmx.web.actions.core.AbstractSvbillAction;
import com.tmx.web.forms.core.dictionary.voucher.ManageVouchersForm;
import com.tmx.web.forms.core.dictionary.voucher.InfoVoucherForm;
import com.tmx.as.entities.bill.voucher.Voucher;
import com.tmx.as.blogic.VoucherManager;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForm;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class InfoVoucherAction extends AbstractSvbillAction {
    private static final String FORWARD_INFO_OPERATOR_PAGE = "infoVoucherForm";

    public ActionForward defaultAction(ActionMapping mapping,
                                       ActionForm form,
                                       HttpServletRequest request,
                                       HttpServletResponse response,
                                       String methodName) throws Throwable {
        InfoVoucherForm infoVoucherForm = (InfoVoucherForm) form;
        Voucher voucher = new VoucherManager().getVoucherById(infoVoucherForm.getVoucherId());
        infoVoucherForm.bindData(voucher);
        return mapping.findForward(FORWARD_INFO_OPERATOR_PAGE);
    }

    public ActionForward preloadForUpdate(ActionMapping mapping,
                                          ActionForm form,
                                          HttpServletRequest request,
                                          HttpServletResponse response) throws Throwable {
        

        return mapping.findForward(FORWARD_INFO_OPERATOR_PAGE);
    }
}