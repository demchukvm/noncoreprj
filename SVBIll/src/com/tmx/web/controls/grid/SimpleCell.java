package com.tmx.web.controls.grid;

public class SimpleCell implements Cell{
    private String value;
    private String styleClass;
    private String style;
    private int colSpan=1;
    private int rowSpan=1;

    public SimpleCell(int colSpan, int rowSpan) {
        this.colSpan = colSpan;
        this.rowSpan = rowSpan;
    }

    public SimpleCell(String value, String styleClass,String style , int colSpan, int rowSpan) {
        this(colSpan, rowSpan);
        this.value = value;
        this.styleClass = styleClass;
        this.style = style;
    }

    public SimpleCell(String value, String styleClass,String style) {
        this.value = value;
        this.styleClass = styleClass;
        this.style = style;
    }

    public String getValue() {
        return value;
    }

    public String getStyleClass() {
        return styleClass;
    }

    public int getColSpan() {
        return colSpan;
    }

    public int getRowSpan() {
        return rowSpan;
    }

    public String getStyle() {
        return style;
    }

    public void setStyle(String style) {
        this.style = style;
    }
}
