package com.tmx.beng.httpsgate.citypay;

import com.sun.org.apache.xml.internal.serialize.OutputFormat;
import com.sun.org.apache.xml.internal.serialize.XMLSerializer;
import com.tmx.as.base.CriteriaWrapper;
import com.tmx.as.base.EntityManager;
import com.tmx.as.base.QueryParameterWrapper;
import com.tmx.as.base.criterion.CriterionWrapper;
import com.tmx.as.base.criterion.Restrictions;
import com.tmx.as.entities.bill.transaction.ClientTransaction;
import com.tmx.as.entities.bill.transaction.OperatorBalanceTransaction;
import com.tmx.as.entities.bill.transaction.SellerBalanceTransaction;
import com.tmx.as.entities.bill.transaction.TerminalBalanceTransaction;
import com.tmx.as.entities.mobipay.citypay.CitypayTransaction;
import com.tmx.as.exceptions.DatabaseException;
import com.tmx.beng.access.Access;
import com.tmx.beng.base.*;
import com.tmx.beng.httpsgate.BadResponseException;
import com.tmx.beng.httpsgate.CyrillicToTranslitConverter;
import com.tmx.beng.httpsgate.HttpsGateException;
import com.tmx.beng.httpsgate.HttpsRequestException;
import com.tmx.beng.httpsgate.citypay.CitypayMessage;
import com.tmx.gate.datastream.base.GateContext;
import com.tmx.util.Configuration;
import com.tmx.util.InitException;
import com.tmx.util.queue.ActionQueue;
import org.apache.commons.digester.Digester;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.params.HttpClientParams;
import org.apache.log4j.Logger;
//import org.dom4j.Document;
//import org.dom4j.DocumentHelper;
//import org.dom4j.Element;
//import org.dom4j.io.OutputFormat;
import org.apache.commons.httpclient.*;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.*;
import java.nio.charset.Charset;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.locks.ReentrantLock;

public abstract class BasicCitypayGate extends BasicGate
{
    protected Logger logger = Logger.getLogger(getGateName());
    final static Charset charset = Charset.forName("UTF-8");
    private Access access = null;
    private ActionQueue messageQueue = null;

    private static final String PROP_URL = "url";
  //  private static final String PROP_PORT = "port";
    private static final String PROP_SERIAL = "serial";
    private static final String PROP_KEYWORD = "keyWord";
   /* private static final String PROP_KEYSTORE_PATH = "keystore.path";
    private static final String PROP_KEYSTORE_PASSWORD = "keystore.password";
    private static final String PROP_TRUSTSTORE_PATH = "truststore.path";
    private static final String PROP_TRUSTSTORE_PASSWORD = "truststore.password";*/
    private static final String PROP_INIT_HANDLER_COUNT = "handlers.initial";
    private static final String PROP_MAX_HANDLER_COUNT = "handlers.max";
    private static final String PROP_SLEEP_BEFORE_STATUS_REQEST = "sleepBeforeStatusRequest";
    /*private static final String PROP_PAYREQ_MAX_COUNT = "paymentRequest.maxAttemptCount";
    private static final String PROP_CONFPAY_MAX_COUNT = "confPayment.maxAttemptCount";
    private static final String PROP_SERVUNAV_MAX_COUNT = "serviceUnavailable.maxAttemptCount";
    private static final String PROP_LOCAL_ADDRESS = "localAddress";*/

   // private String prevBillTransactionNumINT;

    private CodeMapping codeMapping; // для маппинга

    protected abstract String getGateName();

    protected void init0() throws InitException
    {
        properties = GateContext.getInstance().getDatastreamProcessorConfig(getGateName());
        int initHandlerCount = Integer.parseInt(properties.getProperty(PROP_INIT_HANDLER_COUNT, "1"));
        int maxHandlerCount = Integer.parseInt(properties.getProperty(PROP_MAX_HANDLER_COUNT, "5"));
        messageQueue = new ActionQueue(initHandlerCount, maxHandlerCount);

        access = obtainBillingEngineAccess();

        // маппинг
        codeMapping = new CodeMapping();
        codeMapping.init();

        // валидация
        if(properties.getProperty(PROP_URL) == null ||
           properties.getProperty(PROP_SERIAL) == null ||
           properties.getProperty(PROP_KEYWORD) == null)
            throw new InitException("gate.xml properties empty");


    }

    protected void shutdown0(final long timeout)
    {
        logger.info("Start Citypay Gate Shutdown");
        if(messageQueue.actionCount() != 0){
            Runtime.getRuntime().addShutdownHook(new Thread(){
                public void run(){
                    try{
                        long lt = System.currentTimeMillis();
                        while(messageQueue.actionCount() != 0 &&
                                ((System.currentTimeMillis() - lt) < timeout)){
                            sleep(1000);
                        }
                    }
                    catch(Throwable e){
                        e.printStackTrace();
                    }
                }
            });
        }
        logger.info("Citypay Gate shutdown");
    }

    public void enqueueBillMessage(BillingMessage billMessage)
    {
        CitypayGateTask task = new CitypayGateTask(billMessage, this);
        messageQueue.enqueue(task);
    }





    // Gate
    private void updateTransactionStatus(CitypayTransaction citypayTransaction,  CitypayMessage citypayMessage)
    {
        if(citypayTransaction != null)
        {
            try {
                EntityManager entityManager = new EntityManager();
                citypayTransaction.setStatusCodeByCitypay(citypayMessage.getStatus());
                citypayTransaction.setStatusMessageByCitypay(citypayMessage.getResponseMessage());

                entityManager.SAVE(citypayTransaction);

            } catch (DatabaseException e) {
                logger.error("Error of load previousTransaction 1", e);
                e.printStackTrace();
            } catch (Exception ee) {
                logger.error("Error of load previousTransaction 2", ee);
                ee.printStackTrace();
            }
        }
    }
    // ClientTransaction
    private void updateTransactionStatus(ClientTransaction clientTransaction,  CitypayMessage citypayMessage)
    {
        if(clientTransaction != null)
        {
            try {
                EntityManager entityManager = new EntityManager();

                String status = citypayMessage.getStatus();
                if(isEndedCode(status))
                {
                    if(status.equals("9")) { clientTransaction.setStatus("0"); }
                    else { clientTransaction.setStatus("201"); }
                }
                else { clientTransaction.setStatus("1"); }

                entityManager.SAVE(clientTransaction);

            } catch (DatabaseException e) {
                logger.error("Error of load previousTransaction 1", e);
                e.printStackTrace();
            } catch (Exception ee) {
                logger.error("Error of load previousTransaction 2", ee);
                ee.printStackTrace();
            }
        }
    }
    // OperatorTransaction
    private void updateTransactionStatus(OperatorBalanceTransaction operatorTransaction,  CitypayMessage citypayMessage)
    {
        if(operatorTransaction != null)
        {
            try {
                EntityManager entityManager = new EntityManager();

                String status = citypayMessage.getStatus();
                if(isEndedCode(status))
                {
                    if(status.equals("9")) { operatorTransaction.setStatus("0"); }
                    else { operatorTransaction.setStatus("201"); }
                }
                else { operatorTransaction.setStatus("1"); }

                entityManager.SAVE(operatorTransaction);

            } catch (DatabaseException e) {
                logger.error("Error of load previousTransaction 1", e);
                e.printStackTrace();
            } catch (Exception ee) {
                logger.error("Error of load previousTransaction 2", ee);
                ee.printStackTrace();
            }
        }
        
    }
    // SellerTransaction
    private void updateTransactionStatus(List<SellerBalanceTransaction> sellerTransactions,  CitypayMessage citypayMessage)
    {
        if(sellerTransactions != null)
        {
            for(SellerBalanceTransaction sellerTransaction : sellerTransactions)
            {
                if(sellerTransaction != null)
                {
                    try {
                        EntityManager entityManager = new EntityManager();

                        String status = citypayMessage.getStatus();
                        if(isEndedCode(status))
                        {
                            if(status.equals("9")) { sellerTransaction.setStatus("0"); }
                            else { sellerTransaction.setStatus("201"); }
                        }
                        else { sellerTransaction.setStatus("1"); }

                        entityManager.SAVE(sellerTransaction);

                    } catch (DatabaseException e) {
                        logger.error("Error of load previousTransaction 1", e);
                        e.printStackTrace();
                    } catch (Exception ee) {
                        logger.error("Error of load previousTransaction 2", ee);
                        ee.printStackTrace();
                    }
                }
            }
        }
    }
    // TerminalTransaction
    private void updateTransactionStatus(TerminalBalanceTransaction terminalTransaction, CitypayMessage citypayMessage)
    {
        if(terminalTransaction != null)
        {
            try {
                EntityManager entityManager = new EntityManager();

                String status = citypayMessage.getStatus();
                if(isEndedCode(status))
                {
                    if(status.equals("9")) { terminalTransaction.setStatus("0"); }
                    else { terminalTransaction.setStatus("201"); }
                }
                else { terminalTransaction.setStatus("1"); }

                entityManager.SAVE(terminalTransaction);

            } catch (DatabaseException e) {
                logger.error("Error of load previousTransaction 1", e);
                e.printStackTrace();
            } catch (Exception ee) {
                logger.error("Error of load previousTransaction 2", ee);
                ee.printStackTrace();
            }
        }
    }

    public String getStatusFromCitypay(String num)
    {
        String citypayStatus = "";

        CitypayTransaction citypayTransaction = null;
        ClientTransaction clientTransaction = null;
        OperatorBalanceTransaction operatorTransaction = null;
        List<SellerBalanceTransaction> sellerTransactions = null;
        TerminalBalanceTransaction terminalTransaction = null;

        EntityManager entityManager = null;
        try {
            entityManager = new EntityManager();

            CriterionWrapper criterions_1[] = new CriterionWrapper[]{Restrictions.eq("billTransactionNum", num)};
            CriterionWrapper criterions_2[] = new CriterionWrapper[]{Restrictions.eq("transactionGUID", num)};
            CriterionWrapper criterions_3[] = new CriterionWrapper[]{Restrictions.eq("transactionGUID", num)};
            CriterionWrapper criterions_4[] = new CriterionWrapper[]{Restrictions.eq("transactionGUID", num)};
            CriterionWrapper criterions_5[] = new CriterionWrapper[]{Restrictions.eq("transactionGUID", num)};

            citypayTransaction = (CitypayTransaction) entityManager.RETRIEVE(CitypayTransaction.class, null, criterions_1, null);
            clientTransaction = (ClientTransaction) entityManager.RETRIEVE(ClientTransaction.class, null, criterions_2, null);
            operatorTransaction = (OperatorBalanceTransaction) entityManager.RETRIEVE(OperatorBalanceTransaction.class, null, criterions_3, null);
            sellerTransactions = entityManager.RETRIEVE_ALL(SellerBalanceTransaction.class, criterions_4, null);
            terminalTransaction = (TerminalBalanceTransaction) entityManager.RETRIEVE(TerminalBalanceTransaction.class, null, criterions_5, null);

        } catch (DatabaseException e) {
            logger.error("Error of load previousTransaction 1", e);
            e.printStackTrace();
        } catch (Exception ee) {
            logger.error("Error of load previousTransaction 2", ee);
            ee.printStackTrace();
        }

        if(citypayTransaction != null)
        {

            String requestMessage = "";
            try {
                requestMessage = serializeMessage(citypayTransaction, "getTransactionStatus");
            } catch (HttpsRequestException e) {
                logger.error("Bad Citypay request", e);
            }

            // посылаем запрос на проверку статуса
            CitypayMessage citypayMessage = null;
            try {
                citypayMessage = executeCitypayRequest(requestMessage, citypayTransaction.getBillTransactionNum());
            } catch (HttpsRequestException e) {
                logger.error("Bad Citypay request", e);
            } catch(BadResponseException e) {
                logger.error("Bad Citypay request", e);
            }

            updateTransactionStatus(citypayTransaction,  citypayMessage);
            updateTransactionStatus(clientTransaction,  citypayMessage);
            updateTransactionStatus(operatorTransaction,  citypayMessage);
            updateTransactionStatus(sellerTransactions,  citypayMessage);
            updateTransactionStatus(terminalTransaction, citypayMessage);

            if(Integer.parseInt(citypayMessage.getStatus()) == Reference.EndedCode.PAYMENT_NOT_FOUND.code)
            {
                citypayStatus = "\""+citypayMessage.getStatus() + "\" - Payment Not Found";
            }
            if(Integer.parseInt(citypayMessage.getStatus()) == Reference.EndedCode.OK.code)
            {
                citypayStatus = "\""+citypayMessage.getStatus() + "\" - OK";
            }
            else
            {
                citypayStatus = "\""+citypayMessage.getStatus() + "\"" + ", " + citypayMessage.getResponseMessage();
            }
        }
        else
        {
            citypayStatus = "Payment Not Found On CityPay Gate";
        }
        return citypayStatus;
    }

    public void runBillMessage(BillingMessage billingMessage) throws HttpsGateException
    {
        BillingMessageImpl respMessage = new BillingMessageImpl();
        respMessage.setAttributeString(BillingMessage.REQUEST_ID,
                billingMessage.getAttributeString(BillingMessage.REQUEST_ID));
        try{
            // refill payment
            if(BillingMessage.O_REFILL_PAYMENT.equals(billingMessage.getOperationName())){
                refillPayment(billingMessage, respMessage);
            }
            else if(BillingMessage.O_REFILL_PAYMENT_REDIRECT.equals(billingMessage.getOperationName())){
                refillPayment(billingMessage, respMessage);
            }

            // unsupported operation
            else{
                respMessage.setStatusCode(StatusDictionary.SUBMIT_TRANSACTION_ERROR);
                throw new HttpsGateException("Unsupported command",
                           new String[]{billingMessage.getOperationName()});
            }
        }
        catch(Throwable e){
            if(respMessage.getAttributeString(BillingMessage.STATUS_CODE) == null)
                respMessage.setStatusCode(StatusDictionary.UNKNOWN_ERROR);
            respMessage.setAttributeString(BillingMessage.STATUS_MESSAGE, e.getLocalizedMessage());
        }
        finally{
            // send response to billing engine




            try{
                access.obtainConnection().processAsync(respMessage);
            }
            catch(Throwable e){
                logger.error(e.getLocalizedMessage(), e);
            }
        }
    }


    public String getBalance() throws HttpsRequestException, HttpsGateException
    {
        CitypayTransaction citypayTransaction = new CitypayTransaction();
        String requestMessage = "";
        try {
            requestMessage = serializeMessage(citypayTransaction, "getSellerLimit");
        } catch (HttpsRequestException e) {
            logger.error("Failed to serialized Citypay message", e);
        }

        CitypayMessage citypayResponse = null;
        try {
            citypayResponse = executeCitypayRequest(requestMessage, citypayTransaction.getBillTransactionNum());
        } catch(HttpsRequestException e) {
            logger.error("Bad Citypay request", e);
        } catch(BadResponseException e) {
            logger.error("Bad Citypay request", e);
        }

        int size = citypayResponse.getLimit().length();
        StringBuffer sb = new StringBuffer(citypayResponse.getLimit());
        sb.insert(size-2, ".");

        return sb.toString();
    }

    // not use because is not working properly
    public void setCurrent(CitypayTransaction citypayTransaction)
    {
        try {
            ReentrantLock myLock = new ReentrantLock();
            myLock.lock();
            try {
                EntityManager entityManager = new EntityManager();

                CriterionWrapper criterions[] = new CriterionWrapper[] {
                        Restrictions.eq("isLast", true)};

                CitypayTransaction previousTransaction = null;

                    previousTransaction = (CitypayTransaction) entityManager.RETRIEVE(CitypayTransaction.class, null, criterions, null);
                    previousTransaction.setIsLast(false);
                    entityManager.SAVE(previousTransaction);

                    int prevBillTransactionNumINT = Integer.parseInt(previousTransaction.getBillTransactionNumINT());
                    citypayTransaction.setBillTransactionNumINT(++prevBillTransactionNumINT + "");

                    citypayTransaction.setIsLast(true);
                    entityManager.SAVE(citypayTransaction);
            }
            finally {
                myLock.unlock();
            }
        } catch (DatabaseException e) {
            logger.error("Error of load previousTransaction 1", e);
            e.printStackTrace();
        } catch (Exception ee) {
            logger.error("Error of load previousTransaction 2", ee);
            ee.printStackTrace();
        }
    }


    // not use because is not working properly
    public void setCurrent2(CitypayTransaction citypayTransaction)
    {
        ReentrantLock myLock = new ReentrantLock();
        myLock.lock();
        try {
            EntityManager entityManager = new EntityManager();

            Date startDate = new Date();
            startDate.setYear(citypayTransaction.getTransactionTime().getYear());
            startDate.setMonth(citypayTransaction.getTransactionTime().getMonth());
            startDate.setDate(citypayTransaction.getTransactionTime().getDate()-1);
            startDate.setHours(citypayTransaction.getTransactionTime().getHours()-1);
            startDate.setMinutes(0);
            startDate.setSeconds(0);

            CitypayTransaction previousTransaction = null;
            List previousTransactionSet = null;
            try {
                previousTransactionSet = entityManager.EXECUTE_QUERY(CitypayTransaction.class,
                                                                    "dateSet",
                                                                    new QueryParameterWrapper[] {
                                                                            new QueryParameterWrapper("after", startDate),
                                                                            new QueryParameterWrapper("before", citypayTransaction.getTransactionTime())
                                                                    });
                Object[] ret = new Object[2];
                for (int i = 0; i < previousTransactionSet.size(); i++) {
                    Object[] ret2 = (Object[]) previousTransactionSet.get(i);
                    if(ret2[1] != null) {
                        ret = ret2;
                    }
                }
                CriterionWrapper criterions[] = new CriterionWrapper[] {
                    Restrictions.eq("transactionTime", ret[0])};
                previousTransaction = (CitypayTransaction) entityManager.RETRIEVE(CitypayTransaction.class, null, criterions, null);
                previousTransaction.setIsLast(false);
                entityManager.SAVE(previousTransaction);
                int prevBillTransactionNumINT = Integer.parseInt(previousTransaction.getBillTransactionNumINT());
                citypayTransaction.setBillTransactionNumINT(++prevBillTransactionNumINT + "");
                citypayTransaction.setIsLast(true);
                entityManager.SAVE(citypayTransaction);
            } catch (DatabaseException e) {
                logger.error("Error of load previousTransaction 1", e);
                e.printStackTrace();
            } catch (Exception ee) {
                logger.error("Error of load previousTransaction 2", ee);
                ee.printStackTrace();
            }

        } finally {
            myLock.unlock();
        }
    }

    private void rollToFalseValid(CitypayTransaction citypayTransaction) {
        citypayTransaction.setValid(0);
        EntityManager entityManager = new EntityManager();
        try {
            entityManager.SAVE(citypayTransaction);
        } catch (DatabaseException e) {
            e.printStackTrace();
        }
    }

    private void refillPayment(BillingMessage billingMessage, BillingMessageImpl respMessage)
    {
        // сохраняем клиентские данные по транзакции в бд
        respMessage.setOperationName(BillingMessage.O_REFILL_PAYMENT_RESULT);        // response.refillPayment

        EntityManager entityManager = new EntityManager();

        CitypayTransaction citypayTransaction = new CitypayTransaction();
        citypayTransaction.setValid(1);
        citypayTransaction.setTransactionTime(new Date());

        String account = billingMessage.getAttributeString(BillingMessage.ACCOUNT_NUMBER);
        if(account==null || account.isEmpty() || account.equals("")) // если номера телефона нет
        {
            account = billingMessage.getAttributeString(BillingMessage.PAY_ACCOUNT_NUMBER);
            citypayTransaction.setAccount(account);
            citypayTransaction.setServiceNameByBill(billingMessage.getAttributeString(BillingMessage.SERVICE_CODE)+"_pAccount");
        }
        else // если номер есть
        {
            citypayTransaction.setAccountPre(getAccountPre(prepareAccount(account)));
            citypayTransaction.setAccount(getAccountPost(getAccountPre(account), prepareAccount(account)));
            citypayTransaction.setServiceNameByBill(billingMessage.getAttributeString(BillingMessage.SERVICE_CODE));
        }

        //citypayTransaction.setServiceCodeByCitypay(codeMapping.getCitypayServiceCode(billingMessage.getAttributeString(BillingMessage.SERVICE_CODE)));
        citypayTransaction.setServiceCodeByCitypay(codeMapping.getCitypayServiceCode(citypayTransaction.getServiceNameByBill()));

        citypayTransaction.setBillTransactionNum(billingMessage.getAttributeString(BillingMessage.BILL_TRANSACTION_NUM));

        String amount = billingMessage.getAttributeString(BillingMessage.AMOUNT);
        if(amount.contains("."))
        {
            String post = billingMessage.getAttributeString(BillingMessage.AMOUNT).
                    substring(billingMessage.getAttributeString(BillingMessage.AMOUNT).indexOf(".")+1);
            if(post.length()==2)
            {
                citypayTransaction.setAmount(new Integer(billingMessage.getAttributeString(BillingMessage.AMOUNT).replace(".", "")));
            }
            else if(post.length()==1)
            {
                citypayTransaction.setAmount(new Integer(billingMessage.getAttributeString(BillingMessage.AMOUNT).replace(".", "")+"0"));
            }
        }
        else
        {
            int b = Integer.parseInt(billingMessage.getAttributeString(BillingMessage.AMOUNT));
            b *= 100;
            citypayTransaction.setAmount(b);
        }

        citypayTransaction.setTerminal(billingMessage.getAttributeString(BillingMessage.TERMINAL_SN));
        citypayTransaction.setGateName(getGateName());

        try {
            entityManager.SAVE(citypayTransaction);


            CriterionWrapper criterions[] = new CriterionWrapper[] {
                Restrictions.eq("billTransactionNum", citypayTransaction.getBillTransactionNum())};

            CitypayTransaction cPayTransaction = (CitypayTransaction) entityManager.RETRIEVE(CitypayTransaction.class, null, criterions, null);
            citypayTransaction.setBillTransactionNumINT(cPayTransaction.getCitypayTransactionId()+"");

            entityManager.SAVE(citypayTransaction);


        } catch (DatabaseException e) {
            logger.error("Failed to pre-save Citypay transaction", e);
        }

        // Получаем строку с посылаемым запросом
        String requestMessage = "";
        try {
            requestMessage = serializeMessage(citypayTransaction, "refillPayment");
        } catch (HttpsRequestException e) {
            logger.error("Failed to serialized Citypay message", e);
        }

        CitypayMessage citypayResponse = null;

        // Посылаем запрос и получаем ответ
        try {
            logger.info("Request: " + "BillNumber=" + citypayTransaction.getBillTransactionNum() + "\r\n" + requestMessage);  // посылаемый запрос
            System.out.println("Request: " + "BillNumber=" + citypayTransaction.getBillTransactionNum() + "\r\n" + requestMessage);

            citypayResponse = executeCitypayRequest(requestMessage, citypayTransaction.getBillTransactionNum());


            if(citypayResponse == null) // ответа нет
            {
                respMessage.setAttributeString(BillingMessage.STATUS_CODE, StatusDictionary.SUBMIT_TRANSACTION_ERROR + "");
                citypayTransaction.setStatusCodeByCitypay("error");
                citypayTransaction.setStatusMessageByCitypay("submit transaction error");
                respMessage.setStatusCode(StatusDictionary.BEGIN_TRANSACTION_ERROR);

            }
            else // ответ есть
            {
                // Обновляем запись в citypayTransaction
                if(!citypayResponse.getStatus().isEmpty() || citypayResponse.getStatus() != null)
                {
                    citypayTransaction.setStatusCodeByCitypay(citypayResponse.getStatus());
                    citypayTransaction.setStatusMessageByCitypay(citypayResponse.getResponseMessage());
                }
                citypayTransaction.setStatusCodeByCitypay(citypayResponse.getStatus());

                if(isEndedCode(citypayTransaction.getStatusCodeByCitypay()))
                {
                    citypayTransaction.setValid(0);
                }

                boolean transactionEnded = false;

                if(!isEndedCode(citypayTransaction.getStatusCodeByCitypay()) &&     // если не попадает в список конечных
                        isNotEndedCode(citypayTransaction.getStatusCodeByCitypay()))   // и попадает в список неконечных
                {
                    while(transactionEnded == false)
                    {
                        try {
                            Thread.sleep(Long.parseLong(properties.getProperty(PROP_SLEEP_BEFORE_STATUS_REQEST, "30000")));
                        } catch (InterruptedException e) {
                            logger.error("Failed to sleep before 'getStatus' request", e);
                        }

                        // Получаем строку с посылаемым запросом на получение статуса
                        try {
                            requestMessage = serializeMessage(citypayTransaction, "getTransactionStatus");

                        } catch (HttpsRequestException e) {
                            logger.error("Bad Citypay request", e);
                        }

                        // посылаем запрос на проверку статуса
                        try {
                            logger.info("Request GetStatus: " + "BillNumber=" + citypayTransaction.getBillTransactionNum() + "\r\n" + requestMessage);  // посылаемый запрос
                            System.out.println("Request GetStatus: " + "BillNumber=" + citypayTransaction.getBillTransactionNum() + "\r\n" + requestMessage);

                            citypayResponse = executeCitypayRequest(requestMessage, citypayTransaction.getBillTransactionNum());
                        } catch (HttpsRequestException e) {
                            logger.error("Bad Citypay request", e);
                        } catch(BadResponseException e) {
                            logger.error("Bad Citypay request", e);
                        }

                        // обновляем запись в citypayTransaction
                        if(!citypayResponse.getStatus().isEmpty() || citypayResponse.getStatus() != null)
                            citypayTransaction.setStatusMessageByCitypay(citypayResponse.getResponseMessage());

                        citypayTransaction.setStatusCodeByCitypay(citypayResponse.getStatus());
                        citypayTransaction.setCitypayTransactionNum(citypayResponse.getPaymentID());

                        if(!isNotEndedCode(citypayTransaction.getStatusCodeByCitypay()) &&  // если не попадает в список неконечных
                                isEndedCode(citypayTransaction.getStatusCodeByCitypay()))   // и попадает в список конечных
                        {
                            transactionEnded = true;

                            if(citypayTransaction.getStatusCodeByCitypay().equals(Reference.EndedCode.DUPLICATE_CHECK.code+""))
                            {
                                try {
                                    entityManager.DELETE(CitypayTransaction.class, citypayTransaction.getId());
                                } catch (DatabaseException e) {
                                    e.printStackTrace();
                                }
                            }

                            citypayTransaction.setStatusMessageByCitypay(citypayResponse.getProviderStatusName());
                        }
                        else
                            if(isNotEndedCode(citypayTransaction.getStatusCodeByCitypay()) &&  // если попадает в список неконечных
                                !isEndedCode(citypayTransaction.getStatusCodeByCitypay()))   // и непопадает в список конечных
                            {
                                transactionEnded = false;
                            }
                            else                                                             // Неизвестный ответ
                            {
                                transactionEnded = true;
                                respMessage.setAttributeString(BillingMessage.STATUS_MESSAGE, "Unknown Error");
                                respMessage.setStatusCode(StatusDictionary.BEGIN_TRANSACTION_ERROR);
                            }
                    }

                }
                else
                {
                    respMessage.setAttributeString(BillingMessage.STATUS_MESSAGE, "Unknown Error");
                    respMessage.setStatusCode(StatusDictionary.BEGIN_TRANSACTION_ERROR);
                }
            }

            // Сохраняем в бд
            try {
                entityManager.SAVE(citypayTransaction);
            } catch (DatabaseException e) {
                logger.error("Failed to post-save Citypay transaction", e);
            }

            if(citypayTransaction.getStatusCodeByCitypay().equals(Reference.EndedCode.OK.code+""))
                respMessage.setStatusCode(StatusDictionary.STATUS_OK);
            else
            {
                respMessage.setStatusCode(StatusDictionary.BEGIN_TRANSACTION_ERROR);
            }

        } catch(HttpsRequestException e) {
            //rollToFalseValid(citypayTransaction);
            logger.error("Bad Citypay request", e);
        } catch(BadResponseException e) {
            //rollToFalseValid(citypayTransaction);
            logger.error("Bad Citypay request", e);
        }

    }

    private CitypayMessage executeCitypayRequest(String requestMessage, String billNum) throws HttpsRequestException, BadResponseException
    {
        CitypayMessage responseMessage = null;
        PostMethod method = new PostMethod(properties.getProperty(PROP_URL));
        int soTimeout = 120000;
        soTimeout = Integer.parseInt(properties.getProperty("socketTimeout", "120000"));
        method.setRequestBody(requestMessage);
        method.setRequestHeader("Content-Type", "text/xml; charset=utf-8");
        try{
            HttpClient httpClient = new HttpClient();

            HttpClientParams httpClientParams = new HttpClientParams();
            httpClientParams.setSoTimeout(soTimeout);
            httpClient.setParams(httpClientParams);

            int resp = httpClient.executeMethod(method);

            String respon = CyrillicToTranslitConverter.toTranslitString(method.getResponseBodyAsString());
            logger.info("Response: " + "BillNumber=" + billNum + "\r\n" + respon);  // полученный ответ
            System.out.println("Response: " + "BillNumber=" + billNum + "\r\n" + respon);

            if(method.getStatusCode() != HttpStatus.SC_OK)
                throw new HttpsRequestException("Incorrect response code from server " + method.getStatusCode());
            String strResp = method.getResponseBodyAsString();
           // logger.info(strResp);
            responseMessage = CitypayMessage.parseResponse(strResp, resp);
        }
        catch(Exception e){
             e.printStackTrace();
            logger.error(e);
            throw new HttpsRequestException("Request exception " + e, e);
        }
        finally{
            method.releaseConnection();
        }

        return responseMessage;
    }

    // преобразование номера телефона к нужному формату (без 38)
    private String prepareAccount(String account)
    {
        if(account.length() == 12) account = account.substring(2);
        if(account.length() == 11) account = account.substring(1);
        if(account.length() == 9) account = "0" + account;

        return account;
    }

    // получить префикс
    private String getAccountPre(String account)
    {
        return account.substring(0, 3);
    }

    // получить номер без префикса
    private String getAccountPost(String accountPre, String account)
    {
        return account.substring(account.indexOf(accountPre)+2);
    }

    // создание xml запроса
    private String serializeMessage(CitypayTransaction citypayTransaction, String requestType) throws HttpsRequestException
    {
        String requestMessage = "";
        Document doc = null;

        try {
            doc = DocumentBuilderFactory.newInstance().newDocumentBuilder().newDocument();

            Element soapEnvelope = doc.createElement("soap:Envelope");
            soapEnvelope.setAttribute("xmlns:soap", "http://schemas.xmlsoap.org/soap/envelope/");
            soapEnvelope.setAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
            soapEnvelope.setAttribute("xmlns:xsd", "http://www.w3.org/2001/XMLSchema");

            Element body = doc.createElement("soap:Body");

            doc.appendChild(soapEnvelope);
            soapEnvelope.appendChild(body);

            if(requestType.equals("refillPayment"))
            {
                Element processPayment = doc.createElement("ProcessPayment");
                processPayment.setAttribute("xmlns", "http://usmp.com.ua/");

                Element request = doc.createElement("request");

                Element serial = doc.createElement("Serial");
                serial.setTextContent(properties.getProperty(PROP_SERIAL));

                Element keyWord = doc.createElement("KeyWord");
                keyWord.setTextContent(properties.getProperty(PROP_KEYWORD));

                Element payments = doc.createElement("Payments");

                Element paymentDetails = doc.createElement("PaymentDetails");

                Element date = doc.createElement("Date");

                SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd");
                SimpleDateFormat sdf2 = new SimpleDateFormat("HH:mm:");
                SimpleDateFormat sdf3 = new SimpleDateFormat("ss.SS");
                String transDate = sdf1.format(citypayTransaction.getTransactionTime())
                        + "T" + sdf2.format(citypayTransaction.getTransactionTime())
                        + sdf3.format(citypayTransaction.getTransactionTime())
                        + "000+02:00";
                date.setTextContent(transDate);

                Element payElementID = doc.createElement("PayElementID");
                payElementID.setTextContent(citypayTransaction.getServiceCodeByCitypay());

                Element account = doc.createElement("Account");
                if(citypayTransaction.getAccountPre() == null)
                    account.setTextContent(citypayTransaction.getAccount());
                else
                    account.setTextContent("(" + citypayTransaction.getAccountPre() + ")" + citypayTransaction.getAccount());

                Element amount = doc.createElement("Amount");
                amount.setTextContent(citypayTransaction.getAmount().toString());

                Element partnerPointID = doc.createElement("PartnerPointID");
                partnerPointID.setTextContent(citypayTransaction.getTerminal());

                Element chequeNumber = doc.createElement("ChequeNumber");

                chequeNumber.setTextContent(citypayTransaction.getBillTransactionNumINT());

                body.appendChild(processPayment);
                processPayment.appendChild(request);
                request.appendChild(serial);
                request.appendChild(keyWord);
                request.appendChild(payments);
                payments.appendChild(paymentDetails);
                paymentDetails.appendChild(date);
                paymentDetails.appendChild(payElementID);
                paymentDetails.appendChild(account);
                paymentDetails.appendChild(amount);
                paymentDetails.appendChild(partnerPointID);
                paymentDetails.appendChild(chequeNumber);
            }
            if(requestType.equals("getTransactionStatus"))
            {
                Element getStatus = doc.createElement("GetStatus");
                getStatus.setAttribute("xmlns", "http://usmp.com.ua/");

                Element request = doc.createElement("request");

                Element serial = doc.createElement("Serial");
                serial.setTextContent(properties.getProperty(PROP_SERIAL));

                Element keyWord = doc.createElement("KeyWord");
                keyWord.setTextContent(properties.getProperty(PROP_KEYWORD));

                Element chequeNumber = doc.createElement("ChequeNumbers");

                Element inT = doc.createElement("int");
                inT.setTextContent(citypayTransaction.getBillTransactionNumINT());

                body.appendChild(getStatus);
                getStatus.appendChild(request);
                request.appendChild(serial);
                request.appendChild(keyWord);
                request.appendChild(chequeNumber);
                chequeNumber.appendChild(inT);
            }
            if(requestType.equals("getSellerLimit"))
            {
                Element getLimit = doc.createElement("GetLimit");
                getLimit.setAttribute("xmlns", "http://usmp.com.ua/");

                Element request = doc.createElement("request");

                Element serial = doc.createElement("Serial");
                serial.setTextContent(properties.getProperty(PROP_SERIAL));

                Element keyWord = doc.createElement("KeyWord");
                keyWord.setTextContent(properties.getProperty(PROP_KEYWORD));

                body.appendChild(getLimit);
                getLimit.appendChild(request);
                request.appendChild(serial);
                request.appendChild(keyWord);
            }
            if(requestType.equals("getValidatePhone"))
            {
                Element validatePhone = doc.createElement("ValidatePhone");
                validatePhone.setAttribute("xmlns", "http://usmp.com.ua/");

                Element request = doc.createElement("request");

                Element account = doc.createElement("Account");
                if(citypayTransaction.getAccountPre().isEmpty())
                    account.setTextContent(citypayTransaction.getAccount());
                else
                    account.setTextContent("(" + citypayTransaction.getAccountPre() + ")" + citypayTransaction.getAccount());

                Element payElementID = doc.createElement("PayElementID");
                payElementID.setTextContent(citypayTransaction.getServiceCodeByCitypay());

                Element serial = doc.createElement("Serial");
                serial.setTextContent(properties.getProperty(PROP_SERIAL));

                Element keyWord = doc.createElement("KeyWord");
                keyWord.setTextContent(properties.getProperty(PROP_KEYWORD));

                body.appendChild(validatePhone);
                validatePhone.appendChild(request);
                request.appendChild(account);
                request.appendChild(payElementID);
                request.appendChild(serial);
                request.appendChild(keyWord);
            }
        } catch (ParserConfigurationException e) {
            logger.error(e);
        } catch(Exception e){
            throw new HttpsRequestException("Failed to serialize XML");
        }
        requestMessage = serialize(doc);
        System.out.println(requestMessage);
        return requestMessage;
    }

    private String serialize(Document doc) throws HttpsRequestException
    {
        String xml = "";

        try {
            OutputFormat format = new OutputFormat(doc);
            StringWriter stringOut = new StringWriter();
            XMLSerializer serialz = new XMLSerializer(stringOut, format);

            serialz.serialize(doc);

            xml = stringOut.toString();

        } catch(Exception e){
            throw new HttpsRequestException("Failed to serialize XML");
        }

        return xml;
    }

    public boolean isEndedCode(String code)
    {
        boolean flag = false;
        int num = Integer.parseInt(code);

        if(num == Reference.EndedCode.PAYMENT_DECLINED.code ||
            num == Reference.EndedCode.CANCELED.code ||
            num == Reference.EndedCode.OK.code ||
            num == Reference.EndedCode.INVALID_SIGNATURE.code ||
            num == Reference.EndedCode.INVALID_POINT.code ||
            num == Reference.EndedCode.INVALID_PAY_ELEMENT.code ||
            num == Reference.EndedCode.DUPLICATE_CHECK.code ||
            num == Reference.EndedCode.INCORRECT_AMOUNT.code ||
            num == Reference.EndedCode.INCORRECT_BENEFICIARY_DETAIL.code ||
            num == Reference.EndedCode.INCORRECT_BILLING_INFO.code ||
            num == Reference.EndedCode.WRONG_PAY_ELEMENT.code ||
            num == Reference.EndedCode.PAYMENT_NOT_FOUND.code ||
            num == Reference.EndedCode.SERVER_ERROR.code)
            flag = true;

        return flag;
    }

    public boolean isNotEndedCode(String code)
    {
        boolean flag = false;
        int num = Integer.parseInt(code);

        if(num == Reference.NotEndedCode.PAYMENT_ACCEPTED1.code ||
            num == Reference.NotEndedCode.PAYMENT_ACCEPTED2.code ||
            num == Reference.NotEndedCode.CHECK.code ||
            num == Reference.NotEndedCode.APPLICATION_SENT.code ||
            num == Reference.NotEndedCode.INSUFFICIENT_FUNDS1.code ||
            num == Reference.NotEndedCode.CREATING_TRANSACTION.code ||
            num == Reference.NotEndedCode.STARTED_TREATMENT.code ||
            num == Reference.NotEndedCode.IMPORTED.code ||
            num == Reference.NotEndedCode.PROHIBITED_POINT.code ||
            num == Reference.NotEndedCode.BANNED_CARD.code ||
            num == Reference.NotEndedCode.PROHIBITED_ITEM_PAYMENT.code ||
            num == Reference.NotEndedCode.INSUFFICIENT_FUNDS2.code)
            flag = true;

        return flag;
    }

    private class CodeMapping
    {
        private Map<String,String> articleMapping;

        private void init() throws InitException
        {
            articleMapping = new HashMap<String,String>();
            try {
                logger.debug("Start initialization of the CityPay mapping");
                String citypayMappingFile = Configuration.getInstance().getProperty("citypay.mapping_file");
                if (citypayMappingFile == null)
                    throw new InitException("err.citypay_gate.mapping_file_is_not_specified_in_config");

                File configFile = new File(citypayMappingFile);
                Digester d = new Digester();
                d.push(this);

                d.addCallMethod("citypayMapping/article", "addArticleMapping", 2);
                d.addCallParam("citypayMapping/article", 0, "svbillCode");
                d.addCallParam("citypayMapping/article", 1, "citypayCode");
                d.parse(configFile);

                logger.debug("Successful completed initialization of the CityPay mapping");

            } catch (Exception e){
                throw new InitException("Failed to init CityPay gate mapping", e);
            }
        }

        public void addArticleMapping(String billServiceCode, String citypayServiceCode)
        {
            articleMapping.put(billServiceCode, citypayServiceCode);
        }

        private String getCitypayServiceCode(String billServiceCode){
            return articleMapping.get(billServiceCode);
        }

    }

    /** Citypay protocol reference. */
    private static class Reference
    {
        private enum EndedCode
        {
            PAYMENT_DECLINED(4),                // Платеж отклонен
            CANCELED(7),                        // Отменен
            OK(9),                              // Исполнено
            INVALID_SIGNATURE(101),             // Неверная подпись
            INVALID_POINT(102),                 // Неверная точка
            INVALID_PAY_ELEMENT(105),           // Неверный номер платежного элемента
            DUPLICATE_CHECK(108),               // Дублирование номера чека
            INCORRECT_AMOUNT(112),              // Неверная сумма платежа
            INCORRECT_BENEFICIARY_DETAIL(113),  // Неверные реквизиты получателя
            INCORRECT_BILLING_INFO(120),        // Неверные платежные дан-ные
            WRONG_PAY_ELEMENT(121),             // Неверный номер платежного элемента
            PAYMENT_NOT_FOUND(123),             // Платеж не найден
            SERVER_ERROR(2000);                 // Ошибка на стороне сервера

            private int code;

            private EndedCode(int code)
            {
                this.code = code;
            }
        }

        private enum NotEndedCode
        {
            PAYMENT_ACCEPTED1(0),               // Платеж принят
            PAYMENT_ACCEPTED2(1),               // Платеж принят
            CHECK(2),                           // Проверка
            APPLICATION_SENT(5),                // Заявка отправлена
            INSUFFICIENT_FUNDS1(8),             // Недостаточно средств
            CREATING_TRANSACTION(98),           // Создание транзакции
            STARTED_TREATMENT(99),              // Начата обработка
            IMPORTED(100),                      // Импортирован
            PROHIBITED_POINT(103),              // Запрещенный статус точки
            BANNED_CARD(104),                   // Запрещенный статус карты
            PROHIBITED_ITEM_PAYMENT(107),       // Запрещенный элемент платежей
            INSUFFICIENT_FUNDS2(110);           // Недостаточно средств

            private int code;

            private NotEndedCode(int code)
            {
                this.code = code;
            }
        }
    }


}
