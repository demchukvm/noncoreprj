package com.tmx.web.actions.core.restriction.operator_restriction;

import com.tmx.web.actions.core.restriction.AbstractManageRestrictionsAction;
import com.tmx.web.forms.core.restriction.operator_restriction.ManageOperatorRestrictionsForm;
import com.tmx.web.controls.ScrollBox;
import com.tmx.as.blogic.OperatorManager;

import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForm;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class ManageOperatorRestrictionsAction extends AbstractManageRestrictionsAction {

    public ActionForward preloadForDelete(ActionMapping mapping,
                                          ActionForm form,
                                          HttpServletRequest request,
                                          HttpServletResponse response) throws Throwable {
        getRoadmap().collectParameter("/core/message/confirmation", "leftMenuName", "restrictions");
        getRoadmap().setPageForward("confirmation", "onOk", "local:" + this.getName() + "#" + "deleteRestrictionAction");
        getRoadmap().setPageForward("confirmation", "onCancel", "local:" + this.getName() + "#" + "manageRestrictionsAction");
        return mapping.findForward("confirmation");
    }

    public ActionForward delete(ActionMapping mapping,
                              ActionForm form,
                              HttpServletRequest request,
                              HttpServletResponse response) throws Throwable {
        ManageOperatorRestrictionsForm restrictionsForm = (ManageOperatorRestrictionsForm) form;
        return defaultAction(mapping, form, request, response, null);
    }

    public ActionForward defaultAction(ActionMapping mapping, ActionForm form, HttpServletRequest request,
                                       HttpServletResponse response, String methodName) throws Throwable {
        ManageOperatorRestrictionsForm restrictionsForm = (ManageOperatorRestrictionsForm) form;
        restrictionsForm.bindOperators(new OperatorManager().getAllOperators());
        return super.defaultAction(mapping, form, request, response, methodName);
    }

    public ActionForward onCreateRestrictionAction(ActionMapping mapping,
                                       ActionForm form,
                                       HttpServletRequest request,
                                       HttpServletResponse response) throws Throwable {
        ManageOperatorRestrictionsForm manageSellerForm = (ManageOperatorRestrictionsForm) form;
        String selectedKey = ((ScrollBox) manageSellerForm.getRestrictionsTable().getParameterControl("by_operator_id", "operator_id")).getSelectedKey();
        getRoadmap().collectParameter("/core/restriction/operator_restriction/infoOperatorRestriction", PARAMETER_OWNER_NAME, selectedKey);
        return mapping.findForward("createRestrictionAction");
    }
}
