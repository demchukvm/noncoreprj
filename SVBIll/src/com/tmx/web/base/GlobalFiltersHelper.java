package com.tmx.web.base;

import com.tmx.as.base.FilterWrapper;
import com.tmx.as.base.EntityManager;

import java.util.HashSet;
import java.util.Set;
import java.util.Iterator;

/**
 */
public class GlobalFiltersHelper {
    private Set globalFilters = null;

    GlobalFiltersHelper(){
        clean();
    }

    public void clean(){
        globalFilters = new HashSet();
    }

    public void addGlobalFilter(FilterWrapper filter){
        globalFilters.add(filter);
    }

    public synchronized void applyGlobalFilters(){
        EntityManager em = new EntityManager();
        em.cleanThreadGlobalFilters();
        for(Iterator filtersIter = globalFilters.iterator(); filtersIter.hasNext();){
            FilterWrapper filterWrapper = (FilterWrapper)filtersIter.next();
            em.setThreadGlobalFilter(filterWrapper);
        }
    }
}
