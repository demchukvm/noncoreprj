package com.tmx.web.forms.core.tools;

import com.tmx.as.blogic.SellerManager;
import com.tmx.as.blogic.TerminalManager;
import com.tmx.as.entities.bill.terminal.Terminal;
import com.tmx.as.exceptions.DatabaseException;
import com.tmx.util.i18n.MessageResources;
import com.tmx.web.base.BasicActionForm;
import com.tmx.web.controls.Button;
import com.tmx.web.controls.EditBox;
import com.tmx.web.controls.ScrollBox;
import com.tmx.web.controls.TimeSelector;
import com.tmx.web.controls.grid.Grid;
import com.tmx.web.controls.grid.GridException;

import java.util.*;

public class TestBuyVouchersForm extends BasicActionForm
{
    public final String SELLER_CODE = "GenLife";
    public final String SERVICE_CODE1 = "LifeExc";
    public final String SERVICE_CODE2 = "Life";
    public final String PROCESSING = "SVCard";
    public final String LOGIN = "SVCard";
    public final String PASSWORD = "T6%4+31@mZ";

    private Button bBuy;
    private Button bStatus;
    private Button bDownload;
    private Button bReset;

    private ScrollBox sbDistributor;
    private ScrollBox sbTerminalSn;
    private ScrollBox sbServiceCode;
    private ScrollBox sbVoucherNominal;

    private EditBox ebVoucherCount;
    private EditBox ebSellerCode;
    //private EditBox ebServiceCode;
    private EditBox ebCompliteCount;
    private EditBox ebStatus;

    private Button bGetVouchers;
    private TimeSelector from;
    private TimeSelector to;
    private Grid grid;

    private String fileUrl = "";

    protected void init()
    {
        initButtons();
        initScrollBoxes();
        initEditBoxes();
        initTimeSelectors();
        initGrid();
    }

    private void initButtons()
    {
        bBuy = new Button();
        bBuy.setLabelKey("ctrl.button.label.buy");
        bBuy.setAction("/TestBuyVouchersAction");
        bBuy.setCommand("buy");

        bStatus = new Button();
        bStatus.setLabelKey("ctrl.button.label.getStatus");
        bStatus.setAction("/TestBuyVouchersAction");
        bStatus.setCommand("getStatus");
        bStatus.setReadonly(true);

        bDownload = new Button();
        bDownload.setLabelKey("ctrl.button.label.download");
        bDownload.setAction("/TestBuyVouchersAction");
        bDownload.setCommand("download");
        bDownload.setReadonly(true);

        bReset = new Button();
        bReset.setLabelKey("ctrl.button.label.reset");
        bReset.setAction("/TestBuyVouchersAction");
        bReset.setCommand("reset");

        bGetVouchers = new Button();
        bGetVouchers.setLabelKey("ctrl.button.label.show");
        bGetVouchers.setAction("/TestBuyVouchersAction");
        bGetVouchers.setCommand("showVoucers");
    }

    private void initScrollBoxes()
    {
        sbDistributor = new ScrollBox<String[]>()
        {
            protected String getKey(String[] distributor){return distributor[0];}
            protected String getValue(String[] distributor){return distributor[1];}
        };
        sbDistributor.setNullable(true);
        sbDistributor.setMandatory(true);
        final List<String[]> distributor = new LinkedList<String[]>();
        distributor.add(new String[]{"Avancel", "Avancel"});
        //distributor.add(new String[]{"nPay", "nPay"});
        sbDistributor.bind(distributor);

        try {
            Long sellerId = new SellerManager().getSellerId(SELLER_CODE);
            sbTerminalSn = new ScrollBox()
            {
                protected String getKey(Object dataListElement){return ((Terminal) dataListElement).getSerialNumber();}
                protected String getValue(Object dataListElement){return ((Terminal) dataListElement).getSerialNumber();}
            };
            sbTerminalSn.bind(new TerminalManager().getAllSellersTerminals(sellerId));
            sbTerminalSn.setMandatory(true);
            sbTerminalSn.setNullable(true);
        } catch (DatabaseException e) {
            e.printStackTrace();
        }

        sbVoucherNominal = new ScrollBox<String[]>()
        {
            protected String getKey(String[] nominal){return nominal[0];}
            protected String getValue(String[] nominal){return nominal[1];}
        };
        sbVoucherNominal.setNullable(true);
        sbVoucherNominal.setMandatory(true);
        final List<String[]> nominal = new LinkedList<String[]>();
        nominal.add(new String[]{"10", "LifeVoucher10"});
        nominal.add(new String[]{"25", "LifeVoucher25"});
        nominal.add(new String[]{"35", "LifeVoucher35"});
        nominal.add(new String[]{"50", "LifeVoucher50"});
        nominal.add(new String[]{"100", "LifeVoucher100"});
        sbVoucherNominal.bind(nominal);


        sbServiceCode = new ScrollBox<String[]>()
        {
            protected String getKey(String[] serviceCode){return serviceCode[0];}
            protected String getValue(String[] serviceCode){return serviceCode[1];}
        };
        sbServiceCode.setNullable(true);
        sbServiceCode.setMandatory(true);
        final List<String[]> serviceCode = new LinkedList<String[]>();
        serviceCode.add(new String[]{SERVICE_CODE1, SERVICE_CODE1});
        //serviceCode.add(new String[]{SERVICE_CODE2, SERVICE_CODE2});
        sbServiceCode.bind(serviceCode);
    }

    private void initEditBoxes()
    {
        ebVoucherCount = new EditBox();
        ebVoucherCount.setMandatory(true);
        ebVoucherCount.addValidationRule(EditBox.createValidationRule_INTEGER_FORMAT(getLocale()));

        ebSellerCode = new EditBox();
        ebSellerCode.setReadonly(true);
        ebSellerCode.setValue(SELLER_CODE);

//        ebServiceCode = new EditBox();
//        ebServiceCode.setReadonly(true);
//        ebServiceCode.setValue(SERVICE_CODE);

        ebCompliteCount = new EditBox();
        ebCompliteCount.setReadonly(true);
        ebCompliteCount.setValue("0 / 0");

        ebStatus = new EditBox();
        ebStatus.setReadonly(true);
    }

    private void initTimeSelectors()
    {
        GregorianCalendar calendar = new GregorianCalendar();
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.HOUR, 0);
        calendar.set(Calendar.SECOND, 0);

        from = new TimeSelector();
        from.setTime(calendar.getTime());
        to = new TimeSelector();
        to = new TimeSelector();   
    }

    private void initGrid()
    {
        grid = new Grid() {

            public void bind(List data) throws GridException
            {
                // делать проверку на пустой лист

                clear();

                List data1 = getListByDistributor(data, "Avancel"); // avancel_gate
                List data2 = getListByDistributor(data, "nPay");    // life_gate

                createHeader();
                createBody();
                createBodyData(data1, "Avancel");
                createBodyData(data2, "nPay");

                int count1 = getCountByDistributor(data1);
                int count2 = getCountByDistributor(data2);
                int totalCount = count1 + count2;

                double sum1 = getSumByDistributor(data1);
                double sum2 = getSumByDistributor(data2);
                double totalSum = sum1 + sum2;

                createBottomData(count1+"", sum1+"", "Avancel");
                createBottomData(count2+"", sum2+"", "nPay");

                createBottom(totalCount+"", totalSum+"");

            }

            private List getListByDistributor(List data, String distributor)
            {
                List list = new ArrayList();
                for(Object dat : data)
                {
                    Object[] d = (Object[]) dat;
                    if(d[0].equals(distributor))
                    {
                        list.add(d);
                    }
                }
                return list;
            }

            private int getCountByDistributor(List data)
            {
                int count = 0;
                if(data.size() > 0)
                {
                    for(Object dat : data)
                    {
                        Object[] d = (Object[]) dat;
                        count += Integer.parseInt(d[2].toString());
                    }
                }
                return count;
            }

            private double getSumByDistributor(List data)
            {
                double sum = 0;
                if(data.size() > 0)
                {
                    for(Object dat : data)
                    {
                        Object[] d = (Object[]) dat;
                        sum += Double.parseDouble(d[3]==null?"0":d[3].toString());
                    }
                }
                return sum;
            }

            private void createHeader() throws GridException
            {
                setCell(0, 0, getLable("nominal"), "column_header_cell", "font-weight: bold", 1, 2);
                setCell(0, 1, getLable("count"), "column_header_cell", "font-weight: bold", 2, 1);
                setCell(0, 3, getLable("sum"), "column_header_cell", "font-weight: bold", 2, 1);

                setCell(1, 1, getLable("avancel"), "column_header_cell", "font-weight: bold", 1, 1);
                setCell(1, 2, getLable("nPay"), "column_header_cell", "font-weight: bold", 1, 1);
                setCell(1, 3, getLable("avancel"), "column_header_cell", "font-weight: bold", 1, 1);
                setCell(1, 4, getLable("nPay"), "column_header_cell", "font-weight: bold", 1, 1);

            }

            private void createBody() throws GridException
            {
                setCell(2, 0, getLable("lifeVoucher10"), "column_data_cell", "font-weight: bold", 1, 1);
                setCell(3, 0, getLable("lifeVoucher25"), "column_data_cell", "font-weight: bold", 1, 1);
                setCell(4, 0, getLable("lifeVoucher35"), "column_data_cell", "font-weight: bold", 1, 1);
                setCell(5, 0, getLable("lifeVoucher50"), "column_data_cell", "font-weight: bold", 1, 1);
                setCell(6, 0, getLable("lifeVoucher100"), "column_data_cell", "font-weight: bold", 1, 1);
            }

            private void createBodyData(List data, String distributor) throws GridException
            {
                if(distributor.equals("Avancel"))
                {
                    for(Object dat : data)
                    {
                        Object[] d = (Object[]) dat;
                        if(d[1].equals("10"))
                        {
                            setCell(2, 1, d[2].toString(), "column_data_cell", 1, 1);
                            setCell(2, 3, d[3]==null?"0.0":d[3].toString(), "column_data_cell", 1, 1);
                        }
                        else if(d[1].equals("25"))
                        {
                            setCell(3, 1, d[2].toString(), "column_data_cell", 1, 1);
                            setCell(3, 3, d[3]==null?"0.0":d[3].toString(), "column_data_cell", 1, 1);
                        }
                        else if(d[1].equals("35"))
                        {
                            setCell(4, 1, d[2].toString(), "column_data_cell", 1, 1);
                            setCell(4, 3, d[3]==null?"0.0":d[3].toString(), "column_data_cell", 1, 1);
                        }
                        else if(d[1].equals("50"))
                        {
                            setCell(5, 1, d[2].toString(), "column_data_cell", 1, 1);
                            setCell(5, 3, d[3]==null?"0.0":d[3].toString(), "column_data_cell", 1, 1);
                        }
                        else if(d[1].equals("100"))
                        {
                            setCell(6, 1, d[2].toString(), "column_data_cell", 1, 1);
                            setCell(6, 3, d[3]==null?"0.0":d[3].toString(), "column_data_cell", 1, 1);   
                        }
                    }
                }
                else
                {

                    setCell(2, 2, "-", "column_data_cell", 1, 1);
                    setCell(2, 4, "-", "column_data_cell", 1, 1);


                    setCell(3, 2, "-", "column_data_cell", 1, 1);
                    setCell(3, 4, "-", "column_data_cell", 1, 1);


                    setCell(4, 2, "-", "column_data_cell", 1, 1);
                    setCell(4, 4, "-", "column_data_cell", 1, 1);


                    setCell(5, 2, "-", "column_data_cell", 1, 1);
                    setCell(5, 4, "-", "column_data_cell", 1, 1);


                    setCell(6, 2, "-", "column_data_cell", 1, 1);
                    setCell(6, 4, "-", "column_data_cell", 1, 1);
                }

            }

            private void createBottom(String totalCount, String totalSum) throws GridException
            {
                setCell(7, 0, getLable("total"), "column_data_cell", "font-weight: bold; color: green", 1, 2);

                setCell(8, 1, totalCount, "column_data_cell", "font-weight: bold; color: green", 2, 1);
                setCell(8, 3, totalSum, "column_data_cell", "font-weight: bold; color: green", 2, 1);
            }

            private void createBottomData(String count, String sum, String distributor) throws GridException {
                if(distributor.equals("Avancel"))
                {
                    setCell(7, 1, count, "column_data_cell", "font-weight: bold; color: green", 1, 1);
                    setCell(7, 3, sum, "column_data_cell", "font-weight: bold; color: green", 1, 1);
                }
                else
                {
                    setCell(7, 2, "-", "column_data_cell", "font-weight: bold; color: green", 1, 1);
                    setCell(7, 4, "-", "column_data_cell", "font-weight: bold; color: green", 1, 1);
                }
            }

            private String getLable(String key) {
                return MessageResources.getInstance().getOwnedLocalizedMessage(this.getClass(), "labels", getLocale(), key, null);
            }
        };
    }

    

    public Button getbBuy() {
        return bBuy;
    }

    public void setbBuy(Button bBuy) {
        this.bBuy = bBuy;
    }

    public Button getbStatus() {
        return bStatus;
    }

    public void setbStatus(Button bStatus) {
        this.bStatus = bStatus;
    }

    public Button getbDownload() {
        return bDownload;
    }

    public void setbDownload(Button bDownload) {
        this.bDownload = bDownload;
    }

    public Button getbReset() {
        return bReset;
    }

    public void setbReset(Button bReset) {
        this.bReset = bReset;
    }

    public ScrollBox getSbDistributor() {
        return sbDistributor;
    }

    public void setSbDistributor(ScrollBox sbDistributor) {
        this.sbDistributor = sbDistributor;
    }

    public ScrollBox getSbTerminalSn() {
        return sbTerminalSn;
    }

    public void setSbTerminalSn(ScrollBox sbTerminalSn) {
        this.sbTerminalSn = sbTerminalSn;
    }

    public ScrollBox getSbVoucherNominal() {
        return sbVoucherNominal;
    }

    public void setSbVoucherNominal(ScrollBox sbVoucherNominal) {
        this.sbVoucherNominal = sbVoucherNominal;
    }

    public EditBox getEbVoucherCount() {
        return ebVoucherCount;
    }

    public void setEbVoucherCount(EditBox ebVoucherCount) {
        this.ebVoucherCount = ebVoucherCount;
    }

    public EditBox getEbSellerCode() {
        return ebSellerCode;
    }

    public void setEbSellerCode(EditBox ebSellerCode) {
        this.ebSellerCode = ebSellerCode;
    }

//    public EditBox getEbServiceCode() {
//        return ebServiceCode;
//    }
//
//    public void setEbServiceCode(EditBox ebServiceCode) {
//        this.ebServiceCode = ebServiceCode;
//    }

    public ScrollBox getSbServiceCode() {
        return sbServiceCode;
    }

    public void setSbServiceCode(ScrollBox sbServiceCode) {
        this.sbServiceCode = sbServiceCode;
    }

    public EditBox getEbCompliteCount() {
        return ebCompliteCount;
    }

    public void setEbCompliteCount(EditBox ebCompliteCount) {
        this.ebCompliteCount = ebCompliteCount;
    }

    public EditBox getEbStatus() {
        return ebStatus;
    }

    public void setEbStatus(EditBox ebStatus) {
        this.ebStatus = ebStatus;
    }

//    public boolean isCrash1() {
//        return isCrash1;
//    }
//
//    public void setCrash1(boolean crash1) {
//        isCrash1 = crash1;
//    }
//
//    public boolean isCrash2() {
//        return isCrash2;
//    }
//
//    public void setCrash2(boolean crash2) {
//        isCrash2 = crash2;
//    }
//
//    public boolean isCrash3() {
//        return isCrash3;
//    }
//
//    public void setCrash3(boolean crash3) {
//        isCrash3 = crash3;
//    }

    public String getFileUrl() {
        return fileUrl;
    }

    public void setFileUrl(String fileUrl) {
        this.fileUrl = fileUrl;
    }

    public Button getbGetVouchers() {
        return bGetVouchers;
    }

    public void setbGetVouchers(Button bGetVouchers) {
        this.bGetVouchers = bGetVouchers;
    }

    public TimeSelector getFrom() {
        return from;
    }

    public void setFrom(TimeSelector from) {
        this.from = from;
    }

    public TimeSelector getTo() {
        return to;
    }

    public void setTo(TimeSelector to) {
        this.to = to;
    }

    public Grid getGrid() {
        return grid;
    }

    public void setGrid(Grid grid) {
        this.grid = grid;
    }
}
