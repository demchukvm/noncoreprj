package com.tmx.util;

import java.lang.ref.Reference;
import java.lang.ref.ReferenceQueue;
import java.lang.ref.WeakReference;
import java.util.AbstractList;
import java.util.ArrayList;
import java.util.List;

/**
 * Simple realization of weak list, like other weak structure
 * if you modify WeakList  automatically called method expungeStaleEntries() (cross-cutting concerns)
 */
public class WeakList extends AbstractList {

    private final ReferenceQueue queue = new ReferenceQueue();

    private final List list = new ArrayList();

    public boolean add(Object item) {
        expungeStaleEntries();
        return list.add( new WeakReference(item, queue) );
    }

    public Object get(int i) {
        expungeStaleEntries();
        return ((Reference)list.get(i)).get();
    }

    public int size() {
        expungeStaleEntries();
        return list.size();
    }

    public Object remove(int index) {
        return ( (WeakReference)list.remove(index) ).get();
    }

    /**
     * delate empty weak references(after GC) in reference queue
     */
    private void expungeStaleEntries() {
        Object reference = queue.poll();
        while ( reference != null ) {
            int i = list.indexOf( reference );
            if ( i != -1 ) {
                list.remove(i);
            }
            reference = queue.poll();
        }
    }

}
