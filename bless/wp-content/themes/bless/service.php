<?php
/*
  Template Name: Service
 */
?>
<?php get_header();?>


<div class="services_currblock">

	<div class="left">
		<img src="<?php bloginfo('template_url');?>/images/service_shape<?php if(isset($_GET['lang']) & $_GET['lang'] == 'en'){echo '-eng';}?>.gif" alt="Сервисное обслуживание">
	</div>
	
	<div class="right">
		<p><?php echo get_field('text_1');?></p>
	</div>

</div>


<div class="table_parm">
<h3><?php echo get_field('text_2');?></h3>
	<div class="parm">
		<div class="city">
			<span></span> <a class="select"></a>
                        
                        <div style="position:relative;"><div id="hiddencity">
                            
                            <div class="hiddencity" style="border-top:0px;">Киев</div>
                            <div class="hiddencity">Александрия</div>
<div class="hiddencity">Белая Церковь</div>
<div class="hiddencity">Винница</div>
<div class="hiddencity">Донецк</div>
<div class="hiddencity">Днепропетровск</div>
<div class="hiddencity">Днепродзержинск</div>
<div class="hiddencity">Запорожье</div>
<div class="hiddencity">Житомир</div>
<div class="hiddencity">Ивано-Франковск</div>
<div class="hiddencity">Кировоград</div>
<div class="hiddencity">Кривой Рог</div>
<div class="hiddencity">Кременчуг</div>
<div class="hiddencity">Конотоп</div>
<div class="hiddencity">Коростень</div>
<div class="hiddencity">Луганск</div>
<div class="hiddencity">Львов</div>
<div class="hiddencity">Луцк</div>
<div class="hiddencity">Мелитополь</div>
<div class="hiddencity">Мариуполь</div>
<div class="hiddencity">Монастырище</div>
<div class="hiddencity">Мукачево</div>
<div class="hiddencity">Николаев</div>
<div class="hiddencity">Одесса</div>
<div class="hiddencity">Обухов</div>
<div class="hiddencity">Полтава</div>
<div class="hiddencity">Павлоград</div>
<div class="hiddencity">Переяслав-Хмельницький</div>
<div class="hiddencity">Ровно</div>
<div class="hiddencity">Севастополь</div>
<div class="hiddencity">Северодонецк</div>
<div class="hiddencity">Симферополь</div>
<div class="hiddencity">Сумы</div>
<div class="hiddencity">Тернополь</div>
<div class="hiddencity">Ужгород</div>
<div class="hiddencity">Умань</div>
<div class="hiddencity">Херсон</div>
<div class="hiddencity">Харьков</div>
<div class="hiddencity">Хмельницкий</div>
<div class="hiddencity">Черкассы</div>
<div class="hiddencity">Чернигов</div>
<div class="hiddencity">Черновцы</div>




                            
                        </div>
                       </div>
                        
		</div>
            <div class="clear"></div>
		<input id="searchservice" type="submit" value="" style="background: url('<?php bloginfo('template_url');?>/images/search_btn<?php if(isset($_GET['lang']) & $_GET['lang'] == 'en'){echo '-eng';}?>.gif') no-repeat;">
	</div>


<table>
	<tr>
		<td></td>
		<td><?php echo get_field('text_3');?></td>
		<td><?php echo get_field('text_4');?></td>
		<td><?php echo get_field('text_5');?></td>
	</tr>
	<?php include 'blocks/services.php'; ?>
	
</table>

<!--<div class="navi">
	<a href="11" class="prev" style="background: url('<?php bloginfo('template_url');?>/images/navi-prev<?php if(isset($_GET['lang']) & $_GET['lang'] == 'en'){echo '-eng';}?>.png') no-repeat;"></a> <a href="2222" class="next" style="background: url('<?php bloginfo('template_url');?>/images/navi-next<?php if(isset($_GET['lang']) & $_GET['lang'] == 'en'){echo '-eng';}?>.png') no-repeat;"></a>
</div>-->

</div>

<?php get_footer(); ?>