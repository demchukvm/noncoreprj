package com.tmx.beng.httpsgate.globalmoney;

import com.tmx.util.queue.ActionQueueElement;
import com.tmx.beng.base.BillingMessage;
import com.tmx.beng.httpsgate.HttpsGateException;

class GlobalMoneyGateTask extends ActionQueueElement
{
    private BillingMessage procesedMessage;
    private BasicGlobalMoneyGate gate;

    public GlobalMoneyGateTask(BillingMessage billingMessage, BasicGlobalMoneyGate gate)
    {
        this.gate = gate;
        this.procesedMessage = billingMessage;
    }

    public void run() {
        try{
            gate.runBillMessage(procesedMessage);
        }
        catch(HttpsGateException e){
            gate.logger.error(e.getLocalizedMessage(), e);
        }
    }
}
