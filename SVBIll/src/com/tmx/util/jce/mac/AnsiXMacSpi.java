package com.tmx.util.jce.mac;

import javax.crypto.MacSpi;
import javax.crypto.Cipher;

/**
  Implementation of ANSI MAC X-series algorithms
 */
public abstract class AnsiXMacSpi extends MacSpi {
    protected final int CHUNK_LENGTH = 8; //bytes
    /** Holds current MAC calculated under authentificaed data's chunks. */
    protected byte[] currentMac;
    /** Holds excess of authentificated data not used by update() method yet.
     * This is the tail of authentificated data with length = dataLength % chunkLength. */
    protected byte[] chunkExcess;

    protected final String DES_ALGORITHM = "DES";
    protected Cipher desCipher;


    protected abstract byte[] collectToFitChunkSize(byte[] authentificatedData, int offset, int authentificatedDataLength);

    //------------Engine methods
    protected void engineUpdate(byte b) {
        engineUpdate(new byte[]{b}, 0, 1);
    }

    //todo replace with protected
    public void engineReset() {
        currentMac = null;
        chunkExcess = null;
        //desCipher.doFinal(); to reset cipher
    }
    
    protected int engineGetMacLength() {
        return CHUNK_LENGTH;
    }
    //todo replace with protected
    public void engineUpdate(byte[] bytes, int offset, int len) {
        calculateMac(bytes, offset, len);
    }


    //-----------Auxilary methods
    /**
     * Encrypt one chunk of data with secret key.
     *  */
    protected byte[] encryptChunk(Cipher cipher, byte[] encryptedChunk, int chunkNumber) {
        try{
            return cipher.doFinal(encryptedChunk, chunkNumber*CHUNK_LENGTH, CHUNK_LENGTH);
        }
        catch(Exception e){
            throw new RuntimeException("Failed to encrypt data chunk: "+e);
        }
    }

    protected byte[] encryptChunk(Cipher cipher, byte[] encryptedChunk) {
        return encryptChunk(cipher, encryptedChunk, 0);
    }

    protected byte[] xorChunkWithCurrentMac(byte[] authentificatedData, byte[] currentMac, int chunkNumber){
        for(int i=0; i<CHUNK_LENGTH; i++)
            currentMac[i] = (byte)(currentMac[i] ^ authentificatedData[chunkNumber*CHUNK_LENGTH+i]);
        return currentMac;
    }

    protected byte[] propagateZeroUpToChunk(byte[] chunkExcess){
        if(chunkExcess == null || chunkExcess.length == 0)
            return null;

        byte[] chunk = new byte[CHUNK_LENGTH];//filled by 0
        System.arraycopy(chunkExcess, 0 , chunk, 0, chunkExcess.length);
        return chunk;
    }

    protected void calculateMacUnderChunkedData (byte[] authentificatedData){

        if(authentificatedData == null)
            return;//nothig to process on this step

        //now we works with authentificatedData divisible to chunk length
        int chunkCount = authentificatedData.length / CHUNK_LENGTH;
        for(int i=0; i < chunkCount; i++){
            //do first chunk encryption
            if(currentMac == null){
                currentMac = encryptChunk(desCipher, authentificatedData, i);
                continue;
            }

            currentMac = xorChunkWithCurrentMac(authentificatedData, currentMac, i);
            currentMac = encryptChunk(desCipher, currentMac);
        }
    }

    /**
     * Calculates MAC under the union of <code>chunkExcess</code> array and given authentificated data byte array.
     * If authentificated array length is not divisible to chunk length, then reminder of array
     * will be placed to <code>chunkExcess</code> property and wait to be proceed with MAC next time.
     * Calculated MAC assigned to <code>currentMac</code>
     * @param authentificatedData data the MAC shall be calcualted for.
     * */
    protected void calculateMac(byte[] authentificatedData, int offset, int len){
        authentificatedData = collectToFitChunkSize(authentificatedData, offset, len);
        calculateMacUnderChunkedData(authentificatedData);
    }

    //----------------TODO REDUNDANT REMOVE THESE
    protected abstract byte[] engineDoFinal();
    protected abstract void engineInit(java.security.Key key, java.security.spec.AlgorithmParameterSpec algorithmParameterSpec) throws java.security.InvalidKeyException, java.security.InvalidAlgorithmParameterException;

}

