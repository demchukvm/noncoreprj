var div_hidden = true;

function no_bubble(){
	window.event.cancelBubble = true;
}

function submitForm(_action) {
    document.forms[0].action = _action;
    document.forms[0].submit();
}
function getAbsolutePos(el){
    var SL = 0, ST = 0;
    var is_div = /^div$/i.test(el.tagName);
    if (is_div && el.scrollLeft)
        SL = el.scrollLeft;
    if (is_div && el.scrollTop)
        ST = el.scrollTop;
    var r = { x: el.offsetLeft - SL, y: el.offsetTop - ST };
    if (el.offsetParent) {
        var tmp = getAbsolutePos(el.offsetParent);
        r.x += tmp.x;
        r.y += tmp.y;
    }
    return r;
}

function getVisib(obj){
    var value = obj.style.visibility;
    if (!value) {
        if (document.defaultView && typeof (document.defaultView.getComputedStyle) == "function") { // Gecko, W3C
            value = '';
        } else if (obj.currentStyle) { // IE
            value = obj.currentStyle.visibility;
        } else
            value = '';
    }
    return value;
};

function hideShowElements(el){
    // hide all bad elenents
    var tags = new Array("applet", "iframe", "select", "object");
    var p = getAbsolutePos(el);
    var EX1 = p.x;
    var EX2 = el.offsetWidth + EX1;
    var EY1 = p.y;
    var EY2 = el.offsetHeight + EY1;

    for (var k = tags.length; k > 0; ) {
        var ar = document.getElementsByTagName(tags[--k]);
        var cc = null;

        for (var i = ar.length; i > 0;) {
            cc = ar[--i];

            p = getAbsolutePos(cc);
            var CX1 = p.x;
            var CX2 = cc.offsetWidth + CX1;
            var CY1 = p.y;
            var CY2 = cc.offsetHeight + CY1;

            if (div_hidden || (CX1 > EX2) || (CX2 < EX1) || (CY1 > EY2) || (CY2 < EY1)) {
                if (!cc.__msh_save_visibility__div1) {
                    cc.__msh_save_visibility__div1 = getVisib(cc);
                }
                cc.style.visibility = cc.__msh_save_visibility__div1;
            } else {
                if (!cc.__msh_save_visibility__div1) {
                    cc.__msh_save_visibility__div1 = getVisib(cc);
                }
                cc.style.visibility = "hidden";
            }
        }
    }
}

function init_div(x_offset,y_offset,button, div){
    var obj = getRawObject(button);
    p = getAbsolutePos(obj);
    shiftTo(div, p.x + x_offset, p.y + y_offset);
}

function show_div(div){
    el = getRawObject(div);
    style = getObject(div);
    if(style.visibility != "visible"){
        style.visibility = "visible";
        div_hidden = false;
        hideShowElements(getRawObject('outerWrapper'));
    }
    no_bubble();
}

function hide_div(div){
    el = getRawObject(div);
    style = getObject(div);
    if(style.visibility != "hidden"){
        style.visibility = "hidden";
        div_hidden = true;
        hideShowElements(getRawObject('outerWrapper'));
    }
    no_bubble();
}

function popup_or_hide(div){
	var obj = getObject(div);
	if(obj.visibility == "hidden"){
		show_div(div);
	}else{
        hide_div(div);
    }
}

//RM: updated functions to support several popup divs

function show_div(div, outerWrapper){
    el = getRawObject(div);
    style = getObject(div);
    if(style.visibility != "visible"){
        style.visibility = "visible";
        div_hidden = false;
        hideShowElements(getRawObject(outerWrapper));
    }
    no_bubble();
}

function hide_div(div, outerWrapper){
    el = getRawObject(div);
    style = getObject(div);
    if(style.visibility != "hidden"){
        style.visibility = "hidden";
        div_hidden = true;
        hideShowElements(getRawObject(outerWrapper));
    }
    no_bubble();
}

function popup_or_hide(div, outerWrapper){
	var obj = getObject(div);
	if(obj.visibility == "hidden"){
		show_div(div, outerWrapper);
	}else{
        hide_div(div, outerWrapper);
    }
}