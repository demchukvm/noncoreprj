<%@ page language="java" pageEncoding="UTF-8" contentType="text/html; charset=utf-8"
         errorPage="/core/jsp/message/jsperror.jsp" isELIgnored="false" %>
<%@ taglib uri="/WEB-INF/tld/struts-nested.tld" prefix="nested" %>
<%@ taglib uri="/WEB-INF/tld/tmx-controls.tld" prefix="tmx_ctrl" %>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>


<html:form action="/core/transactions/seller_transaction/infoSellerTransaction">
<table border="0" width="100%">

    <!-- BUTTONS -->
    <tr>
        <td style="text-align: left">
            <tmx_ctrl:button property="onCancelButton"/>
        </td>
    </tr>
    <tr>
        <td valign="center" align="center" class="tabForm">
        <table width="100%" border="0">
            <%--<!-- Form header -->--%>
            <tr>
                <td colspan="2">
                    <h4 class="dataLabel">
                        <bean:message bundle="core.labels" key="core.transaction.clienttransaction.infoclienttransactionform.titleGeneral"/>
                    </h4>
                </td>
            </tr>

            <!--BEGIN for abstract info transaction form    -->
            <tr>
                <td class="dataLabel" width="15%">
                    <tmx_ctrl:writeEntityLabel attrName="transactionId"
                                               entityClassName="com.tmx.as.entities.bill.transaction.ClientTransaction"/>:
                </td>
                <td class="dataField" width="35%">
                    <tmx_ctrl:editBox property="editBoxTransactionId"/>
                </td>

                <td class="dataLabel" width="15%">
                    <tmx_ctrl:writeEntityLabel attrName="{@name}"
                                               entityClassName="com.tmx.as.entities.bill.transaction.TransactionType"/>:
                </td>
                <td class="dataField" width="35%">
                    <tmx_ctrl:editBox property="editBoxType"/>
                </td>
            </tr>

            <tr>
                <td class="dataLabel">
                    <tmx_ctrl:writeEntityLabel attrName="transactionGUID"
                                               entityClassName="com.tmx.as.entities.bill.transaction.ClientTransaction"/>:
                </td>
                <td class="dataField">
                    <tmx_ctrl:editBox property="editBoxTransactionGUID"/>
                </td>

                <td class="dataField" >
                    <tmx_ctrl:writeEntityLabel attrName="operationType"
                                               entityClassName="com.tmx.as.entities.bill.transaction.ClientTransaction"/>:
                </td>
                <td class="dataField" >
                       <tmx_ctrl:editBox property="editBoxOperationType" />
                </td>
            </tr>

            <tr>
                <td class="dataLabel" >
                    <tmx_ctrl:writeEntityLabel attrName="cliTransactionNum"
                                               entityClassName="com.tmx.as.entities.bill.transaction.ClientTransaction"/>:
                </td>
                <td class="dataField" >
                       <tmx_ctrl:textArea property="textAreaClientTransactionNum"/>
                </td>

                <td class="dataLabel" >
                    <tmx_ctrl:writeEntityLabel attrName="status"
                                               entityClassName="com.tmx.as.entities.bill.transaction.ClientTransaction"/>:
                </td>
                <td class="dataField" >
                       <tmx_ctrl:editBox property="editBoxStatus" />
                </td>
            </tr>

            <tr>
                <td class="dataLabel">
                    <tmx_ctrl:writeEntityLabel attrName="transactionTime"
                                               entityClassName="com.tmx.as.entities.bill.transaction.ClientTransaction"/>:
                </td>
                <td class="dataField">
                    <tmx_ctrl:timeSelector property="timeSelectorTransactionTime"/>
                </td>

                <td class="dataLabel" >
                    <tmx_ctrl:writeEntityLabel attrName="statusDescription"
                                               entityClassName="com.tmx.as.entities.bill.transaction.ClientTransaction"/>:
                </td>
                <td class="dataField" >
                    <tmx_ctrl:textArea property="textAreaStatusDescription"/>
                </td>
            </tr>

            <tr>
                <td class="dataLabel" width="15%">
                    <tmx_ctrl:writeEntityLabel attrName="clientTime"
                                               entityClassName="com.tmx.as.entities.bill.transaction.ClientTransaction"/>:
                </td>
                <td class="dataField" width="35%">
                       <tmx_ctrl:timeSelector property="timeSelectorClientTime" />
                </td>

                <td class="dataLabel" >
                    <tmx_ctrl:writeEntityLabel attrName="code"
                                               entityClassName="com.tmx.as.entities.bill.transaction.TransactionError"/>:
                </td>
                <td class="dataField" >
                    <tmx_ctrl:editBox property="editBoxTransErrCode"/>
                </td>
            </tr>

            <tr>
                <td class="dataLabel">
                    <tmx_ctrl:writeEntityLabel attrName="amount"
                                               entityClassName="com.tmx.as.entities.bill.transaction.ClientTransaction"/>:
                </td>
                <td class="dataField">
                    <tmx_ctrl:editBox property="editBoxAmount"/>
                </td>

                 <td class="dataLabel" >
                    <tmx_ctrl:writeEntityLabel attrName="message"
                                               entityClassName="com.tmx.as.entities.bill.transaction.TransactionError"/>:
                </td>
                <td class="dataField" >
                    <tmx_ctrl:textArea property="textAreaTransErrMessage"/>
                </td>
            </tr>

            <tr>
                <td class="dataLabel">
                    <tmx_ctrl:writeEntityLabel attrName="accountNumber"
                                               entityClassName="com.tmx.as.entities.bill.transaction.ClientTransaction"/>:
                </td>
                <td class="dataField">
                    <tmx_ctrl:editBox property="editBoxAccountNumber"/>
                </td>

                 <td class="dataLabel" >
                    <tmx_ctrl:writeEntityLabel attrName="{@name}"
                                               entityClassName="com.tmx.as.entities.bill.operator.Operator"/>:
                </td>
                <td class="dataField" >
                    <tmx_ctrl:editBox property="editBoxOperator"/>
                </td>
            </tr>
        </table>

    </tr>

    <!--seller-->
    <tr>
        <td valign="center" align="center" class="tabForm">
        <table width="100%" border="0">
            <%--<!-- Tab header -->--%>
            <tr>
                <td colspan="2">
                    <h4 class="dataLabel">
                        <bean:message bundle="core.labels" key="core.transaction.clienttransaction.infoclienttransactionform.titleSeller"/>
                    </h4>
                </td>
            </tr>

            <tr>
                <td class="dataLabel" width="15%">
                       <tmx_ctrl:writeEntityLabel attrName="{@name}"
                                                  entityClassName="com.tmx.as.entities.bill.seller.Seller"/>:
                </td>
                <td class="dataField" width="35%">
                    <tmx_ctrl:editBox property="editBoxSellerName"/>
                </td>

                <td class="dataLabel" width="15%">
                    <tmx_ctrl:writeEntityLabel attrName="{@name}"
                                               entityClassName="com.tmx.as.entities.bill.balance.SellerBalance"/>:
                </td>
                <td class="dataField" width="35%">
                    <tmx_ctrl:editBox property="editBoxSellerBalanceName" width="55%"/>
                </td>
             </tr>
        </table>
        </td>
    </tr>
    <!--transaction reason-->
    <tr>
        <td valign="center" align="center" class="tabForm">
        <table width="100%" border="0">
            <%--<!-- Tab header -->--%>
            <tr>
                <td colspan="2">
                    <h4 class="dataLabel">
                        <bean:message bundle="core.labels" key="core.transaction.clienttransaction.infoclienttransactionform.titleTransactionReason"/>
                    </h4>
                </td>
            </tr>
           <tr>
                <td class="dataLabel" width="15%">
                       <tmx_ctrl:writeEntityLabel attrName="paymentReason"
                                                  entityClassName="com.tmx.as.entities.bill.transaction.TransactionSupport"/>:
                </td>
                <td class="dataField" width="35%">
                    <tmx_ctrl:textArea property="textAreaSellerBalancePaymentReason"/>
                </td>

                <td class="dataLabel" width="15%">
                    <tmx_ctrl:writeEntityLabel attrName="description"
                                               entityClassName="com.tmx.as.entities.bill.transaction.TransactionSupport"/>:
                </td>
                <td class="dataField" width="35%">
                    <tmx_ctrl:textArea property="textAreaSellerBalanceDescription"/>
                </td>
            </tr>
           <tr>
            <td class="dataLabel" width="15%">
                <tmx_ctrl:writeEntityLabel attrName="UserName"
                                           entityClassName="com.tmx.as.entities.bill.transaction.TransactionSupport"/>:
            </td>
            <td class="dataField" width="35%">
                <tmx_ctrl:editBox property="editBoxExecutedUser"/>
            </td>

           <td style="text-align: left">
               <tmx_ctrl:button property="onSaveButton"/>
           </td>

            </tr>
        </table>
        </td>
    </tr>

    <%--<!-- BUTTONS -->--%>
    <tr>
        <td style="text-align: left">
            <tmx_ctrl:button property="onCancelButton"/>
        </td>
    </tr>
</table>
</html:form>