package com.tmx.as.entities.bill.balance;

import com.tmx.as.entities.bill.seller.Seller;


/**
 */
public class SellerBalance extends AbstractBalance {
    private Seller seller;
    private Long sellerId;

    public Seller getSeller() {
        return seller;
    }

    public void setSeller(Seller seller) {
        this.seller = seller;
    }

    public Long getSellerId() {
        return sellerId;
    }

    public void setSellerId(Long sellerId) {
        this.sellerId = sellerId;
    }
}
