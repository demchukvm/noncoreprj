package com.tmx.beng.base.cache;

import com.tmx.as.entities.bill.terminal_group.TerminalGroup;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;

/**
 */
class TerminalGroupWrapper {
    TerminalGroup terminalGroup;
    Map ballanceId_ballance = new HashMap();
    Map ballanceCode_ballanceId = new HashMap();
    Map tariffId_tariffEntity = new HashMap();
    Map tariffId_tariffFunctionInstance = new HashMap();
    //Map tariffName_tariffFunctionInstance = new HashMap();
    Map terminalId_terminal = new HashMap();
    List restriction = new Vector();
    
    TerminalGroupWrapper(TerminalGroup terminalGroup){
        this.terminalGroup = terminalGroup;
    }

    public List getRestriction() {
        return restriction;
    }

    public TerminalGroup getTerminalGroup() {
        return terminalGroup;
    }
}
