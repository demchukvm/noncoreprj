package com.tmx.beng.httpsgate.dacard.pdu;

/**

 */
public enum DacardMessageType {
    MT_DBLOAD(1),
    MT_RECV(2),
    MT_CONF(3),
    MT_REVER(4),
    MT_SYNCTIME(5),
    MT_EOD(6),
    MT_TPLOAD(7),
    MT_TP_ASK_FOR_NEW_VERSION(8),
    MT_TP_GET_NEW_VERSION(9),
    MT_ONLINE_REPLENISHMENT(10),
    MT_CHECK_REPLENISHMENT_STATE(11),
    MT_REGISTERNEWTERMINAL(12),
    MT_UPDATETERMINALINFO(13),
    MT_TEST(14),
    MT_VALIDATE_INVOICE_INFO(15),
    MT_SUBST_TERMINAL(16),
    MT_SUBST_CONFIRM(17),
    MT_SUBST_FAILED(18),
    MT_SUBST_REVERSE(19),
    MT_CLOSE_SESSION(20),
    MT_GET_PURCHASE_STATUS(21);

    private int code;

    private DacardMessageType(int code){
        this.code = code;
    }

    public int getCode(){
        return code;
    }
}
