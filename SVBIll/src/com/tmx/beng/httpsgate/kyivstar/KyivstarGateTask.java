package com.tmx.beng.httpsgate.kyivstar;

import com.tmx.util.queue.ActionQueueElement;
import com.tmx.beng.base.BillingMessage;
import com.tmx.beng.httpsgate.HttpsGateException;
import org.apache.log4j.Logger;

/**
    Gate task. Initiated from gate via medium connection.
    Processed by asynchronous thread.
 */
public class KyivstarGateTask extends ActionQueueElement {
    private BillingMessage procesedMessage;
    private BasicKyivstarGate gate;

    public KyivstarGateTask(BillingMessage billingMessage, BasicKyivstarGate gate){
        this.gate = gate;
        this.procesedMessage = billingMessage;
    }

    public void run() {
        try{
            gate.runBillMessage(procesedMessage);
        }
        catch(HttpsGateException e){
            gate.logger.error(e.getLocalizedMessage(), e);
        }
    }
}
