package com.tmx.beng.iso8583Gate.processors;

import com.tmx.beng.iso8583Gate.pdu.RuntimeMessage;
import com.tmx.beng.iso8583Gate.pdu.IncorrectFormatException;
import com.tmx.beng.base.*;
import com.tmx.util.InitException;
import com.tmx.as.exceptions.DatabaseException;

/**
To test please wait functionality
 */
public class PleaseWaitProcessor extends BasicProcessor{

    public int getMessageTypeId() {
        return 830;
    }

    public int getProcessingCode() {
        return 900001;
    }

    public int[] getDEToCopy() {
        return new int[]{11, 24, 41, 42};
    }

    public void processSpecificTask(RuntimeMessage reqMsg, RuntimeMessage respMsg)
            throws ProcessingException, BillException, IncorrectFormatException, InitException, DatabaseException {

        logger.debug("Please wait");
    }
}

