package com.tmx.beng.httpsgate.life;

import org.dom4j.*;
import java.util.Iterator;
import java.util.Map;
import java.util.HashMap;

import com.tmx.beng.httpsgate.BadResponseException;


/**
 * Life response. Parses XML to java model
 */
public class LifeResponse {
    private String responseCode;
    private String utilityCompanyCode;
    private String unit;
    private String voucherExpireDate;
    private String amount;
    private String currencyCode;
    private String stan;
    private String voucher;
    private String voucherSerialNumber;
    private String dealerCampaign;
    private String dealerMessage;
    private String customerSurname;
    private String customerCampaign;
    private String customerMessage;
    private String orginatorCityId;
    private String orginatorBranchId;
    private String orginatorTellerId;
    private String orginatorUserId;
    //reconciliation fields
    private Map<String,String> operationResponseCodes;

    public static LifeResponse parseResponse(String response) throws BadResponseException {
        LifeResponse lifeResponse = new LifeResponse();
        try{
            Document doc = DocumentHelper.parseText(response.trim());
            Element root = doc.getRootElement();
            if(!root.getName().equalsIgnoreCase("OTMResponse"))
                throw new BadResponseException("Root root is not 'OTMResponse'");

            lifeResponse.setResponseCode(root.attributeValue("ResponseCode") != null ? root.attributeValue("ResponseCode") : null);

            for(Iterator it = root.elementIterator(); it.hasNext(); ){
                Element element = (Element)it.next();

                // UtilityCompanyCode
                if(element.getName().equalsIgnoreCase("UtilityCompanyCode")){
                    lifeResponse.setUtilityCompanyCode(element != null ? element.getText() : null);
                    continue;
                }

                // Unit
                if(element.getName().equalsIgnoreCase("Unit")){
                    lifeResponse.setUnit(element != null ? element.getText() : null);
                    continue;
                }

                // VoucherExpireDate
                if(element.getName().equalsIgnoreCase("VoucherExpireDate")){
                    lifeResponse.setVoucherExpireDate(element != null ? element.getText() : null);
                    continue;
                }

                // Amount
                if(element.getName().equalsIgnoreCase("Amount")){
                    lifeResponse.setAmount(element != null ? element.getText() : null);
                    continue;
                }

                // CurrencyCode
                if(element.getName().equalsIgnoreCase("CurrencyCode")){
                    lifeResponse.setCurrencyCode(element != null ? element.getText() : null);
                    continue;
                }

                // Stan
                if(element.getName().equalsIgnoreCase("Stan")){
                    lifeResponse.setStan(element != null ? element.getText() : null);
                    continue;
                }

                // Voucher
                if(element.getName().equalsIgnoreCase("Voucher")){
                    lifeResponse.setVoucher(element != null ? element.getText() : null);
                    continue;
                }

                // VoucherSerialNumber
                if(element.getName().equalsIgnoreCase("VoucherSerialNumber")){
                    lifeResponse.setVoucherSerialNumber(element != null ? element.getText() : null);
                    continue;
                }

                // DealerCampaign
                if(element.getName().equalsIgnoreCase("DealerCampaign")){
                    lifeResponse.setDealerCampaign(element != null ? element.getText() : null);
                    continue;
                }

                // DealerMessage
                if(element.getName().equalsIgnoreCase("DealerMessage")){
                    lifeResponse.setDealerMessage(element != null ? element.getText() : null);
                    continue;
                }

                // CustomerSurname, ...
                if(element.getName().equalsIgnoreCase("Customer")){
                    for(Iterator it2 = element.elementIterator(); it2.hasNext(); ){
                        Element element2 = (Element)it2.next();

                        if(element2.getName().equalsIgnoreCase("NameSurname")){
                            lifeResponse.setCustomerSurname(element != null ? element.getText() : null);
                            continue;
                        }

                    }
                    continue;
                }

                // CustomerCampaign
                if(element.getName().equalsIgnoreCase("CustomerCampaign")){
                    lifeResponse.setCustomerCampaign(element != null ? element.getText() : null);
                    continue;
                }

                // CustomerMessage
                if(element.getName().equalsIgnoreCase("CustomerMessage")){
                    lifeResponse.setCustomerMessage(element != null ? element.getText() : null);
                    continue;
                }

                // OrginatorCityId
                if(element.getName().equalsIgnoreCase("OrginatorCityId")){
                    lifeResponse.setOrginatorCityId(element != null ? element.getText() : null);
                    continue;
                }

                // OrginatorBranchId
                if(element.getName().equalsIgnoreCase("OrginatorBranchId")){
                    lifeResponse.setOrginatorBranchId(element != null ? element.getText() : null);
                    continue;
                }

                // OrginatorTellerId
                if(element.getName().equalsIgnoreCase("OrginatorTellerId")){
                    lifeResponse.setOrginatorTellerId(element != null ? element.getText() : null);
                    continue;
                }

                // OrginatorUserId
                if(element.getName().equalsIgnoreCase("OrginatorUserId")){
                    lifeResponse.setOrginatorUserId(element != null ? element.getText() : null);
                    continue;
                }

                // Reconciliation response
                if(element.getName().equalsIgnoreCase("Reconciliations")){
                    lifeResponse.setOperationResponseCodes(new HashMap<String,String>());

                    for(Iterator it2 = element.elementIterator(); it2.hasNext(); ){
                        Element element2 = (Element)it2.next();
                        if(element2.getName().equalsIgnoreCase("Reconciliation")){

                            String operCode = null;
                            String respCode = null;
                            for(Iterator it3 = element2.elementIterator(); it3.hasNext(); ){
                                Element element3 = (Element)it3.next();

                                if(element3.getName().equalsIgnoreCase("ReconOperationCode")){
                                    operCode = element3.getText();
                                    continue;
                                }

                                if(element3.getName().equalsIgnoreCase("ResponseCode")){
                                    respCode = element3.getText();
                                }
                            }
                            if(operCode != null){//
                                lifeResponse.getOperationResponseCodes().put(operCode, respCode);
                            }
                        }
                    }
                    //continue;
                }                

            }
        }
        catch(NumberFormatException e){
            throw new BadResponseException(e.getMessage(), e);
        }
        catch(DocumentException e){
            throw new BadResponseException("Parse Life response error " + e.getMessage(), e);
        }
        return lifeResponse;
    }

    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    public String getUtilityCompanyCode() {
        return utilityCompanyCode;
    }

    public void setUtilityCompanyCode(String utilityCompanyCode) {
        this.utilityCompanyCode = utilityCompanyCode;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getVoucherExpireDate() {
        return voucherExpireDate;
    }

    public void setVoucherExpireDate(String voucherExpireDate) {
        this.voucherExpireDate = voucherExpireDate;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public String getStan() {
        return stan;
    }

    public void setStan(String stan) {
        this.stan = stan;
    }

    public String getVoucher() {
        return voucher;
    }

    public void setVoucher(String voucher) {
        this.voucher = voucher;
    }

    public String getVoucherSerialNumber() {
        return voucherSerialNumber;
    }

    public void setVoucherSerialNumber(String voucherSerialNumber) {
        this.voucherSerialNumber = voucherSerialNumber;
    }

    public String getDealerCampaign() {
        return dealerCampaign;
    }

    public void setDealerCampaign(String dealerCampaign) {
        this.dealerCampaign = dealerCampaign;
    }

    public String getDealerMessage() {
        return dealerMessage;
    }

    public void setDealerMessage(String dealerMessage) {
        this.dealerMessage = dealerMessage;
    }

    public String getCustomerSurname() {
        return customerSurname;
    }

    public void setCustomerSurname(String customerSurname) {
        this.customerSurname = customerSurname;
    }

    public String getCustomerCampaign() {
        return customerCampaign;
    }

    public void setCustomerCampaign(String customerCampaign) {
        this.customerCampaign = customerCampaign;
    }

    public String getCustomerMessage() {
        return customerMessage;
    }

    public void setCustomerMessage(String customerMessage) {
        this.customerMessage = customerMessage;
    }

    public String getOrginatorCityId() {
        return orginatorCityId;
    }

    public void setOrginatorCityId(String orginatorCityId) {
        this.orginatorCityId = orginatorCityId;
    }

    public String getOrginatorBranchId() {
        return orginatorBranchId;
    }

    public void setOrginatorBranchId(String orginatorBranchId) {
        this.orginatorBranchId = orginatorBranchId;
    }

    public String getOrginatorTellerId() {
        return orginatorTellerId;
    }

    public void setOrginatorTellerId(String orginatorTellerId) {
        this.orginatorTellerId = orginatorTellerId;
    }

    public String getOrginatorUserId() {
        return orginatorUserId;
    }

    public void setOrginatorUserId(String orginatorUserId) {
        this.orginatorUserId = orginatorUserId;
    }

    public Map<String, String> getOperationResponseCodes() {
        return operationResponseCodes;
    }

    public void setOperationResponseCodes(Map<String, String> operationResponseCodes) {
        this.operationResponseCodes = operationResponseCodes;
    }
}