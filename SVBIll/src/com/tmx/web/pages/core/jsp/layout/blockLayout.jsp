<%-- Include common set of tag library declarations for each layout --%>
<%@ page language="java" pageEncoding="UTF-8" contentType="text/html; charset=utf-8" errorPage="/core/jsp/message/jsperror.jsp" isELIgnored="false" %>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean" %>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html" %>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic" %>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<html:xhtml />


<tiles:importAttribute ignore="true" />

<div class="block">
	<c:if test="${titleKey != null}">
		<div class="blockTitle"><atleap:message key="${titleKey}" /></div>
	</c:if>
	<tiles:get name="blockBody" />
</div>

