<%@ page language="java" pageEncoding="UTF-8" contentType="text/html; charset=utf-8" isELIgnored="false" %>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-nested" prefix="nested" %>
<%@ taglib uri="http://tmx.com/taglib/tmx-controls" prefix="tmx_ctrl" %>
<%@ taglib uri="http://tmx.com/taglib/tmx-logic" prefix="tmx_logic" %>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html" %>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean" %>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>

<html:form action="/core/dictionary/user/manageUsers">
<table width="100%" border="0">
    <!-- BUTTONS -->
    <tr>
        <td style="text-align: left">
            <tmx_ctrl:button property="onCreateButton"/>
            <tmx_ctrl:button property="onEditButton"/>
            <tmx_ctrl:button property="onDeleteButton"/>
        </td>
    </tr>
    <!-- FILTER SET -->
    <tr>
        <td valign="top" width="100%" align="left">
            <tmx_ctrl:panelFilterSet tableName="usersTable">
            <table>
                <tr>
                    <td class="dataLabel">
                        <tmx_ctrl:writeEntityLabel
                                attrName="login"
                                entityClassName="com.tmx.as.entities.general.user.User"/>:
                    </td>

                    <td class="dataField">
                        <tmx_ctrl:editBox
                                property="usersTable.filterSet.filter(by_login).parameter(login).control"/>
                    </td>

                    <td class="dataLabel">
                        <tmx_ctrl:writeEntityLabel
                                attrName="firstName"
                                entityClassName="com.tmx.as.entities.general.user.User"/>:
                    </td>

                    <td class="dataField">
                        <tmx_ctrl:editBox
                                property="usersTable.filterSet.filter(by_firstName).parameter(firstName).control"/>
                    </td>
                    <td class="dataLabel">
                        <tmx_ctrl:writeEntityLabel
                                attrName="lastName"
                                entityClassName="com.tmx.as.entities.general.user.User"/>:
                    </td>
                    <td class="dataField">
                        <tmx_ctrl:editBox
                                property="usersTable.filterSet.filter(by_lastName).parameter(lastName).control"/>
                    </td>
                </tr>
            </table>
        </tmx_ctrl:panelFilterSet>
        </td>
    </tr>
    <!-- TABLE -->
    <tr>
        <td width="100%">
            <table class='one_outer_cell_table' cellspacing='0' cellpadding='0' width="100%">
                <tr>
                    <td>
                        <table cellspacing='1' cellpadding='1' width="100%">
                            <!-- Table header -->
                            <tr>
                                <th class="column_header_cell" width="40">
                                    <bean:message bundle="core.labels" key="core.common.th.choose"/>
                                </th>

                                <th class="column_header_cell" width="140">
                                    <tmx_ctrl:imgTableOrdering tableName="usersTable"
                                                               orderByFieldName="login"
                                                               labelBundle="as:com.tmx.as.entities.general.user.User"/>
                                </th>

                                <th class="column_header_cell" width="100">
                                    <tmx_ctrl:imgTableOrdering tableName="usersTable"
                                                               orderByFieldName="lastName"
                                                               labelBundle="as:com.tmx.as.entities.general.user.User"/>
                                </th>

                                <th class="column_header_cell" width="140">
                                    <tmx_ctrl:imgTableOrdering tableName="usersTable"
                                                               orderByFieldName="firstName"
                                                               labelBundle="as:com.tmx.as.entities.general.user.User"/>
                                </th>

                                <th class="column_header_cell" width="70">
                                    <tmx_ctrl:imgTableOrdering tableName="usersTable"
                                                               orderByFieldName="active"
                                                               labelBundle="as:com.tmx.as.entities.general.user.User"/>
                                </th>

                                <th class="column_header_cell" width="110">
                                    <tmx_ctrl:imgTableOrdering tableName="usersTable"
                                                               orderByFieldName="role.roleName"
                                                               labelBundle="as:com.tmx.as.entities.general.role.Role"
                                                               labelKey="{@name}"/>

                                </th>

                                <th class="column_header_cell" width="110">
                                    <tmx_ctrl:imgTableOrdering tableName="usersTable"
                                                               orderByFieldName="organization.name"
                                                               labelBundle="as:com.tmx.as.entities.general.organization.Organization"
                                                               labelKey="{@name}"/>
                                </th>
                                
                                <th class="column_header_cell" width="110">
                                    <tmx_ctrl:imgTableOrdering tableName="usersTable"
                                                               orderByFieldName="creationDate"
                                                               labelBundle="as:com.tmx.as.entities.general.user.User"
                                                               labelKey="creationDate"/>
                                </th>
                            </tr>
                            <!-- Table data -->
                            <nested:iterate id="user" property="usersTable.data" type="com.tmx.as.entities.general.user.User">
                                <tr>
                                    <td class='column_data_cell' width="40">
                                        <input type="radio" name="selectedUserId" value="<c:out value="${user.userId}" />"checked="false">
                                    </td>
                                    <td class='column_data_cell' width="140">
                                        <c:out value="${user.login}" />
                                    </td>
                                    <td class='column_data_cell' width="100">
                                        <c:out value="${user.lastName}" />
                                    </td>
                                    <td class='column_data_cell' width="140">
                                        <c:out value="${user.firstName}" />
                                    </td>
                                    <td class='column_data_cell' width="70">
                                        <tmx_ctrl:checkSign name="user" property="active"/>
                                    </td>
                                    <td class='column_data_cell' width="110">
                                        <c:if test="${user.role!= null}">
                                            <c:out value="${user.role.roleName}" />
                                        </c:if>
                                    </td>
                                    <td class='column_data_cell' width="110">
                                        <c:if test="${user.organization!= null}">
                                            <c:out value="${user.organization.name}" />
                                        </c:if>
                                    </td>
                                    <td class='column_data_cell' width="110">
                                        <fmt:formatDate value="${user.creationDate}" pattern="yyyy/MM/dd HH:mm:ss"/>
                                    </td>                                                          
                                </tr>
                            </nested:iterate>
                            <!-- Empty row -->
                            <nested:define id="tableSize" property="usersTable.resultSize" type="java.lang.Integer"/>
                            <c:if test="${tableSize <= 0}">
                                <tr>
                                    <td class='column_data_cell' colspan="7" align="center">
                                        <bean:message bundle="core.labels" key="core.common.th.empty_table" />
                                    </td>
                                </tr>
                            </c:if>

                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td style="text-align: right">
            <tmx_ctrl:imgTablePaging tableName="usersTable"/>
        </td>
    </tr>
    <!-- BUTTONS -->
    <tr>
        <td style="text-align: left">
            <tmx_ctrl:button property="onCreateButton"/>
            <tmx_ctrl:button property="onEditButton"/>
            <tmx_ctrl:button property="onDeleteButton"/>
        </td>
    </tr>    
</table>
</html:form>