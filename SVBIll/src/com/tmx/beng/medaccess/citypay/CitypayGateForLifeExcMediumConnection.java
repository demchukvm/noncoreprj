package com.tmx.beng.medaccess.citypay;

import com.tmx.beng.base.BillingMessage;
import com.tmx.beng.httpsgate.HttpsGateException;
import com.tmx.beng.httpsgate.citypay.CitypayGate;
import com.tmx.beng.httpsgate.citypay.CitypayGateForLifeExc;
import com.tmx.beng.medaccess.MediumConnection;

public class CitypayGateForLifeExcMediumConnection implements MediumConnection {
    private CitypayGateForLifeExc citypayGate = null;

    CitypayGateForLifeExcMediumConnection() throws HttpsGateException {
        citypayGate = CitypayGateForLifeExc.getInstance();
    }

    public void send(BillingMessage msg) {
        citypayGate.enqueueBillMessage(msg);
    }
}