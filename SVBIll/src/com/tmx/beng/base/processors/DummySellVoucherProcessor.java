package com.tmx.beng.base.processors;

import com.tmx.beng.base.*;
import com.tmx.beng.base.memory.Condition;
import com.tmx.beng.base.memory.InMemoryObject;
import com.tmx.beng.base.cache.DBCache;

import java.util.Date;
import java.util.List;
import java.text.SimpleDateFormat;


/**
 * Register message
 */
public class DummySellVoucherProcessor extends TaskProcessor {

    public Task process0(Task task, DBCache dbCache, BillingMessageWritable msg) throws BillException{
        logger.debug("Dummy sell voucher...");

        logger.debug("Service: "+msg.getAttribute(BillingMessage.SERVICE_CODE));
        logger.debug("Voucher nominal: "+msg.getAttribute(BillingMessage.VOUCHER_NOMINAL));

        BillingMessageWritable respMsg = new BillingMessageImpl();
        respMsg.setOperationName(msg.getOperationName());
        respMsg.setAttribute(BillingMessage.VOUCHER_CODE, "12345678901234567890");
        respMsg.setAttribute(BillingMessage.VOUCHER_SECRET_KEY, "09876543210987654321");
        respMsg.setAttribute(BillingMessage.VOUCHER_BEST_BEFORE_DATE, new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(new Date()));
        respMsg.setAttribute(BillingMessage.VOUCHER_RENEVAL_TIME, "260");
        respMsg.setAttribute(BillingMessage.STATUS_CODE, new Integer(0));
        respMsg.setAttribute(BillingMessage.WAIT_TIME, String.valueOf(task.getGlobalContext().getReporter().getWaitTimeEstimate()));


        CallBack callBack = task.getCallBack();
        if(callBack != null)
            callBack.sendBilledMessage(respMsg);

        return task;
    }
}
