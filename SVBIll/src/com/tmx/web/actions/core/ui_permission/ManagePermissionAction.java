package com.tmx.web.actions.core.ui_permission;

import com.tmx.web.base.BasicDispatchedAction;
import com.tmx.web.base.SessionEnvironment;
import com.tmx.web.forms.core.ui_permission.ManagePermissionForm;
import com.tmx.web.forms.core.uipermsetup.ToolBarPermissionForm;
import com.tmx.web.controls.ScrollBox;

import com.tmx.as.base.EntityManager;
import com.tmx.as.entities.general.ui_permission.PermissionType;
import com.tmx.as.entities.general.role.Role;

import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForm;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class ManagePermissionAction extends BasicDispatchedAction {
    private static final String FORWARD = "page";
    public ActionForward defaultAction(ActionMapping mapping,
                                       ActionForm form,
                                       HttpServletRequest request,
                                       HttpServletResponse response,
                                       String methodName) throws Throwable {
        ManagePermissionForm managePermissionForm = (ManagePermissionForm) verifyForm(form, ManagePermissionForm.class);
        managePermissionForm.getTable().setRefreshActionName(this.getName());
        ((ScrollBox) managePermissionForm.getTable().// bind Role
                getFilterSet().getFilter("com_tmx_as_entities_general_ui_permission_Permission_role").
                getParameter("value").getControl()).bind(
                new EntityManager().RETRIEVE_ALL(Role.class,null)
        );
        ((ScrollBox) managePermissionForm.getTable().// bind permissionType
                getFilterSet().getFilter("com_tmx_as_entities_general_ui_permission_Permission_permissionType").
                getParameter("value").getControl()).bind(
                new EntityManager().RETRIEVE_ALL(PermissionType.class,null)
        );
        managePermissionForm.getTable().refresh();
        // set Readonly button
        if(managePermissionForm.getTable().getResultSize().intValue()==0){
            managePermissionForm.getOnDeleteButton().setReadonly(true);
            managePermissionForm.getOnEditButton().setReadonly(true);
        }else{
            managePermissionForm.getOnDeleteButton().setReadonly(false);
            managePermissionForm.getOnEditButton().setReadonly(false);
        }
        // set interactive params
        ToolBarPermissionForm toolBarPermissionForm=(ToolBarPermissionForm)SessionEnvironment.allocateForm(ToolBarPermissionForm.class,"core.uipermsetup.toolBarPermissionForm",mapping,request);
        if(toolBarPermissionForm==null||!toolBarPermissionForm.isActive()){
            managePermissionForm.getInteractiveButton().setLabelKey("ctrl.button.label.on");
            managePermissionForm.getInteractiveButton().setCommand("interactiveActivate");
            managePermissionForm.getInteractiveButton().putParam("forwardUrl",getName()+".do");
            managePermissionForm.getInteractiveCheckBox().setSelected(false);
        }else{
            managePermissionForm.getInteractiveButton().setLabelKey("ctrl.button.label.off");
            managePermissionForm.getInteractiveButton().setCommand("interactiveDectivate");
            managePermissionForm.getInteractiveButton().putParam("forwardUrl",getName()+".do");
            managePermissionForm.getInteractiveCheckBox().setSelected(true);
        }
        return mapping.findForward(FORWARD);
    }
}
