<?php /* Smarty version 2.6.26, created on 2013-02-20 19:18:36
         compiled from affiliate_program.tpl.html */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'cycle', 'affiliate_program.tpl.html', 23, false),array('modifier', 'string_format', 'affiliate_program.tpl.html', 27, false),)), $this); ?>
<?php if ($this->_tpl_vars['SubPage'] == 'balance'): ?>

	<p>
	<?php echo 'Привлеченные покупатели'; ?>
: <b><?php echo $this->_tpl_vars['affiliate_customers']; ?>
</b>
	</p>
	
	<?php if ($this->_tpl_vars['CommissionsNumber']): ?>
		<table cellspacing="0" cellpadding="6" width="100%">
		<tr class="background1">
			<td>&nbsp;</td>
			<td>
			<?php echo 'Общее вознаграждение'; ?>

			</td>
			<td>
			<?php echo 'Выплаты'; ?>

			</td>
			<td>
			<?php echo 'Баланс'; ?>

			</td>
		</tr>
		<?php $_from = $this->_tpl_vars['CurrencyISO3']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['_CurrencyISO']):
?>
		<?php if ($this->_tpl_vars['CommissionsAmount'][$this->_tpl_vars['_CurrencyISO']['currency_iso_3']] || $this->_tpl_vars['PaymentsAmount'][$this->_tpl_vars['_CurrencyISO']['currency_iso_3']]): ?>
		<tr class="<?php echo smarty_function_cycle(array('values' => 'row_odd,row_even'), $this);?>
">
			<td align="center"><B><?php echo $this->_tpl_vars['_CurrencyISO']['currency_iso_3']; ?>
</B>
			</td>
			<td>
			<?php echo ((is_array($_tmp=$this->_tpl_vars['CommissionsAmount'][$this->_tpl_vars['_CurrencyISO']['currency_iso_3']])) ? $this->_run_mod_handler('string_format', true, $_tmp, "%.2f") : smarty_modifier_string_format($_tmp, "%.2f")); ?>

			</td>
			<td>
			<?php echo ((is_array($_tmp=$this->_tpl_vars['PaymentsAmount'][$this->_tpl_vars['_CurrencyISO']['currency_iso_3']])) ? $this->_run_mod_handler('string_format', true, $_tmp, "%.2f") : smarty_modifier_string_format($_tmp, "%.2f")); ?>

			</td>
			<td>
			<?php echo ((is_array($_tmp=($this->_tpl_vars['CommissionsAmount'][$this->_tpl_vars['_CurrencyISO']['currency_iso_3']]-$this->_tpl_vars['PaymentsAmount'][$this->_tpl_vars['_CurrencyISO']['currency_iso_3']]))) ? $this->_run_mod_handler('string_format', true, $_tmp, "%.2f") : smarty_modifier_string_format($_tmp, "%.2f")); ?>

			</td>
		</tr>
			<?php endif; ?>
		<?php endforeach; endif; unset($_from); ?>
		</table>
	<?php else: ?>
		<?php echo 'У Вас нулевой баланс'; ?>

		<?php endif; ?>

<?php endif; ?>
<?php if ($this->_tpl_vars['SubPage'] == 'payments_history'): ?>

	<?php if ($this->_tpl_vars['PaymentsNumber']): ?>
		<table cellspacing="0" cellpadding="6" width="100%">
		<tr class="background1">
				<td><?php echo 'Номер выплаты'; ?>

				</td>
				<td align="center"><?php echo 'blog_postdate'; ?>

				</td>
				<td><?php echo 'Комментарий'; ?>

				</td>
				<td align="right"><?php echo 'Итого'; ?>

				</td>
			</tr>
		<?php $_from = $this->_tpl_vars['Payments']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['_Payment']):
?>
		<tr class="<?php echo smarty_function_cycle(array('values' => 'row_odd,row_even'), $this);?>
">
				<td>
				P-<?php echo $this->_tpl_vars['_Payment']['pID']; ?>

				</td>
				<td align="center">
				<?php echo $this->_tpl_vars['_Payment']['xDate']; ?>

				</td>
				<td>
				<?php echo $this->_tpl_vars['_Payment']['Description']; ?>

				</td>
				<td align="right">
				<?php echo $this->_tpl_vars['_Payment']['Amount']; ?>
 <?php echo $this->_tpl_vars['_Payment']['CurrencyISO3']; ?>

				</td>
			</tr>
		<?php endforeach; endif; unset($_from); ?>
		</table>
	<?php else: ?>
		<?php echo 'До текущего момента выплаты вам не производились'; ?>

		<?php endif; ?>

<?php endif; ?>
<?php if ($this->_tpl_vars['SubPage'] == 'settings'): ?>

	<?php if ($this->_tpl_vars['SettingsSaved']): ?>
		<div class="ok_msg_f"><?php echo 'Информация сохранена'; ?>
</div>
	<?php endif; ?>
	
	<?php if ($this->_tpl_vars['CONF_AFFILIATE_EMAIL_NEW_COMMISSION'] || $this->_tpl_vars['CONF_AFFILIATE_EMAIL_NEW_PAYMENT']): ?>
		<form name="form_settings" action="<?php echo $this->_tpl_vars['REQUEST_URI']; ?>
" method="POST">
		<input name="fACTION" value="SAVE_SETTINGS" type="hidden" >
		<input name="fREDIRECT" value="<?php echo $this->_tpl_vars['REQUEST_URI']; ?>
" type="hidden" >
		<?php if ($this->_tpl_vars['CONF_AFFILIATE_EMAIL_NEW_COMMISSION']): ?>
		<p>
			<input name="EmailOrders" 
				<?php if ($this->_tpl_vars['Settings']['EmailOrders']): ?>
					checked="checked"
				<?php endif; ?>
				class="checknomarging" value="1" type="checkbox" id="id_emailorders" > &nbsp;<label for="id_emailorders"><?php echo 'Я хочу получать email-уведомления, когда привлеченный покупатель делает заказ'; ?>
</label>
		</p>
		<?php endif; ?>		
		<?php if ($this->_tpl_vars['CONF_AFFILIATE_EMAIL_NEW_PAYMENT']): ?>
		<p>
			<input name="EmailPayments" 
				<?php if ($this->_tpl_vars['Settings']['EmailPayments']): ?>
					checked="checked"
				<?php endif; ?>
				class="checknomarging" value="1" type="checkbox" id="id_emailpayments" > &nbsp;<label for="id_emailpayments"><?php echo 'Получать email-уведомления о новых выплатах'; ?>
</label>
			<?php endif; ?>		</p>
		
		<input value="<?php echo 'Сохранить'; ?>
" type="submit" >
		</form>
		<?php endif; ?> 
<?php endif; ?>
<?php if ($this->_tpl_vars['SubPage'] == 'attract_guide'): ?>
	<?php echo $this->_tpl_vars['_AFFP_STRING_ATTRACT_GUIDE']; ?>

<?php endif; ?>