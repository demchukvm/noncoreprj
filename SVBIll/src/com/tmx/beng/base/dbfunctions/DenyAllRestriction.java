package com.tmx.beng.base.dbfunctions;

import com.tmx.beng.base.cache.InvokeFunctionException;
import java.util.Map;

/**
    Restriciton to deny all transactions 
 */
public class DenyAllRestriction extends BasicLimitFunction {

    public void checkLimit(Map actualParams) throws InvokeFunctionException {
        throw new InvokeFunctionException("err.deny_all_restriction");
    }
}

