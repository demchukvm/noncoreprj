package com.tmx.beng.testgate;

import com.tmx.beng.medaccess.GateException;

import java.util.Locale;

/**
 */
public class TestGateException extends GateException {

    public TestGateException(String msg, String[] params, Throwable e, Locale locale) {
        super(msg, params, e, locale);
    }

    public TestGateException(String msg, String[] params) {
        super(msg, params);
    }

    public TestGateException(String msg, String[] params, Locale locale) {
        super(msg, params, locale);
    }

    public TestGateException(String msg, Throwable e, Locale locale) {
        super(msg, e, locale);
    }

    public TestGateException(String msg, Throwable e) {
        super(msg, e);
    }

    public TestGateException(String msg, Locale locale) {
        super(msg, locale);
    }

    public TestGateException(String msg) {
        super(msg);
    }

    public TestGateException(String msg, String[] params, Throwable e) {
        super(msg, params, e);
    }
}
