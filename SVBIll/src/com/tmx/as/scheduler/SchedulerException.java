package com.tmx.as.scheduler;

import com.tmx.util.StructurizedException;

import java.util.Locale;

public class SchedulerException extends StructurizedException {

    public SchedulerException(String errKey){
        super(errKey);
    }

    public SchedulerException(String errKey, Locale locale){
        super(errKey, locale);
    }

    public SchedulerException(String errKey, String[] params){
        super(errKey, params);
    }

    public SchedulerException(String errKey, String[] params, Throwable cause){
        super(errKey, params, cause);
    }

    public SchedulerException(String errKey, String[] params, Locale locale){
        super(errKey, params, locale);
    }

    public SchedulerException(String errKey, Throwable cause){
        super(errKey, cause);
    }

}
