package com.tmx.beng.medaccess.kyivstar;

import com.tmx.beng.httpsgate.kyivstar.KyivstarBonusNoCommissionGate;
import com.tmx.beng.httpsgate.HttpsGateException;
import com.tmx.beng.base.BillingMessage;
import com.tmx.beng.medaccess.MediumConnection;


public class KyivstarBonusNoCommissionGateMediumConnection implements MediumConnection {
    private KyivstarBonusNoCommissionGate gate = null;

    KyivstarBonusNoCommissionGateMediumConnection() throws HttpsGateException {
        gate = KyivstarBonusNoCommissionGate.getInstance();
    }

    public void send(BillingMessage msg) {
        gate.enqueueBillMessage(msg);
    }
}
