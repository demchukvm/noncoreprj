package com.tmx.web.forms.core.dictionary.seller;

import com.tmx.as.entities.general.user.User;
import com.tmx.as.entities.bill.seller.Seller;
import com.tmx.as.exceptions.DatabaseException;
import com.tmx.as.resolver.FunctionSyntaxException;
import com.tmx.as.resolver.ResolverException;
import com.tmx.as.base.criterion.Restrictions;
import com.tmx.as.base.criterion.CriterionWrapper;
import com.tmx.web.base.BasicActionForm;
import com.tmx.web.controls.Button;
import com.tmx.web.controls.CriterionFactory;
import com.tmx.web.controls.EditBox;
import com.tmx.web.controls.ScrollBox;
import com.tmx.web.controls.table.ControlManagerTable;
import com.tmx.web.controls.table.CheckBoxFilterEnabler;
import com.tmx.web.controls.table.Parameter;
import com.tmx.web.forms.core.base.AbstractFormFactory;

public class ManageSellersForm extends BasicActionForm {

    private ControlManagerTable sellersTable;
    private Button onCreateButton;
    private Button onEditButton;
    private Button onDeleteButton;
    private EditBox editBoxCodeFilter;
    private EditBox editBoxNameFilter;
    private ScrollBox scrollBoxParentSeller;

    protected void init() {
        initBoxes();
        initButtons();
        initTables();
        initFilters();
        sellersTable.addDependedControl(onEditButton).addDependedControl(onDeleteButton);
    }

    private void initBoxes() {
        editBoxCodeFilter = new EditBox();
        editBoxNameFilter = new EditBox();
        scrollBoxParentSeller = new ScrollBox(){

            protected String getKey(Object dataListElement) {
                return ((Seller) dataListElement).getSellerId().toString();
            }

            protected String getValue(Object dataListElement) {
                return ((Seller) dataListElement).getName();
            }
        };
    }

    private void initButtons() {
        onCreateButton = new Button();
        onCreateButton.setLabelKey("ctrl.button.label.create");
        onCreateButton.setAction("/core/dictionary/seller/infoSeller");
        onCreateButton.setCommand("preloadForCreate");

        onEditButton = new Button();
        onEditButton.setLabelKey("ctrl.button.label.edit");
        onEditButton.setAction("/core/dictionary/seller/infoSeller");
        onEditButton.setCommand("preloadForUpdate");

        onDeleteButton = new Button();
        onDeleteButton.setLabelKey("ctrl.button.label.delete");
        onDeleteButton.setAction("/core/dictionary/seller/infoSeller");
        onDeleteButton.setCommand("preloadForDelete");
    }

    private void initTables() {
        sellersTable = new ControlManagerTable();
        sellersTable.setRefreshActionName("/core/dictionary/seller/manageSellers");
        sellersTable.setRefreshCommandName("refreshTable");
    }

    private void initFilters() {
        sellersTable.addParameterToFilter("by_active", new CheckBoxFilterEnabler());
        sellersTable.addParameterToFilter("by_rootSeller", new CheckBoxFilterEnabler());
        sellersTable.addParameterToFilter("by_parent", new Parameter("parent_id", scrollBoxParentSeller));
      //  sellersTable.addParameterToFilter("by_id", new Parameter("id", scrollBoxParentSeller));

        CriterionFactory criterionFactory = new CriterionFactory() {

            public CriterionWrapper createCriterion() {
                //initialize criterion wraper
                CriterionWrapper resultCriterion = Restrictions.isNotNull("sellerId");

                //add restrictions
                if (editBoxNameFilter.getValue() != null) {
                    if(!editBoxNameFilter.getValue().equals("")) {
                    resultCriterion = Restrictions.and(resultCriterion, Restrictions.ilike("name", "%" + editBoxNameFilter.getValue() + "%"));
                    }
                }
                if (editBoxCodeFilter.getValue() != null){
                    if(!editBoxCodeFilter.getValue().equals("")) {
                    resultCriterion = Restrictions.and(resultCriterion, Restrictions.ilike("code", "%" + editBoxCodeFilter.getValue() + "%"));
                    }
                }
//                if (!scrollBoxParentSeller.isNullKeySelected()) {
//                    resultCriterion = Restrictions.and(resultCriterion, Restrictions.eq("parentSeller", scrollBoxParentSeller.getSelectedKey()));
//                }


             return resultCriterion;
            }
        };
        sellersTable.setCriterionFactory(criterionFactory);
    }

    public void bindData(User loggeInUser) throws DatabaseException, FunctionSyntaxException, ResolverException {
        AbstractFormFactory.getInstance(loggeInUser).setUpForm(this);
        sellersTable.refresh();
    }

    //--------Properties
    public ControlManagerTable getSellersTable() {
        return sellersTable;
    }

    public void setSellersTable(ControlManagerTable sellersTable) {
        this.sellersTable = sellersTable;
    }

    public Button getOnCreateButton() {
        return onCreateButton;
    }

    public void setOnCreateButton(Button onCreateButton) {
        this.onCreateButton = onCreateButton;
    }

    public Button getOnEditButton() {
        return onEditButton;
    }

    public void setOnEditButton(Button onEditButton) {
        this.onEditButton = onEditButton;
    }

    public Button getOnDeleteButton() {
        return onDeleteButton;
    }

    public void setOnDeleteButton(Button onDeleteButton) {
        this.onDeleteButton = onDeleteButton;
    }

    public EditBox getEditBoxCodeFilter() {
        return editBoxCodeFilter;
    }

    public void setEditBoxCodeFilter(EditBox editBoxCodeFilter) {
        this.editBoxCodeFilter = editBoxCodeFilter;
    }

    public EditBox getEditBoxNameFilter() {
        return editBoxNameFilter;
    }

    public void setEditBoxNameFilter(EditBox editBoxNameFilter) {
        this.editBoxNameFilter = editBoxNameFilter;
    }

    public ScrollBox getScrollBoxParentSeller() {
        return scrollBoxParentSeller;
    }

    public void setScrollBoxParentSeller(ScrollBox scrollBoxParentSeller) {
        this.scrollBoxParentSeller = scrollBoxParentSeller;
    }
}
