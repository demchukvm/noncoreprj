package com.tmx.web.actions.core.dictionary.processing;

import com.tmx.web.base.BasicDispatchedAction;
import com.tmx.web.forms.core.dictionary.processing.ManageProcessingsForm;
import com.tmx.web.controls.table.Table;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForm;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class ManageProcessingsAction extends BasicDispatchedAction {

    private static final String FORWARD_MANAGE_PROCESSINGS_PAGE = "manageProcessingsPage";

    public ActionForward defaultAction(ActionMapping mapping,
                                       ActionForm form,
                                       HttpServletRequest request,
                                       HttpServletResponse response,
                                       String methodName) throws Throwable {
        ManageProcessingsForm manageProcessingsForm = (ManageProcessingsForm) verifyForm(form, ManageProcessingsForm.class);
        Table processingsTable = manageProcessingsForm.getProcessingsTable();
        processingsTable.refresh();
        return mapping.findForward(FORWARD_MANAGE_PROCESSINGS_PAGE);
    }
    
}
