<%@ page language="java" pageEncoding="UTF-8" contentType="text/html; charset=utf-8"
         errorPage="/core/jsp/message/jsperror.jsp" isELIgnored="false" %>
<%@ taglib uri="/WEB-INF/tld/struts-nested.tld" prefix="nested" %>
<%@ taglib uri="/WEB-INF/tld/tmx-controls.tld" prefix="tmx_ctrl" %>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>


<html:form action="/core/dictionary/processing/infoProcessing">
<table border="0" width="100%">

    <!-- BUTTONS -->
    <tr>
        <td style="text-align: left">
            <tmx_ctrl:button property="onApplyButton"/>
            <tmx_ctrl:button property="onSaveButton"/>
            <tmx_ctrl:button property="onResetButton" onClick="javascript:document.forms[0].reset()"/>
            <tmx_ctrl:button property="onCancelButton"/>
        </td>
    </tr>
    <tr>
        <td valign="center" align="center" class="tabForm">
        <table width="100%" border="0">
            <%--<!-- Form header -->--%>
            <%--<tr>--%>
                <%--<td colspan="2">--%>
                    <%--<h4 class="dataLabel">--%>
                        <%--<bean:message bundle="core.labels" key="core.page.dictionary.kiosk.kioskinfoform.titlegeneral"/>--%>
                    <%--</h4>--%>
                <%--</td>--%>
            <%--</tr>--%>
            <tr>

                <td class="dataLabel" width="15%">
                    <tmx_ctrl:writeEntityLabel attrName="processingId"
                                               entityClassName="com.tmx.as.entities.bill.processing.Processing"/>:
                </td>
                <td class="dataField" width="35%">
                    <tmx_ctrl:editBox property="editBoxProcessingId"/>
                </td>


            </tr>

            <tr>
                <td class="dataLabel">
                    <tmx_ctrl:writeEntityLabel attrName="name"
                                               entityClassName="com.tmx.as.entities.bill.processing.Processing"/>:
                </td>
                <td class="dataField">
                    <tmx_ctrl:editBox property="editBoxName"/>
                </td>

                <td class="dataLabel">
                    <tmx_ctrl:writeEntityLabel attrName="code"
                                               entityClassName="com.tmx.as.entities.bill.processing.Processing"/>:
                </td>
                <td class="dataField" width="35%">
                       <tmx_ctrl:editBox property="editBoxCode" />
                </td>
            </tr>

            <tr>
                <td class="dataLabel" width="15%">
                       <tmx_ctrl:writeEntityLabel attrName="registrationDate"
                                                  entityClassName="com.tmx.as.entities.bill.processing.Processing"/>:
                </td>
                <td class="dataField" width="35%">
                    <tmx_ctrl:timeSelector property="timeSelectorRegistrationDate" />
                </td>

                <td class="dataLabel">
                    <tmx_ctrl:writeEntityLabel attrName="description"
                                               entityClassName="com.tmx.as.entities.bill.processing.Processing"/>:
                </td>
                <td class="dataField">
                    <tmx_ctrl:textArea property="textAreaDescription"/>
                </td>
            </tr>
            <tr>
                <td class="dataLabel" width="15%">
                       <tmx_ctrl:writeEntityLabel attrName="login"
                                                  entityClassName="com.tmx.as.entities.bill.processing.Processing"/>:
                </td>
                <td class="dataField" width="35%">
                    <tmx_ctrl:editBox property="editBoxLogin"/>
                </td>
            </tr>
            <tr>
                <td class="dataLabel" width="15%">
                       <tmx_ctrl:writeEntityLabel attrName="password"
                                                  entityClassName="com.tmx.as.entities.bill.processing.Processing"/>:
                </td>
                <td class="dataField" width="35%">
                    <tmx_ctrl:passwordEditBox property="editBoxPassword"/>
                </td>
            </tr>
            <tr>
                <td class="dataLabel" width="15%">
                       <tmx_ctrl:writeEntityLabel attrName="password2"
                                                  entityClassName="com.tmx.as.entities.bill.processing.Processing"/>:
                </td>
                <td class="dataField" width="35%">
                    <tmx_ctrl:passwordEditBox property="editBoxPassword2"/>
                </td>
            </tr>


        </table>
        </td>

    </tr>



    <%--<!-- BUTTONS -->--%>
    <tr>
        <td style="text-align: left">
            <tmx_ctrl:button property="onApplyButton"/>
            <tmx_ctrl:button property="onSaveButton"/>
            <tmx_ctrl:button property="onResetButton" onClick="javascript:document.forms[0].reset()"/>
            <tmx_ctrl:button property="onCancelButton"/>
        </td>
    </tr>
</table>
</html:form>