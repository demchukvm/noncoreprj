package com.tmx.util.cache;

/**
main interface of cache. Not thread safe
 */
public interface Cache {

    public void init() throws CacheException;

    /** gets object identified by key through the cache  */
    public Object get(Object key) throws CacheException;

    public void clear();

    public int getObjectCount();
}
