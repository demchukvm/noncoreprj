package com.tmx.web.actions.core.tariff;

import com.tmx.as.blogic.BlogicException;
import com.tmx.as.blogic.FunctionManager;
import com.tmx.as.blogic.tariff.AbstractTariffManager;
import com.tmx.as.entities.bill.function.FunctionType;
import com.tmx.as.entities.bill.function.ActualFunctionParameter;
import com.tmx.as.entities.bill.tariff.AbstractTariff;
import com.tmx.as.entities.general.user.User;
import com.tmx.as.exceptions.DatabaseException;
import com.tmx.web.actions.core.AbstractSvbillAction;
import com.tmx.web.base.ValidationException;
import com.tmx.web.base.WebException;
import com.tmx.web.controls.table.Parameter;
import com.tmx.web.forms.core.tariff.AbstractInfoTariffForm;
import com.tmx.util.InitException;
import com.tmx.beng.base.BillException;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.LinkedList;


abstract public class AbstractInfoTariffAction extends AbstractSvbillAction {

    //template methods
    abstract protected void preloadForCreate(AbstractInfoTariffForm form, AbstractTariff restriction, User loggedInUser) throws DatabaseException, WebException;
    abstract protected void preloadForUpdate(AbstractInfoTariffForm form, AbstractTariff restriction) throws DatabaseException;
    abstract protected AbstractTariffManager getTariffManager();


    public ActionForward preloadForDelete(ActionMapping mapping,
                                          ActionForm form,
                                          HttpServletRequest request,
                                          HttpServletResponse response) throws Throwable {
        getRoadmap().collectParameter("/core/message/confirmation", "leftMenuName", "tariffs");
        getRoadmap().setPageForward("confirmation","onOk","local:"+this.getName()+"#"+"deleteTariffAction");
        getRoadmap().setPageForward("confirmation","onCancel","local:"+this.getName()+"#"+ "manageTariffsAction");
        return mapping.findForward("confirmation");
    }

    public ActionForward delete(ActionMapping mapping,
                              ActionForm form,
                              HttpServletRequest request,
                              HttpServletResponse response) throws Throwable {
        AbstractInfoTariffForm restForm = (AbstractInfoTariffForm) verifyForm(form, AbstractInfoTariffForm.class);
        getTariffManager().deleteTariff(restForm.getTariffId());
        return mapping.findForward("manageTariffsAction");
    }

    public ActionForward preloadForCreate(ActionMapping mapping,
                              ActionForm form,
                              HttpServletRequest request,
                              HttpServletResponse response) throws Throwable {
        AbstractInfoTariffForm tariffForm = (AbstractInfoTariffForm) verifyForm(form, AbstractInfoTariffForm.class);
        tariffForm.bindData(getLoggedInUser(request));
        preloadForCreate(tariffForm, null, getLoggedInUser(request));
        return mapping.findForward("infoTariffPage");
    }

    public ActionForward preloadForUpdate(ActionMapping mapping,
                              ActionForm form,
                              HttpServletRequest request,
                              HttpServletResponse response) throws Throwable {
        AbstractInfoTariffForm tariffForm = (AbstractInfoTariffForm) verifyForm(form, AbstractInfoTariffForm.class);
        AbstractTariff tariff = getTariffManager().getTariff(tariffForm.getTariffId());
        tariffForm.setFuncInstanceId(tariff.getFunctionInstance().getFunctionInstanceId());
        tariffForm.bindData(tariff, getLoggedInUser(request));
        Long id = tariff.getFunctionInstance().getFunctionInstanceId();
        tariffForm.getFuncParametrsTable().addParameterToFilter("by_function_instance_id", new Parameter("id", id));
        tariffForm.getFuncParametrsTable().refresh();
        preloadForUpdate(tariffForm, tariff);
        List params = new FunctionManager().getActualFunctionParams(getFuncTypeId(tariffForm));
        tariffForm.getDefaultFuncParametrsTable().setData(params);
        validateActualFuncParams(tariffForm);
        return mapping.findForward("infoTariffPage");
    }

    public ActionForward preloadForUpdateIT(ActionMapping mapping,
                              ActionForm form,
                              HttpServletRequest request,
                              HttpServletResponse response) throws Throwable {
        AbstractInfoTariffForm tariffForm = (AbstractInfoTariffForm) verifyForm(form, AbstractInfoTariffForm.class);
        AbstractTariff tariff = getTariffManager().getTariff(tariffForm.getIncomingTariffId());
        Long id = tariff.getFunctionInstance().getFunctionInstanceId();
        tariffForm.setFuncInstanceId(id);
        tariffForm.bindData(tariff, getLoggedInUser(request));

        tariffForm.getFuncParametrsTable().addParameterToFilter("by_function_instance_id", new Parameter("id", id));
        tariffForm.getFuncParametrsTable().refresh();

        preloadForUpdate(tariffForm, tariff);
        List params = new FunctionManager().getActualFunctionParams(getFuncTypeId(tariffForm));
        if (params != null) {
            tariffForm.getDefaultFuncParametrsTable().setData(params);
        }
//        refreshDefaultParams(mapping, form, request, response);


        validateActualFuncParams(tariffForm);
        return mapping.findForward("infoTariffPage");
    }

    public ActionForward save(ActionMapping mapping,
                              ActionForm form,
                              HttpServletRequest request,
                              HttpServletResponse response) throws Throwable {
        AbstractInfoTariffForm tariffForm = (AbstractInfoTariffForm) verifyForm(form, AbstractInfoTariffForm.class);
        tariffForm.validate();
        AbstractTariff tariff = tariffForm.populateFromControls();
        validateActualFuncParams(tariffForm);
        getTariffManager().saveTariff(tariff);
        tariffForm.setTariffId(tariff.getTariffId());
        String forward = (String) getRoadmap().getParameterValue(AbstractManageTariffsAction.DEFAULT_VALID_ACTION, "onSaveAction", true);
        return mapping.findForward(forward);
    }

    public ActionForward apply(ActionMapping mapping,
                              ActionForm form,
                              HttpServletRequest request,
                              HttpServletResponse response) throws Throwable {
        save(mapping, form, request, response);
        preloadForUpdate(mapping, form, request, response);
        return mapping.findForward("infoTariffPage");
    }

    public ActionForward refreshDefaultParams(ActionMapping mapping,
                              ActionForm form,
                              HttpServletRequest request,
                              HttpServletResponse response) throws Throwable {
        AbstractInfoTariffForm tariffForm = (AbstractInfoTariffForm) verifyForm(form, AbstractInfoTariffForm.class);
        tariffForm.validate(Collections.singletonList(tariffForm.getBrowseBoxFunctionTypes()));
        List defaultParams = new FunctionManager().getActualFunctionParams(getFuncTypeId(tariffForm));
        if (defaultParams != null) {
            tariffForm.getDefaultFuncParametrsTable().setData(defaultParams);
        }
        FunctionType functionType = new FunctionManager().getFunctionType(getFuncTypeId(tariffForm));
        tariffForm.getTextAreaFuncDescription().setValue(functionType.getDescription());
        if (defaultParams != null) {
            tariffForm.getFuncParametrsTable().setData(getClonedList(defaultParams));
        }
        validateActualFuncParams(tariffForm);
        return mapping.findForward("infoTariffPage");
    }

    public ActionForward createParam(ActionMapping mapping,
                              ActionForm form,
                              HttpServletRequest request,
                              HttpServletResponse response) throws Throwable {
        AbstractInfoTariffForm tariffForm = (AbstractInfoTariffForm) verifyForm(form, AbstractInfoTariffForm.class);
        tariffForm.validate(Collections.singletonList(tariffForm.getBrowseBoxFunctionTypes()));
        return mapping.findForward("createParam");
    }

    public ActionForward update(ActionMapping mapping,
                              ActionForm form,
                              HttpServletRequest request,
                              HttpServletResponse response) throws Throwable {
        AbstractInfoTariffForm tariffForm = (AbstractInfoTariffForm) verifyForm(form, AbstractInfoTariffForm.class);
        tariffForm.validate(Collections.singletonList(tariffForm.getBrowseBoxFunctionTypes()));
        validateActualFuncParams(tariffForm);
        return mapping.findForward("infoTariffPage");
    }

    public ActionForward refreshFuncParametrsParamTable(ActionMapping mapping,
                              ActionForm form,
                              HttpServletRequest request,
                              HttpServletResponse response) throws Throwable {
        return mapping.findForward("infoTariffPage");
    }

    private List<ActualFunctionParameter> getClonedList(List<ActualFunctionParameter> params) throws CloneNotSupportedException {
        List<ActualFunctionParameter> list = new ArrayList<ActualFunctionParameter>();
        for (ActualFunctionParameter param : params) {
            list.add(param.clone());
        }
        return list;
    }

    private Long getFuncTypeId(AbstractInfoTariffForm tariffForm) {
        if (tariffForm.getBrowseBoxFunctionTypes().getKey() != null && !"".equals(tariffForm.getBrowseBoxFunctionTypes().getKey()))
            return new Long(tariffForm.getBrowseBoxFunctionTypes().getKey());
        return null;
    }

    private Long getFunctionInstanceId(AbstractInfoTariffForm tariffForm) {
        return tariffForm.getFuncInstanceId();
    }

    private void validateActualFuncParams(AbstractInfoTariffForm tariffForm)
            throws ValidationException, BlogicException, InitException, BillException {
        List errors = new FunctionManager().validateActualFunctionParams(getFuncTypeId(tariffForm), tariffForm.getFuncParametrsTable().getData());
        //if params not success
        if (errors != null && !errors.isEmpty()) {
            tariffForm.getParamErrorTable().setData(errors);
            throw new ValidationException(null);
        } else
            tariffForm.getParamErrorTable().clean();
    }
    
}
