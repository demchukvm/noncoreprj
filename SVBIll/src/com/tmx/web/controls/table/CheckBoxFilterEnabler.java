package com.tmx.web.controls.table;

import com.tmx.web.controls.CheckBox;


public class CheckBoxFilterEnabler extends CheckBox implements FilterEnabler{
    public boolean isFilterEnabled() {
        return isSelected();  
    }
}
