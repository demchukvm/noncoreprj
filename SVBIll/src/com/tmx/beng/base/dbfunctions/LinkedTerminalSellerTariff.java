package com.tmx.beng.base.dbfunctions;

import com.tmx.beng.base.cache.InvokeFunctionException;
import com.tmx.beng.base.cache.DBCache;
import com.tmx.beng.base.cache.DBCacheException;
import com.tmx.beng.base.BillingMessage;
import com.tmx.beng.base.Reservation;
import com.tmx.beng.base.ReservationSet;
import com.tmx.as.entities.bill.transaction.TransactionType;
import com.tmx.as.entities.bill.seller.Seller;
import com.tmx.as.entities.bill.terminal.Terminal;

import java.util.*;

/**
 */
public class LinkedTerminalSellerTariff extends BasicTariffFunction {
    public static final String DISCOUNT_PERCENT = "discount_percent";
//    public static final String ANOTHER_SELLER_CODE = "another_seller_code";

    boolean isTariffApplicable(Map actualParams) {
        return BillingMessage.O_REFILL_PAYMENT.equals(task.getProcesedMessage().getOperationName());
    }


    void applyTariff(Map actualParams, Reservation reservation) throws InvokeFunctionException {
        String terminalSN = null;
        try {
            Double discountPercent;
            try {
                discountPercent = Double.parseDouble((String)actualParams.get(DISCOUNT_PERCENT));
            } catch (Exception e) {
                discountPercent = 1d;
            }
            if (discountPercent == null) {
                discountPercent = 1d;
            }

            BillingMessage msg = task.getProcesedMessage();
            DBCache dbCache = task.getSessionDBCache();
            ReservationSet reservSet = (ReservationSet) msg.getAttribute("balanceReservationSet");

            terminalSN = (String) msg.getAttribute(BillingMessage.TERMINAL_SN);
            Terminal terminal = dbCache.getTerminal(terminalSN);

            Seller seller = dbCache.getSellerById(terminal.getSellerOwnerId());
            if(seller == null) {
                throw new InvokeFunctionException("err.terminal_tariff_calc_error: can`t retrive seller for terminal "
                                                            + terminal.getSerialNumber());
            }



            Reservation sellerReservation = reservSet.allocateReservation(Seller.class,
                    seller.getCode(), seller.getPrimaryBalance().getCode());
                  
            Reservation.Reserve reserv = sellerReservation.reservesIterator().next();
            reservation.reserve(null, (reserv.getAmount() * discountPercent), null, TransactionType.CODE_CREDIT);
        }
        catch (DBCacheException e) {
            throw new InvokeFunctionException("err.terminal_tariff_calc_error can`t retrive terminal " + terminalSN + " " + e.getMessage());
        }
        catch (Throwable e) {
            throw new InvokeFunctionException("err.terminal_tariff_calc_error", e);
        }
    }


    public ValidationError[] validate(Map actualParams) {
        List resultErrors = new ArrayList();

//        resultErrors.addAll(ParameterValidator.validateParamNames(actualParams, new String[] {ANOTHER_SELLER_CODE}));
//        resultErrors.addAll(ParameterValidator.validateDiscountPercent(actualParams, new String[] {DISCOUNT_PERCENT}));

        return (ValidationError[]) resultErrors.toArray(new ValidationError[resultErrors.size()]);
    }

    public TariffParameter[] getDefaultParams() {
        TariffParameter discountPercent = new TariffParameter();
        discountPercent.setName(DISCOUNT_PERCENT);
        discountPercent.setValue("1");
        discountPercent.setOrderNum(1l);
        discountPercent.setMandatory(Boolean.FALSE);

        return new TariffParameter[]{discountPercent};
    }
}