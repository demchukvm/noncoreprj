package com.tmx.web.forms.trans_mgmt;

import com.tmx.beng.access.Access;
import com.tmx.web.controls.Button;
import com.tmx.web.controls.EditBox;
import com.tmx.web.base.BasicActionForm;

public class TransactionInfoMtsForm extends BasicActionForm {
    private Access billingEngineAccess = null;
    private Button onCancelButton = null;
    private Button onInfoButton = null;
    private Button onRefreshBalance = null;
    private EditBox editBoxBillTransaction = null;
    private EditBox editBoxCliTransaction = null;
    private EditBox editBoxStatusCode = null;
    private EditBox editBoxStatusMessage = null;
    private EditBox editBoxTransactionTime = null;
    private EditBox editBoxAmount = null;
    private EditBox editBoxCientTime = null;
    private EditBox editBoxAccountNumber = null;
    private EditBox editBoxSellerName = null;
    private EditBox editBoxTerminalNumber = null;
    private EditBox editBoxOperatorName = null;
    private EditBox editBoxMtsBalance = null;

    private EditBox ebFirstPageUrl = null;
    private EditBox ebTotalPages = null;
    private EditBox ebFirstValidMessage = null;
    private Button onGetResult = null;

    private String v1 = "#1 - 0";
    private String v2 = "#2 - 0";
    private String v3 = "#3 - 0";
    private String v4 = "#4 - 0";
    private String v5 = "#5 - 0";
    private String v6 = "#6 - 0";

    public void init() {
        //set up buttons
        onCancelButton = new Button();
        onCancelButton.setLabelKey("ctrl.button.label.cancel");
        onCancelButton.setAction("/transactionInfoMtsAction");
        onCancelButton.setCommand("cancelTransaction");

        onInfoButton = new Button();
        onInfoButton.setLabelKey("ctrl.button.label.info");
        onInfoButton.setAction("/transactionInfoMtsAction");
        onInfoButton.setCommand("info");

        onRefreshBalance = new Button();
        onRefreshBalance.setLabelKey("ctrl.button.label.refresh_balance");
        onRefreshBalance.setAction("/transactionInfoMtsAction");
        onRefreshBalance.setCommand("refreshBalance");

        onGetResult = new Button();
        onGetResult.setLabelKey("ctrl.button.label.ok");
        onGetResult.setAction("/transactionInfoMtsAction");
        onGetResult.setCommand("getResult");

        // set up edit
        editBoxBillTransaction = new EditBox();
        editBoxCliTransaction = new EditBox();
        editBoxStatusCode = new EditBox();
        editBoxStatusCode.setReadonly(true);
        editBoxStatusMessage = new EditBox();
        editBoxStatusMessage.setReadonly(true);
        editBoxTransactionTime = new EditBox();
        editBoxTransactionTime.setReadonly(true);
        editBoxAmount = new EditBox();
        editBoxAmount.setReadonly(true);
        editBoxCientTime = new EditBox();
        editBoxCientTime.setReadonly(true);
        editBoxAccountNumber = new EditBox();
        editBoxAccountNumber.setReadonly(true);
        editBoxSellerName = new EditBox();
        editBoxSellerName.setReadonly(true);
        editBoxTerminalNumber = new EditBox();
        editBoxTerminalNumber.setReadonly(true);
        editBoxOperatorName = new EditBox();
        editBoxOperatorName.setReadonly(true);
        editBoxMtsBalance = new EditBox();
        editBoxMtsBalance.setReadonly(true);

        ebFirstPageUrl = new EditBox();
        ebTotalPages = new EditBox();
        ebFirstValidMessage = new EditBox();
    }

    public void clear() {
        editBoxStatusCode.setValue("");
        editBoxStatusMessage.setValue("");
        editBoxTransactionTime.setValue("");
        editBoxAmount.setValue("");
        editBoxCientTime.setValue("");
        editBoxAccountNumber.setValue("");
        editBoxSellerName.setValue("");
        editBoxTerminalNumber.setValue("");
        editBoxOperatorName.setValue("");
    }

    public Button getOnRefreshBalance() {
        return onRefreshBalance;
    }

    public void setOnRefreshBalance(Button onRefreshBalance) {
        this.onRefreshBalance = onRefreshBalance;
    }

    public EditBox getEditBoxMtsBalance() {
        return editBoxMtsBalance;
    }

    public void setEditBoxMtsBalance(EditBox editBoxMtsBalance) {
        this.editBoxMtsBalance = editBoxMtsBalance;
    }

    public EditBox getEditBoxOperatorName() {
        return editBoxOperatorName;
    }

    public void setEditBoxOperatorName(EditBox editBoxOperatorName) {
        this.editBoxOperatorName = editBoxOperatorName;
    }

    public EditBox getEditBoxTransactionTime() {
        return editBoxTransactionTime;
    }

    public void setEditBoxTransactionTime(EditBox editBoxTransactionTime) {
        this.editBoxTransactionTime = editBoxTransactionTime;
    }

    public EditBox getEditBoxAmount() {
        return editBoxAmount;
    }

    public void setEditBoxAmount(EditBox editBoxAmount) {
        this.editBoxAmount = editBoxAmount;
    }

    public EditBox getEditBoxCientTime() {
        return editBoxCientTime;
    }

    public void setEditBoxCientTime(EditBox editBoxCientTime) {
        this.editBoxCientTime = editBoxCientTime;
    }

    public EditBox getEditBoxAccountNumber() {
        return editBoxAccountNumber;
    }

    public void setEditBoxAccountNumber(EditBox editBoxAccountNumber) {
        this.editBoxAccountNumber = editBoxAccountNumber;
    }

    public EditBox getEditBoxSellerName() {
        return editBoxSellerName;
    }

    public void setEditBoxSellerName(EditBox editBoxSellerName) {
        this.editBoxSellerName = editBoxSellerName;
    }

    public EditBox getEditBoxTerminalNumber() {
        return editBoxTerminalNumber;
    }

    public void setEditBoxTerminalNumber(EditBox editBoxTerminalNumber) {
        this.editBoxTerminalNumber = editBoxTerminalNumber;
    }

    public EditBox getEditBoxCliTransaction() {
        return editBoxCliTransaction;
    }

    public void setEditBoxCliTransaction(EditBox editBoxCliTransaction) {
        this.editBoxCliTransaction = editBoxCliTransaction;
    }

    public Button getOnInfoButton() {
        return onInfoButton;
    }

    public void setOnInfoButton(Button onInfoButton) {
        this.onInfoButton = onInfoButton;
    }

    public Button getOnCancelButton() {
        return onCancelButton;
    }

    public void setOnCancelButton(Button onCancelButton) {
        this.onCancelButton = onCancelButton;
    }

    public EditBox getEditBoxStatusCode() {
        return editBoxStatusCode;
    }

    public void setEditBoxStatusCode(EditBox editBoxStatusCode) {
        this.editBoxStatusCode = editBoxStatusCode;
    }

    public EditBox getEditBoxStatusMessage() {
        return editBoxStatusMessage;
    }

    public void setEditBoxStatusMessage(EditBox editBoxStatusMessage) {
        this.editBoxStatusMessage = editBoxStatusMessage;
    }

    public Access getBillingEngineAccess() {
        return billingEngineAccess;
    }

    public void setBillingEngineAccess(Access billingEngineAccess) {
        this.billingEngineAccess = billingEngineAccess;
    }

    public EditBox getEditBoxBillTransaction() {
        return editBoxBillTransaction;
    }

    public void setEditBoxBillTransaction(EditBox editBoxBillTransaction) {
        this.editBoxBillTransaction = editBoxBillTransaction;
    }

    public EditBox getEbFirstPageUrl() {
        return ebFirstPageUrl;
    }

    public void setEbFirstPageUrl(EditBox ebFirstPageUrl) {
        this.ebFirstPageUrl = ebFirstPageUrl;
    }

    public Button getOnGetResult() {
        return onGetResult;
    }

    public void setOnGetResult(Button onGetResult) {
        this.onGetResult = onGetResult;
    }

    public EditBox getEbTotalPages() {
        return ebTotalPages;
    }

    public void setEbTotalPages(EditBox ebTotalPages) {
        this.ebTotalPages = ebTotalPages;
    }

    public EditBox getEbFirstValidMessage() {
        return ebFirstValidMessage;
    }

    public void setEbFirstValidMessage(EditBox ebFirstValidMessage) {
        this.ebFirstValidMessage = ebFirstValidMessage;
    }

    public String getV1() {
        return v1;
    }

    public void setV1(String v1) {
        this.v1 = v1;
    }

    public String getV2() {
        return v2;
    }

    public void setV2(String v2) {
        this.v2 = v2;
    }

    public String getV3() {
        return v3;
    }

    public void setV3(String v3) {
        this.v3 = v3;
    }

    public String getV4() {
        return v4;
    }

    public void setV4(String v4) {
        this.v4 = v4;
    }

    public String getV5() {
        return v5;
    }

    public void setV5(String v5) {
        this.v5 = v5;
    }

    public String getV6() {
        return v6;
    }

    public void setV6(String v6) {
        this.v6 = v6;
    }
}
