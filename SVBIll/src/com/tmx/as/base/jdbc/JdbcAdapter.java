package com.tmx.as.base.jdbc;

import java.sql.Connection;
import java.sql.SQLException;

/**
   JdbcAdapter used to hide database specific jdbc implementation 
 */
public interface JdbcAdapter {
    /**
     * Adopt jdbc parameter with underlie driver
     * @param param param object
     * @param typeName user defined type name. Use this if param belongs to such type defined in DB
     * @param jdbcConn current jdbc connection
     * @return adopted object
     * @throws SQLException on adaptation error */
    public Object adoptParameter(Object param, String typeName, Connection jdbcConn) throws SQLException;
}
