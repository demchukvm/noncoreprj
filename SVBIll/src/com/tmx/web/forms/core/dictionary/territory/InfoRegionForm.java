package com.tmx.web.forms.core.dictionary.territory;

import com.tmx.web.base.BasicActionForm;
import com.tmx.web.controls.EditBox;
import com.tmx.web.controls.ScrollBox;
import com.tmx.web.controls.Button;
import com.tmx.web.controls.PopulateUtil;
import com.tmx.as.entities.network.territory.Country;
import com.tmx.as.entities.network.territory.Region;
import com.tmx.as.entities.network.territory.District;

public class InfoRegionForm extends BasicActionForm {
    private Long selectedId = null;
    private EditBox id = new EditBox();
    private EditBox shortName = new EditBox();
    private EditBox name = new EditBox();
    private ScrollBox country = new ScrollBox(){
        //-----------Methods to be implemented in the particular successor
        protected String getKey(Object dataListElement) {
            return ((Country)dataListElement).getId().toString();
        }
        protected String getValue(Object dataListElement) {
            return ((Country)dataListElement).getName();
        }
    };

    private Button onSaveButton         = new Button();
    private Button  onResetButton        = new Button();
    private Button  onCancelButton       = new Button();


    protected void init() {
            id.setReadonly(true);
            shortName.setMandatory(true);
            name.setMandatory(true);
            //set up buttons
            onSaveButton = new Button();
            onSaveButton.setLabelKey("ctrl.button.label.save");
            onSaveButton.setAction("/core/dictionary/territory/infoRegion");
            onSaveButton.setCommand("update");

            onResetButton = new Button();
            onResetButton.setLabelKey("ctrl.button.label.reset");

            onCancelButton = new Button();
            onCancelButton.setLabelKey("ctrl.button.label.cancel");
            onCancelButton.setAction("/core/dictionary/territory/manageRegions");
        }


        public void bindData(Region value) {
            id.setValue(value.getId() == null ? "" : value.getId().toString());
            shortName.setValue(value.getShortName());
            name.setValue(value.getName());
            try{
                country.setSelectedKey(value.getDistrict().getId().toString());
            }catch(Throwable t){}
        }

        public Region populateFromControls(Region value) {
            value.setRegionId(PopulateUtil.getLongValue(id));
            value.setShortName(shortName.getValue());
            value.setName(name.getValue());
            District district = new District();
//            district.setCountry(new Long(this.country.getSelectedKey()));
            value.setDistrict(district);
            return value;
        }

        //-------------------PROPERTIES

    public Long getSelectedId() {
        return selectedId;
    }

    public void setSelectedId(Long selectedId) {
        this.selectedId = selectedId;
    }

    public EditBox getId() {
        return id;
    }

    public void setId(EditBox id) {
        this.id = id;
    }

    public EditBox getShortName() {
        return shortName;
    }

    public void setShortName(EditBox shortName) {
        this.shortName = shortName;
    }

    public EditBox getName() {
        return name;
    }

    public void setName(EditBox name) {
        this.name = name;
    }

    public ScrollBox getCountry() {
        return country;
    }

    public void setCountry(ScrollBox country) {
        this.country = country;
    }

    public Button getOnSaveButton() {
        return onSaveButton;
    }

    public void setOnSaveButton(Button onSaveButton) {
        this.onSaveButton = onSaveButton;
    }

    public Button getOnResetButton() {
        return onResetButton;
    }

    public void setOnResetButton(Button onResetButton) {
        this.onResetButton = onResetButton;
    }

    public Button getOnCancelButton() {
        return onCancelButton;
    }

    public void setOnCancelButton(Button onCancelButton) {
        this.onCancelButton = onCancelButton;
    }
}
