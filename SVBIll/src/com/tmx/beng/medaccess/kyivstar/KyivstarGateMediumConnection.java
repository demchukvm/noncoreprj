package com.tmx.beng.medaccess.kyivstar;

import com.tmx.beng.base.BillingMessage;
import com.tmx.beng.httpsgate.kyivstar.KyivstarGate;
import com.tmx.beng.httpsgate.HttpsGateException;
import com.tmx.beng.medaccess.MediumConnection;

/**
    Medium connection to work with Mobipay Kyivstar gate
 */
public class KyivstarGateMediumConnection implements MediumConnection {
    private KyivstarGate kyivstarGate = null;

    KyivstarGateMediumConnection() throws HttpsGateException {
        kyivstarGate = KyivstarGate.getInstance();
    }

    public void send(BillingMessage msg) {
        kyivstarGate.enqueueBillMessage(msg);
    }
}
