package com.tmx.web.forms.graphengine;

import com.tmx.web.base.BasicActionForm;
import com.tmx.engines.graphengine.base.ApproximationReport;

public class BasicGraphPublisherForm extends BasicActionForm {
    private ApproximationReport approximationReport = null;
    private String graphXmlSerialized = null;

    public String getGraphXmlSerialized() {
        return graphXmlSerialized;
    }

    public void setGraphXmlSerialized(String graphXmlSerialized) {
        this.graphXmlSerialized = graphXmlSerialized;
    }

    public ApproximationReport getApproximationReport() {
        return approximationReport;
    }

    public void setApproximationReport(ApproximationReport approximationReport) {
        this.approximationReport = approximationReport;
    }

}
