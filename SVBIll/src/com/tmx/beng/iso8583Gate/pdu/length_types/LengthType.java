package com.tmx.beng.iso8583Gate.pdu.length_types;

import com.tmx.beng.iso8583Gate.pdu.util.BCDConverter;
import com.tmx.beng.iso8583Gate.pdu.IsoParseException;
import com.tmx.beng.iso8583Gate.pdu.IsoSerializeException;

import java.io.InputStream;
import java.io.OutputStream;

/**
 */
public abstract class LengthType {
    protected BCDConverter bcdConverter = new BCDConverter();
    protected String name;

    public abstract int parseLength(InputStream is, int maxLength) throws IsoParseException;

    //public abstract void serializeLength(OutputStream os, int length) throws IsoSerializeException;

    public abstract byte[] serializeLength(int length) throws IsoSerializeException;

    /** type name */
    public String getName(){
        return name;
    }
}
