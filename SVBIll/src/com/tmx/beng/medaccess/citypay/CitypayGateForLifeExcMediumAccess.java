package com.tmx.beng.medaccess.citypay;

import com.tmx.beng.medaccess.GateException;
import com.tmx.beng.medaccess.MediumAccess;
import com.tmx.beng.medaccess.MediumConnection;
import com.tmx.beng.medaccess.dacard.DacardGateMediumConnection;

public class CitypayGateForLifeExcMediumAccess implements MediumAccess {

    public void init()
    {

    }

    public MediumConnection getConnection() throws GateException
    {
        return new CitypayGateForLifeExcMediumConnection();
    }
}