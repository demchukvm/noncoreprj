package com.tmx.as.base;

import com.tmx.as.base.beans.*;
import com.tmx.as.modules.ModuleException;
import com.tmx.util.Configuration;
import com.tmx.util.InitException;
import org.apache.log4j.Logger;

import java.util.*;
import java.util.regex.Pattern;
import java.util.regex.Matcher;
import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {
    private Logger logger = Logger.getLogger("core.Server");
    private final String CONFIG_FILE = "modules.config_file";
    private Map moduleMap = new HashMap();
    private Map profiles = new HashMap();
    public static final String ACT_START = "start";
    public static final String ACT_SHUTDOWN_ABORT = "shutdown_abort";
    public static final String ACT_SHUTDOWN = "shutdown";

    public Server() throws InitException {
        reload();
    }

    private void reload()throws InitException {
        try{
            EmbModulesConfigDocument root = EmbModulesConfigDocument.Factory.parse(
                    new File(Configuration.getInstance().getProperty(CONFIG_FILE)));

            // load embModules
            com.tmx.as.base.beans.Module[] modules = root.getEmbModulesConfig().getEmbModules().getModuleArray();
            for(int i = 0; i < modules.length; i++){
                EmbModule embModule = new EmbModule();
                embModule.name = modules[i].getName();
                embModule.implClass = Class.forName(modules[i].getImplClass());
                embModule.disabled = modules[i].getDisabled();
                moduleMap.put(embModule.name, embModule);
            }

            // load  profiles
            RunProfile[] runProfiles = root.getEmbModulesConfig().getRunProfiles().getRunProfileArray();
            for(int i = 0; i < runProfiles.length; i++){
                Profile profile = new Profile(runProfiles[i].getName());
                profile.startModules = loadModules(runProfiles[i].getStart().getUseModuleArray());
                profile.startModules = checkDepends(profile.startModules);
                profile.shutdownAbortModules = loadModules(runProfiles[i].getShutdownAbort().getUseModuleArray());
                profile.shutdownAbortModules = checkDepends(profile.shutdownAbortModules);
                profile.shutdownGracefulModules = loadModules(runProfiles[i].getShutdownGraceful().getUseModuleArray());
                profile.shutdownGracefulModules = checkDepends(profile.shutdownGracefulModules);
                profiles.put(profile.name, profile);
            }

        }catch(Throwable e){
            e.printStackTrace();
            throw new InitException("load modules config error", e);
        }
    }

    private List checkDepends(List list)throws Throwable{
        List result = new Vector();
        Set runSet = new HashSet();
        int list_size = list.size();
        for(int k = 0; k < list_size; k++){
            for(int i = 0; i < list.size(); i++){
                Module module = (Module) list.get(i);
                boolean fail = false;
                for(int j = 0; j < module.depends.size(); j++){
                    if(!runSet.contains(module.depends.get(j))){
                        fail = true;
                        break;
                    }
                }
                if(!fail){
                    runSet.add(module.name);
                    result.add(module);
                    list.remove(i);
                    i--;
                }
            }
            if(list.size() == 0){
                break;
            }
        }
        if(list.size() != 0){
            StringBuffer modules = new StringBuffer();
            for(int i = 0; i < list.size(); i++){
                modules.append(list.get(i)).append(", ");
            }
            logger.error("Inaccessible depends found " + modules.toString());
            throw new ModuleException("Inaccessible depends found " + modules.toString());
        }
        return result;
    }

    private List loadModules(UseModule[] useModules)throws Throwable{
        Module module;
        List list = new Vector();

        // load module list
        for(int i = 0; i < useModules.length; i++){
            module = new Module();
            try{
                module.ignoreFail = useModules[i].getIgnoreFail();
                module.name = checkModuleName(useModules[i].getName());
                String dependsString = useModules[i].getDepends();
                if(dependsString != null){
                    String[] depends = dependsString.split(",");
                    for(int j = 0; j < depends.length; j++){
                        module.depends.add(checkModuleName(depends[j]));
                    }
                }
                Parameter[] parameters = useModules[i].getParamArray();
                for(int j = 0; j < parameters.length; j++){
                    module.properties.setProperty(parameters[j].getName(), parameters[j].getValue());
                }

                list.add(module);
            }catch(ModuleException e){
                if(!module.ignoreFail){
                    throw e;
                }else{
                    logger.warn(e.getLocalizedMessage());
                }
            }
        }
        return list;
    }

    private String checkModuleName(String name) throws ModuleException{
        String result = name.trim();
        EmbModule embModule = (EmbModule)moduleMap.get(result);
        if(embModule == null){
            throw new ModuleException("Uncknown module name", new String[]{ result });
        }
        if(embModule.disabled){
            throw new ModuleException("Module is disabled", new String[]{ result });
        }
        return result;
    }

    private void runProfile(String profileName, String action)throws Throwable{
        Profile profile = (Profile)profiles.get(profileName);
        if(profile == null){
            throw new InitException("Uncknown profile name " + profileName);
        }

        if(ACT_START.equals(action)){
            for(Iterator it = profile.startModules.iterator(); it.hasNext(); ){
                Module module = (Module) it.next();
                logger.info("Starting module " + module.name);
                try{
                    EmbModule embModule = (EmbModule)moduleMap.get(module.name);
                    EmbeddedModule embeddedModule = (EmbeddedModule)embModule.implClass.newInstance();
                    embeddedModule.start(module.properties);
                }catch(Throwable e){
                    e.printStackTrace();
                    if(!module.ignoreFail){
                        throw new ModuleException("Start module error. Module " + module.name);
                    }
                }
            }
        }else if(ACT_SHUTDOWN.equals(action)){
            for(Iterator it = profile.shutdownGracefulModules.iterator(); it.hasNext(); ){
                Module module = (Module) it.next();
                logger.info("Shutdown module " + module.name);
                try{
                    EmbModule embModule = (EmbModule)moduleMap.get(module.name);
                    EmbeddedModule embeddedModule = (EmbeddedModule)embModule.implClass.newInstance();
                    embeddedModule.shutdownGraceful(module.properties);
                }catch(Throwable e){
                    e.printStackTrace();
                    if(!module.ignoreFail){
                        throw new ModuleException("Shutdown module error. Module " + module.name);
                    }
                }
            }

        }else if(ACT_SHUTDOWN_ABORT.equals(action)){
            for(Iterator it = profile.shutdownAbortModules.iterator(); it.hasNext(); ){
                Module module = (Module) it.next();
                logger.info("Aborting module " + module.name);
                try{
                    EmbModule embModule = (EmbModule)moduleMap.get(module.name);
                    EmbeddedModule embeddedModule = (EmbeddedModule)embModule.implClass.newInstance();
                    embeddedModule.shutdownAbort(module.properties);
                }catch(Throwable e){
                    e.printStackTrace();
                    if(!module.ignoreFail){
                        throw new ModuleException("Abort module error. Module " + module.name);
                    }
                }
            }

        }else{
            throw new InitException("Uncknown action " + action);
        }
    }

    private class EmbModule{
        String name;
        Class implClass;
        boolean disabled;
    }

    private class Module{
        String name;
        List depends = new Vector();
        boolean ignoreFail = false;
        Properties properties = new Properties();
    }

    private class Profile{
        String name;
        List startModules;
        List shutdownAbortModules;
        List shutdownGracefulModules;

        Profile(String name){
            this.name = name;
        }
    }

    public void start(Set params)throws InitException{
        for(Iterator it = params.iterator(); it.hasNext(); ){
            String param = (String)it.next();
            String[] profile = param.split("=");
            if(profile.length == 2 && "profile".equalsIgnoreCase(profile[0])){
                try{
                    // start port listener
                    new PortListener().start();

                    runProfile(profile[1], ACT_START);
                }catch(Throwable e){
                    throw new InitException("Run profile error " + profile[1], e);
                }


                return;
            }
        }
        throw new InitException("No profile specified");
    }

    public int getExternalPort(){
        return Integer.parseInt(Configuration.getInstance().getProperty("shutdown_port", "8008"));
    }

    /**
     * P
     */
    private class PortListener extends Thread {

        public PortListener() {
            setName("EXT_PORT_LISTENER");
        }

        public void run() {
            try {
                ServerSocket ss = new ServerSocket(getExternalPort());
                logger.info("Port listener thread started on "+getExternalPort()+".");
                while (true) {
                    final Socket conn = ss.accept();
                    new Thread() {

                        {
                            setName("Port listener service");
                        }

                        public void run() {
                            InputStream is;
                            OutputStream os;
                            try {
                                is = conn.getInputStream();
                                os = conn.getOutputStream();
                                externalPortProcess(is, os);
                            }catch (Throwable e) {
                                System.err.println("Failed to execute external command:");
                                e.printStackTrace();
                            }
                        }

                    }.start();
                }
            }catch (IOException e) {
                e.printStackTrace();
            }
        }

        private void externalPortProcess(InputStream is, OutputStream os) throws Throwable{
            DataInputStream dis = new DataInputStream(new BufferedInputStream(is));
            DataOutputStream dos = new DataOutputStream(new BufferedOutputStream(os));
            Pattern cmdPattern = Pattern.compile("(.+) profile=(.+)", Pattern.CASE_INSENSITIVE);
            try {
                while (true) {
                    String command = dis.readLine();
                    logger.info("Command '" + command + "' have been received.");

                    Matcher m = cmdPattern.matcher(command);
                    if(m.matches()){
                        if(ACT_SHUTDOWN.equals(m.group(1))){
                            try{
                                runProfile(m.group(2), ACT_SHUTDOWN);
                            }catch(Throwable e){
                                e.printStackTrace();
                            }
                            System.exit(0);
                        }else if(ACT_SHUTDOWN_ABORT.equals(m.group(1))){
                            try{
                                runProfile(m.group(2), ACT_SHUTDOWN_ABORT);
                            }catch(Throwable e){
                                e.printStackTrace();
                            }
                            Runtime.getRuntime().halt(0);
                        }else{
                            logger.info("uncknown action" + m.group(2));
                        }
                    }

                }
            }catch(Throwable ex) {
                ex.printStackTrace();
            }finally{
                try{
                    dis.close();
                    dos.close();
                }catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
        }

    }
}
