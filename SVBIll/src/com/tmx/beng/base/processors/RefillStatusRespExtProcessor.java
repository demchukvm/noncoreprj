package com.tmx.beng.base.processors;

import com.tmx.beng.base.*;
import com.tmx.beng.base.cache.DBCache;

import java.util.Date;
import java.text.SimpleDateFormat;
import java.nio.charset.Charset;
import java.io.UnsupportedEncodingException;

/**
Creates refill status response message and sends it via callback.
 */
public class RefillStatusRespExtProcessor extends TaskProcessor {
        static char[] obrobliaetsa = {'\u041E','\u0411', '\u0420', '\u041E',
                                     '\u0411', '\u041B', '\u042F', '\u042D',
                                    '\u0422', '\u042C', '\u0421',  '\u042F'};

    public Task process0(Task task, DBCache dbCache, BillingMessageWritable msg) {
        logger.debug("Refill status response...");

        BillingMessageImpl resp = new BillingMessageImpl();
        resp.setOperationName(msg.getOperationName());
        resp.setAttributeString(BillingMessage.BILL_TRANSACTION_NUM,
                msg.getAttributeString(BillingMessage.BILL_TRANSACTION_NUM));
        if(msg.getAttribute(BillingMessage.RECEIPT_NUM) != null){
            resp.setAttribute(BillingMessage.RECEIPT_NUM, msg.getAttribute(BillingMessage.RECEIPT_NUM));
        }
        if(msg.getAttribute(BillingMessage.PAY_ID) != null){
            resp.setAttribute(BillingMessage.PAY_ID, msg.getAttribute(BillingMessage.PAY_ID));
        }
        if(msg.getAttribute(BillingMessage.AMOUNT) != null){
            resp.setAttribute(BillingMessage.AMOUNT, msg.getAttribute(BillingMessage.AMOUNT));
        }

        //to support Life gate integration
        if(msg.getAttribute(BillingMessage.VOUCHER_CODE) != null)
            resp.setAttribute(BillingMessage.VOUCHER_CODE, msg.getAttribute(BillingMessage.VOUCHER_CODE));

        if(msg.getAttribute(BillingMessage.VOUCHER_NOMINAL) != null)
            resp.setAttribute(BillingMessage.VOUCHER_NOMINAL, msg.getAttribute(BillingMessage.VOUCHER_NOMINAL));

        if(msg.getAttribute(BillingMessage.VOUCHER_SECRET_KEY) != null)
            resp.setAttribute(BillingMessage.VOUCHER_SECRET_KEY, msg.getAttribute(BillingMessage.VOUCHER_SECRET_KEY));

        if(msg.getAttribute(BillingMessage.VOUCHER_BEST_BEFORE_DATE) != null)
            //resp.setAttribute(BillingMessage.VOUCHER_BEST_BEFORE_DATE, new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format((Date)msg.getAttribute(BillingMessage.VOUCHER_BEST_BEFORE_DATE)));
            resp.setAttribute(BillingMessage.VOUCHER_BEST_BEFORE_DATE, (Date)msg.getAttribute(BillingMessage.VOUCHER_BEST_BEFORE_DATE));


        resp.setAttributeString(BillingMessage.STATUS_CODE, msg.getAttributeString(BillingMessage.STATUS_CODE));
//        resp.setAttributeString(BillingMessage.STATUS_MESSAGE, msg.getAttributeString(BillingMessage.STATUS_MESSAGE));
        if (msg.getAttributeString(BillingMessage.STATUS_CODE).equals("1")) {
            resp.setAttributeString(BillingMessage.STATUS_MESSAGE, new String(obrobliaetsa));
        } else {
            resp.setAttributeString(BillingMessage.STATUS_MESSAGE, msg.getAttributeString(BillingMessage.STATUS_MESSAGE));
        }
        resp.setAttributeString(BillingMessage.WAIT_TIME, String.valueOf(task.getGlobalContext().getReporter().getWaitTimeEstimate()));

        //todo fix that                        
        Date d = task.getTransaction() != null ?
                task.getTransaction().getRegistrationDate() :
                (Date)msg.getAttribute(BillingMessage.REGISTRATION_TIME);

        resp.setAttribute(BillingMessage.REGISTRATION_TIME, d);
        CallBack callBack = task.getCallBack();
        if(callBack != null){
            callBack.sendBilledMessage(resp);
        }

        return task;
    }
}