package com.tmx.beng.base.processors;

import com.tmx.beng.base.*;
import com.tmx.beng.base.cache.DBCache;

/**
 Close transaction task 
 */
public class CloseTransactionProcessor extends TaskProcessor {

    public Task process0(Task task, DBCache dbCache, BillingMessageWritable msg){
        logger.debug("Closing transaction...");
        task.getGlobalContext().getTransactionPool().closeTransaction(task.getProcesedMessage().getAttributeString(BillingMessage.BILL_TRANSACTION_NUM));
        return task;
    }
}
