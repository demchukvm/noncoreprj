-- SQL Loader Control and Data File created by TOAD
-- Variable length, terminated enclosed data formatting
-- 
-- The format for executing this file with SQL Loader is:
-- SQLLDR control=<filename> Be sure to substitute your
-- version of SQL LOADER and the filename for this file.
LOAD DATA
INFILE *
BADFILE 'D:\SVBILL\src\com\tmx\db\general\data\UI_USER_INTERFACE.BAD'
DISCARDFILE 'D:\SVBILL\src\com\tmx\db\general\data\UI_USER_INTERFACE.DSC'
APPEND INTO TABLE UI_USER_INTERFACE
Fields terminated by ";" Optionally enclosed by '"'
(
  UI_USERINTERFACEID NULLIF (UI_USERINTERFACEID="NULL"),
  UI_NAME,
  UI_PARENTID NULLIF (UI_PARENTID="NULL"),
  UIT_INTERFACETYPEID NULLIF (UIT_INTERFACETYPEID="NULL"),
  UI_VALID NULLIF (UI_VALID="NULL")
)
BEGINDATA
1;"main";NULL;2;1
2;"main.main";NULL;3;1
3;"main.main.monitoring";NULL;4;1
7;"main.main.settings";NULL;4;1
8;"main.main.system";NULL;4;1
9;"main.main.logout";NULL;4;1
201;"settings";NULL;2;1
202;"settings.settings";NULL;3;1
203;"settings.settings.personal";NULL;4;1
301;"dictionaries";NULL;2;1
302;"dictionaries.dictionaries";NULL;3;1
308;"dictionaries.dictionaries.users_dictionary";NULL;4;1
13;"balances.balances.operator_balances";NULL;4;1
310;"dictionaries.dictionaries.countries_type_dictionary";NULL;4;1
312;"dictionaries.dictionaries.cities_dictionary";NULL;4;1
10;"dictionaries.dictionaries.regions_type_dictionary";NULL;4;1
11;"dictionaries.dictionaries.processings_dictionary";NULL;4;1
12;"transactions.transactions.operator_transactions";NULL;4;1
14;"restrictions.restrictions.operator_restrictions";NULL;4;1
15;"tariffs.tariffs.operator_tariffs";NULL;4;1
16;"main.main.transactionInfoKS";NULL;4;1
309;"core.dictionary.terminal.manageTerminalsForm.onCreateButton";NULL;5;1
313;"core.dictionary.terminal.manageTerminalsForm.onEditButton";NULL;5;1
314;"core.dictionary.terminal.manageTerminalsForm.onDeleteButton";NULL;5;1
315;"tariffs.tariffs.seller_tariffs";NULL;4;1
316;"tariffs.tariffs.terminal_tariffs";NULL;4;1
317;"core.restriction.terminal_restriction.manageTerminalRestrictionsForm.onCreateButton";NULL;5;1
318;"core.restriction.terminal_restriction.manageTerminalRestrictionsForm.onEditButton";NULL;5;1
319;"core.restriction.terminal_restriction.manageTerminalRestrictionsForm.onDeleteButton";NULL;5;1
320;"main.main.dictionaries";NULL;4;1
321;"main.main.tools";NULL;4;1
