--This table is used to hold transactions registered on Mobipay Kyivstar side 
--and don't matched with our transactions on report reconcilation.
--Kyivstar billing-clerk sent us their unmatched transactions, we load them using sqlldr and 
--generate fixing report using select statement.

DROP TABLE svbill.mksu_mobipay_kyivstar_unrep;
CREATE TABLE svbill.mksu_mobipay_kyivstar_unrep(
   MKSU_PAY_ID		VARCHAR2(255),
   MKSU_MSISDN		VARCHAR2(255),
   MKSU_IP		VARCHAR2(255),
   MKSU_AMOUNT		VARCHAR2(255),
   MKSU_CURRENCY	VARCHAR2(255),
   MKSU_PAY_DATE	TIMESTAMP(6),
   MKSU_PAY_ACCOUNT	VARCHAR2(255),
   MKSU_PAY_VIRTUAL_EXP	VARCHAR2(255),
   MKSU_CARD_NUM	VARCHAR2(255),
   MKSU_CARD_TYPE	VARCHAR2(255),
   MKSU_CARD_EXP_DATE	TIMESTAMP(6),
   MKSU_CTS		TIMESTAMP(6),
   MKSU_CREATOR_UID	VARCHAR2(255),
   MKSU_RECEIPT_NUM	VARCHAR2(255),
   MKSU_PAY_SRC_ID	VARCHAR2(255),
   MKSU_PARTNER_ID	VARCHAR2(255),
   MKSU_BRANCH		VARCHAR2(255),
   MKSU_TRADE_POINT	VARCHAR2(255),
   MKSU_QUOTA		VARCHAR2(255),
   MKSU_BILLING		VARCHAR2(255)
);
COMMENT ON COLUMN svbill.mksu_mobipay_kyivstar_unrep.MKSU_PAY_ID IS 'Internal Mobipay id';


--Query to make fixing report against SVBILL and Mobipay transactions
--Usage: 
--1. clear table 'svbill.mksu_mobipay_kyivstar_unrep'
--2. load data received from Kyivstar into 'svbill.mksu_mobipay_kyivstar_unrep' using 'mksu_mobipay_kyivstar_unrep.ctl'
--3. setup this query: set and time period (single day) in 'where' clause. 
--4. save result in CSV format and upload to Mobipay FTP host with given file name. See gate.xml for FTP config.

SELECT 
'svcard', mks.mks_msisdn, mks.mks_payaccount, trim(TO_CHAR(mks.mks_amount, '999999.99')), mks.mks_payid, 
mks.mks_receiptnum, TO_CHAR(mks.mks_commitdate, 'DD.MM.YYYY HH24:MI:SS'),
mks.mks_sourcetype, mks.mks_bankpayid, mks.mks_branch, mks.mks_tradepoint, 
TO_CHAR(mks.mks_bankoperday, 'dd.mm.yyyy')  
FROM
MKS_MOBIPAY_KYIVSTAR mks, 
MKSU_MOBIPAY_KYIVSTAR_UNREP mksu
WHERE
NVL(mks.MKS_MSISDN, 0) = NVL(mksu.MKSU_MSISDN, 0) 
AND NVL(mks.MKS_PAYACCOUNT, 0) = NVL(mksu.MKSU_PAY_ACCOUNT, 0)
AND mks.MKS_TRADEPOINT = mksu.MKSU_TRADE_POINT
AND mks.MKS_RECEIPTNUM = mksu.MKSU_RECEIPT_NUM
AND mks.MKS_COMMITDATE BETWEEN TO_DATE('2008.02.22', 'yyyy.mm.dd') AND TO_DATE('2008.02.23', 'yyyy.mm.dd')
AND mksu.MKSU_CTS BETWEEN TO_DATE('2008.02.22', 'yyyy.mm.dd') AND TO_DATE('2008.02.23', 'yyyy.mm.dd')
ORDER BY mks.MKS_COMMITDATE ASC;
/