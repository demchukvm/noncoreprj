<?php /* Smarty version 2.6.26, created on 2013-02-22 16:26:02
         compiled from category.tpl.html */ ?>
<script type="text/javascript" src="<?php echo @URL_JS; ?>
/category.js"></script>
	
	<div class="catalog_sw">
	
		
		<div class="cat_ws_cnt">
		</div>
	</div>
	

		<?php if ($this->_tpl_vars['allow_products_search']): ?>
		<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "advanced_search_in_category.tpl.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
		<?php endif; ?>
	

		
				<?php echo $this->_tpl_vars['selected_category']['description']; ?>

		<?php if ($this->_tpl_vars['subcategories_to_be_shown']): ?>
			        
		<?php endif; ?>

<?php if ($this->_tpl_vars['products_to_show']): ?>

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "comparison_products_button.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>



	
<div id="prd_brief_list">
  <?php $_from = $this->_tpl_vars['products_to_show']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['product_brief'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['product_brief']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['product_item']):
        $this->_foreach['product_brief']['iteration']++;
?>
	<?php if (!(($this->_foreach['product_brief']['iteration']-1) % @CONF_COLUMNS_PER_PAGE)): ?><tr><?php endif; ?>
		<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "product_brief.html", 'smarty_include_vars' => array('product_info' => $this->_tpl_vars['product_item'])));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
	<?php if (!(( ($this->_foreach['product_brief']['iteration']-1)+1 ) % @CONF_COLUMNS_PER_PAGE)): ?></tr><?php elseif (($this->_foreach['product_brief']['iteration'] == $this->_foreach['product_brief']['total'])): ?></tr><?php endif; ?>
  <?php endforeach; endif; unset($_from); ?>
</div> 
 
<div class="pagination">
	<?php if ($this->_tpl_vars['catalog_navigator']): ?><p><?php echo $this->_tpl_vars['catalog_navigator']; ?>
</p><?php endif; ?>
</div>

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "comparison_products_button.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

<?php else: ?>
<p>
	<?php if ($this->_tpl_vars['search_with_change_category_ability'] && ! $this->_tpl_vars['advanced_search_in_category']): ?>
		&nbsp;
	<?php else: ?>
		<?php if ($this->_tpl_vars['advanced_search_in_category']): ?>
			&nbsp;&nbsp;&nbsp;&nbsp;< <?php echo 'Ничего не найдено'; ?>
 >
		<?php else: ?>
			&nbsp;&nbsp;&nbsp;&nbsp;< <?php echo 'Нет продуктов'; ?>
 >
		<?php endif; ?>
	<?php endif; ?>
</p>
<?php endif; ?>