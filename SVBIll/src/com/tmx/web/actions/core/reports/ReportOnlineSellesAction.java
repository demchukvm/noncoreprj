package com.tmx.web.actions.core.reports;

import com.tmx.web.base.BasicDispatchedAction;
import com.tmx.web.base.SecurityService;
import com.tmx.web.base.ApplicationEnvironment;
import com.tmx.web.forms.core.reports.ReportOnlineSellesForm;
import com.tmx.as.blogic.SellerManager;
import com.tmx.as.entities.general.user.User;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForm;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class ReportOnlineSellesAction extends BasicDispatchedAction {
    private static final String FORWARD = "page";
    public ActionForward defaultAction(ActionMapping mapping,
                                       ActionForm form,
                                       HttpServletRequest request,
                                       HttpServletResponse response,
                                       String methodName) throws Throwable {
        ReportOnlineSellesForm onlineSellesForm = (ReportOnlineSellesForm) form;
        onlineSellesForm.refresh(new SellerManager(), getLoggedInUser(request));
        return mapping.findForward(FORWARD);
    }

    private User getLoggedInUser(HttpServletRequest request) throws com.tmx.as.exceptions.SecurityException {
        SecurityService securityService = (SecurityService) ApplicationEnvironment.getService(SecurityService.class);
        return securityService.getLoggedInUser(request.getSession());
    }
}
