package com.tmx.beng.httpsgate.kyivstar;

import org.apache.log4j.Logger;

import java.net.ServerSocket;
import java.net.Socket;
import java.io.BufferedReader;
import java.io.PrintWriter;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

import com.tmx.beng.medaccess.MediumConnection;
import com.tmx.beng.medaccess.kyivstar.KyivstarExclusiveGateMediumAccess;
import com.tmx.beng.base.BillingMessageWritable;
import com.tmx.beng.base.BillingMessageImpl;
import com.tmx.beng.base.BillingMessage;

/**

 */
public class KyivstarGateTest extends Thread implements Runnable {
    private static Logger logger = Logger.getLogger("kyivstar_gate.TestConsole");

    public void run() {
        ServerSocket serverSocket = null;
        Socket socket = null;
        BufferedReader reader = null;
        PrintWriter writer = null;
        boolean doExit = false;

        try {
            //MediumConnection gateConn = new KyivstarGateMediumAccess().getConnection();
            MediumConnection gateConn = new KyivstarExclusiveGateMediumAccess().getConnection();
            serverSocket = new ServerSocket(62777);
            socket = serverSocket.accept();
            writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream()));
            writer.print("test>");
            writer.flush();
            reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            while (!doExit) {
                final String command = reader.readLine();
                writer.print("test>");
                writer.flush();
                if ("pay".equalsIgnoreCase(command)) {
                    BillingMessageWritable msg = new BillingMessageImpl();
                    msg.setOperationName(BillingMessage.O_REFILL_PAYMENT);
                    msg.setAttributeString(BillingMessage.BILL_TRANSACTION_NUM, String.valueOf(System.currentTimeMillis()));
                    msg.setAttributeString(BillingMessage.CLI_TRANSACTION_NUM, String.valueOf(System.currentTimeMillis()));
                    msg.setAttributeString(BillingMessage.ACCOUNT_NUMBER, "00380504452149");
                    msg.setAttributeString(BillingMessage.AMOUNT, "1.00");
                    msg.setAttributeString(BillingMessage.SERVICE_CODE, "KyivstarPoP");
                    msg.setAttributeString(BillingMessage.TERMINAL_SN, "555666777");
                    gateConn.send(msg);
                } else if ("getstatus".equalsIgnoreCase(command)) {
                    //Not implemented in gate
                    //BillingMessageWritable msg = new BillingMessageImpl();
                    //msg.setOperationName(BillingMessage.O_REFILL_PAYMENT_STATUS);
                    //msg.setAttributeString(BillingMessage.BILL_TRANSACTION_NUM, "1237451697305");
                    //gateConn.send(msg);
                } else if ("getlast".equalsIgnoreCase(command)) {
                    BillingMessageWritable msg = new BillingMessageImpl();
                    msg.setOperationName(BillingMessage.O_CANCEL_REFILL_PAYMENT);
                    msg.setAttributeString(BillingMessage.BILL_TRANSACTION_NUM, "1237451676976");
                    gateConn.send(msg);
                } else if ("exit".equalsIgnoreCase(command)) {
                    doExit = true;
                } else {
                    writer.println("Unknown command. Use: [auth, pay, voucher, getstatus, getlast, exit] ");
                    writer.flush();
                }
            }
            try {if (reader != null) reader.close();} catch (Exception ex) {}
            try {if (writer != null) writer.close();} catch (Exception ex) {}
            try {if (socket != null) socket.close();} catch (Exception ex) {}
            try {if (serverSocket != null) serverSocket.close();} catch (Exception ex) {}            
        }
        catch (Throwable e) {
            logger.error("Failed to execute test command", e);
            try {if (reader != null) reader.close();} catch (Exception ex) {}
            try {if (writer != null) writer.close();} catch (Exception ex) {}
            try {if (socket != null) socket.close();} catch (Exception ex) {}
            try {if (serverSocket != null) serverSocket.close();} catch (Exception ex) {}
        }

    }
}

