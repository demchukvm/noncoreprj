package com.tmx.beng.iso8583Gate.processors;

import com.tmx.util.StructurizedException;

import java.util.Locale;

/**

 */
public class ProcessingException extends StructurizedException {

    public ProcessingException(String msg, String[] params, Throwable e, Locale locale) {
        super(msg, params, e, locale);
    }

    public ProcessingException(String msg, String[] params) {
        super(msg, params);
    }

    public ProcessingException(String msg, String[] params, Locale locale) {
        super(msg, params, locale);
    }

    public ProcessingException(String msg, Throwable e, Locale locale) {
        super(msg, e, locale);
    }

    public ProcessingException(String msg, Throwable e) {
        super(msg, e);
    }

    public ProcessingException(String msg, Locale locale) {
        super(msg, locale);
    }

    public ProcessingException(String msg) {
        super(msg);
    }

    public ProcessingException(String msg, String[] params, Throwable e) {
        super(msg, params, e);
    }
}

