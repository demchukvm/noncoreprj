<%@ page language="java" pageEncoding="UTF-8" contentType="text/html; charset=utf-8" isELIgnored="false" %>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-nested" prefix="nested" %>
<%@ taglib uri="http://tmx.com/taglib/tmx-controls" prefix="tmx_ctrl" %>
<%@ taglib uri="http://tmx.com/taglib/tmx-logic" prefix="tmx_logic" %>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html" %>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean" %>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>

<html:form action="/core/balance/terminal_balance/browseTerminalsAction">
<tmx_ctrl:browseCallBack/>
<table width="100%" border="0">
    
    <!-- FILTER SET -->
    <tr>
        <td valign="top" width="100%" style="text-align: left">
            <tmx_ctrl:panelFilterSet  tableName="browseTable">
                <table>
                    <tr>

                        <td class="dataLabel">
                            <tmx_ctrl:writeEntityLabel
                                    attrName="{@name}"
                                    entityClassName="com.tmx.as.entities.bill.seller.Seller"/>:
                        </td>
                        <td class="dataField">
                            <tmx_ctrl:selectBox
                                    property="browseTable.filterSet.filter(by_seller_id).parameter(seller_id).control"/>
                        </td>

                        <td class="dataLabel">
                              <tmx_ctrl:writeEntityLabel
                                    attrName="serialNumber"
                                    entityClassName="com.tmx.as.entities.bill.terminal.Terminal"/>:
                        </td>
                        <td class="dataField">
                              <tmx_ctrl:editBox
                                    property="editBoxSerialNumber"/>
                        </td>
                    </tr>
                </table>
            </tmx_ctrl:panelFilterSet>
        </td>
    </tr>
    <!-- TABLE -->
    <tr>
        <td width="100%">
            <table class='one_outer_cell_table' cellspacing='0' cellpadding='0' width="100%">
                <tr>
                    <td>
                        <table cellspacing='1' cellpadding='1' width="100%">
                            <!-- Table header -->
                            <tr>

                                <th class="column_header_cell" width="110">
                                    <tmx_ctrl:imgTableOrdering tableName="browseTable"
                                                               orderByFieldName="serialNumber"
                                                               labelBundle="as:com.tmx.as.entities.bill.terminal.Terminal"
                                                               labelKey="serialNumber"/>
                                </th>

                                <th class="column_header_cell" width="110">
                                    <tmx_ctrl:imgTableOrdering tableName="browseTable"
                                                               orderByFieldName="hardwareSerialNumber"
                                                               labelBundle="as:com.tmx.as.entities.bill.terminal.Terminal"
                                                               labelKey="hardwareSerialNumber"/>
                                </th>

                                <th class="column_header_cell" width="110">
                                    <tmx_ctrl:imgTableOrdering tableName="browseTable"
                                                               orderByFieldName="registrationDate"
                                                               labelBundle="as:com.tmx.as.entities.bill.terminal.Terminal"
                                                               labelKey="registrationDate"/>
                                </th>

                                <th class="column_header_cell" width="110">
                                    <tmx_ctrl:imgTableOrdering tableName="browseTable"
                                                               orderByFieldName="blocked"
                                                               labelBundle="as:com.tmx.as.entities.bill.terminal.Terminal"
                                                               labelKey="blocked"/>
                                </th>

                                <th class="column_header_cell" width="110">
                                    <tmx_ctrl:imgTableOrdering tableName="browseTable"
                                                               orderByFieldName="allowRegistrationUpdate"
                                                               labelBundle="as:com.tmx.as.entities.bill.terminal.Terminal"
                                                               labelKey="allowRegistrationUpdate"/>
                                </th>

                                <th class="column_header_cell" width="110">
                                    <tmx_ctrl:imgTableOrdering tableName="browseTable"
                                                               orderByFieldName="sellerOwner.name"
                                                               labelBundle="as:com.tmx.as.entities.bill.terminal.Terminal"
                                                               labelKey="sellerOwner"/>
                                </th>

                            </tr>
                            <nested:iterate id="terminal" property="browseTable.data" type="com.tmx.as.entities.bill.terminal.Terminal">
                                <tr>
                                    <td class='column_data_cell'>
                                        <tmx_ctrl:browsePick pickKey="${terminal.terminalId}" pickValue="${terminal.serialNumber}"/>
                                    </td>
                                    <td class='column_data_cell'>
                                        <c:out value="${terminal.hardwareSerialNumber}" />
                                    </td>
                                    <td class='column_data_cell'>
                                        <fmt:formatDate value="${terminal.registrationDate}" pattern="yyyy/MM/dd HH:mm:ss"/>
                                    </td>
                                    <td class='column_data_cell'>
                                        <%--<c:out value="${terminal.blocked}" />--%>
                                        <tmx_ctrl:checkSign name="terminal" property="blocked"/>
                                    </td>
                                    <td class='column_data_cell'>
                                        <tmx_ctrl:checkSign name="terminal" property="allowRegistrationUpdate"/>
                                    </td>
                                    <td class='column_data_cell'>
                                        <c:if test="${terminal.sellerOwner!= null}">
                                            <c:out value="${terminal.sellerOwner.name}" />
                                        </c:if>
                                    </td>
                                </tr>
                            </nested:iterate>
                            <!-- Empty row -->
                            <nested:define id="tableSize" property="browseTable.resultSize" type="java.lang.Integer"/>
                            <c:if test="${tableSize <= 0}">
                                <tr>
                                    <td class='column_data_cell' colspan="6" align="center">
                                        <bean:message bundle="core.labels" key="core.common.th.empty_table" />
                                    </td>
                                </tr>
                            </c:if>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td style="text-align: right">
            <tmx_ctrl:imgTablePaging maxBookmarkCount="10" tableName="browseTable"/>
        </td>
    </tr>

</table>
</html:form>