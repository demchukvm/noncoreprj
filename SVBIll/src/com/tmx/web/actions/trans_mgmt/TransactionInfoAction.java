package com.tmx.web.actions.trans_mgmt;

import com.tmx.as.modules.SpringModule;
import com.tmx.web.base.BasicDispatchedAction;
import com.tmx.web.forms.trans_mgmt.TransactionInfoForm;
import com.tmx.beng.access.Access;
import com.tmx.beng.access.Connection;
import com.tmx.beng.base.BillingMessage;
import com.tmx.beng.base.BillingMessageImpl;
import com.tmx.beng.httpsgate.kyivstar.*;
import com.tmx.beng.httpsgate.HttpsRequestException;
import com.tmx.beng.medaccess.GateException;
import com.tmx.util.InitException;
import com.tmx.as.base.EntityManager;
import com.tmx.as.base.FilterWrapper;
import com.tmx.as.base.criterion.CriterionWrapper;
import com.tmx.as.base.criterion.Restrictions;
import com.tmx.as.entities.bill.transaction.ClientTransaction;
import com.tmx.as.entities.mobipay.kyivstar.MobipayKyivstar;
import com.tmx.as.exceptions.DatabaseException;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForm;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.ArrayList;

/**
 * Exapmle action to show how to use tmx controls
 */
public class TransactionInfoAction extends BasicDispatchedAction {

    public ActionForward cancelTransaction(ActionMapping mapping,
                                           ActionForm form,
                                           HttpServletRequest request,
                                           HttpServletResponse response)  throws Throwable {

        TransactionInfoForm transactionInfoForm = (TransactionInfoForm)form;
        BillingMessageImpl msg = new BillingMessageImpl();
        boolean skip = false;
        if(transactionInfoForm.getBillTransaction() != null && transactionInfoForm.getBillTransaction().getValue().length() > 0){
            msg.setAttributeString(BillingMessage.BILL_TRANSACTION_NUM, transactionInfoForm.getBillTransaction().getValue());
        }else if(transactionInfoForm.getCliTransaction() != null && transactionInfoForm.getCliTransaction().getValue().length() > 0){
            msg.setAttributeString(BillingMessage.CLI_TRANSACTION_NUM, transactionInfoForm.getCliTransaction().getValue());
        }else{
            skip = true;
        }

        if(!skip){
            msg.setOperationName(BillingMessage.O_CANCEL_REFILL_PAYMENT);
            msg.setAttributeString(BillingMessage.SERVICE_CODE, transactionInfoForm.getOperatorCode().getValue());

//            Access access = obtainBillingEngineAccess(transactionInfoForm);
            Connection conn = (Connection) SpringModule.getApplicationContext().getBean("billingConnection");
            BillingMessage resp = conn.processSync(msg);

            transactionInfoForm.clear();
            transactionInfoForm.getStatusCode().setValue(resp.getAttributeString(BillingMessage.STATUS_CODE));
            transactionInfoForm.getStatusMessage().setValue(resp.getAttributeString(BillingMessage.STATUS_MESSAGE));
        }else{
            transactionInfoForm.getStatusMessage().setValue("Transaction code expected");
        }
        return mapping.findForward("transactionInfo");
    }

    public ActionForward info(ActionMapping mapping,
                              ActionForm form,
                              HttpServletRequest request,
                              HttpServletResponse response)  throws Throwable {
        TransactionInfoForm transactionInfoForm = (TransactionInfoForm)form;
        retriveLocalBillingInfo(transactionInfoForm);

        return mapping.findForward("transactionInfo");
    }

    public ActionForward infoMobipay(ActionMapping mapping,
                              ActionForm form,
                              HttpServletRequest request,
                              HttpServletResponse response)  throws Throwable {
        TransactionInfoForm transactionInfoForm = (TransactionInfoForm)form;

        String billTransactionNum = transactionInfoForm.getBillTransaction().getValue();

        if (!transactionInfoForm.getPayId().getValue().equals("")) {
            String gateName = transactionInfoForm.getScrollBoxKyivstarGates().getSelectedKey();
            BasicKyivstarGate kyivstarGate = BasicKyivstarGate.getInstance(gateName);
            if (kyivstarGate != null) {
                KyivstarResponse kyivstarResponse = kyivstarGate.getPayResponse(new Long(transactionInfoForm.getPayId().getValue()), billTransactionNum);
                fillControlsOnWeb(transactionInfoForm, kyivstarResponse);
            } else {
                transactionInfoForm.getStatusCodeMobipay().setValue("Can`t get instance for " + gateName);
            }
            return mapping.findForward("transactionInfo");
        }

        ClientTransaction clientTransaction = retriveLocalBillingInfo(transactionInfoForm);

        if (clientTransaction == null) {
            transactionInfoForm.getStatusCodeMobipay().setValue("Client transaction is null");
            return mapping.findForward("transactionInfo");
        }
        CriterionWrapper [] criterions = new CriterionWrapper[] {
                Restrictions.eq("bankPayId", clientTransaction.getTransactionGUID())};

        List mobipayTxs = (new EntityManager()).RETRIEVE_ALL(MobipayKyivstar.class, criterions, null);
        MobipayKyivstar mobipayTx;
        if (mobipayTxs.size() <= 0) {
            transactionInfoForm.getStatusCodeMobipay().setValue("Mobipay transaction not found");
            return mapping.findForward("transactionInfo");
        } else {
            if (mobipayTxs.get(0) == null) {
                transactionInfoForm.getStatusCodeMobipay().setValue("Mobipay transaction not found");
            }            
        }
        mobipayTx = (MobipayKyivstar)mobipayTxs.get(0);
        if (mobipayTx.getGateName() == null || mobipayTx.getGateName().equals("")) {
            transactionInfoForm.getStatusCodeMobipay().setValue("Mobipay transaction not found");
        } else  {
            BasicKyivstarGate kyivstarGate = BasicKyivstarGate.getInstance(mobipayTx.getGateName());
            if (mobipayTx.getPayId() != null) {
                transactionInfoForm.getPayId().setValue(mobipayTx.getPayId().toString());
                try {
                    KyivstarResponse kyivstarResponse = kyivstarGate.getPayResponse(mobipayTx, billTransactionNum);
                    if (kyivstarResponse != null) {
                        fillControlsOnWeb(transactionInfoForm, kyivstarResponse);
                    } else {
                        transactionInfoForm.getStatusCodeMobipay().setValue("pay response is null");
                    }
                } catch (HttpsRequestException e) {
                    transactionInfoForm.getStatusCodeMobipay().setValue(e.getMessage());
                    transactionInfoForm.getPayStatusMobipay().setValue(e.getTraceDump());
                } catch (GateException e) {
                    transactionInfoForm.getStatusCodeMobipay().setValue(e.getLocalizedMessage());
                }
            }

        }


        return mapping.findForward("transactionInfo");
    }

    private void fillControlsOnWeb(TransactionInfoForm transactionInfoForm, KyivstarResponse kyivstarResponse) {
        if (kyivstarResponse.getStatusCode() != null) {
            transactionInfoForm.getStatusCodeMobipay().setValue(kyivstarResponse.getStatusCode().toString());
        }
        if (kyivstarResponse.getPayStatus() != null) {
            transactionInfoForm.getPayStatusMobipay().setValue(kyivstarResponse.getPayStatus().toString());
        }
        if (kyivstarResponse.getReceipt() != null) {
            transactionInfoForm.getReceiptNumMobipay().setValue(kyivstarResponse.getReceipt());
        }
        if (kyivstarResponse.getCts() != null) {
            transactionInfoForm.getCtsMobipay().setValue(
                    new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(
                            kyivstarResponse.getTimeStamp()));
        }
    }

    /*Retrives client transaction stored in local DB, fills needed fields on form
       @return Client transaction
    */
    private ClientTransaction retriveLocalBillingInfo(TransactionInfoForm transactionInfoForm) throws DatabaseException {
        transactionInfoForm.clear();
        ClientTransaction clientTransaction = null;
        EntityManager entityManager = new EntityManager();

        if(transactionInfoForm.getBillTransaction() != null && transactionInfoForm.getBillTransaction().getValue().length() > 0){
            FilterWrapper byBillNumFW = new FilterWrapper("by_billnum");
            byBillNumFW.setParameter("billnum", transactionInfoForm.getBillTransaction().getValue());
            clientTransaction = (ClientTransaction)entityManager.RETRIEVE(
                    ClientTransaction.class,
                    new FilterWrapper[]{byBillNumFW},
                    new String[]{ "operator", "terminal", "seller" }
            );
        }else if(transactionInfoForm.getCliTransaction() != null && transactionInfoForm.getCliTransaction().getValue().length() > 0){
            FilterWrapper byCliNumFW = new FilterWrapper("by_clinum");
            byCliNumFW.setParameter("clinum", transactionInfoForm.getCliTransaction().getValue());
            clientTransaction = (ClientTransaction)entityManager.RETRIEVE(
                    ClientTransaction.class,
                    new FilterWrapper[]{byCliNumFW},
                    new String[]{ "operator", "terminal", "seller" }
            );
        }
        if(clientTransaction != null){
            transactionInfoForm.getBillTransaction().setValue(clientTransaction.getTransactionGUID());
            transactionInfoForm.getCliTransaction().setValue(clientTransaction.getCliTransactionNum());
            transactionInfoForm.getAmount().setValue(clientTransaction.getAmount().toString());
            transactionInfoForm.getStatusCode().setValue(clientTransaction.getStatus());
            if(clientTransaction.getTransactionTime() != null){
                transactionInfoForm.getTransactionTime().setValue(
                    new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(clientTransaction.getTransactionTime()));
            }
            if(clientTransaction.getClientTime() != null){
                transactionInfoForm.getClientTime().setValue(
                    new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(clientTransaction.getClientTime()));
            }
            if(clientTransaction.getAccountNumber() != null){
                transactionInfoForm.getAccountNumber().setValue(clientTransaction.getAccountNumber());
            }
            transactionInfoForm.getSellerName().setValue(clientTransaction.getSeller().getName());
            transactionInfoForm.getTerminalNumber().setValue(clientTransaction.getTerminal().getSerialNumber());
            transactionInfoForm.getOperatorCode().setValue(clientTransaction.getOperator().getCode());
        }else{
            transactionInfoForm.getStatusMessage().setValue("Transaction not found");
        }

        return clientTransaction;
    }

    public ActionForward refreshBalance(ActionMapping mapping,
                              ActionForm form,
                              HttpServletRequest request,
                              HttpServletResponse response)  throws Throwable {

        TransactionInfoForm transactionInfoForm = (TransactionInfoForm)form;
        try{
            transactionInfoForm.getKyivstarBalance().setValue(KyivstarGate.getInstance().getBalance("KyivstarPoP").toString());
        }catch(Throwable e){
            transactionInfoForm.getKyivstarBalance().setValue(e.getLocalizedMessage());
        }

        try{
            transactionInfoForm.getKyivstarWCBalance().setValue(KyivstarBonusNoCommissionGate.getInstance().getBalance("KyivstarPoP").toString());
        }catch(Throwable e){
            transactionInfoForm.getKyivstarWCBalance().setValue(e.getLocalizedMessage());
        }

        try{
            transactionInfoForm.getKyivstarFixConnectBalance().setValue(KyivstarExclusiveGate.getInstance().getBalance("KyivstarPoP").toString());
        }catch(Throwable e){
            transactionInfoForm.getKyivstarFixConnectBalance().setValue(e.getLocalizedMessage());
        }

        return mapping.findForward("transactionInfo");
    }

    private Access obtainBillingEngineAccess(TransactionInfoForm transactionInfoForm) throws InitException {
        if(transactionInfoForm.getBillingEngineAccess() == null){
            InitialContext ctx = null;
            try{
                System.setProperty("java.naming.factory.url.pkgs", ""); //=org.apache.naming
                ctx = new InitialContext();//System.getProperties()
                Access newAccess = (Access)ctx.lookup("java:comp/services/billingEngine");
                if(newAccess == null)
                    throw new InitException("err.csapi.null_basic_access_obtained");

                transactionInfoForm.setBillingEngineAccess(newAccess);
            }
            catch(NamingException e){
                throw new InitException("err.csapi.failed_to_obtain_billing_engine_access", e);
            }
            finally{
                if(ctx != null){
                    try{
                        ctx.close();
                    }
                    catch(NamingException e) {
                        //log.info( "Unable to release initial context", ne );
                    }
                }
            }
        }
        return transactionInfoForm.getBillingEngineAccess();
    }

    protected ActionForward defaultAction(ActionMapping mapping,
                                          ActionForm form,
                                          HttpServletRequest request,
                                          HttpServletResponse response,
                                          String methodName) throws Throwable {
        TransactionInfoForm transactionInfoForm = (TransactionInfoForm)form;

        List<String> kyivstarGates = new ArrayList<String>(5);
        kyivstarGates.add(KyivstarGate.GATE_NAME);
        kyivstarGates.add(KyivstarBonusNoCommissionGate.GATE_NAME);
        kyivstarGates.add(KyivstarBonusCommissionGate.GATE_NAME);
        kyivstarGates.add(KyivstarExclusiveGate.GATE_NAME);
        transactionInfoForm.getScrollBoxKyivstarGates().bind(kyivstarGates);


        return mapping.findForward("transactionInfo");
//        return refreshBalance(mapping, form, request, response);
    }
}
