package com.tmx.beng.base.cache;

import com.tmx.as.base.EntityManager;
import com.tmx.as.base.FilterWrapper;
import com.tmx.as.entities.bill.processing.Processing;
import com.tmx.as.entities.bill.restriction.ProcessingRestriction;
import com.tmx.as.exceptions.DatabaseException;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**

 */
class Processings {
    Map processingId_processing = new HashMap();
    Map processingCode_processing = new HashMap();

    Processings() throws DatabaseException {
        EntityManager entityManager = new EntityManager();

        // load processings
        Iterator it;
        ProcessingWrapper processingWrapper;
        List list = entityManager.RETRIEVE_ALL(Processing.class, null);
        for(it = list.iterator(); it.hasNext();){
            Processing processing = (Processing)it.next();
            processingWrapper = new ProcessingWrapper(processing);
            processingId_processing.put(processing.getProcessingId(), processingWrapper);
            processingCode_processing.put(processing.getCode(), processingWrapper);
        }

        // load processings restriction
        list  = entityManager.RETRIEVE_ALL(ProcessingRestriction.class, (FilterWrapper[])null, new String[]{"functionInstance"});
        for(it = list.iterator(); it.hasNext();){
            ProcessingRestriction processingRestriction = (ProcessingRestriction)it.next();
            processingWrapper = (ProcessingWrapper)processingId_processing.get(processingRestriction.getProcessingId());
            if(processingWrapper != null){
                processingWrapper.restriction.add(processingRestriction);
            }
        }
    }

    public Processing getProcessing(Long id) {
        if(id == null)
            return null;
        ProcessingWrapper wrapper = (ProcessingWrapper)processingId_processing.get(id);
        if(wrapper == null)
            return null;
        return wrapper.getProcessing();
    }

    public ProcessingWrapper getProcessingWrapper(String code){
        return (ProcessingWrapper)processingCode_processing.get(code);
    }

    public Processing getProcessing(String code) {
        ProcessingWrapper wrapper = getProcessingWrapper(code);
        if(wrapper == null) return null;
        return wrapper.getProcessing();
    }
}
