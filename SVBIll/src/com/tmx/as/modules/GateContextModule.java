package com.tmx.as.modules;

import com.tmx.util.InitException;
import com.tmx.gate.datastream.base.GateContext;

import java.util.Properties;

/**

 */
public class GateContextModule extends AbstractModule{

    public void start(Properties props)throws ModuleException{
        try{
            GateContext.getInstance().reload();
        }catch(InitException e){
            throw new ModuleException(e.getLocalizedMessage());
        }
    }

    public void shutdownGraceful(Properties props)throws ModuleException{
    }

    public void shutdownAbort(Properties props)throws ModuleException{
    }
}
