package com.tmx.beng.medaccess.umc;

import com.tmx.beng.httpsgate.HttpsGateException;
import com.tmx.beng.httpsgate.umc.UmcGate;
import com.tmx.beng.base.BillingMessage;
import com.tmx.beng.medaccess.MediumConnection;

/**
  Medium connection to work with UMC gate
 */
public class UmcGateMediumConnection implements MediumConnection {
    private UmcGate umcGate = null;

    UmcGateMediumConnection() throws HttpsGateException {
        umcGate = UmcGate.getInstance();
    }

    public void send(BillingMessage msg) {
        umcGate.enqueueBillMessage(msg);
    }
}
