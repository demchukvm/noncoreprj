package com.tmx.as.entities.bill.gates;

import com.tmx.as.base.BasicEntity;
import java.io.Serializable;
import java.util.Date;

public class ProductGates extends BasicEntity {
    private Long pgId;
    private String productName;
    private String gateName;

    public Long getPgId() {
        return pgId;
    }

    public void setPgId(Long pgId) {
        this.pgId = pgId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getGateName() {
        return gateName;
    }

    public void setGateName(String gateName) {
        this.gateName = gateName;
    }

    public Serializable getId() {
        return getPgId();
    }

    public int getValid() {
        return valid;
    }

    public void setValid(int valid) {
        this.valid = valid;    
    }
}

