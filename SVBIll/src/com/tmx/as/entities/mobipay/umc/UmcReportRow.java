package com.tmx.as.entities.mobipay.umc;

import com.tmx.as.base.BasicEntity;

import java.util.Date;
import java.io.Serializable;

/**

 */
public class UmcReportRow extends BasicEntity {
    private Long umcReportRowId;
    private String dillerTransactionId;
    private String operatorTransactionId;
    private Integer result;
    private String errorCode;
    private Date transactionTime;
    private Double transactionAmount;
    private String msisdn;
    private UmcReport report;

    public Long getUmcReportRowId() {
        return umcReportRowId;
    }

    public void setUmcReportRowId(Long umcReportRowId) {
        this.umcReportRowId = umcReportRowId;
    }

    public String getDillerTransactionId() {
        return dillerTransactionId;
    }

    public void setDillerTransactionId(String dillerTransactionId) {
        this.dillerTransactionId = dillerTransactionId;
    }

    public String getOperatorTransactionId() {
        return operatorTransactionId;
    }

    public void setOperatorTransactionId(String operatorTransactionId) {
        this.operatorTransactionId = operatorTransactionId;
    }

    public Integer getResult() {
        return result;
    }

    public void setResult(Integer result) {
        this.result = result;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public Date getTransactionTime() {
        return transactionTime;
    }

    public void setTransactionTime(Date transactionTime) {
        this.transactionTime = transactionTime;
    }

    public Double getTransactionAmount() {
        return transactionAmount;
    }

    public void setTransactionAmount(Double transactionAmount) {
        this.transactionAmount = transactionAmount;
    }

    public String getMsisdn() {
        return msisdn;
    }

    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }

    public UmcReport getReport() {
        return report;
    }

    public void setReport(UmcReport report) {
        this.report = report;
    }

    public Serializable getId() {
        return getUmcReportRowId();
    }

    public int getValid() {
        return valid;
    }

    public void setValid(int valid) {
        this.valid = valid;
    }
}
