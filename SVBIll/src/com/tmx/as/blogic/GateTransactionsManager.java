package com.tmx.as.blogic;

import com.tmx.as.base.*;
import com.tmx.as.base.criterion.CriterionWrapper;
import com.tmx.as.base.criterion.Restrictions;
import com.tmx.as.entities.bill.operator.Operator;
import com.tmx.as.entities.bill.transaction.*;
import com.tmx.as.entities.bill.seller.Seller;
import com.tmx.as.entities.bill.balance.SellerBalance;
import com.tmx.as.entities.mobipay.kyivstar.MobipayKyivstar;
import com.tmx.as.entities.mobipay.dacard.DacardGateTransaction;
import com.tmx.as.exceptions.DatabaseException;
import com.tmx.web.forms.core.balance.seller_balance.InfoSellerBalanceForm;
import com.tmx.beng.httpsgate.kyivstar.BasicKyivstarGate;
import com.tmx.beng.httpsgate.kyivstar.KyivstarGate;
import com.tmx.beng.httpsgate.kyivstar.KyivstarBonusNoCommissionGate;
import com.tmx.beng.httpsgate.HttpsGateException;
import com.tmx.beng.httpsgate.HttpsRequestException;
import com.tmx.beng.medaccess.GateException;

import java.util.*;


public class GateTransactionsManager extends EntityManager {
    public HashMap getTransactionByGUID(String[] transactionGUIDs) throws DatabaseException {
        HashMap<Class, List> gateTx = new HashMap();
        String  transactionString;

        CriterionWrapper criterion[] = new CriterionWrapper[] {
                Restrictions.in("bankPayId", transactionGUIDs)
        };
        List<MobipayKyivstar> kyivstarTxs = RETRIEVE_ALL(MobipayKyivstar.class, criterion, null);
        KyivstarGate kyivstarGate = null;
        try {
            kyivstarGate = KyivstarGate.getInstance();
        } catch (HttpsGateException e) {
            e.printStackTrace();
            logger.error(e);
        }
        KyivstarBonusNoCommissionGate kyivstarBNCGate = null;
        try {
            kyivstarBNCGate = KyivstarBonusNoCommissionGate.getInstance();
        } catch (HttpsGateException e) {
            e.printStackTrace();
            logger.error(e);
        }

        for (MobipayKyivstar mksTx : kyivstarTxs) {
            if (mksTx.getGateName().equals(kyivstarGate.getGateName())) {
                try {
                    mksTx.setStatusCode(kyivstarGate.getTxStatus(mksTx));
                } catch (GateException e) {
                    logger.error(e);
                    mksTx.setStatusCode(22);
                    mksTx.setStatusMessage(e.getMessage());
                }
            } else if (mksTx.getGateName().equals(kyivstarBNCGate.getGateName())) {
                try {
                    mksTx.setStatusCode(kyivstarGate.getTxStatus(mksTx));
                } catch (GateException e) {
                    logger.error(e);
                    mksTx.setStatusCode(22);
                    mksTx.setStatusMessage(e.getMessage());
                }
            }  else {
                mksTx.setStatusCode(22);
                mksTx.setStatusMessage("???cant_determine_Kyivstar_gate???");
            }
        }

        gateTx.put(MobipayKyivstar.class, kyivstarTxs);

        criterion = new CriterionWrapper[] {
                Restrictions.in("billTransactionNum", transactionGUIDs)
        };
        List dacardTxs = RETRIEVE_ALL(DacardGateTransaction.class, criterion, null);
        gateTx.put(DacardGateTransaction.class, dacardTxs);

        

        return gateTx;
    }

}