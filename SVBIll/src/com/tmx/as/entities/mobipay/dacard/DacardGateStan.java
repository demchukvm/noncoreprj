package com.tmx.as.entities.mobipay.dacard;

import com.tmx.as.base.BasicEntity;

import java.util.Date;
import java.io.Serializable;

/**
 * Entity to track STAN numbers.
 * These numbers are reseted every day to 1 for each TerminalId.
 */
public class DacardGateStan extends BasicEntity {
    private Long dacardGateStanId;
    private String dacardTerminalId;
    private Date day;
    private Integer stan;
    private Date allocationTime;

    public Long getDacardGateStanId() {
        return dacardGateStanId;
    }

    public void setDacardGateStanId(Long dacardGateStanId) {
        this.dacardGateStanId = dacardGateStanId;
    }

    public String getDacardTerminalId() {
        return dacardTerminalId;
    }

    public void setDacardTerminalId(String dacardTerminalId) {
        this.dacardTerminalId = dacardTerminalId;
    }

    public Date getDay() {
        return day;
    }

    public void setDay(Date day) {
        this.day = day;
    }

    public Integer getStan() {
        return stan;
    }

    public void setStan(Integer stan) {
        this.stan = stan;
    }

    public Date getAllocationTime() {
        return allocationTime;
    }

    public void setAllocationTime(Date allocationTime) {
        this.allocationTime = allocationTime;
    }

    public Serializable getId() {
        return getDacardGateStanId();
    }

    public int getValid() {
        return valid;
    }

    public void setValid(int valid) {
        this.valid = valid;
    }
}
