package com.tmx.engines.xmlengine;

import com.tmx.as.base.Entity;
import com.tmx.as.base.HibernateSessionFactoryProvider;
import com.tmx.as.base.EntityResources;
import com.tmx.util.XMLUtil;
import com.tmx.util.StringUtil;
import com.tmx.util.StructurizedException;
import com.tmx.util.Configuration;
import org.w3c.dom.*;
import org.hibernate.metadata.ClassMetadata;
import org.hibernate.Hibernate;
import org.apache.log4j.Logger;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.util.*;
import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.Method;


/**
 * This publisher is unified and could publish ANY entity and set of entities
 * */
public class XMLPublisherUnified implements XMLPublisher {
    private DocumentHeader docHeader = null;
    //Lexemes dictionary
    private String T_DOCUMENT = "document";
    private String T_HEADER = "header";
    private String A_SYSTEM = "system";
    private String A_NAME = "name";
    private String A_VERSION = "version";
    private String T_PAGE = "page";
    private String T_SERVER = "server";
    private String A_URL = "url";
    private String T_BODY = "body";
    private String T_ENTITY = "entity";
    private String T_SET = "set";
    private String A_LABEL = "label";
    private String A_TYPE = "type";
    private String T_ID = "id";
    private String T_PROPERTY = "property";
    private String A_SELECTED_ID = "selectedId";
    private String T_ERROR = "error";
    private String T_ERROR_STACK_TRACE = "stackTrace";
    private String T_ERROR_CAUSE = "cause";
    private String T_LOCALE = "locale";
    private String A_VALUE = "value";
    private String T_LINK = "link";

    /** Map of pairs: [String path to current entity][Set of prototype holders] */
    private Map nodeprototypes = new HashMap();

    private Logger logger = Logger.getLogger("xmlengine.XMLPublisherUnified");

    /** returns body node */
    public Node initializeDocument(DocumentHeader header) throws XMLPublishingException {
        docHeader = header;
        Node bodyNode = null;
        try {
            Document ownerDoc = DocumentBuilderFactory.newInstance().newDocumentBuilder().newDocument();
            Node rootNode = ownerDoc.createElement(T_DOCUMENT);
            ownerDoc.appendChild(rootNode);

            //Append HEADER
            Element headerElement = ownerDoc.createElement(T_HEADER);
            rootNode.appendChild(headerElement);

            Element systemElement = ownerDoc.createElement(A_SYSTEM);
            systemElement.setAttribute(A_NAME, Configuration.getInstance().getProperty("system.name", "unknown"));
            systemElement.setAttribute(A_VERSION, Configuration.getInstance().getProperty("system.version", "unknown"));
            headerElement.appendChild(systemElement);

            Element pageElement = ownerDoc.createElement(T_PAGE);
            pageElement.setAttribute(A_NAME, header.getPageName());
            headerElement.appendChild(pageElement);

            Element serverURLElement = ownerDoc.createElement(T_SERVER);
            serverURLElement.setAttribute(A_URL, header.getFullAppUrl());
            //pageElement.setAttribute("appContextName", header.getAppContextName());
            headerElement.appendChild(serverURLElement);

            Element localeElement = ownerDoc.createElement(T_LOCALE);
            localeElement.setAttribute(A_VALUE, header.getLocale().toString());
            headerElement.appendChild(localeElement);

            //prev, current and next links
            Element linkElement = ownerDoc.createElement(T_LINK);
            linkElement.setAttribute(A_NAME, "history.next");
            linkElement.setAttribute(A_URL, header.getNextPage());
            headerElement.appendChild(linkElement);

            linkElement = ownerDoc.createElement(T_LINK);
            linkElement.setAttribute(A_NAME, "history.prev");
            linkElement.setAttribute(A_URL, header.getPrevPage());
            headerElement.appendChild(linkElement);

            linkElement = ownerDoc.createElement(T_LINK);
            linkElement.setAttribute(A_NAME, "history.current");
            linkElement.setAttribute(A_URL, header.getCurrentPage());
            headerElement.appendChild(linkElement);

            //Append BODY
            bodyNode = ownerDoc.createElement(T_BODY);
            rootNode.appendChild(bodyNode);

        } catch (ParserConfigurationException e) {
            e.printStackTrace();
            throw new XMLPublishingException("err.xml_publisher.failed_to_initialize_xml_doc", e);
        }
        return bodyNode;
    }

    public Node publishEntity(Entity entity, Node parentNode, String entityName, String[] filters) throws XMLPublishingException {
        return publishEntity0(entity, parentNode, null, entityName, filters);
    }

    public Node publishEntityList(List entityList, Node parentNode, String setName, Long selectedId, String[] filters) throws XMLPublishingException {
        return publishEntityList0(entityList, parentNode, null, setName, null, selectedId, filters);
    }

    public String serializeDocument(Node anyNode) throws XMLPublishingException {
        Document document = anyNode.getOwnerDocument();
        String docString = null;
        try {
            docString = XMLUtil.serializeDOM(document);
        }
        catch (IOException e) {
            //RM put into log
            e.printStackTrace();
            throw new XMLPublishingException("err.xml_publisher.xml_doc_serialization_failed", e);
        }
        return docString;
    }

    public Node publishError(StructurizedException error, Node parentNode) throws XMLPublishingException{
        Element errorElement = null;
        try {
            Document ownerDoc = parentNode.getOwnerDocument();
            errorElement = ownerDoc.createElement(T_ERROR);
            errorElement.setAttribute(A_TYPE, error.getClass().getName());
            CDATASection cdata = ownerDoc.createCDATASection(error.getLocalizedMessage());
            errorElement.appendChild(cdata);
            parentNode.appendChild(errorElement);

            //append causes
            Iterator causesIter = error.getCauses().iterator();
            Element causeElement = errorElement;
            while(causesIter.hasNext()){
                Element newCauseElement = ownerDoc.createElement(T_ERROR_CAUSE);
                cdata = ownerDoc.createCDATASection(((Throwable)causesIter.next()).toString());
                newCauseElement.appendChild(cdata);
                causeElement.appendChild(newCauseElement);
                causeElement = newCauseElement;
            }

            //append stack trace
            String stackTrace = "";
            Iterator stackTraces = Arrays.asList(error.getStackTrace()).iterator();
            while(stackTraces.hasNext())
                stackTrace = stackTrace + ((StackTraceElement)stackTraces.next()).toString() + "\n";

            Element stackTraceElement = ownerDoc.createElement(T_ERROR_STACK_TRACE);
            cdata = ownerDoc.createCDATASection(stackTrace);
            stackTraceElement.appendChild(cdata);
            errorElement.appendChild(stackTraceElement);
        }
        catch (Throwable e) {
            logger.error(e.toString());
            throw new XMLPublishingException("err.xml_publisher.failed_to_publish_error", e);
        }
        return errorElement;
    }

    /**
     *
     * */
    public void prepareCustomNode(String targetPath, NodePrototype nodePrototype){
        if(nodePrototype == null)
            return;

        Set nodePrototypes = (Set)nodeprototypes.get(targetPath);
        if(nodePrototypes == null){
            nodePrototypes = new HashSet();
            nodeprototypes.put(targetPath, nodePrototypes);
        }
        nodePrototypes.add(nodePrototype);
    }

    private Node publishEntity0(Entity entity, Node parentNode, String parentName, String entityName, String[] filters) throws XMLPublishingException {
        String fullName = ((parentName == null) ? "" : parentName) + ((parentName != null && entityName != null) ? "." : "") + ((entityName == null) ? "" : entityName);
        Element entityElement = null;
        String entityClassName = null;
        String entityId = null;
        try {
            FiltersResolver filtersResolver = new FiltersResolver(filters);

            entityClassName = entity.getClass().getName();
            entityId = entity.getId().toString();

            CDATASection cdata = null;
            Document ownerDoc = parentNode.getOwnerDocument();
            entityElement = ownerDoc.createElement(T_ENTITY);
            entityElement.setAttribute(A_NAME, (entityName == null) ? "" : entityName);
            entityElement.setAttribute(A_TYPE, entityClassName);
            entityElement.setAttribute(A_LABEL, entity.getLabel("name", docHeader.getLocale()));

            //publish ID
            ClassMetadata classMetadata = HibernateSessionFactoryProvider.getClassMetadata(entity.getClass()); 
            String attrIdName = classMetadata.getIdentifierPropertyName();
            if(filtersResolver.isPropertyAllowed(attrIdName)){
                Element idElement = ownerDoc.createElement(T_ID);
                idElement.setAttribute(A_NAME, attrIdName);
                idElement.setAttribute(A_LABEL, entity.getLabel(attrIdName, docHeader.getLocale()));
                idElement.setAttribute(A_TYPE, entity.getId().getClass().getName());
                cdata = ownerDoc.createCDATASection(entity.getId().toString());
                idElement.appendChild(cdata);
                entityElement.appendChild(idElement);
            }

            //publish all other properties
            Class entityClass = entity.getClass();
            //RM: Warning! Here we are lose all fields are inherited by from entity from BasicEntity and other parents!
            Field[] fields = entityClass.getDeclaredFields();
            if(fields != null){
                for(int i=0; i<fields.length; i++){
                    String propertyName = fields[i].getName();
                    Class propertyClass = fields[i].getType();
                    //Use setter method because field often has private access
                    String getterName = StringUtil.getGetterName(propertyName);
                    Method getter = entityClass.getDeclaredMethod(getterName, null);
                    Object propertyValue = getter.invoke(entity, null);

                    if((propertyValue instanceof Entity) && (filtersResolver.isPropertyAllowed(propertyName))){
                        publishEntity0((Entity)propertyValue, entityElement, fullName, propertyName, filtersResolver.getFiltersForEntity(propertyName));
                    }
                    else if(propertyValue instanceof Set){
                        publishEntitySet0((Set)propertyValue,
                                          entityElement,
                                          fullName,
                                          propertyName,
                                          entity.getLabel(propertyName, docHeader.getLocale()),
                                          null,
                                          filtersResolver.getFiltersForEntityInList(propertyName));
                    }
                    else if((propertyValue instanceof List) && (filtersResolver.isPropertyAllowed(propertyName))){
                        publishEntityList0((List)propertyValue,
                                          entityElement,
                                          fullName,
                                          propertyName,
                                          entity.getLabel(propertyName, docHeader.getLocale()),
                                          null,
                                          filtersResolver.getFiltersForEntityInList(propertyName));
                    }
                    else {
                        //publish field if it is not ID
                        if(!attrIdName.equals(propertyName) && filtersResolver.isPropertyAllowed(propertyName)){
                            Element propertyElement = null;
                            propertyElement = ownerDoc.createElement(T_PROPERTY);
                            propertyElement.setAttribute(A_NAME, propertyName);
                            propertyElement.setAttribute(A_TYPE, propertyClass.getName());
                            propertyElement.setAttribute(A_LABEL, entity.getLabel(propertyName, docHeader.getLocale()));
                            cdata = ownerDoc.createCDATASection((propertyValue == null) ? "" : propertyValue.toString());
                            propertyElement.appendChild(cdata);
                            entityElement.appendChild(propertyElement);
                        }
                    }
                }
            }
            parentNode.appendChild(entityElement);
            //add custom node
            addCustomNode0(entityElement, entity, fullName);
        }
        catch(XMLPublishingException e){
            throw e;//re-throw
        }
        catch(org.hibernate.LazyInitializationException e){
            //ignore exceptoin if entity is not initialized
            logger.debug("Entity '"+entity.getClass().getName()+"' is not initialized and ignored");
        }
        catch(Throwable e) {
            logger.error(e.toString());
            throw new XMLPublishingException("err.xml_publisher.failed_to_publish_entity", new String[]{entityClassName, entityId}, e);
        }
        return entityElement;

    }

    private Node publishEntityList0(List entityList, Node parentNode, String parentName, String setName, String setLabel, Long selectedId, String[] filters) throws XMLPublishingException {
        String fullName = ((parentName == null) ? "" : parentName + ".") + setName + "[*]";
        Element entityListElement = null;
        String selectedIdString = (selectedId == null) ? "" : selectedId.toString();
        try {
            Document ownerDoc = parentNode.getOwnerDocument();
            entityListElement = ownerDoc.createElement(T_SET);
            entityListElement.setAttribute(A_NAME, setName);
            entityListElement.setAttribute(A_SELECTED_ID, selectedIdString);
            if (entityList == null)
                return entityListElement;

            Iterator entitiesIter = entityList.iterator();
            while (entitiesIter.hasNext()) {
                publishEntity0((Entity) entitiesIter.next(), entityListElement, fullName, null, filters);
            }
            parentNode.appendChild(entityListElement);
        }
        catch(XMLPublishingException e){
            throw e; //re-throw
        }
        catch(org.hibernate.LazyInitializationException e){
            //ignore exceptoin if set is not initialized
            logger.debug("Entity list '"+setName+"' is not initialized and ignored");
        }
        catch (Throwable e) {
            logger.error(e.toString());
            throw new XMLPublishingException("err.xml_publisher.failed_to_publish_entity_list", new String[]{setName}, e);
        }
        return entityListElement;

    }

    private Node publishEntitySet0(Set entitySet, Node parentNode, String parentName, String setName, String setLabel, Long selectedId, String[] filters) throws XMLPublishingException {
        String fullName = ((parentName == null) ? "" : parentName + ".") + setName + "[*]";
        Element entitySetElement = null;
        String selectedIdString = (selectedId == null) ? "" : selectedId.toString();
        try {
            Document ownerDoc = parentNode.getOwnerDocument();
            entitySetElement = ownerDoc.createElement(T_SET);
            entitySetElement.setAttribute(A_NAME, setName);
            entitySetElement.setAttribute(A_LABEL, (setLabel == null) ? "" : setLabel);
            entitySetElement.setAttribute(A_SELECTED_ID, selectedIdString);
            if (entitySet == null || !Hibernate.isInitialized(entitySet))
                return entitySetElement;

            Iterator entitiesIter = entitySet.iterator();
            while (entitiesIter.hasNext()) {
                publishEntity0((Entity) entitiesIter.next(), entitySetElement, fullName, null, filters);
            }
            parentNode.appendChild(entitySetElement);
        }
        catch(XMLPublishingException e){
            throw e; //re-throw
        }
        catch(org.hibernate.LazyInitializationException e){
            //ignore exceptoin if set is not initialized
            logger.warn("Entity set '"+setName+"' is not initialized and ignored");
        }
        catch (Throwable e) {
            logger.error(e.toString());
            throw new XMLPublishingException("err.xml_publisher.failed_to_publish_entity_set", e);
        }
        return entitySetElement;

    }

    private void addCustomNode0(Node entityNode, Entity entity, String currentPath) throws XMLPublishingException{
        //Get all node prorotypes for given path and render it into XML nodes
        Set nodePrototypes = (Set)nodeprototypes.get(currentPath);
        if(nodePrototypes == null)
            return;//do nothing
        Iterator iter = nodePrototypes.iterator();
        while(iter.hasNext()){
            ((NodePrototype)iter.next()).renderAndInjectXMLNode(entityNode, entity);
        }
    }

    /** to operate with filters */
    private class FiltersResolver{
        private boolean useFilters = false;
        /** Holds all property names of first level:
         *  1) atomic properties.
         *  2) properties have type of Entity.
         *  3) properties have type of Set (List).
         *
         * Example:
         *
         * initial array of filters:
         * String[]{"myPropertyName", "myEntityName.entityPropertyName", "mySetName[*].entityPropertyName"}
         *
         * allProperties set holds:
         *
         * {"myPropertyName", "myEntityName", "mySetName"}
         *
         * */
        private Set allProperties = null;

        /** For each property name (property type is Set) holds the string array of assigned filters.
         *  Example:
         *
         * initial array of filters:
         * String[]{"mySet1[*].propName1", "mySet1[*].propName2", "mySet1[*].propName3"}
         *
         * setProperties map holds:
         *
         * key: "mySet1"
         * value: String[]{"propName1", "propName2", "propName3"}
         * */
        private Map setProperties = null;
        /** For each property name (property type is Entity) holds the string array of assigned filters.
         *  Example:
         *
         *  initial array of filters:
         * String[]{"entity1.propName1", "entity1.propName2", "entity1.propName3"}
         *
         * setProperties map holds:
         *
         * key: "entity1"
         * value: String[]{"propName1", "propName2", "propName3"}
         * */
        private Map entityProperties = null;

        /** to hide default constructor */
        private FiltersResolver(){
        }

        public FiltersResolver(String[] filters){
            if(filters == null)
                return;

            useFilters = true;
            allProperties = new HashSet();
            setProperties = new HashMap();
            entityProperties = new HashMap();

            //Run along String[] of filters and put them into atomicPropertis, setProperties, entityProperties structures.
            for(int i=0; i<filters.length; i++){
                if(filters[i] == null)
                    continue;

                String parsedFilter = filters[i].trim();
                boolean isAtomicProperty = true;//assumption
                for(int k=0; k<parsedFilter.length(); k++){
                    String symbol = parsedFilter.substring(k, k+1);
                    if(symbol.equals(".")){
                        //we've found filter for entity
                        String entityName = parsedFilter.substring(0, k);
                        String entityFilter = parsedFilter.substring(k+1);
                        //add filter is found to the array of filters of this entity
                        addFilter(entityName, entityFilter, entityProperties);
                        allProperties.add(entityName);
                        isAtomicProperty = false;
                        break;
                    }
                    else if(symbol.equals("[")){
                        String setName = parsedFilter.substring(0, k);
                        int idxCloseBracket = parsedFilter.indexOf("].");
                        String setFilter = parsedFilter.substring(idxCloseBracket+"].".length());
                        //add filter is found to the array of filters of this entity
                        addFilter(setName, setFilter, setProperties);
                        allProperties.add(setName);
                        isAtomicProperty = false;
                        break;
                    }
                }
                if(isAtomicProperty)
                    allProperties.add(parsedFilter);
            }
        }

        public boolean isPropertyAllowed(String propertyName){
            if(!useFilters)
                return true;//if there are no any filters, then all properties are allowed

            if(allProperties.contains(propertyName))
                return true;
            else
                return false;
        }

        public String[] getFiltersForEntity(String entityName){
            //if !useFilters we must return NULL else we must return String[] array with any length
            if(!useFilters)
                return null;

            String[] entityFilters = (String[])entityProperties.get(entityName);
            if(entityFilters == null)
                entityFilters = new String[0];

            return entityFilters;
        }

        public String[] getFiltersForEntityInList(String setName){
            //if !useFilters we must return NULL else we must return String[] array with any length
            if(!useFilters)
                return null;

            String[] setFilters = (String[])setProperties.get(setName);
            if(setFilters == null)
                setFilters = new String[0];

            return setFilters;
        }

        private void addFilter(String filterOwnerName, String filter, Map filterOwnerHolder){
            String[] filters = (String[])filterOwnerHolder.get(filterOwnerName);
            if(filters == null){
                filters = new String[1];
                filters[0] = filter;
            }
            else{
                String[] newFilters = new String[filters.length + 1];
                System.arraycopy(filters, 0, newFilters, 0, filters.length);
                newFilters[newFilters.length - 1] = filter;
                filters = newFilters;
            }
            filterOwnerHolder.put(filterOwnerName, filters);
        }

    }

}