package com.tmx.as.entities.bill.transaction;

/**
    According to SVBILL bussines logic to obtain a ore gain
    some client transactions could be converted into the set
    of delayed transactions which total amounts is equal to
    the amount of original client transaction.
    <br/>
    This entity represents such delayed parts of origianal
    transaction.
    <br>
    For now there is no additional attributes for such transaction
    in comparison with general <code>DelayedTransactin</code>.
    But the new entity and table in database are introduced to
    simplify distinguishing between them.
    simplify distinguishing between them.

 */
public class DelayedTransactionPart extends DelayedTransaction{
}
