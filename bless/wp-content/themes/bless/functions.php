<?php

function bless_language_switcher(){
    $languages = icl_get_languages();
    //echo "<!--qweqwe ".print_r($languages, true)."-->";
    ?><div class="lang"><?php
    foreach($languages as $l){
        //  || $l["language_code"] == "en"
        $act = $l["active"]?" active ":"";
        if($l["language_code"] == "ru") $langs["ru"] = '<a href="'.$l['url'].'" class="rus '.$act.' " ></a>';
        if($l["language_code"] == "en") $langs["en"] = '<a href="'.$l['url'].'" class="eng '.$act.' "></a>';
    }
    //echo join(' ', $langs);
    echo $langs["ru"]." ".$langs["en"];
    ?></div><?php
}