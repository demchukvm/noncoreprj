package com.tmx.as.resolver.functions.criterion;

import com.tmx.as.resolver.functions.Function;
import com.tmx.as.resolver.FunctionSyntaxException;
import com.tmx.as.base.criterion.Restrictions;

import java.util.Hashtable;

public class SizeLe implements Function {
    private Hashtable params=new Hashtable();
    public void set(int paramIndex, String paramName, Object paramValue) throws FunctionSyntaxException {
        params.put(paramName,paramValue);
    }
    public Object returnValue() throws FunctionSyntaxException {
        return Restrictions.sizeLe((String)params.get("propertyName"),((Integer)params.get("size")).intValue());
    }
}



