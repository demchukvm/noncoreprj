package com.tmx.beng.httpsgate.citypay;

/**
 * Created by IntelliJ IDEA.
 * User: Proximan
 * Date: 22.12.2010
 * Time: 10:50:48
 * To change this template use File | Settings | File Templates.
 */
import java.text.SimpleDateFormat;
import java.util.*;

public class DateTest {

    String format(Date visited)
    {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(visited);
    }

    String format1(Date visited)
    {
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:");
        return sdf.format(visited);
    }

    String format2(Date visited)
    {
        SimpleDateFormat sdf = new SimpleDateFormat("ss.SS");
        return sdf.format(visited);
    }

    String format3(Date visited)
    {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
        return sdf.format(visited);
    }

    public static void main(String args[])
    {
        DateTest dt = new DateTest();
        Date date = new Date();  

        System.out.println(dt.format(date)+"T"+dt.format1(date)+":"+Double.parseDouble(dt.format2(date)) +"+02:00");

        String df = dt.format3(date);
        System.out.println(df);
        Random rand = new Random();
        //long k = Long.parseLong(df);
        int i = (int) Long.parseLong(df);
        System.out.println(i);
        

    }

}


