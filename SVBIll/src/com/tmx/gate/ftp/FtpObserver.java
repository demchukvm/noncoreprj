package com.tmx.gate.ftp;


/**
 * The class that implement this interface have the ability to monitor the progress 
 * of upload and download files in the SFtpclient. 
 */ 

public interface FtpObserver {
    /**
     * This method is called every time new bytes are read in downloading process.
     * @param bytes The number of new bytes read from the server.
     */
    void byteRead(int bytes);

    /**
     * This method is called every time new bytes is written to the ftp server in uploading process.
     * @param bytes The number of new bytes write to the server.
     */
    void byteWrite(int bytes);
}
