<%@ page import="com.tmx.web.forms.core.tools.AddNewsForm" %>
<%@ page import="com.tmx.as.base.EntityManager" %>
<%@ page import="com.tmx.as.entities.general.user.User" %>
<%@ page language="java" pageEncoding="UTF-8" contentType="text/html; charset=utf-8"
         errorPage="/core/jsp/message/jsperror.jsp" isELIgnored="false" %>
<%@ taglib uri="/WEB-INF/tld/struts-nested.tld" prefix="nested" %>
<%@ taglib uri="/WEB-INF/tld/tmx-controls.tld" prefix="tmx_ctrl" %>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>

<%--<html:form action="/AddNews">--%>

<div style="border:0px solid gray;width:90%; height:350px;padding:2px;">

	<div style="border:0px solid gray;width:60%; float:left; padding:1px;">
		<p>
			<b>Заголовок</b><br/>
			<input name="messageTitle" type="text" style="background-color:white;border:1px solid black;width:100%;font-family:Times New Roman;font-size: 11pt;" onclick="storeCaret(this);pview_title();" onselect="storeCaret(this);" onkeyup="storeCaret(this);" value="<%= ((AddNewsForm) pageContext.getSession().getAttribute("AddNewsForm")).getMessageTitle() %>"/>
		</p>
		<p>
			<a href="javascript:insert('[left]','[/left]');"><img src="<tmx_ctrl:rewriteUrl href="/core/images/message_editor/ico2.gif"/>" border="0" width="24" height="24" alt="Выровнять по левому краю"></a>
			<a href="javascript:insert('[center]','[/center]');"><img src="<tmx_ctrl:rewriteUrl href="/core/images/message_editor/ico1.gif"/>" border="0" width="24" height="24" alt="Выровнять по центру"></a>
			<a href="javascript:insert('[right]','[/right]');"><img src="<tmx_ctrl:rewriteUrl href="/core/images/message_editor/ico8.gif"/>" border="0" width="24" height="24" alt="Выровнять по правому краю"></a>
			<a href="javascript:insert('[justify]','[/justify]');"><img src="<tmx_ctrl:rewriteUrl href="/core/images/message_editor/ico9.gif"/>" border="0" width="24" height="24" alt="Выровнять по ширине"></a>
			<%--<a href="javascript:insertIMAGE();"><img src="<tmx_ctrl:rewriteUrl href="/core/images/message_editor/ico11.gif"/>" border="0" width="24" height="24" alt="Вставить изображение"></a>--%>
			<a href="javascript:insert('[b]','[/b]');"><img src="<tmx_ctrl:rewriteUrl href="/core/images/message_editor/ico5.gif"/>" border="0" width="24" height="24" alt="Жирный"></a>
			<a href="javascript:insert('[i]','[/i]');"><img src="<tmx_ctrl:rewriteUrl href="/core/images/message_editor/ico6.gif"/>" border="0" width="24" height="24" alt="Курсивный"></a>
			<a href="javascript:insert('[u]','[/u]');"><img src="<tmx_ctrl:rewriteUrl href="/core/images/message_editor/ico7.gif"/>" border="0" width="24" height="24" alt="Подчеркнутый"></a>
			<%--<a href="javascript:insert('[red]','[/red]');"><img src="<tmx_ctrl:rewriteUrl href="/core/images/message_editor/ico10.gif"/>" border="0" width="24" height="24" alt="Абзац"></a>--%>
            <a href="#" onclick="go_back();"><img src="<tmx_ctrl:rewriteUrl href="/core/images/message_editor/ico12.gif"/>" border="0" width="24" height="24" alt="Отменить"></a>
		</p>
		<p>
    	    <textarea id="messageText" name="messageText" rows="15" cols="80" style="width:100%;background-color:white;border:1px solid black;font-family:Times New Roman;font-size: 11pt;width:100%;" onclick="storeCaret(this);pview();" onselect="storeCaret(this);" onkeyup="storeCaret(this);"></textarea>
            <script type="text/javascript">
                var txtA = document.getElementById("messageText");
                txtA.innerHTML="<%=((AddNewsForm) pageContext.getSession().getAttribute("AddNewsForm")).getMessageText() %>";
            </script>
            
        </p>
	</div>



	<!-- контролы -->
	<div style="border:0px solid gray;width:350px; float:right;padding:0px;">
        <table>

            <tr align="left">
                <td colspan="2">
                    <h4>Адресат:</h4>
                </td>
            </tr>

            <tr align="left">
                <td colspan="2">
                    <tmx_ctrl:selectBox property="scrollBoxUsers" />
                </td>
            </tr>

            <%--<tr align="left">--%>
                <%--<td colspan="2">--%>
                    <%--<!-- LastName + FirstName -->--%>

                <%--</td>--%>
            <%--</tr>--%>

            <%--<tr align="left">--%>
                <%--<td colspan="2">--%>
                    <%--<h4>Период показа:</h4>--%>
                <%--</td>--%>
            <%--</tr>--%>


            <%--<tr align="left">--%>
                <%--<td>--%>
                    <%--от--%>
                <%--</td>--%>
                <%--<td>--%>
                    <%--<tmx_ctrl:timeSelector property="timeSelectorActivation"/>--%>
                <%--</td>--%>
            <%--</tr>--%>


            <%--<tr align="left">--%>
                <%--<td>--%>
                    <%--до--%>
                <%--</td>--%>
                <%--<td>--%>
                    <%--<tmx_ctrl:timeSelector property="timeSelectorDeactivation"/>--%>
                <%--</td>--%>
            <%--</tr align="left">--%>


            <tr>
                <td colspan="2">
                    <hr/>
                </td>
            </tr>





            <tr align="left">
                <td>
                    <tmx_ctrl:button property="resetMessage" onClick="clearMessage();"/>
                </td>
                <td>
                    <tmx_ctrl:button property="saveMessage"/>
                </td>
            </tr>

            <tr>
                <td colspan="2">
                    <hr/><hr/>
                </td>
            </tr>

            <tr align="left">
                <td colspan="2">
                    <h4>Зарелизенные сообщения:</h4>
                </td>
            </tr>

            <tr align="left">
                <td colspan="2">
                    <tmx_ctrl:selectBox property="scrollBoxMessages"/>
                </td>
            </tr>

			<tr>
                <td colspan="2">
                    <hr/>
                </td>
            </tr>

            <tr align="left">
                <td colspan="2">
                    <tmx_ctrl:button property="deleteMessage"/>
                </td>
            </tr>

        </table>
	</div>

</div>

<div style="border:0px solid gray;width:750px; padding:3px;">
	<!--  В этом SPANе выводятся ошибки -->
    <span id="for_error">
        <br/>
        <%= ((AddNewsForm) pageContext.getSession().getAttribute("AddNewsForm")).getResponseText() %>
        <% ((AddNewsForm) pageContext.getSession().getAttribute("AddNewsForm")).setResponseText(""); %>
    </span>
</div>




<div style="width:750px; padding:20px;border: 0px solid black;">

	<div style="border: 0px solid black; padding:2px;">

		<div style="border: 0px solid black; background-color:white; padding:2px;text-align:right;font-size:20px;font-weight:bold;">
            <!-- заголовок -->
			<span id="for_title"></span>
			<%--<%= ((AddNewsForm) pageContext.getSession().getAttribute("AddNewsForm")).getMessageTitle() %>--%>
		</div>

		<div style="height: 2px;"></div>

		<div style="border: 1px dotted black; padding:5px;text-align:justify;">
            <!-- текст сообщения -->
            <span id="for_pview" style="width:700px;white-space: pre-wrap;white-space: -moz-pre-wrap;white-space: -pre-wrap;white-space: -o-pre-wrap;word-wrap: break-word;"></span>
			<!--<p id="for_pview" style="width:700px;white-space:pre-line;"></p>-->

			<%--<%= ((AddNewsForm) pageContext.getSession().getAttribute("AddNewsForm")).getMessageText() %>--%>
		</div>

		<div style="height: 2px;"></div>

		<div style="border: 0px solid black; background-color:white; padding:2px;text-align:right;font-style:italic;">
			<c:if test="${messages.autor != null}">
				Автор:
			</c:if>
			<br/>

		</div>

	</div>

</div>


<%--
    <div align="left" style="width:100%;font-family:Times New Roman;font-size: 11pt;">
    <%= ((AddNewsForm) pageContext.getSession().getAttribute("AddNewsForm")).getResponseText() %>
    </div>
--%>
