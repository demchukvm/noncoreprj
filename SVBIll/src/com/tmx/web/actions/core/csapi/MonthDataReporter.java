package com.tmx.web.actions.core.csapi;

import com.tmx.as.base.EntityManager;
import com.tmx.as.blogic.BlogicException;
import com.tmx.as.entities.bill.balance.SellerBalance;
import com.tmx.as.entities.bill.voucher.Voucher;
import com.tmx.as.exceptions.DatabaseException;
import com.tmx.as.scheduler.ProcessingThread;
import com.tmx.beng.httpsgate.avancel.AvancelGate1;
import com.tmx.beng.httpsgate.citypay.CitypayGate;
import com.tmx.beng.httpsgate.kyivstar.KyivstarBonusNoCommissionGate;
import com.tmx.beng.httpsgate.kyivstar.KyivstarExclusiveGate;
import com.tmx.beng.httpsgate.kyivstar.KyivstarGate;
import com.tmx.beng.httpsgate.umc.UmcGate;
import jxl.Workbook;
import jxl.write.Label;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;

import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: Proximan
 * Date: 13.12.2010
 * Time: 15:50:51
 * To change this template use File | Settings | File Templates.
 */
public class MonthDataReporter extends ProcessingThread
{
    @Override
    protected void executeSpecificTask()
    {
        try {
            List sellerBalance = getSellerBalance();
            List voucherRemains = getVoucherRemains();
            List providerBalance = new ArrayList();

            String obj[][] = {
                        {"Kyivstar", getProviderBalance("Kyivstar")},
                        {"KS_BonusNoComission", getProviderBalance("KS_BonusNoComission")},
                        {"KS_FixConnection", getProviderBalance("KS_FixConnection")},
                        {"MTS", getProviderBalance("MTS")},
                        {"Avancel", getProviderBalance("Avancel")},
                        {"CityPay", getProviderBalance("CityPay")},
                    };

            providerBalance.add(obj[0]);
            providerBalance.add(obj[1]);
            providerBalance.add(obj[2]);
            providerBalance.add(obj[3]);
            providerBalance.add(obj[4]);
            providerBalance.add(obj[5]);

            try {
                saveDataToFile(sellerBalance, voucherRemains, providerBalance);
            } catch (BlogicException e) {
                e.printStackTrace();
            }
        } catch (DatabaseException e) {
            e.printStackTrace();
        }
    }

    private List getSellerBalance() throws DatabaseException
    {
        return new EntityManager().EXECUTE_QUERY(SellerBalance.class, "monthDataSaver", null);
    }

    private List getVoucherRemains() throws DatabaseException
    {
        return new EntityManager().EXECUTE_QUERY(Voucher.class, "remains", null);
    }

    private String getProviderBalance(String providerName)
    {
        String val = null;
        if(providerName.equals("Kyivstar"))
        {
            try{
                val = KyivstarGate.getInstance().getBalance("KyivstarPoP").toString();
            }catch(Throwable e){
                val = e.getLocalizedMessage();
            }
        }
        if(providerName.equals("KS_BonusNoComission"))
        {
            try{
                val = KyivstarBonusNoCommissionGate.getInstance().getBalance("KyivstarPoP").toString();
            }catch(Throwable e){
                val = e.getLocalizedMessage();
            }
        }
        if(providerName.equals("KS_FixConnection"))
        {
            try{
                val = KyivstarExclusiveGate.getInstance().getBalance("KyivstarPoP").toString();
            }catch(Throwable e){
                val = e.getLocalizedMessage();
            }
        }
        if(providerName.equals("MTS"))
        {
            try {
                val = UmcGate.getInstance().getBalance("UMCPoP").toString();
            } catch (Throwable e) {
                val = e.getLocalizedMessage();
            }
        }
        if(providerName.equals("Avancel"))
        {
            try {
                val = AvancelGate1.getInstance().getBalance().toString();
            } catch (Throwable e) {
                val = e.getLocalizedMessage();
            }

        }
        if(providerName.equals("CityPay"))
        {
            try {
                val = CitypayGate.getInstance().getBalance().toString();
            } catch (Throwable e) {
                val = e.getLocalizedMessage();
            }
        }
        return val;
    }

    private void saveDataToFile(List sellerBalance, List voucherRemains, List providerBalance) throws BlogicException
    {
        try {
            String xlsFileUrl = composeFileName(".xls");
            File xlsFile = new File(xlsFileUrl);
            WritableWorkbook workbook = null;
            workbook = Workbook.createWorkbook(xlsFile);
            WritableSheet sheet1 = workbook.createSheet("Sellers Balances", 0);
            WritableSheet sheet2 = workbook.createSheet("Vouchers Remains", 1);
            WritableSheet sheet3 = workbook.createSheet("Gates Balances", 2);

            for(int i = 0; i < sellerBalance.size(); i++)
            {
                Object[] rowObject = (Object[]) sellerBalance.get(i);
                if(rowObject[1] != null)
                {
                    for (int j = 0; j < rowObject.length; j++)
                    {
                        String cell = (rowObject[j] != null) ? rowObject[j].toString() : "";
                        Label label = new Label(j, i, cell);
                        sheet1.addCell(label);
                    }
                }
            }

            for(int k = 0; k < voucherRemains.size(); k++)
            {
                Object[] rowObject = (Object[]) voucherRemains.get(k);
                if(rowObject[1] != null)
                {
                    for (int j = 0; j < rowObject.length; j++)
                    {
                        String cell = (rowObject[j] != null) ? rowObject[j].toString() : "";
                        Label label = new Label(j, k, cell);
                        sheet2.addCell(label);
                    }
                }
            }

            for(int l = 0; l < providerBalance.size(); l++)
            {
                Object[] rowObject = (Object[]) providerBalance.get(l);
                if(rowObject[1] != null)
                {
                    for (int j = 0; j < rowObject.length; j++)
                    {
                        String cell = (rowObject[j] != null) ? rowObject[j].toString() : "";
                        Label label = new Label(j, l, cell);
                        sheet3.addCell(label);
                    }
                }
            }

            workbook.write();
            workbook.close();

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private String composeFileName(String fileExtention)
    {
        final DateFormat format = new SimpleDateFormat("yyyy.MM.dd");
        Date date = new Date();
        return new StringBuffer().
                append(System.getProperty("tmx.home")).
                append("/month_data/").
                append("monthData_").
                append(format.format(date)).
                append(fileExtention).
                toString();
    }
}
