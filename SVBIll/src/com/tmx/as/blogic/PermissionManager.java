package com.tmx.as.blogic;

import com.tmx.as.base.EntityManager;
import com.tmx.as.base.FilterWrapper;
import com.tmx.as.entities.general.ui_permission.Permission;
import com.tmx.as.entities.general.ui_permission.PermissionType;
import com.tmx.as.entities.general.ui_permission.UserInterface;
import com.tmx.as.entities.general.ui_permission.UserInterfaceType;
import com.tmx.as.entities.general.role.Role;
import com.tmx.as.exceptions.DatabaseException;
import java.util.List;
import java.util.Map;
import java.util.Hashtable;
import java.util.Iterator;

public class PermissionManager extends EntityManager{

    public List getPermissionTypes() throws DatabaseException {
        return RETRIEVE_ALL(PermissionType.class,null);
    }

    public List<Permission> getRolePermissions(Long roleId) throws DatabaseException {
        FilterWrapper filter = new FilterWrapper("com_tmx_as_entities_general_ui_permission_Permission_role");
        filter.setParameter("value", roleId);
        return RETRIEVE_ALL(Permission.class, new FilterWrapper[]{filter}, new String[]{"permissionType", "userInterface.type"});
    }

    public void saveDifferentPermissionsMapElements(Map permissionsApply, Map permissionsHash,List permissionEntities,
                                                    Long uiTypeId,Long roleId) throws DatabaseException {
        Iterator iterator=permissionsApply.keySet().iterator();
        while (iterator.hasNext()) {
            String uiName=(String)iterator.next();
            Long permissionTypeHash=(Long)permissionsHash.get(uiName);
            Long permissionType=new Long(permissionsApply.get(uiName).toString());
            if(permissionTypeHash!=null){
                if(permissionType.equals(permissionTypeHash)) continue;
                else if(!permissionType.equals(new Long(1))){
                    Permission permission=findPermission(permissionEntities,uiName,uiTypeId);
                    PermissionType permissionTypeObject=new PermissionType();
                    permissionTypeObject.setPermissionTypeId(permissionType);
                    permission.setPermissionType(permissionTypeObject);
                    permissionsHash.put(uiName,permissionType);
                    SAVE(permission);
                }else{
                    Permission permission=findPermission(permissionEntities,uiName,uiTypeId);
                    DELETE(Permission.class,permission.getPermissionId());
                    permissionsHash.remove(uiName);
                }
            }else if(!permissionType.equals(new Long(1))){
                Permission permission=new Permission();
                Role role=new Role();role.setRoleId(roleId);
                permission.setRole(role);
                PermissionType permissionTypeObject=new PermissionType();permissionTypeObject.setPermissionTypeId(permissionType);
                permission.setPermissionType(permissionTypeObject);

                // findUserInterface
                UserInterface userInterface=findUserInterface(uiName,uiTypeId);
                if(userInterface==null){
                    userInterface=new UserInterface();
                    UserInterfaceType userInterfaceType=new UserInterfaceType();userInterfaceType.setUserInterfaceTypeId(uiTypeId);
                    userInterface.setType(userInterfaceType);
                    userInterface.setName(uiName);
                    UserInterfaceType uit =new UserInterfaceType();uit.setUserInterfaceTypeId(uiTypeId);
                    userInterface.setType(uit);
                    SAVE(userInterface);
                }

                permission.setUserInterface(userInterface);
                SAVE(permission);
                permission=(Permission)RETRIEVE(Permission.class,permission.getPermissionId(),new String[]{"permissionType","userInterface.type"});
                permissionEntities.add(permission);
                permissionsHash.put(uiName,permissionType);
            }
        }
    }

    private UserInterface findUserInterface(String name,Long type) throws DatabaseException {
        FilterWrapper filterName=new FilterWrapper("com_tmx_as_entities_general_ui_permission_UserInterface_uiName");
        filterName.setParameter("value",name);
        FilterWrapper filterType=new FilterWrapper("com_tmx_as_entities_general_ui_permission_UserInterface_type");
        filterType.setParameter("value",type);
        return (UserInterface)RETRIEVE(UserInterface.class,new FilterWrapper[]{filterName,filterType},null);
    }

    private Permission findPermission(List permissionEntities,String uiName,Long uiTypeId){
        Iterator iterator=permissionEntities.iterator();
        while (iterator.hasNext()) {
            Permission permission=(Permission)iterator.next();
            if(permission.getUserInterface().getName().equals(uiName)&&
               permission.getUserInterface().getType().getUserInterfaceTypeId().equals(uiTypeId))
               return permission;
        }
        return null;
    }
}
