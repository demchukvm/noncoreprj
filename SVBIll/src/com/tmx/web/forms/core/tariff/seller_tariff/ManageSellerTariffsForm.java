package com.tmx.web.forms.core.tariff.seller_tariff;

import com.tmx.as.blogic.SellerManager;
import com.tmx.as.entities.bill.seller.Seller;
import com.tmx.as.entities.general.user.User;
import com.tmx.as.exceptions.DatabaseException;
import com.tmx.web.controls.ScrollBox;
import com.tmx.web.controls.table.Parameter;
import com.tmx.web.forms.core.tariff.AbstractManageTariffsForm;
import com.tmx.web.forms.core.base.AbstractFormFactory;


public class ManageSellerTariffsForm extends AbstractManageTariffsForm {

    protected void init(){
        super.init();
        initTariffsTable();
        initButtons();
        initFilters();
    }

    protected void bindSpecificData(User loggedInUser) throws DatabaseException {
//        ScrollBox sellersScrollBox = (ScrollBox) getTariffsTable().getParameter("by_seller_id", "seller_id").getControl();
//        sellersScrollBox.bind(new SellerManager().getNestedSellersInSeller(loggeInUser.getSellerOwner()));

        AbstractFormFactory.getInstance(loggedInUser).setUpForm(this);
    }

    private void initTariffsTable() {
        if(getTariffsTable()!=null){
            getTariffsTable().loadQuery("query(query_type=retrieve_all," +
                    "entity_class=com.tmx.as.entities.bill.tariff.SellerTariff," +
                    "table_attr=functionInstance.functionType," +
                    "table_attr=seller," +
                    "order(name=sellerId,type=asc)," +
                    "page_size=15" +
                    ")");
            getTariffsTable().setRefreshActionName("/core/tariff/seller_tariff/manageSellerTariffs");
        }
    }

    private void initFilters(){
        getTariffsTable().addParameterToFilter("by_seller_id", new Parameter("seller_id", initSellersScrollBoxFilter()));
    }

    private ScrollBox initSellersScrollBoxFilter(){
        ScrollBox sellersScrollBox = new ScrollBox(){
                        protected String getKey(Object dataListElement) {
                            return ((Seller)dataListElement).getId().toString();
                        }

                        protected String getValue(Object dataListElement) {
                            return ((Seller)dataListElement).getName();
                        }
                    };
        sellersScrollBox.setNullable(false);
        return sellersScrollBox;
    }

    private void initButtons(){
        getOnCreateButton().setAction("/core/tariff/seller_tariff/infoSellerTariff");
        getOnEditButton().setAction("/core/tariff/seller_tariff/infoSellerTariff");
        getOnDeleteButton().setAction("/core/tariff/seller_tariff/infoSellerTariff");
    }

}
