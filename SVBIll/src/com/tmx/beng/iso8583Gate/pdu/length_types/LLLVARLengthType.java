package com.tmx.beng.iso8583Gate.pdu.length_types;

import com.tmx.beng.iso8583Gate.pdu.IsoParseException;
import com.tmx.beng.iso8583Gate.pdu.IsoSerializeException;
import com.tmx.beng.iso8583Gate.pdu.IncorrectFormatException;

import java.io.InputStream;
import java.io.IOException;
import java.io.OutputStream;

/**
 */
public class LLLVARLengthType extends LengthType {
    {
        name = "LLLVAR";
    }

    public int parseLength(InputStream is, int maxLength) throws IsoParseException {
        try{
            byte[] length = new byte[2];
            is.read(length);
            //length could not be more then int
            return (int)bcdConverter.getLongFromBcd(length, 3/** length value in 3 nibbles */);
        }
        catch(IOException e){
            throw new IsoParseException("err.lllvar_lengthtype.parsing_failed", e);
        }
        catch(IncorrectFormatException e){
            throw new IsoParseException("err.lllvar_lengthtype.parsing_failed", e);
        }        
    }

//    public void serializeLength(OutputStream os, int length) throws IsoSerializeException {
//        try{
//            byte[] bcdLength = bcdConverter.getUnsignedBcdFromLong(length);
//            if(bcdLength.length > 2)
//                throw new IsoSerializeException("err.lllvar_lengthtype.place_is_not_enough_for_value", new String[]{String.valueOf(length)});
//            else if(bcdLength.length == 1){
//                byte[] fixedBcdLength = new byte[2];
//                fixedBcdLength[1] = bcdLength[0];//place into low byte
//                bcdLength = fixedBcdLength;
//            }
//            os.write(bcdLength);
//        }
//        catch(IOException e){
//            throw new IsoSerializeException("err.lllvar_lengthtype.serialization_failed", new String[]{String.valueOf(length)}, e);
//        }
//    }

    public byte[] serializeLength(int length) throws IsoSerializeException{
        byte[] bcdLength = bcdConverter.getUnsignedBcdFromLong(length);
        if (bcdLength.length > 2)
            throw new IsoSerializeException("err.lllvar_lengthtype.place_is_not_enough_for_value", new String[]{String.valueOf(length)});
        else if (bcdLength.length == 1) {
            byte[] fixedBcdLength = new byte[2];
            fixedBcdLength[1] = bcdLength[0];//place into low byte
            bcdLength = fixedBcdLength;
        }
        return bcdLength;
    }
}
