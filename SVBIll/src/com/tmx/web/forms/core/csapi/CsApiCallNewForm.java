package com.tmx.web.forms.core.csapi;

import com.tmx.beng.access.Access;
import com.tmx.web.controls.Button;
import com.tmx.web.controls.TextArea;
import com.tmx.web.forms.xmlengine.BasicXMLPublisherForm;


/**
 */
public class CsApiCallNewForm extends BasicXMLPublisherForm {
    private Access billingEngineAccess = null;
    private String requestXml = null;
    private String desRequestXml = null;

    private TextArea textAreaApi;

    private Button buttonUploadReport;

    public Button getButtonUploadReport() {
        return buttonUploadReport;
    }

    public void setButtonUploadReport(Button buttonUploadReport) {
        this.buttonUploadReport = buttonUploadReport;
  //      textAreaApi.setValue("Hello");
    }

    private void initButtons() {
        String action = "/csapicallnew";
        buttonUploadReport = new Button();
        buttonUploadReport.setLabelKey("1");
        buttonUploadReport.setAction(action);
        buttonUploadReport.setCommand("defaultAction");
    }

    protected void init() {
        initButtons();
    }

    public TextArea getTextAreaApi() {
        return textAreaApi;
    }

    public void setTextAreaApi(TextArea textAreaApi) {
        this.textAreaApi = textAreaApi;
    }

    //------------------Properties

    public String getRequestXml() {
        return requestXml;
    }

    public void setRequestXml(String requestXml) {
        this.requestXml = requestXml;
    }


    public Access getBillingEngineAccess() {
        return billingEngineAccess;
    }

    public void setBillingEngineAccess(Access billingEngineAccess) {
        this.billingEngineAccess = billingEngineAccess;
    }

    public String getDesRequestXml() {
        return desRequestXml;
    }

    public void setDesRequestXml(String desRequestXml) {
        this.desRequestXml = desRequestXml;
    }
}