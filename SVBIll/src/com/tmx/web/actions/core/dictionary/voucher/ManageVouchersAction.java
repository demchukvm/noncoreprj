package com.tmx.web.actions.core.dictionary.voucher;

import com.tmx.as.blogic.BlogicException;
import com.tmx.as.blogic.VoucherManager;
import com.tmx.as.entities.bill.voucher.Voucher;
import com.tmx.as.exceptions.DatabaseException;
import com.tmx.beng.csapi.java.base.BuyVoucherResp;
import com.tmx.web.actions.core.AbstractSvbillAction;
import com.tmx.web.controls.PopulateUtil;
import com.tmx.web.forms.core.dictionary.voucher.ManageVouchersForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForm;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Date;

public class ManageVouchersAction extends AbstractSvbillAction
{

    private static final String FORWARD = "manageVouchersForm";

    public ActionForward defaultAction(ActionMapping mapping,
                                       ActionForm form,
                                       HttpServletRequest request,
                                       HttpServletResponse response,
                                       String methodName) throws Throwable {
        ManageVouchersForm manageVouchersForm = (ManageVouchersForm) form;

        /*
        PrintWriter out = response.getWriter();
        try {
            out.println("<script type=\"text/javascript\">");
            out.println("var width = 800;");
            out.println("var height = 600;");
            out.println("var left = (screen.width - width) / 2;");
            out.println("var top = (screen.height - height) / 2;");
            out.println("window.open('svbill/TestAction.do', 'upload', 'width='+width+',height='+height+',top='+top+',left='+left+',channelmode=no,directories=no,location=no,menubar=no,resizable=yes,scrollbars=yes,status=yes,titlebar=yes,toolbar=no', 'false');");
            out.println("</script>");
        } finally {
            out.close();
        }
        
        */

        reload(manageVouchersForm);

        return mapping.findForward(FORWARD);
    }

    private void reload(ManageVouchersForm manageVouchersForm) throws Throwable
    {
        List voucherRamains = manageVouchersForm.getVouchersRamains();
        manageVouchersForm.getGridVoucherRmains().bind(voucherRamains);

        List voucherNominals = new ArrayList();
        for(Object v : voucherRamains)
        {
            Object[] nominal = (Object[]) v;
            voucherNominals.add(nominal[0]);
        }
        manageVouchersForm.getSbVoucherNominal().bind(voucherNominals);
    }

    public ActionForward cutToPrepaid(ActionMapping mapping,
                                       ActionForm form,
                                       HttpServletRequest request,
                                       HttpServletResponse response) throws Throwable {

        ManageVouchersForm manageVouchersForm = (ManageVouchersForm) form;

        if(manageVouchersForm.getSbVoucherNominal().validate() && manageVouchersForm.getEbVoucherCount().validate())
        {
            String nominal = manageVouchersForm.getSbVoucherNominal().getSelectedKey();
            ArrayList allVouchersByNominal = (ArrayList) new VoucherManager().getAllValidVouchersByNominal(nominal);
            ArrayList cutVouchers = new ArrayList();

            int count = PopulateUtil.getIntValue(manageVouchersForm.getEbVoucherCount());

            if(count > allVouchersByNominal.size())
            {
                count = allVouchersByNominal.size();
            }

            for(int i=0; i<count; i++)
            {
                cutVouchers.add(allVouchersByNominal.get(i));
            }

            allVouchersByNominal.removeAll(cutVouchers);
            System.out.println("cutVouchers.size = " + cutVouchers.size() + " & " + "allVouchersByNominal.size = " + allVouchersByNominal.size());

            ArrayList cutVouchersFinal = new ArrayList();
            cutVouchersFinal.addAll(cutVouchers);

            for (Object cutVoucher : cutVouchers)
            {
                Object[] voucher = (Object[]) cutVoucher;
                boolean flag = new VoucherManager().updateVoucherToCutStatus(Long.parseLong(voucher[0].toString()));
                if(!flag) cutVouchersFinal.remove(cutVoucher);   
            }

            // выгружаем все ваучеры в файл

            manageVouchersForm.setAbsoluteFileName(createCutFile(cutVouchersFinal, nominal));

            manageVouchersForm.getSbVoucherNominal().reset();
            manageVouchersForm.getEbVoucherCount().reset();

            reload(manageVouchersForm);
        }

        return mapping.findForward(FORWARD);
    }

    private String createCutFile(List cutVouchersFinal, String voucherNominal) throws BlogicException
    {
        String fileName = composeVouchersFileName(cutVouchersFinal.size(), voucherNominal);
        
        if(voucherNominal.contains("Intertelecom"))
        {
            createIntertelecomFile(cutVouchersFinal);
        }
        else if(voucherNominal.contains("Kyivstar"))
        {
            createKyivstarFile(cutVouchersFinal, fileName);
        }
        else if(voucherNominal.contains("Life"))
        {
            createLifeFile(cutVouchersFinal);
        }
        else if(voucherNominal.contains("PeopleNet"))
        {
            createPeopleNetFile(cutVouchersFinal);
        }
        else if(voucherNominal.contains("Utel"))
        {
            createUtelFile(cutVouchersFinal);
        }

        return fileName;
    }

    private void createIntertelecomFile(List cutVouchersFinal)
    {

    }

    private void createKyivstarFile(List cutVouchersFinal, String fileName) throws BlogicException
    {
        try{
            PrintStream out = new PrintStream(new FileOutputStream(fileName));

            SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");

            Object[] forMaskRow = (Object[]) cutVouchersFinal.get(0);

            StringBuffer maskFirstRow = new StringBuffer();
           // maskFirstRow.append(dateFormat.format(v.getBestBeforeDate()))


            /*
            final DecimalFormat numberFormat = new DecimalFormat("000000000");
            for (BuyVoucherResp v : vouchers) {
                if(v.getSecretCode() != null && v.getCode() != null && v.getPrice() != null && v.getBestBeforeDate() != null){
                    final StringBuffer row = new StringBuffer();
                    row.append(formatString(v.getSecretCode(), 14, " "));
                    row.append(formatString(v.getCode(), 9, " "));
                    row.append(numberFormat.format(v.getPrice() / 100)); //convert to UAH
                    row.append("S0");
                    row.append(dateFormat.format(v.getBestBeforeDate()));
                    out.println(row.toString());
                }
            }
            return fileUrl; */
        }
        catch (Exception e){
            throw new BlogicException("Failed to dump Life vouchers", e);
        }
    }

    private void createLifeFile(List cutVouchersFinal)
    {

    }

    private void createPeopleNetFile(List cutVouchersFinal)
    {

    }

    private void createUtelFile(List cutVouchersFinal)
    {

    }

    private String composeVouchersFileName(int size, String voucherNominal)
    {
        Date d = new Date();
        final DateFormat format = new SimpleDateFormat("yyyy.MM.dd_HH.mm.ss");
        return new StringBuffer().
                append(System.getProperty("tmx.home")).
                append("/backup/vouchersToPrepaid/").
                append(format.format(new Date())).
                append("_").
                append(voucherNominal).
                append("x").
                append(size).
                append(".csv").
                toString();
    }

    public ActionForward loadFile(ActionMapping mapping,
                                       ActionForm form,
                                       HttpServletRequest request,
                                       HttpServletResponse response) throws Throwable {

        ManageVouchersForm manageVouchersForm = (ManageVouchersForm) form;

        return mapping.findForward(FORWARD);
    }

    public ActionForward addToSvbill(ActionMapping mapping,
                                       ActionForm form,
                                       HttpServletRequest request,
                                       HttpServletResponse response) throws Throwable {

        ManageVouchersForm manageVouchersForm = (ManageVouchersForm) form;
        manageVouchersForm.validate();

        return mapping.findForward(FORWARD);
    }


    public ActionForward upload(ActionMapping mapping,
                                       ActionForm form,
                                       HttpServletRequest request,
                                       HttpServletResponse response) throws Throwable {

        ManageVouchersForm manageVouchersForm = (ManageVouchersForm) form;



        return mapping.findForward(FORWARD);
    }

    
}
