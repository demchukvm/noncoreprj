package com.tmx.beng.base;

import com.tmx.beng.access.Access;
import com.tmx.util.InitException;
import com.tmx.as.base.EntityManager;
import com.tmx.as.base.QueryParameterWrapper;
import com.tmx.as.entities.mobipay.message.Message;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import java.util.Properties;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: Shake
 * Date: 21.02.2007
 * Time: 11:49:59
 */
public abstract class BasicGate {
    protected Properties properties = null;
    public static final Integer MSG_TYPE_ERROR = new Integer(1);

    protected Access obtainBillingEngineAccess() throws InitException {
        InitialContext ctx = null;
        Access newAccess = null;
        try {
            System.setProperty("java.naming.factory.url.pkgs", ""); //=org.apache.naming
            ctx = new InitialContext();//System.getProperties()
            newAccess = (Access) ctx.lookup("java:comp/services/billingEngine");
            if (newAccess == null)
                throw new InitException("err.csapi.null_basic_access_obtained");
        }
        catch (NamingException e) {
            throw new InitException("err.csapi.failed_to_obtain_billing_engine_access", e);
        }
        finally {
            if (ctx != null) {
                try {
                    ctx.close();
                }
                catch (NamingException e) {
                    //log.info( "Unable to release initial context", ne );
                }
            }
        }
        return newAccess;
    }

    /** Get i18n operator's message by its code */
    public String obtainOperatorI18nMessage(String operatorCode, Integer messageType, Long code, String defaultMessage){
        String result = defaultMessage;
        try{

            List list = new EntityManager().EXECUTE_QUERY(
                    Message.class,
                    "get_message",
                    new QueryParameterWrapper[]{
                            new QueryParameterWrapper("operator", operatorCode),
                            new QueryParameterWrapper("code", code),
                            new QueryParameterWrapper("type", messageType)
                    }
            );
            if(list != null && list.iterator().hasNext()){
                result = (String)list.iterator().next() + " (" + code + ")";
            }

        }catch(Throwable e){

        }
        return result;
    }

//    public String getTxStatus() {
//        return         
//    }

}
