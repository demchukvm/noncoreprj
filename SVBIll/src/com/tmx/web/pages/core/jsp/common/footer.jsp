<%@ page language="java" pageEncoding="UTF-8" contentType="text/html; charset=utf-8" errorPage="/core/jsp/message/jsperror.jsp" isELIgnored="false" %>
<%@ taglib uri="http://tmx.com/taglib/tmx-controls" prefix="tmx_ctrl" %>

<table cellpadding='0' cellspacing='0' width='100%' border='0' class='underFooter'>
    <tr>
        <td align='center' class='copyRight'>
            &copy; 2003-2007
            <a href="http://www.tmx.com.ua" target="_blank" class="copyRightLink">Technomatix Inc.</a>
            All Rights Reserved.<br />
            <a href='http://www.tmx.com.ua' target='_blank'>
                <img style='margin-top: 2px' border='0' width='106' height='23' src='<tmx_ctrl:rewriteUrl href="/core/images/common/powered_by_tmx.jpg"/>' alt='Powered By Technomatix'/>
            </a>
        </td>
    </tr>
</table>

