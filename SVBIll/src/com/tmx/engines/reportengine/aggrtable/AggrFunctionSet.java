package com.tmx.engines.reportengine.aggrtable;

import com.tmx.engines.reportengine.base.ReportException;

import java.util.Iterator;
import java.util.List;
import java.util.Set;

/**
 * Set of aggregated functions
 */
public class AggrFunctionSet {
    /** List of aggr set's rows. Each row is List of AggrFunction */
    private List functions = null;
    /** aggr function set position */
    private AggrFunction.Metadata.Position setPosition = null;


    /** get functions count in this set*/
    public int getFunctionsCount(){
        return (functions == null) ? 0 : functions.size();
    }

    /** get function by its orderNum */
    public AggrFunction getFunction(int orderNum){
        return (functions == null) ? null : (AggrFunction)functions.get(orderNum);
    }


    //----------package local methods

    /** Evaluate functions
     * @param headerSet corresponding header set to this aggregated functions set.
     * @param data original data. */
    void evaluateFunctions(HeaderSet headerSet, Data data) throws ReportException {
        for(Iterator iter = functions.iterator(); iter.hasNext();){
            ((AggrFunction)iter.next()).evaluate(headerSet, data);
        }
    }

    /** package local constructor. Initialize internal structure
     * according to the given mappingRules
     * @param aggrFunctionMetadata     Set of AggrFunction.Metadata - aggr functions mapping rules
     * @param position             descibes how current HeaderSet used in aggregated table
     * */
    AggrFunctionSet(Set aggrFunctionMetadata, AggrFunction.Metadata.Position position){
        setPosition = position;
        functions = new SafeArrayList();
        for(Iterator iter = aggrFunctionMetadata.iterator(); iter.hasNext();){
            AggrFunction.Metadata metadata = (AggrFunction.Metadata)iter.next();
            if(setPosition.equals(metadata.getPosition())){
                ((SafeArrayList)functions).safeSet(metadata.getOrderNum().intValue(), new AggrFunction(metadata));
            }
        }
    }
}
