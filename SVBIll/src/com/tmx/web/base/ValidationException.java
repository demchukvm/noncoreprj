package com.tmx.web.base;

import com.tmx.util.StructurizedException;
import java.util.Locale;

import org.apache.struts.action.ActionForward;

/** */
public class ValidationException extends StructurizedException {
    /** Used to setup input page forward if validated form used by more then one JSP input pages.
     * For only one JSP use 'imput' action attribute in struts-config.xml file. */
    private ActionForward inputPageForward = null;

    public ValidationException(String errKey, ActionForward inputPageForward) {
        super(errKey);
        this.inputPageForward = inputPageForward;
    }

    public ValidationException(String errKey, Throwable cause) {
        super(errKey, cause);
    }

    public ValidationException(String errKey, Throwable cause, Locale locale) {
        super(errKey, cause, locale);
    }

    public ValidationException(String errKey, String[] params, Throwable cause) {
        super(errKey, params, cause);
    }

    public ValidationException(String errKey) {
        super(errKey);
    }

    public ValidationException(String errKey, String[] params) {
        super(errKey, params);
    }

    public ValidationException(String errKey, String[] params, Throwable cause, Locale locale) {
        super(errKey, params, cause, locale);
    }


    public ActionForward getInputPageForward() {
        return inputPageForward;
    }
}