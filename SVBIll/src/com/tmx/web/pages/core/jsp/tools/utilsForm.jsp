<%@ page language="java" pageEncoding="UTF-8" contentType="text/html; charset=utf-8"
         errorPage="/core/jsp/message/jsperror.jsp" isELIgnored="false" %>
<%@ taglib uri="/WEB-INF/tld/struts-nested.tld" prefix="nested" %>
<%@ taglib uri="/WEB-INF/tld/tmx-controls.tld" prefix="tmx_ctrl" %>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>


<html:form action="/core/tools/utils">
<table border="0" width="100%">

    <!-- BUTTONS -->
    <tr>
        <td style="text-align: left">

        </td>
    </tr>

    <%--<!-- GET PROBLEM TXS TAB -->--%>
    <%--<tr>--%>
        <%--<td valign="center" align="center" class="tabForm">--%>
            <%--<table width="100%" border="0">--%>
                <%--<!-- Tab header -->--%>
                <%--<tr>--%>
                    <%--<td colspan="2">--%>
                        <%--<h4 class="dataLabel">--%>
                            <%--<bean:message bundle="core.labels" key="core.tools.infobuyvoucherform.purchase_query.title"/>--%>
                        <%--</h4>--%>
                    <%--</td>--%>
                <%--</tr>--%>
            <%--</table>--%>
        <%--</td>--%>
    <%--</tr>--%>


    <%--<!-- CHECK STATUSES TAB -->--%>
    <%--<tr>--%>
        <%--<td valign="center" align="center" class="tabForm">--%>
            <%--<table width="100%" border="0">--%>
                <%--<!-- Tab header -->--%>
                <%--<tr>--%>
                    <%--<td colspan="2">--%>
                        <%--<h4 class="dataLabel">--%>
                            <%--<bean:message bundle="core.labels" key="core.tools.infobuyvoucherform.purchase_status.title"/>--%>
                        <%--</h4>--%>
                    <%--</td>--%>
                <%--</tr>--%>

                <%--<tr>--%>
                    <%--<td class="dataLabel" width="15%">--%>
                        <%--<tmx_ctrl:writeEntityLabel attrName="transactionGUIDs"--%>
                                                   <%--entityClassName="com.tmx.as.entities.bill.transaction.ClientTransaction"/>:--%>
                    <%--</td>--%>
                    <%--<td class="dataField" width="150%">--%>
                        <%--<tmx_ctrl:editBox property="ebBillingTransactionNumbers"/>--%>
                    <%--</td>--%>

                <%--</tr>--%>
            <%--</table>--%>
        <%--</td>--%>
    <%--</tr>--%>
    <!-- BLOCKING GATES TAB -->
    <tr>
        <td valign="center" align="center" class="tabForm">
            <table width="100%" border="0">
                <!-- Tab header -->
                <tr>
                    <td colspan="2">
                        <h4 class="dataLabel">
                         <bean:message bundle="core.labels" key="core.tools.blockingGates"/>
                        </h4>
                    </td>
                </tr>

                <tr>
                    <td class="dataLabel">
                        <tmx_ctrl:writeEntityLabel attrName="gates"
                                                   entityClassName="com.tmx.as.entities.bill.transaction.ClientTransaction"/>:
                    </td>
                    <td class="dataField">
                        <tmx_ctrl:multipleSelectBox property="multipleSelectBoxMediumAccesses"/>
                    </td>
                    <td class="dataField">
                        <tmx_ctrl:button property="btnRemoveMediumAccesses"/>
                    </td>

                    <td class="dataLabel" >
                        <tmx_ctrl:writeEntityLabel attrName="gates"
                                                   entityClassName="com.tmx.as.entities.bill.transaction.ClientTransaction"/>:
                    </td>
                    <td class="dataField">
                        <tmx_ctrl:multipleSelectBox property="multipleSelectBoxRemovedElements"/>
                    </td>

                    <td class="dataField" width="150%">
                        <tmx_ctrl:button property="btnRestroreMediumAccesses"/>
                        <tmx_ctrl:button property="btnRestoreDefault"/>
                    </td>

                </tr>
            </table>
        </td>
    </tr>

    <tr>
        <td valign="center" align="center" class="tabForm">
            <table width="100%" border="0">

            </table>
        </td>
    </tr>

    <!-- BUTTONS -->
    <tr>
        <td style="text-align: left">
        </td>
    </tr>
</table>
</html:form>