package com.tmx.web.actions.core.dictionary.seller;

import com.tmx.as.blogic.SellerManager;
import com.tmx.as.blogic.AddressManager;
import com.tmx.as.blogic.BlogicException;
import com.tmx.as.blogic.restriction.SellerRestrictionManager;
import com.tmx.as.blogic.tariff.SellerTariffManager;
import com.tmx.as.entities.bill.seller.Seller;
import com.tmx.as.entities.bill.bank.BankAccount;
import com.tmx.as.entities.bill.tariff.SellerTariff;
import com.tmx.as.entities.bill.function.FunctionInstance;
import com.tmx.as.entities.bill.function.FunctionType;
import com.tmx.as.entities.bill.function.ActualFunctionParameter;
import com.tmx.as.entities.bill.restriction.SellerRestriction;
import com.tmx.as.entities.network.territory.Region;
import com.tmx.as.exceptions.DatabaseException;
import com.tmx.as.base.EntityManager;
import com.tmx.web.actions.core.AbstractSvbillAction;
import com.tmx.web.base.WebException;
import com.tmx.web.forms.core.dictionary.seller.InfoSellerForm;
import com.tmx.web.forms.core.base.AbstractFormFactory;
import com.tmx.web.controls.ScrollBox;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.util.*;

public class InfoSellerAction extends AbstractSvbillAction {

    private static final String FORWARD_MANAGE_SELLERS_ACTION = "manageSellersAction";
    private static final String FORWARD_INFO_SELLER_PAGE = "infoSellerPage";
    private static final String FORWARD_MANAGE_DELETE_ACTION = "manageDeleteAction";
    private FunctionInstance functionInstance;


    public ActionForward preloadForCreate(ActionMapping mapping,
                                          ActionForm form,
                                          HttpServletRequest request,
                                          HttpServletResponse response) throws Throwable {
        getInfoSellerForm(form).reset();
        AbstractFormFactory.getInstance(getLoggedInUser(request)).setUpForm(getInfoSellerForm(form));
        getInfoSellerForm(form).bindData(getLoggedInUser(request));
        getInfoSellerForm(form).getScrollBoxProcessings().setSelectedKey("3");
        getInfoSellerForm(form).getCheckBoxIsAttachRestriction().setSelected(true);
        return mapping.findForward(FORWARD_INFO_SELLER_PAGE);
    }

    public ActionForward preloadForUpdate(ActionMapping mapping,
                                          ActionForm form,
                                          HttpServletRequest request,
                                          HttpServletResponse response) throws Throwable {
        Seller seller = new SellerManager().getSeller( getInfoSellerForm(form).getSelectedSellerId() );
        getInfoSellerForm(form).reset();
        getInfoSellerForm(form).bindData( seller , getLoggedInUser(request));

        InfoSellerForm infoSellerForm = (InfoSellerForm)form;
        //for physical address
        updateScrollBoxesRegionCities(infoSellerForm.getScrollBoxDistrictPhys(),
                                infoSellerForm.getScrollBoxRegionPhys(),
                                infoSellerForm.getScrollBoxCityPhys());
        //for legal address
        updateScrollBoxesRegionCities(infoSellerForm.getScrollBoxDistrictLegal(),
                                infoSellerForm.getScrollBoxRegionLegal(),
                                infoSellerForm.getScrollBoxCityLegal());

        //in this moment, in seller info form can be only one BankAcount  
        List accounts = new SellerManager().getBankAcount(seller.getSellerId());
        if( accounts!=null && !accounts.isEmpty() ){
            getInfoSellerForm(form).bindData( (BankAccount) accounts.get(0), getLoggedInUser(request) );
            getInfoSellerForm(form).bindData(new SellerManager().getContract(seller.getSellerId()));
        }
        disableCheckBoxForExistingTariffs(form, seller);


        //------------------------------------------------
        return mapping.findForward(FORWARD_INFO_SELLER_PAGE);
    }
    /*
    Method looks for existent tariffs for seller and disables needed check boxes
     */
    private void disableCheckBoxForExistingTariffs(ActionForm form, Seller seller) throws DatabaseException, WebException {
        //create set of tariffed operators
        SellerTariffManager sellerTariffManager = new SellerTariffManager();
        List<SellerTariff> tariffs = sellerTariffManager.getSellerTarifs(seller.getSellerId());
        List<String> tariffedOperators =  new ArrayList<String>();
        for (SellerTariff tariff : tariffs) {
            if (tariff.getActive()) {//tariff must be active
                Set<ActualFunctionParameter> parameters = tariff.getFunctionInstance().getFunctionParameters();
                for (ActualFunctionParameter parameter: parameters) {
                    if (parameter.getName().equals("operator_code")) {
                        tariffedOperators.add(parameter.getValue());
                    }
                }
            }
        }
        //Disable check boxes for existent tariffs
        getInfoSellerForm(form).getCheckBoxIsAttachBeelineTariff().setReadonly(tariffedOperators.contains("Beeline"));
        getInfoSellerForm(form).getCheckBoxIsAttachLifeTariff().setReadonly(tariffedOperators.contains("Life"));
        getInfoSellerForm(form).getCheckBoxIsAttachKSTariff().setReadonly(tariffedOperators.contains("KyivstarPoP"));
        getInfoSellerForm(form).getCheckBoxIsAttachCdmaTariff().setReadonly(tariffedOperators.contains("CDMA"));
        getInfoSellerForm(form).getCheckBoxIsAttachMTSTariff().setReadonly(tariffedOperators.contains("UMCPoP"));
        getInfoSellerForm(form).getCheckBoxIsAttachInterTelecomTariff().setReadonly(tariffedOperators.contains("InterTelecom"));
    }

    public ActionForward save(ActionMapping mapping,
                              ActionForm form,
                              HttpServletRequest request,
                              HttpServletResponse response) throws Throwable {
        InfoSellerForm infoSellerForm = (InfoSellerForm) form;
        infoSellerForm.validate();
        SellerManager sellerManager = new SellerManager();

        Seller seller = sellerManager.saveSeller(getInfoSellerForm(form).populateBankAccount(), getInfoSellerForm(form).populateContract());

        attachBillingParameters(getInfoSellerForm(form), seller);

//        try {
//            if (!sellerManager.getSeller(infoSellerForm.getSelectedSellerId()).getParentSeller().getSellerId().equals(
//                    new Long(infoSellerForm.getScrollBoxParentSellers().getSelectedKey()))) {//if parent seller change
//                sellerManager.saveSeller(getInfoSellerForm(form).populateBankAccount(), getInfoSellerForm(form).populateContract(), new Long(infoSellerForm.getScrollBoxParentSellers().getSelectedKey()));
//            } else {
//                sellerManager.saveSeller(getInfoSellerForm(form).populateBankAccount(), getInfoSellerForm(form).populateContract());
//            }
//        } catch (NullPointerException e) {//if parent seller is not set
//            sellerManager.saveSeller(getInfoSellerForm(form).populateBankAccount(), getInfoSellerForm(form).populateContract());
//        }

        return mapping.findForward(FORWARD_MANAGE_SELLERS_ACTION);
    }

    private void attachBillingParameters(InfoSellerForm form, Seller seller) throws BlogicException, DatabaseException {
        SellerTariffManager sellerTariffManager = new SellerTariffManager();
        Double discountPercent;

        if (form.getCheckBoxIsAttachRestriction().isSelected()) {
            
            createSellerRestriction(seller, form.getEditBoxMinSellerBalance().getValue().trim());
        }

        if (!form.getCheckBoxIsAttachMTSTariff().isReadonly() && !form.getEditBoxMTSDiscountPercent().getValue().equals("")) {
            discountPercent = Double.parseDouble(form.getEditBoxMTSDiscountPercent().getValue().trim().replace(",", "."));
            discountPercent = 1 - discountPercent/100;

            createSellerTariff(sellerTariffManager, seller, "UMCPoP", discountPercent.toString());
        }
        if (!form.getCheckBoxIsAttachLifeTariff().isReadonly() && !form.getEditBoxLifeDiscountPercent().getValue().equals("")) {
            discountPercent = Double.parseDouble(form.getEditBoxLifeDiscountPercent().getValue().trim().replace(",", "."));
            discountPercent = 1 - discountPercent/100;

            createSellerTariff(sellerTariffManager, seller, "Life", discountPercent.toString());
        }
        if (!form.getCheckBoxIsAttachKSTariff().isReadonly() && !form.getEditBoxKSDiscountPercent().getValue().equals("")) {
            discountPercent = Double.parseDouble(form.getEditBoxKSDiscountPercent().getValue().trim().replace(",", "."));
            discountPercent = 1 - discountPercent/100;

            String sellerDelta = form.getEditBoxSellerDelta().getValue().trim();
            createSellerTariff(sellerTariffManager, seller, "KyivstarPoP", discountPercent.toString(), sellerDelta);
        }

        if (!form.getCheckBoxIsAttachBeelineTariff().isReadonly() && !form.getEditBoxBeelineDiscountPercent().getValue().equals("")) {
            discountPercent = Double.parseDouble(form.getEditBoxBeelineDiscountPercent().getValue().trim().replace(",", "."));
            discountPercent = 1 - discountPercent/100;

            createSellerTariff(sellerTariffManager, seller, "Beeline", discountPercent.toString());
        }

        if (!form.getCheckBoxIsAttachCdmaTariff().isReadonly() && !form.getEditBoxCdmaDiscountPercent().getValue().equals("")) {
            discountPercent = Double.parseDouble(form.getEditBoxCdmaDiscountPercent().getValue().trim().replace(",", "."));
            discountPercent = 1 - discountPercent/100;

            createSellerTariff(sellerTariffManager, seller, "CDMA", discountPercent.toString());
        }

        if (!form.getCheckBoxIsAttachInterTelecomTariff().isReadonly() && !form.getEditBoxInterTelecomDiscountPercent().getValue().equals("")) {
            discountPercent = Double.parseDouble(form.getEditBoxInterTelecomDiscountPercent().getValue().trim().replace(",", "."));
            discountPercent = 1 - discountPercent/100;

            createSellerTariff(sellerTariffManager, seller, "InterTelecom", discountPercent.toString());
        }
         
    }
                                                                 
    private void createSellerRestriction(Seller seller,
                                         String minSellerBalance) throws DatabaseException, BlogicException {
        if (minSellerBalance.equals("")) {
            minSellerBalance = "0";
        }
        SellerRestriction restriction = new SellerRestriction();

        restriction.setName(seller.getCode() + "_restriction");
        restriction.setSeller(seller);
        restriction.setSellerId(seller.getSellerId());
        restriction.setFunctionInstance(getFunctionInstance(minSellerBalance));

        (new SellerRestrictionManager()).saveRestriction(restriction);
    }
    //Get instance of seller restriction function
    public FunctionInstance getFunctionInstance(String minSellerBalance) throws DatabaseException {
        FunctionInstance fi = new FunctionInstance();
        FunctionType functionType = getFunctionType(6l);

        HashSet<ActualFunctionParameter> actualFunctionParameters = new HashSet<ActualFunctionParameter>();

        ActualFunctionParameter parameter = new ActualFunctionParameter();
        parameter.setName("min_limit");
        parameter.setValue(minSellerBalance);
        parameter.setMandatory(true);
        parameter.setOrderNum(1l);
        actualFunctionParameters.add(parameter);

        parameter = new ActualFunctionParameter();
        parameter.setName("max_limit");
        parameter.setValue("100000");
        parameter.setMandatory(true);
        parameter.setOrderNum(2l);
        actualFunctionParameters.add(parameter);

        fi.setFunctionType(functionType);
        fi.setFunctionParameters(actualFunctionParameters);


        return fi;
    }


    private void createSellerTariff(SellerTariffManager sellerTariffManager,
                                                        Seller seller,
                                                        String operatorString,
                                                        String discountPercent) throws BlogicException, DatabaseException {
        createSellerTariff(sellerTariffManager, seller, operatorString, discountPercent, null);
    }

    private void createSellerTariff(SellerTariffManager sellerTariffManager, Seller seller,
                                            String operatorString, String discountPercent,
                                            String sellerDelta) throws BlogicException, DatabaseException {
        SellerTariff tariff = new SellerTariff();

        tariff.setName(seller.getCode() + "_" + operatorString + "_tariff");
        //TODO: in future, set date as next day, and close current tariff
        GregorianCalendar calendar = new GregorianCalendar();
        calendar.set(Calendar.HOUR, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        tariff.setActivationDate(calendar.getTime());

        tariff.setFunctionInstance(getFunctionInstance(operatorString, discountPercent, sellerDelta));
        tariff.setSeller(seller);


        sellerTariffManager.saveTariff(tariff);
    }


    public FunctionInstance getFunctionInstance(String operatorCode, String discountPercent,
                                                String sellerDelta) throws DatabaseException {

        FunctionType functionType;
        FunctionInstance functionInstance = new FunctionInstance();

        if (sellerDelta == null) {
            functionType = getFunctionType(9l);//SellerOperatorTariff
            functionInstance.setFunctionParameters(getParametersSet(operatorCode, discountPercent));
        } else {
            functionType = getFunctionType(30l);//GradiatedRefillTariff
            functionInstance.setFunctionParameters(getParametersSet(operatorCode, discountPercent, sellerDelta));
        }

        functionInstance.setFunctionType(functionType);

        functionInstance.setValid(1);
        return functionInstance;
    }

    private FunctionType getFunctionType(Long functionTypeId) throws DatabaseException {
        FunctionType functionType = (FunctionType)(new EntityManager()).RETRIEVE(FunctionType.class, functionTypeId);
        functionType.setName(functionType.getName());
        return functionType;
    }

    private HashSet getParametersSet(String operaorCode, String discountPercent, String sellerDelta) {
        HashSet<ActualFunctionParameter> parameters = new HashSet<ActualFunctionParameter>();

        ActualFunctionParameter parameter = new ActualFunctionParameter();
        parameter.setName("operator_code");
        parameter.setValue(operaorCode);
        parameter.setMandatory(true);
        parameter.setOrderNum(1l);
        parameters.add(parameter);

        parameter = new ActualFunctionParameter();
        parameter.setName("discount_percent_op");
        parameter.setValue(discountPercent);
        parameter.setMandatory(true);
        parameter.setOrderNum(2l);
        parameters.add(parameter);

        parameter = new ActualFunctionParameter();
        parameter.setName("tx_cost_op");
        parameter.setValue("0");
        parameter.setMandatory(true);
        parameter.setOrderNum(3l);
        parameters.add(parameter);

        parameter = new ActualFunctionParameter();
        parameter.setName("seller_delta");
        parameter.setValue(sellerDelta);
        parameter.setMandatory(true);
        parameter.setOrderNum(4l);
        parameters.add(parameter);

        parameter = new ActualFunctionParameter();
        parameter.setName("operator_limit");
        parameter.setValue("19.99");
        parameter.setMandatory(true);
        parameter.setOrderNum(5l);
        parameters.add(parameter);

        parameter = new ActualFunctionParameter();
        parameter.setName("fee");
        parameter.setValue("1");
        parameter.setMandatory(true);
        parameter.setOrderNum(6l);
        parameters.add(parameter);

        return parameters;

    }

    private HashSet getParametersSet(String operaorCode, String discountPercent) {
        HashSet<ActualFunctionParameter> parameters = new HashSet<ActualFunctionParameter>();

        ActualFunctionParameter parameter = new ActualFunctionParameter();
        parameter.setName("operator_code");
        parameter.setValue(operaorCode);
        parameter.setMandatory(true);
        parameter.setOrderNum(1l);
        parameters.add(parameter);

        parameter = new ActualFunctionParameter();
        parameter.setName("discount_percent_op");
        parameter.setValue(discountPercent);
        parameter.setMandatory(true);
        parameter.setOrderNum(2l);
        parameters.add(parameter);

        parameter = new ActualFunctionParameter();
        parameter.setName("tx_cost_op");
        parameter.setValue("0");
        parameter.setMandatory(true);
        parameter.setOrderNum(3l);
        parameters.add(parameter);

        return parameters;
    }

    public ActionForward apply(ActionMapping mapping,
                              ActionForm form,
                              HttpServletRequest request,
                              HttpServletResponse response) throws Throwable {
        save(mapping, form, request, response);
        return mapping.findForward(FORWARD_INFO_SELLER_PAGE);
    }

    public ActionForward preloadForDelete(ActionMapping mapping,
                                          ActionForm form,
                                          HttpServletRequest request,
                                          HttpServletResponse response) throws Throwable {
        getRoadmap().collectParameter("/core/message/confirmation", "leftMenuName", "dictionaries");
        getRoadmap().setPageForward("confirmation","onOk","local:"+this.getName()+"#"+FORWARD_MANAGE_DELETE_ACTION);
        getRoadmap().setPageForward("confirmation","onCancel","local:"+this.getName()+"#"+ FORWARD_MANAGE_SELLERS_ACTION);
        return mapping.findForward("confirmation");
    }

    public ActionForward delete(ActionMapping mapping,
                              ActionForm form,
                              HttpServletRequest request,
                              HttpServletResponse response) throws Throwable {
        new SellerManager().deleteSeller( getInfoSellerForm(form).getSelectedSellerId() );
        return mapping.findForward(FORWARD_MANAGE_SELLERS_ACTION);
    }

    public ActionForward defaultAction(ActionMapping mapping,
                                       ActionForm form,
                                       HttpServletRequest request,
                                       HttpServletResponse response,
                                       String methodName) throws Throwable {
        return preloadForCreate(mapping, form, request, response);
    }

    public ActionForward updateRegionScrollBoxePhys(ActionMapping mapping,
                                       ActionForm form,
                                       HttpServletRequest request,
                                       HttpServletResponse response) throws Throwable {
        InfoSellerForm infoSellerForm = (InfoSellerForm)form;
        infoSellerForm.validate(mapping.findForward(FORWARD_MANAGE_SELLERS_ACTION));

        updateScrollBoxesRegionCities(infoSellerForm.getScrollBoxDistrictPhys(),
                                infoSellerForm.getScrollBoxRegionPhys(),
                                infoSellerForm.getScrollBoxCityPhys());
        return mapping.findForward(FORWARD_INFO_SELLER_PAGE);
    }

    public ActionForward updateRegionScrollBoxeLegal(ActionMapping mapping,
                                       ActionForm form,
                                       HttpServletRequest request,
                                       HttpServletResponse response) throws Throwable {
        InfoSellerForm infoSellerForm = (InfoSellerForm)form;
        infoSellerForm.validate(mapping.findForward(FORWARD_MANAGE_SELLERS_ACTION));

        updateScrollBoxesRegionCities(infoSellerForm.getScrollBoxDistrictLegal(),
                                infoSellerForm.getScrollBoxRegionLegal(),
                                infoSellerForm.getScrollBoxCityLegal());
        
        return mapping.findForward(FORWARD_INFO_SELLER_PAGE);
    }

    private void updateScrollBoxesRegionCities(ScrollBox scrollBoxDistrict, ScrollBox scrollBoxRegion, ScrollBox scrollBoxCity)
                                                throws DatabaseException {
        List regions = Collections.EMPTY_LIST;
        List cities = Collections.EMPTY_LIST;
        if(!scrollBoxDistrict.isNullKeySelected()) {
            regions = new AddressManager().getRegionsByDistrict(
                    new Long(scrollBoxDistrict.getSelectedKey()));
        }
        if (!scrollBoxRegion.isNullKeySelected() && regions.size() > 0) {
            cities = new AddressManager().
                    getCitiesByRegion( ((Region)regions.get(0)).getRegionId() );
        }
        scrollBoxRegion.bind(regions);
        scrollBoxCity.bind(cities);
    }

    public ActionForward updateScrollBoxCityLegal(ActionMapping mapping,
                                       ActionForm form,
                                       HttpServletRequest request,
                                       HttpServletResponse response) throws Throwable {
        InfoSellerForm infoSellerForm = (InfoSellerForm)form;
        infoSellerForm.validate(mapping.findForward(FORWARD_MANAGE_SELLERS_ACTION));

        List cities = Collections.EMPTY_LIST;
        if(!infoSellerForm.getScrollBoxRegionLegal().isNullKeySelected()) {
            cities = new AddressManager().getCitiesByRegion(
                    new Long(infoSellerForm.getScrollBoxRegionLegal().getSelectedKey()));
        }
        infoSellerForm.getScrollBoxCityLegal().bind(cities);
        return mapping.findForward(FORWARD_INFO_SELLER_PAGE);
    }

    public ActionForward updateScrollBoxCityPhys(ActionMapping mapping,
                                       ActionForm form,
                                       HttpServletRequest request,
                                       HttpServletResponse response) throws Throwable {
        InfoSellerForm infoSellerForm = (InfoSellerForm)form;
        infoSellerForm.validate(mapping.findForward(FORWARD_MANAGE_SELLERS_ACTION));

        List cities = Collections.EMPTY_LIST;
        if(!infoSellerForm.getScrollBoxRegionPhys().isNullKeySelected()) {
            cities = new AddressManager().getCitiesByRegion(
                    new Long(infoSellerForm.getScrollBoxRegionPhys().getSelectedKey()));
        }
        infoSellerForm.getScrollBoxCityPhys().bind(cities);
        return mapping.findForward(FORWARD_INFO_SELLER_PAGE);
    }
    
    // PRIVATE METHODS-----------------------------------------------------------------

    private InfoSellerForm getInfoSellerForm(ActionForm form) throws WebException {
        return (InfoSellerForm) verifyForm(form, InfoSellerForm.class);           
    }


}
