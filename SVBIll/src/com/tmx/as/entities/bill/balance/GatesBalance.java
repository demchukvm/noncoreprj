package com.tmx.as.entities.bill.balance;

import com.tmx.as.base.BasicEntity;
import java.io.Serializable;

/**
 * Created by IntelliJ IDEA.
 * User: Proximan
 * Date: 05.01.2011
 * Time: 9:56:46
 * To change this template use File | Settings | File Templates.
 */
public class GatesBalance extends BasicEntity
{
    private Long gateId;
    private String gateName;
    private String balance;

    public Long getGateId() {
        return gateId;
    }

    public void setGateId(Long gateId) {
        this.gateId = gateId;
    }

    public String getGateName() {
        return gateName;
    }

    public void setGateName(String gateName) {
        this.gateName = gateName;
    }

    public String getBalance() {
        return balance;
    }

    public void setBalance(String balance) {
        this.balance = balance;
    }

    public Serializable getId() {
        return gateId;
    }

    public int getValid() {
        return valid;
    }

    public void setValid(int valid) {
        this.valid = valid;
    }
}