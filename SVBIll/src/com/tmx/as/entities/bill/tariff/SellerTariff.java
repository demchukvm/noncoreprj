package com.tmx.as.entities.bill.tariff;

import com.tmx.as.entities.bill.seller.Seller;
import com.tmx.as.entities.bill.balance.SellerBalance;
import com.tmx.as.entities.bill.function.FunctionInstance;

/**

 */
public class SellerTariff extends AbstractTariff {
    private Seller seller;
    private Long sellerId;
    private Long sellerBalanceId;
    private SellerBalance sellerBalance;

    //--------Properties
    public Seller getSeller() {
        return seller;
    }

    public void setSeller(Seller seller) {
        this.seller = seller;
    }

    public Long getSellerId() {
        return sellerId;
    }

    public void setSellerId(Long sellerId) {
        this.sellerId = sellerId;
    }

    public Long getSellerBalanceId() {
        return sellerBalanceId;
    }

    public void setSellerBalanceId(Long sellerBalanceId) {
        this.sellerBalanceId = sellerBalanceId;
    }

    public SellerBalance getSellerBalance() {
        return sellerBalance;
    }

    public void setSellerBalance(SellerBalance sellerBalance) {
        this.sellerBalance = sellerBalance;
    }

    public Object clone() throws CloneNotSupportedException {
        SellerTariff clone = (SellerTariff)super.clone();
        clone.setFunctionInstance((FunctionInstance) clone.getFunctionInstance().clone());
        clone.setTariffId(null);//new clone is transient
        return clone;
    }

}
