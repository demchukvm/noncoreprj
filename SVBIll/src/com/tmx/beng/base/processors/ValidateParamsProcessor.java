package com.tmx.beng.base.processors;

import com.tmx.as.base.EntityManager;
import com.tmx.as.entities.bill.function.ActualFunctionParameter;
import com.tmx.as.entities.bill.function.FunctionType;
import com.tmx.as.exceptions.DatabaseException;
import com.tmx.beng.base.*;
import com.tmx.beng.base.cache.DBCache;
import com.tmx.beng.base.cache.FunctionWrapper;
import com.tmx.beng.base.cache.InvokeFunctionException;
import com.tmx.beng.base.dbfunctions.ValidationError;
import com.tmx.beng.base.dbfunctions.BasicTariffFunction;
import com.tmx.util.InitException;

import java.util.*;


public class ValidateParamsProcessor extends TaskProcessor{

    protected Task process0(Task task, DBCache dbCache, BillingMessageWritable msg) throws BillException {
        BillingMessageWritable respMsg = new BillingMessageImpl();
        respMsg.setAttribute(BillingMessage.STATUS_CODE, 0);
        Long functionTypeId = (Long) msg.getAttribute(BillingMessage.FUNCTION_TYPE_ID);
        List params = (List) msg.getAttribute(BillingMessage.ACTUAL_PARAMS_LIST);
        try {
			FunctionType type = (FunctionType) new EntityManager().RETRIEVE(FunctionType.class, functionTypeId);

            if (type == null)
                throw new BillException("err.retrieve_null_object.", new String[] {functionTypeId.toString()});

            FunctionWrapper wrapper = new FunctionWrapper(type);
            ((BasicTariffFunction)wrapper.getObjectInstance()).init(task);
            HashMap map = new HashMap();
            for (Object param : params) {
                ActualFunctionParameter parameter = (ActualFunctionParameter) param;
                map.put(parameter.getName(), parameter.getValue());
            }
            ValidationError[] errors = wrapper.validateFunctionParams(map);
            
            if (errors == null || errors.length == 0)
                respMsg.setAttribute(BillingMessage.ERROR_LIST, new ArrayList());
            else
                respMsg.setAttribute(BillingMessage.ERROR_LIST, new ArrayList(Arrays.asList(errors)));

            CallBack callBack = task.getCallBack();
	        if(callBack != null)
	            callBack.sendBilledMessage(respMsg);
		} catch (DatabaseException e) {
			throw new BillException("err.load_function_type.error", e);
		} catch (InitException e) {
			throw new BillException("err.create_function_wrapper.error", e);
		} catch (InvokeFunctionException e) {
			throw new BillException("err.invoke_validate_params_method.error", e);
		}
        return task;
    }

}
