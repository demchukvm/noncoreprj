package com.tmx.web.controls.table;

/**
 * This interface used by Filter.
 * Implement this inteface by controls to manage filter behaviour
 */
public interface FilterEnabler {

   /**
    * @return true, if filter is enabled
    * @see com.tmx.web.controls.table.Filter
    */
    public boolean isFilterEnabled();

}
