package com.tmx.web.base.menuservice;

import com.tmx.util.StructurizedException;
import java.util.Locale;

/**
 */
public class MenuException extends StructurizedException {

    public MenuException(String errKey){
        super(errKey);
    }

    public MenuException(String errKey, Locale locale){
        super(errKey, locale);
    }

    public MenuException(String errKey, String[] params){
        super(errKey, params);
    }

    public MenuException(String errKey, String[] params, Throwable cause){
        super(errKey, params, cause);
    }

    public MenuException(String errKey, String[] params, Locale locale){
        super(errKey, params, locale);
    }

    public MenuException(String errKey, Throwable cause){
        super(errKey, cause);
    }
}
