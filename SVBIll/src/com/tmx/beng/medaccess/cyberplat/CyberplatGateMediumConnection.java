package com.tmx.beng.medaccess.cyberplat;

import com.tmx.beng.base.BillingMessage;
import com.tmx.beng.httpsgate.HttpsGateException;
import com.tmx.beng.httpsgate.cyberplat.CyberplatGate;
import com.tmx.beng.medaccess.MediumConnection;

public class CyberplatGateMediumConnection implements MediumConnection {
    private CyberplatGate cyberplatGate = null;

    CyberplatGateMediumConnection() throws HttpsGateException {
        cyberplatGate = CyberplatGate.getInstance();
    }

    public void send(BillingMessage msg) {
        cyberplatGate.enqueueBillMessage(msg);
    }
}