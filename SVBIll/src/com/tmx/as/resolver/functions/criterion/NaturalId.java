package com.tmx.as.resolver.functions.criterion;

import com.tmx.as.resolver.functions.Function;
import com.tmx.as.resolver.FunctionSyntaxException;
import com.tmx.as.base.criterion.Restrictions;

public class NaturalId implements Function {
    public void set(int paramIndex, String paramName, Object paramValue) throws FunctionSyntaxException {
    }
    public Object returnValue() throws FunctionSyntaxException {
        return Restrictions.naturalId();
    }
}
