<%@ page language="java" pageEncoding="UTF-8" contentType="text/html; charset=utf-8" isELIgnored="false" %>
<!--errorPage="/core/jsp/message/jsperror.jsp"-->
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean" %>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c" %>
<%@ taglib uri="http://tmx.com/taglib/tmx-logic" prefix="tmx_logic" %>
<%@ taglib uri="http://tmx.com/taglib/tmx-controls" prefix="tmx_ctrl" %>

<table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0" class="layouttable">
    <tr class="noBorder">
        <td id="header" align="center" nowrap="nowrap">
            <c:if test="${requestScope['tile.headerAndFooterLayout.header'] != null}">
                <tiles:insert page="${requestScope['tile.headerAndFooterLayout.header']}" ignore="true"/>
            </c:if>
        </td>
    </tr>
    <tr class="noBorder">
        <td id="center" height="100%" valign="middle" align="center">
            <c:if test="${requestScope['tile.headerAndFooterLayout.content'] != null}">
                <tiles:insert page="${requestScope['tile.headerAndFooterLayout.content']}" ignore="true"/>
            </c:if>
        </td>
    </tr>
	<%--
    <tr class="noBorder">
        <td id="footer" bgcolor="red"> &nbsp;
            <c:if test="${requestScope['tile.headerAndFooterLayout.footer'] != null}">
                <tiles:insert page="${requestScope['tile.headerAndFooterLayout.footer']}" ignore="true"/>
            </c:if>
        </td>
    </tr>
	--%>
</table>