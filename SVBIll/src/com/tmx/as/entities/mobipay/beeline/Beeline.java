package com.tmx.as.entities.mobipay.beeline;

import com.tmx.as.base.BasicEntity;
import com.tmx.as.entities.mobipay.report.Report;

import java.util.Date;
import java.io.Serializable;

/**
 */
public class Beeline extends BasicEntity {
    private Long beelineId;
    private String msisdn;
    private Double amount;
    private Long payId;
    private Integer receiptNum;
    private Date commitDate;
    private Integer currency;
    private String bankPayId;
    private Integer statusCode;
    private String statusMessage;
    private String tradePoint;
    private Report report;
    private String branch;

    public String getBranch() {
        return branch;
    }

    public void setBranch(String branch) {
        this.branch = branch;
    }

    public Report getReport() {
        return report;
    }

    public void setReport(Report report) {
        this.report = report;
    }

    public String getTradePoint() {
        return tradePoint;
    }

    public void setTradePoint(String tradePoint) {
        this.tradePoint = tradePoint;
    }

    public Integer getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }

    public void setStatusCodeInt(int statusCode) {
        this.statusCode = new Integer(statusCode);
    }

    public String getStatusMessage() {
        return statusMessage;
    }

    public void setStatusMessage(String statusMessage) {
        if(statusMessage != null && statusMessage.length() > 255){
            statusMessage = statusMessage.substring(0, 255);
        }
        this.statusMessage = statusMessage;
    }

    public String getMsisdn() {
        return msisdn;
    }

    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }

    public Integer getCurrency() {
        return currency;
    }

    public void setCurrency(Integer currency) {
        this.currency = currency;
    }


    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Long getPayId() {
        return payId;
    }

    public void setPayId(Long payId) {
        this.payId = payId;
    }

    public Integer getReceiptNum() {
        return receiptNum;
    }

    public void setReceiptNum(Integer receiptNum) {
        this.receiptNum = receiptNum;
    }

    public Date getCommitDate() {
        return commitDate;
    }

    public void setCommitDate(Date commitDate) {
        this.commitDate = commitDate;
    }

    public String getBankPayId() {
        return bankPayId;
    }

    public void setBankPayId(String bankPayId) {
        this.bankPayId = bankPayId;
    }


    public Long getBeelineId() {
        return beelineId;
    }

    public void setBeelineId(Long beelineId) {
        this.beelineId = beelineId;
    }

    public Serializable getId() {
        return getBeelineId();
    }

    public int getValid() {
        return valid;
    }

    public void setValid(int valid) {
        this.valid = valid;
    }
}
