package com.tmx.as.base.jdbc;

import java.sql.Array;
import java.sql.Connection;
import java.sql.SQLException;

/**
    JdbcAdapter Service Provider Interface.

    Extend this interface if you need to support parameter adaptation
    of specific types in specific way by underlie jdbc driver capabilites. 

 */
public interface JdbcAdapterSpi {
    public Array adoptArray(Object[] array, String typeName, Connection jdbcConn) throws SQLException;
}
