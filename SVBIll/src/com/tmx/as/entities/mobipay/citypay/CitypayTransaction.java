package com.tmx.as.entities.mobipay.citypay;

import com.tmx.as.base.BasicEntity;
import java.io.Serializable;
import java.util.Date;

public class CitypayTransaction extends BasicEntity
{
    private Long citypayTransactionId;
    private Date transactionTime;
    private String citypayTransactionNum;
    private String billTransactionNum;
    private String billTransactionNumINT;
    private String statusCodeByCitypay;
    private String statusMessageByCitypay;
    //private String statusCodeForBilling;
    //private String statusMessageForBilling;
    private String accountPre;
    private String account;
    private Integer amount;
    private String terminal;
    private String serviceCodeByCitypay;
    private String serviceNameByBill;
    private String gateName;
    private boolean isLast;

    public Long getCitypayTransactionId() {
        return citypayTransactionId;
    }

    public void setCitypayTransactionId(Long citypayTransactionId) {
        this.citypayTransactionId = citypayTransactionId;
    }

    public Date getTransactionTime() {
        return transactionTime;
    }

    public void setTransactionTime(Date transactionTime) {
        this.transactionTime = transactionTime;
    }

    public String getCitypayTransactionNum() {
        return citypayTransactionNum;
    }

    public void setCitypayTransactionNum(String citypayTransactionNum) {
        this.citypayTransactionNum = citypayTransactionNum;
    }

    public String getBillTransactionNum() {
        return billTransactionNum;
    }

    public void setBillTransactionNum(String billTransactionNum) {
        this.billTransactionNum = billTransactionNum;
    }

    public String getStatusCodeByCitypay() {
        return statusCodeByCitypay;
    }

    public void setStatusCodeByCitypay(String statusCodeByCitypay) {
        this.statusCodeByCitypay = statusCodeByCitypay;
    }

    public String getStatusMessageByCitypay() {
        return statusMessageByCitypay;
    }

    public void setStatusMessageByCitypay(String statusMessageByCitypay) {
        this.statusMessageByCitypay = statusMessageByCitypay;
    }

    /*public String getStatusCodeForBilling() {
        return statusCodeForBilling;
    }

    public void setStatusCodeForBilling(String statusCodeForBilling) {
        this.statusCodeForBilling = statusCodeForBilling;
    }

    public String getStatusMessageForBilling() {
        return statusMessageForBilling;
    }

    public void setStatusMessageForBilling(String statusMessageForBilling) {
        this.statusMessageForBilling = statusMessageForBilling;
    }*/

    public String getAccountPre() {
        return accountPre;
    }

    public void setAccountPre(String accountPre) {
        this.accountPre = accountPre;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public String getTerminal() {
        return terminal;
    }

    public void setTerminal(String terminal) {
        this.terminal = terminal;
    }

    public String getServiceCodeByCitypay() {
        return serviceCodeByCitypay;
    }

    public void setServiceCodeByCitypay(String serviceCodeByCitypay) {
        this.serviceCodeByCitypay = serviceCodeByCitypay;
    }

    public String getServiceNameByBill() {
        return serviceNameByBill;
    }

    public void setServiceNameByBill(String serviceNameByBill) {
        this.serviceNameByBill = serviceNameByBill;
    }

    public String getGateName() {
        return gateName;
    }

    public void setGateName(String gateName) {
        this.gateName = gateName;
    }

    public Serializable getId() {
        return getCitypayTransactionId();
    }

    public int getValid() {
        return valid;
    }

    public void setValid(int valid) {
        this.valid = valid;
    }

    public String getBillTransactionNumINT() {
        return billTransactionNumINT;
    }

    public void setBillTransactionNumINT(String billTransactionNumINT) {
        this.billTransactionNumINT = billTransactionNumINT;
    }

    public boolean getIsLast() {
        return isLast;
    }

    public void setIsLast(boolean isLast) {
        this.isLast = isLast;
    }
}
