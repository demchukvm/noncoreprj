package com.tmx.beng.base.channels;

import com.tmx.beng.base.BillingMessage;
import com.tmx.beng.base.BillException;

import java.util.List;

public class ClientTransactionMaskChannel extends AbstractChannel{
    private static final String DEFAULT_CHANNEL_ID = "defaultChannelId";

    public AbstractChannel.OutChannel process(BillingMessage msg) throws BillException {
        final int outChannelId = resolveOutChannelId(msg.getAttributeString(BillingMessage.CLI_TRANSACTION_NUM));
        AbstractChannel.OutChannel outChannel = getOutChannel(outChannelId);
        if(outChannel == null)
            throw new BillException("err.client_transaction_mask_channel.no_channel_found_with_id", new String[]{String.valueOf(outChannelId)});
        return outChannel;
    }

    private int resolveOutChannelId(String cliTransactionNum) throws BillException{
        String[] outChannelsIds = getParamListsNames();
        for (String outChannelId : outChannelsIds) {
            List cliTransNumTemplateList = getParamListValues(outChannelId);
            for (Object cliTransNumTemplate : cliTransNumTemplateList) {
                String cliTransNumPrefix = (String) cliTransNumTemplate;
                if (isCliTransNumTemplateMatch(cliTransNumPrefix, cliTransactionNum))
                    return Integer.parseInt(outChannelId);
            }
        }
        //return default channel id
        /*
        String defaultChannelId = getParamValue(DEFAULT_CHANNEL_ID);
        if(defaultChannelId == null)
            throw new BillException("err.client_transaction_mask_channel.no_default_channel_id_defined_in_config");
        try{
            return Integer.parseInt(defaultChannelId);
        }
        catch(NumberFormatException e){
            throw new BillException("err.client_transaction_mask_channel.failed_to_parse_default_channel_id", new String[]{defaultChannelId});
        }
        */
        return 0;
    }

    private boolean isCliTransNumTemplateMatch(String cliTransNumTemplate, String cliTransNum){
        return cliTransNum.startsWith(cliTransNumTemplate);
    }
}
