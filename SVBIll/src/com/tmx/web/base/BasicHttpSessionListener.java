package com.tmx.web.base;

import javax.servlet.http.HttpSessionListener;
import javax.servlet.http.HttpSessionEvent;

/**
   Basic implementation of session listener
 */
public class BasicHttpSessionListener implements HttpSessionListener {

    public void sessionCreated(HttpSessionEvent httpSessionEvent) {
        //do nothing
    }

    public void sessionDestroyed(HttpSessionEvent httpSessionEvent) {
        RequestEnvironment.releaseReqContext();
    }
}
