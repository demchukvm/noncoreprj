<%@ page language="java" pageEncoding="UTF-8" contentType="text/html; charset=utf-8"
         errorPage="/core/jsp/message/jsperror.jsp" isELIgnored="false" %>
<%@ taglib uri="/WEB-INF/tld/struts-nested.tld" prefix="nested" %>
<%@ taglib uri="/WEB-INF/tld/tmx-controls.tld" prefix="tmx_ctrl" %>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ page import="com.tmx.web.forms.core.tools.RefillForm" %>


<html:form action="/core/tools/refill">


<table border="0" width="100%">
    <tr>

        <td align="right" valign="top" width="40%">
            !!!
            <bean:message bundle="core.OpErrors" key="error1"/>
            <table style="background-color:#f6f6f6;border: solid 1px #ccc;">
                <tr>
                   <td colspan="2">
                       <select name="refillType" style="font-size:12px;">
                           <option value="msisdn" >По номеру телефона</option>
                           <option value="payAccount">По номеру лиц.счета</option> 
                       </select>
                       <br>
                       <span style="color:#ff0000;font-size:10px;">* номера Киевстар, МТС</span>
                   </td>
                </tr>
                <tr>
                    <td width="60px">Номер: 38</td>
                    <td><input size="20" name="refillNum" type="text" value="<%= ((RefillForm) pageContext.getSession().getAttribute("core.tools.RefillForm")).getRefillNum() %>"></td>
                </tr>
                <tr>
                    <td>Сумма:</td>
                    <td><input size="20" name="refillSum" type="text"  value="<%= ((RefillForm) pageContext.getSession().getAttribute("core.tools.RefillForm")).getRefillSum() %>"></td>
                </tr>
                <tr>
                    <td colspan="2" align="center">
                        <tmx_ctrl:button property="btnRefill"/>
                        <tmx_ctrl:button property="btnClear"/>
                        <%--<input type="button" title="Пополнить" />--%>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" align="center">
                       <%-- <input name="responseMessage" readonly="readonly" type="text" size="50" style="font-size: 11px;" value="<%= ((RefillForm) pageContext.getSession().getAttribute("core.tools.RefillForm")).getResponseMessage() %>"> --%>
                        <span style="color:#008000; font-size: 11px;"><%= ((RefillForm) pageContext.getSession().getAttribute("core.tools.RefillForm")).getResponseMessage() %></span>
                        <%--
                        <textarea rows="1" cols="40" name="xmlDocSerialized" readonly="readonly">
                           <%= ((RefillForm) pageContext.getSession().getAttribute("core.tools.RefillForm")).getResponseMessage().trim() %>
                        </textarea>
                        --%>
                    </td>
                </tr>
            </table>
        </td>


        <td valign="top" align="left">

            <div onClick="MyFunc('ram1')" style="background-color:#f6f6f6;border: solid 1px #ccc; width: 500px;text-align:center;">
                <span style="font-size:15px; color:#008000;">Киевстар</span>
            </div>
            <div id="ram1" style="display:none;border: solid 1px #ccc;width: 500px;">
                <%--<%= ((RefillForm) pageContext.getSession().getAttribute("core.tools.RefillForm")).responseKS %> --%>
                <table>
                <%
                    String [][] responseKS = ((RefillForm) pageContext.getSession().getAttribute("core.tools.RefillForm")).responseKS;

                    for(int i=0; i<responseKS.length; i++)
                    {
                        String strA = responseKS[i][0];
                        String strB = responseKS[i][1];
                        byte[] utf8Bytes = strB.getBytes("UTF8");
                        String KS = new String(utf8Bytes,"UTF8");
                %>
                         <%= "<tr><td align=\"center\" width=\"40\">"+strA+"</td><td>"+KS+"</td></tr>"%>
                <%
                    }

                %>
                
                </table>
            </div>

            <div onClick="MyFunc('ram2')" style="background-color:#f6f6f6;border: solid 1px #ccc; width: 500px;text-align:center;">
                <span style="font-size:15px; color:#008000;">МТС</span>
            </div>
            <div id="ram2" style="display:none;border: solid 1px #ccc;width: 500px;">
                <%--<%= ((RefillForm) pageContext.getSession().getAttribute("core.tools.RefillForm")).responseMTS %>--%>

                    

                <%--
                    int val = Integer.parseInt((String)getAttribute("Processing.errKS.count"));

                    for(int i=0; i<val; i++)
                    {
                        String str = "errKS"+i;
                %>
                    <tmx_ctrl:writeEntityLabel entityClassName="com.tmx.as.entities.bill.processing.Processing" attrName="<%=str%>"></tmx_ctrl:writeEntityLabel>
                <%
                    }
                --%>
            </div>

        </td>
    </tr>
</table>
</html:form>