package com.tmx.beng.httpsgate.citypay;

import com.tmx.beng.httpsgate.HttpsGateException;
import com.tmx.util.Configuration;
import com.tmx.util.InitException;
import org.apache.commons.httpclient.contrib.ssl.AuthSSLProtocolSocketFactory;
import org.apache.commons.httpclient.protocol.Protocol;
import org.apache.commons.httpclient.protocol.ProtocolSocketFactory;
import org.apache.log4j.Logger;

import java.net.URL;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CitypayGate extends BasicCitypayGate
{
    protected static final String GATE_NAME = "citypay_gate";
    protected static Logger logger = Logger.getLogger("citypay_gate." + GATE_NAME);
    private static CitypayGate citypayGate = null;

    private static final String PROP_KEYSTORE_PATH = "keystore.path";
    private static final String PROP_TRUSTSTORE_PATH = "truststore.path";
    private static final String PROP_KEYSTORE_PASSWORD = "keystore.password";
    private static final String PROP_TRUSTSTORE_PASSWORD = "truststore.password";
    private static final String PROP_PORT = "port";


    public static CitypayGate getInstance() throws HttpsGateException {
        if(citypayGate == null){
            logger.error("Attempt to obtain not inited CitypayGate");
            throw new HttpsGateException("err.CitypayGate.null_instance");
        }
        return citypayGate;
    }

    public static void init()throws InitException {
        logger.info("Start CitypayGate initialization");
        citypayGate = new CitypayGate();
        citypayGate.init0();

        initSSL(citypayGate);

        logger.info("CitypayGate initialized");
    }

    public static void shutdown(final long timeout) {
        logger.info("Start CitypayGate Shutdown");
        citypayGate.shutdown0(timeout);
        logger.info("CitypayGate shutdown");
    }

    private static void initSSL(CitypayGate citypayGate) throws InitException
    {
        String keyStorePath = null;
        String trustStorePath = null;
        try {
            keyStorePath = Configuration.getInstance().substituteVariablesInString(citypayGate.properties.getProperty(PROP_KEYSTORE_PATH));
            trustStorePath = Configuration.getInstance().substituteVariablesInString(citypayGate.properties.getProperty(PROP_TRUSTSTORE_PATH));
        } catch(Exception e) {
            throw new InitException("err.citypay_http_gate.failed_to_get_parameter", e);
        }

        String keyStorePassword = citypayGate.properties.getProperty(PROP_KEYSTORE_PASSWORD);
        String trustStorePassword = citypayGate.properties.getProperty(PROP_TRUSTSTORE_PASSWORD);

        try{
            int port = Integer.parseInt(citypayGate.properties.getProperty(PROP_PORT));

            Protocol authhttps = new Protocol("https",
	    	    (ProtocolSocketFactory) new AuthSSLProtocolSocketFactory(
                        new URL("file:" + keyStorePath), keyStorePassword,
                        new URL("file:" + trustStorePath), trustStorePassword), port);
            Protocol.registerProtocol("https", authhttps);
        }
        catch(Exception e){
            logger.error("Failed to initialize 'https' protocol", e);
            throw new InitException("err.citypay_https_gate.failed_to_init_https_citypay_protocol", e);
        }
    }

    protected String getGateName(){
        return GATE_NAME;
    }

}
