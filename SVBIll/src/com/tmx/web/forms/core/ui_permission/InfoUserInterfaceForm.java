package com.tmx.web.forms.core.ui_permission;

import com.tmx.web.base.BasicActionForm;
import com.tmx.web.controls.EditBox;
import com.tmx.web.controls.Button;
import com.tmx.web.controls.PopulateUtil;
import com.tmx.web.controls.ScrollBox;
import com.tmx.as.entities.general.ui_permission.UserInterface;
import com.tmx.as.entities.general.ui_permission.UserInterfaceType;

public class InfoUserInterfaceForm extends BasicActionForm {
    // work
    private Long selectedUserInterfaceId;
    private final static String action="/core/ui_permission/infoUserInterface";
    private UserInterface content;
    // info
    private EditBox userInterfaceId;
    private EditBox name;
    private ScrollBox type;
    // buttons
    private Button buttonApply;
    private Button buttonSave;
    private Button buttonCancel;
    private Button buttonReset;
    protected void init() {
        // info
        userInterfaceId=new EditBox();userInterfaceId.setReadonly(true);
        name=new EditBox();name.setMandatory(true);
        type=new ScrollBox() {
            protected String getKey(Object dataListElement) {
                return ((UserInterfaceType)dataListElement).getUserInterfaceTypeId().toString();
            }

            protected String getValue(Object dataListElement) {
                return ((UserInterfaceType)dataListElement).getName();
            }
        };
        // buttons
        buttonApply=new Button();
        buttonApply.setLabelKey("ctrl.button.label.apply");
        buttonApply.setAction(action);
        buttonApply.setCommand("apply");

        buttonSave=new Button();
        buttonSave.setLabelKey("ctrl.button.label.save");
        buttonSave.setAction(action);
        buttonSave.setCommand("save");

        buttonCancel=new Button();
        buttonCancel.setLabelKey("ctrl.button.label.cancel");
        buttonCancel.setAction("/core/ui_permission/manageUserInterface");

        buttonReset =new Button();
        buttonReset.setLabelKey("ctrl.button.label.reset");
    }

    public void bindData(UserInterface value) {
        content=value;
        userInterfaceId.setValue(value.getId() == null ? "" : value.getId().toString());
        name.setValue(value.getName());
        if(value.getType()!=null)
            type.setSelectedKey(value.getType().getUserInterfaceTypeId().toString());
    }

    public UserInterface populateFromControls() {
        content.setUserInterfaceId(PopulateUtil.getLongValue(userInterfaceId));
        content.setName(name.getValue());
        UserInterfaceType userInterfaceType=new UserInterfaceType();
        userInterfaceType.setUserInterfaceTypeId(new Long(type.getSelectedKey()));
        content.setType(userInterfaceType);
        return content;
    }

    public Button getButtonApply() {
        return buttonApply;
    }

    public void setButtonApply(Button buttonApply) {
        this.buttonApply = buttonApply;
    }

    public Button getButtonSave() {
        return buttonSave;
    }

    public void setButtonSave(Button buttonSave) {
        this.buttonSave = buttonSave;
    }

    public Button getButtonCancel() {
        return buttonCancel;
    }

    public void setButtonCancel(Button buttonCancel) {
        this.buttonCancel = buttonCancel;
    }

    public Button getButtonReset() {
        return buttonReset;
    }

    public void setButtonReset(Button buttonReset) {
        this.buttonReset = buttonReset;
    }

    public Long getSelectedUserInterfaceId() {
        return selectedUserInterfaceId;
    }

    public void setSelectedUserInterfaceId(Long selectedUserInterfaceId) {
        this.selectedUserInterfaceId = selectedUserInterfaceId;
    }

    public UserInterface getContent() {
        return content;
    }

    public void setContent(UserInterface content) {
        this.content = content;
    }

    public EditBox getUserInterfaceId() {
        return userInterfaceId;
    }

    public void setUserInterfaceId(EditBox userInterfaceId) {
        this.userInterfaceId = userInterfaceId;
    }

    public EditBox getName() {
        return name;
    }

    public void setName(EditBox name) {
        this.name = name;
    }

    public ScrollBox getType() {
        return type;
    }

    public void setType(ScrollBox type) {
        this.type = type;
    }
}

