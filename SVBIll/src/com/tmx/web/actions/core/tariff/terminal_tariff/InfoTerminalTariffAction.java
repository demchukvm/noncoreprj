package com.tmx.web.actions.core.tariff.terminal_tariff;

import com.tmx.as.blogic.tariff.AbstractTariffManager;
import com.tmx.as.blogic.tariff.TerminalTariffManager;
import com.tmx.as.blogic.TerminalManager;
import com.tmx.as.entities.bill.tariff.AbstractTariff;
import com.tmx.as.entities.bill.terminal.Terminal;
import com.tmx.as.entities.general.user.User;
import com.tmx.as.exceptions.DatabaseException;
import com.tmx.web.actions.core.tariff.AbstractInfoTariffAction;
import com.tmx.web.forms.core.tariff.AbstractInfoTariffForm;
import com.tmx.web.forms.core.tariff.terminal_tariff.InfoTerminalTariffForm;
import com.tmx.web.base.WebException;


public class InfoTerminalTariffAction extends AbstractInfoTariffAction {

    protected void preloadForCreate(AbstractInfoTariffForm form, AbstractTariff abstractTariff, User loggedInUser) throws DatabaseException, WebException {
        String terminalId = getRoadmap().getParameterValue(getName(), ManageTerminalTariffsAction.PARAMETER_OWNER_NAME, true).toString();
        if (!"".equals(terminalId)) {
            Terminal terminal = new TerminalManager().getTerminal(terminalId);
            ((InfoTerminalTariffForm) form).getBrowseBoxTerminals().setKey(terminal.getTerminalId().toString());
            ((InfoTerminalTariffForm) form).getBrowseBoxTerminals().setValue(terminal.getSerialNumber());
        }
    }

    protected void preloadForUpdate(AbstractInfoTariffForm form, AbstractTariff restriction) throws DatabaseException {
        //do nothing
    }

    protected AbstractTariffManager getTariffManager() {
        return new TerminalTariffManager();  
    }

}
