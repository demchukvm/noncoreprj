package com.tmx.web.forms.core.balance;

import com.tmx.as.entities.general.user.User;
import com.tmx.as.entities.bill.seller.Seller;
import com.tmx.as.base.criterion.CriterionWrapper;
import com.tmx.as.base.criterion.Restrictions;
import com.tmx.as.base.EntityManager;
import com.tmx.as.exceptions.DatabaseException;
import com.tmx.web.base.BasicActionForm;
import com.tmx.web.controls.Button;
import com.tmx.web.controls.EditBox;
import com.tmx.web.controls.CriterionFactory;
import com.tmx.web.controls.ScrollBox;
import com.tmx.web.controls.table.ControlManagerTable;


abstract public class AbstractManageBalancesForm extends BasicActionForm {

    private ControlManagerTable balancesTable;
    private Button onCreateButton;
    private Button onEditButton;
    private Button onDeleteButton;

    protected EditBox editBoxBalanceName;
    protected EditBox editBoxBalanceCode;

    protected ScrollBox scrollBoxSellerOwner;
    protected EditBox editlBoxSellerDescription;

    abstract protected void bindSpecificData(User loggedInUser) throws Throwable;

    protected void init(){
        onCreateButton = new Button();
        onCreateButton.setLabelKey("ctrl.button.label.create");
        onCreateButton.setCommand("preloadForCreate");

        onEditButton = new Button();
        onEditButton.setLabelKey("ctrl.button.label.edit");
        onEditButton.setCommand("preloadForUpdate");

        onDeleteButton = new Button();
        onDeleteButton.setLabelKey("ctrl.button.label.delete");
        onDeleteButton.setCommand("preloadForDelete");

        editBoxBalanceName = new EditBox();
        editBoxBalanceCode =  new EditBox();
        editlBoxSellerDescription =  new EditBox();

        scrollBoxSellerOwner = new ScrollBox() {
            protected String getKey(Object dataListElement) {
                return ((Seller)dataListElement).getSellerId().toString();
            }

            protected String getValue(Object dataListElement) {
                return ((Seller)dataListElement).getName();
            }
        };
    }

    public void bindData(User loggedInUser) throws Throwable{
        bindSpecificData(loggedInUser);
        if(balancesTable!=null){//child can set it null
            balancesTable.refresh();
            disableButtonsOnTableState();
        }
    }
        
    /**
     * disable or enable buttons depending on table state(empty or not)
     */
    public void disableButtonsOnTableState(){
        if(balancesTable !=null && balancesTable.getData().size()>0)
            disableButtons(false);
        else
            disableButtons(true);
    }

    private void disableButtons(boolean enable){
        if(onEditButton!=null && onDeleteButton!=null){//sub classes can set null
            onEditButton.setReadonly(enable);
            onDeleteButton.setReadonly(enable);
        }
    }

    protected CriterionFactory getCriterionFactory() {
        CriterionFactory criterionFactory = new CriterionFactory() {

            public CriterionWrapper createCriterion() {
                //initialize criterion wrapper
                CriterionWrapper resultCriterion = Restrictions.isNotNull("balanceId");

                 //add restrictions
                if (editBoxBalanceName.getValue() != null && !editBoxBalanceName.getValue().equals("")) {
                    resultCriterion = Restrictions.and(resultCriterion,
                            Restrictions.ilike("name", "%" + editBoxBalanceName.getValue() + "%"));
                }
                if (editBoxBalanceCode.getValue() != null && !editBoxBalanceCode.getValue().equals("")) {
                    resultCriterion = Restrictions.and(resultCriterion,
                            Restrictions.ilike("code", "%" + editBoxBalanceCode.getValue() + "%"));
                }
                if (editlBoxSellerDescription.getValue() != null && !editlBoxSellerDescription.getValue().equals("")) {
                    resultCriterion = Restrictions.and(resultCriterion,
                            Restrictions.ilike("seller.description", editlBoxSellerDescription.getValue() + "%"));
                }
                if (scrollBoxSellerOwner.getSelectedKey() != null) {
                    if(!scrollBoxSellerOwner.isNullKeySelected()) {
                        try {
                            Seller seller = (Seller)(new EntityManager()).RETRIEVE(Seller.class, scrollBoxSellerOwner.getSelectedKey());
                            resultCriterion = Restrictions.and(resultCriterion,
                                    Restrictions.eq("terminal.sellerOwner", seller));
                        } catch (DatabaseException e) {
                            e.printStackTrace();
                        }
                    }
                }

                return resultCriterion;
            }
        };

        return criterionFactory;
    }

    public ControlManagerTable getBalancesTable() {
        return balancesTable;
    }

    public void setBalancesTable(ControlManagerTable balancesTable) {
        this.balancesTable = balancesTable;
    }

    public Button getOnCreateButton() {
        return onCreateButton;
    }

    public void setOnCreateButton(Button onCreateButton) {
        this.onCreateButton = onCreateButton;
    }

    public Button getOnEditButton() {
        return onEditButton;
    }

    public void setOnEditButton(Button onEditButton) {
        this.onEditButton = onEditButton;
    }

    public Button getOnDeleteButton() {
        return onDeleteButton;
    }

    public void setOnDeleteButton(Button onDeleteButton) {
        this.onDeleteButton = onDeleteButton;
    }

    public EditBox getEditBoxBalanceName() {
        return editBoxBalanceName;
    }

    public void setEditBoxBalanceName(EditBox editBoxBalanceName) {
        this.editBoxBalanceName = editBoxBalanceName;
    }

    public EditBox getEditBoxBalanceCode() {
        return editBoxBalanceCode;
    }

    public void setEditBoxBalanceCode(EditBox editBoxBalanceCode) {
        this.editBoxBalanceCode = editBoxBalanceCode;
    }

    public ScrollBox getScrollBoxSellerOwner() {
        return scrollBoxSellerOwner;
    }

    public void setScrollBoxSellerOwner(ScrollBox scrollBoxSellerOwner) {
        this.scrollBoxSellerOwner = scrollBoxSellerOwner;
    }

    public EditBox getEditlBoxSellerDescription() {
        return editlBoxSellerDescription;
    }

    public void setEditlBoxSellerDescription(EditBox editlBoxSellerDescription) {
        this.editlBoxSellerDescription = editlBoxSellerDescription;
    }
}
