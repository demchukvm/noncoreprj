package com.tmx.web.forms.core.dictionary.user;

import com.tmx.web.base.BasicActionForm;
import com.tmx.web.controls.Button;
import com.tmx.web.controls.EditBox;
import com.tmx.web.controls.table.CheckBoxFilterEnabler;
import com.tmx.web.controls.table.Filter;
import com.tmx.web.controls.table.ControlManagerTable;

public class ManageUsersForm extends BasicActionForm {
    private ControlManagerTable usersTable;
    private Button onCreateButton;
    private Button onEditButton;
    private Button onDeleteButton;

    protected void init() {
        //setup buttons
        onCreateButton = new Button();
        onCreateButton.setLabelKey("ctrl.button.label.create");
        onCreateButton.setAction("/core/dictionary/user/userInfo");
        onCreateButton.setCommand("preloadForCreate");

        onEditButton = new Button();
        onEditButton.setLabelKey("ctrl.button.label.edit");
        onEditButton.setAction("/core/dictionary/user/userInfo");
        onEditButton.setCommand("preloadForUpdate");

        onDeleteButton = new Button();
        onDeleteButton.setLabelKey("ctrl.button.label.delete");
        onDeleteButton.setAction("/core/dictionary/user/userInfo");
        onDeleteButton.setCommand("preloadForDelete");

        initTable();
    }

    private void initTable() {
        usersTable = new ControlManagerTable("query(query_type=retrieve_all," +
                "entity_class=com.tmx.as.entities.general.user.User," +
                "order(name=userId,type=asc)," +
                "table_attr=role," +
                "table_attr=organization," +
                "page_size=15" +
                ")");

        usersTable.setRefreshActionName("/core/dictionary/user/manageUsers");
        
        usersTable.addParameterToFilter("by_login", "login", new EditBox());
        usersTable.addParameterToFilter("by_firstName", "firstName", new EditBox());
        usersTable.addParameterToFilter("by_lastName", "lastName", new EditBox());

        Filter byActiveFilter = new Filter();
        CheckBoxFilterEnabler checkBoxFilterEnabler = new CheckBoxFilterEnabler();
        checkBoxFilterEnabler.setSelected(true);
        byActiveFilter.setFilterEnabler(checkBoxFilterEnabler);
//        usersTable.getFilterSet().addFilter("by_active", byActiveFilter);
        
        usersTable.addDependedControl(onDeleteButton).addDependedControl(onEditButton);
    }

    //--------Properties

    public ControlManagerTable getUsersTable() {
        return usersTable;
    }

    public void setUsersTable(ControlManagerTable usersTable) {
        this.usersTable = usersTable;
    }

    public Button getOnCreateButton() {
        return onCreateButton;
    }

    public void setOnCreateButton(Button onCreateButton) {
        this.onCreateButton = onCreateButton;
    }

    public Button getOnEditButton() {
        return onEditButton;
    }

    public void setOnEditButton(Button onEditButton) {
        this.onEditButton = onEditButton;
    }

    public Button getOnDeleteButton() {
        return onDeleteButton;
    }

    public void setOnDeleteButton(Button onDeleteButton) {
        this.onDeleteButton = onDeleteButton;
    }
}
