package com.tmx.beng.testgate;

import com.tmx.util.queue.ActionQueueElement;
import com.tmx.beng.base.BillingMessage;
import org.apache.log4j.Logger;

/**
 */
public class TestGateTask extends ActionQueueElement {
    private BillingMessage procesedMessage = null;
    private static Logger logger = Logger.getLogger("gate.TestGateTask");

    public TestGateTask(BillingMessage billingMessage){
        this.procesedMessage = billingMessage;
    }

    public void run() {
        try{
            TestGate.getInstance().runBillMessage(procesedMessage);
        }
        catch(TestGateException e){
            logger.error(e.getLocalizedMessage(), e);
        }
    }
}
