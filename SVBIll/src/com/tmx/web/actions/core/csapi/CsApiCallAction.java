package com.tmx.web.actions.core.csapi;

import com.tmx.as.modules.SpringModule;
import com.tmx.beng.base.BillingMessage;
import com.tmx.beng.base.BillingMessageWritable;
import com.tmx.web.forms.core.csapi.CsApiCallForm;
import com.tmx.web.base.BasicDispatchedAction;
import com.tmx.util.InitException;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForm;
import org.apache.log4j.Logger;
import com.tmx.beng.access.Access;
import com.tmx.beng.access.Connection;
import com.tmx.beng.csapi.xml.v1.base.ClientPAD;
import com.tmx.beng.base.BillException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.naming.InitialContext;
import javax.naming.NamingException;

/**
 * http://localhost:9090/svbill/core/jsp/csapi/api_test_form.jsp
 */
public class CsApiCallAction extends BasicDispatchedAction {
    Logger logger = Logger.getLogger("webcsapi."+getClass().getName());

    public ActionForward clientSideApiCall(ActionMapping mapping,
                                                        ActionForm form,
                                                        HttpServletRequest request,
                                                        HttpServletResponse response) throws Throwable {

        CsApiCallForm csApiCallForm = (CsApiCallForm) verifyForm(form, CsApiCallForm.class);
        ClientPAD clientPAD = new ClientPAD();
        try{

            //1. Allocate access to billing engine

            //--------TODO(A.N.) Ununderstendebl exception--------------------
//            try{
            Connection conn = (Connection) SpringModule.getApplicationContext().getBean("billingConnection");
//            }
//            catch(InitException e){
//                logger.error("Client initialization error: \n"+e.getTraceDump());
//                csApiCallForm.setXmlDocSerialized(clientPAD.buildErrorResponse(ClientPAD.CSAPI_CLIENT_INIT_ERROR, getLocale(request)));
//                throw new HandledBillingException();
//            }
            //------------------

            long t = System.currentTimeMillis();
            ClientPAD.ParsingResult resp;

            //2. Parse request
            try{
                logger.info("Request XML: " + csApiCallForm.getRequestXml());
                resp = clientPAD.parseRequest(csApiCallForm.getRequestXml());
                if (csApiCallForm.getDesRequestXml() != null) {
                    resp = clientPAD.parseRequest(csApiCallForm.getDesRequestXml());
                }
                logger.debug("Parse time: " + (System.currentTimeMillis() - t));
            }
            catch(BillException e){
                logger.error("Parsing error: \n"+e.getTraceDump());
                csApiCallForm.setXmlDocSerialized(clientPAD.buildErrorResponse(ClientPAD.CSAPI_REQUEST_PARSING_ERROR, getLocale(request)));
                throw new HandledBillingException();
            }

            //3. Process request
            try{
                resp.setBillingMessage(conn.processSync(resp.getBillingMessage()));
            }
            catch(BillException e){
                logger.error("Processing error: \n"+e.getTraceDump());
                csApiCallForm.setXmlDocSerialized(clientPAD.buildErrorResponse(ClientPAD.CSAPI_REQUEST_PROCESSING_ERROR, getLocale(request)));
                throw new HandledBillingException();
            }


            

            //4. Serialize response
            csApiCallForm.setXmlDocSerialized(clientPAD.safeSerializeResponse(resp.getBillingMessage(), resp.getVersion()));
            logger.info("Response XML: " + csApiCallForm.getXmlDocSerialized());
        }
        catch(HandledBillingException e){
            logger.error("Error: " + e.toString());
        }
        catch(Throwable e){
            e.printStackTrace();
            logger.error("Unknown error: \n"+e.toString());
            csApiCallForm.setXmlDocSerialized(clientPAD.buildErrorResponse(ClientPAD.CSAPI_UNKNOWN_ERROR, getLocale(request)));
        }
        return mapping.findForward("csapicall_resp");
    }

    /** Builds general xml error response on web csapi level */
    protected ActionForward defaultAction(ActionMapping mapping,
                                          ActionForm form,
                                          HttpServletRequest request,
                                          HttpServletResponse response,
                                          String methodName) throws Throwable{

        return clientSideApiCall(mapping, form, request, response);
    }

    private class HandledBillingException extends Throwable{
    }
}


