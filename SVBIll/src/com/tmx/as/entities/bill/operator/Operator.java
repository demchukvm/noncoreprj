package com.tmx.as.entities.bill.operator;

import com.tmx.as.base.BasicEntity;
import com.tmx.as.entities.bill.balance.OperatorBalance;
import com.tmx.as.entities.bill.address.Address;

import java.util.Date;
import java.io.Serializable;

public class Operator extends BasicEntity {
    private Long operatorId;
    private String code;
    private String name;
    private Date registrationDate;
    private String description;
    private OperatorBalance primaryBalance;
    private Address address = null;

    public Long getOperatorId() {
        return operatorId;
    }

    public void setOperatorId(Long operatorId) {
        this.operatorId = operatorId;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getRegistrationDate() {
        return registrationDate;
    }

    public void setRegistrationDate(Date registrationDate) {
        this.registrationDate = registrationDate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public OperatorBalance getPrimaryBalance() {
        return primaryBalance;
    }

    public void setPrimaryBalance(OperatorBalance primaryBalance) {
        this.primaryBalance = primaryBalance;
    }


    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public Serializable getId() {
        return getOperatorId();
    }

    public int getValid() {
        return valid;
    }

    public void setValid(int valid) {
        this.valid = valid;
    }


}
