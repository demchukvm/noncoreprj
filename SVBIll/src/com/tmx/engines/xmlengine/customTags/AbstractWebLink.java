package com.tmx.engines.xmlengine.customTags;

import org.w3c.dom.Node;
import org.w3c.dom.Element;
import com.tmx.as.base.Entity;

/** Use prepareURL() and prepareContent() in successors to
 * implement your link. */
public abstract class AbstractWebLink extends AbstractNode {
    private String url = null;

    public AbstractWebLink(String linkName){
        setName(linkName);
    }

    /** AbstractLink descendants will implements their prepairURL()
     * and its value will be populated into the CDATA of AbstractNode */
    public String getCDATASectionValue(){
        return prepareContent();
    }

    public Node renderAndInjectXMLNode(Node parentNode, Entity entity) {
        Element webLinkNode = (Element)super.renderAndInjectXMLNode(parentNode, entity);
        setURL(prepareURL());
        if(getURL() != null && getURL().trim().length() > 0)
            webLinkNode.setAttribute("url", getURL());
        return webLinkNode;
    }

    public abstract String prepareURL();

    /** To be overriden in successors */
    public String prepareContent(){
        return null;
    }

    public String getTagName() {
        return "link";
    }

    //--------proeprties

    public String getURL(){
        return url;
    }

    public void setURL(String url){
        this.url = url;
    }
}
