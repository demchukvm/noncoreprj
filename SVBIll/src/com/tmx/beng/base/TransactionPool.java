package com.tmx.beng.base;

import com.tmx.beng.base.memory.InMemoryTable;

import java.util.List;
import java.util.Iterator;

/**
 */
public class TransactionPool extends InMemoryTable {

    public Transaction openTransaction(){
        Transaction tx = new TransactionImpl();
        insert(tx);
        return tx;
    }

    public String createTransactionId(){
        return nextId();
    }

    public void closeTransaction(String transactionId){
        Transaction tx = (Transaction)lookup(transactionId);
        delete(transactionId);
        tx.close();
    }

    public void joinTransaction(String joinedTransactionId, Task joinedTask) throws BillException{
        Transaction tx = (Transaction)lookup(joinedTransactionId);
        if(tx == null)
            throw new BillException("transaction_for_join_to_not_found",
                    new String[]{
                            joinedTransactionId
            });

        tx.addTask(joinedTask);
    }

    /** Lookup task by enclosed message name in the transaction  */
    public Task lookupTask(String transactionId, String taskName) throws BillException{
        Transaction tx = (Transaction)lookup(transactionId);
        if(tx == null)
            throw new BillException("transaction_to_not_found", new String[]{transactionId});

        Task foundTask = null;
        for(Iterator it = tx.getTasks().iterator(); it.hasNext();){
            Task task = (Task)it.next();
            if(taskName.equals(task.getProcesedMessage().getOperationName()))
                if(foundTask == null)
                    foundTask = task;
                else
                    throw new BillException("duplicate_task_found", new String[]{taskName});
        }

        if(foundTask != null)
            return foundTask;
        else
            return null;
            //throw new BillException("task_not_found", new String[]{taskName});
    }

    /** Lookup task by enclosed message name in the transaction  */
    public Task lookupFirstTask(String transactionId) throws BillException{
        Transaction tx = (Transaction)lookup(transactionId);
        if(tx == null)
            throw new BillException("transaction_to_not_found", new String[]{transactionId});

        Iterator it = tx.getTasks().iterator();
        return (it.hasNext() ? (Task)it.next() : null);
    }

    @Override
    public String toString() {
        return rows.toString();
    }
}
