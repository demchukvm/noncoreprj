package com.tmx.beng.medaccess.globalmoney;

import com.tmx.beng.medaccess.GateException;
import com.tmx.beng.medaccess.MediumAccess;
import com.tmx.beng.medaccess.MediumConnection;
import com.tmx.beng.medaccess.globalmoney.GlobalMoneyGateMediumConnection;

public class GlobalMoneyGateMediumAccess implements MediumAccess {

    public void init()
    {

    }

    public MediumConnection getConnection() throws GateException
    {
        return new GlobalMoneyGateMediumConnection();
    }
}