package com.tmx.web.taglib.html.ctrl;

import com.tmx.web.base.ApplicationEnvironment;
import javax.servlet.jsp.JspException;
import org.apache.struts.taglib.TagUtils;
import com.tmx.web.controls.ExpandingPanel;

/**
 */
public class ExpandingPanelTag extends PanelTag {
    private String closeImage = null;
    private String openImage = null;
    private String divStyle = null;
    private String tableStyle = null;
    //constant to populate ExpandingPanel.expanded property
    protected static String EXPANDED_PROPERTY_NAME = ".expanded";

    //----------------------Properties

    public String getDivStyle() {
        return (divStyle != null) ? divStyle : getDefaultProperty("tag.expandingPanel.style.divStyle");
    }

    public void setDivStyle(String divStyle) {
        this.divStyle = divStyle;
    }

    public String getTableStyle() {
        return (tableStyle != null) ? tableStyle : getDefaultProperty("tag.expandingPanel.style.tableStyle");
    }

    public void setTableStyle(String tableStyle) {
        this.tableStyle = tableStyle;
    }

    public String getCloseImage() {
        return (closeImage != null) ? closeImage : ApplicationEnvironment.getAppDeploymentName() + getDefaultProperty("tag.expandingPanel.img.closeImg");
    }

    public void setCloseImage(String closeImage) {
        this.closeImage = closeImage;
            }

    public String getOpenImage() {
        return (openImage != null) ? openImage : ApplicationEnvironment.getAppDeploymentName() + getDefaultProperty("tag.expandingPanel.img.openImg");
    }

    public void setOpenImage(String openImage) {
        this.openImage = openImage;
    }

    public String getTitle(){
        return (getTitleKey() != null) ? getLocalizedLabel(getTitleKey(), null) : getLocalizedLabel("tag.expandingPanel.label.title", null);
    }

    //------------------------Parsing events
    public int doStartTmxTag() throws JspException {
        super.doStartTag();
        StringBuffer sb = new StringBuffer();
        String panelName = getSeparatorReplacedName(getPanelName());
        ExpandingPanel panelCtrl = (ExpandingPanel)getPanel();

        sb.append("<script language='JavaScript'>\r");
        sb.append("var ").append(panelName).append("_show = ").append(panelCtrl.isExpanded()).append(";\r");

        sb.append("function ").append(panelName).append("_show_filter(){\r");
        sb.append("document.getElementById('").append(panelName).append(EXPANDED_PROPERTY_NAME).append("').value=")
                .append(panelName).append("_show;\r");
        sb.append("if(");
        sb.append(panelName).append("_show == false){\r");
        sb.append(" document.getElementById('").append(panelName).append("_table').style.display='none';\r");
        sb.append(" document.getElementById('").append(panelName).append("_img').src='").append(getOpenImage()).append("';\r");
        sb.append("}else{\r");
        sb.append(" document.getElementById('").append(panelName).append("_table').style.display='inline';\r");
        sb.append(" document.getElementById('").append(panelName).append("_img').src='").append(getCloseImage()).append("';\r");
        sb.append("}\r");
        sb.append(panelName).append("_show = !").append(panelName).append("_show;\r");
        sb.append("}\r");

        sb.append("registerFunction('window.onload', function(){").append(panelName).append("_show_filter();});\r");
        sb.append("</script>\r");

        sb.append("<input type='hidden' name='").append(panelName).append(EXPANDED_PROPERTY_NAME).append("'").
                append("value='").append(panelCtrl.isExpanded()).append("'/>");

        sb.append("<div class='").append(getDivStyle()).append("'>\r");
        sb.append("<div onclick='").append(panelName).append("_show_filter()' style='cursor : hand; width:100%'>\r");
        sb.append("<img name='").append(panelName).append("_img'>&nbsp;&nbsp;");
        sb.append(getTitle());
        sb.append("</div>\r");
        sb.append("<table border='0' width='100%' id='").append(panelName).append("_table' class='").append(getTableStyle() != null ? getTableStyle() :"");
        sb.append("'><tr><td>");

        TagUtils.getInstance().write(pageContext, sb.toString());
        return EVAL_BODY_INCLUDE;
    }

    public int doEndTmxTag() throws JspException{
        StringBuffer sb = new StringBuffer();
        sb.append("</td></tr></table></div>\r\r");

        TagUtils.getInstance().write(pageContext, sb.toString());

        super.doEndTag();
        return EVAL_PAGE;
    }

}

