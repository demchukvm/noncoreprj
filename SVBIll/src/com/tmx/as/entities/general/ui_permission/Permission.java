package com.tmx.as.entities.general.ui_permission;

import com.tmx.as.base.BasicEntity;
import com.tmx.as.entities.general.role.Role;

import java.io.Serializable;

/**
 */
public class Permission extends BasicEntity {
    private Long permissionId;
    private UserInterface userInterface;
    private PermissionType permissionType;
    private Role role;

    public Long getPermissionId() {
        return permissionId;
    }

    public void setPermissionId(Long permissionId) {
        this.permissionId = permissionId;
    }

    public UserInterface getUserInterface() {
        return userInterface;
    }

    public void setUserInterface(UserInterface userInterface) {
        this.userInterface = userInterface;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public PermissionType getPermissionType() {
        return permissionType;
    }

    public void setPermissionType(PermissionType permissionType) {
        this.permissionType = permissionType;
    }

    public Serializable getId() {
        return getPermissionId();
    }

    public int getValid() {
        return valid;
    }

    public void setValid(int valid) {
        this.valid = valid;
    }
}
