package com.tmx.as.entities.bill.restriction;

import com.tmx.as.entities.bill.seller.Seller;
import com.tmx.as.entities.bill.function.FunctionInstance;

/**

 */
public class SellerRestriction extends AbstractRestriction{
    private Seller seller;
    private Long sellerId;

    //--------------Properties
    public Seller getSeller() {
        return seller;
    }

    public void setSeller(Seller seller) {
        this.seller = seller;
    }

    public Long getSellerId() {
        return sellerId;
    }

    public void setSellerId(Long sellerId) {
        this.sellerId = sellerId;
    }
    public Object clone() throws CloneNotSupportedException {
        SellerRestriction clone = (SellerRestriction)super.clone();
        clone.setFunctionInstance((FunctionInstance) clone.getFunctionInstance().clone());
        clone.setRestrictionId(null);//new clone is transient
        return clone;
    }
}
