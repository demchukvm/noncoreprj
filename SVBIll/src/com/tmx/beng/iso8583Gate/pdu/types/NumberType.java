package com.tmx.beng.iso8583Gate.pdu.types;

import com.tmx.beng.iso8583Gate.pdu.RuntimeDataElement;
import com.tmx.beng.iso8583Gate.pdu.IsoParseException;
import com.tmx.beng.iso8583Gate.pdu.IsoSerializeException;
import com.tmx.beng.iso8583Gate.pdu.IncorrectFormatException;
import com.tmx.beng.iso8583Gate.pdu.util.BCDConverter;
import java.io.InputStream;
import java.io.IOException;
import java.io.OutputStream;

/**
 */
public class NumberType extends Type {
    {
        name = "n";
    }
    private BCDConverter bcdConverter = new BCDConverter();

    public void parse(InputStream is, int length, RuntimeDataElement rde) throws IsoParseException {
        try{
            long value = bcdConverter.getLongFromBcd(is, length);
            rde.setValue(value);
        }
        catch(IOException e){
            throw new IsoParseException("err.number_type.parsing_failed", e);
        }
        catch(IncorrectFormatException e){
            throw new IsoParseException("err.number_type.parsing_failed", e);
        }
    }

    public SerializedValue serialize(boolean fixedLength, int bcdLengthLimit, RuntimeDataElement rde) throws IsoSerializeException{
        try{
            byte[] bcdData = fixedLength ?
                    bcdConverter.getUnsignedBcdFromLong(rde.getLong(), bcdLengthLimit) :
                    bcdConverter.getCompactedUnsignedBcdFromLong(rde.getLong(), bcdLengthLimit);


/*          Expansion resolved by bcdConvertor above
            if(fixedLength && (bcdData.length < bcdConverter.getByteSize(bcdLengthLimit)))
                bcdData = expandFixedLengthValue(bcdData, bcdLengthLimit);
*/

            SerializedValue sv = new SerializedValue();
            sv.data = bcdData;
            sv.length = bcdConverter.getBcdSize(bcdData.length);
            return sv;
        }
        catch (IncorrectFormatException e) {
            throw new IsoSerializeException("err.number_type.serialization_failed", e);
        }
    }

    protected byte[] expandFixedLengthValue0(byte[] value, int fixedByteLength) throws IncorrectFormatException{
        //fill head of byte array by 0 to match fixed length
        byte[] expandedData = new byte[fixedByteLength];
        System.arraycopy(value, 0, expandedData, expandedData.length-value.length, value.length);
        return expandedData;
    }
}
