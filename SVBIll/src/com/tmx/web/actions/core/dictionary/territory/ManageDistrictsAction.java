package com.tmx.web.actions.core.dictionary.territory;

import com.tmx.web.base.BasicDispatchedAction;
import com.tmx.web.forms.core.dictionary.territory.ManageRegionsForm;
import com.tmx.web.forms.core.dictionary.territory.ManageDistrictsForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForm;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class ManageDistrictsAction extends BasicDispatchedAction {
    private static final String FORWARD_MANAGE_ALARM_LEVELS_PAGE = "managePage";

    public ActionForward defaultAction(ActionMapping mapping,
                                       ActionForm form,
                                       HttpServletRequest request,
                                       HttpServletResponse response,
                                       String methodName) throws Throwable {
        ManageDistrictsForm manageDistrictsForm = (ManageDistrictsForm)
                                    verifyForm(form, ManageDistrictsForm.class);
        manageDistrictsForm.getTable().setRefreshActionName(this.getName());
        manageDistrictsForm.getTable().refresh();
        return mapping.findForward(FORWARD_MANAGE_ALARM_LEVELS_PAGE);
    }
}
