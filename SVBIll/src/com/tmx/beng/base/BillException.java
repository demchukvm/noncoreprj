package com.tmx.beng.base;

import com.tmx.util.StructurizedException;
import java.util.Locale;

/**
 */
public class BillException extends StructurizedException {

    public BillException(String msg, String[] params, Throwable e, Locale locale) {
        super(msg, params, e, locale);
    }

    public BillException(String msg, String[] params) {
        super(msg, params);
    }

    public BillException(String msg, String[] params, Locale locale) {
        super(msg, params, locale);
    }

    public BillException(String msg, Throwable e, Locale locale) {
        super(msg, e, locale);
    }

    public BillException(String msg, Throwable e) {
        super(msg, e);
    }

    public BillException(String msg, Locale locale) {
        super(msg, locale);
    }

    public BillException(String msg) {
        super(msg);
    }

    public BillException(String msg, String[] params, Throwable e) {
        super(msg, params, e);
    }
}
