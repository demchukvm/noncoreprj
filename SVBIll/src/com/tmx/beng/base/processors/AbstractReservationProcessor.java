package com.tmx.beng.base.processors;

import com.tmx.as.base.EntityManager;
import com.tmx.as.entities.bill.transaction.AbstractTransaction;
import com.tmx.as.entities.bill.transaction.OperationType;
import com.tmx.as.entities.bill.transaction.TransactionSupport;
import com.tmx.as.entities.bill.transaction.TransactionType;
import com.tmx.as.entities.general.user.User;
import com.tmx.as.exceptions.DatabaseException;
import com.tmx.beng.base.*;
import com.tmx.beng.base.cache.DBCache;
import com.tmx.beng.base.cache.DBCacheException;

import java.util.*;


abstract public class AbstractReservationProcessor extends TaskProcessor {

    private final String SCHEMA_NAME = "svbill";
    public final int ROUND_SCALE = 3;

    protected Task process0(Task task, DBCache dbCache, BillingMessageWritable msg) throws BillException {
        logger.debug("reservation...");
        try {
            TransConfig config = getTransactionConfiguration(dbCache, msg);
            List transactions = new LinkedList();

            transactions.addAll(prepareOperatorTransactions(msg, dbCache, config));    // идет вызов из DefaultReservationProcessor
            transactions.addAll(prepareSellerTransactions(msg, dbCache, config));
            transactions.addAll(prepareTerminalTransactions(msg, dbCache, config));
            transactions.addAll(prepareClientTransaction(msg, dbCache, config));

            saveTransactions(config, transactions);//save transactions before add procedure

            transactions.addAll(prepareChangeBalancesProcedure(msg, dbCache, config));

            msg.setAttribute(BillingMessage.ENTITY_LIST, transactions);
        } catch (Throwable e) {
            task.getProcesedMessage().setStatusCode(StatusDictionary.RESERVATION_ERROR);
            logger.error(e.getLocalizedMessage(), e);
            if (e instanceof BillException)
                throw (BillException) e;
            throw new BillException(e.getLocalizedMessage(), e);
        }
        return task;
    }

    protected void saveTransactions(BillingMessageWritable msg, List transactions) throws DatabaseException {
        EntityManager entityManager = new EntityManager();
        try {
            entityManager.BEGIN(SCHEMA_NAME);
            for (Iterator it = transactions.iterator(); it.hasNext();) {
                Object obj = it.next();
                if (obj instanceof AbstractTransaction) {
                    AbstractTransaction tx = (AbstractTransaction) obj;
                    tx.setStatus(msg.getAttributeString(BillingMessage.STATUS_CODE));
                    if (tx.getTransactionSupport() != null) {
                        entityManager.SAVE(tx.getTransactionSupport());
                    }
                    entityManager.SAVE(tx);
                }
            }
            entityManager.COMMIT(SCHEMA_NAME);
        } catch (DatabaseException e) {
            entityManager.ROLLBACK(SCHEMA_NAME);
            throw e;
        }
    }

    private void saveTransactions(TransConfig config, List transactions) throws DatabaseException {
        EntityManager entityManager = new EntityManager();
        try {
            entityManager.BEGIN(SCHEMA_NAME);
            for (Iterator it = transactions.iterator(); it.hasNext();) {
                Object obj = it.next();
                if (obj instanceof AbstractTransaction) {
                    AbstractTransaction tx = (AbstractTransaction) obj;
                    tx.setStatus(config.status);
                    if (tx.getTransactionSupport() != null) {
                        entityManager.SAVE(tx.getTransactionSupport());
                    }
                    entityManager.SAVE(tx);
                }
            }
            entityManager.COMMIT(SCHEMA_NAME);
        } catch (DatabaseException e) {
            entityManager.ROLLBACK(SCHEMA_NAME);
            throw e;
        }
    }

    protected double getSignedAmount(String transTypeCode, double amount) {
        if (TransactionType.CODE_CREDIT.equals(transTypeCode))
            return amount;
        else if (TransactionType.CODE_DEBET.equals(transTypeCode))
            return -amount;
        else
            throw new IllegalArgumentException("undefined transaction type code - " + transTypeCode);
    }

    protected List prepareOperatorTransactions(BillingMessageWritable msg, DBCache dbCache, TransConfig config)
            throws DBCacheException {
        return Collections.EMPTY_LIST;
    }


    protected List prepareSellerTransactions(BillingMessageWritable msg, DBCache dbCache, TransConfig config)
            throws DBCacheException {
        return Collections.EMPTY_LIST;
    }


    protected List prepareTerminalTransactions(BillingMessageWritable msg, DBCache dbCache, TransConfig config)
            throws DBCacheException {
        return Collections.EMPTY_LIST;
    }


    protected List prepareClientTransaction(BillingMessageWritable msg, DBCache dbCache, TransConfig config)
            throws DBCacheException {
        return Collections.EMPTY_LIST;
    }


    protected List prepareChangeBalancesProcedure(BillingMessageWritable msg, DBCache dbCache, TransConfig config)
            throws DBCacheException {
        return Collections.EMPTY_LIST;
    }

    protected TransConfig getTransactionConfiguration(DBCache cache, BillingMessageWritable msg) {
        TransConfig config = new TransConfig();
        config.transRegTime = (Date) msg.getAttribute(BillingMessage.REGISTRATION_TIME);
        config.clientTime = (Date) msg.getAttribute(BillingMessage.CLIENT_TIME);
        config.status = Integer.toString(StatusDictionary.TRANSACTION_OPENED);
        config.reservationSet = (ReservationSet) msg.getAttribute(BillingMessage.BALANCE_RESERVATION_SET);
        config.operatorCode = msg.getAttributeString(BillingMessage.SERVICE_CODE);
        config.sellerCode = msg.getAttributeString(BillingMessage.SELLER_CODE);
        config.terminalSn = msg.getAttributeString(BillingMessage.TERMINAL_SN);
        config.transactionSupport = getTransactionSupport(msg);
        config.transactionGUID = msg.getAttributeString(BillingMessage.BILL_TRANSACTION_NUM);
        config.cliTransactionNum = msg.getAttributeString(BillingMessage.CLI_TRANSACTION_NUM);
        config.operationsType = cache.getOperationType(getOperationTypeName(msg));
        return config;
    }

    protected TransactionSupport getTransactionSupport(BillingMessageWritable msg) {
        //transaction support
        TransactionSupport transSupport = new TransactionSupport();
        //user for transaction support
        if (msg.getAttribute(BillingMessage.USER_ACTOR_ID) != null) {
            Long userActorId = new Long(msg.getAttributeString(BillingMessage.USER_ACTOR_ID));
            User userActor = new User();
            userActor.setUserId(userActorId);
            transSupport.setUserActor(userActor);
        }
        //-----------------------------
        transSupport.setDescription(msg.getAttributeString(BillingMessage.TRANSACTION_DESCRIPTION));
        transSupport.setPaymentReason(msg.getAttributeString(BillingMessage.PAYMENT_REASON));
        return transSupport;
    }

    private String getOperationTypeName(BillingMessageWritable msg){
        return msg.getOperationName().split("\\.")[1];   
    }

    protected static class TransConfig {
        Date transRegTime;
        Date clientTime;
        String status;
        ReservationSet reservationSet;
        String operatorCode;
        String sellerCode;
        String terminalSn;
        TransactionSupport transactionSupport;
        String transactionGUID;
        String cliTransactionNum;
        OperationType operationsType;
    }

}
