package com.tmx.beng.base.processors;

import com.tmx.beng.base.*;
import com.tmx.beng.base.memory.Condition;
import com.tmx.beng.base.memory.InMemoryObject;
import com.tmx.beng.base.memory.InMemoryException;
import com.tmx.beng.base.dbfunctions.BasicFunction;
import com.tmx.beng.base.cache.DBCache;
import com.tmx.beng.base.cache.FunctionWrapper;
import com.tmx.as.base.FilterWrapper;
import com.tmx.as.base.EntityManager;
import com.tmx.as.entities.bill.transaction.TerminalBalanceTransaction;
import com.tmx.as.exceptions.DatabaseException;

import java.util.List;

/**
  Validate client transaction number
 */
public class ValidateCliTransactionProcessor extends TaskProcessor {

    public Task process0(Task task, DBCache dbCache, BillingMessageWritable msg)throws BillException {
        logger.debug("Validate client transaction...");

        final String cliTransactionNum = msg.getAttributeString(BillingMessage.CLI_TRANSACTION_NUM);
        task.getTransaction().setCliTransactionNum(cliTransactionNum);

        //lookup in memory
        List transs = task.getGlobalContext().getTransactionPool().lookup(new Condition(){
            public boolean evaluate(InMemoryObject obj) {
                return cliTransactionNum.equals(obj.getValue("cliTransactionNum"));
            }
        });
        if(transs.size() > 1){
            BillException billException = new BillException("err.validate_client_trans.duplicate_transaction_num", new String[]{cliTransactionNum});
            msg.setStatus(StatusDictionary.VALIDATE_CLIENT_TRANSACTION_ERROR, billException.getLocalizedMessage());
            throw billException;
        }


        FilterWrapper byBillNumFW = new FilterWrapper("by_clinum");
        byBillNumFW.setParameter("clinum", cliTransactionNum);
        try {
            TerminalBalanceTransaction termTx = (TerminalBalanceTransaction) new EntityManager().
                    RETRIEVE(TerminalBalanceTransaction.class, new FilterWrapper[]{byBillNumFW}, null);

            if (termTx != null) {
                BillException billException = new BillException("err.validate_client_trans.duplicate_transaction_num", new String[]{cliTransactionNum});
                msg.setStatus(StatusDictionary.VALIDATE_CLIENT_TRANSACTION_ERROR, billException.getLocalizedMessage());
                throw billException;
            }

        } catch (DatabaseException e) {
          msg.setStatus(StatusDictionary.VALIDATE_CLIENT_TRANSACTION_ERROR, e.getLocalizedMessage());
          throw new BillException("err.validate_client_trans.database_error", e);
        }

        return task;
    }
}
