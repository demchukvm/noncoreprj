package com.tmx.beng.base.dbfunctions;

import com.tmx.beng.base.cache.InvokeFunctionException;
import com.tmx.beng.base.BillingMessage;

import java.util.Map;

/**
    To limit transaction with fractional limit
 */
public class TransactionNonFractionalLimit extends BasicLimitFunction{

    public void checkLimit(Map actualParams) throws InvokeFunctionException {
        double amount = Double.parseDouble(task.getProcesedMessage().getAttributeString(BillingMessage.AMOUNT));
        if(amount != (double)Math.round(amount))
            throw new InvokeFunctionException("err.bill.dbfunctions.amount_with_fraction_forbidden", new String[]{String.valueOf(amount)});
    }
}
