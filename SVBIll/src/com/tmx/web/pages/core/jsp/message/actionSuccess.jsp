<%@ page language="java" pageEncoding="UTF-8" contentType="text/html; charset=utf-8" errorPage="/core/jsp/message/jsperror.jsp" isELIgnored="false" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>

<div class="main_message">
    <bean:message bundle="core.labels" key="core.page.message.actionsuccess.msg.action_success"/>
</div>
