package com.tmx.beng.base;

import com.tmx.util.i18n.MessageResources;

import java.util.*;

/**

 */
public class StatusDictionary {
    public final static String STATUS_MSGS_BUNDLE = "bill_statuses";
    /** Request registered in Billing Engine and Transaction has opened. */
    public static int STATUS_OK = 0;
    public static int TRANSACTION_OPENED = 1;
    public static int SYNC_BILL_WAIT_INTERRUPED = 2;
    public static int BILLING_RETURNS_NULL_MESSAGE = 3;
    public static int JOINED_REQUEST_NOT_FOUND = 4;
    public static int WAIT_FOR_BILLING_TIME_OUT = 5;
    public static int AUTHENTICATION_ERROR = 6;
    public static int UNCKNOWN_COMMAND = 7;
    public static int OPEN_TRANSACTION_ERROR = 8;
    public static int MEDIUM_CONNECT_ERROR = 9;
    public static int LIMITS_ERROR = 10;
    public static int TARIFF_ERROR = 11;
    public static int RESERVATION_ERROR = 12;                      
    public static int COPY_CALLBACK_ERROR = 13;
    public static int CANCEL_RESERVATION_ERROR = 14;
    public static int SAVE_TO_DB_ERROR = 15;
    public static int PAYMENT_PROPS_CHECK_ERROR = 16;
    public static int UNKNOWN_STATUS = 17;
    public static int IDENTIFY_STATUS_ERROR = 18;    
    public static int VALIDATE_CLIENT_TRANSACTION_ERROR = 19;
    public static int TIME_RECONCIL_ERROR = 20;
    public static int TERMINAL_AUTOREGISTRATION_ERROR = 21;
    public static int FIND_TRANSACTION_ERROR = 22;
    public static int NO_VOUCHER_FOUND = 23;
    public static int TRANSACTION_CANCELED = 50;
    public static int ACCOUNT_TIMEOUT = 52;
    /** Status used to test errors handling by billing engine */
    public static int TEST_ERROR = 51;
    public static int INCORRECT_AMOUNT = 900;
    
    // https gate statuses
    public static int UNKNOWN_ERROR = 200;
    public static int BEGIN_TRANSACTION_ERROR = 201;
    public static int QUERY_PAYMENT_ERROR = 202;
    public static int SUBMIT_TRANSACTION_ERROR = 203;
    public static int CANCEL_TRANSACTION_ERROR = 250;
    public static int UNSUPPORTED_COMMAND = 204;
    public static int TRANSACTION_NOT_FOUND = 205;
    public static int REGION_NOT_FOUND = 206;
    //https gate status for UMC
    public static int MSISDN_NOT_VALID = 207;
    public static int FAILED_TO_CHECK_MSISDN = 208;
    public static int REQUEST_PAYMENT_ERROR = 209;
    public static int CONFIRM_PAYMENT_ERROR = 210;
    //avancel gate statuses
    public static int AVANCEL_AUTHORIZATION_ERROR = 501;//balance not changed 
    public static int AVANCEL_REFILL_ERROR = 502;//balance not changed
    public static int AVANCEL_GETSTATUS_ERROR = 503;
    public static int AVANCEL_GETLASTRESPONSE_ERROR = 504;
    public static int AVANCEL_TRANSACTION_PROCESSED_BY_GATE = 505;//still in processing by our Avancel gate. Balance may be changed
    public static int AVANCEL_OPERATOR_FAILED_TRANSACTION = 506;//balance not changed
    public static int AVANCEL_TRANSACTION_PROCESSED_BY_PARTNER = 507;//still in processing
    public static int AVANCEL_NO_ENOUGH_MONEY = 508;//balance not changed
    public static int AVANCEL_NO_PRICES = 509;//balance not changed
    public static int AVANCEL_INCORRECT_PRICES = 510;//balance not changed
    public static int AVANCEL_NO_REQUESTED_ARTICLE = 511;//balance not changed
    public static int AVANCEL_INCORRECT_REFILL_AMOUNT = 512;//balance not changed
    public static int AVANCEL_SERVICE_UNAVAILABLE = 513;//balance not changed
    public static int AVANCEL_INCORRECT_MSISDN = 514;//balance not changed
    public static int AVANCEL_UNCONFIRMED_OK = 515;//balance changed, but not confirmed!    BusinessStatus = in_progress
    //life (npay) statuses
    public static int OTM_SUBSCRIBER_INQUIRY_ERROR = 601; // 'subscriberInquiry' error
    public static int OTM_VOUCHER_SALE_AND_ACTIVATION_ERROR = 602; // 'voucherSaleAndActivation' error
    public static int OTM_REVERSAL_ERROR = 603; // 'reversal' error
    public static int OTM_REVERSAL_COMPLETED = 604; // 'reversal' finished successfully
    public static int OTM_PARTIAL_VOUCHER_SALE = 605; // 'partialVoucherSale' error
    //dacard statuses
    public static int PARTNER_ERROR = 701; //balance not changed






    // CyberPlat Errors
    public static int CYBERPLAT_OK = 0;
    public static int CYBERPLAT_1 = 1;
    public static int CYBERPLAT_2 = 2;
    public static int CYBERPLAT_3 = 3;
    public static int CYBERPLAT_4 = 4;
    public static int CYBERPLAT_5 = 5;
    public static int CYBERPLAT_6 = 6;
    public static int CYBERPLAT_7 = 7;
    public static int CYBERPLAT_8 = 8;
    public static int CYBERPLAT_9 = 9;
    public static int CYBERPLAT_10 = 10;
    public static int CYBERPLAT_11 = 11;
    public static int CYBERPLAT_12 = 12;
    public static int CYBERPLAT_13 = 13;
    public static int CYBERPLAT_15 = 15;
    public static int CYBERPLAT_17 = 17;
    public static int CYBERPLAT_18 = 18;
    public static int CYBERPLAT_19 = 19;
    public static int CYBERPLAT_20 = 20;
    public static int CYBERPLAT_21 = 21;
    public static int CYBERPLAT_22 = 22;
    public static int CYBERPLAT_23 = 23;
    public static int CYBERPLAT_223 = 223;
    public static int CYBERPLAT_24 = 24;
    public static int CYBERPLAT_25 = 25;
    public static int CYBERPLAT_26 = 26;
    public static int CYBERPLAT_27 = 27;
    public static int CYBERPLAT_30 = 30;
    public static int CYBERPLAT_31 = 31;
    public static int CYBERPLAT_32 = 32;
    public static int CYBERPLAT_33 = 33;
    public static int CYBERPLAT_34 = 34;
    public static int CYBERPLAT_35 = 35;
    public static int CYBERPLAT_36 = 36;
    public static int CYBERPLAT_37 = 37;
    public static int CYBERPLAT_38 = 38;
    public static int CYBERPLAT_39 = 39;
    public static int CYBERPLAT_40 = 40;
    public static int CYBERPLAT_41 = 41;
    public static int CYBERPLAT_42 = 42;
    public static int CYBERPLAT_43 = 43;
    public static int CYBERPLAT_44 = 44;
    public static int CYBERPLAT_45 = 45;
    public static int CYBERPLAT_46 = 46;
    public static int CYBERPLAT_47 = 47;
    public static int CYBERPLAT_48 = 48;
    public static int CYBERPLAT_50 = 50;
    public static int CYBERPLAT_51 = 51;
    public static int CYBERPLAT_52 = 52;
    public static int CYBERPLAT_53 = 53;
    public static int CYBERPLAT_54 = 54;
    public static int CYBERPLAT_55 = 55;
    public static int CYBERPLAT_56 = 56;
    public static int CYBERPLAT_57 = 57;
    public static int CYBERPLAT_81 = 81;
    public static int CYBERPLAT_82 = 82;








    public static String resolveStatusMessage(int code){
        return getLocalizedMessage("status."+code, null);
    }


    private static String getLocalizedMessage(String key, String[] params, Locale locale){
        return MessageResources.getInstance().getOwnedLocalizedMessage(StatusDictionary.class, STATUS_MSGS_BUNDLE, locale, key, params);
    }

    private static String getLocalizedMessage(String key, String[] params){
        return getLocalizedMessage(key, params, 
                new Locale(System.getProperty("default_locale", Locale.ENGLISH.getLanguage())));
    }

    /**
     * Method returns all regestry statuses
     * @return statuses
     */
    public static List<StatusObject> getStatuses() {
        final String regexp = "^\\?\\?\\?(.+)\\?\\?\\?$";
        List<StatusObject> statuses = new ArrayList<StatusObject>();

        for (int i = 0; i < 220; i++) {
            if (!resolveStatusMessage(i).matches(regexp)) {
                StatusObject statusObject = new StatusObject(i, resolveStatusMessage(i));
                statuses.add(statusObject);
            }
        }
        return statuses;
    }

    public static class StatusObject {
        private int key;
        private String value;

        public StatusObject(int key, String value) {
            this.key = key;
            this.value = value;
        }

        public Integer getKey() {
            return key;
        }

        public void setKey(int key) {
            this.key = key;
        }

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }
    }
}

