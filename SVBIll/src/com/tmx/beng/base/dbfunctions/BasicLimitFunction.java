package com.tmx.beng.base.dbfunctions;

import com.tmx.beng.base.cache.InvokeFunctionException;

import java.util.Map;

/**
    Basic abstract limit function
 */
public abstract class BasicLimitFunction extends BasicFunction{

    abstract void checkLimit(Map actualParams) throws InvokeFunctionException ;

    final public void invoke(Map actualParams) throws InvokeFunctionException {
        checkLimit(actualParams);
    }
}
