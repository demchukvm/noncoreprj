package com.tmx.beng.iso8583Gate.mapper;

import java.util.Map;
import java.util.HashMap;

/**
 */
public class CrossParam {
    private String mappedParamName;
    private String sourceParamName;
    private Map valueMapping;

    public CrossParam() {
        this.valueMapping = new HashMap();
    }

    public String getMappedParamName() {
        return mappedParamName;
    }

    public void setMappedParamName(String mappedParamName) {
        this.mappedParamName = mappedParamName;
    }

    public String getSourceParamName() {
        return sourceParamName;
    }

    public void setSourceParamName(String sourceParamName) {
        this.sourceParamName = sourceParamName;
    }

    public Map getValueMapping() {
        return valueMapping;
    }

    public void setValueMapping(Map valueMapping) {
        this.valueMapping = valueMapping;
    }

    public void addValueMapping(String originalVal, String mappedVal){
        valueMapping.put(originalVal, mappedVal);
    }

    public String getMappedValue(String originalVal){
        String mappedValue = null;
        if(originalVal != null){
            mappedValue = (String)valueMapping.get(originalVal);
            mappedValue = mappedValue != null ? mappedValue : originalVal;
        }
        return mappedValue;
    }
}
