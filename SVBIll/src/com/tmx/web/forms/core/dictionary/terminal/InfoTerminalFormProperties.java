package com.tmx.web.forms.core.dictionary.terminal;

import com.tmx.web.base.BasicActionForm;
import com.tmx.web.controls.*;
import com.tmx.as.entities.bill.seller.Seller;
import com.tmx.as.entities.bill.terminal.EquipmentType;
import com.tmx.as.entities.bill.trade_putlet.TradePutletType;


public class InfoTerminalFormProperties extends BasicActionForm {//ID
    private Long selectedTerminalId;

    private Long managerId;
    private Long directorId;
    private Long tradePutletId;
    private Long tradePutletAddressId;

    private CheckBox checkBoxBlocked;
    private CheckBox checkBoxIsAttachRestriction;
    private CheckBox checkBoxIsAttachTariffOnline;
    private CheckBox checkBoxIsAttachTariffVouchers;

    private TimeSelector timeSelectorRegistrationDate;

    private EditBox editBoxTerminalId;
    private EditBox editBoxSerialNumber;
    private EditBox editBoxHardwareSerialNumber;
    private EditBox editBoxRegistrationDate;
    private EditBox editBoxLogin;
    private EditBox editBoxPassword;
    private EditBox editBoxPassword2;
//    private EditBox editBoxAddressId = new EditBox();
    private EditBox editBoxAddress;
    private EditBox editBoxRegion;
    private EditBox editBoxCity;
    private EditBox editBoxBuilding;
    private EditBox editBoxTradePutlet;
    private EditBox editBoxDirectorEmail;
    private EditBox editBoxDirectorPhone;
    private EditBox editBoxManagerEmail;
    private EditBox editBoxManagerPhone;

    private ScrollBox scrollBoxTradePutletType;
    private ScrollBox scrollBoxSellers;
    private ScrollBox scrollBoxEqTypes;

    //Button----------------------------------
    private Button onApplyButton;
    private Button onSaveButton;
    private Button onResetButton;
    private Button onCancelButton;

    protected void init() {
        initIds();
        initEditBoxes();
        initTimeSelectors();
        initCheckBoxes();
        initScrollBoxes();
        initButtons();
    }

    private void initIds() {
        managerId = null;
        directorId = null;
        tradePutletId = null;
        tradePutletAddressId = null;    
    }

    private void initScrollBoxes(){
        setScrollBoxSellers(new ScrollBox() {
            protected String getKey(Object dataListElement) {
                return ((Seller) dataListElement).getSellerId().toString();
            }

            protected String getValue(Object dataListElement) {
                return ((Seller) dataListElement).getName();
            }
        });
        getScrollBoxSellers().setMandatory(true);
        getScrollBoxSellers().setNullable(true);

        setScrollBoxEqTypes(new ScrollBox() {
            protected String getKey(Object dataListElement) {
                return ((EquipmentType) dataListElement).getEquipmentTypeId().toString();
            }

            protected String getValue(Object dataListElement) {
                return ((EquipmentType) dataListElement).getName();
            }
        });
        getScrollBoxEqTypes().setNullable(true);

        setScrollBoxTradePutletType(new ScrollBox() {
            protected String getKey(Object dataListElement) {
                return ((TradePutletType) dataListElement).getTradePutletTypeId().toString();
            }

            protected String getValue(Object dataListElement) {
                return ((TradePutletType) dataListElement).getName();
            }
        });
        getScrollBoxTradePutletType().setNullable(true);

        

    }

    private void initButtons() {
        setOnSaveButton(new Button());
        getOnSaveButton().setLabelKey("ctrl.button.label.save");
        getOnSaveButton().setAction("/core/dictionary/terminal/infoTerminal");
        getOnSaveButton().setCommand("save");

        setOnResetButton(new Button());
        getOnResetButton().setLabelKey("ctrl.button.label.reset");

        setOnCancelButton(new Button());
        getOnCancelButton().setLabelKey("ctrl.button.label.cancel");
//        getOnCancelButton().setAction("/core/dictionary/terminal/manageTerminals");

        /*
        setOnApplyButton(new Button());
        getOnApplyButton().setLabelKey("ctrl.button.label.apply");
        getOnApplyButton().setAction("/core/dictionary/terminal/infoTerminal");
        getOnApplyButton().setCommand("apply");
        */
    }

    private void initCheckBoxes() {
        setCheckBoxBlocked(new CheckBox());
        getCheckBoxBlocked().setSelected(false);

        setCheckBoxIsAttachRestriction(new CheckBox());
        getCheckBoxIsAttachRestriction().setSelected(false);

        checkBoxIsAttachTariffOnline = new CheckBox();
        checkBoxIsAttachTariffOnline.setSelected(false);

        checkBoxIsAttachTariffVouchers = new CheckBox();
        checkBoxIsAttachTariffVouchers.setSelected(false);
    }

    private void initTimeSelectors() {
        setTimeSelectorRegistrationDate(new TimeSelector());
    }

    private void initEditBoxes() {
        setEditBoxAddress(new EditBox());
        setEditBoxRegion(new EditBox());
        setEditBoxCity(new EditBox());
        setEditBoxBuilding(new EditBox());
        setEditBoxTradePutlet(new EditBox());
        setEditBoxDirectorEmail(new EditBox());
        setEditBoxDirectorPhone(new EditBox());
        setEditBoxManagerEmail(new EditBox());
        setEditBoxManagerPhone(new EditBox());
        setEditBoxTerminalId(new EditBox());
        
        getEditBoxTerminalId().setReadonly( true );

        editBoxSerialNumber = new EditBox();
        editBoxSerialNumber.setMandatory(true);
        editBoxSerialNumber.addValidationRule(EditBox.createValidationRule_NOT_NULL());

        setEditBoxHardwareSerialNumber(new EditBox());

        setEditBoxRegistrationDate(new EditBox());
        getEditBoxRegistrationDate().setReadonly( true );
        setEditBoxLogin(new EditBox());

        setEditBoxPassword(new EditBox());
        setEditBoxPassword2(new EditBox());
        getEditBoxPassword2().addValidationRule(EditBox.createValidationRule_PASSWD_FIELDS_MATCH(getEditBoxPassword()));
    }

    public Long getSelectedTerminalId() {
        return selectedTerminalId;
    }

    public void setSelectedTerminalId(Long selectedTerminalId) {
        this.selectedTerminalId = selectedTerminalId;
    }

    public EditBox getEditBoxTerminalId() {
        return editBoxTerminalId;
    }

    public void setEditBoxTerminalId(EditBox editBoxTerminalId) {
        this.editBoxTerminalId = editBoxTerminalId;
    }

    public CheckBox getCheckBoxBlocked() {
        return checkBoxBlocked;
    }

    public void setCheckBoxBlocked(CheckBox checkBoxBlocked) {
        this.checkBoxBlocked = checkBoxBlocked;
    }

    public EditBox getEditBoxSerialNumber() {
        return editBoxSerialNumber;
    }

    public void setEditBoxSerialNumber(EditBox editBoxSerialNumber) {
        this.editBoxSerialNumber = editBoxSerialNumber;
    }

    public EditBox getEditBoxHardwareSerialNumber() {
        return editBoxHardwareSerialNumber;
    }

    public void setEditBoxHardwareSerialNumber(EditBox editBoxHardwareSerialNumber) {
        editBoxHardwareSerialNumber.setMandatory(true);
        this.editBoxHardwareSerialNumber = editBoxHardwareSerialNumber;
    }

    public EditBox getEditBoxRegistrationDate() {
        return editBoxRegistrationDate;
    }

    public void setEditBoxRegistrationDate(EditBox editBoxRegistrationDate) {
        this.editBoxRegistrationDate = editBoxRegistrationDate;
    }

    public EditBox getEditBoxLogin() {
        return editBoxLogin;
    }

    public void setEditBoxLogin(EditBox editBoxLogin) {
        this.editBoxLogin = editBoxLogin;
    }

    public EditBox getEditBoxPassword() {
        return editBoxPassword;
    }

    public void setEditBoxPassword(EditBox editBoxPassword) {
        this.editBoxPassword = editBoxPassword;
    }

    public EditBox getEditBoxPassword2() {
        return editBoxPassword2;
    }

    public void setEditBoxPassword2(EditBox editBoxPassword2) {
        this.editBoxPassword2 = editBoxPassword2;
    }

    public Button getOnApplyButton() {
        return onApplyButton;
    }

    public void setOnApplyButton(Button onApplyButton) {
        this.onApplyButton = onApplyButton;
    }

    public Button getOnSaveButton() {
        return onSaveButton;
    }

    public void setOnSaveButton(Button onSaveButton) {
        this.onSaveButton = onSaveButton;
    }

    public Button getOnResetButton() {
        return onResetButton;
    }

    public void setOnResetButton(Button onResetButton) {
        this.onResetButton = onResetButton;
    }

    public Button getOnCancelButton() {
        return onCancelButton;
    }

    public void setOnCancelButton(Button onCancelButton) {
        this.onCancelButton = onCancelButton;
    }

    public TimeSelector getTimeSelectorRegistrationDate() {
        return timeSelectorRegistrationDate;
    }

    public void setTimeSelectorRegistrationDate(TimeSelector timeSelectorRegistrationDate) {
        this.timeSelectorRegistrationDate = timeSelectorRegistrationDate;
    }

    public ScrollBox getScrollBoxSellers() {
        return scrollBoxSellers;
    }

    public void setScrollBoxSellers(ScrollBox scrollBoxSellers) {
        this.scrollBoxSellers = scrollBoxSellers;
    }

    public ScrollBox getScrollBoxEqTypes() {
        return scrollBoxEqTypes;
    }

    public void setScrollBoxEqTypes(ScrollBox scrollBoxEqTypes) {
        this.scrollBoxEqTypes = scrollBoxEqTypes;
    }

    public EditBox getEditBoxAddress() {
        return editBoxAddress;
    }

    public void setEditBoxAddress(EditBox editBoxAddress) {
        this.editBoxAddress = editBoxAddress;
    }

    public EditBox getEditBoxRegion() {
        return editBoxRegion;
    }

    public void setEditBoxRegion(EditBox editBoxRegion) {
        this.editBoxRegion = editBoxRegion;
    }

    public EditBox getEditBoxCity() {
        return editBoxCity;
    }

    public void setEditBoxCity(EditBox editBoxCity) {
        this.editBoxCity = editBoxCity;
    }

    public EditBox getEditBoxBuilding() {
        return editBoxBuilding;
    }

    public void setEditBoxBuilding(EditBox editBoxBuilding) {
        this.editBoxBuilding = editBoxBuilding;
    }

    public EditBox getEditBoxTradePutlet() {
        return editBoxTradePutlet;
    }

    public void setEditBoxTradePutlet(EditBox editBoxTradePutlet) {
        this.editBoxTradePutlet = editBoxTradePutlet;
    }

    public ScrollBox getScrollBoxTradePutletType() {
        return scrollBoxTradePutletType;
    }

    public void setScrollBoxTradePutletType(ScrollBox scrollBoxTradePutletType) {
        this.scrollBoxTradePutletType = scrollBoxTradePutletType;
    }

    public EditBox getEditBoxDirectorEmail() {
        return editBoxDirectorEmail;
    }

    public void setEditBoxDirectorEmail(EditBox editBoxDirectorEmail) {
        this.editBoxDirectorEmail = editBoxDirectorEmail;
    }

    public EditBox getEditBoxDirectorPhone() {
        return editBoxDirectorPhone;
    }

    public void setEditBoxDirectorPhone(EditBox editBoxDirectorPhone) {
        this.editBoxDirectorPhone = editBoxDirectorPhone;
    }

    public EditBox getEditBoxManagerEmail() {
        return editBoxManagerEmail;
    }

    public void setEditBoxManagerEmail(EditBox editBoxManagerEmail) {
        this.editBoxManagerEmail = editBoxManagerEmail;
    }

    public EditBox getEditBoxManagerPhone() {
        return editBoxManagerPhone;
    }

    public void setEditBoxManagerPhone(EditBox editBoxManagerPhone) {
        this.editBoxManagerPhone = editBoxManagerPhone;
    }

    public Long getManagerId() {
        return managerId;
    }

    public void setManagerId(Long managerId) {
        this.managerId = managerId;
    }

    public Long getDirectorId() {
        return directorId;
    }

    public void setDirectorId(Long directorId) {
        this.directorId = directorId;
    }

    public Long getTradePutletId() {
        return tradePutletId;
    }

    public void setTradePutletId(Long tradePutletId) {
        this.tradePutletId = tradePutletId;
    }

    public Long getTradePutletAddressId() {
        return tradePutletAddressId;
    }

    public void setTradePutletAddressId(Long tradePutletAddressId) {
        this.tradePutletAddressId = tradePutletAddressId;
    }

    public CheckBox getCheckBoxIsAttachRestriction() {
        return checkBoxIsAttachRestriction;
    }

    public void setCheckBoxIsAttachRestriction(CheckBox checkBoxIsAttachRestriction) {
        this.checkBoxIsAttachRestriction = checkBoxIsAttachRestriction;
    }

    public CheckBox getCheckBoxIsAttachTariffOnline() {
        return checkBoxIsAttachTariffOnline;
    }

    public void setCheckBoxIsAttachTariffOnline(CheckBox checkBoxIsAttachTariffOnline) {
        this.checkBoxIsAttachTariffOnline = checkBoxIsAttachTariffOnline;
    }

    public CheckBox getCheckBoxIsAttachTariffVouchers() {
        return checkBoxIsAttachTariffVouchers;
    }

    public void setCheckBoxIsAttachTariffVouchers(CheckBox checkBoxIsAttachTariffVouchers) {
        this.checkBoxIsAttachTariffVouchers = checkBoxIsAttachTariffVouchers;
    }
}
