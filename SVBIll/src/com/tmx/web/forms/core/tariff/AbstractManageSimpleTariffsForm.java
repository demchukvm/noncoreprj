package com.tmx.web.forms.core.tariff;

import com.tmx.as.blogic.BlogicException;
import com.tmx.as.entities.general.user.User;
import com.tmx.as.exceptions.DatabaseException;
import com.tmx.web.controls.Button;
import com.tmx.web.controls.EditBox;
import com.tmx.web.controls.table.*;

import java.util.Date;

/**
 * for imposible terminals tariffs form
 */
abstract public class AbstractManageSimpleTariffsForm extends AbstractManageTariffsForm{

    private ControlManagerTable incomingTriffsTable;

    private Button onCopyButton, onCopyAllButton;
    private Button onCreateITButton, onEditITButton, onDeleteITButton;//for incoming tariff(only for superuser)
    private Long incomingTariffId;
    private Long tariffId;

    abstract public void copySelectedTariff() throws BlogicException, DatabaseException;// copy from incoming tariffs to subsellers tariffs

    abstract public void copyAllTariffs(User loggeInUser) throws BlogicException, DatabaseException;// copy all from incoming tariffs to sub sellers tariffs

    protected void init(){
        super.init();
        initButtons();
        initTables();
    }

    private void initButtons() {
        onCopyButton = new Button();
        onCopyButton.setLabelKey("ctrl.button.label.copy");
        onCopyButton.setCommand("preloadForCopy");

        onCopyAllButton = new Button();
        onCopyAllButton.setLabelKey("ctrl.button.label.copy_all");
        onCopyAllButton.setCommand("preloadForCopyAll");

        onCreateITButton = new Button();
        onCreateITButton.setLabelKey("ctrl.button.label.create");
        onCreateITButton.setCommand("onCreateIncomingTariffAction");

        onEditITButton = new Button();
        onEditITButton.setLabelKey("ctrl.button.label.edit");
        onEditITButton.setCommand("preloadForUpdateIT");

        onDeleteITButton = new Button();
        onDeleteITButton.setLabelKey("ctrl.button.label.delete");
        onDeleteITButton.setCommand("preloadForDeleteIT");
    }

    private void initTables(){
        incomingTriffsTable = new ControlManagerTable();
        incomingTriffsTable.addParameterToFilter("by_tariff_name", new Parameter("tariff_name", new EditBox()));

        Filter filter = new Filter("by_active_tariff", new Parameter("current_date", new Date()));

        /*****************************************/
        CheckBoxFilterEnabler isActive = new CheckBoxFilterEnabler();
        isActive.setSelected(true);
        filter.setFilterEnabler(isActive);
        /*****************************************/
        
       // filter.setFilterEnabler(new CheckBoxFilterEnabler());
        incomingTriffsTable.addFilter(filter);

        
    }

    public ControlManagerTable getIncomingTriffsTable() {
        return incomingTriffsTable;
    }

    public void setIncomingTriffsTable(ControlManagerTable incomingTriffsTable) {
        this.incomingTriffsTable = incomingTriffsTable;
    }

    public Button getOnCopyButton() {
        return onCopyButton;
    }

    public void setOnCopyButton(Button onCopyButton) {
        this.onCopyButton = onCopyButton;
    }

    public Button getOnCopyAllButton() {
        return onCopyAllButton;
    }

    public void setOnCopyAllButton(Button onCopyAllButton) {
        this.onCopyAllButton = onCopyAllButton;
    }

    public Long getIncomingTariffId() {
        return incomingTariffId;
    }

    public void setIncomingTariffId(Long incomingTariffId) {
        this.incomingTariffId = incomingTariffId;
    }

    public Long getTariffId() {
        return tariffId;
    }

    public void setTariffId(Long tariffId) {
        this.tariffId = tariffId;
    }

    public Button getOnCreateITButton() {
        return onCreateITButton;
    }

    public void setOnCreateITButton(Button onCreateITButton) {
        this.onCreateITButton = onCreateITButton;
    }

    public Button getOnEditITButton() {
        return onEditITButton;
    }

    public void setOnEditITButton(Button onEditITButton) {
        this.onEditITButton = onEditITButton;
    }

    public Button getOnDeleteITButton() {
        return onDeleteITButton;
    }

    public void setOnDeleteITButton(Button onDeleteITButton) {
        this.onDeleteITButton = onDeleteITButton;
    }
}
