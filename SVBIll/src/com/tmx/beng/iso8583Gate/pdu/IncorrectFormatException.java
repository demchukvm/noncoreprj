package com.tmx.beng.iso8583Gate.pdu;

import com.tmx.util.StructurizedException;

import java.util.Locale;

/**
 */
public class IncorrectFormatException extends StructurizedException {

    public IncorrectFormatException(String msg, String[] params, Throwable e, Locale locale) {
        super(msg, params, e, locale);
    }

    public IncorrectFormatException(String msg, String[] params) {
        super(msg, params);
    }

    public IncorrectFormatException(String msg, String[] params, Locale locale) {
        super(msg, params, locale);
    }

    public IncorrectFormatException(String msg, Throwable e, Locale locale) {
        super(msg, e, locale);
    }

    public IncorrectFormatException(String msg, Throwable e) {
        super(msg, e);
    }

    public IncorrectFormatException(String msg, Locale locale) {
        super(msg, locale);
    }

    public IncorrectFormatException(String msg) {
        super(msg);
    }

    public IncorrectFormatException(String msg, String[] params, Throwable e) {
        super(msg, params, e);
    }
}
