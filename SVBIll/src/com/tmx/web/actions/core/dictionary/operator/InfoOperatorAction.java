package com.tmx.web.actions.core.dictionary.operator;

import com.tmx.web.actions.core.AbstractSvbillAction;
import com.tmx.web.forms.core.dictionary.seller.InfoSellerForm;
import com.tmx.web.forms.core.dictionary.operator.InfoOperatorForm;
import com.tmx.web.base.WebException;
import com.tmx.as.entities.bill.seller.Seller;
import com.tmx.as.entities.bill.bank.BankAccount;
import com.tmx.as.entities.bill.operator.Operator;
import com.tmx.as.blogic.SellerManager;
import com.tmx.as.blogic.OperatorManager;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForm;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: �������������
 * Date: 23.01.2008
 * Time: 14:46:56
 * To change this template use File | Settings | File Templates.
 */
public class InfoOperatorAction extends AbstractSvbillAction {
     private static final String FORWARD_MANAGE_OPERATORS_ACTION = "manageOperatorsAction";
    private static final String FORWARD_INFO_OPERATOR_PAGE = "infoOperatorPage";
    private static final String FORWARD_MANAGE_DELETE_ACTION = "manageDeleteAction";


    public ActionForward preloadForCreate(ActionMapping mapping,
                                          ActionForm form,
                                          HttpServletRequest request,
                                          HttpServletResponse response) throws Throwable {
        getInfoOperatorForm(form).reset();
        getInfoOperatorForm(form).getEditBoxOperatorId().setReadonly(true);
        getInfoOperatorForm(form).getTimeSelectorRegistrationDate().setReadonly(true);
        return mapping.findForward(FORWARD_INFO_OPERATOR_PAGE);
    }

    public ActionForward save(ActionMapping mapping,
                              ActionForm form,
                              HttpServletRequest request,
                              HttpServletResponse response) throws Throwable {
        InfoOperatorForm operatorForm = getInfoOperatorForm(form);
        Operator operator = operatorForm.populateOperator();
        new OperatorManager().saveOperator(operator);
        operatorForm.getEditBoxOperatorId().setValue(operator.getOperatorId().toString());
        
        return mapping.findForward(FORWARD_MANAGE_OPERATORS_ACTION);
    }

    public ActionForward apply(ActionMapping mapping,
                              ActionForm form,
                              HttpServletRequest request,
                              HttpServletResponse response) throws Throwable {
        save(mapping, form, request, response);
        return mapping.findForward(FORWARD_INFO_OPERATOR_PAGE); 
    }

    public ActionForward preloadForUpdate(ActionMapping mapping,
                                          ActionForm form,
                                          HttpServletRequest request,
                                          HttpServletResponse response) throws Throwable {
        Operator operator = new OperatorManager().getOperator( getInfoOperatorForm(form).getSelectedOperatorId() );
        getInfoOperatorForm(form).reset();
        getInfoOperatorForm(form).bindData(operator , getLoggedInUser(request));
        getInfoOperatorForm(form).getEditBoxOperatorId().setReadonly(true);
        getInfoOperatorForm(form).getTimeSelectorRegistrationDate().setReadonly(true);
        return mapping.findForward(FORWARD_INFO_OPERATOR_PAGE);
    }

     public ActionForward preloadForDelete(ActionMapping mapping,
                                          ActionForm form,
                                          HttpServletRequest request,
                                          HttpServletResponse response) throws Throwable {
        getRoadmap().collectParameter("/core/message/confirmation", "leftMenuName", "dictionaries");
        getRoadmap().setPageForward("confirmation","onOk","local:"+this.getName()+"#"+FORWARD_MANAGE_DELETE_ACTION);
        getRoadmap().setPageForward("confirmation","onCancel","local:"+this.getName()+"#"+ FORWARD_MANAGE_OPERATORS_ACTION);
        System.out.println("ManageOperatorsAction.preloadForDelete");
        return mapping.findForward("confirmation");
    }

    public ActionForward delete(ActionMapping mapping,
                              ActionForm form,
                              HttpServletRequest request,
                              HttpServletResponse response) throws Throwable {
        new OperatorManager().deleteOperator( getInfoOperatorForm(form).getSelectedOperatorId() );
        return mapping.findForward(FORWARD_MANAGE_OPERATORS_ACTION);
    }
    // PRIVATE METHODS-----------------------------------------------------------------
    private InfoOperatorForm getInfoOperatorForm(ActionForm form) throws WebException {
        return (InfoOperatorForm) verifyForm(form, InfoOperatorForm.class);
    }
}
