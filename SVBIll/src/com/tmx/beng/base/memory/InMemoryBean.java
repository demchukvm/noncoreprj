package com.tmx.beng.base.memory;

import com.tmx.util.StringUtil;

import java.lang.reflect.Method;
import java.lang.reflect.InvocationTargetException;

/**

 */
public abstract class InMemoryBean implements InMemoryObject{

    public Object getValue(String propertyName){
        try{
            String getMethodName = StringUtil.getGetterName(propertyName);
            Method method = getClass().getMethod(getMethodName, null);
            return method.invoke(this, null);
        }
        catch(NoSuchMethodException e){
            //throw new InMemoryException("err.in_memory_bean.no_such_getter", e);
            return null;//on erorr
        }
        catch(IllegalAccessException e){
            //throw new InMemoryException("err.in_memory_bean.illegal_getter_error", e);
            return null;//on erorr
        }
        catch(InvocationTargetException e){
            //throw new InMemoryException("err.in_memory_bean.getter_call_error", e);
            return null;//on erorr
        }
    }
}
