package com.tmx.beng.httpsgate.kyivstar;

import org.dom4j.*;

import java.util.Iterator;
import java.util.Date;
import java.text.SimpleDateFormat;
import java.text.ParseException;

import com.tmx.beng.httpsgate.BadResponseException;


/**
 * Kyivstar response. Parses XML to java model
 */
public class KyivstarResponse {
    private Long statusCode = null;
    //private String statusCode = "";
    private Long payStatus = null;
    //private String payStatus = "";
    private Long payId = null;
    private String receipt = null;
    private Date timeStamp = null;
    private Date cts = null;
    private String name = null;
    private Double balance = null;
    private Double quota = null;
    private String account = null;

    private static final String TIMESTAMP_FORMAT = "dd.MM.yyyy HH:mm:ss";

    public static KyivstarResponse parseResponse(String response)throws BadResponseException {
        KyivstarResponse kyivstarResponse = new KyivstarResponse();
        try{
            Document doc = DocumentHelper.parseText(response.trim());
            Element root = doc.getRootElement();
            if(!root.getName().equalsIgnoreCase("pay-response")){
                throw new BadResponseException("Root root is not pay-response");
            }

            for(Iterator it = root.elementIterator(); it.hasNext(); ){
                Element element = (Element)it.next();

                // statusCode
                if(element.getName().equalsIgnoreCase("status_code")){
                    String statusCode = element.getTextTrim();
                    if(statusCode.contains("-"))
                    {
                        statusCode = statusCode.replace("-","");
                        statusCode = statusCode+"00";
                    }
                    kyivstarResponse.statusCode = new Long(statusCode);
                    //kyivstarResponse.statusCode = element.getTextTrim();

                    //kyivstarResponse.statusCode = new Long(element.getTextTrim());
                    continue;
                }

                // payStatus
                if(element.getName().equalsIgnoreCase("pay_status")){
                    String payStatus = element.getTextTrim();
                    if(payStatus.contains("-"))
                    {
                        payStatus = payStatus.replace("-","");
                        payStatus = payStatus+"00";
                    }
                    kyivstarResponse.payStatus = new Long(payStatus);
                    //kyivstarResponse.payStatus = element.getTextTrim();

                    //kyivstarResponse.payStatus = new Long(element.getTextTrim());
                    continue;
                }

                // pay_id
                if(element.getName().equalsIgnoreCase("pay_id")){
                    kyivstarResponse.payId = new Long(element.getTextTrim());
                    continue;
                }

                // receipt
                if(element.getName().equalsIgnoreCase("receipt")){
                    kyivstarResponse.receipt = element.getTextTrim();
                    continue;
                }

                // time_stamp
                if(element.getName().equalsIgnoreCase("time_stamp")){
                    kyivstarResponse.timeStamp = new SimpleDateFormat(TIMESTAMP_FORMAT).parse(element.getTextTrim());
                    continue;
                }

                // cts
                if(element.getName().equalsIgnoreCase("cts")){
                    kyivstarResponse.cts = new SimpleDateFormat(TIMESTAMP_FORMAT).parse(element.getTextTrim());
                    continue;
                }

                // name
                if(element.getName().equalsIgnoreCase("name")){
                    kyivstarResponse.name = element.getTextTrim();
                    continue;
                }

                // balance
                if(element.getName().equalsIgnoreCase("balance")){
                    kyivstarResponse.balance = new Double(element.getTextTrim());
                    continue;
                }

                // quota
                if(element.getName().equalsIgnoreCase("quota")){
                    kyivstarResponse.quota = new Double(element.getTextTrim());
                    continue;
                }

                // account
                if(element.getName().equalsIgnoreCase("account")){
                    kyivstarResponse.account = element.getTextTrim();
                }
            }

            if(kyivstarResponse.statusCode == null){
                throw new BadResponseException("Bad Response from server");
            }
        }catch(ParseException e){
            throw new BadResponseException(e.getMessage(), e);
        }catch(NumberFormatException e){
            throw new BadResponseException(e.getMessage(), e);
        }catch(DocumentException e){
            throw new BadResponseException("Parse beeline response error " + e.getMessage(), e);
        }
        return kyivstarResponse;
    }

    public Long getPayStatus() {
    //public String getPayStatus() {
        return payStatus;
    }

    public Double getQuota() {
        return quota;
    }

    public Long getStatusCode() {
    //public String getStatusCode() {
        return statusCode;
    }

    public Long getPayId() {
        return payId;
    }

    public String getReceipt() {
        return receipt;
    }

    public Date getTimeStamp() {
        return timeStamp;
    }

    public String getName() {
        return name;
    }

    public Double getBalance() {
        return balance;
    }

    public String getAccount() {
        return account;
    }

    public Date getCts() {
        return cts;
    }
}
