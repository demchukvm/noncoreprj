package com.tmx.web.actions.core.tariff.terminal_tariff;

import com.tmx.web.actions.core.tariff.AbstractManageTariffsAction;
import com.tmx.web.forms.core.tariff.terminal_tariff.ManageTerminalTariffsForm;
import com.tmx.web.forms.core.base.AbstractFormFactory;
import com.tmx.web.controls.BrowseBox;
import com.tmx.as.blogic.tariff.AbstractTariffManager;
import com.tmx.as.blogic.tariff.TerminalTariffManager;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForm;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class ManageTerminalTariffsAction extends AbstractManageTariffsAction {

    public static final String PARAMETER_OWNER_NAME = "terminalId";

    protected AbstractTariffManager getTariffManager() {
        return new TerminalTariffManager(); 
    }

    //roadmap setuping
    public ActionForward defaultAction(ActionMapping mapping,
                                       ActionForm form,
                                       HttpServletRequest request,
                                       HttpServletResponse response,
                                       String methodName) throws Throwable {
        getRoadmap().collectParameter(DEFAULT_VALID_ACTION, "onCancelAction", "/core/tariff/terminal_tariff/manageTerminalTariffs");
        getRoadmap().collectParameter(DEFAULT_VALID_ACTION, "onSaveAction", "manageTariffsAction");
        AbstractFormFactory.getInstance(getLoggedInUser(request)).setUpForm((ManageTerminalTariffsForm) form);
        return super.defaultAction(mapping, form, request, response, methodName);
    }

    public ActionForward onCreateTariffAction(ActionMapping mapping,
                                       ActionForm form,
                                       HttpServletRequest request,
                                       HttpServletResponse response) throws Throwable {
        ManageTerminalTariffsForm operatorTariffsForm = (ManageTerminalTariffsForm) form;
        String selectedKey = ((BrowseBox) operatorTariffsForm.getTariffsTable().getParameterControl("by_terminal_id", "terminal_id")).getKey();
        getRoadmap().collectParameter("/core/tariff/terminal_tariff/infoTerminalTariff", PARAMETER_OWNER_NAME, selectedKey);
        return mapping.findForward("infoTariffAction");
    }


}
