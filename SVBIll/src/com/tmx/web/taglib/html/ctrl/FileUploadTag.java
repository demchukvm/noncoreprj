package com.tmx.web.taglib.html.ctrl;

import com.tmx.web.controls.FileUpload;
import javax.servlet.jsp.JspException;

import org.apache.struts.taglib.TagUtils;

/**

 */
public class FileUploadTag extends AbstractControlTag {
    //CSS styles properties
    private String styleClass = null;
    private String readonlyStyleClass = null;

    //------------------------------Properties
    public String getStyleClass() {
        return (styleClass != null) ? styleClass : getDefaultProperty("tag.fileUpload.style.styleClass");
    }

    public void setStyleClass(String styleClass) {
        this.styleClass = styleClass;
    }

    public String getReadonlyStyleClass() {
        return (readonlyStyleClass != null) ? readonlyStyleClass : getDefaultProperty("tag.fileUpload.style.styleClassReadonly");
    }

    public void setReadonlyStyleClass(String readonlyStyleClass) {
        this.readonlyStyleClass = readonlyStyleClass;
    }


    //------------------------------Abstract methods
    protected int controlStartTag() throws JspException {
        StringBuffer resultHTML = new StringBuffer();
        FileUpload fileUpload = getFileUpload();
        if(fileUpload == null){
            TagUtils.getInstance().write(pageContext,"FileUpload '"+getProperty()+"' is not found");
            return SKIP_BODY;
        }

        String readonly = (fileUpload.isReadonly()) ? " readonly=\"true\" " : "";
        String value = " value=\""+((fileUpload.getUploadedFileName() == null) ? "" : fileUpload.getUploadedFileName()) +"\" ";
        String styleClass = " class=\""+((fileUpload.isReadonly()) ? getReadonlyStyleClass() : getStyleClass())+"\" ";
        String type = " type=\"file\" ";
        String name = "name=\""+getProperty()+"."+FileUpload.FILE_SUFFIX+"\"";
        //Render HTML
        resultHTML.append("\r");
        resultHTML.append("<input " + name + type + value + styleClass + readonly + renderCssStyleAttribute() + "/>\r");

        TagUtils.getInstance().write(pageContext,resultHTML.toString());
        return SKIP_BODY;

    }

   protected FileUpload getFileUpload() throws JspException{
        Object object = getPropertyObject();
        return (object != null && object instanceof FileUpload) ?
               (FileUpload)object : null;
   }
}
