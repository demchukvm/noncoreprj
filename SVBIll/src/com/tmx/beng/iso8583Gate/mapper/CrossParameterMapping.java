package com.tmx.beng.iso8583Gate.mapper;

import java.util.Map;
import java.util.HashMap;

/**
*/
public class CrossParameterMapping {
    /** originalName - > parameter */
    private Map valueMap = null;
    private String protocolVersion = null;

    public CrossParameterMapping(){
        valueMap = new HashMap();
    }

    public void addCrossParam(CrossParam val){
        valueMap.put(val.getMappedParamName(), val);
    }

    public String getProtocolVersion() {
        return protocolVersion;
    }

    public void setProtocolVersion(String protocolVersion) {
        this.protocolVersion = protocolVersion;
    }

    public CrossParam getExtValueByOriginalValue(String originalValue){
        return (CrossParam)valueMap.get(originalValue);
    }
}
