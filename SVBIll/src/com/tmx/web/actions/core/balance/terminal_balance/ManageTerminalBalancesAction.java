package com.tmx.web.actions.core.balance.terminal_balance;

import com.tmx.web.actions.core.balance.AbstractManageBalancesAction;
import com.tmx.web.forms.core.balance.terminal_balance.ManageTerminalBalancesForm;
import com.tmx.web.forms.core.balance.AbstractManageBalancesForm;
import com.tmx.web.controls.BrowseBox;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class ManageTerminalBalancesAction extends AbstractManageBalancesAction {

    protected String getManageForwardPage() {
        return "manageTerminalBalancesPage";
    }

 

    public ActionForward filter(ActionMapping mapping,
                               ActionForm form,
                               HttpServletRequest request,
                               HttpServletResponse response) throws Throwable {
        ManageTerminalBalancesForm manageTerminalBalancesForm = (ManageTerminalBalancesForm) verifyForm
                (form, AbstractManageBalancesForm.class);
        final BrowseBox browseBox = (BrowseBox) manageTerminalBalancesForm.getBalancesTable().getParameterControl("by_terminal_id", "terminal_id");
//        SellerManager sm = new SellerManager();
//        if (!manageTerminalBalancesForm.getScrollBoxSellerOwner().isNullKeySelected()) {
//            final Seller seller = sm.getSeller(PopulateUtil.getLongKey(manageTerminalBalancesForm.getScrollBoxSellerOwner()));
//            manageTerminalBalancesForm.getBalancesTable().setCriterionFactory(new CriterionFactory() {
//                public CriterionWrapper createCriterion() {
//                    if (browseBox.getKey() == null) {
//                        return Restrictions.eq("terminal.sellerOwner", seller);
//                    } else {
//                        return null;
//                    }
//                }
//            });
//        }
        return mapping.findForward(getManageForwardPage());
    }


}
