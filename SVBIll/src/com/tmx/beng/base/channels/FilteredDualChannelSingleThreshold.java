package com.tmx.beng.base.channels;

import com.tmx.beng.base.BillingMessage;
import com.tmx.beng.base.BillException;

/**
 This threshold is used like <code>DualChannelSingleThreshold</code>, but it concerns the filter by operation.
 This filter managed by parameter "operationFilter". Values of this parameter belong to the domain of
 operation names defined in <code>BillingMessage</code> constants. 
 */
public class FilteredDualChannelSingleThreshold extends DualChannelSingleThreshold {

    private final static String OPERATION_FILTER = "operationFilter";

    public OutChannel process(BillingMessage msg) throws BillException {
        if(msg.getOperationName() == null)
            throw new BillException("err.bill.null_pointer_operation_name_is_absent");

        try {
            return (getOperationFilter().equals(msg.getOperationName())) ?
                    getOutChannel(0) :
                    super.process(msg);
        }
        catch (NumberFormatException e) {
            throw new BillException("err.bill.cannot_parse_amount", e);
        }
    }

    //from parameters take operation name for filter
    private String getOperationFilter() throws BillException {
        if(getParamValue(OPERATION_FILTER) == null)
            throw new BillException("err.bill.paramter_operation_filter_is_not_set_for_threshold"); 

        return getParamValue(OPERATION_FILTER);
    }

}
