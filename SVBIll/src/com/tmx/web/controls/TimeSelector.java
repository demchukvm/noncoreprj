package com.tmx.web.controls;

import java.util.Date;
import java.text.SimpleDateFormat;
import java.text.ParseException;

/**
Control to handle date selection on the web
 */
public class TimeSelector extends BasicControl{
    private Date time = null;
    private Date firstUpdatedTime = null;
    /** Holds data inputed by user. Used for validation only. */
    private String enteredTimeString = null;
    private static String format = "yyyy-MM-dd HH:mm";
    /** Constants */
    //suffix to be used in HTML to auto populate property 'time'
    public static final String TIME_SUFFIX = "timeString";

    public void initialize() {
        //add date validation rule
        addValidationRule(createValidationRule_DATETIME_FORMAT());
    }

    public void safeReset() {
        time = firstUpdatedTime;
    }

    public Date getTime() {
        return time;
    }

    public String getTimeString(){
        return (time == null) ? "" : new SimpleDateFormat(format).format(time);
    }

    public void setTime(Date time) {
        if(!isUpdated()){
            firstUpdatedTime = time;
            notifyUpdate();
        }
        this.time = time;
    }

    /** Call on populatin from html form */
    public void setTimeString(String time){
        enteredTimeString = time;
        if(time == null || "".equals(time.trim())){
            this.time = null;
        }
        else{
            try{
                this.time = new SimpleDateFormat(format).parse(time);
            }
            catch(ParseException e){
                //this.time = new Date();
                this.time = this.time != null ? this.time : null;//reset date if format is incorrect and there is no old value                
            }
        }
    }

    public static ValidationRule createValidationRule_DATETIME_FORMAT() {

        return new ValidationRule("datetime_format", new Object[]{TimeSelector.format}) {
            public boolean validate(BasicControl control, Object[] args) {
                if(control instanceof TimeSelector &&
                   args != null && args[0] != null && args[0] instanceof String &&
                   ((TimeSelector) control).enteredTimeString != null &&
                   ((TimeSelector) control).enteredTimeString.trim().length() >0){
                    try{
                        new SimpleDateFormat(format).parse(((TimeSelector) control).enteredTimeString);
                    }
                    catch(ParseException e){
                        return false;
                    }
                }
                return true;
            }
        };
    }

    public static ValidationRule createValidationRule_START_END_DATES(TimeSelector endDate) {

        return new MultiControlValidationRule("start_end_dates", null, new BasicControl[]{endDate}) {
            public boolean validate(BasicControl control, Object[] args, BasicControl[] coupledCtrls) {
                if(control instanceof TimeSelector &&
                   coupledCtrls != null && coupledCtrls[0] != null && coupledCtrls[0] instanceof TimeSelector &&
                   ((TimeSelector) control).enteredTimeString != null && ((TimeSelector)coupledCtrls[0]).enteredTimeString != null){
                    try{
                        Date startDate = new SimpleDateFormat(format).parse(((TimeSelector) control).enteredTimeString);
                        Date endDate = new SimpleDateFormat(format).parse(((TimeSelector) coupledCtrls[0]).enteredTimeString);
                        if(endDate.before(startDate))
                            return false;
                    }
                    catch(ParseException e){
                        return true;//validate only date precedence, but format
                    }
                }
                return true;
            }
        };
    }
}
