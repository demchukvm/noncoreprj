package com.tmx.beng.base.processors;

import com.tmx.beng.base.Processor;
import org.apache.log4j.Logger;

/**

 */
public abstract class BasicProcessor implements Processor {
    protected Logger logger = Logger.getLogger("bill." + getClass());
    protected String configFile = null;

    public void setConfigFile(String configFile) {
        this.configFile = configFile;
    }

    public String getConfigFile() {
        return configFile;
    }
}
