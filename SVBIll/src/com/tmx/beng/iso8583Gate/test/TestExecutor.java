package com.tmx.beng.iso8583Gate.test;

import com.tmx.beng.iso8583Gate.pdu.RuntimeMessage;
import com.tmx.util.InitException;
import com.tmx.util.Configuration;
import com.tmx.util.StructurizedException;
import com.tmx.as.jndi.JndiService;
import com.tmx.as.jsse.JsseSimpleService;

import java.util.Date;

import org.apache.log4j.PropertyConfigurator;

/**

 */
public class TestExecutor {
    public static void main(String[] args) throws Throwable{
        try{
            TestExecutor testExecuter = new TestExecutor();
            testExecuter.init();
            testExecuter.work();
        }
        catch(StructurizedException e){
           System.out.println(e.getTraceDump());
        }
        catch(Throwable e){
           e.printStackTrace();
        }

    }


    public void work(){
        for(int i=0; i<1; i++){
            try{
                ISO8583Client client = new ISO8583Client("ssi_v1");
                client.reload();
                client.connect("127.0.0.1", 5890, false);
                RuntimeMessage[] rms = client.send(new RuntimeMessage[]{client.prepareRequestMessage()});
                //Thread.sleep(10000);
                rms = client.send(new RuntimeMessage[]{client.prepareRequestMessage()});
                client.disonnect();
            }
            catch(Throwable e){
                e.printStackTrace();
            }
        }
    }

    public void init() throws InitException{
        String tmxHome = System.getProperty("tmx.home");
        if (tmxHome == null)
            throw new InitException("System property 'tmx.home' is not set");

        //2. Initiate properties loading
        Configuration config = Configuration.getInstance();

        //3. Configure logging
        PropertyConfigurator.configure(config.getProperty("log_config", tmxHome + "/conf/log4j.properties"));

        //4. Initialize JNDI
        JndiService.getInstance().reload();

        //5. Initialize simple JSSE
        JsseSimpleService.getInstance().reload();

    }


}
