package com.tmx.web.struts;

import org.apache.struts.tiles.ComponentDefinition;
import org.apache.struts.tiles.xmlDefinition.XmlDefinition;

/**
 * <p>This class provides possibility to get name of parent tiles definition</p>
 * <p><a href="HeritableComponentDefinition.java.html"><i>View Source</i></a></p>
 * <p/>
 *
 */
public class HeritableComponentDefinition extends ComponentDefinition {

	/**
	 * Extends attribute value.
	 */
	private String inherit;

	/**
	 * Copy Constructor.
	 * Creates a new definition initialized with parent definition.
	 * Does a shallow copy : attributes are shared between copies, but not the Map
	 * containing attributes. Added link to parent definition.
	 */
	public HeritableComponentDefinition(ComponentDefinition definition) {
		super(definition);
	}

	/**
	 * Constructor.
	 * Creates a new definition initialized from a RawDefinition.
	 * Raw definitions are used to read definition from a data source (xml file, db, ...).
	 * A RawDefinition mainly contains properties of type String, while Definition
	 * contains more complex type (ex : Controller).
	 * Do a shallow copy : attributes are shared between objects, but not the Map
	 * containing attributes.
	 * OO Design issues : Actually RawDefinition (XmlDefinition) extends ComponentDefinition.
	 * This must not be the case. I have do it because I am lazy.
	 */
	public HeritableComponentDefinition(XmlDefinition definition) {
		this((ComponentDefinition) definition);
		setExtends(definition.getExtends());
	}

	/**
	 * Sets name of definition that is extented by this one
	 *
	 * @param name Name of the extended definition.
	 */
	public void setExtends(String name) {
		inherit = name;
	}

	/**
	 * Gets name of definition that is extented by this one
	 *
	 * @return Name of the extended definition.
	 */
	public String getExtends() {
		return inherit;
	}

	/**
	 * Gets whether this definition extends some other definition or not
     *
     * @return Whether this definition extends some other definition
	 */
	public boolean isExtending() {
		return inherit != null;
	}

}
