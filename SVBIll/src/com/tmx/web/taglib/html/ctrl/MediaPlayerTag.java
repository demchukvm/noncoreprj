package com.tmx.web.taglib.html.ctrl;

import com.tmx.web.controls.MediaPlayer;
import com.tmx.web.controls.BasicControl;
import com.tmx.web.base.WebException;

import javax.servlet.jsp.JspException;

/**

 */
public class MediaPlayerTag extends AbstractControlTag {
    //css properties
    private String buttonStyleClass;
    private String buttonDisabledStyleClass;

    public String getButtonStyleClass() {
        return (buttonStyleClass != null) ? buttonStyleClass : getDefaultProperty("tag.mediaPlayer.style.buttonStyleClass");
    }

    public void setButtonStyleClass(String buttonStyleClass) {
        this.buttonStyleClass = buttonStyleClass;
    }

    public String getButtonDisabledStyleClass() {
        return (buttonDisabledStyleClass != null) ? buttonDisabledStyleClass : getDefaultProperty("tag.mediaPlayer.style.buttonDisabledStyleClass");
    }

    public void setButtonDisabledStyleClass(String buttonDisabledStyleClass) {
        this.buttonDisabledStyleClass = buttonDisabledStyleClass;
    }

    public String getWidth() {
        return (width != null) ? width : getDefaultProperty("tag.mediaPlayer.width");
    }

    public String getHeight() {
        return (height != null) ? height : getDefaultProperty("tag.mediaPlayer.height");        
    }

    //------------------------------Abstract methods
    protected String renderControlHTML() throws JspException {
        StringBuffer resultHTML = new StringBuffer();
        MediaPlayer mediaPlayer = getMediaPlayer();
        if (mediaPlayer == null)
            return "MediaPlayer '" + getProperty() + "' is not found";

        //Render 'play' button
        String type = " type=\"button\" ";
        String name = " name=\"" + getProperty() + ".playButton" + "\" ";
        String value = " value=\"" + getLocalizedLabel("tag.mediaPlayer.button.play", null) + "\" ";
        String styleClass = " class=\"" + (mediaPlayer.isReadonly() ? getButtonDisabledStyleClass() : getButtonStyleClass()) + "\" ";
        String readonly = (mediaPlayer.isReadonly() || mediaPlayer.getContentUrl() == null) ? " disabled " : "";
        resultHTML.append("\r");
        resultHTML.append("<input " + type + name + value + styleClass + readonly + getOnEventAttrsString() + "/>\r");

        //render player for IE
        resultHTML.append("<br/>\r");
        resultHTML.append("<object id='").append(getMediaPlayerName()).append("' height='").
                   append(getHeight()).append("' ").append("width='").append(getWidth()).
                   append("' classid='CLSID:6BF52A52-394A-11d3-B153-00C04F79FAA6'>\r").
                   append("</object>");

        return resultHTML.toString();
    }

    protected String renderOnClickScript(BasicControl ctrl) throws WebException {
        StringBuffer resultHTML = new StringBuffer();
        MediaPlayer mediaPlayer = (MediaPlayer)ctrl;
        //Add script entry from onClick attr or default for button
        if(mediaPlayer.isReadonly() || mediaPlayer.getContentUrl() == null){
            resultHTML.append("     //do nothing, because mediaPlayer is disabled or no cnotent assigned. \r");
        }
        else{
            resultHTML.append("\tvar mediaPlayer = document.getElementById('").append(getMediaPlayerName()).append("');\r");
            resultHTML.append("\tmediaPlayer.controls.stop();\r");
            resultHTML.append("\tmediaPlayer.URL = '").append(mediaPlayer.getContentUrl()).append("';\r");
            resultHTML.append("\tmediaPlayer.controls.play();\r");
        }
        return resultHTML.toString();
    }

    protected int controlStartTag() throws JspException {
        return 0;  //To change body of implemented methods use File | Settings | File Templates.
    }

    private String getMediaPlayerName(){
        return getProperty() + ".mediaPlayer";
    }

    protected MediaPlayer getMediaPlayer() throws JspException {
        Object object = getPropertyObject();
        return (object != null && object instanceof MediaPlayer) ?
                (MediaPlayer) object : null;
    }
}
