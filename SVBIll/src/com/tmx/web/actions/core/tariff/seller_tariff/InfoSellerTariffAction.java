package com.tmx.web.actions.core.tariff.seller_tariff;

import com.tmx.as.blogic.tariff.AbstractTariffManager;
import com.tmx.as.blogic.tariff.SellerTariffManager;
import com.tmx.as.entities.bill.tariff.AbstractTariff;
import com.tmx.as.entities.general.user.User;
import com.tmx.as.exceptions.DatabaseException;
import com.tmx.web.actions.core.tariff.AbstractInfoTariffAction;
import com.tmx.web.base.WebException;
import com.tmx.web.forms.core.tariff.AbstractInfoTariffForm;
import com.tmx.web.forms.core.tariff.seller_tariff.InfoSellerTariffForm;


public class InfoSellerTariffAction extends AbstractInfoTariffAction {

    protected void preloadForCreate(AbstractInfoTariffForm form, AbstractTariff abstractTariff, User loggedInUser) throws DatabaseException, WebException {
        String sellerId = getRoadmap().getParameterValue(getName(), ManageSimpleSellerTariffsAction.PARAMETER_OWNER_NAME, true).toString();
        ((InfoSellerTariffForm) form).getScrollBoxSellers().setSelectedKey(sellerId);

        
    }

    protected void preloadForUpdate(AbstractInfoTariffForm form, AbstractTariff abstractTariff) throws DatabaseException {
        //do nothing
    }

    protected AbstractTariffManager getTariffManager() {
        return new SellerTariffManager();  
    }

}
