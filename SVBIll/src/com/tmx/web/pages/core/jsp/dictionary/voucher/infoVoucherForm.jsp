<%@ page language="java" pageEncoding="UTF-8" contentType="text/html; charset=utf-8" isELIgnored="false" %>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-nested" prefix="nested" %>
<%@ taglib uri="http://tmx.com/taglib/tmx-controls" prefix="tmx_ctrl" %>
<%@ taglib uri="http://tmx.com/taglib/tmx-logic" prefix="tmx_logic" %>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html" %>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean" %>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>

<html:form action="/core/dictionary/voucher/infoVoucherAction">
<table width="100%" border="0">
    <!-- BUTTONS -->
    <tr>
        <td style="text-align: left">
            <tmx_ctrl:button property="buttonBack"/>
            <%--<tmx_ctrl:button property="onEditButton"/>--%>
            <%--<tmx_ctrl:button property="onDeleteButton"/>--%>
        </td>
    </tr>
    <!--FILTER-->
    <tr>
        <td valign="top" width="100%" align="left">
            <%--<tmx_ctrl:panelFilterSet tableName="operatorsTable">--%>
            <!--<table>-->
                <!--<tr>-->
                    <!--<td class="dataLabel">-->
                        <%--<tmx_ctrl:writeEntityLabel--%>
                                <%--attrName="{@name}"--%>
                                <%--entityClassName="com.tmx.as.entities.bill.operator.Operator"/>:--%>
                    <!--</td>-->
                    <!--<td class="dataField">-->
                        <%--<tmx_ctrl:selectBox--%>
                                <%--property="operatorsTable.filterSet.filter(by_id).parameter(id).control"/>--%>
                    <!--</td>-->
                <!--</tr>-->
            <!--</table>-->
        <%--</tmx_ctrl:panelFilterSet>--%>
        </td>
    </tr>
     <!-- MAIN TAB -->
    <tr>
        <td valign="center" align="center" class="tabForm">
            <table width="100%" border="0">
                <%--<!-- Form header -->--%>
                <tr>
                    <td colspan="2">
                        <h4 class="dataLabel">
                            <bean:message bundle="core.labels" key="core.dictionary.seller.infoVoucherform.titlegeneral"/>
                        </h4>
                    </td>
                </tr>

                <tr>
                    <td class="dataLabel" width="15%">
                        <tmx_ctrl:writeEntityLabel attrName="VoucherId"
                                                   entityClassName="com.tmx.as.entities.bill.voucher.Voucher"/>:
                    </td>
                    <td class="dataField" width="35%">
                        <tmx_ctrl:editBox property="editBoxVoucherId"/>
                    </td>

                    <td class="dataLabel" width="15%">
                        <tmx_ctrl:writeEntityLabel attrName="code"
                                                   entityClassName="com.tmx.as.entities.bill.voucher.Voucher"/>:
                    </td>
                    <td class="dataField" width="35%">
                        <tmx_ctrl:editBox property="editBoxCode"/>
                    </td>

                </tr>

                <tr>
                    <td class="dataLabel">
                        <tmx_ctrl:writeEntityLabel attrName="nominal"
                                                   entityClassName="com.tmx.as.entities.bill.voucher.Voucher"/>:
                    </td>
                    <td class="dataField">
                        <tmx_ctrl:editBox property="editBoxNominal"/>
                    </td>

                    <td class="dataLabel">
                        <tmx_ctrl:writeEntityLabel attrName="price"
                                                   entityClassName="com.tmx.as.entities.bill.voucher.Voucher"/>:
                    </td>
                    <td class="dataField" width="35%">
                        <tmx_ctrl:editBox property="editBoxPrice"/>
                    </td>
                </tr>

                <tr>
                    <td class="dataLabel" width="15%">
                        <tmx_ctrl:writeEntityLabel attrName="status"
                                                   entityClassName="com.tmx.as.entities.bill.voucher.Voucher"/>:
                    </td>
                    <td class="dataField" width="35%">
                        <tmx_ctrl:editBox property="editBoxVoucherStatus"/>
                    </td>

                    <td class="dataLabel">
                        <tmx_ctrl:writeEntityLabel attrName="soldInBillTransactionNum"
                                                   entityClassName="com.tmx.as.entities.bill.voucher.Voucher"/>:
                    </td>
                    <td class="dataField">
                        <tmx_ctrl:editBox property="editBoxSoldInBillTransactionNum"/>
                    </td>
                </tr>

                <tr>
                    <td class="dataLabel">
                        <tmx_ctrl:writeEntityLabel attrName="loadingTime"
                                                   entityClassName="com.tmx.as.entities.bill.voucher.Voucher"/>:
                    </td>
                    <td class="dataField">
                        <tmx_ctrl:timeSelector property="timeSelectorLoadingTime"/>
                    </td>

                    <td class="dataLabel">
                        <tmx_ctrl:writeEntityLabel attrName="soldTime"
                                                   entityClassName="com.tmx.as.entities.bill.voucher.Voucher"/>:
                    </td>
                    <td class="dataField">
                        <tmx_ctrl:timeSelector property="timeSelectorSoldTime"/>
                    </td>
                </tr>
                <tr>
                    <td class="dataLabel">
                        <tmx_ctrl:writeEntityLabel attrName="bestBeforeDate"
                                                   entityClassName="com.tmx.as.entities.bill.voucher.Voucher"/>:
                    </td>
                    <td class="dataField">
                        <tmx_ctrl:timeSelector property="timeSelectorBestBeforeDate"/>
                    </td>

                    <td class="dataLabel">
                        <tmx_ctrl:writeEntityLabel attrName="returnTime"
                                                   entityClassName="com.tmx.as.entities.bill.voucher.Voucher"/>:
                    </td>
                    <td class="dataField">
                        <tmx_ctrl:timeSelector property="timeSelectorReturnTime"/>
                    </td>
                </tr>

                <tr>
                    <td class="dataLabel">
                        <tmx_ctrl:writeEntityLabel attrName="secretCode"
                                                   entityClassName="com.tmx.as.entities.bill.voucher.Voucher"/>:
                    </td>
                    <td class="dataField">
                        <tmx_ctrl:editBox property="editBoxSecretCode"/>
                    </td>

                    <td class="dataLabel">
                        <%--<tmx_ctrl:writeEntityLabel attrName="returnTime"--%>
                                                   <%--entityClassName="com.tmx.as.entities.bill.voucher.Voucher"/>:--%>
                    </td>
                    <td class="dataField">
                        <%--<tmx_ctrl:timeSelector property="timeSelectorReturnTime"/>--%>
                    </td>
                </tr>


            </table>
        </td>

    </tr>

    <!-- BUTTONS -->
    <tr>
        <td style="text-align: left">
            <tmx_ctrl:button property="buttonBack"/>
            <%--<tmx_ctrl:button property="onEditButton"/>--%>
            <%--<tmx_ctrl:button property="onDeleteButton"/>--%>
        </td>
    </tr>
</table>
</html:form>