package com.tmx.beng.csapi.xml.v1.base;

import com.tmx.gate.csapi.v1.base.beans.*;
import com.tmx.beng.base.BillException;
import com.tmx.beng.base.BillingMessage;
import com.tmx.beng.base.BillingMessageImpl;

import java.util.regex.Matcher;


/**
 * Created by IntelliJ IDEA.
 * User: Shake
 * Date: 27.04.2007
 * Time: 11:16:02
 */
public class VerifyV10 extends AbstractVerify{

    public void verify(RootDocument root, BillingMessageImpl billMsg) throws BillException {
        super.verify(root, billMsg);

        OperationsType operations = root.getRoot().getRequest().getOperations();
        if(operations.getRefillPayment() != null){
            RefillPaymentReqType operation = operations.getRefillPayment();
            if(operation.getAccountNumber() == null) throw new BillException("err.csapi.account_number_not_found");

            // check msisdn
            Matcher matcher = accountPattern.matcher(operation.getAccountNumber());
            if(!matcher.matches()){
                throw new BillException("err.csapi.incorrect_msisdn_format");
            }
            billMsg.setAttributeString(BillingMessage.ACCOUNT_NUMBER, operation.getAccountNumber());
        }else if(operations.getRefillPaymentStatus() != null){
        }else if(operations.getBuyVoucher() != null){
        }else{
            throw new BillException("err.csapi.uncknown_operation");
        }
    }

    public void serialize(RootDocument root, BillingMessage msg){
        super.serialize(root, msg);
    }
}
