package com.tmx.web.actions.core.message;

import com.tmx.web.base.BasicDispatchedAction;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForm;
import org.apache.struts.tiles.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.ServletContext;

public class ConfirmationAction extends BasicDispatchedAction {
    private static final String FORWARD = "page";
    public ActionForward defaultAction(ActionMapping mapping,
                                       ActionForm form,
                                       HttpServletRequest request,
                                       HttpServletResponse response,
                                       String methodName) throws Throwable {
    String leftMenuName=(String)getRoadmap().getParameterValue(this.getName(), "leftMenuName",true);
    ComponentContext context = ComponentContext.getContext( request );
    if( context == null ){
          context = new ComponentContext();
          ComponentContext.setContext(context, request);
    }
    context.putAttribute("tile.loggedInLayout.leftMenuName", leftMenuName);

        return mapping.findForward(FORWARD);
    }
}
