package com.tmx.web.base;

import com.tmx.util.XMLUtil;
import com.tmx.util.InitException;
import com.sun.org.apache.xpath.internal.XPathAPI;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import javax.servlet.ServletContext;
import java.util.HashMap;
import java.util.Locale;
import java.util.Vector;

public class UserInterfaceService implements Service {

    private HashMap roles = new HashMap();
    private HashMap forwards = new HashMap();
    private String defaultRoleName = null;
    private String DEFAULT_MENU_FORWARD_NAME = "menuDefault";

    public void init(ServletContext context) throws InitException {
        try {

            String userInterFile = ApplicationEnvironment.getInitParameter("interface.user_interface_file", null);
            if (userInterFile == null)
                throw new InitException("err.ui_service.ui_file_not_specified");
            userInterFile = System.getProperty("tmx.home") + userInterFile;

            //Load menus from XML
            Document doc = XMLUtil.getDocumentFromFile(userInterFile);
            String xpathRole = "/ui/role";
            NodeList rolesNode = XPathAPI.selectNodeList(doc, xpathRole);
            for (int i = 0; i < rolesNode.getLength(); i++) {
                Node roleNode = rolesNode.item(i);
                if (roleNode.getNodeType() == Node.ATTRIBUTE_NODE)
                    continue;
                UserInterfaceService.RoleHandler role = new UserInterfaceService.RoleHandler(((Element) roleNode).getAttribute("name"));

                String xPathDefaultRole = "/ui/role[@name='" + role.name + "']/@default";
                Node isDefault = XPathAPI.selectSingleNode(doc, xPathDefaultRole);
                if (isDefault != null)
                    defaultRoleName = role.name;

                String xPathAllow = "/ui/role[@name='" + role.name + "']/menu";
                NodeList allows = XPathAPI.selectNodeList(doc, xPathAllow);
                for (int j = 0; j < allows.getLength(); j++) {
                    Node menuNode = allows.item(j);
                    if (menuNode.getNodeType() == Node.ATTRIBUTE_NODE)
                        continue;
                    MenuHandler menu = new MenuHandler();
                    menu.forwardName = ((Element) menuNode).getAttribute("forward");
                    role.addMenu(menu);
                }

                roles.put(role.name, role);
            }

            if (defaultRoleName == null)
                throw new InitException("err.ui_service.default_role_not_specified");

            //Load forwards from struts-config.xml
            loadForwards();

        }
        catch(InitException e){
            //rethrow
            throw e;
        }
        catch(Exception e) {
            throw new InitException("err.ui_service.init_failed", e, Locale.getDefault());
        }
    }

    public String getForwardURL(String forwardName) throws WebException {
        ForwardHandler forwardHandler = (ForwardHandler)forwards.get(forwardName);
        if(forwardHandler == null)
            throw new WebException("err.ui_service.menu_forward_url_not_found", new String[]{forwardName});
        return forwardHandler.forwardPath;
    }

    public String getActionURL(String ActionName) {
        //TODO !!!!
        throw new RuntimeException("RM: Method not implemented yet");
    }

    public String getMenuForwardName(String roleName) throws WebException {
        RoleHandler role = (RoleHandler) roles.get(roleName);
        if (role == null)
            role = (RoleHandler) roles.get(defaultRoleName);

        if (role == null)
            throw new WebException("err.ui_service.default_role_not_found", new String[]{defaultRoleName});

        MenuHandler menu = (MenuHandler) role.menu.firstElement();
        if (menu == null)
            throw new WebException("err.ui_service.forward_for_role_not_found", new String[]{roleName});

        return menu.forwardName;
    }

    public String getMenuForwardURL(String roleName) throws WebException {
        String forwardName = getMenuForwardName(roleName);
        if(forwardName == null)
            return null;
        return getForwardURL(forwardName);
    }

    public String getDefaultMenuForwardURL() throws WebException{
        return getForwardURL(DEFAULT_MENU_FORWARD_NAME);
    }

    /**
     * 1) Validates menus's forwards against the struts-config.xml. So far as
     * UserInterfaceService could be inited before struts servlet does, we can not
     * use struts model to work with forwars.
     * We could parse struts-config.xml ourselves.
     * 2) Sets the actual URL into <code>roles</code> map to be used directly in JSP.
     */
/*
    RM: Not used

    private void buildMenuForwardsURL() throws InitException {
        ResourceService resourceService = ApplicationEnvironment.getResourceService();
        if (resourceService == null)
            throw new InitException("err.init.interface.required_resource_service_is_not_published");

        Document strutsConfigXML = resourceService.getStrutsConfigXML();
        if (strutsConfigXML == null)
            throw new InitException("err.init.interface.required_struts_config_xml_not_found");

        //parse it
        if (roles == null)
            return;//nothing to validate
        try {

            Iterator roleNamesIterator = roles.keySet().iterator();
            while (roleNamesIterator.hasNext()) {
                String roleName = (String) roleNamesIterator.next();
                if (roleName == null)
                    continue;
                RoleHandler roleHandler = (RoleHandler) roles.get(roleName);
                if (roleHandler == null || roleHandler.menu == null) {
                    //TODO RM put to log
                    System.out.println("Warning: Role '" + roleName + "' or its menu from config is null");
                    continue;
                }

                //Comparison along menus of current role
                for (int i = 0; i < roleHandler.menu.size(); i++) {
                    MenuHandler menuHandler = (MenuHandler) roleHandler.menu.elementAt(i);
                    String xpathGlobalForward = "/struts-config/global-forwards/forward[@name='" + menuHandler.forward + "']";
                    Node forwardNode = XPathAPI.selectSingleNode(strutsConfigXML, xpathGlobalForward);
                    if (forwardNode == null)
                        throw new InitException("err.init.interface.forward_not_found_for_role_menu_definition", new String[]{roleName, menuHandler.forward});

                    //Gets and saves the actual URL
                    if (forwardNode.getNodeType() == Node.ATTRIBUTE_NODE)
                        continue;
                    String path = ((Element) forwardNode).getAttribute("path");
                    menuHandler.forwardURL = ApplicationEnvironment.getAppDeploymentName() + path;
                }
            }
        }
        catch (InitException e) {
            throw e;//throw out
        }
        catch (Exception e) {
            throw new InitException("err.init.interface.failed_to_build_actual_menu_urls", e);
        }
    }
*/
    /**
     * Load forwards from struts-config.xml directly. Useful at the startup time
     * when the Struts forwards not loaded yet.
     */
    private void loadForwards() throws InitException {
        ResourceService resourceService = ApplicationEnvironment.getResourceService();
        if (resourceService == null)
            throw new InitException("err.ui_service.required_resource_service_is_not_published");

        Document strutsConfigXML = resourceService.getStrutsConfigXML();
        if (strutsConfigXML == null)
            throw new InitException("err.ui_service.required_struts_config_xml_not_found");

        try {
            forwards = new HashMap();
            String xpathForwards = "/struts-config/global-forwards/forward";
            NodeList forwardsNodes = XPathAPI.selectNodeList(strutsConfigXML, xpathForwards);
            if (forwardsNodes != null) {
                for (int i = 0; i < forwardsNodes.getLength(); i++) {
                    Node forwardNode = forwardsNodes.item(i);
                    if (forwardNode.getNodeType() == Node.ATTRIBUTE_NODE)
                        continue;
                    ForwardHandler forwardHandler = new ForwardHandler();
                    forwardHandler.forwardName = ((Element) forwardNode).getAttribute("name");
                    forwardHandler.forwardPath = ((Element) forwardNode).getAttribute("path");
                    forwards.put(forwardHandler.forwardName, forwardHandler);
                }
            }
        }
        catch (Exception e) {
            throw new InitException("err.ui_service.failed_to_parse_struts_config_xml", e);
        }

    }

    private class RoleHandler {
        String name = null;
        Vector menu = null;

        RoleHandler(String roleName) {
            name = roleName;
            menu = new Vector();
        }

        void addMenu(MenuHandler menuHandler) {
            menu.add(menuHandler);
        }
    }

    private class MenuHandler {
        /**
         * Name of the forward
         */
        String forwardName = null;

    }

    private class ForwardHandler{
        String forwardName = null;
        String forwardPath = null;
    }
}