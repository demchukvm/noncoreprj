package com.tmx.beng.base.processors;

import com.tmx.beng.base.*;
import com.tmx.beng.base.dbfunctions.BasicFunction;
import com.tmx.beng.base.cache.FunctionWrapper;
import com.tmx.beng.base.cache.DBCache;

import java.util.List;
import java.util.Iterator;

/**

 */
public class CopyCallbackProcessor extends TaskProcessor {

    public Task process0(Task task, DBCache dbCache, BillingMessageWritable msg)throws BillException {
        logger.debug("Copy callback...");
        try{

            Task senderTask = task.getGlobalContext().getTransactionPool().lookupFirstTask(
                    msg.getAttributeString(BillingMessage.REQUEST_ID)
                );
            if(senderTask.getCallBack() != null)
                task.setCallBack(senderTask.getCallBack());
           

//            List tasks = task.getGlobalContext().getTransactionPool().getTransactionTaskList(task.getProcesedMessage().getTransactionId());
////RM:TODO  use message name to identify message
//            for(Iterator it = tasks.iterator(); it.hasNext();){
//                Task t = (Task)it.next();
//                if(t.getCallBack() != null){
//                    task.setCallBack(t.getCallBack());
//                }
//            }
        }
        catch(Throwable e){
            msg.setStatusCode(StatusDictionary.COPY_CALLBACK_ERROR);
            logger.error(e.getLocalizedMessage(), e);
            throw new BillException("err.copy_callback_proc.error", e);
        }
        return task;
    }
}
