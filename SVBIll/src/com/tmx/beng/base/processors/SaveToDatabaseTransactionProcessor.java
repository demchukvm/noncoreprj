package com.tmx.beng.base.processors;

import com.tmx.beng.base.*;
import com.tmx.beng.base.cache.DBCache;
import com.tmx.as.base.EntityManager;
import com.tmx.as.base.ProcedureWrapper;
import com.tmx.as.exceptions.DatabaseException;
import com.tmx.as.entities.bill.transaction.AbstractTransaction;
import com.tmx.as.entities.bill.transaction.TransactionError;

import java.util.List;
import java.util.Iterator;

/**
 Close transaction task
 */
public abstract class SaveToDatabaseTransactionProcessor extends TaskProcessor {
    private final String SCHEMA_NAME = "svbill";

    protected abstract List getSavedTransactionEntitiesList(Task task, DBCache dbCache, BillingMessage msg) throws BillException;

    public Task process0(Task task, DBCache dbCache, BillingMessageWritable msg)throws BillException{
        logger.debug("Saving to database transaction...");
        EntityManager entityManager = new EntityManager();
        try{

            List entityList = getSavedTransactionEntitiesList(task, dbCache, msg);

            if(entityList == null){
                throw new BillException("err.bill.reg_processor.entity_list_task_not_found");
            }

            //prepare and save error if status is not ok
            String statusCode = msg.getAttributeString(BillingMessage.STATUS_CODE);
            TransactionError error = null;
            if(statusCode != null && StatusDictionary.STATUS_OK != Integer.parseInt(statusCode)){
                error = new TransactionError();
                error.setCode(statusCode);       
                error.setMessage(msg.getAttributeString(BillingMessage.STATUS_MESSAGE));
            }

            //save
            entityManager.BEGIN(SCHEMA_NAME);
            for(Iterator it = entityList.iterator(); it.hasNext(); ){
                Object obj = it.next();                   
                if(obj instanceof AbstractTransaction){
                    AbstractTransaction tx = (AbstractTransaction)obj;
                    tx.setStatus(statusCode);
                    if(error != null){
                        entityManager.SAVE(error);
                        tx.setError(error);
                    }
                    entityManager.SAVE(tx);
                }
                else if(obj instanceof ProcedureWrapper){
                    //if status ok (no error) then update balance in db
                    if(error == null)
                        ((ProcedureWrapper)obj).invoke(entityManager);
                }
            }
            entityManager.COMMIT(SCHEMA_NAME);

        }
        catch(BillException e){
            msg.setStatusCode(StatusDictionary.SAVE_TO_DB_ERROR);
            throw e;
        }
        catch(Throwable e){
            try{
                entityManager.ROLLBACK(SCHEMA_NAME);
            }
            catch(DatabaseException de){
                logger.error(de.getLocalizedMessage());
            }
            msg.setStatusCode(StatusDictionary.SAVE_TO_DB_ERROR);
            logger.error(e.getLocalizedMessage(), e);
            throw new BillException(e.getLocalizedMessage(), e);
        }
        return task;
    }
}
