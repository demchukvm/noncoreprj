package com.tmx.beng.medaccess.kyivstar;

import com.tmx.beng.medaccess.MediumAccess;
import com.tmx.beng.medaccess.MediumConnection;
import com.tmx.beng.medaccess.GateException;

/**
  Medium Access to obtain Mobipay Kyivstar medium connection
 */
public class KyivstarGateMediumAccess implements MediumAccess {

    public void init() {
    }

    public MediumConnection getConnection() throws GateException {
        return new KyivstarGateMediumConnection();  //To change body of implemented methods use File | Settings | File Templates.
    }
}
