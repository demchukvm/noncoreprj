package com.tmx.web.forms.core.uipermsetup;

import com.tmx.web.base.BasicActionForm;
import com.tmx.web.base.ApplicationEnvironment;
import com.tmx.web.controls.ScrollBox;
import com.tmx.web.controls.Button;
import com.tmx.web.taglib.html.ctrl.PermissionControlTagInterface;
import com.tmx.web.taglib.html.filter_ctrl.PermissionFormTagInterface;
import com.tmx.as.entities.general.role.Role;
import com.tmx.as.entities.general.ui_permission.PermissionType;

import java.util.Hashtable;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

import org.apache.struts.action.ActionMapping;
import org.apache.struts.config.ActionConfig;
import org.apache.struts.config.ModuleConfig;

import javax.servlet.http.HttpServletRequest;

public class ToolBarPermissionForm extends BasicActionForm implements PermissionControlTagInterface, PermissionFormTagInterface {
    private String forwardUrl;
    private boolean active;
    //-----
    private Hashtable permissionsForm=new Hashtable();
    private Hashtable permissionsMenu;
    private Hashtable permissionsMenuContainer;
    private Hashtable permissionsMenuItem;
    private Hashtable permissionsFormControl;
    //-----
    private Hashtable applyPermissionsForm;
    private Hashtable applyPermissionsMenu;
    private Hashtable applyPermissionsMenuContainer;
    private Hashtable applyPermissionsMenuItem;
    private Hashtable applyPermissionsFormControl;
    //-----
    private List permissionEntities;
    private List permissionTypes;
    // controls
    private ScrollBox roles;
    private Button update;
    private Button setup;
    private Button apply;
    private Button close;

    protected void init() {
        roles=new ScrollBox() {
            protected String getKey(Object dataListElement) {
                return ((Role)dataListElement).getRoleId().toString();
            }
            protected String getValue(Object dataListElement) {
                return ((Role)dataListElement).getRoleName();
            }
        };
        String action="/core/uipermsetup/toolBarPermissionAction";
        ModuleConfig mc = ApplicationEnvironment.getModuleConfig();
        ActionConfig actionConfig = mc.findActionConfig(action);
        String url=null;
        if(actionConfig != null)
            url = ApplicationEnvironment.getAppDeploymentName() + actionConfig.getPath() + ApplicationEnvironment.getActionExtension()+ "?" + "command=setup";

        roles.setOnChange("javascript: document.forms[0].action=\""+url+"\";\n" +
                "     document.forms[0].method='POST';\n" +
                "     document.forms[0].submit();");

        update=new Button();
        update.setLabelKey("ctrl.button.label.refresh");
        update.setAction(action);
        update.setCommand("update");

        apply=new Button();
        apply.setLabelKey("ctrl.button.label.apply");
        apply.setAction(action);
        apply.setCommand("apply");

        close=new Button();
        close.setLabelKey("ctrl.button.label.close");
        close.setAction(action);
        close.setCommand("interactiveDectivate");
    }

    public void reset(ActionMapping mapping, HttpServletRequest request) {
        super.reset(mapping, request);
        applyPermissionsForm=new Hashtable();
        applyPermissionsMenu=new Hashtable();
        applyPermissionsMenuContainer=new Hashtable();
        applyPermissionsMenuItem=new Hashtable();
        applyPermissionsFormControl=new Hashtable();
    }

    public String getForwardUrl() {
        return forwardUrl;
    }

    public void setForwardUrl(String forwardUrl) {
        this.forwardUrl = forwardUrl;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public ScrollBox getRoles() {
        return roles;
    }

    public void setRoles(ScrollBox roles) {
        this.roles = roles;
    }

    public Button getUpdate() {
        return update;
    }

    public void setUpdate(Button update) {
        this.update = update;
    }

    public Button getSetup() {
        return setup;
    }

    public void setSetup(Button setup) {
        this.setup = setup;
    }

    public Button getApply() {
        return apply;
    }

    public void setApply(Button apply) {
        this.apply = apply;
    }

    public Button getClose() {
        return close;
    }

    public void setClose(Button close) {
        this.close = close;
    }

    public Hashtable getPermissionsForm() {
        return permissionsForm;
    }

    public void setPermissionsForm(Hashtable permissionsForm) {
        this.permissionsForm = permissionsForm;
    }

    public Hashtable getPermissionsMenu() {
        return permissionsMenu;
    }

    public void setPermissionsMenu(Hashtable permissionsMenu) {
        this.permissionsMenu = permissionsMenu;
    }

    public Hashtable getPermissionsMenuContainer() {
        return permissionsMenuContainer;
    }

    public void setPermissionsMenuContainer(Hashtable permissionsMenuContainer) {
        this.permissionsMenuContainer = permissionsMenuContainer;
    }

    public Hashtable getPermissionsMenuItem() {
        return permissionsMenuItem;
    }

    public void setPermissionsMenuItem(Hashtable permissionsMenuItem) {
        this.permissionsMenuItem = permissionsMenuItem;
    }

    public Hashtable getPermissionsFormControl() {
        return permissionsFormControl;
    }

    public void setPermissionsFormControl(Hashtable permissionsFormControl) {
        this.permissionsFormControl = permissionsFormControl;
    }

    public Hashtable getApplyPermissionsForm() {
        return applyPermissionsForm;
    }

    public void setApplyPermissionsForm(Hashtable applyPermissionsForm) {
        this.applyPermissionsForm = applyPermissionsForm;
    }

    public Hashtable getApplyPermissionsMenu() {
        return applyPermissionsMenu;
    }

    public void setApplyPermissionsMenu(Hashtable applyPermissionsMenu) {
        this.applyPermissionsMenu = applyPermissionsMenu;
    }

    public Hashtable getApplyPermissionsMenuContainer() {
        return applyPermissionsMenuContainer;
    }

    public void setApplyPermissionsMenuContainer(Hashtable applyPermissionsMenuContainer) {
        this.applyPermissionsMenuContainer = applyPermissionsMenuContainer;
    }

    public Hashtable getApplyPermissionsMenuItem() {
        return applyPermissionsMenuItem;
    }

    public void setApplyPermissionsMenuItem(Hashtable applyPermissionsMenuItem) {
        this.applyPermissionsMenuItem = applyPermissionsMenuItem;
    }

    public Hashtable getApplyPermissionsFormControl() {
        return applyPermissionsFormControl;
    }

    public void setApplyPermissionsFormControl(Hashtable applyPermissionsFormControl) {
        this.applyPermissionsFormControl = applyPermissionsFormControl;
    }

    public List getPermissionEntities() {
        return permissionEntities;
    }

    public void setPermissionEntities(List permissionEntities) {
        this.permissionEntities = permissionEntities;
    }

    public Long getFormControlPermission(String control){
       Long permission = (Long)this.permissionsFormControl.get(control);
       return permission==null?new Long(1):permission;
    }

    public Long getFormPermission(String form) {
        Long permission = (Long)this.permissionsForm.get(form);
        return permission==null?new Long(1):permission;
    }

    public List getPermissionTypes() {
        return permissionTypes;
    }

    public void setPermissionTypes(List permissionTypes) {
        this.permissionTypes = permissionTypes;
    }

    public void setPermissionTypeObjects(List permissionTypeObjects) {
        Iterator it=permissionTypeObjects.iterator();
        List permissionTypes=new ArrayList();
        while (it.hasNext()) {
            PermissionType permissionType=(PermissionType)it.next();
            Object[] bean=new Object[]{permissionType.getPermissionTypeId(),permissionType.getShortName()};
            permissionTypes.add(bean);
        }
        this.permissionTypes=permissionTypes;
    }
}
