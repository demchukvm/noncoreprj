<?php

$query = new WP_Query(array('post_type' => 'goods', 'showposts' => -1));

while ($query->have_posts()) : $query->the_post();?>


<div class="product" id="product<?php echo $post->ID;?>">
    <div class="photo">
        <img src="<?php echo get_field("image");?>" alt="" width="375" />
    </div>
    <div class="info">

        <div class="phone-label">
            <span> <?php echo get_field('model');?></span>
        </div>


        <h3><?php echo get_the_content();?></h3>


        <ul class="features">
            <?php echo get_field('description');?>
        </ul>
        <a class="buy"
           style="background: #04a9e1 url('<?php bloginfo('template_url');?>/images/buy_online-bnt<?php if (isset($_GET['lang']) & $_GET['lang'] == 'en') {
               echo '-eng';
           }?>.gif') no-repeat;"></a>

        <div class="hid_form">


            <div class="feedback_block" style="width:400px;">
                <form method="POST" style="width:400px;">
                    <div class="left">
                        <div class="name"><span>Имя</span> <input type="text" name="name" id="form_name<?php echo $post->ID;?>" value=""></div>
                        <div class="city"><span>Город</span> <input type="text" name="city" id="form_city<?php echo $post->ID;?>" value=""></div>
                        <div class="mail"><span>Email</span> <input type="text" name="mail" id="form_mail<?php echo $post->ID;?>" value=""></div>
                        <div class="phone"><span>Телефон</span> <input type="text" name="phone" id="form_phone<?php echo $post->ID;?>" value=""></div>
                    </div>
					<input type="hidden" name="phone_model" value="<?php echo get_field('model');?>" />
                    <div id="<?php echo $post->ID;?>" class="buyphone">Да, я хочу его!</div>

                </form>

            </div>


        </div>
        <div class="buyinfo"></div>

    </div>

</div>




<? endwhile;

wp_reset_postdata();
?>







