package com.tmx.web.taglib.html.filter_ctrl;

import com.tmx.web.controls.TimeSelector;
import com.tmx.web.base.ApplicationEnvironment;

import javax.servlet.jsp.JspException;

/**

 */
public class TimeSelectorFilterTag extends AbstractFilterTag{
    private String parameterName = null;
    //CSS styles properties
    private String allowManualChange = "true";
    private String editStyleClass = null;
    private String readonlyEditStyleClass = null;
    private String calendarButtonImage = null;
    //html id suffix of calendar button
    public static final String CALENDER_BUTTON_ID_SUFFIX = "TimeButton";
    //Style classes
    private final String CALENDAR_BUTTON_CLASS = "calendar_button";

    //------------------------------Properties
    public String getEditStyleClass() {
        return (editStyleClass != null) ? editStyleClass : getDefaultProperty("tag.timeSelectorFilter.style.editStyleClass");
    }

    public void setEditStyleClass(String styleClass) {
        this.editStyleClass = styleClass;
    }

    public String getReadonlyEditStyleClass() {
        return (readonlyEditStyleClass != null) ? readonlyEditStyleClass : getDefaultProperty("tag.timeSelectorFilter.style.readonlyEditStyleClass");
    }

    public void setReadonlyEditStyleClass(String readonlyStyleClass) {
        this.readonlyEditStyleClass = readonlyStyleClass;
    }

    public String getCalendarButtonImage() {
        return (calendarButtonImage != null) ? calendarButtonImage : ApplicationEnvironment.getAppDeploymentName() + getDefaultProperty("tag.timeSelectorFilter.img.calendarButtonImg");
    }

    public void setCalendarButtonImage(String calendarButtonImage) {
        this.calendarButtonImage = calendarButtonImage;
    }

    public String getParameterName() {
        return parameterName;
    }

    public void setParameterName(String parameterName) {
        this.parameterName = parameterName;
    }

    public String getAllowManualChange() {
        return allowManualChange;
    }

    public void setAllowManualChange(String allowManualChange) {
        this.allowManualChange = allowManualChange;
    }

    //------------------------------Protected methods
    protected String renderControlHTML() throws JspException {
//        super.renderControlHTML();
        StringBuffer resultHTML = new StringBuffer();
        String path = getProperty() + "." + getFilter().getParameter(parameterName).getPath() + "." + TimeSelector.TIME_SUFFIX;
        String dotReplacedPath = getSeparatorReplacedName(path);
        TimeSelector ts = (TimeSelector)getFilter().getParameter(parameterName).getControl();

        if(ts == null)
            return "Filter TimeSelector '"+getProperty()+"' is not found";

        //Render HTML
        resultHTML.append("<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\">\r");
        resultHTML.append(" <tr>\r");

        //edit box
        resultHTML.append("     <td>\r");

        String readonly = (getAllowManualChange().equalsIgnoreCase("false")||ts.isReadonly()) ? " readonly=\"true\" " :"";
        String id = " id=\""+dotReplacedPath+"." + TimeSelector.TIME_SUFFIX +"\" ";
        String name = " name=\""+path+"\""; //" name=\""+ts.getFullName()+"."+TimeSelector.TIME_SUFFIX+"\" "; //" name=\""+getProperty()+"."+TimeSelector.TIME_SUFFIX+"\" ";
        String type = " type=\"text\" ";
        String value = " value=\""+ts.getTimeString()+"\" ";
        String styleClass = " class=\""+((getAllowManualChange().equalsIgnoreCase("false")||ts.isReadonly()) ? getReadonlyEditStyleClass() : getEditStyleClass())+"\" ";
        //Render HTML
        resultHTML.append("\r");
        resultHTML.append("         <input " + id + name + type + value + getOnEnterPress() + styleClass + readonly +"/>\r");
        resultHTML.append("     </td>\r");

        if(!ts.isReadonly()){
            //calendar button
            resultHTML.append("     <td>&nbsp;\r");
            resultHTML.append("         <img alt=\"\" src=\""+getCalendarButtonImage()+"\" id=\""+dotReplacedPath + "." + CALENDER_BUTTON_ID_SUFFIX+"\" class=\""+CALENDAR_BUTTON_CLASS+"\"\r");
            resultHTML.append("     <td>\r");
        }


        resultHTML.append(" </tr>\r");
        resultHTML.append("</table>\r");

        if(!ts.isReadonly()){
            //Render JS
            // calendar
            resultHTML.append("<script type=\"text/javascript\">");
            resultHTML.append("     Calendar.setup({\r");
            resultHTML.append("         inputField     :    \""+ dotReplacedPath + "." + TimeSelector.TIME_SUFFIX + "\",      // id of the input field\r");
            resultHTML.append("         ifFormat       :    \"%Y-%m-%d %H:%M\",       // format of the input field\r");
            resultHTML.append("         button         :    \""+ dotReplacedPath + "."+ CALENDER_BUTTON_ID_SUFFIX +"\",   // trigger for the calendar (button ID)\r");
            resultHTML.append("         singleClick    :    true,         // one-click mode\r");
            resultHTML.append("         step           :    1,            // show all years in drop-down boxes (instead of every other year as default)\r");
            resultHTML.append("         showsTime      :    true,\r");
            resultHTML.append("         timeFormat     :    \"24\"\r");
            resultHTML.append("     });\r");
            resultHTML.append("\r");
            resultHTML.append("</script>\r");
            resultHTML.append("\r");
        }

        return resultHTML.toString();
    }

}

