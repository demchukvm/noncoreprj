package com.tmx.as.entities.bill.address;

import com.tmx.as.base.BasicEntity;
import com.tmx.as.entities.network.territory.Country;
import com.tmx.as.entities.network.territory.District;
import com.tmx.as.entities.network.territory.Region;
import com.tmx.as.entities.network.territory.City;

import java.io.Serializable;

/**
 */
public class Address extends BasicEntity{
    private Long addressId;
    private String addressLine1;
    private String addressLine2;
    private String zipCode;
    private Country country;
    private District district;
    private Region region;
    private City city;
    private String building;
    private String office;
    private String room;
    private String phone;
    private String fax;
    private String note;
//    private String phone0;
//    private String phone1;
//    private String mobPhone0;
//    private String mobPhone1;
//    private String eMail0;
//    private String eMail1;
//    private String eMail2;

    public Region getRegion() {
        return region;
    }

    public void setRegion(Region region) {
        this.region = region;
    }
    
    public Long getAddressId() {
        return addressId;
    }

    public void setAddressId(Long addressId) {
        this.addressId = addressId;
    }

    public String getAddressLine1() {
        return addressLine1;
    }

    public void setAddressLine1(String addressLine1) {
        this.addressLine1 = addressLine1;
    }

    public String getAddressLine2() {
        return addressLine2;
    }

    public void setAddressLine2(String addressLine2) {
        this.addressLine2 = addressLine2;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    public District getDistrict() {
        return district;
    }

    public void setDistrict(District district) {
        this.district = district;
    }

    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }

    public String getBuilding() {
        return building;
    }

    public void setBuilding(String building) {
        this.building = building;
    }

    public String getOffice() {
        return office;
    }

    public void setOffice(String office) {
        this.office = office;
    }

    public String getRoom() {
        return room;
    }

    public void setRoom(String room) {
        this.room = room;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Serializable getId(){
        return getAddressId();
    }

    public int getValid() {
        return valid;
    }

    public void setValid(int valid) {
        this.valid = valid;
    }
}
