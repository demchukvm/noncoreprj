package com.tmx.web.forms.core.tools;

import com.tmx.as.blogic.OperatorManager;
import com.tmx.as.entities.bill.operator.Operator;
import com.tmx.as.exceptions.DatabaseException;
import com.tmx.web.controls.Button;
import com.tmx.web.controls.EditBox;
import com.tmx.web.base.BasicActionForm;
import com.tmx.web.controls.ScrollBox;

public class CyberplatTransactionInfoForm extends BasicActionForm
{
    private Button getStatus = null;
    private EditBox billingNumber = null;
    private String cyberplatStatusInfo = "";


    private EditBox ebBillNumber = null;
    private ScrollBox sbProduct = null;
    private Button bOk = null;

    private String avancel_gate_1 = "";
    private String citypay_gate = "";
    private String citypay_gate_life = "";
    private String cyberplat_gate = "";
    private String dacard_gate = "";
    private String kyivstar_bonus_no_commission_gate = "";
    private String kyivstar_exclusive_gate = "";
    private String kyivstar_gate = "";
    private String umc_gate = "";

    private String billingNum = "";
    private String clientNum = "";
    private String transactionTime = "";
    private String account = "";
    private String amount = "";
    private String product = "";
    private String terminalSn = "";
    private String sellerCode = "";
    private String status = "";

    public void init()
    {
        getStatus = new Button();
        getStatus.setLabelKey("ctrl.button.label.view");
        getStatus.setAction("/cyberplatTransactionInfoAction");
        getStatus.setCommand("getStatus");

        billingNumber = new EditBox();
        billingNumber.setReadonly(false);

        ebBillNumber = new EditBox();
        ebBillNumber.setReadonly(false);
        ebBillNumber.setMandatory(true);

        bOk = new Button();
        bOk.setLabelKey("ctrl.button.label.ok");
        bOk.setAction("/cyberplatTransactionInfoAction");
        bOk.setCommand("getInfo");

        sbProduct = new ScrollBox() {
            protected String getKey(Object dataListElement){return ((Operator) dataListElement).getCode();}
            protected String getValue(Object dataListElement){return ((Operator) dataListElement).getName();}
        };
        sbProduct.setNullable(false);
        try {
            sbProduct.bind(new OperatorManager().getAllOperators());
        } catch (DatabaseException e) {
            e.printStackTrace();
        }

    }

    public Button getGetStatus() {
        return getStatus;
    }

    public void setGetStatus(Button getStatus) {
        this.getStatus = getStatus;
    }

    public EditBox getBillingNumber() {
        return billingNumber;
    }

    public void setBillingNumber(EditBox billingNumber) {
        this.billingNumber = billingNumber;
    }

    public String getCyberplatStatusInfo() {
        return cyberplatStatusInfo;
    }

    public void setCyberplatStatusInfo(String cyberplatStatusInfo) {
        this.cyberplatStatusInfo = cyberplatStatusInfo;
    }

    public EditBox getEbBillNumber() {
        return ebBillNumber;
    }

    public void setEbBillNumber(EditBox ebBillNumber) {
        this.ebBillNumber = ebBillNumber;
    }

    public ScrollBox getSbProduct() {
        return sbProduct;
    }

    public void setSbProduct(ScrollBox sbProduct) {
        this.sbProduct = sbProduct;
    }

    public Button getbOk() {
        return bOk;
    }

    public void setbOk(Button bOk) {
        this.bOk = bOk;
    }

    public String getAvancel_gate_1() {
        return avancel_gate_1;
    }

    public void setAvancel_gate_1(String avancel_gate_1) {
        this.avancel_gate_1 = avancel_gate_1;
    }

    public String getCitypay_gate() {
        return citypay_gate;
    }

    public void setCitypay_gate(String citypay_gate) {
        this.citypay_gate = citypay_gate;
    }

    public String getCitypay_gate_life() {
        return citypay_gate_life;
    }

    public void setCitypay_gate_life(String citypay_gate_life) {
        this.citypay_gate_life = citypay_gate_life;
    }

    public String getCyberplat_gate() {
        return cyberplat_gate;
    }

    public void setCyberplat_gate(String cyberplat_gate) {
        this.cyberplat_gate = cyberplat_gate;
    }

    public String getDacard_gate() {
        return dacard_gate;
    }

    public void setDacard_gate(String dacard_gate) {
        this.dacard_gate = dacard_gate;
    }

    public String getKyivstar_bonus_no_commission_gate() {
        return kyivstar_bonus_no_commission_gate;
    }

    public void setKyivstar_bonus_no_commission_gate(String kyivstar_bonus_no_commission_gate) {
        this.kyivstar_bonus_no_commission_gate = kyivstar_bonus_no_commission_gate;
    }

    public String getKyivstar_exclusive_gate() {
        return kyivstar_exclusive_gate;
    }

    public void setKyivstar_exclusive_gate(String kyivstar_exclusive_gate) {
        this.kyivstar_exclusive_gate = kyivstar_exclusive_gate;
    }

    public String getKyivstar_gate() {
        return kyivstar_gate;
    }

    public void setKyivstar_gate(String kyivstar_gate) {
        this.kyivstar_gate = kyivstar_gate;
    }

    public String getUmc_gate() {
        return umc_gate;
    }

    public void setUmc_gate(String umc_gate) {
        this.umc_gate = umc_gate;
    }

    public String getBillingNum() {
        return billingNum;
    }

    public void setBillingNum(String billingNum) {
        this.billingNum = billingNum;
    }

    public String getClientNum() {
        return clientNum;
    }

    public void setClientNum(String clientNum) {
        this.clientNum = clientNum;
    }

    public String getTransactionTime() {
        return transactionTime;
    }

    public void setTransactionTime(String transactionTime) {
        this.transactionTime = transactionTime;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public String getTerminalSn() {
        return terminalSn;
    }

    public void setTerminalSn(String terminalSn) {
        this.terminalSn = terminalSn;
    }

    public String getSellerCode() {
        return sellerCode;
    }

    public void setSellerCode(String sellerCode) {
        this.sellerCode = sellerCode;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
