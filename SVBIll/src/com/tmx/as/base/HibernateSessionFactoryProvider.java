package com.tmx.as.base;

import com.tmx.as.base.jdbc.JdbcAdapterFactory;
import com.tmx.as.exceptions.DatabaseException;
import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.metadata.ClassMetadata;

import java.io.File;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;


public class HibernateSessionFactoryProvider {
    /** Array of session factories */
    private static SessionFactory[] sessionFactories;
    /** Array of configuration files. Elements in this array are corresponds
     * the elements from sessionFactories by index. */
    private static String[] hibernateConfigFilesNames;
    /** Contains pairs of entity name and corresponding session factory instance.
     * [String entityName][SessionFactory sessionFactory].
     * Used to quickly get required sessionFactory for given entity */
    private static Map entityInSessionFactory = new HashMap();
    /** Contains pairs of Sessoin Factory Id and corresponding session factory instance.
     * [String sessionFactoryId][SessionFactory sessionFactory]
     * Used to quickly get required sessionFactory by its id. */
    private static Map sessionFactoryById = new HashMap();
    /** The reverse of sessionFactoryById:
     * [SessionFactory sessionFactory][String sessionFactoryId]
     * Used to quickly get required id of sessionFactory. */
    private static Map sessionFactoryByIdReverse = new HashMap();
    /** Map of all entities metadata in application scope (Joined from all session factories).  */
    private static Map allClassMetadata = new HashMap();
    /** Map of configurationsBySF under sessionFactories ID */
    private static Map configurationsBySF;
    private static Logger logger = Logger.getLogger("core.HibernateSessionFactoryProvider");


    static {
        try {
            hibernateConfigFilesNames = com.tmx.util.Configuration.getInstance().getPropertyAsArray("hibernate.config_files");
            sessionFactories = new SessionFactory[hibernateConfigFilesNames.length];
            configurationsBySF = new HashMap();
            for(int i=0; i<hibernateConfigFilesNames.length; i++){
                logger.info("Configure hibernate session factory from '"+hibernateConfigFilesNames[i]+"'... ");
                //Create and hold session factory
                Configuration cfg = new Configuration().configure(new File(hibernateConfigFilesNames[i]));
                sessionFactories[i] = cfg.buildSessionFactory();

                //Store factory by its id into sessionFactoryById and sessionFactoryByIdReverse
                String sessionFactoryId = cfg.getProperty("tmx.session_factory_id");
                if(sessionFactoryId == null)
                    throw new ExceptionInInitializerError("Mandatory property 'tmx.session_factory_id' is not found in one of the hbm.xml mapping files.");
                if(sessionFactoryById.containsKey(sessionFactoryId))
                    throw new ExceptionInInitializerError("Not unique value of property 'tmx.session_factory_id = "+sessionFactoryId+"' is found among hbm.xml mapping files.");
                sessionFactoryById.put(sessionFactoryId, sessionFactories[i]);
                configurationsBySF.put(sessionFactories[i], cfg);
                sessionFactoryByIdReverse.put(sessionFactories[i], sessionFactoryId);


                //1) Load entitity names from this sessionFactory to the entityInSessionFactory.
                //2) load entities metadatas into allClassMetadata
                Map entityMetadatas = sessionFactories[i].getAllClassMetadata();
                for(Iterator entityNameIter = entityMetadatas.keySet().iterator(); entityNameIter.hasNext();){
                    String entityName = (String)entityNameIter.next();
                    entityInSessionFactory.put(entityName, sessionFactories[i]);
                    if(allClassMetadata.containsKey(entityName))
                        throw new ExceptionInInitializerError("Duplicate definition of entity '"+entityName+"'. Check all hibernate config files.");
                    else
                        allClassMetadata.put(entityName, entityMetadatas.get(entityName));
                }

                //init jdbc adapter facotry
                JdbcAdapterFactory.getInstance().init(cfg.getProperty("hibernate.connection.datasource"), sessionFactoryId);
                logger.info("Successfully.");
            }
            // Create the SessionFactory from hibernate.cfg.xml
//            sessionFactory = new Configuration().configure(new File(System.getProperty("tmx.home") +
//                    File.separator + "conf" + File.separator + "hibernate.cfg.xml"))
//                    .buildSessionFactory();
        }
        catch (Throwable ex) {
            // Make sure you log the exception, as it might be swallowed
            logger.error("Initial SessionFactory creation failed." + ex);
            throw new ExceptionInInitializerError(ex);
        }
    }

    public static SessionFactory getSessionFactory(Class entityClass) throws DatabaseException {
        return getSessionFactory(entityClass.getName());
    }

    public static Configuration getConfiguration(Class entityClass) throws DatabaseException {
        return getConfiguration(entityClass.getName());
    }

    public static SessionFactory getSessionFactory(String entityClassName) throws DatabaseException {
        SessionFactory sessFactory = (SessionFactory)entityInSessionFactory.get(entityClassName);
        if(sessFactory == null)
            throw new DatabaseException("err.entity_not_configured_in_config", new String[]{entityClassName});
        return sessFactory;
    }

    public static Configuration getConfiguration(String entityClassName) throws DatabaseException {
        Configuration cfg = (Configuration) configurationsBySF.get(getSessionFactory(entityClassName));
        if(cfg == null)
            throw new DatabaseException("err.entity_not_configured_in_config", new String[]{entityClassName});
        return cfg;
    }

    public static SessionFactory getSessionFactory(Entity entity) throws DatabaseException {
        return getSessionFactory(getUnwrapedFromCglibEnchancerClassName(entity));
    }

    public static Configuration getConfiguration(Entity entity) throws DatabaseException {
        return getConfiguration(getUnwrapedFromCglibEnchancerClassName(entity));
    }

    /** On <code>load</code> operations CGLIB enchancer replaces original entity instance with enchanced one.
     * For example for <code>com.tmx.as.entities.bill.seller.Seller</code> it can produces
     * <code>com.tmx.as.entities.bill.seller.Seller$$EnhancerByCGLIB$$8ab9d555</code>. To look <code>SessionFactory, Configuration
     * </code> or another object in our maps by entity class name, we should unwrap the enchanced class name to original.
     * @param entity entity to get its unwrapped class name
     * @return unwrapped entity class name 
     * */
    private static String getUnwrapedFromCglibEnchancerClassName(Entity entity){
        final String possibleWrappedName = entity.getClass().getName();
        final int idx = possibleWrappedName.indexOf("$$EnhancerByCGLIB$$");
        return idx < 0 ? possibleWrappedName : possibleWrappedName.substring(0, idx);
    }

    public static SessionFactory getSessionFactoryById(String sessionFactoryId) {
        return (SessionFactory)sessionFactoryById.get(sessionFactoryId);
    }

    public static Configuration getConfigurationById(String sessionFactoryId) {
        return (Configuration) configurationsBySF.get(getSessionFactoryById(sessionFactoryId));
    }

    /**
     * To test DB connection when application is starting
     */
    public static void testConnections() {
        logger.info("Hibernate DB connections testing ...");
        for(int i=0; i<sessionFactories.length; i++){
            logger.info("Test hibernate session factory '"+hibernateConfigFilesNames[i]+"'... ");
            Session session = sessionFactories[i].getCurrentSession();
            session.beginTransaction();
            session.getTransaction().rollback();
            sessionFactories[i].close();
            logger.info("Successfully.");
        }
        logger.info("All connections are successful.");
    }
    

    public static Map getAllClassMetadata(){
        return allClassMetadata;
    }

    public static ClassMetadata getClassMetadata(Class entityClass){
        return (ClassMetadata)allClassMetadata.get(entityClass.getName());
    }


    public static String resolveSessionFactoryId(Entity entity) throws DatabaseException{
        return (String)sessionFactoryByIdReverse.get(getSessionFactory(entity));
    }

    public static String resolveSessionFactoryId(Class entityClass) throws DatabaseException{
        return (String)sessionFactoryByIdReverse.get(getSessionFactory(entityClass));
    }
}
