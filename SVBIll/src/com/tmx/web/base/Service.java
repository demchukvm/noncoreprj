package com.tmx.web.base;

import javax.servlet.ServletContext;

public interface Service {
    public void init(ServletContext context) throws com.tmx.util.InitException;
}
