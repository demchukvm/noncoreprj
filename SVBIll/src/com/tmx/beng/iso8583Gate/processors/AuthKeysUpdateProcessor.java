package com.tmx.beng.iso8583Gate.processors;

import com.tmx.beng.iso8583Gate.pdu.RuntimeMessage;
import com.tmx.beng.iso8583Gate.pdu.IncorrectFormatException;
import com.tmx.beng.iso8583Gate.pdu.util.HexConverter;
import com.tmx.beng.iso8583Gate.mapper.BillingMapper;
import com.tmx.beng.iso8583Gate.mapper.BillingMapperMetadata;
import com.tmx.beng.iso8583Gate.mac.MacController;
import com.tmx.beng.iso8583Gate.mac.MacException;
import com.tmx.beng.base.BillException;
import com.tmx.as.exceptions.DatabaseException;
import com.tmx.as.entities.iso8583.transaction.Iso8583Transaction;
import com.tmx.as.entities.iso8583.key.Key;
import com.tmx.as.entities.iso8583.key.MasterKey;
import com.tmx.as.base.EntityManager;
import com.tmx.as.base.FilterWrapper;
import com.tmx.util.InitException;
import java.util.Date;

/**
 */
public class AuthKeysUpdateProcessor extends BasicProcessor{

    public int getMessageTypeId() {
        return 810;
    }

    public int getProcessingCode() {
        return 960000;
    }

    public int[] getDEToCopy() {
        return new int[]{11, 24, 41};
    }

    public void processSpecificTask(RuntimeMessage reqMsg, RuntimeMessage respMsg)
        throws ProcessingException, BillException, IncorrectFormatException, InitException, DatabaseException, MacException{

        logger.debug("Auth keys update");
        BillingMapper billingMapper = BillingMapperMetadata.getInstance().getBillMapper(reqMsg);

        //prepare and pre-save iso transaction in iso datasource
        Iso8583Transaction tx = billingMapper.preSaveTransaction(reqMsg);
        //Update current MAC key
        Key key = new MacController().updateMacKey(tx.getTerminalId());
        billingMapper.prepare0810_960000Msg(respMsg, key);
    }
}
