package com.tmx.web.taglib.html;

import com.tmx.web.taglib.base.TableBasicTag;
import com.tmx.web.controls.table.FilterSet;

import javax.servlet.jsp.JspException;

import org.apache.struts.taglib.TagUtils;

/**

 */
public abstract class AbstractFilterSetTag extends TableBasicTag {

    protected static String FILTER_SET_VISIBLE_NAME = ".filterSet.visible";
    //Constant for autopopulation, find and set comand in the filter set in table
    protected static final String FILTERSET_NAME = "filterSet";
    protected static final String FILTERSET_COMMAND_NAME = "filterSetCommand";

    public int doStartTmxTag() throws JspException {
        StringBuffer sb = new StringBuffer();
        String tableName = getTableName();
        sb.append("<input type='hidden' name='").append(tableName).append(FILTER_SET_VISIBLE_NAME).append("'").
                append("value='").append(getFilterSetObject().isVisible()).append("'/>");

        //optional refresh command to support dispatchedActions
        String refreshCommand = getTable().getRefreshCommandName();
        if(refreshCommand != null){
            sb.append("<input type='hidden' name='").append("command").append("'").
                append("value='").append(refreshCommand).append("'/>");
        }

        TagUtils.getInstance().write(pageContext, sb.toString());
        return EVAL_BODY_INCLUDE;
    }

    public int doEndTmxTag() throws JspException{
        //RM: to prevent nested forms   TagUtils.getInstance().write(pageContext, "</form>");
        return EVAL_PAGE;
    }

    protected FilterSet getFilterSetObject() throws JspException{
        return getTable().getFilterSet();
    }

}
