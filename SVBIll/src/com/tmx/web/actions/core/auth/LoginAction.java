package com.tmx.web.actions.core.auth;

import com.tmx.as.entities.general.user.User;
import com.tmx.web.base.*;
import com.tmx.web.base.menuservice.Menu;
import com.tmx.web.base.menuservice.MenuService;
import com.tmx.web.forms.core.auth.LoginForm;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.RedirectingActionForward;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class LoginAction extends BasicDispatchedAction
{
    private static final String FORWARD = "login";

    public ActionForward defaultAction(ActionMapping mapping,
                                          ActionForm form,
                                          HttpServletRequest request,
                                          HttpServletResponse response,
                                          String methodName) throws Throwable {

        LoginForm loginForm = (LoginForm) verifyForm(form, LoginForm.class);

        loginForm.getEbLogin().reset();
        loginForm.getEbPassword().reset();

        return mapping.findForward(FORWARD);
    }

    public ActionForward login(ActionMapping mapping,
                                               ActionForm form,
                                               HttpServletRequest request,
                                               HttpServletResponse response) throws Throwable {

        LoginForm loginForm = (LoginForm) verifyForm(form, LoginForm.class);
        loginForm.validate();
        SecurityService securityService = (SecurityService) ApplicationEnvironment.getService(SecurityService.class);
        User user = securityService.login(loginForm.getEbLogin().getValue(), loginForm.getEbPassword().getValue(), request.getSession());

        Menu menu = MenuService.getInstance().initializeMenu("main", request);
        String startUpUrl = menu.getFirstItemURL();
        if(startUpUrl == null) throw new WebException("err.login_action.there_are_any_appropriate_menu_item_to_open");

        startUpUrl = ((startUpUrl.startsWith(ApplicationEnvironment.getAppDeploymentName()) ?
                      startUpUrl.substring(ApplicationEnvironment.getAppDeploymentName().length()) :
                      startUpUrl));

        return new RedirectingActionForward(startUpUrl);
    }

}
