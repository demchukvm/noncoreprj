package com.tmx.as.resolver.functions;

import com.tmx.as.resolver.FunctionSyntaxException;

public class Int implements Function{
    private Integer value;
    public void set(int paramIndex, String paramName, Object paramValue) throws FunctionSyntaxException {
        value=new Integer((String)paramValue);
    }

    public Object returnValue() throws FunctionSyntaxException {
        return value;
    }
}
