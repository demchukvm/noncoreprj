package com.tmx.util.jce.mac.redundant;

import com.tmx.util.jce.mac.AnsiXMacSpi;
import com.tmx.util.jce.mac.AnsiX99MacSpi;
import com.tmx.util.jce.mac.AnsiX919MacSpi;

import java.util.Map;
import java.util.HashMap;
import java.security.NoSuchAlgorithmException;

/**
   TODO Redundant
   This is temporary factory for use while com.tmx.util.jce.Provider will not signed.
 */
public class MACFactory {
    private static final Map macs = new HashMap();

    static{
        register("X9_9", AnsiX99MacSpi.class);
        register("X9_19", AnsiX919MacSpi.class);
    }


    public static AnsiXMacSpi getInstance(String algorithm) throws NoSuchAlgorithmException {
        Class macImpl = (Class)macs.get(algorithm);
        try{
            return (AnsiXMacSpi)macImpl.newInstance();
        }
        catch(Exception e){
            throw new NoSuchAlgorithmException("err.mac_factory.mac_instantiation"+algorithm);
        }
    }

    private static void register(String algorithmName, Class macImpl) {
        macs.put(algorithmName, macImpl);
    }
}
