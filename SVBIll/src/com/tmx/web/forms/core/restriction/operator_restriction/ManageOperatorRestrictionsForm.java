package com.tmx.web.forms.core.restriction.operator_restriction;

import com.tmx.as.blogic.OperatorManager;
import com.tmx.as.entities.bill.operator.Operator;
import com.tmx.as.entities.general.user.User;
import com.tmx.as.exceptions.DatabaseException;
import com.tmx.web.controls.ScrollBox;
import com.tmx.web.controls.table.Parameter;
import com.tmx.web.forms.core.restriction.AbstractManageRestrictionsForm;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;


public class ManageOperatorRestrictionsForm extends AbstractManageRestrictionsForm {

    protected void init(){
        super.init();
        initRestrictionsTable();
        initButtons();
    }

    private void initRestrictionsTable() {
        if(getRestrictionsTable()!=null){
            getRestrictionsTable().loadQuery("query(query_type=retrieve_all," +
                    "entity_class=com.tmx.as.entities.bill.restriction.OperatorRestriction," +
                    "table_attr=functionInstance.functionType," +
                    "table_attr=operator," +
                    "order(name=restrictionId,type=asc)," +
                    "page_size=15" +
                    ")");
            getRestrictionsTable().setRefreshActionName("/core/restriction/operator_restriction/manageOperatorRestrictions");
            initFilters();
        }
    }

    private void initFilters(){
        getRestrictionsTable().addParameterToFilter("by_operator_id", new Parameter("operator_id",initOperatorsScrollBoxFilter()));
    }

    private ScrollBox initOperatorsScrollBoxFilter(){
        ScrollBox operatorsScrollBox = new ScrollBox(){
            protected String getKey(Object dataListElement) {
                return ((Operator)dataListElement).getId().toString();
            }

            protected String getValue(Object dataListElement) {
                return ((Operator)dataListElement).getName();
            }
        };
        operatorsScrollBox.setNullable(true);
        return operatorsScrollBox;
    }

    public void bindOperators(List<Operator> operators) {
        ScrollBox operatorsScrollBox = (ScrollBox) getRestrictionsTable().getParameter("by_operator_id", "operator_id").getControl();

        Collections.sort(operators, new Comparator<Operator>()
        {
          public int compare(Operator op1, Operator op2) {
            return op1.getName().compareTo(op2.getName());
          }
        });

        operatorsScrollBox.bind(operators);
    }
    
    private void initButtons() {
        getOnCreateButton().setAction("/core/restriction/operator_restriction/manageOperatorRestrictions");
        getOnEditButton().setAction("/core/restriction/operator_restriction/infoOperatorRestriction");
        getOnDeleteButton().setAction("/core/restriction/operator_restriction/infoOperatorRestriction");
//        getOnCreateIRButton().setAction("/core/restriction/operator_restriction/manageOperatorRestrictions");
//        getOnEditIRButton().setAction("/core/restriction/operator_restriction/infoOperatorRestriction");
//        getOnDeleteIRButton().setAction("/core/restriction/operator_restriction/infoOperatorRestriction");
    }

}
