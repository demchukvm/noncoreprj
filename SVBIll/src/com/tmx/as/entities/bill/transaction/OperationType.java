package com.tmx.as.entities.bill.transaction;

import com.tmx.as.base.BasicEntity;

import java.io.Serializable;


public class OperationType extends BasicEntity {

    private Long operationTypeId;
    private String name;
    private String description;


    public Long getOperationTypeId() {
        return operationTypeId;
    }

    public void setOperationTypeId(Long operationTypeId) {
        this.operationTypeId = operationTypeId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Serializable getId() {
        return getOperationTypeId();  
    }

    public int getValid() {
        return valid;  
    }

    public void setValid(int valid) {
        this.valid = valid;
    }
    
}
