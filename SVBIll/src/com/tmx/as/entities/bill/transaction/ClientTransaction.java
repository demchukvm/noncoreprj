package com.tmx.as.entities.bill.transaction;

import com.tmx.as.entities.bill.seller.Seller;
import com.tmx.as.entities.bill.operator.Operator;
import com.tmx.as.entities.bill.terminal.Terminal;
import com.tmx.as.entities.bill.balance.OperatorBalance;
import com.tmx.as.entities.bill.balance.SellerBalance;
import com.tmx.as.entities.bill.balance.TerminalBalance;
import com.tmx.as.entities.bill.voucher.Voucher;

import java.util.Set;

/**
 */
public class ClientTransaction extends AbstractTransaction{
    private Operator operator;
    private OperatorBalance operatorBalance;
    private Seller seller;
    private Voucher voucher;
    private SellerBalance sellerBalance;
    private Terminal terminal;
    private TerminalBalance terminalBalance;
    /** the name of operation triggered this transaction : refill, buyVoucher etc */
    private String operationName;
    /** code of service proviced in this transaction */
    private String serviceCode;
    /** account number (msisdn) used in this transaction */
    private String accountNumber;
    /** contract number used in this transaction */
    private String contractNumber;
    /** voucher nominal used in this transaction */
    private String voucherNominal;

    private Set operatorTransactions;
    private Set sellerTransactions;
    private Set terminalTransactions;

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    //------------Properties
    public Operator getOperator() {
        return operator;
    }

    public void setOperator(Operator operator) {
        this.operator = operator;
    }

    public Seller getSeller() {
        return seller;
    }

    public void setSeller(Seller seller) {
        this.seller = seller;
    }

    public Terminal getTerminal() {
        return terminal;
    }

    public void setTerminal(Terminal terminal) {
        this.terminal = terminal;
    }

    public TerminalBalance getTerminalBalance() {
        return terminalBalance;
    }

    public void setTerminalBalance(TerminalBalance terminalBalance) {
        this.terminalBalance = terminalBalance;
    }

    public OperatorBalance getOperatorBalance() {
        return operatorBalance;
    }

    public void setOperatorBalance(OperatorBalance operatorBalance) {
        this.operatorBalance = operatorBalance;
    }

    public SellerBalance getSellerBalance() {
        return sellerBalance;
    }

    public void setSellerBalance(SellerBalance sellerBalance) {
        this.sellerBalance = sellerBalance;
    }

    public String getOperationName() {
        return operationName;
    }

    public void setOperationName(String operationName) {
        this.operationName = operationName;
    }

    public String getServiceCode() {
        return serviceCode;
    }

    public void setServiceCode(String serviceCode) {
        this.serviceCode = serviceCode;
    }

    public String getContractNumber() {
        return contractNumber;
    }

    public void setContractNumber(String contractNumber) {
        this.contractNumber = contractNumber;
    }

    public String getVoucherNominal() {
        return voucherNominal;
    }

    public void setVoucherNominal(String voucherNominal) {
        this.voucherNominal = voucherNominal;
    }

    public Set getOperatorTransactions() {
        return operatorTransactions;
    }

    public void setOperatorTransactions(Set operatorTransactions) {
        this.operatorTransactions = operatorTransactions;
    }

    public Set getSellerTransactions() {
        return sellerTransactions;
    }

    public void setSellerTransactions(Set sellerTransactions) {
        this.sellerTransactions = sellerTransactions;
    }

    public Set getTerminalTransactions() {
        return terminalTransactions;
    }

    public void setTerminalTransactions(Set terminalTransactions) {
        this.terminalTransactions = terminalTransactions;
    }

    public Voucher getVoucher() {
        return voucher;
    }

    public void setVoucher(Voucher voucher) {
        this.voucher = voucher;
    }
}
