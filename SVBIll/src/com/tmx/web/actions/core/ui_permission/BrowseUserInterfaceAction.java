package com.tmx.web.actions.core.ui_permission;

import com.tmx.web.controls.ScrollBox;
import com.tmx.web.actions.core.base.AbstractBrowseAction;
import com.tmx.web.forms.core.ui_permission.BrowseUserInterfaceForm;
import com.tmx.as.base.EntityManager;
import com.tmx.as.entities.general.ui_permission.UserInterfaceType;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForm;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class BrowseUserInterfaceAction extends AbstractBrowseAction {
    private static final String FORWARD = "page";
    protected String getForwardName() {
        return FORWARD;
    }

    public ActionForward openBrowseWindow(ActionMapping mapping,
                                       ActionForm form,
                                       HttpServletRequest request,
                                       HttpServletResponse response) throws Throwable {

        super.openBrowseWindow(mapping, form, request, response);

        BrowseUserInterfaceForm manageUserInterfaceForm = (BrowseUserInterfaceForm) verifyForm(form, BrowseUserInterfaceForm.class);
        manageUserInterfaceForm.getBrowseTable().setRefreshActionName(this.getName());
        ((ScrollBox) manageUserInterfaceForm.getBrowseTable().// bind services
                getFilterSet().getFilter("com_tmx_as_entities_general_ui_permission_UserInterface_type").
                getParameter("value").getControl()).bind(
                new EntityManager().RETRIEVE_ALL(UserInterfaceType.class,null)
        );
        return mapping.findForward(getForwardName());
    }
}
