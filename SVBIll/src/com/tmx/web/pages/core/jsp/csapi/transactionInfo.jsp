<%@ taglib prefix="nested" uri="http://jakarta.apache.org/struts/tags-nested" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/tld/tmx-controls.tld" prefix="tmx_ctrl" %>

<table border="0">
	<tr>
		<td rowspan="2">
			<table style="border: dotted 1px silver;">
				<tr>
					<th colspan="2">Данные SVBill</th>
				</tr>
				<tr>
					<td align="center">BillTransaction</td>
					<td><tmx_ctrl:editBox name="transactionInfoForm" property="billTransaction" /></td>
				</tr>
				<tr>
					<td align="center">ClientTransaction</td>
					<td><tmx_ctrl:editBox name="transactionInfoForm" property="cliTransaction"/></td>
				</tr>
				<tr>
					<td align="center">StatusCode</td>
					<td><tmx_ctrl:editBox name="transactionInfoForm" property="statusCode"/></td>
				</tr>
				<tr>
					<td colspan="2" align="center">-</td>
				</tr>
				<tr>
					<td align="center">StatusMessage</td>
					<td><tmx_ctrl:editBox name="transactionInfoForm" property="statusMessage"/></td>
				</tr>
				<tr>
					<td align="center">TransactionTime</td>
					<td><tmx_ctrl:editBox name="transactionInfoForm" property="transactionTime"/></td>
				</tr>
				<tr>
					<td align="center">ClientTime</td>
					<td><tmx_ctrl:editBox name="transactionInfoForm" property="clientTime"/></td>
				</tr>
				<tr>
					<td align="center">AccountNumber</td>
					<td><tmx_ctrl:editBox name="transactionInfoForm" property="accountNumber"/></td>
				</tr>
				<tr>
					<td align="center">Amount</td>
					<td><tmx_ctrl:editBox name="transactionInfoForm" property="amount"/></td>
				</tr>
				<tr>
					<td align="center">Seller</td>
					<td><tmx_ctrl:editBox name="transactionInfoForm" property="sellerName"/></td>
				</tr>
				<tr>
					<td align="center">Terminal</td>
					<td><tmx_ctrl:editBox name="transactionInfoForm" property="terminalNumber"/></td>
				</tr>
				<tr>
					<td align="center">Product</td>
					<td><tmx_ctrl:editBox name="transactionInfoForm" property="operatorCode"/></td>
				</tr>
				<tr>
					<td colspan="2" align="center"><tmx_ctrl:button property="onInfoButton"/></td>
				</tr>
			</table>
		</td>
		<td width="40px" rowspan="2"/>
		<td>
			<table style="border: dotted 1px silver;">
				<tr>
					<th colspan="2">Данные Kyivstar (ACT=3)</th>
				</tr>
				<tr>
					<td align="center">PAY_ID</td>
					<td><tmx_ctrl:editBox name="transactionInfoForm" property="payId" /></td>
				</tr>
				<tr>
					<td align="center">RECEIPT</td>
					<td><tmx_ctrl:editBox name="transactionInfoForm" property="receiptNumMobipay"/></td>
				</tr>
				<tr>
					<td align="center">PAY_STATUS</td>
					<td><tmx_ctrl:editBox name="transactionInfoForm" property="payStatusMobipay"/></td>
				</tr>
				<tr>
					<td align="center">STATUS_CODE</td>
					<td><tmx_ctrl:editBox name="transactionInfoForm" property="statusCodeMobipay"/></td>
				</tr>
				<tr>
					<td align="center">CTS</td>
					<td><tmx_ctrl:editBox name="transactionInfoForm" property="ctsMobipay"/></td>
				</tr>
				<tr>
					<td colspan="2" align="center"><tmx_ctrl:selectBox name="transactionInfoForm" property="scrollBoxKyivstarGates" width="100%"/></td>
				</tr>
				<tr>
					<td colspan="2" align="center"><tmx_ctrl:button property="onInfoMobipayButton"/></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td align="center" style="vertical-align:top;">
			<br>
			<tmx_ctrl:button property="onCancelButton"/>
			<br><br>
            <table border="1">
                <tr><td>PAY_ID</td><td>ID транзакції в Київстар</td></tr>
                <tr><td>RECEIPT</td><td>номер чека</td></tr>
                <tr><td>PAY_STATUS</td><td>статус транзакції</td></tr>
                <tr><td>STATUS_CODE</td><td>результат операції</td></tr>
                <tr><td>CST</td><td>дата встановлення статусу транзакції</td></tr>
            </table>
		</td>
	</tr>
</table>

<br><br>

<b>PAY_STATUS</b>
<table border="1">
	<tr><th>Код</th><th>Опис</th></tr>
    <tr><td>101</td><td>Дані коректні. Платіж прийнятий на виконання.</td></tr>
    <tr><td>102</td><td>Розпочато перевірку можливості платежу.</td></tr>
    <tr><td>110</td><td>Платіж підтверджений. Трансакція поставлена в чергу платежів у білінгову систему.</td></tr>
    <tr><td>111</td><td>Платіж успішно завершений.</td></tr>
    <tr><td>112</td><td>Некритична помилка. Повтор проведення платежу.</td></tr>
    <tr><td>-104 / 10400</td><td>Платіж скасований користувачем.</td></tr>
    <tr><td>-111 / 11100</td><td>Критична помилка. Платіж не виконаний.</td></tr>
    <tr><td>-112 / 11200</td><td>Перевищено кількість спроб. Платіж відхилений.</td></tr>
    <tr><td>-102 / 10200</td><td>Перевірка неуспішна. Платіж неможливий.</td></tr>
    <tr><td>120</td><td>Платіж перебуває в обробці тарифікаційною системою.</td></tr>
    <tr><td>-103 / 10300</td><td>Платіж скасований по таймауту.</td></tr>
    <tr><td>115</td><td>Платіж анульований.</td></tr>
    <tr><td>116</td><td>Платіж анульовано у зовнішній білінговій системі.</td></tr>
</table>

<br><br>

<b>STATUS_CODE</b>
<table border="1">
	<tr><th>Код</th><th>Опис</th></tr>
    <tr><td>-96 / 9600</td><td>У абонента немає послуг для замовлення</td></tr>
    <tr><td>-95 / 9500</td><td>Помилка отримання даних опису послуг</td></tr>
    <tr><td>-94 / 9400</td><td>Помилка отримання даних про доступні абоненту послуги</td></tr>
    <tr><td>-93 / 9300</td><td>Функціонал замовлення послуг вимкнено</td></tr>
    <tr><td>-92 / 9200</td><td>Невірно заданий код ідентифікатора послуги в команді АСТ=0</td></tr>
    <tr><td>-89 / 8900</td><td>Транзакція знаходиться в процесі анулювання</td></tr>
    <tr><td>-87 / 8700</td><td>Анулювання платежу неможливе тому що неможливо анулювати бонус</td></tr>
    <tr><td>-86 / 8600</td><td>Анулювання платежу неможливе, тому що платіж уже врахований у білінгу</td></tr>
    <tr><td>-85 / 8500</td><td>Анулювання платежу неможливе для цього абонента</td></tr>
    <tr><td>-84 / 8400</td><td>Платіж уже був анульований</td></tr>
    <tr><td>-83 / 8300</td><td>У білінговій системі по чеку знайдено більше одного платежу</td></tr>
    <tr><td>-82 / 8200</td><td>Платіж не знайдено у білінговій системі</td></tr>
    <tr><td>-81 / 8100</td><td>Час можливості анулювання платежу минув</td></tr>
    <tr><td>-60 / 6000</td><td>Відмовлено в зміні пароля</td></tr>
    <tr><td>-59 / 5900</td><td>Сервіс недоступний (внутрішній збій системи)</td></tr>
    <tr><td>-51 / 5100</td><td>Помилка сценарію</td></tr>
    <tr><td>-50 / 5000</td><td>Помилка виконання запиту в БД</td></tr>
    <tr><td>-49 / 4900</td><td>Збій білінгової системи</td></tr>
    <tr><td>-48 / 4800</td><td>Перевищено кількість спроб платежу</td></tr>
    <tr><td>-46 / 4600</td><td>Платіж зареєстрований, але не проведений</td></tr>
    <tr><td>-42 / 4200</td><td>Платіж на зазначену суму неможливий для цього абонента</td></tr>
    <tr><td>-41 / 4100</td><td>Приймання платежів для абонента неможливе</td></tr>
    <tr><td>-40 / 4000</td><td>Абонент не знайдений</td></tr>
    <tr><td>-38 / 3800</td><td>Перевищено кількість спроб проведення платежу</td></tr>
    <tr><td>-37 / 3700</td><td>Помилка одержання інформації про квоту</td></tr>
    <tr><td>-36 / 3600</td><td>Квота платежів відключена</td></tr>
    <tr><td>-35 / 3500</td><td>Квота платежів вичерпана</td></tr>
    <tr><td>-34 / 3400</td><td>Приймання платежів від партнера заблоковане</td></tr>
    <tr><td>-32 / 3200</td><td>Невідомий тип джерела платежу</td></tr>
    <tr><td>-31 / 3100</td><td>Сума платежу поза діапазоном, заданим для джерела платежу</td></tr>
    <tr><td>-27 / 2700</td><td>Трансакція не знайдена</td></tr>
    <tr><td>-26 / 2600</td><td>Операція недопустима для цієї трансакції</td></tr>
    <tr><td>-25 / 2500</td><td>Доступ заборонено</td></tr>
    <tr><td>-20 / 2000</td><td>Створення трансакції неможливе</td></tr>
    <tr><td>-19 / 1900</td><td>Невірний формат суми платежу</td></tr>
    <tr><td>-18 / 1800</td><td>Невірний формат дати платежу</td></tr>
    <tr><td>-17 / 1700</td><td>Не заданий пароль</td></tr>
    <tr><td>-16 / 1600</td><td>Заданий недопустимий параметр</td></tr>
    <tr><td>-15 / 1500</td><td>Не заданий ідентифікатор трансакції</td></tr>
    <tr><td>-14 / 1400</td><td>Валюта не підтримується</td></tr>
    <tr><td>-13 / 1300</td><td>Не задана сума платежу</td></tr>
    <tr><td>-12 / 1200</td><td>Номер одержувача платежу або особовий рахунок не заданий або невірний</td></tr>
    <tr><td>-11 / 1100</td><td>Дія не передбачена</td></tr>
    <tr><td>-10 / 1000</td><td>Не заданий номер чека</td></tr>
    <tr><td>-3 / 300</td><td>Невірний логін/пароль</td></tr>
    <tr><td>-2 / 200</td><td>Доступ з цього IP не передбачений</td></tr>
    <tr><td>20</td><td>Трансакція створена</td></tr>
    <tr><td>21</td><td>Платіж можливий</td></tr>
    <tr><td>22</td><td>Платіж підтверджений</td></tr>
    <tr><td>23</td><td>Платіж скасований</td></tr>
    <tr><td>30</td><td>Запит успішно завершений</td></tr>
    <tr><td>60</td><td>Пароль успішно змінений</td></tr>
    <tr><td>70</td><td>Стан трансакції визначений</td></tr>
    <tr><td>80</td><td>Анулювання платежу успішно проведене</td></tr>
    <tr><td>91</td><td>Одержання інформації про список послуг, яки можуть бути заказані абонентом, виконано успішно</td></tr>
    <tr><td>92</td><td>Одержання інформації про послуги, яки пропонує  Оператор, виконано успішно</td></tr>
</table>