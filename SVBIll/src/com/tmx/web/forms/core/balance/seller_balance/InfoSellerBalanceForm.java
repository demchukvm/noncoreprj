package com.tmx.web.forms.core.balance.seller_balance;

import com.tmx.as.entities.bill.balance.AbstractBalance;
import com.tmx.as.entities.bill.balance.SellerBalance;
import com.tmx.as.entities.bill.seller.Seller;
import com.tmx.as.entities.bill.operator.Operator;
import com.tmx.web.controls.PopulateUtil;
import com.tmx.web.controls.ScrollBox;
import com.tmx.web.controls.table.ControlManagerTable;
import com.tmx.web.forms.core.balance.AbstractInfoBalanceForm;

public class InfoSellerBalanceForm extends AbstractInfoBalanceForm {

    private Long sellerId;
    private ScrollBox scrollBoxOperator;

    protected void initButtonsAction() {
        onApplyButton.setAction("/core/balance/seller_balance/infoSellerBalance");
        onApplyTransButton.setAction("/core/balance/seller_balance/infoSellerBalance");
        onCancelButton.setAction("/core/balance/seller_balance/manageSellerBalances");
        onSaveButton.setAction("/core/balance/seller_balance/infoSellerBalance");
        onChangeBalanceButton.setAction("/core/balance/seller_balance/changeSellerBalance");
        onCreateRestButton.setAction("/core/balance/seller_balance/sellerBalanceRestriction");
        onEditRestButton.setAction("/core/balance/seller_balance/sellerBalanceRestriction");
        onDeleteRestButton.setAction("/core/balance/seller_balance/sellerBalanceRestriction");
    }

    protected ScrollBox initScrollBoxBalanceOwner() {
        return scrollBoxBalanceOwner = new ScrollBox(){
            protected String getKey(Object dataListElement) {
                return ((Seller)dataListElement).getId().toString();
            }
            protected String getValue(Object dataListElement) {
                return ((Seller)dataListElement).getName();
            }
        };
    }

    protected ControlManagerTable initTransactionsTable() {

        ControlManagerTable transactionsTable = new ControlManagerTable("query(query_type=retrieve_all," +
                "entity_class=com.tmx.as.entities.bill.transaction.SellerBalanceTransaction," +
                "table_attr=seller," +
                "table_attr=sellerBalance," +
//                "table_attr=clientTransaction," +
                "table_attr=clientTransaction.operator," +
                "order(name=transactionTime,type=desc)," +
                "page_size=15" +
                ")");
        transactionsTable.setRefreshActionName("/core/balance/seller_balance/infoSellerBalance");
        transactionsTable.setRefreshCommandName("refreshTransactionsTable");

        scrollBoxOperator = new ScrollBox() {

            protected String getKey(Object dataListElement) {
                return ((Operator) dataListElement).getId().toString();
            }

            protected String getValue(Object dataListElement) {
                return ((Operator) dataListElement).getName();
            }
        };
        scrollBoxOperator.setNullable(true);

//        CriterionFactory criterionFactory = new CriterionFactory() {
//
//            public CriterionWrapper createCriterion() {
//                if (!scrollBoxOperator.getSelectedKey().equals(ScrollBox.NULL_KEY)) {
//
//                    return Restrictions.eq( "clientTransaction.operator.operatorId", scrollBoxOperator.getSelectedKey());
//                } else {
//                    return null;
//                }
//            }
//        };
//        transactionsTable.setCriterionFactory(criterionFactory);

        return transactionsTable;
    }

    protected void bindData(AbstractBalance balance) { }

    public AbstractBalance populateFromControls() {
        Long sellerId = PopulateUtil.getLongKey(scrollBoxBalanceOwner);
        SellerBalance balance = new SellerBalance();
        Seller seller = new Seller();
        seller.setSellerId(sellerId);
        balance.setSeller(seller);
        return balance;
    }

    public Long getSellerId() {
        return sellerId;
    }

    public void setSellerId(Long sellerId) {
        this.sellerId = sellerId;
    }

    public ScrollBox getScrollBoxOperator() {
        return scrollBoxOperator;
    }

    public void setScrollBoxOperator(ScrollBox scrollBoxOperator) {
        this.scrollBoxOperator = scrollBoxOperator;
    }
}
