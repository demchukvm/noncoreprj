package com.tmx.beng.base.processors;

import com.tmx.beng.base.*;
import com.tmx.beng.base.cache.DBCache;
import com.tmx.beng.access.Access;
import com.tmx.beng.ussdgate.UssdGate;
import com.tmx.util.InitException;

import javax.naming.InitialContext;
import javax.naming.NamingException;

/**
 * Register message
 */
public class TestProcessor extends TaskProcessor {

    public Task process0(Task task, DBCache dbCache, BillingMessageWritable msg) throws BillException {
        logger.debug("Test...");
        InitialContext ctx = null;
        Access newAccess = null;
        try {
            System.setProperty("java.naming.factory.url.pkgs", ""); //=org.apache.naming
            ctx = new InitialContext();//System.getProperties()
            newAccess = (Access) ctx.lookup("java:comp/services/billingEngine");
            if (newAccess == null)
                throw new BillException("err.csapi.null_basic_access_obtained");
        }
        catch (NamingException e) {
            throw new BillException("err.csapi.failed_to_obtain_billing_engine_access", e);
        }
        finally {
            if (ctx != null) {
                try {
                    ctx.close();
                }
                catch (NamingException e) {
                    //log.info( "Unable to release initial context", ne );
                }
            }
        }

        BillingMessageImpl billingMessage = new BillingMessageImpl();
        billingMessage.setAttributeString(BillingMessage.BILL_TRANSACTION_NUM, task.getTransaction().getTransactionId());
        billingMessage.setOperationName(BillingMessage.O_REFILL_PAYMENT_RESULT);
        //billingMessage.setStatusCode(UssdGate.USSD_STATUS_OK);
        billingMessage.setStatus(111111, "operator problem");
        newAccess.obtainConnection().processAsync(billingMessage);

        return task;
    }

}
