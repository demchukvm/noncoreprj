package com.tmx.web.controls;

import com.tmx.util.i18n.MessageResources;
import com.tmx.web.base.SessionEnvironment;
import com.tmx.web.base.RequestEnvironment;
import com.tmx.web.base.BasicActionForm;

import java.util.*;

 /**
  * Basic abstract control
  * 
  * This class has two state:
  *     a) updated equal false when componen isn't initialize
  *     b) updated equal true when componen is initialize
  */
public abstract class BasicControl implements EventListener {
    private final String RESOURCE_BANDLE_NAME = "ctrl_labels";
    private BasicActionForm owner = null;
    private BasicControl parent = null;
    private String name = null;
    private boolean readonly = false;
    private boolean updated = false;//this field will be change on component init

    //Event handlers scripts
    private String onClick = null;
    private String onBlur = null;
    private String onMouseOver = null;
    private String onMouseOut = null;
    private String onDbClick = null;
    private String onChange = null;
    private String onFocus = null;
    private String onKeyDown = null;
    private String onKeyPress = null;
    private String onKeyUp = null;
    private String onMouseDown = null;
    private String onMouseMove = null;
    private String onMouseUp = null;

    //....etc

    private List validationRules = null;
    private boolean mandatory = false;

    /** Initialization method */
    public final void init(String name, BasicActionForm owner, BasicControl parent){
        this.name = name;
        this.owner = owner;
        this.parent = parent;
        initialize();
    }

    /** Initialisatin method to be implemented in successors */
    public abstract void initialize();

    //-----------------Getters

    public BasicActionForm getOwner() {
        return owner;
    }

    public BasicControl getParent() {
        return parent;
    }

    /** Return name of this control */
    public String getName() {
        return name;
    }

    public boolean isReadonly() {
        return readonly;
    }

     /**
      *  
      */
     protected void notifyUpdate(){
        updated = true;    
     }

     
     public void reset(){
        if(updated)
            safeReset();
     }

     public boolean isUpdated(){
         return updated;
     }

     /**
     * Child must override this method to create reset logic
     */
     protected void safeReset(){}


    //-----------------Setter
    public void setReadonly(boolean readonly) {
        this.readonly = readonly;
    }

    /** Returns full dot-notaieted name of this control form most parent control.
     * The owner form name is not included.*/
    public String getFullName(){
        String prefix = null;
        if(parent != null)
            prefix = parent.getFullName();

        return ((prefix != null) ? prefix+"." : "") +getName();
    }

    /** Retruns full contorl's name with owner form's name at start */
    public String getFullPath(){
        return owner.getFormName()+"."+getFullName();
    }

    //-----------------WebEvent handlers. You can override their behaviour in the successor.

    public void onBeforeAction(WebEvent event){}

    public void onAfterAction(WebEvent event){}

    //----------------


    /** Is current control contains controls contaiener */
    public boolean isContainer(){
        return false; //default behaviour. Override it if successor is container
    }

    /** Returns container */
    public ControlContainer getContainer(){
        return null; //default behaviour. Override it if successor is container
    }

    protected Locale getLocale(){
        Locale locale = null;
        try{
            locale = SessionEnvironment.getLocale(RequestEnvironment.currentReqContext().getRequest().getSession());
        }
        catch(NullPointerException e){
            /** Possible if session for selected request will be invalidated */
            locale = Locale.getDefault();
        }
        return locale;
    }

    //------------------------Protected methods
    protected String getLocalizedLabel(String key, Locale locale){
        return MessageResources.getInstance().getOwnedLocalizedMessage(BasicControl.class, getResourceBundleName(), locale, key, null);
    }

    /** Ever successors should implement this method to identify its resource bundle name. */
    protected String getResourceBundleName(){
        return RESOURCE_BANDLE_NAME;
    }



    //-------------Event handlers properties

    public String getOnClick() {
        return onClick;
    }

    public void setOnClick(String onClick) {
        this.onClick = onClick;
    }

    public String getOnChange() {
        return onChange;
    }

    public void setOnChange(String onChange) {
        this.onChange = onChange;
    }

    public String getOnBlur() {
        return onBlur;
    }

    public void setOnBlur(String onBlur) {
        this.onBlur = onBlur;
    }

    public String getOnMouseOver() {
        return onMouseOver;
    }

    public void setOnMouseOver(String onMouseOver) {
        this.onMouseOver = onMouseOver;
    }

    public String getOnMouseOut() {
        return onMouseOut;
    }

    public void setOnMouseOut(String onMouseOut) {
        this.onMouseOut = onMouseOut;
    }

    public String getOnDbClick() {
        return onDbClick;
    }

    public void setOnDbClick(String onDbClick) {
        this.onDbClick = onDbClick;
    }

    public String getOnFocus() {
        return onFocus;
    }

    public void setOnFocus(String onFocus) {
        this.onFocus = onFocus;
    }

    public String getOnKeyDown() {
        return onKeyDown;
    }

    public void setOnKeyDown(String onKeyDown) {
        this.onKeyDown = onKeyDown;
    }

    public String getOnKeyPress() {
        return onKeyPress;
    }

    public void setOnKeyPress(String onKeyPress) {
        this.onKeyPress = onKeyPress;
    }

    public String getOnKeyUp() {
        return onKeyUp;
    }

    public void setOnKeyUp(String onKeyUp) {
        this.onKeyUp = onKeyUp;
    }

    public String getOnMouseDown() {
        return onMouseDown;
    }

    public void setOnMouseDown(String onMouseDown) {
        this.onMouseDown = onMouseDown;
    }

    public String getOnMouseMove() {
        return onMouseMove;
    }

    public void setOnMouseMove(String onMouseMove) {
        this.onMouseMove = onMouseMove;
    }

    public String getOnMouseUp() {
        return onMouseUp;
    }

    public void setOnMouseUp(String onMouseUp) {
        this.onMouseUp = onMouseUp;
    }
    //-------------End Event handlers properties


    //-------------------------Validation support methods
    public void addValidationRule(ValidationRule rule){
        if(validationRules == null)
            validationRules = new ArrayList();

        rule.initialize(this);
        validationRules.add(rule);
    }


    public boolean isValidationPassed(){
        if(validationRules != null){
            boolean validationPassed = true;
            for(int i=0; i<validationRules.size(); i++)
                validationPassed &= ((ValidationRule)validationRules.get(i)).isValidationPassed();
            return validationPassed;
        }
        else{
            return true;
        }
    }

    public List getValidationRules() {
        return validationRules;
    }

    /** Validate all assigned rules.
     * @return false if any assigned validation rule fails. */
    public boolean validate(){
        if(validationRules != null){
            boolean validationPassed = true;//ok by default
            for(Iterator iter = validationRules.iterator(); iter.hasNext();)
                validationPassed &= ((ValidationRule)iter.next()).executeValidation();
            return validationPassed;
        }
        return true;//validation ok
    }

    public void setMandatory(boolean mandatory){
        this.mandatory = mandatory; 
    }

    public boolean isMandatory() {
        return mandatory;
    }
}
