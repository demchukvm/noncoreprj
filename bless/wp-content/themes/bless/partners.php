<?php
/*
  Template Name: Partners
 */
?>
<?php get_header();?>
<div class="partners_currblock">

	<div class="left">
		<img src="<?php bloginfo('template_url');?>/images/partners_shape<?php if(isset($_GET['lang']) & $_GET['lang'] == 'en'){echo '-eng';}?>.gif" alt="Партнерская программа BLESS">
	</div>
	
	<div class="right">
		<p><?php echo get_field('text_1');?></p>
	</div>

</div><!--.about_currblock-->


<div class="partner_text">

	<h2><?php echo get_field('text_2');?></h2>


	<div class="left">
	
		<ul>
			<li>
			<?php echo get_field('text_3');?> <br /><br /><?php echo get_field('text_4');?>
			</li>
		</ul>
	</div>
	
	<div class="center">
		<ul>
			<li>
			<?php echo get_field('text_5');?><br /><br /><?php echo get_field('text_6');?>
			</li>
		</ul>
	</div>
	
	<div class="right">		
		<p><?php echo get_field('text_7');?></p>
	</div>

</div><!--.about_text-->


<div class="feedback_block">
<h2><?php echo get_field('text_8');?></h2>

	<form method="POST">
		<input type="hidden" name="type" value="part" id="form_type" />
		<div class="left">
			<div class="name"><span><?php echo get_field('text_9');?></span> <input type="text" name="name" id="form_name" value=""></div>
			<div class="mail"><span><?php echo get_field('text_10');?></span> <input type="text" name="mail" id="form_mail" value=""></div>
			<div class="phone"><span><?php echo get_field('text_11');?></span> <input type="text" name="phone" id="form_phone" value=""></div>


            <div class="org"><span>Организация:</span> <input type="text" name="name" id="form_org" value=""></div>
            <div class="website"><span>Вебсайт:</span> <input type="text" name="mail" id="form_website" value=""></div>
            <div class="partner_city"><span>Город:</span> <input type="text" name="phone" id="partner_city" value=""></div>


		</div>
		
		<div class="right">
			
			<div class="message" id="big_message">
			<span><?php echo get_field('text_12');?></span>
			<textarea id="form_message" name="text"></textarea>
			</div>
                    <div id="send_form_button" style="background: #f1b40e url('<?php bloginfo('template_url');?>/images/submit_btn<?php if(isset($_GET['lang']) & $_GET['lang'] == 'en'){echo '-eng';}?>.gif') no-repeat;"></div>
		</div>
	</form>

</div>

<?php get_footer(); ?>