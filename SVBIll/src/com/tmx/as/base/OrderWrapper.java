package com.tmx.as.base;

import com.tmx.as.resolver.ResolverException;
import java.lang.Cloneable;

/** Use to create and fill Orders before the DB session will be opened. */
public class OrderWrapper implements Cloneable {
    private String orderByFieldName;
    /** optional attribute to support orders for QUERY (not only for RETRIEVE, RETRIEVE_ALL).
     * alias should refer to entity (or table then sql support will be applied) name in HQL
     * (SQL not supported yet) query. */
    private String alias;
    private OrderType orderType;
    public static final OrderType ASC = new OrderType("asc");
    public static final OrderType DESC = new OrderType("desc");


    /** private constructor to hide default constructor */
/* RM:  sometimes we need to create OrderWrapper instance before defining
        its params
    private OrderWrapper(){
    }
*/

    public OrderWrapper(){
    }

    public OrderWrapper(String orderByFieldName, OrderType orderType){
        this.orderByFieldName = orderByFieldName;
        this.orderType = orderType;
    }

    public String getOrderByFieldName(){
        return orderByFieldName;
    }

    public String getAlias(){
        return alias;
    }

    public OrderType getOrderType(){
        return orderType;
    }

    public boolean isOrderType(String orderTypeName){
        orderTypeName = orderTypeName.toLowerCase();
        if(orderType.toString().toLowerCase().equals(orderTypeName))
            return true;
        else
            return false;
    }

    /** Publish geven filters array into the string
    * @param orders array of orders to publish into String
    * @param formatString enble formating result string */
    public static String ordersToString(OrderWrapper[] orders, boolean formatString){
        StringBuffer ordersString = new StringBuffer();
        //Pepare formatting strings
        String retSymb = (formatString) ? "\r" : "";

        if(orders != null){
            for(int i = 0; i < orders.length; i++){
                ordersString.append("[by_field='").append(orders[i].getOrderByFieldName()).append("' ");
                if(orders[i].getAlias() != null)
                    ordersString.append(", alias='").append(orders[i].getAlias()).append("' ");
                ordersString.append("type='").append((orders[i].getOrderType() == null) ? "null" : orders[i].getOrderType().toString()).append("']").append(retSymb);
            }
        }
        //Return NULL instead of empty String. It is more obviosly for publishing
        String result = ordersString.toString().trim();
        return (result.length() > 0) ? result : "[no orders]";
    }

    /** Creates new OrderWrapper[] array with size have increased for one position.
     * Then put given order into the last cell of new array. */
    public static OrderWrapper[] appendToOrders(OrderWrapper[] orders, OrderWrapper order){
        if(orders == null)
            orders = new OrderWrapper[0];
        if(order == null)
            return orders;
        OrderWrapper[] newOrders = new OrderWrapper[orders.length + 1];
        System.arraycopy(orders, 0, newOrders, 0, orders.length);
        newOrders[newOrders.length-1] = order;
        return newOrders;
    }

    static class OrderType{
        private String orderType;

        private OrderType(String orderType){
            this.orderType = orderType;
        }

        public String toString(){
            return orderType;
        }
    }

    public void set(String fieldName, String fieldValue) throws ResolverException {
        if(fieldName.toLowerCase().equals("name")){
            orderByFieldName = fieldValue;
            return;
        }
        if(fieldName.toLowerCase().equals("alias")){
            alias = fieldValue;
            return;
        }
        if(fieldName.toLowerCase().equals("type")){
            if(fieldValue.toLowerCase().equals("asc")){
                orderType = ASC;
                return;
            }
            if(fieldValue.toLowerCase().equals("desc")){
                orderType = DESC;
                return;
            }
            throw new ResolverException("err.resolver.unknown_order_type", new String[]{this.getClass().getName(), fieldValue.toString()});
        }
        //RM: throw exception if field not found
        throw new ResolverException("err.resolver.unknown_parameter_for", new String[]{this.getClass().getName(), fieldName, fieldValue.toString()});
    }
}
