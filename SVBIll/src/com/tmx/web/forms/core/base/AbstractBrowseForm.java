package com.tmx.web.forms.core.base;

import com.tmx.as.entities.general.user.User;
import com.tmx.as.exceptions.DatabaseException;
import com.tmx.as.resolver.FunctionSyntaxException;
import com.tmx.as.resolver.ResolverException;
import com.tmx.web.base.BasicActionForm;
import com.tmx.web.controls.table.Table;
import com.tmx.web.controls.table.ControlManagerTable;

/**
    Form to handle 'browse against some entities' request from any page.
    You could add such browse structures as this one has into your particular form and
    do not use separate form for browse.
 */
public abstract class AbstractBrowseForm extends BasicActionForm {

    private ControlManagerTable browseTable;

    public ControlManagerTable getBrowseTable() {
        return browseTable;
    }

    public void setBrowseTable(ControlManagerTable browseTable) {
        this.browseTable = browseTable;
    }
    
}
