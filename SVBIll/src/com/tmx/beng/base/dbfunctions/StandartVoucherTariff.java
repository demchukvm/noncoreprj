package com.tmx.beng.base.dbfunctions;

import com.tmx.as.entities.bill.transaction.TransactionType;
import com.tmx.beng.base.BillingMessage;
import com.tmx.beng.base.Reservation;
import com.tmx.beng.base.cache.InvokeFunctionException;

import java.util.Map;
import java.util.List;
import java.util.ArrayList;

public class StandartVoucherTariff extends BasicTariffFunction {

    boolean isTariffApplicable(Map actualParams) {
        return BillingMessage.O_REFILL_PAYMENT.equals(task.getProcesedMessage().getOperationName());
    }

    void applyTariff(Map actualParams, Reservation reservation) throws InvokeFunctionException {
        try{
            final String voucherNominal = task.getProcesedMessage().getAttributeString(BillingMessage.VOUCHER_NOMINAL);
            double price = Double.parseDouble(String.valueOf(actualParams.get(voucherNominal)));
            //prepare reservation on primary tariff
            reservation.reserve(null, price, null, TransactionType.CODE_CREDIT);
        }
        catch(Throwable e){
            throw new InvokeFunctionException("err.tariff.calc_error", e);
        }
    }

    public TariffParameter[] getDefaultParams() {
        VoucherValues voucherValues = new VoucherValues();
        Vouchers voucherNames = new Vouchers();
        int vouchersCount = voucherValues.getVoucherValues().length;
        TariffParameter[] result = new TariffParameter[vouchersCount];
        for (int i=0; i<vouchersCount; i++){
            TariffParameter paramCost = new TariffParameter();

            paramCost.setName(voucherNames.getVoucherNames()[i]);
            paramCost.setValue(voucherValues.getVoucherValues()[i].toString());
            paramCost.setOrderNum(new Long(i));
            paramCost.setMandatory(Boolean.FALSE);

            result[i] = paramCost;
        }

        return result;
    }

    public ValidationError[] validate(Map actualParams) {
        List resultErrors = new ArrayList();
        Vouchers voucherNames = new Vouchers();
        resultErrors.addAll(ParameterValidator.validateParamNames(actualParams, voucherNames.getVoucherNames()));
        
        return (ValidationError[]) resultErrors.toArray(new ValidationError[resultErrors.size()]);
    }

    public static class VoucherValues {
        private static final Double[] voucherValues = {
                4.90, 14.70, 29.40, 58.80, 88.20,
                24.38, 48.75, 97.50, 292.50,
                9.65, 23.88, 47.75, 95.50,
                9.70, 24.25, 48.50, 97.00, 194.00
        };

        public Double[] getVoucherValues() {
            return voucherValues;
        }

    }
}
