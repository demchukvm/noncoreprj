package com.tmx.web.controls.table;

import com.tmx.web.controls.BasicControl;

import java.util.*;


public class FilterSet {
    public  static final String CMD_RESET =  "reset";
    private Map filters = new HashMap();
    private Map filterIds = new HashMap();
    private boolean visible = false;
    private int idx = 0;
    private String filterSetCommand = null;
    private Table parent;

    public Set getFilterNames(){
        return filters.keySet();
    }

    /**
     * Get filter from FiltersMap
     * @param filterName
     * @return filter
     */

    public Filter getFilter(String filterName){
        if(filters.containsKey(filterName)){
            return (Filter)filters.get(filterName);
        }else
            return null;
    }


    public Filter getFilterByIndex(int index){
        Integer id = new Integer(index);
        if(filterIds.containsKey(id)){
            return getFilter((String)filterIds.get(id));
        }
        return null;
    }

    public void setFilterByIndex(int index, Filter filter){
        Integer id = new Integer(index);
        if(filterIds.containsKey(id)){
            addFilter((String)filterIds.get(id), filter);
        }
    }

    /**
     * Add filter to FiltersMap
     * @param filterName
     * @param filter
     */
    public void addFilter(String filterName, Filter filter){
        filter.setParent(this);
        filter.setName(filterName);
        filter.setId(idx);
        filters.put(filterName, filter);
        filterIds.put(new Integer(idx++), filterName);
    }

    public void addFilter(Filter filter){
        filter.setParent(this);
        filter.setId(idx);
        filters.put(filter.getName(), filter);         //<----------|TODO(A.N.): error part!!!
        filterIds.put(new Integer(idx++), filter.getName());//<-----|
    }

    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    public String getPath(){
        return "filterSet";
    }

    public String getAppliedParameters(){
        StringBuffer sb = new StringBuffer();
        List params = new Vector();

        Iterator iter = filters.values().iterator();
        while(iter.hasNext()){
            Filter filter = (Filter)iter.next();
            String filterStr = filter.getAppliedParameters();
            if(filterStr != null && !"".equals(filterStr)){
                params.add(filterStr);
            }
        }
        if(params.size() > 0){
            sb.append("query(");
            iter = params.iterator();
            while(iter.hasNext()){
                sb.append((String)iter.next());
                if(iter.hasNext()){
                    sb.append(",");
                }
            }
            sb.append(")");
        }
        return sb.toString();
    }

    /** FilterSet commands processed in the refresh() method only. */
    public String getFilterSetCommand() {
        return filterSetCommand;
    }

    public void setFilterSetCommand(String filterSetCommand) {
        this.filterSetCommand = filterSetCommand;
    }

    /** Called on table refresh. Handle FilterSet according to FilterSet command. */
    void refresh(){
        if(FilterSet.CMD_RESET.equals(getFilterSetCommand()))
            safeReset();//reset filters
    }

    /** Reset all enclosed controls using their reset() methods. */
    void safeReset() {
        Iterator filterNameIterator = getFilterNames().iterator();
        while (filterNameIterator.hasNext()) {
            Filter filter = getFilter((String) filterNameIterator.next());//get filter

            if (filter.getFilterEnabler() instanceof BasicControl)//for filter enabler
                ((BasicControl) filter.getFilterEnabler()).reset();

            Iterator parameterNameIterator = filter.getParameterNames().iterator();
            while (parameterNameIterator.hasNext()) {
                Parameter parameter = filter.getParameter((String) parameterNameIterator.next());
                if(parameter.getControl()!=null)
                    parameter.getControl().reset();
            }
        }
        setFilterSetCommand(null);// delete command
    }

    /** Validate filterset. Used only from Table validate() */
    boolean validate() {
        boolean valid = true;
        //validate controls contained in table's filters
        Iterator filterNameIterator = getFilterNames().iterator();
        while (filterNameIterator.hasNext()) {
            Filter filter = getFilter((String) filterNameIterator.next());

            // validation for filter enabler
            FilterEnabler filterEnabler = filter.getFilterEnabler();
            if (filterEnabler instanceof BasicControl)
                valid &= ((BasicControl) filterEnabler).validate();

            Iterator parameterNameIterator = filter.getParameterNames().iterator();
            while (parameterNameIterator.hasNext()) {
                Parameter parameter = filter.getParameter((String) parameterNameIterator.next());
                valid &= parameter.getControl() == null || parameter.getControl().validate();
            }
        }
        return valid;
    }

    public Table getParent() {
        return parent;
    }

    void setParent(Table parent) {
        this.parent = parent;
    }
}
