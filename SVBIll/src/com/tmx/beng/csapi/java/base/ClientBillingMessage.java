package com.tmx.beng.csapi.java.base;

import com.tmx.beng.base.BillingMessageImpl;
import com.tmx.beng.base.BillingMessage;
import com.tmx.beng.access.Connection;
import com.tmx.util.MD5;

import java.util.Date;



abstract class BillingMessageWrapper implements com.tmx.beng.csapi.java.base.BillingMessage {
    protected BillingMessageImpl message = new BillingMessageImpl();
    protected Connection connect;
    
}

abstract class AbstractBasicMsg extends BillingMessageWrapper implements ClientMessages.BasicMessage {
    public void setProcessingCode(String processingCode) {
        message.setAttributeString(BillingMessage.PROCESSING_CODE, processingCode);
    }

    public void setPassword(String password) {
        message.setAttributeString(BillingMessage.PASSWORD, password);
    }

    public void setPasswordMD5(String password) {
        message.setAttributeString(BillingMessage.PASSWORD, MD5.getHashString(password).toUpperCase());
    }

    public void setLogin(String login) {
        message.setAttributeString(BillingMessage.LOGIN, login);
    }

    public void setTerminalSN(String terminalSN) {
        message.setAttributeString(BillingMessage.TERMINAL_SN, terminalSN);
    }

    public void setSellerCode(String sellerCode) {
        message.setAttributeString(BillingMessage.SELLER_CODE, sellerCode);
    }

    public void setClientTime(Date date) {
        message.setAttribute(BillingMessage.CLIENT_TIME, date);
    }
}

//class BuyVoucherMsgImpl extends AbstractBasicMsg implements ClientMessages.BuyVoucherMsg {
//    public void setServiceCode(String serviceCode) {
//        message.setAttributeString(BillingMessage.SERVICE_CODE, serviceCode);
//    }
//
//    public void setVoucherNominal(String voucherNominal) {
//        message.setAttributeString(BillingMessage.VOUCHER_NOMINAL, voucherNominal);
//    }
//}

/*class ValidateParamsMsgImpl extends BillingMessageWrapper implements ClientMessages.ValidateParamsMsg {
    public ValidateParamsMsgImpl() {
        message.setOperationName(BillingMessage.O_VALIDATE_FUNCTION_PARAMS);
    }

    public void setActualFunctionParams(List params) {
        message.setAttribute(BillingMessage.ACTUAL_PARAMS_LIST, params);
    }
    
    public void setFunctionTypeId(Long functionTypeId) {
        message.setAttribute(BillingMessage.FUNCTION_TYPE_ID, functionTypeId);
    }
}*/

/*class DefaultParamsMsgImpl extends BillingMessageWrapper implements ClientMessages.DefaultParamsMsg {
		public DefaultParamsMsgImpl() {
			message.setOperationName(BillingMessage.O_RETRIEVE_DEFAULT_FUNCTION_PARAMS);
		}

		public void setFunctionTypeId(Long functionTypeId) {
			message.setAttribute(BillingMessage.FUNCTION_TYPE_ID, functionTypeId);
		}

    public ClientMessages.DefaultParamsResp handleResponseMessage() {
        return null;  
    }
}*/
