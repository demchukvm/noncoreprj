package com.tmx.as.entities.general.user;

import com.tmx.as.base.BasicEntity;
import com.tmx.as.entities.bill.seller.Seller;
import com.tmx.as.entities.general.group.Group;
import com.tmx.as.entities.general.organization.Organization;
import com.tmx.as.entities.general.role.Role;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 */
public class User extends BasicEntity {

    private Long userId;
    private String lastName;
    private String firstName;
    private String patronymic;
    private int admin;
    private String login;
    private String password;
    private int active;
    private String email;
    private Date creationDate;
    private Role role;
    private Organization organization;
    private Set groups = new HashSet();
    private Seller sellerOwner;

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public int getAdmin() {
        return admin;
    }

    public void setAdmin(int admin) {
        this.admin = admin;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getActive() {
        return active;
    }

    public void setActive(int active) {
        this.active = active;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public Set getGroups() {
        return groups;
    }

    public void setGroups(Set groups) {
        this.groups = groups;
    }

    public void addToGroup(Group group) {
        this.getGroups().add(group);
        group.getUsers().add(this);
    }

    public void removeFromGroup(Group group) {
        this.getGroups().remove(group);
        group.getUsers().remove(this);
    }

    /**
     * Set default values for fields
     */
    public void setDefaults() {
    }


    public Serializable getId() {
        return getUserId();
    }

    public int getValid() {
        return valid;
    }

    public void setValid(int valid) {
        this.valid = valid;
    }

    public Organization getOrganization() {
        return organization;
    }

    public void setOrganization(Organization organization) {
        this.organization = organization;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }

    public Seller getSellerOwner() {
        return sellerOwner;
    }

    public void setSellerOwner(Seller sellerOwner) {
        this.sellerOwner = sellerOwner;
    }
}
