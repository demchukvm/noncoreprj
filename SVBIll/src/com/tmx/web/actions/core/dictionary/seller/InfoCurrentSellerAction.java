package com.tmx.web.actions.core.dictionary.seller;

import com.tmx.web.actions.core.AbstractSvbillAction;

import com.tmx.web.forms.core.dictionary.seller.InfoSellerForm;
import com.tmx.web.controls.Button;
import com.tmx.web.controls.BasicControl;
import com.tmx.as.entities.bill.seller.Seller;
import com.tmx.as.blogic.SellerManager;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class InfoCurrentSellerAction extends AbstractSvbillAction {

    private static final String FORWARD_MANAGE_SELLERS_PAGE = "infoCurrentSellerPage";

    public ActionForward defaultAction(ActionMapping mapping,
                                       ActionForm form,
                                       HttpServletRequest request,
                                       HttpServletResponse response,
                                       String methodName) throws Throwable {
        InfoSellerForm infoSellerForm = (InfoSellerForm) verifyForm(form, InfoSellerForm.class);
        Seller completeSeller = new SellerManager().getSeller(getAssociatedSeller(request).getSellerId());
        infoSellerForm.bindData(completeSeller, getLoggedInUser(request));
        //disable all controls for first page        
        for (BasicControl controls : infoSellerForm.getControls()) {
            controls.setReadonly(true);
        }

        return mapping.findForward(FORWARD_MANAGE_SELLERS_PAGE);
    }    

}
