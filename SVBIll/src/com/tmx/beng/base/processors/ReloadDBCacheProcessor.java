package com.tmx.beng.base.processors;

import com.tmx.beng.base.Task;
import com.tmx.beng.base.BillingMessageWritable;
import com.tmx.beng.base.BillException;
import com.tmx.beng.base.cache.DBCache;
import com.tmx.util.InitException;

/**
    Reload DBCache
 */
public class ReloadDBCacheProcessor extends TaskProcessor {

    protected Task process0(Task task, DBCache dbCache, BillingMessageWritable msg) throws BillException {
        try{
            dbCache.reload();
        }
        catch(InitException e){
            msg.setStatus(1000, "failed to reload DBCache");
            throw new BillException("err.reload_dbcache_proc.failed_to_realod", e);
        }
        return task;
    }
}
