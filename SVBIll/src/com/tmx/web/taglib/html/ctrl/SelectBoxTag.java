package com.tmx.web.taglib.html.ctrl;

import com.tmx.web.controls.ScrollBox;
import javax.servlet.jsp.JspException;

import org.apache.struts.taglib.TagUtils;

/**
 * Render ScrollBox control as HTML select element
 */
public class SelectBoxTag extends AbstractControlTag {
    //CSS styles properties
    private String selectBoxStyleClass = null;

    private String readonlySelectBoxStyleClass = null;

    //------------------------------Properties

    public String getSelectBoxStyleClass() {
        return (selectBoxStyleClass != null) ? selectBoxStyleClass : getDefaultProperty("tag.selectBox.style.selectBoxStyleClass");
    }

    public void setSelectBoxStyleClass(String selectBoxStyleClass) {
        this.selectBoxStyleClass = selectBoxStyleClass;
    }

    public String getReadonlySelectBoxStyleClass() {
        return (readonlySelectBoxStyleClass != null) ? readonlySelectBoxStyleClass : getDefaultProperty("tag.selectBox.style.readonlySelectBoxStyleClass");
    }

    public void setReadonlySelectBoxStyleClass(String readonlySelectBoxStyleClass) {
        this.readonlySelectBoxStyleClass = readonlySelectBoxStyleClass;
    }

    //------------------------------Abstract methods
    protected int controlStartTag() throws JspException {
        StringBuffer resultHTML = new StringBuffer();
        ScrollBox sb = getScrollBox();
        if(sb == null){
            TagUtils.getInstance().write(pageContext,"ScrollBox '"+getProperty()+"' is not found");
            return SKIP_BODY;
        }

        if(!sb.isBounded()){
            TagUtils.getInstance().write(pageContext,"Data is not bound to ScrollBox '"+getProperty()+"'");
            return SKIP_BODY;
        }
        
        //Render HTML
        resultHTML.append("\r");
        String name = " name=\""+getProperty() +"." +ScrollBox.SELECTEDKEY_SUFFIX + "\" ";
        String styleClass = " class=\"" + (sb.isReadonly() ? getReadonlySelectBoxStyleClass() : getSelectBoxStyleClass()) + "\" ";
        String readonly = (sb.isReadonly()) ? " disabled" : "";

        //StringBuffer style = new StringBuffer(" style=\"{");
        StringBuffer style = new StringBuffer(" style=\"");
        if(getWidth() != null)
            //style.append(" width: "+getWidth()+"; ");

            style.append("background-color: #fafafa;");
        //style.append("}\"");
        style.append("\"");


        resultHTML.append("<select "+ name + styleClass + style + readonly + getOnEventAttrsString() + ">\r");
        sb.resetForRendering();
        while(sb.hasNext()){
            ScrollBox.Entry entry = sb.next();
            String attrSelectedHTML = (entry.getKey().equals(sb.getSelectedEntry().getKey()) ? "selected=\"selected\"" : "");
            resultHTML.append("    <option value="+entry.getKey()+ " " + attrSelectedHTML + ">\r");
            resultHTML.append((entry.getValue() == null) ? "" : entry.getValue() + "\r");
            resultHTML.append("    </option>\r");
        }
        resultHTML.append("</select>\r");
        resultHTML.append("\r");


        TagUtils.getInstance().write(pageContext,resultHTML.toString());
        return SKIP_BODY;
    }


    private ScrollBox getScrollBox() throws JspException{
        Object object = getPropertyObject();
        return (object != null && object instanceof ScrollBox) ? (ScrollBox)object : null;
    }
}
