package com.tmx.web.forms.core.tools;

import com.tmx.web.controls.Button;
import com.tmx.web.base.BasicActionForm;

public class NewsForm extends BasicActionForm
{
    private Button saveMessage = null;
    private Button reset = null;
    private String messageTitle = null;
    private String messageText = null;

    private String responseText = null;
/*    private EditBox editBoxCitypayBalance = null;
    private EditBox editBoxAvancelBalance = null;
    private EditBox editBoxKSBalance = null;
    private EditBox editBoxKSBNCBalance = null;
    private EditBox editBoxKSFCBalance = null;
    private EditBox editBoxMTSBalance = null;*/

    public void init()
    {
        saveMessage = new Button();
        saveMessage.setLabelKey("ctrl.button.label.save");
        saveMessage.setAction("/core/tools/news");
        saveMessage.setCommand("saveMessage");

        reset = new Button();
        reset.setLabelKey("ctrl.button.label.reset");
      //  saveMessage.setAction("/core/tools/news");
      //  saveMessage.setCommand("saveMessage");

        messageTitle = "";
        messageText = "";

        responseText = "";

/*        editBoxCitypayBalance = new EditBox();
        editBoxCitypayBalance.setReadonly(true);

        editBoxAvancelBalance = new EditBox();
        editBoxAvancelBalance.setReadonly(true);

        editBoxKSBalance = new EditBox();
        editBoxKSBalance.setReadonly(true);

        editBoxKSBNCBalance = new EditBox();
        editBoxKSBNCBalance.setReadonly(true);

        editBoxKSFCBalance = new EditBox();
        editBoxKSFCBalance.setReadonly(true);

        editBoxMTSBalance = new EditBox();
        editBoxMTSBalance.setReadonly(true);*/
    }

    public Button getSaveMessage() {
        return saveMessage;
    }

    public void setSaveMessage(Button saveMessage) {
        this.saveMessage = saveMessage;
    }

    public String getMessageTitle() {
        return messageTitle;
    }

    public void setMessageTitle(String messageTitle) {
        this.messageTitle = messageTitle;
    }

    public String getMessageText() {
        return messageText;
    }

    public void setMessageText(String messageText) {
        this.messageText = messageText;
    }

    public String getResponseText() {
        return responseText;
    }

    public void setResponseText(String responseText) {
        this.responseText = responseText;
    }

    public Button getReset() {
        return reset;
    }

    public void setReset(Button reset) {
        this.reset = reset;
    }

    /*   public Button getOnRefreshBalance() {
        return onRefreshBalance;
    }

    public void setOnRefreshBalance(Button onRefreshBalance) {
        this.onRefreshBalance = onRefreshBalance;
    }

    public EditBox getEditBoxCitypayBalance() {
        return editBoxCitypayBalance;
    }

    public void setEditBoxCitypayBalance(EditBox editBoxCitypayBalance) {
        this.editBoxCitypayBalance = editBoxCitypayBalance;
    }

    public EditBox getEditBoxAvancelBalance() {
        return editBoxAvancelBalance;
    }

    public void setEditBoxAvancelBalance(EditBox editBoxAvancelBalance) {
        this.editBoxAvancelBalance = editBoxAvancelBalance;
    }

    public EditBox getEditBoxKSBalance() {
        return editBoxKSBalance;
    }

    public void setEditBoxKSBalance(EditBox editBoxKSBalance) {
        this.editBoxKSBalance = editBoxKSBalance;
    }

    public EditBox getEditBoxKSBNCBalance() {
        return editBoxKSBNCBalance;
    }

    public void setEditBoxKSBNCBalance(EditBox editBoxKSBNCBalance) {
        this.editBoxKSBNCBalance = editBoxKSBNCBalance;
    }

    public EditBox getEditBoxKSFCBalance() {
        return editBoxKSFCBalance;
    }

    public void setEditBoxKSFCBalance(EditBox editBoxKSFCBalance) {
        this.editBoxKSFCBalance = editBoxKSFCBalance;
    }

    public EditBox getEditBoxMTSBalance() {
        return editBoxMTSBalance;
    }

    public void setEditBoxMTSBalance(EditBox editBoxMTSBalance) {
        this.editBoxMTSBalance = editBoxMTSBalance;
    }*/
}
