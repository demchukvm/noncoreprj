package com.tmx.beng.base.cache;

import com.tmx.as.exceptions.DatabaseException;
import com.tmx.as.base.EntityManager;
import com.tmx.as.base.FilterWrapper;
import com.tmx.as.entities.bill.balance.TerminalGroupBalance;
import com.tmx.as.entities.bill.tariff.TerminalGroupTariff;
import com.tmx.as.entities.bill.restriction.TerminalGroupRestriction;
import com.tmx.as.entities.bill.terminal_group.TerminalGroup;
import com.tmx.util.InitException;

import java.util.*;

/**
 */
class TerminalGroups {
    Map terminalGroupId_terminalGroup = Collections.synchronizedMap(new HashMap());

    TerminalGroups(Sellers sellers)throws DatabaseException, InitException {
        EntityManager entityManager = new EntityManager();

        // load terminal groups
        Iterator it;
        TerminalGroupWrapper wrapper;
        List list = entityManager.RETRIEVE_ALL(TerminalGroup.class, (FilterWrapper[]) null, new String[]{"primaryBalance"});
        for(it = list.iterator(); it.hasNext();){
            TerminalGroup terminalGroup = (TerminalGroup)it.next();
            wrapper = new TerminalGroupWrapper(terminalGroup);
            terminalGroupId_terminalGroup.put(terminalGroup.getTerminalGroupId(), wrapper);
            SellerWrapper sellerWrapper = (SellerWrapper)sellers.sellerId_seller.get(terminalGroup.getSellerOwnerId());
            if(sellerWrapper != null){
                sellerWrapper.terminaGroupId_terminalGroup.put(terminalGroup.getTerminalGroupId(), wrapper);
            }
        }


        // load terminal group ballance
        list  = entityManager.RETRIEVE_ALL(TerminalGroupBalance.class, null);
        for(it = list.iterator(); it.hasNext();){
            TerminalGroupBalance ballance = (TerminalGroupBalance)it.next();
            wrapper = (TerminalGroupWrapper)terminalGroupId_terminalGroup.get(ballance.getTerminalGroupId());
            if(wrapper != null && ballance.getCode() != null && ballance.getBalanceId() != null){
                wrapper.ballanceId_ballance.put(ballance.getBalanceId(), ballance);
                wrapper.ballanceCode_ballanceId.put(ballance.getCode(), ballance.getBalanceId());
            }
        }

        // load terminal group tariff
        list  = entityManager.RETRIEVE_ALL(TerminalGroupTariff.class, (FilterWrapper[]) null, new String[]{"functionInstance"});
        for(it = list.iterator(); it.hasNext();){
            TerminalGroupTariff tariff = (TerminalGroupTariff)it.next();
            wrapper = (TerminalGroupWrapper)terminalGroupId_terminalGroup.get(tariff.getTerminalGroupId());
            if(wrapper != null){
                wrapper.tariffId_tariffEntity.put(tariff.getTariffId(), tariff);
                FunctionWrapper functionWrapper = new FunctionWrapper(tariff.getFunctionInstance());
                wrapper.tariffId_tariffFunctionInstance.put(tariff.getTariffId(), functionWrapper);
                //cache tariff by name if name is not null
//                if(tariff.getName() != null){
//                    if(wrapper.tariffName_tariffFunctionInstance.containsKey(tariff.getName()))
//                        throw new InitException("Duplicate tariff name for terminal group", new String[]{tariff.getName(), wrapper.getTerminalGroup().getName()});
//                    wrapper.tariffName_tariffFunctionInstance.put(tariff.getName(), functionWrapper);
//                }
            }
        }

        // load terminal group restrictions
        list  = entityManager.RETRIEVE_ALL(TerminalGroupRestriction.class, (FilterWrapper[]) null, new String[]{"functionInstance"});
        for(it = list.iterator(); it.hasNext();){
            TerminalGroupRestriction restriction = (TerminalGroupRestriction)it.next();
            wrapper = (TerminalGroupWrapper)terminalGroupId_terminalGroup.get(restriction.getTerminalGroupId());
            if(wrapper != null){
                FunctionWrapper functionWrapper = new FunctionWrapper(restriction.getFunctionInstance());
                wrapper.restriction.add(functionWrapper);
            }
        }
    }

    public TerminalGroupWrapper getTerminalGroupWrapper(Long id){
        return (TerminalGroupWrapper)terminalGroupId_terminalGroup.get(id);
    }
    
    public TerminalGroup getTerminalGroup(Long id) {
        TerminalGroupWrapper wrapper = getTerminalGroupWrapper(id);
        if(wrapper == null) return null;
        return wrapper.getTerminalGroup();
    }

}
