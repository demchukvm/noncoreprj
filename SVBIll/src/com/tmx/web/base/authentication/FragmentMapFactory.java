package com.tmx.web.base.authentication;

import com.tmx.web.base.authentication.CommandFragmentMap;


abstract public class FragmentMapFactory {

    static public FragmentMapFactory getInstance(){
        return null;
    }

    abstract public CommandFragmentMap getFragmentPool(String roleName);

}
