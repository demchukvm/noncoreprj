package com.tmx.web.actions.core.ui_permission;

import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForm;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.tmx.web.base.BasicDispatchedAction;
import com.tmx.web.base.Roadmap;
import com.tmx.web.forms.core.ui_permission.InfoPermissionForm;
import com.tmx.as.base.EntityManager;
import com.tmx.as.base.FilterWrapper;
import com.tmx.as.entities.general.ui_permission.Permission;
import com.tmx.as.entities.general.ui_permission.PermissionType;
import com.tmx.as.entities.general.role.Role;

public class InfoPermissionAction extends BasicDispatchedAction {
    private static final String FORWARD_MANAGE_ACTION = "manageAction";
    private static final String FORWARD_INFO_PAGE = "infoPage";
    private static final String FORWARD_MANAGE_DELETE_ACTION = "manageDeleteAction";

    public ActionForward preloadForCreate(ActionMapping mapping,
                                          ActionForm form,
                                          HttpServletRequest request,
                                          HttpServletResponse response) throws Throwable {
        InfoPermissionForm infoPermissionForm = (InfoPermissionForm) verifyForm(form, InfoPermissionForm.class);
        preload0(infoPermissionForm);
        infoPermissionForm.bindData(new Permission());
        return mapping.findForward(FORWARD_INFO_PAGE);
    }

    public ActionForward preloadForUpdate(ActionMapping mapping,
                                          ActionForm form,
                                          HttpServletRequest request,
                                          HttpServletResponse response) throws Throwable {
        InfoPermissionForm permissionForm = (InfoPermissionForm) verifyForm(form, InfoPermissionForm.class);
        preload0(permissionForm);
        permissionForm.bindData((Permission)new EntityManager().RETRIEVE(Permission.class,permissionForm.getSelectedPermissionId(),new String[]{"userInterface"}));
        return mapping.findForward(FORWARD_INFO_PAGE);
    }

    public ActionForward apply(ActionMapping mapping,
                              ActionForm form,
                              HttpServletRequest request,
                              HttpServletResponse response) throws Throwable {
        validateTransactionToken(request);
        InfoPermissionForm permissionForm = (InfoPermissionForm) verifyForm(form, InfoPermissionForm.class);
        permissionForm.validate(mapping.findForward(FORWARD_INFO_PAGE));
        Permission permission=permissionForm.populateFromControls();
        new EntityManager().SAVE(permission);
        //render after saving
        permissionForm.setSelectedPermissionId(permission.getPermissionId());
        permissionForm.getActionExecStatus().setNewStatus("apply", "applied");
        return preloadForUpdate(mapping,form,request,response);
    }

    public ActionForward save(ActionMapping mapping,
                              ActionForm form,
                              HttpServletRequest request,
                              HttpServletResponse response) throws Throwable {
        apply(mapping, form, request, response);
        return mapping.findForward(FORWARD_MANAGE_ACTION);
    }

    public ActionForward preloadForDelete(ActionMapping mapping,
                                          ActionForm form,
                                          HttpServletRequest request,
                                          HttpServletResponse response) throws Throwable {
        Roadmap roadmap=getRoadmap();
        roadmap.collectParameter("/core/message/confirmation", "leftMenuName", "ui_permission");
        roadmap.setPageForward("confirmation","onOk","local:"+this.getName()+"#"+FORWARD_MANAGE_DELETE_ACTION);
        roadmap.setPageForward("confirmation","onCancel","local:"+this.getName()+"#"+FORWARD_MANAGE_ACTION);
        return mapping.findForward("confirmation");
    }

    public ActionForward delete(ActionMapping mapping,
                              ActionForm form,
                              HttpServletRequest request,
                              HttpServletResponse response) throws Throwable {
        InfoPermissionForm permissionForm = (InfoPermissionForm) verifyForm(form, InfoPermissionForm.class);
        new EntityManager().DELETE(Permission.class,permissionForm.getSelectedPermissionId());
        return mapping.findForward(FORWARD_MANAGE_ACTION);
    }

    private void preload0(InfoPermissionForm form) throws Throwable{
        EntityManager em=new EntityManager();
        FilterWrapper filter=new FilterWrapper("by_notsuperuser");
        form.getRole().bind(em.RETRIEVE_ALL(Role.class,new FilterWrapper[]{filter}));
        form.getPermissionType().bind(em.RETRIEVE_ALL(PermissionType.class,null));
    }
}
