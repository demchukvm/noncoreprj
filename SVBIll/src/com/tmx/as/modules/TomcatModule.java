package com.tmx.as.modules;

import com.tmx.util.Configuration;

import java.util.Properties;

/**
    Apache Tomcate module 
 */
public class TomcatModule extends AbstractModule{

    public void start(Properties props)throws ModuleException{
        Configuration config = Configuration.getInstance();
        String catalinaBase = config.getProperty("catalina.base");
        if(catalinaBase == null)
            throw new ModuleException("Required property 'catalina.base' is not defined in the config file (typicaly 'setup.properties')");
        System.setProperty("catalina.base", catalinaBase);
        logger.info("Set system property 'catalina.base="+catalinaBase+"'");

        // Get Tomcat config particular for our application and strtup Tomcat using that file.
        String webConfig = config.getProperty("web_config", config.getProperty("tmx.home") + "/conf/tomcat-server.xml");
        logger.info("Using tomcat configuration '"+webConfig+"'");
        logger.info("Starting tomcat...");
        org.apache.catalina.startup.Catalina.main(
            new String[] { "-config" , webConfig, "-debug" , "start" }
        );
    }                                                                                                       

    public void shutdownGraceful(Properties props)throws ModuleException{
        Runtime.getRuntime().addShutdownHook(new Thread(){
            public void run(){
                try{
                    //1. Initiate properties loading
                    Configuration config = Configuration.getInstance();

                    //2. Set catalina.home system property (Strongly required to startup Tomcat with its default environment)
                    String catalinaBase = config.getProperty("catalina.base");
                    if(catalinaBase == null){
                        throw new ModuleException("Required property 'catalina.base' is not defined in the config file (typicaly 'setup.properties')");
                    }
                    System.setProperty("catalina.base", catalinaBase);

                    //3. Get Tomcat config particular for our application and strtup Tomcat using that file.
                    String webConfig = config.getProperty("web_config", config.getProperty("tmx.home") + "/conf/tomcat-server.xml");
                    org.apache.catalina.startup.Catalina.main(
                            new String[] { "-config" , webConfig, "-debug" , "stop" }
                    );

                    logger.info("Server has been stopped.");
                }catch(Throwable e){
                    e.printStackTrace();
                }
            }                     
        });
    }

    public void shutdownAbort(Properties props)throws ModuleException{
        //1. Initiate properties loading
        Configuration config = Configuration.getInstance();

        //2. Set catalina.home system property (Strongly required to startup Tomcat with its default environment)
        String catalinaBase = config.getProperty("catalina.base");
        if(catalinaBase == null){
            throw new ModuleException("Required property 'catalina.base' is not defined in the config file (typicaly 'setup.properties')");
        }
        System.setProperty("catalina.base", catalinaBase);

        //3. Get Tomcat config particular for our application and strtup Tomcat using that file.
        String webConfig = config.getProperty("web_config", config.getProperty("tmx.home") + "/conf/tomcat-server.xml");
        org.apache.catalina.startup.Catalina.main(
                new String[] { "-config" , webConfig, "-debug" , "stop" }
        );

        logger.info("Server has been stopped.");
    }
}
