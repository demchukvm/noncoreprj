package com.tmx.web.taglib.html.filter_ctrl;

import org.apache.struts.taglib.TagUtils;
import javax.servlet.jsp.JspException;

import com.tmx.web.taglib.html.AbstractFilterSetTag;
import com.tmx.web.base.ApplicationEnvironment;
import com.tmx.web.controls.table.FilterSet;

/**
 */
public class PanelFilterSetTag extends AbstractFilterSetTag {
    private String closeImage = null;
    private String openImage = null;
    private String divStyle = null;
    private String tableStyle = null;
    private String applyButtonStyle = null;
    private String width = null;

    public String getWidth() {
        return (width != null) ? width : getDefaultProperty("tag.panelFilterSet.style.width");
    }

    public void setWidth(String width) {
        this.width = width;
    }

    public String getApplyButtonStyle() {
        return (applyButtonStyle != null) ? applyButtonStyle : getDefaultProperty("tag.panelFilterSet.style.applyButtonStyle");
    }

    public void setApplyButtonStyle(String applyButtonStyle) {
        this.applyButtonStyle = applyButtonStyle;
    }

    public String getDivStyle() {
        return (divStyle != null) ? divStyle : getDefaultProperty("tag.panelFilterSet.style.divStyle");
    }

    public void setDivStyle(String divStyle) {
        this.divStyle = divStyle;
    }

    public String getTableStyle() {
        return (tableStyle != null) ? tableStyle : getDefaultProperty("tag.panelFilterSet.style.tableStyle");
    }

    public void setTableStyle(String tableStyle) {
        this.tableStyle = tableStyle;
    }

    public String getCloseImage() {
        return (closeImage != null) ? closeImage : ApplicationEnvironment.getAppDeploymentName() + getDefaultProperty("tag.panelFilterSet.img.closeImg");
    }

    public void setCloseImage(String closeImage) {
        this.closeImage = closeImage;
    }

    public String getOpenImage() {
        return (openImage != null) ? openImage : ApplicationEnvironment.getAppDeploymentName() + getDefaultProperty("tag.panelFilterSet.img.openImg");
    }

    public void setOpenImage(String openImage) {
        this.openImage = openImage;
    }

    public int doStartTmxTag() throws JspException {
        super.doStartTmxTag();
        StringBuffer sb = new StringBuffer();
        String fname = getSeparatorReplacedName(getProperty());

        sb.append("<script language='JavaScript'>\r");
        sb.append("var ").append(fname).append("_show = ").append(getFilterSetObject().isVisible()).append(";\r");

        sb.append("function ").append(fname).append("_show_filter(){\r");
        sb.append("\tdocument.getElementById('").append(fname).append(FILTER_SET_VISIBLE_NAME).append("').value=")
                .append(fname).append("_show;\r");
        sb.append("\tif(");
        sb.append(fname).append("_show == false){\r");
        sb.append("\t\tdocument.getElementById('").append(fname).append("_table').style.display='none';\r");
        sb.append("\t\tdocument.getElementById('").append(fname).append("_img').src='").append(getOpenImage()).append("';\r");
        sb.append("\t}else{\r");
        sb.append("\t\tdocument.getElementById('").append(fname).append("_table').style.display='inline';\r");
        sb.append("\t\tdocument.getElementById('").append(fname).append("_img').src='").append(getCloseImage()).append("';\r");
        sb.append("\t}\r");
        sb.append(fname).append("_show = !").append(fname).append("_show;\r");
        sb.append("}\r");

        sb.append("registerFunction('window.onload', function(){").append(fname).append("_show_filter();});\r");
        sb.append("</script>\r");

        sb.append("<div class='").append(getDivStyle()).append("'>\r");
        sb.append("<div onclick='").append(fname).append("_show_filter()' style='cursor : hand; width:100%'>\r");
        sb.append("<img name='").append(fname).append("_img'>&nbsp;&nbsp;\r");
        sb.append(getLocalizedLabel("tag.panelFilterSet.label.filters", null));
        sb.append("</div>\r");
        sb.append("<table border='0' width='").append(getWidth()).append("' id='").append(fname).append("_table' class='").append(getTableStyle() != null ? getTableStyle() :"");
        sb.append("'>\r<tr>\r");



        //Subtable for buttons
        sb.append("\r<td><table><tr>\r");
        //Submit button, on click - submit form------------------------------------------------------------------
        sb.append("<td style='width:5' align='right' >\r");
        sb.append("<input type='button' class='").append(getApplyButtonStyle()).append("' value='")
                .append(getLocalizedLabel("tag.panelFilterSet.label.apply",null))
                .append("' onclick='"  +  getSeparatorReplacedName(getProperty()) + "_onClick()" +  "'>\r</td>\r");
        sb.append(renderOnClickScript());

        //Reset Button-------------------------------------------------------------------------------------------
        String javaScriptFunctionName = getTableName() + "_on_reset_click()";

        sb.append("<td style='width:5' align='left'>\r");
        sb.append("<input type='button' class='").append(getApplyButtonStyle()).append("' value='")
                .append(getLocalizedLabel("tag.panelFilterSet.label.reset",null))
                .append("' onclick='"+javaScriptFunctionName+"'>\r</td>\r");//set listener

        sb.append("\r</tr></table></td>\r");
        sb.append("</tr><tr><td>\r");

        TagUtils.getInstance().write(pageContext, sb.toString());
        return EVAL_BODY_INCLUDE;
    }

    /**
     * Create html code. contains:
     *                    visible:  1. Submit button
     *                              2. Reset button
     *
     *                  unvisible:  3. hidden element binded with reset button
     *                              4. JS function - reset button listener                   
     * @throws JspException
     */
    public int doEndTmxTag() throws JspException{
        setProperty(getTableName());
        StringBuffer sb = new StringBuffer();

        // because tables have different names, and all table has only one
        // hidden element and java script listener binded with reset button
        String hiddenTagId = getTableName();
        String javaScriptFunctionName = getTableName() + "_on_reset_click()";

        sb.append("</td>\r");


        //Hidden element, used for autopopulation------------------------------------------------------------------
        sb.append("<input type=\"hidden\" id='"+hiddenTagId+"' name=\""+getProperty()+"."+FILTERSET_NAME+"."+FILTERSET_COMMAND_NAME+
                "\" value=\""+""+"\" \\>\r");

        //begin java script: event listener on reset button click----------------------------------------------
        sb.append("    <script type=\"text/javascript\">\r");
        sb.append("\r");
        sb.append("        function "+javaScriptFunctionName+"{\r");
        sb.append("            hidden = document.getElementById('"+hiddenTagId+"');\r");
        sb.append("            hidden.value = '"+ FilterSet.CMD_RESET +"';\r");
        sb.append("            document.forms[0].submit();\r");
        sb.append("        }\r");
        sb.append("\r");
        sb.append("    </script>\r");
        //end java script------------------------------------------------------------------------------------------
        
        sb.append("\r");
        sb.append("</tr>\r</table>\r</div>\r");

        TagUtils.getInstance().write(pageContext, sb.toString());

        super.doEndTmxTag();
        return EVAL_PAGE;
    }

    protected String renderOnClickScript() throws JspException {
        String actionUrl = ApplicationEnvironment.getAppDeploymentName() +
                           getFilterSetObject().getParent().getRefreshActionName() +
                           ApplicationEnvironment.getActionExtension() + "?command=" +
                           getFilterSetObject().getParent().getRefreshCommandName();
        StringBuffer resultHTML = new StringBuffer();
        resultHTML.append("<script type=\"text/javascript\">");
        resultHTML.append("\r");
        resultHTML.append("     function " + getSeparatorReplacedName(getProperty()) + "_"+"onClick() {\r");
        resultHTML.append("     document.forms[0].action=\""+actionUrl+"\";\r");
        resultHTML.append("     document.forms[0].method='POST';\r");
        resultHTML.append("     document.forms[0].submit();\r");
        resultHTML.append("     }\r");
        resultHTML.append("\r");
        resultHTML.append("</script>\r");
        return resultHTML.toString();
    }


}
