package com.tmx.as.blogic;

import com.tmx.util.StructurizedException;

import java.util.Locale;

/**
 * This exception must throw in business logic layer - for example UserManager or other 
 * and catch in actions. It encapsulate problem information evolved in business logic algorithms  
 * @author Andrey Nagorniy
 */
public class BlogicException extends StructurizedException {

     public BlogicException(String errKey, Throwable cause){
        super(errKey, cause);
    }

    public BlogicException(String errKey, Throwable cause, Locale locale){
        super(errKey, cause, locale);
    }

    public BlogicException(String errKey, String[] params, Throwable cause){
        super(errKey, params, cause);
    }

    public BlogicException(String errKey){
        super(errKey);
    }

    public BlogicException(String errKey, String[] params){
        super(errKey, params);
    }

    public BlogicException(String errKey, String[] params, Throwable cause, Locale locale){
        super(errKey, params, cause, locale);
    }


}
