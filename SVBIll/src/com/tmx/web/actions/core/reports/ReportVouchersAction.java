package com.tmx.web.actions.core.reports;

import com.tmx.web.base.BasicDispatchedAction;
import com.tmx.web.base.SecurityService;
import com.tmx.web.base.ApplicationEnvironment;
import com.tmx.web.forms.core.reports.ReportVouchersForm;
import com.tmx.web.forms.core.base.AbstractFormFactory;
import com.tmx.web.controls.grid.Grid;
import com.tmx.web.controls.grid.GridException;
import com.tmx.web.controls.ScrollBox;
import com.tmx.web.controls.BrowseBox;
import com.tmx.as.blogic.VoucherManager;
import com.tmx.as.blogic.SellerManager;
import com.tmx.as.blogic.TerminalManager;
import com.tmx.as.exceptions.DatabaseException;
import com.tmx.as.entities.bill.voucher.Voucher;
import com.tmx.as.entities.bill.seller.Seller;
import com.tmx.as.entities.bill.terminal.Terminal;
import com.tmx.as.entities.general.user.User;
import com.tmx.util.i18n.MessageResources;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForm;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

public class ReportVouchersAction extends BasicDispatchedAction {
    private static final String FORWARD = "page";

    private final String SLD_STATUS = "SLD";
    private final String NEW_STATUS = "NEW";

    public ActionForward defaultAction(ActionMapping mapping,
                                       ActionForm form,
                                       HttpServletRequest request,
                                       HttpServletResponse response,
                                       String methodName) throws Throwable {
        ReportVouchersForm reportVauchersForm = (ReportVouchersForm) form;

        reportVauchersForm.bindData(); //TODO: move to separate method
        AbstractFormFactory.getInstance(getLoggedInUser(request)).setUpForm(reportVauchersForm);
        reportVauchersForm.getButtonSubmit().setAction("/core/report/reportVauchersAction");
        reportVauchersForm.refresh();

        return mapping.findForward(FORWARD);
    }

    public ActionForward refreshVoucherReportGrid(ActionMapping mapping,
                                                  ActionForm form,
                                                  HttpServletRequest request,
                                                  HttpServletResponse response) throws Throwable {
        ReportVouchersForm reportVauchersForm = (ReportVouchersForm) form;
        if (!reportVauchersForm.getScrollBoxNominal().isNullKeySelected()) {
            reportVauchersForm.getScrollBoxOperator().setSelectedKey(ScrollBox.NULL_KEY); //clear operatot filter
        }

        if(reportVauchersForm.getBrowseBoxTerminals().getKey() != null && !reportVauchersForm.getBrowseBoxTerminals().getKey().equals("")) {
            Terminal terminal = new TerminalManager().getTerminal(new Long(reportVauchersForm.getBrowseBoxTerminals().getKey()));
            if (terminal.getSellerOwner() != null) {
                reportVauchersForm.getScrollBoxSeller().setSelectedKey(terminal.getSellerOwner().getSellerId().toString());
            }
        }

        reportVauchersForm.refresh();
        
        return mapping.findForward(FORWARD);
    }

    public ActionForward updateBrowseBoxTerminals(ActionMapping mapping,
                                                  ActionForm form,
                                                  HttpServletRequest request,
                                                  HttpServletResponse response) throws Throwable {
        ReportVouchersForm reportVauchersForm = (ReportVouchersForm) form;

        if (!reportVauchersForm.getScrollBoxSeller().isNullKeySelected()) {
            reportVauchersForm.getBrowseBoxTerminals().reset();
            reportVauchersForm.getBrowseBoxTerminals().setKey("");
        }

        return mapping.findForward(FORWARD);
    }

    protected User getLoggedInUser(HttpServletRequest request) throws com.tmx.as.exceptions.SecurityException {
        SecurityService securityService = (SecurityService) ApplicationEnvironment.getService(SecurityService.class);
        return securityService.getLoggedInUser(request.getSession());
    }
}
