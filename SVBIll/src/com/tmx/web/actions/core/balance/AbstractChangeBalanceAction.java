package com.tmx.web.actions.core.balance;

import com.tmx.web.forms.core.balance.AbstractChangeBalanceForm;
import com.tmx.web.actions.core.AbstractSvbillAction;
import com.tmx.as.blogic.TransactionManager;
import com.tmx.as.blogic.balance.AbstractBalanceManager;
import com.tmx.as.entities.bill.transaction.TransactionType;
import com.tmx.util.InitException;
import com.tmx.beng.base.BillException;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForm;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;


abstract public class AbstractChangeBalanceAction extends AbstractSvbillAction{

    abstract protected String getInfoPageForward();
    abstract protected AbstractBalanceManager getBalanceManager() throws InitException, BillException;

    public ActionForward change(ActionMapping mapping,
                                       ActionForm form,
                                       HttpServletRequest request,
                                       HttpServletResponse response) throws Throwable {
        AbstractChangeBalanceForm abstractChangeBalForm = (AbstractChangeBalanceForm)verifyForm(form, AbstractChangeBalanceForm.class);
        abstractChangeBalForm.validate();
        getBalanceManager().changeBalance(abstractChangeBalForm.populateBalanceConfig(getLoggedInUser(request)));
        return mapping.findForward(getInfoPageForward());
    }

    public ActionForward preloadForChange(ActionMapping mapping,
                                       ActionForm form,
                                       HttpServletRequest request,
                                       HttpServletResponse response) throws Throwable {
        AbstractChangeBalanceForm abstractChangeBalForm = (AbstractChangeBalanceForm)verifyForm(form, AbstractChangeBalanceForm.class);
        abstractChangeBalForm.reset();
        List types = new TransactionManager().getAllTransactionType();
        abstractChangeBalForm.getTransTypeScrollBox().bind(types);
        abstractChangeBalForm.getTransTypeScrollBox().setSelectedKey(((TransactionType) types.get(1)).getCode());
        abstractChangeBalForm.bindData(getBalanceManager().getBalance(abstractChangeBalForm.getBalanceId()));
        return mapping.findForward("changeBalancePage");
    }

}
