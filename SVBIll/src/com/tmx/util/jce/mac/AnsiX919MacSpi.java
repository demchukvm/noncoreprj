package com.tmx.util.jce.mac;

import javax.crypto.Cipher;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESKeySpec;
import java.security.Key;
import java.security.InvalidKeyException;
import java.security.InvalidAlgorithmParameterException;
import java.security.spec.AlgorithmParameterSpec;

/**
    ANSI X9.19 MAC SPI
    Not a thread safe implementation.
 */
public final class AnsiX919MacSpi extends AnsiXMacSpi {
    private final String DESEDE_ALGORITHM = "DESede";
    private Cipher desEdeCipher;//triple DES

    //-----------Engine methods
    //todo replace with protected
    public void engineInit(Key key, AlgorithmParameterSpec algorithmParameterSpec) throws InvalidKeyException, InvalidAlgorithmParameterException {
        if(algorithmParameterSpec == null)
            throw new InvalidAlgorithmParameterException("Mandatory algoritm parameter is absent");

        if(algorithmParameterSpec instanceof X919AlgParameterSpec){
            desCipher = ((X919AlgParameterSpec)algorithmParameterSpec).getDesCipher();
            if(desCipher == null)
                throw new InvalidAlgorithmParameterException("Algoritm parameter has null DES cipher");
            if(!DES_ALGORITHM.equals(desCipher.getAlgorithm()))
                throw new InvalidAlgorithmParameterException("Algoritm parameter has not a DES cipher");

            desEdeCipher = ((X919AlgParameterSpec)algorithmParameterSpec).getDesEdeCipher();
            if(desEdeCipher == null)
                throw new InvalidAlgorithmParameterException("Algoritm parameter has null DESede cipher");
            if(!DESEDE_ALGORITHM.equals(desEdeCipher.getAlgorithm()))
                throw new InvalidAlgorithmParameterException("Algoritm parameter has not a DESede cipher");
        }
        else
            throw new InvalidAlgorithmParameterException("Algoritm parameter should be instatnce of "+X919AlgParameterSpec.class);

        desEdeCipher.init(Cipher.ENCRYPT_MODE, key);
        desCipher.init(Cipher.ENCRYPT_MODE, convertTripleDesToDesKey(key));
    }

    //todo replace with protected
    public byte[] engineDoFinal() {
        byte[] lastChunk = propagateZeroUpToChunk(chunkExcess);
        chunkExcess = null;
        calculateMacForLastChunk(lastChunk);
        byte[] tmpCurrentMac = new byte[CHUNK_LENGTH];
        System.arraycopy(currentMac, 0, tmpCurrentMac, 0, CHUNK_LENGTH); //save MAC to chunk size to save it during reset. Fit it to chunk size

        engineReset();
        return tmpCurrentMac;
    }


    //-----------Auxilary methods
    /** Unite excess data from previos step and authentificated data from this step to
     * prodice divisible to chunk size data block. New excess data is stored.
     * Returns null if nothing to process on this step.
     *
     * We always should have one enough chunk (or part of chunk) in excess data to be encoded with Triple DES
     * cipher on doFinal call.
     *  */
    protected byte[] collectToFitChunkSize(byte[] authentificatedData, int offset, int authentificatedDataLength){
        //1. Prepare shunk length divisible data block
        if(chunkExcess == null)
            chunkExcess = new byte[0];

        int unionLength = chunkExcess.length + authentificatedDataLength;
        if(unionLength <= CHUNK_LENGTH){
            // Nothing to process with MAC. Authentificated data with previos chunk excess less then 1 chunk.
            // Collect authentificated data in the chunkExcess.
            byte[] buffer = new byte[unionLength];
            System.arraycopy(chunkExcess, 0, buffer, 0, chunkExcess.length);
            System.arraycopy(authentificatedData, offset, buffer, chunkExcess.length, authentificatedDataLength);
            chunkExcess = buffer;
            return null;
        }
        else if(unionLength % CHUNK_LENGTH > 0){
            // Prepare chunks for process with MAC.
            int newAuthDataLength = (unionLength / CHUNK_LENGTH) * CHUNK_LENGTH;
            byte[] authDataBuffer = new byte[newAuthDataLength];
            System.arraycopy(chunkExcess, 0, authDataBuffer, 0, chunkExcess.length);
            System.arraycopy(authentificatedData, offset, authDataBuffer, chunkExcess.length, newAuthDataLength-chunkExcess.length);

            byte[] excessBuffer = new byte[authentificatedDataLength + chunkExcess.length - newAuthDataLength];
            System.arraycopy(authentificatedData, newAuthDataLength-chunkExcess.length+offset, excessBuffer, 0, excessBuffer.length);
            authentificatedData = authDataBuffer;
            chunkExcess = excessBuffer;
        }
        else {
            // Union of previos excess data and authentificatedData divisible to chunk size.
            // Prepare chunks for process with MAC. Hold last chunk in excess data to be
            // encoded with TripleDES cipher if doFinal call will be done after this step.
            int newAuthDataLength = (unionLength / CHUNK_LENGTH - 1) * CHUNK_LENGTH;
            byte[] authDataBuffer = new byte[newAuthDataLength];
            System.arraycopy(chunkExcess, 0, authDataBuffer, 0, chunkExcess.length);
            System.arraycopy(authentificatedData, offset, authDataBuffer, chunkExcess.length, newAuthDataLength-chunkExcess.length);

            byte[] excessBuffer = new byte[CHUNK_LENGTH];
            System.arraycopy(authentificatedData, newAuthDataLength-chunkExcess.length+offset, excessBuffer, 0, excessBuffer.length);
            authentificatedData = authDataBuffer;
            chunkExcess = excessBuffer;
        }
        return authentificatedData;
    }

    /** convert Triple DES Key to DES Key */    
    private Key convertTripleDesToDesKey(Key tripleDesKey) throws InvalidKeyException{
        try{
            byte[] rawDesKey = new byte[CHUNK_LENGTH];
            System.arraycopy(tripleDesKey.getEncoded(), 0, rawDesKey, 0, CHUNK_LENGTH);
            DESKeySpec keyspec = new DESKeySpec(rawDesKey);
            SecretKeyFactory keyfactory = SecretKeyFactory.getInstance(DES_ALGORITHM);
            return keyfactory.generateSecret(keyspec);
        }
        catch(Exception e){
            throw new InvalidKeyException("Failed to convert DESede key into DES key: "+e.toString());
        }

    }

    private void calculateMacForLastChunk(byte[] lastChunk){
        //xor last chunk with current MAC if this is not a first step
        if(currentMac != null)
            currentMac = xorChunkWithCurrentMac(lastChunk, currentMac, 0);
        else
            currentMac = lastChunk;

        //encrypt last MAC with triple des.
        currentMac = encryptChunk(desEdeCipher, currentMac);
    }

}