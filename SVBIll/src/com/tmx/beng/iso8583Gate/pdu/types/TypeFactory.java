package com.tmx.beng.iso8583Gate.pdu.types;

import java.util.Map;
import java.util.HashMap;


/**
 */
public class TypeFactory {
    private static Map instances = new HashMap();
    public static final Type NUMBER = new NumberType();
    public static final Type BINARY = new BinaryType();
    public static final Type ACHARS = new AlphabeticCharactersType();
    public static final Type ANCHARS = new AlphabeticNumericCharactersType();
    public static final Type ANSCHARS = new AlphabeticNumericSpecialCharactersType();
    public static final Type ASCHARS = new AlphabeticSpecialCharactersType();
    public static final Type SCHARS = new SpecialCharactersType();
    public static final Type NSCHARS = new NumericSpecialCharactersType();   
    public static final Type TIMESTAMP = new TimestampType();
    public static final Type Z = new ZType();


    static{
        instances.put(NUMBER.getName(), NUMBER);
        instances.put(BINARY.getName(), BINARY);
        instances.put(ACHARS.getName(), ACHARS);
        instances.put(ANCHARS.getName(), ANCHARS);
        instances.put(ANSCHARS.getName(), ANSCHARS);
        instances.put(ASCHARS.getName(), ASCHARS);
        instances.put(SCHARS.getName(), SCHARS);
        instances.put(NSCHARS.getName(), NSCHARS);
        instances.put(TIMESTAMP.getName(), TIMESTAMP);
        instances.put(Z.getName(), Z);
    }


    public static Type getInstance(String name){
        return (Type)instances.get(name);
    }
    
}
