package com.tmx.beng.base.processors;

import com.tmx.beng.base.*;
import com.tmx.beng.base.cache.DBCache;

/**
 * Register message
 */
public class ResponseErrorProcessor extends TaskProcessor {

    public Task process0(Task task, DBCache dbCache, BillingMessageWritable msg) throws BillException{
        return task;/** do nothing - only for pipeline rollback */
    }

    public Task rollback(Task task, Throwable e) {
        logger.debug("Response ERROR");
        try{
            logger.error(e.getLocalizedMessage(), e);

            BillingMessageImpl resp = new BillingMessageImpl();
            resp.setOperationName(task.getProcesedMessage().getOperationName());
            //evaluate status
            String status = task.getProcesedMessage().getAttributeString(BillingMessage.STATUS_CODE);
            resp.setStatus(
                    (status != null) ? Integer.parseInt(status) : StatusDictionary.UNKNOWN_STATUS,
                    e.getLocalizedMessage());

            resp.setAttribute(BillingMessage.REGISTRATION_TIME, task.getProcesedMessage().getAttribute(BillingMessage.REGISTRATION_TIME));



            if(task.getCallBack() != null){
                task.getCallBack().sendBilledMessage(resp);
            }
            else{
                logger.error("Callback is null");
            }
        }
        catch(Throwable t){
            logger.error(t.getLocalizedMessage(), t);
        }
        return super.rollback(task, e);
    }
}
