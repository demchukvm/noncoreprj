package com.tmx.web.forms.core.dictionary.territory;

import com.tmx.web.base.BasicActionForm;
import com.tmx.web.controls.EditBox;
import com.tmx.web.controls.ScrollBox;
import com.tmx.web.controls.Button;
import com.tmx.web.controls.PopulateUtil;
import com.tmx.as.entities.network.territory.Country;

public class InfoCountryForm extends BasicActionForm {

    private Long selectedId;
    private EditBox id = new EditBox();
    private EditBox shortName = new EditBox();
    private EditBox name = new EditBox();
    
    private Button onSaveButton = new Button();
    private Button  onResetButton = new Button();
    private Button  onCancelButton = new Button();


    protected void init() {
            id.setReadonly(true);
            shortName.setMandatory(true);
            name.setMandatory(true);
            //set up buttons
            onSaveButton = new Button();
            onSaveButton.setLabelKey("ctrl.button.label.save");
            onSaveButton.setAction("/core/dictionary/territory/infoCountry");
            onSaveButton.setCommand("update");

            onResetButton = new Button();
            onResetButton.setLabelKey("ctrl.button.label.reset");

            onCancelButton = new Button();
            onCancelButton.setLabelKey("ctrl.button.label.cancel");
            onCancelButton.setAction("/core/dictionary/territory/manageCountries");
        }


    public void bindData(Country value) {
        id.setValue(value.getId() == null ? "" : value.getId().toString());
        shortName.setValue(value.getShortName());
        name.setValue(value.getName());
    }

    public Country populateFromControls(Country value) {
        value.setCountryId(PopulateUtil.getLongValue(id));
        value.setShortName(shortName.getValue());
        value.setName(name.getValue());
        return value;
    }

        //-------------------PROPERTIES

    public Long getSelectedId() {
        return selectedId;
    }

    public void setSelectedId(Long selectedId) {
        this.selectedId = selectedId;
    }

    public EditBox getId() {
        return id;
    }

    public void setId(EditBox id) {
        this.id = id;
    }

    public EditBox getShortName() {
        return shortName;
    }

    public void setShortName(EditBox shortName) {
        this.shortName = shortName;
    }

    public EditBox getName() {
        return name;
    }

    public void setName(EditBox name) {
        this.name = name;
    }

    public Button getOnSaveButton() {
        return onSaveButton;
    }

    public void setOnSaveButton(Button onSaveButton) {
        this.onSaveButton = onSaveButton;
    }

    public Button getOnResetButton() {
        return onResetButton;
    }

    public void setOnResetButton(Button onResetButton) {
        this.onResetButton = onResetButton;
    }

    public Button getOnCancelButton() {
        return onCancelButton;
    }

    public void setOnCancelButton(Button onCancelButton) {
        this.onCancelButton = onCancelButton;
    }
}
