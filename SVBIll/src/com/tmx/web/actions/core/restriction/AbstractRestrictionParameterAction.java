package com.tmx.web.actions.core.restriction;

import com.tmx.as.entities.bill.function.ActualFunctionParameter;
import com.tmx.as.blogic.FunctionManager;
import com.tmx.web.base.Roadmap;
import com.tmx.web.base.WebException;
import com.tmx.web.forms.core.restriction.RestrictionParameterForm;
import com.tmx.web.forms.core.restriction.AbstractInfoRestrictionForm;
import com.tmx.web.actions.core.balance.AbstractBalanceAction;
import com.tmx.web.controls.table.ControlManagerTable;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Collections;
import java.util.List;


abstract public class AbstractRestrictionParameterAction extends AbstractBalanceAction {

    abstract protected AbstractInfoRestrictionForm getInfoRestrictionForm(ActionMapping mapping, HttpServletRequest request) throws WebException;

    private final String[] infoSeller = {"/core/restriction/seller_restriction/sellerRestrictionParameter",
                                        "/core/restriction/seller_restriction/infoSellerRestriction",
                                        "/core/restriction/seller_restriction/sellerRestrictionParameter"};

    private final String[] infoOperator = {"/core/restriction/operator_restriction/operatorRestrictionParameter",
                                        "/core/restriction/operator_restriction/infoOperatorRestriction",
                                        "/core/restriction/operator_restriction/operatorRestrictionParameter"};

    private final String[] infoTerminal = {"/core/restriction/terminal_restriction/terminalRestrictionParameter",
                                        "/core/restriction/terminal_restriction/infoTerminalRestriction",
                                        "/core/restriction/terminal_restriction/terminalRestrictionParameter"};


     public ActionForward preloadForDelete(ActionMapping mapping,
                                                ActionForm form,
                                                HttpServletRequest request,
                                                HttpServletResponse response) throws Throwable {
              Roadmap roadmap=getRoadmap();
              roadmap.collectParameter("/core/message/confirmation", "leftMenuName", "restrictions");
              roadmap.setPageForward("confirmation","onOk","local:"+this.getName()+"#"+"manageDeleteAction");
              roadmap.setPageForward("confirmation","onCancel","local:"+this.getName()+"#"+ "infoRestrictionUpdate");
              return mapping.findForward("confirmation");
          }
    public ActionForward delete(ActionMapping mapping,
                                   ActionForm form,
                                   HttpServletRequest request,
                                   HttpServletResponse response) throws Throwable {
             RestrictionParameterForm parameterForm = (RestrictionParameterForm) form;
             AbstractInfoRestrictionForm restFrom = getInfoRestrictionForm(mapping, request);
             ActualFunctionParameter delParam = findParam(restFrom.getFuncParametrsTable(), parameterForm.getParameterId());
             restFrom.getFuncParametrsTable().removeData(Collections.singletonList(delParam));
             return mapping.findForward("infoRestrictionUpdate");
         }

    public ActionForward preloadForCreate(ActionMapping mapping,
                                          ActionForm form,
                                          HttpServletRequest request,
                                          HttpServletResponse response) throws Throwable {
        RestrictionParameterForm paramForm = (RestrictionParameterForm) form;
        paramForm.reset();
        paramForm.setParameterId(null);
        setForward(form);
        return mapping.findForward("parameterPage");
    }

    public ActionForward preloadForUpdate(ActionMapping mapping,
                                   ActionForm form,
                                   HttpServletRequest request,
                                   HttpServletResponse response) throws Throwable {
             RestrictionParameterForm paramForm = (RestrictionParameterForm) form;
             AbstractInfoRestrictionForm restForm = getInfoRestrictionForm(mapping, request);
             ActualFunctionParameter param = findParam(restForm.getFuncParametrsTable(), paramForm.getParameterId());
             paramForm.reset();
             paramForm.bindData(param);
             setForward(form);
             return mapping.findForward("parameterPage");
         }


    public ActionForward save(ActionMapping mapping,
                                   ActionForm form,
                                   HttpServletRequest request,
                                   HttpServletResponse response) throws Throwable {
             RestrictionParameterForm parameterForm = (RestrictionParameterForm) verifyForm(form, RestrictionParameterForm.class);
             parameterForm.validate();

             ActualFunctionParameter param = parameterForm.getParameterId() == null ? new ActualFunctionParameter() :// if create new param
                     findParam(getInfoRestrictionForm(mapping, request).getFuncParametrsTable(), parameterForm.getParameterId());

             param.setName(parameterForm.populateFromControl().getName());
             param.setOrderNum(parameterForm.populateFromControl().getOrderNum());
             param.setValue(parameterForm.populateFromControl().getValue());
             param.setType(parameterForm.populateFromControl().getType());

             boolean hasParam = false;
             List<ActualFunctionParameter> paramTable = getInfoRestrictionForm(mapping, request).getFuncParametrsTable().getData();

             for (int i=0; i<paramTable.size(); i++) {
               if (param.getName().equals(paramTable.get(i).getName())) {
                   hasParam = true;
                   paramTable.set(i,param);
               }
             }

     //        if (parameterForm.getParameterId() == null && !hasParam) { //if create new param
             if (!hasParam) { //if create new param
                 getInfoRestrictionForm(mapping, request).getFuncParametrsTable().addData(Collections.singletonList(param));
             }

             return mapping.findForward("infoRestrictionUpdate");
         }

    public ActionForward apply(ActionMapping mapping,
                                   ActionForm form,
                                   HttpServletRequest request,
                                   HttpServletResponse response) throws Throwable {
             save(mapping, form, request, response);
     //        preloadForUpdate(mapping, form, request, response);
             return mapping.findForward("parameterPage");
         }

    private ActualFunctionParameter findParam(ControlManagerTable table, Long paramId) {
             return (ActualFunctionParameter) table.getData().get(paramId.intValue());
         }

    private void setForward(ActionForm form) {
        RestrictionParameterForm paramForm = (RestrictionParameterForm) form;
        String formName = paramForm.getFormName();
        if (formName.equals("core.restriction.seller_restriction.sellerParameterForm")) {
            paramForm.getOnApplyButton().setAction(infoSeller[0]);
            paramForm.getOnCancelButton().setAction(infoSeller[1]);
            paramForm.getOnSaveButton().setAction(infoSeller[2]);
        }

        if (formName.equals("core.restriction.operator_restriction.operatorParameterForm")) {
            paramForm.getOnApplyButton().setAction(infoOperator[0]);
            paramForm.getOnCancelButton().setAction(infoOperator[1]);
            paramForm.getOnSaveButton().setAction(infoOperator[2]);
        }

        if (formName.equals("core.restriction.terminal_restriction.terminalParameterForm")) {
            paramForm.getOnApplyButton().setAction(infoTerminal[0]);
            paramForm.getOnCancelButton().setAction(infoTerminal[1]);
            paramForm.getOnSaveButton().setAction(infoTerminal[2]);
        }
    }
}
