package com.tmx.beng.ussdgate;

import com.tmx.util.StructurizedException;
import com.tmx.beng.medaccess.GateException;

import java.util.Locale;

/**
 */
public class UssdGateException extends GateException {

    public UssdGateException(String msg, String[] params, Throwable e, Locale locale) {
        super(msg, params, e, locale);
    }

    public UssdGateException(String msg, String[] params) {
        super(msg, params);
    }

    public UssdGateException(String msg, String[] params, Locale locale) {
        super(msg, params, locale);
    }

    public UssdGateException(String msg, Throwable e, Locale locale) {
        super(msg, e, locale);
    }

    public UssdGateException(String msg, Throwable e) {
        super(msg, e);
    }

    public UssdGateException(String msg, Locale locale) {
        super(msg, locale);
    }

    public UssdGateException(String msg) {
        super(msg);
    }

    public UssdGateException(String msg, String[] params, Throwable e) {
        super(msg, params, e);
    }
}
