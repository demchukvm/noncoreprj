package com.tmx.engines.reportengine.aggrtable.validation;

import com.tmx.engines.reportengine.aggrtable.DataElement;

/**

 */
public abstract class BasicValidationRule implements ValidationRule{
    private String ownerFactName = null;
    private int runNumber = -1;
        
    public void init(int runNumber, String factFunctionName){
        this.ownerFactName = factFunctionName;
        this.runNumber = runNumber;
    }

    public String getOwnerFactName() {
        return ownerFactName;
    }

    public int getRunNumber() {
        return runNumber;
    }

    protected Double convertToDouble(DataElement data) {
        double resultValue = 0;
        if(data == null)
            return null;
        Object originalValue = data.getOriginalValue();
        if(originalValue == null)
            return null;
        if(originalValue instanceof Long){
            resultValue = ((Long)originalValue).doubleValue();
        }
        else if(originalValue instanceof Integer){
            resultValue = ((Integer)originalValue).doubleValue();
        }
        else if(originalValue instanceof Float){
            resultValue = ((Float)originalValue).doubleValue();
        }
        else if(originalValue instanceof Double){
            resultValue = ((Double)originalValue).doubleValue();
        }
        else if(originalValue instanceof Short){
            resultValue = ((Short)originalValue).doubleValue();
        }
        else{
            return null;
        }
        return new Double(resultValue);
    }

}
