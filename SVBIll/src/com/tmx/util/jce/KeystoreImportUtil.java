package com.tmx.util.jce;

import com.tmx.util.Configuration;
import com.tmx.util.InitException;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import java.io.*;
import java.security.KeyFactory;
import java.security.KeyStore;
import java.security.cert.Certificate;
import java.security.cert.CertificateFactory;
import java.security.interfaces.RSAPrivateKey;
import java.security.spec.PKCS8EncodedKeySpec;
import java.util.Collection;

/**
 *
 */
public class KeystoreImportUtil {
   // private Logger logger = Logger.getLogger("util");
    private String keystorename;
    private String keypass;
    private String alias;
    private String keyfile;
    private String certfile;


    private void init() throws InitException {
        String tmxHome = System.getProperty("tmx.home");
        if (tmxHome == null)
            throw new InitException("System property 'tmx.home' is not set");

        //2. Initiate properties loading
        Configuration config = Configuration.getInstance();

        //3. Configure logging
        PropertyConfigurator.configure(config.getProperty("log_config", tmxHome + "/conf/log4j.properties"));

        //required props
        //logger.info("KeystoreImportUtil initialization...");

        keypass = System.getProperty("keypass");
        if(keypass == null)
            throw new InitException("System property 'keypass' is not specified");

        alias = System.getProperty("alias");
        if(alias == null)
            throw new InitException("System property 'alias' is not specified");

        keystorename = System.getProperty("keystorename");
        if(keystorename == null)
            throw new InitException("System property 'keystorename' is not specified");

        keyfile = System.getProperty("keyfile");
        if(keyfile == null)
            throw new InitException("System property 'keyfile' is not specified");

        certfile = System.getProperty("certfile");
        if(certfile == null)
            throw new InitException("System property 'certfile' is not specified");

       // logger.info(" successful");
    }

    /**
     * <p>Takes two file names for a key and the certificate for the key,
     * and imports those into a keystore. Optionally it takes an alias
     * for the key.
     * <p>The first argument is the filename for the key. The key should be
     * in PKCS8-format.
     **/
    public static void main(String args[]) throws InitException{
        KeystoreImportUtil util = new KeystoreImportUtil();
        util.init();
        util.run();
    }


    private void run(){
        try {
            // initializing and clearing keystore
            KeyStore ks = KeyStore.getInstance("JKS", "SUN");
            ks.load(null, keypass.toCharArray());
            System.out.println("Using keystore-file : "+keystorename);
            ks.store(new FileOutputStream ( keystorename  ),
                    keypass.toCharArray());
            ks.load(new FileInputStream ( keystorename ),
                    keypass.toCharArray());

            // loading Key
            InputStream fl = fullStream (keyfile);
            byte[] key = new byte[fl.available()];
            fl.read(key, 0, fl.available());
            fl.close();

            KeyFactory kf = KeyFactory.getInstance("RSA");
            PKCS8EncodedKeySpec keysp = new PKCS8EncodedKeySpec(key);
            RSAPrivateKey privKey = (RSAPrivateKey) kf.generatePrivate(keysp);

            // loading CertificateChain
            CertificateFactory cf = CertificateFactory.getInstance("X.509");
            InputStream certstream = fullStream (certfile);

            Collection c = cf.generateCertificates(certstream) ;
            Certificate[] certs = new Certificate[c.toArray().length];

            if (c.size() == 1) {
                certstream = fullStream (certfile);
                System.out.println("One certificate, no chain.");
                Certificate cert = cf.generateCertificate(certstream) ;
                certs[0] = cert;
            }
            else {
                System.out.println("Certificate chain length: "+c.size());
                certs = (Certificate[])c.toArray();
            }

            //storing keystore
            ks.setKeyEntry(alias, privKey, keypass.toCharArray(), certs);
           // logger.info("Key and certificate stored.");
          //  logger.info("Alias:"+alias+"  Password:"+keypass);
            ks.store(new FileOutputStream(keystorename), keypass.toCharArray());
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /**
     * <p>Creates an InputStream from a file, and fills it with the complete
     * file. Thus, available() on the returned InputStream will return the
     * full number of bytes the file contains</p>
     * @param fname The filename
     * @return The filled InputStream
     * @exception java.io.IOException, if the Streams couldn't be created.
     **/
    private static InputStream fullStream(String fname) throws IOException {
        FileInputStream fis = new FileInputStream(fname);
        DataInputStream dis = new DataInputStream(fis);
        byte[] bytes = new byte[dis.available()];
        dis.readFully(bytes);
        ByteArrayInputStream bais = new ByteArrayInputStream(bytes);
        return bais;
    }
}
