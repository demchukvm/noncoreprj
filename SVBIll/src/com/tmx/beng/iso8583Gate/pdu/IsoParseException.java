package com.tmx.beng.iso8583Gate.pdu;

import com.tmx.util.StructurizedException;
import java.util.Locale;

/**

 */
public class IsoParseException extends StructurizedException {

    public IsoParseException(String msg, String[] params, Throwable e, Locale locale) {
        super(msg, params, e, locale);
    }

    public IsoParseException(String msg, String[] params) {
        super(msg, params);
    }

    public IsoParseException(String msg, String[] params, Locale locale) {
        super(msg, params, locale);
    }

    public IsoParseException(String msg, Throwable e, Locale locale) {
        super(msg, e, locale);
    }

    public IsoParseException(String msg, Throwable e) {
        super(msg, e);
    }

    public IsoParseException(String msg, Locale locale) {
        super(msg, locale);
    }

    public IsoParseException(String msg) {
        super(msg);
    }

    public IsoParseException(String msg, String[] params, Throwable e) {
        super(msg, params, e);
    }
}
