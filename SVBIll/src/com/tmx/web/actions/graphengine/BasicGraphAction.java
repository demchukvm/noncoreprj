package com.tmx.web.actions.graphengine;

import com.tmx.web.base.*;
import com.tmx.web.forms.graphengine.BasicGraphPublisherForm;
import com.tmx.web.forms.graphengine.GraphSetupForm;
import com.tmx.as.exceptions.DatabaseException;
import com.tmx.engines.graphengine.base.GraphException;
import com.tmx.engines.graphengine.base.Graph;
import com.tmx.engines.graphengine.base.GraphService;
import com.tmx.engines.graphengine.base.ApproximationReport;
import com.tmx.engines.graphengine.beans.Set;
import com.tmx.engines.graphengine.beans.Block;
import com.tmx.engines.graphengine.beans.Data;
import com.tmx.engines.graphengine.util.MathTransform;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForm;
import org.apache.commons.math.MathException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public abstract class BasicGraphAction extends BasicAction {
    private Graph graph = null;

    public abstract ActionForward executeSpecificGraphAction(ActionMapping mapping,
                                                                 ActionForm form,
                                                                 HttpServletRequest request,
                                                                 HttpServletResponse response) throws DatabaseException, GraphException, WebException;

    public ActionForward executeSpecificAction(ActionMapping mapping,
                                               ActionForm form,
                                               HttpServletRequest request,
                                               HttpServletResponse response) throws Throwable {

        //you could add some general code for all graphs actions here
        ActionForward forward;
        forward = executeSpecificGraphAction(mapping, form, request, response);

        //common graph processing algorythm
        if(graph != null){

            //1. Approximate
            ApproximationReport approximationReport = null;
            if(form != null && form instanceof GraphSetupForm){
                GraphSetupForm graphSetupForm = (GraphSetupForm)form;
                if(graphSetupForm.getUseApproximation().isSelected()){
                    approximationReport = GraphService.getInstance().approximate(graph, graphSetupForm.getApproximationPoints());
                }
            }
            getBasicGraphPublisherForm(request).setApproximationReport(approximationReport);

            //2. Apply dimensions
            GraphService.getInstance().applyDimensions(graph);

            //3. Serialize doc
            graph.validate();
            getBasicGraphPublisherForm(request).setGraphXmlSerialized(graph.serialize());
        }
        else
            getBasicGraphPublisherForm(request).setGraphXmlSerialized("");

        return forward;
    }

    protected void initializeGraph(String graphTemplateName) throws GraphException{
        graph = GraphService.getInstance().initializeGraph(graphTemplateName);
        GraphService.getInstance().clearGraphSets(graph);
    }

    protected void initializeDefaultGraph(String graphType) throws GraphException{
        graph = GraphService.getInstance().initializeDefaultGraph(graphType);
        GraphService.getInstance().clearGraphSets(graph);
    }

    public Graph getGraph() throws GraphException{
        if(graph == null)
            throw new GraphException("err.web.graph_not_inited_yet");
        return graph;
    }

    private BasicGraphPublisherForm getBasicGraphPublisherForm(HttpServletRequest request){
        String graphPublisherFormName = "graphPublisherForm";
        BasicGraphPublisherForm _graphPublisherForm = (BasicGraphPublisherForm)SessionEnvironment.getAttr(request.getSession(true), graphPublisherFormName);
        if(_graphPublisherForm == null){
            _graphPublisherForm = new BasicGraphPublisherForm();
            SessionEnvironment.setAttr(request.getSession(), graphPublisherFormName, _graphPublisherForm);
        }
        return _graphPublisherForm;
    }

}
