package com.tmx.web.actions.core.balance;

import com.tmx.as.base.FilterWrapper;
import com.tmx.as.base.QueryParameterWrapper;
import com.tmx.as.base.criterion.CriterionWrapper;
import com.tmx.as.base.criterion.Restrictions;
import com.tmx.as.blogic.TransactionManager;
import com.tmx.as.blogic.OperatorManager;
import com.tmx.as.entities.bill.balance.AbstractBalance;
import com.tmx.as.entities.bill.operator.Operator;
import com.tmx.as.entities.general.user.User;
import com.tmx.as.exceptions.DatabaseException;
import com.tmx.beng.base.BillException;
import com.tmx.beng.base.StatusDictionary;
import com.tmx.util.InitException;
import com.tmx.web.base.Roadmap;
import com.tmx.web.controls.ScrollBox;
import com.tmx.web.controls.CriterionFactory;
import com.tmx.web.controls.table.FilterSet;
import com.tmx.web.controls.table.Table;
import com.tmx.web.forms.core.balance.AbstractInfoBalanceForm;
import com.tmx.web.forms.core.balance.seller_balance.InfoSellerBalanceForm;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.*;
import static java.util.Calendar.getInstance;

/**
 * Template method design pattern
 */
abstract public class AbstractInfoBalanceAction extends AbstractBalanceAction {

    abstract protected String getManageAction();

    abstract protected String getInfoPage();

    abstract protected String getManageDeleteAction();

    abstract protected String getInfoTransactionAction();

    //-------------------for template method design pattern---------------------------------
    abstract protected void preloadForUpdate(AbstractInfoBalanceForm form, AbstractBalance balance) throws DatabaseException, InitException, BillException, ClassNotFoundException;

    abstract protected void preloadForCreate(AbstractInfoBalanceForm form);

    //set up data, dependent on target balance(for example SellerBalance)
    abstract protected void preload(AbstractInfoBalanceForm form, AbstractBalance balance) throws Throwable;

    //--------------------------------------------------------------------------------------

    public ActionForward preloadForCreate(ActionMapping mapping,
                                          ActionForm form,
                                          HttpServletRequest request,
                                          HttpServletResponse response) throws Throwable {

        AbstractInfoBalanceForm abstractInfoBalanceForm = (AbstractInfoBalanceForm) form;
        abstractInfoBalanceForm.reset();
        preloadFilters(abstractInfoBalanceForm, getLoggedInUser(request));
        abstractInfoBalanceForm.disableControls(false);
        abstractInfoBalanceForm.disableButtons(true);
        abstractInfoBalanceForm.getOnChangeBalanceButton().setReadonly(true);
        preloadForCreate(abstractInfoBalanceForm);
        return mapping.findForward(getInfoPage());
    }

    public ActionForward preloadForUpdate(ActionMapping mapping,
                                          ActionForm form,
                                          HttpServletRequest request,
                                          HttpServletResponse response) throws Throwable {
        Roadmap roadmap = getRoadmap();
        roadmap.collectParameter(getInfoTransactionAction(), "onCancelAction", getInfoPage());

        //get form and reset it
        AbstractInfoBalanceForm abstractInfoBalanceForm = (AbstractInfoBalanceForm) verifyForm(form, AbstractInfoBalanceForm.class);
        abstractInfoBalanceForm.reset();

        //get target balance manager from sub class(by factory method pattern), and load target balance
        AbstractBalance balance = getBalanceManager().getBalance(abstractInfoBalanceForm.getBalanceId());

        //set up data by action, dependent on taget balance
        preload(abstractInfoBalanceForm, balance);

        //set up data by form
        abstractInfoBalanceForm.bind(balance);

        //preload common filters for all transaction table
        preloadFilters(abstractInfoBalanceForm, getLoggedInUser(request));

        //fill the financial activities tab by default date values
        initFinancialActivTabDef(abstractInfoBalanceForm);


        preloadForUpdate(abstractInfoBalanceForm, balance);

        //refresh tables
        abstractInfoBalanceForm.getTransactionsTable().refresh();

        //calculate transactions summa(on this abstraction level)
//        preloadTransSumma(abstractInfoBalanceForm);
        
        //load data to controls dependent from table filters
        if (abstractInfoBalanceForm instanceof InfoSellerBalanceForm) {//SD: temporary workaround
            preloadTableDependData(abstractInfoBalanceForm);
        }

        //close all controls
        abstractInfoBalanceForm.disableControls(true);
        abstractInfoBalanceForm.disableButtons(false);

        abstractInfoBalanceForm.getStatusScrollBox().bind(StatusDictionary.getStatuses());

        List<Operator> operators = new OperatorManager().getAllOperators();
        HashMap<Long, String> operatorsMap = new HashMap<Long, String>();
        for(Operator operator:operators) {
            operatorsMap.put(operator.getOperatorId(),operator.getName());
        }
        abstractInfoBalanceForm.setOperators(operatorsMap);
        return mapping.findForward(getInfoPage());
    }


    public ActionForward save(ActionMapping mapping,
                              ActionForm form,
                              HttpServletRequest request,
                              HttpServletResponse response) throws Throwable {
        AbstractInfoBalanceForm abstractInfoBalanceForm = (AbstractInfoBalanceForm) form;
        AbstractBalance balance = abstractInfoBalanceForm.populate();
        getBalanceManager().saveBalance(balance);
        abstractInfoBalanceForm.setBalanceId((Long) balance.getId());
        return mapping.findForward(getManageAction());
    }

    //
    public ActionForward apply(ActionMapping mapping,
                               ActionForm form,
                               HttpServletRequest request,
                               HttpServletResponse response) throws Throwable {
        save(mapping, form, request, response);
        preloadForUpdate(mapping, form, request, response);
        return mapping.findForward(getInfoPage());
    }

    //

    /*
    *applyTrans button fills the financial activities tab on a form
    * ulike the apply button, wich fills the data controlls in all form, exept the financial activities tab
    * */
    public ActionForward applyTrans(ActionMapping mapping,
                                    ActionForm form,
                                    HttpServletRequest request,
                                    HttpServletResponse response) throws Throwable {
        AbstractInfoBalanceForm abstractInfoBalanceForm = (AbstractInfoBalanceForm) verifyForm(form, AbstractInfoBalanceForm.class);
        Table table = abstractInfoBalanceForm.getTransactionsTable();
        String entityClass = table.getQuery().getEntityClass();
        QueryParameterWrapper[] transWrapper;
        transWrapper = getTransFilters(abstractInfoBalanceForm);
        final String noTransactions = "0.0";
        if (transWrapper[0].getValue() != null && transWrapper[1].getValue() != null) {
// -----------OK_status transactions
            TransactionManager.CurrentAmountResult calcCurAmountRes = new TransactionManager().calculateCurrentAmount(entityClass, "TransCurrentAmount", transWrapper);
            Float CurAmmount = calcCurAmountRes.getCurrentTransaction();
            abstractInfoBalanceForm.getEditBoxTransactionCurrentAmmount().setValue(CurAmmount == null ? noTransactions : CurAmmount.toString());

            TransactionManager.IncreaseBallanceResult calcIncRes = new TransactionManager().calculateBallanceIncrease(entityClass, "IncrsBalanceSum", transWrapper);
            Float IncSum = calcIncRes.getIncreaseBallanceSum();
            abstractInfoBalanceForm.getEditBoxTransactionIncreaceAmount().setValue(IncSum == null ? noTransactions : IncSum.toString());

            TransactionManager.TransCalculationResult calcRes = new TransactionManager().calculateBallanceTransactions(entityClass, "NomTransferReward", transWrapper);

            Float nominalTransSum = calcRes.getNominalTransSum();
            abstractInfoBalanceForm.getEditBoxNominalTransactionAmount().setValue(nominalTransSum == null ? noTransactions : nominalTransSum.toString());

            Float transferSum = calcRes.getTransferSum();
            abstractInfoBalanceForm.getEditBoxTransferAmount().setValue(transferSum == null ? noTransactions : transferSum.toString());

            Float awardSum = calcRes.getAwardSum();
            abstractInfoBalanceForm.getEditBoxTransactionAward().setValue(awardSum == null ? noTransactions : awardSum.toString());
        }
        return mapping.findForward(getInfoPage());
    }


    public ActionForward preloadForDelete(ActionMapping mapping,
                                          ActionForm form,
                                          HttpServletRequest request,
                                          HttpServletResponse response) throws Throwable {
        Roadmap roadmap = getRoadmap();
        roadmap.collectParameter("/core/message/confirmation", "leftMenuName", "balances");
        roadmap.setPageForward("confirmation", "onOk", "local:" + this.getName() + "#" + getManageDeleteAction());
        roadmap.setPageForward("confirmation", "onCancel", "local:" + this.getName() + "#" + getManageAction());
        return mapping.findForward("confirmation");
    }

    public ActionForward delete(ActionMapping mapping,
                                ActionForm form,
                                HttpServletRequest request,
                                HttpServletResponse response) throws Throwable {
        AbstractInfoBalanceForm abstractInfoBalanceForm = (AbstractInfoBalanceForm) verifyForm(form, AbstractInfoBalanceForm.class);
        getBalanceManager().deleteBalance(abstractInfoBalanceForm.getBalanceId());
        return mapping.findForward(getManageAction());
    }

    protected void preloadFilters(AbstractInfoBalanceForm abstractInfoBalanceForm, User loggedInUser)
            throws DatabaseException, InitException, BillException {
        abstractInfoBalanceForm.getScrollBoxBalanceOwner().bind(getBalanceManager().getAllOwner(loggedInUser));
        FilterSet filterSet = abstractInfoBalanceForm.getTransactionsTable().getFilterSet();

        ScrollBox scrollBoxTransType = (ScrollBox) filterSet.getFilter("by_transaction_type").getParameter("type").getControl();
        scrollBoxTransType.bind(new TransactionManager().getAllTransactionType());
    }

    protected void preloadTransSumma(AbstractInfoBalanceForm form) throws Throwable {
        Table table = form.getTransactionsTable();
        FilterWrapper[] wrappers = table.getQuery().getFilters();
        CriterionWrapper[] criterions = table.getQuery().getCriterions();

        String entityClass = table.getQuery().getEntityClass();
        TransactionManager.CalculationResult result = new TransactionManager().calculateTransactions(wrappers, entityClass, criterions);


        form.getEditBoxAllTransaction().setValue(result.getTransactionCaunt().toString());
        form.getEditBoxSuccessTransaction().setValue(result.getTransactionSuccessCount().toString());
        form.getEditBoxAllTransAmount().setValue(result.getTransactionAmount().toString());
        form.getEditBoxSuccessTransAmount().setValue(result.getTransactionSuccessAmount().toString());

    }

    private List<Object> preloadTableDependInfo(AbstractInfoBalanceForm form) throws Throwable {
        Table table = form.getTransactionsTable();
        FilterWrapper[] wrappers = table.getQuery().getFilters();
        String[] names = {"by_FromDate", "by_ToDate", "by_client_amount_from", "by_client_amount_to",
                            "by_billnum_like", "by_clinum_like"
        };

        String[] paramNames = {"date", "date", "from_amount", "to_amount",
                            "billnum", "clinum"
        };

        List<Object> parameters = new ArrayList<Object>();

//        def params init
        GregorianCalendar prototype = new GregorianCalendar();
        GregorianCalendar calFrom = new GregorianCalendar(
                                        prototype.get(Calendar.YEAR),
                                        prototype.get(Calendar.MONTH),
                                        prototype.get(Calendar.DATE),
                                        0,
                                        0
        );
        Date timeFrom = calFrom.getTime();
        Calendar calTo = getInstance();
        Date timeTo = calTo.getTime();

        parameters.add(timeFrom);
        parameters.add(timeTo);
        parameters.add(Float.MIN_VALUE);
        parameters.add(Float.MAX_VALUE);
        parameters.add("");
        parameters.add("");


        for (int i = 0; i < names.length; i++)
            for (FilterWrapper wrapper : wrappers) {
                if (wrapper.getName().equals(names[i])){
                    parameters.set(i, wrapper.getParamValue(paramNames[i]));
                }

            }

        return parameters;
    }


    private QueryParameterWrapper[] getTransFilters(AbstractInfoBalanceForm form) {
        Date timeFrom = form.getTimeSelectorTransFrom().getTime();
        Date timeTo = form.getTimeSelectorTransTo().getTime();
        Long id = form.getBalanceId();
        QueryParameterWrapper[] parameterWrapper = new QueryParameterWrapper[]{
                new QueryParameterWrapper("timeFrom", timeFrom),                          
                new QueryParameterWrapper("timeTo", timeTo),
                new QueryParameterWrapper("id", id),
        };

        return parameterWrapper;
    }

    private QueryParameterWrapper[] getDependDataFilters(AbstractInfoBalanceForm form, List<Object> parameters, Long min, Long max) {
        Date timeFrom = (Date) parameters.get(0);
        Date timeTo = (Date) parameters.get(1);
        Long id = form.getBalanceId();
        Float amountMin = Float.parseFloat(parameters.get(2).toString());
        Float amountMax = Float.parseFloat(parameters.get(3).toString());
        String transGuid = ("%" + parameters.get(4).toString() + "%");  //'%' is for 'like' in SQL
        String cliTransNum = ("%" + parameters.get(5).toString() + "%");  //'%' is for 'like' in SQL
        List<QueryParameterWrapper> parameterWrapper = new ArrayList<QueryParameterWrapper>();
        parameterWrapper.add(new QueryParameterWrapper("timeFrom", timeFrom));
        parameterWrapper.add(new QueryParameterWrapper("timeTo", timeTo));
        parameterWrapper.add(new QueryParameterWrapper("id", id));
        parameterWrapper.add(new QueryParameterWrapper("amountMin", amountMin));
        parameterWrapper.add(new QueryParameterWrapper("amountMax", amountMax));
        parameterWrapper.add(new QueryParameterWrapper("transGuid", transGuid));
        parameterWrapper.add(new QueryParameterWrapper("cliTransNum", cliTransNum));
        if ((min!=null)&&(max!=null)){
            parameterWrapper.add(new QueryParameterWrapper("statusMin", min));
            parameterWrapper.add(new QueryParameterWrapper("statusMax", max));
        }
        QueryParameterWrapper[] parameterWrapperArray =  new QueryParameterWrapper[parameterWrapper.size()];
        for (int i = 0; i< parameterWrapper.size(); i++){
            parameterWrapperArray[i] = parameterWrapper.get(i);
        }
        return parameterWrapperArray;
    }

    public QueryParameterWrapper[] getDefaultTransFilters(AbstractInfoBalanceForm form) {
        Long id = form.getBalanceId();

        GregorianCalendar prototype = new GregorianCalendar();
        GregorianCalendar calFrom = new GregorianCalendar(
                                        prototype.get(Calendar.YEAR),
                                        prototype.get(Calendar.MONTH),
                                        prototype.get(Calendar.DATE),
                                        0,
                                        0
        );

        Date timeFrom = calFrom.getTime();
        form.getTimeSelectorTransFrom().setTime(timeFrom);

        Calendar calTo = getInstance();
        Date timeTo = calTo.getTime();
        form.getTimeSelectorTransTo().setTime(timeTo);

        QueryParameterWrapper[] parameterWrapper = new QueryParameterWrapper[]{
                new QueryParameterWrapper("timeFrom", timeFrom),
                new QueryParameterWrapper("timeTo", timeTo),
                new QueryParameterWrapper("id", id),
        };
        return parameterWrapper;
    }

    //-------------------------refreshing tables methods-----------------------------------------------------------
    public ActionForward refreshTransactionsTable(ActionMapping mapping,
                                                  ActionForm form,
                                                  HttpServletRequest request,
                                                  HttpServletResponse response) throws Throwable {
        AbstractInfoBalanceForm abstractInfoBalanceForm = (AbstractInfoBalanceForm) verifyForm(form, AbstractInfoBalanceForm.class);
//        abstractInfoBalanceForm.getTransactionsTable().refresh();
        preloadTransSumma(abstractInfoBalanceForm);
        if (abstractInfoBalanceForm instanceof InfoSellerBalanceForm) { //SD: temporary workaround
            preloadTableDependData(abstractInfoBalanceForm);

            final InfoSellerBalanceForm sellerBalanceForm = (InfoSellerBalanceForm) abstractInfoBalanceForm;
            final Operator operator;
//            sellerBalanceForm.getTransactionsTable().setCriterionFactory(null);
//            if (!sellerBalanceForm.getScrollBoxOperator().getSelectedKey().equals(ScrollBox.NULL_KEY)) {
//                operator = new OperatorManager().getOperator(new Long(sellerBalanceForm.getScrollBoxOperator().getSelectedKey()));
//                CriterionFactory criterionFactory = new CriterionFactory() {
//                    public CriterionWrapper createCriterion() {
//                        if (!sellerBalanceForm.getScrollBoxOperator().getSelectedKey().equals(ScrollBox.NULL_KEY)) {
//                            return Restrictions.eq("clientTransaction.operator", operator);
//                        } else {
//                            return null;
//                        }
//                    }
//                };
//                sellerBalanceForm.getTransactionsTable().setCriterionFactory(criterionFactory);
//            }
        }
        abstractInfoBalanceForm.getTransactionsTable().refresh();
        return mapping.findForward(getInfoPage());
    }

    private void initFinancialActivTabDef(AbstractInfoBalanceForm form) throws DatabaseException, ClassNotFoundException {
        QueryParameterWrapper[] defaultTransFilters = getDefaultTransFilters(form);
        Table table = form.getTransactionsTable();
        String entityClass = table.getQuery().getEntityClass();
        final String noTransactions = "0.00";

        if (defaultTransFilters[2].getValue() != null) {
            TransactionManager.CurrentAmountResult calcCurAmountRes = new TransactionManager().calculateCurrentAmount(entityClass, "TransCurrentAmount", defaultTransFilters);
            Float CurAmmount = calcCurAmountRes.getCurrentTransaction();
            form.getEditBoxTransactionCurrentAmmount().setValue(CurAmmount == null ? noTransactions : CurAmmount.toString());

            TransactionManager.IncreaseBallanceResult calcIncRes = new TransactionManager().calculateBallanceIncrease(entityClass, "IncrsBalanceSum", defaultTransFilters);
            Float IncSum = calcIncRes.getIncreaseBallanceSum();
            form.getEditBoxTransactionIncreaceAmount().setValue(IncSum == null ? noTransactions : IncSum.toString());

            TransactionManager.TransCalculationResult calcRes = new TransactionManager().calculateBallanceTransactions(entityClass, "NomTransferReward", defaultTransFilters);

            Float nominalTransSum = calcRes.getNominalTransSum();
            form.getEditBoxNominalTransactionAmount().setValue(nominalTransSum == null ? noTransactions : nominalTransSum.toString());

            Float transferSum = calcRes.getTransferSum();
            form.getEditBoxTransferAmount().setValue(transferSum == null ? noTransactions : transferSum.toString());

            Float awardSum = calcRes.getAwardSum();
            form.getEditBoxTransactionAward().setValue(awardSum == null ? noTransactions : awardSum.toString());
        }
    }

    private void preloadTableDependData(AbstractInfoBalanceForm form) throws Throwable {
        Table table = form.getTransactionsTable();
        String entityClass = table.getQuery().getEntityClass();
        List<Object> parameters = preloadTableDependInfo(form);
        QueryParameterWrapper[] wrappers = getDependDataFilters(form, parameters, null, null);
        final String noTransactions = "0.00";

        TransactionManager.DependTransCalcResult calcAllCount = new TransactionManager().calculateAllCount(entityClass, "allCount", wrappers);
        Long allCount = calcAllCount.getAllTransCount();
        form.getEditBoxAllCount().setValue(allCount == null ? noTransactions : allCount.toString());

        TransactionManager.DependTransCalcResult calcDebet = new TransactionManager().calculateDebet(entityClass, "debetTransactions", wrappers);
        Float debetSum = calcDebet.getDebetTransSum();
        Long debetCount = calcDebet.getDebetTransCount();
        form.getEditBoxDebetSum().setValue(debetSum == null ? noTransactions : debetSum.toString());
        form.getEditBoxDebetCount().setValue(debetCount == null ? noTransactions : debetCount.toString());

        wrappers = getDependDataFilters(form, parameters, 0L, 0L);
        TransactionManager.DependTransCalcResult calcCreditOkCount = new TransactionManager().calculateCreditCount(entityClass, "creditCount", wrappers);
        Long creditOkCount = calcCreditOkCount.getCreditTransCount();
        form.getEditBoxCreditOkCount().setValue(creditOkCount == null ? noTransactions : creditOkCount.toString());

        TransactionManager.DependTransCalcResult calcCreditOkSum = new TransactionManager().calculateCreditSum(entityClass, "nomCreditTransactions", wrappers);
        Float creditOkSum = calcCreditOkSum.getCreditNomTransSum();
        form.getEditBoxCreditOkSum().setValue(creditOkSum == null ? noTransactions : creditOkSum.toString());

        TransactionManager.DependTransCalcResult calcAwardOkSum = new TransactionManager().calculateAward(entityClass, "transactionAward", wrappers);
        Float awardOkSum = calcAwardOkSum.getAwardTransSum();
        form.getEditBoxAwardOkSum().setValue(awardOkSum == null ? noTransactions : awardOkSum.toString());

        wrappers = getDependDataFilters(form, parameters, 1L, Long.MAX_VALUE);
        TransactionManager.DependTransCalcResult calcCreditErrCount = new TransactionManager().calculateCreditCount(entityClass, "creditCount", wrappers);
        Long creditErrCount = calcCreditErrCount.getCreditTransCount();
        form.getEditBoxCreditErrCount().setValue(creditErrCount == null ? noTransactions : creditErrCount.toString());

        TransactionManager.DependTransCalcResult calcCreditErrSum = new TransactionManager().calculateCreditSum(entityClass, "nomCreditTransactions", wrappers);
        Float creditErrSum = calcCreditErrSum.getCreditNomTransSum();
        form.getEditBoxCreditErrSum().setValue(creditErrSum == null ? noTransactions : creditErrSum.toString());

        TransactionManager.DependTransCalcResult calcAwardErrSum = new TransactionManager().calculateAward(entityClass, "transactionAward", wrappers);
        Float awardErrSum = calcAwardErrSum.getAwardTransSum();
        form.getEditBoxAwardErrSum().setValue(awardErrSum == null ? noTransactions : awardErrSum.toString());
    }

}
