package com.tmx.as.entities.mobipay.globalmoney;

import com.tmx.as.base.BasicEntity;

import java.io.Serializable;
import java.util.Date;

public class GlobalMoneyTransaction extends BasicEntity
{
    private Integer GMTransactionId;
    private Date requestTime;
    private Date responseTime;

    private String billTransactionNum;
    private String GMTransactionNum;
    private String amount;
    private String account;
    private String terminal;

    private String billServiceCode;
    private String GMServiceCode;
    private String commission;

    private String status;
    private String voucher;
    private String voucher_expire;
    private String receipt_no;

    public Integer getGMTransactionId() {
        return GMTransactionId;
    }

    public void setGMTransactionId(Integer GMTransactionId) {
        this.GMTransactionId = GMTransactionId;
    }

    public Date getRequestTime() {
        return requestTime;
    }

    public void setRequestTime(Date requestTime) {
        this.requestTime = requestTime;
    }

    public Date getResponseTime() {
        return responseTime;
    }

    public void setResponseTime(Date responseTime) {
        this.responseTime = responseTime;
    }

    public String getBillTransactionNum() {
        return billTransactionNum;
    }

    public void setBillTransactionNum(String billTransactionNum) {
        this.billTransactionNum = billTransactionNum;
    }

    public String getGMTransactionNum() {
        return GMTransactionNum;
    }

    public void setGMTransactionNum(String GMTransactionNum) {
        this.GMTransactionNum = GMTransactionNum;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getBillServiceCode() {
        return billServiceCode;
    }

    public void setBillServiceCode(String billServiceCode) {
        this.billServiceCode = billServiceCode;
    }

    public String getGMServiceCode() {
        return GMServiceCode;
    }

    public void setGMServiceCode(String GMServiceCode) {
        this.GMServiceCode = GMServiceCode;
    }

    public String getCommission() {
        return commission;
    }

    public void setCommission(String commission) {
        this.commission = commission;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getVoucher() {
        return voucher;
    }

    public void setVoucher(String voucher) {
        this.voucher = voucher;
    }

    public String getVoucher_expire() {
        return voucher_expire;
    }

    public void setVoucher_expire(String voucher_expire) {
        this.voucher_expire = voucher_expire;
    }

    public String getReceipt_no() {
        return receipt_no;
    }

    public void setReceipt_no(String receipt_no) {
        this.receipt_no = receipt_no;
    }

    public String getTerminal() {
        return terminal;
    }

    public void setTerminal(String terminal) {
        this.terminal = terminal;
    }

    public Serializable getId() {
        return getGMTransactionId();
    }

    public int getValid() {
        return valid;
    }

    public void setValid(int valid) {
        this.valid = valid;
    }

}