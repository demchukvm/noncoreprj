package com.tmx.beng.iso8583Gate.processors;

import com.tmx.beng.iso8583Gate.pdu.RuntimeMessage;

/**
 */
public interface ISOProcessor {
    public RuntimeMessage process(RuntimeMessage requestMsg) throws ProcessingException;
}
