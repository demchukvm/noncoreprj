package com.tmx.beng.base;

import com.tmx.beng.base.memory.InMemoryObject;

import java.util.List;
import java.util.Date;

/**

 */
public interface Transaction extends InMemoryObject  {
    public String getTransactionId();
    public void addTask(Task task);
    public void close();
    public List getTasks();
    public void finish();
    public Date getRegistrationDate();

    public void setCliTransactionNum(String s);

}

