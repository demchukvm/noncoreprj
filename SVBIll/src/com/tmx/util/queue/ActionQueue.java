package com.tmx.util.queue;

import java.io.PrintStream;

/**
 * ActionQueue postpones execution of ActionQueueElement's run()-method defined procedure.
 * Activities are executed by working threads (queue handlers), the maximum number of which are defined
 * at the queue creation time. It is recommended to keep that number equal to the number of system-available CPUs.
 */
public class ActionQueue {

    private static int groupNumber = 0;
    private int threadId = 0;

    private TimeCounter tc;
    private static Thread delayedActionsProcessorThread;
    private static final String WAITERS = "WAITERS";
    private static final String ACTIONS = "ACTIONS";

    public String getName() {
        return name;
    }

    private String name;

    public TimeCounter getTimeCounter() {
        return tc;
    }

    /**
     * Constructs ActionQueue object.
     *
     * @param name                     work queue name.
     * @param initialQueueHandlerCount how many PooledThreads to instantiate and run on startup.
     * @param maximumQueueHandlerCount maximum thread limit.
     */
    public ActionQueue(String name, int initialQueueHandlerCount, int maximumQueueHandlerCount) {
        this.name = name = (name == null ? "ActionQueue #" + groupNumber : name);
        tc = new TimeCounter(
                STAT,
                Long.MAX_VALUE,
                10000
        ).setReportPrefix(name).setMaximumNotFinishedActivitiesToDescribe(2).setSilent(false);

        _first = null;
        _numWaiters = 0;
        _threadCount = 0;
        firstLast = new ActionQueueElement[2];
        _actionCount = 0;
        _lastCollectionTime = System.currentTimeMillis();
        _threadMaxIdle = 0L;

        tg = new ThreadGroup(name);
        tg.setMaxPriority(1);

        setThreadMin(initialQueueHandlerCount);
        setThreadMax(maximumQueueHandlerCount);
        startMinThreads();
    }

    private ThreadGroup tg;

    /**
     * Constructs ActionQueue object.
     *
     * @param initialQueueHandlerCount how many PooledThreads to instantiate and run on startup.
     * @param maximumQueueHandlerCount maximum thread limit.
     */
    public ActionQueue(int initialQueueHandlerCount, int maximumQueueHandlerCount) {
        this(null, initialQueueHandlerCount, maximumQueueHandlerCount);
    }

    public ActionQueue() {
        this(1, 1);
    }

    public ActionQueue setSilent(boolean silent) {
        this.tc.setSilent(silent);
        return this;
    }

    public ActionQueue setReportPeriod(long reportIntervalIterations, long reportIntervalMillis) {
        this.tc.setReportPeriod(reportIntervalIterations, reportIntervalMillis);
        return this;
    }

    private final static int ENQUEUE = 0;
//    private final static int DEQUEUE = 1;
    private final static int PASSED = 2;
    private final static int RUN_ACTION = 3;
    private final static int DEQUEUE_FAIL = 4;
    private final static int WAIT = 5;
//    private final static int NOTIFY_WAITED = 6;
    //
    private final static String[] STAT = new String[]{
            "ENQUEUE",
            "DEQUEUE",
            "PASSED",
            "RUN_ACTION",
            "DEQUEUE_FAIL",
            "WAIT",
            "NOTIFY_WAITED"
    };

    class PoolProcessor extends Thread implements Versionable {
        PoolProcessor next;
        PoolProcessor prev;
        private boolean _exit;
        private boolean _idle;
        private int _idleTime;
        private int version;

        public synchronized int getVersion() {
            return version;
        }

        public synchronized void incVersion() {
            version++;
        }

        PoolProcessor() {
            super(tg, ActionQueue.this.getName() + ": T" + (++threadId) + '/' + getThreadMax());
            setDaemon(true);
            next = null;
            prev = null;
            _exit = false;
            _idle = true;
            _idleTime = 0;
            add(this);
        }

        public void run() {
            do {
                synchronized (this) {
                    _idle = true;
                }
                final ActionQueueElement action = dequeue(this);
                if (action != null) {
                    synchronized (this) {
                        _idle = false;
                    }
                    exec(action, this);
                }
                if (_exit) {
                    break;
                }
            } while (true);
            remove(this);
        }

        public synchronized int idleTime(int i) {
            if (_idle) {
                _idleTime += i;
            }
            return _idleTime;
        }

        public synchronized boolean isIdle() {
            return _idle;
        }
    }

    protected static final String IS_TMOUT_ON = "tmout";

    interface Versionable {
        int getVersion();
        void incVersion();
    }

    private class VersionableImpl implements Versionable {
        private int version;

        public synchronized int getVersion() {
            return version;
        }

        public synchronized void incVersion() {
            version++;
        }

    }

    private void exec(ActionQueueElement action, Versionable v) {
        try {
            action.setPoolProcessor(v);
            //
            boolean toCount = action.canBeCounted();
            Object t = null;
            if (toCount) {
                t = tc.start(RUN_ACTION);
            }
            //
            final long timeoutTimestamp = action.getExecutionTimeoutDeadlineTimestamp();
            final Versionable poolProcessor = action.poolProcessor;
            String tmout = System.getProperty(IS_TMOUT_ON);
            if (tmout != null && tmout.length() > 0 && timeoutTimestamp > 0) {
                long toWait = action.timeLeft();
                if (toWait < 0) {
                    if (poolProcessor == v) {
                        action.onTimeout();
                    }
                    v.incVersion();
                }
                else {
                    if (poolProcessor == v) {
                        try {
                            action.setWaitingPoolProcessorVersion(v.getVersion());
                            enqueue(action);
                            synchronized (v) {
                                if (action.waitingPoolProcessorVersion == v.getVersion()) {
                                    // just enqueued action was not done
//                                                System.out.println("toWait="+toWait);
                                    v.wait(toWait + 1);
                                }
                                v.incVersion();
                            }
                            // "notification" or "timeout" end of wait here
                            ActionQueueElement aqe = removeActivityQueueElementFromQueue(action);
                            if (aqe == action) {
                                // not done, and will not be
                                action.onTimeout();
                            } else if (action.waitingPoolProcessorVersion != v.getVersion()) {
                                // may be in action now
                                action.onTimeout();
                            } else {
                                // ok, action done in time
                            }
                        } finally {
                            action.setWaitingPoolProcessorVersion(-1);
                        }
                    }
                    else if (poolProcessor != null) {
                        // this action is waiting for notification
                        boolean run = false;
                        synchronized (poolProcessor) {
                            if (poolProcessor.getVersion() == action.waitingPoolProcessorVersion) {
                                run = true;
                            }
                        }
                        if (run && action.timeLeft() >= 0) {
                            action.run();
                            synchronized (poolProcessor) {
                                if (poolProcessor.getVersion() == action.waitingPoolProcessorVersion) {
                                    action.waitingPoolProcessorVersion++;
                                    poolProcessor.notify();
                                }
                            }
                        }
                    }
                }
            } else {
                action.run();
                v.incVersion();
            }
            //
            if (toCount) {
                tc.end(t); //RUN_ACTION
            }
        } catch (Throwable throwable) {
            if (getTimeCounter() != null && getTimeCounter().getReportStream() != null) {
                getTimeCounter().getReportStream().print(String.valueOf(throwable));
            }
            throwable.printStackTrace();
            if (throwable instanceof ThreadDeath) {
                throw (ThreadDeath) throwable;
            }
        } finally {
            action.unsetPoolProcessor(v);
        }
    }

    private PoolProcessor _first;
    private int _numWaiters, _threadCount, _actionCount;
    private ActionQueueElement[] firstLast;
    private long _lastCollectionTime;
    protected int _threadMax, _threadMin;
    protected long _threadMaxIdle;

    public int getThreadMaxIdle() {
        return (int) (_threadMaxIdle / 1000L);
    }

    public void setThreadMaxIdle(int i) {
        if (i < 0) {
            throw new RuntimeException("Invalid negative ThreadMaxIdle value for ActionQueue");
        } else {
            _threadMaxIdle = i * 1000;
        }
    }

    public int getThreadMax() {
        return _threadMax;
    }

    public synchronized void setThreadMax(int i) {
        if (i < 0) {
            throw new RuntimeException("Invalid negative ThreadMax value for ActionQueue");
        }
        _threadMax = i;
        if (_threadMax != 0 && _threadMax < _threadMin) {
            _threadMax = _threadMin;
        }
    }

    public int getThreadMin() {
        return _threadMin;
    }

    public synchronized void setThreadMin(int i) {
        if (i < 0) {
            throw new RuntimeException("Invalid negative ThreadMin value for ActionQueue");
        }
        _threadMin = i;
        if (getThreadMax() != 0 && _threadMin > getThreadMax()) {
            _threadMin = getThreadMax();
        }
    }

    public synchronized void startMinThreads() {
        int j = _threadMin - _threadCount;
        if (j > 0) {
            for (int k = 0; k < j; k++) {
                (new PoolProcessor()).start();
            }
        }
    }

    public synchronized void collect(long l) {
        if (_threadMaxIdle == 0L) {
            return;
        }
        int i = (int) (l - _lastCollectionTime);
        _lastCollectionTime = l;
        PoolProcessor poolProcessor = _first;
        for (int j = _threadCount; poolProcessor != null && j > _threadMin;) {
            if ((long) poolProcessor.idleTime(i) >= _threadMaxIdle) {
                PoolProcessor poolProcessor1 = poolProcessor;
                poolProcessor1._exit = true;
                poolProcessor1.interrupt();
                poolProcessor = poolProcessor1.next;
                j--;
            } else {
                poolProcessor = poolProcessor.next;
            }
        }

    }

    private synchronized void add(PoolProcessor poolProcessor) {
        if (_first != null) {
            _first.prev = poolProcessor;
            poolProcessor.next = _first;
            _first = poolProcessor;
        } else {
            _first = poolProcessor;
        }
        _threadCount++;
    }

    private synchronized void remove(PoolProcessor poolProcessor) {
        if (_first == poolProcessor) {
            _first = poolProcessor.next;
        }
        if (poolProcessor.prev != null) {
            poolProcessor.prev.next = poolProcessor.next;
        }
        if (poolProcessor.next != null) {
            poolProcessor.next.prev = poolProcessor.prev;
        }
        _threadCount--;
    }

    public final synchronized void die(PoolProcessor poolProcessor) {
        if (getThreadMax() != 0 && _threadCount > getThreadMax()) {
            poolProcessor._exit = true;
        }
    }

    private static void addLastAction(ActionQueueElement[] firstLast, ActionQueueElement e) {
        if (firstLast[1] == null) {
            firstLast[0] = firstLast[1] = e;
            e.prev = e.next = null;
        } else {
            firstLast[1].next = e;
            e.prev = firstLast[1];
            e.next = null;
            firstLast[1] = e;
        }
    }

    private static ActionQueueElement removeFirstAction(ActionQueueElement[] firstLast) {
        if (firstLast[1] == null) {
            return null;
        }
        else {
            ActionQueueElement e = firstLast[0];
            firstLast[0] = firstLast[0].next;
            if (firstLast[0] == null) {
                firstLast[1] = null;
            } else {
                firstLast[0].prev = null;
            }
            e.prev = e.next = null;
            return e;
        }
    }

    private static ActionQueueElement removeAction(ActionQueueElement[] firstLast, ActionQueueElement e) {
        if (firstLast[0] == e) { //head
            ActionQueueElement next = e.next;
            if (next != null) {
                next.prev = null;
            }
            firstLast[0] = next;
            if (firstLast[0] == null) {
                firstLast[1] = null;
            }
        }
        else if (firstLast[1] == e) { //tail
            ActionQueueElement prev = e.prev;
            prev.next = null;
            firstLast[1] = prev;
        }
        else { // middle
            ActionQueueElement prev = e.prev;
            ActionQueueElement next = e.next;
            prev.next = next;
            next.prev = prev;
        }
        e.prev = e.next = null;
        return e;
    }

    private final static ActionQueueElement[] delayedFirstLast = new ActionQueueElement[2];
    private static int delayedActionCount;

    static {
        (delayedActionsProcessorThread = new DelayedActionsProcessorThread()).start();
    }

    private static ObjectPool delayedActions = new ObjectPool("delayedActions", new ObjectPoolElement() {
        public Object newPoolObjectInstance() {
            return new DelayedAction();
        }
    });

    public void enqueueDelayed(ActionQueueElement work, long millisToDelay) {
        if (millisToDelay == 0) {
            enqueue(work);
        } else {
            synchronized (delayedFirstLast) {
                if (millisToDelay < ((DelayedActionsProcessorThread) delayedActionsProcessorThread).getMinSleepTime()) {
                    delayedActionsProcessorThread.interrupt();
                }
                delayedFirstLast.notify();
                DelayedAction da = (DelayedAction) delayedActions.getObject();
                da.recycle(this, work);
                addLastAction(
                        delayedFirstLast,
                        da.setEnqueueTime(System.currentTimeMillis() + millisToDelay)
                );
                delayedActionCount++;
            }
        }
    }

    private static class DelayedAction extends ActionQueueElement {
        private ActionQueue q;
        private ActionQueueElement e;

        public void recycle(ActionQueue q, ActionQueueElement e) {
            this.q = q;
            this.e = e;
        }

        public void run() {
            try {
                q.enqueue(e);
            } finally {
                q = null;
                e = null;
                delayedActions.returnObject(this);
            }
        }
    }

    private synchronized ActionQueueElement removeActivityQueueElementFromQueue(ActionQueueElement e) {
        boolean inQueue = e == firstLast[0] || e.prev != null || e.next != null;
        if (inQueue) {
            return removeAction(firstLast, e);
        } else {
            return null;
        }
    }

    private synchronized ActionQueueElement dequeue(PoolProcessor pw) {
        try {
            ActionQueueElement action;
            _numWaiters++;
            tc.set(WAITERS, _numWaiters);
            while (_actionCount == 0) {
                Object wchkPnt = tc.start(WAIT);
                try {
                    this.wait();
                }
                catch (InterruptedException interruptedexception) {
                }
                finally {
                    tc.end(wchkPnt);
                }
            }
            _numWaiters--;
            tc.set(WAITERS, _numWaiters);
            //
            // Seek dequeueable action
            //
            for (int i = 0; i < _actionCount; ++i) {
                action = removeFirstAction(firstLast);
                if (action != null) {
                    if (action.canBeDequeued()) {
                        _actionCount--;
                        tc.end(action); //PASSED
                        tc.set(ACTIONS, _actionCount);
                        action.setPoolProcessor(pw);
                        return action;
                    } else {
                        tc.count(DEQUEUE_FAIL);
                        addLastAction(firstLast, action);
                    }
                }
            }
            return null;
        } finally {
            this.notifyAll();
        }
    }

    public void execute(ActionQueueElement action) {
        VersionableImpl v = new VersionableImpl();
        action.setEnqueueTime(System.currentTimeMillis());
        exec(action, v);
    }

    public boolean enqueue(ActionQueueElement action) {
        if (action == null || !action.canBeEnqueued()) {
            return false;
        }
        action.setEnqueueTime(System.currentTimeMillis());
        tc.start(PASSED, action);

        Object chk = tc.start(ENQUEUE);

        PoolProcessor poolProcessor = null;
        synchronized (this) {
            _actionCount++;
            addLastAction(firstLast, action);
            int threadMax = getThreadMax();
            if (_actionCount > _numWaiters && (threadMax == 0 || _threadCount < threadMax)) {
                poolProcessor = new PoolProcessor();
            }
            if (_numWaiters > 0) {
//                tc.count(NOTIFY_WAITED);
                this.notifyAll();
            }
        }

        tc.set(ACTIONS, _actionCount);
        if (poolProcessor != null) {
            poolProcessor.start();
            tc.set("THREADS", _threadCount);
        }

        tc.end(chk);
        return true;
    }

    public int threadsAllocated() {
        return _threadCount;
    }

    public int inUseThreads() {
        int i = 0;
        for (PoolProcessor poolProcessor = _first; poolProcessor != null; poolProcessor = poolProcessor.next) {
            if (!poolProcessor.isIdle()) {
                i++;
            }
        }
        return i;
    }

    public synchronized int actionCount() {
        return _actionCount;
    }

    public int idleThreads() {
        return _numWaiters;
    }

    public int idledTimeoutThreads() {
        if (_threadMaxIdle == 0L) {
            return 0;
        }
        int i = 0;
        for (PoolProcessor poolProcessor = _first; poolProcessor != null; poolProcessor = poolProcessor.next) {
            if ((long) poolProcessor._idleTime >= _threadMaxIdle) {
                i++;
            }
        }
        return i;
    }

    public ActionQueue setReportStream(PrintStream reportStream) {
        tc.setReportStream(reportStream);
        return this;
    }

    public void printStatistics(PrintStream pw) {
        tc.report(pw);
    }

    public synchronized void close() {
        tc.setSilent(true);

        for (PoolProcessor poolProcessor = _first; poolProcessor != null;) {
            PoolProcessor poolProcessor1 = poolProcessor;
            poolProcessor = poolProcessor1.next;
            poolProcessor1._exit = true;
            poolProcessor1.interrupt();
        }
        _first = null;
    }

    private static class DelayedActionsProcessorThread extends Thread {
        long minSleepTime = Long.MAX_VALUE;

        public long getMinSleepTime() {
            synchronized (delayedFirstLast) {
                return minSleepTime;
            }
        }

        public DelayedActionsProcessorThread() {
            setName("Delayed Actions Processor Thread");
            setDaemon(true);
        }

        public void run() {
            while (true) {
                try {
                    synchronized (delayedFirstLast) {
                        long currentTime = System.currentTimeMillis();
                        int iMax = delayedActionCount;
                        for (int i = 0; i < iMax; ++i) {
                            ActionQueueElement actionQueueElement = removeFirstAction(delayedFirstLast);
                            long timeLeft = actionQueueElement.getEnqueueTime() - currentTime;
                            if (timeLeft <= 0) {
                                delayedActionCount--;
                                actionQueueElement.run();
                            } else {
                                minSleepTime = timeLeft < minSleepTime ? timeLeft : minSleepTime;
                                addLastAction(delayedFirstLast, actionQueueElement);
                            }
                        }
                        if (minSleepTime == Long.MAX_VALUE) {
                            delayedFirstLast.wait();
                        }
                    }

                    if (minSleepTime != Long.MAX_VALUE) {
                        try {
                            sleep(minSleepTime);
                        } catch (InterruptedException e) {
                        }
                    }
                } catch (InterruptedException t) {
//                    t.printStackTrace();
                } catch (Throwable t) {
                    t.printStackTrace();
                }
            }
        }
    }

    public static void main(String[] args) {
        ActionQueue aq = new ActionQueue("test", 1, 1);
        aq.enqueue(new ActionQueueElement() {
            public String toString() {
                return "AQE #1";
            }

            public void run() {
/*
                try {
                    Thread.sleep(20);
                    //System.out.println("Done!");
                } catch (InterruptedException e) {
                    e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                }
*/
            }

            public long getExecutionTimeoutDeadlineTimestamp() {
                return getEnqueueTime() + 50;
            }

            public void onTimeout() {
                System.out.println("AQE #1: TIMEOUT detected!!");
                Thread.dumpStack();
            }
        });
        //
        aq.execute(new ActionQueueElement() {
            public String toString() {
                return "AQE #2";
            }

            public void run() {
/*
                try {
                    Thread.sleep(20);
                    //System.out.println("Done!");
                } catch (InterruptedException e) {
                    e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                }
*/
            }

            public long getExecutionTimeoutDeadlineTimestamp() {
                return getEnqueueTime() + 100;
            }

            public void onTimeout() {
                System.out.println("AQE #2: TIMEOUT detected!!");
                Thread.dumpStack();
            }
        });
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    /** To support inherited classes */

    protected TimeCounter getTc() {
        return tc;
    }

    protected static String getActions() {
        return ACTIONS;
    }

    protected int get_actionCount() {
        return _actionCount;
    }

    protected void dec_actionCount(){
        _actionCount--;
    }

    protected ActionQueueElement[] getFirstLast() {
        return firstLast;
    }


}
