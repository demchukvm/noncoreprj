package com.tmx.as.entities.bill.balance;

import com.tmx.as.entities.bill.terminal.Terminal;
import com.tmx.as.entities.bill.terminal_group.TerminalGroup;

/**
 */
public class TerminalGroupBalance extends AbstractBalance {
    private TerminalGroup terminalGroup;
    private Long terminalGroupId;

    public TerminalGroup getTerminalGroup() {
        return terminalGroup;
    }

    public void setTerminalGroup(TerminalGroup terminalGroup) {
        this.terminalGroup = terminalGroup;
    }

    public Long getTerminalGroupId() {
        return terminalGroupId;
    }

    public void setTerminalGroupId(Long terminalGroupId) {
        this.terminalGroupId = terminalGroupId;
    }
}
