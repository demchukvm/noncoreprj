package com.tmx.as.modules;

import com.tmx.gate.datastream.base.GateContext;
import com.tmx.util.InitException;
import com.tmx.as.scheduler.SchedulerContext;
import com.tmx.as.scheduler.SchedulerException;

import java.util.Properties;

/**
 * Created by IntelliJ IDEA.
 * User: shake
 * Date: 25.06.2007
 * Time: 15:56:19
 */
public class SchedulerModule extends AbstractModule{

    public void start(Properties props)throws ModuleException{
        try{
            SchedulerContext schContext = SchedulerContext.getInstance();
            schContext.reload();
            schContext.start();
        }catch(Throwable e){
            throw new ModuleException(e.getLocalizedMessage());
        }
    }

    public void shutdownGraceful(Properties props)throws ModuleException{
        SchedulerContext schContext = SchedulerContext.getInstance();
        schContext.stopScheduleThread();
    }

    public void shutdownAbort(Properties props)throws ModuleException{
        shutdownGraceful(props);
    }
}
