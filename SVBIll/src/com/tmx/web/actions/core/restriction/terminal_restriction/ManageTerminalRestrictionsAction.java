package com.tmx.web.actions.core.restriction.terminal_restriction;

import com.tmx.web.actions.core.restriction.AbstractManageRestrictionsAction;
import com.tmx.web.base.Roadmap;
import com.tmx.web.forms.core.restriction.terminal_restriction.ManageTerminalRestrictionsForm;
import com.tmx.web.forms.core.base.AbstractFormFactory;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForm;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class ManageTerminalRestrictionsAction extends AbstractManageRestrictionsAction {

    public static final String PARAMETER_OWNER_NAME = "ownerId";

    public ActionForward preloadForDelete(ActionMapping mapping,
                                          ActionForm form,
                                          HttpServletRequest request,
                                          HttpServletResponse response) throws Throwable {
        Roadmap roadmap = getRoadmap();
        roadmap.collectParameter("/core/message/confirmation", "leftMenuName", "restrictions");
        roadmap.setPageForward("confirmation", "onOk", "local:" + this.getName() + "#" + "deleteRestrictionAction");
        roadmap.setPageForward("confirmation", "onCancel", "local:" + this.getName() + "#" + "manageRestrictionsAction");
        return mapping.findForward("confirmation");
    }

    public ActionForward delete(ActionMapping mapping,
                              ActionForm form,
                              HttpServletRequest request,
                              HttpServletResponse response) throws Throwable {
        ManageTerminalRestrictionsForm restrictionsForm = (ManageTerminalRestrictionsForm) form;
        return defaultAction(mapping, form, request, response, null);
    }

    public ActionForward defaultAction(ActionMapping mapping, ActionForm form, HttpServletRequest request,
                                       HttpServletResponse response, String methodName) throws Throwable {
        ManageTerminalRestrictionsForm restForm = (ManageTerminalRestrictionsForm) form;
        AbstractFormFactory.getInstance(getLoggedInUser(request)).setUpForm(restForm);
        return super.defaultAction(mapping, form, request, response, methodName);
    }

    public ActionForward onCreateRestrictionAction(ActionMapping mapping,
                                       ActionForm form,
                                       HttpServletRequest request,
                                       HttpServletResponse response) throws Throwable {
        ManageTerminalRestrictionsForm restrictionsForm = (ManageTerminalRestrictionsForm) form;
        String selectedKey = restrictionsForm.getTerminalFilter().getKey();
        getRoadmap().collectParameter("/core/restriction/terminal_restriction/infoTerminalRestriction", PARAMETER_OWNER_NAME, selectedKey);
        return mapping.findForward("createRestrictionAction");
    }
}
