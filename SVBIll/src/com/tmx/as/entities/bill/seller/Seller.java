package com.tmx.as.entities.bill.seller;

import com.tmx.as.base.BasicEntity;
import com.tmx.as.entities.bill.address.Address;
import com.tmx.as.entities.bill.balance.SellerBalance;
import com.tmx.as.entities.bill.person.Person;
import com.tmx.as.entities.bill.processing.Processing;
import com.tmx.as.entities.bill.terminal_group.TerminalGroup;

import java.io.Serializable;
import java.util.Date;

/**
 */
public class Seller extends BasicEntity implements Comparable {
    private Long sellerId;
    private String code;
    private String name;
    private Date registrationDate;
    private String description;
    private Address physicalAddress;
    private Address legalAddress;
    private SellerBalance primaryBalance;
    private TerminalGroup defaultTerminalGroup;
    private Long defaultTerminalGroupId;
    private Processing processing;
    private Long processingId;
    private Boolean active;
    private String certificateNumber;
    private String inn;
    private String okpo;
    private Person director;
    private Person accountant;
//    contact data
    private String phone0;
    private String phone1;
    private String mobPhone0;
    private String mobPhone1;
    private String mail0;
    private String mail1;
    private String mail2;


    /** left margin of group set */
    private Integer leftMargin;
    /** right margin of group set */
    private Integer rightMargin;
    /** Identifier of tree to which this group belongs as node.
     * All groups under the same root group have the same rootId.
     * This is mandatory field. */
    private Long treeId;
    private Seller parentSeller;
    private Seller rootSeller;
    private Boolean root;


    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public Long getDefaultTerminalGroupId() {
        return defaultTerminalGroupId;
    }

    public void setDefaultTerminalGroupId(Long defaultTerminalGroupId) {
        this.defaultTerminalGroupId = defaultTerminalGroupId;
    }

    public Long getProcessingId() {
        return processingId;
    }

    public void setProcessingId(Long processingId) {
        this.processingId = processingId;
    }

    public Processing getProcessing() {
        return processing;
    }

    public void setProcessing(Processing processing) {
        this.processing = processing;
    }

    public TerminalGroup getDefaultTerminalGroup() {
        return defaultTerminalGroup;
    }

    public void setDefaultTerminalGroup(TerminalGroup defaultTerminalGroup) {
        this.defaultTerminalGroup = defaultTerminalGroup;
    }

    public Long getSellerId() {
        return sellerId;
    }

    public void setSellerId(Long sellerId) {
        this.sellerId = sellerId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Date getRegistrationDate() {
        return registrationDate;
    }

    public void setRegistrationDate(Date registrationDate) {
        this.registrationDate = registrationDate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Address getPhysicalAddress() {
        return physicalAddress;
    }

    public void setPhysicalAddress(Address physicalAddress) {
        this.physicalAddress = physicalAddress;
    }

    public Address getLegalAddress() {
        return legalAddress;
    }

    public void setLegalAddress(Address legalAddress) {
        this.legalAddress = legalAddress;
    }

    public SellerBalance getPrimaryBalance() {
        return primaryBalance;
    }

    public void setPrimaryBalance(SellerBalance primaryBalance) {
        this.primaryBalance = primaryBalance;
    }

    public Integer getLeftMargin() {
        return leftMargin;
    }

    public void setLeftMargin(Integer leftMargin) {
        this.leftMargin = leftMargin;
    }

    public Integer getRightMargin() {
        return rightMargin;
    }

    public void setRightMargin(Integer rightMargin) {
        this.rightMargin = rightMargin;
    }

    public Long getTreeId() {
        return treeId;
    }

    public void setTreeId(Long treeId) {
        this.treeId = treeId;
    }

    public Seller getParentSeller() {
        return parentSeller;
    }

    public void setParentSeller(Seller parentSeller) {
        this.parentSeller = parentSeller;
    }

    public Seller getRootSeller() {
        return rootSeller;
    }

    public void setRootSeller(Seller rootSeller) {
        this.rootSeller = rootSeller;
    }

    public Serializable getId() {
        return getSellerId();
    }

    public int getValid() {
        return valid;
    }

    public void setValid(int valid) {
        this.valid = valid;
    }

    public String getCertificateNumber() {
        return certificateNumber;
    }

    public void setCertificateNumber(String certificateNumber) {
        this.certificateNumber = certificateNumber;
    }

    public String getInn() {
        return inn;
    }

    public void setInn(String inn) {
        this.inn = inn;
    }

    public String getOkpo() {
        return okpo;
    }

    public void setOkpo(String okpo) {
        this.okpo = okpo;
    }

    public Person getAccountant() {
        return accountant;
    }

    public void setAccountant(Person accountant) {
        this.accountant = accountant;
    }

    public Person getDirector() {
        return director;
    }

    public void setDirector(Person director) {
        this.director = director;
    }

    public String getPhone0() {
        return phone0;
    }

    public void setPhone0(String phone0) {
        this.phone0 = phone0;
    }

    public String getPhone1() {
        return phone1;
    }

    public void setPhone1(String phone1) {
        this.phone1 = phone1;
    }

    public String getMobPhone0() {
        return mobPhone0;
    }

    public void setMobPhone0(String mobPhone0) {
        this.mobPhone0 = mobPhone0;
    }

    public String getMobPhone1() {
        return mobPhone1;
    }

    public void setMobPhone1(String mobPhone1) {
        this.mobPhone1 = mobPhone1;
    }

    public String getMail0() {
        return mail0;
    }

    public void setMail0(String mail0) {
        this.mail0 = mail0;
    }

    public String getMail1() {
        return mail1;
    }

    public void setMail1(String mail1) {
        this.mail1 = mail1;
    }

    public String getMail2() {
        return mail2;
    }

    public void setMail2(String mail2) {
        this.mail2 = mail2;
    }

    public Boolean getRoot() {
        return root;
    }

    public void setRoot(Boolean root) {
        this.root = root;
    }

    public int compareTo(Object o) {
        return this.getCode().compareToIgnoreCase(((Seller)o).getCode());
    }
}
