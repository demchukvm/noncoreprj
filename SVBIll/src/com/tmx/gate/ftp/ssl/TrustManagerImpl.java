package com.tmx.gate.ftp.ssl;

import java.security.cert.X509Certificate;
import javax.net.ssl.*;
import com.sun.net.ssl.*;


/** SSL Trust Manager */
class TrustManagerImpl implements com.sun.net.ssl.X509TrustManager {

  public TrustManagerImpl() {}


  public X509Certificate[] getAcceptedIssuers(){
    return null;
  }


  public void checkServerTrusted(X509Certificate ax509certificate[], String authType){
    return;
  }


  public void checkClientTrusted(X509Certificate ax509certificate[], String authType){
    return;
  }


  public boolean isServerTrusted(X509Certificate[] certs){
    return true;
  }

  public boolean isClientTrusted(X509Certificate[] certs){
    return true;
  }
  
}
