package com.tmx.web.actions.core.transactions.operator_balance_transaction;

import com.tmx.web.base.BasicDispatchedAction;
import com.tmx.web.forms.core.transaction.operator_balance_transaction.ManageOperatorBalanceTransactionsForm;
import com.tmx.web.controls.table.FilterSet;
import com.tmx.web.controls.ScrollBox;
import com.tmx.as.exceptions.DatabaseException;
import com.tmx.as.blogic.TransactionManager;
import com.tmx.as.base.FilterWrapper;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForm;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class ManageOperatorBalanceTransactionsAction extends BasicDispatchedAction {
    
    private static final String FORWARD_MANAGE_OPERATOR_TRANSACTIONS_PAGE = "manageOperatorTransactionsPage";

    public ActionForward defaultAction(ActionMapping mapping,
                                       ActionForm form,
                                       HttpServletRequest request,
                                       HttpServletResponse response,
                                       String methodName) throws Throwable {
        ManageOperatorBalanceTransactionsForm transactionsForm = (ManageOperatorBalanceTransactionsForm) form;
        transactionsForm.getOperatorTransactionsTable().refresh();
        preloadFilters(transactionsForm);
        setUpCalculationResult(transactionsForm);
        getRoadmap().collectParameter("/core/transactions/operator_transaction/infoOperatorTransaction", "onCancelAction", "manageOperatorTransactionsAction");
        return mapping.findForward(FORWARD_MANAGE_OPERATOR_TRANSACTIONS_PAGE);
    }

    private void setUpCalculationResult(ManageOperatorBalanceTransactionsForm form) throws ClassNotFoundException, DatabaseException {
        FilterWrapper[] filterWrappers = form.getOperatorTransactionsTable().getQuery().getFilters();
        String entityClassName = form.getOperatorTransactionsTable().getQuery().getEntityClass();
        TransactionManager.CalculationResult calcResult
                = new TransactionManager().calculateTransactions(filterWrappers, entityClassName);
        form.getEditBoxAllTransaction().setValue( calcResult.getTransactionCaunt().toString() );
        form.getEditBoxSuccessTransaction().setValue( calcResult.getTransactionSuccessCount().toString() );
        form.getEditBoxAllTransAmount().setValue( calcResult.getTransactionAmount().toString() );
        form.getEditBoxSuccessTransAmount().setValue( calcResult.getTransactionSuccessAmount().toString() );
    }

    private void preloadFilters(ManageOperatorBalanceTransactionsForm form) throws DatabaseException {
        FilterSet filterSet = form.getOperatorTransactionsTable().getFilterSet();
        ScrollBox scrollBoxOperator = (ScrollBox)filterSet.getFilter("by_operator").getParameter("operator").getControl();
        scrollBoxOperator.bind(new TransactionManager().getAllOperators());
        ScrollBox scrollBoxTransType = (ScrollBox)filterSet.getFilter("by_transaction_type").getParameter("type").getControl();
        scrollBoxTransType.bind(new TransactionManager().getAllTransactionType());

    }

}
