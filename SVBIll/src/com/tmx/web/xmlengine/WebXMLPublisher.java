package com.tmx.web.xmlengine;

import com.tmx.engines.xmlengine.XMLPublisher;
import com.tmx.engines.xmlengine.NodePrototype;
import org.w3c.dom.*;

/**Extension of XMLPublisher to generate web oriented XML.
 */
public interface WebXMLPublisher extends XMLPublisher {

    public Node addLink(Node parantNode, NodePrototype nodePrototype);
    public Node addGroup(Node parentNode, String groupName, String groupId);    
}
