package com.tmx.web.actions.core.reports;

import com.tmx.as.entities.bill.terminal.EquipmentType;
import com.tmx.as.entities.general.role.Role;
import jxl.format.CellFormat;
import jxl.write.WritableCell;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForm;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.tmx.web.forms.core.reports.ReportNetworksForm;
import com.tmx.web.forms.core.base.AbstractFormFactory;
import com.tmx.web.forms.core.tools.BatchBuyVouchersForm;
import com.tmx.web.base.SecurityService;
import com.tmx.web.base.ApplicationEnvironment;
import com.tmx.web.base.BasicDispatchedAction;
import com.tmx.as.entities.general.user.User;
import com.tmx.as.entities.bill.voucher.BatchBuyVoucher;
import com.tmx.as.blogic.BlogicException;
import com.tmx.as.base.EntityManager;

import java.util.ArrayList;
import java.util.List;
import java.util.Date;
import java.io.*;
import java.text.SimpleDateFormat;
import java.text.DateFormat;
import jxl.write.WritableWorkbook;
import jxl.write.WritableSheet;
import jxl.write.Label;
import jxl.Workbook;

/**
 * Created by IntelliJ IDEA.
 * User: Администратор
 * Date: 27.10.2009
 * Time: 18:57:52
 * To change this template use File | Settings | File Templates.
 */
public class ReportNetworksAction extends BasicDispatchedAction {
       private static final String FORWARD = "page";
       private static final String APPENDER = ",";

    public ActionForward defaultAction(ActionMapping mapping,
                                       ActionForm form,
                                       HttpServletRequest request,
                                       HttpServletResponse response,
                                       String methodName) throws Throwable {
        ReportNetworksForm reportNetworksForm = (ReportNetworksForm) verifyForm(form, ReportNetworksForm.class);

        if(getLoggedInUser(request).getRole().getRoleId() == 1)
        {
            reportNetworksForm.setRoleFlag(true);
        }
        //
            List<EquipmentType> terminalTypes = new EntityManager().RETRIEVE_ALL(EquipmentType.class);
            reportNetworksForm.getScrollBoxEquipmentType().bind(terminalTypes);
        //
        //bind data take into account user roles
        AbstractFormFactory.getInstance(getLoggedInUser(request)).setUpForm(reportNetworksForm);

//        reportNetworksForm.refresh();
        return mapping.findForward(FORWARD);
    }

    public ActionForward refreshGrid(ActionMapping mapping,
                                   ActionForm form,
                                   HttpServletRequest request,
                                   HttpServletResponse response) throws Throwable {
        ReportNetworksForm reportNetworksForm = (ReportNetworksForm) verifyForm(form, ReportNetworksForm.class);



        reportNetworksForm.refresh();
        return mapping.findForward(FORWARD);
    }

    public ActionForward uploadReport(ActionMapping mapping,
                                   ActionForm form,
                                   HttpServletRequest request,
                                   HttpServletResponse response) throws Throwable {
        ReportNetworksForm reportNetworksForm = (ReportNetworksForm) verifyForm(form, ReportNetworksForm.class);

        List reportData = reportNetworksForm.getSalesReport();
        String termType = reportNetworksForm.getScrollBoxEquipmentType().getSelectedKey();
        File file = null;
        //if(reportNetworksForm.getCheckBoxGroupByTerminals().isSelected())  // если выводить по терминалам
        if(reportNetworksForm.isCheckBoxGroupByTerminals() == true)  // если выводить по терминалам
        {
            List filteredData = filterDataByTerminalType(reportData, termType);
            file = dumpReportDataToFiles(filteredData, reportNetworksForm);   // ось тут отфильтровать надо
        }
        else    // если выводить без терминалов
        {
            file = dumpReportDataToFiles(reportData, reportNetworksForm);   // это выгрузка всех данных - ось тут отфильтровать надо
        }
        download(file, response);
        return mapping.findForward(FORWARD);
    }

    private List filterDataByTerminalType(List data, String termType)
    {
        List filteredData = new ArrayList();
        if(!termType.equals("________NULL_KEY_________"))      // если есть фильтр по типу терминала
        {
            for(Object obj : data)
            {
                Object[] ret = (Object[]) obj;
                if (ret[7].equals(termType))
                {
                    Object[] retFiltered = new Object[7];
                    retFiltered[0] = ret[0];
                    retFiltered[1] = ret[1];
                    retFiltered[2] = ret[2];
                    retFiltered[3] = ret[3];
                    retFiltered[4] = ret[4];
                    retFiltered[5] = ret[5];
                    retFiltered[6] = ret[6];
                    filteredData.add(retFiltered);
                }
            }
        }
        else                                    // без фильтра по типу терминала
        {
            for(Object obj : data)
            {
                Object[] ret = (Object[]) obj;
                Object[] retFiltered = new Object[7];
                retFiltered[0] = ret[0];
                retFiltered[1] = ret[1];
                retFiltered[2] = ret[2];
                retFiltered[3] = ret[3];
                retFiltered[4] = ret[4];
                retFiltered[5] = ret[5];
                retFiltered[6] = ret[6];
                filteredData.add(retFiltered);
            }
        }
        return filteredData;
    }

    private User getLoggedInUser(HttpServletRequest request) throws com.tmx.as.exceptions.SecurityException {
        SecurityService securityService = (SecurityService) ApplicationEnvironment.getService(SecurityService.class);
        return securityService.getLoggedInUser(request.getSession());
    }

    private File dumpReportDataToFiles(List<Object[]> reportData, ReportNetworksForm reportNetworksForm) throws BlogicException {
        try{
//            final String csvFileUrl = composeReposrtFileName(".csv");
            final String xlsFileUrl = composeReposrtFileName(".xls");
//            final PrintStream out = new PrintStream(new FileOutputStream(csvFileUrl));
//            final SimpleDateFormat dateFormat = new SimpleDateFormat("ddMMyyyy");
//            final DecimalFormat numberFormat = new DecimalFormat("000000000");
            File xlsFile = new File(xlsFileUrl);
            WritableWorkbook workbook = Workbook.createWorkbook(xlsFile);
            WritableSheet sheet = workbook.createSheet("1stSheet", 0);


            for (int i = 0; i < reportData.size(); i++)
            {                                                       // проходимся по каждому столбцу
                Object[] rowObject = reportData.get(i);
                if (rowObject[1] != null) {
//                    final StringBuffer row = new StringBuffer();
//                    row.append(rowObject[0]); //sellerId
//                    row.append(APPENDER);
//                    row.append(rowObject[1]);//sellerName
//                    row.append(APPENDER);
//                    row.append(rowObject[2]);//operatorId
//                    row.append(APPENDER);
//                    row.append(rowObject[4]);//operatorName
//                    row.append(APPENDER);
//                    row.append(rowObject[3]);//voucherNominal
//                    row.append(APPENDER);
//                    row.append(rowObject[6]);//sellerTx_count
//                    row.append(APPENDER);
//                    row.append(rowObject[5]);//sellerTx_amoun
//                    row.append(APPENDER);
//                    row.append(rowObject[7]);//sum_com
//                    out.println(row.toString());
                    for (int j = 0; j < rowObject.length; j++)
                    {
                        String cell = (rowObject[j] != null) ? rowObject[j].toString() : "";
                        Label label = new Label(j, i+1, cell);
//                        label.setCellFormat(cf);
                        sheet.addCell(label);
                    }

                }
                
                //if(reportNetworksForm.getCheckBoxGroupByTerminals().isSelected()) // с терминалами
                if(reportNetworksForm.isCheckBoxGroupByTerminals() == true) // с терминалами
                {
                    sheet.addCell(new Label(0, 0, "= DILLER ="));
                    sheet.addCell(new Label(1, 0, "= TERMINAL ="));
                    sheet.addCell(new Label(2, 0, "= OPERATOR ="));
                    sheet.addCell(new Label(3, 0, "= VAUCHER ="));
                    sheet.addCell(new Label(4, 0, "= SUMMA ="));
                    sheet.addCell(new Label(5, 0, "= KOLICHESTVO ="));
                    sheet.addCell(new Label(6, 0, "= COMISSIYA ="));
                }
                else                                                    // без терминалов
                {
                    sheet.addCell(new Label(0, 0, "= DILLER ="));
                    sheet.addCell(new Label(1, 0, "= OPERATOR ="));
                    sheet.addCell(new Label(2, 0, "= VAUCHER ="));
                    sheet.addCell(new Label(3, 0, "= SUMMA ="));
                    sheet.addCell(new Label(4, 0, "= KOLICHESTVO ="));
                    sheet.addCell(new Label(5, 0, "= COMISSIYA ="));
                }
            }
                    
            workbook.write();
            workbook.close();
            return xlsFile;
        }
        catch (Exception e){
            throw new BlogicException("Failed to dump Life vouchers", e);
        }
    }

    /*private boolean isIntegerNumber(String str)
    {
        boolean flag = true;

        char [] ch = str.toCharArray();
        for(int i=0; i<ch.length; i++)
        {
            if(!Character.isDigit(ch[i])) flag = false;
        }

        return flag;
    }

    private boolean isDoubleNumber(String str)
    {
        boolean flag = true;

        char [] ch = str.toCharArray();
        for(int i=0; i<ch.length; i++)
        {
            if(!Character.isDigit(ch[i]) || ch[i] != '.') flag = false;
        }

        return flag;
    }
*/

    private String composeReposrtFileName(String fileExtention) {
        final DateFormat format = new SimpleDateFormat("yyyy.MM.dd_HH");
        Date date = new Date();
        return new StringBuffer().
                append(System.getProperty("tmx.home")).
                append("/backup/reports/sales/").
                append("salesReport_").
                append(format.format(date)).
                append(fileExtention).
                toString();
    }

    public void download(File file, HttpServletResponse response) throws Throwable {

        response.setContentType("application/octet-stream");
        response.setHeader("Content-Disposition", "attachment; filename=\"" + file.getName() + "\"");

        FileInputStream fis = null;
        OutputStream os = null;
        try {
            fis = new FileInputStream(file);
            os = response.getOutputStream();
            byte[] buffer = new byte[2048];
            int bytesRead = fis.read(buffer);
            while (bytesRead >= 0) {
                if (bytesRead > 0)
                    os.write(buffer, 0, bytesRead);
                bytesRead = fis.read(buffer);
            }

        }
        finally {
            if (fis != null)
                fis.close();
            if (os != null) {
                os.flush();
                os.close();
            }
        }
        //return mapping.findForward(FORWARD_INFO_BUYVOUCHER_PAGE);
    }
}

//    Tell me why are we
//    So blind to see.
//    That the ones we hurt
//    Are you and me?
//    Tell me why are we
//    So blind to see.
//    That the ones we hurt
//    Are you and me?
