package com.tmx.beng.httpsgate.citypay;

import org.apache.log4j.Logger;
import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import java.util.Iterator;
import javax.xml.soap.Name;

/**
 * Created by IntelliJ IDEA.
 * User: user
 * Date: 03.12.2010
 * Time: 14:06:08
 * To change this template use File | Settings | File Templates.
 */
public class CitypayMessage
{
    private static Logger logger = Logger.getLogger("citypay_gate." + CitypayMessage.class);

    private String chequeNumber;
    private String status;
    private String providerStatusName;
    private String paymentID;
    private String limit;
    private String providerReceiptTime;
    private String providerPayCode;
    private String providerReceipt;
   // private String responseCode;
    private String responseMessage;

    public static CitypayMessage parseResponse(String response, int respCode)
    {
        CitypayMessage citypayResponse = new CitypayMessage();
        citypayResponse.status = respCode + "";
        try {
            Document doc = DocumentHelper.parseText(response.trim());
            Element root = doc.getRootElement();
            if(root.getQualifiedName().equalsIgnoreCase("soap:Envelope"))
            {
                Iterator it_root = root.elementIterator();
                while(it_root.hasNext())
                {
                    Element body = (Element)it_root.next();
                    if(body.getQualifiedName().equalsIgnoreCase("soap:Body"))
                    {
                        Iterator it_body = body.elementIterator();
                        while(it_body.hasNext())
                        {
                            Element element = (Element)it_body.next();
                            if(element.getQualifiedName().equalsIgnoreCase("ProcessPaymentResponse"))     // проведение платежа
                            {
                                parseProcessPaymentResponse(citypayResponse, element);
                                continue;
                            }
                            if(element.getQualifiedName().equalsIgnoreCase("GetStatusResponse"))     // проверка статуса платежа
                            {
                                parseGetStatusResponse(citypayResponse, element);
                                continue;
                            }
                            if(element.getQualifiedName().equalsIgnoreCase("GetLimitResponse"))     // получение лимита
                            {
                                parseGetLimitResponse(citypayResponse, element);
                                continue;
                            }
                            
                            if(element.getQualifiedName().equalsIgnoreCase("soap:Fault"))     // получение ошибочного ответа
                            {
                                parseErrorResponse(citypayResponse, element);
                                continue;
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {

        }
        return citypayResponse;
    }

    public static void parseErrorResponse(CitypayMessage citypayResponse,
                                                              Element element)
    {
        try {
            Iterator itr1 = element.elementIterator();
            while(itr1.hasNext())
            {
                Element elm1 = (Element) itr1.next();
                if(elm1.getQualifiedName().equalsIgnoreCase("faultstring"))
                {
                    citypayResponse.responseMessage = elm1.getTextTrim();
                }
            }
        } catch (Exception e) {

        }
    }

    public static void parseProcessPaymentResponse(CitypayMessage citypayResponse,
                                                              Element element)
    {
        try {
            Iterator itr1 = element.elementIterator();
            while(itr1.hasNext())
            {
                Element elm1 = (Element) itr1.next();
                if(elm1.getQualifiedName().equalsIgnoreCase("ProcessPaymentResult"))
                {
                    Iterator itr2 = elm1.elementIterator();
                    while(itr2.hasNext())
                    {
                        Element elm2 = (Element) itr2.next();
                        if(elm2.getQualifiedName().equalsIgnoreCase("Statuses"))
                        {
                            parseStatuses(citypayResponse, elm2);
                        }
                    }
                }
            }
        } catch (Exception e) {

        }
    }

    public static void parseStatuses(CitypayMessage citypayResponse,
                                                         Element element)
    {
        try {
            Iterator itr1 = element.elementIterator();
            while(itr1.hasNext())
            {
                Element elm1 = (Element) itr1.next();
                if(elm1.getQualifiedName().equalsIgnoreCase("PaymentStatusDetails"))
                {
                    Iterator itr2 = elm1.elementIterator();
                    while(itr2.hasNext())
                    {
                        Element elm2 = (Element) itr2.next();
                        if(elm2.getQualifiedName().equalsIgnoreCase("ChequeNumber"))
                        {
                            citypayResponse.chequeNumber = elm2.getTextTrim();
                            continue;
                        }
                        if(elm2.getQualifiedName().equalsIgnoreCase("Status"))
                        {
                            citypayResponse.status = elm2.getTextTrim();
                            continue;
                        }
                        if(elm2.getQualifiedName().equalsIgnoreCase("ProviderStatusName"))
                        {
                            citypayResponse.providerStatusName = elm2.getTextTrim();
                            continue;
                        }
                        if(elm2.getQualifiedName().equalsIgnoreCase("PaymentID"))
                        {
                            citypayResponse.paymentID = elm2.getTextTrim();
                            continue;
                        }
                        if(elm2.getQualifiedName().equalsIgnoreCase("ProviderReceiptTime"))
                        {
                            citypayResponse.providerReceiptTime = elm2.getTextTrim();
                            continue;
                        }
                        if(elm2.getQualifiedName().equalsIgnoreCase("ProviderPayCode"))
                        {
                            citypayResponse.providerPayCode = elm2.getTextTrim();
                            continue;
                        }
                        if(elm2.getQualifiedName().equalsIgnoreCase("ProviderReceipt"))
                        {
                            citypayResponse.providerReceipt = elm2.getTextTrim();
                            continue;
                        }
                    }
                }
            }
        } catch (Exception e) {

        }
    }

    public static void parseGetStatusResponse(CitypayMessage citypayResponse,
                                                         Element element)
    {
        try {
            Iterator itr1 = element.elementIterator();
            while(itr1.hasNext())
            {
                Element elm1 = (Element) itr1.next();
                if(elm1.getQualifiedName().equalsIgnoreCase("GetStatusResult"))
                {
                    Iterator itr2 = elm1.elementIterator();
                    while(itr2.hasNext())
                    {
                        Element elm2 = (Element) itr2.next();
                        if(elm2.getQualifiedName().equalsIgnoreCase("Statuses"))
                        {
                            parseStatuses(citypayResponse, elm2);
                            continue;
                        }
                        if(elm2.getQualifiedName().equalsIgnoreCase("ErrorCode"))
                        {
                           // citypayResponse.responseCode = elm2.getTextTrim();
                            citypayResponse.status = elm2.getTextTrim();
                            continue;
                        }
                        if(elm2.getQualifiedName().equalsIgnoreCase("ErrorMessage"))
                        {
                            citypayResponse.responseMessage = elm2.getTextTrim();
                            continue;
                        }
                    }
                }
            }
        } catch (Exception e) {

        }
    }

    public static void parseGetLimitResponse(CitypayMessage citypayResponse,
                                                        Element element)
    {
        try {
            Iterator itr1 = element.elementIterator();
            while(itr1.hasNext())
            {
                Element elm1 = (Element) itr1.next();
                if(elm1.getQualifiedName().equalsIgnoreCase("GetLimitResult"))
                {
                    Iterator itr2 = elm1.elementIterator();
                    while(itr2.hasNext())
                    {
                        Element elm2 = (Element) itr2.next();
                        if(elm2.getQualifiedName().equalsIgnoreCase("Limit"))
                        {
                            citypayResponse.limit = elm2.getTextTrim();
                        }
                    }
                }
            }
        } catch (Exception e) {

        }
    }

    public static Logger getLogger() {
        return logger;
    }

    public static void setLogger(Logger logger) {
        CitypayMessage.logger = logger;
    }

    public String getChequeNumber() {
        return chequeNumber;
    }

    public void setChequeNumber(String chequeNumber) {
        this.chequeNumber = chequeNumber;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getProviderStatusName() {
        return providerStatusName;
    }

    public void setProviderStatusName(String providerStatusName) {
        this.providerStatusName = providerStatusName;
    }

    public String getPaymentID() {
        return paymentID;
    }

    public void setPaymentID(String paymentID) {
        this.paymentID = paymentID;
    }

    public String getLimit() {
        return limit;
    }

    public void setLimit(String limit) {
        this.limit = limit;
    }

    public String getProviderReceiptTime() {
        return providerReceiptTime;
    }

    public void setProviderReceiptTime(String providerReceiptTime) {
        this.providerReceiptTime = providerReceiptTime;
    }

    public String getProviderPayCode() {
        return providerPayCode;
    }

    public void setProviderPayCode(String providerPayCode) {
        this.providerPayCode = providerPayCode;
    }

    public String getProviderReceipt() {
        return providerReceipt;
    }

    public void setProviderReceipt(String providerReceipt) {
        this.providerReceipt = providerReceipt;
    }

/*    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }*/

    public String getResponseMessage() {
        return responseMessage;
    }

    public void setResponseMessage(String responseMessage) {
        this.responseMessage = responseMessage;
    }
}
