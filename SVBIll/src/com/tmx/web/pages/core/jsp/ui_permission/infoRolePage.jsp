<%@ page language="java" pageEncoding="UTF-8" contentType="text/html; charset=utf-8" isELIgnored="false" %>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-nested" prefix="nested" %>
<%@ taglib uri="http://tmx.com/taglib/tmx-controls" prefix="tmx_ctrl" %>
<%@ taglib uri="http://tmx.com/taglib/tmx-logic" prefix="tmx_logic" %>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html" %>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean" %>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<table width="100%" border="0">
    <!-- BUTTONS -->
    <tr>
        <td style="text-align: left">
            <tmx_ctrl:button property="buttonApply"/>
            <tmx_ctrl:button property="buttonSave"/>
            <tmx_ctrl:button property="buttonReset" onClick="javascript:document.forms[0].reset()"/>
            <tmx_ctrl:button property="buttonCancel"/>
            <tmx_ctrl:writeActionStatus listener="apply"/>
        </td>
    </tr>
    <tr>
        <!--InfoTaskParamStringForm info-->
        <td valign="center" align="center" class="tabForm">
            <table width="100%" border="0">
                <%--<!-- Form header -->--%>
                <tr>
                    <td colspan="2">
                        <h4 class="dataLabel">
                            <bean:message bundle="core.labels" key="core.page.ui_permission.infoRole.title"/>
                        </h4>
                    </td>
                </tr>
                <tr>
                    <!-- roleId -->
                    <td class="dataLabel">
                        <tmx_ctrl:writeEntityLabel attrName="roleId"
                                                   entityClassName="com.tmx.as.entities.general.role.Role"/>:
                    </td>
                    <td class="dataField">
                        <tmx_ctrl:editBox property="roleId"/>
                    </td>
                </tr>
                <tr>
                    <!-- key -->
                    <td class="dataLabel">
                        <tmx_ctrl:writeEntityLabel attrName="key"
                                                   entityClassName="com.tmx.as.entities.general.role.Role"/>:
                    </td>
                    <td class="dataField">
                        <tmx_ctrl:editBox property="key"/>
                    </td>
                    <!-- roleName -->
                    <td class="dataLabel">
                        <tmx_ctrl:writeEntityLabel attrName="roleName"
                                                   entityClassName="com.tmx.as.entities.general.role.Role"/>:
                    </td>
                    <td class="dataField">
                        <tmx_ctrl:editBox property="roleName"/>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <!-- BUTTONS -->
    <tr>
        <td style="text-align: left">
            <tmx_ctrl:button property="buttonApply"/>
            <tmx_ctrl:button property="buttonSave"/>
            <tmx_ctrl:button property="buttonReset" onClick="javascript:document.forms[0].reset()"/>
            <tmx_ctrl:button property="buttonCancel"/>
        </td>
    </tr>
</table>
