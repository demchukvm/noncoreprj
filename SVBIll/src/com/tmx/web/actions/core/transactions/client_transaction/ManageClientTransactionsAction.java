package com.tmx.web.actions.core.transactions.client_transaction;

import com.tmx.as.blogic.TransactionManager;
import com.tmx.as.entities.bill.operator.Operator;
import com.tmx.as.exceptions.DatabaseException;
import com.tmx.as.base.FilterWrapper;
import com.tmx.as.base.EntityManager;
import com.tmx.as.base.criterion.CriterionWrapper;
import com.tmx.as.entities.bill.terminal.EquipmentType;
import com.tmx.web.actions.core.AbstractSvbillAction;
import com.tmx.web.controls.ScrollBox;
import com.tmx.web.controls.table.FilterSet;
import com.tmx.web.forms.core.base.AbstractFormFactory;
import com.tmx.web.forms.core.transaction.client_transaction.ManageClientTransactionsForm;
import com.tmx.beng.base.StatusDictionary;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;


public class ManageClientTransactionsAction extends AbstractSvbillAction {

    private static final String FORWARD_MANAGE_CLIENT_TRANSACTIONS_PAGE = "manageClientTransactionsPage";
                                                                           
    public ActionForward defaultAction(ActionMapping mapping,
                                       ActionForm form,
                                       HttpServletRequest request,
                                       HttpServletResponse response,
                                       String methodName) throws Throwable {
        ManageClientTransactionsForm transactionsForm = (ManageClientTransactionsForm) form;
        List<EquipmentType> terminalTypes = new EntityManager().RETRIEVE_ALL(EquipmentType.class);
        transactionsForm.getScrollBoxTerminalTypes().bind(terminalTypes);
        AbstractFormFactory.getInstance(getLoggedInUser(request)).setUpForm(transactionsForm);
        preloadFilters(transactionsForm);
        transactionsForm.getClientTransactionsTable().refresh();
        setUpCalculationResult(transactionsForm);
        return mapping.findForward(FORWARD_MANAGE_CLIENT_TRANSACTIONS_PAGE);
    }

    private void setUpCalculationResult(ManageClientTransactionsForm form) throws ClassNotFoundException, DatabaseException {
        FilterWrapper[] filterWrappers = form.getClientTransactionsTable().getQuery().getFilters();
        CriterionWrapper[] criterionWrappers = form.getClientTransactionsTable().getQuery().getCriterions();
        String entityClassName = form.getClientTransactionsTable().getQuery().getEntityClass();
        TransactionManager.CalculationResult calcResult
                = new TransactionManager().calculateTransactions(filterWrappers, entityClassName, criterionWrappers);

        form.getEditBoxAllTransaction().setValue( calcResult.getTransactionCaunt().toString() );
        form.getEditBoxSuccessTransaction().setValue( calcResult.getTransactionSuccessCount().toString() );
        form.getEditBoxErrorTransaction().setValue((calcResult.getTransactionCaunt() - calcResult.getTransactionSuccessCount()) + "");

        form.getEditBoxAllTransAmount().setValue(calcResult.getTransactionAmount().toString());
        form.getEditBoxSuccessTransAmount().setValue( calcResult.getTransactionSuccessAmount().toString() );
//        form.getEditBoxErrorTransAmount().setValue((calcResult.getTransactionSuccessAmount() - calcResult.getTransactionSuccessAmount()) + "");
    }

    private void preloadFilters(ManageClientTransactionsForm form) throws DatabaseException {
        FilterSet filterSet = form.getClientTransactionsTable().getFilterSet();
        
        ScrollBox scrollBoxOperator = (ScrollBox)filterSet.getFilter("by_operator").getParameter("operator").getControl();

        List operators = new TransactionManager().getAllOperators();
        //Collections.sort(operators);
        //scrollBoxOperator.bind( new TransactionManager().getAllOperators() );
        Collections.sort(operators, new Comparator<Operator>()
        {
          public int compare(Operator op1, Operator op2) {
            return op1.getName().compareTo(op2.getName());
          }
        });
        scrollBoxOperator.bind(operators);


        form.getScrollBoxTransTypes().bind(new TransactionManager().getAllTransactionType());
        
        List<StatusDictionary.StatusObject> statuses = StatusDictionary.getStatuses();
        form.getScrollBoxStatuses().bind(statuses);
    }


    public ActionForward refreshTable(ActionMapping mapping,
                                      ActionForm form,
                                      HttpServletRequest request,
                                      HttpServletResponse response) throws Throwable {
        ManageClientTransactionsForm transactionsForm = (ManageClientTransactionsForm) form;
        transactionsForm.getClientTransactionsTable().refresh();
        setUpCalculationResult(transactionsForm);
        return mapping.findForward(FORWARD_MANAGE_CLIENT_TRANSACTIONS_PAGE);
    }
}
