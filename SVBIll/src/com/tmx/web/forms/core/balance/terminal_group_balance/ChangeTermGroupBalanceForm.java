package com.tmx.web.forms.core.balance.terminal_group_balance;

import com.tmx.web.forms.core.balance.AbstractChangeBalanceForm;

public class ChangeTermGroupBalanceForm extends AbstractChangeBalanceForm {

    protected void initChangeBalanceForm() {
        getOnCancelButton().setAction("/core/balance/terminal_group_balance/infoTerminalGroupBalance");
        getOnChangeButton().setAction("/core/balance/terminal_group_balance/changeTerminalGroupBalance");
    }

}
