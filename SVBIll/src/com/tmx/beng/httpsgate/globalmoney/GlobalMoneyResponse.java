package com.tmx.beng.httpsgate.globalmoney;

import org.json.*;
import java.util.Date;


public class GlobalMoneyResponse
{
    private Integer result;
    //private Integer transactionId;
    private Integer status;
    private Integer limit;
    private String voucher;
    private String voucher_expire;
    private String receipt_no;
    private Date responseTime;

    public GlobalMoneyResponse()
    {
        result = -1;
        //transactionId = 0;
        status = -1;
        limit = -1;
        voucher = "";
        voucher_expire = "";
        receipt_no = "";
    }

    public void parseResponse(String response)
    {
        responseTime = new Date();

        try {
            JSONObject resp = new JSONObject(response);

            if(!resp.isNull("result"))
            {
            	result = resp.getInt("result");
            }

            if(!resp.isNull("statuses"))
            {
	            JSONArray PaymentStatusDataList = resp.getJSONArray("statuses");
	            JSONObject PaymentStatusData = PaymentStatusDataList.getJSONObject(0);

                /*
	            if(!PaymentStatusData.isNull("transactionId"))
	            {
	            	transactionId = PaymentStatusData.getInt("transactionId");
	            }
	            */

	            if(!PaymentStatusData.isNull("status"))
	            {
	            	status = PaymentStatusData.getInt("status");
	            }

	            if(!PaymentStatusData.isNull("limit"))
	            {
	            	limit = PaymentStatusData.getInt("limit");
	            }

	            if(!PaymentStatusData.isNull("receipt"))
	            {
	            	JSONObject Receipt = PaymentStatusData.getJSONObject("receipt");

	            	if(!Receipt.isNull("values"))
	            	{
		            	JSONObject values =  Receipt.getJSONObject("values");

		            	if(!values.isNull("voucher"))
		            	{
		            		voucher = values.getString("voucher");
		            	}

		            	if(!values.isNull("voucher_expire"))
		            	{
		 	           		voucher_expire = values.getString("voucher_expire");
		            	}

		            	if(!values.isNull("receipt_no"))
		            	{
		            		receipt_no = values.getString("receipt_no");
		            	}

	            	}
	            }
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public Integer getResult() {
        return result;
    }

    public Date getResponseTime() {
        return responseTime;
    }
//    public Integer getTransactionId() {
//        return transactionId;
//    }

    public Integer getStatus() {
        return status;
    }

    public Integer getLimit() {
        return limit;
    }

    public String getVoucher() {
        return voucher;
    }

    public String getVoucher_expire() {
        return voucher_expire;
    }

    public String getReceipt_no() {
        return receipt_no;
    }
}