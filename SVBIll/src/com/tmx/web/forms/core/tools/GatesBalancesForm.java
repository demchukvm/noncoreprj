package com.tmx.web.forms.core.tools;

import com.tmx.web.controls.Button;
import com.tmx.web.controls.EditBox;
import com.tmx.web.base.BasicActionForm;
import com.tmx.web.controls.MultipleSelectBox;
import com.tmx.web.controls.ScrollBox;

import java.util.HashMap;
import java.util.List;

public class GatesBalancesForm extends BasicActionForm
{
    private Button onRefreshBalance = null;
    private EditBox editBoxCitypayBalance = null;
    private EditBox editBoxCitypayForLifeBalance = null;
    private EditBox editBoxCyberplatBalance = null;
    private EditBox editBoxAvancelBalance = null;
    private EditBox editBoxKSBalance = null;
    private EditBox editBoxKSBNCBalance = null;
    private EditBox editBoxKSFCBalance = null;
    private EditBox editBoxMTSBalance = null;

    private String citypaySmile;
    private String citypayForLifeSmile;
    private String cyberplatSmile;
    private String avancelSmile;
    private String ksSmile;
    private String ksbncSmile;
    private String ksfcSmile;
    private String mtsSmile;

    public final String SMILE1 = "/core/images/common/smiles/smile_1.gif";
    public final String SMILE2 = "/core/images/common/smiles/smile_2.gif";
    public final String SMILE3 = "/core/images/common/smiles/smile_3.gif";
    public final String SMILE4 = "/core/images/common/smiles/smile_4.gif";
    public final String SMILE5 = "/core/images/common/smiles/smile_5.gif";

    public void init()
    {
        onRefreshBalance = new Button();
        onRefreshBalance.setLabelKey("ctrl.button.label.refresh_balance");
        onRefreshBalance.setAction("/GatesBalancesAction");
        onRefreshBalance.setCommand("refreshBalance");

        editBoxCitypayBalance = new EditBox();
        editBoxCitypayBalance.setReadonly(true);

        editBoxCitypayForLifeBalance = new EditBox();
        editBoxCitypayForLifeBalance.setReadonly(true);

        editBoxCyberplatBalance = new EditBox();
        editBoxCyberplatBalance.setReadonly(true);

        editBoxAvancelBalance = new EditBox();
        editBoxAvancelBalance.setReadonly(true);

        editBoxKSBalance = new EditBox();
        editBoxKSBalance.setReadonly(true);

        editBoxKSBNCBalance = new EditBox();
        editBoxKSBNCBalance.setReadonly(true);

        editBoxKSFCBalance = new EditBox();
        editBoxKSFCBalance.setReadonly(true);

        editBoxMTSBalance = new EditBox();
        editBoxMTSBalance.setReadonly(true);
    }

    public Button getOnRefreshBalance() {
        return onRefreshBalance;
    }

    public void setOnRefreshBalance(Button onRefreshBalance) {
        this.onRefreshBalance = onRefreshBalance;
    }

    public EditBox getEditBoxCitypayBalance() {
        return editBoxCitypayBalance;
    }

    public void setEditBoxCitypayBalance(EditBox editBoxCitypayBalance) {
        this.editBoxCitypayBalance = editBoxCitypayBalance;
    }

    public EditBox getEditBoxAvancelBalance() {
        return editBoxAvancelBalance;
    }

    public void setEditBoxAvancelBalance(EditBox editBoxAvancelBalance) {
        this.editBoxAvancelBalance = editBoxAvancelBalance;
    }

    public EditBox getEditBoxKSBalance() {
        return editBoxKSBalance;
    }

    public void setEditBoxKSBalance(EditBox editBoxKSBalance) {
        this.editBoxKSBalance = editBoxKSBalance;
    }

    public EditBox getEditBoxKSBNCBalance() {
        return editBoxKSBNCBalance;
    }

    public void setEditBoxKSBNCBalance(EditBox editBoxKSBNCBalance) {
        this.editBoxKSBNCBalance = editBoxKSBNCBalance;
    }

    public EditBox getEditBoxKSFCBalance() {
        return editBoxKSFCBalance;
    }

    public void setEditBoxKSFCBalance(EditBox editBoxKSFCBalance) {
        this.editBoxKSFCBalance = editBoxKSFCBalance;
    }

    public EditBox getEditBoxMTSBalance() {
        return editBoxMTSBalance;
    }

    public void setEditBoxMTSBalance(EditBox editBoxMTSBalance) {
        this.editBoxMTSBalance = editBoxMTSBalance;
    }

    public EditBox getEditBoxCitypayForLifeBalance() {
        return editBoxCitypayForLifeBalance;
    }

    public void setEditBoxCitypayForLifeBalance(EditBox editBoxCitypayForLifeBalance) {
        this.editBoxCitypayForLifeBalance = editBoxCitypayForLifeBalance;
    }

    public EditBox getEditBoxCyberplatBalance() {
        return editBoxCyberplatBalance;
    }

    public void setEditBoxCyberplatBalance(EditBox editBoxCyberplatBalance) {
        this.editBoxCyberplatBalance = editBoxCyberplatBalance;
    }

    public String getCitypaySmile() {
        return citypaySmile;
    }

    public void setCitypaySmile(String citypaySmile) {
        this.citypaySmile = citypaySmile;
    }

    public String getCitypayForLifeSmile() {
        return citypayForLifeSmile;
    }

    public void setCitypayForLifeSmile(String citypayForLifeSmile) {
        this.citypayForLifeSmile = citypayForLifeSmile;
    }

    public String getCyberplatSmile() {
        return cyberplatSmile;
    }

    public void setCyberplatSmile(String cyberplatSmile) {
        this.cyberplatSmile = cyberplatSmile;
    }

    public String getAvancelSmile() {
        return avancelSmile;
    }

    public void setAvancelSmile(String avancelSmile) {
        this.avancelSmile = avancelSmile;
    }

    public String getKsSmile() {
        return ksSmile;
    }

    public void setKsSmile(String ksSmile) {
        this.ksSmile = ksSmile;
    }

    public String getKsbncSmile() {
        return ksbncSmile;
    }

    public void setKsbncSmile(String ksbncSmile) {
        this.ksbncSmile = ksbncSmile;
    }

    public String getKsfcSmile() {
        return ksfcSmile;
    }

    public void setKsfcSmile(String ksfcSmile) {
        this.ksfcSmile = ksfcSmile;
    }

    public String getMtsSmile() {
        return mtsSmile;
    }

    public void setMtsSmile(String mtsSmile) {
        this.mtsSmile = mtsSmile;
    }
}
