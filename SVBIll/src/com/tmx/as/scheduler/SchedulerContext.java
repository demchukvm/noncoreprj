package com.tmx.as.scheduler;

import java.util.*;

import org.apache.log4j.*;
import com.sun.org.apache.xpath.internal.XPathAPI;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.w3c.dom.Node;
import org.w3c.dom.Element;
import com.tmx.util.InitException;
import com.tmx.util.XMLUtil;
import com.tmx.util.Configuration;
import com.tmx.as.base.Reconfigurable;

/** (Singleton pattern) */
public class SchedulerContext implements Reconfigurable {
    /** Only single instance available */
    private static SchedulerContext schedulerContext;

    /**
     * The schedule thread
     */
    private ScheduleThread scheduleThread = null;
    /**
     * The map of processing threads
     */
    private Map procThreadMap = new HashMap();
    /**
     * Logger
     */
    private static Logger logger = Logger.getLogger("scheduler.SchedulerContext");

    /** to hide default constructor */
    private SchedulerContext(){
    }

    /** Return scheduler context instance */
    public static SchedulerContext getInstance(){
        if(schedulerContext == null)
            schedulerContext = new SchedulerContext();

        return schedulerContext;
    }

    /** reload configuration */
    public void reload() throws InitException {
        try {
            logger.debug("Start initialization of the Scheduler Context");
            String schedulerFile = Configuration.getInstance().getProperty("scheduler.config_file");
            if (schedulerFile == null)
                throw new InitException("err.scheduler.config_file_is_not_specified", Locale.getDefault());

            //Load config from XML
            Document doc = XMLUtil.getDocumentFromFile(schedulerFile);

            ScheduleThread.Config schThreadConfig = ScheduleThread.createConfig();

            schThreadConfig.setSleepTime(10000);//default value (ms)
            //try to override default value
            try {
                String xpathSleepTimeNode = "/scheduler/schedulerThread/param[@name='sleepTime']/@value";
                Node sleepTimeNode = XPathAPI.selectSingleNode(doc, xpathSleepTimeNode);
                if (sleepTimeNode != null && sleepTimeNode.getNodeType() == Node.ATTRIBUTE_NODE)
                    schThreadConfig.setSleepTime(Long.parseLong(sleepTimeNode.getNodeValue()));
            }
            catch (Exception e) {/** ignore */}

            schThreadConfig.setAutostart(true);//default value (ms)
            //try to override default value
            try {
                String xpathAutostartNode = "/scheduler/schedulerThread/param[@name='autoStart']/@value";
                Node autostartNode = XPathAPI.selectSingleNode(doc, xpathAutostartNode);
                if (autostartNode != null && autostartNode.getNodeType() == Node.ATTRIBUTE_NODE)
                    schThreadConfig.setAutostart(Boolean.valueOf(autostartNode.getNodeValue()));
            }
            catch (Exception e) {/** ignore */}

            scheduleThread = schThreadConfig.instantiate();

            String xpathProcessingThread = "/scheduler/processingThreads/processingThread";
            NodeList processingThreadNodes = XPathAPI.selectNodeList(doc, xpathProcessingThread);
            Map procThreadsConfigs = new HashMap();
            for (int i = 0; i < processingThreadNodes.getLength(); i++) {
                Node processingThreadNode = processingThreadNodes.item(i);
                if (processingThreadNode.getNodeType() == Node.ATTRIBUTE_NODE)
                    continue;

                ProcessingThread.Config procThreadConfig = ProcessingThread.createConfig();
                String procThrNameStr = ((Element) processingThreadNode).getAttribute("name");
                if (procThrNameStr == null)
                    throw new InitException("err.scheduler.proc_thread_name_is_null_in_config");
                if (procThreadsConfigs.containsKey(procThrNameStr))
                    throw new InitException("err.scheduler.duplicate_proc_thread_name_in_config", new String[]{procThrNameStr});
                procThreadConfig.setName(procThrNameStr);

                String procThrClassStr = ((Element) processingThreadNode).getAttribute("implClass");
                if (procThrClassStr == null || "".equals(procThrClassStr))
                    throw new InitException("   err.scheduler.proc_thread_class_is_null_in_config", new String[]{procThrClassStr});

                try {
                    Class implClass = Class.forName(procThrClassStr);
                    if(!ProcessingThread.class.isAssignableFrom(implClass))
                        throw new InitException("err.scheduler.proc_class_is_not_successor_of", new String[]{implClass.getName(), ProcessingThread.class.getName()});

                    procThreadConfig.setImplClass(implClass);
                }
                catch (Exception e) {
                    throw new InitException("err.scheduler.failed_get_proc_thread_class", e);
                }

                procThreadsConfigs.put(procThreadConfig.getName(), procThreadConfig);
            }

            //Reinit threads after the config reloading
            reinitProcessingThreads(procThreadsConfigs);

            logger.debug("Successful completed initialization of the Scheduler Context");


        }
        catch (InitException e) {
            //rethrow
            throw e;
        }
        catch (Exception e) {
            throw new InitException("err.scheduler.reload_failed", e, Locale.getDefault());
        }
    }

    private void reinitProcessingThreads(Map procThreadsConfigs) throws InitException{
        procThreadMap.clear();
        for(Iterator iter = procThreadsConfigs.keySet().iterator(); iter.hasNext();) {
            String procThreadName = (String) iter.next();
            ProcessingThread.Config ptConfig = (ProcessingThread.Config)procThreadsConfigs.get(procThreadName);
            try {
                ProcessingThread pt = ptConfig.instantiate();
                procThreadMap.put(procThreadName, pt);
                logger.debug("Successful inited the processing thread '" + procThreadName + "'.");
            }
            catch (Exception e) {
                throw new InitException("err.scheduler.failed_to_init_processing_thread", new String[]{procThreadName}, e);
            }
        }
    }


    public void start() throws SchedulerException{
        if(scheduleThread == null)
            throw new SchedulerException("err.scheduler.schedule_thread_is_null_in_the_context");

        if(scheduleThread.getConfig().isAutostart())
            startScheduleThread();
    }

    /**
     * Start the thread if it's not started.
     */
    public void startScheduleThread() throws SchedulerException{
        if(scheduleThread == null)
            throw new SchedulerException("err.scheduler.schedule_thread_is_null_in_the_context");

        if (!scheduleThread.isAlive()) {
            scheduleThread.start();
            logger.info("Successfully started Scheduler thread");
        }
        else {
            logger.info("Failed to start Scheduler thread, becouse it is already alive");
        }
    }


    /**
     * Graceful stop the schedule thread.
     */
    public void stopScheduleThread() {
        if (scheduleThread.isAlive()) {
            scheduleThread.shutDown();
            logger.info("Successfully stoped Scheduler thread '" + scheduleThread.getName() + "'");
        }
        else {
            logger.info("Failed to stop Scheduler thread '" + scheduleThread.getName() + "', becouse it is already stoped");
        }
    }


    /**
     * Return is schedule thread alive.
     */
    public boolean isScheduleThreadAlive() {
        return scheduleThread.isAlive();
    }


    /**
     * Start processing thread (Use for manual management).
     */
    public void startProcThread(String procThreadName) throws InitException, SchedulerException{
        try {
                ProcessingThread procThread = (ProcessingThread)procThreadMap.get(procThreadName);
            if(procThread == null)
                throw new SchedulerException("err.scheduler.failed_to_start_unknown_processing_thread", new String[]{procThreadName});

            logger.debug("Starting the procThread '" + procThreadName + "'");
            if (!procThread.isAlive()) {
                //It is very strange, but if twice call the next method here, then thread have not been started
                //((ProcessingThread)procThreadMap.get(procThreadName)).start();
                //So I create, replace and start the new existence of Thread
                //Create and replace thread
                procThreadMap.put(procThreadName, procThread.getNewInstance());
                //Start the thread
                ((ProcessingThread) procThreadMap.get(procThreadName)).start();
                logger.debug("Successful started procThread '" + procThreadName + "'");
            }
            else {
                logger.debug("Failed to start procThread '" + procThreadName + "', because it is already started.");
            }
        }
        catch (Exception e) {
            logger.error("Failed to start procThread '" + procThreadName + "', because: " + e.toString());
        }
    }


    /**
     * Is processing thread alive (Use for manual management).
     */
    public boolean isProcThreadAlive(String procThreadName) {
        try {
            if (((ProcessingThread) procThreadMap.get(procThreadName)).isAlive()) {
//        logger.debug("Successful got the ACTIVE processing thread '"+procThreadName+"' status.");
                return true;
            } else {
//        logger.debug("Successful got the DIED processing thread '"+procThreadName+"' status.");        
            }
        }
        catch (Exception e) {
            logger.error("Failed to get status of procThread '" + procThreadName + "', because: " + e.toString());
        }
        return false;

    }

    /**
     * Return the iterator of procThreads names.
     */
    public Iterator getProcThreadNames() {
        return procThreadMap.keySet().iterator();
    }

    /**
     * Start the thread if it's not started. RM: ???
     */
    public void createScheduleThread(ScheduleThread.Config config) {
        scheduleThread = config.instantiate();
    }

    public Map getProcThreadMap() {
        return procThreadMap;
    }

 }