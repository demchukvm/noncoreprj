package com.tmx.web.forms.core.ui_permission;

import com.tmx.web.controls.*;
import com.tmx.web.controls.table.*;
import com.tmx.web.base.BasicActionForm;

public class ManageRoleForm extends BasicActionForm {
    private Table table;
    private Button onCreateButton;
    private Button onEditButton;
    private Button onDeleteButton;

    protected void init() {
        table = new Table("query(query_type=retrieve_all," +
                "entity_class=com.tmx.as.entities.general.role.Role," +
                "filter(name=by_notsuperuser)," +
                "order(name=id,type=asc)," +
                "page_size=10" +
                ")");
        // filters
//
//        filter.setFilterEnabler(new FilterEnabler(){public boolean isFilterEnabled() {return true;}});
//        table.getFilterSet().addFilter("by_notsuperuser", filter);

        Filter filter = new Filter();
        filter.addParameter("value", new Parameter(new EditBox()));
        table.getFilterSet().addFilter
                ("com_tmx_as_entities_general_role_Role_key", filter);

        filter = new Filter();
        filter.addParameter("value", new Parameter(new EditBox()));
        table.getFilterSet().addFilter
                ("com_tmx_as_entities_general_role_Role_roleName", filter);
        //setup buttons
        String action="/core/ui_permission/infoRole";
        onCreateButton = new Button();
        onCreateButton.setLabelKey("ctrl.button.label.create");
        onCreateButton.setAction(action);
        onCreateButton.setCommand("preloadForCreate");

        onEditButton = new Button();
        onEditButton.setLabelKey("ctrl.button.label.edit");
        onEditButton.setAction(action);
        onEditButton.setCommand("preloadForUpdate");

        onDeleteButton = new Button();
        onDeleteButton.setLabelKey("ctrl.button.label.delete");
        onDeleteButton.setAction(action);
        onDeleteButton.setCommand("preloadForDelete");
    }

    public Table getTable() {
        return table;
    }

    public void setTable(Table table) {
        this.table = table;
    }

    public Button getOnCreateButton() {
        return onCreateButton;
    }

    public void setOnCreateButton(Button onCreateButton) {
        this.onCreateButton = onCreateButton;
    }

    public Button getOnEditButton() {
        return onEditButton;
    }

    public void setOnEditButton(Button onEditButton) {
        this.onEditButton = onEditButton;
    }

    public Button getOnDeleteButton() {
        return onDeleteButton;
    }

    public void setOnDeleteButton(Button onDeleteButton) {
        this.onDeleteButton = onDeleteButton;
    }
}
