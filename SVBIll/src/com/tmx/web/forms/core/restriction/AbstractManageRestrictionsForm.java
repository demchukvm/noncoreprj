package com.tmx.web.forms.core.restriction;

import com.tmx.as.entities.general.user.User;
import com.tmx.as.exceptions.DatabaseException;
import com.tmx.as.resolver.FunctionSyntaxException;
import com.tmx.as.resolver.ResolverException;
import com.tmx.as.blogic.BlogicException;
import com.tmx.web.base.BasicActionForm;
import com.tmx.web.controls.Button;
import com.tmx.web.controls.EditBox;
import com.tmx.web.controls.table.Parameter;
import com.tmx.web.controls.table.Table;
import com.tmx.web.controls.table.ControlManagerTable;

abstract public class AbstractManageRestrictionsForm extends BasicActionForm {

    private ControlManagerTable restrictionsTable;
    private ControlManagerTable incomingRestrictionsTable = new ControlManagerTable();

    private Button onCopyButton;
    private Button onCopyAllButton;
    private Button onCreateIRButton;
    private Button onEditIRButton;
    private Button onDeleteIRButton;

    private Button onCreateButton;
    private Button onEditButton;
    private Button onDeleteButton;
    private Long incomingRestrictionId;

    protected void init(){
        initButtons();
        initTables();
        //after control initializations
        restrictionsTable.addDependedControl(onEditButton).addDependedControl(onDeleteButton);
    }

    private void initButtons() {
        onCreateButton = new Button();
        onCreateButton.setLabelKey("ctrl.button.label.create");
        onCreateButton.setCommand("onCreateRestrictionAction");

        onEditButton = new Button();
        onEditButton.setLabelKey("ctrl.button.label.edit");
        onEditButton.setCommand("preloadForUpdate");

        onDeleteButton = new Button();
        onDeleteButton.setLabelKey("ctrl.button.label.delete");
        onDeleteButton.setCommand("preloadForDelete");

        onCopyButton = new Button();
        onCopyButton.setLabelKey("ctrl.button.label.copy");
        onCopyButton.setCommand("preloadForCopy");

        onCopyAllButton = new Button();
        onCopyAllButton.setLabelKey("ctrl.button.label.copy_all");
        onCopyAllButton.setCommand("preloadForCopyAll");

        onDeleteIRButton = new Button();
        onDeleteIRButton.setLabelKey("ctrl.button.label.delete");
        onDeleteIRButton.setCommand("preloadForDeleteIR");

        onEditIRButton = new Button();
        onEditIRButton.setLabelKey("ctrl.button.label.edit");
        onEditIRButton.setCommand("preloadForUpdateIR");

        onCreateIRButton = new Button();
        onCreateIRButton.setLabelKey("ctrl.button.label.create");
        onCreateIRButton.setCommand("onCreateIncomingRestrictionAction");
    }

    private void initTables(){
        restrictionsTable = new ControlManagerTable();
        restrictionsTable.addParameterToFilter("by_restriction_name", new Parameter("restriction_name", new EditBox()));
        restrictionsTable.setRefreshCommandName("refreshRestrictionsTable");
    }

    public ControlManagerTable getRestrictionsTable() {
        return restrictionsTable;
    }

    public void setRestrictionsTable(ControlManagerTable restrictionsTable) {
        this.restrictionsTable = restrictionsTable;
    }

    public Button getOnCreateButton() {
        return onCreateButton;
    }

    public void setOnCreateButton(Button onCreateButton) {
        this.onCreateButton = onCreateButton;
    }

    public Button getOnEditButton() {
        return onEditButton;
    }

    public void setOnEditButton(Button onEditButton) {
        this.onEditButton = onEditButton;
    }

    public Button getOnDeleteButton() {
        return onDeleteButton;
    }

    public void setOnDeleteButton(Button onDeleteButton) {
        this.onDeleteButton = onDeleteButton;
    }

    public Button getOnCopyButton() {
        return onCopyButton;
    }

    public void setOnCopyButton(Button onCopyButton) {
        this.onCopyButton = onCopyButton;
    }

    public Button getOnCopyAllButton() {
        return onCopyAllButton;
    }

    public void setOnCopyAllButton(Button onCopyAllButton) {
        this.onCopyAllButton = onCopyAllButton;
    }

    public Button getOnCreateIRButton() {
        return onCreateIRButton;
    }

    public void setOnCreateIRButton(Button onCreateIRButton) {
        this.onCreateIRButton = onCreateIRButton;
    }

    public Button getOnEditIRButton() {
        return onEditIRButton;
    }

    public void setOnEditIRButton(Button onEditIRButton) {
        this.onEditIRButton = onEditIRButton;
    }

    public Button getOnDeleteIRButton() {
        return onDeleteIRButton;
    }

    public void setOnDeleteIRButton(Button onDeleteIRButton) {
        this.onDeleteIRButton = onDeleteIRButton;
    }

    public Long getIncomingRestrictionId() {
        return incomingRestrictionId;
    }

    public void setIncomingRestrictionId(Long incomingRestrictionId) {
        this.incomingRestrictionId = incomingRestrictionId;
    }
    public ControlManagerTable getIncomingRestrictionsTable() {
        return incomingRestrictionsTable;
    }

    public void setIncomingRestrictionsTable(ControlManagerTable incomingRestrictionsTable) {
        this.incomingRestrictionsTable = incomingRestrictionsTable;
    }
}
