package com.tmx.web.actions.core.tools;

import com.tmx.as.base.QueryParameterWrapper;
import com.tmx.as.base.criterion.CriterionWrapper;
import com.tmx.as.base.criterion.Restrictions;
import com.tmx.as.blogic.*;
import com.tmx.as.entities.bill.terminal.Terminal;
import com.tmx.as.entities.mobipay.avancel.AvancelTransaction;
import com.tmx.beng.httpsgate.HttpsGateException;
import com.tmx.beng.httpsgate.avancel.AvancelGate1;
import com.tmx.beng.httpsgate.avancel.AvancelGate2;
import com.tmx.beng.httpsgate.avancel.AvancelGate3;
import com.tmx.beng.httpsgate.avancel.AvancelGate4;
import com.tmx.web.controls.PopulateUtil;
import com.tmx.web.controls.grid.GridException;
import com.tmx.web.actions.core.AbstractSvbillAction;
import com.tmx.as.entities.bill.voucher.BatchBuyVoucher;
import com.tmx.as.base.EntityManager;
import com.tmx.as.exceptions.DatabaseException;
import com.tmx.beng.csapi.java.base.BuyVoucherResp;
import com.tmx.web.forms.core.tools.TestBuyVouchersForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForm;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.*;
import java.io.FileInputStream;
import java.io.File;
import java.io.OutputStream;
import java.util.concurrent.atomic.AtomicReference;

public class TestBuyVouchersAction extends AbstractSvbillAction
{
    private static final String FORWARD = "testBuyVouchers";

    private boolean isComplete = false;
    private String total = "";
    private String complete = "";

    private BatchBuyVoucher getBatchBuyVoucherObject(TestBuyVouchersForm form, String login)
    {
        BatchBuyVoucher buyOperation = new BatchBuyVoucher();
        buyOperation.setTransactionTime(new Date());
        buyOperation.setVoucherNominal(Integer.parseInt(form.getSbVoucherNominal().getSelectedKey()));
        buyOperation.setStatus("In Process");
        buyOperation.setUserActor(login);
        buyOperation.setSellerCode(form.SELLER_CODE);
        buyOperation.setProcessingCode(form.PROCESSING);
        buyOperation.setTerminalSn(form.getSbTerminalSn().getSelectedKey());
        buyOperation.setServiceCode(form.getSbServiceCode().getSelectedKey());

        return buyOperation;
    }

    public ActionForward buy(ActionMapping mapping,
                                       ActionForm form,
                                       HttpServletRequest request,
                                       HttpServletResponse response) throws Throwable
    {
        final TestBuyVouchersForm testBuyVouchersForm = (TestBuyVouchersForm) form;
        testBuyVouchersForm.validate();

        changeButton(testBuyVouchersForm, true, false, true, true);

        final int countTotal = PopulateUtil.getIntValue(testBuyVouchersForm.getEbVoucherCount());
        final BatchBuyVoucher buyOperation = getBatchBuyVoucherObject(testBuyVouchersForm, getLoggedInUser(request).getLogin());
        buyOperation.setVoucherCount(countTotal);

        final EntityManager em = new EntityManager();
        em.SAVE(buyOperation);


        if(testBuyVouchersForm.getSbDistributor().getSelectedKey().equals("Avancel"))
        {
            /* It needs avancel_gate_2, avancel_gate_3 & avancel_gate_4 */

            final String billTransactionNum = (new Date().getTime()) + "";   // чтобы у всех транзакций был одинаковый

            buyOperation.setOperationType("Avancel");     // в другой ветке указывать nPay

            int countGates = 3;
            int modulo = countTotal % countGates;
            int countWithoutModulo = (countTotal - modulo) / countGates;
            final int countGate_2 = countWithoutModulo;
            final int countGate_3 = countWithoutModulo;
            final int countGate_4 = countWithoutModulo + modulo;

            total = countTotal + "";
            complete = "0";
            testBuyVouchersForm.getEbCompliteCount().setValue(complete + " / " + total);
            testBuyVouchersForm.getEbStatus().setValue("In Process");

            final ArrayList<Long> trIdGate2 = new ArrayList<Long>();
            final ArrayList<Long> trIdGate3 = new ArrayList<Long>();
            final ArrayList<Long> trIdGate4 = new ArrayList<Long>();

            final ArrayList<Object[]> vouchersGate2 = new ArrayList<Object[]>();
            final ArrayList<Object[]> vouchersGate3 = new ArrayList<Object[]>();
            final ArrayList<Object[]> vouchersGate4 = new ArrayList<Object[]>();


            final AtomicReference<Thread> buyerThread_main = new AtomicReference<Thread>(new Thread() {
                public void run() {

                    // SEND 1
                    final Thread sendThread_1 = new Thread() {
                        public void run() {
                            if (countGate_2 > 0) {
                                for (int i = 0; i < countGate_2; i++) {
                                    try {
                                        Long transactionId = AvancelGate2.getInstance().genVoucherSend(
                                                testBuyVouchersForm.getSbVoucherNominal().getSelectedKey(),
                                                testBuyVouchersForm.getSbServiceCode().getSelectedKey(),
                                                testBuyVouchersForm.getSbTerminalSn().getSelectedKey(),
                                                billTransactionNum
                                        );
                                        trIdGate2.add(transactionId);
                                    } catch (HttpsGateException e) {
                                        e.printStackTrace();
                                    }
                                }
                            }
                        }
                    };

                    // SEND 2
                    final Thread sendThread_2 = new Thread() {
                        public void run() {
                            if (countGate_3 > 0) {
                                for (int i = 0; i < countGate_3; i++) {
                                    try {
                                        Long transactionId = AvancelGate3.getInstance().genVoucherSend(
                                                testBuyVouchersForm.getSbVoucherNominal().getSelectedKey(),
                                                testBuyVouchersForm.getSbServiceCode().getSelectedKey(),
                                                testBuyVouchersForm.getSbTerminalSn().getSelectedKey(),
                                                billTransactionNum
                                        );
                                        trIdGate3.add(transactionId);
                                    } catch (HttpsGateException e) {
                                        e.printStackTrace();
                                    }
                                }
                            }

                        }
                    };

                    // SEND 3
                    final Thread sendThread_3 = new Thread() {
                        public void run() {
                            if (countGate_4 > 0) {
                                for (int i = 0; i < countGate_4; i++) {
                                    try {
                                        Long transactionId = AvancelGate4.getInstance().genVoucherSend(
                                                testBuyVouchersForm.getSbVoucherNominal().getSelectedKey(),
                                                testBuyVouchersForm.getSbServiceCode().getSelectedKey(),
                                                testBuyVouchersForm.getSbTerminalSn().getSelectedKey(),
                                                billTransactionNum
                                        );
                                        trIdGate4.add(transactionId);
                                    } catch (HttpsGateException e) {
                                        e.printStackTrace();
                                    }
                                }
                            }
                        }
                    };

                    // STATUS 1
                    final Thread statThread_1 = new Thread() {
                        public void run() {
                            if (trIdGate2.size() > 0) {
                                for (Long id : trIdGate2) {
                                    try {
                                        Object[] voucher = AvancelGate2.getInstance().genVoucherStat(id);
                                        vouchersGate2.add(voucher);
                                    } catch (HttpsGateException e) {
                                        e.printStackTrace();
                                    }
                                }
                            }
                        }
                    };

                    // STATUS 2
                    final Thread statThread_2 = new Thread() {
                        public void run() {
                            if (trIdGate3.size() > 0) {
                                for (Long id : trIdGate3) {
                                    try {
                                        Object[] voucher = AvancelGate3.getInstance().genVoucherStat(id);
                                        vouchersGate3.add(voucher);
                                    } catch (HttpsGateException e) {
                                        e.printStackTrace();
                                    }
                                }
                            }
                        }
                    };

                    // STATUS 3
                    final Thread statThread_3 = new Thread() {
                        public void run() {
                            if (trIdGate4.size() > 0) {
                                for (Long id : trIdGate4) {
                                    try {
                                        Object[] voucher = AvancelGate4.getInstance().genVoucherStat(id);
                                        vouchersGate4.add(voucher);
                                    } catch (HttpsGateException e) {
                                        e.printStackTrace();
                                    }
                                }
                            }
                        }
                    };

                    sendThread_1.start();
                    sendThread_2.start();
                    sendThread_3.start();


                    // CHECK IF SEND IS COMPLETE
                    try {
                        sendThread_1.join();
                        sendThread_2.join();
                        sendThread_3.join();

                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                    statThread_1.start();
                    statThread_2.start();
                    statThread_3.start();

                    // CHECK IF STATUS IS COMPLETE
                    try {
                        statThread_1.join();
                        statThread_2.join();
                        statThread_3.join();

                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                    isComplete = true;
                    buyOperation.setStatus("Complete");

                    ArrayList<Object[]> vouchers = new ArrayList<Object[]>();
                    vouchers.addAll(vouchersGate2);
                    vouchers.addAll(vouchersGate3);
                    vouchers.addAll(vouchersGate4);

                    List<BuyVoucherResp> v = new LinkedList<BuyVoucherResp>();
                    for (Object[] voucher : vouchers) {
                        BuyVoucherResp vv = new BuyVoucherResp();
                        vv.setCode(voucher[0].toString());
                        vv.setSecretCode(voucher[1].toString());
                        vv.setPrice(Double.parseDouble(voucher[2].toString()));

                        vv.setBestBeforeDate((Date)voucher[3]);

                        v.add(vv);
                    }

                    buyOperation.setVoucherCount(v.size());

                    try {
                        VoucherManager manager = new VoucherManager();
                        manager.dumpVouchersIntoFile(buyOperation, v);
                        testBuyVouchersForm.setFileUrl(buyOperation.getDumpFileUrl());
                    } catch(BlogicException e) {
                        testBuyVouchersForm.getEbStatus().setValue("Dumping Error");
                    }

                    try {
                        em.SAVE(buyOperation);
                    } catch (DatabaseException e) {
                        testBuyVouchersForm.getEbStatus().setValue("DB Error");
                    }

                    // после генерации
                    changeButton(testBuyVouchersForm, true, true, false, true);

                    complete = v.size() + "";
                    testBuyVouchersForm.getEbCompliteCount().setValue(complete + " / " + total);
                    testBuyVouchersForm.getEbStatus().setValue("Complete");

                }
            });


            /*
            // Для проверки незаконченных покупок ваучеров

            final ArrayList<Long> IdGate = new ArrayList<Long>();
            
            IdGate.add((long)39124501);
            IdGate.add((long)39124661);
            IdGate.add((long)39124889);
            IdGate.add((long)39125783);
            IdGate.add((long)39126908);
            IdGate.add((long)39127321);
            IdGate.add((long)39128740);
            IdGate.add((long)39129644);
            IdGate.add((long)39130416);
            IdGate.add((long)39130890);
            IdGate.add((long)39131853);
            IdGate.add((long)39132322);
            IdGate.add((long)39132398);
            IdGate.add((long)39132640);
            IdGate.add((long)39133171);
            IdGate.add((long)39135424);
            IdGate.add((long)39136023);
            IdGate.add((long)39136426);
            IdGate.add((long)39136628);
            IdGate.add((long)39136845);
            IdGate.add((long)39137934);
            IdGate.add((long)39227415);

            final ArrayList<Object[]> vouchersGate = new ArrayList<Object[]>();

            final AtomicReference<Thread> buyerThread_main = new AtomicReference<Thread>(new Thread() {
                public void run() {
                    // STATUS 1

                    for (Long id : IdGate) {
                        try {
                            Object[] voucher = AvancelGate1.getInstance().genVoucherStat(id);
                            vouchersGate.add(voucher);
                        } catch (HttpsGateException e) {
                            e.printStackTrace();
                        }
                    }

                }
            });
            */


            // Для выгрузки ваучеров в файл за определенный период

            /*

            final AtomicReference<Thread> buyerThread_main = new AtomicReference<Thread>(new Thread() {
                public void run() {

                    ArrayList<Object[]> vouchers = new ArrayList<Object[]>();

                    Date from = testBuyVouchersForm.getFrom().getTime();
                    Date to = testBuyVouchersForm.getTo().getTime();

                    CriterionWrapper[] criterions = new CriterionWrapper[] {
                            Restrictions.between("requestTime", from, to),
                            Restrictions.eq("msisdn", "voucher"),
                            Restrictions.isNotNull("avancelTransactionIdentifier")
                    };

                    List<AvancelTransaction> voucherList = null;
                    try {
                        voucherList = new EntityManager().RETRIEVE_ALL(AvancelTransaction.class, criterions, null);
                    } catch (DatabaseException e) {
                        e.printStackTrace();
                    }

                    for(AvancelTransaction aT : voucherList)
                    {
                        Object[] obj = new Object[4];
                        obj[0] = aT.getVoucherSerialNumber();
                        obj[1] = aT.getVoucherSecretCode();
                        obj[2] = (int) (aT.getAmount() * 100) + "";
                        try {
                            obj[3] = AvancelGate1.getInstance().parseDate(aT.getVoucherExpirationDate());
                        } catch (HttpsGateException e) {
                            e.printStackTrace();
                        }
                        vouchers.add(obj);

                    }

                    List<BuyVoucherResp> v = new LinkedList<BuyVoucherResp>();
                    for (Object[] voucher : vouchers) {
                        BuyVoucherResp vv = new BuyVoucherResp();
                        vv.setCode(voucher[0].toString());
                        vv.setSecretCode(voucher[1].toString());
                        vv.setPrice(Double.parseDouble(voucher[2].toString()));

                        vv.setBestBeforeDate((Date)voucher[3]);

                        v.add(vv);
                    }

                    buyOperation.setVoucherCount(v.size());

                    try {
                        VoucherManager manager = new VoucherManager();
                        manager.dumpVouchersIntoFile(buyOperation, v);
                        testBuyVouchersForm.setFileUrl(buyOperation.getDumpFileUrl());
                    } catch(BlogicException e) {
                        testBuyVouchersForm.getEbStatus().setValue("Dumping Error");
                    }

                    try {
                        em.SAVE(buyOperation);
                    } catch (DatabaseException e) {
                        testBuyVouchersForm.getEbStatus().setValue("DB Error");
                    }

                    // после генерации
                    changeButton(testBuyVouchersForm, true, true, false, true);

                    complete = v.size() + "";
                    testBuyVouchersForm.getEbCompliteCount().setValue(complete + " / " + total);
                    testBuyVouchersForm.getEbStatus().setValue("Complete");

                }
            });
            */

            buyerThread_main.get().start();
        }
        else if(testBuyVouchersForm.getSbDistributor().getSelectedKey().equals("nPay"))
        {

        }

        return mapping.findForward(FORWARD);
    }



    public ActionForward getStatus(ActionMapping mapping,
                                       ActionForm form,
                                       HttpServletRequest request,
                                       HttpServletResponse response) throws Throwable {

        TestBuyVouchersForm testBuyVouchersForm = (TestBuyVouchersForm) form;

        testBuyVouchersForm.getEbCompliteCount().setValue(complete + " / " + total);

        if(isComplete == true)
        {
            testBuyVouchersForm.getEbStatus().setValue("Complete");
            changeButton(testBuyVouchersForm, true, true, false, true);
        }
        else
        {
            testBuyVouchersForm.getEbStatus().setValue("In Process");   
        }


        return mapping.findForward(FORWARD);
    }


    public ActionForward download(ActionMapping mapping,
                                  ActionForm form,
                                  HttpServletRequest request,
                                  HttpServletResponse response) throws Throwable {

        TestBuyVouchersForm testBuyVouchersForm = (TestBuyVouchersForm) form;

        File voucherDumpFile = new File(testBuyVouchersForm.getFileUrl());
        if(voucherDumpFile == null || !voucherDumpFile.exists())
            return null;//RM: make friendly-user response

        response.setContentType("application/octet-stream");
        response.setHeader("Content-Disposition", "attachment; filename=\"" + voucherDumpFile.getName() + "\"");

        FileInputStream fis = null;
        OutputStream os = null;
        try {
            fis = new FileInputStream(voucherDumpFile);
            os = response.getOutputStream();
            byte[] buffer = new byte[2048];
            int bytesRead = fis.read(buffer);
            while (bytesRead >= 0) {
                if (bytesRead > 0)
                    os.write(buffer, 0, bytesRead);
                bytesRead = fis.read(buffer);
            }
        }
        finally {
            if (fis != null)
                fis.close();
            if (os != null) {
                os.flush();
                os.close();
            }
            changeButton(testBuyVouchersForm, true, true, false, false);
        }

        //return mapping.findForward(FORWARD);
        return null;
        
    }


    public ActionForward defaultAction(ActionMapping mapping,
                                       ActionForm form,
                                       HttpServletRequest request,
                                       HttpServletResponse response,
                                       String methodName) throws Throwable {

        return mapping.findForward(FORWARD);
    }

    public ActionForward reset(ActionMapping mapping,
                                       ActionForm form,
                                       HttpServletRequest request,
                                       HttpServletResponse response) throws Throwable {

        TestBuyVouchersForm testBuyVouchersForm = (TestBuyVouchersForm) form;

        testBuyVouchersForm.getSbVoucherNominal().reset();
        testBuyVouchersForm.getEbVoucherCount().reset();
        testBuyVouchersForm.getEbSellerCode().setValue(testBuyVouchersForm.SELLER_CODE);
        testBuyVouchersForm.getSbDistributor().reset();
        testBuyVouchersForm.getSbServiceCode().reset();
        testBuyVouchersForm.getSbTerminalSn().reset();

        testBuyVouchersForm.getEbCompliteCount().setValue("0 / 0");
        testBuyVouchersForm.getEbStatus().reset();

        testBuyVouchersForm.setFileUrl("");

        changeButton(testBuyVouchersForm, false, true, true, false);

        isComplete = false;
        total = "";
        complete = "";

        return mapping.findForward(FORWARD);
    }

    private void changeButton(TestBuyVouchersForm form, boolean buy, boolean status, boolean download, boolean reset)
    {
        form.getbBuy().setReadonly(buy);
        form.getbStatus().setReadonly(status);
        form.getbDownload().setReadonly(download);
        form.getbReset().setReadonly(reset);
    }


    public ActionForward showVoucers(ActionMapping mapping,
                                       ActionForm form,
                                       HttpServletRequest request,
                                       HttpServletResponse response) throws Throwable {

        TestBuyVouchersForm testBuyVouchersForm = (TestBuyVouchersForm) form;
        bindGrid(testBuyVouchersForm);
        

        return mapping.findForward(FORWARD);
    }


    private void bindGrid(TestBuyVouchersForm form) throws GridException, DatabaseException, Exception
    {
        List data = null;

        Date from = form.getFrom().getTime();
        Date to = form.getTo().getTime();

        data = getData(from, to, "10", "Avancel");
        data.addAll(getData(from, to, "25", "Avancel"));
        data.addAll(getData(from, to, "35", "Avancel"));
        data.addAll(getData(from, to, "50", "Avancel"));
        data.addAll(getData(from, to, "100", "Avancel"));

        /*
        data.addAll(getData(from, to, "10"+"00", "nPay"));
        data.addAll(getData(from, to, "25"+"00", "nPay"));
        data.addAll(getData(from, to, "35"+"00", "nPay"));
        data.addAll(getData(from, to, "50"+"00", "nPay"));
        data.addAll(getData(from, to, "100"+"00", "nPay"));
        */
        form.getGrid().bind(data);
    }

    private List getData(Date from, Date to, String nominal, String distributor) throws DatabaseException
    {
        List data = null;

        QueryParameterWrapper[] wrapper = new QueryParameterWrapper[]{
            new QueryParameterWrapper("fromT", from == null ? new Date() : from),
            new QueryParameterWrapper("toT", to == null ? new Date() : to),
            new QueryParameterWrapper("voucherNominal", nominal)
        };

        if(distributor.equals("Avancel"))
        {
            data = new EntityManager().EXECUTE_QUERY(BatchBuyVoucher.class, "getLifeVouchersByAvancel", wrapper);
        }
        else if(distributor.equals("nPay"))
        {
            data = new EntityManager().EXECUTE_QUERY(BatchBuyVoucher.class, "getLifeVouchersByNPay", wrapper);
        }

        return data;
    }

}
