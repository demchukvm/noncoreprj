package com.tmx.util;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.UTFDataFormatException;
import java.io.File;
import java.text.SimpleDateFormat;
import java.text.ParseException;
import java.util.Date;
import java.util.Random;
import java.util.Locale;

public class StringUtil {

    /** Returns the given URL with well-formed attached parameter. */
    public static String addUrlParam(String url, String param){
        return (url.indexOf("?") <0 ) ? (url + "?" + param) : (url + "&" + param);
    }


    /** If class name contains dot-notated sequence of packages names, then discard them.
     * Null safe. */
    public static String getStrippedClassName(Class cls){
        if(cls == null)
            return null;
        String className = cls.getName();
        int lastDotIdx = className.lastIndexOf(".");
        if(lastDotIdx > 0)
            className = className.substring(lastDotIdx+1);
        return className;
    }    

    public static Locale createLocale(String lang_COUNTRY){
        if(lang_COUNTRY == null)
            return null;
        String[] localeLexemes = lang_COUNTRY.split("_");
        return new Locale(localeLexemes[0], localeLexemes[1]);
    }

    /** Generates the uniqe string from digits based on current timestamp and RNG seed.*/
    public static String generateUniqueDigitString(){
        String uniqueDigitString = String.valueOf(System.currentTimeMillis()) + "_";
        Random rnd = new Random();
        uniqueDigitString = uniqueDigitString + String.valueOf(Math.abs(rnd.nextLong()));
        return uniqueDigitString;
    }

    /**
     * Return object Date by field string and DateTime format
     */
    public static Date createDate(String date, String format) throws ParseException {
        Date result;
        result = new SimpleDateFormat(format).parse(date);
        return result;
    }

    /**
     * Return String by field date and DateTime format
     */
    public static String getFormattedDate(java.util.Date date, String format) {
        String res = "";
        if (format == null)
            format = "yyyy-MM-dd HH:mm:ss";
        if (date != null) {
            if(format != null){
                SimpleDateFormat sf = new SimpleDateFormat(format);
                res = sf.format(date);
            }
            else{
                res = date.toString();
            }
        }
        return res;
    }

    /**
     * Returns Getter method name by field name
     */
    public static String getGetterName(String fieldName) {
        return "get" + fieldName.substring(0, 1).toUpperCase() + fieldName.substring(1, fieldName.length());
    }

    /**
     * Returns Field name by getter name
     */
    public static String getFieldName(String getterName) {
        if(getterName.substring(0, 3).equals("get")){
            String baseName = getterName.substring(3);
            return baseName.substring(0,1).toLowerCase() + baseName.substring(1);
        }
        return null;
    }

    /**
     * Returns Setter method name by field name
     */
    public static String getSetterName(String fieldName) {
        return "set" + fieldName.substring(0, 1).toUpperCase() + fieldName.substring(1, fieldName.length());
    }

    /** Create instance name according to the Bean naming rules.
     * That is mean the instance name is created from corresponding class name
     * by changing into lower case first character of class name:
     * <code>SomeClass -> someClass</code>.
     * */
    public static String getInstanceName(Class classs){
        String onlyClassName = classs.getName();
        int lastDotIdx = classs.getName().lastIndexOf(".");
        if(lastDotIdx < classs.getName().length())
            onlyClassName = classs.getName().substring(++lastDotIdx);
        onlyClassName = (onlyClassName == null || onlyClassName.length() <= 0) ? classs.getName() : onlyClassName;
        return onlyClassName.substring(0, 1).toLowerCase() + onlyClassName.substring(1);
    }

    public static String toUTF8(String src) {
        StringBuffer sb = new StringBuffer(src.length() * 2);
        appendUTF8(sb, src);
        return sb.toString();
    }

    public static void appendUTF8(StringBuffer sb, String src) {
        int c;
        int len = src.length();

        for (int i = 0; i < len; i++) {
            c = src.charAt(i);

            if ((c >= 0x0001) && (c <= 0x007F)) {
                sb.append((char) c);
            } else if (c > 0x07FF) {
                sb.append((char) (0xE0 | ((c >> 12) & 0x0F)));
                sb.append((char) (0x80 | ((c >> 6) & 0x3F)));
                sb.append((char) (0x80 | ((c >> 0) & 0x3F)));
            } else {
                sb.append((char) (0xC0 | ((c >> 6) & 0x1F)));
                sb.append((char) (0x80 | ((c >> 0) & 0x3F)));
            }
        }
    }

    public static String fromUTF8(String src) throws UTFDataFormatException {
        //if (1==1) return src;
        int len = src.length();
        int c, char2, char3;
        char[] str = new char[len];
        int strlen = 0;
        int count = 0;
        while (count < len) {
            c = (int) src.charAt(count) & 0xff;
            switch (c >> 4) {
                case 0:
                case 1:
                case 2:
                case 3:
                case 4:
                case 5:
                case 6:
                case 7:
                    /* 0xxxxxxx*/
                    count++;
                    str[strlen++] = (char) c;
                    break;
                case 12:
                case 13:
                    /* 110x xxxx   10xx xxxx*/
                    count += 2;
                    if (count > len)
                        throw new UTFDataFormatException();
                    char2 = src.charAt(count - 1);
                    if ((char2 & 0xC0) != 0x80)
                        throw new UTFDataFormatException();
                    str[strlen++] = (char) (((c & 0x1F) << 6) | (char2 & 0x3F));
                    break;
                case 14:
                    /* 1110 xxxx  10xx xxxx  10xx xxxx */
                    count += 3;
                    if (count > len)
                        throw new UTFDataFormatException();
                    char2 = src.charAt(count - 2);
                    char3 = src.charAt(count - 1);
                    if (((char2 & 0xC0) != 0x80) || ((char3 & 0xC0) != 0x80))
                        throw new UTFDataFormatException();
                    str[strlen++] = (char) (((c & 0x0F) << 12) |
                            ((char2 & 0x3F) << 6) |
                            ((char3 & 0x3F) << 0));
                    break;
                default:
                    /* 10xx xxxx,  1111 xxxx */
                    throw new UTFDataFormatException();
            }
        }
        return new String(str, 0, strlen);
    }

    public static String Cescape(String str) {
        StringBuffer sb = new StringBuffer(str.length() + 30);
        for (int i = 0; i < str.length(); i++) {
            final char c = str.charAt(i);
            switch (c) {
                case '\r':
                    continue;
                case '\n':
                    sb.append('\\');
                    sb.append('n');
                    continue;
                case '\\':
                    sb.append('\\');
                    sb.append('\\');
                    continue;
                case '"':
                    sb.append('\\');
                    sb.append('"');
                    continue;
                default:
                    sb.append(c);
            }
        }
        return sb.toString();
    }

    public static String formatException(Exception ex) {
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        ex.printStackTrace(pw);
        pw.flush();
        return sw.getBuffer().toString();
    }

    public static String removeChar(String value, char c) {
        // ��-�� �� ������ �� �� ������...
        char[] s = value.toCharArray();
        StringBuffer b = new StringBuffer(s.length);
        for (int i = 0; i < s.length; i++) {
            if (s[i] != '\r')
                b.append(s[i]);
        }
        value = b.toString();
        return value;
    }

    public static boolean checkValid(String str) {
        boolean res = false;
        if (str != null)
            if (str.length() > 0)
                res = true;
        return res;
    }

    public static String checkNull(String str) {
        if (str == null)
            return "";
        return str;
    }

    /**
     * Convert string for use as html plaintext. Replace some special characters with it html codes.
     *
     * @param inputString string for converting
     * @return safe html plaintext
     */
    public static String convertForHTML(String inputString) {
        inputString = checkNull(inputString);
        return inputString.replaceAll("&", "&amp;").
        replaceAll("\"", "&quot;").
        replaceAll("\'", "&rsquo;").
        replaceAll("<", "&lt;").
        replaceAll(">", "&gt;");
//        replaceAll("[", "&#91;").
//        replaceAll("]", "&#93;").
//        replaceAll("\\", "\\\\");
    }

    /**
     * Convert string for use as sql string value. Replace some special characters with it codes.
     *
     * @param inputString string for converting
     * @return safe sql string value
     */
    public static String convertForSQL(String inputString) {
        inputString = checkNull(inputString);
        inputString.replaceAll("\'", "\\\'");
        return inputString;
    }

    public static String getFormattedDate(java.util.Date date) {
        String res = "";
        if (date != null) {
            SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            res = sf.format(date);
        }
        return res;
    }

    public static String trimString(String where, String what) {
        if (!checkValid(where) || where.length() <= what.length())
            return where;
        StringBuffer res = new StringBuffer(where);
        while (res.indexOf(what) == 0)
            res.delete(0, what.length());
        while (res.length() > what.length() && res.lastIndexOf(what) == res.length() - what.length())
            res.delete(res.length() - what.length(), res.length());
        return res.toString();
    }

    /**
     * Extract short class name from full class name (with full package name). If package name is not found -
     * return input string.
     * For example, result of extractShortClassName("com.tmx.lgms.util.StringUtil") is "StringUtil".
     * And result of extractShortClassName("StringUtil") is "StringUtil".
     *
     * @param fullClassName full class name
     * @return short class name
     */
    public static String extractShortClassName(String fullClassName) {
        int index = fullClassName.lastIndexOf('.');
        return index > 0 ? fullClassName.substring(index + 1) : fullClassName;
    }

    /**
     * Extract file name from full file name with path.
     *
     * @param fullFileName full file name with path
     * @return file name (with extension)
     */
    public static String extractFileName(String fullFileName) {
        int index;
        int index1 = fullFileName.lastIndexOf("/");
        int index2 = fullFileName.lastIndexOf("\\");
        index = index1 > index2 ? index1 : index2;
        return index > 0 ? fullFileName.substring(index+1) : fullFileName;
    }


    /**
     * Extract file extension from full file name with path.
     *
     * @param fullFileName full file name with path
     * @return extension or empty string if there is no extension.
     */
    public static String extractFileExtension(String fullFileName) {
        int index = fullFileName.lastIndexOf(".");
        return index > 0 && index < (fullFileName.length() - 1) ? /** dot is found but not a last char */
                fullFileName.substring(index+1) : "";
    }

    /**
     * Extract path from full file name with path.
     *
     * @param fullFileName full file name with path
     * @return path with closing slash
     */
    public static String extractPath(String fullFileName) {
        int index;
        int index1 = fullFileName.lastIndexOf("/");
        int index2 = fullFileName.lastIndexOf("\\");
        index = index1 > index2 ? index1 : index2;
        return index > 0 ? fullFileName.substring(0, index + 1) : "";
    }


    public static String nullSafeNormalize(String ps){
        if(ps == null)
            return "";
        else
            return StringUtil.normalize(ps);
    }


    /**
     * Normalize the given path string.  A normal path string has no empty
     * segments (i.e., occurrences of "//"), no segments equal to ".", and no
     * segments equal to ".." that are preceded by a segment not equal to "..".
     * In contrast to Unix-style pathname normalization, for URI paths we
     * always retain trailing slashes.
     */
    public static String normalize(String ps) {
        ps = ps.replace('\\', '/');

        // Does this path need normalization?
        int ns = needsNormalization(ps);    // Number of segments
        if (ns < 0)
            // Nope -- just return it
            return ps;

        char[] path = ps.toCharArray();        // Path in char-array form

        // Split path into segments
        int[] segs = new int[ns];        // Segment-index array
        split(path, segs);

        // Remove dots
        removeDots(path, segs);

        // Prevent scheme-name confusion
        maybeAddLeadingDot(path, segs);

        // Join the remaining segments and return the result
        String s = new String(path, 0, join(path, segs));
        if (s.equals(ps)) {
            // string was already normalized
            return ps;
        }
        return s;
    }


    /**
     * Check the given path to see if it might need normalization.  A path
     * might need normalization if it contains duplicate slashes, a "."
     * segment, or a ".." segment.  Return -1 if no further normalization is
     * possible, otherwise return the number of segments found.
     * <p/>
     * This method takes a string argument rather than a char array so that
     * this test can be performed without invoking path.toCharArray().
     */
    private static int needsNormalization(String path) {
        boolean normal = true;
        int ns = 0;            // Number of segments
        int end = path.length() - 1;    // Index of last char in path
        int p = 0;            // Index of next char in path

        // Skip initial slashes
        while (p <= end) {
            if (path.charAt(p) != '/') break;
            p++;
        }
        if (p > 1) normal = false;

        // Scan segments
        while (p <= end) {

            // Looking at "." or ".." ?
            if ((path.charAt(p) == '.')
                    && ((p == end)
                    || ((path.charAt(p + 1) == '/')
                    || ((path.charAt(p + 1) == '.')
                    && ((p + 1 == end)
                    || (path.charAt(p + 2) == '/')))))) {
                normal = false;
            }
            ns++;

            // Find beginning of next segment
            while (p <= end) {
                if (path.charAt(p++) != '/')
                    continue;

                // Skip redundant slashes
                while (p <= end) {
                    if (path.charAt(p) != '/') break;
                    normal = false;
                    p++;
                }

                break;
            }
        }
        return normal ? -1 : ns;
    }

    /**
     * Split the given path into segments, replacing slashes with nulls and
     * filling in the given segment-index array.
     * <p/>
     * Preconditions:
     * segs.length == Number of segments in path
     * <p/>
     * Postconditions:
     * All slashes in path replaced by '\0'
     * segs[i] == Index of first char in segment i (0 <= i < segs.length)
     */
    private static void split(char[] path, int[] segs) {
        int end = path.length - 1;    // Index of last char in path
        int p = 0;            // Index of next char in path
        int i = 0;            // Index of current segment

        // Skip initial slashes
        while (p <= end) {
            if (path[p] != '/') break;
            path[p] = '\0';
            p++;
        }

        while (p <= end) {

            // Note start of segment
            segs[i++] = p++;

            // Find beginning of next segment
            while (p <= end) {
                if (path[p++] != '/')
                    continue;
                path[p - 1] = '\0';

                // Skip redundant slashes
                while (p <= end) {
                    if (path[p] != '/') break;
                    path[p++] = '\0';
                }
                break;
            }
        }

        if (i != segs.length)
            throw new InternalError();    // ASSERT
    }


    /**
     * Remove "." segments from the given path, and remove segment pairs
     * consisting of a non-".." segment followed by a ".." segment.
     */
    private static void removeDots(char[] path, int[] segs) {
        int ns = segs.length;
        int end = path.length - 1;

        for (int i = 0; i < ns; i++) {
            int dots = 0;        // Number of dots found (0, 1, or 2)

            // Find next occurrence of "." or ".."
            do {
                int p = segs[i];
                if (path[p] == '.') {
                    if (p == end) {
                        dots = 1;
                        break;
                    } else if (path[p + 1] == '\0') {
                        dots = 1;
                        break;
                    } else if ((path[p + 1] == '.')
                            && ((p + 1 == end)
                            || (path[p + 2] == '\0'))) {
                        dots = 2;
                        break;
                    }
                }
                i++;
            } while (i < ns);
            if ((i > ns) || (dots == 0))
                break;

            if (dots == 1) {
                // Remove this occurrence of "."
                segs[i] = -1;
            } else {
                // If there is a preceding non-".." segment, remove both that
                // segment and this occurrence of ".."; otherwise, leave this
                // ".." segment as-is.
                int j;
                for (j = i - 1; j >= 0; j--) {
                    if (segs[j] != -1) break;
                }
                if (j >= 0) {
                    int q = segs[j];
                    if (!((path[q] == '.')
                            && (path[q + 1] == '.')
                            && (path[q + 2] == '\0'))) {
                        segs[i] = -1;
                        segs[j] = -1;
                    }
                }
            }
        }
    }


    /**
     * DEVIATION: If the normalized path is relative, and if the first
     * segment could be parsed as a scheme name, then prepend a "." segment
     */
    private static void maybeAddLeadingDot(char[] path, int[] segs) {

        if (path[0] == '\0')
            // The path is absolute
            return;

        int ns = segs.length;
        int f = 0;            // Index of first segment
        while (f < ns) {
            if (segs[f] >= 0)
                break;
            f++;
        }
        if ((f >= ns) || (f == 0))
            // The path is empty, or else the original first segment survived,
            // in which case we already know that no leading "." is needed
            return;

        int p = segs[f];
        while ((p < path.length) && (path[p] != ':') && (path[p] != '\0')) p++;
        if (p >= path.length || path[p] == '\0')
            // No colon in first segment, so no "." needed
            return;

        // At this point we know that the first segment is unused,
        // hence we can insert a "." segment at that position
        path[0] = '.';
        path[1] = '\0';
        segs[0] = 0;
    }


    /**
     * Join the segments in the given path according to the given segment-index
     * array, ignoring those segments whose index entries have been set to -1,
     * and inserting slashes as needed.  Return the length of the resulting
     * path.
     * <p/>
     * Preconditions:
     * segs[i] == -1 implies segment i is to be ignored
     * path computed by split, as above, with '\0' having replaced '/'
     * <p/>
     * Postconditions:
     * path[0] .. path[return value] == Resulting path
     */
    private static int join(char[] path, int[] segs) {
        int ns = segs.length;        // Number of segments
        int end = path.length - 1;    // Index of last char in path
        int p = 0;            // Index of next path char to write

        if (path[p] == '\0') {
            // Restore initial slash for absolute paths
            path[p++] = '/';
        }

        for (int i = 0; i < ns; i++) {
            int q = segs[i];        // Current segment
            if (q == -1)
                // Ignore this segment
                continue;

            if (p == q) {
                // We're already at this segment, so just skip to its end
                while ((p <= end) && (path[p] != '\0'))
                    p++;
                if (p <= end) {
                    // Preserve trailing slash
                    path[p++] = '/';
                }
            } else if (p < q) {
                // Copy q down to p
                while ((q <= end) && (path[q] != '\0'))
                    path[p++] = path[q++];
                if (q <= end) {
                    // Preserve trailing slash
                    path[p++] = '/';
                }
            } else
                throw new InternalError(); // ASSERT false
        }

        return p;
    }


    /** return all /R and /N symbols from given string */
    public static String removeReturnSymbols(String str){
        if(str == null)
            return str;

        str = str.replaceAll("\\n", "");
        str = str.replaceAll("\\r", "");

        return str;
    }

}
