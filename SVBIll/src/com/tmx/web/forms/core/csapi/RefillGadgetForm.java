package com.tmx.web.forms.core.csapi;

import com.tmx.web.controls.Button;
import com.tmx.web.controls.EditBox;
import com.tmx.web.controls.ScrollBox;

import java.util.LinkedList;
import java.util.List;

public class RefillGadgetForm extends CsApiCallForm 
{
    private ScrollBox sbProducts;

    private ScrollBox sbTerminals;

    private EditBox ebAmount;
    private EditBox ebAccount;
    private Button bRefill;


    private String responseXML;
    private String message;

    private String refillStory;

    public final String ACCESS_DENIED = "Access denied";

    private Button bAllThisMonth;

    private String lastRefillData;
    private String billNumber;
    private String status;
    private String statusMessage;

    protected void init()
    {
        responseXML = "";
        message = "";
        refillStory = "";
        lastRefillData = "";
        billNumber = "";
        status = "";
        statusMessage = "";


        sbProducts = new ScrollBox<String[]>()
        {
            protected String getKey(String[] products){return products[0];}
            protected String getValue(String[] products){return products[1];}
        };
        final List<String[]> products = new LinkedList<String[]>();
        products.add(new String[]{"KyivstarBonusNoCommission:payAccount", "KBNC_account"});
        products.add(new String[]{"KyivstarBonusNoCommission:msisdn", "KBNC_msisdn"});
        products.add(new String[]{"LifeExc:msisdn", "LifeExc_msisdn"});
        products.add(new String[]{"MTSExc:payAccount", "MTSExc_account"});
        products.add(new String[]{"MTSExc:msisdn", "MTSExc_msisdn"});
        sbProducts.bind(products);
        //sbProducts.setNullable(true);
        sbProducts.setMandatory(true);


        sbTerminals = new ScrollBox<String[]>()
        {
            protected String getKey(String[] terminals){return terminals[0];}
            protected String getValue(String[] terminals){return terminals[1];}
        };
        final List<String[]> terminals = new LinkedList<String[]>();
        terminals.add(new String[]{"UN_00004", "UN_00004 (MTSExc)"});
        terminals.add(new String[]{"UN_00586", "UN_00586 (KSBNC)"});
        terminals.add(new String[]{"UN_00687", "UN_00687 (KSBNC)"});
        terminals.add(new String[]{"UN_10474", "UN_10474 (KSBNC)"});
        terminals.add(new String[]{"UN_00888", "UN_00888 (LifeExc)"});
        sbTerminals.bind(terminals);
        //sbTerminals.setNullable(true);
        sbTerminals.setMandatory(true);

        ebAmount = new EditBox();
        ebAmount.setMandatory(true);

        ebAccount = new EditBox();
        ebAccount.setMandatory(true);

        bRefill = new Button();
        bRefill.setLabelKey("ctrl.button.label.refill");
        bRefill.setAction("/RefillGadgetAction");
        bRefill.setCommand("clientSideApiCall");

        bAllThisMonth = new Button();
        bAllThisMonth.setLabelKey("ctrl.button.label.refill");   // !!!!
        bAllThisMonth.setAction("/RefillGadgetAction");
        bAllThisMonth.setCommand("getAllThisMonth");
    }

    public ScrollBox getSbProducts() {
        return sbProducts;
    }

    public void setSbProducts(ScrollBox sbProducts) {
        this.sbProducts = sbProducts;
    }

    public ScrollBox getSbTerminals() {
        return sbTerminals;
    }

    public void setSbTerminals(ScrollBox sbTerminals) {
        this.sbTerminals = sbTerminals;
    }

    public EditBox getEbAmount() {
        return ebAmount;
    }

    public void setEbAmount(EditBox ebAmount) {
        this.ebAmount = ebAmount;
    }

    public EditBox getEbAccount() {
        return ebAccount;
    }

    public void setEbAccount(EditBox ebAccount) {
        this.ebAccount = ebAccount;
    }

    public Button getbRefill() {
        return bRefill;
    }

    public void setbRefill(Button bRefill) {
        this.bRefill = bRefill;
    }

    public String getResponseXML() {
        return responseXML;
    }

    public void setResponseXML(String responseXML) {
        this.responseXML = responseXML;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getRefillStory() {
        return refillStory;
    }

    public void setRefillStory(String refillStory) {
        this.refillStory = refillStory;
    }

    public Button getbAllThisMonth() {
        return bAllThisMonth;
    }

    public void setbAllThisMonth(Button bAllThisMonth) {
        this.bAllThisMonth = bAllThisMonth;
    }

    public String getLastRefillData() {
        return lastRefillData;
    }

    public void setLastRefillData(String lastRefillData) {
        this.lastRefillData = lastRefillData;
    }

    public String getBillNumber() {
        return billNumber;
    }

    public void setBillNumber(String billNumber) {
        this.billNumber = billNumber;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatusMessage() {
        return statusMessage;
    }

    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }
}
