package com.tmx.beng.medaccess.citypay;

import com.tmx.beng.base.BillingMessage;
import com.tmx.beng.httpsgate.HttpsGateException;
import com.tmx.beng.httpsgate.citypay.CitypayGate;
import com.tmx.beng.medaccess.MediumConnection;

public class CitypayGateMediumConnection implements MediumConnection {
    private CitypayGate citypayGate = null;

    CitypayGateMediumConnection() throws HttpsGateException {
        citypayGate = CitypayGate.getInstance();
    }

    public void send(BillingMessage msg) {
        citypayGate.enqueueBillMessage(msg);
    }
}