package com.tmx.web.forms.core.dictionary.voucher;

import com.tmx.web.base.BasicActionForm;
import com.tmx.web.controls.EditBox;
import com.tmx.web.controls.TimeSelector;
import com.tmx.web.controls.Button;
import com.tmx.as.entities.bill.voucher.Voucher;

public class InfoVoucherForm extends BasicActionForm {
    Long voucherId;

    EditBox editBoxVoucherId,
            editBoxCode,
            editBoxNominal,
            editBoxPrice,
            editBoxVoucherStatus,
            editBoxSecretCode,
            editBoxSoldInBillTransactionNum;
    TimeSelector timeSelectorLoadingTime,
                timeSelectorSoldTime,
                timeSelectorBestBeforeDate,
                timeSelectorReturnTime;

    Button buttonBack;

    protected void init() {
        initBoxes();
        initTimeSelectors();
        initButtons();
    }

    private void initBoxes() {
        editBoxVoucherId = new EditBox();
        editBoxVoucherId.setReadonly(true);

        editBoxCode = new EditBox();
        editBoxCode.setReadonly(true);

        editBoxNominal = new EditBox();
        editBoxNominal.setReadonly(true);

        editBoxPrice = new EditBox();
        editBoxPrice.setReadonly(true);

        editBoxVoucherStatus = new EditBox();
        editBoxVoucherStatus.setReadonly(true);

        editBoxSoldInBillTransactionNum = new EditBox();
        editBoxSoldInBillTransactionNum.setReadonly(true);

        editBoxSecretCode = new EditBox();
        editBoxSecretCode.setReadonly(true);
    }

    private void initTimeSelectors() {
        timeSelectorLoadingTime = new TimeSelector();
        timeSelectorLoadingTime.setReadonly(true);

        timeSelectorSoldTime = new TimeSelector();
        timeSelectorSoldTime.setReadonly(true);

        timeSelectorBestBeforeDate = new TimeSelector();
        timeSelectorBestBeforeDate.setReadonly(true);

        timeSelectorReturnTime = new TimeSelector();
        timeSelectorReturnTime.setReadonly(true);
    }

    private void initButtons() {
        buttonBack = new Button();
        buttonBack.setLabelKey("ctrl.button.label.cancel");
        buttonBack.setAction("/core/dictionary/voucher/manageVouchers");
    }

    public void bindData(Voucher voucher) {
        editBoxVoucherId.setValue(voucherId.toString());
        editBoxCode.setValue(voucher.getCode());
        editBoxNominal.setValue(voucher.getNominal());
        editBoxPrice.setValue(voucher.getPrice().toString());
        editBoxVoucherStatus.setValue(voucher.getStatus());
        editBoxSoldInBillTransactionNum.setValue(voucher.getSoldInBillTransactionNum());
        editBoxSecretCode.setValue(voucher.getSecretCode());

        timeSelectorLoadingTime.setTime(voucher.getLoadingTime());
        timeSelectorSoldTime.setTime(voucher.getSoldTime());
        timeSelectorBestBeforeDate.setTime(voucher.getBestBeforeDate());
    }
                                                                                                             
    public EditBox getEditBoxVoucherStatus() {
        return editBoxVoucherStatus;
    }

    public void setEditBoxVoucherStatus(EditBox editBoxVoucherStatus) {
        this.editBoxVoucherStatus = editBoxVoucherStatus;
    }

    public Long getVoucherId() {
        return voucherId;
    }

    public void setVoucherId(Long voucherId) {
        this.voucherId = voucherId;
    }

    public EditBox getEditBoxVoucherId() {
        return editBoxVoucherId;
    }

    public void setEditBoxVoucherId(EditBox editBoxVoucherId) {
        this.editBoxVoucherId = editBoxVoucherId;
    }

    public EditBox getEditBoxCode() {
        return editBoxCode;
    }

    public void setEditBoxCode(EditBox editBoxCode) {
        this.editBoxCode = editBoxCode;
    }

    public TimeSelector getTimeSelectorBestBeforeDate() {
        return timeSelectorBestBeforeDate;
    }

    public void setTimeSelectorBestBeforeDate(TimeSelector timeSelectorBestBeforeDate) {
        this.timeSelectorBestBeforeDate = timeSelectorBestBeforeDate;
    }

    public EditBox getEditBoxNominal() {
        return editBoxNominal;
    }

    public void setEditBoxNominal(EditBox editBoxNominal) {
        this.editBoxNominal = editBoxNominal;
    }

    public EditBox getEditBoxPrice() {
        return editBoxPrice;
    }

    public void setEditBoxPrice(EditBox editBoxPrice) {
        this.editBoxPrice = editBoxPrice;
    }

    public EditBox getEditBoxSoldInBillTransactionNum() {
        return editBoxSoldInBillTransactionNum;
    }

    public void setEditBoxSoldInBillTransactionNum(EditBox editBoxSoldInBillTransactionNum) {
        this.editBoxSoldInBillTransactionNum = editBoxSoldInBillTransactionNum;
    }

    public TimeSelector getTimeSelectorLoadingTime() {
        return timeSelectorLoadingTime;
    }

    public void setTimeSelectorLoadingTime(TimeSelector timeSelectorLoadingTime) {
        this.timeSelectorLoadingTime = timeSelectorLoadingTime;
    }

    public TimeSelector getTimeSelectorSoldTime() {
        return timeSelectorSoldTime;
    }

    public void setTimeSelectorSoldTime(TimeSelector timeSelectorSoldTime) {
        this.timeSelectorSoldTime = timeSelectorSoldTime;
    }

    public TimeSelector getTimeSelectorReturnTime() {
        return timeSelectorReturnTime;
    }

    public void setTimeSelectorReturnTime(TimeSelector timeSelectorReturnTime) {
        this.timeSelectorReturnTime = timeSelectorReturnTime;
    }

    public Button getButtonBack() {
        return buttonBack;
    }

    public void setButtonBack(Button buttonBack) {
        this.buttonBack = buttonBack;
    }

    public EditBox getEditBoxSecretCode() {
        return editBoxSecretCode;
    }

    public void setEditBoxSecretCode(EditBox editBoxSecretCode) {
        this.editBoxSecretCode = editBoxSecretCode;
    }
}

