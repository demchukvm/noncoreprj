package com.tmx.beng.httpsgate.dacard.pdu;

import java.io.OutputStream;
import java.io.InputStream;

/**
 */
public class EmptyTypePAD implements TypePAD{

    public void serialize(OutputStream os, Object value) throws DacardPadException {
        throw new DacardPadException("ID_TYPE_EMPTY reserved and not implemented");
    }

    public Object parse(InputStream is, short dataSize) throws DacardPadException {
        throw new DacardPadException("ID_TYPE_EMPTY reserved and not implemented");
    }

    public void serializeLengthInBytes(OutputStream os, Object value) throws DacardPadException{
        throw new DacardPadException("ID_TYPE_EMPTY reserved and not implemented");
    }

}
