package com.tmx.as.base.jdbc;

import java.sql.Array;
import java.sql.Connection;
import java.sql.SQLException;

/**
    This jdbc adapter used if no appropriate adapter found by JdbcAdapterFactory.
    It is impossible to implement JdbcAdapterSpi without underlie driver
    so this implementaion just throws DatabaseException on each SPI method. 
 */
public class VoidJdbcAdapter extends BasicJdbcAdapter implements JdbcAdapter, JdbcAdapterSpi{

    public Array adoptArray(Object[] array, String typeName, Connection jdbcConn) throws SQLException {
        throw new SQLException("VoidJdbcAdapter: Array adaptation method is not supported");
    }
}
