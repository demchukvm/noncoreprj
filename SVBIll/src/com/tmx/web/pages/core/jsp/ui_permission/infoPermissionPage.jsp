<%@ page language="java" pageEncoding="UTF-8" contentType="text/html; charset=utf-8" isELIgnored="false" %>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-nested" prefix="nested" %>
<%@ taglib uri="http://tmx.com/taglib/tmx-controls" prefix="tmx_ctrl" %>
<%@ taglib uri="http://tmx.com/taglib/tmx-logic" prefix="tmx_logic" %>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html" %>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean" %>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<table width="100%" border="0">
    <!-- BUTTONS -->
    <tr>
        <td style="text-align: left">
            <tmx_ctrl:button property="buttonApply"/>
            <tmx_ctrl:button property="buttonSave"/>
            <tmx_ctrl:button property="buttonReset" onClick="javascript:document.forms[0].reset()"/>
            <tmx_ctrl:button property="buttonCancel"/>
            <tmx_ctrl:writeActionStatus listener="apply"/>
        </td>
    </tr>
    <tr>
        <!--InfoTaskParamStringForm info-->
        <td valign="center" align="center" class="tabForm">
            <table width="100%" border="0">
                <%--<!-- Form header -->--%>
                <tr>
                    <td colspan="2">
                        <h4 class="dataLabel">
                            <bean:message bundle="core.labels" key="core.page.ui_permission.infoPermission.title"/>
                        </h4>
                    </td>
                </tr>
                <tr>
                    <!-- permissionId -->
                    <td class="dataLabel">
                        <tmx_ctrl:writeEntityLabel attrName="permissionId"
                                                   entityClassName="com.tmx.as.entities.general.ui_permission.Permission"/>:
                    </td>
                    <td class="dataField">
                        <tmx_ctrl:editBox property="permissionId"/>
                    </td>
                    <!-- role -->
                    <td class="dataLabel">
                        <tmx_ctrl:writeEntityLabel attrName="role"
                                                   entityClassName="com.tmx.as.entities.general.ui_permission.Permission"/>:
                    </td>
                    <td class="dataField">
                        <tmx_ctrl:selectBox property="role"/>
                    </td>
                </tr>
                <tr>
                    <!-- userInterface -->
                    <td class="dataLabel">
                        <tmx_ctrl:writeEntityLabel attrName="userInterface"
                                                   entityClassName="com.tmx.as.entities.general.ui_permission.Permission"/>:
                    </td>
                    <td class="dataField">
                        <tmx_ctrl:browseBox property="userInterface" width="290px"/>
                    </td>
                    <!-- permissionType -->
                    <td class="dataLabel">
                        <tmx_ctrl:writeEntityLabel attrName="permissionType"
                                                   entityClassName="com.tmx.as.entities.general.ui_permission.Permission"/>:
                    </td>
                    <td class="dataField">
                        <tmx_ctrl:selectBox property="permissionType"/>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <!-- BUTTONS -->
    <tr>
        <td style="text-align: left">
            <tmx_ctrl:button property="buttonApply"/>
            <tmx_ctrl:button property="buttonSave"/>
            <tmx_ctrl:button property="buttonReset" onClick="javascript:document.forms[0].reset()"/>
            <tmx_ctrl:button property="buttonCancel"/>
        </td>
    </tr>
</table>
