package com.tmx.beng.ussdgate;

import com.tmx.util.queue.ActionQueue;
import com.tmx.util.InitException;
import com.tmx.beng.base.BillingMessage;
import com.tmx.beng.base.BillingMessageImpl;
import com.tmx.beng.base.BasicGate;
import com.tmx.beng.base.StatusDictionary;
import com.tmx.beng.access.Access;
import com.tmx.gate.datastream.base.GateContext;
import com.tmx.as.entities.ussd.transaction.UssdTransaction;
import com.tmx.as.base.EntityManager;
import com.tmx.as.base.FilterWrapper;
import com.tmx.as.base.OrderWrapper;
import com.tmx.as.exceptions.DatabaseException;
import org.apache.log4j.Logger;
import org.apache.catalina.util.Base64;

import java.net.Socket;
import java.io.InputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.regex.Pattern;
import java.util.regex.Matcher;
import java.util.*;


public class UssdGate extends BasicGate {
    // properties names constants
    private static final String USSD_GATE_NAME = "ussd_gate";
    private static final String USSD_RESERVE_GATE_NAME = "ussd_gate_reserve";

    private static Logger logger = Logger.getLogger("gate." + USSD_GATE_NAME);
//    private static Logger loggerReserve = Logger.getLogger("gate." + USSD_GATE_NAME);

    private static Hashtable gates = new Hashtable();
    private Access access = null;
    private ConnectThread connectThread = new ConnectThread();
    private boolean bShutdown = false;
    private boolean Initialized = false;

    private String currentName;

    // regexp pattern constants
    private static final Pattern mainSplitPattern = Pattern.compile("[^\\(]*\\(([^\\|\\(\\)]+)\\|([0-9]+)(\\|([^\\)\\(]+))?\\)(.*)", Pattern.DOTALL);
    private static final Pattern subSplitPattern = Pattern.compile("\\|");

    // queue
    private ActionQueue messageQueue = null;

    // properties names constants
    private static final String PROP_HOST = "ho" +
            "st";
    private static final String PROP_PORT = "port";
    private static final String PROP_CLIENTID = "client_id";

    // command ids
    public static final int CMD_INITIALIZE = 0;
    public static final int CMD_ABONENT_REFILL = 1;
    public static final int CMD_MODEM_REFILL = 2;
    public static final int CMD_MODEM_BALLANCE = 3;
    public static final int CMD_ASK_MODEM_REFILL = 4;
    public static final int CMD_REFILL_TRANSACTION = 6;

    public static final int USSD_STATUS_OK = 100;



    public static UssdGate getInstance(String name)throws UssdGateException {
        if(gates.isEmpty()){
            logger.error("Attempt to obtain not inited billing engine");
            throw new UssdGateException("err.ussd_gate.null_instance");
        }
        return (UssdGate) gates.get(name);
    }

    public static void init() throws InitException {
        logger.info("Start USSD Gate initialization");
        UssdGate ussdGate = new UssdGate();
        ussdGate.messageQueue = new ActionQueue(1,5);
        ussdGate.initByName(USSD_GATE_NAME);
        gates.put(USSD_GATE_NAME, ussdGate);

        UssdGate ussdGateReserve = new UssdGate();
        ussdGateReserve.messageQueue = new ActionQueue(1,5);
        ussdGateReserve.initByName(USSD_RESERVE_GATE_NAME);
        gates.put(USSD_RESERVE_GATE_NAME, ussdGateReserve);
        logger.info("USSD Gate initialized");
    }

    public static void shutdown(boolean abort, final long timeout) throws InitException {
        logger.info("Start USSD Gate Shutdown");
        
        final UssdGate ussdGate = (UssdGate) gates.get(USSD_GATE_NAME);
        if(ussdGate.messageQueue.actionCount() == 0 || abort){
            ussdGate.bShutdown = true;
        }else{
            Runtime.getRuntime().addShutdownHook(new Thread(){
                public void run(){
                    try{
                        long lt = System.currentTimeMillis();
                        while(ussdGate.messageQueue.actionCount() != 0 &&
                            ((System.currentTimeMillis() - lt) < timeout)){
                            sleep(1000);
                        }
                        ussdGate.bShutdown = true;
                    }catch(Throwable e){
                        e.printStackTrace();
                    }
                }
            });
        }

//        final UssdGate ussdGateReserve = (UssdGate) gates.get(USSD_GATE_NAME);
//        if(ussdGateReserve.messageQueue.actionCount() == 0 || abort){
//            ussdGateReserve.bShutdown = true;
//        }else{
//            Runtime.getRuntime().addShutdownHook(new Thread(){
//                public void run(){
//                    try{
//                        long lt = System.currentTimeMillis();
//                        while(ussdGateReserve.messageQueue.actionCount() != 0 &&
//                            ((System.currentTimeMillis() - lt) < timeout)){
//                            sleep(1000);
//                        }
//                        ussdGateReserve.bShutdown = true;
//                    }catch(Throwable e){
//                        e.printStackTrace();
//                    }
//                }
//            });
//        }
        logger.info("USSD Gate shutdown");
    }

    private void initByName(String name)throws InitException{
        properties = GateContext.getInstance().getDatastreamProcessorConfig(name);
        access = obtainBillingEngineAccess();
        connectThread.start();
        currentName = name;
    }

    public void enqueueBillMessage(BillingMessage billMessage){
        UssdGateTask task = new UssdGateTask(billMessage, currentName);
        messageQueue.enqueue(task);
    }

    public void runBillMessage(BillingMessage billingMessage) throws UssdGateException{

        // refill payment
        if(BillingMessage.O_REFILL_PAYMENT.equals(billingMessage.getOperationName())){
            UssdTransaction ussdTransaction = new UssdTransaction();
            ussdTransaction.setAmount(new Double(billingMessage.getAttributeString(BillingMessage.AMOUNT)));
            ussdTransaction.setMsisdn(billingMessage.getAttributeString(BillingMessage.ACCOUNT_NUMBER));
            ussdTransaction.setServiceCode(billingMessage.getAttributeString(BillingMessage.SERVICE_CODE));
            ussdTransaction.setBillTransactionNum(billingMessage.getAttributeString(BillingMessage.BILL_TRANSACTION_NUM));
            ussdTransaction.setRequestId(billingMessage.getAttributeString(BillingMessage.REQUEST_ID));
            ussdTransaction.setBeginDate(new Date());
            try{
                new EntityManager().SAVE(ussdTransaction);
            }catch(DatabaseException e){
                e.printStackTrace();
            }

            connectThread.sendCommand(new Token(
                    billingMessage.getAttributeString(BillingMessage.REQUEST_ID),
                    CMD_ABONENT_REFILL,
                    new String[]{
                            billingMessage.getAttributeString(BillingMessage.ACCOUNT_NUMBER),
                            billingMessage.getAttributeString(BillingMessage.AMOUNT),
                            billingMessage.getAttributeString(BillingMessage.SERVICE_CODE)
                    }
            ));
        }else{
            throw new UssdGateException("Unsupported command");
        }

    }

    /* token */
    private class Token{
        public String requestId;
        public int commandId;
        public String[] items;

        public Token(String reqId, int commId, String[] items){
            this.requestId = reqId;
            this.items = items;
            this.commandId = commId;
        }
    }


    /* connection thread */
    private class ConnectThread extends Thread{
        private Socket socket = null;
        private StringBuffer stringBuffer = new StringBuffer();
        private OutputStream outputStream = null;

        public void run(){
            byte[] buf = new byte[255];
            int rd;

            try{
                String host = properties.getProperty(PROP_HOST, "localhost");
                int port = Integer.parseInt(properties.getProperty(PROP_PORT, "2544"));
                String clientId = properties.getProperty(PROP_CLIENTID, "0");

                while(!bShutdown){
                    try{
                        Initialized = false;
                        while(socket == null || !socket.isConnected() || socket.isClosed()){
                            try{
                                socket = new Socket(host, port);
                                if(socket.isConnected()) break;
                            }catch(Throwable e){
                                logger.error(e.getLocalizedMessage());
                            }
                            sleep(1000);
                        }

                        InputStream is = socket.getInputStream();
                        outputStream = socket.getOutputStream();

                        // send initialization
                        sendCommand(new Token(
                                "0",
                                CMD_INITIALIZE,
                                new String[]{
                                        clientId
                                }), true);

                        do{
                            rd = is.read(buf);
                            if(rd > 0){
                                onReadData(new String(buf, 0, rd));
                            }
                        }while(rd >=0);

                    }catch(IOException e){
                        logger.info(e.getLocalizedMessage() + " host=" + host + " port=" + port, e);
                    }catch(UssdGateException e){
                        logger.info(e.getLocalizedMessage() + " host=" + host + " port=" + port, e);
                    }

                    if(socket != null && !socket.isClosed()){
                        try{
                            socket.close();
                        }catch(IOException e){
                            logger.error(e);
                        }
                    }

                }
            }catch(InterruptedException e){
                logger.error("ConnectThread was interrupted", e);
            }

        }

        private void onReadData(String readData){
            stringBuffer.append(readData);

            Matcher matcher = mainSplitPattern.matcher(stringBuffer.toString());
            while(matcher.matches()){
                stringBuffer.delete(matcher.start(),matcher.start(5));
                String[] items = subSplitPattern.split(matcher.group(4));
                onReadToken(new Token(matcher.group(1),// requestId
                        Integer.parseInt(matcher.group(2)), // command Id
                        items)); // additional params
                matcher = mainSplitPattern.matcher(stringBuffer.toString());
            }
        }


        private void onReadToken(Token token){
            switch(token.commandId){
                case CMD_INITIALIZE:
                {

                    Initialized = true;
                }
                    break;
                case CMD_ABONENT_REFILL:
                {
                    try{
                        BillingMessageImpl billingMessage = new BillingMessageImpl();
                        billingMessage.setAttributeString(BillingMessage.REQUEST_ID, token.requestId);
                        billingMessage.setOperationName(BillingMessage.O_REFILL_PAYMENT_RESULT);
                        int statusCode = Integer.parseInt(token.items[0]);
                        billingMessage.setStatusCode(statusCode == USSD_STATUS_OK ? StatusDictionary.STATUS_OK : statusCode);
                        String statusMessage = null;
                        if(statusCode != USSD_STATUS_OK && token.items.length > 1){
                            statusMessage = new String(Base64.decode(token.items[1].getBytes()));
                            billingMessage.setAttributeString(BillingMessage.STATUS_MESSAGE, statusMessage);
                        }
                        access.obtainConnection().processAsync(billingMessage);

                        // update ussd transaction
                        // search transaction by
                        EntityManager entityManager = new EntityManager();
                        FilterWrapper by_request = new FilterWrapper("by_request");
                        by_request.setParameter("request", token.requestId);
                        UssdTransaction ussdTransaction = (UssdTransaction)entityManager
                                .RETRIEVE(UssdTransaction.class, new FilterWrapper[]{ by_request }, null);
                        if(ussdTransaction != null){
                            ussdTransaction.setCommitDate(new Date());
                            ussdTransaction.setStatusCode(new Integer(billingMessage.getAttributeString(BillingMessage.STATUS_CODE)));
                            ussdTransaction.setStatusMessage(statusMessage);
                            entityManager.SAVE(ussdTransaction);
                        }else{
                            logger.error("Transaction not found. RequestId=" + token.requestId);
                        }
                    }catch(Throwable e){
                        logger.error(e.getLocalizedMessage(), e);
                    }
                }
                    break;
                case CMD_MODEM_REFILL:
                {

                }
                    break;
                case CMD_MODEM_BALLANCE:
                {

                }
                    break;
                case CMD_ASK_MODEM_REFILL:
                {

                }
                    break;
                case CMD_REFILL_TRANSACTION:
                {
                    OnReceiveTransaction(token);
                }
                    break;
                default:
                    logger.error("Uncknown command from USSDGateService #" + token.commandId);
                    break;
            }
        }

        private void OnReceiveTransaction(Token token){
            try{
                EntityManager entityManager = new EntityManager();
                String msisdn = token.items[0];
                Double amount = new Double(token.items[1]);
                String transaction = token.items[2];

                // search transaction
                FilterWrapper by_begin_date = new FilterWrapper("by_begin_date");
                Calendar calendar = GregorianCalendar.getInstance();
                calendar.add(Calendar.DAY_OF_MONTH, -1);
                by_begin_date.setParameter("date", calendar.getTime());
                FilterWrapper by_msisdn = new FilterWrapper("by_msisdn");
                by_msisdn.setParameter("msisdn", msisdn);
                FilterWrapper by_amount = new FilterWrapper("by_amount");
                by_amount.setParameter("amount", amount);
                FilterWrapper by_status = new FilterWrapper("by_status");
                by_status.setParameter("code", new Integer(StatusDictionary.STATUS_OK));
                FilterWrapper by_conf_trans_null = new FilterWrapper("by_conf_trans_null");

                List list = entityManager
                        .RETRIEVE_ALL(UssdTransaction.class, new FilterWrapper[]{
                                by_begin_date,
                                by_msisdn,
                                by_amount,
                                by_status,
                                by_conf_trans_null
                        },
                        new OrderWrapper[]{
                                new OrderWrapper("beginDate", OrderWrapper.ASC),
                        });
                UssdTransaction ussdTransaction;
                if(list == null || list.size() == 0){
                    ussdTransaction = new UssdTransaction();
                }else{
                    ussdTransaction = (UssdTransaction)list.get(0);
                }

                // save
                ussdTransaction.setConfirmDate(new Date());
                ussdTransaction.setConfirmTransaction(transaction);
                ussdTransaction.setMsisdn(msisdn);
                ussdTransaction.setAmount(amount);
                entityManager.SAVE(ussdTransaction);
            }catch(Throwable e){
                e.printStackTrace();
                logger.error(e.getLocalizedMessage());
            }
        }

        public void sendCommand(Token token) throws UssdGateException{
            sendCommand(token, false);
        }

        private void sendCommand(Token token, boolean bSkipInitializationTest) throws UssdGateException{
            try{
                if(!bSkipInitializationTest){
                    while(!Initialized){
                        sleep(500);
                    }
                }
                StringBuffer tmp = new StringBuffer();
                tmp.append("(");
                tmp.append(token.requestId).append("|");
                tmp.append(token.commandId);
                for(int i = 0; i < token.items.length; i++){
                    tmp.append("|").append(token.items[i]);
                }
                tmp.append(")");

                outputStream.write(tmp.toString().getBytes());
                outputStream.flush();
            }catch(IOException e){
                throw new UssdGateException(e.getLocalizedMessage(), e);
            }catch(Throwable e){
                throw new UssdGateException(e.getLocalizedMessage(), e);
            }
        }

        public void test(){
            OnReceiveTransaction(new Token("sa",1,new String[]{
                    "380679317664",
                    "5",
                    "9713465263984"
            }));
        }
    }


    public void test(){
        connectThread.test();
    }
}
