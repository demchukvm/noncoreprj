package com.tmx.beng.base.cache;

import com.tmx.as.base.EntityManager;
import com.tmx.as.base.FilterWrapper;
import com.tmx.as.entities.bill.balance.TerminalBalance;
import com.tmx.as.entities.bill.restriction.TerminalRestriction;
import com.tmx.as.entities.bill.tariff.TerminalTariff;
import com.tmx.as.entities.bill.terminal.Terminal;
import com.tmx.as.exceptions.DatabaseException;
import com.tmx.util.InitException;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 */
class Terminals {
    Map terminalId_terminal = new HashMap();
    Map terminalSN_terminal = new HashMap();

    Terminals(TerminalGroups terminalGroups, Sellers sellers)throws DatabaseException, InitException {
        EntityManager entityManager = new EntityManager();

        // load terminals
        Iterator it;
        TerminalWrapper wrapper;
        List list = entityManager.RETRIEVE_ALL(Terminal.class, (FilterWrapper[]) null, new String[]{"primaryBalance"});
        for(it = list.iterator(); it.hasNext();){
            Terminal terminal = (Terminal)it.next();
            wrapper = new TerminalWrapper(terminal);
            terminalId_terminal.put(terminal.getTerminalId(), wrapper);
            terminalSN_terminal.put(terminal.getSerialNumber(), wrapper);
            SellerWrapper sellerWrapper = terminal.getSellerOwnerId() != null ?
                    (SellerWrapper)sellers.sellerId_seller.get(terminal.getSellerOwnerId()) :
                    null;
            if(sellerWrapper != null)
                sellerWrapper.terminalId_terminal.put(terminal.getTerminalId(), wrapper);

            TerminalGroupWrapper terminalGroupWrapper = terminal.getTerminalGroupId() != null ?
                    (TerminalGroupWrapper)terminalGroups.terminalGroupId_terminalGroup.get(terminal.getTerminalGroupId()) :
                    null;
            if(terminalGroupWrapper != null)
                terminalGroupWrapper.terminalId_terminal.put(terminal.getTerminalId(), wrapper);
        }

        // load terminal ballance
        list  = entityManager.RETRIEVE_ALL(TerminalBalance.class, null);
        for(it = list.iterator(); it.hasNext();){
            TerminalBalance ballance = (TerminalBalance)it.next();
            wrapper = (TerminalWrapper)terminalId_terminal.get(ballance.getTerminalId());
            if(wrapper != null && ballance.getBalanceId() != null && ballance.getCode() != null){
                wrapper.balanceId_balance.put(ballance.getBalanceId(), ballance);
                wrapper.balanceCode_balanceId.put(ballance.getCode(), ballance.getBalanceId());
            }
        }

        // load terminals tariff
        list  = entityManager.RETRIEVE_ALL(TerminalTariff.class, (FilterWrapper[]) null, new String[]{"functionInstance"});
        for(it = list.iterator(); it.hasNext();){
            TerminalTariff tariff = (TerminalTariff)it.next();
            wrapper = (TerminalWrapper)terminalId_terminal.get(tariff.getTerminalId());
            if(wrapper != null){
                wrapper.tariffId_tariffEntity.put(tariff.getTariffId(), tariff);

                FunctionWrapper functionWrapper = new FunctionWrapper(tariff.getFunctionInstance());
                wrapper.tariffId_tariffFunctionInstance.put(tariff.getTariffId(), functionWrapper);
                //cache tariff by name if name is not null
//                if(tariff.getName() != null){
//                    if(wrapper.tariffName_tariffFunctionInstance.containsKey(tariff.getName()))
//                        throw new InitException("Duplicate tariff name for terminal", new String[]{tariff.getName(), wrapper.getTerminal().getSerialNumber()});
//                    wrapper.tariffName_tariffFunctionInstance.put(tariff.getName(), functionWrapper);
//                }
            }
        }

        // load terminal restrictions
        list  = entityManager.RETRIEVE_ALL(TerminalRestriction.class, (FilterWrapper[]) null, new String[]{"functionInstance"});
        for(it = list.iterator(); it.hasNext();){
            TerminalRestriction restriction = (TerminalRestriction)it.next();
            wrapper = (TerminalWrapper)terminalId_terminal.get(restriction.getTerminalId());
            if(wrapper != null)
                wrapper.restriction.add(new FunctionWrapper(restriction.getFunctionInstance()));
        }
/*        list = entityManager.EXECUTE_QUERY(TerminalRestriction.class, "retreiveAll", null);
        TerminalRestrictionWrapper restriction = null;
        for(it = list.iterator(); it.hasNext();){
            Object[] objects = (Object[])it.next();
            TerminalRestriction terminalRestriction = (TerminalRestriction)objects[0];
            if(restriction == null || terminalRestriction != restriction.restriction){
                if(restriction != null){
                    wrapper = (TerminalWrapper)terminalId_terminal.get(terminalRestriction.getTerminalId());
                    if(wrapper != null){
                        wrapper.restriction.add(restriction);
                    }
                }
                restriction = new TerminalRestrictionWrapper((TerminalRestriction)objects[0]);
                restriction.function = (FunctionInstance)objects[1];
            }
            restriction.addFunctionParam((ActualFunctionParameter)objects[3]);
        }
        if(restriction != null){
            wrapper = (TerminalWrapper)terminalId_terminal.get(restriction.restriction.getTerminalId());
            if(wrapper != null){
                wrapper.restriction.add(restriction);
            }
        }*/
    }

    public Terminal getTerminal(Long id) {
        if(id == null)
            return null;
        TerminalWrapper wrapper = (TerminalWrapper)terminalId_terminal.get(id);
        if(wrapper == null)
            return null;
        return wrapper.getTerminal();
    }

    public TerminalWrapper getTerminalWrapper(String terminalSN){
        return (TerminalWrapper)terminalSN_terminal.get(terminalSN);
    }

    public Terminal getTerminal(String terminalSN) {
        TerminalWrapper wrapper = getTerminalWrapper(terminalSN); 
        if(wrapper == null) return null;
        return wrapper.getTerminal();
    }
}
