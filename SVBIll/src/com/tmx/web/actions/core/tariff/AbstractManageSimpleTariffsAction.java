package com.tmx.web.actions.core.tariff;

import com.tmx.web.forms.core.tariff.AbstractManageSimpleTariffsForm;
import com.tmx.web.base.Roadmap;
import com.tmx.web.controls.ScrollBox;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


abstract public class AbstractManageSimpleTariffsAction extends AbstractManageTariffsAction{

    public ActionForward preloadForCopy(ActionMapping mapping,
                                       ActionForm form,
                                       HttpServletRequest request,
                                       HttpServletResponse response) throws Throwable {
        AbstractManageSimpleTariffsForm tariffsForm = (AbstractManageSimpleTariffsForm) verifyForm(form, AbstractManageSimpleTariffsForm.class);
        tariffsForm.copySelectedTariff();
        return defaultAction(mapping, form, request, response, null);
    }

    public ActionForward preloadForCopyAll(ActionMapping mapping,
                                       ActionForm form,
                                       HttpServletRequest request,
                                       HttpServletResponse response) throws Throwable {
        AbstractManageSimpleTariffsForm tariffsForm = (AbstractManageSimpleTariffsForm) verifyForm(form, AbstractManageSimpleTariffsForm.class);
        tariffsForm.copyAllTariffs(getLoggedInUser(request));
        return defaultAction(mapping, form, request, response, null);
    }

    public ActionForward preloadForDelete(ActionMapping mapping,
                                          ActionForm form,
                                          HttpServletRequest request,
                                          HttpServletResponse response) throws Throwable {
        Roadmap roadmap=getRoadmap();
        roadmap.collectParameter("/core/message/confirmation", "leftMenuName", "tariffs");
        roadmap.setPageForward("confirmation","onOk","local:"+this.getName()+"#"+"deleteTariffAction");
        roadmap.setPageForward("confirmation","onCancel","local:"+this.getName()+"#"+ "manageTariffsAction");
        return mapping.findForward("confirmation");
    }

    //preload for delete incoming tariff
    public ActionForward preloadForDeleteIT(ActionMapping mapping,
                                          ActionForm form,
                                          HttpServletRequest request,
                                          HttpServletResponse response) throws Throwable {
        Roadmap roadmap=getRoadmap();
        roadmap.collectParameter("/core/message/confirmation", "leftMenuName", "tariffs");
        roadmap.setPageForward("confirmation","onOk","local:"+this.getName()+"#"+"deleteIncomingTariffAction");
        roadmap.setPageForward("confirmation","onCancel","local:"+this.getName()+"#"+ "manageTariffsAction");
        return mapping.findForward("confirmation");
    }

    public ActionForward deleteIncomingTariff(ActionMapping mapping,
                              ActionForm form,
                              HttpServletRequest request,
                              HttpServletResponse response) throws Throwable {
        AbstractManageSimpleTariffsForm tariffsForm = (AbstractManageSimpleTariffsForm) form;
        getTariffManager().deleteTariff(tariffsForm.getIncomingTariffId());
        return defaultAction(mapping, form, request, response, null);
    }

    public ActionForward delete(ActionMapping mapping,
                              ActionForm form,
                              HttpServletRequest request,
                              HttpServletResponse response) throws Throwable {
        AbstractManageSimpleTariffsForm tariffsForm = (AbstractManageSimpleTariffsForm) form;
        getTariffManager().deleteTariff(tariffsForm.getTariffId());
        return defaultAction(mapping, form, request, response, null);
    }

    public ActionForward refreshTariffsTable(ActionMapping mapping,
                              ActionForm form,
                              HttpServletRequest request,
                              HttpServletResponse response) throws Throwable {
        AbstractManageSimpleTariffsForm tariffsForm = (AbstractManageSimpleTariffsForm) verifyForm(form, AbstractManageSimpleTariffsForm.class);
        tariffsForm.getTariffsTable().refresh();
        changeButtonVisibility(tariffsForm);
        return mapping.findForward("manageTariffsPage");
    }

    public ActionForward refreshIncomingTariffsTable(ActionMapping mapping,
                              ActionForm form,
                              HttpServletRequest request,
                              HttpServletResponse response) throws Throwable {
        AbstractManageSimpleTariffsForm tariffsForm = (AbstractManageSimpleTariffsForm) verifyForm(form, AbstractManageSimpleTariffsForm.class);
        tariffsForm.getIncomingTriffsTable().refresh();
        changeButtonVisibility(tariffsForm);
        return mapping.findForward("manageTariffsPage");
    }

    public ActionForward defaultAction(ActionMapping mapping, ActionForm form, HttpServletRequest request,
                                       HttpServletResponse response, String methodName) throws Throwable {
        AbstractManageSimpleTariffsForm tariffsForm = (AbstractManageSimpleTariffsForm) verifyForm(form, AbstractManageSimpleTariffsForm.class);
        tariffsForm.getIncomingTriffsTable().refresh();
        changeButtonVisibility(tariffsForm);
        return super.defaultAction(mapping, form, request, response, methodName);
    }

    private void changeButtonVisibility(AbstractManageSimpleTariffsForm tariffsForm) {
        ScrollBox scrollBoxSubSellers = (ScrollBox) tariffsForm.getTariffsTable().getParameterControl("by_seller_id", "seller_id");

        if (scrollBoxSubSellers.isNullKeySelected() || tariffsForm.getIncomingTriffsTable().getData().isEmpty()) {
            tariffsForm.getOnCopyButton().setReadonly(true);
            tariffsForm.getOnCopyAllButton().setReadonly(true);
        } else {
            tariffsForm.getOnCopyButton().setReadonly(false);
            tariffsForm.getOnCopyAllButton().setReadonly(false);
        }

        if (tariffsForm.getIncomingTriffsTable().getData().isEmpty()) {
            tariffsForm.getOnDeleteITButton().setReadonly(true);
            tariffsForm.getOnEditITButton().setReadonly(true);
        } else {
            tariffsForm.getOnDeleteITButton().setReadonly(false);
            tariffsForm.getOnEditITButton().setReadonly(false);
        }

    }


}
