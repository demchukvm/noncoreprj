package com.tmx.web.forms.core.dictionary.processing;

import com.tmx.web.base.BasicActionForm;
import com.tmx.web.controls.Button;
import com.tmx.web.controls.table.ControlManagerTable;

public class ManageProcessingsForm extends BasicActionForm {
    
    private ControlManagerTable processingsTable;
    private Button onCreateButton;
    private Button onEditButton;
    private Button onDeleteButton;

     protected void init() {
         //setup buttons
         String action="/core/dictionary/processing/infoProcessing";
         onCreateButton = new Button();
         onCreateButton.setLabelKey("ctrl.button.label.create");
         onCreateButton.setAction(action);
         onCreateButton.setCommand("preloadForCreate");

         onEditButton = new Button();
         onEditButton.setLabelKey("ctrl.button.label.edit");
         onEditButton.setAction(action);
         onEditButton.setCommand("preloadForUpdate");

         onDeleteButton = new Button();
         onDeleteButton.setLabelKey("ctrl.button.label.delete");
         onDeleteButton.setAction(action);
         onDeleteButton.setCommand("preloadForDelete");

         initTable();
    }

    private void initTable() {
        processingsTable = new ControlManagerTable("query(query_type=retrieve_all," +
                "entity_class=com.tmx.as.entities.bill.processing.Processing," +
                "order(name=id,type=asc)," +
                "page_size=15" +
                ")");
        processingsTable.setRefreshActionName("/core/dictionary/processing/manageProcessings");
        processingsTable.addDependedControl(onEditButton).addDependedControl(onDeleteButton);
    }

    //--------Properties
    public ControlManagerTable getProcessingsTable() {
        return processingsTable;
    }

    public void setProcessingsTable(ControlManagerTable processingsTable) {
        this.processingsTable = processingsTable;
    }

    public Button getOnCreateButton() {
        return onCreateButton;
    }

    public void setOnCreateButton(Button onCreateButton) {
        this.onCreateButton = onCreateButton;
    }

    public Button getOnEditButton() {
        return onEditButton;
    }

    public void setOnEditButton(Button onEditButton) {
        this.onEditButton = onEditButton;
    }

    public Button getOnDeleteButton() {
        return onDeleteButton;
    }

    public void setOnDeleteButton(Button onDeleteButton) {
        this.onDeleteButton = onDeleteButton;
    }

}
