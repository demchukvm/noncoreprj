package com.tmx.beng.iso8583Gate.pdu.substructure.collation;


import com.tmx.beng.iso8583Gate.pdu.substructure.BasicRuntimeSubDE;
import java.util.*;

/**
    Runtime sub data element for collation operations 
 */
public class BasicCollationRSDE extends BasicRuntimeSubDE {
    private Map elements;
    private Set availableElementNames = BasicCollationSDEF.getAvailableElementNames();

    BasicCollationRSDE() {
        this.elements = new HashMap();
    }

    public void addValue(String name, String value) {
        if(availableElementNames.contains(name))
            elements.put(name, value);
    }

    public String getValue(String name) {
        return (String)elements.get(name);
    }
}
