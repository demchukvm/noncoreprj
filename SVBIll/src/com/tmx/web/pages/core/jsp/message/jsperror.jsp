<%@ page import="org.apache.struts.action.ActionErrors" %>
<!--
 This page renders errors occured on JSP level
-->
<%@ page language="java" pageEncoding="UTF-8" contentType="text/html; charset=utf-8" isELIgnored="false" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/tmx-controls.tld" prefix="tmx_ctrl" %>

<html:html>
    <head>
        <title><bean:message bundle="core.labels" key="core.page.message.error.title"/></title>
        <meta http-equiv="expires" content="0">
        <meta http-equiv="Pragma" content="no-cache">
        <meta http-equiv="Cache-Control" content="no-cache">
        <LINK REL="STYLESHEET" HREF="<html:rewrite page='core/styles/common.css'/>" TYPE="text/css">
        <LINK REL="STYLESHEET" HREF="<html:rewrite page='core/styles/ctrl.css'/>" TYPE="text/css">
        <LINK REL="STYLESHEET" HREF="<html:rewrite page='core/styles/ctrl.colors.default.css'/>" TYPE="text/css">
        <LINK REL="STYLESHEET" HREF="<html:rewrite page='core/styles/plain_table.css'/>" TYPE="text/css">
        <script type="text/javascript" src="<html:rewrite page='core/scripts/dhtml.js'/>"></script>
        <script type="text/javascript" src="<html:rewrite page='core/scripts/scroll.js'/>"></script>
        <script type="text/javascript" src="<html:rewrite page='core/scripts/common.js'/>"></script>
        <!-- IMPORTANT: Include after other include scripts
        to prevent overriding of onEvent functons -->
        <script type="text/javascript" src="<html:rewrite page='core/scripts/global.js'/>"></script>

        <!-- news -->
     <%--   <script type="text/javascript" src="<html:rewrite page='core/scripts/news.js'/>"></script> --%>
    </head>

    <body>

    <table border="0" width="100%" height="100%">
        <tr>
            <td valign="center" align="center">

                <table border="0" class="login_lines">
                    <tr>
                        <td align="center">
                            <div class="title"><bean:message bundle="core.labels" key="core.page.message.error.title"/></div>
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <!-- Print errors -->
                            <div class="errors"><html:errors bundle="core.errors" property="<%= ActionErrors.GLOBAL_ERROR %>"/></div>
                        </td>
                    </tr>
                </table>

                <br/>
                <input type="button" class="button" onclick="javascript:history.back()" value="<bean:message bundle="core.labels" key="core.common.button.ok"/>">
            </td>
        </tr>
    </table>
    </body>
</html:html>

