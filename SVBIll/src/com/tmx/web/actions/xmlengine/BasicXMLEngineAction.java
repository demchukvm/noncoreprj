package com.tmx.web.actions.xmlengine;

import com.tmx.web.base.*;
import com.tmx.web.forms.xmlengine.BasicXMLPublisherForm;
import com.tmx.engines.xmlengine.XMLPublisherFactory;
import com.tmx.engines.xmlengine.XMLPublisher;
import com.tmx.engines.xmlengine.XMLPublishingException;
import com.tmx.engines.xmlengine.DocumentHeader;
import com.tmx.as.exceptions.DatabaseException;
import com.tmx.util.StructurizedException;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForm;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.tmx.util.Configuration;


public abstract class BasicXMLEngineAction extends BasicAction {
    private DocumentHeader docHeader;
    private Logger logger = Logger.getLogger("xmlengine.BasicXMLEngineAction");

    public abstract ActionForward executeSpecificXMLEngineAction(ActionMapping mapping,
                                                                 ActionForm form,
                                                                 HttpServletRequest request,
                                                                 HttpServletResponse response) throws Throwable;

    public ActionForward executeSpecificAction(ActionMapping mapping,
                                               ActionForm form,
                                               HttpServletRequest request,
                                               HttpServletResponse response) throws Throwable {
        ActionForward forward = null;
        try {

            populateDocumentHeader0(mapping, form, request, response);

            forward = executeSpecificXMLEngineAction(mapping, form, request, response);
        }
        catch (Throwable e) {
            BasicXMLPublisherForm xmlPublisherForm = null;
            if (form == null || !(form instanceof BasicXMLPublisherForm)) {
                xmlPublisherForm = new BasicXMLPublisherForm();
            }
            else{
                xmlPublisherForm = (BasicXMLPublisherForm)form;
            }
            SessionEnvironment.setAttr(request.getSession(), "xmlPublisherForm", xmlPublisherForm);            
            publishException(e, xmlPublisherForm);
            return (mapping.findForward("xmlPublisher"));
        }
        return forward;
    }


    private void populateDocumentHeader0(ActionMapping mapping,
                                         ActionForm form,
                                         HttpServletRequest request,
                                         HttpServletResponse response) {

        docHeader = new DocumentHeader();
        docHeader.setServerUrl(request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort());
        docHeader.setAppContextName(request.getContextPath());
        docHeader.setLocale(request.getLocale());

        HttpSession httpSession = ((HttpServletRequest)request).getSession();
        UserContextForm userContextForm = (UserContextForm) SessionEnvironment.getAttr(httpSession, SessionEnvironment.USER_CONTEXT_FORM);

        docHeader.setNextPage(userContextForm.getRequestHistory().getNext());
        docHeader.setPrevPage(userContextForm.getRequestHistory().getPrev());
        docHeader.setCurrentPage(userContextForm.getRequestHistory().getCurrent());

    }

    public DocumentHeader getDocumentHeader() {
        return docHeader;
    }

    private void publishException(Throwable error, BasicXMLPublisherForm form) {
        try {
            XMLPublisher xmlPublisher = XMLPublisherFactory.createXMLPublisher();
            getDocumentHeader().setPageName("publishingError");
            org.w3c.dom.Node rootNode = xmlPublisher.initializeDocument(getDocumentHeader());
            StructurizedException structError = null;
            if (error instanceof StructurizedException) {
                structError = (StructurizedException) error;
            } else {
                structError = new StructurizedException("err.xml_publisher.unknown_error", error);
            }

            xmlPublisher.publishError(structError, rootNode);
            form.setXmlDocSerialized(xmlPublisher.serializeDocument(rootNode));
            logger.error(structError.getLocalizedMessage());
        }
        catch (XMLPublishingException e) {
            logger.error(e.toString());
            if (form != null) {
                StringBuffer generalError = new StringBuffer();
                generalError.append("<document>");
                generalError.append("<head>");
                generalError.append("<system name=\"");
                generalError.append(Configuration.getInstance().getProperty("system.name", "unknown"));
                generalError.append("\" version=\"");
                generalError.append(Configuration.getInstance().getProperty("system.version", "unknown"));
                generalError.append("\"/>");
                generalError.append("<page name=\"publishingError\"/>");
                generalError.append("</head>");
                generalError.append("<body>");
                generalError.append("<error name=\"");
                generalError.append(e.getClass().getName());
                generalError.append("\">");
                generalError.append("<![CDATA[");
                generalError.append(e.getLocalizedMessage());
                generalError.append("]]>");
                generalError.append("</error>");
                generalError.append("</body>");
                generalError.append("</document>");

                form.setXmlDocSerialized(generalError.toString());
            }
        }

    }
}