package com.tmx.web.controls;

import com.tmx.util.i18n.MessageResources;

import java.util.Locale;

/**
 Control validation rule
 */
public abstract class ValidationRule {
    /** Message to be shown on validation error */
    private String errorMessageKey;
    /** Indicates state of this validation rule instance.
     * Setup to true if validation() return false. */
    protected boolean validationPassed = true;
    /** Validated control */
    protected BasicControl control = null;
    /** Validation arguments */
    protected Object[] arguments = null;


    protected ValidationRule(String errorMessageKey, Object[] args) {
        this.errorMessageKey = errorMessageKey;
        this.arguments = args;
    }

    void initialize(BasicControl ctrl){
        control = ctrl;
        validationPassed = true;
    }

    /** To call on Form validation. */
    boolean executeValidation(){
        validationPassed = validate(control, arguments);
        return validationPassed;
    }

    //----------For usage on contrlols rendering
    public String getErrorMessageKey() {
        return errorMessageKey;
    }

    public String publishErrorMessage(Locale locale) {
        //convert args to string
        String[] args = null;
        if(arguments != null){
            args = new String[arguments.length];
            for(int i=0; i<arguments.length; i++)
                args[i] = arguments[i] != null ? arguments[i].toString() : null;
        }

        //reset validation error status after publishing
        validationPassed = true;
        
        return MessageResources.getInstance().getOwnedLocalizedMessage(this.getClass(), "validation_msgs", locale, errorMessageKey, args);
    }

    public boolean isValidationPassed() {
        return validationPassed;
    }
    //---------


    /** Method to implement particular validation logic
     * @return false on validation error
     * @param control control to be validated. */
    public abstract boolean validate(BasicControl control, Object[] args);
}
