package com.tmx.web.actions.core.dictionary.terminal;

import com.tmx.web.actions.core.AbstractSvbillAction;
import com.tmx.web.forms.core.dictionary.terminal.ManageTerminalsForm;
import com.tmx.web.forms.core.base.AbstractFormFactory;
import com.tmx.web.controls.table.Parameter;
import com.tmx.web.controls.ScrollBox;
import com.tmx.as.base.EntityManager;
import com.tmx.as.entities.bill.terminal.EquipmentType;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;


public class ManageTerminalsAction extends AbstractSvbillAction {

    private static final String FORWARD_MANAGE_TERMINALS_PAGE = "manageTerminalsPage";

    public ActionForward defaultAction(ActionMapping mapping,
                                       ActionForm form,
                                       HttpServletRequest request,
                                       HttpServletResponse response,
                                       String methodName) throws Throwable {
        ManageTerminalsForm manageTerminalsForm = (ManageTerminalsForm) form;
//        manageTerminalsForm.getTerminalsTable().addParameterToFilter("by_seller_id", new Parameter("seller_id", getLoggedInUser(request).getSellerOwner().getSellerId()));\
        AbstractFormFactory.getInstance(getLoggedInUser(request)).setUpForm(manageTerminalsForm);
        List<EquipmentType> listET  = (new EntityManager()).RETRIEVE_ALL(EquipmentType.class);
        manageTerminalsForm.getScrollBoxEquipmentType().bind(listET);
 //       manageTerminalsForm.getScrollBoxEquipmentType().setNullable(true);
        

        manageTerminalsForm.getTerminalsTable().setRefreshActionName("/core/dictionary/terminal/manageTerminals");
//        manageTerminalsForm.getTerminalsTable().setRefreshCommandName("refreshTable");
        manageTerminalsForm.getTerminalsTable().refresh();

        getRoadmap().collectParameter("/core/dictionary/terminal/infoTerminal",
                                                        "cancelActionFrominfoTerminalForm",
                                                        "/core/dictionary/terminal/manageTerminals");
        getRoadmap().collectParameter("/core/dictionary/terminal/infoTerminal",
                                                        "cancelActionOnDelete",
                                                        "manageTerminalsAction");

        return mapping.findForward(FORWARD_MANAGE_TERMINALS_PAGE);
    }

    public ActionForward refreshTable(ActionMapping mapping,
                                  ActionForm form,
                                  HttpServletRequest request,
                                  HttpServletResponse response) throws Throwable {
        ManageTerminalsForm manageTerminalsForm = (ManageTerminalsForm) form;
        manageTerminalsForm.getTerminalsTable().refresh();

        return mapping.findForward(FORWARD_MANAGE_TERMINALS_PAGE);
    }
}
