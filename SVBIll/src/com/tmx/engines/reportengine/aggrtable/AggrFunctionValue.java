package com.tmx.engines.reportengine.aggrtable;

import java.text.DecimalFormat;
import java.text.FieldPosition;

/**
 * Aggregated funciton value
 */
public class AggrFunctionValue {
    private int span = 1;
    private double value = 0;

    public int getSpan() {
        return span;
    }

    void setSpan(int span) {
        this.span = span;
    }

    public double getValue() {
        return value;
    }

    public String getFormattedValue(){
        //TODO: RM take format from xml template
        DecimalFormat df = new DecimalFormat(",###");
        return df.format(value, new StringBuffer(), new FieldPosition(0)).toString();
    }

    void setValue(double value) {
        this.value = value;
    }
}
