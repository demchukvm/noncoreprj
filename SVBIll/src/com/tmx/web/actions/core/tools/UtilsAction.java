/*//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                        L O C A L     V A R I A N T
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////*/
/*
package com.tmx.web.actions.core.tools;

import com.tmx.beng.base.MediumAccessPool;
import com.tmx.web.actions.core.AbstractSvbillAction;
import com.tmx.web.forms.core.tools.UtilsForm;
import com.tmx.as.blogic.TransactionManager;
import com.tmx.as.blogic.GateTransactionsManager;
import com.tmx.as.exceptions.DatabaseException;
import com.tmx.beng.base.BillingEngine;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForm;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Arrays;

public class UtilsAction extends AbstractSvbillAction
{
    private static final String FORWARD_TRANSACTION_UTILS_PAGE = "transactionUtilsPage";

    public ActionForward defaultAction(ActionMapping mapping,
                                       ActionForm form,
                                       HttpServletRequest request,
                                       HttpServletResponse response,
                                       String methodName) throws Throwable {
        UtilsForm utilsForm = (UtilsForm) form;

        MediumAccessPool mediumAccessPool = BillingEngine.getInstance().getMediumAccessPool();
        utilsForm.getMultipleSelectBoxMediumAccesses().bind(Arrays.asList(mediumAccessPool.getMediumAccessPoolKeys().toArray()));
        utilsForm.getMultipleSelectBoxRemovedElements().bind(Arrays.asList(mediumAccessPool.getRemovedElemetsKeys().toArray()));

        return mapping.findForward(FORWARD_TRANSACTION_UTILS_PAGE);
    }

    private ActionForward preloadForChecking(ActionMapping mapping,
                                             ActionForm form,
                                             HttpServletRequest request,
                                             HttpServletResponse response) throws DatabaseException {

        TransactionManager manager = new TransactionManager();
        HashMap transactions = manager.getProblemTransactions();
        return mapping.findForward(FORWARD_TRANSACTION_UTILS_PAGE);
    }

    private ActionForward getStatus(ActionMapping mapping,
                                             ActionForm form,
                                             HttpServletRequest request,
                                             HttpServletResponse response) throws DatabaseException {

        UtilsForm utilsForm = (UtilsForm) form;

        String[] guids = utilsForm.getEbBillingTransactionNumbers().getValue().replace('.', ',').split(",");
        HashMap transactionsStatuses = (new GateTransactionsManager()).getTransactionByGUID(guids);

        return mapping.findForward(FORWARD_TRANSACTION_UTILS_PAGE);
    }

    public ActionForward removeMediumAccesses(ActionMapping mapping,
                                             ActionForm form,
                                             HttpServletRequest request,
                                             HttpServletResponse response) throws Throwable {

        UtilsForm utilsForm = (UtilsForm) form;

        MediumAccessPool mediumAccessPool = BillingEngine.getInstance().getMediumAccessPool();
        mediumAccessPool.removeElements(utilsForm.getMultipleSelectBoxMediumAccesses().getSelectedKeys());
        utilsForm.getMultipleSelectBoxMediumAccesses().bind(Arrays.asList(mediumAccessPool.getMediumAccessPoolKeys().toArray()));
        utilsForm.getMultipleSelectBoxRemovedElements().bind(Arrays.asList(mediumAccessPool.getRemovedElemetsKeys().toArray()));

        return mapping.findForward(FORWARD_TRANSACTION_UTILS_PAGE);
    }

    public ActionForward restroreMediumAccesses(ActionMapping mapping,
                                             ActionForm form,
                                             HttpServletRequest request,
                                             HttpServletResponse response) throws Throwable {

        UtilsForm utilsForm = (UtilsForm) form;

        MediumAccessPool mediumAccessPool = BillingEngine.getInstance().getMediumAccessPool();
        mediumAccessPool.restoreElements(utilsForm.getMultipleSelectBoxRemovedElements().getSelectedKeys());
        utilsForm.getMultipleSelectBoxMediumAccesses().bind(Arrays.asList(mediumAccessPool.getMediumAccessPoolKeys().toArray()));
        utilsForm.getMultipleSelectBoxRemovedElements().bind(Arrays.asList(mediumAccessPool.getRemovedElemetsKeys().toArray()));

        return mapping.findForward(FORWARD_TRANSACTION_UTILS_PAGE);
    }

    public ActionForward restoreDefault(ActionMapping mapping,
                                             ActionForm form,
                                             HttpServletRequest request,
                                             HttpServletResponse response) throws Throwable {

        UtilsForm utilsForm = (UtilsForm) form;

        MediumAccessPool mediumAccessPool = BillingEngine.getInstance().getMediumAccessPool();
        mediumAccessPool.init();
        ((UtilsForm) form).getMultipleSelectBoxMediumAccesses().bind(Arrays.asList(mediumAccessPool.getMediumAccessPoolKeys().toArray()));
        ((UtilsForm) form).getMultipleSelectBoxRemovedElements().bind(Arrays.asList(mediumAccessPool.getRemovedElemetsKeys().toArray()));
        
        return mapping.findForward(FORWARD_TRANSACTION_UTILS_PAGE);
    }
}
*/

/*//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                        S E R V E R     V A R I A N T
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////*/


package com.tmx.web.actions.core.tools;

import com.tmx.as.modules.SpringModule;
import com.tmx.beng.access.GateConnection;
import com.tmx.web.actions.core.AbstractSvbillAction;
import com.tmx.web.forms.core.tools.UtilsForm;
import com.tmx.as.blogic.TransactionManager;
import com.tmx.as.blogic.GateTransactionsManager;
import com.tmx.as.exceptions.DatabaseException;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForm;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Arrays;

/* (Andrey Nagorniy): Using RMI and Spring */

public class UtilsAction extends AbstractSvbillAction
{
    private static final String FORWARD_TRANSACTION_UTILS_PAGE = "transactionUtilsPage";

    public ActionForward defaultAction(ActionMapping mapping,
                                       ActionForm form,
                                       HttpServletRequest request,
                                       HttpServletResponse response,
                                       String methodName) throws Throwable {
        UtilsForm utilsForm = (UtilsForm) form;

        GateConnection conn = (GateConnection) SpringModule.getApplicationContext().getBean("gateConnection");
        utilsForm.getMultipleSelectBoxMediumAccesses().bind(Arrays.asList(conn.getMediumAccessPoolKeys().toArray()));
        utilsForm.getMultipleSelectBoxRemovedElements().bind(Arrays.asList(conn.getRemovedElemetsKeys().toArray()));

        return mapping.findForward(FORWARD_TRANSACTION_UTILS_PAGE);
    }

    private ActionForward preloadForChecking(ActionMapping mapping,
                                             ActionForm form,
                                             HttpServletRequest request,
                                             HttpServletResponse response) throws DatabaseException {

        TransactionManager manager = new TransactionManager();
        HashMap transactions = manager.getProblemTransactions();
        return mapping.findForward(FORWARD_TRANSACTION_UTILS_PAGE);
    }

    private ActionForward getStatus(ActionMapping mapping,
                                             ActionForm form,
                                             HttpServletRequest request,
                                             HttpServletResponse response) throws DatabaseException {

        UtilsForm utilsForm = (UtilsForm) form;

        String[] guids = utilsForm.getEbBillingTransactionNumbers().getValue().replace('.', ',').split(",");
        HashMap transactionsStatuses = (new GateTransactionsManager()).getTransactionByGUID(guids);

        return mapping.findForward(FORWARD_TRANSACTION_UTILS_PAGE);
    }

    public ActionForward removeMediumAccesses(ActionMapping mapping,
                                             ActionForm form,
                                             HttpServletRequest request,
                                             HttpServletResponse response) throws Throwable {

        UtilsForm utilsForm = (UtilsForm) form;

        GateConnection conn = (GateConnection) SpringModule.getApplicationContext().getBean("gateConnection");
        conn.removeElements(utilsForm.getMultipleSelectBoxMediumAccesses().getSelectedKeys());
        utilsForm.getMultipleSelectBoxMediumAccesses().bind(Arrays.asList(conn.getMediumAccessPoolKeys().toArray()));
        utilsForm.getMultipleSelectBoxRemovedElements().bind(Arrays.asList(conn.getRemovedElemetsKeys().toArray()));

        return mapping.findForward(FORWARD_TRANSACTION_UTILS_PAGE);
    }

    public ActionForward restroreMediumAccesses(ActionMapping mapping,
                                             ActionForm form,
                                             HttpServletRequest request,
                                             HttpServletResponse response) throws Throwable {

        UtilsForm utilsForm = (UtilsForm) form;

        GateConnection conn = (GateConnection) SpringModule.getApplicationContext().getBean("gateConnection");
        conn.restoreElements(utilsForm.getMultipleSelectBoxRemovedElements().getSelectedKeys());
        utilsForm.getMultipleSelectBoxMediumAccesses().bind(Arrays.asList(conn.getMediumAccessPoolKeys().toArray()));
        utilsForm.getMultipleSelectBoxRemovedElements().bind(Arrays.asList(conn.getRemovedElemetsKeys().toArray()));

        return mapping.findForward(FORWARD_TRANSACTION_UTILS_PAGE);
    }

    public ActionForward restoreDefault(ActionMapping mapping,
                                             ActionForm form,
                                             HttpServletRequest request,
                                             HttpServletResponse response) throws Throwable {

        UtilsForm utilsForm = (UtilsForm) form;

        GateConnection conn = (GateConnection) SpringModule.getApplicationContext().getBean("gateConnection");
        utilsForm.getMultipleSelectBoxMediumAccesses().bind(Arrays.asList(conn.getMediumAccessPoolKeys().toArray()));
        utilsForm.getMultipleSelectBoxRemovedElements().bind(Arrays.asList(conn.getRemovedElemetsKeys().toArray()));

        return mapping.findForward(FORWARD_TRANSACTION_UTILS_PAGE);
    }
}