package com.tmx.beng.base;

/**
 */
public class TaskContext {
    private int status = StatusDictionary.STATUS_OK;


    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}
