package com.tmx.as.scheduler;

import java.util.*;

import org.apache.log4j.*;
import com.tmx.as.exceptions.DatabaseException;
import com.tmx.util.InitException;

public class ScheduleThread extends Thread {

    /**
     * The timeout in miliseconds between wakeups of the thread. Specified to 10 seconds.
     */
    public static final int timeout = 10000;//default value
    /**
     * Logger
     */
    private Logger logger;
    /**
     * Flag to stop thread
     */
    private boolean mayBeRunned = true;
    /**
     * Scheduler thread name
     */
    public static final String NM_SCHEDULE_THREAD = "SCHEDULE_THREAD";

    private Config config = null;


    /**
     * Constructor
     */
    private ScheduleThread(Config conf) {
        setName(ScheduleThread.NM_SCHEDULE_THREAD);
        config = conf;
        logger = Logger.getLogger("scheduler." + getName());
    }


    /**
     * Graceful stop processing thread
     */
    public void stopProcThread(String procThreadName) throws Exception {
        throw new Exception("Method does not implemented yet");
    }


    /**
     * Graceful stop all processing threads
     */
    public void stopAllProcThreads() throws Exception {
        throw new Exception("Method does not implemented yet");
    }


    /**
     * Supply the thread alive. If need restart it.
     */
    public void supplyAlive(String procThreadName) {
        try {
            SchedulerContext schContext = SchedulerContext.getInstance();
            //1. Get thread
            ProcessingThread procThread = (ProcessingThread) schContext.getProcThreadMap().get(procThreadName);

            //2. Get thread schedule
            Schedule sch = procThread.getSchedule();

            //3. Get current datetime
            long currDate = new java.util.Date().getTime();

            //4. Get ms to start
            long msToStart = sch.getLastStartUpTime() + sch.getInterval() - currDate;

            //Print report
            logger.debug("Supply alive report for '" + procThreadName + "':");
            logger.debug("---Is alive:       " + schContext.isProcThreadAlive(procThreadName));
            logger.debug("---Is blocked:     " + sch.getBlocked());
            logger.debug("---Start date(ms): " + sch.getStartDate());
            logger.debug("---Interval(ms):   " + sch.getInterval());
            logger.debug("---LastStartUp(ms):" + sch.getLastStartUpTime());
            logger.debug("---CurrentTime(ms):" + currDate);
            logger.debug("---TimeToStart(ms):" + msToStart);

            // Exit if thread already alive
            if (schContext.isProcThreadAlive(procThreadName)) {
                logger.debug("Not started: already alive");
                return;
            }

            // Exit if thread blocked
            if (sch.getBlocked()) {
                logger.debug("Not started: blocked");
                return;
            }

            // Exit if before start date
            if (currDate < sch.getStartDate()) {
                logger.debug("Not started: before start time");
                return;
            }

            // Exit if before interval end
            if (msToStart > 0) {
                logger.debug("Not started: before interval end");
                return;
            }

            //Execute thread
//This code make floating start up interval. 
//      sch.setLastStartUpTime(currDate);
//This code calculate last start up
            //Calculate count of available startups
            double currStartUpCountDouble = (currDate - sch.getStartDate()) / sch.getInterval();
            if (currStartUpCountDouble <= 0) currStartUpCountDouble = 0;
            //Round to nearest integer.
            long currStartUpCount = Math.round(currStartUpCountDouble);

            //Safely save lastStartUp time in schedule in DB
            try {
                sch.setLastStartUpTime(sch.getStartDate() + currStartUpCount * sch.getInterval());
            }
            catch (DatabaseException e) {
                logger.error("Failed to save LastStartUpTim for processing thread '" + procThreadName + "'");
            }

            try{
                schContext.startProcThread(procThreadName);
            }
            catch(InitException e){
                logger.error("Failed to start processing thread '" + procThreadName + "': "+e.toString());
            }
            catch(SchedulerException e){
                logger.error("Failed to start processing thread '" + procThreadName + "': "+e.toString());
            }

            //Use scheduler thread instead simply start()!!!. Else started process is hovered
//      procThread.start();
            logger.info("Started: '" + procThreadName + "'");
        }
        catch (Exception e) {
            logger.error("Failed to supply alive processing thread " + procThreadName + ", becouse: " + e.toString());
        }
    }


    /**
     * Set flag to stop the execution
     */
    public void shutDown() {
        mayBeRunned = false;
    }


    /**
     * Main parallel executed method
     */
    public void run() {
        logger.info("Scheduler thread is runned");
        SchedulerContext schContext = SchedulerContext.getInstance();
        while (mayBeRunned) {
            //Wake up
            logger.debug("Wake up");

            Iterator procThreadNamesIter = schContext.getProcThreadMap().keySet().iterator();
            while (procThreadNamesIter.hasNext()) {
                supplyAlive((String) procThreadNamesIter.next());
            }

            //Sleep
            try {
                this.sleep(config.getSleepTime());
                logger.debug("Sleep");
            }
            catch (InterruptedException e) {
                logger.error(e.toString());
            }
        }// while
        //Allow self to execute next time
        schContext.createScheduleThread(config);
        mayBeRunned = true;
        logger.info("Scheduler thread is shutdowned");

    }

    public static Config createConfig(){
        return new Config();
    }

    public Config getConfig(){
        return config;
    }

    public static class Config {
        private long sleepTime;
        private boolean autostart;


        public long getSleepTime() {
            return sleepTime;
        }

        public void setSleepTime(long sleepTime) {
            this.sleepTime = sleepTime;
        }

        public boolean isAutostart() {
            return autostart;
        }

        public void setAutostart(boolean autostart) {
            this.autostart = autostart;
        }

        public ScheduleThread instantiate(){
            return new ScheduleThread(this);
        }
    }

}