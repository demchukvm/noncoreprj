package com.tmx.web.forms.core.balance;

import com.tmx.web.base.BasicActionForm;
import com.tmx.web.controls.*;
import com.tmx.web.controls.table.ControlManagerTable;
import com.tmx.as.entities.bill.balance.AbstractBalance;
import com.tmx.as.entities.bill.transaction.TransactionType;
import com.tmx.as.entities.bill.transaction.SellerBalanceTransaction;
import com.tmx.util.i18n.DictionaryWrapper;
import com.tmx.beng.base.StatusDictionary;

import java.util.Date;
import java.text.DecimalFormat;
import java.util.GregorianCalendar;
import java.util.Calendar;
import java.util.HashMap;
import static java.util.Calendar.getInstance;

import org.hibernate.mapping.Map;

/**
 * this class is super for all balance form
 * Template method design pattern
 * @author Andrey Nagorniy
 */
abstract public class AbstractInfoBalanceForm extends BasicActionForm {
    protected Long balanceId;

    //--------Controls----------------------
    protected EditBox editBoxBalanceId;
    protected EditBox editBoxCode;
    protected EditBox editBoxName;

    protected TimeSelector timeSelectorRegDate;

    protected EditBox editBoxAmount;
    protected TextArea textAreaDescription;

    protected CheckBox checkBoxBlocked;
    protected TimeSelector timeSelectorTransFrom;
    protected TimeSelector timeSelectorTransTo;

    //financial activities
    protected EditBox editBoxTransactionIncreaceAmount;
    protected EditBox editBoxTransactionCurrentAmmount;
    protected EditBox editBoxNominalTransactionAmount;
    protected EditBox editBoxTransferAmount;
    protected EditBox editBoxTransactionAward;
    protected EditBox editBoxAccountNumber;
    protected EditBox editBoxOperatorName;

    private EditBox editBoxFulProfit;
    private EditBox editBoxSpareCash;

    // Table dependant statictics
    protected EditBox editBoxAllCount;
    protected EditBox editBoxDebetSum;
    protected EditBox editBoxDebetCount;
    protected EditBox editBoxCreditOkCount;
    protected EditBox editBoxCreditOkSum;
    protected EditBox editBoxAwardOkSum;
    protected EditBox editBoxAwardErrSum;
    protected EditBox editBoxCreditErrCount;
    protected EditBox editBoxCreditErrSum;

    protected HashMap<Long, String> operators;

    public EditBox getEditBoxAllCount() {
        return editBoxAllCount;
    }

    public void setEditBoxAllCount(EditBox editBoxAllCount) {
        this.editBoxAllCount = editBoxAllCount;
    }

    public EditBox getEditBoxDebetSum() {
        return editBoxDebetSum;
    }

    public void setEditBoxDebetSum(EditBox editBoxDebetSum) {
        this.editBoxDebetSum = editBoxDebetSum;
    }

    public EditBox getEditBoxDebetCount() {
        return editBoxDebetCount;
    }

    public void setEditBoxDebetCount(EditBox editBoxDebetCount) {
        this.editBoxDebetCount = editBoxDebetCount;
    }

    public EditBox getEditBoxCreditOkCount() {
        return editBoxCreditOkCount;
    }

    public void setEditBoxCreditOkCount(EditBox editBoxCreditOkCount) {
        this.editBoxCreditOkCount = editBoxCreditOkCount;
    }

    public EditBox getEditBoxCreditOkSum() {
        return editBoxCreditOkSum;
    }

    public void setEditBoxCreditOkSum(EditBox editBoxCreditOkSum) {
        this.editBoxCreditOkSum = editBoxCreditOkSum;
    }

    public EditBox getEditBoxAwardOkSum() {
        return editBoxAwardOkSum;
    }

    public void setEditBoxAwardOkSum(EditBox editBoxAwardOkSum) {
        this.editBoxAwardOkSum = editBoxAwardOkSum;
    }

    public EditBox getEditBoxCreditErrCount() {
        return editBoxCreditErrCount;
    }

    public void setEditBoxCreditErrCount(EditBox editBoxCreditErrCount) {
        this.editBoxCreditErrCount = editBoxCreditErrCount;
    }

    public EditBox getEditBoxCreditErrSum() {
        return editBoxCreditErrSum;
    }

    public void setEditBoxCreditErrSum(EditBox editBoxCreditErrSum) {
        this.editBoxCreditErrSum = editBoxCreditErrSum;
    }

    public EditBox getEditBoxAwardErrSum() {
        return editBoxAwardErrSum;
    }

    public void setEditBoxAwardErrSum(EditBox editBoxAwardErrSum) {
        this.editBoxAwardErrSum = editBoxAwardErrSum;
    }


    //tables
    protected ControlManagerTable transactionsTable;

    //for transactionsTable
    protected EditBox editBoxAllTransaction;
    protected EditBox editBoxSuccessTransaction;

    protected EditBox editBoxAllTransAmount;
    protected EditBox editBoxSuccessTransAmount;

    protected ScrollBox scrollBoxBalanceOwner;

    protected DictionaryWrapper transactionStatusDictionary;

    //Buttons
    protected Button onApplyButton;


    protected Button onApplyTransButton;
    protected Button onSaveButton;
    private Button onResetButton;
    protected Button onCancelButton;
    protected Button onChangeBalanceButton;
    //restriction buttons
    protected Button onCreateRestButton;
    protected Button onEditRestButton;
    protected Button onDeleteRestButton;
    //tariff buttons
    protected Button onCreateTariffButton;
    protected Button onEditTariffButton;
    protected Button onDeleteTariffButton;
    //-----------------------------------------------------------

    //----------------for template method design pattern---------------
    //for controls
    abstract protected void initButtonsAction();
    abstract protected ScrollBox initScrollBoxBalanceOwner();
    //for tables
    abstract protected ControlManagerTable initTransactionsTable();
    //for bind and populate operation
    abstract protected void bindData(AbstractBalance balance);
    abstract protected AbstractBalance populateFromControls();
    //----------------------------------------------------------------

    protected void init() {
        initScrollBoxes();
        initEditBoxes();
        initTimeSelectors();
        initTextAreas();
        initCheckBoxes();
        initButtons();
        initButtonsAction();//from sub class
        initTables();
        bindFilters();//private method in this class

        operators = new HashMap();
    }

    private void initTextAreas() {
        textAreaDescription = new TextArea();
    }

    private void initTimeSelectors() {
        timeSelectorRegDate = new TimeSelector();
        timeSelectorRegDate.setReadonly(true);
        timeSelectorTransFrom = new TimeSelector();
        timeSelectorTransTo = new TimeSelector();
    }

    private void initScrollBoxes() {
        scrollBoxBalanceOwner = initScrollBoxBalanceOwner();
    }

    private void initCheckBoxes() {
        checkBoxBlocked = new CheckBox();
    }

    private void initTables() {
        transactionsTable = initTransactionsTable();//from sub class
    }

    private void initEditBoxes() {
        editBoxAllTransaction = new EditBox();
        editBoxAllTransaction.setReadonly(true);
        editBoxSuccessTransaction = new EditBox();
        editBoxSuccessTransaction.setReadonly(true);
        editBoxAllTransAmount = new EditBox();
        editBoxAllTransAmount.setReadonly(true);
        editBoxSuccessTransAmount = new EditBox();
        editBoxSuccessTransAmount.setReadonly(true);
        editBoxBalanceId = new EditBox();
        editBoxBalanceId.setReadonly(true);
        editBoxCode = new EditBox();
        editBoxCode.setMandatory(true);
        editBoxName = new EditBox();
        editBoxName.setMandatory(true);
        editBoxAmount = new EditBox();
        editBoxAmount.setValue("0");

        editBoxTransactionIncreaceAmount = new EditBox();
        editBoxTransactionCurrentAmmount = new EditBox();
        editBoxNominalTransactionAmount = new EditBox();
        editBoxTransferAmount = new EditBox();
        editBoxTransactionAward = new EditBox();
        editBoxFulProfit = new EditBox();
        editBoxSpareCash = new EditBox();

        editBoxTransactionIncreaceAmount.setReadonly(true);
        editBoxTransactionCurrentAmmount.setReadonly(true);
        editBoxNominalTransactionAmount.setReadonly(true);
        editBoxTransactionAward.setReadonly(true);
        editBoxTransferAmount.setReadonly(true);

        editBoxAllCount = new EditBox();
        editBoxDebetSum = new EditBox();
        editBoxDebetCount = new EditBox();
        editBoxCreditOkCount = new EditBox();
        editBoxCreditOkSum = new EditBox();
        editBoxAwardOkSum = new EditBox();
        editBoxCreditErrCount = new EditBox();
        editBoxCreditErrSum = new EditBox();
        editBoxAwardErrSum = new EditBox();

        editBoxAllCount.setReadonly(true);
        editBoxDebetCount.setReadonly(true);
        editBoxDebetSum.setReadonly(true);
        editBoxCreditOkCount.setReadonly(true);
        editBoxCreditOkSum.setReadonly(true);
        editBoxAwardOkSum.setReadonly(true);
        editBoxCreditErrCount.setReadonly(true);
        editBoxCreditErrSum.setReadonly(true);
        editBoxAwardErrSum.setReadonly(true);
    }

    /**
     * init all battons in this form
     */
    private void initButtons(){
        //----global buttons------------------------------
        onSaveButton = new Button();
        onSaveButton.setLabelKey("ctrl.button.label.save");
        onSaveButton.setCommand("save");

        onResetButton = new Button();
        onResetButton.setLabelKey("ctrl.button.label.reset");

        onCancelButton = new Button();
        onCancelButton.setLabelKey("ctrl.button.label.cancel");

        onApplyButton = new Button();
        onApplyButton.setLabelKey("ctrl.button.label.apply");
        onApplyButton.setCommand("apply");

        onApplyTransButton = new Button();
        onApplyTransButton.setLabelKey("ctrl.button.label.apply");
        onApplyTransButton.setCommand("applyTrans");

        onChangeBalanceButton = new Button();
        onChangeBalanceButton.setLabelKey("ctrl.button.label.change_balance");
        onChangeBalanceButton.setCommand("preloadForChange");
        //----------------------------------------------------

        //---restriction buttons----------------------------------
        onCreateRestButton = new Button();
        onCreateRestButton.setLabelKey("ctrl.button.label.create");
        onCreateRestButton.setCommand("preloadForCreate");

        onEditRestButton = new Button();
        onEditRestButton.setLabelKey("ctrl.button.label.edit");
        onEditRestButton.setCommand("preloadForUpdate");

        onDeleteRestButton = new Button();
        onDeleteRestButton.setLabelKey("ctrl.button.label.delete");
        onDeleteRestButton.setCommand("preloadForDelete");
        //--------------------------------------------------------

        //----tariffs buttons------------------------------------

        onCreateTariffButton = new Button();
        onCreateTariffButton.setLabelKey("ctrl.button.label.create");
        onCreateTariffButton.setCommand("preloadForCreate");

        onEditTariffButton = new Button();
        onEditTariffButton.setLabelKey("ctrl.button.label.edit");
        onEditTariffButton.setCommand("preloadForUpdate");

        onDeleteTariffButton = new Button();
        onDeleteTariffButton.setLabelKey("ctrl.button.label.delete");
        onDeleteTariffButton.setCommand("preloadForDelete");

        //-------------------------------------------------------
    }

    /**
     * This methods can be called form sub classes, for more simple action and command defination
     * @param saveAction
     * @param cancelAction
     * @param applayAction
     */
    protected void setButtonsAction(String saveAction, String cancelAction, String applayAction){
        onSaveButton.setAction(saveAction);
        onCancelButton.setAction(cancelAction);
        onApplyButton.setAction(applayAction);
        onApplyTransButton.setAction(applayAction);
    }
    protected void setButtonsCMD(String saveCMD, String cancelCMD, String applayCMD){
        onSaveButton.setCommand(saveCMD);
        onCancelButton.setCommand(cancelCMD);
        onApplyButton.setCommand(applayCMD);
        onApplyTransButton.setCommand(applayCMD);
    }
    //-------------------------------------------------------------------------------------------

    public void bind(AbstractBalance balance){
        DecimalFormat formatter = new DecimalFormat("0.00");

        bindData(balance);

        editBoxBalanceId.setValue(balance.getBalanceId().toString());
        editBoxCode.setValue(balance.getCode());
        editBoxName.setValue(balance.getName());
        timeSelectorRegDate.setTime(balance.getRegistrationDate());
        textAreaDescription.setValue(balance.getDescription());

        if(balance.getAmount() != null){
//            String amount = balance.getAmount().toString();
            editBoxAmount.setValue(formatter.format(balance.getAmount()));
        }

        checkBoxBlocked.setSelected(balance.getBlocked());
    }

    private void
    bindFilters(){
        transactionsTable.addParameterToFilter("by_client_amount_from", "from_amount", new EditBox());
        transactionsTable.addParameterToFilter("by_client_amount_to", "to_amount", new EditBox());
        transactionsTable.addParameterToFilter("by_transaction_type", "type", new ScrollBox(true){
                                        protected String getKey(Object dataListElement) {
                                            return ((TransactionType)dataListElement).getId().toString();
                                        }

                                        protected String getValue(Object dataListElement) {
                                            return ((TransactionType)dataListElement).getName();
                                        }
                                    });
        transactionsTable.addParameterToFilter("by_client_statusCode", "statusCode", initStatusScrollBox(true));



        GregorianCalendar prototype = new GregorianCalendar();
        GregorianCalendar calFrom = new GregorianCalendar(
                                        prototype.get(Calendar.YEAR),
                                        prototype.get(Calendar.MONTH),
                                        prototype.get(Calendar.DATE),
                                        0,
                                        0
        );

        Date dateFrom = calFrom.getTime();
        TimeSelector timeFrom = new TimeSelector();
        timeFrom.setTime(dateFrom);
        Calendar calTo = getInstance();
        Date dateTo = calTo.getTime();
        TimeSelector timeTo = new TimeSelector();
        timeTo.setTime(dateTo);

        transactionsTable.addParameterToFilter("by_FromDate", "date", timeFrom);
        transactionsTable.addParameterToFilter("by_ToDate", "date", timeTo);
        //for billing transaction number
        transactionsTable.addParameterToFilter("by_billnum_like", "billnum", new EditBox());
        //for client transaction number
        transactionsTable.addParameterToFilter("by_clinum_like", "clinum", new EditBox());
        transactionStatusDictionary = new DictionaryWrapper(getLocale(),
                StatusDictionary.class, StatusDictionary.STATUS_MSGS_BUNDLE);
        
    }
    private ScrollBox initStatusScrollBox (boolean nullable) {
        ScrollBox<StatusDictionary.StatusObject> StatusScrollBox = new ScrollBox<StatusDictionary.StatusObject>() {

            protected String getKey(StatusDictionary.StatusObject sellerRestrName) {
                return  sellerRestrName.getKey().toString();
            }

            protected String getValue(StatusDictionary.StatusObject sellerRestrName) {
                return sellerRestrName.getValue();
            }
        };
    StatusScrollBox.setNullable(nullable);
    return StatusScrollBox;
}
    public ScrollBox<StatusDictionary.StatusObject> getStatusScrollBox() {
        return (ScrollBox<StatusDictionary.StatusObject>) getTransactionsTable().getParameterControl("by_client_statusCode", "statusCode");
    }

    public AbstractBalance populate(){
        AbstractBalance balance = populateFromControls();

        if(balance != null){

            if(editBoxBalanceId.getValue() != null)
                balance.setBalanceId(PopulateUtil.getLongValue(editBoxBalanceId));

            balance.setName(editBoxName.getValue());
            balance.setCode(editBoxCode.getValue());
            balance.setRegistrationDate(new Date());
            balance.setDescription(textAreaDescription.getValue());
            balance.setAmount(PopulateUtil.getDoubleValue(editBoxAmount));
            balance.setBlocked(checkBoxBlocked.isSelected());
        }

        return balance;
    }

    public EditBox getEditBoxBalanceId() {
        return editBoxBalanceId;
    }

    public void setEditBoxBalanceId(EditBox editBoxBalanceId) {
        this.editBoxBalanceId = editBoxBalanceId;
    }

    public EditBox getEditBoxCode() {
        return editBoxCode;
    }

    public void setEditBoxCode(EditBox editBoxCode) {
        this.editBoxCode = editBoxCode;
    }

    public EditBox getEditBoxName() {
        return editBoxName;
    }

    public void setEditBoxName(EditBox editBoxName) {
        this.editBoxName = editBoxName;
    }

    public TimeSelector getTimeSelectorRegDate() {
        return timeSelectorRegDate;
    }

    public void setTimeSelectorRegDate(TimeSelector timeSelectorRegDate) {
        this.timeSelectorRegDate = timeSelectorRegDate;
    }

    public TextArea getTextAreaDescription() {
        return textAreaDescription;
    }

    public void setTextAreaDescription(TextArea textAreaDescription) {
        this.textAreaDescription = textAreaDescription;
    }

    public EditBox getEditBoxAmount() {
        return editBoxAmount;
    }

    public void setEditBoxAmount(EditBox editBoxAmount) {
        this.editBoxAmount = editBoxAmount;
    }

    public CheckBox getCheckBoxBlocked() {
        return checkBoxBlocked;
    }

    public void setCheckBoxBlocked(CheckBox checkBoxBlocked) {
        this.checkBoxBlocked = checkBoxBlocked;
    }

    public Long getBalanceId() {
        return balanceId;
    }

    public void setBalanceId(Long balanceId) {
        this.balanceId = balanceId;
    }

    public ControlManagerTable getTransactionsTable() {
        return transactionsTable;
    }

    public void setTransactionsTable(ControlManagerTable transactionsTable) {
        this.transactionsTable = transactionsTable;
    }

    public EditBox getEditBoxAllTransaction() {
        return editBoxAllTransaction;
    }

    public void setEditBoxAllTransaction(EditBox editBoxAllTransaction) {
        this.editBoxAllTransaction = editBoxAllTransaction;
    }

    public EditBox getEditBoxSuccessTransaction() {
        return editBoxSuccessTransaction;
    }

    public void setEditBoxSuccessTransaction(EditBox editBoxSuccessTransaction) {
        this.editBoxSuccessTransaction = editBoxSuccessTransaction;
    }

    public EditBox getEditBoxAllTransAmount() {
        return editBoxAllTransAmount;
    }

    public void setEditBoxAllTransAmount(EditBox editBoxAllTransAmount) {
        this.editBoxAllTransAmount = editBoxAllTransAmount;
    }

    public EditBox getEditBoxSuccessTransAmount() {
        return editBoxSuccessTransAmount;
    }

    public void setEditBoxSuccessTransAmount(EditBox editBoxSuccessTransAmount) {
        this.editBoxSuccessTransAmount = editBoxSuccessTransAmount;
    }

    public DictionaryWrapper getTransactionStatusDictionary() {
        return transactionStatusDictionary;
    }

    public void setTransactionStatusDictionary(DictionaryWrapper transactionStatusDictionary) {
        this.transactionStatusDictionary = transactionStatusDictionary;
    }

    public Button getOnApplyButton() {
        return onApplyButton;
    }

    public void setOnApplyButton(Button onApplyButton) {
        this.onApplyButton = onApplyButton;
    }

    public Button getOnSaveButton() {
        return onSaveButton;
    }

    public void setOnSaveButton(Button onSaveButton) {
        this.onSaveButton = onSaveButton;
    }

    public Button getOnResetButton() {
        return onResetButton;
    }

    public void setOnResetButton(Button onResetButton) {
        this.onResetButton = onResetButton;
    }

    public Button getOnCancelButton() {
        return onCancelButton;
    }

    public void setOnCancelButton(Button onCancelButton) {
        this.onCancelButton = onCancelButton;
    }

    public Button getOnApplyTransButton() {
        return onApplyTransButton;
    }

    public void setOnApplyTransButton(Button onApplyTransButton) {
        this.onApplyTransButton = onApplyTransButton;
    }

    public ScrollBox getScrollBoxBalanceOwner() {
        return scrollBoxBalanceOwner;
    }

    public void setScrollBoxBalanceOwner(ScrollBox scrollBoxBalanceOwner) {
        this.scrollBoxBalanceOwner = scrollBoxBalanceOwner;
    }

    public Button getOnChangeBalanceButton() {
        return onChangeBalanceButton;
    }

    public void setOnChangeBalanceButton(Button onChangeBalanceButton) {
        this.onChangeBalanceButton = onChangeBalanceButton;
    }

    public Button getOnCreateRestButton() {
        return onCreateRestButton;
    }

    public void setOnCreateRestButton(Button onCreateRestButton) {
        this.onCreateRestButton = onCreateRestButton;
    }

    public Button getOnEditRestButton() {
        return onEditRestButton;
    }

    public void setOnEditRestButton(Button onEditRestButton) {
        this.onEditRestButton = onEditRestButton;
    }

    public Button getOnDeleteRestButton() {
        return onDeleteRestButton;
    }

    public void setOnDeleteRestButton(Button onDeleteRestButton) {
        this.onDeleteRestButton = onDeleteRestButton;
    }

    public Button getOnCreateTariffButton() {
        return onCreateTariffButton;
    }

    public void setOnCreateTariffButton(Button onCreateTariffButton) {
        this.onCreateTariffButton = onCreateTariffButton;
    }

    public Button getOnEditTariffButton() {
        return onEditTariffButton;
    }

    public void setOnEditTariffButton(Button onEditTariffButton) {
        this.onEditTariffButton = onEditTariffButton;
    }

    public Button getOnDeleteTariffButton() {
        return onDeleteTariffButton;
    }

    public void setOnDeleteTariffButton(Button onDeleteTariffButton) {
        this.onDeleteTariffButton = onDeleteTariffButton;
    }


    public TimeSelector getTimeSelectorTransFrom() {
        return timeSelectorTransFrom;
    }

    public void setTimeSelectorTransFrom(TimeSelector timeSelectorTransFrom) {
        this.timeSelectorTransFrom = timeSelectorTransFrom;
    }

    public TimeSelector getTimeSelectorTransTo() {
        return timeSelectorTransTo;
    }

    public void setTimeSelectorTransTo(TimeSelector timeSelectorTransTo) {
        this.timeSelectorTransTo = timeSelectorTransTo;
    }

    public EditBox getEditBoxNominalTransactionAmount() {
        return editBoxNominalTransactionAmount;
    }

    public void setEditBoxNominalTransactionAmount(EditBox editBoxNominalTransactionAmount) {
        this.editBoxNominalTransactionAmount = editBoxNominalTransactionAmount;
    }

    public EditBox getEditBoxTransactionAward() {
        return editBoxTransactionAward;
    }

    public void setEditBoxTransactionAward(EditBox editBoxTransactionAward) {
        this.editBoxTransactionAward = editBoxTransactionAward;
    }

    public EditBox getEditBoxTransferAmount() {
        return editBoxTransferAmount;
    }

    public void setEditBoxTransferAmount(EditBox editBoxTransferAmount) {
        this.editBoxTransferAmount = editBoxTransferAmount;
    }

    public EditBox getEditBoxTransactionCurrentAmmount() {
        return editBoxTransactionCurrentAmmount;
    }

    public void setEditBoxTransactionCurrentAmmount(EditBox editBoxTransactionCurrentAmmount) {
        this.editBoxTransactionCurrentAmmount = editBoxTransactionCurrentAmmount;
    }

    public EditBox getEditBoxTransactionIncreaceAmount() {
        return editBoxTransactionIncreaceAmount;
    }

    public void setEditBoxTransactionIncreaceAmount(EditBox editBoxTransactionIncreaceAmount) {
        this.editBoxTransactionIncreaceAmount = editBoxTransactionIncreaceAmount;
    }
    public void disableButtons(boolean disable){
        onCreateRestButton.setReadonly(disable);
        onEditRestButton.setReadonly(disable);
        onDeleteRestButton.setReadonly(disable);
        onCreateTariffButton.setReadonly(disable);
        onEditTariffButton.setReadonly(disable);
        onDeleteTariffButton.setReadonly(disable);
    }

    public void disableControls(boolean disable){
        getScrollBoxBalanceOwner().setReadonly(disable);
        getEditBoxAmount().setReadonly(disable);
    }

    public EditBox getEditBoxAccountNumber() {
        return editBoxAccountNumber;
    }

    public void setEditBoxAccountNumber(EditBox editBoxAccountNumber) {
        this.editBoxAccountNumber = editBoxAccountNumber;
    }

    public EditBox getEditBoxOperatorName() {
        return editBoxOperatorName;
    }

    public void setEditBoxOperatorName(EditBox editBoxOperatorName) {
        this.editBoxOperatorName = editBoxOperatorName;
    }

    public HashMap<Long, String> getOperators() {
        return operators;
    }

    public void setOperators(HashMap<Long, String> operators) {
        this.operators = operators;
    }

    public EditBox getEditBoxFulProfit() {
        return editBoxFulProfit;
    }

    public void setEditBoxFulProfit(EditBox editBoxFulProfit) {
        this.editBoxFulProfit = editBoxFulProfit;
    }

    public EditBox getEditBoxSpareCash() {
        return editBoxSpareCash;
    }

    public void setEditBoxSpareCash(EditBox editBoxSpareCash) {
        this.editBoxSpareCash = editBoxSpareCash;
    }
}
