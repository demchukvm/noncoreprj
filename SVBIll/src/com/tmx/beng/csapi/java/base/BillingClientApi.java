package com.tmx.beng.csapi.java.base;

import com.tmx.as.modules.SpringModule;
import com.tmx.beng.access.Access;
import com.tmx.beng.access.Connection;
import com.tmx.beng.base.*;
import com.tmx.beng.base.BillingMessage;
import com.tmx.beng.base.dbfunctions.ValidationError;
import com.tmx.util.InitException;
import com.tmx.util.MD5;
import com.tmx.as.blogic.ClientTransactionNumGenerator;
import com.tmx.as.blogic.BlogicException;
import com.tmx.as.entities.bill.function.ActualFunctionParameter;
import org.apache.log4j.Logger;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import java.util.Date;
import java.util.List;

/**
 * This class provide layer for sending messages to the billing engine
 * and constrain access visibility
 */
public class BillingClientApi {

    private Connection connect;

    protected Logger logger = Logger.getLogger("core.BillingClientApi");

    public BillingClientApi() throws InitException, BillException {
        connect = openBillingConnection();
    }

//    public Connection openBillingConnection() throws InitException, BillException {
//        return getBillingEngineAccess().obtainConnection();
//    }

    public Connection openBillingConnection() throws InitException, BillException {
        return (Connection) SpringModule.getApplicationContext().getBean("billingConnection");
    }

//    private Access getBillingEngineAccess() throws InitException {
//        InitialContext ctx = null;
//        try {
//            System.setProperty("java.naming.factory.url.pkgs", ""); //=org.apache.naming
//            ctx = new InitialContext();//System.getProperties()
//            Access newAccess = (Access) ctx.lookup("java:comp/services/billingEngine");
//            if (newAccess == null)
//                throw new InitException("err.csapi.null_basic_access_obtained");
//            return newAccess;
//        }
//        catch (NamingException e) {
//            throw new InitException("err.csapi.failed_to_obtain_billing_engine_access", e);
//        }
//        finally {
//            closeInitialContext(ctx);
//        }
//    }
//
//    private void closeInitialContext(InitialContext ctx) {
//        if (ctx != null) {
//            try {
//                ctx.close();
//            }
//            catch (NamingException e) {
//                logger.info("Unable to release initial context", e);
//            }
//        }
//    }

    public RefillMessage createReffilMessage(String who, String suffix) {
        DefaultRefillMessage message = new DefaultRefillMessage(this, BillingMessage.O_REFILL_PAYMENT);
        message.setClientTime(new Date());
        ClientTransactionNumGenerator cliTransactionNumGen = new ClientTransactionNumGenerator(who, suffix);
        message.setCliTransactionNum(cliTransactionNumGen.nextNumber());
        return message;
    }

    public WebSellerRefillMessage createSellerRefillMessage() {
        SellerRefillMessage message = new SellerRefillMessage(this, BillingMessage.O_CHAHGE_SELLER_BALLANCE);
        message.setClientTime(new Date());
        ClientTransactionNumGenerator cliTransactionNumGen = new ClientTransactionNumGenerator("web",
                "refill_seller_balance_tx");
        message.setCliTransactionNum(cliTransactionNumGen.nextNumber());
        return message;
    }

    public WebOperatorRefillMessage createOperatorRefillMessage() {
        OperatorRefillMessage message = new OperatorRefillMessage(this, BillingMessage.O_CHAHGE_OPERATOR_BALLANCE);
        message.setClientTime(new Date());
        ClientTransactionNumGenerator cliTransactionNumGen = new ClientTransactionNumGenerator("web",
                "refill_operator_balance_tx");
        message.setCliTransactionNum(cliTransactionNumGen.nextNumber());
        return message;
    }

    public WebTerminalRefillMessage createTerminalRefillMessage() {
        TerminalRefillMessage message = new TerminalRefillMessage(this, BillingMessage.O_CHAHGE_TERMINAL_BALLANCE);
        message.setClientTime(new Date());
        ClientTransactionNumGenerator cliTransactionNumGen = new ClientTransactionNumGenerator("web",
                "refill_terminal_balance_tx");
        message.setCliTransactionNum(cliTransactionNumGen.nextNumber());
        return message;
    }

    public BuyVoucherMessage createBuyVoucherMessage() {
        DefaultBuyVoucherMessage message = new DefaultBuyVoucherMessage(this, BillingMessage.O_BUY_VOUCHER);
        message.setClientTime(new Date());
        ClientTransactionNumGenerator cliTransactionNumGen = new ClientTransactionNumGenerator("test",
                "buy_voucher_tx");
        message.setCliTransactionNum(cliTransactionNumGen.nextNumber());
        return message;
    }

    public ReloadDBCacheMessage createReloadDBCacheMessage() {
        return new DefaultReloadDBCacheMessage(this, BillingMessage.O_RELOAD_DB_CACHE);
    }

    public DumpDBCacheMessage createDumpDBCacheMessage() {
        return new DefaultDumpDBCacheMessage(this, BillingMessage.O_DB_CACHE_DUMP);
    }

    //TODO NEW
    public DefaultParamsReq createDefaultParamsMsg() {
    	return new DefaultParamsReq(connect);
    }

    public ValidateParamReq createValidateParamsMsg() {
        return new ValidateParamReq(connect);
    }

    public BuyVoucherReq createBuyVoucherMsg() {
        BuyVoucherReq message = new BuyVoucherReq(connect);
        message.setClientTime(new Date());
        return message;
    }

    // Proximan 09.11.2010
    public GenVoucherReq createGenVoucherMsg() {
        GenVoucherReq message = new GenVoucherReq(connect);
        message.setClientTime(new Date());
        return message;
    }
    //

    public RefillPaymentReq createRefillPaymentMsg() {
        return new RefillPaymentReq(connect);
    }

    public RefillPaymentStatusReq createRefillPaymentStatusMsg() {
        return new RefillPaymentStatusReq(connect);
    }
    //------END NEW

    public static interface Message {
        public void setAmount(String amount);

        public void setCliTransactionNum(String cliTransactionNum);

        public void setClientTime(Date clientTime);

        public boolean send();
    }

    public static interface BuyVoucherMessage extends RefillMessage{
        public void setVoucherNominal(String voucherNominal);
    }

    private static class DefaultBuyVoucherMessage extends DefaultRefillMessage implements BuyVoucherMessage {

        DefaultBuyVoucherMessage(BillingClientApi manager, String operationName) {
            super(manager, operationName);
        }

        public void setVoucherNominal(String voucherNominal) {
            message.setAttributeString(BillingMessage.VOUCHER_NOMINAL, voucherNominal);    
        }

    }

    public static interface ReloadDBCacheMessage extends Message {
    }

    public static interface DumpDBCacheMessage extends Message {
    }

    public static interface RefillMessage extends Message {
        public void setProcessingCode(String processingCode);

        public void setPassword(String password);

        public void setLogin(String login);

        public void setAccountNumber(String accountNumber);

        public void setServiceCode(String serviceCode);

        public void setTerminalSN(String terminalSN);

        public void setSellerCode(String sellerCode);
    }

    public static interface WebRefillMessage extends Message {
        public void setBalanceCode(String balanceCode);

        public void setUserId(String userId);

        public void setTransactionType(String type);

        public void setPaymentReason(String paymentReason);

        public void setTransDescription(String description);
    }

    public static interface WebSellerRefillMessage extends WebRefillMessage {
        public void setSellerCode(String sellerCode);
    }

    public static interface WebOperatorRefillMessage extends WebRefillMessage {
        public void setServiceCode(String serviceCode);
    }

    public static interface WebTerminalRefillMessage extends WebRefillMessage {
        public void setTerminalSN(String terminalSN);
    }


    private static abstract class AbstractMessage implements Message {

        protected BillingMessageImpl message = new BillingMessageImpl();
        private BillingClientApi billingClient;

        AbstractMessage(BillingClientApi manager, String operationName) {
            billingClient = manager;
            message.setOperationName(operationName);
        }

        public void setAmount(String amount) {
            message.setAttributeString(BillingMessage.AMOUNT, amount);
        }

        public void setCliTransactionNum(String cliTransactionNum) {
            message.setAttributeString(BillingMessage.CLI_TRANSACTION_NUM, cliTransactionNum);
        }

        public void setClientTime(Date clientTime) {
            message.setAttribute(BillingMessage.CLIENT_TIME, clientTime);
        }

        public boolean send() {
            try {
                BillingMessage respMsg = billingClient.openBillingConnection().processSync(message);
                final int statusCode = Integer.parseInt(respMsg.getAttributeString(BillingMessage.STATUS_CODE));
                if (statusCode == StatusDictionary.STATUS_OK)
                    return true;
            } catch (Throwable e) {
                //TODO(AN)????
            }
            return false;
        }

    }

    private static class DefaultRefillMessage extends AbstractMessage implements RefillMessage {

        DefaultRefillMessage(BillingClientApi manager, String operationName) {
            super(manager, operationName);
        }

        public void setProcessingCode(String processingCode) {
            message.setAttributeString(BillingMessage.PROCESSING_CODE, processingCode);
        }

        public void setPassword(String password) {
            message.setAttributeString(BillingMessage.PASSWORD, MD5.getHashString(password).toUpperCase());
        }

        public void setLogin(String login) {
            message.setAttributeString(BillingMessage.LOGIN, login);
        }

        public void setAccountNumber(String accountNumber) {
            message.setAttributeString(BillingMessage.ACCOUNT_NUMBER, accountNumber);
        }

        public void setServiceCode(String serviceCode) {
            message.setAttributeString(BillingMessage.SERVICE_CODE, serviceCode);
        }

        public void setTerminalSN(String terminalSN) {
            message.setAttributeString(BillingMessage.TERMINAL_SN, terminalSN);
        }

        public void setSellerCode(String sellerCode) {
            message.setAttributeString(BillingMessage.SELLER_CODE, sellerCode);
        }
    }

    private static abstract class AbstractWebRefillMessage extends AbstractMessage implements WebRefillMessage {

        AbstractWebRefillMessage(BillingClientApi manager, String operationName) {
            super(manager, operationName);
        }

        public void setUserId(String userId) {
            message.setAttributeString(BillingMessage.USER_ACTOR_ID, userId);
        }

        public void setTransactionType(String type) {
            message.setAttributeString(BillingMessage.TRANSACTION_TYPE_ID, type);
        }

        public void setPaymentReason(String paymentReason) {
            message.setAttributeString(BillingMessage.PAYMENT_REASON, paymentReason);
        }

        public void setTransDescription(String description) {
            message.setAttributeString(BillingMessage.TRANSACTION_DESCRIPTION, description);
        }

        public void setBalanceCode(String balanceCode) {
            message.setAttributeString(BillingMessage.BALANCE_CODE, balanceCode);
        }
    }

    private static class SellerRefillMessage extends AbstractWebRefillMessage implements WebSellerRefillMessage {

        SellerRefillMessage(BillingClientApi manager, String operationName) {
            super(manager, operationName);
        }

        public void setSellerCode(String sellerCode) {
            message.setAttributeString(BillingMessage.SELLER_CODE, sellerCode);
        }

    }

    private static class TerminalRefillMessage extends AbstractWebRefillMessage implements WebTerminalRefillMessage {

        TerminalRefillMessage(BillingClientApi manager, String operationName) {
            super(manager, operationName);
        }

        public void setTerminalSN(String terminalSN) {
            message.setAttributeString(BillingMessage.TERMINAL_SN, terminalSN);
        }
    }

    private static class OperatorRefillMessage extends AbstractWebRefillMessage implements WebOperatorRefillMessage {

        OperatorRefillMessage(BillingClientApi manager, String operationName) {
            super(manager, operationName);
        }

        public void setServiceCode(String serviceCode) {
            message.setAttributeString(BillingMessage.SERVICE_CODE, serviceCode);
        }
    }

    private class DefaultReloadDBCacheMessage extends AbstractMessage implements ReloadDBCacheMessage {
        DefaultReloadDBCacheMessage(BillingClientApi manager, String operationName) {
            super(manager, operationName);
        }
    }

    private class DefaultDumpDBCacheMessage extends AbstractMessage implements DumpDBCacheMessage {

        DefaultDumpDBCacheMessage(BillingClientApi manager, String operationName) {
            super(manager, operationName);
        }
    }

}


















