package com.tmx.beng.base.dbfunctions;

import com.tmx.beng.base.cache.InvokeFunctionException;
import com.tmx.beng.base.BillingMessage;

import java.util.Map;
import java.util.List;
import java.util.ArrayList;

/**
 */
public class TransactionMinMaxLimit extends BasicLimitFunction {
    public static final String MAX_AMOUNT = "max_amount";
    public static final String MIN_AMOUNT = "min_amount";

    public void checkLimit(Map actualParams) throws InvokeFunctionException {
        Double amount = new Double(task.getProcesedMessage().getAttributeString(BillingMessage.AMOUNT));

        // check max amount limit
        String maxAmountStr = (String) actualParams.get(MAX_AMOUNT);
        Double maxAmount = maxAmountStr != null ? new Double(maxAmountStr) : null;
        if (maxAmount != null && maxAmount.compareTo(amount) < 0) {
            throw new InvokeFunctionException("err.bill.dbfunctions.max_amount_limit_exceed");
        }

        // check min amount limit
        String minAmountStr = (String) actualParams.get(MIN_AMOUNT);
        Double minAmount = minAmountStr != null ? new Double(minAmountStr) : null;
        if (minAmount != null && minAmount.compareTo(amount) > 0) {
            throw new InvokeFunctionException("err.bill.dbfunctions.min_amount_limit_exceed");
        }
    }

    public ValidationError[] validate(Map actualParams) {
        List resultErrors = new ArrayList();
        resultErrors.addAll(ParameterValidator.validateParamNames(actualParams, new String[] {MAX_AMOUNT, MIN_AMOUNT}));
        return (ValidationError[]) resultErrors.toArray(new ValidationError[resultErrors.size()]);
    }

    public TariffParameter[] getDefaultParams() {
        return new TariffParameter[] {
                new TariffParameter(MIN_AMOUNT, "1", true),
                new TariffParameter(MAX_AMOUNT, "1000", true),
        };
    }
}
