package com.tmx.web.actions.core.transactions.client_transaction;

import com.tmx.web.base.BasicDispatchedAction;
import com.tmx.web.base.SessionEnvironment;
import com.tmx.web.forms.core.transaction.client_transaction.InfoClientTransactionForm;
import com.tmx.web.forms.core.transaction.client_transaction.ManageClientTransactionsForm;
import com.tmx.web.forms.core.dictionary.seller.ManageSellersForm;
import com.tmx.as.blogic.TransactionManager;
import com.tmx.as.entities.bill.transaction.ClientTransaction;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForm;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;


public class TransactionHandling extends BasicDispatchedAction {

    private static final String FORWARD_INFO_CLIENT_TRANSACTION_PAGE = "infoClientTransactionPage";

    private static final String FORWARD_MANAGE_CLIENT_TRANSACTIONS_PAGE = "manageClientTransactionsPage";

    public ActionForward defaultAction(ActionMapping mapping,
                                       ActionForm form,
                                       HttpServletRequest request,
                                       HttpServletResponse response,
                                       String methodName) throws Throwable {
        InfoClientTransactionForm ctForm = (InfoClientTransactionForm) form;
        ctForm.getSelectedTransactionId();

        return mapping.findForward(FORWARD_MANAGE_CLIENT_TRANSACTIONS_PAGE);
    }

        public ActionForward testMethod(ActionMapping mapping,
                                  ActionForm form,
                                  HttpServletRequest request,
                                  HttpServletResponse response) throws Throwable {

        return mapping.findForward(FORWARD_INFO_CLIENT_TRANSACTION_PAGE);
    }

}