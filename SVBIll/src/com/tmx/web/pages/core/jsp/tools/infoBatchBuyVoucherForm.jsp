<%@ page language="java" pageEncoding="UTF-8" contentType="text/html; charset=utf-8"
         errorPage="/core/jsp/message/jsperror.jsp" isELIgnored="false" %>
<%@ taglib uri="/WEB-INF/tld/struts-nested.tld" prefix="nested" %>
<%@ taglib uri="/WEB-INF/tld/tmx-controls.tld" prefix="tmx_ctrl" %>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>


<html:form action="/core/tools/batchBuyVoucher">
<table border="0" width="100%">

    <!-- BUTTONS -->
    <tr>
        <td style="text-align: left">
            <tmx_ctrl:button property="btBuy"/>
            <tmx_ctrl:button property="btGetStatus"/>
            <tmx_ctrl:button property="btDownload"/>
            <tmx_ctrl:button property="btReset"/>
        </td>
    </tr>

    <!-- PURCHASE QUERY TAB -->
    <tr>
        <td valign="center" align="center" class="tabForm">
            <table width="100%" border="0">
                <!-- Tab header -->
                <tr>
                    <td colspan="2">
                        <h4 class="dataLabel">
                            <bean:message bundle="core.labels" key="core.tools.infobuyvoucherform.purchase_query.title"/>
                        </h4>
                    </td>
                </tr>

                <tr>
                    <td class="dataLabel" width="15%">
                        <tmx_ctrl:writeEntityLabel attrName="voucherNominal"
                                                   entityClassName="com.tmx.as.entities.bill.voucher.BatchBuyVoucher"/>:
                    </td>
                    <%--<td class="dataField" width="35%">--%>
                        <%--<tmx_ctrl:editBox property="ebVoucherNominal"/>--%>
                    <%--</td>--%>
                    <td class="dataField" width="35%">
	                    <tmx_ctrl:selectBox property="sbVoucherNominal"/>
                    </td>

                    <td class="dataLabel" width="15%">
                        <bean:message bundle="core.labels" key="core.tools.infobuyvoucherform.label.sleep_between_interval"/>
                    </td>
                    <td class="dataField" width="35%">
                        <tmx_ctrl:editBox property="ebSleepBetweenInterval"/>
                    </td>
                </tr>

                <tr>
                    <td class="dataLabel" width="15%">
                        <tmx_ctrl:writeEntityLabel attrName="voucherCount"
                                                   entityClassName="com.tmx.as.entities.bill.voucher.BatchBuyVoucher"/>:
                    </td>
                    <td class="dataField" width="35%">
                        <tmx_ctrl:editBox property="ebVoucherCount"/>
                    </td>

                    <td class="dataLabel" width="15%">
                        <tmx_ctrl:writeEntityLabel attrName="batchBuyVoucherId"
                                                   entityClassName="com.tmx.as.entities.bill.voucher.BatchBuyVoucher"/>:
                    </td>
                    <td class="dataField" width="35%">
                        <tmx_ctrl:editBox property="ebBatchBuyVoucherId"/>
                    </td>
                </tr>

                <tr>
                    <td class="dataLabel" width="15%">
                        <tmx_ctrl:writeEntityLabel attrName="serviceCode"
                                                   entityClassName="com.tmx.as.entities.bill.voucher.BatchBuyVoucher"/>:
                    </td>
                    <td class="dataField" width="35%">
                        <tmx_ctrl:selectBox property="sbServiceCode"/>
                    </td>

                    <td class="dataLabel" width="15%">
                                                <tmx_ctrl:writeEntityLabel attrName="transactionTime"
                                                   entityClassName="com.tmx.as.entities.bill.voucher.BatchBuyVoucher"/>:
                    </td>
                    <td class="dataField" width="35%">
                        <tmx_ctrl:timeSelector property="tsTransactionTime"/>
                    </td>
                </tr>

                <tr>
                    <td class="dataLabel" width="15%">
                        <tmx_ctrl:writeEntityLabel attrName="operationType"
                                                   entityClassName="com.tmx.as.entities.bill.voucher.BatchBuyVoucher"/>:
                    </td>
                    <td class="dataField" width="35%">
                        <tmx_ctrl:selectBox property="sbOperationType"/>
                    </td>

                    <td class="dataLabel" width="15%">
                        &nbsp;
                    </td>
                    <td class="dataField" width="35%">
                        &nbsp;
                    </td>
                </tr>


            </table>
        </td>
    </tr>


    <!-- PURCHASE STATUS TAB -->
    <tr>
        <td valign="center" align="center" class="tabForm">
            <table width="100%" border="0">
                <!-- Tab header -->
                <tr>
                    <td colspan="2">
                        <h4 class="dataLabel">
                            <bean:message bundle="core.labels" key="core.tools.infobuyvoucherform.purchase_status.title"/>
                        </h4>
                    </td>
                </tr>

                <tr>
                    <td class="dataLabel" width="15%">
                        <tmx_ctrl:writeEntityLabel attrName="status"
                                                   entityClassName="com.tmx.as.entities.bill.voucher.BatchBuyVoucher"/>:
                    </td>
                    <td class="dataField" width="35%">
                        <tmx_ctrl:editBox property="ebStatus"/>
                    </td>

                    <td class="dataLabel" width="15%">
                        <bean:message bundle="core.labels" key="core.tools.infobuyvoucherform.label.vouchers_obtained"/>
                    </td>
                    <td class="dataField" width="35%">
                        <tmx_ctrl:editBox property="ebVouchersObtained"/>
                    </td>
                </tr>
            </table>
        </td>
    </tr>

    <!-- AUTHORIZATION TAB -->
    <tr>
        <td valign="center" align="center" class="tabForm">
            <table width="100%" border="0">
                <!-- Tab header -->
                <tr>
                    <td colspan="2">
                        <h4 class="dataLabel">
                            <bean:message bundle="core.labels" key="core.tools.infobuyvoucherform.authorization.title"/>
                        </h4>
                    </td>
                </tr>

                <tr>
                    <td class="dataLabel" width="15%">
                        <tmx_ctrl:writeEntityLabel attrName="processingCode"
                                                   entityClassName="com.tmx.as.entities.bill.voucher.BatchBuyVoucher"/>:
                    </td>
                    <td class="dataField" width="35%">
                        <tmx_ctrl:selectBox property="sbProcessingCode"/>
                    </td>

                    <td class="dataLabel" width="15%">
                        <tmx_ctrl:writeEntityLabel attrName="login"
                                                   entityClassName="com.tmx.as.entities.general.user.User"/>:
                    </td>
                    <td class="dataField" width="35%">
                        <tmx_ctrl:editBox property="ebLogin"/>
                    </td>
                </tr>

                <tr>
                    <td class="dataLabel" width="15%">
                        <tmx_ctrl:writeEntityLabel attrName="sellerCode"
                                                   entityClassName="com.tmx.as.entities.bill.voucher.BatchBuyVoucher"/>:
                    </td>
                    <td class="dataField" width="35%">
                        <tmx_ctrl:selectBox property="sbSellerCode"/>
                    </td>

                    <td class="dataLabel" width="15%">
                        <tmx_ctrl:writeEntityLabel attrName="password"
                                                   entityClassName="com.tmx.as.entities.general.user.User"/>:
                    </td>
                    <td class="dataField" width="35%">
                        <tmx_ctrl:passwordEditBox property="ebPassword"/>
                    </td>
                </tr>

                <tr>
                    <td class="dataLabel" width="15%">
                        <tmx_ctrl:writeEntityLabel attrName="terminalSn"
                                                   entityClassName="com.tmx.as.entities.bill.voucher.BatchBuyVoucher"/>:
                    </td>
                    <td class="dataField" width="35%">
                        <tmx_ctrl:editBox property="ebTerminalSn"/>
                    </td>

                    <td class="dataLabel" width="15%">
                        &nbsp;
                    </td>
                    <td class="dataField" width="35%">
                        &nbsp;
                    </td>
                </tr>
            </table>
        </td>
    </tr>

    <!-- BUTTONS -->
    <tr>
        <td style="text-align: left">
            <tmx_ctrl:button property="btBuy"/>
            <tmx_ctrl:button property="btGetStatus"/>
            <tmx_ctrl:button property="btDownload"/>
            <tmx_ctrl:button property="btReset"/>
        </td>
    </tr>
</table>
</html:form>