package com.tmx.as.entities.bill.balance;

import com.tmx.as.entities.bill.terminal.Terminal;

/**
 */
public class TerminalBalance extends AbstractBalance {
    private Terminal terminal;
    private Long terminalId;

    public Terminal getTerminal() {
        return terminal;
    }

    public void setTerminal(Terminal terminal) {
        this.terminal = terminal;
    }

    public Long getTerminalId() {
        return terminalId;
    }

    public void setTerminalId(Long terminalId) {
        this.terminalId = terminalId;
    }
}
