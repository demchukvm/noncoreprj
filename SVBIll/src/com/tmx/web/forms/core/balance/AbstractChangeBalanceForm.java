package com.tmx.web.forms.core.balance;

import com.tmx.as.entities.bill.transaction.TransactionType;
import com.tmx.as.entities.bill.balance.AbstractBalance;
import com.tmx.as.entities.general.user.User;
import com.tmx.as.blogic.balance.AbstractBalanceManager;
import com.tmx.web.base.BasicActionForm;
import com.tmx.web.controls.*;

/**
 * Template method design pattern
 * @author Andrew Nagorniy
 */
abstract public class AbstractChangeBalanceForm extends BasicActionForm {
    private Long balanceId;
    private Button onCancelButton;
    private Button onChangeButton;
    private EditBox amountEditBox;
    private ScrollBox transTypeScrollBox;
    private TextArea descriptionTextArea;
    private EditBox paymentReasonEditBox;
    protected EditBox editBoxBalanceName;
    protected EditBox editBoxOwnerName;

    /**
     * Template method
     */
//    abstract protected void initChangeBalanceForm();

    protected void init(){
        onCancelButton = new Button();
        onCancelButton.setLabelKey("ctrl.button.label.cancel");
        onCancelButton.setCommand("preloadForUpdate");

        onChangeButton = new Button();
        onChangeButton.setLabelKey("ctrl.button.label.apply");
        onChangeButton.setCommand("change");

        editBoxBalanceName = new EditBox();
        editBoxBalanceName.setReadonly(true);

        editBoxOwnerName = new EditBox();
        editBoxOwnerName.setReadonly(true);

        transTypeScrollBox = new ScrollBox(){
            protected String getKey(Object dataListElement) {
                return ((TransactionType)dataListElement).getCode();
            }

            protected String getValue(Object dataListElement) {
                TransactionType type = (TransactionType)dataListElement;

                String result = type.getName();
                if(type.getDescription() !=null )
                    result += "("+type.getDescription()+")";
                return result;
            }
        };

        amountEditBox = new EditBox();
        amountEditBox.setMandatory(true);
        amountEditBox.addValidationRule(EditBox.createValidationRule_NUMBER_FORMAT("##.##"));


        paymentReasonEditBox = new EditBox();
        paymentReasonEditBox.setMandatory(true);
        descriptionTextArea = new TextArea();

    }

    public void bindData(AbstractBalance  abstractBalance) {
        editBoxBalanceName.setValue(abstractBalance.getName());        
    }

    public AbstractBalanceManager.BalanceModificationConfig populateBalanceConfig(User userActor){
        AbstractBalanceManager.BalanceModificationConfig conf = new AbstractBalanceManager.BalanceModificationConfig();

        TransactionType type = new TransactionType();
        type.setCode( getTransTypeScrollBox().getSelectedEntry().getKey() );
        type.setName( getTransTypeScrollBox().getSelectedEntry().getValue() );
        
        conf.setBalanceId( getBalanceId() );
        conf.setAmount( PopulateUtil.getDoubleValue(getAmountEditBox()) );
        conf.setDescription( getDescriptionTextArea().getValue() );
        conf.setType( type );
        conf.setUserActorId( userActor.getUserId() );
        conf.setPaymentReason( PopulateUtil.getStringValue(getPaymentReasonEditBox()) );
        
        return conf;
    }

    public Button getOnCancelButton() {
        return onCancelButton;
    }

    public void setOnCancelButton(Button onCancelButton) {
        this.onCancelButton = onCancelButton;
    }

    public Button getOnChangeButton() {
        return onChangeButton;
    }

    public void setOnChangeButton(Button onChangeButton) {
        this.onChangeButton = onChangeButton;
    }

    public EditBox getAmountEditBox() {
        return amountEditBox;
    }

    public void setAmountEditBox(EditBox amountEditBox) {
        this.amountEditBox = amountEditBox;
    }

    public Long getBalanceId() {
        return balanceId;
    }

    public void setBalanceId(Long balanceId) {
        this.balanceId = balanceId;
    }

    public ScrollBox getTransTypeScrollBox() {
        return transTypeScrollBox;
    }

    public void setTransTypeScrollBox(ScrollBox transTypeScrollBox) {
        this.transTypeScrollBox = transTypeScrollBox;
    }

    public TextArea getDescriptionTextArea() {
        return descriptionTextArea;
    }

    public void setDescriptionTextArea(TextArea descriptionTextArea) {
        this.descriptionTextArea = descriptionTextArea;
    }

    public EditBox getPaymentReasonEditBox() {
        return paymentReasonEditBox;
    }

    public void setPaymentReasonEditBox(EditBox paymentReasonEditBox) {
        this.paymentReasonEditBox = paymentReasonEditBox;
    }

    public EditBox getEditBoxBalanceName() {
        return editBoxBalanceName;
    }

    public void setEditBoxBalanceName(EditBox editBoxBalanceName) {
        this.editBoxBalanceName = editBoxBalanceName;
    }

    public EditBox getEditBoxOwnerName() {
        return editBoxOwnerName;
    }

    public void setEditBoxOwnerName(EditBox editBoxOwnerName) {
        this.editBoxOwnerName = editBoxOwnerName;
    }
}
