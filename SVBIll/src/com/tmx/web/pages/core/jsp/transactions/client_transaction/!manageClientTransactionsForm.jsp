<%@ page language="java" pageEncoding="UTF-8" contentType="text/html; charset=utf-8" isELIgnored="false" %>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-nested" prefix="nested" %>
<%@ taglib uri="http://tmx.com/taglib/tmx-controls" prefix="tmx_ctrl" %>
<%@ taglib uri="http://tmx.com/taglib/tmx-logic" prefix="tmx_logic" %>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html" %>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean" %>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>


<html:form action="/core/transactions/client_transaction/manageClientTransactions">
<table width="100%" border="1">

    <!-- FILTER SET -->
    <tr>
        <td valign="top" width="100%" style="text-align: left">
            <tmx_ctrl:panelFilterSet tableName="clientTransactionsTable">
            <table border="1">
                <tr>
                    <td class="dataLabel">
                        <tmx_ctrl:writeEntityLabel
                                attrName="transactionFromTime"
                                entityClassName="com.tmx.as.entities.bill.transaction.ClientTransaction"/>:
                    </td>
                    <td class="dataField">
                        <tmx_ctrl:timeSelector
                                property="clientTransactionsTable.filterSet.filter(by_FromDate).parameter(date).control"/>
                    </td>

                    <td class="dataLabel">
                        <tmx_ctrl:writeEntityLabel
                                attrName="transactionToTime"
                                entityClassName="com.tmx.as.entities.bill.transaction.ClientTransaction"/>:
                    </td>
                    <td class="dataField">
                        <tmx_ctrl:timeSelector
                                property="clientTransactionsTable.filterSet.filter(by_ToDate).parameter(date).control"/>
                    </td>

                 <%--   <td class="dataField">
                            <tmx_ctrl:writeEntityLabel
                                attrName="terminalSerialNumber"
                                entityClassName="com.tmx.as.entities.bill.transaction.ClientTransaction"/>:
                    </td>   --%>

                    <td align="center">
                        <img src="<tmx_ctrl:rewriteUrl href="/core/images/gray/ctrl/term_num.gif"/>" alt="Номер терминала">
                    </td>



                    <td class="dataField">
                        <tmx_ctrl:editBox property="editBoxTerminalSerialNumber"/>
                    </td>

                    <td class="dataField">
                        <tmx_ctrl:writeEntityLabel
                                attrName="{@name}"
                                entityClassName="com.tmx.as.entities.bill.terminal.EquipmentType"/>:
                    </td>
                    <td class="dataField">
                        <tmx_ctrl:selectBox property="scrollBoxTerminalTypes" width="150"/>
                    </td>

                </tr>
                <tr>    
                    <td class="dataField">
                        <tmx_ctrl:writeEntityLabel
                                attrName="{@name}"
                                entityClassName="com.tmx.as.entities.bill.transaction.TransactionType"/>:
                    </td>
                    <td class="dataField">
                        <tmx_ctrl:selectBox property="scrollBoxTransTypes" width="100"/>
                                <%--property="clientTransactionsTable.filterSet.filter(by_transaction_type).parameter(type).control"/>--%>
                    </td>

                    <td class="dataField">

                    </td>
                    <td class="dataField">
                    </td>

           <%--         <td class="dataField">
                            <tmx_ctrl:writeEntityLabel
                                attrName="serialNumber"
                                entityClassName="com.tmx.as.entities.bill.voucher.Voucher"/>:
                    </td>   --%>

                    <td align="center">
                        <img src="<tmx_ctrl:rewriteUrl href="/core/images/gray/ctrl/voucher.gif"/>" alt="Номер ваучера">
                    </td>

                    <td class="dataField">
                        <tmx_ctrl:editBox property="editBoxVoucherCode"/>
                    </td>

                 <%--   <td class="dataField">
                        <tmx_ctrl:writeEntityLabel
                                attrName="accountNumber"
                                entityClassName="com.tmx.as.entities.bill.transaction.ClientTransaction"/>:
                    </td> --%>

                    <td align="center">
                        <img src="<tmx_ctrl:rewriteUrl href="/core/images/gray/ctrl/tel_num.png"/>" alt="Номер телефона">
                    </td>

                    <td class="dataField">
                        <tmx_ctrl:editBox property="editBoxAccountNumber"/>
                    </td>
                </tr>
                <tr>
                 <%--   <td class="dataLabel">
                        <tmx_ctrl:writeEntityLabel
                                attrName="{@name}"
                                entityClassName="com.tmx.as.entities.bill.operator.Operator"/>:
                    </td>  --%>

                    <td align="center">
                        <img src="<tmx_ctrl:rewriteUrl href="/core/images/gray/ctrl/operator.gif"/>" alt="Оператор">
                    </td>

                    <td class="dataField">
                        <tmx_ctrl:selectBox
                                property="clientTransactionsTable.filterSet.filter(by_operator).parameter(operator).control" width="300"/>
                    </td>

                    <td class="dataField">

                    </td>
                    <td class="dataField">
                    </td>

                    <td class="dataLabel">
                        <tmx_ctrl:writeEntityLabel
                                attrName="transactionGUID"
                                entityClassName="com.tmx.as.entities.bill.transaction.ClientTransaction"/>:
                    </td>
                    <td class="dataField">
                        <tmx_ctrl:editBox
                                property="clientTransactionsTable.filterSet.filter(by_billnum_like).parameter(billnum).control"/>
                    </td>

                    <td class="dataField">
                        <tmx_ctrl:writeEntityLabel
                                attrName="status"
                                entityClassName="com.tmx.as.entities.bill.transaction.ClientTransaction"/>:
                    </td>
                    <td class="dataField">
                        <tmx_ctrl:selectBox property="scrollBoxStatuses" width="400"/>           <!-- !!!!!!!!!!!!!!!! -->
                    </td>
                </tr>
                <tr>
              <%--      <td class="dataLabel">
                        <tmx_ctrl:writeEntityLabel
                                attrName="{@name}"
                                entityClassName="com.tmx.as.entities.bill.seller.Seller"/>:
                    </td>   --%>

                    <td align="center">
                        <img src="<tmx_ctrl:rewriteUrl href="/core/images/gray/ctrl/seller_.png"/>" alt="Диллер">
                    </td>

                    <td class="dataField">
                        <tmx_ctrl:selectBox
                                property="clientTransactionsTable.filterSet.filter(by_seller).parameter(seller).control" width="300"/>
                    </td>

                    <td class="dataLabel">

                    </td>
                    <td class="dataField">

                    </td>

                    <td class="dataLabel">
                        <tmx_ctrl:writeEntityLabel
                                attrName="cliTransactionNum"
                                entityClassName="com.tmx.as.entities.bill.transaction.ClientTransaction"/>:
                    </td>
                    <td class="dataField">
                        <tmx_ctrl:editBox
                                property="clientTransactionsTable.filterSet.filter(by_clinum_like).parameter(clinum).control"/>
                    </td>
                    <td class="dataField">

                    </td>
                    <td class="dataField">

                    </td>
                </tr>
                <tr>
                    <td class="dataLabel">
                        <tmx_ctrl:writeEntityLabel
                                attrName="amountFrom"
                                entityClassName="com.tmx.as.entities.bill.transaction.ClientTransaction"/>:
                    </td>
                    <td class="dataField">
                        <tmx_ctrl:editBox
                                property="clientTransactionsTable.filterSet.filter(by_client_amount_from).parameter(from_amount).control"/>
                    </td>

                    <td class="dataLabel">
                        <tmx_ctrl:writeEntityLabel attrName="amountTo"
                                                   entityClassName="com.tmx.as.entities.bill.transaction.ClientTransaction"/>:
                    </td>
                    <td class="dataField">
                        <tmx_ctrl:editBox
                                property="clientTransactionsTable.filterSet.filter(by_client_amount_to).parameter(to_amount).control"/>
                    </td>
                    <td class="dataLabel">
                        <tmx_ctrl:writeEntityLabel attrName="errorTransactionsFilter"
                                                   entityClassName="com.tmx.as.entities.bill.transaction.ClientTransaction"/>:
                    </td>
                    <td class="dataField">
                         <tmx_ctrl:checkBox property="chekBoxErrorTransactions" />
                    </td>
                    <td class="dataField">

                    </td>
                    <td class="dataField">

                    </td>
            </table>
        </tmx_ctrl:panelFilterSet>
        </td>
    </tr>
    <!-- TABLE -->
    <tr>
        <td width="100%">
            <table class='one_outer_cell_table' cellspacing='0' cellpadding='0' width="100%" border="1">
                <tr>
                    <td>
                        <table cellspacing='1' cellpadding='1' width="100%" border="1">
                            <!-- Table header -->
                            <tr>

                                <th class="column_header_cell" width="110">
                                    <tmx_ctrl:imgTableOrdering tableName="clientTransactionsTable"
                                                               orderByFieldName="transactionTime"
                                                               labelBundle="as:com.tmx.as.entities.bill.transaction.ClientTransaction"
                                                               labelKey="transactionTime"/>
                                </th>

                                <th class="column_header_cell" width="110">
                                    <tmx_ctrl:imgTableOrdering tableName="clientTransactionsTable"
                                                               orderByFieldName="operator.name"
                                                               labelBundle="as:com.tmx.as.entities.bill.operator.Operator"
                                                               labelKey="{@name}"/>
                                </th>

                                <th class="column_header_cell" width="110">
                                    <tmx_ctrl:imgTableOrdering tableName="clientTransactionsTable"
                                                               orderByFieldName="seller.name"
                                                               labelBundle="as:com.tmx.as.entities.bill.seller.Seller"
                                                               labelKey="{@name}"/>
                                </th>


                                <th class="column_header_cell" width="110">
                                    <tmx_ctrl:imgTableOrdering tableName="clientTransactionsTable"
                                                               orderByFieldName="transactionGUID"
                                                               labelBundle="as:com.tmx.as.entities.bill.transaction.ClientTransaction"
                                                               labelKey="transactionGUID"/>
                                </th>

                                <th class="column_header_cell" width="110">
                                    <tmx_ctrl:imgTableOrdering tableName="clientTransactionsTable"
                                                               orderByFieldName="cliTransactionNum"
                                                               labelBundle="as:com.tmx.as.entities.bill.transaction.ClientTransaction"
                                                               labelKey="cliTransactionNum"/>
                                </th>

                                <th class="column_header_cell" width="110">
                                    <tmx_ctrl:imgTableOrdering tableName="clientTransactionsTable"
                                                               orderByFieldName="terminal.serialNumber"
                                                               labelBundle="as:com.tmx.as.entities.bill.terminal.Terminal"
                                                               labelKey="serialNumber"/>
                                </th>

                                <th class="column_header_cell" width="110">
                                    <tmx_ctrl:imgTableOrdering tableName="clientTransactionsTable"
                                                               orderByFieldName="accountNumber"
                                                               labelBundle="as:com.tmx.as.entities.bill.transaction.ClientTransaction"
                                                               labelKey="accountNumber"/>
                                </th>

                                <th class="column_header_cell" width="110">
                                    <tmx_ctrl:imgTableOrdering tableName="clientTransactionsTable"
                                                               orderByFieldName="amount"
                                                               labelBundle="as:com.tmx.as.entities.bill.transaction.ClientTransaction"
                                                               labelKey="amount"/>
                                </th>

                                <th class="column_header_cell" width="110">
                                    <tmx_ctrl:imgTableOrdering tableName="clientTransactionsTable"
                                                               orderByFieldName="status"
                                                               labelBundle="as:com.tmx.as.entities.bill.transaction.ClientTransaction"
                                                               labelKey="status"/>
                                </th>

                                <%--<th class="column_header_cell" width="110">--%>
                                    <%--<tmx_ctrl:imgTableOrdering tableName="clientTransactionsTable"--%>
                                                               <%--orderByFieldName="status"--%>
                                                               <%--labelBundle="as:com.tmx.as.entities.bill.transaction.ClientTransaction"--%>
                                                               <%--labelKey="status"/>--%>
                                <%--</th>--%>

                            </tr>
                            <nested:iterate id="clientTransaction" property="clientTransactionsTable.data" type="com.tmx.as.entities.bill.transaction.ClientTransaction">
                                <tr>
                                    <td class='column_data_cell'>
                                            <fmt:formatDate value="${clientTransaction.transactionTime}" pattern="yyyy/MM/dd HH:mm:ss"/>
                                    </td>

                                    <td class='column_data_cell'>
                                        <c:if test="${clientTransaction.operator != null}">
                                            <c:out value="${clientTransaction.operator.name}" />
                                        </c:if>
                                    </td>

                                    <td class='column_data_cell'>
                                        <c:if test="${clientTransaction.seller != null}">
                                            <c:out value="${clientTransaction.seller.name}" />
                                        </c:if>
                                    </td>

                                    <td class='column_data_cell'>
                                        <html:link action="/core/transactions/client_transaction/infoClientTransaction"
                                                   paramId="selectedTransactionId" paramName="clientTransaction" paramProperty="tansactionId">
                                            <c:out value="${clientTransaction.transactionGUID}" />
                                        </html:link>
                                    </td>

                                    <td class='column_data_cell'>
                                            <c:out value="${clientTransaction.cliTransactionNum}"/>
                                    </td>

                                    <td class='column_data_cell'>
                                        <c:if test="${clientTransaction.terminal != null}">
                                            <c:out value="${clientTransaction.terminal.serialNumber}" />
                                        </c:if>
                                    </td>

                                    <td class='column_data_cell'>
                                            <c:out value="${clientTransaction.accountNumber}"/>
                                    </td>

                                    <td class='column_data_cell'>
                                        <fmt:formatNumber value="${clientTransaction.amount}" currencySymbol="" type="currency"/>
                                    </td>
                                    <td class='column_data_cell'>
                                        <tmx_ctrl:write
                                                name="core.transactions.client_transaction.manageClientTransactionsForm"
                                                property="transactionStatusDictionary.value(status.${clientTransaction.status})" ignore="true" length="13"/>
                                    </td>

                                    <%--<td class='column_data_cell'>--%>
                                        <%--<html:link action="/core/transactions/client_transaction/transactionHandling"--%>
                                                   <%--paramId="selectedTransactionId" paramName="clientTransaction" paramProperty="tansactionId">--%>
                                            <%--<c:out value="${clientTransaction.transactionGUID}" />--%>
                                        <%--</html:link>--%>
                                    <%--</td>--%>
                                </tr>
                            </nested:iterate>
                            <!-- Empty row -->
                            <nested:define id="tableSize" property="clientTransactionsTable.resultSize" type="java.lang.Integer"/>
                            <c:if test="${tableSize <= 0}">
                                <tr>
                                    <td class='column_data_cell' colspan="9" align="center">
                                        <bean:message bundle="core.labels" key="core.common.th.empty_table" />
                                    </td>
                                </tr>
                            </c:if>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td style="text-align: right">
            <tmx_ctrl:imgTablePaging maxBookmarkCount="10" tableName="clientTransactionsTable"/>
        </td>
    </tr>

    <tr>
        <td valign="center" align="center" class="tabForm">
            <table width="100%" border="1">
            <%--<!-- Form header -->--%>
                <tr>
                    <td colspan="2">
                        <h4 class="dataLabel">
                            <bean:message bundle="core.labels" key="core.transaction.clienttransaction.infoclienttransactionform.titleAmount"/>
                        </h4>
                    </td>
                </tr>
                <tr>
                    <td class="dataLabel">
                        <tmx_ctrl:writeEntityLabel
                                attrName="successTransactions"
                                entityClassName="com.tmx.as.entities.bill.transaction.ClientTransaction"/>:
                    </td>
                    <td class="dataField">
                        <tmx_ctrl:editBox
                                property="editBoxSuccessTransaction"/>
                    </td>


                    <td class="dataLabel">
                        <tmx_ctrl:writeEntityLabel
                                attrName="allTransactions"
                                entityClassName="com.tmx.as.entities.bill.transaction.ClientTransaction"/>:
                    </td>
                    <td class="dataField">
                        <tmx_ctrl:editBox
                                property="editBoxAllTransaction"/>
                    </td>
                </tr>

                <tr>
                    <td class="dataLabel">
                        <tmx_ctrl:writeEntityLabel
                                attrName="errorTransactions"
                                entityClassName="com.tmx.as.entities.bill.transaction.ClientTransaction"/>:
                    </td>
                    <td class="dataField">
                        <tmx_ctrl:editBox
                                property="editBoxErrorTransaction"/>
                    </td>


                    <td class="dataLabel">
                        <%--<tmx_ctrl:writeEntityLabel--%>
                                <%--attrName="allTransactions"--%>
                                <%--entityClassName="com.tmx.as.entities.bill.transaction.ClientTransaction"/>:--%>
                    </td>
                    <td class="dataField">
                        <%--<tmx_ctrl:editBox--%>
                                <%--property="editBoxAllTransaction"/>--%>
                    </td>
                </tr>




                <tr>
                    <td class="dataLabel">
                        <tmx_ctrl:writeEntityLabel
                                attrName="successTransactionAmount"
                                entityClassName="com.tmx.as.entities.bill.transaction.ClientTransaction"/>:
                    </td>
                    <td class="dataField">
                        <tmx_ctrl:editBox
                                property="editBoxSuccessTransAmount"/>
                    </td>


                    <td class="dataLabel">
                        <tmx_ctrl:writeEntityLabel
                                attrName="allTransactionAmount"
                                entityClassName="com.tmx.as.entities.bill.transaction.ClientTransaction"/>:
                    </td>
                    <td class="dataField">
                        <tmx_ctrl:editBox
                                property="editBoxAllTransAmount"/>
                    </td>
                </tr>
            </table>
        </td>
    </tr>

</table>
<!--<table valign="center" align="center">-->
    <!---->
<!--</table>-->

</html:form>