package com.tmx.web.controls;

import com.tmx.as.base.criterion.CriterionWrapper;

public interface CriterionFactory{
    public CriterionWrapper createCriterion();
}
