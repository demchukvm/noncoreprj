<?php /* Smarty version 2.6.26, created on 2013-02-20 18:24:31
         compiled from short_affiliate_program.tpl.html */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'string_format', 'short_affiliate_program.tpl.html', 14, false),array('modifier', 'set_query_html', 'short_affiliate_program.tpl.html', 26, false),array('modifier', 'translate', 'short_affiliate_program.tpl.html', 26, false),)), $this); ?>
<tr>
	<td valign="top">
		<strong><?php echo 'Партнерская программа'; ?>
</strong>
		<p class="paddingblock">
		<?php if ($this->_tpl_vars['affiliate_customers']): ?>
			<?php echo 'Привлеченные покупатели'; ?>
: <?php echo $this->_tpl_vars['affiliate_customers']; ?>

		<?php else: ?>
		<?php echo 'Привлеченных покупателей нет'; ?>

		<?php endif; ?>
			<br />
			<?php echo 'Баланс'; ?>
:
			<?php $_from = $this->_tpl_vars['CurrencyISO3']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['_CurrencyISO']):
?>
			<?php if ($this->_tpl_vars['CommissionsAmount'][$this->_tpl_vars['_CurrencyISO']['currency_iso_3']] || $this->_tpl_vars['PaymentsAmount'][$this->_tpl_vars['_CurrencyISO']['currency_iso_3']]): ?>
				<?php echo $this->_tpl_vars['_CurrencyISO']['currency_iso_3']; 
 echo ((is_array($_tmp=($this->_tpl_vars['CommissionsAmount'][$this->_tpl_vars['_CurrencyISO']['currency_iso_3']]-$this->_tpl_vars['PaymentsAmount'][$this->_tpl_vars['_CurrencyISO']['currency_iso_3']]))) ? $this->_run_mod_handler('string_format', true, $_tmp, "%.2f") : smarty_modifier_string_format($_tmp, "%.2f")); ?>

			<?php endif; ?>
			<?php endforeach; endif; unset($_from); ?>
		</p>
	</td>
	
	<td valign="top" align="right">
		<?php unset($this->_sections['affi']);
$this->_sections['affi']['name'] = 'affi';
$this->_sections['affi']['loop'] = is_array($_loop=$this->_tpl_vars['SubDivs']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['affi']['show'] = true;
$this->_sections['affi']['max'] = $this->_sections['affi']['loop'];
$this->_sections['affi']['step'] = 1;
$this->_sections['affi']['start'] = $this->_sections['affi']['step'] > 0 ? 0 : $this->_sections['affi']['loop']-1;
if ($this->_sections['affi']['show']) {
    $this->_sections['affi']['total'] = $this->_sections['affi']['loop'];
    if ($this->_sections['affi']['total'] == 0)
        $this->_sections['affi']['show'] = false;
} else
    $this->_sections['affi']['total'] = 0;
if ($this->_sections['affi']['show']):

            for ($this->_sections['affi']['index'] = $this->_sections['affi']['start'], $this->_sections['affi']['iteration'] = 1;
                 $this->_sections['affi']['iteration'] <= $this->_sections['affi']['total'];
                 $this->_sections['affi']['index'] += $this->_sections['affi']['step'], $this->_sections['affi']['iteration']++):
$this->_sections['affi']['rownum'] = $this->_sections['affi']['iteration'];
$this->_sections['affi']['index_prev'] = $this->_sections['affi']['index'] - $this->_sections['affi']['step'];
$this->_sections['affi']['index_next'] = $this->_sections['affi']['index'] + $this->_sections['affi']['step'];
$this->_sections['affi']['first']      = ($this->_sections['affi']['iteration'] == 1);
$this->_sections['affi']['last']       = ($this->_sections['affi']['iteration'] == $this->_sections['affi']['total']);
?>
		
			<?php if ($this->_tpl_vars['SubDivs'][$this->_sections['affi']['index']]['unickey'] == 'affiliate_settings'): ?>
				<?php if ($this->_tpl_vars['CONF_AFFILIATE_EMAIL_NEW_COMMISSION'] || $this->_tpl_vars['CONF_AFFILIATE_EMAIL_NEW_PAYMENT']): ?>
				<div>
					<a href="<?php echo ((is_array($_tmp="?did=".($this->_tpl_vars['SubDivs'][$this->_sections['affi']['index']]['id'])."&ukey=".($this->_tpl_vars['SubDivs'][$this->_sections['affi']['index']]['ukey']))) ? $this->_run_mod_handler('set_query_html', true, $_tmp) : smarty_modifier_set_query_html($_tmp)); ?>
"><?php echo ((is_array($_tmp=$this->_tpl_vars['SubDivs'][$this->_sections['affi']['index']]['name'])) ? $this->_run_mod_handler('translate', true, $_tmp) : smarty_modifier_translate($_tmp)); ?>
</a>
				</div>
				<?php endif; ?>
			<?php else: ?>
				<div>
					<a href="<?php echo ((is_array($_tmp="?did=".($this->_tpl_vars['SubDivs'][$this->_sections['affi']['index']]['id'])."&ukey=".($this->_tpl_vars['SubDivs'][$this->_sections['affi']['index']]['ukey']))) ? $this->_run_mod_handler('set_query_html', true, $_tmp) : smarty_modifier_set_query_html($_tmp)); ?>
"><?php echo ((is_array($_tmp=$this->_tpl_vars['SubDivs'][$this->_sections['affi']['index']]['name'])) ? $this->_run_mod_handler('translate', true, $_tmp) : smarty_modifier_translate($_tmp)); ?>
</a>
				</div>
			<?php endif; ?>
		<?php endfor; endif; ?>		
	</td>
</tr>