package com.tmx.web.actions.core.csapi;

import com.tmx.web.forms.core.csapi.CsApiCallForm;
import com.tmx.web.base.BasicDispatchedAction;
import com.tmx.util.DesCipher;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForm;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.crypto.*;
import javax.crypto.spec.SecretKeySpec;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import sun.misc.BASE64Encoder;
import sun.misc.BASE64Decoder;


/**
 */
public class DesCsApiCallAction extends BasicDispatchedAction {
    Logger logger = Logger.getLogger("webcsapi."+getClass().getName());

    /** Decoded msg */

    protected ActionForward defaultAction(ActionMapping mapping,
                                          ActionForm form,
                                          HttpServletRequest request,
                                          HttpServletResponse response,
                                          String methodName) throws Throwable{
        CsApiCallForm csApiCallForm = (CsApiCallForm) verifyForm(form, CsApiCallForm.class);
//        String cipheredXml = csApiCallForm.getRequestXml();
//        logger.info("Request XML: " + cipheredXml);
//
//        Cipher cipher;
//        try {
//            //parameter "DES" specifies type of cipher we want to create
//            //through the factory method. It includes algorithm, mode and
//            //padding. You can define only algorithm and in that case default
//            //values will be used for mode and padding.
//            cipher = Cipher.getInstance("DES");
//        } catch (Exception e) {
//            //requested cryptographic algorithm is not available
//            logger.error(e);
//            throw new Throwable(e.getMessage());
//        }
//
//        //The lenght of keyString is 8. It's important characteristic
//        //for encryption
//        String keyString = "12345678";
//        byte[] keyData = keyString.getBytes();
//        //key object specifies a secret key
//        //it uses to construct the secret key for our cipher object from a byte
//        //array
//        SecretKeySpec key = new SecretKeySpec(keyData, 0, keyData.length, "DES");
////        String cypheredString = encrypt(cipheredXml, cipher, key);
//        //decrypt encrypted message
//        String decypheredString = decrypt(cipheredXml, cipher, key);
        //send message to csAPI action
//        String tmp = DesCipher.encrypt(cipheredXml);
//        String decypheredString = DesCipher.decrypt(tmp);
//        logger.debug("!!!decypheredString" + decypheredString);
//        ((CsApiCallForm)form).setDesRequestXml(decypheredString);
        return mapping.findForward("csapi_action");
    }

     /**
     * Encrtyption method
     * @param str string of clearing text
     * @param cipher object of class Cipher
      * @param key DES key
      * @return ecnrypted string in Base64 format
     * @throws Throwable some crypto exceptions
    */
    public String encrypt(String str, Cipher cipher, SecretKeySpec key) throws Throwable {
         cipher.init(Cipher.ENCRYPT_MODE, key);
         int cypheredBytes;
         byte [] outputBytes = new byte [1000];
         try {
            byte[] utf8Str = str.getBytes("UTF8");
            cypheredBytes = cipher.doFinal (utf8Str, 0, utf8Str.length, outputBytes, 0);
        //            byte[] enc = cipher.doFinal(utf8);

        //            return new sun.misc.BASE64Encoder().encode(enc);
            return new String(outputBytes, 0, cypheredBytes);
        } catch (Exception e) {
            //requested cryptographic algorithm is not available
            logger.error(e);
            throw new Throwable(e.getMessage());
        }
    }

    /**
     * Dencrtyption method
     * @param str string of clearing text
     * @param cipher object of class Cipher
      * @param key DES key
      * @return decnrypted string in Base64 format
     * @throws Throwable some crypto exceptions
    */
    public String decrypt(String str, Cipher cipher, SecretKeySpec key) throws Throwable {
        cipher.init(Cipher.DECRYPT_MODE, key);
        int cypheredBytes;
        byte [] outputBytes = new byte [1000];
        try {
            byte[] strBytes = str.getBytes();//new sun.misc.BASE64Decoder().decodeBuffer(str);
//            byte[] utf8 = cipher.doFinal(dec);
            cypheredBytes = cipher.doFinal (strBytes, 0, strBytes.length, outputBytes, 0);
            return new String(outputBytes, 0, cypheredBytes, "UTF8");
//            return new String(utf8, "UTF8");
//            return new String(utf8);//, 0, utf8.length);
        } catch (Exception e) {
            //requested cryptographic algorithm is not available
            logger.error(e);
            throw new Throwable(e.getMessage());
        }
    }

    protected String cryptingDecrypting(String textToCrypting) throws Throwable {
        //Object of this class provides the functionality for
        //encryption and decryption.
        Cipher cipher;

        try {
            //parameter "DES" specifies type of cipher we want to create
            //through the factory method. It includes algorithm, mode and
            //padding. You can define only algorithm and in that case default
            //values will be used for mode and padding.
            cipher = Cipher.getInstance("DES");
        } catch (Exception ex) {
            //requested cryptographic algorithm is not available
            throw new Throwable(ex.getMessage());
        }

        //The lenght of keyString is 8. It's important characteristic
        //for encryption
        String keyString = "testtest";

        byte[] keyData = keyString.getBytes();

        //key object specifies a secret key
        //it uses to construct the secret key for our cipher object from a byte
        //array
        SecretKeySpec key = new SecretKeySpec(keyData, 0, keyData.length,
                "DES");

        try {
            //initializing with setting up the type of operation it's to be
            //used for. In this case - for encryption.
            cipher.init(Cipher.ENCRYPT_MODE, key);
        } catch (InvalidKeyException ex) {
            //given key object is inappropriate for initializing this cipher
            throw new Throwable(ex.getMessage());
        }

        int cypheredBytes;
        int cypheredBytesAlt;

//        byte[] inputBytes = textToCrypting.getBytes();
//        byte[] outputBytes = new byte[1000];
        char[] charArray = textToCrypting.toCharArray();
        byte[] bArray = new byte[textToCrypting.toCharArray().length];
        for (int j=0; j < charArray.length; j++) {
            bArray[j] = (byte)charArray[j];                    
        }
        for (int j=0; j < charArray.length; j++) {
            charArray[j] = (char)bArray[j];
        }
        charArray.toString();
        bArray.toString();
        byte[] inputBytes = new BASE64Decoder().decodeBuffer(textToCrypting);
        byte[] outputBytes = new byte[1000];
        byte[] outputBytesAlt = new byte[1000];

        String bytesAsString = new BASE64Encoder().encode(inputBytes);

        try {
            //doFinal method encrypts data to outputBytes array.
            //for unknown or big counts of data update method more recomended
            //counts of crypted bytes saved inside cypheredBytes variable
            cypheredBytes = cipher.doFinal(inputBytes, 0, inputBytes.length,
                    outputBytes, 0);

            cypheredBytesAlt = cipher.doFinal(bArray, 0, bArray.length,
                    outputBytesAlt, 0);
        } catch (Exception e) {
            throw new Throwable(e.getMessage());
        }

        String str = new String(outputBytes, 0, cypheredBytes);
        inputBytes = str.getBytes();

       try {
            //change state of the cipher object for decrypting
            cipher.init(Cipher.DECRYPT_MODE, key);
        } catch (InvalidKeyException ex) {
            throw new Throwable(ex.getMessage());
        }

        try {
            //decrypts data
            cypheredBytes = cipher.doFinal(inputBytes, 0, inputBytes.length,
                                                outputBytes, 0);
            cypheredBytesAlt = cipher.doFinal(inputBytes, 0, inputBytes.length,
                                                outputBytes, 0);
        } catch (Exception ex) {

            throw new Throwable(ex.getMessage());
        }
        return new String(outputBytes, 0, cypheredBytes);
    }
}
