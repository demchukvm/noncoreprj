package com.tmx.beng.base.dbfunctions;

import com.tmx.beng.base.cache.InvokeFunctionException;
import com.tmx.beng.base.BillingMessage;

import java.util.Map;
import java.math.BigDecimal;

/**
    Check the payment amount is positive and no more fractional then 0.01.
 */
public class TransactionPositiveAndFractionalLimit extends BasicLimitFunction{
    /** Round to fractinal digit */
    private static final int ROUND_TO_DIGIT = 2;

    public void checkLimit(Map actualParams) throws InvokeFunctionException {
        double amount = Double.parseDouble(task.getProcesedMessage().getAttributeString(BillingMessage.AMOUNT));

        //check positive amount
        if(amount <= 0)
            throw new InvokeFunctionException("err.bill.dbfunctions.non_positive_amount", new String[]{String.valueOf(amount)});

        //check fraction power
        BigDecimal amountBD = new BigDecimal(amount);
        amountBD = amountBD.setScale(ROUND_SCALE, BigDecimal.ROUND_HALF_UP);
        if(amountBD.doubleValue() != amount)
            throw new InvokeFunctionException("err.bill.dbfunctions.excessive_fraction", new String[]{String.valueOf(amount), String.valueOf(ROUND_TO_DIGIT)});    
    }

    public ValidationError[] validate(Map actualParams) {
        return null;  
    }

    public TariffParameter[] getDefaultParams() {
        return null;
    }
}
