package com.tmx.beng.base;

//import com.tmx.beng.httpsgate.cyberplat.CyberplatGate;
import com.tmx.beng.medaccess.*;
import com.tmx.beng.medaccess.citypay.CitypayGateForLifeExcMediumAccess;
import com.tmx.beng.medaccess.cyberplat.CyberplatGateMediumAccess;
import com.tmx.beng.medaccess.dacard.DacardGateMediumConnection;
import com.tmx.beng.medaccess.dacard.DacardGateMediumAccess;
import com.tmx.beng.medaccess.globalmoney.GlobalMoneyGateMediumAccess;
import com.tmx.beng.medaccess.life.LifeGateMediumAccess;
import com.tmx.beng.medaccess.ussd.UssdGateMediumAccess;
import com.tmx.beng.medaccess.ussd.UssdGateReserveMediumAccess;
import com.tmx.beng.medaccess.umc.UmcGateMediumAccess;
import com.tmx.beng.medaccess.test.TestGateMediumAccess;
import com.tmx.beng.medaccess.kyivstar.KyivstarGateMediumAccess;
import com.tmx.beng.medaccess.kyivstar.KyivstarExclusiveGateMediumAccess;
import com.tmx.beng.medaccess.kyivstar.KyivstarBonusNoCommissionGateMediumAccess;
import com.tmx.beng.medaccess.kyivstar.KyivstarBonusCommissionGateMediumAccess;
import com.tmx.beng.medaccess.beeline.BeelineGateMediumAccess;
import com.tmx.beng.medaccess.avancel.AvancelGateMediumAccess1;
import com.tmx.beng.medaccess.avancel.AvancelGateMediumAccess2;
import com.tmx.beng.medaccess.avancel.AvancelGateMediumAccess3;
import com.tmx.beng.medaccess.citypay.CitypayGateMediumAccess;
import com.tmx.util.InitException;

import java.util.Map;
import java.util.HashMap;
import java.util.Set;

/**
 RM: Poor hardcoded implementation of medium access pool  
 */
public class MediumAccessPool{

    private Map<String,MediumAccess> pool = new HashMap<String,MediumAccess>();
    private Map<String,MediumAccess> removedElements = new HashMap<String,MediumAccess>();

    /** initialize within billing engine initialization */
    public void init() throws InitException {
        try {
            MediumAccess newAccess = new UssdGateMediumAccess();// ctx.lookup("java:comp/services/medium/UssdGate");
            newAccess.init();
            pool.put("ussd_gate", newAccess);

            newAccess = new UssdGateReserveMediumAccess();// ctx.lookup("java:comp/services/medium/UssdGate");
            newAccess.init();
            pool.put("ussd_gate_reserve", newAccess);

            newAccess = new KyivstarGateMediumAccess();// ctx.lookup("java:comp/services/medium/UssdGate");
            newAccess.init();
            pool.put("kyivstar_gate", newAccess);

            newAccess = new KyivstarExclusiveGateMediumAccess();// ctx.lookup("java:comp/services/medium/UssdGate");
            newAccess.init();
            pool.put("kyivstar_exclusive_gate", newAccess);

            newAccess = new KyivstarBonusCommissionGateMediumAccess();// ctx.lookup("java:comp/services/medium/UssdGate");
            newAccess.init();
            pool.put("kyivstar_bonus_commission_gate", newAccess);

            newAccess = new KyivstarBonusNoCommissionGateMediumAccess();// ctx.lookup("java:comp/services/medium/UssdGate");
            newAccess.init();
            pool.put("kyivstar_bonus_no_commission_gate", newAccess);

            newAccess = new BeelineGateMediumAccess();// ctx.lookup("java:comp/services/medium/UssdGate");
            newAccess.init();
            pool.put("beeline_gate", newAccess);

            newAccess = new UmcGateMediumAccess();// ctx.lookup("java:comp/services/medium/UssdGate");
            newAccess.init();
            pool.put("umc_gate", newAccess);

            //Life HTTPS gate
            newAccess = new LifeGateMediumAccess();// ctx.lookup("java:comp/services/medium/LifeGate");
            newAccess.init();
            pool.put("life_gate", newAccess);

//            //Avancel HTTPS gate No 1
            newAccess = new AvancelGateMediumAccess1();// ctx.lookup("java:comp/services/medium/LifeGate");
            newAccess.init();
            pool.put("avancel_gate_1", newAccess);

            //Avancel HTTPS gate No 2
            newAccess = new AvancelGateMediumAccess2();// ctx.lookup("java:comp/services/medium/LifeGate");
            newAccess.init();
            pool.put("avancel_gate_2", newAccess);

            //Avancel HTTPS gate No 3
            newAccess = new AvancelGateMediumAccess3();// ctx.lookup("java:comp/services/medium/LifeGate");
            newAccess.init();
            pool.put("avancel_gate_3", newAccess);

            //Citypay HTTPS gate
            newAccess = new CitypayGateMediumAccess();
            newAccess.init();
            pool.put("citypay_gate", newAccess);

            newAccess = new CitypayGateForLifeExcMediumAccess();
            newAccess.init();
            pool.put("citypay_gate_life", newAccess);

            newAccess = new CyberplatGateMediumAccess();
            newAccess.init();
            pool.put("cyberplat_gate", newAccess);

            newAccess = new TestGateMediumAccess();// ctx.lookup("java:comp/services/medium/TestGate");
            newAccess.init();
            pool.put("test_gate", newAccess);

            newAccess = new DacardGateMediumAccess();// ctx.lookup("java:comp/services/medium/DacardtGate");
            newAccess.init();
            pool.put("dacard_gate", newAccess);

            newAccess = new GlobalMoneyGateMediumAccess();
            newAccess.init();
            pool.put("globalmoney_gate", newAccess);

            removedElements.clear();
            
        }catch (Throwable e) {
            throw new InitException("err.csapi.failed_to_obtain_billing_engine_access", e);
        }
    }

    /** For usage from pipeline processors */
    public MediumAccess getMediumAccess(String mediumName){
        return pool.get(mediumName);
    }

    public Set<String> getMediumAccessPoolKeys() {
        return pool.keySet();
    }
    public Set<String> getRemovedElemetsKeys() {
        return removedElements.keySet();
    }

    public void removeElements(Set<String> keys) {
        Object[] keyArray = keys.toArray();
        for (Object key : keyArray) {
            removedElements.put((String)key, pool.remove(key));
        }
    }

    public void restoreElements (Set<String> keys) {
        Object[] keyArray = keys.toArray();
        for (Object key : keyArray) {
            pool.put((String)key, removedElements.remove(key));
        }
    }
}
