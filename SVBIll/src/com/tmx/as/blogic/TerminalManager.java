package com.tmx.as.blogic;

import com.tmx.as.base.EntityManager;
import com.tmx.as.base.FilterWrapper;
import com.tmx.as.base.criterion.CriterionWrapper;
import com.tmx.as.base.criterion.Restrictions;
import com.tmx.as.entities.bill.balance.TerminalBalance;
import com.tmx.as.entities.bill.terminal.Terminal;
import com.tmx.as.entities.bill.terminal.EquipmentType;
import com.tmx.as.entities.bill.trade_putlet.TradePutletType;
import com.tmx.as.exceptions.DatabaseException;

import java.util.List;
import java.util.Date;
import java.util.ArrayList;


public class TerminalManager  extends EntityManager {

    public static final String PRIMARY_BALANCE_DESCRIPTION = "Primary balance";
    public static final String PRIMARY_BALANCE_PREFIX = "_primary_balance";
    public static final String PRIMARY_BALANCE_NAME = "Primary balance";

    //----------------------------------------LOAD-----------------------------------------------
    public List getAllTerminals() throws DatabaseException {
        return RETRIEVE_ALL(Terminal.class, null);
    }

    public List getAllSellersTerminals(Long sellerId) throws DatabaseException {
        CriterionWrapper[] criterions = new CriterionWrapper[] {Restrictions.eq("sellerOwnerId", sellerId)};
        return RETRIEVE_ALL(Terminal.class, criterions, null);
    }

    public List getAllEquipmentTypes() throws DatabaseException {
        return RETRIEVE_ALL(EquipmentType.class, null);
    }

    public List getAllTradePutletTypes() throws DatabaseException {
        return RETRIEVE_ALL(TradePutletType.class, null);
    }

    public Terminal getTerminal(Long terminalId) throws DatabaseException {
        String[] attributes = new String[]{"sellerOwner",
                                            "primaryBalance",
                                            "terminalGroup",
                                            "equipmentType",
                                            "tradePutlet.director",
                                            "tradePutlet.manager",
                                            "tradePutlet.type",
                                            "tradePutlet.address",
                                            "tradePutlet.address.district",
                                            "tradePutlet.address.region",
                                            "tradePutlet.address.city"};
        return (Terminal) RETRIEVE( Terminal.class, terminalId, attributes );
    }

    public Terminal getTerminal(String id) throws DatabaseException {
        Long terminalId = Long.valueOf( id );
        return getTerminal( terminalId );
    }

    public Terminal getTerminalBySN(String sn) throws DatabaseException {
        FilterWrapper filterWrapper = new FilterWrapper("by_SerialNumber");
        filterWrapper.setParameter("serialNumberStr", sn);
        FilterWrapper[] wrappers = new FilterWrapper[]{filterWrapper};
        return (Terminal) new EntityManager().RETRIEVE(Terminal.class, wrappers, null);
    }
    public List<Terminal> getTerminalsBySN(List<String> serialNumbers) throws DatabaseException {
        CriterionWrapper [] criterions = new CriterionWrapper[]{Restrictions.in("serialNumber", serialNumbers)};
        return new EntityManager().RETRIEVE_ALL(Terminal.class, criterions, null);
    }

    //-----------------------------------------DELETE------------------------------------------------------------
    public void deleteTerminal(Long terminalId) throws DatabaseException {
        DELETE(Terminal.class, terminalId );
    }

    public void deleteTerminal(String id) throws DatabaseException {
        Long terminalId = Long.valueOf( id );
        deleteTerminal( terminalId );
    }

    public void deleteTerminal(Terminal terminal) throws DatabaseException {
        deleteTerminal( terminal.getTerminalId() );
    }

    //---------------------------------------------SAVE-----------------------------------------------------------
    public void saveTerminal(Terminal savedTerminal) throws DatabaseException {
        BEGIN(savedTerminal);
            if(savedTerminal.getTradePutlet() != null){

                if(savedTerminal.getTradePutlet().getManager() != null)
                    SAVE(savedTerminal.getTradePutlet().getManager());
                if(savedTerminal.getTradePutlet().getDirector() != null)
                    SAVE(savedTerminal.getTradePutlet().getDirector());
                if(savedTerminal.getTradePutlet().getAddress() != null)
                    SAVE(savedTerminal.getTradePutlet().getAddress());

                SAVE(savedTerminal.getTradePutlet());
            }
            SAVE(savedTerminal, new String[]{"sellerOwner", "login", "password", "blocked", "registrationDate", "serialNumber", "hardwareSerialNumber"});

        COMMIT(savedTerminal);
    }

    //Save seller with primary balance
    public void saveTerminal(Terminal savedTerminal, long primaryAmount) throws BlogicException, DatabaseException {
        saveTerminal(savedTerminal, new Long(primaryAmount));
    }
    
    public void nonPermanentUpdateTerminals() throws DatabaseException {
        CriterionWrapper[] criterionWrappers = new CriterionWrapper[] {
                Restrictions.isNull("primaryBalance")
//                Restrictions.eq("primaryBalance", "")
        };
        //retrive all valid terminals without Primary Balance
        List balanceExlcudeTerminals = RETRIEVE_ALL(Terminal.class, criterionWrappers, new String[]{"sellerOwner"});

        for (Object terminal : balanceExlcudeTerminals) {
            Terminal savedTerminal = (Terminal)terminal;
            TerminalBalance primaryBalance = createPrimaryBalanceNonPermanent(savedTerminal, 0L);
            primaryBalance.setTerminal(savedTerminal);
            SAVE(primaryBalance);
            savedTerminal.setPrimaryBalance(primaryBalance);
            SAVE(savedTerminal, new String[]{"primaryBalance"});
        }

    }

    public void saveTerminal(Terminal savedTerminal, Long primaryAmount) throws BlogicException, DatabaseException {
        saveTerminal(savedTerminal, createPrimaryBalance(savedTerminal, primaryAmount));
    }

    public void saveTerminal(Terminal savedTerminal, TerminalBalance primaryBalance) throws BlogicException, DatabaseException {
        if ( !primaryBalance.isTransient() )//if this primaryBalance is assigned to another seller
            throw new BlogicException("err.terminalmanager.balance_is_not_transient");//TODO(A.N.) make it unchecked

        if( !savedTerminal.isTransient() ){//if terminal with primary balance is already exist
            saveTerminal(savedTerminal);
            return;
        }

        try {
            BEGIN(savedTerminal);
                SAVE(savedTerminal.getTradePutlet().getManager());
                SAVE(savedTerminal.getTradePutlet().getDirector());
                SAVE(savedTerminal.getTradePutlet().getAddress());
                SAVE(savedTerminal.getTradePutlet());
                SAVE(savedTerminal);
                primaryBalance.setTerminal(savedTerminal);
                SAVE(primaryBalance);
                savedTerminal.setPrimaryBalance(primaryBalance);
                SAVE(savedTerminal, new String[]{"terminalBalance"});
            COMMIT(savedTerminal);
        } catch (DatabaseException e) {
            ROLLBACK(savedTerminal);
            throw new DatabaseException("err.terminalmanager.terminal_creation_failed", e);
        }
    }

    //primary terminal balance for terminal
    private TerminalBalance createPrimaryBalance(Terminal terminal, Long amount){
        TerminalBalance terminalBalance = new TerminalBalance();

        terminalBalance.setBlocked(false);
        terminalBalance.setDescription(PRIMARY_BALANCE_DESCRIPTION);
        terminalBalance.setName(PRIMARY_BALANCE_NAME);
        terminalBalance.setRegistrationDate(new Date());
        terminalBalance.setCode(terminal.getSerialNumber() + PRIMARY_BALANCE_PREFIX);
        terminalBalance.setAmount(new Double(amount.doubleValue()));
        
        return terminalBalance;
    }
    //primary terminal balance for terminal
    private TerminalBalance createPrimaryBalanceNonPermanent(Terminal terminal, Long amount){
        TerminalBalance terminalBalance = new TerminalBalance();

        terminalBalance.setBlocked(false);
        terminalBalance.setDescription(PRIMARY_BALANCE_DESCRIPTION);
        terminalBalance.setName(PRIMARY_BALANCE_NAME);
        terminalBalance.setRegistrationDate(new Date());
        terminalBalance.setCode(terminal.getSerialNumber() + PRIMARY_BALANCE_PREFIX);
        terminalBalance.setAmount(new Double(amount.doubleValue()));

        return terminalBalance;
    }

}
