package com.tmx.beng.medaccess;

import com.tmx.util.StructurizedException;

import java.util.Locale;

/**
 */
public class GateException extends StructurizedException {

    public GateException(String msg, String[] params, Throwable e, Locale locale) {
        super(msg, params, e, locale);
    }

    public GateException(String msg, String[] params) {
        super(msg, params);
    }

    public GateException(String msg, String[] params, Locale locale) {
        super(msg, params, locale);
    }

    public GateException(String msg, Throwable e, Locale locale) {
        super(msg, e, locale);
    }

    public GateException(String msg, Throwable e) {
        super(msg, e);
    }

    public GateException(String msg, Locale locale) {
        super(msg, locale);
    }

    public GateException(String msg) {
        super(msg);
    }

    public GateException(String msg, String[] params, Throwable e) {
        super(msg, params, e);
    }
}
