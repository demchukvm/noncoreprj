<%@ page language="java" pageEncoding="UTF-8" contentType="text/html; charset=utf-8"
         errorPage="/core/jsp/message/jsperror.jsp" isELIgnored="false" %>
<%@ taglib uri="/WEB-INF/tld/struts-nested.tld" prefix="nested" %>
<%@ taglib uri="/WEB-INF/tld/tmx-controls.tld" prefix="tmx_ctrl" %>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>


<html:form action="/core/dictionary/seller/infoSeller">
<table border="0" width="100%">

    <!-- BUTTONS -->
    <tr>
        <td style="text-align: left">
            <tmx_ctrl:button property="onApplyButton"/>
            <tmx_ctrl:button property="onSaveButton"/>
            <tmx_ctrl:button property="onResetButton" onClick="javascript:document.forms[0].reset()"/>
            <tmx_ctrl:button property="onCancelButton"/>

        </td>
    </tr>
    <tr>
        <td valign="center" align="center" class="tabForm">
            <table width="100%" border="0">
                <%--<!-- Form header -->--%>
                <tr>
                    <td colspan="2">
                        <h4 class="dataLabel">
                            <bean:message bundle="core.labels" key="core.dictionary.seller.infosellerform.titlegeneral"/>
                        </h4>
                    </td>
                </tr>

                <tr>
                    <td class="dataLabel" width="15%">
                        <tmx_ctrl:writeEntityLabel attrName="sellerId"
                                                   entityClassName="com.tmx.as.entities.bill.seller.Seller"/>:
                    </td>
                    <td class="dataField" width="35%">
                        <tmx_ctrl:editBox property="editBoxSellerId"/>
                    </td>

                    <td class="dataLabel" width="15%">
                        <tmx_ctrl:writeEntityLabel attrName="parentSeller"
                                                   entityClassName="com.tmx.as.entities.bill.seller.Seller"/>:
                    </td>
                    <td class="dataField" width="35%">
                        <tmx_ctrl:selectBox property="scrollBoxParentSellers"/>
                    </td>

                </tr>

                <tr>
                    <td class="dataLabel">
                        <tmx_ctrl:writeEntityLabel attrName="name"
                                                   entityClassName="com.tmx.as.entities.bill.seller.Seller"/>:
                    </td>
                    <td class="dataField">
                        <tmx_ctrl:editBox property="editBoxName"/>
                    </td>

                    <td class="dataLabel">
                        <tmx_ctrl:writeEntityLabel attrName="code"
                                                   entityClassName="com.tmx.as.entities.bill.seller.Seller"/>:
                    </td>
                    <td class="dataField" width="35%">
                        <tmx_ctrl:editBox property="editBoxCode"/>
                    </td>
                </tr>

                <tr>
                    <td class="dataLabel" width="15%">
                        <tmx_ctrl:writeEntityLabel attrName="registrationDate"
                                                   entityClassName="com.tmx.as.entities.bill.seller.Seller"/>:
                    </td>
                    <td class="dataField" width="35%">
                        <tmx_ctrl:timeSelector property="timeSelectorRegistrationDate"/>
                    </td>

                    <td class="dataLabel">
                        <tmx_ctrl:writeEntityLabel attrName="description"
                                                   entityClassName="com.tmx.as.entities.bill.seller.Seller"/>:
                    </td>
                    <td class="dataField">
                        <tmx_ctrl:textArea property="textAreaDescription"/>
                    </td>
                </tr>

                <tr>
                    <td class="dataLabel">
                        <tmx_ctrl:writeEntityLabel attrName="{@name}"
                                                   entityClassName="com.tmx.as.entities.bill.processing.Processing"/>:
                    </td>
                    <td class="dataField">
                        <tmx_ctrl:selectBox property="scrollBoxProcessings"/>
                    </td>

                    <td class="dataLabel">
                        <tmx_ctrl:writeEntityLabel attrName="active"
                                                   entityClassName="com.tmx.as.entities.bill.seller.Seller"/>:
                    </td>
                    <td class="dataField">
                        <tmx_ctrl:checkBox property="checkBoxActive"/>
                    </td>
                </tr>
            </table>
        </td>

    </tr>

    <tr>
    <td valign="center" align="center" class="tabForm">
    <table width="100%" border="0">

        <tr>
            <td colspan="2">
                <h4 class="dataLabel">
                    <tmx_ctrl:writeEntityLabel attrName="billingParameters"
                                       entityClassName="com.tmx.as.entities.bill.seller.Seller"/>:
                </h4>
            </td>

            <%--<td class="dataLabel">--%>
            <%--<tmx_ctrl:writeEntityLabel attrName="isattachRestriction"--%>
                                       <%--entityClassName="com.tmx.as.entities.bill.seller.Seller"/>:--%>
            <%--</td>--%>
            <%--<td class="dataField" width="35%">--%>
                <%--<tmx_ctrl:checkBox property="checkBoxIsAttachRestriction"/>--%>
            <%--</td>--%>
        </tr>

        <tr>
                  <%--Restriction--%>
            <td class="dataLabel" width="250">
            <tmx_ctrl:writeEntityLabel attrName="isattachRestriction"
                                       entityClassName="com.tmx.as.entities.bill.seller.Seller"/>:
            </td>
            <td class="dataField" width="35%">
                <tmx_ctrl:checkBox property="checkBoxIsAttachRestriction"/>
            </td>

            <td class="dataLabel">
            <tmx_ctrl:writeEntityLabel attrName="minimumSellerBalance"
                                       entityClassName="com.tmx.as.entities.bill.seller.Seller"/>:
            </td>
            <td class="dataField" width="35%">
                <tmx_ctrl:editBox property="editBoxMinSellerBalance"/>
            </td>
        </tr>

        <tr>
                  <%--KS--%>
            <td class="dataLabel" width="200">
            <b> <tmx_ctrl:writeEntityLabel attrName="isattachKSTariff"
                                       entityClassName="com.tmx.as.entities.bill.seller.Seller"/>: </b>
            </td>
            <td class="dataField" width="35%">
                <tmx_ctrl:checkBox property="checkBoxIsAttachKSTariff"/>
            </td>
                  <%--MTS--%>
            <td class="dataLabel" width="200">
            <b><tmx_ctrl:writeEntityLabel attrName="isattachMTSTariff"
                                       entityClassName="com.tmx.as.entities.bill.seller.Seller"/>: </b>
            </td>
            <td class="dataField" width="35%">
                <tmx_ctrl:checkBox property="checkBoxIsAttachMTSTariff"/>
            </td>
        </tr>
        <tr>
                <%--KS--%>
            <td class="dataLabel">
            <tmx_ctrl:writeEntityLabel attrName="discountPercent"
                                       entityClassName="com.tmx.as.entities.bill.seller.Seller"/>:
            </td>
            <td class="dataField" width="35%">
                <tmx_ctrl:editBox property="editBoxKSDiscountPercent"/>
            </td>
                <%--MTS--%>
            <td class="dataLabel">
            <tmx_ctrl:writeEntityLabel attrName="discountPercent"
                                       entityClassName="com.tmx.as.entities.bill.seller.Seller"/>:
            </td>
            <td class="dataField" width="35%">
                <tmx_ctrl:editBox property="editBoxMTSDiscountPercent"/>
            </td>
        </tr>

        <tr>
                <%--KS--%>
            <td class="dataLabel" width="200">
            <tmx_ctrl:writeEntityLabel attrName="sellerDelta"
                                       entityClassName="com.tmx.as.entities.bill.seller.Seller"/>:
            </td>
            <td class="dataField" width="35%">
                <tmx_ctrl:editBox property="editBoxSellerDelta"/>
            </td>
                <%--MTS--%>
            <%--<td class="dataLabel" width="200">--%>
            <%--<tmx_ctrl:writeEntityLabel attrName="discountPercent"--%>
                                       <%--entityClassName="com.tmx.as.entities.bill.seller.Seller"/>:--%>
            <%--</td>--%>
            <%--<td class="dataField" width="35%">--%>
                <%--<tmx_ctrl:checkBox property="editBoxMTSDiscountPercent"/>--%>
            <%--</td>--%>
        </tr>

        <tr>
                  <%--Life:)--%>
            <td class="dataLabel" width="200">
            <b> <tmx_ctrl:writeEntityLabel attrName="isattachLifeTariff"
                                       entityClassName="com.tmx.as.entities.bill.seller.Seller"/>: </b>
            </td>
            <td class="dataField" width="35%">
                <tmx_ctrl:checkBox property="checkBoxIsAttachLifeTariff"/>
            </td>

                  <%--Beeline--%>
            <td class="dataLabel" width="200">
            <b> <tmx_ctrl:writeEntityLabel attrName="isattachBeelineTariff"
                                       entityClassName="com.tmx.as.entities.bill.seller.Seller"/>: </b>
            </td>
            <td class="dataField" width="35%">
                <tmx_ctrl:checkBox property="checkBoxIsAttachBeelineTariff"/>
            </td>

        </tr>
        <tr>
                  <%--Life--%>
            <td class="dataLabel">
            <tmx_ctrl:writeEntityLabel attrName="discountPercent"
                                       entityClassName="com.tmx.as.entities.bill.seller.Seller"/>:
            </td>
            <td class="dataField" width="35%">
                <tmx_ctrl:editBox property="editBoxLifeDiscountPercent"/>
            </td>
                 <%--Beeline--%>
            <td class="dataLabel">
            <tmx_ctrl:writeEntityLabel attrName="discountPercent"
                                       entityClassName="com.tmx.as.entities.bill.seller.Seller"/>:
            </td>
            <td class="dataField" width="35%">
                <tmx_ctrl:editBox property="editBoxBeelineDiscountPercent"/>
            </td>
        </tr>

        <tr>
                  <%--CDMA--%>
            <td class="dataLabel" width="200">
            <b> <tmx_ctrl:writeEntityLabel attrName="isattachCdmaTariff"
                                       entityClassName="com.tmx.as.entities.bill.seller.Seller"/>:* </b>
            </td>
            <td class="dataField" width="35%">
                <tmx_ctrl:checkBox property="checkBoxIsAttachCdmaTariff"/>
            </td>

                  <%--INTERT ELECOM--%>
            <td class="dataLabel" width="200">
            <b> <tmx_ctrl:writeEntityLabel attrName="isattachInterTelecomTariff"
                                       entityClassName="com.tmx.as.entities.bill.seller.Seller"/>: -</b>
            </td>
            <td class="dataField" width="35%">
                <tmx_ctrl:checkBox property="checkBoxIsAttachInterTelecomTariff"/>
            </td>

        </tr>
        <tr>
                  <%--CDMA--%>
            <td class="dataLabel">
            <tmx_ctrl:writeEntityLabel attrName="discountPercent"
                                       entityClassName="com.tmx.as.entities.bill.seller.Seller"/>:
            </td>
            <td class="dataField" width="35%">
                <tmx_ctrl:editBox property="editBoxCdmaDiscountPercent"/>
            </td>
                 <%--INTER TELECOM--%>
            <td class="dataLabel">
            <tmx_ctrl:writeEntityLabel attrName="discountPercent"
                                       entityClassName="com.tmx.as.entities.bill.seller.Seller"/>:
            </td>
            <td class="dataField" width="35%">
                <tmx_ctrl:editBox property="editBoxInterTelecomDiscountPercent"/>
            </td>
        </tr>

    </table>
    </td>
    </tr>

    <tr>
    <td valign="center" align="center" class="tabForm">
    <table width="100%" border="0">
    
    <tr>
        <td colspan="2">
            <h4 class="dataLabel">
                <bean:message bundle="core.labels" key="core.dictionary.seller.infosellerform.titlephysicaladdress"/>
            </h4>
        </td>

        <td colspan="2">
            <h4 class="dataLabel">
                <bean:message bundle="core.labels" key="core.dictionary.seller.infosellerform.titlegeneraladdress"/>
            </h4>
        </td>
    </tr>

    <tr>
        <td class="dataLabel" width="15%">
            <tmx_ctrl:writeEntityLabel attrName="district"
                                       entityClassName="com.tmx.as.entities.bill.address.Address"/>:

        </td>
        <td class="dataField" width="35%">
            <tmx_ctrl:selectBox property="scrollBoxDistrictPhys"/>
        </td>

        <td class="dataLabel">
            <tmx_ctrl:writeEntityLabel attrName="district"
                                       entityClassName="com.tmx.as.entities.bill.address.Address"/>:
        </td>
        <td class="dataField" width="35%">
            <tmx_ctrl:selectBox property="scrollBoxDistrictLegal"/>
        </td>
    </tr>

    <tr>
        <td class="dataLabel" width="15%">
            <tmx_ctrl:writeEntityLabel attrName="region"
                                       entityClassName="com.tmx.as.entities.bill.address.Address"/>:

        </td>
        <td class="dataField" width="35%">
            <tmx_ctrl:selectBox property="scrollBoxRegionPhys"/>
        </td>

        <td class="dataLabel">
            <tmx_ctrl:writeEntityLabel attrName="region"
                                       entityClassName="com.tmx.as.entities.bill.address.Address"/>:
        </td>
        <td class="dataField">
            <tmx_ctrl:selectBox property="scrollBoxRegionLegal"/>
        </td>
    </tr>

    <tr>
        <td class="dataLabel" width="15%">
            <tmx_ctrl:writeEntityLabel attrName="zipCode"
                                       entityClassName="com.tmx.as.entities.bill.address.Address"/>:

        </td>
        <td class="dataField" width="35%">
            <tmx_ctrl:editBox property="editBoxZipCodePhys"/>
        </td>

        <td class="dataLabel">
            <tmx_ctrl:writeEntityLabel attrName="zipCode"
                                       entityClassName="com.tmx.as.entities.bill.address.Address"/>:
        </td>
        <td class="dataField">
            <tmx_ctrl:editBox property="editBoxZipCodeLegal"/>
        </td>
    </tr>

    <tr>
        <td class="dataLabel" width="15%">
            <tmx_ctrl:writeEntityLabel attrName="city"
                                       entityClassName="com.tmx.as.entities.bill.address.Address"/>:

        </td>
        <td class="dataField" width="35%">
            <tmx_ctrl:selectBox property="scrollBoxCityPhys"/>
        </td>

        <td class="dataLabel">
            <tmx_ctrl:writeEntityLabel attrName="city"
                                       entityClassName="com.tmx.as.entities.bill.address.Address"/>:
        </td>
        <td class="dataField">
            <tmx_ctrl:selectBox property="scrollBoxCityLegal"/>
        </td>
    </tr>
    <!--street-->
    <tr>
        <td class="dataLabel" width="15%">
            <tmx_ctrl:writeEntityLabel attrName="addressLine1"
                                       entityClassName="com.tmx.as.entities.bill.address.Address"/>:

        </td>
        <td class="dataField" width="35%">
            <tmx_ctrl:editBox property="editBoxAddressPhys"/>
        </td>

        <td class="dataLabel">
            <tmx_ctrl:writeEntityLabel attrName="addressLine1"
                                       entityClassName="com.tmx.as.entities.bill.address.Address"/>:
        </td>
        <td class="dataField">
            <tmx_ctrl:editBox property="editBoxAddressLegal"/>
        </td>
    </tr>
     <!--# build-->
    <tr>
        <td class="dataLabel" width="15%">
            <tmx_ctrl:writeEntityLabel attrName="building"
                                       entityClassName="com.tmx.as.entities.bill.address.Address"/>:

        </td>
        <td class="dataField" width="35%">
            <tmx_ctrl:editBox property="editBoxBuildingPhys"/>
        </td>

        <td class="dataLabel">
            <tmx_ctrl:writeEntityLabel attrName="building"
                                       entityClassName="com.tmx.as.entities.bill.address.Address"/>:
        </td>
        <td class="dataField">
            <tmx_ctrl:editBox property="editBoxBuildingLegal"/>
        </td>
    </tr>

    <tr>
        <td class="dataLabel" width="15%">
            <tmx_ctrl:writeEntityLabel attrName="office"
                                       entityClassName="com.tmx.as.entities.bill.address.Address"/>:

        </td>
        <td class="dataField" width="35%">
            <tmx_ctrl:editBox property="editBoxOfficePhys"/>
        </td>

        <td class="dataLabel">
            <tmx_ctrl:writeEntityLabel attrName="office"
                                       entityClassName="com.tmx.as.entities.bill.address.Address"/>:
        </td>
        <td class="dataField">
            <tmx_ctrl:editBox property="editBoxOfficeLegal"/>
        </td>
    </tr>

    <tr>
        <td class="dataLabel" width="15%">
            <tmx_ctrl:writeEntityLabel attrName="note"
                                       entityClassName="com.tmx.as.entities.bill.address.Address"/>:

        </td>
        <td class="dataField" width="35%">
            <tmx_ctrl:textArea property="textAreaNotePhys"/>
        </td>

        <td class="dataLabel">
            <tmx_ctrl:writeEntityLabel attrName="note"
                                       entityClassName="com.tmx.as.entities.bill.address.Address"/>:
        </td>
        <td class="dataField">
            <tmx_ctrl:textArea property="textAreaNoteLegal"/>
        </td>
    </tr>


    </table>
    </td>
    </tr>
    <!--contact data-->
    <tr>
        <td valign="center" align="center" class="tabForm">
            <table width="100%" border="0">

                <tr>
                    <td colspan="2">
                        <h4 class="dataLabel">
                            <bean:message bundle="core.labels" key="core.dictionary.seller.infosellerform.titlecontact"/>
                        </h4>
                    </td>
                </tr>


                <tr>
                    <td class="dataLabel">
                        <tmx_ctrl:writeEntityLabel attrName="phone"
                                                   entityClassName="com.tmx.as.entities.bill.seller.Seller"/>:
                    </td>
                    <td class="dataField">
                        <tmx_ctrl:editBox property="editBoxPhone0"/>
                    </td>
                    <td class="dataLabel">
                        <tmx_ctrl:writeEntityLabel attrName="eMail"
                                                   entityClassName="com.tmx.as.entities.bill.seller.Seller"/>:
                    </td>
                    <td class="dataField">
                        <tmx_ctrl:editBox property="editBoxMail0"/>
                    </td>
                </tr>

                <tr>
                    <td class="dataLabel">
                    </td>
                    <td class="dataField">
                        <tmx_ctrl:editBox property="editBoxPhone1"/>
                    </td>
                    <td class="dataLabel">
                    </td>
                    <td class="dataField">
                        <tmx_ctrl:editBox property="editBoxMail1"/>
                    </td>
                </tr>

                <tr>
                    <td class="dataLabel">
                        <tmx_ctrl:writeEntityLabel attrName="mobPhone"
                                                   entityClassName="com.tmx.as.entities.bill.seller.Seller"/>:
                    </td>
                    <td class="dataField">
                        <tmx_ctrl:editBox property="editBoxMobPhone0"/>
                    </td>
                    <td class="dataLabel">
                    </td>
                    <td class="dataField">
                        <tmx_ctrl:editBox property="editBoxMail2"/>
                    </td>
                </tr>
                <tr>
                    <td class="dataLabel">
                    </td>
                    <td class="dataField">
                        <tmx_ctrl:editBox property="editBoxMobPhone1"/>
                    </td>
                    <td class="dataLabel">
                    </td>
                    <td class="dataField">
                    </td>
                </tr>
            </table>
        </td>
    </tr>

    <!--company props-->
    <tr>
        <td valign="center" align="center" class="tabForm">
            <table width="100%" border="0">

                <tr>
                    <td colspan="2">
                        <h4 class="dataLabel">
                            <bean:message bundle="core.labels" key="core.dictionary.seller.infosellerform.titleprops"/>
                        </h4>
                    </td>
                </tr>
                
                <tr>
                    <td class="dataLabel" width="15%">
                        <tmx_ctrl:writeEntityLabel attrName="certificateNumber"
                                                   entityClassName="com.tmx.as.entities.bill.seller.Seller"/>:
                    </td>
                    <td class="dataField" width="35%">
                        <tmx_ctrl:editBox property="editBoxCertificateNumber"/>
                    </td>

                    <td class="dataLabel" width="15%">
                    <tmx_ctrl:writeEntityLabel attrName="{@name}"
                                               entityClassName="com.tmx.as.entities.bill.bank.Bank"/>:

                    </td>
                    <td class="dataField" width="35%">
                        <tmx_ctrl:editBox property="editBoxBankName"/>
                    </td>
                </tr>
                <tr>
                    <td class="dataLabel">
                        <tmx_ctrl:writeEntityLabel attrName="okpo"
                                                   entityClassName="com.tmx.as.entities.bill.seller.Seller"/>:
                    </td>
                    <td class="dataField">
                        <tmx_ctrl:editBox property="editBoxOkpo"/>
                    </td>

                    <td class="dataLabel">
                        <tmx_ctrl:writeEntityLabel attrName="mfo"
                                                   entityClassName="com.tmx.as.entities.bill.bank.Bank"/>:
                    </td>
                    <td class="dataField">
                        <tmx_ctrl:editBox property="editBoxBankMfo"/>
                    </td>
                </tr>
            <tr>
                <td class="dataLabel">
                        <tmx_ctrl:writeEntityLabel attrName="inn"
                                                   entityClassName="com.tmx.as.entities.bill.seller.Seller"/>:
                </td>
                <td class="dataField">
                    <tmx_ctrl:editBox property="editBoxInn"/>
                </td>

                <td class="dataLabel">
                    <tmx_ctrl:writeEntityLabel attrName="accountNumber"
                                               entityClassName="com.tmx.as.entities.bill.bank.BankAccount"/>:
                </td>
                <td class="dataField">
                    <tmx_ctrl:editBox property="editBoxBankAccountNumber"/>
                </td>
            </tr>
            <tr>
                <td class="dataLabel" width="15%">
                        <tmx_ctrl:writeEntityLabel attrName="registrationDate"
                                                   entityClassName="com.tmx.as.entities.bill.contract.Contract"/>:
                </td>
                <td class="dataField" width="35%">
                    <tmx_ctrl:timeSelector property="timeSelectorSignatureDate"/>
                </td>

                <td class="dataLabel">
                        <tmx_ctrl:writeEntityLabel attrName="contractNumber"
                                                   entityClassName="com.tmx.as.entities.bill.contract.Contract"/>:
                </td>
                <td class="dataField">
                    <tmx_ctrl:editBox property="editBoxSymbol"/>
                </td>
            </tr>
            <tr>
                <td class="dataLabel" width="15%">
                </td>
                <td class="dataField" width="35%">
                </td>
                <td class="dataLabel" width="15%">
                    <tmx_ctrl:writeEntityLabel attrName="deadLine"
                                               entityClassName="com.tmx.as.entities.bill.contract.Contract"/>:
                </td>
                <td class="dataField" width="35%">
                    <tmx_ctrl:timeSelector property="timeSelectorDeadLine"/>
                </td>
             </tr>
            </table>
        </td>
    </tr>

   <%--<!-- BUTTONS -->--%>
    <tr>
        <td style="text-align: left">
            <tmx_ctrl:button property="onApplyButton"/>
            <tmx_ctrl:button property="onSaveButton"/>
            <tmx_ctrl:button property="onResetButton" onClick="javascript:document.forms[0].reset()"/>
            <tmx_ctrl:button property="onCancelButton"/>
        </td>
    </tr>
</table>
</html:form>