package com.tmx.beng.base.processors;

import com.tmx.as.base.EntityManager;
import com.tmx.as.base.QueryParameterWrapper;
import com.tmx.as.base.criterion.CriterionWrapper;
import com.tmx.as.base.criterion.Restrictions;
import com.tmx.as.entities.bill.seller.Seller;
import com.tmx.as.entities.bill.tariff.SellerTariff;
import com.tmx.as.entities.bill.terminal.Terminal;
import com.tmx.as.entities.bill.transaction.SellerBalanceTransaction;
import com.tmx.as.exceptions.DatabaseException;
import com.tmx.beng.base.*;
import com.tmx.beng.base.cache.DBCache;
import com.tmx.util.MD5;

import java.util.List;


/**
 */
public class ActivateMobileTerminalProcessor extends TaskProcessor {

    public Task process0(Task task, DBCache dbCache, BillingMessageWritable msg) throws BillException {
        logger.info("Activate mobile terminal...");
        BillingMessageImpl resp = new BillingMessageImpl();

        try {
            EntityManager entityManager = new EntityManager();

            CriterionWrapper criterions1[] = new CriterionWrapper[] {
                    Restrictions.eq("serialNumber", msg.getAttributeString(BillingMessage.TERMINAL_SN))
            };

            Terminal terminal = (Terminal) entityManager.RETRIEVE(Terminal.class, null, criterions1, null);

            if(terminal != null)
            {
                String realTerminalPswd = MD5.getHashString(terminal.getPassword());
                String rqstTerminalPswd = msg.getAttributeString(BillingMessage.TERMINAL_PSWD);
                if(realTerminalPswd.equals(rqstTerminalPswd))
                {
                    CriterionWrapper criterions2[] = new CriterionWrapper[] {
                            Restrictions.eq("sellerId", terminal.getSellerOwnerId())
                    };

                    Seller seller = (Seller) entityManager.RETRIEVE(Seller.class, terminal.getSellerOwnerId());


                    if(seller != null)
                    {
                        resp.setAttributeString(BillingMessage.SELLER_CODE, seller.getCode());

                        String[] service = {
                            "Beeline",
                            "KS1-25",
                            "KS25-99",
                            "KS100+",
                            "Life",
                            "UMCPoP"
                        };

                        for(int i=0; i<service.length; i++)
                        {
                            QueryParameterWrapper[] qpw = {
                                new QueryParameterWrapper("sellerId", seller.getSellerId()),
                                new QueryParameterWrapper("serviceCode", service[i])
                            };

                            List list = new EntityManager().EXECUTE_QUERY(SellerTariff.class, "fee", qpw);

                            if(list.size() > 0)
                            {
                                String fee = list.get(0).toString();
                                if(service[i].equals("Beeline"))
                                {
                                    resp.setAttributeString(BillingMessage.FEE_BEELINE, fee);
                                }
                                else if(service[i].equals("KS1-25"))
                                {
                                    resp.setAttributeString(BillingMessage.FEE_KS0124, fee);
                                }
                                else if(service[i].equals("KS25-99"))
                                {
                                    resp.setAttributeString(BillingMessage.FEE_KS2599, fee);
                                }
                                else if(service[i].equals("KS100+"))
                                {
                                    resp.setAttributeString(BillingMessage.FEE_KS100, fee);
                                }
                                else if(service[i].equals("Life"))
                                {
                                    resp.setAttributeString(BillingMessage.FEE_LIFE, fee);
                                }
                                else if(service[i].equals("UMCPoP"))
                                {
                                    resp.setAttributeString(BillingMessage.FEE_UMCPOP, fee);
                                }
                            }
                        }
                    }
                    else
                    {
                        // диллер не найден
                        logger.info("SellerCode not found");
                    }
                }
                else
                {
                    // пароль не верный
                    logger.info("TerminalPswd is wrong");
                }
            }
            else
            {
                // номер терминала не найден
                logger.info("TerminalSn not found");
            }

        } catch (DatabaseException e) {
            logger.error("DatabaseException", e);
            e.printStackTrace();
        } catch (Exception ee) {
            logger.error("Exception", ee);
            ee.printStackTrace();
        }


        resp.setOperationName(BillingMessage.O_ACTIVATE_TERMINAL_RESULT);


        CallBack callBack = task.getCallBack();
        if(callBack != null)
            callBack.sendBilledMessage(resp);

        return task;
    }
}
