package com.tmx.web.taglib.html;

import com.tmx.as.base.EntityResources;
import com.tmx.as.modules.EntityResourcesModule;
import com.tmx.web.base.ApplicationEnvironment;
import org.apache.struts.taglib.TagUtils;

import javax.servlet.jsp.JspException;
import java.util.Locale;

public class ImgTableOrderingTag extends TableOrderingTag {

    private String orderingAscImage;
    private String orderingDescImage;
    private String noOrderingImage;
    //@depricated. Use 'labelBundle' and 'labelKey' instead.
    private String entityClassName;
    private String hrefStyleClass;
    /**
     * labelBundle refers to resources file.
     * Supported formats:
     *  1) 'web:web_bundle_name', example: 'web:core.labels'
     *  2) 'as:full_entity_class_path', example: 'as:com.tmx.as.entities.general.user.User'
     * */
    private String labelBundle;
    /** label key in resources file. If label key is null then orderByFieldName will
     * be used to evaluate label. */
    private String labelKey;

    //-----------Properties
    public String getEntityClassName() {
        return entityClassName;
    }

    public void setEntityClassName(String entityClassName) {
        this.entityClassName = entityClassName;
    }

    public String getHrefStyleClass() {
        return hrefStyleClass != null ? hrefStyleClass : getDefaultProperty("tag.imgTableOrderingTag.style.hrefStyleClass");
    }

    public void setHrefStyleClass(String hrefStyleClass) {
        this.hrefStyleClass = hrefStyleClass;
    }

    public void setOrderingDescImage(String orderingDescImage) {
        this.orderingDescImage = (orderingDescImage != null && orderingDescImage.startsWith("/") && !orderingDescImage.startsWith(ApplicationEnvironment.getAppDeploymentName()) ? ApplicationEnvironment.getAppDeploymentName() : "") + orderingDescImage;
    }

    public String getOrderingDescImage() {
        return (orderingDescImage != null) ? orderingDescImage : ApplicationEnvironment.getAppDeploymentName() + getDefaultProperty("tag.imgTableOrderingTag.img.orderingDescImage");
    }

    public void setOrderingAscImage(String orderingAscImage) {
        this.orderingAscImage = (orderingAscImage != null && orderingAscImage.startsWith("/") && !orderingAscImage.startsWith(ApplicationEnvironment.getAppDeploymentName()) ? ApplicationEnvironment.getAppDeploymentName() : "") + orderingAscImage;
    }

    public String getOrderingAscImage() {
        return (orderingAscImage != null) ? orderingAscImage : ApplicationEnvironment.getAppDeploymentName() + getDefaultProperty("tag.imgTableOrderingTag.img.orderingAscImage");
    }


    public String getNoOrderingImage() {
        return (noOrderingImage != null) ? noOrderingImage : ApplicationEnvironment.getAppDeploymentName() + getDefaultProperty("tag.imgTableOrderingTag.img.noOrderingImage");
    }

    public void setNoOrderingImage(String noOrderingImage) {
        this.noOrderingImage = (noOrderingImage != null && noOrderingImage.startsWith("/") && !noOrderingImage.startsWith(ApplicationEnvironment.getAppDeploymentName()) ? ApplicationEnvironment.getAppDeploymentName() : "") + noOrderingImage;
    }

    public String getLabelBundle() {
        return labelBundle;
    }

    public void setLabelBundle(String labelBundle) {
        this.labelBundle = labelBundle;
    }

    public String getLabelKey() {
        return labelKey;
    }

    public void setLabelKey(String labelKey) {
        this.labelKey = labelKey;
    }

    //------------------------------Parsing events
    public int doStartTmxTag() throws JspException {
        String tablePagingHTML = renderTableOrderingHTML();
        TagUtils.getInstance().write(pageContext, tablePagingHTML);
        return (EVAL_BODY_TAG);
    }

    private String renderTableOrderingHTML() throws JspException {
        StringBuffer resultHTML = new StringBuffer();
        renderOrderingHref(resultHTML);
        return resultHTML.toString();
    }


    private void renderOrderingHref(StringBuffer resultHTML) throws JspException {

        String orderType;
        String imageName = null;

        if (isOrderAlreadyApplied("asc")) {
            orderType = "desc";
            imageName = getOrderingAscImage();
        }
        else if (isOrderAlreadyApplied("desc")){
            orderType = "asc";
            imageName = getOrderingDescImage();
        }
        else{
            orderType = "asc";
            imageName = getNoOrderingImage();
        }

        //href on title
        resultHTML.append("<a href=\"#\" class=\""+getHrefStyleClass()+"\" ");
        resultHTML.append("onClick=\"document.forms[0].action='");
        resultHTML.append(getRefreshActionUrl());
        resultHTML.append("?");
        String refreshCommand = getRefreshActionCommand();
        if(refreshCommand != null)
            resultHTML.append("command=").append(refreshCommand).append("&");
        resultHTML.append(buildParameterString(orderType));
        resultHTML.append("';document.forms[0].submit()\">");
        resultHTML.append(getLableName());
        resultHTML.append("</a>");

        resultHTML.append("&nbsp;");

        //href on image (has another css style then href on title has)
        resultHTML.append("<a href=\"#\" ");
        resultHTML.append("onClick=\"document.forms[0].action='");
        resultHTML.append(getRefreshActionUrl());
        resultHTML.append("?");
        if(refreshCommand != null)
            resultHTML.append("command=").append(refreshCommand).append("&");
        resultHTML.append(buildParameterString(orderType));
        resultHTML.append("';document.forms[0].submit()\">");
        resultHTML.append(getImageTag(imageName));
        resultHTML.append("</a>");

    }

    private String getImageTag(String imageName) {
        if (imageName == null) return "";
        StringBuffer imageTag = new StringBuffer();
        imageTag.append("<img src=\"");
        imageTag.append(imageName);
        imageTag.append("\" border=\"0\">");
        return imageTag.toString();
    }

    private String getLableName() throws JspException{
        //to support depricated 'entityClassName' attribute
        if(getEntityClassName() != null)
            return getEntityLabel(getEntityClassName(), getOrderByFieldName());

        String labelKey = this.labelKey;        //RM:   use local param to prevent modifing property. 
        String labelBundle = this.labelBundle;  //      Property stores its state between tags usages on pages. release() has no effect.
        //resolve labelBundle
        if(labelKey == null)
            labelKey = getOrderByFieldName();

        if(labelBundle == null && labelKey != null)
            return labelKey;

        if(labelBundle == null || labelKey == null)
            return "";

        final String WEB_PREFIX = "web:";
        final String AS_PREFIX = "as:";
        if(labelBundle.indexOf(AS_PREFIX) == 0)
            return getEntityLabel(labelBundle.substring(AS_PREFIX.length()), labelKey);
        else if(labelBundle.indexOf(WEB_PREFIX) == 0){
            labelBundle = labelBundle.substring(WEB_PREFIX.length());
            String localized = TagUtils.getInstance().message(pageContext, labelBundle, getLocale().toString(), labelKey);
            return localized == null ? "" : localized;
        }
        return labelKey;//return as is.
    }

    private String getEntityLabel(String entityFullClassName, String attrName) {
        Class entityClass = null;
        try {
            entityClass = Class.forName(entityFullClassName);
        }
        catch (ClassNotFoundException e) {
            return "???label_class_not_found:" + entityFullClassName + "???";
        }
        EntityResources entityResources = EntityResourcesModule.getEntityResources();
        Locale currentLocale = getLocale();
        return entityResources.getEntityLabel(entityClass, attrName, currentLocale);
    }


    public void release() {
        super.release();
        this.labelBundle = null;
        this.labelKey = null;
        this.orderingAscImage = null;
        this.orderingDescImage = null;
        this.noOrderingImage = null;
        this.entityClassName = null;
        this.hrefStyleClass = null;        
    }
}
