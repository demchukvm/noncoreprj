package com.tmx.engines.reportengine.aggrtable.validation;

import com.tmx.engines.reportengine.aggrtable.beans.DataValidation;
import com.tmx.util.InitException;

import java.util.List;
import java.util.ArrayList;

/**
 */
public class ValidationRuleFactory {

    /** @return ordered array of validation rules */
    public static ValidationRule[] instantiateRules(DataValidation.ValidationRule[] validationRulesCfg) throws InitException {
        ValidationRule[] rules = null;
        if(validationRulesCfg != null){
            rules = new ValidationRule[validationRulesCfg.length];
            for(int i=0; i<validationRulesCfg.length; i++){
                ValidationRule rule = null;
                try{
                    Class ruleImpl = Class.forName(validationRulesCfg[i].getImplClass());
                    rule = (ValidationRule)ruleImpl.newInstance();
                    rule.init(validationRulesCfg[i].getRunNumber(), validationRulesCfg[i].getFactName());
                }
                catch(ClassNotFoundException e){
                    throw new InitException("err.validation_rule.impl_not_found", new String[]{validationRulesCfg[i].getImplClass()});
                }
                catch(InstantiationException e){
                    throw new InitException("err.validation_rule.failed_to_instantiate_impl", new String[]{validationRulesCfg[i].getImplClass()});
                }
                catch(IllegalAccessException e){
                    throw new InitException("err.validation_rule.failed_to_instantiate_impl", new String[]{validationRulesCfg[i].getImplClass()});
                }
                try{
                    rules[rule.getRunNumber()] = rule;
                }
                catch(IndexOutOfBoundsException e){
                    throw new InitException("err.validation_rule.incorrect_rule_run_number", new String[]{String.valueOf(rule.getRunNumber())});
                }
            }
        }

        //validate rules by run number
        if(rules != null)
            for(int i=0;i<rules.length; i++)
                if(rules[i] == null)
                    throw new InitException("err.validation_rule.inconsistent_rule_run_number", new String[]{String.valueOf(i)});

        return rules;
    }
}
