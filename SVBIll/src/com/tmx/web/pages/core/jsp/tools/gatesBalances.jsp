<%@ page import="com.tmx.web.forms.core.tools.GatesBalancesForm" %>
<%@ taglib prefix="nested" uri="http://jakarta.apache.org/struts/tags-nested" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/tld/tmx-controls.tld" prefix="tmx_ctrl" %>

<%--<html:form action="/GatesBalancesAction">--%>

<% 
	String value = ""; 
	String smile = "";
%>

<table style="border:black 0px dotted;" bgcolor="white" cellspacing="10" cellpadding="5">
    <tr>
		<td><img src='<tmx_ctrl:rewriteUrl href="/core/images/common/gates_logos/elpos_logo.gif"/>' height="15" width="75"></td>
		<%
			value = ((GatesBalancesForm) pageContext.getSession().getAttribute("GatesBalancesForm")).getEditBoxAvancelBalance().getValue().toString();
            smile = ((GatesBalancesForm) pageContext.getSession().getAttribute("GatesBalancesForm")).getAvancelSmile();

            if(smile.equals("/core/images/common/smiles/smile_1.gif")) {
		%>
        <td align="left" style="color:red; border-right:black 1px dotted;border-bottom:black 1px dotted;font-size:small;"><b>Avancel (Elpos)</b></td>
		<td style="color:red; border-right:black 1px dotted;border-bottom:black 1px dotted;font-size:small;" align="right"><b><%= value %></b></td>
		<% } else { %>
		<td align="left" style="border-right:black 1px dotted;border-bottom:black 1px dotted;">Avancel (Elpos)</td>
		<td align="right" style="border-right:black 1px dotted;border-bottom:black 1px dotted;font-size:small;"><%= value %></td>
		<% } %>
		<td><img src='<tmx_ctrl:rewriteUrl href="<%=smile%>"/>'></td>
    </tr>
	
	

	<tr>
		<td><img src='<tmx_ctrl:rewriteUrl href="/core/images/common/gates_logos/cyberplat_logo.gif"/>' height="15" width="75"></td>
		<%
			value = ((GatesBalancesForm) pageContext.getSession().getAttribute("GatesBalancesForm")).getEditBoxCyberplatBalance().getValue().toString();
			smile = ((GatesBalancesForm) pageContext.getSession().getAttribute("GatesBalancesForm")).getCyberplatSmile();
			if(smile.equals("/core/images/common/smiles/smile_1.gif")) {
		%>
        <td align="left" style="color:red; border-right:black 1px dotted;border-bottom:black 1px dotted;font-size:small;"><b>CyberPlat</b></td>
		<td style="color:red; border-right:black 1px dotted;border-bottom:black 1px dotted;font-size:small;" align="right"><b><%= value %></b></td>
		<% } else { %>
		<td align="left" style="border-right:black 1px dotted;border-bottom:black 1px dotted;">CyberPlat</td>
		<td align="right" style="border-right:black 1px dotted;border-bottom:black 1px dotted;font-size:small;"><%= value %></td>
		<% } %>
		<td><img src='<tmx_ctrl:rewriteUrl href="<%=smile%>"/>'></td>
    </tr>
	
	
	
    <tr>
		<td><img src='<tmx_ctrl:rewriteUrl href="/core/images/common/gates_logos/citypay_logo.gif"/>' height="15" width="50"></td>
		<%
			value = ((GatesBalancesForm) pageContext.getSession().getAttribute("GatesBalancesForm")).getEditBoxCitypayBalance().getValue().toString();
            smile = ((GatesBalancesForm) pageContext.getSession().getAttribute("GatesBalancesForm")).getCitypaySmile();
			if(smile.equals("/core/images/common/smiles/smile_1.gif")) {
		%>
        <td align="left" style="color:red; border-right:black 1px dotted;border-bottom:black 1px dotted;font-size:small;"><b>City-Pay</b></td>
		<td style="color:red; border-right:black 1px dotted;border-bottom:black 1px dotted;font-size:small;" align="right"><b><%= value %></b></td>
		<% } else { %>
		<td align="left" style="border-right:black 1px dotted;border-bottom:black 1px dotted;">City-Pay</td>
		<td align="right" style="border-right:black 1px dotted;border-bottom:black 1px dotted;font-size:small;"><%= value %></td>
		<% } %>
		<td><img src='<tmx_ctrl:rewriteUrl href="<%=smile%>"/>'></td>
    </tr>
	
	
	
	
	
    <tr>
		<td><img src='<tmx_ctrl:rewriteUrl href="/core/images/common/gates_logos/citypay_logo.gif"/>' height="15" width="50"></td>
		<%
			value = ((GatesBalancesForm) pageContext.getSession().getAttribute("GatesBalancesForm")).getEditBoxCitypayForLifeBalance().getValue().toString();
            smile = ((GatesBalancesForm) pageContext.getSession().getAttribute("GatesBalancesForm")).getCitypayForLifeSmile();
			if(smile.equals("/core/images/common/smiles/smile_1.gif")) {
		%>
        <td align="left" style="color:red; border-right:black 1px dotted;border-bottom:black 1px dotted;font-size:small;"><b>City-Pay (LifeEx)</b></td>
		<td style="color:red; border-right:black 1px dotted;border-bottom:black 1px dotted;font-size:small;" align="right"><b><%= value %></b></td>
		<% } else { %>
		<td align="left" style="border-right:black 1px dotted;border-bottom:black 1px dotted;">City-Pay (LifeEx)</td>
		<td align="right" style="border-right:black 1px dotted;border-bottom:black 1px dotted;font-size:small;"><%= value %></td>
		<% } %>
		<td><img src='<tmx_ctrl:rewriteUrl href="<%=smile%>"/>'></td>
    </tr>
	
	
	
	
	
    <tr>
		<td><img src='<tmx_ctrl:rewriteUrl href="/core/images/common/gates_logos/ks_logo.gif"/>' height="15" width="73"></td>
		<%
			value = ((GatesBalancesForm) pageContext.getSession().getAttribute("GatesBalancesForm")).getEditBoxKSBalance().getValue().toString();
            smile = ((GatesBalancesForm) pageContext.getSession().getAttribute("GatesBalancesForm")).getKsSmile();
			if(smile.equals("/core/images/common/smiles/smile_1.gif")) {
		%>
        <td align="left" style="color:red; border-right:black 1px dotted;border-bottom:black 1px dotted;font-size:small;"><b>Kyivstar (0SVC)</b></td>
		<td style="color:red; border-right:black 1px dotted;border-bottom:black 1px dotted;font-size:small;" align="right"><b><%= value %></b></td>
		<% } else { %>
		<td align="left" style="border-right:black 1px dotted;border-bottom:black 1px dotted;">Kyivstar (0SVC)</td>
		<td align="right" style="border-right:black 1px dotted;border-bottom:black 1px dotted;font-size:small;"><%= value %></td>
		<% } %>
		<td><img src='<tmx_ctrl:rewriteUrl href="<%=smile%>"/>'></td>
    </tr>
	
	
	
	
	
    <tr>
		<td><img src='<tmx_ctrl:rewriteUrl href="/core/images/common/gates_logos/ks_logo.gif"/>' height="15" width="73"></td>
		<%
			value = ((GatesBalancesForm) pageContext.getSession().getAttribute("GatesBalancesForm")).getEditBoxKSBNCBalance().getValue().toString();
            smile = ((GatesBalancesForm) pageContext.getSession().getAttribute("GatesBalancesForm")).getKsbncSmile();
			if(smile.equals("/core/images/common/smiles/smile_1.gif")) {
		%>
        <td align="left" style="color:red; border-right:black 1px dotted;border-bottom:black 1px dotted;font-size:small;"><b>Kyivstar BonusNoComission (2SVC)</b></td>
		<td style="color:red; border-right:black 1px dotted;border-bottom:black 1px dotted;font-size:small;" align="right"><b><%= value %></b></td>
		<% } else { %>
		<td align="left" style="border-right:black 1px dotted;border-bottom:black 1px dotted;">Kyivstar BonusNoComission (2SVC)</td>
		<td align="right" style="border-right:black 1px dotted;border-bottom:black 1px dotted;font-size:small;"><%= value %></td>
		<% } %>
		<td><img src='<tmx_ctrl:rewriteUrl href="<%=smile%>"/>'></td>
    </tr>
	
	
	
	
	
    <tr>
		<td><img src='<tmx_ctrl:rewriteUrl href="/core/images/common/gates_logos/ks_logo.gif"/>' height="15" width="73"></td>
		<%
			value = ((GatesBalancesForm) pageContext.getSession().getAttribute("GatesBalancesForm")).getEditBoxKSFCBalance().getValue().toString();
            smile = ((GatesBalancesForm) pageContext.getSession().getAttribute("GatesBalancesForm")).getKsfcSmile();
			if(smile.equals("/core/images/common/smiles/smile_1.gif")) {
		%>
        <td align="left" style="color:red; border-right:black 1px dotted;border-bottom:black 1px dotted;font-size:small;"><b>Kyivstar FixConnect (7SVC)</b></td>
		<td style="color:red; border-right:black 1px dotted;border-bottom:black 1px dotted;font-size:small;" align="right"><b><%= value %></b></td>
		<% } else { %>
		<td align="left" style="border-right:black 1px dotted;border-bottom:black 1px dotted;">Kyivstar FixConnect (7SVC)</td>
		<td align="right" style="border-right:black 1px dotted;border-bottom:black 1px dotted;font-size:small;"><%= value %></td>
		<% } %>
		<td><img src='<tmx_ctrl:rewriteUrl href="<%=smile%>"/>'></td>
    </tr>
	
	
	
	
	
    <tr>
		<td><img src='<tmx_ctrl:rewriteUrl href="/core/images/common/gates_logos/mts_logo.gif"/>' height="15" ></td>
		<%
			value = ((GatesBalancesForm) pageContext.getSession().getAttribute("GatesBalancesForm")).getEditBoxMTSBalance().getValue().toString();
            smile = ((GatesBalancesForm) pageContext.getSession().getAttribute("GatesBalancesForm")).getMtsSmile();
			if(smile.equals("/core/images/common/smiles/smile_1.gif")) {
		%>
        <td align="left" style="color:red; border-right:black 1px dotted;border-bottom:black 1px dotted;font-size:small;"><b>MTS</b></td>
		<td style="color:red; border-right:black 1px dotted;border-bottom:black 1px dotted;font-size:small;" align="right"><b><%= value %></b></td>
		<% } else { %>
		<td align="left" style="border-right:black 1px dotted;border-bottom:black 1px dotted;">MTS</td>
		<td align="right" style="border-right:black 1px dotted;border-bottom:black 1px dotted;font-size:small;"><%= value %></td>
		<% } %>
		<td><img src='<tmx_ctrl:rewriteUrl href="<%=smile%>"/>'></td>
    </tr>
	
	
	
    <tr>
        <td align="center" colspan="4">
            <tmx_ctrl:button property="onRefreshBalance"/>
        </td>
    </tr>
</table>
