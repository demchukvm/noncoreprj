package com.tmx.beng.access;

import com.tmx.beng.base.BillException;

/**

 */
public interface Access {
    public Connection obtainConnection() throws BillException;
}
