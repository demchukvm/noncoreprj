package com.tmx.beng.base;

import com.tmx.as.blogic.ExclusiveManager;
import com.tmx.as.exceptions.DatabaseException;
import com.tmx.beng.base.channels.ClientTransactionMaskChannel;
import com.tmx.util.cache.FileCache;
import com.tmx.util.cache.CacheException;
import com.tmx.beng.base.beans.*;
import com.tmx.beng.base.channels.AbstractChannel;
import com.tmx.util.Configuration;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.*;
import java.io.InputStream;

/**
 * Singleton design pattern
 */
public class ChannelResolver {

    private ArrayList beelineExc = new ArrayList();
    private ArrayList beelineFixConnectExc = new ArrayList();
    private ArrayList kyivstarExc = new ArrayList();
    private ArrayList kyivstarFixConnectExc = new ArrayList();
    private ArrayList lifeExc = new ArrayList();
    private ArrayList mtsExc = new ArrayList();

    public boolean isContains(String serviceCode, String cliTransNum)
    {
        if(serviceCode.equals("BeelineExc"))
        {
            for(Object t : beelineExc)
                if(cliTransNum.startsWith(t.toString()))
                    return true;
            return false;
        }
        else if(serviceCode.equals("Beeline_FixConnect"))
        {
            for(Object t : beelineFixConnectExc)
                if(cliTransNum.startsWith(t.toString()))
                    return true;
            return false;
        }
        else if(serviceCode.equals("KyivstarBonusNoCommission"))
        {
            for(Object t : kyivstarExc)
                if(cliTransNum.startsWith(t.toString()))
                    return true;
            return false;
        }
        else if(serviceCode.equals("KS_FixConnect"))
        {
            for(Object t : kyivstarFixConnectExc)
                if(cliTransNum.startsWith(t.toString()))
                    return true;
            return false;
        }
        else if(serviceCode.equals("LifeExc"))
        {
            for(Object t : lifeExc)
                if(cliTransNum.startsWith(t.toString()))
                    return true;
            return false;
        }
        else if(serviceCode.equals("MTSExc"))
        {
            for(Object t : mtsExc)
                if(cliTransNum.startsWith(t.toString()))
                    return true;
            return false;
        }
        else
            return false;
    }

    private void loadAllExclusives() throws DatabaseException {
        loadBeelineExc();
        loadKyivstarExc();
        loadLifeExc();
        loadMtsExc();
    }

    public void loadBeelineExc() throws DatabaseException {
        beelineExc = new ExclusiveManager().getAllTerminalsFromExclusiveTable("ExclusiveBeeline");
        beelineFixConnectExc = new ExclusiveManager().getAllTerminalsFromExclusiveTable("ExclusiveBeeline");
    }

    public void loadKyivstarExc() throws DatabaseException {
        kyivstarExc = new ExclusiveManager().getAllTerminalsFromExclusiveTable("ExclusiveKyivstar");
        kyivstarFixConnectExc = new ExclusiveManager().getAllTerminalsFromExclusiveTable("ExclusiveKyivstar");
    }

    public void loadLifeExc() throws DatabaseException {
        lifeExc = new ExclusiveManager().getAllTerminalsFromExclusiveTable("ExclusiveLife");
    }

    public void loadMtsExc() throws DatabaseException {
        mtsExc = new ExclusiveManager().getAllTerminalsFromExclusiveTable("ExclusiveMTS");
    }


    private static ChannelResolver instance;
    private ChannelCache channelCache;

    private ChannelResolver(){
        channelCache = new ChannelCache();

        try {
            loadAllExclusives();
        } catch (DatabaseException e) {
            e.printStackTrace();
        }
    }

    public static ChannelResolver getInstance(){
        if(instance == null)
            instance = new ChannelResolver();
        return instance;
    }

    public String getChannel(String name, BillingMessage msg, String configFile) throws BillException
    {
        try {
            AbstractChannel inChannel = (AbstractChannel)( (HashMap)channelCache.get(configFile) ).get(name);
            return inChannel != null ? inChannel.process(msg).getName() : name;
        } catch(CacheException e) {
            throw new BillException("err.bill.failed_to_get_channel", new String[]{ configFile }, e);
        }
    }

    public ArrayList getTerminalList(String inChannelName, String outChannelName) throws CacheException
    {
        ArrayList terminals = new ArrayList();

        String configFile = Configuration.getInstance().getProperty("channel_resolver_pre.config_file");
        File file = new File(configFile);
        ChannelResolverDocument root = null;
        try {
            InputStream is = new FileInputStream(file);
            root = ChannelResolverDocument.Factory.parse(is);
        }catch(Throwable e){
            throw new CacheException("err.channel_resolver_pre.failed_to_parse_config", new String[]{ configFile }, e);
        }

        InChannelType[] inChannelTypes = root.getChannelResolver().getInChannelArray();

        for(int i=0; i<inChannelTypes.length; i++)
        {
            if(inChannelTypes[i].getName().equals(inChannelName))
            {
                ParameterListType[] parameterListTypes = inChannelTypes[i].getParameterListArray();
                OutChannelType[] outChannelTypes = inChannelTypes[i].getOutChannelArray();

                for(int j=0; j<outChannelTypes.length; j++)
                {
                    if(outChannelTypes[j].getName().equals(outChannelName))
                    {
                        int id = outChannelTypes[j].getId();

                        for(int k=0; k<parameterListTypes.length; k++)
                        {
                            if(parameterListTypes[k].getName().equals(id+""))
                            {
                                ParameterListType.AddValue[] addValue = parameterListTypes[k].getAddValueArray();

                                for(ParameterListType.AddValue val : addValue)
                                {
                                    terminals.add(val.getValue());
                                }
                            }
                        }
                    }
                }
            }
        }

        Collections.sort(terminals, new Comparator<Object>()
        {
          public int compare(Object t1, Object t2) {
            return t1.toString().compareTo(t2.toString());
          }
        });

        return terminals;
    }

    private static class ChannelCache extends FileCache{

        public ChannelCache() {
            maxSize = 100;
            cleanCount = 10;
            cleanType = CleanAlgorithm.Type.CLEAN_N_SELDOM_ACCESSED;
        }

        protected Object getRealObjectFromFile(InputStream is, String path) throws CacheException {
            ChannelResolverDocument root = getChannelResolverDocument(is, path);
            HashMap channelMap = new HashMap();
            InChannelType[] inChannelTypes = root.getChannelResolver().getInChannelArray();

            for(int i=0; i<inChannelTypes.length; i++){
                AbstractChannel inChannel =  createInChannel(inChannelTypes[i]);
                channelMap.put(inChannelTypes[i].getName(), inChannel);
            }

            return channelMap;
        }

        private AbstractChannel createInChannel(InChannelType inChannelType) throws CacheException {
            try {
                Class channelClass = Class.forName(inChannelType.getImplClass());
                AbstractChannel inChannel = (AbstractChannel) channelClass.newInstance();

                for(int i=0; i<inChannelType.getOutChannelArray().length; i++){
                    OutChannelType outChannelType = inChannelType.getOutChannelArray(i);
                    AbstractChannel.OutChannel outChannel = new AbstractChannel.OutChannel(outChannelType.getId(), outChannelType.getName());
                    inChannel.addOutChannel(outChannel);
                }

                for(int i=0; i<inChannelType.getParameterArray().length; i++){
                    inChannel.addParam(inChannelType.getParameterArray(i).getName(), 
                                       inChannelType.getParameterArray(i).getValue());
                }

                for(int i=0; i<inChannelType.getParameterListArray().length; i++){
                    List paramsList = new ArrayList();
                    ParameterListType pList = inChannelType.getParameterListArray(i);
                    inChannel.addParamList(pList.getName(), paramsList);
                    for(int j=0; j<pList.getAddValueArray().length; j++){
                        paramsList.add(pList.getAddValueArray(j).getValue());
                    }
                }

                return inChannel;
            } catch (InstantiationException e) {
                throw new CacheException("err.channel_resolver.failed_to_create_object",e);
            } catch (IllegalAccessException e) {
                throw new CacheException("err.channel_resolver.failed_to_create_object",e);
            } catch (ClassNotFoundException e) {
                throw new CacheException("err.channel_resolver.failed_to_load_class",e);
            }
        }

        private ChannelResolverDocument getChannelResolverDocument(InputStream is, String path) throws CacheException {
            try{
                return ChannelResolverDocument.Factory.parse(is);
            }catch(Throwable e){
                throw new CacheException("err.channel_resolver.failed_to_parse_config", new String[]{ path }, e);
            }
        }
    }


}
