package com.tmx.as.entities.bill.modem;

import com.tmx.as.base.BasicEntity;
import com.tmx.as.entities.bill.modem_group.ModemGroup;

import java.io.Serializable;

/**
 */
public class Modem extends BasicEntity {
    private Long modemId;
    private ModemGroup modemGroup;

    public Long getModemId() {
        return modemId;
    }

    public void setModemId(Long modemId) {
        this.modemId = modemId;
    }

    public ModemGroup getModemGroup() {
        return modemGroup;
    }

    public void setModemGroup(ModemGroup modemGroup) {
        this.modemGroup = modemGroup;
    }

    public Serializable getId() {
        return getModemId();
    }

    public int getValid() {
        return valid;
    }

    public void setValid(int valid) {
        this.valid = valid;
    }
}
