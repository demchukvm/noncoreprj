package com.tmx.beng.base.processors;

import com.tmx.beng.base.*;
import com.tmx.beng.base.cache.DBCache;
import com.tmx.beng.base.cache.DBCacheException;
import com.tmx.as.entities.bill.operator.Operator;
import com.tmx.as.entities.bill.transaction.*;
import com.tmx.as.entities.bill.balance.OperatorBalance;
import com.tmx.as.entities.bill.balance.TerminalBalance;
import com.tmx.as.entities.bill.balance.SellerBalance;
import com.tmx.as.entities.bill.terminal.Terminal;
import com.tmx.as.entities.bill.seller.Seller;
import com.tmx.as.entities.bill.voucher.Voucher;
import com.tmx.as.base.ProcedureWrapper;
import com.tmx.as.base.QueryParameterWrapper;
import com.tmx.as.blogic.balance.SellerBalanceManager;
import com.tmx.as.blogic.balance.TerminalBalanceManager;
import com.tmx.as.blogic.TransactionManager;
import com.tmx.as.exceptions.DatabaseException;

import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.sql.Types;
import java.math.BigDecimal;

/**
 * Default refill for refill pipe
 */
public class DefaultReservationProcessor extends AbstractReservationProcessor{

    protected List prepareOperatorTransactions(BillingMessageWritable msg, DBCache dbCache, TransConfig config) throws DBCacheException {
        Reservation reservation = config.reservationSet.getReservation(Operator.class, config.operatorCode);

        List transactions = new ArrayList();
        for (Iterator iter = reservation.reservesIterator(); iter.hasNext();) {
            Reservation.Reserve reserve = (Reservation.Reserve) iter.next();
            // operator transaction
            OperatorBalanceTransaction tx = new OperatorBalanceTransaction();
            //time
            tx.setTransactionTime(config.transRegTime);
            tx.setClientTime(config.clientTime);
            //amount
            double amount = reserve.getAmount();
            dbCache.changeOperatorBalance(config.operatorCode, reserve.getBalanceCode(), amount,
                    reserve.getTransTypeCode());
            reserve.markApplied();
            tx.setAmount(new Double(amount));
            //operator
            Operator operator = new Operator();
            operator.setOperatorId(dbCache.getOperator(config.operatorCode).getOperatorId());
            tx.setOperator(operator);
            //operator balance
            OperatorBalance operatorBalance = new OperatorBalance();
            operatorBalance.setBalanceId(
                    dbCache.getOperatorBalance(config.operatorCode, reserve.getBalanceCode()).getBalanceId());
            tx.setOperatorBalance(operatorBalance);
            //BillTransactionNumber
            tx.setTransactionGUID(msg.getAttributeString(BillingMessage.BILL_TRANSACTION_NUM));
            //CliTransactionNumber
            tx.setCliTransactionNum(msg.getAttributeString(BillingMessage.CLI_TRANSACTION_NUM));
            //transaction type
            tx.setType(new TransactionType(reserve.getTransTypeCode()));
            tx.setOperationType(config.operationsType);
            transactions.add(tx);
        }
        return transactions;
    }

    protected List prepareSellerTransactions(BillingMessageWritable msg, DBCache dbCache, TransConfig config) throws DBCacheException {
        String sellerCode = msg.getAttributeString(BillingMessage.SELLER_CODE);
        List sellersParentsChainCodes = dbCache.getSellerWrapperParentsChainCodes(sellerCode);
        List<SellerBalanceTransaction> resultList = new ArrayList<SellerBalanceTransaction>();
        Double amountSubtractFee = Double.parseDouble((String)msg.getAttribute(BillingMessage.AMOUNT));
        for (Object sellersParentsChainCode : sellersParentsChainCodes) {
            String nextSellerCode = (String) sellersParentsChainCode;
            Reservation reservation = config.reservationSet.getReservation(Seller.class, nextSellerCode);
            List<SellerBalanceTransaction> transactions = new ArrayList<SellerBalanceTransaction>();
            for (Iterator iter = reservation.reservesIterator(); iter.hasNext();) {
                Reservation.Reserve reserve = (Reservation.Reserve) iter.next();
                SellerBalanceTransaction tx = new SellerBalanceTransaction();
                //time
                tx.setTransactionTime(config.transRegTime);
                tx.setClientTime(config.clientTime);
                //amount
                double amount = reserve.getAmount();
                dbCache.changeSellerBalance(nextSellerCode, reserve.getBalanceCode(), amount, reserve.getTransTypeCode());
                reserve.markApplied();
                tx.setAmount(reserve.getAmount());
                //save pre tx balance amount & fee for tx
                try {
                    tx.setPreTxBalance((new SellerBalanceManager()).getBalanceByCode(reserve.getBalanceCode()).getAmount());
                } catch (DatabaseException dbe) {
                    logger.error("Can`t set current balance amount " + reserve.getBalanceCode() + " " + dbe.getMessage());
                } catch (Exception e) {
                    logger.error("Can`t set current balance amount " + reserve.getBalanceCode() + " " + e.getMessage());
                }

                if(msg.getAttributeString(BillingMessage.Y) == null)
                {
                    //calculate fee considering fee of subseller
                    try {
                        if (amountSubtractFee < amount) {
                            amountSubtractFee = amountSubtractFee + reserve.getExternalCommission(); //GraduatedRefillTariff takes of 1 hrn
                        }
                        BigDecimal feeBD = new BigDecimal(amountSubtractFee - amount);
                        feeBD = feeBD.setScale(ROUND_SCALE, BigDecimal.ROUND_HALF_UP);
                        //save fee
                        tx.setFee(feeBD.doubleValue());
                        //substruct fee from amount of tx
                        amountSubtractFee = amountSubtractFee - feeBD.doubleValue();
                    } catch (Exception e) {
                        logger.error("Can`t calculate fee of transaction " + e.getMessage());
                    }
                }
                else
                {
                    try {
                        double totalAmount = Double.parseDouble(msg.getAttributeString(BillingMessage.TOTAL_AMOUNT));
                        double y = Double.parseDouble(msg.getAttributeString(BillingMessage.Y));
                        BigDecimal feeBD = new BigDecimal(totalAmount-amount-y);
                        tx.setFee(feeBD.doubleValue());
                    } catch (Exception e) {
                        logger.error("Can`t calculate fee of transaction " + e.getMessage());
                    }
                }

                //seller
                Seller seller = new Seller();
                seller.setSellerId(dbCache.getSeller(msg.getAttributeString(BillingMessage.SELLER_CODE)).getSellerId());
                tx.setSeller(seller);
                //seller balance
                SellerBalance sellerBalance = new SellerBalance();
                sellerBalance.setBalanceId(dbCache.getSellerBalance(nextSellerCode, reserve.getBalanceCode()).getBalanceId());
                tx.setSellerBalance(sellerBalance);
                //BillTransactionNumber
                tx.setTransactionGUID(msg.getAttributeString(BillingMessage.BILL_TRANSACTION_NUM));
                //CliTransactionNumber
                tx.setCliTransactionNum(msg.getAttributeString(BillingMessage.CLI_TRANSACTION_NUM));
                //transaction type
                tx.setType(new TransactionType(reserve.getTransTypeCode()));
                tx.setOperationType(config.operationsType);
                transactions.add(tx);
            }
            resultList.addAll(transactions);
        }

//        for (int i = 0; i < resultList.size(); i ++) {
//            SellerBalanceTransaction stx = resultList.get(i);
//            Seller seller = dbCache.getSeller(stx.getSeller().getCode());
//            if (seller.getP)
//        }

        return resultList;
    }

    private Double calculateFee(BillingMessageWritable msg, Double previosFee, SellerBalanceTransaction tx, double amount) {
        BigDecimal feeBD = new BigDecimal(Double.parseDouble((String)msg.getAttribute(BillingMessage.AMOUNT)) - amount - previosFee);
        feeBD = feeBD.setScale(ROUND_SCALE, BigDecimal.ROUND_HALF_UP);
        //save fee
        tx.setFee(feeBD.doubleValue());
        //save last calculated fee
        previosFee = feeBD.doubleValue();
        return previosFee;
    }

    protected List prepareTerminalTransactions(BillingMessageWritable msg, DBCache dbCache, TransConfig config) throws DBCacheException {
        Reservation reservation = config.reservationSet.getReservation(Terminal.class, config.terminalSn);
        List transactions = new ArrayList();
        for(Iterator iter = reservation.reservesIterator(); iter.hasNext();){
            Reservation.Reserve reserve = (Reservation.Reserve)iter.next();
            TerminalBalanceTransaction tx = new TerminalBalanceTransaction();
            //time
            tx.setTransactionTime(config.transRegTime);
            tx.setClientTime(config.clientTime);
            //amount
            double amount = reserve.getAmount();
            dbCache.changeTerminalBalance(config.terminalSn, reserve.getBalanceCode(), amount, reserve.getTransTypeCode());
            reserve.markApplied();
            tx.setAmount(amount);
            //save pre tx balance amount
            try {
                tx.setPreTxBalance((new TerminalBalanceManager()).getBalanceByCode(reserve.getBalanceCode()).getAmount());
            } catch (DatabaseException dbe) {
                logger.error("Can`t set current balance amount " + reserve.getBalanceCode() + " " + dbe.getMessage());
            } catch (Exception e) {
                logger.error("Can`t set current balance amount " + reserve.getBalanceCode() + " " + e.getMessage());
            }
            //terminal
            Terminal terminal = new Terminal();
            terminal.setTerminalId(dbCache.getTerminal(config.terminalSn).getTerminalId());
            tx.setTerminal(terminal);
            //terminal balance
            TerminalBalance terminalBalance = new TerminalBalance();
            terminalBalance.setBalanceId(dbCache.getTerminalBalance(config.terminalSn, reserve.getBalanceCode()).getBalanceId());
            tx.setTerminalBalance(terminalBalance);
            //BillTransactionNumber
            tx.setTransactionGUID(msg.getAttributeString(BillingMessage.BILL_TRANSACTION_NUM));
            //CliTransactionNumber
            tx.setCliTransactionNum(msg.getAttributeString(BillingMessage.CLI_TRANSACTION_NUM));
            //transaction type
            tx.setType( new TransactionType(reserve.getTransTypeCode()) );
            tx.setOperationType(config.operationsType);
            transactions.add(tx);
        }
        return transactions;
    }

    protected List prepareClientTransaction(BillingMessageWritable msg, DBCache dbCache, TransConfig config) throws DBCacheException {
        ClientTransaction clientTransaction = new ClientTransaction();
        //time
        clientTransaction.setTransactionTime(config.transRegTime);
        clientTransaction.setClientTime(config.clientTime);
        //amount
        Double amount = Double.valueOf(msg.getAttributeString(BillingMessage.AMOUNT));
        clientTransaction.setAmount(amount);
        //terminal
        Terminal terminal = new Terminal();
        terminal.setTerminalId(dbCache.getTerminal(msg.getAttributeString(BillingMessage.TERMINAL_SN)).getTerminalId());
        clientTransaction.setTerminal(terminal);
        //seller
        Seller seller = new Seller();
        seller.setSellerId(dbCache.getSeller(msg.getAttributeString(BillingMessage.SELLER_CODE)).getSellerId());
        clientTransaction.setSeller(seller);
        //operator
        Operator operator = new Operator();
        operator.setOperatorId(dbCache.getOperator(msg.getAttributeString(BillingMessage.SERVICE_CODE)).getOperatorId());
        clientTransaction.setOperator(operator);
        //BillTransactionNumber
        clientTransaction.setTransactionGUID(msg.getAttributeString(BillingMessage.BILL_TRANSACTION_NUM));
        //CliTransactionNumber
        clientTransaction.setCliTransactionNum(msg.getAttributeString(BillingMessage.CLI_TRANSACTION_NUM));
        clientTransaction.setAccountNumber(msg.getAttributeString(BillingMessage.ACCOUNT_NUMBER));
        if(clientTransaction.getAccountNumber() == null)
            clientTransaction.setAccountNumber(msg.getAttributeString(BillingMessage.PAY_ACCOUNT_NUMBER));
        //clientTransaction.setContractNumber(msg.getAttributeString(BillingMessage....));
        clientTransaction.setVoucherNominal(msg.getAttributeString(BillingMessage.VOUCHER_NOMINAL));
        Voucher voucher = (Voucher) msg.getAttribute(BillingMessage.VOUCHER);
        if(voucher != null) {
            clientTransaction.setVoucher(voucher);
        }
        clientTransaction.setOperationName(msg.getOperationName());
        clientTransaction.setServiceCode(msg.getAttributeString(BillingMessage.SERVICE_CODE));
        clientTransaction.setType(new TransactionType(TransactionType.CODE_CREDIT));
        clientTransaction.setOperationType(config.operationsType);
        List result = new ArrayList();
        result.add(clientTransaction);
        return result;
    }

    protected List prepareChangeBalancesProcedure(BillingMessageWritable msg, DBCache dbCache, TransConfig config) throws DBCacheException {
        final String VARCHAR_ARRAY_TYPE = "VARCHAR_ARRAY_TYPE";
        final String FLOAT_ARRAY_TYPE = "FLOAT_ARRAY_TYPE";

        //prepare parameters to pass into SPL procedure
        //operator
        Reservation reservation = config.reservationSet.getReservation(Operator.class, config.operatorCode);
        List balancesOwnersCodes = new ArrayList();//list of String
        List balancesCodes = new ArrayList();//list of String
        List balancesAmounts = new ArrayList();//list of Float
        for(Iterator iter = reservation.reservesIterator(); iter.hasNext();){
            Reservation.Reserve reserve = (Reservation.Reserve)iter.next();
            balancesOwnersCodes.add(config.operatorCode);
            balancesCodes.add(reserve.getBalanceCode());
            balancesAmounts.add(new Float(reserve.getAmount()));//double -> float!
        }
        String[] operatorCodesArray = (String[])balancesOwnersCodes.toArray(new String[0]);
        String[] operatorBalancesCodesArray = (String[])balancesCodes.toArray(new String[0]);
        Float[] operatorBalancesAmountsArray = (Float[])balancesAmounts.toArray(new Float[0]);

        //seller
        List sellersParentsChainCodes = dbCache.getSellerWrapperParentsChainCodes(config.sellerCode);
        balancesOwnersCodes = new ArrayList();//list of String
        balancesCodes = new ArrayList();//list of String
        balancesAmounts = new ArrayList();//list of Float
        for(Iterator sellersIter = sellersParentsChainCodes.iterator(); sellersIter.hasNext();){
            String nextSellerCode = (String)sellersIter.next();
            reservation = config.reservationSet.getReservation(Seller.class, nextSellerCode);
            for(Iterator iter = reservation.reservesIterator(); iter.hasNext();){
                Reservation.Reserve reserve = (Reservation.Reserve)iter.next();
                balancesOwnersCodes.add(nextSellerCode);
                balancesCodes.add(reserve.getBalanceCode());
                balancesAmounts.add(new Float(reserve.getAmount()));//double -> float!
            }
        }
        String[] sellerCodesArray = (String[])balancesOwnersCodes.toArray(new String[0]);
        String[] sellerBalancesCodesArray = (String[])balancesCodes.toArray(new String[0]);
        Float[] sellerBalancesAmountsArray = (Float[])balancesAmounts.toArray(new Float[0]);

        //seller
        reservation = config.reservationSet.getReservation(Terminal.class, config.terminalSn);
        balancesOwnersCodes = new ArrayList();//list of String
        balancesCodes = new ArrayList();//list of String
        balancesAmounts = new ArrayList();//list of Float
        for(Iterator iter = reservation.reservesIterator(); iter.hasNext();){
            Reservation.Reserve reserve = (Reservation.Reserve)iter.next();
            balancesOwnersCodes.add(config.terminalSn);
            balancesCodes.add(reserve.getBalanceCode());
            balancesAmounts.add(new Float(reserve.getAmount()));//double -> float!
        }
        String[] termianlSnArray = (String[])balancesOwnersCodes.toArray(new String[0]);
        String[] termianlBalancesIdsArray = (String[])balancesCodes.toArray(new String[0]);
        Float[] termianlBalancesAmountsArray = (Float[])balancesAmounts.toArray(new Float[0]);


        ProcedureWrapper wrapper =  new ProcedureWrapper(
                new QueryParameterWrapper[]{
                        QueryParameterWrapper.createOutParameter(1, Types.INTEGER),

                        new QueryParameterWrapper(2, operatorCodesArray, VARCHAR_ARRAY_TYPE),
                        new QueryParameterWrapper(3, operatorBalancesCodesArray, VARCHAR_ARRAY_TYPE),
                        new QueryParameterWrapper(4, operatorBalancesAmountsArray, FLOAT_ARRAY_TYPE),

                        new QueryParameterWrapper(5, sellerCodesArray, VARCHAR_ARRAY_TYPE),
                        new QueryParameterWrapper(6, sellerBalancesCodesArray, VARCHAR_ARRAY_TYPE),
                        new QueryParameterWrapper(7, sellerBalancesAmountsArray, FLOAT_ARRAY_TYPE),

                        new QueryParameterWrapper(8, termianlSnArray, VARCHAR_ARRAY_TYPE),
                        new QueryParameterWrapper(9, termianlBalancesIdsArray, VARCHAR_ARRAY_TYPE),
                        new QueryParameterWrapper(10, termianlBalancesAmountsArray, FLOAT_ARRAY_TYPE)
                },
                Operator.class,
                "change_balances");
                          
        List result = new ArrayList();
        result.add(wrapper);
        return result;
    }

    public Task rollback(Task task, Throwable e) {
        logger.info("Rollback Reservation...");
        try{
            BillingMessageWritable msg = task.getProcesedMessage();
            //change status code to error in transactions
            resaveTransactions(msg);
            // operator transaction
            DBCache dbCache = task.getSessionDBCache();
            ReservationSet reservationSet = (ReservationSet)msg.getAttribute(BillingMessage.BALANCE_RESERVATION_SET);

            // cancel operator reservations
            String operatorCode = msg.getAttributeString(BillingMessage.SERVICE_CODE);
            Reservation reservation = reservationSet.getReservation(Operator.class, operatorCode);
            for(Iterator iter = reservation.reservesIterator(); iter.hasNext();){
                Reservation.Reserve reserve = (Reservation.Reserve)iter.next();
                if(reserve.isApplied())
                    dbCache.changeOperatorBalance(
                            operatorCode,
                            reserve.getBalanceCode(),
                            -reserve.getAmount(), reserve.getTransTypeCode());
                    reservation.cancelReserve(reserve.getId());
            }

            // cancel seller reservations
            String sellerCode = msg.getAttributeString(BillingMessage.SELLER_CODE);
            List sellersParentsChainCodes = dbCache.getSellerWrapperParentsChainCodes(sellerCode);
            for(Iterator sellersIter = sellersParentsChainCodes.iterator(); sellersIter.hasNext();){
                String nextSellerCode = (String)sellersIter.next();
                reservation = reservationSet.getReservation(Seller.class, nextSellerCode);
                for(Iterator iter = reservation.reservesIterator(); iter.hasNext();){
                    Reservation.Reserve reserve = (Reservation.Reserve)iter.next();
                    if(reserve.isApplied())
                        dbCache.changeSellerBalance(
                                nextSellerCode,
                                reserve.getBalanceCode(),
                                -reserve.getAmount(), reserve.getTransTypeCode());
                        reservation.cancelReserve(reserve.getId());
                }
            }

            // cancel terminal reservations
            String terminalSn = msg.getAttributeString(BillingMessage.TERMINAL_SN);
            reservation = reservationSet.getReservation(Terminal.class, terminalSn);
            for(Iterator iter = reservation.reservesIterator(); iter.hasNext();){
                Reservation.Reserve reserve = (Reservation.Reserve)iter.next();
                if(reserve.isApplied())
                    dbCache.changeTerminalBalance(
                            terminalSn,
                            reserve.getBalanceCode(),
                            -reserve.getAmount(), reserve.getTransTypeCode());
                    reservation.cancelReserve(reserve.getId());
            }

        }
        catch(Throwable te){
            logger.error("Rollback reservation failed: " + te.getLocalizedMessage(), te);
        }
        return super.rollback(task, e);
    }
    //Save transactions after exception
    private void resaveTransactions(BillingMessageWritable msg) throws DatabaseException {
        try {
            List txs = (new TransactionManager()).getAllTransactionsByGUID(msg.getAttributeString(BillingMessage.BILL_TRANSACTION_NUM));
            saveTransactions(msg, txs);
        } catch (Exception exc) {
            logger.error("Failed to set error status in DB");
            logger.error(exc);
        }
    }

}
