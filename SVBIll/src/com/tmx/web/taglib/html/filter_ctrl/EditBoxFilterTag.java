package com.tmx.web.taglib.html.filter_ctrl;

import com.tmx.web.controls.EditBox;

import javax.servlet.jsp.JspException;

/**
 */
public class EditBoxFilterTag extends AbstractFilterTag{
    private String parameterName = null;
    //CSS styles properties
    private String styleClass = null;
    private String readonlyStyleClass = null;

    //------------------------------Properties
    public String getStyleClass() {
        return (styleClass != null) ? styleClass : getDefaultProperty("tag.editBoxFilter.style.styleClass");
    }

    public void setStyleClass(String styleClass) {
        this.styleClass = styleClass;
    }

    public String getReadonlyStyleClass() {
        return (readonlyStyleClass != null) ? readonlyStyleClass : getDefaultProperty("tag.editBoxFilter.style.styleClassReadonly");
    }

    public void setReadonlyStyleClass(String readonlyStyleClass) {
        this.readonlyStyleClass = readonlyStyleClass;
    }

    public String getParameterName() {
        return parameterName;
    }

    public void setParameterName(String parameterName) {
        this.parameterName = parameterName;
    }

    //------------------------------Protected methods
    protected String renderControlHTML() throws JspException {
//        super.renderControlHTML();
        StringBuffer resultHTML = new StringBuffer();
        String path = getProperty() + "." + getFilter().getParameter(parameterName).getPath() + "." + EditBox.VALUE_SUFFIX;
        EditBox eb = (EditBox)getFilter().getParameter(parameterName).getControl();

        if(eb == null)
            return "Filter EditBox '"+getProperty()+"' is not found";

        String readonly = (eb.isReadonly()) ? " readonly=\"true\" " : "";
        String value = " value=\""+((eb.getValue() == null) ? "" : eb.getValue()) +"\" ";
        String styleClass = " class=\""+((eb.isReadonly()) ? getReadonlyStyleClass() : getStyleClass())+"\" ";
        String type = " type=\"text\" ";
        String name = "name=\""+path+"\"";
        //Render HTML
        resultHTML.append("\r");
        resultHTML.append("<input " + name + type + value + styleClass + readonly +"/>\r");

        return resultHTML.toString();
    }

}
