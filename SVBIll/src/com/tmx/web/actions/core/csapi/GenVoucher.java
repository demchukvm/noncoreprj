package com.tmx.web.actions.core.csapi;

import com.tmx.as.base.EntityManager;
import com.tmx.as.blogic.BlogicException;
import com.tmx.as.blogic.VoucherManager;
import com.tmx.as.entities.bill.voucher.BatchBuyVoucher;
import com.tmx.as.exceptions.DatabaseException;
import com.tmx.beng.base.BillingMessage;
import com.tmx.beng.csapi.java.base.BuyVoucherResp;
import com.tmx.beng.csapi.xml.v1.base.ClientPAD;
import com.tmx.web.controls.PopulateUtil;
import com.tmx.web.forms.core.tools.BatchBuyVouchersForm;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: User
 * Date: 29.10.2010
 * Time: 15:52:17
 * To change this template use File | Settings | File Templates.
 */

public class GenVoucher
{
//    private List<BuyVoucherResp> vouch;

    public List<BuyVoucherResp> buy(ClientPAD.ParsingResult resp) throws Throwable
    {

        final BatchBuyVoucher batchBuyVoucher = new BatchBuyVoucher();
        batchBuyVoucher.setCliTransactionNum(resp.getBillingMessage().getAttributeString(BillingMessage.CLI_TRANSACTION_NUM));
        batchBuyVoucher.setOperationType(BillingMessage.O_GEN_VOUCHER); // "request.refillPayment"
        batchBuyVoucher.setProcessingCode("SVCard");
        batchBuyVoucher.setSellerCode(resp.getBillingMessage().getAttributeString(BillingMessage.SELLER_CODE));
        batchBuyVoucher.setServiceCode(resp.getBillingMessage().getAttributeString(BillingMessage.SERVICE_CODE));
        batchBuyVoucher.setStatus("InProgress");
        batchBuyVoucher.setTerminalSn(resp.getBillingMessage().getAttributeString(BillingMessage.TERMINAL_SN));
        batchBuyVoucher.setTransactionTime(new Date());
        batchBuyVoucher.setUserActor("admin");
        String count = resp.getBillingMessage().getAttributeString(BillingMessage.VOUCHER_COUNT);
        batchBuyVoucher.setVoucherCount(Integer.parseInt(count));
        String nominal = resp.getBillingMessage().getAttributeString(BillingMessage.VOUCHER_NOMINAL);
        batchBuyVoucher.setVoucherNominal(Integer.parseInt(nominal));

        final String login = "SVCard";
        final String password = "T6%4+31@mZ";

        int interval = 0;
        if(batchBuyVoucher.getVoucherCount() < 10)
            interval = 1;
        else if(batchBuyVoucher.getVoucherCount() > 10 && batchBuyVoucher.getVoucherCount() < 100)
            interval = 5;
        else if(batchBuyVoucher.getVoucherCount() > 100 && batchBuyVoucher.getVoucherCount() < 500)
            interval = 10;
        else if(batchBuyVoucher.getVoucherCount() > 500 && batchBuyVoucher.getVoucherCount() < 1000)
            interval = 20;
        else if(batchBuyVoucher.getVoucherCount() > 1000 && batchBuyVoucher.getVoucherCount() < 10000)
            interval = 50;
        else if(batchBuyVoucher.getVoucherCount() > 10000)
            interval = 100;

        final Long sleepBetweenInterval = (long) interval;

        final EntityManager em = new EntityManager();
        em.SAVE(batchBuyVoucher);
        //vouchersForm.getEbBatchBuyVoucherId().setValue(buyOperation.getBatchBuyVoucherId().toString());

        final List<BuyVoucherResp> vouchers = new LinkedList<BuyVoucherResp>();
        //vouch = vouchers;



        final Thread buyerThread = new Thread(){
            public void run() {
                VoucherManager manager = new VoucherManager();
                try{
                    manager.buyVouchersBatch(
                            batchBuyVoucher,
                            login,
                            password,
                            sleepBetweenInterval,
                            vouchers);
                }
                catch (Throwable e){
                    batchBuyVoucher.setStatus("Failed");
                }


                try{
                    em.SAVE(batchBuyVoucher);
                }
                catch (DatabaseException e){


                }

            }
        };
        buyerThread.start();

        int i = 1;
        while(buyerThread.isAlive()) {
        //while(vouchers.isEmpty()) {
            System.out.println("С П А М   #"+i++);
        }
        
        return vouchers;

    }

}

