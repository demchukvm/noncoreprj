package com.tmx.util.jce.mac;

import javax.crypto.Cipher;
import java.security.spec.AlgorithmParameterSpec;

/**

 */
public class X99AlgParameterSpec implements AlgorithmParameterSpec {
    private Cipher desCipher;

    public X99AlgParameterSpec(Cipher desCipher){
        this.desCipher = desCipher; 
    }

    public Cipher getDesCipher() {
        return desCipher;
    }


}
