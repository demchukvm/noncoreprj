package com.tmx.beng.base.cache;

import com.tmx.as.entities.bill.seller.Seller;

import java.util.Map;
import java.util.HashMap;
import java.util.List;
import java.util.Vector;

/**

 */
class SellerWrapper {
    Seller seller;
    SellerWrapper parentSellerWrapper;
    Map balanceId_balance = new HashMap();
    Map balanceCode_balanceId = new HashMap();    
    Map tariffId_tariffEntity = new HashMap();
    Map tariffId_tariffFunctionInstance = new HashMap();
    //Map tariffName_tariffFunctionInstance = new HashMap();
    Map terminalId_terminal = new HashMap();
    Map terminaGroupId_terminalGroup = new HashMap();
    List restriction = new Vector();

    SellerWrapper(Seller seller) {
        this.seller = seller;
    }

    public Seller getSeller() {
        return seller;
    }

}
