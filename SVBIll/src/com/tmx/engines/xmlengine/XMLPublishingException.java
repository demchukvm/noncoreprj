package com.tmx.engines.xmlengine;

import com.tmx.util.StructurizedException;

import java.util.Locale;

public class XMLPublishingException extends StructurizedException {

    public XMLPublishingException(String errKey, Throwable cause){
        super(errKey, cause);
    }

    public XMLPublishingException(String errKey, Throwable cause, Locale locale){
        super(errKey, cause, locale);
    }

    public XMLPublishingException(String errKey){
        super(errKey);
    }

    public XMLPublishingException(String errKey, String[] params){
        super(errKey, params);
    }

    public XMLPublishingException(String errKey, String[] params, Throwable cause){
        super(errKey, params, cause);
    }    

    public XMLPublishingException(String errKey, String[] params, Throwable cause, Locale locale){
        super(errKey, params, cause, locale);
    }
}
