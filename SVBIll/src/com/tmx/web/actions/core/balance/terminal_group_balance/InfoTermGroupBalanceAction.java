package com.tmx.web.actions.core.balance.terminal_group_balance;

import com.tmx.web.forms.core.balance.AbstractInfoBalanceForm;
import com.tmx.web.forms.core.balance.terminal_group_balance.InfoTermGroupBalanceForm;
import com.tmx.web.controls.table.Table;
import com.tmx.web.controls.table.ControlManagerTable;
import com.tmx.web.controls.BrowseBox;
import com.tmx.web.actions.core.balance.AbstractInfoBalanceAction;
import com.tmx.as.entities.bill.balance.AbstractBalance;
import com.tmx.as.entities.bill.balance.TerminalGroupBalance;
import com.tmx.as.entities.general.user.User;
import com.tmx.as.blogic.balance.AbstractBalanceManager;
import com.tmx.as.blogic.balance.TerminalGroupBalanceManager;
import com.tmx.as.blogic.TransactionManager;
import com.tmx.as.exceptions.DatabaseException;
import com.tmx.util.InitException;
import com.tmx.beng.base.BillException;


public class InfoTermGroupBalanceAction extends AbstractInfoBalanceAction {

    protected String getManageAction() {
        return "manageTermGroupBalancesAction";
    }

    protected String getInfoPage() {
        return "infoTermGroupBalancePage";
    }

    protected String getManageDeleteAction() {
        return "manageDeleteAction";
    }

    protected String getInfoTransactionAction() {
        return "/core/balance/terminal_group_balance/infoTerminalGroupBalance";
    }

    protected AbstractBalanceManager getBalanceManager() throws InitException, BillException {
        return new TerminalGroupBalanceManager();
    }

    protected void initTransTableConstrains(ControlManagerTable table, AbstractBalance balance) {
        TerminalGroupBalance operatorBalance = (TerminalGroupBalance)balance;
        Long groupId = operatorBalance.getTerminalGroupId();
        String query = "query(query_type=execute_query," +
                       "entity_class=com.tmx.as.entities.bill.transaction.TerminalBalanceTransaction," +
                       "query_name=retrieveTerminalGroupTransaction," +
                       "query_parameter(name=groupId, value=" + (groupId != null ? groupId : new Long(0)) + ", type=long)," +
                       "page_size=10" +
                        ")";
        table.loadQuery(query);
    }

    protected void initRestricTableConstrains(ControlManagerTable table, AbstractBalance balance) {

    }

    protected void initTariffsTableConstrains(ControlManagerTable table, AbstractBalance balance) {

    }

    protected void preloadForUpdate(AbstractInfoBalanceForm form, AbstractBalance balance, User user) {
        
    }

    protected void preloadForCreate(AbstractInfoBalanceForm form) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    protected void preload(AbstractInfoBalanceForm form, AbstractBalance balance) throws Throwable {
        TerminalGroupBalance terminalGroupBalance = (TerminalGroupBalance)balance;
        InfoTermGroupBalanceForm infoForm = (InfoTermGroupBalanceForm) form;

        BrowseBox browseBox = infoForm.getBrowseBoxAddTermGroup();
        if(terminalGroupBalance.getTerminalGroup()!=null){
            browseBox.setValue(terminalGroupBalance.getTerminalGroupId().toString());
            browseBox.setKey(terminalGroupBalance.getTerminalGroupId().toString());
        }
    }

    @Override
    protected TransactionManager.CashCalculation getCashCalculation(AbstractInfoBalanceForm form) throws DatabaseException {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

}
