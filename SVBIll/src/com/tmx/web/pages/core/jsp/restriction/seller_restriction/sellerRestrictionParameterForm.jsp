<%@ page language="java" pageEncoding="UTF-8" contentType="text/html; charset=utf-8"
         errorPage="/core/jsp/message/jsperror.jsp" isELIgnored="false" %>
<%@ taglib uri="/WEB-INF/tld/struts-nested.tld" prefix="nested" %>
<%@ taglib uri="/WEB-INF/tld/tmx-controls.tld" prefix="tmx_ctrl" %>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>


<html:form action="/core/restriction/seller_restriction/sellerRestrictionParameter">
<table border="0" width="100%">

    <!-- BUTTONS -->
    <tr>
        <td style="text-align: left">
            <tmx_ctrl:button property="onApplyButton"/>
            <tmx_ctrl:button property="onSaveButton"/>
            <tmx_ctrl:button property="onResetButton" onClick="javascript:document.forms[0].reset()"/>
            <tmx_ctrl:button property="onCancelButton"/>
        </td>
    </tr>
    <tr>
        <td valign="center" align="center" class="tabForm">
        <table width="100%" border="0">
            <tr>
                <td class="dataLabel" width="15%">
                    <tmx_ctrl:writeEntityLabel attrName="balanceId"
                                               entityClassName="com.tmx.as.entities.bill.balance.SellerBalance"/>:
                </td>
                <td class="dataField" width="35%">
                    <tmx_ctrl:editBox property="editBoxParameterId"/>
                </td>

                <td class="dataLabel" width="15%">
                    <tmx_ctrl:writeEntityLabel attrName="orderNum"
                                               entityClassName="com.tmx.as.entities.bill.function.ActualFunctionParameter"/>:
                </td>
                <td class="dataField" width="35%">
                    <tmx_ctrl:editBox property="editBoxOrderNum"/>
                </td>

            </tr>

            <tr>
                <td class="dataLabel" width="15%">
                    <tmx_ctrl:writeEntityLabel attrName="name"
                                               entityClassName="com.tmx.as.entities.bill.function.ActualFunctionParameter"/>:
                </td>
                <td class="dataField" width="35%">
                     <tmx_ctrl:editBox property="editBoxParamName" width="230px"/>
                </td>

                <td class="dataLabel" width="15%">
                    <tmx_ctrl:writeEntityLabel attrName="value"
                                               entityClassName="com.tmx.as.entities.bill.function.ActualFunctionParameter"/>:
                </td>
                <td class="dataField" width="35%">
                     <tmx_ctrl:editBox property="editBoxValue" width="230px"/>
                </td>
            </tr>

        </table>
        </td>

    </tr>

    <!-- BUTTONS -->
    <tr>
        <td style="text-align: left">
            <tmx_ctrl:button property="onApplyButton"/>
            <tmx_ctrl:button property="onSaveButton"/>
            <tmx_ctrl:button property="onResetButton" onClick="javascript:document.forms[0].reset()"/>
            <tmx_ctrl:button property="onCancelButton"/>
        </td>
    </tr>
</table>
</html:form>