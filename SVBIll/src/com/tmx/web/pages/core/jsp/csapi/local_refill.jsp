<%--
  Created by IntelliJ IDEA.
  User: Proximan
  Date: 30.06.2010
  Time: 9:35:45
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://tmx.com/taglib/tmx-controls" prefix="tmx_ctrl" %>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html" %>

<html:form action="/localRefill">
  <head></head>

  <body>
  <%--<tmx_ctrl:form action="/localRefill">--%>
            <table align="center">
                <tr>
                    <td colspan="2">
                        <select size="1" name="operatorType">
                            <option value="SSM">SSM</option>
                            <option value="SVCOM">SVCOM</option>
                            <option value="Life">Life</option>
                        </select>
                    </td>
                </tr>
                <!-- По номеру -->
                <tr><td colspan="2"><h4 class="dataLabel">По номеру</h4></td></tr>
                <tr>
                    <td class="dataLabel">Номер телафона:</td>  <!-- Лейбл: номер телефона-->
                    <td><tmx_ctrl:editBox property="telephone" width="150px" /></td>  <!-- Поле-->
                </tr>
                <tr>
                    <td class="dataLabel">Сумма:</td>  <!-- Лейбл: сумма-->
                    <td class="dataField"><tmx_ctrl:editBox property="amount1" width="150px" /></td>  <!-- Поле-->
                </tr>
                <tr>
                    <td colspan="2" align="right" class="dataField"><tmx_ctrl:button property="submitByTelephone" /></td>  <!-- Кнопка: передать -->
                </tr>


                <tr>
                    <td colspan="2"><hr /></td>  <!-- Кнопка: передать -->
                </tr>

                <!-- По лицевому счету -->
                <tr><td colspan="2"><h4 class="dataLabel">По лицевому счету</h4></td></tr>
                <tr>
                    <td class="dataLabel">Номер лицевого счета:</td>  <!-- Лейбл: номер лицевого счета-->
                    <td class="dataField"><tmx_ctrl:editBox property="account" width="150px" /></td>  <!-- Поле-->
                </tr>
                <tr>
                    <td class="dataLabel">Сумма:</td>  <!-- Лейбл: сумма-->
                    <td class="dataField"><tmx_ctrl:editBox property="amount2" width="150px" /></td>  <!-- Поле-->
                </tr>
                <tr>
                    <td colspan="2" align="right" class="dataField"><tmx_ctrl:button property="submitByAccount" /></td>  <!-- Кнопка: передать -->
                </tr>
            </table>
<%--</tmx_ctrl:form>--%>


</body>

</html:form>
<%--</html>--%>
