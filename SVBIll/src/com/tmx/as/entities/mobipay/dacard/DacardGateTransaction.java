package com.tmx.as.entities.mobipay.dacard;

import com.tmx.as.base.BasicEntity;
import java.io.Serializable;
import java.util.Date;

/**
    Entity to handle Dacard gate transaction
 */
public class DacardGateTransaction extends BasicEntity {
    private Long dacardGateTransactionId;
    //request area
    private Byte messageType;
    private Integer amount;
    private Date localDate;
    private String localDateString;
    private String terminalId;
    private String invoiceInfo;
    /** STAN. Cycled value 1..999999, traced individually per terminalId.
     * Shouldn't be duplicated in one day for one terminalId. */
    private Integer msgCount;
    private String rrn;
    private Integer articleId;
    private Integer fee1;
    private Integer fee2;
    /** Identifier of reconciliation period (day). 1..999, traced individually per terminalId. */
    private Integer reconIndicator;
    //response area
    private String fee;
    private String prn1Name;
    private String prn1Value;
    private Integer errorCode;
    private Integer checkReplenishmentErrorCode;
    private String errorDescription;
    /** SVBILL billing transaction identifier */
    private String billTransactionNum;
    /** Client identifier of transaction. We set "invoiceRrn = billTransactionNum" */
    private String invoiceRrn;
    /** Transaction status code handled by SBILL */
    private Integer statusCode;
    private String statusMessage;
    private String gateName;

    public Long getDacardGateTransactionId() {
        return dacardGateTransactionId;
    }

    public void setDacardGateTransactionId(Long dacardGateTransactionId) {
        this.dacardGateTransactionId = dacardGateTransactionId;
    }

    public Byte getMessageType() {
        return messageType;
    }

    public void setMessageType(Byte messageType) {
        this.messageType = messageType;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public Date getLocalDate() {
        return localDate;
    }

    public void setLocalDate(Date localDate) {
        this.localDate = localDate;
    }

    public String getLocalDateString() {
        return localDateString;
    }

    public void setLocalDateString(String localDateString) {
        this.localDateString = localDateString;
    }

    public String getTerminalId() {
        return terminalId;
    }

    public void setTerminalId(String terminalId) {
        this.terminalId = terminalId;
    }

    public String getInvoiceInfo() {
        return invoiceInfo;
    }

    public void setInvoiceInfo(String invoiceInfo) {
        this.invoiceInfo = invoiceInfo;
    }

    public Integer getMsgCount() {
        return msgCount;
    }

    public void setMsgCount(Integer msgCount) {
        this.msgCount = msgCount;
    }

    public String getRrn() {
        return rrn;
    }

    public void setRrn(String rrn) {
        this.rrn = rrn;
    }

    public Integer getArticleId() {
        return articleId;
    }

    public void setArticleId(Integer articleId) {
        this.articleId = articleId;
    }

    public Integer getFee1() {
        return fee1;
    }

    public void setFee1(Integer fee1) {
        this.fee1 = fee1;
    }

    public Integer getFee2() {
        return fee2;
    }

    public void setFee2(Integer fee2) {
        this.fee2 = fee2;
    }

    public Integer getReconIndicator() {
        return reconIndicator;
    }

    public void setReconIndicator(Integer reconIndicator) {
        this.reconIndicator = reconIndicator;
    }

    public String getFee() {
        return fee;
    }

    public void setFee(String fee) {
        this.fee = fee;
    }

    public String getPrn1Name() {
        return prn1Name;
    }

    public void setPrn1Name(String prn1Name) {
        this.prn1Name = prn1Name;
    }

    public String getPrn1Value() {
        return prn1Value;
    }

    public void setPrn1Value(String prn1Value) {
        this.prn1Value = prn1Value;
    }

    public Integer getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(Integer errorCode) {
        this.errorCode = errorCode;
    }

    public Integer getCheckReplenishmentErrorCode() {
        return checkReplenishmentErrorCode;
    }

    public void setCheckReplenishmentErrorCode(Integer checkReplenishmentErrorCode) {
        this.checkReplenishmentErrorCode = checkReplenishmentErrorCode;
    }

    public String getErrorDescription() {
        return errorDescription;
    }

    public void setErrorDescription(String errorDescription) {
        this.errorDescription = errorDescription;
    }

    public String getBillTransactionNum() {
        return billTransactionNum;
    }

    public void setBillTransactionNum(String billTransactionNum) {
        this.billTransactionNum = billTransactionNum;
    }

    public String getInvoiceRrn() {
        return invoiceRrn;
    }

    public void setInvoiceRrn(String invoiceRrn) {
        this.invoiceRrn = invoiceRrn;
    }

    public Integer getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }

    public String getStatusMessage() {
        return statusMessage;
    }

    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }

    public String getGateName() {
        return gateName;
    }

    public void setGateName(String gateName) {
        this.gateName = gateName;
    }

    public Serializable getId() {
        return getDacardGateTransactionId();
    }

    public int getValid() {
        return valid;
    }

    public void setValid(int valid) {
        this.valid = valid;
    }
}