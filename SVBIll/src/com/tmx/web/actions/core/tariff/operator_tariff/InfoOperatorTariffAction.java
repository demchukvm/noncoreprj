package com.tmx.web.actions.core.tariff.operator_tariff;

import com.tmx.as.blogic.tariff.AbstractTariffManager;
import com.tmx.as.blogic.tariff.OperatorTariffManager;
import com.tmx.as.entities.bill.tariff.AbstractTariff;
import com.tmx.as.entities.general.user.User;
import com.tmx.as.exceptions.DatabaseException;
import com.tmx.web.actions.core.tariff.AbstractInfoTariffAction;
import com.tmx.web.forms.core.tariff.AbstractInfoTariffForm;
import com.tmx.web.forms.core.tariff.operator_tariff.InfoOperatorTariffForm;
import com.tmx.web.base.WebException;


public class InfoOperatorTariffAction extends AbstractInfoTariffAction {

    protected void preloadForCreate(AbstractInfoTariffForm form, AbstractTariff restriction, User loggedInUser) throws DatabaseException, WebException {
        String sellerId = getRoadmap().getParameterValue(getName(), ManageOperatorTariffsAction.PARAMETER_OWNER_NAME, true).toString();
        ((InfoOperatorTariffForm) form).getScrollBoxOperators().setSelectedKey(sellerId);
    }

    protected void preloadForUpdate(AbstractInfoTariffForm form, AbstractTariff restriction) throws DatabaseException {
        //do nothing    
    }

    protected AbstractTariffManager getTariffManager() {
        return new OperatorTariffManager();
    }


}
