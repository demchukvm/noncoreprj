package com.tmx.beng.iso8583Gate.base;

import com.tmx.beng.iso8583Gate.ISO8583Service;
import com.tmx.beng.iso8583Gate.pdu.MessageCommonFormat;
import com.tmx.beng.iso8583Gate.pdu.MessageParticularFormat;
import com.tmx.beng.iso8583Gate.base.beans.Protocol;
import com.tmx.beng.iso8583Gate.base.beans.Mac;
import com.tmx.util.InitException;
import com.tmx.util.Configuration;
import java.util.Map;
import java.util.HashMap;


/**
Loads and holds protocol message templates. 
 */
public class ProtocolMetadata {
    /** [String version][ProtocolVersion version] */
    Map protocolVersions = null;

    public void reload() throws InitException{
        protocolVersions = new HashMap();
        Protocol[] protocols = ISO8583Service.getInstance().getConfig().getIso8583ConfDocument().getIso8583Conf().getProtocols().getProtocolArray();
        for(int i=0; i<protocols.length; i++){
            ProtocolVersion protocolVersion = new ProtocolVersion();
            protocolVersion.reload(protocols[i]);
            protocolVersions.put(protocols[i].getVersion(), protocolVersion);
        }
    }

    public ProtocolVersion getProtocolVersion(String version){
        return (ProtocolVersion)protocolVersions.get(version);
    }

    public class ProtocolVersion {
        private MessageCommonFormat messageCommonFormat;
        private Map messageParticularFormats;
        private MacConfig macConfig;

        private void reload(Protocol protocol) throws InitException {
            //1. load common message format
            MessageCommonFormat localMessageCommonFormat = new MessageCommonFormat();
            String path = null;
            try{
                path = Configuration.getInstance().substituteVariablesInString(protocol.getPath());
            }
            catch(Exception e){
                throw new InitException("err.protocol_version.failed_to_substitute_path", new String[]{path}, e);
            }

            localMessageCommonFormat.load(path, protocol.getMessageCommonFormat(), protocol.getVersion());


            //2. load MAC implementation if required
            Mac macBean = protocol.getMac();
            MacConfig localMacConfig = null;
            if(macBean != null){
                localMacConfig = new MacConfig(macBean.getEnabled(), macBean.getAlgorithm());
                localMacConfig.reload(macBean);
            }


            //3. load particular messages formats
            Map localMessageParticularFormats = new HashMap();
            com.tmx.beng.iso8583Gate.base.beans.MessageParticularFormat[] msgPartFormat = protocol.getMessageParticularFormats().getMessageParticularFormatArray();
            for (int i = 0; i < msgPartFormat.length; i++) {
                MessageParticularFormat messageParticularFormat = new MessageParticularFormat();
                messageParticularFormat.load(path, msgPartFormat[i].getFile());
                localMessageParticularFormats.put(
                        messageParticularFormat.resolveFormatId(),
                        messageParticularFormat);
            }

            //assign global properties
            messageCommonFormat = localMessageCommonFormat;
            messageParticularFormats = localMessageParticularFormats;
            macConfig = localMacConfig;
        }

        public MessageCommonFormat getMessageCommonFormat(){
            return messageCommonFormat;
        }

        public MessageParticularFormat getMessageParticularFormat(String messageTypeId, String processingCode){
            String messageFormatId = MessageParticularFormat.resolveFormatId(messageTypeId, processingCode);
            return (MessageParticularFormat)messageParticularFormats.get(messageFormatId);
        }

        public MacConfig getMacConfig() {
            return macConfig;
        }

        /** MAC configuration */
        public class MacConfig {
            private boolean enabled = false;
            private String algorithm = null;
            private Map exceptMacMessages;

            private MacConfig(boolean enabled, String algorithm) {
                this.enabled = enabled;
                this.algorithm = algorithm;
                this.exceptMacMessages = new HashMap(); 
            }

            public boolean isEnabled(){
                return enabled;
            }

            public String getAlgorithm() {
                return algorithm;
            }

            public boolean isExceptMac(Long messageTypeId, Long processingCode){
                return exceptMacMessages.containsKey(evaluateUniqueName(messageTypeId, processingCode));
            }

            void reload(Mac macConfBean){
                Map localExceptMacMessages = new HashMap();
                com.tmx.beng.iso8583Gate.base.beans.MessageRef[] msgRefs = macConfBean.getExceptMacForMessages().getMessageRefArray();
                if(msgRefs == null)
                    return;
                for(int i=0; i< msgRefs.length; i++){
                    MessageRef msgRef = new MessageRef(Long.valueOf(msgRefs[i].getMessageTypeId()), Long.valueOf(msgRefs[i].getProcessingCode()));
                    localExceptMacMessages.put(evaluateUniqueName(msgRef.messageTypeId, msgRef.processingCode), msgRef);
                }
                exceptMacMessages = localExceptMacMessages;
            }

            String evaluateUniqueName(Long messageTypeId, Long processingCode){
                final String DELIMITER = ".";
                return processingCode != null ?
                        messageTypeId + DELIMITER + processingCode :
                        messageTypeId.toString();
            }


            private class MessageRef{
                private Long messageTypeId;
                private Long processingCode;

                MessageRef(Long messageTypeId, Long processingCode) {
                    this.messageTypeId = messageTypeId;
                    this.processingCode = processingCode;
                }
            }
        }

    }

}
