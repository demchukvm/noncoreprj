<%@ page language="java" pageEncoding="UTF-8" contentType="text/html; charset=utf-8" isELIgnored="false" %>
<!--errorPage="/core/jsp/message/jsperror.jsp"-->
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean" %>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c" %>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-nested" prefix="nested" %>
<%@ taglib uri="/WEB-INF/tld/tmx-controls.tld" prefix="tmx_ctrl" %>

<table cellpadding="0" cellspacing="0" width="100%" height="63" border="0">
    <tr align="left">
		<td width="137" height="63" background="<tmx_ctrl:rewriteUrl href="/core/images/common/header4.png"/>" />
		
		
		<td align="right">
			<table cellpadding="0" cellspacing="0" border="0" style="margin-right:10px;">
				<tr>
					<td align="right">
						<b><tmx_ctrl:writeEntityLabel attrName="{@name}" entityClassName="com.tmx.as.entities.general.user.User"/></b>
					</td>
					<td align="center" width="10" rowspan="3" background="<tmx_ctrl:rewriteUrl href="/core/images/common/vline.png"/>"/>
					<td>
						<nested:write name="userContextForm" property="currentUser.lastName"/>&nbsp;
						<nested:write name="userContextForm" property="currentUser.firstName"/>
					</td>
					
				</tr>
				<tr>
					<td align="right">
						<b><tmx_ctrl:writeEntityLabel attrName="{@name}" entityClassName="com.tmx.as.entities.general.role.Role"/></b>
					</td>
					<td>
						<nested:write name="userContextForm" property="currentUser.role.roleName"/>
					</td>
				</tr>
				<tr>
					<td align="right">
						<b><tmx_ctrl:writeEntityLabel attrName="sellerOwner" entityClassName="com.tmx.as.entities.general.user.User"/></b>
					</td>
					<td>
						<nested:notEmpty name="userContextForm" property="currentUser.organization">
							<nested:write name="userContextForm" property="currentUser.sellerOwner.name"/>
						</nested:notEmpty>	
					</td>
				</tr>
			</table>
		<td>
				

    </tr>
</table>