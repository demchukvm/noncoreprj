package com.tmx.beng.base.cache;

import com.tmx.util.StructurizedException;

import java.util.Locale;

/**
 */
public class DBCacheException extends StructurizedException {

    public DBCacheException(String msg, String[] params, Throwable e, Locale locale) {
        super(msg, params, e, locale);
    }

    public DBCacheException(String msg, String[] params) {
        super(msg, params);
    }

    public DBCacheException(String msg, String[] params, Locale locale) {
        super(msg, params, locale);
    }

    public DBCacheException(String msg, Throwable e, Locale locale) {
        super(msg, e, locale);
    }

    public DBCacheException(String msg, Throwable e) {
        super(msg, e);
    }

    public DBCacheException(String msg, Locale locale) {
        super(msg, locale);
    }

    public DBCacheException(String msg) {
        super(msg);
    }

    public DBCacheException(String msg, String[] params, Throwable e) {
        super(msg, params, e);
    }
}
