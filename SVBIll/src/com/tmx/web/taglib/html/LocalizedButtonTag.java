package com.tmx.web.taglib.html;

import org.apache.struts.taglib.TagUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.JspException;
import java.util.Locale;

/**
 * Created by IntelliJ IDEA.
 * User: galyamov
 * Date: 12/6/2006
 * Time: 15:45:30
 * To change this template use File | Settings | File Templates.
 */
public class LocalizedButtonTag extends com.tmx.web.taglib.base.LocalizableTag {

    protected String imageDir = "images/button/";

    protected String buttonName = null;

    //--------------------Properties

    public String getButtonName() {
        return buttonName;
    }

    public void setButtonName(String buttonName) {
        this.buttonName = buttonName;
    }

    //------------------------------Parsing events

    public int doStartTmxTag() throws JspException {
        String labelHTML = renderLabelHTML();
        TagUtils.getInstance().write(pageContext, labelHTML);
        return (EVAL_BODY_TAG);
    }

    //------------------------------Rendering methods
    private String renderLabelHTML() {
        Locale currentLocale = getLocale();

        HttpServletRequest httpServletRequest = (HttpServletRequest) pageContext.getRequest();

        StringBuffer resultHTML = new StringBuffer();

        resultHTML.
                append("<img src=\"").
                append(httpServletRequest.getContextPath()).
                append("/").
                append(imageDir).
                append(buttonName.substring(0, buttonName.lastIndexOf("."))).
                append("_").
                append(currentLocale.getLanguage()).
                append(buttonName.substring(buttonName.lastIndexOf("."), buttonName.length())).
                append("\" border=\"0\"/>");

        return resultHTML.toString();
    }


}
