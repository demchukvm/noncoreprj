package com.tmx.web.base;

import com.tmx.as.entities.general.user.User;
import com.tmx.as.exceptions.DatabaseException;
import com.tmx.as.exceptions.SecurityException;
import com.tmx.web.base.menuservice.MenuService;
import com.tmx.web.controls.WebEvent;
import com.tmx.web.taglib.html.ctrl.BrowseBoxTag;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Set;
import java.util.Iterator;
import java.util.Map;


public abstract class BasicAction extends Action implements BasicActionInterface {
    private ActionMapping mapping;
    private ActionForm form;
    private HttpServletRequest request;
    private HttpServletResponse response;
    private Logger logger = Logger.getLogger("web.BasicAction");

    public abstract ActionForward executeSpecificAction(ActionMapping mapping,
                                                        ActionForm form,
                                                        HttpServletRequest request,
                                                        HttpServletResponse response) throws Throwable;

    public ActionForward execute(ActionMapping mapping,
                                 ActionForm form,
                                 HttpServletRequest request,
                                 HttpServletResponse response) throws Exception {

        //Assign form name from mapping
        assignFormName(form, mapping);

        //Assign HttpRequest with this Action
        assignParameters(mapping, form, request, response);

        // save action name for abstractLayout.jsp output
        try {
            request.setAttribute("com.tmx.web.base.BasicAction.name",getName());
        } catch (WebException e) {
            new Exception(e);
        }

        //Check is canceled
        ActionForward actionForward = isCanceled(request, mapping);
        if (actionForward != null)
            return actionForward;

        try{
            /** Call setup own roadmap from received http params.
             * In common approach all actions should use roadmap to
             * get its forwarding info. Roadmap for some action is populated in
             * its predecessers. Particular actions could override setupOwnRoadmap() to
             * get data about its forwarding behavior from http params and put
             * it into the roadmap. Of course the hard-coded forwarding is still have place
             * where its is neded.
             *  */
            setupOwnRoadmap(form, request);

            // Collect scheduled parameters into roadmap
            collectScheduledParameters(form, request);

            // Listen menu requests
            MenuService.getInstance().listenMenuRequests(request);

            //reset the previos state of action
            if(form instanceof BasicActionForm)
                resetPreviosActionStatus((BasicActionForm)form);

            //fire event onBeforeAction
            if(form instanceof BasicActionForm)
                ((BasicActionForm)form).fireOnBeforeAction(new WebEvent(this, mapping, request));

            // Call specific action implementation
            actionForward = executeSpecificAction(mapping, form, request, response);

            //fire event onAfterAction
            if(form instanceof BasicActionForm)
                ((BasicActionForm)form).fireOnAfterAction(new WebEvent(this, mapping, request));
        }
        catch(WebException e){
            //Publish the Web exception
            RequestEnvironment.saveError(request, e);
            actionForward = (mapping.findForward("error"));
            logger.error(e.toString());
        }
        catch(SecurityException e){
            //Publish the Web exception
            RequestEnvironment.saveError(request, e);
            actionForward = (mapping.findForward("accessDenied"));
            logger.error(e.toString());
        }
        catch(DatabaseException e){
            //Publish the Database exception
            RequestEnvironment.saveError(request, e);
            actionForward = (mapping.findForward("error"));
            logger.error(e.toString());
        }
        catch(ValidationException e){
            //Return to input page and render validation exception
            RequestEnvironment.saveError(request, e);
            actionForward = e.getInputPageForward() != null ? e.getInputPageForward() : mapping.getInputForward();
            logger.error(e.toString());
            logger.error("Forwrded to input page "+actionForward != null ? actionForward.getPath() : null);
        }
        catch(Throwable e){
            //Publish the generic exception
            WebException generalException = new WebException("err.basic_action.general_error_on_web", e);
            RequestEnvironment.saveError(request, generalException);
            actionForward = (mapping.findForward("error"));
            logger.error(e.toString());
        }
        /** prepare new transaction's token to be generated on page.
         * Transaction's token could be validated with validateTransactionToken() in
         * particular actions to verify the request was sent exactly from recenlty rendered page. */
        prepareNewTransactionToken(request);
        return actionForward;
    }


    /** Successors of BasicAction could override this method to setup its own roadmap from
     * give HTTP request. */
    public void setupOwnRoadmap(ActionForm form, HttpServletRequest request) throws WebException{}

    private ActionForward isCanceled(HttpServletRequest request, ActionMapping mapping) {
        //Check is canceled
        if ((isCancelled(request) || request.getParameter("org.apache.struts.taglib.html.CANCEL") != null)) {
            String ft = "error";
            try {
                User user = ((SecurityService) ApplicationEnvironment.getService(SecurityService.class)).getLoggedInUser(request.getSession());
                UserInterfaceService userInterfaceService = (UserInterfaceService) ApplicationEnvironment.getService(UserInterfaceService.class);
                ft = userInterfaceService.getMenuForwardName(user.getRole().getKey());
            }
            catch (Throwable e) {
                logger.error(e.toString());
            }
            return (mapping.findForward(ft));
        }
        return null;
    }

    private void prepareNewTransactionToken(HttpServletRequest request){
        //ignore browse pages
        if(request.getParameter(BrowseBoxTag.BROWSE_PARAM_NAME) == null)
            saveToken(request);
    }

    /** Use this method in BasicAction's successors before starting critical business-transaction
     * to verify is current request was sent from recently generated page.
     * Prevents duplicate request handling. */
    protected void validateTransactionToken(HttpServletRequest request) throws WebException{
        if(!isTokenValid(request))
            throw new WebException("err.basic_action.transaction_token_validation_failed");
    }

    /** Assign form name from mapping */    
    private void assignFormName(ActionForm form, ActionMapping mapping){
        if(form instanceof BasicActionForm)
            ((BasicActionForm)form).setFormName(mapping.getName());
    }

    /** Assign incoming params sith action instance to use it outside execute() method. */
    private void assignParameters(ActionMapping mapping,
                                  ActionForm form,
                                  HttpServletRequest request,
                                  HttpServletResponse response){
        this.mapping = mapping;
        this.form = form;
        this.request = request;
        this.response = response;
    }

    /** Collect schedled parameters into roadmap */
    private void collectScheduledParameters(ActionForm form, HttpServletRequest request) throws WebException{
        Map scheduledParameters = getRoadmap().getScheduledParameters(this.getName());
        if(scheduledParameters == null)
            return;

        Iterator iter = scheduledParameters.keySet().iterator();
        while(iter.hasNext()){
            String paramName = (String)iter.next();
            Set collectForActionNameSet = (Set)scheduledParameters.get(paramName);
            String paramValue = request.getParameter(paramName);
            if(paramValue != null){
                //collect parameter for each action from the Set
                if(collectForActionNameSet != null){
                    Iterator iterActions = collectForActionNameSet.iterator();
                    while(iterActions.hasNext()){
                        getRoadmap().collectParameter((String)iterActions.next(), paramName, paramValue);
                    }
                }
            }

        }
    }

    /** Get roadmap assigned with current http session.
     * Create it and assign to session, if it's not exist/ */
    protected Roadmap getRoadmap() throws WebException{
        if(request == null)
            throw new WebException("err.basic_action.there_is_no_request_assigned_with_action", new String[]{this.getClass().getName()});

        return SessionEnvironment.allocateRoadmap(request.getSession());
    }

    /** Uses the <code>path</code> received from mapping as Action name*/
    protected String getName() throws WebException{
        return mapping.getPath();
    }

    /** Verify is the actually Form has ther same class as the prospective Form has. */
    protected ActionForm verifyForm(ActionForm actuallyForm, Class prospectiveForm) throws WebException{
        if(actuallyForm == null)
            throw new WebException("err.basic_action.actual_form_is_null");

        if(prospectiveForm == null)
            throw new WebException("err.basic_action.prospective_form_class_is_null");

         if(!prospectiveForm.isInstance(actuallyForm))
            throw new WebException("err.basic_action.actually_and_prospective_forms_class_mismatch", new String[]{actuallyForm.getClass().getName(), prospectiveForm.getName()});

        return actuallyForm;
    }

    private void resetPreviosActionStatus(BasicActionForm form){
        form.getActionExecStatus().reset();
    }

}
