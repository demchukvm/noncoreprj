package com.tmx.as.entities.bill.gates;

import com.tmx.as.base.BasicEntity;
import java.io.Serializable;

public class Gates extends BasicEntity {
    private Long gId;
    private String gateName;

    public Long getGId() {
        return gId;
    }

    public void setGId(Long gId) {
        this.gId = gId;
    }

    public String getGateName() {
        return gateName;
    }

    public void setGateName(String gateName) {
        this.gateName = gateName;
    }

    public Serializable getId() {
        return getGId();
    }

    public int getValid() {
        return valid;
    }

    public void setValid(int valid) {
        this.valid = valid;    
    }
}

