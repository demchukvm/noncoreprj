package com.tmx.beng.medaccess.ussd;

import com.tmx.beng.medaccess.MediumConnection;
import com.tmx.beng.base.BillingMessage;
import com.tmx.beng.ussdgate.UssdGate;
import com.tmx.beng.ussdgate.UssdGateException;
import org.apache.log4j.Logger;

/**
    Medium connection to work with USSD gate
 */
public class UssdGateMediumConnection implements MediumConnection {
    private UssdGate ussdGate = null;

    UssdGateMediumConnection() throws UssdGateException {
        ussdGate = UssdGate.getInstance( "ussd_gate" );
    }

    public void send(BillingMessage msg) {
        ussdGate.enqueueBillMessage(msg);
    }
}
