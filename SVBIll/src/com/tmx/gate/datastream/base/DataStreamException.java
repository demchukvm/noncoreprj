package com.tmx.gate.datastream.base;

import com.tmx.util.StructurizedException;
import java.util.Locale;

/** DataStreamException */
public class DataStreamException extends StructurizedException {
    public DataStreamException(String errKey){
        super(errKey);
    }

    public DataStreamException(String errKey, Locale locale){
        super(errKey, locale);
    }

    public DataStreamException(String errKey, String[] params){
        super(errKey, params);
    }

    public DataStreamException(String errKey, String[] params, Throwable cause){
        super(errKey, params, cause);
    }

    public DataStreamException(String errKey, String[] params, Locale locale){
        super(errKey, params, locale);
    }

    public DataStreamException(String errKey, Throwable cause){
        super(errKey, cause);
    }
}
