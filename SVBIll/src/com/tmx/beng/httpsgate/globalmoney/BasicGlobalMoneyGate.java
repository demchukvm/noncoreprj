package com.tmx.beng.httpsgate.globalmoney;

import com.tmx.as.entities.mobipay.globalmoney.GlobalMoneyTransaction;
import com.tmx.beng.httpsgate.*;

import com.tmx.as.base.EntityManager;
import com.tmx.as.base.criterion.CriterionWrapper;
import com.tmx.as.base.criterion.Restrictions;
import com.tmx.as.exceptions.DatabaseException;
import com.tmx.beng.access.Access;
import com.tmx.beng.base.*;
import com.tmx.gate.datastream.base.GateContext;
import com.tmx.util.Configuration;
import com.tmx.util.InitException;
import com.tmx.util.queue.ActionQueue;
import org.apache.commons.digester.Digester;
import org.apache.commons.httpclient.DefaultHttpMethodRetryHandler;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.params.HttpMethodParams;
import org.apache.commons.httpclient.protocol.Protocol;
import org.apache.log4j.Logger;
import java.io.*;
import java.util.*;

public abstract class BasicGlobalMoneyGate extends BasicGate
{
    protected Logger logger = Logger.getLogger(getGateName());

    private Access access = null;
    private ActionQueue messageQueue = null;
    private CodeMapping codeMapping;

	private static final String SERIAL = "serial";
	private static final String LOGIN = "login";
	private static final String AUTH_CODE = "authCode";
    private static final String URL_CHECK_PAY = "url.payCheck";
	private static final String URL_PAY = "url.pay";	
	private static final String URL_GET_STATUS = "url.status";
    private static final String PROP_INIT_HANDLER_COUNT = "handlers.initial";
    private static final String PROP_MAX_HANDLER_COUNT = "handlers.max";

    // сделать локальными
    //private String request;
	//private String response;


    protected abstract String getGateName();

    protected void init0() throws InitException
    {
        properties = GateContext.getInstance().getDatastreamProcessorConfig(getGateName());
        int initHandlerCount = Integer.parseInt(properties.getProperty(PROP_INIT_HANDLER_COUNT, "1"));
        int maxHandlerCount = Integer.parseInt(properties.getProperty(PROP_MAX_HANDLER_COUNT, "15"));
        messageQueue = new ActionQueue(initHandlerCount, maxHandlerCount);
        access = obtainBillingEngineAccess();

        codeMapping = new CodeMapping();
        codeMapping.init();
    }

    protected void shutdown0(final long timeout)
    {
        logger.info("Start GlobalMoney Gate Shutdown");
        if(messageQueue.actionCount() != 0){
            Runtime.getRuntime().addShutdownHook(new Thread(){
                public void run(){
                    try{
                        long lt = System.currentTimeMillis();
                        while(messageQueue.actionCount() != 0 &&
                                ((System.currentTimeMillis() - lt) < timeout)){
                            sleep(1000);
                        }
                    }
                    catch(Throwable e){
                        e.printStackTrace();
                    }
                }
            });
        }
        logger.info("GlobalMoney Gate shutdown");
    }

    public void enqueueBillMessage(BillingMessage billMessage)
    {
        GlobalMoneyGateTask task = new GlobalMoneyGateTask(billMessage, this);
        messageQueue.enqueue(task);
    }

    public void runBillMessage(BillingMessage billingMessage) throws HttpsGateException
    {
        BillingMessageImpl respMessage = new BillingMessageImpl();
        respMessage.setAttributeString(BillingMessage.REQUEST_ID, billingMessage.getAttributeString(BillingMessage.REQUEST_ID));
        try{
            if(BillingMessage.O_REFILL_PAYMENT.equals(billingMessage.getOperationName()))
            {
                refillPayment(billingMessage, respMessage);
            }
            else
            {
                respMessage.setStatusCode(StatusDictionary.SUBMIT_TRANSACTION_ERROR);
                throw new HttpsGateException("Unsupported command", new String[]{billingMessage.getOperationName()});
            }
        } catch(Throwable e){
            if(respMessage.getAttributeString(BillingMessage.STATUS_CODE) == null)
            {
                respMessage.setStatusCode(StatusDictionary.UNKNOWN_ERROR);
            }
            respMessage.setAttributeString(BillingMessage.STATUS_MESSAGE, e.getLocalizedMessage());
        }
        finally {
            try {
                access.obtainConnection().processAsync(respMessage);
            } catch(Throwable e) {
                logger.error(e.getLocalizedMessage(), e);
            }
        }
    }

    private void refillPayment(BillingMessage billingMessage, BillingMessageImpl respMessage) throws HttpsGateException, HttpsRequestException {

        respMessage.setOperationName(BillingMessage.O_REFILL_PAYMENT_RESULT);

        boolean isPayAccount = false;
        String request = "";
        String response = "";

        // pre-save
        EntityManager entityManager = new EntityManager();
        GlobalMoneyTransaction transaction = new GlobalMoneyTransaction();
        transaction.setBillTransactionNum(billingMessage.getAttributeString(BillingMessage.BILL_TRANSACTION_NUM));
        transaction.setRequestTime(new Date());
        
		String service = billingMessage.getAttributeString(BillingMessage.SERVICE_CODE);
        String account = billingMessage.getAttributeString(BillingMessage.ACCOUNT_NUMBER);

        if(account==null || account.isEmpty() || account.equals(""))
        {
			service = service + "_payAccount";
            transaction.setAccount(billingMessage.getAttributeString(BillingMessage.PAY_ACCOUNT_NUMBER)); // это место уточнить ??
            isPayAccount = true;
        }
        else
        {
			service = service + "_msisdn";
            transaction.setAccount("380" + account);
		}
        String amount = billingMessage.getAttributeString(BillingMessage.AMOUNT).replace(',','.');
        if(amount.contains("."))
        {
            String post = amount.substring(amount.indexOf(".")+1);
            if(post.length()==2)
            {
                transaction.setAmount(amount.replace(".", ""));
            }
            else if(post.length()==1)
            {
                transaction.setAmount(amount.replace(".", "")+"0");
            }
        }
        else
        {
            int b = Integer.parseInt(amount);
            b *= 100;
            transaction.setAmount(b+"");
        }
		transaction.setBillServiceCode(billingMessage.getAttributeString(BillingMessage.SERVICE_CODE));
        transaction.setGMServiceCode(codeMapping.getGMServiceCode(service));
        transaction.setCommission(codeMapping.getCommission(service));
        transaction.setTerminal(billingMessage.getAttributeString(BillingMessage.TRADE_POINT));

        try {
            entityManager.SAVE(transaction);
            CriterionWrapper criterions[] = new CriterionWrapper[] {Restrictions.eq("billTransactionNum", transaction.getBillTransactionNum())};
            GlobalMoneyTransaction _transaction = (GlobalMoneyTransaction) entityManager.RETRIEVE(GlobalMoneyTransaction.class, null, criterions, null);
            transaction.setGMTransactionNum(_transaction.getGMTransactionId()+"");
            entityManager.SAVE(transaction);
        } catch (DatabaseException e) {
            logger.error("Failed to pre-save GlobalMoney transaction", e);
        }

        GlobalMoneyResponse gmr = new GlobalMoneyResponse();

		// Check
		System.out.println("CHECK");
		request = serialized("CHECK", transaction, isPayAccount);
		response = send(properties.getProperty(URL_CHECK_PAY), transaction.getBillTransactionNum(), request, "CHECK");
		if(response != "")
		{
			gmr.parseResponse(response);

			if(gmr.getResult() == 0)
			{
				// Status
				while(gmr.getResult() == 0 && gmr.getStatus() != 0 && gmr.getStatus() != 1 && gmr.getStatus() < 100)
				{
					System.out.println("CHECK_STATUS");
					request = serialized("STATUS", transaction, isPayAccount);
					response = send(properties.getProperty(URL_GET_STATUS), transaction.getBillTransactionNum(), request, "STATUS");
					if(response != "")
					{
						gmr.parseResponse(response);
					}
				}

				if(gmr.getResult() == 0 && (gmr.getStatus() == 0 || gmr.getStatus() == 1))
				{
					// Pay
					System.out.println("PAY");
					request = serialized("PAY", transaction, isPayAccount);
					response = send(properties.getProperty(URL_PAY), transaction.getBillTransactionNum(), request, "PAY");
					if(response != "")
					{
						gmr.parseResponse(response);

						if(gmr.getResult() == 0)
						{
							// Status
							while(gmr.getResult() == 0 && gmr.getStatus() != 3 && gmr.getStatus() < 100)
							{
                                // pause
                                try {
                                    Thread.sleep(Long.parseLong("5000"));
                                } catch (InterruptedException e) {
                                    logger.error("Failed to sleep before 'PAY_STATUS' request", e);
                                }

								System.out.println("PAY_STATUS");
								request = serialized("STATUS", transaction, isPayAccount);
								response = send(properties.getProperty(URL_GET_STATUS), transaction.getBillTransactionNum(), request, "STATUS");
								if(response != "")
								{
									gmr.parseResponse(response);
								}
							}

							if(gmr.getResult() == 0 && gmr.getStatus() == 3)
							{
								// OK
								System.out.println("COMPLETE");
                                logger.info("PAY billNum: " + transaction.getBillTransactionNum());
                                logger.info("PAY complete");

                                respMessage.setStatusCode(StatusDictionary.STATUS_OK);
							}
							else
							{
								// error - pay
								System.out.println("PAY ERROR");
                                logger.error("PAY billNum: " + transaction.getBillTransactionNum());
                                logger.error("PAY pay_error");

                                respMessage.setAttributeString(BillingMessage.STATUS_MESSAGE, "pay_error");
                                respMessage.setStatusCode(StatusDictionary.BEGIN_TRANSACTION_ERROR);
							}
						}
						else
						{
							// error - pay
							System.out.println("PAY ERROR");
                            logger.error("PAY billNum: " + transaction.getBillTransactionNum());
                            logger.error("PAY pay_error");

                            respMessage.setAttributeString(BillingMessage.STATUS_MESSAGE, "pay_error");
                            respMessage.setStatusCode(StatusDictionary.BEGIN_TRANSACTION_ERROR);
						}
					}
					else
					{
						// error - gate connection - not got message from server;
                        logger.error("PAY billNum: " + transaction.getBillTransactionNum());
                        logger.error("PAY gate_connection_error");

                        respMessage.setAttributeString(BillingMessage.STATUS_MESSAGE, "unknown_error");
                        respMessage.setStatusCode(StatusDictionary.BEGIN_TRANSACTION_ERROR);
					}

				}
				else
				{
					// error - check
					System.out.println("CHECK ERROR");
                    logger.error("CHECK billNum: " + transaction.getBillTransactionNum());
                    logger.error("CHECK check_error");

                    respMessage.setAttributeString(BillingMessage.STATUS_MESSAGE, "check_error");
                    respMessage.setStatusCode(StatusDictionary.BEGIN_TRANSACTION_ERROR);
				}
			}
            else
			{
				// error - check
				System.out.println("CHECK ERROR");
                logger.error("CHECK billNum: " + transaction.getBillTransactionNum());
                logger.error("CHECK check_error");

                respMessage.setAttributeString(BillingMessage.STATUS_MESSAGE, "check_error");
                respMessage.setStatusCode(StatusDictionary.BEGIN_TRANSACTION_ERROR);
			}
		}
		else
		{
			// error - gate connection - not got message from server;
            logger.error("CHECK billNum: " + transaction.getBillTransactionNum());
            logger.error("CHECK gate_connection_error");

            respMessage.setAttributeString(BillingMessage.STATUS_MESSAGE, "unknown_error");
            respMessage.setStatusCode(StatusDictionary.BEGIN_TRANSACTION_ERROR);
		}


        // save
        transaction.setReceipt_no(gmr.getReceipt_no());
        transaction.setResponseTime(gmr.getResponseTime());
        transaction.setStatus(gmr.getStatus()+"");

        try {
            entityManager.SAVE(transaction);
        } catch (DatabaseException e) {
            logger.error("Failed to post-save GlobalMoney transaction", e);
        }
    }

    private String serialized(String act, GlobalMoneyTransaction tr, boolean isPayAccount)
	{
        String request = "";
        
		if(act.equals("CHECK") || act.equals("PAY"))
		{
            if(isPayAccount)
            {
                request = "{\"terminalDescr\":{\"serial\":" + properties.getProperty(SERIAL) + "," +
					"\"login\":\""+ properties.getProperty(LOGIN) +"\"," +
					"\"authCode\":\"" + properties.getProperty(AUTH_CODE) + "\"}," +
					"\"payments\":[{\"transactionId\":" + tr.getGMTransactionId() + "," +
					"\"amount\":" + tr.getAmount() + "," +
					"\"commission\":" + tr.getCommission() + "," +
					"\"offline\":false," +
					"\"timeStamp\":" + tr.getRequestTime().getTime() + "," +
					"\"serviceId\":" + tr.getGMServiceCode() + "," +
					"\"voucher\":false," +
					"\"sourceId\":\"" + tr.getTerminal() + "\"," +
					"\"sourceType\":0," +
					"\"accountInfo\":{\"accounts\":{\"account\":\"" + tr.getAccount() + "\"}}}]}";   
            }
            else
            {
			    request = "{\"terminalDescr\":{\"serial\":" + properties.getProperty(SERIAL) + "," +
					"\"login\":\""+ properties.getProperty(LOGIN) +"\"," +
					"\"authCode\":\"" + properties.getProperty(AUTH_CODE) + "\"}," +
					"\"payments\":[{\"transactionId\":" + tr.getGMTransactionId() + "," +
					"\"amount\":" + tr.getAmount() + "," +
					"\"commission\":" + tr.getCommission() + "," +
					"\"offline\":false," +
					"\"timeStamp\":" + tr.getRequestTime().getTime() + "," +
					"\"serviceId\":" + tr.getGMServiceCode() + "," +
					"\"voucher\":false," +
					"\"sourceId\":\"" + tr.getTerminal() + "\"," +
					"\"sourceType\":0," +
					"\"accountInfo\":{\"accounts\":{\"msisdn\":\"" + tr.getAccount() + "\"}}}]}";   // ??
            }
		}
		else if(act.equals("STATUS"))
		{
			request = "{\"terminalDescr\":{\"serial\":" + properties.getProperty(SERIAL) + "," +
					"\"login\":\""+ properties.getProperty(LOGIN) +"\"," +
					"\"authCode\":\"" + properties.getProperty(AUTH_CODE) + "\"}," +
					"\"statuses\":[{\"transactionId\":" + tr.getGMTransactionId() + "}]}";
		}

        System.out.println("request: " + request);

        logger.info(act + " billNum: " + tr.getBillTransactionNum());
        logger.info(act + " request: " + request);

        return request;
	}
  
	private String send(String url, String billingTransaction, String request, String act)throws HttpsRequestException
    {
        String response = "";
        GlobalMoneyResponse gmr = new GlobalMoneyResponse();

        do {
            PostMethod method = new PostMethod(url);
            method.setRequestHeader("Content-Type", "text/json");
            method.setRequestBody(request);

            try {
                HttpClient httpClient = new HttpClient();
                method.getParams().setParameter(HttpMethodParams.RETRY_HANDLER, new DefaultHttpMethodRetryHandler(3, false));

                Protocol.registerProtocol("https", new Protocol("https", new EasySSLProtocolSocketFactory(), 443));

                httpClient.executeMethod(method);

                if(method.getStatusCode() == HttpStatus.SC_OK)
                {
                    response = method.getResponseBodyAsString();
                    System.out.println(act + " response: " + response);

                    gmr.parseResponse(response);

                    logger.info(act + " billNum: " + billingTransaction);
                    logger.info(act + " response: " + response);
                }
                else
                {
                    response = "";
                    System.out.println(act + " response: error");
                    logger.error(act + " billNum: " + billingTransaction);
                    logger.error(act + " response: error");
                }
            } catch(IOException e) {
                e.printStackTrace();
            } catch(Throwable e) {
                e.printStackTrace();
            } finally {
                method.releaseConnection();
            }
        } while(gmr.getResult() == 401);

        return response;
	}


    private class CodeMapping
    {
        private Map<String,String> articleMapping_gbCode;
        private Map<String,String> articleMapping_commission;

        private void init() throws InitException
        {
            articleMapping_gbCode = new HashMap<String,String>();
            articleMapping_commission = new HashMap<String,String>();
            try {
                logger.debug("Start initialization of the GlobalMoney mapping");
                String gmMappingFile = Configuration.getInstance().getProperty("globalmoney.mapping_file");
                if (gmMappingFile == null)
                    throw new InitException("err.globalmoney.mapping_file_is_not_specified_in_config");

                File configFile = new File(gmMappingFile);
                Digester d = new Digester();
                d.push(this);

                d.addCallMethod("gbMapping/article", "addArticleMapping", 3);
                d.addCallParam("gbMapping/article", 0, "svbillCode");
                d.addCallParam("gbMapping/article", 1, "gbCode");
				d.addCallParam("gbMapping/article", 2, "commission");
				
                d.parse(configFile);

                logger.debug("Successful completed initialization of the GlobalMoney mapping");

            } catch (Exception e){
                throw new InitException("Failed to init GlobalMoney gate mapping", e);
            }
        }

        public void addArticleMapping(String svbillCode, String gbCode, String commission) {
            articleMapping_gbCode.put(svbillCode, gbCode);
            articleMapping_commission.put(svbillCode, commission);
        }

        private String getGMServiceCode(String svbillCode) {
            return articleMapping_gbCode.get(svbillCode);
        }

		private String getCommission(String svbillCode) {
            return articleMapping_commission.get(svbillCode);
        }
    }

}
