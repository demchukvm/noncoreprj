package com.tmx.web.actions.core.ui_permission;

import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForm;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.tmx.web.controls.ScrollBox;
import com.tmx.web.base.BasicDispatchedAction;
import com.tmx.web.forms.core.ui_permission.ManageUserInterfaceForm;
import com.tmx.as.base.EntityManager;
import com.tmx.as.entities.general.ui_permission.UserInterfaceType;

public class ManageUserInterfaceAction extends BasicDispatchedAction {
    private static final String FORWARD = "page";
    public ActionForward defaultAction(ActionMapping mapping,
                                       ActionForm form,
                                       HttpServletRequest request,
                                       HttpServletResponse response,
                                       String methodName) throws Throwable {
        ManageUserInterfaceForm manageUserInterfaceForm = (ManageUserInterfaceForm) verifyForm(form, ManageUserInterfaceForm.class);
        manageUserInterfaceForm.getTable().setRefreshActionName(this.getName());
        ((ScrollBox) manageUserInterfaceForm.getTable().// bind services
                getFilterSet().getFilter("com_tmx_as_entities_general_ui_permission_UserInterface_type").
                getParameter("value").getControl()).bind(
                new EntityManager().RETRIEVE_ALL(UserInterfaceType.class,null)
        );
        manageUserInterfaceForm.getTable().refresh();
        // set Readonly button
        if(manageUserInterfaceForm.getTable().getResultSize().intValue()==0){
            manageUserInterfaceForm.getOnDeleteButton().setReadonly(true);
            manageUserInterfaceForm.getOnEditButton().setReadonly(true);
        }else{
            manageUserInterfaceForm.getOnDeleteButton().setReadonly(false);
            manageUserInterfaceForm.getOnEditButton().setReadonly(false);
        }
        //initStructRelations(manageUserInterfaceForm);
        return mapping.findForward(FORWARD);
    }
}
