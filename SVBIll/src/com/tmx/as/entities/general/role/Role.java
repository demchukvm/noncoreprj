package com.tmx.as.entities.general.role;

import com.tmx.as.base.BasicEntity;

import java.io.Serializable;

public class Role extends BasicEntity {
    /** Predefinde role. It has all permissions. */
    public final static String SUPERUSER = "SUPERUSER";
    public final static String GUEST = "GUEST";
    private Long roleId;
    private String key;
    private String roleName;

    public Long getRoleId() {
        return roleId;
    }

    public void setRoleId(Long roleId) {
        this.roleId = roleId;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public Serializable getId() {
        return getRoleId();
    }

    public int getValid() {
        return valid;
    }

    public void setValid(int valid) {
        this.valid = valid;
    }
}
