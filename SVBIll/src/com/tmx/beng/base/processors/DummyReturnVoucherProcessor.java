package com.tmx.beng.base.processors;

import com.tmx.beng.base.*;
import com.tmx.beng.base.cache.DBCache;
import com.tmx.beng.base.cache.DBCacheException;
import com.tmx.as.base.criterion.CriterionWrapper;
import com.tmx.as.base.criterion.Restrictions;
import com.tmx.as.entities.bill.transaction.*;
import com.tmx.as.entities.bill.voucher.Voucher;
import com.tmx.as.entities.bill.seller.Seller;
import com.tmx.as.entities.bill.balance.SellerBalance;
import com.tmx.as.entities.bill.operator.Operator;
import com.tmx.as.entities.bill.terminal.Terminal;
import com.tmx.as.exceptions.DatabaseException;
import com.tmx.as.blogic.TransactionManager;
import com.tmx.as.base.EntityManager;
import com.tmx.as.base.ProcedureWrapper;
import com.tmx.as.base.QueryParameterWrapper;

import java.util.Map;
import java.util.List;
import java.sql.Types;


/**
 * Return voucher.
 */
public class DummyReturnVoucherProcessor extends TaskProcessor {

    public Task process0(Task task, DBCache dbCache, BillingMessageWritable msg) throws BillException{
        logger.debug("Dummy return voucher...");
        BillingMessageWritable respMsg = new BillingMessageImpl();
        try {
            respMsg.setAttribute(BillingMessage.STATUS_CODE, new Integer(0));
            respMsg.setAttribute(BillingMessage.WAIT_TIME, String.valueOf(task.getGlobalContext().getReporter().getWaitTimeEstimate()));

//            String cliTransNum = (String) task.getProcesedMessage().getAttribute(BillingMessage.CLI_TRANSACTION_NUM);
//            //Reversal can bew without client transaction/voucher inside!
//            if (cliTransNum != null) {
//                try {
//                    TransactionManager tm = new TransactionManager();
//                    ClientTransaction clientTransaction = tm.getClientTransactionByClientNum(cliTransNum);
//
//                    if(Integer.parseInt(clientTransaction.getStatus()) == StatusDictionary.STATUS_OK) {//only for OK operations
//                        //Retrive all related transaction
//                        Map transactions = tm.getAllTransactionByGuid(clientTransaction.getTransactionGUID());
//                        try {
//                            cancelTransactions(clientTransaction, transactions, dbCache);
//                        } catch (DBCacheException e) {
//                            throw new BillException("err.dbcache.to_cancel_transactions_return_voucher" + e.getMessage());
//                        }catch (DatabaseException e) {
//                            throw new BillException("err.db.to_cancel_transactions_return_voucher" + e.getMessage());
//                        }
//
//                        //return voucher to DB
//                        Voucher voucher = clientTransaction.getVoucher();
//                        if (voucher != null) {
//                            voucher.setStatus("NEW");
//                            respMsg.setAttributeString(BillingMessage.STATUS_MESSAGE,
//                                            "voucher_code: " + ((voucher.getCode() != null) ? voucher.getCode() : null) );
//
//                        }
//                        try {
//                            (new EntityManager()).SAVE(voucher);
//                        } catch (DatabaseException e) {
//                            throw new BillException("err.cant_return_voucher_to_DB" + e.getMessage());
//                        }
//                    }
//                } catch (DatabaseException e) {
//                    respMsg.setAttributeString(BillingMessage.STATUS_MESSAGE,
//                            "unknown error during returning voucher " + e.getMessage());
//                }
//            }
        } catch(Exception e) {
            respMsg.setAttributeString(BillingMessage.STATUS_MESSAGE,
                            "failed_to_retrive_client_transaction_for_reversal_message" + e.getMessage());
        }

        CallBack callBack = task.getCallBack();
        if(callBack != null)
            callBack.sendBilledMessage(respMsg);

        return task;
    }

    private void cancelTransactions(ClientTransaction clientTransaction, Map transactions, DBCache dbCache)
                                            throws DBCacheException, DatabaseException {
        List sellerTransactions = (List) transactions.get(TransactionManager.SELLER_TRANSACTIONS);
        List operatorTransactions = (List) transactions.get(TransactionManager.OPERATOR_TRANSACTIONS);
        List terminalTransactions = (List) transactions.get(TransactionManager.TERMINAL_TRANSACTIONS);
        EntityManager em = new EntityManager();

        //change Seller balances (increase balances, coze tx was wrong)
        for (Object sellerTransaction : sellerTransactions) {
            SellerBalanceTransaction st = (SellerBalanceTransaction) sellerTransaction;
            //Change balance in DB cache
            dbCache.changeSellerBalance(st.getSeller().getCode(),
                    st.getSellerBalance().getCode(),
                    st.getAmount(),
                    TransactionType.CODE_DEBET);
            //change status of Seller tx
            st.setStatus(Integer.toString(StatusDictionary.TRANSACTION_CANCELED));
            //Change balance in DB
            ProcedureWrapper wrapper = new ProcedureWrapper(
                                            new QueryParameterWrapper[]{
                                                    QueryParameterWrapper.createOutParameter(1, Types.INTEGER),
                                                    //change seller balance, tx reference to 1 seller and few balances
                                                    new QueryParameterWrapper(2, st.getSellerBalance().getSellerId()),
                                                    new QueryParameterWrapper(3, -st.getAmount()),},
                                            Seller.class,
                                            "change_seller_balance");
            wrapper.invoke(em);
            em.SAVE(st);
        }

        //change Terminal balances (increase balances, coze tx was wrong)
        for (Object terminalTransaction : terminalTransactions) {
            TerminalBalanceTransaction tt = (TerminalBalanceTransaction) terminalTransaction;
            //Change balance in DB cache
            //String terminalSN, String balanceCode, double value, String transTypeCode)
            dbCache.changeTerminalBalance(tt.getTerminal().getSerialNumber(),
                    tt.getTerminalBalance().getCode(),
                    tt.getAmount(),
                    TransactionType.CODE_DEBET);
            //change status of Terminal tx
            tt.setStatus(Integer.toString(StatusDictionary.TRANSACTION_CANCELED));
            //Change balance in DB
            ProcedureWrapper wrapper = new ProcedureWrapper(
                                            new QueryParameterWrapper[]{
                                                    QueryParameterWrapper.createOutParameter(1, Types.INTEGER),
                                                    //change seller balance, tx reference to 1 seller and few balances
                                                    new QueryParameterWrapper(2, tt.getTerminalBalance().getTerminalId()),
                                                    new QueryParameterWrapper(3, -tt.getAmount()),},
                                            Terminal.class,
                                            "change_terminal_balance");
            wrapper.invoke(em);
            em.SAVE(tt);
        }

        //change Operator balances (increase balance, coze tx was wrong)
        for (Object operatorTransaction : operatorTransactions) {
            OperatorBalanceTransaction ot = (OperatorBalanceTransaction) operatorTransaction;

            //Change balance in DB Cache
            dbCache.changeOperatorBalance(ot.getOperator().getCode(),
                    ot.getOperatorBalance().getCode(),
                    ot.getAmount(),
                    TransactionType.CODE_DEBET);
            //change status of Operator tx
            ot.setStatus(Integer.toString(StatusDictionary.TRANSACTION_CANCELED));

            //Change balance in real DB (using procedure)
            ProcedureWrapper wrapper = new ProcedureWrapper(
                                            new QueryParameterWrapper[]{
                                                    QueryParameterWrapper.createOutParameter(1, Types.INTEGER),
                                                    new QueryParameterWrapper(2, ot.getOperator().getOperatorId()),
                                                    new QueryParameterWrapper(3, -ot.getAmount()),},
                                            Operator.class,
                                            "change_operator_balance");
            wrapper.invoke(em);
            em.SAVE(ot);
        }
        clientTransaction.setStatus(Integer.toString(StatusDictionary.TRANSACTION_CANCELED));
        em.SAVE(clientTransaction);
    }
}