package com.tmx.web.forms.core.base;

import com.tmx.as.entities.general.role.Role;
import com.tmx.as.entities.general.user.User;
import com.tmx.as.exceptions.DatabaseException;
import com.tmx.web.forms.core.balance.seller_balance.ManageSellerBalancesForm;
import com.tmx.web.forms.core.balance.terminal_balance.BrowseTerminalsForm;
import com.tmx.web.forms.core.balance.terminal_balance.ManageTerminalBalancesForm;
import com.tmx.web.forms.core.balance.AbstractInfoBalanceForm;
import com.tmx.web.forms.core.dictionary.seller.ManageSellersForm;
import com.tmx.web.forms.core.dictionary.seller.InfoSellerForm;
import com.tmx.web.forms.core.dictionary.terminal.InfoTerminalForm;
import com.tmx.web.forms.core.dictionary.user.UserInfoForm;
import com.tmx.web.forms.core.dictionary.terminal.ManageTerminalsForm;
import com.tmx.web.forms.core.restriction.seller_restriction.ManageSellerRestrictionsForm;
import com.tmx.web.forms.core.restriction.seller_restriction.InfoSellerRestrictionForm;
import com.tmx.web.forms.core.restriction.terminal_restriction.ManageTerminalRestrictionsForm;
import com.tmx.web.forms.core.tariff.seller_tariff.ManageSellerTariffsForm;
import com.tmx.web.forms.core.tariff.seller_tariff.ManageSimpleSellerTariffsForm;
import com.tmx.web.forms.core.tariff.seller_tariff.InfoSellerTariffForm;
import com.tmx.web.forms.core.tariff.terminal_tariff.ManageTerminalTariffsForm;
import com.tmx.web.forms.core.transaction.client_transaction.ManageClientTransactionsForm;
import com.tmx.web.forms.core.transaction.seller_balance_transaction.ManageSellerBalanceTransactionsForm;
import com.tmx.web.forms.core.transaction.terminal_balance_transaction.ManageTerminalBalanceTransactionsForm;
import com.tmx.web.forms.core.reports.ReportPaymentForm;
import com.tmx.web.forms.core.reports.ReportVouchersForm;
import com.tmx.web.forms.core.reports.ReportNetworksForm;

/**
 * Abstract factory design pattern
 *@author Andrey N
 */
abstract public class AbstractFormFactory {

    static protected User loggedInUser;//TODO: encapsulate
    
    public static AbstractFormFactory getInstance(User loggedInUser){
        AbstractFormFactory.loggedInUser = loggedInUser;
        return getInstance(loggedInUser.getRole());
    }

    private static AbstractFormFactory getInstance(Role role){
        if("SUPERUSER".equals(role.getRoleName()))
            return new SuperUserFormFactory();

        if("BUSINESS_ADMIN".equals(role.getRoleName()))
            return new BusinessAdminFormFactory();

        return new BusinessAdminFormFactory();
    }

    abstract public void setUpForm(UserInfoForm userInfoForm) throws DatabaseException;

    abstract public void setUpForm(ManageSellersForm manageSellersForm) throws DatabaseException;

    abstract public void setUpForm(ManageSellerBalanceTransactionsForm form) throws DatabaseException;

    abstract public void setUpForm(ManageTerminalBalanceTransactionsForm form) throws DatabaseException;

    abstract public void setUpForm(ManageClientTransactionsForm form) throws DatabaseException;

    abstract public void setUpForm(ManageSellerBalancesForm form) throws DatabaseException;

    abstract public void setUpForm(ManageTerminalBalancesForm form, User user) throws DatabaseException;

    abstract public void setUpForm(ManageSellerTariffsForm form) throws DatabaseException;

    abstract public void setUpForm(ManageTerminalTariffsForm form) throws DatabaseException;

    abstract public void setUpForm(ManageSellerRestrictionsForm form) throws DatabaseException;

    abstract public void setUpForm(ManageTerminalRestrictionsForm form) throws DatabaseException;

    abstract public void setUpForm(BrowseTerminalsForm form) throws DatabaseException;

    abstract public void setUpForm(ManageSimpleSellerTariffsForm form) throws DatabaseException;

    abstract public void setUpForm(ManageTerminalsForm form) throws DatabaseException;

    abstract public void setUpForm(InfoTerminalForm form) throws DatabaseException;

    abstract public void setUpForm(ReportPaymentForm form) throws DatabaseException;

    abstract public void setUpForm(InfoSellerForm form) throws DatabaseException;

    abstract public void setUpForm(AbstractInfoBalanceForm form) throws DatabaseException;

    abstract public void setUpForm(InfoSellerRestrictionForm form) throws DatabaseException;

    abstract public void setUpForm(InfoSellerTariffForm form) throws DatabaseException;

    abstract public void setUpForm(ReportVouchersForm form) throws DatabaseException;

    abstract public void setUpForm(ReportNetworksForm form) throws DatabaseException;

}
