package com.tmx.engines.xmlengine;

import com.tmx.web.xmlengine.WebXMLPublisherUnified;
import com.tmx.web.xmlengine.WebXMLPublisher;


public class XMLPublisherFactory {

    public static WebXMLPublisher createXMLPublisher(){
        //return new XMLPublisherSimple();
        //return new XMLPublisherUnified();
        return new WebXMLPublisherUnified();
    }
}
