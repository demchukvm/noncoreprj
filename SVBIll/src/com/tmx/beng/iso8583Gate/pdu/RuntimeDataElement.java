package com.tmx.beng.iso8583Gate.pdu;

import com.tmx.beng.iso8583Gate.pdu.types.TypeFactory;
import com.tmx.beng.iso8583Gate.pdu.types.BinaryType;
import com.tmx.beng.iso8583Gate.pdu.types.Type;
import com.tmx.beng.iso8583Gate.pdu.substructure.RuntimeSubDataElement;
import com.tmx.beng.iso8583Gate.ISO8583Service;

import java.util.Date;

/**

 */
public class RuntimeDataElement {
    private DataElementFormat format;
    private int actualLength = -1;
    private ValueHandler valueHandler = new ValueHandler();
    private RuntimeMessage message;

    public RuntimeDataElement(DataElementFormat def){
        this.format = def;
    }


    //-------------Simple properties
    public void setActualLength(int length){
        this.actualLength = length;
    }

    public int getActualLength() {
        return actualLength;
    }

    public DataElementFormat getFormat() {
        return format;
    }

    public ParticularDataElementFormat getParticularFormat(){
        if(message == null)
            return null;
        MessageParticularFormat mpf = message.getMessageParticularFormat();
        if(mpf == null)
            return null;
        return mpf.getDataElementFormat(getFormat().getName());
    }

    void setMessage(RuntimeMessage message) {
        this.message = message;
    }

    public void setValue(byte[] value) {
        this.valueHandler.binaryValue = value;
    }

    /** set binary value from int array. Only lower byte of each int is used.
     * @param value int array to convert into byte array. */
    public void setValue(int[] value) {
        byte[] data = new byte[value.length];
        for(int i=0; i<value.length; i++)
            data[i] = (byte)(value[i] & 0x000000ff);

        this.valueHandler.binaryValue = data;
    }

    public void setValue(long value){
        this.valueHandler.longValue = value;
    }

    public void setValue(String value){
        this.valueHandler.stringValue = value;
    }

    public void setValue(Date value){
        this.valueHandler.dateValue = value;
    }

    public void setValue(RuntimeSubDataElement value){
        this.valueHandler.rsde = value;
    }

    public boolean getBit(int bitnum) throws IncorrectFormatException{
        if(TypeFactory.BINARY.equals(format.getType())){
            BinaryType binaryType = (BinaryType)format.getType();
            return binaryType.getBitmapBit(bitnum, valueHandler.binaryValue);
        }
        else{
            throw new IncorrectFormatException("err.get_bit_supported_only_for_binary_type", new String[]{format.getType().getName()});
        }
    }

    public byte[] getBinary() throws IncorrectFormatException{
        if(TypeFactory.BINARY.equals(format.getType())){
            return valueHandler.binaryValue;
        }
        else{
            throw new IncorrectFormatException("err.get_binary_supported_only_for_binary_type", new String[]{format.getType().getName()});
        }
    }

    public long getLong() throws IncorrectFormatException{
        if(TypeFactory.NUMBER.equals(format.getType())){
            return valueHandler.longValue;
        }
        else{
            throw new IncorrectFormatException("err.get_int_supported_only_for_number_type", new String[]{format.getType().getName()});
        }
    }

    public Date getDate() throws IncorrectFormatException{
        if(TypeFactory.TIMESTAMP.equals(format.getType())){
            return valueHandler.dateValue;
        }
        else{
            throw new IncorrectFormatException("err.get_date_supported_only_for_timestamp_type", new String[]{format.getType().getName()});
        }
    }

    public String getString(){
        Type type = format.getType();
        if(TypeFactory.ACHARS.equals(type) || TypeFactory.ANCHARS.equals(type) ||
           TypeFactory.ANSCHARS.equals(type) || TypeFactory.ASCHARS.equals(type) ||
           TypeFactory.SCHARS.equals(type) || TypeFactory.NSCHARS.equals(type) || TypeFactory.Z.equals(type)){

            return valueHandler.stringValue;
        }
        else if(TypeFactory.NUMBER.equals(type)){
            return String.valueOf(valueHandler.longValue);
        }
        else if(TypeFactory.TIMESTAMP.equals(type)){
            return valueHandler.dateValue.toString();//rm: we can format it with attached pattern in future
        }
        else if(TypeFactory.BINARY.equals(type)){
            BinaryType binaryType = (BinaryType)format.getType();
            return binaryType.getHexString(valueHandler.binaryValue);
        }
        else{
            return null;
            //throw new IncorrectFormatException("err.get_string_unsupported", new String[]{type.getName()});
        }
    }

    public RuntimeSubDataElement getSubstructureDataElement(){
        return valueHandler.rsde;
    }
    
    private class ValueHandler{
        private byte[] binaryValue;
        private long longValue;
        private String stringValue;
        private Date dateValue;
        private RuntimeSubDataElement rsde;
    }
}
