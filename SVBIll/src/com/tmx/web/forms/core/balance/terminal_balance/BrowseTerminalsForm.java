package com.tmx.web.forms.core.balance.terminal_balance;

import com.tmx.as.entities.bill.seller.Seller;
import com.tmx.as.entities.general.user.User;
import com.tmx.as.exceptions.DatabaseException;
import com.tmx.as.resolver.FunctionSyntaxException;
import com.tmx.as.resolver.ResolverException;
import com.tmx.as.base.criterion.CriterionWrapper;
import com.tmx.as.base.criterion.Restrictions;
import com.tmx.web.controls.ScrollBox;
import com.tmx.web.controls.CriterionFactory;
import com.tmx.web.controls.EditBox;
import com.tmx.web.controls.table.Parameter;
import com.tmx.web.controls.table.Table;
import com.tmx.web.controls.table.ControlManagerTable;
import com.tmx.web.forms.core.base.AbstractBrowseForm;
import com.tmx.web.forms.core.base.AbstractFormFactory;


public class BrowseTerminalsForm extends AbstractBrowseForm {
    EditBox editBoxSerialNumber;

    protected void init(){

        editBoxSerialNumber = new EditBox();
        initTable();

        ScrollBox scrollBoxSellers = new ScrollBox() {

            protected String getKey(Object dataListElement) {
                return ((Seller) dataListElement).getSellerId().toString();
            }

            protected String getValue(Object dataListElement) {
                return ((Seller) dataListElement).getName();
            }

        };
        scrollBoxSellers.setNullable(false);
        getBrowseTable().addParameterToFilter("by_seller_id", new Parameter("seller_id", scrollBoxSellers));
    }

    private void initTable() {
       setBrowseTable(new ControlManagerTable("query(query_type=retrieve_all," +
               "entity_class=com.tmx.as.entities.bill.terminal.Terminal," +
               "table_attr=sellerOwner," +
               "order(name=id,type=asc)," +
               "page_size=15" +
               ")"));

       CriterionFactory factory = new CriterionFactory() {
               public CriterionWrapper createCriterion() {
                 if(getEditBoxSerialNumber().getValue() != null) {
                    return Restrictions.ilike("serialNumber", "%" + getEditBoxSerialNumber().getValue() + "%");
                 } else return null;
               }
           };

           getBrowseTable().setCriterionFactory(factory);
   }

    public EditBox getEditBoxSerialNumber() {
       return editBoxSerialNumber;
   }

    public void setEditBoxSerialNumber(EditBox editBoxSerialNumber) {
       this.editBoxSerialNumber = editBoxSerialNumber;
   }
}
