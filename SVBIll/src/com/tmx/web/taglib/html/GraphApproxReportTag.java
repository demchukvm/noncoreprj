package com.tmx.web.taglib.html;

import org.apache.struts.taglib.TagUtils;
import org.apache.struts.taglib.nested.NestedPropertyHelper;
import javax.servlet.jsp.JspException;
import javax.servlet.http.HttpServletRequest;
import com.tmx.util.i18n.MessageResources;
import com.tmx.web.base.SessionEnvironment;
import com.tmx.web.taglib.base.LocalizableTag;
import com.tmx.engines.graphengine.base.ApproximationReport;
import java.util.Locale;


/**
Publish Graph approximation report
 */
public class GraphApproxReportTag extends LocalizableTag {
    /** Form bean name */
    private String name;
    /** Report property */
    private String reportProperty;

    //--------------------Properties

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getReportProperty() {
        return reportProperty;
    }

    public void setReportProperty(String reportProperty) {
        this.reportProperty = reportProperty;
    }

    //------------------------------Parsing events
    public int doStartTmxTag() throws JspException {
        String reportHTML = renderReportHTML();
        TagUtils.getInstance().write(pageContext, reportHTML);
        return (EVAL_BODY_TAG);
    }

    //------------------------------Rendering methods
    private String renderReportHTML() throws JspException {
        StringBuffer resultHTML = new StringBuffer();
        Locale locale = SessionEnvironment.getLocale(((HttpServletRequest) pageContext.getRequest()).getSession());

        ApproximationReport approximationReport = getGraphApproximationReport();
        if(approximationReport == null)
            return resultHTML.toString();

        MessageResources msgResources = MessageResources.getInstance();
        resultHTML.append("<table border=\"0\" cellpadding=\"5\" cellspacing=\"0\" width=\"100%\" height=\"100%\">");
        //title
        resultHTML.append("<tr><td colspan=\"5\">");
        resultHTML.append(msgResources.getOwnedLocalizedMessage(ApproximationReport.class, "labels", getLocale(), "approximation_report.title", null));
        resultHTML.append("</td></tr>");

        //headers
        resultHTML.append("<tr>");
        resultHTML.append("<th>" + msgResources.getOwnedLocalizedMessage(ApproximationReport.class, "labels", getLocale(), "approximation_report.dataset.id", null) + "</th>");
        resultHTML.append("<th>" + msgResources.getOwnedLocalizedMessage(ApproximationReport.class, "labels", getLocale(), "approximation_report.dataset.name", null) + "</th>");
        resultHTML.append("<th>" + msgResources.getOwnedLocalizedMessage(ApproximationReport.class, "labels", getLocale(), "approximation_report.dataset.is_approximation_used", null) + "</th>");
        resultHTML.append("<th>" + msgResources.getOwnedLocalizedMessage(ApproximationReport.class, "labels", getLocale(), "approximation_report.dataset.initial_points_count", null) + "</th>");
        resultHTML.append("<th>" + msgResources.getOwnedLocalizedMessage(ApproximationReport.class, "labels", getLocale(), "approximation_report.dataset.approximated_points_count", null) + "</th>");
        resultHTML.append("<th>" + msgResources.getOwnedLocalizedMessage(ApproximationReport.class, "labels", getLocale(), "approximation_report.dataset.points_on_graph_count", null) + "</th>");
        resultHTML.append("</tr>");

        //data
        int rowId = 0;
        for(int i = 0; i < approximationReport.getBlockReportCount(); i++){
            ApproximationReport.BlockReport blockReport = approximationReport.getBlockReport(i);
            if(!blockReport.isBlockEnabled())
                continue;
            rowId++;
            resultHTML.append("<tr>");
            resultHTML.append("<td align=\"center\">" + rowId + "</td>");
            resultHTML.append("<td align=\"left\">" + blockReport.getName() + "</td>");
            resultHTML.append("<td align=\"center\"" + ((blockReport.isApproxUsed()) ? ("style=\"{color: #ff0000; font-weight: bold;}\"") : "") + ">" + blockReport.isApproxUsed() + "</td>");
            resultHTML.append("<td align=\"center\">" + blockReport.getInitialPointsCount() + "</td>");
            resultHTML.append("<td align=\"center\">" + ((blockReport.isApproxUsed()) ? String.valueOf(blockReport.getApproxedPointsCount()) : "---") + "</td>");
            resultHTML.append("<td align=\"center\">" + ((blockReport.isApproxUsed()) ? String.valueOf(blockReport.getApproxedPointsCount()) : String.valueOf(blockReport.getInitialPointsCount())) + "</td>");
            resultHTML.append("</tr>");
        }

        //values
        resultHTML.append("</table>");

        return resultHTML.toString();
    }

    //------------------------------Bean interactin methods
    private ApproximationReport getGraphApproximationReport() throws JspException{
        HttpServletRequest request = (HttpServletRequest) pageContext.getRequest();
        String originalName = (getName() != null) ? getName() : NestedPropertyHelper.getCurrentName(request, this);
        String originalProperty = getReportProperty();
        String nestedProperty = NestedPropertyHelper.getAdjustedProperty(request, originalProperty);

        Object value = TagUtils.getInstance().lookup(pageContext, originalName, nestedProperty, null);
        ApproximationReport approximationReport = null;
        if (value != null && (value instanceof ApproximationReport))
            approximationReport = (ApproximationReport) value;
        return approximationReport;
    }



}
