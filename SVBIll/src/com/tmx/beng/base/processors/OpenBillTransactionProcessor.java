package com.tmx.beng.base.processors;

import com.tmx.beng.base.*;
import com.tmx.beng.base.memory.Condition;
import com.tmx.beng.base.memory.InMemoryObject;
import com.tmx.beng.base.cache.DBCache;

import java.util.Date;
import java.util.List;


/**
 * Register message 
 */
public class OpenBillTransactionProcessor extends TaskProcessor {

    public Task process0(Task task, DBCache dbCache, BillingMessageWritable msg) throws BillException{
        logger.debug("Opening bill transaction...");

        if(msg.getAttributeString(BillingMessage.BILL_TRANSACTION_NUM) != null){
            msg.setStatusCode(StatusDictionary.OPEN_TRANSACTION_ERROR);
            throw new BillException("err.bill.reg_processor.message_already_has_transaction_id", new String[]{
                    task.getProcesedMessage().getAttributeString(BillingMessage.BILL_TRANSACTION_NUM)});
        }

        //instantiate transaction
        String transactionId = task.getGlobalContext().getTransactionPool().createTransactionId();
        msg.setAttributeString(BillingMessage.BILL_TRANSACTION_NUM, transactionId);
        msg.setAttribute(BillingMessage.REGISTRATION_TIME, new Date());

        return task;
    }
}
