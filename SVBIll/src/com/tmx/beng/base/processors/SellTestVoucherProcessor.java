package com.tmx.beng.base.processors;

import com.tmx.beng.base.*;
import com.tmx.beng.base.cache.DBCache;
import com.tmx.beng.base.cache.DBCacheException;
import com.tmx.as.entities.bill.terminal.Terminal;
import com.tmx.as.blogic.TransactionManager;
import com.tmx.as.exceptions.DatabaseException;

import java.util.GregorianCalendar;
import java.util.Date;
import java.util.Calendar;

/**

 */
public class SellTestVoucherProcessor extends TaskProcessor {
//    TODO: move to another processor, or rename this
    protected Task process0(Task task, DBCache dbCache, BillingMessageWritable msg) throws BillException {
        logger.debug("Voucher reservation processor...");

        String voucherNominal = msg.getAttributeString(BillingMessage.VOUCHER_NOMINAL);
        /*something like PING*/
        if (voucherNominal.toLowerCase().indexOf("test") > 0) {
            //publish voucher to be handled by SellVoucherProcessor
            msg.setStatusCode(StatusDictionary.STATUS_OK);
            throw new BillException("test_voucher_sold", new String[]{voucherNominal});
        } else if (voucherNominal.toLowerCase().indexOf("balances") > 0) {/*get balance query*/
            try {             
                Terminal terminal = dbCache.getTerminal(msg.getAttribute(BillingMessage.TERMINAL_SN).toString());
                String tBalanceAmount = dbCache.getTerminalPrimaryBalanceAmount(terminal.getSerialNumber()).toString();
                String sBalanceAmount =  dbCache.getSellerPrimaryBalanceAmount(terminal.getSellerOwnerId()).toString();
                msg.setStatusCode(StatusDictionary.UNKNOWN_ERROR);
                throw new BillException("", new String[]{"Terminal= ".concat(tBalanceAmount), "Dealer= ".concat(sBalanceAmount)});
            } catch (DBCacheException e) {
                msg.setStatusCode(StatusDictionary.TEST_ERROR);
                throw new BillException("failed_to_get_balances", new String[]{msg.getAttribute(BillingMessage.TERMINAL_SN).toString()});
            }
        } else if (voucherNominal.toLowerCase().indexOf("finactivites") > 0) {/*retrive financial activites for period*/
            TransactionManager.CashCalculation calculation = getFinActivities(task, dbCache, msg);
            msg.setStatusCode(StatusDictionary.UNKNOWN_ERROR);
            throw new BillException("", new String[]{"Fee= ".concat(calculation.getFullProfit().toString()),
                                                    "Credit= ".concat(calculation.getTakenFromRootSeller().toString()),
                                                    "Sum= ".concat(calculation.getAmountTx().toString())});
        }
        return task;
    }

    private TransactionManager.CashCalculation getFinActivities(Task task, DBCache dbCache, BillingMessageWritable msg) throws BillException {
        GregorianCalendar calendar = new GregorianCalendar();
//        if (msg.getAttribute(BillingMessage.ACCOUNT_NUMBER) == null) {
//            throw new BillException("Date is not set. [ddMMyy]");
//        }
//        try {
//            calendar.setTime((new SimpleDateFormat("ddMMyy")).parse(msg.getAttribute(BillingMessage.AMOUNT).toString()));
//        } catch (ParseException e) {
////           throw new BillException("Date is not set correctly. [ddMMyy] " + e.getMessage());
//        }
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MINUTE, 0);

        TransactionManager manager = new TransactionManager();
        Long sellerId;
        Long balanceId;
        try {
            sellerId = (dbCache.getTerminal(msg.getAttribute(BillingMessage.TERMINAL_SN).toString())).getSellerOwnerId();
            balanceId = dbCache.getSellerById(sellerId).getPrimaryBalance().getBalanceId();
        } catch (DBCacheException e) {
            throw new BillException("err_get_seller_owner " + e.getMessage());
        }
        try {
            return manager.getCashCalculation(balanceId, sellerId, calendar.getTime(), new Date());
        } catch (DatabaseException e) {
            throw  new BillException("err_get_fin.activities " + e.getMessage());
        }

    }
}
