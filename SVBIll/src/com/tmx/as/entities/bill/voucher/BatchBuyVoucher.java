package com.tmx.as.entities.bill.voucher;

import com.tmx.as.base.BasicEntity;

import java.io.Serializable;
import java.util.Date;

/**
 This entity stores information about batch aquiring vouchers.
 */
public class BatchBuyVoucher extends BasicEntity {
    private Long batchBuyVoucherId;
    private Date transactionTime;
    private Integer voucherCount;
    private Integer voucherNominal;
    private String userActor;
    private String status;
    private String sellerCode;
    private String processingCode;
    private String terminalSn;
    private String serviceCode;
    private String operationType;
    private String dumpFileUrl;
    // Proximan 01.11.2010
    private String cliTransactionNum;
    /*private String genSellerCode;
    private String genServiceCode;
    private String genService;
    private String genTerminalSN;*/
    // поддиллер
    // сервайс
    // тип ГЕН

    public Long getBatchBuyVoucherId() {
        return batchBuyVoucherId;
    }

    public void setBatchBuyVoucherId(Long batchBuyVoucherId) {
        this.batchBuyVoucherId = batchBuyVoucherId;
    }

    public Date getTransactionTime() {
        return transactionTime;
    }

    public void setTransactionTime(Date transactionTime) {
        this.transactionTime = transactionTime;
    }

    public Integer getVoucherCount() {
        return voucherCount;
    }

    public void setVoucherCount(Integer voucherCount) {
        this.voucherCount = voucherCount;
    }

    public Integer getVoucherNominal() {
        return voucherNominal;
    }

    public void setVoucherNominal(Integer voucherNominal) {
        this.voucherNominal = voucherNominal;
    }

    public String getUserActor() {
        return userActor;
    }

    public void setUserActor(String userActor) {
        this.userActor = userActor;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getSellerCode() {
        return sellerCode;
    }

    public void setSellerCode(String sellerCode) {
        this.sellerCode = sellerCode;
    }

    public String getProcessingCode() {
        return processingCode;
    }

    public void setProcessingCode(String processingCode) {
        this.processingCode = processingCode;
    }

    public String getTerminalSn() {
        return terminalSn;
    }

    public void setTerminalSn(String terminalSn) {
        this.terminalSn = terminalSn;
    }

    public String getServiceCode() {
        return serviceCode;
    }

    public void setServiceCode(String serviceCode) {
        this.serviceCode = serviceCode;
    }

    public String getOperationType() {
        return operationType;
    }

    public void setOperationType(String operationType) {
        this.operationType = operationType;
    }

    public String getDumpFileUrl() {
        return dumpFileUrl;
    }

    public void setDumpFileUrl(String dumpFileUrl) {
        this.dumpFileUrl = dumpFileUrl;
    }

    public Serializable getId() {
        return getBatchBuyVoucherId();
    }

    public int getValid() {
        return valid;
    }

    public void setValid(int valid) {
        this.valid = valid;
    }

    // Proximan 01.11.2010

    public String getCliTransactionNum() {
        return cliTransactionNum;
    }

    public void setCliTransactionNum(String cliTransactionNum) {
        this.cliTransactionNum = cliTransactionNum;
    }

    /*public String getGenSellerCode() {
        return genSellerCode;
    }

    public void setGenSellerCode(String genSellerCode) {
        this.genSellerCode = genSellerCode;
    }

    public String getGenServiceCode() {
        return genServiceCode;
    }

    public void setGenServiceCode(String genServiceCode) {
        this.genServiceCode = genServiceCode;
    }

    public String getGenService() {
        return genService;
    }

    public void setGenService(String genService) {
        this.genService = genService;
    }

    public String getGenTerminalSN() {
        return genTerminalSN;
    }

    public void setGenTerminalSN(String genTerminalSN) {
        this.genTerminalSN = genTerminalSN;
    }*/

    // //////////////
}
