package com.tmx.web.base;

import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForm;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.lang.reflect.Method;
import java.lang.reflect.InvocationTargetException;

/**
 * Dispatch incoming request to one of the enclosed methods
 */
public class BasicDispatchedAction extends BasicAction{
    /** Name of command paramter are used to dispatch action from jsp.
     * Or use mapping.getParameter() and add 'paramter' attribute to
     * the struts config file - as it is used in Struts default action */
    private final String COMMAND_PARAMETER = "command";

    public final ActionForward executeSpecificAction(ActionMapping mapping,
                                                     ActionForm form,
                                                     HttpServletRequest request,
                                                     HttpServletResponse response) throws Throwable{

        String methodName = request.getParameter(COMMAND_PARAMETER);
        if(methodName == null || "null".equals(methodName.toLowerCase())){
            methodName = getRoadmap().getDefaultActionCommandName(this.getName());
            if(methodName == null)
                return defaultAction(mapping, form, request, response, null);
        }

        Method dispatchedMethod = null;
        try{
            dispatchedMethod = this.getClass().getMethod(methodName, new Class[]{ActionMapping.class, ActionForm.class, HttpServletRequest.class, HttpServletResponse.class});
        }
        catch(NoSuchMethodException e){
            return defaultAction(mapping, form, request, response, methodName);
        }
        try{
            return (ActionForward)dispatchedMethod.invoke(this, new Object[]{mapping, form, request, response});            
        }
        catch(InvocationTargetException e){
            //try to unwrap original exctption from InvocationTargetException
            throw e.getCause();
        }
    }


    /**  Override in successors if you want to define custom behaviour
     * on unknown command */
    protected ActionForward defaultAction(ActionMapping mapping,
                                          ActionForm form,
                                          HttpServletRequest request,
                                          HttpServletResponse response,
                                          String methodName) throws Throwable{

        throw new WebException("err.dispathed_action.unknown_command", new String[]{methodName});
    }
}
