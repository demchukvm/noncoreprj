package com.tmx.web.taglib.base;

import org.apache.struts.taglib.nested.NestedPropertyTag;
import java.util.Locale;
import com.tmx.web.base.SessionEnvironment;
import com.tmx.util.i18n.MessageResources;
import javax.servlet.http.HttpServletRequest;

public abstract class LocalizableTag extends TmxNestedPropertyTag {

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    protected Locale getLocale(){
        if(pageContext == null || pageContext.getRequest() == null || ((HttpServletRequest)pageContext.getRequest()).getSession() == null)
            return Locale.getDefault();//default locale has been set from setpu.properties "default_locale" parameter.
        else{
            Locale locale = SessionEnvironment.getLocale(((HttpServletRequest)pageContext.getRequest()).getSession());
            return (locale == null) ? Locale.getDefault() : locale;
        }
    }

    protected String getLocalizedLabel(String key, String[] params){
        return MessageResources.getInstance().getOwnedLocalizedMessage(this.getClass(), "tags_labels", getLocale(), key, params);
    }

    protected String getDefaultProperty(String key){
        return MessageResources.getInstance().getOwnedLocalizedMessage(this.getClass(), "tags_defaults", null, key, null);
    }

}
