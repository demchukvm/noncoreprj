package com.tmx.beng.base;

/**
 */
public interface BillingMessageWritable extends BillingMessage{
    public void setAttributeString(String name, String value);
    public void setAttribute(String name, Object value);
    public void setStatusCode(int statusCode);
    public void setStatus(int code, String message);
    public void setOperationName(String operationName);

}
