package com.tmx.beng.base.cache;

import com.tmx.as.entities.bill.terminal.Terminal;

import java.util.Map;
import java.util.HashMap;
import java.util.List;
import java.util.Vector;

/**
 */
class TerminalWrapper {
    /** Terminal entity */
    Terminal terminal;
    /** balances by Id */
    Map balanceId_balance = new HashMap();
    Map balanceCode_balanceId = new HashMap();    
    /** tariff entities by tariff ids */
    Map tariffId_tariffEntity = new HashMap();
    /** terminal tariffs function instances */
    Map tariffId_tariffFunctionInstance = new HashMap();
    /** tariff function instances by tariff names */
    //Map tariffName_tariffFunctionInstance = new HashMap();
    /** terminal restricitons */
    List restriction = new Vector();

    TerminalWrapper(Terminal terminal){
        this.terminal = terminal;
    }

    public List getRestriction() {
        return restriction;
    }

    public Terminal getTerminal() {
        return terminal;
    }
}
