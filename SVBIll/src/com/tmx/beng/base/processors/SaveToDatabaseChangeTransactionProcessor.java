package com.tmx.beng.base.processors;

import com.tmx.beng.base.Task;
import com.tmx.beng.base.BillingMessage;
import com.tmx.beng.base.BillException;
import com.tmx.beng.base.cache.DBCache;

import java.util.List;


public class SaveToDatabaseChangeTransactionProcessor extends SaveToDatabaseTransactionProcessor{

    protected List getSavedTransactionEntitiesList(Task task, DBCache dbCache, BillingMessage msg) throws BillException {
        return (List)msg.getAttribute(BillingMessage.ENTITY_LIST);  
    }

}
