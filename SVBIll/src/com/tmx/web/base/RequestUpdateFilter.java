package com.tmx.web.base;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/** Updates current request in the  */
public class RequestUpdateFilter  implements Filter {

    public void init(FilterConfig filterConfig) throws ServletException {
        //do nothing
    }

    /** Set the current Request into current UserContextForm.
     * Create it if it is not exists yet */
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain chain) throws IOException, ServletException {
        HttpSession httpSession = ((HttpServletRequest)servletRequest).getSession();

        //reallocate request
        RequestEnvironment.reallocateReqContext(new RequestContext((HttpServletRequest)servletRequest, (HttpServletResponse)servletResponse));

        UserContextForm userContextForm = (UserContextForm)SessionEnvironment.getAttr(httpSession, SessionEnvironment.USER_CONTEXT_FORM);
        if(userContextForm == null)
            userContextForm = new UserContextForm();

        SessionEnvironment.setAttr(httpSession, SessionEnvironment.USER_CONTEXT_FORM, userContextForm);
        //Set or update request and session for form
//        userContextForm.setHttpRequest((HttpServletRequest)servletRequest);

        //Add current request into request history
        HttpServletRequest httpServletRequest = (HttpServletRequest)servletRequest;
        //String requestURL = httpServletRequest.getScheme() + "://" + httpServletRequest.getServerName() + ":" + httpServletRequest.getServerPort() + httpServletRequest.getContextPath() + httpServletRequest.getServletPath() + httpServletRequest.getRequestURI() + "|" + httpServletRequest.getRequestURL() + httpServletRequest.getQueryString();
        String requestURL = httpServletRequest.getRequestURL() +
                ((httpServletRequest.getQueryString() != null) ? ("?" + httpServletRequest.getQueryString()) : "");
        userContextForm.getRequestHistory().handleNewRequest(requestURL);

        chain.doFilter(servletRequest, servletResponse);
    }

    /** Free the refs */
    public void destroy() {
        //do nothing
    }
}
