<%@ page language="java" pageEncoding="UTF-8" contentType="text/html; charset=utf-8" isELIgnored="false" %>
<!--errorPage="/core/jsp/message/jsperror.jsp"-->
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean" %>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c" %>
<%@ taglib uri="http://tmx.com/taglib/tmx-logic" prefix="tmx_logic" %>
<%@ taglib uri="http://tmx.com/taglib/tmx-controls" prefix="tmx_ctrl" %>

<!-- toolBarPlaceHolder -->
    <c:if test="${requestScope['tile.loggedInLayout.toolBars'] != null}">
    <tr>
        <tmx_logic:iterator items="${requestScope['tile.loggedInLayout.toolBars']}" var="pageBlock">
            <tiles:insert page="${pageBlock}"/>
        </tmx_logic:iterator>
    </tr>
    </c:if>