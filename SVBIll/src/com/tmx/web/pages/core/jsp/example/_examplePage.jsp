<%@ page language="java" pageEncoding="UTF-8" contentType="text/html; charset=utf-8" errorPage="/core/jsp/message/jsperror.jsp" isELIgnored="false" %>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/tmx-controls.tld" prefix="tmx_ctrl" %>
<%--
Example page -- to explain how to use tmx controls from form
--%>
<html>
  <head>
      <title>example jsp page</title>
      <LINK REL="STYLESHEET" HREF="<html:rewrite page='/css/controls.css'/>" TYPE="text/css">
      <script type="text/javascript" src="<html:rewrite page='/management/script/dhtml.js'/>"></script>
      <script type="text/javascript" src="<html:rewrite page='/management/script/scroll.js'/>"></script>
      <script type="text/javascript" src="<html:rewrite page='/management/script/common.js'/>"></script>
      <script type="text/javascript" src="<html:rewrite page='/management/script/calendar/calendar.js'/>"></script>
      <script type="text/javascript" src="<html:rewrite page='/management/script/calendar/lang/calendar-en.js'/>"></script>
      <script type="text/javascript" src="<html:rewrite page='/management/script/calendar/calendar-setup.js'/>"></script>
      <!-- IMPORTANT: Include after other include scripts
      to prevent overriding of onEvent functons -->
      <script type="text/javascript" src="<html:rewrite page='/scripts/global.js'/>"></script>

      <!-- news -->
   <%--     <script type="text/javascript" src="<html:rewrite page='core/scripts/news.js'/>"></script> --%>
      
  </head>

  <!-- IMPORTANT: It is recomended to use tmx_ctrl:BODY tag instead of plain
     HTML body, or html:body
-->
  <tmx_ctrl:body>

    <!-- IMPORTANT: All tmx controls should be enclosed into form. -->
    <html:form action="_exampleAction">

        <!--  Check boxes -->
        <tmx_ctrl:checkBox name="_exampleForm" property="checkBox1"/>
        <br/>
        <tmx_ctrl:checkBox name="_exampleForm" property="myPanel.checkBox2"/>
        <br/>
        <tmx_ctrl:checkBox name="_exampleForm" property="myPanel.myPanel2.checkBox3"/>
        <br/>

        <!--  Select boxes -->
        <tmx_ctrl:selectBox name="_exampleForm" property="selectRoles1" selectBoxStyleClass="editbox width110"/>
        <br/>
        <tmx_ctrl:selectBox name="_exampleForm" property="myPanel.myPanel2.selectRoles3" selectBoxStyleClass="editbox width110"/>
        <br/>

        <!--  Radio button groups -->
        <tmx_ctrl:radioButtonGroup name="_exampleForm" property="radioRoles1" styleClass="radiobuttongroup width_290"/>
        <br/>
        <tmx_ctrl:radioButtonGroup name="_exampleForm" property="myPanel.myPanel2.radioRoles3"  styleClass="radiobuttongroup width_290"/>
        <br/>

        <!-- Text area -->
        <tmx_ctrl:textArea name="_exampleForm" property="textArea1"/>

        <html:submit/>

    </html:form>

  </tmx_ctrl:body>
</html>