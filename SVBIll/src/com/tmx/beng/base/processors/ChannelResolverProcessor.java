package com.tmx.beng.base.processors;

import com.tmx.beng.base.*;
import com.tmx.beng.base.cache.DBCache;

/**
Check payement properties (service, account and other)
 */
public class ChannelResolverProcessor extends TaskProcessor {

    public Task process0(Task task, DBCache dbCache, BillingMessageWritable msg) throws BillException {
        logger.debug("Channel resolver...");

        String channel = ChannelResolver.getInstance().getChannel(
                msg.getAttributeString(BillingMessage.SERVICE_CODE),
                msg,
                configFile
        );

        if(channel.equals("Beeline"))
        {
            channel = getChannelByAmount(msg.getAttributeString(BillingMessage.AMOUNT));
        }

        if(channel.equals("BeelineExc") ||
                channel.equals("Beeline_FixConnect") ||
                channel.equals("KyivstarBonusNoCommission") ||
                channel.equals("KS_FixConnect") ||
                channel.equals("LifeExc") ||
                channel.equals("MTSExc"))
        {

            if(channel.equals("BeelineExc"))
            {
                if(!ChannelResolver.getInstance().isContains(channel, msg.getAttributeString(BillingMessage.CLI_TRANSACTION_NUM)))
                    throw new BillException("err.bill.out_channel_is_absent.service_is_blocked",
                            new String[]{"BeelineExc", msg.getAttributeString(BillingMessage.CLI_TRANSACTION_NUM)});
            }

            if(channel.equals("Beeline_FixConnect"))
            {
                if(!ChannelResolver.getInstance().isContains(channel, msg.getAttributeString(BillingMessage.CLI_TRANSACTION_NUM)))
                    throw new BillException("err.bill.out_channel_is_absent.service_is_blocked",
                            new String[]{"Beeline_FixConnect", msg.getAttributeString(BillingMessage.CLI_TRANSACTION_NUM)});
            }

            if(channel.equals("KyivstarBonusNoCommission"))
            {
                if(!ChannelResolver.getInstance().isContains(channel, msg.getAttributeString(BillingMessage.CLI_TRANSACTION_NUM)))
                    throw new BillException("err.bill.out_channel_is_absent.service_is_blocked",
                            new String[]{"KyivstarBonusNoCommission", msg.getAttributeString(BillingMessage.CLI_TRANSACTION_NUM)});
            }

            if(channel.equals("KS_FixConnect"))
            {
                if(!ChannelResolver.getInstance().isContains(channel, msg.getAttributeString(BillingMessage.CLI_TRANSACTION_NUM)))
                    throw new BillException("err.bill.out_channel_is_absent.service_is_blocked",
                            new String[]{"KS_FixConnect", msg.getAttributeString(BillingMessage.CLI_TRANSACTION_NUM)});
            }

            if(channel.equals("LifeExc"))
            {
                if(!ChannelResolver.getInstance().isContains(channel, msg.getAttributeString(BillingMessage.CLI_TRANSACTION_NUM)))
                    throw new BillException("err.bill.out_channel_is_absent.service_is_blocked",
                            new String[]{"LifeExc", msg.getAttributeString(BillingMessage.CLI_TRANSACTION_NUM)});
            }

            if(channel.equals("MTSExc"))
            {
                if(!ChannelResolver.getInstance().isContains(channel, msg.getAttributeString(BillingMessage.CLI_TRANSACTION_NUM)))
                    throw new BillException("err.bill.out_channel_is_absent.service_is_blocked",
                            new String[]{"MTSExc", msg.getAttributeString(BillingMessage.CLI_TRANSACTION_NUM)});
            }


        }

        msg.setAttribute(BillingMessage.SERVICE_CODE, channel);
        return task;
    }

    private String getChannelByAmount(String amount)
    {
        String channel = "";
        int b = getNumericAmount(amount);

        if(b>0 && b<2500) { channel = "KS1-25"; }
        else if(b>2500 && b<10000) { channel = "KS25-99"; }
        else if(b>10000) { channel = "KS100+"; }

        return channel;
    }

    private int getNumericAmount(String amount)
    {
        int b = 0;
        if(amount.contains("."))
        {
            String post = amount.substring(amount.indexOf(".")+1);
            if(post.length()==2)
            {
                b = new Integer(amount.replace(".", ""));
            }
            else if(post.length()==1)
            {
                b = new Integer(amount.replace(".", "")+"0");
            }
        }
        else
        {
            b = Integer.parseInt(amount);
            b *= 100;

        }
        return b;
    }

}
