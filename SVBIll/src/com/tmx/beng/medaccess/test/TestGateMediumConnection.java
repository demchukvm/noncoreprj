package com.tmx.beng.medaccess.test;

import com.tmx.beng.base.BillingMessage;
import com.tmx.beng.testgate.TestGate;
import com.tmx.beng.testgate.TestGateException;
import com.tmx.beng.medaccess.MediumConnection;
import org.apache.log4j.Logger;

/**

 */
public class TestGateMediumConnection implements MediumConnection {
    private TestGate testGate = null;

    TestGateMediumConnection() throws TestGateException {
        testGate = TestGate.getInstance();
    }

    public void send(BillingMessage msg) {
        testGate.enqueueBillMessage(msg);
    }
}
