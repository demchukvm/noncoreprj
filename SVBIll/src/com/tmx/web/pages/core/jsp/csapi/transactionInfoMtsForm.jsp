<%@ page import="com.tmx.web.forms.trans_mgmt.TransactionInfoMtsForm" %>
<%@ taglib prefix="nested" uri="http://jakarta.apache.org/struts/tags-nested" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/tld/tmx-controls.tld" prefix="tmx_ctrl" %>

<html:html>
<head>
</head>
    <html:form action="/transactionInfoMtsAction">
        
        <%--<div width="100%" align="right">
            UmcBalance
            <tmx_ctrl:editBox property="editBoxMtsBalance"/>
            <tmx_ctrl:button property="onRefreshBalance"/>
        </div>--%>

        <table border=0 align="center">
            <tr>
                <td class="dataLabel">BillTransaction</td>
                <td class="dataLabel">
                    <tmx_ctrl:editBox property="editBoxBillTransaction" />
                </td>
            </tr>
            <tr>
                <td>ClientTransaction</td>
                <td class="dataLabel">
                    <tmx_ctrl:editBox property="editBoxCliTransaction"/>
                </td>
            </tr>
            <tr>
                <td>StatusCode</td>
                <td class="dataLabel">
                    <tmx_ctrl:editBox property="editBoxStatusCode"/>
                </td>
            </tr>
            <tr>
                <td>StatusMessage</td>
                <td class="dataLabel">
                    <tmx_ctrl:editBox property="editBoxStatusMessage"/>
                </td>
            </tr>
            <tr>
                <td>TransactionTime</td>
                <td class="dataLabel">
                    <tmx_ctrl:editBox property="editBoxTransactionTime"/>
                </td>
            </tr>
            <tr>
                <td>ClientTime</td>
                <td class="dataLabel">
                    <tmx_ctrl:editBox property="editBoxCientTime"/>
                </td>
            </tr>
            <tr>
                <td>AccountNumber</td>
                <td class="dataLabel">
                    <tmx_ctrl:editBox property="editBoxAccountNumber"/>
                </td>
            </tr>
            <tr>
                <td>Amount</td>
                <td class="dataLabel">
                    <tmx_ctrl:editBox property="editBoxAmount"/>
                </td>
            </tr>
            <tr>
                <td>Seller</td>
                <td class="dataLabel">
                    <tmx_ctrl:editBox property="editBoxSellerName"/>
                </td>
            </tr>
            <tr>
                <td>Terminal</td>
                <td class="dataLabel">
                    <tmx_ctrl:editBox property="editBoxTerminalNumber"/>
                </td>
            </tr>
            <tr>
                <td>Operator</td>
                <td class="dataLabel">
                    <tmx_ctrl:editBox property="editBoxOperatorName"/>
                </td>
            </tr>
            <tr>
                <td colspan="2" align="right">
                    <tmx_ctrl:button property="onCancelButton"/>
                    <tmx_ctrl:button property="onInfoButton"/>
                </td>
            </tr>
        </table>

        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />

        <table border=0 align="center">
            <tr>
                <td>URL первой страницы:</td>
                <td><tmx_ctrl:editBox property="ebFirstPageUrl" /></td>
            </tr>
            <tr>
                <td>Всего страниц:</td>
                <td><tmx_ctrl:editBox property="ebTotalPages" /></td>
            <tr>
                <td>Номер первого сообщение с голосом:</td>
                <td><tmx_ctrl:editBox property="ebFirstValidMessage" /></td>
            <tr>
                <td colspan="2"><tmx_ctrl:button property="onGetResult"/></td>
            </tr>
            <tr>
                <td colspan="2">
                    <%= ((TransactionInfoMtsForm) pageContext.getSession().getAttribute("transactionInfoMtsForm")).getV1() %><br />
                    <%= ((TransactionInfoMtsForm) pageContext.getSession().getAttribute("transactionInfoMtsForm")).getV2() %><br />
                    <%= ((TransactionInfoMtsForm) pageContext.getSession().getAttribute("transactionInfoMtsForm")).getV3() %><br />
                    <%= ((TransactionInfoMtsForm) pageContext.getSession().getAttribute("transactionInfoMtsForm")).getV4() %><br />
                    <%= ((TransactionInfoMtsForm) pageContext.getSession().getAttribute("transactionInfoMtsForm")).getV5() %><br />
                    <%= ((TransactionInfoMtsForm) pageContext.getSession().getAttribute("transactionInfoMtsForm")).getV6() %> <br /><br /><br />
                </td>
            </tr>
        </table>
    </html:form>

</html:html>