package com.tmx.web.forms.core.tariff.seller_tariff;

import com.tmx.as.base.criterion.CriterionWrapper;
import com.tmx.as.base.criterion.Restrictions;
import com.tmx.as.blogic.BlogicException;
import com.tmx.as.blogic.tariff.SellerTariffManager;
import com.tmx.as.entities.bill.seller.Seller;
import com.tmx.as.entities.general.user.User;
import com.tmx.as.exceptions.DatabaseException;
import com.tmx.web.controls.CheckBox;
import com.tmx.web.controls.CriterionFactory;
import com.tmx.web.controls.PopulateUtil;
import com.tmx.web.controls.ScrollBox;
import com.tmx.web.forms.core.tariff.AbstractManageSimpleTariffsForm;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;


public class ManageSimpleSellerTariffsForm extends AbstractManageSimpleTariffsForm {

    private List sellers;
    private List subSellers;


    public void copySelectedTariff() throws BlogicException, DatabaseException {// copy from incoming tariffs to subsellers tariffs
        new SellerTariffManager().copyTariff(getIncomingTariffId(), getSelectedSubSellerId());
    }

    public void copyAllTariffs(User loggeInUser) throws BlogicException, DatabaseException{// copy all from incoming tariffs to sub sellers tariffs
        new SellerTariffManager().copyAllTariffs(getSelectedSellerIdIncomingTable(), getSelectedSubSellerId());
    }

    protected void init(){
        super.init();
        initTariffsTable();
        initTariffsTableBySeller();
        initButtons();
        initFilters();
    }

    public void bindSellers(List sellers) {
        ScrollBox sellersScrollBox = (ScrollBox)getIncomingTriffsTable().getParameterControl("by_seller_id", "seller_id");
        sellersScrollBox.bind(sellers);
        this.sellers = sellers;
    }

    public void bindSubSellers(List subSellers) {
        ScrollBox sellersScrollBox = (ScrollBox)getTariffsTable().getParameterControl("by_seller_id", "seller_id");

        Collections.sort(subSellers, new Comparator<Seller>()
        {
          public int compare(Seller s1, Seller s2) {
            return s1.getCode().compareTo(s2.getCode());
          }
        });

        sellersScrollBox.bind(subSellers);
        this.subSellers = subSellers;
    }

    public Long getSelectedSubSellerId() {
        return PopulateUtil.getLongKey( (ScrollBox)getTariffsTable().getParameterControl("by_seller_id", "seller_id") );
    }

    public String getSelectedSubSellerName() {
        return ((ScrollBox) getTariffsTable().getParameterControl("by_seller_id", "seller_id")).getSelectedEntry().getValue();
    }

    public Long getSelectedSellerIdIncomingTable() {
        return PopulateUtil.getLongKey( (ScrollBox)getIncomingTriffsTable().getParameterControl("by_seller_id", "seller_id") );
    }

    public String getSelectedSellerName() {
        return ((ScrollBox) getIncomingTriffsTable().getParameterControl("by_seller_id", "seller_id")).getSelectedEntry().getValue();
    }

    public void setUpTariffsTableFilters() {
        CriterionFactory factory = new CriterionFactory() {
            public CriterionWrapper createCriterion() {
                if (subSellers.isEmpty() || subSellers == null)
                    return Restrictions.in("seller", getNonExistentSellers());
                return Restrictions.in("seller", subSellers);
            }
        };
        getTariffsTable().setCriterionFactory(factory);
    }

    private Seller[] getNonExistentSellers() {
        Seller seller = new Seller();
        seller.setSellerId(new Long(-1));
        return new Seller[]{seller};
    }

    public List getSellers() {
        return sellers;
    }

    public List getSubSellers() {
        return subSellers;
    }

    private void initTariffsTable() {
        getTariffsTable().loadQuery("query(query_type=retrieve_all," +
                "entity_class=com.tmx.as.entities.bill.tariff.SellerTariff," +
                "table_attr=functionInstance.functionType," +
                "table_attr=functionInstance.functionParameters," +
                "table_attr=seller," +
                "order(name=sellerId,type=asc)," +
                "page_size=150" +
                ")");
        getTariffsTable().setRefreshActionName("/core/tariff/seller_tariff/manageSimpleSellerTariffs");
        getTariffsTable().setRefreshCommandName("refreshTariffsTable");
    }

    private void initTariffsTableBySeller() {
        getIncomingTriffsTable().loadQuery("query(query_type=retrieve_all," +
                "entity_class=com.tmx.as.entities.bill.tariff.SellerTariff," +
                "table_attr=functionInstance.functionType," +
                "table_attr=functionInstance.functionParameters," +
                "table_attr=seller," +
                "order(name=sellerId,type=asc)," +
                "page_size=150" +
                ")");
        getIncomingTriffsTable().setRefreshActionName("/core/tariff/seller_tariff/manageSimpleSellerTariffs");
        getIncomingTriffsTable().setRefreshCommandName("refreshIncomingTariffsTable");
    }

    private void initFilters(){
        getTariffsTable().addParameterToFilter("by_seller_id", "seller_id", initSellersScrollBoxFilter(true));
        getIncomingTriffsTable().addParameterToFilter("by_seller_id", "seller_id", initSellersScrollBoxFilter(true));

    }

    private ScrollBox initSellersScrollBoxFilter(boolean nullable){
        ScrollBox sellersScrollBox = new ScrollBox(){
            protected String getKey(Object dataListElement) {
                return ((Seller)dataListElement).getId().toString();
            }

            protected String getValue(Object dataListElement) {
                return ((Seller)dataListElement).getName();
            }
        };
        sellersScrollBox.setNullable(nullable);
        return sellersScrollBox;
    }

    private void initButtons(){
        getOnCreateButton().setAction("/core/tariff/seller_tariff/manageSimpleSellerTariffs");
        getOnEditButton().setAction("/core/tariff/seller_tariff/infoSellerTariff");
        getOnDeleteButton().setAction("/core/tariff/seller_tariff/manageSimpleSellerTariffs");
        
        getOnCopyButton().setAction("/core/tariff/seller_tariff/manageSimpleSellerTariffs");
        getOnCopyAllButton().setAction("/core/tariff/seller_tariff/manageSimpleSellerTariffs");
        
        getOnDeleteITButton().setAction("/core/tariff/seller_tariff/manageSimpleSellerTariffs");
        getOnCreateITButton().setAction("/core/tariff/seller_tariff/manageSimpleSellerTariffs");
        getOnEditITButton().setAction("/core/tariff/seller_tariff/infoSellerTariff");
    }

}
