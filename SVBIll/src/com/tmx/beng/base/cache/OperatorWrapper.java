package com.tmx.beng.base.cache;

import com.tmx.as.entities.bill.operator.Operator;

import java.util.List;
import java.util.Vector;
import java.util.Map;
import java.util.HashMap;

/**
 */
class OperatorWrapper {
    Operator operator;
    Map balanceId_balance = new HashMap();
    Map balanceCode_balanceId = new HashMap();
    //Map ballanceId_tariff = new HashMap();
    Map tariffId_tariffEntity = new HashMap();
    Map tariffId_tariffFunctionInstance = new HashMap();
    //Map tariffName_tariffFunctionInstance = new HashMap();
    List restriction = new Vector();

    OperatorWrapper(Operator operator){
        this.operator = operator;
    }

    public Operator getOperator() {
        return operator;
    }

    public List getRestriction() {
        return restriction;
    }
}
