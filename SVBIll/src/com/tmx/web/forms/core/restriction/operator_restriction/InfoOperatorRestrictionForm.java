package com.tmx.web.forms.core.restriction.operator_restriction;

import com.tmx.web.forms.core.restriction.AbstractInfoRestrictionForm;
import com.tmx.web.controls.ScrollBox;
import com.tmx.web.controls.PopulateUtil;
import com.tmx.as.entities.bill.restriction.AbstractRestriction;
import com.tmx.as.entities.bill.restriction.OperatorRestriction;
import com.tmx.as.entities.bill.operator.Operator;
import com.tmx.as.entities.general.user.User;
import com.tmx.as.exceptions.DatabaseException;


public class InfoOperatorRestrictionForm extends AbstractInfoRestrictionForm {

    private ScrollBox scrollBoxOperator;

    protected void init() {
        super.init();
        initButtons();
        initTables();
        initBrowseBoxes();
        scrollBoxOperator = new ScrollBox<Operator>(){
            protected String getKey(Operator dataListElement) {
                return dataListElement.getOperatorId().toString();
            }
            protected String getValue(Operator dataListElement) {
                return dataListElement.getName();
            }
        };
    }

    private void initBrowseBoxes() {
        getBrowseBoxFunctionTypes().setAction("/core/restriction/operator_restriction/browseOperatorFuncTypesAction");
    }

    private void initButtons(){
        getOnSaveButton().setAction("/core/restriction/operator_restriction/infoOperatorRestriction");
        getOnApplyButton().setAction("/core/restriction/operator_restriction/infoOperatorRestriction");
        getOnCancelButton().setAction("/core/restriction/operator_restriction/manageOperatorRestrictions");

        getOnRefreshParamButton().setAction("/core/restriction/operator_restriction/infoOperatorRestriction");
        getOnCreateParamButton().setAction("/core/restriction/operator_restriction/infoOperatorRestriction");
        getOnEditParamButton().setAction("/core/restriction/operator_restriction/operatorRestrictionParameter");
        getOnDeleteParamButton().setAction("/core/restriction/operator_restriction/operatorRestrictionParameter");
    }

    private void initTables(){
        getFuncParametrsTable().setRefreshActionName("/core/restriction/operator_restriction/infoOperatorRestriction");
        getDefaultFuncParametrsTable().setRefreshActionName("/core/restriction/operator_restriction/infoOperatorRestriction");
    }

    protected AbstractRestriction populateFromSpecificControls() {
        OperatorRestriction restriction = new OperatorRestriction();

        Operator operator = new Operator();
        operator.setOperatorId(PopulateUtil.getLongKey(scrollBoxOperator));
        restriction.setOperator(operator);

        return restriction;
    }

    protected void bindSpecificData(AbstractRestriction restriction, User loggedInUser) throws DatabaseException {
        //do nohing
    }

    protected void bindSpecificData(AbstractRestriction restriction) {
        OperatorRestriction rest = (OperatorRestriction)restriction;
        scrollBoxOperator.setSelectedKey(PopulateUtil.getString(rest.getOperatorId()));
    }

    public ScrollBox getScrollBoxOperator() {
        return scrollBoxOperator;
    }

    public void setScrollBoxOperator(ScrollBox scrollBoxOperator) {
        this.scrollBoxOperator = scrollBoxOperator;
    }

}
