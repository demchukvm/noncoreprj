package com.tmx.web.actions.core.csapi;

import com.tmx.as.modules.SpringModule;
import com.tmx.web.forms.core.csapi.CsApiCallForm;
import com.tmx.web.forms.core.csapi.LifeRequestForm;
import com.tmx.web.base.BasicDispatchedAction;
import com.tmx.util.InitException;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForm;
import org.apache.log4j.Logger;
import com.tmx.beng.access.Access;
import com.tmx.beng.access.Connection;
import com.tmx.beng.csapi.xml.v1.base.ClientPAD;
import com.tmx.beng.base.BillException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.naming.InitialContext;
import javax.naming.NamingException;


/**
 */
public class LifeRequestlAction extends BasicDispatchedAction {
    Logger logger = Logger.getLogger("webcsapi."+getClass().getName());

    public ActionForward lifeRequestCall(ActionMapping mapping,
                                                        ActionForm form,
                                                        HttpServletRequest request,
                                                        HttpServletResponse response) throws Throwable {

        LifeRequestForm lifeRequestForm = (LifeRequestForm) verifyForm(form, CsApiCallForm.class);
        ClientPAD clientPAD = new ClientPAD();
        try{

            //TODO(A.N) use this connection
            Connection conn = (Connection) SpringModule.getApplicationContext().getBean("billingConnection");
            //1. Allocate access to billing engine
//            try{
//                Access access = obtainBillingEngineAccess(lifeRequestForm);
//                conn = access.obtainConnection();

//                Connection conn = (Connection) SpringModule.getApplicationContext().getBean("billingConnection");

//            }
//            catch(InitException e){
//                e.printStackTrace();
//                throw new HandledBillingException();
//            }

            long t = System.currentTimeMillis();
            ClientPAD.ParsingResult resp;

            //2. Parse request
//            try{
////                logger.info("Request XML: " + lifeRequestForm.getRequestXml());
////                resp = clientPAD.parseRequest(lifeRequestForm.getRequestXml());
//                logger.debug("Parse time: " + (System.currentTimeMillis() - t));
//            }
//            catch(BillException e){
//                logger.error("Parsing error: \n"+e.getTraceDump());
//                lifeRequestForm.setXmlDocSerialized(clientPAD.buildErrorResponse(ClientPAD.CSAPI_REQUEST_PARSING_ERROR, getLocale(request)));
//                throw new HandledBillingException();
//            }

            //3. Process request
//            try{
//                resp.setBillingMessage(conn.processSync(resp.getBillingMessage()));
//            }
//            catch(BillException e){
//                logger.error("Processing error: \n"+e.getTraceDump());
//                lifeRequestForm.setXmlDocSerialized(clientPAD.buildErrorResponse(ClientPAD.CSAPI_REQUEST_PROCESSING_ERROR, getLocale(request)));
//                throw new HandledBillingException();
//            }

            //4. Serialize request
//            lifeRequestForm.setXmlDocSerialized(clientPAD.safeSerializeResponse(resp.getBillingMessage(), resp.getVersion()));
            logger.info("Response XML: " + lifeRequestForm.getXmlDocSerialized());
//        }
//        catch(HandledBillingException e){
            /** do nothing already handled */
        }
        catch(Throwable e){
            logger.error("Unknown error: \n"+e.toString());
            lifeRequestForm.setXmlDocSerialized(clientPAD.buildErrorResponse(ClientPAD.CSAPI_UNKNOWN_ERROR, getLocale(request)));
        }
        return mapping.findForward("csapicall_resp");
    }


    private Access obtainBillingEngineAccess(LifeRequestForm lifeRequestForm) throws InitException{
        if(lifeRequestForm.getBillingEngineAccess() == null){
            InitialContext ctx = null;
            try{
                System.setProperty("java.naming.factory.url.pkgs", ""); //=org.apache.naming
                ctx = new InitialContext();//System.getProperties()
                Access newAccess = (Access)ctx.lookup("java:comp/services/billingEngine");
                if(newAccess == null)
                    throw new InitException("err.csapi.null_basic_access_obtained");

                lifeRequestForm.setBillingEngineAccess(newAccess);
            }
            catch(NamingException e){
                throw new InitException("err.csapi.failed_to_obtain_billing_engine_access", e);
            }
            finally{
			    if(ctx != null){
				    try{
    					ctx.close();
    				}
	    			catch(NamingException e) {
		    			//log.info( "Unable to release initial context", ne );
			    	}
			    }
            }
        }
        return lifeRequestForm.getBillingEngineAccess();
    }

    /** Builds general xml error response on web csapi level */

    protected ActionForward defaultAction(ActionMapping mapping,
                                          ActionForm form,
                                          HttpServletRequest request,
                                          HttpServletResponse response,
                                          String methodName) throws Throwable{

        return lifeRequestCall(mapping, form, request, response);
    }

    private class HandledBillingException extends Throwable{
    }
}