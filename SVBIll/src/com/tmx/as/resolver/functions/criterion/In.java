package com.tmx.as.resolver.functions.criterion;

import com.tmx.as.resolver.functions.Function;
import com.tmx.as.resolver.FunctionSyntaxException;
import com.tmx.as.base.criterion.Restrictions;

import java.util.Hashtable;
import java.util.Vector;

public class In implements Function {
    private Hashtable params=new Hashtable();
    private Vector values=new Vector();
    public void set(int paramIndex, String paramName, Object paramValue) throws FunctionSyntaxException {
        if(paramName.equalsIgnoreCase("val")){
            values.add(paramValue);
            return;
        }
        params.put(paramName,paramValue);
    }
    public Object returnValue() throws FunctionSyntaxException {
        return Restrictions.in((String)params.get("propertyName"),values);
    }
}
