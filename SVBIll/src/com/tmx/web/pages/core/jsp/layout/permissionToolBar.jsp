<%@ page language="java" pageEncoding="UTF-8" contentType="text/html; charset=utf-8" isELIgnored="false" %>
<!--errorPage="/core/jsp/message/jsperror.jsp"-->
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean" %>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c" %>
<%@ taglib uri="http://tmx.com/taglib/tmx-logic" prefix="tmx_logic" %>
<%@ taglib uri="http://tmx.com/taglib/tmx-controls" prefix="tmx_ctrl" %>
<c:if test="${sessionScope['core.uipermsetup.toolBarPermissionForm']!=null}">
<bean:define id="permissionForm" name="core.uipermsetup.toolBarPermissionForm" type="com.tmx.web.forms.core.uipermsetup.ToolBarPermissionForm"/>
    <c:if test="${permissionForm.active==true}">
        <td style="text-align:left;" colspan="2" class="toolBarPlaceHolder">
            <html:form action="/core/uipermsetup/toolBarPermissionAction">
            <span class="dataLabel">
            <tmx_ctrl:writeEntityLabel
                    attrName="{@name}"
                    entityClassName="com.tmx.as.entities.general.role.Role"/>:
            </span>
            <tmx_ctrl:selectBox name="core.uipermsetup.toolBarPermissionForm" property="roles" permissioned="false"/>
            <tmx_ctrl:button name="core.uipermsetup.toolBarPermissionForm" property="update" permissioned="false"/>
            <tmx_ctrl:button name="core.uipermsetup.toolBarPermissionForm" property="apply" permissioned="false"/>
            <tmx_ctrl:button name="core.uipermsetup.toolBarPermissionForm" property="close" permissioned="false"/>
            <%
                com.tmx.web.forms.core.uipermsetup.ToolBarPermissionForm form = (com.tmx.web.forms.core.uipermsetup.ToolBarPermissionForm) session.getAttribute("core.uipermsetup.toolBarPermissionForm");
                form.setForwardUrl(request.getRequestURL().toString() + "?" + request.getQueryString());
            %>
            </html:form>
        </td>
    </c:if>
</c:if>