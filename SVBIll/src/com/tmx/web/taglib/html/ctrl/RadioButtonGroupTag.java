package com.tmx.web.taglib.html.ctrl;

import com.tmx.web.controls.RadioButtonGroup;

import javax.servlet.jsp.JspException;

/**
 * Render radio button group
 */
public class RadioButtonGroupTag extends AbstractControlTag{
    //CSS styles properties
    private String styleClass = null;

    //------------------------------Properties

    public String getStyleClass() {
        return (styleClass != null) ? styleClass : getDefaultProperty("tag.radioButtonGroup.style");
    }

    public void setStyleClass(String styleClass) {
        this.styleClass = styleClass;
    }

    protected int controlStartTag() throws JspException {
        return 0;  //To change body of implemented methods use File | Settings | File Templates.
    }

    //------------------------------Abstract methods
    protected String renderControlHTML() throws JspException {
        StringBuffer resultHTML = new StringBuffer();
        RadioButtonGroup rbg = getRadioButtonGroup();
        if(!rbg.isBounded())
            return "Data is not bound to RadioButtonGroup '"+getProperty()+"'";

        //Render HTML
        resultHTML.append("\r");
        rbg.reset();
        resultHTML.append("<div class=\""+getStyleClass()+"\">\r");
        while(rbg.hasNext()){
            RadioButtonGroup.Entry entry = rbg.next();
            String attrSelectedHTML = (entry.getKey().equals(rbg.getSelectedEntry().getKey()) ? " checked" : "");
            resultHTML.append("   <input type=\"radio\" name=\""+getProperty() +"."+ RadioButtonGroup.SELECTEDKEY_SUFFIX +"\" value="+entry.getKey()+ " " + attrSelectedHTML + "/>\r");
            resultHTML.append(entry.getValue()+"\r");
            resultHTML.append("   <br/>\r");
        }
        resultHTML.append("</div>\r");
        resultHTML.append("\r");


        return resultHTML.toString();
    }

   private RadioButtonGroup getRadioButtonGroup() throws JspException{
        Object object = getPropertyObject();
        return (object != null && object instanceof RadioButtonGroup) ?
               (RadioButtonGroup)object : null;
   }
}
