package com.tmx.beng.medaccess;

import com.tmx.beng.ussdgate.UssdGateException;

/**
    Medium Access to obtain medium connection
 */
public interface MediumAccess {
    public void init();
    public MediumConnection getConnection() throws GateException ;
}
