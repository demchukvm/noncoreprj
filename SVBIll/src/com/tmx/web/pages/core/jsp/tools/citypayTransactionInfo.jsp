<%@ page import="com.tmx.web.forms.core.tools.GatesBalancesForm" %>
<%@ page import="java.util.HashMap" %>
<%@ page import="java.util.Set" %>
<%@ page import="com.tmx.web.forms.core.tools.CitypayTransactionInfoForm" %>
<%@ taglib prefix="nested" uri="http://jakarta.apache.org/struts/tags-nested" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/tld/tmx-controls.tld" prefix="tmx_ctrl" %>

<%--<html:form action="/citypayTransactionInfoAction">--%>

<table>
    <tr>
        <td align="left" colspan="2">
            № биллинговой транзакции
        </td>
        <td>
            <tmx_ctrl:editBox property="billingNumber" />
        </td>
    </tr>
    <tr>
        <td>
            <tmx_ctrl:button property="getStatus" />
        </td>
        <td>
            citypay_gate : <%= ((CitypayTransactionInfoForm) pageContext.getSession().getAttribute("citypayTransactionInfoForm")).getCitypayStatusInfo() %>
        </td>
    </tr>
    <tr>
        <td>
            <tmx_ctrl:button property="getStatusLifeExc" />
        </td>
        <td>
            citypay_gate_life : <%= ((CitypayTransactionInfoForm) pageContext.getSession().getAttribute("citypayTransactionInfoForm")).getCitypayForLifeExcStatusInfo() %>
        </td>
    </tr>
<%--    <tr>
        <td align="left" colspan="2">Статус City-Pay<br />
        <tmx_ctrl:editBox property="citypayStatusInfo"/></td>
    </tr>--%>
    <%--<tr>
        <td align="center" colspan="2"></td>
    </tr>--%>
</table>

<br />
<br />
<b>Таблица кодов статусов платежей</b>
<table style="text-align:left;" border="1">
    <tr align="center">
        <th>Код</th>
        <th>Статус</th>
        <th>Описание</th>
        <th>Конечный</th>
        <th>Ошибка</th>
    </tr>
    <tr>
        <td>0,1</td>
        <td>Платеж принят</td>
        <td></td>
        <td>Нет</td>
        <td>Нет</td>
    </tr>
        <tr>
        <td>2</td>
        <td>Проверка</td>
        <td></td>
        <td>Нет</td>
        <td>Нет</td>
    </tr>
        <tr>
        <td>4</td>
        <td>Платеж отклонен</td>
        <td>Недостаточно средств у дилера</td>
        <td>Да</td>
        <td>Нет</td>
    </tr>
        <tr>
        <td>5</td>
        <td>Заявка отправлена</td>
        <td></td>
        <td>Нет</td>
        <td>Нет</td>
    </tr>
        <tr>
        <td>7</td>
        <td>Отменен</td>
        <td></td>
        <td>Да</td>
        <td>Нет</td>
    </tr>
        <tr>
        <td>8</td>
        <td>Недостаточно средств</td>
        <td></td>
        <td>Нет</td>
        <td>Нет</td>
    </tr>
        <tr>
        <td>9</td>
        <td>Исполнено</td>
        <td></td>
        <td>Да</td>
        <td>Нет</td>
    </tr>
        <tr>
        <td>98</td>
        <td>Создание транзакции</td>
        <td>Отправлена заявка на создание транзакции, но ответ от провайдера еще не был получен</td>
        <td>Нет</td>
        <td>Нет</td>
    </tr>
        <tr>
        <td>99</td>
        <td>Начата обработка</td>
        <td>Начата обработка платежа, но он ещё не отправ-лен провайдеру</td>
        <td>Нет</td>
        <td>Нет</td>
    </tr>
        <tr>
        <td>100</td>
        <td>Импортирован</td>
        <td>Платеж принят от точки</td>
        <td>Нет</td>
        <td>Нет</td>
    </tr>
        <tr>
        <td>101</td>
        <td>Неверная подпись</td>
        <td>Неверная подпись строки с информацией о платеже</td>
        <td>Да</td>
        <td>Да</td>
    </tr>
        <tr>
        <td>102</td>
        <td>Неверная точка</td>
        <td>Не существует указанной точки платежей</td>
        <td>Да</td>
        <td>Да</td>
    </tr>
        <tr>
        <td>103</td>
        <td>Запрещенный статус точки</td>
        <td>Статус точки не позволяет проведение платежей через неё</td>
        <td>Нет</td>
        <td>Да</td>
    </tr>
        <tr>
        <td>104</td>
        <td>Запрещенный статус карты</td>
        <td>Статус карты не позволяет проведение по ней платежей</td>
        <td>Нет</td>
        <td>Да</td>
    </tr>
        <tr>
        <td>105</td>
        <td>Неверный номер платежного элемента</td>
        <td>Не существует указанного элемента платежей</td>
        <td>Да</td>
        <td>Да</td>
    </tr>
        <tr>
        <td>107</td>
        <td>Запрещенный элемент платежей</td>
        <td>Указанный элемент платежей запрещен</td>
        <td>Нет</td>
        <td>Да</td>
    </tr>
        <tr>
        <td>108</td>
        <td>Дублирование номера чека</td>
        <td>Дублирование номера чека в пределах одной точки</td>
        <td>Да</td>
        <td>Да</td>
    </tr>
        <tr>
        <td>110</td>
        <td>Недостаточно средств</td>
        <td>Недостаточно средств на счету дилера для проведения операции</td>
        <td>Нет</td>
        <td>Да</td>
    </tr>
        <tr>
        <td>112</td>
        <td>Неверная сумма платежа</td>
        <td>Недопустимое значение суммы платежа</td>
        <td>Да</td>
        <td>Да</td>
    </tr>
        <tr>
        <td>113</td>
        <td>Неверные реквизиты получателя</td>
        <td>Не указаны реквизиты получателя платежа (тел.номер и т.п.)</td>
        <td>Да</td>
        <td>Да</td>
    </tr>
        <tr>
        <td>120</td>
        <td>Неверные платежные данные</td>
        <td>Ошибка в типе передаваемых данных (Неверный формат даты, суммы, номера чека)</td>
        <td>Да</td>
        <td>Да</td>
    </tr>
        <tr>
        <td>121</td>
        <td>Неверный номер платежного элемента</td>
        <td>Неверное значение PayElementID</td>
        <td>Да</td>
        <td>Да</td>
    </tr>
        <tr>
        <td>123</td>
        <td>Платеж не найден</td>
        <td>Отсутствует информация о платеже с указанным номером чека</td>
        <td>Да</td>
        <td>Да</td>
    </tr>
        <tr>
        <td>2000</td>
        <td>Ошибка на стороне сервера</td>
        <td>Ошибка на стороне платежного сервера CityPAY. Платеж не создан.</td>
        <td>Да</td>
        <td>Да</td>
    </tr>
</table>
<br/>
<br/>
<b>Таблица кодов ошибок, возникающих при приеме платежей</b>
<table style="text-align:left;" border="1">
     <tr align="center">
        <th>Код</th>
        <th>Сообщение</th>
        <th>Описание</th>
    </tr>
    <tr>
        <td>210</td>
        <td>Incorrect KeyWord</td>
        <td>В запросе указан неверный KeyWord</td>
    </tr>
    <tr>
        <td>211</td>
        <td>Incorrect Point</td>
        <td>Запрос принят от несуществующей точки</td>
    </tr>
    <tr>
        <td>212</td>
        <td>Incorrect XML</td>
        <td>Ошибка во входящем XML</td>
    </tr>
</table>

