package com.tmx.as.resolver.functions.criterion;

import com.tmx.as.resolver.functions.Function;
import com.tmx.as.resolver.FunctionSyntaxException;
import com.tmx.as.base.criterion.Restrictions;

import java.util.Hashtable;
import java.util.Vector;

public class AllEq implements Function {
    private Hashtable params=new Hashtable();
    private Vector keys=new Vector();
    private Vector vals=new Vector();
    public void set(int paramIndex, String paramName, Object paramValue) throws FunctionSyntaxException {
        if(paramName.equalsIgnoreCase("key")){
            keys.add(paramValue);return;
        }else if(paramName.equalsIgnoreCase("val")){
            vals.add(paramValue);return;
        }
        params.put(paramName,paramValue);
    }

    public Object returnValue() throws FunctionSyntaxException {
        Hashtable map=new Hashtable();
        for (int i = 0; i < keys.size(); i++) {
            map.put(keys.get(i),vals.get(i));
        }
        return Restrictions.allEq(map);
    }
}
