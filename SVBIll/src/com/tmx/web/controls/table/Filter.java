package com.tmx.web.controls.table;

import java.util.*;

/**
 * see central method in this class - getAppliedParameters()
 */
public class Filter {
    private Map parameters = new HashMap();
    private Map parameterIds = new HashMap();
    private int idx = 0;
    private FilterSet parent;
    private String name;
    private int id;
    private FilterEnabler filterEnabler;

    public Filter(){
    }

    public Filter(String name){
        this.name = name;
    }

    public Filter(String name, Parameter parameter){
        this.name = name;
        addParameter(parameter);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public FilterSet getParent() {
        return parent;
    }

    public void setParent(FilterSet parent) {
        this.parent = parent;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void addParameter(String parameterName, Parameter parameter){
        parameter.setParent(this);
        parameter.setName(parameterName);
        parameter.setId(idx);
        parameters.put(parameterName, parameter);      //<-----------| TODO:(A.N.) error part!!!
        parameterIds.put(new Integer(idx++), parameterName);//<------|
    }

    public void addParameter(Parameter parameter){
        parameter.setParent(this);
        parameter.setId(idx);
        parameters.put(parameter.getName(), parameter);
        parameterIds.put(new Integer(idx++), parameter.getName());
    }


    public Parameter getParameter(String parameterName){
        if(parameters.containsKey(parameterName)){
            return (Parameter)parameters.get(parameterName);
        } else
            return null;
    }

    public Parameter getParameterByIndex(int index){
        Integer id = new Integer(index);
        if(parameterIds.containsKey(id)){
            String parameterName = (String)parameterIds.get(id);
            return getParameter(parameterName);
        }
        return null;
    }

    public void setParameterByIndex(int index, Parameter parameter){
        Integer id = new Integer(index);
        if(parameterIds.containsKey(id)){
            addParameter((String)parameterIds.get(id), parameter);
        }
    }

    public String getPath(){
        StringBuffer sb = new StringBuffer();
        if(parent != null)
            sb.append(parent.getPath()).append(".");
//        sb.append("filter[").append(id).append("]");//by idx
        sb.append("filter(").append(name).append(")");//by name        
        return sb.toString();
    }

    /**
     * central method in this class!!!
     * @return example: filter(name=xxx,parameter(name=active,value=true,type=Boolean))
     * @see Parameter , Table
     */
    public String getAppliedParameters(){
        StringBuffer sb = new StringBuffer();
        Iterator iter = parameters.values().iterator();
        List params = new Vector();//list of a string parameters

        if(parameters.size() != 0){//if filter with parameters

            //if filter with parameters, and filterEnabler is absent or filter enabled
            if( (filterEnabler == null) || (filterEnabler.isFilterEnabled()) ){

                //filling a params - list of a string parameters
                while(iter.hasNext()){
                    Parameter parameter = (Parameter)iter.next();
                    String paramStr = parameter.getAppliedParameters();
                    if(paramStr != null && !"".equals(paramStr)){
                        params.add(paramStr);
                    }
                }

                if(params.size() > 0){//if list of the parameters isn't empty
                    sb.append("filter(name=").append(name).append(",");
                    iter = params.iterator();
                    while(iter.hasNext()){
                        sb.append((String)iter.next());
                        if(iter.hasNext()){
                            sb.append(",");
                        }
                    }
                    sb.append(")");
                }
            }
            else{  //if filter with parameters, and filter isn't enabled
                //do nothing
            }
        }
        else if( (filterEnabler != null) && (filterEnabler.isFilterEnabled()) ){//if filter without parameters but enabled
            //creating string "filter(name=xxx)" without parameters
            sb.append("filter(name=").append(name);
            sb.append(")");
        }

        return sb.toString();
    }

    public Set getParameterNames(){
        return parameters.keySet();
    }

    public FilterEnabler getFilterEnabler() {
        return filterEnabler;
    }

    public void setFilterEnabler(FilterEnabler filterEnabler) {
        this.filterEnabler = filterEnabler;
    }
}
