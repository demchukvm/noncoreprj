-- RFC: 0000625
-- Note: Remove 0.10 UAH transaction cost for Seller and Terminals tariffs for all operators.

update ap_actual_func_param ap set ap.ap_value = 0 where
ap.AP_ACTUALFUNCPARAMID = 5; 

update ap_actual_func_param ap set ap.ap_value = 0 where
ap.AP_ACTUALFUNCPARAMID = 6; 