<%@ page import="com.tmx.web.forms.core.tools.RuleTerminalsForm" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="com.tmx.web.controls.table.ControlManagerTable" %>
<%@ taglib prefix="nested" uri="http://jakarta.apache.org/struts/tags-nested" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/tld/tmx-controls.tld" prefix="tmx_ctrl" %>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>

<html:form action="/RuleTerminalsAction">

<table border="0">
	<tr>
		<td valign="top">

			<table border="0" cellspacing="0" cellpadding="10">
				<tr>
					<td class="dataLabel" style="border-left: black 1px dotted;border-right: black 1px dotted;border-top: black 1px dotted;">
						<bean:message bundle="core.labels" key="ruleTerminals.label.terminal"/>
						<tmx_ctrl:editBox property="ebCheckTerminal"/>
						<tmx_ctrl:button property="bCheck"/>
						<tmx_ctrl:button property="bClear"/>
					</td>
				</tr>
				<tr>
					<td style="border-left: black 1px dotted;border-right: black 1px dotted;border-bottom: black 1px dotted;">
						<table border="0" cellspacing="0" cellpadding="0" style="border:black 1px solid;">
							<tr>
								<th class="column_header_cell" width="90" style="border-right:black 1px solid;">BeelineExc</th>
								<th class="column_header_cell" width="90" style="border-right:black 1px solid;">KyivstarExc</th>
								<th class="column_header_cell" width="90" style="border-right:black 1px solid;">LifeExc</th>
								<th class="column_header_cell" width="90">MTSExc</th>
							</tr>
							<%
								boolean beeline = ((RuleTerminalsForm) pageContext.getSession().getAttribute("RuleTerminalsForm")).isBeeline();
								boolean kyivstar = ((RuleTerminalsForm) pageContext.getSession().getAttribute("RuleTerminalsForm")).isKyivstar();
								boolean life = ((RuleTerminalsForm) pageContext.getSession().getAttribute("RuleTerminalsForm")).isLife();
								boolean mts = ((RuleTerminalsForm) pageContext.getSession().getAttribute("RuleTerminalsForm")).isMts();
							%>
							<tr>
								<td height="15" align="center" style="border-top:black 1px solid;border-right:black 1px solid;"><%=beeline==true?"+":"&nbsp;"%></td>
								<td align="center" style="border-top:black 1px solid;border-right:black 1px solid;"><%=kyivstar==true?"+":"&nbsp;"%></td>
								<td align="center" style="border-top:black 1px solid;border-right:black 1px solid;"><%=life==true?"+":"&nbsp;"%></td>
								<td align="center" style="border-top:black 1px solid;"><%=mts==true?"+":"&nbsp;"%></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td height="15"/>
				</tr>
				<tr>
					<td colspan="2" class="dataLabel" style="border-left: black 1px dotted;border-right: black 1px dotted;border-top: black 1px dotted;">
						<bean:message bundle="core.labels" key="ruleTerminals.label.table"/>
						<tmx_ctrl:selectBox property="sbExclusiveTable"/>
						<tmx_ctrl:button property="bShow"/>
					</td>
				</tr>
				<tr>
					<!--<td height="5" style="border-left: black 1px dotted;border-right: black 1px dotted;"/>-->
				</tr>
				<tr>
					<td colspan="2" style="border-left: black 1px dotted;border-right: black 1px dotted;border-bottom: black 1px dotted;">
						<table cellspacing='0' cellpadding='0' border="0">
						<%
							String chosenTable = ((RuleTerminalsForm) pageContext.getSession().getAttribute("RuleTerminalsForm")).getChosenTable();
							if(!chosenTable.equals(""))
							{
						%>
							<tr>
								<th colspan="3">
									<% String tableName = chosenTable.replace("Exclusive", "");%>
									<h1><%=tableName%></h1>
								</th>
								<th>
									<nested:define id="tableSize" property="exclusiveTable.resultSize" type="java.lang.Integer"/>
									<c:if test="${tableSize > 0}">
										<b><bean:message bundle="core.labels" key="ruleTerminals.label.total"/> <%=tableSize %></b>
									</c:if>
								</th>
							</tr>
						<% } %>
							<tr>
								<td colspan="4" height="5"/>
							</tr>
							<tr style="background-color: #EEEEEE;">
								<td colspan="3" align="center">
									<tmx_ctrl:editBox property="ebAddTerminal"/>
									<tmx_ctrl:button property="bAddTerminal"/>
								</td>
								<td align="center">
									<tmx_ctrl:button property="bRemoveTerminals"/>
								</td>
							</tr>
							<tr>
								<td colspan="4" height="5"/>
							</tr>
							<tr>
								<th class="column_header_cell" width="20" style="border-left:black 1px solid;border-top:black 1px solid;border-right:black 1px solid;border-bottom:black 1px solid;">
								&nbsp;
								</th>
								<th class="column_header_cell" width="110" style="border-top:black 1px solid;border-right:black 1px solid;border-bottom:black 1px solid;">
									<tmx_ctrl:writeEntityLabel  attrName="terminal"
																entityClassName="com.tmx.as.entities.bill.exclusive.Exclusives"/>
								</th>
								<th class="column_header_cell" width="110" style="border-top:black 1px solid;border-right:black 1px solid;border-bottom:black 1px solid;">
									<tmx_ctrl:writeEntityLabel  attrName="addDate"
																entityClassName="com.tmx.as.entities.bill.exclusive.Exclusives"/>
								</th>
								<th class="column_header_cell" width="110" style="border-top:black 1px solid;border-right:black 1px solid;border-bottom:black 1px solid;">
									<tmx_ctrl:writeEntityLabel  attrName="who"
																entityClassName="com.tmx.as.entities.bill.exclusive.Exclusives"/>
								</th>
							</tr>
							<%
								if(!chosenTable.equals(""))
								{
									if(chosenTable.equals("ExclusiveBeeline"))
									{
								%>
							<nested:iterate id="exclusiveTerminals" property="exclusiveTable.data" type="com.tmx.as.entities.bill.exclusive.ExclusiveBeeline">
							<tr>
								<td class='column_data_cell' style="border-left:black 1px solid;border-right:black 1px solid;border-bottom:black 1px solid;">
									<input type="radio" name="chosenTerminalId" value="<c:out value="${exclusiveTerminals.excBeelineId}"/>">
								</td>
								<td class='column_data_cell' style="border-right:black 1px solid;border-bottom:black 1px solid;">
									<c:if test="${exclusiveTerminals.terminal != null}">
										<c:out value="${exclusiveTerminals.terminal}" />
									</c:if>
								</td>
								<td class='column_data_cell' style="border-right:black 1px solid;border-bottom:black 1px solid;">
									<c:if test="${exclusiveTerminals.addDate != null}">
										<fmt:formatDate value="${exclusiveTerminals.addDate}" pattern="dd.MM.yyyy"/>
									</c:if>
									<c:if test="${exclusiveTerminals.addDate == null}">
										&nbsp;
									</c:if>
								</td>
								<td class='column_data_cell' style="border-right:black 1px solid;border-bottom:black 1px solid;">
									<c:if test="${exclusiveTerminals.addUser != null}">
										<c:out value="${exclusiveTerminals.addUser.lastName}" />
										<c:out value="${exclusiveTerminals.addUser.firstName}" />
									</c:if>
									<c:if test="${exclusiveTerminals.addUser == null}">
										&nbsp;
									</c:if>
								</td>
							</tr>
							</nested:iterate>
								<%
									}
									else if(chosenTable.equals("ExclusiveKyivstar"))
									{
								%>
							<nested:iterate id="exclusiveTerminals" property="exclusiveTable.data" type="com.tmx.as.entities.bill.exclusive.ExclusiveKyivstar">
							<tr>
								<td class='column_data_cell' style="border-left:black 1px solid;border-right:black 1px solid;border-bottom:black 1px solid;">
									<input type="radio" name="chosenTerminalId" value="<c:out value="${exclusiveTerminals.excKyivstarId}"/>">
								</td>
								<td class='column_data_cell' style="border-right:black 1px solid;border-bottom:black 1px solid;">
									<c:if test="${exclusiveTerminals.terminal != null}">
										<c:out value="${exclusiveTerminals.terminal}" />
									</c:if>
								</td>
								<td class='column_data_cell' style="border-right:black 1px solid;border-bottom:black 1px solid;">
									<c:if test="${exclusiveTerminals.addDate != null}">
										<fmt:formatDate value="${exclusiveTerminals.addDate}" pattern="dd.MM.yyyy"/>
									</c:if>
									<c:if test="${exclusiveTerminals.addDate == null}">
										&nbsp;
									</c:if>
								</td>
								<td class='column_data_cell' style="border-right:black 1px solid;border-bottom:black 1px solid;">
									<c:if test="${exclusiveTerminals.addUser != null}">
										<c:out value="${exclusiveTerminals.addUser.lastName}" />
										<c:out value="${exclusiveTerminals.addUser.firstName}" />
									</c:if>
									<c:if test="${exclusiveTerminals.addUser == null}">
										&nbsp;
									</c:if>
								</td>
							</tr>
							</nested:iterate>
								<%
									}
									else if(chosenTable.equals("ExclusiveLife"))
									{
								%>
							<nested:iterate id="exclusiveTerminals" property="exclusiveTable.data" type="com.tmx.as.entities.bill.exclusive.ExclusiveLife">
							<tr>
								<td class='column_data_cell' style="border-left:black 1px solid;border-right:black 1px solid;border-bottom:black 1px solid;">
									<input type="radio" name="chosenTerminalId" value="<c:out value="${exclusiveTerminals.excLifeId}"/>">
								</td>
								<td class='column_data_cell' style="border-right:black 1px solid;border-bottom:black 1px solid;">
									<c:if test="${exclusiveTerminals.terminal != null}">
										<c:out value="${exclusiveTerminals.terminal}" />
									</c:if>
								</td>
								<td class='column_data_cell' style="border-right:black 1px solid;border-bottom:black 1px solid;">
									<c:if test="${exclusiveTerminals.addDate != null}">
										<fmt:formatDate value="${exclusiveTerminals.addDate}" pattern="dd.MM.yyyy"/>
									</c:if>
									<c:if test="${exclusiveTerminals.addDate == null}">
										&nbsp;
									</c:if>
								</td>
								<td class='column_data_cell' style="border-right:black 1px solid;border-bottom:black 1px solid;">
									<c:if test="${exclusiveTerminals.addUser != null}">
										<c:out value="${exclusiveTerminals.addUser.lastName}" />
										<c:out value="${exclusiveTerminals.addUser.firstName}" />
									</c:if>
									<c:if test="${exclusiveTerminals.addUser == null}">
										&nbsp;
									</c:if>
								</td>
							</tr>
							</nested:iterate>
								<%
									}
									else if(chosenTable.equals("ExclusiveMTS"))
									{
								%>
							<nested:iterate id="exclusiveTerminals" property="exclusiveTable.data" type="com.tmx.as.entities.bill.exclusive.ExclusiveMTS">
							<tr>
								<td class='column_data_cell' style="border-left:black 1px solid;border-right:black 1px solid;border-bottom:black 1px solid;">
									<input type="radio" name="chosenTerminalId" value="<c:out value="${exclusiveTerminals.excMTSId}"/>">
								</td>
								<td class='column_data_cell' style="border-right:black 1px solid;border-bottom:black 1px solid;">
									<c:if test="${exclusiveTerminals.terminal != null}">
										<c:out value="${exclusiveTerminals.terminal}" />
									</c:if>
								</td>
								<td class='column_data_cell' style="border-right:black 1px solid;border-bottom:black 1px solid;">
									<c:if test="${exclusiveTerminals.addDate != null}">
										<fmt:formatDate value="${exclusiveTerminals.addDate}" pattern="dd.MM.yyyy"/>
									</c:if>
									<c:if test="${exclusiveTerminals.addDate == null}">
										&nbsp;
									</c:if>
								</td>
								<td class='column_data_cell' style="border-right:black 1px solid;border-bottom:black 1px solid;">
									<c:if test="${exclusiveTerminals.addUser != null}">
										<c:out value="${exclusiveTerminals.addUser.lastName}" />
										<c:out value="${exclusiveTerminals.addUser.firstName}" />
									</c:if>
									<c:if test="${exclusiveTerminals.addUser == null}">
										&nbsp;
									</c:if>
								</td>
							</tr>
							</nested:iterate>
								<%
									}

								%>

							<%--<nested:define id="tableSize" property="exclusiveTable.resultSize" type="java.lang.Integer"/>--%>
                            <c:if test="${tableSize <= 0}">
							<tr>
								<td class='column_data_cell' colspan="4" align="center">
									<bean:message bundle="core.labels" key="label.emptyTable" />
								</td>
							</tr>
                            </c:if>
							<%--<c:if test="${tableSize > 0}">
							<tr>
								<td colspan="4" align="right">
									<b><bean:message bundle="core.labels" key="ruleTerminals.label.total"/> <%=tableSize %></b>
								</td>
							</tr>
                            </c:if>--%>
							<%
								}
							%>
							<tr>
								<td colspan="4" height="5"/>
							</tr>

						</table>
					</td>
				</tr>
			</table>
		</td>
        <td width="100"/>
        <td valign="top">
            <table border="0" cellspacing="0" cellpadding="10" style="border:black 1px dotted;">
                <tr>
                    <td>
						<tmx_ctrl:selectBox property="sbSellers"/>
						<tmx_ctrl:button property="bGetTerminals"/>
					</td>
                </tr>
                <tr>
                    <td>
                        <table border="0" cellspacing='0' cellpadding='0' width="100%">
                            <tr>
                                <th class="column_header_cell" style="border-top:black 1px solid;border-bottom:black 1px solid;border-left:black 1px solid;border-right:black 1px solid;">
                                    <tmx_ctrl:writeEntityLabel  attrName="{@name}"
																entityClassName="com.tmx.as.entities.bill.terminal.Terminal"/>
                                </th>
                                <th class="column_header_cell" style="border-top:black 1px solid;border-bottom:black 1px solid;border-right:black 1px solid;">
                                    <tmx_ctrl:writeEntityLabel  attrName="{@name}"
																entityClassName="com.tmx.as.entities.bill.trade_putlet.TradePutlet"/>
                                </th>
                            </tr>
                            <nested:iterate id="tradePoints" property="tradePointsTable.data" type="Object[]">
                            <tr>
                                <td class='column_data_cell' style="border-left:black 1px solid;border-bottom:black 1px solid;border-right:black 1px solid;">
                                    <%= tradePoints[0] %>
                                </td>
                                <td class='column_data_cell' style="border-bottom:black 1px solid;border-right:black 1px solid;">
                                    <% if(tradePoints[1] != null) { %>
                                        <%= tradePoints[1] %>
                                    <% } else  { %>
                                    <bean:message bundle="core.labels" key="ruleTerminals.label.notSpecified"/>
                                    <% } %>
                                </td>
                            </tr>
                            </nested:iterate>
                            <tr>
                                <td colspan="2" align="right">
                                    <nested:define id="tableSize1" property="tradePointsTable.resultSize" type="java.lang.Integer"/>
                                    <b><bean:message bundle="core.labels" key="ruleTerminals.label.total"/> <%=tableSize1 %></b>
                                </td>
                            </tr>
                       </table>
                    </td>
                </tr>
            </table>
        </td>





    </tr>
</table>



</html:form>