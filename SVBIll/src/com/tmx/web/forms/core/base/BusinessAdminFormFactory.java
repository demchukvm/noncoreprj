package com.tmx.web.forms.core.base;

import com.tmx.web.forms.core.dictionary.terminal.InfoTerminalForm;
import com.tmx.web.forms.core.dictionary.user.UserInfoForm;
import com.tmx.web.forms.core.dictionary.seller.ManageSellersForm;
import com.tmx.web.forms.core.dictionary.seller.InfoSellerForm;
import com.tmx.web.forms.core.dictionary.terminal.ManageTerminalsForm;
import com.tmx.web.forms.core.transaction.seller_balance_transaction.ManageSellerBalanceTransactionsForm;
import com.tmx.web.forms.core.transaction.terminal_balance_transaction.ManageTerminalBalanceTransactionsForm;
import com.tmx.web.forms.core.transaction.client_transaction.ManageClientTransactionsForm;
import com.tmx.web.forms.core.balance.seller_balance.ManageSellerBalancesForm;
import com.tmx.web.forms.core.balance.terminal_balance.ManageTerminalBalancesForm;
import com.tmx.web.forms.core.balance.terminal_balance.BrowseTerminalsForm;
import com.tmx.web.forms.core.balance.AbstractInfoBalanceForm;
import com.tmx.web.forms.core.tariff.seller_tariff.ManageSellerTariffsForm;
import com.tmx.web.forms.core.tariff.seller_tariff.ManageSimpleSellerTariffsForm;
import com.tmx.web.forms.core.tariff.seller_tariff.InfoSellerTariffForm;
import com.tmx.web.forms.core.tariff.terminal_tariff.ManageTerminalTariffsForm;
import com.tmx.web.forms.core.restriction.seller_restriction.ManageSellerRestrictionsForm;
import com.tmx.web.forms.core.restriction.seller_restriction.InfoSellerRestrictionForm;
import com.tmx.web.forms.core.restriction.terminal_restriction.ManageTerminalRestrictionsForm;
import com.tmx.web.forms.core.reports.ReportPaymentForm;
import com.tmx.web.forms.core.reports.ReportVouchersForm;
import com.tmx.web.forms.core.reports.ReportNetworksForm;
import com.tmx.web.controls.ScrollBox;
import com.tmx.web.controls.BrowseBox;
import com.tmx.web.controls.CriterionFactory;
import com.tmx.as.exceptions.DatabaseException;
import com.tmx.as.entities.bill.seller.Seller;
import com.tmx.as.entities.bill.balance.SellerBalance;
import com.tmx.as.entities.general.user.User;
import com.tmx.as.blogic.SellerManager;
import com.tmx.as.blogic.balance.SellerBalanceManager;
import com.tmx.as.base.criterion.Restrictions;
import com.tmx.as.base.criterion.CriterionWrapper;
import com.tmx.as.base.EntityManager;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Collections;


public class BusinessAdminFormFactory extends AbstractFormFactory{

    public void setUpForm(UserInfoForm userInfoForm) throws DatabaseException {
        
    }

    public void setUpForm(ManageSellersForm manageSellersForm) throws DatabaseException {

        manageSellersForm.getSellersTable().loadQuery( new SellerManager().getSellersInSellerQuery(loggedInUser.getSellerOwner().getSellerId()) );
        
        manageSellersForm.getScrollBoxParentSeller().bind(new SellerManager().getNestedSellersInSeller(loggedInUser));
        manageSellersForm.getScrollBoxParentSeller().setNullable(true);
        manageSellersForm.getScrollBoxParentSeller().setSelectedKey(ScrollBox.NULL_KEY);
    }

    /**
     * if null key selected, then include "in criterion" 
     * @param form
     * @throws DatabaseException
     */
    public void setUpForm(ManageSellerBalanceTransactionsForm form) throws DatabaseException {
        final List<Seller> sellers = new SellerManager().getNestedSellersInSeller(loggedInUser.getSellerOwner(), true, true);
        final List<SellerBalance> sellerBalances = new ArrayList<SellerBalance>();
        for(Seller seller : sellers) {
            sellerBalances.add(seller.getPrimaryBalance());
        }

        Collections.sort(sellerBalances, new Comparator<SellerBalance>()
        {
          public int compare(SellerBalance sb1, SellerBalance sb2) {
            return sb1.getCode().compareTo(sb2.getCode());
          }
        });

        final ScrollBox<Seller> scrollBoxSellers = (ScrollBox<Seller>) form.getSellerTransactionsTable().getParameterControl("by_seller", "seller");
        scrollBoxSellers.setNullable(true);
        form.getSellerTransactionsTable().setCriterionFactory(new CriterionFactory() {
            public CriterionWrapper createCriterion() {
                if (scrollBoxSellers.isNullKeySelected())
                    return Restrictions.in("seller", sellers);
                return null;
            }
        });
        Collections.sort(sellers);
        scrollBoxSellers.bind(sellers);

        final ScrollBox<SellerBalance> scrollBoxSellerBalance = (ScrollBox<SellerBalance>) form.getSellerTransactionsTable().getParameterControl("by_seller_balance", "sellerBalance");
        scrollBoxSellerBalance.setNullable(true);

        form.getSellerTransactionsTable().setCriterionFactory(new CriterionFactory() {
            public CriterionWrapper createCriterion() {
                if (scrollBoxSellerBalance.getSelectedKey() == null || scrollBoxSellerBalance.isNullKeySelected()) {
                    return Restrictions.in("sellerBalance", sellerBalances);
                } else {
                    SellerBalance sellerBalance = null;
                    //retrive needed balance
                    try {
                        sellerBalance = (SellerBalance)(new SellerBalanceManager()).getBalance(new Long(scrollBoxSellerBalance.getSelectedKey()));
                    } catch (DatabaseException e) {e.printStackTrace();}

                    return Restrictions.eq("sellerBalance", sellerBalance);
                }
            }
        });

        scrollBoxSellerBalance.bind(sellerBalances);
    }

    public void setUpForm(ManageTerminalBalanceTransactionsForm form) throws DatabaseException {
        final BrowseBox browseBox = (BrowseBox) form.getTerminalTransactionsTable().getParameterControl("by_terminal_id", "terminal_id");
        if (!browseBox.isUpdated()) {//if !browseBox.isUpdated() is absent then defaultAction always set browseBox key in null
            browseBox.setKey(null);
        }
        final List<Seller> sellers = new SellerManager().getNestedSellersInSeller(loggedInUser.getSellerOwner(), true);
        form.getTerminalTransactionsTable().setCriterionFactory(new CriterionFactory() {
            public CriterionWrapper createCriterion() {
                if ("".equals(browseBox.getKey()) || browseBox.getKey() == null || "0".equals(browseBox.getKey()))
                    return Restrictions.in("terminal.sellerOwner", sellers);
                return null;
            }
        });
    }

    public void setUpForm(ManageClientTransactionsForm form) throws DatabaseException {
        final ManageClientTransactionsForm manageClientTransactionsForm = form;



        final ScrollBox<Seller> scrollBox = (ScrollBox<Seller>) form.getClientTransactionsTable().getParameterControl("by_seller", "seller");
        final List<Seller> sellers = new SellerManager().getNestedSellersInSeller(loggedInUser.getSellerOwner(), true);
        scrollBox.setNullable(true);
        CriterionFactory criterionFactory = new CriterionFactory() {

            public CriterionWrapper createCriterion() {
                CriterionWrapper resultCriterion = Restrictions.isNotNull("tansactionId");//redunant

                if (manageClientTransactionsForm.getEditBoxAccountNumber().getValue() != null && !manageClientTransactionsForm.getEditBoxAccountNumber().getValue().equals("")) {
                    resultCriterion = Restrictions.and(resultCriterion,
                            Restrictions.ilike("accountNumber", "%" + manageClientTransactionsForm.getEditBoxAccountNumber().getValue() + "%"));
                }

                if (manageClientTransactionsForm.getEditBoxTerminalSerialNumber().getValue() != null && !manageClientTransactionsForm.getEditBoxTerminalSerialNumber().getValue().equals("")) {
                    resultCriterion = Restrictions.and(resultCriterion,
                            Restrictions.ilike("terminal.serialNumber", "%" + manageClientTransactionsForm.getEditBoxTerminalSerialNumber().getValue() + "%"));
                }

                if (manageClientTransactionsForm.getEditBoxVoucherCode().getValue() != null && !manageClientTransactionsForm.getEditBoxVoucherCode().getValue().equals("")) {
                    resultCriterion = Restrictions.and(resultCriterion,
                            Restrictions.ilike("voucher.code", "%" + manageClientTransactionsForm.getEditBoxVoucherCode().getValue() + "%"));
                }

                if (!manageClientTransactionsForm.getScrollBoxTransTypes().isNullKeySelected()) {
                    resultCriterion = Restrictions.and(resultCriterion,
                            Restrictions.ilike("type.code", manageClientTransactionsForm.getScrollBoxTransTypes().getSelectedKey()));
                }

                 if (!manageClientTransactionsForm.getScrollBoxTerminalTypes().isNullKeySelected()) {
                    resultCriterion = Restrictions.and(resultCriterion,
                            Restrictions.eq("terminal.equipmentType.equipmentTypeId", new Long(manageClientTransactionsForm.getScrollBoxTerminalTypes().getSelectedKey())));
                }

                if (!manageClientTransactionsForm.getScrollBoxStatuses().isNullKeySelected()) {//by status
                    resultCriterion = Restrictions.and(resultCriterion,
                            Restrictions.eq("status", manageClientTransactionsForm.getScrollBoxStatuses().getSelectedKey()));
                    //manageClientTransactionsForm.getChekBoxErrorTransactions().setSelected(false);
                    manageClientTransactionsForm.setChekBoxErrorTransactions(false);
                } else {
                    //if (manageClientTransactionsForm.getChekBoxErrorTransactions().isSelected()) {// all error transactions
                    manageClientTransactionsForm.set_chekBoxErrorTransactions(manageClientTransactionsForm.isChekBoxErrorTransactions());
                    if (manageClientTransactionsForm.isChekBoxErrorTransactions() == true) {
                        resultCriterion = Restrictions.and(resultCriterion,
                            Restrictions.not(Restrictions.eq("status", "0")));
                        manageClientTransactionsForm.setChekBoxErrorTransactions(false);
                    }
                }

                if (scrollBox.isNullKeySelected()) {
                    resultCriterion = Restrictions.and(resultCriterion,
                                    Restrictions.in("seller", sellers));
                }

                return resultCriterion;
            }
        };
        manageClientTransactionsForm.getClientTransactionsTable().setCriterionFactory(criterionFactory);
        scrollBox.bind(sellers);
    }

    public void setUpForm(ManageSellerBalancesForm form) throws DatabaseException {
        final ScrollBox<Seller> scrollBox = (ScrollBox<Seller>) form.getBalancesTable().getParameterControl("by_seller_id", "seller_id");
        final List<Seller> sellers = new SellerManager().getNestedSellersInSeller(loggedInUser.getSellerOwner(), true);
        scrollBox.setNullable(true);
        form.getBalancesTable().setCriterionFactory(new CriterionFactory() {
            public CriterionWrapper createCriterion() {
                if (scrollBox.isNullKeySelected())
                    return Restrictions.in("seller", sellers);
                return null;
            }
        });
        scrollBox.bind(sellers);
    }

    public void setUpForm(ManageTerminalBalancesForm form, User user) throws DatabaseException {
        final BrowseBox browseBox = (BrowseBox) form.getBalancesTable().getParameterControl("by_terminal_id", "terminal_id");
        if (browseBox != null && !browseBox.isUpdated()) {//if !browseBox.isUpdated() is absent then defaultAction always set browseBox key in null
            browseBox.setKey(null);
        }
        SellerManager sm = new SellerManager();
        final List<Seller> sellers = sm.getNestedSellersInSeller(loggedInUser.getSellerOwner(), true);

        ScrollBox scrollBox = form.getScrollBoxSellerOwner();
        scrollBox.bind(sellers);
        scrollBox.setNullable(true);

        Seller seller = null;
        if (form.getScrollBoxSellerOwner().getSelectedKey() != null) {//for any cases
            if (!form.getScrollBoxSellerOwner().isNullKeySelected()) {
                Long sellerId = new Long(form.getScrollBoxSellerOwner().getSelectedKey());
                seller = (Seller) (new EntityManager()).RETRIEVE(Seller.class, sellerId);
            }
        }
        //final variables for anonim class
        final Seller finalSeller = seller;
        final ManageTerminalBalancesForm finalForm = form;
        CriterionFactory criterionFactory = new CriterionFactory() {

            public CriterionWrapper createCriterion() {
                //initialize criterion wrapper
                CriterionWrapper resultCriterion = Restrictions.isNotNull("balanceId");

                 //add restrictions
                if (finalForm.getEditBoxBalanceName().getValue() != null && !finalForm.getEditBoxBalanceName().getValue().equals("")) {
                    resultCriterion = Restrictions.and(resultCriterion,
                            Restrictions.ilike("name", "%" + finalForm.getEditBoxBalanceName().getValue() + "%"));
                }
                if (finalForm.getEditBoxBalanceCode().getValue() != null && !finalForm.getEditBoxBalanceCode().getValue().equals("")) {
                    resultCriterion = Restrictions.and(resultCriterion,
                            Restrictions.ilike("code", "%" + finalForm.getEditBoxBalanceCode().getValue() + "%"));
                }
                if (finalSeller != null) {
                        resultCriterion = Restrictions.and(resultCriterion,
                                                        Restrictions.eq("terminal.sellerOwner", finalSeller));
                    }  else {
                        resultCriterion = Restrictions.and(resultCriterion,
                                Restrictions.in("terminal.sellerOwner", sellers));                        
                    }
                return resultCriterion;
            }
        };

//
        form.getBalancesTable().setCriterionFactory(criterionFactory);
    }

    public void setUpForm(ManageSellerTariffsForm form) throws DatabaseException {
        if(form.getTariffsTable() != null){
            ScrollBox scrollBox = (ScrollBox) form.getTariffsTable().getParameterControl("by_seller_id", "seller_id");
            if(scrollBox != null){// && !scrollBox.isUpdated()){
                scrollBox.setNullable(true);
                scrollBox.bind(new SellerManager().getNestedSellersInSeller(loggedInUser.getSellerOwner(), false));
            }
        }
    }

    public void setUpForm(ManageTerminalTariffsForm form) throws DatabaseException {
        final BrowseBox browseBox = (BrowseBox) form.getTariffsTable().getParameterControl("by_terminal_id", "terminal_id");
        if (!browseBox.isUpdated()) {//if !browseBox.isUpdated() is absent then defaultAction always set browseBox key in null
            browseBox.setKey(null);
        }
        final List<Seller> sellers = new SellerManager().getNestedSellersInSeller(loggedInUser.getSellerOwner(), true);
        form.getTariffsTable().setCriterionFactory(new CriterionFactory() {
            public CriterionWrapper createCriterion() {
                if ("".equals(browseBox.getKey()) || browseBox.getKey() == null)
                    return Restrictions.in("terminal.sellerOwner", sellers);
                return null;
            }
        });
    }

    public void setUpForm(ManageSellerRestrictionsForm form) throws DatabaseException {
        form.getOnDeleteButton().setReadonly(true);
        form.getOnEditButton().setReadonly(true);
        form.bindSellers(Collections.singletonList(loggedInUser.getSellerOwner()));
        form.getSellersScrollBox().setReadonly(true);
        form.getSellersScrollBox().setSelectedKey(loggedInUser.getSellerOwner().getSellerId().toString());
    }

    public void setUpForm(ManageTerminalRestrictionsForm form) throws DatabaseException {
        final BrowseBox browseBox = (BrowseBox) form.getRestrictionsTable().getParameterControl("by_terminal_id", "terminal_id");
        if (!browseBox.isUpdated()) {//if !browseBox.isUpdated() is absent then defaultAction always set browseBox key in null
            browseBox.setKey(null);
        }
        final List<Seller> sellers = new SellerManager().getNestedSellersInSeller(loggedInUser.getSellerOwner(), true);
        form.getRestrictionsTable().setCriterionFactory(new CriterionFactory() {
            public CriterionWrapper createCriterion() {
                if ("".equals(browseBox.getKey()) || browseBox.getKey() == null)
                    return Restrictions.in("terminal.sellerOwner", sellers);
                return null;
            }
        });
    }

    public void setUpForm(BrowseTerminalsForm form) throws DatabaseException {
        final ScrollBox<Seller> scrollBox = (ScrollBox<Seller>) form.getBrowseTable().getParameterControl("by_seller_id", "seller_id");
        final List<Seller> sellers = new SellerManager().getNestedSellersInSeller(loggedInUser.getSellerOwner(), true);
        scrollBox.setNullable(true);

        final BrowseTerminalsForm finalForm = form;
        form.getBrowseTable().setCriterionFactory(new CriterionFactory() {
            public CriterionWrapper createCriterion() {
                CriterionWrapper resultCriterion = Restrictions.isNotNull("terminalId");
                if (scrollBox.isNullKeySelected()) {
                    resultCriterion =  Restrictions.and(resultCriterion,
                                        Restrictions.in("sellerOwner", sellers));
                }
                if(finalForm.getEditBoxSerialNumber().getValue() != null) {
                    resultCriterion =  Restrictions.and(resultCriterion,
                            Restrictions.ilike("serialNumber", "%" + finalForm.getEditBoxSerialNumber().getValue() + "%"));
                 }
                return resultCriterion;
            }
        });
        scrollBox.bind(sellers);

    }

    public void setUpForm(ManageSimpleSellerTariffsForm form) throws DatabaseException {
        ScrollBox scrollBox = (ScrollBox) form.getIncomingTriffsTable().getParameterControl("by_seller_id", "seller_id");
        scrollBox.setNullable(false);
        form.bindSellers(Collections.singletonList(loggedInUser.getSellerOwner()));

        form.bindSubSellers(new SellerManager().getNestedSellersInSeller(form.getSelectedSellerIdIncomingTable(), false));

        List subSellers = new SellerManager().getNestedSellersInSeller(form.getSelectedSellerIdIncomingTable(), false);
        if (subSellers.size() > 0) {
            form.getOnCreateButton().setReadonly(false);
        } else {
            form.getOnCreateButton().setReadonly(true);
        }
    }

    public void setUpForm(final ManageTerminalsForm form) throws DatabaseException 
    {
        form.getScrollBoxSellerOwner().bind( new SellerManager().getNestedSellersInSeller(loggedInUser) );

        final List<Seller> sellers = new SellerManager().getNestedSellersInSeller(loggedInUser.getSellerOwner(), true);

        CriterionFactory criterion = new CriterionFactory() {
            public CriterionWrapper createCriterion() {

                CriterionWrapper resultCriterion = Restrictions.in("sellerOwner", sellers);

                resultCriterion = Restrictions.and(resultCriterion, Restrictions.isNotNull("terminalId"));
                
                if(form.getEditBoxBySerialNumber().getValue() != null && !form.getEditBoxBySerialNumber().getValue().equals(""))
                {
                    resultCriterion = Restrictions.and(resultCriterion,
                            Restrictions.ilike("serialNumber", "%" + form.getEditBoxBySerialNumber().getValue() + "%"));
                }

                if(form.getScrollBoxSellerOwner().getSelectedKey() != null && ! form.getScrollBoxSellerOwner().isNullKeySelected())
                {
                    resultCriterion = Restrictions.and(resultCriterion,
                            Restrictions.eq("sellerOwner.sellerId", new Long(form.getScrollBoxSellerOwner().getSelectedKey())));
                }

                if (form.getScrollBoxEquipmentType().getSelectedKey() != null && !form.getScrollBoxEquipmentType().isNullKeySelected())
                {
                    resultCriterion = Restrictions.and(resultCriterion,
                            Restrictions.eq("equipmentType.equipmentTypeId", new Long(form.getScrollBoxEquipmentType().getSelectedKey())));
                }

                return resultCriterion;
            }

        };

        form.getTerminalsTable().setCriterionFactory(criterion);

        //form.getScrollBoxSellerOwner().setSelectedKey(ScrollBox.NULL_KEY);
        //form.getScrollBoxEquipmentType().setSelectedKey(ScrollBox.NULL_KEY);

        form.getOnCreateButton().setReadonly(true);
        form.getOnEditButton().setReadonly(false);
        form.getOnDeleteButton().setReadonly(true);
    }

    public void setUpForm(InfoTerminalForm form) throws DatabaseException
    {
        form.getEditBoxTerminalId().setReadonly(true);
        form.getEditBoxRegistrationDate().setReadonly(true);
        form.getEditBoxLogin().setReadonly(true);

        form.getEditBoxSerialNumber().setReadonly(true);
        form.getEditBoxHardwareSerialNumber().setReadonly(true);
        form.getScrollBoxEqTypes().setReadonly(true);

        form.getCheckBoxIsAttachRestriction().setReadonly(true);
        form.getCheckBoxIsAttachTariffOnline().setReadonly(true);
        form.getCheckBoxIsAttachTariffVouchers().setReadonly(true);

        form.getEditBoxTradePutlet().setReadonly(true);
        form.getEditBoxDirectorEmail().setReadonly(true);
        form.getEditBoxManagerEmail().setReadonly(true);
        form.getEditBoxAddress().setReadonly(true);
        form.getEditBoxCity().setReadonly(true);

        form.getScrollBoxTradePutletType().setReadonly(true);
        form.getEditBoxDirectorPhone().setReadonly(true);
        form.getEditBoxManagerPhone().setReadonly(true);
        form.getEditBoxRegion().setReadonly(true);
        form.getEditBoxBuilding().setReadonly(true);
    }

    public void setUpForm(ReportPaymentForm form) throws DatabaseException {
        form.getSellersScrollBox().setNullable(true);
        SellerManager sellerManager = new SellerManager();
        List subSellerList = Collections.EMPTY_LIST;
        List sellerList = new ArrayList();
//        sellerList.add(loggedInUser.getSellerOwner());
        sellerList.addAll(sellerManager.getSellersInSeller(loggedInUser.getSellerOwner().getSellerId(), true));
        if (form.getSellersScrollBox().getSelectedKey() != null) {
            if (!form.getSellersScrollBox().isNullKeySelected()) {
                subSellerList = sellerManager.getSellersInSeller(new Long(form.getSellersScrollBox().getSelectedKey()), true);
            }
        }
        form.bindSellersScrollBox(sellerList);
        form.bindSubSellersScrollBox(subSellerList);
        form.bindFilters(sellerList, subSellerList);

    }

    public void setUpForm(InfoSellerForm form) throws DatabaseException {
        form.getScrollBoxParentSellers().bind(new SellerManager().getNestedSellersInSeller(loggedInUser));
        form.getScrollBoxParentSellers().setNullable(false);

//        form.getEditBoxKSDiscountPercent().setReadonly(true);
//        form.getEditBoxMTSDiscountPercent().setReadonly(true);
//        form.getEditBoxLifeDiscountPercent().setReadonly(true);
//        form.getEditBoxBeelineDiscountPercent().setReadonly(true);
//        form.getEditBoxSellerDelta().setReadonly(true);
//
//        form.getCheckBoxIsAttachKSTariff().setReadonly(true);
//        form.getCheckBoxIsAttachMTSTariff().setReadonly(true);
//        form.getCheckBoxIsAttachLifeTariff().setReadonly(true);
//        form.getCheckBoxIsAttachBeelineTariff().setReadonly(true);

        //form.getScrollBoxParentSellers().setSelectedKey(loggedInUser.getSellerOwner().getSellerId().toString());
        form.getScrollBoxParentSellers().setSelectedKey(ScrollBox.NULL_KEY);
    }

    public void setUpForm(AbstractInfoBalanceForm form) throws DatabaseException {
        Seller seller = (new SellerManager()).getSellerOnlyPrimaryBalance(loggedInUser.getSellerOwner().getSellerId());
        if(seller.getPrimaryBalance().getBalanceId().equals(form.getBalanceId())) {
            form.getOnChangeBalanceButton().setReadonly(true);
        }
    }

    public void setUpForm(InfoSellerRestrictionForm form) throws DatabaseException {
        form.getScrollBoxSeller().bind(new SellerManager().getNestedSellersInSeller(loggedInUser.getSellerOwner(), false));    
    }

    public void setUpForm(InfoSellerTariffForm form) throws DatabaseException {
        form.getScrollBoxSellers().bind(new SellerManager().getNestedSellersInSeller(loggedInUser.getSellerOwner(), false));
    }
    public void setUpForm(ReportVouchersForm form) throws DatabaseException {
        List sellers = new ArrayList();
        sellers.add(new SellerManager().getSeller(loggedInUser.getSellerOwner().getSellerId()));

        form.getScrollBoxSeller().setNullable(false);
        form.getScrollBoxSeller().bind(sellers);
        form.getScrollBoxSeller().setSelectedKey( ((Seller)sellers.get(0)).getSellerId().toString() ) ;
    }

    public void setUpForm(ReportNetworksForm form) throws DatabaseException {
        SellerManager manager = new SellerManager();
        List<Seller> sellers = manager.getNestedSellersInSeller(loggedInUser.getSellerOwner(), true);
        Collections.sort(sellers);
        form.getMultipleSelectBoxSellerBalances().bind(sellers);
        form.getMultipleSelectBoxSellerBalances().selectAll();
        List<Seller> subSellers = manager.getSellersInSeller(loggedInUser.getSellerOwner().getSellerId());
        Collections.sort(subSellers);
        //form.getMultipleSelectBoxSubSellerBalances().bind(manager.getSellersInSeller(loggedInUser.getSellerOwner().getSellerId()));
        form.getMultipleSelectBoxSubSellerBalances().bind(subSellers);
    }
}
