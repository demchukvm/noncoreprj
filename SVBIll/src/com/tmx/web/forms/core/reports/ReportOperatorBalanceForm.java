package com.tmx.web.forms.core.reports;

import com.tmx.web.base.BasicActionForm;
import com.tmx.web.controls.table.ControlManagerTable;
import com.tmx.web.controls.table.Filter;
import com.tmx.web.controls.table.Parameter;
import com.tmx.web.controls.ScrollBox;
import com.tmx.web.controls.TimeSelector;
import com.tmx.web.controls.EditBox;
import com.tmx.web.controls.CriterionFactory;
import com.tmx.as.entities.bill.transaction.TransactionType;
import com.tmx.as.entities.bill.operator.Operator;
import com.tmx.as.base.criterion.CriterionWrapper;
import com.tmx.as.base.criterion.Restrictions;
import com.tmx.beng.base.StatusDictionary;

public class ReportOperatorBalanceForm extends BasicActionForm {
    private ControlManagerTable table;
    private ScrollBox operatorsScrollBox;

    private EditBox editBoxAllTransaction;
    private EditBox editBoxSuccessTransaction;

    private EditBox editBoxAllTransAmount;
    private EditBox editBoxSuccessTransAmount;
    private EditBox editBoxPaymentReason;

    protected void init() {
        initTables();
        initEditBoxes();
        initFilters();
    }

    /**
     * Methods fill table with DB
     */
    private void initTables() {
       table = new ControlManagerTable("query(query_type=retrieve_all," +
                "entity_class=com.tmx.as.entities.bill.transaction.OperatorBalanceTransaction,"  +
                "filter(name=by_operation_type,parameter(name=operationType,value=3,type=long))," +
                "filter(name=by_client_status,parameter(name=status,value="+ StatusDictionary.STATUS_OK + ",type=String))," +
                "table_attr=operator," +
                "table_attr=transactionSupport.userActor,"  +
                "table_attr=type," +
                "order(name=id,type=desc)," +
                "page_size=10" +
                ")");
    }

    private void initEditBoxes() {
        editBoxAllTransaction = new EditBox();
        editBoxSuccessTransaction = new EditBox();

        editBoxAllTransAmount = new EditBox();
        editBoxSuccessTransAmount = new EditBox();
        editBoxPaymentReason = new EditBox();
    }

     private void initFilters() {
         Filter filter = new Filter();
         filter.addParameter("date", new Parameter(new TimeSelector()));
         table.getFilterSet().addFilter("by_FromDate", filter);

         filter = new Filter();
         filter.addParameter("date", new Parameter(new TimeSelector()));
         table.getFilterSet().addFilter("by_ToDate", filter);

//         filter = new Filter();
//         filter.addParameter("paymentReason", new Parameter(new EditBox()));
//         table.getFilterSet().addFilter("by_paymentReason", filter);

         filter = new Filter();
         EditBox editBox = new EditBox();
         editBox.addValidationRule(EditBox.createValidationRule_INTEGER_FORMAT(getLocale()));
         filter.addParameter("from_amount", new Parameter(editBox));
         table.getFilterSet().addFilter("by_client_amount_from", filter);

         filter = new Filter();
         editBox = new EditBox();
         editBox.addValidationRule(EditBox.createValidationRule_INTEGER_FORMAT(getLocale()));
         filter.addParameter("to_amount", new Parameter(editBox));
         table.getFilterSet().addFilter("by_client_amount_to", filter);

         ScrollBox scrollBoxOperTypes = new ScrollBox(){
            protected String getKey(Object dataListElement) {
                return ((TransactionType)dataListElement).getId().toString();
            }
            protected String getValue(Object dataListElement) {
                return ((TransactionType)dataListElement).getName();
            }
        };
        scrollBoxOperTypes.setNullable(true);
        filter = new Filter();
        filter.addParameter("type", new Parameter(scrollBoxOperTypes));
        table.getFilterSet().addFilter("by_transaction_type",filter);

        ScrollBox scrollBoxOperators = new ScrollBox(){
            protected String getKey(Object dataListElement) {
                return ((Operator)dataListElement).getOperatorId().toString();
            }
            protected String getValue(Object dataListElement) {
                return ((Operator) dataListElement).getName();

            }
        };
        scrollBoxOperators.setNullable(true);
        filter = new Filter();
        filter.addParameter("operator", new Parameter(scrollBoxOperators));
        table.getFilterSet().addFilter("by_operator",filter);

         CriterionFactory criterion = new CriterionFactory() {

             public CriterionWrapper createCriterion() {
                 if (editBoxPaymentReason.getValue() != null) {
                     if (!editBoxPaymentReason.getValue().equals("")) {
                         return Restrictions.ilike("transactionSupport.paymentReason", "%" + editBoxPaymentReason.getValue() + "%");
                     }
                 }
                 return null;//To change body of implemented methods use File | Settings | File Templates.
             }
         };
         table.setCriterionFactory(criterion);
     }

    public ControlManagerTable getTable() {
        return table;
    }

    public void setTable(ControlManagerTable table) {
        this.table = table;
    }

    public ScrollBox getOperatorsScrollBox() {
        return operatorsScrollBox;
    }

    public void setOperatorsScrollBox(ScrollBox operatorsScrollBox) {
        this.operatorsScrollBox = operatorsScrollBox;
    }

    public EditBox getEditBoxAllTransaction() {
        return editBoxAllTransaction;
    }

    public void setEditBoxAllTransaction(EditBox editBoxAllTransaction) {
        this.editBoxAllTransaction = editBoxAllTransaction;
    }

    public EditBox getEditBoxSuccessTransaction() {
        return editBoxSuccessTransaction;
    }

    public void setEditBoxSuccessTransaction(EditBox editBoxSuccessTransaction) {
        this.editBoxSuccessTransaction = editBoxSuccessTransaction;
    }

    public EditBox getEditBoxAllTransAmount() {
        return editBoxAllTransAmount;
    }

    public void setEditBoxAllTransAmount(EditBox editBoxAllTransAmount) {
        this.editBoxAllTransAmount = editBoxAllTransAmount;
    }

    public EditBox getEditBoxSuccessTransAmount() {
        return editBoxSuccessTransAmount;
    }

    public void setEditBoxSuccessTransAmount(EditBox editBoxSuccessTransAmount) {
        this.editBoxSuccessTransAmount = editBoxSuccessTransAmount;
    }

    public EditBox getEditBoxPaymentReason() {
        return editBoxPaymentReason;
    }

    public void setEditBoxPaymentReason(EditBox editBoxPaymentReason) {
        this.editBoxPaymentReason = editBoxPaymentReason;
    }
}

