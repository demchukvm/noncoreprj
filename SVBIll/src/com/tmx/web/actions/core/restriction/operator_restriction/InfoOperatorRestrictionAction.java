package com.tmx.web.actions.core.restriction.operator_restriction;

import com.tmx.as.blogic.restriction.AbstractRestrictionManager;
import com.tmx.as.blogic.restriction.OperatorRestrictionManager;
import com.tmx.as.blogic.OperatorManager;
import com.tmx.as.entities.bill.restriction.AbstractRestriction;
import com.tmx.as.entities.bill.restriction.OperatorRestriction;
import com.tmx.as.entities.general.user.User;
import com.tmx.as.exceptions.DatabaseException;

import com.tmx.web.actions.core.restriction.AbstractInfoRestrictionAction;
import com.tmx.web.actions.core.restriction.AbstractManageRestrictionsAction;
import com.tmx.web.forms.core.restriction.AbstractInfoRestrictionForm;
import com.tmx.web.forms.core.restriction.operator_restriction.InfoOperatorRestrictionForm;
import com.tmx.web.base.WebException;


public class InfoOperatorRestrictionAction extends AbstractInfoRestrictionAction {

    protected void preloadForCreate(AbstractInfoRestrictionForm form, AbstractRestriction restriction, User loggedInUser) throws DatabaseException, WebException {
             InfoOperatorRestrictionForm restrForm = (InfoOperatorRestrictionForm) form;
             String operatorId = getRoadmap().getParameterValue(getName(), AbstractManageRestrictionsAction.PARAMETER_OWNER_NAME, true).toString();
             restrForm.getScrollBoxOperator().bind(new OperatorManager().getAllOperators());
             restrForm.getScrollBoxOperator().setSelectedKey(operatorId);
//             {!!!!}
     //        prepareRoadmap();
                       }

         private void prepareRoadmap() throws WebException {
             getRoadmap().collectParameter("/core/restriction/operator_restriction/operatorRestrictionParameter", "infoAction", getName());
             getRoadmap().collectParameter("/core/restriction/operator_restriction/operatorRestrictionParameter", "paramAction", "/core/restriction/operator_restriction/operatorRestrictionParameter");
                   }


         protected void preloadForUpdate(AbstractInfoRestrictionForm form, AbstractRestriction restriction, User loggedInUser) throws DatabaseException, WebException {
             InfoOperatorRestrictionForm restrForm = (InfoOperatorRestrictionForm) form;
                       restrForm.getScrollBoxOperator().bind(new OperatorManager().getAllOperators());
                       restrForm.getScrollBoxOperator().setSelectedKey(((OperatorRestriction)restriction).getOperator().getOperatorId().toString());
                       restrForm.getScrollBoxOperator().setReadonly(true);

                       prepareRoadmap();
                   }

         protected AbstractRestrictionManager getRestrictionManager() {
             return new OperatorRestrictionManager();
                   }

         protected void preloadForUpdate(AbstractInfoRestrictionForm form, AbstractRestriction restriction) throws DatabaseException {
             //do nothing
                   }
     }
