package com.tmx.as.entities.bill.bank;

import com.tmx.as.base.BasicEntity;
import com.tmx.as.entities.bill.seller.Seller;

import java.io.Serializable;


public class BankAccount extends BasicEntity {
    private Long bankAccountId;
    private String accountNumber;
    private Seller seller;
    private Bank bank;

    public Long getBankAccountId() {
        return bankAccountId;
    }

    public void setBankAccountId(Long bankAccountId) {
        this.bankAccountId = bankAccountId;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public Serializable getId() {
        return bankAccountId;
    }

    public int getValid() {
        return valid;
    }

    public void setValid(int valid) {
        this.valid = valid;
    }

    public Seller getSeller() {
        return seller;
    }

    public void setSeller(Seller seller) {
        this.seller = seller;
    }

    public Bank getBank() {
        return bank;
    }

    public void setBank(Bank bank) {
        this.bank = bank;
    }
}
