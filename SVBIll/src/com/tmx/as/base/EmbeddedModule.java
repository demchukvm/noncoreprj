package com.tmx.as.base;

import org.apache.log4j.Logger;

import java.util.Properties;

import com.tmx.as.modules.ModuleException;

/**
 * Created by IntelliJ IDEA.
 * User: shake
 * Date: 07.06.2007
 * Time: 12:54:35
 */
public interface EmbeddedModule {
    public void start(Properties props) throws ModuleException;
    public void shutdownGraceful(Properties props) throws ModuleException;
    public void shutdownAbort(Properties props) throws ModuleException;
}
