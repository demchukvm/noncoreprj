package com.tmx.gate.ftp.ssl;

import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.SecureRandom;


public class RandomGenerator extends Thread {
  private SecureRandom secureRandom = null;
  private boolean finished = false;

  public RandomGenerator() {}


  public void run() {
    byte abyte0[] = new byte[1];
    try {
      secureRandom = SecureRandom.getInstance("SHA1PRNG", "SUN");
    }
    catch (NoSuchAlgorithmException e){
      e.printStackTrace();
    }
    catch (NoSuchProviderException e) {
      e.printStackTrace();
    }
    secureRandom.nextBytes(abyte0);
    finished = true;
  }


  public SecureRandom getSecureRandom() {
    return secureRandom;
  }


  public boolean isFinished() {
    return finished;
  }

}
