<%@ page language="java" pageEncoding="UTF-8" contentType="text/html; charset=utf-8"
         errorPage="/core/jsp/message/jsperror.jsp" isELIgnored="false" %>
<%@ taglib uri="/WEB-INF/tld/struts-nested.tld" prefix="nested" %>
<%@ taglib uri="/WEB-INF/tld/tmx-controls.tld" prefix="tmx_ctrl" %>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>


<html:form action="/core/restriction/terminal_restriction/infoTerminalRestriction">
<table border="0" width="100%">

    <bean:define id="form" name="core.restriction.terminal_restriction.infoTerminalRestrictionForm"
                 type="com.tmx.web.forms.core.restriction.terminal_restriction.InfoTerminalRestrictionForm"/>

    <!-- BUTTONS -->
    <tr>
        <td style="text-align: left">
            <tmx_ctrl:button property="onSaveButton"/>
            <tmx_ctrl:button property="onApplyButton"/>
            <tmx_ctrl:button property="onResetButton" onClick="javascript:document.forms[0].reset()"/>
            <tmx_ctrl:button property="onCancelButton"/>
        </td>
    </tr>

    <tr>
        <td valign="center" align="center" class="tabForm">
        <table width="100%" border="0">
            <!-- Form header -->
            <tr>
                <td colspan="2">
                    <h4 class="dataLabel">
                        <bean:message bundle="core.labels" key="core.balance.sellerrestriction.infosellerrestrictionform.titleGeneral"/>
                    </h4>
                </td>
            </tr>

            <!--BEGIN for abstract info transaction form    -->
            <tr>
                <td class="dataLabel" width="15%">
                    <tmx_ctrl:writeEntityLabel attrName="balanceId"
                                               entityClassName="com.tmx.as.entities.bill.balance.SellerBalance"/>:
                </td>
                <td class="dataField" width="35%">
                    <tmx_ctrl:editBox property="editBoxRestrictionId"/>
                </td>
            </tr>

            <tr>
                <td class="dataLabel" width="15%">
                    <tmx_ctrl:writeEntityLabel attrName="name"
                                               entityClassName="com.tmx.as.entities.bill.balance.SellerBalance"/>:
                </td>
                <td class="dataField" width="35%">
                    <tmx_ctrl:editBox property="editBoxName" width="270px"/>
                </td>

                <td class="dataLabel" width="15%">
                    <tmx_ctrl:writeEntityLabel attrName="{@name}"
                                               entityClassName="com.tmx.as.entities.bill.terminal.Terminal"/>:
                </td>
                <td class="dataField" width="35%">
                    <tmx_ctrl:browseBox property="browseBoxTerminals" width="270px"/>
                </td>
            </tr>

        </table>
        </td>
        </tr>

    <tr>
         <td valign="center" align="center" class="tabForm">
            <table width="100%" border="0">
                <!-- Form header -->
                <tr>
                    <td colspan="2">
                        <h4 class="dataLabel">
                            <bean:message bundle="core.labels" key="core.balance.sellerrestriction.infosellerrestrictionform.titleFunction"/>
                        </h4>
                    </td>
                </tr>

                <!--BEGIN for abstract info transaction form    -->

                <tr>
                    <td class="dataLabel" width="15%">
                        <tmx_ctrl:writeEntityLabel attrName="balanceId"
                                                   entityClassName="com.tmx.as.entities.bill.balance.SellerBalance"/>:
                    </td>
                    <td class="dataField" width="35%">
                        <tmx_ctrl:editBox property="editBoxFunctionInstanceId"/>
                    </td>

                    <td class="dataLabel" width="15%">
                        <tmx_ctrl:writeEntityLabel attrName="name"
                                                   entityClassName="com.tmx.as.entities.bill.function.FunctionType"/>:
                    </td>
                    <td class="dataField" width="35%">
                        <tmx_ctrl:browseBox property="browseBoxFunctionTypes" width="270px"/>
                    </td>
                </tr>

                <tr>
                    <td class="dataLabel" width="15%">

                    </td>
                    <td class="dataField" width="35%">

                    </td>

                    <td class="dataLabel" width="15%">
                        <tmx_ctrl:writeEntityLabel attrName="description"
                                                   entityClassName="com.tmx.as.entities.bill.function.FunctionType"/>:
                    </td>
                    <td class="dataField" width="35%">
                        <tmx_ctrl:textArea property="textAreaFuncDescription"/>
                    </td>
                </tr>

            </table>
         </td>
       </tr>



    <tr>
        <td width="100%">
            <table class='one_outer_cell_table' cellspacing='0' cellpadding='0' width="100%">

                <tr>
                    <td>
                        <table cellspacing='1' cellpadding='1' width="100%">
                            <!-- Table header -->
                            <tr>
                                <th class="column_header_cell" width="110">
                                    <tmx_ctrl:imgTableOrdering tableName="defaultFuncParametrsTable"
                                                               orderByFieldName="orderNum"
                                                               labelBundle="as:com.tmx.as.entities.bill.function.ActualFunctionParameter"
                                                               labelKey="orderNum"/>
                                </th>

                                <th class="column_header_cell" width="110">
                                    <tmx_ctrl:imgTableOrdering tableName="defaultFuncParametrsTable"
                                                               orderByFieldName="name"
                                                               labelBundle="as:com.tmx.as.entities.bill.function.ActualFunctionParameter"
                                                               labelKey="name"/>
                                </th>

                                <th class="column_header_cell" width="110">
                                    <tmx_ctrl:imgTableOrdering tableName="defaultFuncParametrsTable"
                                                               orderByFieldName="value"
                                                               labelBundle="as:com.tmx.as.entities.bill.function.ActualFunctionParameter"
                                                               labelKey="value"/>
                                </th>

                                <th class="column_header_cell" width="110">
                                    <tmx_ctrl:imgTableOrdering tableName="defaultFuncParametrsTable"
                                                               orderByFieldName="mandatory"
                                                               labelBundle="as:com.tmx.as.entities.bill.function.ActualFunctionParameter"
                                                               labelKey="mandatory"/>
                                </th>
                            </tr>

                            <nested:iterate id="parameter" property="defaultFuncParametrsTable.data" type="com.tmx.as.entities.bill.function.ActualFunctionParameter">
                                <tr>
                                    <td class='column_data_cell'>
                                        <c:out value="${parameter.orderNum}" />
                                    </td>
                                    <td class='column_data_cell'>
                                        <c:out value="${parameter.name}" />
                                    </td>
                                    <td class='column_data_cell'>
                                        <c:out value="${parameter.value}" />
                                    </td>
                                    <td class='column_data_cell'>
                                        <tmx_ctrl:checkSign name="parameter" property="mandatory"/>
                                        <%--<c:out value="${parameter.mandatory}" />--%>
                                    </td>
                                </tr>
                            </nested:iterate>
                            <!-- Empty row -->
                            <nested:define id="tableSize" property="defaultFuncParametrsTable.resultSize" type="java.lang.Integer"/>
                            <c:if test="${tableSize <= 0}">
                                <tr>
                                    <td class='column_data_cell' colspan="5" align="center">
                                        <bean:message bundle="core.labels" key="core.common.th.empty_table" />
                                    </td>
                                </tr>
                            </c:if>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>





    <tr>
        <td colspan="2">
            <h4 class="dataLabel">
                <bean:message bundle="core.labels" key="core.balance.sellertariff.infosellertariffform.titleParameters"/>
            </h4>
        </td>
    </tr>

    <tr>
        <td style="text-align: left">
            <tmx_ctrl:button property="onRefreshParamButton"/>
            <tmx_ctrl:button property="onCreateParamButton"/>
            <tmx_ctrl:button property="onEditParamButton"/>
            <tmx_ctrl:button property="onDeleteParamButton"/>
        </td>
    </tr>

    <tr>
        <td width="100%">
            <table class='one_outer_cell_table' cellspacing='0' cellpadding='0' width="100%">

                <tr>
                    <td>
                        <table cellspacing='1' cellpadding='1' width="100%">
                            <!-- Table header -->
                            <tr>
                                <th class="column_header_cell" width="110">
                                    <bean:message bundle="core.labels" key="core.common.th.choose"/>
                                </th>

                                <th class="column_header_cell" width="110">
                                    <tmx_ctrl:imgTableOrdering tableName="funcParametrsTable"
                                                               orderByFieldName="orderNum"
                                                               labelBundle="as:com.tmx.as.entities.bill.function.ActualFunctionParameter"
                                                               labelKey="orderNum"/>
                                </th>

                                <th class="column_header_cell" width="110">
                                    <tmx_ctrl:imgTableOrdering tableName="funcParametrsTable"
                                                               orderByFieldName="name"
                                                               labelBundle="as:com.tmx.as.entities.bill.function.ActualFunctionParameter"
                                                               labelKey="name"/>
                                </th>

                                <th class="column_header_cell" width="110">
                                    <tmx_ctrl:imgTableOrdering tableName="funcParametrsTable"
                                                               orderByFieldName="value"
                                                               labelBundle="as:com.tmx.as.entities.bill.function.ActualFunctionParameter"
                                                               labelKey="value"/>
                                </th>

                            </tr>
                            <nested:iterate id="parameter" property="funcParametrsTable.data" type="com.tmx.as.entities.bill.function.ActualFunctionParameter">
                                <tr>
                                    <td class='column_data_cell'>
                                        <input type="radio" name="parameterId" value="<%= form.getFuncParametrsTable().getData().indexOf(parameter)%>"
                                               checked="false">
                                    </td>
                                    <td class='column_data_cell'>
                                        <c:out value="${parameter.orderNum}" />
                                    </td>
                                    <td class='column_data_cell'>
                                        <c:out value="${parameter.name}" />
                                    </td>
                                    <td class='column_data_cell'>
                                        <c:out value="${parameter.value}" />
                                    </td>
                                </tr>
                            </nested:iterate>
                            <!-- Empty row -->
                            <nested:define id="tableSize" property="funcParametrsTable.resultSize" type="java.lang.Integer"/>
                            <c:if test="${tableSize <= 0}">
                                <tr>
                                    <td class='column_data_cell' colspan="7" align="center">
                                        <bean:message bundle="core.labels" key="core.common.th.empty_table" />
                                    </td>
                                </tr>
                            </c:if>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td style="text-align: right">
            <tmx_ctrl:imgTablePaging maxBookmarkCount="10" tableName="funcParametrsTable"/>
        </td>
    </tr>

    <nested:define id="errorTableSize" property="paramErrorTable.resultSize" type="java.lang.Integer"/>
    <c:if test="${errorTableSize > 0}">
        <tr>
            <td colspan="2">
                <h4 class="dataLabel" style="color: #FF0000">
                    <bean:message bundle="core.labels" key="core.balance.sellertariff.infosellertariffform.titleErrorParameters"/>
                </h4>
            </td>
        </tr>
        <tr>
            <td>
                <table cellspacing='1' cellpadding='1' width="100%">
                    <nested:iterate id="error" property="paramErrorTable.data" type="com.tmx.beng.base.dbfunctions.ValidationError">
                        <tr>
                            <td align="left" style="color: #FF0000">
                                <c:out value="${error.description}" />
                            </td>
                        </tr>
                    </nested:iterate>
                </table>
            </td>
        </tr>
    </c:if>

</table>
</html:form>