package com.tmx.web.actions.core.csapi;

import com.tmx.as.modules.SpringModule;
import com.tmx.beng.access.Access;
import com.tmx.beng.access.Connection;
import com.tmx.beng.base.BillException;
import com.tmx.beng.csapi.xml.v1.base.ClientPAD;
import com.tmx.util.InitException;
import com.tmx.web.base.BasicDispatchedAction;
import com.tmx.web.forms.core.csapi.CsApiCallForm;
import com.tmx.web.forms.core.csapi.LocalRefillForm;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


/**
 */
public class LocalRefillAction extends BasicDispatchedAction
{
    Logger logger = Logger.getLogger("webcsapi."+getClass().getName());

    private String xmlMTSExc_LocalSSM_p1 =
            "<root xmlns=\"http://tmx.com/gate/csapi/v1/base/beans\">" +
                    "<request protocol=\"1.1\">" +
                        "<auth>" +
                            "<terminalSn>localssm-01</terminalSn>" +
                            "<sellerCode>LocalSSM</sellerCode>" +
                            "<processingCode>Local</processingCode>" +
                            "<login>Local</login>" +
                            "<password>7460dfdf78c61247fc883fd5e4553fb4</password>" +
                            "<clientTime>2009.05.21 18:13:22</clientTime>" +
                        "</auth>" +
                        "<operations>" +
                            "<refillPayment>" +
                                "<cliTransactionNum>UN_00004_manual2010.07.05 13:10:00</cliTransactionNum>" +
                                "<serviceCode>MTSExc</serviceCode>" +
                                "<amount>";

    private String xmlMTSExc_LocalSSM_p2 =
                                          "</amount>" +
                                "<msisdn>";

    private String xmlMTSExc_LocalSSM_p3 =
                                                    "</msisdn>" +
                            "</refillPayment>" +
                        "</operations>" +
                    "</request>" +
            "</root>";


    public ActionForward byTelephoneNum(ActionMapping mapping,
                                              ActionForm form,
                                              HttpServletRequest request,
                                              HttpServletResponse response) throws Throwable{
        LocalRefillForm localRefillForm = (LocalRefillForm) verifyForm(form, LocalRefillForm.class);
        String xml = xmlMTSExc_LocalSSM_p1+localRefillForm.getAmount1().getValue()+xmlMTSExc_LocalSSM_p2+localRefillForm.getTelephone().getValue()+xmlMTSExc_LocalSSM_p3;
        localRefillForm.setRequestXml(xml);
        return clientSideApiCall(mapping, form, request, response);
        //return mapping.findForward("operationComplete");
    }

    public ActionForward byAccountNum(ActionMapping mapping,
                                              ActionForm form,
                                              HttpServletRequest request,
                                              HttpServletResponse response) throws Throwable{

        return mapping.findForward("operationComplete");
    }

    /*public ActionForward defaultAction(ActionMapping mapping,
                                       ActionForm form,
                                       HttpServletRequest request,
                                       HttpServletResponse response,
                                       String methodName) throws Throwable {
        LocalRefillForm localRefillForm = (LocalRefillForm) form;
        return mapping.findForward("operationComplete");
    }*/

        public ActionForward clientSideApiCall(ActionMapping mapping,
                                               ActionForm form,
                                               HttpServletRequest request,
                                               HttpServletResponse response) throws Throwable {

        LocalRefillForm localRefillForm = (LocalRefillForm) verifyForm(form, LocalRefillForm.class);
        ClientPAD clientPAD = new ClientPAD();
        try{
            Connection conn = (Connection) SpringModule.getApplicationContext().getBean("billingConnection");

            //1. Allocate access to billing engine
//            try{
//                Access access = obtainBillingEngineAccess(localRefillForm);
//                conn = access.obtainConnection();
//            }
//            catch(InitException e){
//                logger.error("Client initialization error: \n"+e.getTraceDump());
//                localRefillForm.setXmlDocSerialized(clientPAD.buildErrorResponse(ClientPAD.CSAPI_CLIENT_INIT_ERROR, getLocale(request)));
//                throw new HandledBillingException();
//            }

            long t = System.currentTimeMillis();
            ClientPAD.ParsingResult resp;

            //2. Parse request
            try{
                logger.info("Request XML: " + localRefillForm.getRequestXml());
                resp = clientPAD.parseRequest(localRefillForm.getRequestXml());
                if (localRefillForm.getDesRequestXml() != null) {
                    resp = clientPAD.parseRequest(localRefillForm.getDesRequestXml());
                }
                logger.debug("Parse time: " + (System.currentTimeMillis() - t));
            }
            catch(BillException e){
                logger.error("Parsing error: \n"+e.getTraceDump());
                localRefillForm.setXmlDocSerialized(clientPAD.buildErrorResponse(ClientPAD.CSAPI_REQUEST_PARSING_ERROR, getLocale(request)));
                throw new HandledBillingException();
            }

            //3. Process request
            try{
                resp.setBillingMessage(conn.processSync(resp.getBillingMessage()));
            }
            catch(BillException e){
                logger.error("Processing error: \n"+e.getTraceDump());
                localRefillForm.setXmlDocSerialized(clientPAD.buildErrorResponse(ClientPAD.CSAPI_REQUEST_PROCESSING_ERROR, getLocale(request)));
                throw new HandledBillingException();
            }

            //4. Serialize request
            localRefillForm.setXmlDocSerialized(clientPAD.safeSerializeResponse(resp.getBillingMessage(), resp.getVersion()));
            logger.info("Response XML: " + localRefillForm.getXmlDocSerialized());
        }
        catch(HandledBillingException e){
            /** do nothing already handled */
        }
        catch(Throwable e){
            logger.error("Unknown error: \n"+e.toString());
            localRefillForm.setXmlDocSerialized(clientPAD.buildErrorResponse(ClientPAD.CSAPI_UNKNOWN_ERROR, getLocale(request)));
        }
        return mapping.findForward("operationComplete");
    }

        private Access obtainBillingEngineAccess(LocalRefillForm localRefillForm) throws InitException{
        if(localRefillForm.getBillingEngineAccess() == null){
            InitialContext ctx = null;
            try{
                System.setProperty("java.naming.factory.url.pkgs", ""); //=org.apache.naming
                ctx = new InitialContext();//System.getProperties()
                Access newAccess = (Access)ctx.lookup("java:comp/services/billingEngine");
                if(newAccess == null)
                    throw new InitException("err.csapi.null_basic_access_obtained");

                localRefillForm.setBillingEngineAccess(newAccess);
            }
            catch(NamingException e){
                throw new InitException("err.csapi.failed_to_obtain_billing_engine_access", e);
            }
            finally{
			    if(ctx != null){
				    try{
    					ctx.close();
    				}
	    			catch(NamingException e) {
		    			//log.info( "Unable to release initial context", ne );
			    	}
			    }
            }
        }
        return localRefillForm.getBillingEngineAccess();
    }

    private class HandledBillingException extends Throwable{
    }
}