package com.tmx.engines.xmlengine.customTags;

import com.tmx.engines.xmlengine.NodePrototype;
import com.tmx.as.base.Entity;
import org.w3c.dom.Node;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.CDATASection;

public abstract class AbstractNode implements NodePrototype {
    private Entity entity = null;
    private String name = "";
    private String id = "";


    public Node renderAndInjectXMLNode(Node parentNode, Entity entity) {
        setEntity(entity);

        Document ownerDoc = parentNode.getOwnerDocument();
        Element abstractNode = ownerDoc.createElement(getTagName());

        if(getName() != null && getName().trim().length() > 0)
            abstractNode.setAttribute("name", getName());

        if(getId() != null && getId().trim().length() > 0)
            abstractNode.setAttribute("id", getId());

        CDATASection cdata = ownerDoc.createCDATASection(getCDATASectionValue());
        abstractNode.appendChild(cdata);
        parentNode.appendChild(abstractNode);
        return abstractNode;
    }

    protected abstract String getCDATASectionValue();

    //---------------------Getters and setters
    protected Entity getEntity() {
        return entity;
    }

    protected void setEntity(Entity entity) {
        this.entity = entity;
    }

    /** to be overriden in descendants */
    protected String getName(){
        return this.name;
    }

    /** to be overriden in descendants */
    protected void setName(String name){
        this.name = (name == null) ? "" : name;
    }

    /** to be overriden in descendants */
    protected String getId(){
        return this.id;
    }

    /** to be overriden in descendants */
    protected void setId(String id){
        this.id = (id == null) ? "" : id;
    }

    /** to be overriden in descendants */
    public String getTagName() {
        return "abstractTag";
    }

}
