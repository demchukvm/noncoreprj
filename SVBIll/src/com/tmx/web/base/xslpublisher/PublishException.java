package com.tmx.web.base.xslpublisher;

import com.tmx.util.StructurizedException;
import java.util.Locale;

/**
 */
public class PublishException extends StructurizedException {

    public PublishException(String errKey){
        super(errKey);
    }

    public PublishException(String errKey, Locale locale){
        super(errKey, locale);
    }

    public PublishException(String errKey, String[] params){
        super(errKey, params);
    }

    public PublishException(String errKey, String[] params, Throwable cause){
        super(errKey, params, cause);
    }

    public PublishException(String errKey, String[] params, Locale locale){
        super(errKey, params, locale);
    }

    public PublishException(String errKey, Throwable cause){
        super(errKey, cause);
    }
}
