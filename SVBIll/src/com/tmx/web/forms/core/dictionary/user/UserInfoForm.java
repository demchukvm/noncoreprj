package com.tmx.web.forms.core.dictionary.user;

import com.tmx.web.base.BasicActionForm;
import com.tmx.web.controls.*;

import com.tmx.as.entities.general.user.User;
import com.tmx.as.entities.general.role.Role;
import com.tmx.as.entities.general.organization.Organization;
import com.tmx.as.entities.bill.seller.Seller;

import java.util.List;
import java.util.Date;

public class UserInfoForm extends BasicActionForm {

    private Long selectedUserId = null;

    private ScrollBox selectRoles = null;
    private ScrollBox selectOrganizations = null;
    private ScrollBox scrollBoxSellers;

    //private CheckBox checkBoxActive = null;
    private boolean checkBoxActive;
    private boolean _checkBoxActive;

    private EditBox editBoxUserId = null;
    private EditBox editBoxFirstName = null;
    private EditBox editBoxLastName = null;
    private EditBox editBoxAdmin = null;
    private EditBox editBoxLogin = null;
    private EditBox editBoxPassword = null;
    private EditBox editBoxPassword2 = null;
    private EditBox editBoxEmail = null;
    private EditBox editBoxActive = null;

    private TimeSelector timeSelectorCreateTime;

    private Button onSaveButton = null;
    private Button onResetButton = null;
    private Button onCancelButton = null;


    protected void init() {
        //initialze and register controls
        selectRoles = new ScrollBox() {
            protected String getKey(Object dataListElement) {
                return ((Role) dataListElement).getRoleId().toString();
            }

            protected String getValue(Object dataListElement) {
                return ((Role) dataListElement).getRoleName();
            }
        };

        selectOrganizations = new ScrollBox() {
            protected String getKey(Object dataListElement) {
                return ((Organization) dataListElement).getOrganizationId().toString();
            }

            protected String getValue(Object dataListElement) {
                return ((Organization) dataListElement).getName();
            }
        };

        scrollBoxSellers = new ScrollBox() {
            protected String getKey(Object dataListElement) {
                return ((Seller)dataListElement).getSellerId().toString();
            }

            protected String getValue(Object dataListElement) {
                return ((Seller)dataListElement).getName();
            }
        };
        scrollBoxSellers.setMandatory(true);

        editBoxFirstName = new EditBox();
        editBoxFirstName.setMandatory(true);
        editBoxLastName = new EditBox();
        editBoxLastName.setMandatory(true);
        editBoxAdmin = new EditBox();
        editBoxUserId = new EditBox();
        editBoxUserId.setReadonly(true);
        editBoxEmail = new EditBox();
        editBoxLogin = new EditBox();
        editBoxLogin.setMandatory(true);
        editBoxPassword = new EditBox();
        editBoxPassword.setMandatory(true);
        editBoxPassword.addValidationRule(EditBox.createValidationRule_MIN_FIELD_LENGTH(8));
        editBoxPassword2 = new EditBox();
        editBoxPassword2.addValidationRule(EditBox.createValidationRule_PASSWD_FIELDS_MATCH(editBoxPassword));
        editBoxActive = new EditBox();
        //checkBoxActive = new CheckBox();
        //checkBoxActive.setSelected(true);
        //checkBoxActive = true;
        timeSelectorCreateTime = new TimeSelector();
        timeSelectorCreateTime.setReadonly(true);

        //set up buttons
        onSaveButton = new Button();
        onSaveButton.setLabelKey("ctrl.button.label.save");
        onSaveButton.setAction("/core/dictionary/user/userInfo");
        onSaveButton.setCommand("save");

        onResetButton = new Button();
        onResetButton.setLabelKey("ctrl.button.label.reset");

        onCancelButton = new Button();
        onCancelButton.setLabelKey("ctrl.button.label.cancel");
        onCancelButton.setAction("/core/dictionary/user/manageUsers");
    }


    
    public void bindData(User user, List roles, List organizations) {
        timeSelectorCreateTime.setTime(user.getCreationDate());
        editBoxUserId.setValue(user.getUserId() == null ? "" : user.getUserId().toString());
        editBoxLogin.setValue(user.getLogin() == null ? "" : user.getLogin());
        editBoxFirstName.setValue(user.getFirstName() == null ? "" : user.getFirstName());
        editBoxEmail.setValue(user.getEmail() == null ? "" : user.getEmail());
        editBoxLastName.setValue(user.getLastName() == null ? "" : user.getLastName());
        editBoxPassword.setValue(user.getPassword() == null ? "" : user.getPassword());
        editBoxPassword2.setValue(user.getPassword() == null ? "" : user.getPassword());
//        checkBoxActive.setSelected(/** if new user */user.getId() == null || /** if existed user */user.getActive() == 1);
 //       checkBoxActive.setSelected(user.getActive() == 1);

        checkBoxActive = (user.getActive() == 1 ? true : false);

        selectRoles.bind(roles);
        selectOrganizations.bind(organizations);

        if (user.getRole() != null && user.getRole().getRoleId() != null)
            selectRoles.setSelectedKey(user.getRole().getId().toString());

    }

    public User populateFromControls(User user) {

        user.setUserId(PopulateUtil.getLongValue(editBoxUserId));
        user.setLogin(editBoxLogin.getValue());
        user.setFirstName(editBoxFirstName.getValue());
        user.setLastName(editBoxLastName.getValue());
        user.setPassword(editBoxPassword.getValue());
        user.setEmail(editBoxEmail.getValue());
 //       user.setActive(checkBoxActive.isSelected() ? 1 : 0);
        user.setActive(checkBoxActive == true ? 1 : 0);
        if(timeSelectorCreateTime.getTime() == null) {
            user.setCreationDate(new Date());
        } else {
            user.setCreationDate(timeSelectorCreateTime.getTime());
        }

        Organization organization = new Organization();
        organization.setOrganizationId(PopulateUtil.getLongKey(selectOrganizations));
        user.setOrganization(organization);

        Role role = new Role();
        role.setRoleId(PopulateUtil.getLongKey(selectRoles));
        user.setRole(role);

        Seller seller = new Seller();
        seller.setSellerId(PopulateUtil.getLongKey(scrollBoxSellers));
        user.setSellerOwner(seller);

        return user;
    }

    //-------------------PROPERTIES
    /*public CheckBox getCheckBoxActive() {
        return checkBoxActive;
    }

    public void setCheckBoxActive(CheckBox checkBoxActive) {
        this.checkBoxActive = checkBoxActive;
    }*/

    public boolean isCheckBoxActive() {
        return checkBoxActive;
    }

    public void setCheckBoxActive(boolean checkBoxActive) {
        this.checkBoxActive = checkBoxActive;
    }

    public boolean is_checkBoxActive() {
        return _checkBoxActive;
    }

    public void set_checkBoxActive(boolean _checkBoxActive) {
        this._checkBoxActive = _checkBoxActive;
    }

    public EditBox getEditBoxFirstName() {
        return editBoxFirstName;
    }

    public void setEditBoxFirstName(EditBox editBoxFirstName) {
        this.editBoxFirstName = editBoxFirstName;
    }

    public EditBox getEditBoxLastName() {
        return editBoxLastName;
    }

    public void setEditBoxLastName(EditBox editBoxLastName) {
        this.editBoxLastName = editBoxLastName;
    }

    public EditBox getEditBoxAdmin() {
        return editBoxAdmin;
    }

    public void setEditBoxAdmin(EditBox editBoxAdmin) {
        this.editBoxAdmin = editBoxAdmin;
    }

    public EditBox getEditBoxUserId() {
        return editBoxUserId;
    }

    public void setEditBoxUserId(EditBox editBoxUserId) {
        this.editBoxUserId = editBoxUserId;
    }

    public EditBox getEditBoxLogin() {
        return editBoxLogin;
    }

    public void setEditBoxLogin(EditBox editBoxLogin) {
        this.editBoxLogin = editBoxLogin;
    }

    public EditBox getEditBoxPassword() {
        return editBoxPassword;
    }

    public void setEditBoxPassword(EditBox editBoxPassword) {
        this.editBoxPassword = editBoxPassword;
    }

    public EditBox getEditBoxActive() {
        return editBoxActive;
    }

    public void setEditBoxActive(EditBox editBoxActive) {
        this.editBoxActive = editBoxActive;
    }

    public ScrollBox getSelectRoles() {
        return selectRoles;
    }

    public void setSelectRoles(ScrollBox selectRoles) {
        this.selectRoles = selectRoles;
    }

    public ScrollBox getSelectOrganizations() {
        return selectOrganizations;
    }

    public void setSelectOrganizations(ScrollBox selectOrganizations) {
        this.selectOrganizations = selectOrganizations;
    }

    public Long getSelectedUserId() {
        //to support form submit on from self on self
        return (selectedUserId != null) ? selectedUserId : PopulateUtil.getLongValue(editBoxUserId);
    }

    public void setSelectedUserId(Long selectedUserId) {
        this.selectedUserId = selectedUserId;
    }

    public EditBox getEditBoxPassword2() {
        return editBoxPassword2;
    }

    public void setEditBoxPassword2(EditBox editBoxPassword2) {
        this.editBoxPassword2 = editBoxPassword2;
    }

    public EditBox getEditBoxEmail() {
        return editBoxEmail;
    }

    public void setEditBoxEmail(EditBox editBoxEmail) {
        this.editBoxEmail = editBoxEmail;
    }

    public TimeSelector getTimeSelectorCreateTime() {
        return timeSelectorCreateTime;
    }

    public void setTimeSelectorCreateTime(TimeSelector timeSelectorCreateTime) {
        this.timeSelectorCreateTime = timeSelectorCreateTime;
    }

    public Button getOnSaveButton() {
        return onSaveButton;
    }

    public void setOnSaveButton(Button onSaveButton) {
        this.onSaveButton = onSaveButton;
    }

    public Button getOnResetButton() {
        return onResetButton;
    }

    public void setOnResetButton(Button onResetButton) {
        this.onResetButton = onResetButton;
    }

    public Button getOnCancelButton() {
        return onCancelButton;
    }

    public void setOnCancelButton(Button onCancelButton) {
        this.onCancelButton = onCancelButton;
    }

    public ScrollBox getScrollBoxSellers() {
        return scrollBoxSellers;
    }

    public void setScrollBoxSellers(ScrollBox scrollBoxSellers) {
        this.scrollBoxSellers = scrollBoxSellers;
    }
}
