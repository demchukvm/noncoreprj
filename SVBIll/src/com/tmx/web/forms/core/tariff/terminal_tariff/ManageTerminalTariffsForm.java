package com.tmx.web.forms.core.tariff.terminal_tariff;

import com.tmx.as.entities.general.user.User;
import com.tmx.as.exceptions.DatabaseException;
import com.tmx.web.controls.BrowseBox;
import com.tmx.web.controls.table.Parameter;
import com.tmx.web.forms.core.tariff.AbstractManageTariffsForm;
import com.tmx.web.forms.core.base.AbstractFormFactory;


public class ManageTerminalTariffsForm extends AbstractManageTariffsForm {

    protected void init(){
        super.init();
        initTariffsTable();
        initButtons();
        initFilters();
    }

    protected void bindSpecificData(User loggedInUser) throws DatabaseException {
//        getTariffsTable().clean();//TODO: go to Roman Markin!!!
        AbstractFormFactory.getInstance(loggedInUser).setUpForm(this);
    }

    private void initTariffsTable() {
        getTariffsTable().loadQuery("query(query_type=retrieve_all," +
                "entity_class=com.tmx.as.entities.bill.tariff.TerminalTariff," +
                "table_attr=functionInstance.functionType," +
                "table_attr=terminal," +
                "order(name=terminalId,type=asc)," +
                "page_size=15" +
                ")");
        getTariffsTable().setRefreshActionName("/core/tariff/terminal_tariff/manageTerminalTariffs");
    }

    private void initFilters(){
        BrowseBox browseBox = new BrowseBox("/core/balance/terminal_balance/browseTerminalsAction");
//        browseBox.setKey("0");//imposible value
        getTariffsTable().addParameterToFilter("by_terminal_id", new Parameter("terminal_id", browseBox));    
    }

    private void initButtons(){
        getOnCreateButton().setAction("/core/tariff/terminal_tariff/manageTerminalTariffs");
        getOnEditButton().setAction("/core/tariff/terminal_tariff/infoTerminalTariff");
        getOnDeleteButton().setAction("/core/tariff/terminal_tariff/infoTerminalTariff");
    }

}
