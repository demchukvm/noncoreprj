package com.tmx.beng.csapi.xml.v1.base;

import com.tmx.beng.base.BillException;
import com.tmx.beng.base.BillingMessage;
import com.tmx.beng.base.BillingMessageImpl;
import com.tmx.gate.csapi.v1.base.beans.RootDocument;
import com.tmx.util.XMLUtil;
import com.tmx.util.i18n.MessageResources;

import java.io.IOException;
import java.util.Locale;

/**
Client XML Protocol Assembler-Disassembler Unit
 */
public class ClientPAD {
    private final static String STATUS_MSGS_BUNDLE = "csapi_statuses";
    /** WEB API errors starts from 301 up to 399 */
    public final static int CSAPI_UNKNOWN_ERROR = 301;
    public final static int CSAPI_CLIENT_INIT_ERROR = 302;    
    public final static int CSAPI_REQUEST_PARSING_ERROR = 303;
    public final static int CSAPI_RESPONSE_SERIALIZE_ERROR = 304;
    public final static int CSAPI_REQUEST_PROCESSING_ERROR = 305;
    public final static int CSAPI_REQUEST_DATA_FORMAT_ERROR = 306;

    /**  Constructor from XML */
    private RootDocument parse0(String is) throws BillException {
        RootDocument rootDoc = null;
        if (is == null) {
            throw new BillException("err.csapi.message_is_null");
        }

        try{
            // Bind the instance to the generated XMLBeans types.
            rootDoc = RootDocument.Factory.parse(is);
        }
        catch(Throwable e){
            throw new BillException("err.csapi.failed_to_parse_message", e);
        }
        return rootDoc;
    }


    public ParsingResult parseRequest(String messageSrc) throws BillException{

        RootDocument root = parse0(messageSrc);
        BillingMessageImpl billMsg = new BillingMessageImpl();

        ParsingResult parsingResult = new ParsingResult();
        parsingResult.setBillingMessage(billMsg);

        // get version
        parsingResult.setVersion(root.getRoot().getRequest().getProtocol());

        // validate
        if(parsingResult.getVersion().equals("1.0")){
            new VerifyV10().verify(root, billMsg);
        }else if(parsingResult.getVersion().equals("1.1")){
            new VerifyV11().verify(root, billMsg);
        }else{
            throw new BillException("err.csapi.unsupported_protocol", new String[]{ parsingResult.getVersion()});
        }
        return parsingResult;
    }

    private String serializeResponse(BillingMessage msg, String version) throws IOException {
        RootDocument root = RootDocument.Factory.newInstance();

        if("1.0".equals(version)){
            new VerifyV10().serialize(root, msg);
        }else if("1.1".equals(version)){
            new VerifyV11().serialize(root, msg);
        }
        return XMLUtil.serializeDOM(root.getRoot().getDomNode().getOwnerDocument());
    }

    public String safeSerializeResponse(BillingMessage msg, String version){
        String serializedMessage = null;
        try{
            serializedMessage = serializeResponse(msg, version);
        }
        catch(Throwable e){
            e.printStackTrace();
            serializedMessage = buildGeneralXmlErrorResponse(CSAPI_RESPONSE_SERIALIZE_ERROR, Locale.ENGLISH);
        }
        return serializedMessage;        
    }

    public String buildErrorResponse(int errorCode, Locale locale){
        return buildGeneralXmlErrorResponse(errorCode, locale);
    }

    private String resolveStatusMessage(int code, Locale locale){
        return getLocalizedMessage("status."+code, null, locale);
    }

    private String getLocalizedMessage(String key, String[] params, Locale locale){
        return MessageResources.getInstance().getOwnedLocalizedMessage(this.getClass(), STATUS_MSGS_BUNDLE, locale, key, params);
    }
    
    private String buildGeneralXmlErrorResponse(int errorCode, Locale locale){
        String localaizedStatusMsg = resolveStatusMessage(errorCode, locale);
        StringBuffer error = new StringBuffer();
        error.append("<?xml version='1.0' encoding='UTF-8'?>").
                append("<root xmlns='http://tmx.com/gate/csapi/v1/base/beans'>").
                append("    <response protocol='1.0'>").
                append("        <operationsResults>").
                append("            <generalResponse>").
                append("                <statusCode>").append(errorCode).append("</statusCode>").
                append("                <statusDesc>").append(localaizedStatusMsg).append("</statusDesc>").
                append("            </generalResponse>").
                append("        </operationsResults>").
                append("    </response>").
                append("</root>");
      return error.toString();
    }


    /** Class implements the parsing result */
    public class ParsingResult {
        private BillingMessage billingMessage;
        private String version;

        public BillingMessage getBillingMessage() {
            return billingMessage;
        }

        public void setBillingMessage(BillingMessage billingMessage) {
            this.billingMessage = billingMessage;
        }

        public String getVersion() {
            return version;
        }

        public void setVersion(String version) {
            this.version = version;
        }
    }
}
