<%@ page language="java" pageEncoding="UTF-8" contentType="text/html; charset=utf-8" isELIgnored="false" %>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-nested" prefix="nested" %>
<%@ taglib uri="http://tmx.com/taglib/tmx-controls" prefix="tmx_ctrl" %>
<%@ taglib uri="http://tmx.com/taglib/tmx-logic" prefix="tmx_logic" %>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html" %>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean" %>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>

<table width="100%" border="0">
    <tr>
        <td style="text-align: left">
            <tmx_ctrl:button property="onCreateButton"/>
            <tmx_ctrl:button property="onEditButton"/>
            <tmx_ctrl:button property="onDeleteButton"/>
        </td>
    </tr>

    <tr>
        <td valign="top" width="100%" align="left">
            <tmx_ctrl:panelFilterSet tableName="terminalsTable">
                <table>
                    <tr>
                        <td class="dataLabel">
                            <bean:message bundle="core.labels" key="manageTerminals.label.serialNumber"/>:
                        </td>
                        <td class="dataField">
                            <tmx_ctrl:editBox property="editBoxBySerialNumber"/>
                        </td>

                        <td class="dataLabel">
                            <bean:message bundle="core.labels" key="manageTerminals.label.sellerCode"/>:
                        </td>
                        <td class="dataField">
                            <tmx_ctrl:selectBox property="scrollBoxSellerOwner"/>
                        </td>
                        <td class="dataLabel">
                            <bean:message bundle="core.labels" key="manageTerminals.label.terminalType"/>:
                        </td>
                        <td class="dataField">
                            <tmx_ctrl:selectBox property="scrollBoxEquipmentType"/>
                        </td>

                        <td class="dataLabel">
                            <bean:message bundle="core.labels" key="manageTerminals.label.blocked"/>
                        </td>

                        <td class="dataField">
                            <tmx_ctrl:checkBox property="terminalsTable.filterSet.filter(by_blocked).filterEnabler" />
                        </td>
                    </tr>
                </table>
			</tmx_ctrl:panelFilterSet>
        </td>
    </tr>



    <!-- TABLE -->
    <tr>
        <td width="100%">
            <table class='one_outer_cell_table' cellspacing='0' cellpadding='0' width="100%">
                <tr>
                    <td>
                        <table cellspacing='1' cellpadding='1' width="100%">
                            <!-- Table header -->
                            <tr>
                                <th class="column_header_cell" width="20px">
                                    <bean:message bundle="core.labels" key="th.choice"/>
                                </th>

                                <th class="column_header_cell">
                                    <tmx_ctrl:imgTableOrdering tableName="terminalsTable"
                                                               orderByFieldName="serialNumber"
                                                               labelBundle="as:com.tmx.as.entities.bill.terminal.Terminal"
                                                               labelKey="th.serialNumber"/>
                                </th>

                                <%--<th class="column_header_cell">--%>
                                    <%--<tmx_ctrl:imgTableOrdering tableName="terminalsTable"--%>
                                                               <%--orderByFieldName="equipmentType.name"--%>
                                                               <%--labelBundle="as:com.tmx.as.entities.bill.terminal.Terminal"--%>
                                                               <%--labelKey="th.terminalType"/>--%>
                                <%--</th>--%>

                                <th class="column_header_cell">
                                    <tmx_ctrl:imgTableOrdering tableName="terminalsTable"
                                                               orderByFieldName="registrationDate"
                                                               labelBundle="as:com.tmx.as.entities.bill.terminal.Terminal"
                                                               labelKey="th.registrationDate"/>
                                </th>

                                <th class="column_header_cell">
                                    <tmx_ctrl:imgTableOrdering tableName="terminalsTable"
                                                               orderByFieldName="blocked"
                                                               labelBundle="as:com.tmx.as.entities.bill.terminal.Terminal"
                                                               labelKey="th.blocked"/>
                                </th>

                                <th class="column_header_cell">
                                    <tmx_ctrl:imgTableOrdering tableName="terminalsTable"
                                                               orderByFieldName="sellerOwner.name"
                                                               labelBundle="as:com.tmx.as.entities.bill.terminal.Terminal"
                                                               labelKey="th.assignedTo"/>
                                </th>

                            </tr>
                            <nested:iterate id="terminal" property="terminalsTable.data" type="com.tmx.as.entities.bill.terminal.Terminal">
                                <tr>
                                    <td class='column_data_cell'>
                                        <input type="radio" name="selectedTerminalId" value="<c:out value="${terminal.id}" />"
                                               checked="false">
                                    </td>
                                    <td class='column_data_cell'>
                                        <c:out value="${terminal.serialNumber}" />
                                    </td>
                                    <%--<td class='column_data_cell'>--%>
                                        <%--<c:if test="${terminal.equipmentType!= null}">--%>
                                            <%--<c:out value="${terminal.equipmentType.name}" />--%>
                                        <%--</c:if>--%>
                                        <%--<c:if test="${terminal.equipmentType== null}">--%>
                                            <%-----%>
                                        <%--</c:if>--%>
                                    <%--</td>--%>
                                    <td class='column_data_cell'>
                                        <fmt:formatDate value="${terminal.registrationDate}" pattern="yyyy/MM/dd HH:mm:ss"/>
                                    </td>
                                    <td class='column_data_cell'>
                                        <tmx_ctrl:checkSign name="terminal" property="blocked"/>
                                    </td>
                                    <td class='column_data_cell'>
                                        <c:if test="${terminal.sellerOwner!= null}">
                                            <c:out value="${terminal.sellerOwner.name}" />
                                        </c:if>
                                    </td>
                                </tr>
                            </nested:iterate>
                            <!-- Empty row -->
                            <nested:define id="tableSize" property="terminalsTable.resultSize" type="java.lang.Integer"/>
                            <c:if test="${tableSize <= 0}">
                                <tr>
                                    <td class='column_data_cell' colspan="7" align="center">
                                        <bean:message bundle="core.labels" key="label.emptyTable" />
                                    </td>
                                </tr>
                            </c:if>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td style="text-align: right">
            <tmx_ctrl:imgTablePaging maxBookmarkCount="10" tableName="terminalsTable"/>
        </td>
    </tr>
    <!-- BUTTONS -->
    <tr>
        <td style="text-align: left">
            <tmx_ctrl:button property="onCreateButton"/>
            <tmx_ctrl:button property="onEditButton"/>
            <tmx_ctrl:button property="onDeleteButton"/>
        </td>
    </tr>
</table>
<%--</html:form>--%>

