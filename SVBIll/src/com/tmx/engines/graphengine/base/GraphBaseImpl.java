package com.tmx.engines.graphengine.base;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.apache.log4j.Logger;
import com.sun.org.apache.xpath.internal.XPathAPI;
import org.apache.xmlbeans.XmlOptions;
import com.tmx.util.XMLUtil;
import com.tmx.util.StringUtil;
import com.tmx.engines.graphengine.beans.RootDocument;

import java.net.URL;
import java.io.IOException;
import java.util.Iterator;
import java.util.ArrayList;

/**
 */
public abstract class GraphBaseImpl implements Graph {
    protected Logger logger = Logger.getLogger(GraphService.LOGCLASS_GRAPH + "." + getClass().getName());
    protected RootDocument rootDoc = null;
    protected GraphService.GraphConfig config = null;


    public void validate() throws GraphException{
        try {
            ArrayList errors = new ArrayList();
            XmlOptions opts = new XmlOptions();
            opts.setErrorListener(errors);
            if(!rootDoc.validate(opts)){
                String graphName = ((config != null) ? config.getName() : null);
                StringBuffer errorBuffer = new StringBuffer("Validation failed of Graph "+graphName+":\n");
                Iterator iter = errors.iterator();
                while(iter.hasNext())
                    errorBuffer.append("   >> " + iter.next() + "\n");
                logger.error(errorBuffer.toString());
                throw new GraphException("err.run.graph.template_validation_failed", new String[]{graphName});
            }
        }
        catch (Exception e) {
            //RM put into log
            e.printStackTrace();
            throw new GraphException("err.graph.xml_doc_serialization_failed", e);
        }
    }


    public String serialize() throws GraphException{
        String docString = null;
        try {
            docString = StringUtil.removeReturnSymbols(rootDoc.toString());
        }
        catch (Exception e) {
            //RM put into log
            e.printStackTrace();
            throw new GraphException("err.graph.xml_doc_serialization_failed", e);
        }
        return docString;
    }

    
    public RootDocument getRootDocument(){
        return rootDoc;
    }


    public void init(GraphService.GraphConfig config, RootDocument rootDoc) throws GraphException{
        this.config = config;
        this.rootDoc = rootDoc;
    }

}
