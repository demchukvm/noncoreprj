package com.tmx.as.jndi;

import com.tmx.as.base.Reconfigurable;
import com.tmx.util.InitException;
import com.tmx.util.Configuration;
import com.tmx.util.XMLUtil;
import com.sun.org.apache.xpath.internal.XPathAPI;
import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import javax.naming.*;
import java.util.*;
import java.io.File;

/**
 */
public class JndiService implements Reconfigurable {
    /** Only single instance available */
    private static JndiService jndiService;
    private Config serviceConfig = null;

    public static String LOGCLASS_REPORT = "jndi";
    /** Logger */
    private static Logger logger = Logger.getLogger(LOGCLASS_REPORT + ".JndiService");

    private JndiService(){
    }


    public static JndiService getInstance(){
        if(jndiService == null)
            jndiService = new JndiService();

        return jndiService;
    }

    public void reload() throws InitException {
        reloadConfig();

        // Initial environment with various properties
        Enumeration enumeration = serviceConfig.getEnvProps().keys();
        while(enumeration.hasMoreElements()){
            String key = (String)enumeration.nextElement();
            System.setProperty(key, serviceConfig.getEnvProps().getProperty(key));
        }

        InitialContext ctx = null;
        try{
            ctx = new InitialContext();

            for(Iterator iter = serviceConfig.getResources().keySet().iterator(); iter.hasNext();){
                String resourceJndiName = (String)iter.next();
                Config.Resource resource = (Config.Resource)serviceConfig.getResources().get(resourceJndiName);
                //JNDI implementation specific setup
                Object wrappedResource = null;
                if("com.sun.jndi.fscontext.RefFSContextFactory".equals(serviceConfig.getEnvProps().getProperty(Context.INITIAL_CONTEXT_FACTORY))){
                    wrappedResource = setupFscontext(resource);
                }
                ctx.rebind(resource.getJndiName(), wrappedResource);
                logger.info("Succesfully bound object '"+resource.getType()+"' using factory '"+resource.getFactory()+"' under JNDI name '"+resource.getJndiName()+"'");
            }

            logger.debug("Successful completed initialization of the JNDI service");
        }
        catch(NamingException e){
            throw new InitException("err.jndi_service.failed_to_rebind_initial_context", e);
        }
    }

    private void reloadConfig() throws InitException{
        try {
            logger.debug("Start initialization of the JNDI Service");
            String jndiConfigFile = Configuration.getInstance().getProperty("jndi_service.config_file");
            if (jndiConfigFile == null)
                throw new InitException("err.jndi_service.config_file_is_not_specified", Locale.getDefault());

            //Load config from XML
            Document doc = XMLUtil.getDocumentFromFile(jndiConfigFile);

            //String xpathJndiService = "/jndi";
            //Node jndiServiceNode = XPathAPI.selectSingleNode(doc, xpathJndiService);
            Config localConfig = new Config();

            //retrieve environment props
            String xpathEnvProps = "/jndi/environment/property";
            NodeList envPropsNodes = XPathAPI.selectNodeList(doc, xpathEnvProps);
            for (int i = 0; i < envPropsNodes.getLength(); i++) {
                Node propNode = envPropsNodes.item(i);
                if (propNode.getNodeType() == Node.ATTRIBUTE_NODE)
                    continue;

                localConfig.getEnvProps().setProperty(((Element)propNode).getAttribute("name"),
                                                      ((Element)propNode).getAttribute("value") != null ? Configuration.getInstance().substituteVariablesInString(((Element)propNode).getAttribute("value")) : null);
            }


            //retrieve global naming resources
            String xpathGlobalNamingRes = "/jndi/globalNamingResources/resource";
            NodeList glbNamingResNodes = XPathAPI.selectNodeList(doc, xpathGlobalNamingRes);
            for (int i = 0; i < glbNamingResNodes.getLength(); i++) {
                Node resNode = glbNamingResNodes.item(i);
                if (resNode.getNodeType() == Node.ATTRIBUTE_NODE)
                    continue;

                Config.Resource resource = localConfig.new Resource();
                resource.setJndiName(((Element)resNode).getAttribute("jndiName"));
                resource.setType(((Element)resNode).getAttribute("type"));
                resource.setFactory(((Element)resNode).getAttribute("factory"));

                //retrieve resource properties
                String xpathResParams = "/jndi/globalNamingResources/resource[@jndiName=\""+resource.getJndiName()+"\"]/parameter";
                NodeList resParamsNodes = XPathAPI.selectNodeList(doc, xpathResParams);

                for (int j = 0; j < resParamsNodes.getLength(); j++) {
                    Node paramNode = resParamsNodes.item(j);
                    if (paramNode.getNodeType() == Node.ATTRIBUTE_NODE)
                        continue;

                    resource.getParameters().setProperty(((Element)paramNode).getAttribute("name"),
                                                         ((Element)paramNode).getAttribute("value")
                            );
                }


                //add resource
                localConfig.getResources().put(resource.getJndiName(), resource);
            }

            //set reloaded config
            serviceConfig = localConfig;
            logger.debug("Successfully reload configuration of the JNDI service");

        }
        catch (InitException e) {
            //rethrow
            throw e;
        }
        catch (Exception e) {
            throw new InitException("err.jndi_service.reload_failed", e, Locale.getDefault());
        }


//-----------
//        try {
//            logger.debug("Start initialization of the JNDI Service");
//            String jndiConfigFile = Configuration.getInstance().getProperty("jndi_service.config_file");
//            if (jndiConfigFile == null)
//                throw new InitException("err.jndi_service.config_file_is_not_specified");

//            File configFile = new File(jndiConfigFile);
//            Digester digester = new Digester();
//            JndiService localService = new JndiService();
//            digester.push(localService);
//            addRules(digester);
//            digester.parse(configFile);
//            jndiService = (JndiService)digester.getRoot();

//
//            logger.debug("Successful completed initialization of the JNDI Service");
//
//        }
//        catch (InitException e) {
//            //rethrow
//            throw e;
//        }
//        catch (Exception e) {
//            throw new InitException("err.jndi_service.reload_failed", e);
//        }
    }

    //---------------private methods
//    private void addRules(Digester d){
////        d.addSetProperties("jndi");
////        d.addSetNestedProperties("jndi/environment");
//
//        d.addObjectCreate("jndi/environment/property", EnvironmentProperty.class);
//        d.addSetProperties("jndi/environment/property");
//        d.addSetNext("jndi/environment", "addEnvironmentProperty", "com.tmx.as.jndi.EnvironmentProperty");
//
//        //d.addSetNext("jndi/globalNamingResources", "set", "com.tmx.as.jndi.GlobalNamingResources");
//
//    }

//    public void addEnvironmentProperty(EnvironmentProperty environmentProperty) {
//        environmentProps.put(environmentProperty.getName(), environmentProperty);
//    }

    /** Return wrapped resource to be bound in context */
    private Object setupFscontext(Config.Resource resource){
        // Construct BasicDataSource reference
        Reference ref = new Reference(resource.getType(), resource.getFactory(), null);

        Enumeration propNames = resource.getParameters().propertyNames();
        while(propNames.hasMoreElements()){
            String name = (String)propNames.nextElement();
            ref.add(new StringRefAddr(name, resource.getParameters().getProperty(name)));
        }
        return ref;
    }

    private class Config{
        private Properties envProps = new Properties();
        private Map resources = new HashMap();

        public Properties getEnvProps() {
            return envProps;
        }

        public Map getResources() {
            return resources;
        }

        private class Resource{
            private String jndiName = null;
            private String type = null;
            private String factory = null;
            private Properties parameters = new Properties();

            public String getJndiName() {
                return jndiName;
            }

            private void setJndiName(String jndiName) {
                this.jndiName = jndiName;
            }

            public String getType() {
                return type;
            }

            private void setType(String type) {
                this.type = type;
            }

            public String getFactory() {
                return factory;
            }

            private void setFactory(String factory) {
                this.factory = factory;
            }

            public Properties getParameters() {
                return parameters;
            }
        }
    }


}
