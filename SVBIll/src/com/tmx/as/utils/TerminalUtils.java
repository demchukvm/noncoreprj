package com.tmx.as.utils;

import com.tmx.util.InitException;
import com.tmx.util.Configuration;
import com.tmx.as.blogic.TerminalManager;
import com.tmx.as.exceptions.DatabaseException;
import org.apache.log4j.PropertyConfigurator;
import org.apache.log4j.Logger;

/**
 * Util class for terminals
 */
public class TerminalUtils {

    public static void main(String args[]) throws InitException, DatabaseException {
        String tmxHome = System.getProperty("tmx.home");
        if (tmxHome == null) {
            throw new InitException("System property 'tmx.home' is not set");
        }

        //2. Initiate properties loading
        Configuration config = Configuration.getInstance();

        addBalances();
    }

    /**
     * Method add primary balance to Termilans withaut it
     * @throws DatabaseException
     */
    private static void addBalances() throws DatabaseException {
        new TerminalManager().nonPermanentUpdateTerminals();
    }
}
