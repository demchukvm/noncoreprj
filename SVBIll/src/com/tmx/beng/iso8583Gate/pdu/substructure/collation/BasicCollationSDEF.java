package com.tmx.beng.iso8583Gate.pdu.substructure.collation;

import com.tmx.beng.iso8583Gate.pdu.substructure.BasicSubDEFormat;
import com.tmx.beng.iso8583Gate.pdu.substructure.RuntimeSubDataElement;
import com.tmx.beng.iso8583Gate.pdu.RuntimeDataElement;
import com.tmx.beng.iso8583Gate.pdu.IsoParseException;
import com.tmx.beng.iso8583Gate.pdu.IsoSerializeException;
import com.tmx.beng.iso8583Gate.pdu.types.AlphabeticNumericSpecialCharactersType;
import com.tmx.beng.iso8583Gate.pdu.types.Type;
import com.tmx.beng.iso8583Gate.pdu.length_types.LLLVARLengthType;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Set;
import java.util.HashSet;
import java.util.Arrays;

/**
    Sub data element format for collation operations 
 */
public class BasicCollationSDEF extends BasicSubDEFormat {
    public static final String TAG_NAME = "TAG_NAME";
    public static final String SALES_COUNT = "SALES_COUNT";
    public static final String SALES_AMOUNT = "SALES_AMOUNT";
    public static final String VOID_COUNT = "VOID_COUNT";
    public static final String VOID_AMOUNT = "VOID_AMOUNT";
    /** define names, sequnce and length of entire elements */
    private static String[] names = new String[]{TAG_NAME, SALES_COUNT, SALES_AMOUNT, VOID_COUNT, VOID_AMOUNT};
    private static int[] lengts = new int[]{2, 3, 12, 3, 12};



    public void parse(RuntimeDataElement rde) throws IsoParseException {
        RuntimeSubDataElement runtimeSDE = prepare(rde);
        byte[] sourceBytes = rde.getString().getBytes();
        ByteArrayInputStream sourceBais = new ByteArrayInputStream(sourceBytes);
        LLLVARLengthType lengthType = new LLLVARLengthType();
        int actualLength = lengthType.parseLength(sourceBais, 3);
        AlphabeticNumericSpecialCharactersType ansType = new AlphabeticNumericSpecialCharactersType();
        for(int i=0; i<names.length && sourceBais.available() > 0; i++)
            runtimeSDE.addValue(names[i], ansType.parse(sourceBais, lengts[i]));
    }

    public void serialize(RuntimeDataElement rde) throws IsoSerializeException {
        RuntimeSubDataElement runtimeSDE = rde.getSubstructureDataElement();
        if(!(runtimeSDE instanceof BasicCollationRSDE))
            throw new IsoSerializeException("err.collation_sdef.incorrect_rsde_type", new String[]{runtimeSDE.getClass().toString()});

        BasicCollationRSDE rsde = (BasicCollationRSDE)runtimeSDE;
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        LLLVARLengthType lengthType = new LLLVARLengthType();
        AlphabeticNumericSpecialCharactersType ansType = new AlphabeticNumericSpecialCharactersType();

        try{
            byte[] lengthData = lengthType.serializeLength(evalLength());
            baos.write(lengthData);
            for(int i=0; i<lengts.length; i++){
                Type.SerializedValue serializedEL = ansType.serialize(true, lengts[i], rsde.getValue(names[i]));
                baos.write(serializedEL.data);
            }
        }
        catch(IOException e){
            throw new IsoSerializeException("err.collation_sdef.serialization_failed", e);
        }
        rde.setValue(new String(baos.toByteArray()));
    }

    public RuntimeSubDataElement getRuntimeSubDataElementInstance() {
        return new BasicCollationRSDE();
    }

    static Set getAvailableElementNames(){
        return new HashSet(Arrays.asList(names));
    }

    private int evalLength(){
        int length = 0;
        for(int i=0; i<lengts.length; i++)
            length = length + lengts[i];
        return length;
    }

}
