package com.tmx.as.entities.bill.transaction;

import java.util.Date;

/**
    Because of some reasons transaction proggress could be suspensed and then resumed.
    The reason could be the fault of external system or media which is
    crushed transaction. Or that could be done by business logic of our billing.
    Such transaction is called <code>DelayedTransaction</code>. Delayed transaction has
    reference onto <code>ClientTransaction</code> which was delayed (by <code>origTransactionGUID</code>),
    attribute to schedule resume time and to reflect timestamps of actual suspend/resume events. If
    <code>DelayedTransaction</code> created
    for <code>ClientTransaction</code> last one should be marked with DELAYED status.
    <br/>
    <code>DelayedTransaction</code> could be also delayed. In this case no additional
    <code>DeleydTransaction</code> instances created, but status of this dealayed instance is
    updated with one of the values which are mean the transaction is not in final state and should be scheduled to
    process again.
    <br/>
    In common case the logic of production the <code>DelayedTransaction</code> is places in special pipeline
    processor or pipeline rollback code.
    <br/>
    Delayed transactions are observed by scheduler until the time of resuming come. Then scheduler via
    <code>DelayedTransactionProcessingThread</code> instantiates transaction and send corresponding message into
    billing engine.
    <br/>
    <code>DelayedTransaction</code> should has a new unique <code>origTransactionGUID</code> because
    if this transaction is created on original transaction fault the external system
    which is process it could require a new <code>origTransactionGUID</code>.
    <br/>
    On <code>DelayedTransaction</code> processing the corresponding supplementary transactions are
    created: <code>OperatorBalanceTransaction</code>, <code>SellerBallanceTransaction</code>,
    <code>TerminalBalanceTransaction</code>.

*/
public class DelayedTransaction extends ClientTransaction{
    /** Reference by origTransactionGUID onto the original client transaction.
     * The transaction which is delayed. */
    private String origTransactionGUID;
    /** The time when transaction should be resumed to processing */
    private Date scheduledToResume;
    /** Flag to identify is transaction finished in any way (fatal error or  success processing) */
    private Boolean finished;

    public String getOrigTransactionGUID() {
        return origTransactionGUID;
    }

    public void setOrigTransactionGUID(String origTransactionGUID) {
        this.origTransactionGUID = origTransactionGUID;
    }

    public Date getScheduledToResume() {
        return scheduledToResume;
    }

    public void setScheduledToResume(Date scheduledToResume) {
        this.scheduledToResume = scheduledToResume;
    }

    public Boolean getFinished() {
        return finished;
    }

    public void setFinished(Boolean finished) {
        this.finished = finished;
    }
}
