package com.tmx.web.controls.table;

import com.tmx.as.exceptions.DatabaseException;
import com.tmx.as.resolver.FunctionSyntaxException;
import com.tmx.as.resolver.ResolverException;
import com.tmx.web.controls.BasicControl;

import java.util.*;


public class ControlManagerTable extends Table{

    private ArrayList dependedControls = new ArrayList();

    public ControlManagerTable(String query) {
        super(query);
    }

    public ControlManagerTable() {
        super();
    }

    public void refresh() throws ResolverException, FunctionSyntaxException, DatabaseException {
        super.refresh();
        disableControls(!(getResultSize().intValue()>0));
    }

    public void setData(List data) {
        super.setData(data);
        resultSize = data.size();
        disableControls(!(getResultSize().intValue()>0));
    }

    public void addData(Collection data) {
        getData().addAll(data);
        resultSize = getData().size();
        disableControls(!(getResultSize().intValue()>0));
    }

    public void removeData(Collection data) {
        getData().removeAll(data);
        resultSize = getData().size();
        disableControls(!(getResultSize().intValue()>0));
    }

    private void disableControls(boolean enable){
        for (Object dependedControl : dependedControls) {
            ((BasicControl) dependedControl).setReadonly(enable);
        }
    }

    public ControlManagerTable addDependedControl(BasicControl control){
        dependedControls.add(control);
        return this;
    }

    /**
     * @param filterName - filter name
     * @return null if filter is absent
     */
    public Filter getFilter(String filterName){
        return getFilterSet().getFilter(filterName);
    }

    /**
     * @param filterName - filterName
     * @param parameterName - parameter name
     * @return null if parameter or filter is absent
     */
    public Parameter getParameter(String filterName, String parameterName){
        if(getFilter(filterName) != null)
            return getFilter(filterName).getParameter(parameterName);
        else return null;
    }

    public void addFilter(Filter filter){
        if(filter!=null && filter.getName()!=null)
            getFilterSet().addFilter(filter);
    }

    public void addFilter(String filterName){
        Filter filter = new Filter(filterName);
        getFilterSet().addFilter(filter);
    }

    public void addParameterToFilter(String filterName, Parameter parameter){
        if(filterName!=null && parameter!=null){
            if(getFilter(filterName) != null)
                getFilter(filterName).addParameter(parameter);
            else{
                Filter filter = new Filter(filterName);
                filter.addParameter(parameter);
                getFilterSet().addFilter(filter);
            }
        }
    }

    public void addParameterToFilter(String filterName, FilterEnabler enabler){
            if(getFilter(filterName) != null){
//                getFilter(filterName).addParameter(parameter);
                getFilter(filterName).setFilterEnabler(enabler);
            } else {
                Filter filter = new Filter(filterName);
//                filter.addParameter(parameter);
                filter.setFilterEnabler(enabler);
                getFilterSet().addFilter(filter);
            }
    }

    public void addParameterToFilter(String filterName, String parameterName, BasicControl control){
        if(control!=null && parameterName!=null){
            addParameterToFilter(filterName, new Parameter(parameterName, control));
        }
    }

    public BasicControl getParameterControl(String filterName, String parameterName){
        return getParameter(filterName, parameterName).getControl();
    }

}
