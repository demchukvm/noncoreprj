package com.tmx.beng.httpsgate.avancel.mapping;

import java.util.Map;
import java.util.HashMap;

/**
 */
public class OperationMapping {
    private String name;
    private Map<String,String> map;

    public OperationMapping() {
        this.map = new HashMap<String,String>();
    }

    public void setName(String name){
        this.name = name;
    }

    public String getName() {
        return name;                                        
    }

    public void addServiceMapping(String svbillCode, String avancelCode){
        map.put(svbillCode, avancelCode);
    }

    public Map<String, String> getMap() {
        return map;
    }
}
