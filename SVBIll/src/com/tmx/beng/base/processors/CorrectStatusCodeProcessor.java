package com.tmx.beng.base.processors;

import com.tmx.beng.base.*;
import com.tmx.beng.base.cache.DBCache;

/**
 Join to opened transaction.
 Used when
 (1) process responds from operators and
 (2) status requests from client.
 */
public class CorrectStatusCodeProcessor extends TaskProcessor {

    public Task process0(Task task, DBCache dbCache, BillingMessageWritable msg) throws BillException {
        logger.debug("Correcting status code...");

        msg.setStatusCode(
                CodeWrapping.getInstance().getExternalCode(
                        new Long(msg.getAttributeString(BillingMessageWritable.STATUS_CODE)),
                        configFile
                ).intValue()
        );
        return task;
    }
}
