package com.tmx.beng.httpsgate.umc;

import com.tmx.util.queue.ActionQueueElement;
import com.tmx.beng.base.BillingMessage;
import com.tmx.beng.httpsgate.HttpsGateException;
import org.apache.log4j.Logger;

/**
    Gate task. Initiated from gate via medium connection.
    Processed by asynchronous thread.
 */
public class UmcHttpsGateTask extends ActionQueueElement {
    private BillingMessage procesedMessage = null;
    private static Logger logger = Logger.getLogger("gate.HttpsTask");

    public UmcHttpsGateTask(BillingMessage billingMessage){
        this.procesedMessage = billingMessage;
    }

    public void run() {
        try{
            UmcGate.getInstance().runBillMessage(procesedMessage);
        }
        catch(HttpsGateException e){
            logger.error(e.getLocalizedMessage(), e);
        }
    }
}
