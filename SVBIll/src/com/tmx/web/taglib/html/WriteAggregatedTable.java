package com.tmx.web.taglib.html;

import org.apache.struts.taglib.TagUtils;
import org.apache.struts.taglib.nested.NestedPropertyHelper;

import javax.servlet.jsp.JspException;
import javax.servlet.http.HttpServletRequest;

import com.tmx.engines.reportengine.aggrtable.*;
import com.tmx.engines.reportengine.aggrtable.beans.Fact;
import com.tmx.engines.reportengine.aggrtable.beans.LookAndFeel;
import com.tmx.engines.reportengine.aggrtable.beans.CommonStyle;
import com.tmx.engines.reportengine.base.ReportException;
import com.tmx.util.i18n.MessageResources;

import java.util.Iterator;
import java.util.Map;
import java.util.HashMap;
import java.util.Arrays;

/**
 * Write aggregated table
 */
public class WriteAggregatedTable extends com.tmx.web.taglib.base.LocalizableTag/** extends BaseHandlerTag */
{
    /**
     * The name of aggregated table. Should be the same as used in form bean
     */
    private String tableName = null;
    /**
     * Form bean name
     */
    private String name = null;

    //-------Properties

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    //------------------------------Parsing events
    public int doStartTmxTag() throws JspException {
        String tableHTML = renderAggregatedTableHTML();
        TagUtils.getInstance().write(pageContext, tableHTML);
        return (EVAL_BODY_TAG);
    }

    //------------------------------Rendering methods

    private String renderAggregatedTableHTML() throws JspException {
        AggregatedTable table = getAggregatedTable();
        StringBuffer resultHTML = new StringBuffer();

        try {
            if (table != null && table.isLoaded()) {
                //prepare lnf
                LookAndFeel lnf = table.getLookAndFeel();
                Map rowHeaderLnF = organizeHeaderRowsLnF(lnf.getRowHeader().getHeaderRowArray());
                Map colHeaderLnF = organizeHeaderRowsLnF(lnf.getColumnHeader().getHeaderRowArray());
                Map factHeaderLnF = organizeDataLnF(lnf.getFactHeader().getFactArray());
                Map factDataLnF = organizeDataLnF(lnf.getData().getFactArray());
                Map aggrFunctionLnF = organizeAggrFunctionLnF(lnf.getAggregateFunctions().getFunctionArray());
                final String NO_DATA = getLocalizedLabel("tag.writeAggregatedTable.msg.no_data", null);
                final String VALIDATION_ERROR = getLocalizedLabel("tag.writeAggregatedTable.msg.validation_error", null);
                String aggrFunctionPositionCol = lnf.getAggregateFunctions().getAggrFunctionStyle().getAggrFunctionPositionCol();
                String aggrFunctionPositionRow = lnf.getAggregateFunctions().getAggrFunctionStyle().getAggrFunctionPositionRow();
                Map positionAggrColLnF = organizePositionAggrColLnF(lnf.getAggregateFunctions().getFunctionArray());
                Map positionAggrRowLnF = organizePositionAggrRowLnF(lnf.getAggregateFunctions().getFunctionArray());

                //css style
                resultHTML.append("<style type='text/css'>");
                resultHTML.append("<!--");

                resultHTML.append(".one_outer_cell_table");
                resultHTML.append("{");
                resultHTML.append("background-color:" + lnf.getGrid().getColour() + ";");
                resultHTML.append("}");

                renderCommonStyle(resultHTML, ".column_header_cell", lnf.getColumnHeader().getCommonStyle());
                renderCommonStyle(resultHTML, ".row_header_cell", lnf.getRowHeader().getCommonStyle());
                renderCommonStyle(resultHTML, ".fact_header_cell", lnf.getFactHeader().getCommonStyle());
                renderCommonStyle(resultHTML, ".fact_data_cell", lnf.getData().getCommonStyle());
                renderCommonStyle(resultHTML, ".agg_func_cell", lnf.getAggregateFunctions().getCommonStyle());

                resultHTML.append("-->");
                resultHTML.append("</style>");

                //one outer cell
                resultHTML.append("<table class='one_outer_cell_table' cellspacing='" + lnf.getGrid().getOuterLineSize() + "' cellpadding='" + lnf.getGrid().getOuterLineSize() + "'>");
                resultHTML.append("<tr>");
                resultHTML.append("<td>");

                resultHTML.append("<table cellspacing='" + lnf.getGrid().getInnerLineSize() + "' cellpadding='" + lnf.getGrid().getInnerLineSize() + "'>");

                //----------add columns header set
                resultHTML.append("<tr>");
                //add first cell (use the same look and feel like for data cell)
                resultHTML.append("<td class='fact_data_cell' colspan='" + table.getFirstCellColspan() + "' rowspan='" + table.getFirstCellRowspan() + "'>");
                resultHTML.append("&nbsp;");
                resultHTML.append("</td>");

                //-----------add column headers
                for (int rowIdx = 0; rowIdx < table.getColumnHeaderSet().getRowsCount(); rowIdx++) {
                    if (rowIdx != 0)
                        resultHTML.append("<tr>");

                    String style = "";
                    String positionAggr = "";

                    //---add cell with row Aggr funct name
                    if(rowIdx == 0){
                        for (int aggrFuncIdx = 0; aggrFuncIdx < table.getRowAggrFunctionSet().getFunctionsCount(); aggrFuncIdx++){
                            //----add cell with function name
                            int aggrFuncNameCellRowSpan = table.getColumnHeaderSet().getRowsCount();
                            if ("column".equals(lnf.getFactHeader().getPosition()))
                                aggrFuncNameCellRowSpan++;//If fact row oriented, then colspan++

                            AggrFunction aggrFunction = table.getRowAggrFunctionSet().getFunction(aggrFuncIdx);
                            style = "";

                            if (aggrFunctionLnF.get(aggrFunction.getId()) != null)
                                style = (String) aggrFunctionLnF.get(aggrFunction.getId());
                            if(positionAggrRowLnF.get(aggrFunction.getId()) != null)
                                positionAggr = (String) positionAggrRowLnF.get(aggrFunction.getId());
                            else positionAggr = aggrFunctionPositionRow;

                            if(positionAggr.equals("beforData"))
                            {
                              resultHTML.append("<td class='agg_func_cell' " + style + " colspan='" + table.getData().getFactCount() + "' rowspan='" + aggrFuncNameCellRowSpan + "'>\r");
                              resultHTML.append(aggrFunction.getName(getLocale()) + "\r");
                              resultHTML.append("</td>\r");
                            }
                            //----end of add cell with function name
                        }
                    }


                    if (colHeaderLnF.get(table.getColumnHeaderSet().getRowName(rowIdx)) != null)
                        style = (String) colHeaderLnF.get(table.getColumnHeaderSet().getRowName(rowIdx));

                    for (Iterator iter = table.getColumnHeaderSet().getRowHeaders(rowIdx).iterator(); iter.hasNext();) {
                        Header header = (Header) iter.next();
                        resultHTML.append("<td colspan='" + header.getSpan() + "' class='column_header_cell' " + style + ">");
                        resultHTML.append(header.getLabel());
                        resultHTML.append("</td>");
                    }

                    //---add header title
                    resultHTML.append("<td class='column_header_cell' " + style + ">");
                    resultHTML.append(table.getColumnHeaderSet().getRowLabel(rowIdx, getLocale()));
                    resultHTML.append("</td>");
                    //---end of add header title

                    //---add cell with row Aggr funct name
                    if(rowIdx == 0){
                        for (int aggrFuncIdx = 0; aggrFuncIdx < table.getRowAggrFunctionSet().getFunctionsCount(); aggrFuncIdx++){
                            //----add cell with function name
                            int aggrFuncNameCellRowSpan = table.getColumnHeaderSet().getRowsCount();
                            if ("column".equals(lnf.getFactHeader().getPosition()))
                                aggrFuncNameCellRowSpan++;//If fact row oriented, then colspan++

                            AggrFunction aggrFunction = table.getRowAggrFunctionSet().getFunction(aggrFuncIdx);
                            style = "";
                            if (aggrFunctionLnF.get(aggrFunction.getId()) != null)
                                style = (String) aggrFunctionLnF.get(aggrFunction.getId());

                            if(positionAggrRowLnF.get(aggrFunction.getId()) != null)
                                positionAggr = (String) positionAggrRowLnF.get(aggrFunction.getId());
                            else positionAggr = aggrFunctionPositionRow;

                            if(positionAggr.equals("afterData"))
                            {
                              resultHTML.append("<td class='agg_func_cell' " + style + " colspan='" + table.getData().getFactCount() + "' rowspan='" + aggrFuncNameCellRowSpan + "'>\r");
                              resultHTML.append(aggrFunction.getName(getLocale()) + "\r");
                              resultHTML.append("</td>\r");
                            }
                            //----end of add cell with function name
                        }
                    }

                    if (rowIdx != table.getColumnHeaderSet().getRowsCount() - 1)
                        resultHTML.append("</tr>");
                }
                resultHTML.append("</tr>");
                //----------end of add columns header set

                //---------add fact headers if they are in columns
                if ("column".equals(lnf.getFactHeader().getPosition())) {
                    resultHTML.append("<tr>");
                    int colsCount = table.getData().getColsCount();//evaluate size
                    for (int dataColIdx = 0; dataColIdx < colsCount; dataColIdx++) {
                        for (Iterator iter = table.getData().getFactNames().keySet().iterator(); iter.hasNext();) {
                            Integer factIdx = (Integer) iter.next();
                            String style = "";
                            String factName = (String) table.getData().getFactNames().get(factIdx);
                            if (factHeaderLnF.get(factName) != null)
                                style = (String) factHeaderLnF.get(factName);
                            resultHTML.append("<td class='fact_header_cell' " + style + ">");
                            resultHTML.append(factName);
                            resultHTML.append("</td>");
                        }
                    }
                    //add row name
                    resultHTML.append("<td class='column_header_cell'>");
                    resultHTML.append("(Facts)");
                    resultHTML.append("</td>");
                    //End of add row name
                    resultHTML.append("</tr>");
                }
                //---------end of add fact headers if they are in columns

                //-----Add cells for Column Aggregated Functions
                boolean firstCol=true;
                for (int aggrFuncIdx = 0; aggrFuncIdx < table.getColumnAggrFunctionSet().getFunctionsCount(); aggrFuncIdx++){

                    //----add cell with function name
                    int aggrFuncNameCellColSpan = table.getRowHeaderSet().getRowsCount();
                    if ("row".equals(lnf.getFactHeader().getPosition()))
                        aggrFuncNameCellColSpan++;//If fact row oriented, then colspan++

                    AggrFunction aggrFunction = table.getColumnAggrFunctionSet().getFunction(aggrFuncIdx);
                    String style = "";
                    String positionAggr=null;
                    if (aggrFunctionLnF.get(aggrFunction.getId()) != null)
                        style = (String) aggrFunctionLnF.get(aggrFunction.getId());

                    if(positionAggrColLnF.get(aggrFunction.getId()) != null)
                        positionAggr = (String) positionAggrColLnF.get(aggrFunction.getId());
                    else positionAggr = aggrFunctionPositionCol;

                    if(positionAggr.equals("beforData"))
                    {
                      resultHTML.append("<tr>\r");
                      resultHTML.append("<td class='agg_func_cell' " + style + " colspan='" + aggrFuncNameCellColSpan + "' rowspan='" + table.getData().getFactCount() + "'>\r");
                      resultHTML.append(aggrFunction.getName(getLocale()) + "\r");
                      resultHTML.append("</td>\r");

                      //---add cell befor data cells for left align aggrfunc
                      int colspan2 = 0;
                      for(int count=0;count<table.getRowAggrFunctionSet().getFunctionsCount();count++)
                      {
                        if(positionAggrRowLnF.get(table.getRowAggrFunctionSet().getFunction(count).getId()) != null)
                            positionAggr = (String) positionAggrRowLnF.get(table.getRowAggrFunctionSet().getFunction(count).getId());
                        else positionAggr = aggrFunctionPositionRow;

                        if(positionAggr.equals("beforData"))
                          colspan2 = colspan2 + table.getData().getFactCount();
                      }
                    //---end of add cell befor data cells
                      if(colspan2>0)
                      {
                          resultHTML.append("<td class='fact_data_cell' colspan='" + colspan2  + "' rowspan='" + table.getData().getFactCount() + "'>\r");
                          resultHTML.append("&nbsp;");
                          resultHTML.append("</td>");
                      }

                      //---- add cells with agg funct values
                      for (int factIdx = 0; factIdx < aggrFunction.getFactsCount(); factIdx++) {
                          style = "";
                          String factName = (String) table.getData().getFactNames().get(new Integer(factIdx));
                          if (factDataLnF.get(factName) != null)
                              style = (String) factDataLnF.get(factName);

                          if (factIdx > 0)
                              resultHTML.append("<tr>\r");
                          for (int valIdx = 0; valIdx < aggrFunction.getValuesCount(); valIdx++) {
                              AggrFunctionValue aggrFunctValue = aggrFunction.getValue(factIdx, valIdx);

                              int colSpan = aggrFunctValue != null ? aggrFunctValue.getSpan() : 1;
                              if ("column".equals(lnf.getFactHeader().getPosition()))
                                  colSpan = colSpan * table.getData().getFactCount();//If fact column oriented, then colspan * factCount
                              resultHTML.append("<td class='fact_data_cell' " + style + " colspan='" + colSpan + "'>\r");
                              resultHTML.append((aggrFunctValue != null) ? aggrFunctValue.getFormattedValue() : "");
                              resultHTML.append("</td>\r");
                          }
                        //-----span under right column with column header set labels & row aggr functions cells
//                          if (aggrFuncIdx == 0 && factIdx == 0) {
                          if (firstCol){
                            firstCol=false;
//                              int rowSpan = table.getColumnAggrFunctionSet().getFunctionsCount() * table.getData().getFactCount();
                              int rowSpan =0;
                              for(int count=0;count<table.getColumnAggrFunctionSet().getFunctionsCount();count++)
                              {
                                if(positionAggrColLnF.get(table.getColumnAggrFunctionSet().getFunction(count).getId()) != null)
                                    positionAggr = (String) positionAggrColLnF.get(table.getColumnAggrFunctionSet().getFunction(count).getId());
                                else positionAggr = aggrFunctionPositionCol;

                                if(positionAggr.equals("beforData"))
                                  rowSpan = rowSpan + table.getData().getFactCount();
                              }
                              int _colspan = 1;//span under right column with column header set labels
//                              _colspan = _colspan + table.getRowAggrFunctionSet().getFunctionsCount() * table.getData().getFactCount();//span under right columns with row aggregated funcs
                              for(int count=0;count<table.getRowAggrFunctionSet().getFunctionsCount();count++)
                              {
                                if(positionAggrRowLnF.get(table.getRowAggrFunctionSet().getFunction(count).getId()) != null)
                                    positionAggr = (String) positionAggrRowLnF.get(table.getRowAggrFunctionSet().getFunction(count).getId());
                                else positionAggr = aggrFunctionPositionRow;

                                if(positionAggr.equals("afterData"))
                                  _colspan = _colspan + table.getData().getFactCount();
                              }
                              resultHTML.append("<td class='fact_data_cell' rowspan='" + rowSpan + "' colspan='"+_colspan+"'>\r");
                              resultHTML.append("&nbsp;\r");
                              resultHTML.append("</td>\r");
                          }
                          //---end of add colspanned cell under data cells
                          resultHTML.append("</tr>\r");
                      }
                    }
//                    resultHTML.append("</tr>\r");
                }
                //-----End of add cells for Column Aggregated functions



                //----------add rows header set and data
                int colCount = table.getData().getRowsCount();//evaluate size

                int[] headerRowSpan = new int[table.getRowHeaderSet().getRowsCount()];
                int[] currHeaderColIdx = new int[table.getRowHeaderSet().getRowsCount()];
                int[] currAggrFuncRowSpan = new int[table.getRowAggrFunctionSet().getFunctionsCount()];
                int[] currAggrFuncRowSpan2 = new int[table.getRowAggrFunctionSet().getFunctionsCount()];
                int[] currAggrFuncIdx = new int[table.getRowAggrFunctionSet().getFunctionsCount()];
                int[] currAggrFuncIdx2 = new int[table.getRowAggrFunctionSet().getFunctionsCount()];
                //initialize
                Arrays.fill(headerRowSpan, 0);
                Arrays.fill(currHeaderColIdx, 0);
                Arrays.fill(currAggrFuncRowSpan, 0);
                Arrays.fill(currAggrFuncRowSpan2, 0);
                Arrays.fill(currAggrFuncIdx, -1);
                Arrays.fill(currAggrFuncIdx2, -1);
                //prepare header's row span correction
                int rowSpanCorrection = 1;
                if ("row".equals(lnf.getFactHeader().getPosition())) {
                    rowSpanCorrection = table.getData().getFactCount();
                }

                for (int colIdx = 0; colIdx < colCount; colIdx++) {
                    resultHTML.append("<tr>");
                    //add row headers in current html row
                    for (int rowIdx = 0; rowIdx < table.getRowHeaderSet().getRowsCount(); rowIdx++) {
                        if (headerRowSpan[rowIdx] == 0) {
                            String style = "";
                            if (rowHeaderLnF.get(table.getRowHeaderSet().getRowName(rowIdx)) != null)
                                style = (String) rowHeaderLnF.get(table.getRowHeaderSet().getRowName(rowIdx));
                            //print new
                            Header header = (Header) table.getRowHeaderSet().getRowHeaders(rowIdx).get(currHeaderColIdx[rowIdx]);
                            currHeaderColIdx[rowIdx]++;
                            resultHTML.append("<td rowspan='" + header.getSpan() + "' class='row_header_cell' " + style + ">");
                            resultHTML.append(header.getLabel());
                            resultHTML.append("</td>");
                            //set span and decrement it, because first output has already occured
                            headerRowSpan[rowIdx] = header.getSpan() - rowSpanCorrection;
                        } else if (headerRowSpan[rowIdx] > 0) {
                            //ignore header output. decrement span
                            headerRowSpan[rowIdx] = headerRowSpan[rowIdx] - rowSpanCorrection;
                        }
                    }

                    //----add data in current row if fact header has column orientation
                    if ("column".equals(lnf.getFactHeader().getPosition())) {

                        String positionAggr = null;
                        //add aggr functions values cells
                        for (int aggrFuncIdx = 0; aggrFuncIdx < table.getRowAggrFunctionSet().getFunctionsCount(); aggrFuncIdx++){
                            AggrFunction aggrFunction = table.getRowAggrFunctionSet().getFunction(aggrFuncIdx);

                            //---- add cells with agg funct values
                            for (int factIdx = 0; factIdx < aggrFunction.getFactsCount(); factIdx++) {
                                //check is current aggr function value should be written all previos cell rowspan spreads instead.
                                if(currAggrFuncRowSpan2[aggrFuncIdx] == 0){
                                    if(factIdx == 0){//modify current span and idx only once for row (do it on first fact)
                                        currAggrFuncIdx2[aggrFuncIdx]++;//select next cell
                                    }
                                    String style = "";
                                    String factName = (String) table.getData().getFactNames().get(new Integer(factIdx));
                                    if (factDataLnF.get(factName) != null)
                                        style = (String) factDataLnF.get(factName);

                                    if(positionAggrRowLnF.get(aggrFunction.getId()) != null)
                                        positionAggr = (String) positionAggrRowLnF.get(aggrFunction.getId());
                                    else positionAggr = aggrFunctionPositionRow;

                                    if(positionAggr.equals("beforData"))
                                    {
                                      AggrFunctionValue aggrFunctValue = aggrFunction.getValue(factIdx, currAggrFuncIdx2[aggrFuncIdx]);
                                      resultHTML.append("<td class='fact_data_cell' " + style + " rowspan='"+aggrFunctValue.getSpan()+"'>\r");
                                      resultHTML.append((aggrFunctValue != null) ? aggrFunctValue.getFormattedValue() : "");
                                      resultHTML.append("</td>\r");
                                      if(factIdx == aggrFunction.getFactsCount() -1){//set current span only once for row (do it on last fact - to all facts to be processed before)
                                          currAggrFuncRowSpan2[aggrFuncIdx] = aggrFunction.getValue(factIdx, currAggrFuncIdx2[aggrFuncIdx]).getSpan();
                                      }
                                    }
                                }
                                if(factIdx == aggrFunction.getFactsCount() -1)//decrement current span only once for row (do it on last fact)
                                    currAggrFuncRowSpan2[aggrFuncIdx]--;
                            }
                        }
                        //end aggr functions values cells
                        

                        int colsCount = table.getData().getColsCount();//evaluate size
                        for (int dataColIdx = 0; dataColIdx < colsCount; dataColIdx++) {
                            for (Iterator iter = table.getData().getFactNames().keySet().iterator(); iter.hasNext();) {
                                Integer factIdx = (Integer) iter.next();
                                DataElement dataElement = (DataElement) table.getData().getRow(factIdx.intValue(), colIdx).get(dataColIdx);
                                String style = "";
                                String factName = (String) table.getData().getFactNames().get(factIdx);
                                if (factDataLnF.get(factName) != null)
                                    style = (String) factDataLnF.get(factName);
                                resultHTML.append("<td class='fact_data_cell' " + style + ">");
//                                resultHTML.append((dataElement == null) ? NO_DATA : (dataElement.getValidationError() == null ?
//                                        dataElement.getFormattedValue() : dataElement.getValidationError().toLocalizedString(getLocale())));

                                if(dataElement == null){
                                    resultHTML.append(NO_DATA);
                                }
                                else if(dataElement.getValidationError() == null){
                                    resultHTML.append(dataElement.getFormattedValue());
                                }
                                else{
                                    resultHTML.append("<div title=\"")
                                              .append(dataElement.getValidationError().toLocalizedString(getLocale()))
                                              .append("\">")
                                              .append(VALIDATION_ERROR)
                                              .append("</div>");
                                }
                                
                                resultHTML.append("</td>");
                            }
                        }
                        //add empty rowspanned cell under column headerset cell with names
                        if (colIdx == 0) {
                            resultHTML.append("<td class='fact_data_cell' rowspan='" + colCount + "'>");
                            resultHTML.append("&nbsp;");
                            resultHTML.append("</td>");
                        }
                        //end of add empty rowspanned cell under column headerset cell with names

                        //add aggr functions values cells
                        for (int aggrFuncIdx = 0; aggrFuncIdx < table.getRowAggrFunctionSet().getFunctionsCount(); aggrFuncIdx++){
                            AggrFunction aggrFunction = table.getRowAggrFunctionSet().getFunction(aggrFuncIdx);

                            //---- add cells with agg funct values
                            for (int factIdx = 0; factIdx < aggrFunction.getFactsCount(); factIdx++) {
                                //check is current aggr function value should be written all previos cell rowspan spreads instead.
                                if(currAggrFuncRowSpan[aggrFuncIdx] == 0){
                                    if(factIdx == 0){//modify current span and idx only once for row (do it on first fact)
                                        currAggrFuncIdx[aggrFuncIdx]++;//select next cell
                                    }
                                    String style = "";
                                    String factName = (String) table.getData().getFactNames().get(new Integer(factIdx));
                                    if (factDataLnF.get(factName) != null)
                                        style = (String) factDataLnF.get(factName);

                                    if(positionAggrRowLnF.get(aggrFunction.getId()) != null)
                                        positionAggr = (String) positionAggrRowLnF.get(aggrFunction.getId());
                                    else positionAggr = aggrFunctionPositionRow;

                                    if(positionAggr.equals("afterData"))
                                    {
                                      AggrFunctionValue aggrFunctValue = aggrFunction.getValue(factIdx, currAggrFuncIdx[aggrFuncIdx]);
                                      resultHTML.append("<td class='fact_data_cell' " + style + " rowspan='"+aggrFunctValue.getSpan()+"'>\r");
                                      resultHTML.append((aggrFunctValue != null) ? aggrFunctValue.getFormattedValue() : "");
                                      resultHTML.append("</td>\r");
                                      if(factIdx == aggrFunction.getFactsCount() -1){//set current span only once for row (do it on last fact - to all facts to be processed before)
                                          currAggrFuncRowSpan[aggrFuncIdx] = aggrFunction.getValue(factIdx, currAggrFuncIdx[aggrFuncIdx]).getSpan();
                                      }
                                    }
                                }
                                if(factIdx == aggrFunction.getFactsCount() -1)//decrement current span only once for row (do it on last fact)
                                    currAggrFuncRowSpan[aggrFuncIdx]--;
                            }
                        }
                        //end aggr functions values cells

                    }
                    //----end of add data in current row if fact header has column orientation

                    //----add data in current row if fact header has row orientation
                    if ("row".equals(lnf.getFactHeader().getPosition())) {
                        for (Iterator iter = table.getData().getFactNames().keySet().iterator(); iter.hasNext();) {
                            Integer factIdx = (Integer) iter.next();
                            if (factIdx.intValue() != 0)
                                resultHTML.append("<tr>");

                            String factName = (String) table.getData().getFactNames().get(factIdx);

                            //----add fact name header
                            String style = "";
                            if (factHeaderLnF.get(factName) != null)
                                style = (String) factHeaderLnF.get(factName);
                            resultHTML.append("<td class='fact_header_cell' " + style + ">");
                            resultHTML.append(factName);
                            resultHTML.append("</td>");
                            //----end of add fact name header

                            style = "";
                            if (factDataLnF.get(factName) != null)
                                style = (String) factDataLnF.get(factName);

                            for (Iterator iterData = table.getData().getRow(factIdx.intValue(), colIdx).iterator(); iterData.hasNext();){
                                DataElement dataElement = (DataElement) iterData.next();
                                resultHTML.append("<td class='fact_data_cell' " + style + ">");
//                                resultHTML.append((dataElement == null) ? NO_DATA : (dataElement.getValidationError() == null ?
//                                        dataElement.getFormattedValue() : dataElement.getValidationError().toLocalizedString(getLocale())));

                                if(dataElement == null){
                                    resultHTML.append(NO_DATA);
                                }
                                else if(dataElement.getValidationError() == null){
                                    resultHTML.append(dataElement.getFormattedValue());
                                }
                                else{
                                    resultHTML.append("<div title=\"")
                                              .append(dataElement.getValidationError().toLocalizedString(getLocale()))
                                              .append("\">")
                                              .append(VALIDATION_ERROR)
                                              .append("</div>");
                                }

                                resultHTML.append("</td>");
                            }

                            //add empty rowspanned cell under column headerset cell with names
                            if (colIdx == 0 && factIdx.intValue() == 0) {
                                resultHTML.append("<td class='fact_data_cell' rowspan='" + colCount * table.getData().getFactCount() + "'>");
                                resultHTML.append("&nbsp;");
                                resultHTML.append("</td>");
                            }
                            //end of add empty rowspanned cell under column headerset cell with names

                            //add aggr functions values cells
                            for (int aggrFuncIdx = 0; aggrFuncIdx < table.getRowAggrFunctionSet().getFunctionsCount(); aggrFuncIdx++){
                                AggrFunction aggrFunction = table.getRowAggrFunctionSet().getFunction(aggrFuncIdx);

                                //---- add cells with agg funct values
                                for (int _factIdx = 0; _factIdx < aggrFunction.getFactsCount(); _factIdx++) {
                                    //check is current aggr function value should be written all previos cell rowspan spreads instead.
                                    if (currAggrFuncRowSpan[aggrFuncIdx] == 0) {
                                        if (_factIdx == 0)
                                        {//modify current span and idx only once for row (do it on first fact)
                                            currAggrFuncIdx[aggrFuncIdx]++;//select next cell
                                        }
                                        AggrFunctionValue aggrFunctValue = aggrFunction.getValue(_factIdx, currAggrFuncIdx[aggrFuncIdx]);
                                        int rowSpan = aggrFunctValue.getSpan() * table.getData().getFactCount();

                                        String _factName = (String) table.getData().getFactNames().get(new Integer(_factIdx));
                                        if (factDataLnF.get(_factName) != null)
                                            style = (String) factDataLnF.get(_factName);

                                        resultHTML.append("<td class='fact_data_cell' " + style + " rowspan='" + rowSpan + "'>\r");
                                        resultHTML.append((aggrFunctValue != null) ? aggrFunctValue.getFormattedValue() : "");
                                        resultHTML.append("</td>\r");

                                        if (_factIdx == aggrFunction.getFactsCount() - 1)
                                        {//set current span only once for row (do it on last fact - to all facts to be processed before)
                                            currAggrFuncRowSpan[aggrFuncIdx] = rowSpan;
                                        }
                                    }
                                    if (_factIdx == aggrFunction.getFactsCount() - 1)//decrement current span only once for row (do it on last fact)
                                        currAggrFuncRowSpan[aggrFuncIdx]--;
                                }
                            }
                            //end aggr functions values cells

                            if (factIdx.intValue() != table.getData().getFactNames().keySet().size() - 1)
                                resultHTML.append("</tr>");
                        }
                    }
                    //----end of add data in current row if fact header has row orientation

                    resultHTML.append("</tr>");
                }

                //-----add labels for row HeaderSet
                resultHTML.append("<tr>");
                for (int rowIdx = 0; rowIdx < table.getRowHeaderSet().getRowsCount(); rowIdx++) {
                    String style = "";
                    if (rowHeaderLnF.get(table.getRowHeaderSet().getRowName(rowIdx)) != null)
                        style = (String) rowHeaderLnF.get(table.getRowHeaderSet().getRowName(rowIdx));
                    resultHTML.append("<td class='row_header_cell' " + style + ">");
                    resultHTML.append(table.getRowHeaderSet().getRowLabel(rowIdx, getLocale()));
                    resultHTML.append("</td>");
                }

                //---add cell with 'Facts' label
                if ("row".equals(lnf.getFactHeader().getPosition())) {
                    resultHTML.append("<td class='column_header_cell'>");
                    resultHTML.append("(Facts)");
                    resultHTML.append("</td>");
                }
                //---end of add cell with 'Facts' label

                //---add colspanned cell under data cells & row aggr functions cells
                int colspan = ("column".equals(lnf.getFactHeader().getPosition())) ? (table.getData().getFactCount() * table.getData().getColsCount()) : (table.getData().getColsCount());
                colspan++;//span under right column with column header set labels
                colspan = colspan + table.getRowAggrFunctionSet().getFunctionsCount()*table.getData().getFactCount();//span under right columns with row aggregated funcs
                resultHTML.append("<td class='fact_data_cell' colspan='" + colspan + "'>");
                resultHTML.append("&nbsp;");
                resultHTML.append("</td>");
                //---end of add colspanned cell under data cells
                resultHTML.append("</tr>");
                //-----End of add labels for row HeaderSet

                firstCol=true;
                //-----Add cells for Column Aggregated Functions
                for (int aggrFuncIdx = 0; aggrFuncIdx < table.getColumnAggrFunctionSet().getFunctionsCount(); aggrFuncIdx++){

                    //----add cell with function name
                    int aggrFuncNameCellColSpan = table.getRowHeaderSet().getRowsCount();
                    if ("row".equals(lnf.getFactHeader().getPosition()))
                        aggrFuncNameCellColSpan++;//If fact row oriented, then colspan++

                    AggrFunction aggrFunction = table.getColumnAggrFunctionSet().getFunction(aggrFuncIdx);
                    String style = "";
                    String positionAggr=null;
                    if (aggrFunctionLnF.get(aggrFunction.getId()) != null)
                        style = (String) aggrFunctionLnF.get(aggrFunction.getId());

                    if(positionAggrColLnF.get(aggrFunction.getId()) != null)
                        positionAggr = (String) positionAggrColLnF.get(aggrFunction.getId());
                    else positionAggr = aggrFunctionPositionCol;

                    if(positionAggr.equals("afterData"))
                    {
                      resultHTML.append("<tr>\r");
                      resultHTML.append("<td class='agg_func_cell' " + style + " colspan='" + aggrFuncNameCellColSpan + "' rowspan='" + table.getData().getFactCount() + "'>\r");
                      resultHTML.append(aggrFunction.getName(getLocale()) + "\r");
                      resultHTML.append("</td>\r");
                      //----end of add cell with function name

                      //---add cell befor data cells for left align aggrfunc
                      int colspan2 = 0;
                      for(int count=0;count<table.getRowAggrFunctionSet().getFunctionsCount();count++)
                      {
                        if(positionAggrRowLnF.get(table.getRowAggrFunctionSet().getFunction(count).getId()) != null)
                            positionAggr = (String) positionAggrRowLnF.get(table.getRowAggrFunctionSet().getFunction(count).getId());
                        else positionAggr = aggrFunctionPositionRow;

                        if(positionAggr.equals("beforData"))
                          colspan2 = colspan2 + table.getData().getFactCount();
                      }
                      //---end of add cell befor data cells
                      if(colspan2>0)
                      {
                          resultHTML.append("<td class='fact_data_cell' colspan='" + colspan2  + "' rowspan='" + table.getData().getFactCount() + "'>\r");
                          resultHTML.append("&nbsp;");
                          resultHTML.append("</td>");
                      }
                    
                      //---- add cells with agg funct values
                      for (int factIdx = 0; factIdx < aggrFunction.getFactsCount(); factIdx++) {
                          style = "";
                          String factName = (String) table.getData().getFactNames().get(new Integer(factIdx));
                          if (factDataLnF.get(factName) != null)
                              style = (String) factDataLnF.get(factName);

                          if (factIdx > 0)
                              resultHTML.append("<tr>\r");
                          for (int valIdx = 0; valIdx < aggrFunction.getValuesCount(); valIdx++) {
                              AggrFunctionValue aggrFunctValue = aggrFunction.getValue(factIdx, valIdx);

                              int colSpan = aggrFunctValue != null ? aggrFunctValue.getSpan() : 1;
                              if ("column".equals(lnf.getFactHeader().getPosition()))
                                  colSpan = colSpan * table.getData().getFactCount();//If fact column oriented, then colspan * factCount
                              resultHTML.append("<td class='fact_data_cell' " + style + " colspan='" + colSpan + "'>\r");
                              resultHTML.append((aggrFunctValue != null) ? aggrFunctValue.getFormattedValue() : "");
                              resultHTML.append("</td>\r");
                          }
                          //-----span under right column with column header set labels & row aggr functions cells
//                          if (aggrFuncIdx == 0 && factIdx == 0) {
                            if (firstCol){
                              firstCol=false;  
//                              int rowSpan = table.getColumnAggrFunctionSet().getFunctionsCount() * table.getData().getFactCount();
                              int rowSpan =0;
                              for(int count=0;count<table.getColumnAggrFunctionSet().getFunctionsCount();count++)
                              {
                                if(positionAggrColLnF.get(table.getColumnAggrFunctionSet().getFunction(count).getId()) != null)
                                    positionAggr = (String) positionAggrColLnF.get(table.getColumnAggrFunctionSet().getFunction(count).getId());
                                else positionAggr = aggrFunctionPositionCol;

                                if(positionAggr.equals("afterData"))
                                  rowSpan = rowSpan + table.getData().getFactCount();
                              }
                              int _colspan = 1;//span under right column with column header set labels
//                              _colspan = _colspan + table.getRowAggrFunctionSet().getFunctionsCount() * table.getData().getFactCount();//span under right columns with row aggregated funcs
                              for(int count=0;count<table.getRowAggrFunctionSet().getFunctionsCount();count++)
                              {
                                if(positionAggrRowLnF.get(table.getRowAggrFunctionSet().getFunction(count).getId()) != null)
                                    positionAggr = (String) positionAggrRowLnF.get(table.getRowAggrFunctionSet().getFunction(count).getId());
                                else positionAggr = aggrFunctionPositionRow;

                                if(positionAggr.equals("afterData"))
                                  _colspan = _colspan + table.getData().getFactCount();
                              }
                              resultHTML.append("<td class='fact_data_cell' rowspan='" + rowSpan + "' colspan='"+_colspan+"'>\r");
                              resultHTML.append("&nbsp;\r");
                              resultHTML.append("</td>\r");
                          }
                          //---end of add colspanned cell under data cells
                          resultHTML.append("</tr>\r");
                      }
                    }
//                    resultHTML.append("</tr>\r");
                }
                //-----End of add cells for Column Aggregated functions

                resultHTML.append("</table>");

                //end of one outer cell
                resultHTML.append("</td>");
                resultHTML.append("</tr>");
                resultHTML.append("</table>");
            } else {
                resultHTML.append(getLocalizedLabel("tag.writeAggregatedTable.msg.table_not_loaded_yet", null));
            }
        }
        catch (ReportException e) {
            resultHTML = new StringBuffer(e.getLocalizedMessage());
        }
        return resultHTML.toString();
    }

    private Map organizeHeaderRowsLnF(com.tmx.engines.reportengine.aggrtable.beans.Header.HeaderRow[] headerRowLnF) {
        Map headerRowsLnF = new HashMap();
        if (headerRowLnF == null)
            return headerRowsLnF;
        String style = null;
        for (int i = 0; i < headerRowLnF.length; i++) {
            style = "style='";
            style = style + ((headerRowLnF[i].getAlign() != null) ? ("text-align:" + headerRowLnF[i].getAlign() + ";") : "");
            style = style + ((headerRowLnF[i].getValign() != null) ? ("vertical-align:" + headerRowLnF[i].getValign() + ";") : "");
            style = style + ((headerRowLnF[i].getBcgcolour() != null) ? ("background-color:" + headerRowLnF[i].getBcgcolour() + ";") : "");
            if (headerRowLnF[i].getFont() != null) {
                style = style + ((headerRowLnF[i].getFont().getColour() != null) ? ("color:" + headerRowLnF[i].getFont().getColour() + ";") : "");
                style = style + ((headerRowLnF[i].getFont().getFamily() != null) ? ("font-family:" + headerRowLnF[i].getFont().getFamily() + ";") : "");
                style = style + ((headerRowLnF[i].getFont().getSize() > 0) ? ("font-size:" + headerRowLnF[i].getFont().getSize() + ";") : "");
                style = style + ((headerRowLnF[i].getFont().getWeight() != null) ? ("font-weight:" + headerRowLnF[i].getFont().getWeight() + ";") : "");
            }
            style = style + "'";

            headerRowsLnF.put(headerRowLnF[i].getName(), style);
        }

        return headerRowsLnF;
    }

    private Map organizeDataLnF(Fact[] factLnF) {
        Map factsLnF = new HashMap();
        if (factLnF == null)
            return factsLnF;
        String style = null;
        for (int i = 0; i < factLnF.length; i++) {
            style = "style='";
            style = style + ((factLnF[i].getAlign() != null) ? ("text-align:" + factLnF[i].getAlign() + ";") : "");
            style = style + ((factLnF[i].getValign() != null) ? ("vertical-align:" + factLnF[i].getValign() + ";") : "");
            style = style + ((factLnF[i].getBcgcolour() != null) ? ("background-color:" + factLnF[i].getBcgcolour() + ";") : "");
            if (factLnF[i].getFont() != null) {
                style = style + ((factLnF[i].getFont().getColour() != null) ? ("color:" + factLnF[i].getFont().getColour() + ";") : "");
                style = style + ((factLnF[i].getFont().getFamily() != null) ? ("font-family:" + factLnF[i].getFont().getFamily() + ";") : "");
                style = style + ((factLnF[i].getFont().getSize() > 0) ? ("font-size:" + factLnF[i].getFont().getSize() + ";") : "");
                style = style + ((factLnF[i].getFont().getWeight() != null) ? ("font-weight:" + factLnF[i].getFont().getWeight() + ";") : "");
            }
            style = style + "'";

            factsLnF.put(factLnF[i].getName(), style);
        }
        return factsLnF;
    }

    private Map organizePositionAggrColLnF(LookAndFeel.AggregateFunctions.Function[] aggrFunctionLnF) {
        Map aggrFunctionsLnF = new HashMap();
        if (aggrFunctionLnF == null)
            return aggrFunctionsLnF;
        String positionAggr = null;
        for (int i = 0; i < aggrFunctionLnF.length; i++) {

            positionAggr = ((aggrFunctionLnF[i].getAggrFunctionPositionCol() != null) ? aggrFunctionLnF[i].getAggrFunctionPositionCol() : null);
            aggrFunctionsLnF.put(new Integer(aggrFunctionLnF[i].getId()), positionAggr);
        }
        return aggrFunctionsLnF;
    }
        
    private Map organizePositionAggrRowLnF(LookAndFeel.AggregateFunctions.Function[] aggrFunctionLnF) {
        Map aggrFunctionsLnF = new HashMap();
        if (aggrFunctionLnF == null)
            return aggrFunctionsLnF;
        String positionAggr = null;
        for (int i = 0; i < aggrFunctionLnF.length; i++) {

            positionAggr = ((aggrFunctionLnF[i].getAggrFunctionPositionRow() != null) ? aggrFunctionLnF[i].getAggrFunctionPositionRow() : null);
            aggrFunctionsLnF.put(new Integer(aggrFunctionLnF[i].getId()), positionAggr);
        }
        return aggrFunctionsLnF;
    }

        private Map organizeAggrFunctionLnF(LookAndFeel.AggregateFunctions.Function[] aggrFunctionLnF) {
            Map aggrFunctionsLnF = new HashMap();
            if (aggrFunctionLnF == null)
                return aggrFunctionsLnF;
            String style = null;
            for (int i = 0; i < aggrFunctionLnF.length; i++) {
                style = "style='";
                style = style + ((aggrFunctionLnF[i].getAlign() != null) ? ("text-align:" + aggrFunctionLnF[i].getAlign() + ";") : "");
                style = style + ((aggrFunctionLnF[i].getValign() != null) ? ("vertical-align:" + aggrFunctionLnF[i].getValign() + ";") : "");
                style = style + ((aggrFunctionLnF[i].getBcgcolour() != null) ? ("background-color:" + aggrFunctionLnF[i].getBcgcolour() + ";") : "");
                if (aggrFunctionLnF[i].getFont() != null) {
                    style = style + ((aggrFunctionLnF[i].getFont().getColour() != null) ? ("color:" + aggrFunctionLnF[i].getFont().getColour() + ";") : "");
                    style = style + ((aggrFunctionLnF[i].getFont().getFamily() != null) ? ("font-family:" + aggrFunctionLnF[i].getFont().getFamily() + ";") : "");
                    style = style + ((aggrFunctionLnF[i].getFont().getSize() > 0) ? ("font-size:" + aggrFunctionLnF[i].getFont().getSize() + ";") : "");
                    style = style + ((aggrFunctionLnF[i].getFont().getWeight() != null) ? ("font-weight:" + aggrFunctionLnF[i].getFont().getWeight() + ";") : "");
                }
                style = style + "'";

                aggrFunctionsLnF.put(new Integer(aggrFunctionLnF[i].getId()), style);
            }

        return aggrFunctionsLnF;
    }

    private void renderCommonStyle(StringBuffer resultHTML, String styleName, CommonStyle commonStyle) {
        resultHTML.append(styleName);
        resultHTML.append("{");
        resultHTML.append((commonStyle.getBcgcolour() != null) ? ("background-color:" + commonStyle.getBcgcolour() + ";") : "");
        resultHTML.append((commonStyle.getAlign() != null) ? ("text-align:" + commonStyle.getAlign() + ";") : "");
        resultHTML.append((commonStyle.getValign() != null) ? ("vertical-align:" + commonStyle.getValign() + ";") : "");
        if (commonStyle.getFont() != null) {
            resultHTML.append((commonStyle.getFont().getColour() != null) ? ("color:" + commonStyle.getFont().getColour() + ";") : "");
            resultHTML.append((commonStyle.getFont().getFamily() != null) ? ("font-family:" + commonStyle.getFont().getFamily() + ";") : "");
            resultHTML.append((commonStyle.getFont().getSize() > 0) ? ("font-size:" + commonStyle.getFont().getSize() + ";") : "");
            resultHTML.append((commonStyle.getFont().getWeight() != null) ? ("font-weight:" + commonStyle.getFont().getWeight() + ";") : "");
        }
        resultHTML.append("}");
    }

    //------------------------------Bean interaction methods
    private AggregatedTable getAggregatedTable() throws JspException {
        HttpServletRequest request = (HttpServletRequest) pageContext.getRequest();
        String originalName = (getName() != null) ? getName() : NestedPropertyHelper.getCurrentName(request, this);
        //Read Table from Bean form
        String originalProperty = getTableName();
        String nestedProperty = NestedPropertyHelper.getAdjustedProperty(request, originalProperty);

        Object aggTable = TagUtils.getInstance().lookup(pageContext, originalName, nestedProperty, null);
        if (aggTable != null && (aggTable instanceof AggregatedTable))
            return (AggregatedTable) aggTable;
        else
            return null;
    }

}
