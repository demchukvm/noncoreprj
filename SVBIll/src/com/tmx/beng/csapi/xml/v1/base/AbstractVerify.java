
package com.tmx.beng.csapi.xml.v1.base;

import com.tmx.gate.csapi.v1.base.beans.*;
import com.tmx.beng.base.BillingMessage;
import com.tmx.beng.base.BillException;
import com.tmx.beng.base.BillingMessageImpl;
import org.apache.log4j.Logger;

import java.text.SimpleDateFormat;
import java.text.ParseException;
import java.util.regex.Pattern;
import java.util.regex.Matcher;

/**
 */
public class AbstractVerify {
    final static String MSG_TYPE = "request.";
    protected static final Pattern amountPattern = Pattern.compile("[0-9]{1,10}(.[0-9]{1,2})?");
    protected static final Pattern msisdnPattern = Pattern.compile("[0-9]{9,12}");
    protected static final Pattern accountPattern = Pattern.compile("[0-9.]{9,12}");

    public void verify(RootDocument root, BillingMessageImpl billMsg) throws BillException {
        Matcher matcher;
        
        AuthType auth = root.getRoot().getRequest().getAuth();
        if (auth.getTerminalSn() == null) throw new BillException("err.csapi.terminal_sn_not_found");
        billMsg.setAttributeString(BillingMessage.TERMINAL_SN, auth.getTerminalSn());
           
        billMsg.setAttributeString(BillingMessage.IS_REDIRECT, "false"); // for redirect from KS to BeelineExc & BeelineFixConnect

        if (auth.getTerminalPswd() != null) {
            billMsg.setAttribute(BillingMessage.TERMINAL_PSWD, auth.getTerminalPswd());
        }
        if (auth.getSellerCode() == null) throw new BillException("err.csapi.seller_code_not_found");
        billMsg.setAttributeString(BillingMessage.SELLER_CODE, auth.getSellerCode());
        if (auth.getProcessingCode() == null) throw new BillException("err.csapi.processing_code_not_found");
        billMsg.setAttributeString(BillingMessage.PROCESSING_CODE, auth.getProcessingCode());
        if (auth.getLogin() == null) throw new BillException("err.csapi.login_not_found");
        billMsg.setAttributeString(BillingMessage.LOGIN, auth.getLogin());
        if (auth.getPassword() == null) throw new BillException("err.csapi.password_not_found");
        billMsg.setAttributeString(BillingMessage.PASSWORD, auth.getPassword());

        //validate client time
        if(auth.getClientTime() != null){
            try{
                java.util.Date regDate = new SimpleDateFormat("yyyy.MM.dd HH:mm:ss").parse(auth.getClientTime());
                billMsg.setAttribute(BillingMessage.CLIENT_TIME, regDate);
            }catch(ParseException e){
                throw new BillException("err.csapi.incorrect_client_time", new String[]{auth.getClientTime()});
            }
        }

        OperationsType operations = root.getRoot().getRequest().getOperations();
      //  logger.info(operations);


        if(operations.getRefillPayment() != null)
        {
            RefillPaymentReqType operation = operations.getRefillPayment();
            billMsg.setOperationName(MSG_TYPE + operation.getDomNode().getNodeName());
            if(operation.getCliTransactionNum() == null)
                throw new BillException("err.csapi.cli_transaction_num_not_found");
            billMsg.setAttributeString(BillingMessage.CLI_TRANSACTION_NUM, operation.getCliTransactionNum());
            if(operation.getServiceCode() == null)
                throw new BillException("err.csapi.service_code_not_found");
            billMsg.setAttributeString(BillingMessage.SERVICE_CODE, operation.getServiceCode());

            // 01.10.2010 Proximan
            billMsg.setAttributeString(BillingMessage.TRADE_POINT, operation.getTradePoint());

            if(operation.getAmount() == null)
                throw new BillException("err.csapi.amount_not_found");

            // check amount
            matcher = amountPattern.matcher(operation.getAmount());
            if(!matcher.matches()){
                throw new BillException("err.csapi.incorrect_amount_format");
            }
            if(Double.parseDouble(operation.getAmount()) == 0){
                throw new BillException("err.csapi.amount_is_0");
            }
            billMsg.setAttributeString(BillingMessage.AMOUNT, operation.getAmount());
            billMsg.setAttributeString(BillingMessage.TOTAL_AMOUNT, operation.getAmount());

            if(operation.getIsMobileJava() != null)
            {
                billMsg.setAttributeString(BillingMessage.IS_MOBILE_JAVA, operation.getIsMobileJava());
            }

        }
        else if(operations.getRefillPaymentStatus() != null)
        {
            RefillPaymentStatusReqType operation = operations.getRefillPaymentStatus();
            billMsg.setOperationName(MSG_TYPE + operation.getDomNode().getNodeName());
            if(operation.getCliTransactionNum() == null && operation.getBillTransactionNum() == null)
                throw new BillException("err.csapi.cli_and_bill_transaction_num_not_found");
            billMsg.setAttributeString(BillingMessage.BILL_TRANSACTION_NUM, operation.getBillTransactionNum());
            billMsg.setAttributeString(BillingMessage.CLI_TRANSACTION_NUM, operation.getCliTransactionNum());
        }
        else if(operations.getBuyVoucher() != null)
        {
            BuyVoucherReqType operation = operations.getBuyVoucher();
            billMsg.setOperationName(MSG_TYPE + operation.getDomNode().getNodeName());
            if(operation.getCliTransactionNum() == null)
                throw new BillException("err.csapi.cli_transaction_num_not_found");
            billMsg.setAttributeString(BillingMessage.CLI_TRANSACTION_NUM, operation.getCliTransactionNum());
            if(operation.getVoucherNominal() == null)
                throw new BillException("err.csapi.voucher_nominal_not_found");
            billMsg.setAttributeString(BillingMessage.VOUCHER_NOMINAL, operation.getVoucherNominal());
            if(operation.getServiceCode() == null)
                throw new BillException("err.csapi.service_code_not_found");
            billMsg.setAttributeString(BillingMessage.SERVICE_CODE, operation.getServiceCode());

        }
        else if(operations.getRegisterTerminal() != null)
        {
            RegisterTerminalReqType operation = operations.getRegisterTerminal();
            billMsg.setOperationName(MSG_TYPE + operation.getDomNode().getNodeName());
            if(operation.getTerminalSignature() == null)
                throw new BillException("err.csapi.terminal_signature_not_found");
            billMsg.setAttributeString(BillingMessage.TERMINAL_SIGNATURE, operation.getTerminalSignature());
        }
        else if(operations.getActivateMobileTerminal() != null)
        {
            ActivateMobileTerminalReqType operation = operations.getActivateMobileTerminal();
            billMsg.setOperationName(MSG_TYPE + operation.getDomNode().getNodeName());
            billMsg.setAttributeString(BillingMessage.IS_MOBILE_JAVA, operation.getIsMobileJava());
        }
        else if(operations.getServicesParameters() != null)
        {
            ServicesParametersReqType operation = operations.getServicesParameters();
            billMsg.setOperationName(MSG_TYPE + operation.getDomNode().getNodeName());
            billMsg.setAttributeString(BillingMessage.IS_MOBILE_JAVA, operation.getIsMobileJava());
        }
        else if(operations.getBalances() != null)
        {
            BalancesReqType operation = operations.getBalances();
            billMsg.setOperationName(MSG_TYPE + operation.getDomNode().getNodeName());
            billMsg.setAttributeString(BillingMessage.IS_MOBILE_JAVA, operation.getIsMobileJava());
        }
        else if(operations.getReportFull() != null)
        {
            ReportFullReqType operation = operations.getReportFull();
            billMsg.setOperationName(MSG_TYPE + operation.getDomNode().getNodeName());
            billMsg.setAttributeString(BillingMessage.ACCOUNT, operation.getAccount());
            billMsg.setAttributeString(BillingMessage.FROM, operation.getFrom());
            billMsg.setAttributeString(BillingMessage.TO, operation.getTo());
            billMsg.setAttributeString(BillingMessage.IS_MOBILE_JAVA, operation.getIsMobileJava());
        }
        else if(operations.getReportShort() != null)
        {
            ReportShortReqType operation = operations.getReportShort();
            billMsg.setOperationName(MSG_TYPE + operation.getDomNode().getNodeName());
            billMsg.setAttributeString(BillingMessage.FROM, operation.getFrom());
            billMsg.setAttributeString(BillingMessage.TO, operation.getTo());
            billMsg.setAttributeString(BillingMessage.IS_MOBILE_JAVA, operation.getIsMobileJava());
        }

    }

    public void serialize(RootDocument root, BillingMessage msg){
        if(BillingMessage.O_REFILL_PAYMENT.equals(msg.getOperationName()))
        {
            RefillPaymentRespType resp = root.addNewRoot().addNewResponse().addNewOperationsResults().addNewRefillPayment();
            if(msg.getAttributeString(BillingMessage.BILL_TRANSACTION_NUM) != null){
                resp.setBillTransactionNum(msg.getAttributeString(BillingMessage.BILL_TRANSACTION_NUM));
            }
            resp.setStatusCode(msg.getAttributeString(BillingMessage.STATUS_CODE));
            if(msg.getAttributeString(BillingMessage.STATUS_MESSAGE) != null){
                resp.setStatusMessage(msg.getAttributeString(BillingMessage.STATUS_MESSAGE));
            }
            if(msg.getAttributeString(BillingMessage.WAIT_TIME) != null){
                resp.setWaitTime(Integer.parseInt(msg.getAttributeString(BillingMessage.WAIT_TIME)));
            }
            if(msg.getAttributeString(BillingMessage.REGISTRATION_TIME) != null){
                resp.setRegistrationTime(formatTime((java.util.Date)msg.getAttribute(BillingMessage.REGISTRATION_TIME)));
            }
            if(msg.getAttributeString(BillingMessage.IS_MOBILE_JAVA) != null)
                resp.setIsMobileJava(msg.getAttributeString(BillingMessage.IS_MOBILE_JAVA));
        }
        else if(BillingMessage.O_REFILL_PAYMENT_RESULT.equals(msg.getOperationName()))
        {
            RefillPaymentRespType resp = root.addNewRoot().addNewResponse().addNewOperationsResults().addNewRefillPayment();
            if(msg.getAttributeString(BillingMessage.BILL_TRANSACTION_NUM) != null){
                resp.setBillTransactionNum(msg.getAttributeString(BillingMessage.BILL_TRANSACTION_NUM));
            }
            resp.setStatusCode(msg.getAttributeString(BillingMessage.STATUS_CODE));
            if(msg.getAttributeString(BillingMessage.STATUS_MESSAGE) != null){
                resp.setStatusMessage(msg.getAttributeString(BillingMessage.STATUS_MESSAGE));
            }
            if(msg.getAttributeString(BillingMessage.WAIT_TIME) != null){
                resp.setWaitTime(Integer.parseInt(msg.getAttributeString(BillingMessage.WAIT_TIME)));
            }
            if(msg.getAttributeString(BillingMessage.REGISTRATION_TIME) != null){
                resp.setRegistrationTime(formatTime((java.util.Date)msg.getAttribute(BillingMessage.REGISTRATION_TIME)));
            }
            if(msg.getAttributeString(BillingMessage.IS_MOBILE_JAVA) != null)
                resp.setIsMobileJava(msg.getAttributeString(BillingMessage.IS_MOBILE_JAVA));
        }
        else if(BillingMessage.O_REFILL_PAYMENT_STATUS.equals(msg.getOperationName()))
        {
            RefillPaymentStatusRespType resp = root.addNewRoot().addNewResponse().addNewOperationsResults().addNewRefillPaymentStatus();
            if(msg.getAttributeString(BillingMessage.BILL_TRANSACTION_NUM) != null){
                resp.setBillTransactionNum(msg.getAttributeString(BillingMessage.BILL_TRANSACTION_NUM));
            }
            resp.setStatusCode(msg.getAttributeString(BillingMessage.STATUS_CODE));
            if(msg.getAttributeString(BillingMessage.STATUS_MESSAGE) != null){
                resp.setStatusMessage(msg.getAttributeString(BillingMessage.STATUS_MESSAGE));
            }
            if(msg.getAttributeString(BillingMessage.WAIT_TIME) != null){
                resp.setWaitTime(
                    Integer.parseInt(msg.getAttributeString(BillingMessage.WAIT_TIME)));
            }
            if(msg.getAttributeString(BillingMessage.REGISTRATION_TIME) != null){
                resp.setRegistrationTime(formatTime((java.util.Date)msg.getAttribute(BillingMessage.REGISTRATION_TIME)));
            }
        }
        else if(BillingMessage.O_BUY_VOUCHER.equals(msg.getOperationName()))
        {
            BuyVoucherRespType resp = root.addNewRoot().addNewResponse().addNewOperationsResults().addNewBuyVoucher();
            resp.setStatusCode(msg.getAttributeString(BillingMessage.STATUS_CODE));
            if(msg.getAttributeString(BillingMessage.STATUS_MESSAGE) != null){
                resp.setStatusMessage(msg.getAttributeString(BillingMessage.STATUS_MESSAGE));
            }
            if(msg.getAttributeString(BillingMessage.VOUCHER_CODE) != null){
                resp.setVoucherCode(msg.getAttributeString(BillingMessage.VOUCHER_CODE));
            }
            if(msg.getAttributeString(BillingMessage.VOUCHER_SECRET_KEY) != null){
                resp.setVoucherSecretKey(msg.getAttributeString(BillingMessage.VOUCHER_SECRET_KEY));
            }
            if(msg.getAttributeString(BillingMessage.VOUCHER_BEST_BEFORE_DATE) != null){
                resp.setVoucherBestBeforeDate(msg.getAttributeString(BillingMessage.VOUCHER_BEST_BEFORE_DATE));
            }
            if(msg.getAttributeString(BillingMessage.VOUCHER_RENEVAL_TIME) != null){
                resp.setRenevalTime(msg.getAttributeString(BillingMessage.VOUCHER_RENEVAL_TIME));
            }
        }
        else if(BillingMessage.O_REGISTER_TERMINAL.equals(msg.getOperationName()))
        {
            RegisterTerminalRespType resp = root.addNewRoot().addNewResponse().addNewOperationsResults().addNewRegistrationTerminal();
            resp.setStatusCode(msg.getAttributeString(BillingMessage.STATUS_CODE));
            if(msg.getAttributeString(BillingMessage.STATUS_MESSAGE) != null){
                resp.setStatusMessage(msg.getAttributeString(BillingMessage.STATUS_MESSAGE));
            }
            if(msg.getAttributeString(BillingMessage.CLIENTCERTIFICATE) != null){
                resp.setClientCertificate(msg.getAttributeString(BillingMessage.CLIENTCERTIFICATE));
            }
            if(msg.getAttributeString(BillingMessage.CLIENTCERTIFICATEKEY) != null){
                resp.setClientCertificateKey(msg.getAttributeString(BillingMessage.CLIENTCERTIFICATEKEY));
            }
        }
        else if(BillingMessage.O_ACTIVATE_TERMINAL_RESULT.equals(msg.getOperationName()))
        {
            ActivateMobileTerminalRespType resp = root.addNewRoot().addNewResponse().addNewOperationsResults().addNewActivateMobileTerminal();

            if(msg.getAttributeString(BillingMessage.SELLER_CODE) != null)
                resp.setSellerCode(msg.getAttributeString(BillingMessage.SELLER_CODE));

            if(msg.getAttributeString(BillingMessage.FEE_BEELINE) != null)
                resp.setFeeBeeline(msg.getAttributeString(BillingMessage.FEE_BEELINE));

            if(msg.getAttributeString(BillingMessage.FEE_CDMA) != null)
                resp.setFeeCDMA(msg.getAttributeString(BillingMessage.FEE_CDMA));

            if(msg.getAttributeString(BillingMessage.FEE_INTERTELECOM) != null)
                resp.setFeeInterTelecom(msg.getAttributeString(BillingMessage.FEE_INTERTELECOM));
                                    
            if(msg.getAttributeString(BillingMessage.FEE_KS0124) != null)
                resp.setFeeKS0124(msg.getAttributeString(BillingMessage.FEE_KS0124));

            if(msg.getAttributeString(BillingMessage.FEE_KS2599) != null)
                resp.setFeeKS2599(msg.getAttributeString(BillingMessage.FEE_KS2599));

            if(msg.getAttributeString(BillingMessage.FEE_KS100) != null)
                resp.setFeeKS100(msg.getAttributeString(BillingMessage.FEE_KS100));

            if(msg.getAttributeString(BillingMessage.FEE_LIFE) != null)
                resp.setFeeLife(msg.getAttributeString(BillingMessage.FEE_LIFE));

            if(msg.getAttributeString(BillingMessage.FEE_UMCPOP) != null)
                resp.setFeeUMCPOP(msg.getAttributeString(BillingMessage.FEE_UMCPOP));

            resp.setIsMobileJava("true");

        }
        else if(BillingMessage.O_SERVICE_PARAMETERS_RESULT.equals(msg.getOperationName()))
        {
            ServicesParametersRespType resp = root.addNewRoot().addNewResponse().addNewOperationsResults().addNewServicesParameters();

            if(msg.getAttributeString(BillingMessage.FEE_BEELINE) != null)
                resp.setFeeBeeline(msg.getAttributeString(BillingMessage.FEE_BEELINE));

            if(msg.getAttributeString(BillingMessage.FEE_CDMA) != null)
                resp.setFeeCDMA(msg.getAttributeString(BillingMessage.FEE_CDMA));

            if(msg.getAttributeString(BillingMessage.FEE_INTERTELECOM) != null)
                resp.setFeeInterTelecom(msg.getAttributeString(BillingMessage.FEE_INTERTELECOM));

            if(msg.getAttributeString(BillingMessage.FEE_KS0124) != null)
                resp.setFeeKS0124(msg.getAttributeString(BillingMessage.FEE_KS0124));

            if(msg.getAttributeString(BillingMessage.FEE_KS2599) != null)
                resp.setFeeKS2599(msg.getAttributeString(BillingMessage.FEE_KS2599));

            if(msg.getAttributeString(BillingMessage.FEE_KS100) != null)
                resp.setFeeKS100(msg.getAttributeString(BillingMessage.FEE_KS100));

            if(msg.getAttributeString(BillingMessage.FEE_LIFE) != null)
                resp.setFeeLife(msg.getAttributeString(BillingMessage.FEE_LIFE));

            if(msg.getAttributeString(BillingMessage.FEE_UMCPOP) != null)
                resp.setFeeUMCPOP(msg.getAttributeString(BillingMessage.FEE_UMCPOP));

            resp.setIsMobileJava("true");
        }
        else if(BillingMessage.O_BALANCES_RESULT.equals(msg.getOperationName()))
        {
            BalancesRespType resp = root.addNewRoot().addNewResponse().addNewOperationsResults().addNewBalances();

            if(msg.getAttributeString(BillingMessage.SELLER_BALANCE) != null)
                resp.setSellerBalance(msg.getAttributeString(BillingMessage.SELLER_BALANCE));

            if(msg.getAttributeString(BillingMessage.TERMINAL_BALANCE) != null)
                resp.setTerminalBalance(msg.getAttributeString(BillingMessage.TERMINAL_BALANCE));

            resp.setIsMobileJava("true");
        }
        else if(BillingMessage.O_REPORT_FULL_RESULT.equals(msg.getOperationName()))
        {
            ReportFullRespType resp = root.addNewRoot().addNewResponse().addNewOperationsResults().addNewReportFull();

            if(msg.getAttributeString(BillingMessage.TRANSACTION_LIST) != null)
                resp.setTransactionsList(msg.getAttributeString(BillingMessage.TRANSACTION_LIST));

            resp.setIsMobileJava("true");
        }
        else if(BillingMessage.O_REPORT_SHORT_RESULT.equals(msg.getOperationName()))
        {
            ReportShortRespType resp = root.addNewRoot().addNewResponse().addNewOperationsResults().addNewReportShort();

            if(msg.getAttributeString(BillingMessage.STATUS_OK) != null)
                resp.setStatusOk(msg.getAttributeString(BillingMessage.STATUS_OK));

            if(msg.getAttributeString(BillingMessage.STATUS_ERROR) != null)
                resp.setStatusError(msg.getAttributeString(BillingMessage.STATUS_ERROR));

            if(msg.getAttributeString(BillingMessage.STATUS_UNKNOWN) != null)
                resp.setStatusUnknown(msg.getAttributeString(BillingMessage.STATUS_UNKNOWN));

            if(msg.getAttributeString(BillingMessage.COUNT_BEELINE) != null)
                resp.setCountBeeline(msg.getAttributeString(BillingMessage.COUNT_BEELINE));

            if(msg.getAttributeString(BillingMessage.COUNT_CDMA) != null)
                resp.setCountCDMA(msg.getAttributeString(BillingMessage.COUNT_CDMA));

            if(msg.getAttributeString(BillingMessage.COUNT_INTERTELECOM) != null)
                resp.setCountInterTelecom(msg.getAttributeString(BillingMessage.COUNT_INTERTELECOM));

            if(msg.getAttributeString(BillingMessage.COUNT_KS0124) != null)
                resp.setCountKS0124(msg.getAttributeString(BillingMessage.COUNT_KS0124));

            if(msg.getAttributeString(BillingMessage.COUNT_KS2599) != null)
                resp.setCountKS2599(msg.getAttributeString(BillingMessage.COUNT_KS2599));

            if(msg.getAttributeString(BillingMessage.COUNT_KS100) != null)
                resp.setCountKS100(msg.getAttributeString(BillingMessage.COUNT_KS100));

            if(msg.getAttributeString(BillingMessage.COUNT_LIFE) != null)
                resp.setCountLife(msg.getAttributeString(BillingMessage.COUNT_LIFE));

            if(msg.getAttributeString(BillingMessage.COUNT_UMCPOP) != null)
                resp.setCountUMCPOP(msg.getAttributeString(BillingMessage.COUNT_UMCPOP));

            if(msg.getAttributeString(BillingMessage.SUM_BEELINE) != null)
                resp.setSumBeeline(msg.getAttributeString(BillingMessage.SUM_BEELINE));

            if(msg.getAttributeString(BillingMessage.SUM_CDMA) != null)
                resp.setSumCDMA(msg.getAttributeString(BillingMessage.SUM_CDMA));

            if(msg.getAttributeString(BillingMessage.SUM_INTERTELECOM) != null)
                resp.setSumInterTelecom(msg.getAttributeString(BillingMessage.SUM_INTERTELECOM));

            if(msg.getAttributeString(BillingMessage.SUM_KS0124) != null)
                resp.setSumKS0124(msg.getAttributeString(BillingMessage.SUM_KS0124));

            if(msg.getAttributeString(BillingMessage.SUM_KS2599) != null)
                resp.setSumKS2599(msg.getAttributeString(BillingMessage.SUM_KS2599));

            if(msg.getAttributeString(BillingMessage.SUM_KS100) != null)
                resp.setSumKS100(msg.getAttributeString(BillingMessage.SUM_KS100));

            if(msg.getAttributeString(BillingMessage.SUM_LIFE) != null)
                resp.setSumLife(msg.getAttributeString(BillingMessage.SUM_LIFE));

            if(msg.getAttributeString(BillingMessage.SUM_UMCPOP) != null)
                resp.setSumUMCPOP(msg.getAttributeString(BillingMessage.SUM_UMCPOP));

            if(msg.getAttributeString(BillingMessage.COUNT_TOTAL) != null)
                resp.setCountTotal(msg.getAttributeString(BillingMessage.COUNT_TOTAL));

            if(msg.getAttributeString(BillingMessage.SUM_TOTAL) != null)
                resp.setSumTotal(msg.getAttributeString(BillingMessage.SUM_TOTAL));

            resp.setIsMobileJava("true");
        }
    }

    protected String formatTime(java.util.Date date){
        SimpleDateFormat format = new SimpleDateFormat("yyyy.MM.dd HH:mm:ss");
        return date == null ? null : format.format(date);
    }

    // Proximan 09.11.2010
    Logger logger = Logger.getLogger("webcsapi."+getClass().getName());
    //
}
