package com.tmx.web.controls;


import java.util.Hashtable;

public class Button extends BasicControl {
    private String labelKey = null;
    private String action = null;
    private String forward = null;
    private String command = null;
    private String url = null;
    //to work with roadmap
    private String listenDispatchEvent = null;
    private String listenEventOnPage = null;
    private Hashtable params=new Hashtable();

    public void initialize() {
    }

    //------------Properties
    public String getLabelKey() {
        return labelKey;
    }

    public String getLocalizedLabel(){
        return getLocalizedLabel(getLabelKey(), getLocale());
    }

    public void setLabelKey(String labelKey) {
        this.labelKey = labelKey;
    }

    public String getListenDispatchEvent() {
        return listenDispatchEvent;
    }

    public String getListenEventOnPage() {
        return listenEventOnPage;
    }

    public void setListenOnEvent(String listenOnPage, String event){
        this.listenDispatchEvent = event;
        this.listenEventOnPage = listenOnPage;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getForward() {
        return forward;
    }

    public void setForward(String forward) {
        this.forward = forward;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getCommand() {
        return command;
    }

    public void setCommand(String command) {
        this.command = command;
    }

    public void putParam(String paramName,String paramValue){
        params.put(paramName,paramValue);
    }

    public void clearParams(String param){
        params.clear();
    }

    public Hashtable getParams() {
        return params;
    }

    protected void safeReset() {

    }
}