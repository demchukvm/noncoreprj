package com.tmx.engines.xmlengine;

import com.tmx.as.base.Entity;
import com.tmx.util.StructurizedException;
import org.w3c.dom.Node;
import java.util.List;

public interface XMLPublisher {

    public Node initializeDocument(DocumentHeader header) throws XMLPublishingException;
    /** Publish given <code>entity</code> into XML as child of given <code>parentNode</code>.
     * @param entity        the given entity to be published
     * @param parentNode    the given parent node
     * @param entityName    the name of entity to be specifed into XML as 'name' attribute of entity
     * @param filters       the list of names of given entity to be published. You could specify NULL
     * id you want to publish all available attributes in entity hierarchy. You could operate
     * with nested attributes using dot-notation. Example: "performer.name".
     * */
    public Node publishEntity(Entity entity, Node parentNode, String entityName, String[] filters) throws XMLPublishingException;
    public Node publishEntityList(List entityList, Node parentNode, String setName, Long selectedId, String[] filters) throws XMLPublishingException;
    public String serializeDocument(Node anyNode) throws XMLPublishingException;
    public Node publishError(StructurizedException error, Node parentNode) throws XMLPublishingException;
    /** Prepare creation of custom node. Specify the node prototype and path in dot notation
     * to identify where the custom node should be injected in result XML document.
     * @param targetPath    dot-notated path to identify where the custom node should be injected.
     * Example: "favorite_performer.albums[*].style".
     * @param nodePrototype NodePrototype implementation to be rendered into XML Node
     * */
    public void prepareCustomNode(String targetPath, NodePrototype nodePrototype);
}
