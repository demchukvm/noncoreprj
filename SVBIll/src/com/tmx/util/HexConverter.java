package com.tmx.util;

import java.io.InputStream;
import java.io.IOException;
import java.util.Map;
import java.util.HashMap;

/**
 */
public class HexConverter {
    private DigitsEncodingTable encodingTable = new DigitsEncodingTable();

    public String getStringFromHex(byte[] hexBytes){
        return getStringFromHex(hexBytes, 0, getHexSize(hexBytes.length));
    }

    public String getStringFromHex(byte[] hexBytes, int hexSize){
        return getStringFromHex(hexBytes, 0, hexSize);
    }

    public String getStringFromHex(InputStream is, int hexSize) throws IOException {
        return getStringFromHex(is, 0, hexSize);
    }

    public String getStringFromHex(InputStream is, int skipFirstBytes, int hexSize) throws IOException {
        byte[] data = new byte[getByteSize(hexSize)];
        is.read(data, skipFirstBytes, data.length);
        return getStringFromHex(data, hexSize);
    }

    /**
     * @param hexBytes source byte array with HEX digits
     * @param skipFirstBytes count of heading bytes to skip
     * @param hexSize count of HEX digits to take from byte array
     * @return string representation of HEX value
     * */
    public String getStringFromHex(byte[] hexBytes, int skipFirstBytes, int hexSize){
        StringBuffer value = new StringBuffer();
        for(int i=skipFirstBytes; i<getByteSize(hexSize); i++){
            int hhVal = (hexBytes[i] & 0xF0) >> 4;
            value.append(encodingTable.decToHex(hhVal));
            int llVal = hexBytes[i] & 0x0F;
            value.append(encodingTable.decToHex(llVal));
        }
        return value.toString();
    }

    public byte[] getHexFromString(String hexString){
        byte[] hexBytes = new byte[getByteSize(hexString.length())];
        boolean highNibble = getHexSize(hexBytes.length) == hexString.length(); /** to add 0 nibble in head byte if required */
        int byteIdx = 0;

        for(int i=0; i<hexString.length(); i++){
            String hexDigit = hexString.substring(i,i+1);
            int decValue = encodingTable.hexToDec(hexDigit);
            if(highNibble){
                hexBytes[byteIdx] = (byte)(hexBytes[byteIdx] | ((decValue & 0x0F) << 4));
            }
            else{
                hexBytes[byteIdx] = (byte)(hexBytes[byteIdx] | (decValue & 0x0F));
                byteIdx++;
            }
            highNibble = !highNibble;
        }

        return hexBytes;
    }

    public int getByteSize(int hexSize){
        return (hexSize >> 1) + ((hexSize % 2 == 0) ? 0 : 1); //2 HEX digits in 1 byte
    }

    public int getHexSize(int byteSize){
        return byteSize << 1; //2 HEX digits in 1 byte
    }

    /** reverse the byte array. First bytes should become last.
     * @param data source data
     * @return reversed data */ 
    public byte[] reverseBytes(byte[] data){
        byte[] swappedBytes = new byte[data.length];
        for(int i=0; i<data.length; i++)
            swappedBytes[data.length-i-1] = data[i];
        return swappedBytes;
    }

    public String getStringHexFromInt(int value){
        byte[] b = new byte[4];
        for (int i = 0; i < 4; i++) {
            int offset = (b.length - 1 - i) * 8;
            b[i] = (byte) ((value >>> offset) & 0xFF);
        }
        return getStringFromHex(b);       
    }

    private static class DigitsEncodingTable {
        private static final String[] decToHexlTable = new String[]{
                "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "A", "B", "C", "D", "E", "F"
        };

        private static final Map hexToDecTable = new HashMap();

        static{
            hexToDecTable.put("0", new Integer(0));
            hexToDecTable.put("1", new Integer(1));
            hexToDecTable.put("2", new Integer(2));
            hexToDecTable.put("3", new Integer(3));
            hexToDecTable.put("4", new Integer(4));
            hexToDecTable.put("5", new Integer(5));
            hexToDecTable.put("6", new Integer(6));
            hexToDecTable.put("7", new Integer(7));
            hexToDecTable.put("8", new Integer(8));
            hexToDecTable.put("9", new Integer(9));
            hexToDecTable.put("A", new Integer(10));
            hexToDecTable.put("a", new Integer(10));
            hexToDecTable.put("B", new Integer(11));
            hexToDecTable.put("b", new Integer(11));
            hexToDecTable.put("C", new Integer(12));
            hexToDecTable.put("c", new Integer(12));
            hexToDecTable.put("D", new Integer(13));
            hexToDecTable.put("d", new Integer(13));
            hexToDecTable.put("E", new Integer(14));
            hexToDecTable.put("e", new Integer(14));
            hexToDecTable.put("F", new Integer(15));
            hexToDecTable.put("f", new Integer(15));
        }

        private String decToHex(int decDigit){
            return decToHexlTable[decDigit] != null ? decToHexlTable[decDigit] : "";
        }

        private int hexToDec(String hexDigit){
            Integer decInt = (Integer)hexToDecTable.get(hexDigit);
            return decInt == null ? 0 : decInt.intValue();
        }

    }
}
