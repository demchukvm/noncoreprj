package com.tmx.beng.httpsgate.kyivstar;

import com.tmx.beng.httpsgate.HttpsGateException;
import com.tmx.util.InitException;
import org.apache.log4j.Logger;

/**
 */
public class KyivstarGate extends BasicKyivstarGate{
    public static final String GATE_NAME = "kyivstar_gate";
    protected static Logger logger = Logger.getLogger(GATE_NAME);
    private static KyivstarGate gate = null;

    public static KyivstarGate getInstance() throws HttpsGateException {
        if(gate == null){
            logger.error("Attempt to obtain not inited billing engine");
            throw new HttpsGateException("err.KyivstarGate.null_instance");
        }
        return gate;
    }

    public static void init() throws InitException {
        logger.info("Start KyivstarGate initialization");
        gate = new KyivstarGate();
        gate.init0();
        logger.info("KyivstarGate initialized");
    }
                                                                      

    public static void shutdown(final long timeout) throws InitException {
        logger.info("Start KyivstarGate Shutdown");
        gate.shutdown0(timeout);
        logger.info("KyivstarGate shutdown");
    }

    public String getGateName(){
        return GATE_NAME;
    }

}
