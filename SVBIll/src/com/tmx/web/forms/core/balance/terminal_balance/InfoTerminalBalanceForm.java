package com.tmx.web.forms.core.balance.terminal_balance;

import com.tmx.as.entities.bill.balance.AbstractBalance;
import com.tmx.as.entities.bill.balance.TerminalBalance;
import com.tmx.as.entities.bill.terminal.Terminal;
import com.tmx.web.controls.BrowseBox;
import com.tmx.web.controls.PopulateUtil;
import com.tmx.web.controls.ScrollBox;
import com.tmx.web.controls.table.ControlManagerTable;
import com.tmx.web.forms.core.balance.AbstractInfoBalanceForm;


public class InfoTerminalBalanceForm extends AbstractInfoBalanceForm {

    private Long terminalId;
    private BrowseBox browseBoxTerminals;

    protected void init(){
        super.init();
        initBrowses();
    }

    private void initBrowses(){
        browseBoxTerminals = new BrowseBox("/core/balance/terminal_balance/browseTerminalsAction");
        browseBoxTerminals.setMandatory(true);
    }

    protected void initButtonsAction() {
        onApplyButton.setAction("/core/balance/terminal_balance/infoTerminalBalance");
        onApplyTransButton.setAction("/core/balance/terminal_balance/infoTerminalBalance");
        onCancelButton.setAction("/core/balance/terminal_balance/manageTerminalBalances");
        onSaveButton.setAction("/core/balance/terminal_balance/infoTerminalBalance");
        onChangeBalanceButton.setAction("/core/balance/terminal_balance/changeTerminalBalance");
        onCreateRestButton.setAction("/core/balance/terminal_balance/terminalRestriction");
        onEditRestButton.setAction("/core/balance/terminal_balance/terminalRestriction");
        onDeleteRestButton.setAction("/core/balance/terminal_balance/terminalRestriction");
    }

    protected ScrollBox initScrollBoxBalanceOwner() {
        return scrollBoxBalanceOwner = new ScrollBox(){
            protected String getKey(Object dataListElement) {
                return ((Terminal)dataListElement).getId().toString();
            }
            protected String getValue(Object dataListElement) {
                return ((Terminal)dataListElement).getSerialNumber();
            }
        };
    }

    protected ControlManagerTable initTransactionsTable() {
        ControlManagerTable transactionsTable = new ControlManagerTable("query(query_type=retrieve_all," +
                "entity_class=com.tmx.as.entities.bill.transaction.TerminalBalanceTransaction," +
                "table_attr=terminal," +
                "table_attr=terminalBalance," +
                "table_attr=clientTransaction," +
                "order(name=transactionTime,type=desc)," +
                "page_size=15" +
                ")");
        transactionsTable.setRefreshActionName("/core/balance/terminal_balance/infoTerminalBalance");
        transactionsTable.setRefreshCommandName("refreshTransactionsTable");
        return transactionsTable;
    }

    protected void bindData(AbstractBalance balance) {
        TerminalBalance terminalBalance = (TerminalBalance) balance;
        browseBoxTerminals.setKey(terminalBalance.getTerminal().getTerminalId().toString());
        browseBoxTerminals.setValue(terminalBalance.getTerminal().getSerialNumber());
    }

    public AbstractBalance populateFromControls() {
        TerminalBalance terminalBalance = new TerminalBalance();
        Terminal terminal = new Terminal();
        terminal.setTerminalId(PopulateUtil.getLongKey(browseBoxTerminals));
        terminalBalance.setTerminal(terminal);
        return terminalBalance;
    }

      public Long getTerminalId() {
        return terminalId;
    }

    public void setTerminalId(Long terminalId) {
        this.terminalId = terminalId;
    }

    public BrowseBox getBrowseBoxTerminals() {
        return browseBoxTerminals;
    }

    public void setBrowseBoxTerminals(BrowseBox browseBoxTerminals) {
        this.browseBoxTerminals = browseBoxTerminals;
    }
}
