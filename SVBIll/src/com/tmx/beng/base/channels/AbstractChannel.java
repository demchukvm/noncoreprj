package com.tmx.beng.base.channels;

import com.tmx.beng.base.BillException;
import com.tmx.beng.base.BillingMessage;

import java.util.HashMap;
import java.util.List;


public abstract class AbstractChannel {

    private HashMap parameters = new HashMap();
    private HashMap parametersLists = new HashMap();
    private HashMap outChannels = new HashMap();

    abstract public OutChannel process(BillingMessage msg) throws BillException;

    public void addParam(String name, String val){
        parameters.put(name, val);
    }

    protected String getParamValue(String name){
        return (String)parameters.get(name);
    }

    public void addParamList(String name, List val){
        parametersLists.put(name, val);
    }

    /** Retrun params names
     * @return array of params names*/
    protected String[] getParamNames(){
        return (String[])parameters.keySet().toArray(new String[0]);
    }

    protected List getParamListValues(String name){
        return (List)parametersLists.get(name);
    }

    /** Retrun array of enclosed params lists names
     * @return array of params lists */
    protected String[] getParamListsNames(){
        return (String[])parametersLists.keySet().toArray(new String[0]);
    }

    public void addOutChannel(OutChannel outChannel){
        outChannels.put(String.valueOf(outChannel.getId()), outChannel);
    }

    protected OutChannel getOutChannel(int id) throws BillException{
        OutChannel channel = (OutChannel)outChannels.get(String.valueOf(id));
        if(channel == null)
            //throw new BillException("err.bill.out_channel_is_absent", new String[]{String.valueOf(id)});
            throw new BillException("err.bill.out_channel_is_absent.service_is_blocked", new String[]{String.valueOf(id)});
        return channel;
    }

    public static class OutChannel{
        private int id;
        private String name;

        public OutChannel(int id, String name){
            this.id = id;
            this.name = name;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }

}
