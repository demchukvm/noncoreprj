package com.tmx.web.taglib.html;

import org.apache.struts.taglib.TagUtils;
import org.apache.struts.taglib.html.Constants;
import org.apache.struts.util.MessageResources;

import javax.servlet.jsp.tagext.TagSupport;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import java.io.IOException;

/**
 * HTML Body tag
 */
public class BodyTag extends TagSupport {
    /**
     * The onload event script.
     */
    private String onload = null;
    /**
     * The onclick event script.
     */
    private String onclick = null;

    /**
     * The message resources for this package.
     */
    protected static MessageResources messages =
        MessageResources.getMessageResources(Constants.Package + ".LocalStrings");
    //-----------------Properties

    public String getOnload() {
        return onload;
    }

    public void setOnload(String onload) {
        this.onload = onload;
    }

    public String getOnclick() {
        return onclick;
    }

    public void setOnclick(String onclick) {
        this.onclick = onclick;
    }

    // --------------------------------------------------------- Public Methods

    /**
     * Render the beginning of this form.
     *
     * @exception javax.servlet.jsp.JspException if a JSP exception has occurred
     */
    public int doStartTag() throws JspException {
        // Create an appropriate "body" element based on our parameters
        StringBuffer results = new StringBuffer();
        results.append(this.renderBodyStartElement());
        TagUtils.getInstance().write(pageContext, results.toString());
        return (EVAL_BODY_INCLUDE);
    }



    //-----------------------------------------Private methods
    /**
     * Generates the opening <code>&lt;body&gt;</code> element with appropriate
     * attributes.
     */
    protected String renderBodyStartElement() {
        StringBuffer results = new StringBuffer("<body");

        // render attributes

        /** Suppress rendering of this attributes to prevent
         * overriding of onEvent scripts assigned with this body tag
         * in the globals.js.
          */
        //renderAttribute(results, "onload", getOnload());
        //renderAttribute(results, "onclick", getOnclick());

        // Hook for additional attributes
        renderOtherAttributes(results);

        results.append(">\r");

        /** Render JS to wrap onEvent handlers into functions
         * registered in globals.js
         */
        renderOnEventScriptWrapper(results);

        return results.toString();
    }

    /**
     * Renders attribute="value" if not null
     */
    protected void renderAttribute(StringBuffer results, String attribute, String value) {
        if (value != null) {
            results.append(" ");
            results.append(attribute);
            results.append("=\"");
            results.append(value);
            results.append("\"");
        }
    }

    /**
     * 'Hook' to enable this tag to be extended and
     *  additional attributes added.
     */
    protected void renderOtherAttributes(StringBuffer results) {
    }


    protected void renderOnEventScriptWrapper(StringBuffer results){
        results.append("<script type=\"text/javascript\">\r");
        results.append("<!--\r");
        if(getOnload() != null)
            results.append("    registerFunction('window.onload', function (){"+getOnload()+"});\r");
        if(getOnclick() != null)
            results.append("    registerFunction('document.onclick', function (){"+getOnclick()+"});\r");
        results.append("-->\r");
        results.append("</script>\r");
    }

    /**
     * Render the end of body.
     *
     * @exception JspException if a JSP exception has occurred
     */
    public int doEndTag() throws JspException {
        // Render a tag representing the end of our current form
        StringBuffer results = new StringBuffer("</body>");
        // Print this value to our output writer
        JspWriter writer = pageContext.getOut();
        try {
            writer.print(results.toString());
        } catch (IOException e) {
            throw new JspException(messages.getMessage("common.io", e.toString()));
        }

        // Continue processing this page
        return (EVAL_PAGE);

    }


    public void release() {
        super.release();
        onclick = null;
        onload = null;
    }

}
