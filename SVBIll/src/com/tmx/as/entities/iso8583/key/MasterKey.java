package com.tmx.as.entities.iso8583.key;

import com.tmx.as.base.BasicEntity;

import java.io.Serializable;

/**
 Master key
 */
public class MasterKey extends BasicEntity {
    private Long masterKeyId;
    private String terminalId;
    private String keyName;
    private String encryptedMasterKey;
    private Integer index;

    public Long getMasterKeyId() {
        return masterKeyId;
    }

    public void setMasterKeyId(Long masterKeyId) {
        this.masterKeyId = masterKeyId;
    }

    public String getTerminalId() {
        return terminalId;
    }

    public void setTerminalId(String terminalId) {
        this.terminalId = terminalId;
    }

    public String getKeyName() {
        return keyName;
    }

    public void setKeyName(String keyName) {
        this.keyName = keyName;
    }

    public String getEncryptedMasterKey() {
        return encryptedMasterKey;
    }

    public void setEncryptedMasterKey(String encryptedMasterKey) {
        this.encryptedMasterKey = encryptedMasterKey;
    }

    public Integer getIndex() {
        return index;
    }

    public void setIndex(Integer index) {
        this.index = index;
    }

    public Serializable getId() {
        return getMasterKeyId();
    }

    public int getValid() {
        return valid;
    }

    public void setValid(int valid) {
        this.valid = valid;
    }
}
