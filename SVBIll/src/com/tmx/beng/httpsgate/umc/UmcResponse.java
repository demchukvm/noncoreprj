package com.tmx.beng.httpsgate.umc;

import org.dom4j.*;
import java.util.Iterator;

import com.tmx.beng.httpsgate.BadResponseException;


/**
 * UMC response. Parses XML to java model
 */
public class UmcResponse {
    private String msisdn;  
    private Long phoneType;
    private Long error;
    private String partnerId;
    private String transId;
    private Long result;
    private Double quota;
    private Double balance;

    public static UmcResponse parseResponse(String response) throws BadResponseException {
        UmcResponse umcResponse = new UmcResponse();
        try{
            Document doc = DocumentHelper.parseText(response.trim());
            Element root = doc.getRootElement();
            if(!root.getName().equalsIgnoreCase("response"))
                throw new BadResponseException("Root root is not 'response'");

            for(Iterator it = root.elementIterator(); it.hasNext(); ){
                Element element = (Element)it.next();

                // msisdn
                if(element.getName().equalsIgnoreCase("msisdn")){
                    umcResponse.msisdn = element.getTextTrim();
                    continue;
                }

                // phone_type
                if(element.getName().equalsIgnoreCase("phone_type")){
                    umcResponse.phoneType = element.getTextTrim() != null  && !"null".equals(element.getTextTrim().toLowerCase()) ? new Long(element.getTextTrim()) : null;
                    continue;
                }

                // error
                if(element.getName().equalsIgnoreCase("error")){
                    umcResponse.error = element.getTextTrim() != null && !"null".equals(element.getTextTrim().toLowerCase()) ? new Long(element.getTextTrim()) : null;
                    continue;
                }

                // partnerID
                if(element.getName().equalsIgnoreCase("partnerID")){
                    umcResponse.partnerId = element.getTextTrim();
                    continue;
                }

                // transID
                if(element.getName().equalsIgnoreCase("transID")){
                    umcResponse.transId = element.getTextTrim();
                    continue;
                }

                // result
                if(element.getName().equalsIgnoreCase("result")){
                    umcResponse.result = element.getTextTrim() != null  && !"null".equals(element.getTextTrim().toLowerCase()) ? new Long(element.getTextTrim()) : null;
                    continue;
                }

                // quota
                if(element.getName().equalsIgnoreCase("quota")){
                    umcResponse.quota = new Double(element.getTextTrim());
                    continue;
                }
                if(element.getName().equalsIgnoreCase("balance")){
                    umcResponse.balance = new Double(element.getTextTrim());
                }

            }
        }
        catch(NumberFormatException e){
            throw new BadResponseException(e.getMessage(), e);
        }
        catch(DocumentException e){
            throw new BadResponseException("Parse UMC response error " + e.getMessage(), e);
        }
        return umcResponse;
    }

    public String getMsisdn() {
        return msisdn;
    }

    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }

    public Long getPhoneType() {
        return phoneType;
    }

    public void setPhoneType(Long phoneType) {
        this.phoneType = phoneType;
    }

    public Long getError() {
        return error;
    }

    public void setError(Long error) {
        this.error = error;
    }

    public String getPartnerId() {
        return partnerId;
    }

    public void setPartnerId(String partnerId) {
        this.partnerId = partnerId;
    }

    public String getTransId() {
        return transId;
    }

    public void setTransId(String transId) {
        this.transId = transId;
    }

    public Long getResult() {
        return result;
    }

    public void setResult(Long result) {
        this.result = result;
    }

    public Double getQuota() {
        return quota;
    }

    public void setQuota(Double quota) {
        this.quota = quota;
    }

    public Double getBalance() {
        return balance;
    }

    public void setBalance(Double balance) {
        this.balance = balance;
    }
}
