package com.tmx.beng.httpsgate.dacard;

import com.tmx.beng.medaccess.GateException;
import java.util.Locale;

/**
 */
public class DacardGateException extends GateException {

    public DacardGateException(String msg, String[] params, Throwable e, Locale locale) {
        super(msg, params, e, locale);
    }

    public DacardGateException(String msg, String[] params) {
        super(msg, params);
    }

    public DacardGateException(String msg, String[] params, Locale locale) {
        super(msg, params, locale);
    }

    public DacardGateException(String msg, Throwable e, Locale locale) {
        super(msg, e, locale);
    }

    public DacardGateException(String msg, Throwable e) {
        super(msg, e);
    }

    public DacardGateException(String msg, Locale locale) {
        super(msg, locale);
    }

    public DacardGateException(String msg) {
        super(msg);
    }

    public DacardGateException(String msg, String[] params, Throwable e) {
        super(msg, params, e);
    }
}

