package com.tmx.beng.httpsgate.avancel;

import org.apache.log4j.Logger;
import com.tmx.beng.httpsgate.HttpsGateException;
import com.tmx.util.InitException;

/**

 */
public class AvancelGate1 extends BasicAvancelGate{
    protected static final String GATE_NAME = "avancel_gate_1";
    protected static Logger logger = Logger.getLogger("avancel_gate_1." + GATE_NAME);
    private static AvancelGate1 avancelGate = null;


    public static AvancelGate1 getInstance() throws HttpsGateException {
        if(avancelGate == null){
            logger.error("Attempt to obtain not inited AvancelGate1");
            throw new HttpsGateException("err.AvancelGate1.null_instance");
        }
        return avancelGate;
    }

    public static void init()throws InitException {
        logger.info("Start AvancelGate1 initialization");
        avancelGate = new AvancelGate1();
        avancelGate.init0();
        logger.info("AvancelGate1 initialized");
    }

    public static void shutdown(final long timeout) {
        logger.info("Start AvancelGate1 Shutdown");
        avancelGate.shutdown0(timeout);
        logger.info("AvancelGate1 shutdown");
    }

    protected String getGateName(){
        return GATE_NAME;
    }    

}
