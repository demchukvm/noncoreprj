package com.tmx.util;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.w3c.dom.Node;
import org.w3c.dom.Element;
import org.apache.xml.serialize.OutputFormat;
import org.apache.xml.serialize.Method;
import org.xml.sax.helpers.AttributesImpl;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.*;
import java.util.NoSuchElementException;
import java.util.Iterator;
import java.net.URL;


//TODO RM e.printStackTrace() --> log & exception throwing


public class XMLUtil {

    private static DocumentBuilder dbuilder;

    public static DocumentBuilder getDocumentBuilder() {
        if (dbuilder == null) {
            /*
            if (System.getProperty("javax.xml.parsers.DocumentBuilderFactory") == null)
                System.getProperties().put("javax.xml.parsers.DocumentBuilderFactory"
                        ,"org.apache.xerces.jaxp.DocumentBuilderFactoryImpl");
            if (System.getProperty("javax.xml.parsers.SAXParserFactory") == null)
                System.getProperties().put("javax.xml.parsers.SAXParserFactory", "org.apache.xerces.jaxp.SAXParserFactoryImpl");
            */

            DocumentBuilderFactory dFactory = DocumentBuilderFactory.newInstance();
            try {
                dbuilder = dFactory.newDocumentBuilder();
            } catch (ParserConfigurationException e) {
                //TODO use Exeptions from properties
                throw new RuntimeException("err.xml_parser_failed");
            }
        }
        return dbuilder;
    }

    public static OutputFormat getDefaultXMLFormat() {
        OutputFormat format = new OutputFormat(Method.XML, null, true);
        format.setPreserveSpace(true);
        format.setLineWidth(9999999);
        return format;
    }

    public static String serializeDOM(Document doc) throws IOException {
        return serializeDOM(doc, getDefaultXMLFormat());
    }

    public static String serializeDOM(Document doc, OutputFormat format) throws IOException {
        StringWriter dest = new StringWriter();
        org.apache.xml.serialize.XMLSerializer serializer = new org.apache.xml.serialize.XMLSerializer(dest, format);
        serializer.asDOMSerializer().serialize(doc);
        return dest.toString();
    }

    static class ElementIterator implements java.util.Iterator {
        NodeList nl;
        Node next = null;
        int index = -1;
        public ElementIterator(Node node) {
            nl = node.getChildNodes();
        }
        void findNext() {
            if (next != null) return;
            ;
            for(index++;index<nl.getLength();index++) {
                if (nl.item(index) instanceof Element) {
                    next = nl.item(index);
                    break;
                }
            }
        }
        public boolean hasNext() {
            if (next == null) findNext();
            return next != null;
        }
        public Object next() {
            if (!hasNext()) throw new NoSuchElementException();
            Node o = next;
            next = null;
            return o;
        }

        public void remove() {
        }
    }

    public static Iterator getNodeElementIterator(Node node) {
        return new ElementIterator(node);
    }

    public static org.xml.sax.Attributes getNodeAttributes(Node node) {
        AttributesImpl ai = new AttributesImpl();

        NodeList attributes = node.getChildNodes();
        for (int j = 0; j < attributes.getLength(); j++) {
            Node an = attributes.item(j);
            if (an.getNodeType() == Node.ATTRIBUTE_NODE)
                ai.addAttribute(an.getNamespaceURI()
                        , an.getLocalName()
                        , an.getNodeName()
                        , "CDATA"
                        , an.getNodeValue()
                );
        }

        return ai;
    }

    public static Document getDocument(String xmlFragment) {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        StringReader stringReader = new StringReader(xmlFragment);
        InputSource inputSource = new InputSource(stringReader);
        Document res = null;
        try {
            res = factory.newDocumentBuilder().parse(inputSource);
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        }
        return res;
    }

    public static Document getDocumentFromURL(URL url){
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        Document res = null;
        InputStream input = null;
        try {
            InputSource is = new InputSource(url.toExternalForm());
            input = url.openStream();
            is.setByteStream(input);
            res = factory.newDocumentBuilder().parse(is);
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        }
        finally {
            try{input.close();}catch(IOException e) {}
        }
        return res;
    }

    public static Document getDocumentFromFile(String filename) {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        Document res = null;
        FileInputStream fis = null;
        try {
            fis = new FileInputStream(filename);
            res = factory.newDocumentBuilder().parse(fis);
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        }
        finally {
            try{fis.close();}catch(Exception e) {}
        }
        return res;
    }


}