package com.tmx.web.taglib.html.ctrl;

import com.tmx.web.controls.Button;
import com.tmx.web.controls.BasicControl;
import com.tmx.web.base.WebException;
import com.tmx.web.base.ApplicationEnvironment;
import com.tmx.web.base.Roadmap;

import javax.servlet.jsp.JspException;
import org.apache.struts.config.ModuleConfig;
import org.apache.struts.config.ForwardConfig;
import org.apache.struts.config.ActionConfig;
import org.apache.struts.taglib.TagUtils;

import java.util.Hashtable;
import java.util.Enumeration;

/**
 */
public class ButtonTag extends AbstractControlTag {
    protected String styleClass = null;
    protected String disabledStyleClass = null;
    protected String hoveredStyleClass = null;

    private String pageName = null;

    //-------------------------Properties

    public String getStyleClass() {
        return (styleClass != null) ? styleClass : getDefaultProperty("tag.button.style.styleClass");
    }

    public void setStyleClass(String styleClass) {
        this.styleClass = styleClass;
    }

    public String getDisabledStyleClass() {
        return (disabledStyleClass != null) ? disabledStyleClass : getDefaultProperty("tag.button.style.disabledStyleClass");
    }

    public void setDisabledStyleClass(String disabledStyleClass) {
        this.disabledStyleClass = disabledStyleClass;
    }

    public String getHoveredStyleClass() {
        return (hoveredStyleClass != null) ? hoveredStyleClass : getDefaultProperty("tag.button.style.hoveredStyleClass");
    }

    public void setHoveredStyleClass(String hoveredStyleClass) {
        this.hoveredStyleClass = hoveredStyleClass;
    }

    public String getPageName() {
        return pageName;
    }

    public void setPageName(String pageName) {
        this.pageName = pageName;
    }

    //------------------------------Abstract methods
    protected int controlStartTag() throws JspException {
        StringBuffer resultHTML = new StringBuffer();
        Button button = getButton();
        if(button == null){
            TagUtils.getInstance().write(pageContext,"Button "+getProperty()+" not found");
            return SKIP_BODY;
        }
        renderButton(resultHTML, button);
        TagUtils.getInstance().write(pageContext,resultHTML.toString());
        return SKIP_BODY;
    }

    protected void renderButton(StringBuffer resultHTML, Button button) {
        String type = " type=\"button\" ";
        String name = " name=\""+getProperty()+"\" ";
        String value = " value=\""+button.getLocalizedLabel()+"\" ";
        String styleClass = " class=\""+(button.isReadonly() ? getDisabledStyleClass() : getStyleClass())+"\" ";
        String readonly = (button.isReadonly()) ? " disabled " : "";
        resultHTML.append("<input " + type + name + value + styleClass + readonly + getOnEventAttrsString() + "\\>\r");

        // render button params
        Hashtable params=button.getParams();
        Enumeration enumeration=params.keys();
        while (enumeration.hasMoreElements()) {
            String paramName=(String)enumeration.nextElement();
            resultHTML.append("\n<input type='hidden' name='"+paramName+"' value='"+params.get(paramName)+"'/>\n");
        }
    }

    protected String renderOnClickScript(BasicControl ctrl) throws WebException{
        StringBuffer resultHTML = new StringBuffer();
        Button button = (Button)ctrl;

        //Add script entry from onClick attr or default for button
        if(button.isReadonly()){
            resultHTML.append("     //do nothing, because button is disabled. \r");
        }
        else{
            String actionUrl = evaluateAction(button);
            resultHTML.append("     document.forms[0].action=\""+actionUrl+"\";\r");
            resultHTML.append("     document.forms[0].method='POST';\r");
            resultHTML.append("     document.forms[0].submit();\r");
        }
        return resultHTML.toString();
    }

    protected String evaluateAction(Button button) throws WebException{
        String url = null;
        int xorValue = 0;
        if(button.getAction() != null){
            xorValue++;
            ModuleConfig mc = ApplicationEnvironment.getModuleConfig();
            ActionConfig actionConfig = mc.findActionConfig(button.getAction());
            if(actionConfig == null)
                throw new WebException("err.button_tag.unregistered_action", new String[]{button.getAction()});
            url = ApplicationEnvironment.getAppDeploymentName() + actionConfig.getPath() + ApplicationEnvironment.getActionExtension();
        }
        else if(button.getForward() != null){
            xorValue++;
            ModuleConfig mc = ApplicationEnvironment.getModuleConfig();
            ForwardConfig forwardConfig = mc.findForwardConfig(button.getForward());
            url = ApplicationEnvironment.getAppDeploymentName() + forwardConfig.getPath();
        }
        else if(button.getUrl() != null){
            xorValue++;
            url = button.getUrl();
        }

        if(xorValue == 1){
            if(button.getCommand() != null)
                url = url + "?" + COMMAND + "=" + button.getCommand();

            return url;
        }


        //if there is no explicit dispatch info set for button then
        // try to obtain it from roadmap
        String pageName = getPageName() != null ? getPageName() : button.getListenEventOnPage();
        String onEvent = button.getListenDispatchEvent();
        if(pageName != null){
            xorValue=0;
            url = null;
            Roadmap roadmap = getRoadmap();

            String actionUrl = null;
            String forwardUrl = null;
            try{
                actionUrl = roadmap.getPageActionURL(pageName, onEvent);
            }
            catch(WebException e){
                actionUrl = null;
            }
            try{
                forwardUrl = roadmap.getPageForwardURL(pageName, onEvent);
            }
            catch(WebException e){
                forwardUrl = null;
            }

            if(actionUrl != null){
                xorValue++;
                url = actionUrl;
            }
            else if(forwardUrl != null){
                xorValue++;
                url = forwardUrl;
            }

            if(xorValue == 1){
                if(roadmap.getPageCommand(pageName, onEvent) != null)
                    url = url + "?" + COMMAND + "=" + roadmap.getPageCommand(pageName, onEvent);

                return url;
            }
        }

        if(xorValue > 1)
            throw new WebException("err.tag.button.invalid_attribute_combination");
        else
            return null;

    }

    private Button getButton() throws JspException{
        Object object = getPropertyObject();
        return (object != null && object instanceof Button) ?
               (Button)object : null;
    }
}
