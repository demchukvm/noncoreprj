package com.tmx.web.actions.core.tariff;

import com.tmx.as.blogic.balance.AbstractBalanceManager;
import com.tmx.as.entities.bill.function.ActualFunctionParameter;

import com.tmx.web.base.Roadmap;
import com.tmx.web.base.BasicDispatchedAction;
import com.tmx.web.base.WebException;
import com.tmx.web.forms.core.tariff.TariffParameterForm;
import com.tmx.web.forms.core.tariff.AbstractInfoTariffForm;
import com.tmx.web.controls.table.ControlManagerTable;
import com.tmx.util.InitException;
import com.tmx.beng.base.BillException;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Collections;

abstract public class AbstractTariffParameterAction extends BasicDispatchedAction {

    abstract protected AbstractBalanceManager getBalanceManager() throws InitException, BillException;
    abstract protected AbstractInfoTariffForm getInfoTariffForm(ActionMapping mapping, HttpServletRequest request) throws WebException;

    public ActionForward preloadForDelete(ActionMapping mapping,
                                          ActionForm form,
                                          HttpServletRequest request,
                                          HttpServletResponse response) throws Throwable {
        Roadmap roadmap=getRoadmap();
        roadmap.collectParameter("/core/message/confirmation", "leftMenuName", "balances");
        roadmap.setPageForward("confirmation","onOk","local:"+this.getName()+"#"+"deleteParameterAction");
        roadmap.setPageForward("confirmation","onCancel","local:"+this.getName()+"#"+ "update");
        return mapping.findForward("confirmation");
    }

    public ActionForward delete(ActionMapping mapping,
                              ActionForm form,
                              HttpServletRequest request,
                              HttpServletResponse response) throws Throwable {
        TariffParameterForm parameterForm = (TariffParameterForm) verifyForm(form, TariffParameterForm.class);
        AbstractInfoTariffForm tariffFrom = getInfoTariffForm(mapping, request);
        ActualFunctionParameter delParam = findParam(tariffFrom.getFuncParametrsTable(), parameterForm.getParameterId());
        tariffFrom.getFuncParametrsTable().removeData(Collections.singletonList(delParam));
        return mapping.findForward("update");
    }

    public ActionForward preloadForCreate(ActionMapping mapping,
                              ActionForm form,
                              HttpServletRequest request,
                              HttpServletResponse response) throws Throwable {
        TariffParameterForm parameterForm = (TariffParameterForm) verifyForm(form, TariffParameterForm.class);
        parameterForm.reset();
        parameterForm.setParameterId(null);
        return mapping.findForward("parameterPage");
    }

    public ActionForward preloadForUpdate(ActionMapping mapping,
                              ActionForm form,
                              HttpServletRequest request,
                              HttpServletResponse response) throws Throwable {
        TariffParameterForm parameterForm = (TariffParameterForm) verifyForm(form, TariffParameterForm.class);
        AbstractInfoTariffForm tariffForm = getInfoTariffForm(mapping, request);
        ActualFunctionParameter param = findParam(tariffForm.getFuncParametrsTable(), parameterForm.getParameterId());
        parameterForm.reset();
        parameterForm.bindData(param);
        return mapping.findForward("parameterPage");
    }

    public ActionForward save(ActionMapping mapping,
                              ActionForm form,
                              HttpServletRequest request,
                              HttpServletResponse response) throws Throwable {
        TariffParameterForm parameterForm = (TariffParameterForm) verifyForm(form, TariffParameterForm.class);
        parameterForm.validate();

        if(parameterForm.getEditBoxValue().getValue().contains(","))
        {
            parameterForm.getEditBoxValue().setValue(parameterForm.getEditBoxValue().getValue().replace(',', '.'));   
        }

        AbstractInfoTariffForm tariffForm = getInfoTariffForm(mapping, request);

        ActualFunctionParameter param = parameterForm.getParameterId() == null ? new ActualFunctionParameter() :// if create new param
                findParam(tariffForm.getFuncParametrsTable(), parameterForm.getParameterId());

        param.setName(parameterForm.populateFromControl().getName());
        param.setOrderNum(parameterForm.populateFromControl().getOrderNum());
        param.setValue(parameterForm.populateFromControl().getValue());
        param.setType(parameterForm.populateFromControl().getType());
        
        if (parameterForm.getParameterId() == null) { //if create new param
            tariffForm.getFuncParametrsTable().addData(Collections.singletonList(param));
        }
        
        return mapping.findForward("update");
    }

    private ActualFunctionParameter findParam(ControlManagerTable table, Long paramId) {
        return (ActualFunctionParameter) table.getData().get(paramId.intValue());
    }

    /**
     * @deprecated
     */
    public ActionForward apply(ActionMapping mapping,
                              ActionForm form,
                              HttpServletRequest request,
                              HttpServletResponse response) throws Throwable {
        save(mapping, form, request, response);
        return mapping.findForward("parameterPage");
    }

}
