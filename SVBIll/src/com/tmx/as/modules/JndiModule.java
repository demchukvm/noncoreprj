package com.tmx.as.modules;

import com.tmx.as.base.SecurityService;
import com.tmx.as.jndi.JndiService;
import com.tmx.util.InitException;

import java.util.Properties;

/**
 * Created by IntelliJ IDEA.
 * User: shake
 * Date: 25.06.2007
 * Time: 15:56:19
 */
public class JndiModule extends AbstractModule{

    public void start(Properties props)throws ModuleException{
        try{
            JndiService.getInstance().reload();
        }catch(InitException e){
            throw new ModuleException(e.getLocalizedMessage());
        }
    }

    public void shutdownGraceful(Properties props)throws ModuleException{
    }

    public void shutdownAbort(Properties props)throws ModuleException{
    }
}
