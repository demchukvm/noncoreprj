package com.tmx.beng.httpsgate.dacard.pdu;

import java.io.OutputStream;
import java.io.InputStream;

/**
 Data elemnt ype protocol assembler-disassembler
 */
public interface TypePAD<T> {

    /** Writes to output stream data bytes of the value.
     * @param os the output stream used to serialize value into;
     * @param value the value to serialize into output stream.
     * @throws DacardPadException on sericalization exception. */
    public void serialize(OutputStream os, T value) throws DacardPadException;
    public T parse(InputStream is, short dataSize) throws DacardPadException;
    public void serializeLengthInBytes(OutputStream os, T value) throws DacardPadException;
}
