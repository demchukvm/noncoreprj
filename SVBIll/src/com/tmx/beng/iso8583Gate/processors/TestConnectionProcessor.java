package com.tmx.beng.iso8583Gate.processors;

import com.tmx.beng.iso8583Gate.pdu.RuntimeMessage;

/**

 */
public class TestConnectionProcessor extends BasicProcessor{

    public int getMessageTypeId() {
        return 810;
    }

    public int getProcessingCode() {
        return 990000;
    }

    public int[] getDEToCopy() {
        return new int[]{11, 24, 41};
    }

    public void processSpecificTask(RuntimeMessage reqMsg, RuntimeMessage respMsg) {
    }
}
