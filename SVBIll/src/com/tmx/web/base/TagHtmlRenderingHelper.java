package com.tmx.web.base;

import java.util.*;

/**
 * Supports tags rendering into HTML.
 * The main issue this helper should solve is to support coordination
 * between tags. As tags are processed mutually independently within page they
 * are defined in, they need such helper to allocate shared JS array's items,
 * to create parts of shared JS procedures and etc.
 */
public class TagHtmlRenderingHelper extends TagRenderingHelper{

    /** Reset helper */
    public void reset(){
        scriptArraysSizes = new HashMap();
        //scriptFunctions = new HashMap();
    }

    //---------------Shared Script arrays support
    private Map scriptArraysSizes = new HashMap();
    /** Allocates next element of specified JS array and returns
     * its index */
    public int allocateScriptArrayElement(String scriptArrayName){
        Integer currentSize = (Integer)scriptArraysSizes.get(scriptArrayName);
        if(currentSize == null){
            scriptArraysSizes.put(scriptArrayName, new Integer(0));
            return 0;
        }
        else{
            Integer incrementedValue = new Integer(currentSize.intValue()+1);
            scriptArraysSizes.put(scriptArrayName, incrementedValue);
            return incrementedValue.intValue();
        }
    }


    //--------------Shared Script functions support

//  RM: Don't used
//
//    private Map scriptFunctions = new HashMap();
//
//    /**
//     * Appends given script fragment to the function
//     * @param functionSignature signature of function the script fragment to be added to
//     * Example: myFunction(myParam1, myParam2)
//     * @param entryBody the script fragment to be added
//     * */
//    public void appendScriptFunctionEntry(String functionSignature, String entryBody){
//        List entryList = (List)scriptFunctions.get(functionSignature);
//        entryBody = entryBody.endsWith(";") ? entryBody : entryBody + ";";
//        if(entryList == null){
//            entryList = new ArrayList();
//            scriptFunctions.put(functionSignature, entryList);
//        }
//        entryList.add(entryBody);
//    }
//
//    /**
//     * render script function
//     * */
//    public String renderJsFunction(String functionSignature){
//        StringBuffer functionCode = new StringBuffer("function ");
//        functionCode.append(functionSignature);
//        functionCode.append("{\r");
//        List entryList = (List)scriptFunctions.get(functionSignature);
//        if(entryList != null){
//            for(Iterator iter = entryList.iterator(); iter.hasNext();)
//                functionCode.append(iter.next() + "\r");
//        }
//        functionCode.append("}\r\r");
//        return functionCode.toString();
//    }
}
