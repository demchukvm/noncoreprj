package com.tmx.web.actions.core.transactions.operator_balance_transaction;

import com.tmx.web.base.BasicDispatchedAction;
import com.tmx.web.forms.core.transaction.operator_balance_transaction.InfoOperatorBalanceTransactionForm;
import com.tmx.as.blogic.TransactionManager;
import com.tmx.as.entities.bill.transaction.OperatorBalanceTransaction;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForm;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class InfoOperatorBalanceTransactionAction extends BasicDispatchedAction {

     private static final String FORWARD_INFO_OPERATOR_TRANSACTION_PAGE = "infoOperatorTransactionPage";

    public ActionForward defaultAction(ActionMapping mapping,
                                       ActionForm form,
                                       HttpServletRequest request,
                                       HttpServletResponse response,
                                       String methodName) throws Throwable {
        InfoOperatorBalanceTransactionForm infoOperatorBalanceTransactionForm = 
                (InfoOperatorBalanceTransactionForm) verifyForm(form, InfoOperatorBalanceTransactionForm .class);

        infoOperatorBalanceTransactionForm.reset();
        TransactionManager transactionManager = new TransactionManager();

        OperatorBalanceTransaction operatorBalanceTransaction =
                transactionManager.getOperatorBalanceTransaction(infoOperatorBalanceTransactionForm.getSelectedTransactionId());

        infoOperatorBalanceTransactionForm.bindData(operatorBalanceTransaction);

        return mapping.findForward(FORWARD_INFO_OPERATOR_TRANSACTION_PAGE);
    }

    public ActionForward preloadForCancel(ActionMapping mapping,
                                       ActionForm form,
                                       HttpServletRequest request,
                                       HttpServletResponse response) throws Throwable {
        return mapping.findForward((String) getRoadmap().getParameterValue(this.getName(), "onCancelAction", true));
    }
}
