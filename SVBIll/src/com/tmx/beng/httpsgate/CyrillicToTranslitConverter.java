package com.tmx.beng.httpsgate;

import java.util.HashMap;
import java.util.HashSet;

/**
 * Created by IntelliJ IDEA.
 * User: Proximan
 * Date: 15.12.2010
 * Time: 11:02:03
 * To change this template use File | Settings | File Templates.
 */
public class CyrillicToTranslitConverter
{
    public static String toTranslitString(String cyrillicString)
    {
        HashMap<String, String> alpha = new HashMap<String, String>();
        alpha.put("а", "a"); alpha.put("А", "A");
        alpha.put("б", "b"); alpha.put("Б", "B");
        alpha.put("в", "v"); alpha.put("В", "V");
        alpha.put("г", "g"); alpha.put("Г", "G");
        alpha.put("д", "d"); alpha.put("Д", "D");
        alpha.put("е", "e"); alpha.put("Е", "E");
        alpha.put("ё", "e"); alpha.put("Ё", "E");
        alpha.put("ж", "j"); alpha.put("Ж", "J");
        alpha.put("з", "z"); alpha.put("З", "Z");
        alpha.put("и", "i"); alpha.put("И", "I");
        alpha.put("й", "y"); alpha.put("Й", "Y");
        alpha.put("к", "k"); alpha.put("К", "K");
        alpha.put("л", "l"); alpha.put("Л", "L");
        alpha.put("м", "m"); alpha.put("М", "M");
        alpha.put("н", "n"); alpha.put("Н", "N");
        alpha.put("о", "o"); alpha.put("О", "O");
        alpha.put("п", "p"); alpha.put("П", "P");
        alpha.put("р", "r"); alpha.put("Р", "R");
        alpha.put("с", "s"); alpha.put("С", "S");
        alpha.put("т", "t"); alpha.put("Т", "T");
        alpha.put("у", "u"); alpha.put("У", "U");
        alpha.put("ф", "f"); alpha.put("Ф", "F");
        alpha.put("х", "h"); alpha.put("Х", "H");
        alpha.put("ц", "c"); alpha.put("Ц", "C");
        alpha.put("ч", "4"); alpha.put("Ч", "4");
        alpha.put("ш", "w"); alpha.put("Ш", "W");
        alpha.put("щ", "w"); alpha.put("Щ", "W");
        alpha.put("ъ", "'");
        alpha.put("ы", "i");
        alpha.put("ь", "'"); 
        alpha.put("э", "e"); alpha.put("Э", "E");
        alpha.put("ю", "yu"); alpha.put("Ю", "YU");
        alpha.put("я", "a"); alpha.put("Я", "YA");

        alpha.put("ґ", "g"); alpha.put("Ґ", "G");
        alpha.put("є", "e"); alpha.put("Є", "E");
        alpha.put("і", "i"); alpha.put("І", "I");

        String translitString = "";

        for(int i=0; i<cyrillicString.length(); i++)
        {
            String ch = cyrillicString.charAt(i)+"";
            if(alpha.containsKey(ch))
                translitString += alpha.get(ch);
            else
                translitString += ch;
        }

        return translitString;
    }
}

