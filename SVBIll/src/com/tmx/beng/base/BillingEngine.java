package com.tmx.beng.base;

import com.tmx.util.queue.ActionQueue;
import com.tmx.util.InitException;
import org.apache.log4j.Logger;


public class BillingEngine {
    private static Logger logger = Logger.getLogger("bill");
    private static BillingEngine billingEngine = null;
    private GlobalContext globalContext = null;

    private ActionQueue processingQueue = null;

    /** hide constructor */
    private BillingEngine(){
    }

    public static void init() throws InitException {
        logger.info("Start Billing Engine initialization");
        billingEngine = new BillingEngine();
        billingEngine.init0();
        logger.info("Billing Engine successfully initialized");
    }

    public void init0() throws InitException {
        globalContext = new GlobalContext();
        //initialize last
        globalContext.init();
        processingQueue = new ActionQueue(1,5);
    }

    public static BillingEngine getInstance() throws BillException{
        if(billingEngine == null){
            logger.error("Attempt to obtain not inited billing engine");
            throw new BillException("err.bill_eng.null_instance");
        }
        return billingEngine;
    }

    void processBillMessage(BillingMessage message, CallBack callBack) throws BillException{
        Task task = globalContext.allocateTask(message, callBack);
        processingQueue.enqueue(task);
    }

    public Reporter getReporter(){
        return globalContext.getReporter();
    }

    public MediumAccessPool getMediumAccessPool() {
        return globalContext.getMediumAccessPool();
    }
}
