package com.tmx.beng.httpsgate.beeline;

import org.dom4j.*;

import java.util.Date;
import java.util.Iterator;
import java.text.SimpleDateFormat;
import java.text.ParseException;

import com.tmx.beng.httpsgate.BadResponseException;

/**
 * Beeline response. Parses XML to java model   
 */
public class BeelineResponse {
    private Long statusCode = null;
    private Long payId = null;
    private String receipt = null;
    private Date timeStamp = null;
    private Long payStatus = null;

    private static final String TIMESTAMP_FORMAT = "dd.MM.yyyy HH:mm:ss";

    public static BeelineResponse parseResponse(String response)throws BadResponseException {
        BeelineResponse beelineResponse = new BeelineResponse();
        try{
            Document doc = DocumentHelper.parseText(response.trim());
            Element root = doc.getRootElement();
            if(!root.getName().equalsIgnoreCase("pay-response")){
                throw new BadResponseException("Root root is not pay-response");
            }

            for(Iterator it = root.elementIterator(); it.hasNext(); ){
                Element element = (Element)it.next();

                // statusCode
                if(element.getName().equalsIgnoreCase("status_code")){
                    beelineResponse.statusCode = new Long(element.getTextTrim());
                    continue;
                }

                // pay_id
                if(element.getName().equalsIgnoreCase("pay_id")){
                    beelineResponse.payId = new Long(element.getTextTrim());
                    continue;
                }

                // receipt
                if(element.getName().equalsIgnoreCase("receipt")){
                    beelineResponse.receipt = element.getTextTrim();
                    continue;
                }

                // time_stamp
                if(element.getName().equalsIgnoreCase("time_stamp")){
                    beelineResponse.timeStamp = new SimpleDateFormat(TIMESTAMP_FORMAT).parse(element.getTextTrim());
                    continue;
                }

                // pay_status
                if(element.getName().equalsIgnoreCase("pay_status")){
                    beelineResponse.payStatus = new Long(element.getTextTrim());
                }

            }
        }catch(ParseException e){
            throw new BadResponseException(e.getMessage(), e);
        }catch(NumberFormatException e){
            throw new BadResponseException(e.getMessage(), e);
        }catch(DocumentException e){
            throw new BadResponseException("Parse beeline response error " + e.getMessage(), e);
        }
        return beelineResponse;
    }

    public Long getStatusCode() {
        return statusCode;
    }

    public Long getPayId() {
        return payId;
    }

    public String getReceipt() {
        return receipt;
    }

    public Date getTimeStamp() {
        return timeStamp;
    }

    public Long getPayStatus() {
        return payStatus;
    }
}
