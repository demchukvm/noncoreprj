package com.tmx.beng.csapi.java.base;

/**

 */
public class DefaultClientResp {
    private String statusCode;
    private Long waitTime;
    private String statusMessage;

    public String getStatusCode() {
        return statusCode;
    }

    void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public Long getWaitTime() {
        return waitTime;
    }

    void setWaitTime(Long waitTime) {
        this.waitTime = waitTime;
    }

    public String getStatusMessage() {
        return statusMessage;
    }

    void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }
}
