package com.tmx.gate.mail;

import java.io.*;
import java.net.Socket;

public class SMTPSession extends Object {

    public String host;     // Host name we connect to
    public int port;     // port number we connect to, default=25

    public String recipient;
    public String sender;
    public String[] message;
    public String msgCoding;

    protected Socket sessionSock;

    protected BufferedReader inStream;
    protected BufferedWriter outStream;
    private boolean debugAllowed;

    public SMTPSession() {
    }

    public SMTPSession(String host, String recipient,
                       String sender, String[] message) {
        this(host, 25, recipient, sender, message);
    }

    public SMTPSession(String host, int port, String recipient,
                       String sender, String[] message) {
        this(host, port, recipient, sender, message, null);
    }

    public SMTPSession(String host, int port, String recipient,
                       String sender, String[] message, String msgCoding) {
        this.host = host;
        this.port = port;
        if (this.port <= 0) this.port = 25;

        this.recipient = recipient;
        this.message = message;
        this.sender = sender;
        this.msgCoding = msgCoding;

        String debug = System.getProperty("smtp_debug");
        if (debug != null && (debug.equalsIgnoreCase("y") || debug.equalsIgnoreCase("true"))) {
            debugAllowed = true;
        } else {
            debugAllowed = false;
        }
    }

// Close down the session

    public void close()
            throws IOException {
        sessionSock.close();
        sessionSock = null;
    }

// Connect to the server

    protected void connect()
            throws IOException {
        if (debugAllowed) {
            debug( "SMTPSession: connect, host=" + host + ", port=" + port);
        }
        sessionSock = new Socket(host, port);
        inStream = new BufferedReader(new InputStreamReader(sessionSock.getInputStream()));
        outStream = new BufferedWriter(new OutputStreamWriter(sessionSock.getOutputStream()));
    }

    public static final void debug(String msg) {
        System.out.println(msg);
    }

// Send a command and wait for a response

    protected String doCommand(String commandString)
            throws IOException {
        if (debugAllowed) {
            debug( commandString);
        }
        outStream.write(commandString);
        outStream.newLine();
        outStream.flush();
        String response = getResponse();
        if (debugAllowed) {
            debug( response);
        }
        return response;
    }

// Get a response back from the server. Handles multi-line responses
// and returns them as part of the string.

    protected String getResponse()
            throws IOException {
        String response = "";

        for (; ;) {
            String line = inStream.readLine();

            if (line == null) {
                throw new IOException("Bad response from server.");
            }

// FTP response lines should at the very least have a 3-digit number

            if (line.length() < 3) {
                throw new IOException("Bad response from server.");
            }
            response += line + "\n";

// If there isn't a '-' immediately after the number, we've gotten the
// complete response. ('-' is the continuation character for FTP responses)

            if ((line.length() == 3) ||
                    (line.charAt(3) != '-'))
                return response;
        }
    }


// Sends a message using the SMTP protocol

    public void sendMessage()
            throws IOException {
        connect();

// After connecting, the SMTP server will send a response string. Make
// sure it starts with a '2' (reponses in the 200's are positive
// responses.

        String response = getResponse();
        if (response.charAt(0) != '2') {
            throw new IOException(response);
        }

// Introduce ourselves to the SMTP server with a polite "HELO"
        response = doCommand("HELO " + host);

        if (response.charAt(0) != '2') {
            throw new IOException(response);
        }

// Tell the server who this message is from

        response = doCommand("MAIL FROM: " + sender);

        if (response.charAt(0) != '2') {
            throw new IOException(response);
        }


// Now tell the server who we want to send a message to

        response = doCommand("RCPT TO: " + recipient);

        if (response.charAt(0) != '2') {
            throw new IOException(response);
        }

// Okay, now send the mail message

        response = doCommand("DATA");

// We expect a response beginning with '3' indicating that the server
// is ready for data.

        if (response.charAt(0) != '3') {
            throw new IOException(response);
        }

// Send each line of the message

        for (int i = 0; i < message.length; i++) {

// Check for a blank line
            if (message[i].length() == 0) {
                outStream.newLine();
                outStream.flush();
                continue;
            }

// If the line begins with a ".", put an extra "." in front of it.

            byte[] ba = null;
            String b;
            if (msgCoding == null) {
                ba = ((message[i].charAt(0) == '.' ? "." : "") + message[i]).getBytes();
                b = new String(ba);
            } else {
                ba = ((message[i].charAt(0) == '.' ? "." : "") + message[i]).getBytes(msgCoding);
                b = new String(ba, msgCoding);
            }
            outStream.write(b);
            outStream.newLine();
            outStream.flush();
        }

// A "." on a line by itself ends a message.

        response = doCommand(".");

        if (response.charAt(0) != '2') {
            throw new IOException(response);
        }

        close();
    }

    public static void main(String[] args) throws IOException {
        SMTPSession s = new SMTPSession("82.193.104.90", 25, "markin@tmx.com.ua", "markin@tmx.com.ua", new String[]{"Subject: test", "BillingMessage text"}, "KOI8");
        s.sendMessage();
    }
}