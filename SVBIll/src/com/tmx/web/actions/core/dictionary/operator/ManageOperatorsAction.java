
package com.tmx.web.actions.core.dictionary.operator;

import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForm;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.tmx.web.forms.core.dictionary.seller.ManageSellersForm;
import com.tmx.web.forms.core.dictionary.operator.ManageOperatorsForm;
import com.tmx.web.forms.core.base.AbstractFormFactory;
import com.tmx.web.actions.core.AbstractSvbillAction;

/**
 * Created by IntelliJ IDEA.
 * User: �������������
 * Date: 18.01.2008
 * Time: 15:29:56
 * To change this template use File | Settings | File Templates.
 */
public class ManageOperatorsAction extends AbstractSvbillAction {
     private static final String FORWARD_MANAGE_OPERATORS_PAGE = "manageOperatorsPage";

    public ActionForward defaultAction(ActionMapping mapping,
                                       ActionForm form,
                                       HttpServletRequest request,
                                       HttpServletResponse response,
                                       String methodName) throws Throwable {
        ManageOperatorsForm manageOperatorsForm = (ManageOperatorsForm) form;
//       AbstractFormFactory.getInstance( getLoggedInUser(request) ).setUpForm(manageSellerForm);
        manageOperatorsForm.getOperatorsTable().refresh();            
        manageOperatorsForm.bindData();
        return mapping.findForward(FORWARD_MANAGE_OPERATORS_PAGE);
    }
}
