package com.tmx.beng.medaccess.avancel;

import com.tmx.beng.medaccess.MediumAccess;
import com.tmx.beng.medaccess.MediumConnection;
import com.tmx.beng.medaccess.GateException;

/**
 * Medium access to obtain Avancel connection
 */
public class AvancelGateMediumAccess1 implements MediumAccess {

    public void init() {
    }

    public MediumConnection getConnection() throws GateException {
        return new AvancelGateMediumConnection1();
    }
}
