package com.tmx.web.taglib.html;

import org.apache.struts.taglib.TagUtils;
import org.apache.struts.taglib.nested.NestedPropertyHelper;
import javax.servlet.jsp.JspException;
import javax.servlet.http.HttpServletRequest;

/** Writes requested table's query. Use for debug purposes. */
public class TableWriteQueryTag extends com.tmx.web.taglib.base.TableBasicTag {

    //------------------------------Parsing events
    public int doStartTmxTag() throws JspException {
        String tablePagingHTML = renderTableQueryHTML();
        TagUtils.getInstance().write(pageContext, tablePagingHTML);
        return (EVAL_BODY_TAG);
    }

    //------------------------------Rendering methods

    private String renderTableQueryHTML() throws JspException{
        StringBuffer resultHTML = new StringBuffer();
        resultHTML.append("Table '");
        resultHTML.append(getTableName());
        resultHTML.append("' query: [");
        resultHTML.append(getTableQuery());
        resultHTML.append("]");
        return resultHTML.toString();
    }


    //------------------------------Bean interaction methods
    private String getTableQuery() throws JspException {
        String queryString = null;

        HttpServletRequest request = (HttpServletRequest)pageContext.getRequest();
        String originalName = (getName() != null) ? getName() : NestedPropertyHelper.getCurrentName(request, this);
        //Read pageNumber from Query from Table from Bean form
        String originalProperty = getTableName() + ".query.stringPresentation";
        String nestedProperty = NestedPropertyHelper.getAdjustedProperty(request, originalProperty);

        Object value = TagUtils.getInstance().lookup(pageContext, originalName, nestedProperty, null);
        if(value != null && (value instanceof String))
            queryString = (String)value;
        return queryString;
    }


}
