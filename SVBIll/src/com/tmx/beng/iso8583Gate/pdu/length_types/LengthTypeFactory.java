package com.tmx.beng.iso8583Gate.pdu.length_types;

import java.util.Map;
import java.util.HashMap;

/**
 */
public class LengthTypeFactory {
    private static Map instances = new HashMap();
    public static LengthType FIXED = new FixedLengthType();
    public static LengthType LVAR = new LVARLengthType();
    public static LengthType LLVAR = new LLVARLengthType();
    public static LengthType LLLVAR = new LLLVARLengthType();

    static{
        instances.put(FIXED.getName(), FIXED);
        instances.put(LVAR.getName(), LVAR);
        instances.put(LLVAR.getName(), LLVAR);
        instances.put(LLLVAR.getName(), LLLVAR);
    }


    public static LengthType getInstance(String name){
        return (LengthType)instances.get(name);
    }
}
