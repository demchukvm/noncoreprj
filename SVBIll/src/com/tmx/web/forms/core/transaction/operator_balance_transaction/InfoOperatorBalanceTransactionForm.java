package com.tmx.web.forms.core.transaction.operator_balance_transaction;

import com.tmx.web.forms.core.transaction.AbstractInfoTransactionForm;
import com.tmx.web.controls.EditBox;
import com.tmx.web.controls.TextArea;
import com.tmx.as.entities.bill.transaction.OperatorBalanceTransaction;
import com.tmx.as.entities.bill.operator.Operator;
import com.tmx.as.exceptions.DatabaseException;

public class InfoOperatorBalanceTransactionForm extends AbstractInfoTransactionForm {

    private EditBox editBoxOperatorName;
    private EditBox editBoxOperatorBalanceName;
    private TextArea textAreaOperatorBalancePaymentReason;
    private TextArea textAreaOperatorBalanceDescription;

    protected void init() {
        //initialze and register controls
        super.init();

        editBoxOperatorName = new TextArea();
        editBoxOperatorName.setReadonly(true);

        editBoxOperatorBalanceName = new TextArea();
        editBoxOperatorBalanceName.setReadonly(true);

        textAreaOperatorBalancePaymentReason = new TextArea();
        textAreaOperatorBalancePaymentReason.setReadonly(true);

        textAreaOperatorBalanceDescription = new TextArea();
        textAreaOperatorBalanceDescription.setReadonly(true);

//        onCancelButton.setAction("/core/transactions/operator_transaction/manageOperatorTransactions");
        onCancelButton.setAction("/core/transactions/operator_transaction/infoOperatorTransaction");
        onCancelButton.setCommand("preloadForCancel");
    }


    public void bindData(OperatorBalanceTransaction operatorBalanceTransaction) throws DatabaseException {
        super.bindData(operatorBalanceTransaction);

        //for operator
        Operator operator = operatorBalanceTransaction.getOperator();
        if(operator != null){
            editBoxOperatorName.setValue(operator.getName());
            if(operator.getPrimaryBalance() != null) {
                editBoxOperatorBalanceName.setValue(operator.getPrimaryBalance().getName());
            }
//            TransactionSupport ts = new TransactionManager().getOperatorBalanceTransaction((Long) operatorBalanceTransaction.getId())
//                                                                                    .getTransactionSupport();
            if (operatorBalanceTransaction.getTransactionSupport() != null) { //it`s possible that "TS_TransactionSupportId" == null in DB
                textAreaOperatorBalancePaymentReason.setValue(operatorBalanceTransaction.getTransactionSupport().getPaymentReason());
                textAreaOperatorBalanceDescription.setValue(operatorBalanceTransaction.getTransactionSupport().getDescription());
                editBoxExecutedUser.setValue(operatorBalanceTransaction.getTransactionSupport().getUserActor().getLogin());
            }
        }
        
    }

    public EditBox getEditBoxOperatorName() {
        return editBoxOperatorName;
    }

    public void setEditBoxOperatorName(EditBox editBoxOperatorName) {
        this.editBoxOperatorName = editBoxOperatorName;
    }

    public EditBox getEditBoxOperatorBalanceName() {
        return editBoxOperatorBalanceName;
    }

    public void setEditBoxOperatorBalanceName(EditBox editBoxOperatorBalanceName) {
        this.editBoxOperatorBalanceName = editBoxOperatorBalanceName;
    }

    public TextArea getTextAreaOperatorBalancePaymentReason() {
        return textAreaOperatorBalancePaymentReason;
    }

    public void setTextAreaOperatorBalancePaymentReason(TextArea textAreaOperatorBalancePaymentReason) {
        this.textAreaOperatorBalancePaymentReason = textAreaOperatorBalancePaymentReason;
    }

    public TextArea getTextAreaOperatorBalanceDescription() {
        return textAreaOperatorBalanceDescription;
    }

    public void setTextAreaOperatorBalanceDescription(TextArea textAreaOperatorBalanceDescription) {
        this.textAreaOperatorBalanceDescription = textAreaOperatorBalanceDescription;
    }
}
