package com.tmx.as.blogic;

import com.tmx.as.base.EntityManager;
import com.tmx.as.base.OrderWrapper;
import com.tmx.as.base.criterion.CriterionWrapper;
import com.tmx.as.base.criterion.Restrictions;
import com.tmx.as.entities.bill.exclusive.*;
import com.tmx.as.entities.bill.terminal.Terminal;
import com.tmx.as.exceptions.DatabaseException;
import com.tmx.as.entities.general.user.User;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ExclusiveManager extends EntityManager
{
    public List<Exclusives> getAllExclusives() throws DatabaseException
    {
        return RETRIEVE_ALL(Exclusives.class, null, new OrderWrapper[]{new OrderWrapper("exclusive", OrderWrapper.ASC)});
    }

    public List getAllFromExclusiveTable(String tableName) throws DatabaseException
    {
        OrderWrapper[] orderWrapper = new OrderWrapper[]{new OrderWrapper("terminal", OrderWrapper.ASC)};

        if(tableName.equals("ExclusiveBeeline"))
            return RETRIEVE_ALL(ExclusiveBeeline.class, null, orderWrapper, new String[]{"addUser"},-1,-1);
        else if(tableName.equals("ExclusiveKyivstar"))
            return RETRIEVE_ALL(ExclusiveKyivstar.class, null, orderWrapper, new String[]{"addUser"},-1,-1);
        else if(tableName.equals("ExclusiveLife"))
            return RETRIEVE_ALL(ExclusiveLife.class, null, orderWrapper, new String[]{"addUser"},-1,-1);
        else if(tableName.equals("ExclusiveMTS"))
            return RETRIEVE_ALL(ExclusiveMTS.class, null, orderWrapper, new String[]{"addUser"},-1,-1);
        else
            return null;

    }

    public ArrayList getAllTerminalsFromExclusiveTable(String tableName) throws DatabaseException
    {
        ArrayList terminals = new ArrayList();
        List table = null;

        OrderWrapper[] orderWrapper = new OrderWrapper[]{new OrderWrapper("terminal", OrderWrapper.ASC)};

        if(tableName.equals("ExclusiveBeeline"))
            table = RETRIEVE_ALL(ExclusiveBeeline.class, null, orderWrapper, new String[]{"addUser"},-1,-1);
        else if(tableName.equals("ExclusiveKyivstar"))
            table = RETRIEVE_ALL(ExclusiveKyivstar.class, null, orderWrapper, new String[]{"addUser"},-1,-1);
        else if(tableName.equals("ExclusiveLife"))
            table = RETRIEVE_ALL(ExclusiveLife.class, null, orderWrapper, new String[]{"addUser"},-1,-1);
        else if(tableName.equals("ExclusiveMTS"))
            table = RETRIEVE_ALL(ExclusiveMTS.class, null, orderWrapper, new String[]{"addUser"},-1,-1);

        for(Object t : table)
        {
            String terminal = "";

            if(tableName.equals("ExclusiveBeeline"))
            {
                ExclusiveBeeline tt = (ExclusiveBeeline) t;
                terminal = tt.getTerminal();
            }
            else if(tableName.equals("ExclusiveKyivstar"))
            {
                ExclusiveKyivstar tt = (ExclusiveKyivstar) t;
                terminal = tt.getTerminal();
            }
            else if(tableName.equals("ExclusiveLife"))
            {
                ExclusiveLife tt = (ExclusiveLife) t;
                terminal = tt.getTerminal();
            }
            else if(tableName.equals("ExclusiveMTS"))
            {
                ExclusiveMTS tt = (ExclusiveMTS) t;
                terminal = tt.getTerminal();
            }

            terminals.add(terminal);
        }

        return terminals;
    }

    public boolean addTerminalToTable(String tableName, String terminal, User user) throws DatabaseException
    {
        if(!isDuplicate(tableName, terminal))
        {
            Date date = new Date();

            if(tableName.equals("ExclusiveBeeline"))
            {
                ExclusiveBeeline table = new ExclusiveBeeline();
                table.setTerminal(terminal);
                table.setAddUser(user);
                table.setAddDate(date);
                SAVE(table);
            }
            else if(tableName.equals("ExclusiveKyivstar"))
            {
                ExclusiveKyivstar table = new ExclusiveKyivstar();
                table.setTerminal(terminal);
                table.setAddUser(user);
                table.setAddDate(date);
                SAVE(table);
            }
            else if(tableName.equals("ExclusiveLife"))
            {
                ExclusiveLife table = new ExclusiveLife();
                table.setTerminal(terminal);
                table.setAddUser(user);
                table.setAddDate(date);
                SAVE(table);
            }
            else if(tableName.equals("ExclusiveMTS"))
            {
                ExclusiveMTS table = new ExclusiveMTS();
                table.setTerminal(terminal);
                table.setAddUser(user);
                table.setAddDate(date);
                SAVE(table);
            }

            return true;
        }

        return false;
    }

    private boolean isDuplicate(String tableName, String terminal) throws DatabaseException
    {
        List allTerminals = getAllFromExclusiveTable(tableName);
        ArrayList<String> term = new ArrayList<String>();

        for (Object t : allTerminals)
        {
            if(tableName.equals("ExclusiveBeeline"))
            {
                ExclusiveBeeline tt = (ExclusiveBeeline) t;
                term.add(tt.getTerminal());
            }
            else if(tableName.equals("ExclusiveKyivstar"))
            {
                ExclusiveKyivstar tt = (ExclusiveKyivstar) t;
                term.add(tt.getTerminal());
            }
            else if(tableName.equals("ExclusiveLife"))
            {
                ExclusiveLife tt = (ExclusiveLife) t;
                term.add(tt.getTerminal());
            }
            else if(tableName.equals("ExclusiveMTS"))
            {
                ExclusiveMTS tt = (ExclusiveMTS) t;
                term.add(tt.getTerminal());
            }
        }

        if(term.contains(terminal))
            return true;
        else
            return false;
    }

    public void removeTerminalFromTable(String tableName, Long terminalId) throws DatabaseException
    {
        if(tableName.equals("ExclusiveBeeline"))
        {
            DELETE_STRICTLY(ExclusiveBeeline.class, terminalId);
        }
        else if(tableName.equals("ExclusiveKyivstar"))
        {
            DELETE_STRICTLY(ExclusiveKyivstar.class, terminalId);
        }
        else if(tableName.equals("ExclusiveLife"))
        {
            DELETE_STRICTLY(ExclusiveLife.class, terminalId);
        }
        else if(tableName.equals("ExclusiveMTS"))
        {
            DELETE_STRICTLY(ExclusiveMTS.class, terminalId);
        }
    }

    public boolean isBeelineExclusive(String terminal) throws DatabaseException
    {
        CriterionWrapper cw = Restrictions.eq("terminal", terminal);
        ExclusiveBeeline beeline = (ExclusiveBeeline) RETRIEVE(ExclusiveBeeline.class, null, new CriterionWrapper[]{cw}, null);
        if(beeline != null)
            return true;
        else
            return false;
    }

    public boolean isKyivstarExclusive(String terminal) throws DatabaseException
    {
        CriterionWrapper cw = Restrictions.eq("terminal", terminal);
        ExclusiveKyivstar kyivstar = (ExclusiveKyivstar) RETRIEVE(ExclusiveKyivstar.class, null, new CriterionWrapper[]{cw}, null);
        if(kyivstar != null)
            return true;
        else
            return false;
    }

    public boolean isLifeExclusive(String terminal) throws DatabaseException
    {
        CriterionWrapper cw = Restrictions.eq("terminal", terminal);
        ExclusiveLife life = (ExclusiveLife) RETRIEVE(ExclusiveLife.class, null, new CriterionWrapper[]{cw}, null);
        if(life != null)
            return true;
        else
            return false;
    }

    public boolean isMTSExclusive(String terminal) throws DatabaseException
    {
        CriterionWrapper cw = Restrictions.eq("terminal", terminal);
        ExclusiveMTS mts = (ExclusiveMTS) RETRIEVE(ExclusiveMTS.class, null, new CriterionWrapper[]{cw}, null);
        if(mts != null)
            return true;
        else
            return false;
    }
}
