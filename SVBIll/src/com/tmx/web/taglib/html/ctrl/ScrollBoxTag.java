package com.tmx.web.taglib.html.ctrl;

import com.tmx.web.controls.ScrollBox;
import javax.servlet.jsp.JspException;

import org.apache.struts.taglib.TagUtils;

/**
 * Scroll box tag
 */
public class ScrollBoxTag extends AbstractControlTag {
    //CSS styles properties
    private String editBoxStyleClass = null;
    private String outerWrapperStyleClass = null;
    private String cellStyleClass = null;
    private String hoveredCellStyleClass = null;
    private String scrollStyleClass = null;
    private String scrollLineupStyleClass = null;
    private String scrollLinedownStyleClass = null;
    private String scrollThumbStyleClass = null;    
    //-----------------------------Constants
    //html name suffix of scrolbox Edit field
    private static final String SCROLLBOX_EDIT_SUFFIX = "edit";
    //JS
    private static final String NO_BUBBLE = "no_bubble()";
    //Name building constants
    private static final String PSEUDO_WINDOW = "pseudoWindow";
    private static final String INNER_WRAPPER = "innerWrapper";
    private static final String OUTER_WRAPPER = "outerWrapper";
    private static final String SCROLL_WRAPPER = "scrollWrapper";
    private static final String LINEUP = "lineup";
    private static final String LINEDOWN = "linedown";
    private static final String THUMB = "thumb";

    //-----------------Properties

    public String getEditBoxStyleClass() {
        return (editBoxStyleClass != null) ? editBoxStyleClass : getDefaultProperty("tag.scrollBox.style.editBoxStyleClass");
    }

    public void setEditBoxStyleClass(String editBoxStyleClass) {
        this.editBoxStyleClass = editBoxStyleClass;
    }

    public String getOuterWrapperStyleClass() {
        return (outerWrapperStyleClass != null) ? outerWrapperStyleClass : getDefaultProperty("tag.scrollBox.style.outerWrapperStyleClass");
    }

    public void setOuterWrapperStyleClass(String outerWrapperStyleClass) {
        this.outerWrapperStyleClass = outerWrapperStyleClass;
    }

    public String getCellStyleClass() {
        return (cellStyleClass != null) ? cellStyleClass : getDefaultProperty("tag.scrollBox.style.cellStyleClass");
    }

    public void setCellStyleClass(String cellStyleClass) {
        this.cellStyleClass = cellStyleClass;
    }

    public String getHoveredCellStyleClass() {
        return (hoveredCellStyleClass != null) ? hoveredCellStyleClass : getDefaultProperty("tag.scrollBox.style.hoveredCellStyleClass");
    }

    public void setHoveredCellStyleClass(String hoveredCellStyleClass) {
        this.hoveredCellStyleClass = hoveredCellStyleClass;
    }

    public String getScrollStyleClass() {
        return (scrollStyleClass != null) ? scrollStyleClass : getDefaultProperty("tag.scrollBox.style.scrollStyleClass");
    }

    public void setScrollStyleClass(String scrollStyleClass) {
        this.scrollStyleClass = scrollStyleClass;
    }

    public String getScrollLineupStyleClass() {
        return (scrollLineupStyleClass != null) ? scrollLineupStyleClass : getDefaultProperty("tag.scrollBox.style.scrollLineupStyleClass");
    }

    public void setScrollLineupStyleClass(String scrollLineupStyleClass) {
        this.scrollLineupStyleClass = scrollLineupStyleClass;
    }

    public String getScrollLinedownStyleClass() {
        return (scrollLinedownStyleClass != null) ? scrollLinedownStyleClass : getDefaultProperty("tag.scrollBox.style.scrollLinedownStyleClass");
    }

    public void setScrollLinedownStyleClass(String scrollLinedownStyleClass) {
        this.scrollLinedownStyleClass = scrollLinedownStyleClass;
    }

    public String getScrollThumbStyleClass() {
        return (scrollThumbStyleClass != null) ? scrollThumbStyleClass : getDefaultProperty("tag.scrollBox.style.scrollThumbStyleClass");
    }

    public void setScrollThumbStyleClass(String scrollThumbStyleClass) {
        this.scrollThumbStyleClass = scrollThumbStyleClass;
    }

    //-----------------Abstract methods
    protected int controlStartTag() throws JspException {
        StringBuffer resultHTML = new StringBuffer();
        ScrollBox sb = getScrollBox();
        if(!sb.isBounded()){
            TagUtils.getInstance().write(pageContext,"Data is not bound to ScrollBox '"+getProperty()+"'");
            return SKIP_BODY;
        }

        //Render HTML
        resultHTML.append("\r");
        resultHTML.append("<table border=\"0\" cellpadding=\"2\" cellspacing=\"2\" width=\"100%\">\r");
        resultHTML.append("    <tr>\r");
        resultHTML.append("        <td>\r");
        resultHTML.append("            <input type=\"text\" name=\""+getProperty()+"."+SCROLLBOX_EDIT_SUFFIX+"\" value=\""+sb.getSelectedEntry().getValue()+"\" onclick=\"popup_or_hide('"+getProperty()+"."+PSEUDO_WINDOW+"', '"+getProperty()+"."+OUTER_WRAPPER+"')\" readonly=\"readonly\" class=\""+getEditBoxStyleClass()+"\"/>\r");
        resultHTML.append("            <input type=\"hidden\" name=\""+getProperty()+"."+ScrollBox.SELECTEDKEY_SUFFIX+"\" value=\""+sb.getSelectedEntry().getKey()+"\"/>\r");
        resultHTML.append("        </td>\r");
        resultHTML.append("    </tr>\r");
        resultHTML.append("</table>\r");
        resultHTML.append("\r");

        resultHTML.append("<div id=\""+getProperty()+"."+PSEUDO_WINDOW+"\"  style=\"position:absolute; visibility: hidden;\" onclick=\""+NO_BUBBLE +";\">\r");
        resultHTML.append("    <div id=\""+getProperty()+"."+OUTER_WRAPPER+"\" class=\""+ getOuterWrapperStyleClass() +"\">\r");
        resultHTML.append("        <div id=\""+getProperty()+"."+INNER_WRAPPER+"\" style=\"position:absolute; top:0px; left:0px; padding:5px; \">\r");
        resultHTML.append("            <table>\r");
        sb.resetForRendering();
        while(sb.hasNext()){
            ScrollBox.Entry entry = sb.next();
            resultHTML.append("                <tr>\r");
            resultHTML.append("                    <td class=\""+getCellStyleClass()+"\" onclick='"+getSeparatorReplacedName(getProperty())+"_selectEntry(this.innerHTML, "+entry.getKey()+");' onmouseover=\"this.className='"+getHoveredCellStyleClass()+"'\" onmouseout=\"this.className='"+getCellStyleClass()+"'\">\r");
            resultHTML.append("                    "+entry.getValue()+"\r");
            resultHTML.append("                    </td>\r");
            resultHTML.append("                </tr>\r");

        }
        resultHTML.append("            </table>\r");
        resultHTML.append("        </div>\r");
        resultHTML.append("     </div>\r");
        resultHTML.append("     <div id=\""+getProperty()+"."+SCROLL_WRAPPER+"\" class=\""+getScrollStyleClass()+"\">\r");
        resultHTML.append("         <img alt=\"\" src=\"#\" id=\""+getProperty()+"."+LINEUP+"\" name=\""+getProperty()+"."+LINEUP+"\" class=\""+getScrollLineupStyleClass()+"\">\r");
        resultHTML.append("         <img alt=\"\" src=\"#\" id=\""+getProperty()+"."+LINEDOWN+"\" name=\""+getProperty()+"."+LINEDOWN+"\" class=\""+getScrollLinedownStyleClass()+"\">\r");
        resultHTML.append("         <img alt=\"\" src=\"#\" id=\""+getProperty()+"."+THUMB+"\" name=\""+getProperty()+"."+THUMB+"\" class=\""+getScrollThumbStyleClass()+"\">\r");
        resultHTML.append("     </div>\r");
        resultHTML.append("</div>\r");
        resultHTML.append("\r");

        //Scripts
        resultHTML.append("    <script type=\"text/javascript\">\r");
        resultHTML.append("\r");
        resultHTML.append("        function "+getSeparatorReplacedName(getProperty())+"_init(){\r");
        int scrollBarsIdx = getTagRenderingHelper().allocateScriptArrayElement("scrollBars");
        resultHTML.append("            scrollBars["+scrollBarsIdx+"] = new scrollBar(\""+getProperty()+"."+PSEUDO_WINDOW+"\", \""+getProperty()+"."+OUTER_WRAPPER+"\", \""+getProperty()+"."+INNER_WRAPPER+"\");\r");
        resultHTML.append("            scrollBars["+scrollBarsIdx+"].appendScroll(\""+getProperty()+"."+SCROLL_WRAPPER+"\",\""+getProperty()+"."+LINEUP+"\",\""+getProperty()+"."+LINEDOWN+"\",\""+getProperty()+"."+THUMB+"\");\r");
        resultHTML.append("            initDrag();\r");
        resultHTML.append("        }\r");
        resultHTML.append("\r");
        resultHTML.append("        function "+getSeparatorReplacedName(getProperty())+"_selectEntry(text, id){\r");
        resultHTML.append("            edit = getRawObject('"+getProperty()+"."+SCROLLBOX_EDIT_SUFFIX+"');\r");
        resultHTML.append("            edit.value = text;\r");
        resultHTML.append("            hidden = getRawObject('"+getProperty()+"."+ScrollBox.SELECTEDKEY_SUFFIX+"');\r");
        resultHTML.append("            hidden.value = id;\r");
        resultHTML.append("            hide_div('"+getProperty()+"."+PSEUDO_WINDOW+"', '"+getProperty()+"."+OUTER_WRAPPER+"');\r");
        resultHTML.append("        }\r");
        resultHTML.append("\r");
        resultHTML.append("        registerSingleFunction('window.onload', function() {initDHTMLAPI()});\n");
        resultHTML.append("        registerFunction('window.onload', function() {"+getSeparatorReplacedName(getProperty())+"_init()});\n");
        resultHTML.append("        registerFunction('window.onload', function() {init_div(0,20,'"+getProperty()+"."+SCROLLBOX_EDIT_SUFFIX+"','"+getProperty()+"."+PSEUDO_WINDOW+"')});\r");
        resultHTML.append("        registerFunction('document.onclick', function() {hide_div('"+getProperty()+"."+PSEUDO_WINDOW+"', '"+getProperty()+"."+OUTER_WRAPPER+"')});\r");
        resultHTML.append("\r");
        resultHTML.append("    </script>\r");
        resultHTML.append("\r");

        TagUtils.getInstance().write(pageContext,resultHTML.toString());
        return SKIP_BODY;
    }

   private ScrollBox getScrollBox() throws JspException{
        Object object = getPropertyObject();
        return (object != null && object instanceof ScrollBox) ?
               (ScrollBox)object : null;
//               new ScrollBox(){
//                   protected String getKey(Object dataListElement) {
//                       return "SCROLL_BOX_NOT_FOUND";
//                   }
//
//                   protected String getValue(Object dataListElement) {
//                       return "SCROLL_BOX_NOT_FOUND";
//                   }
//               };
    }
}
