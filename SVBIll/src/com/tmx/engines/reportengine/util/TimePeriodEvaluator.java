package com.tmx.engines.reportengine.util;

import com.tmx.util.i18n.MessageResources;

import java.util.*;
import java.text.SimpleDateFormat;
import java.text.ParseException;
import java.math.BigDecimal;

//TODO RM: should be removed. Use TimePeriodSelector instead  
public class TimePeriodEvaluator {
    public static final String FORMAT = "yyyy-MM-dd HH:mm";

    public static List getTimePeriodList(Locale locale, TimePeriod manual){

        List timePeriodList = new Vector();

        GregorianCalendar cal = new GregorianCalendar(locale);
        cal.setTimeInMillis(System.currentTimeMillis());

        int year = cal.get( Calendar.YEAR );
        int month = cal.get( Calendar.MONTH ) + 1;
        int day = cal.get( Calendar.DAY_OF_MONTH );
        int hour24 = cal.get( Calendar.HOUR_OF_DAY );
        int hour12 = cal.get( Calendar.HOUR );
        int amIs0OrPmIs1 = cal.get( Calendar.AM_PM );
        int minute = cal.get( Calendar.MINUTE );
        int second = cal.get( Calendar.SECOND );
        int week = cal.get( Calendar.WEEK_OF_MONTH );
        int dayOfWeek = cal.get( Calendar.DAY_OF_WEEK );
        if (dayOfWeek ==1) {
            dayOfWeek =6;
        } else {
            dayOfWeek =dayOfWeek-2;
        }

        GregorianCalendar from = new GregorianCalendar(locale);
        GregorianCalendar to = new GregorianCalendar(locale);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(FORMAT);

        //TODAY
        from.set(year, month-1, day, 0, 0);
        to.set(year, month-1, day, hour24, minute);
        timePeriodList.add(new TimePeriod(TimePeriod.Type.TODAY, from.getTime(), to.getTime(), getLocalizedLabel("label.today", locale)));

        //YESTERDAY
        from.set(year, month-1, day-1, 0, 0);
        to.set(year, month-1, day-1, 23, 59);
        timePeriodList.add(new TimePeriod(TimePeriod.Type.YESTERDAY, from.getTime(), to.getTime(), getLocalizedLabel("label.yesterday", locale)));

        //THIS WEEK
        from.set(year, month-1, day-dayOfWeek, 0, 0);
        to.set(year, month-1, day, hour24, minute);
        timePeriodList.add(new TimePeriod(TimePeriod.Type.THIS_WEEK, from.getTime(), to.getTime(), getLocalizedLabel("label.this_week", locale)));

        //PREV WEEK
        from.set(year, month-1, day-dayOfWeek-7, 0, 0);
        to.set(year, month-1, day-dayOfWeek-1, 23, 59);
        timePeriodList.add(new TimePeriod(TimePeriod.Type.PREV_WEEK, from.getTime(), to.getTime(), getLocalizedLabel("label.prev_week", locale)));

        //THIS MONTH
        from.set(year, month-1, 1, 0, 0);
        to.set(year, month-1, day, hour24, minute);
        timePeriodList.add(new TimePeriod(TimePeriod.Type.THIS_MONTH, from.getTime(), to.getTime(), getLocalizedLabel("label.this_month", locale)));

        //PREV MOUTH
        from.set(year, month-2, 1, 0, 0);
        to.set(year, month-2, 1, 0, 0);
        int maxDayInMmonth = to.getActualMaximum(Calendar.DAY_OF_MONTH);
        to.set(year, month-2, maxDayInMmonth,  23, 59);
        timePeriodList.add(new TimePeriod(TimePeriod.Type.PREV_MONTH, from.getTime(), to.getTime(), getLocalizedLabel("label.prev_month", locale)));

        //THIS QUARTAL
        BigDecimal d = new BigDecimal(month);
        int quartal = d.divide(new BigDecimal(3d), BigDecimal.ROUND_UP).intValue();
        from.set(year, (quartal*3)-3, 1, 0, 0);
        d = new BigDecimal(month);
        quartal = d.divide(new BigDecimal(3d), BigDecimal.ROUND_UP).intValue();
        to.set(year, month-1, day, hour24, minute);
        timePeriodList.add(new TimePeriod(TimePeriod.Type.THIS_QUARTAL, from.getTime(), to.getTime(), getLocalizedLabel("label.this_quartal", locale)));

        //PREV QUARTAL
        from.set(year, (quartal*3)-6, 1, 0, 0);
        to.set(year, (quartal*3)-4, 1, 0, 0);
        maxDayInMmonth = to.getActualMaximum(Calendar.DAY_OF_MONTH);
        to.set(year, (quartal*3)-4, maxDayInMmonth, 23, 59);
        timePeriodList.add(new TimePeriod(TimePeriod.Type.PREV_QUARTAL, from.getTime(), to.getTime(), getLocalizedLabel("label.prev_quartal", locale)));

        //THIS YEAR
        from.set(year, 0, 1, 0, 0);
        to.set(year, month-1, day, hour24, minute);
        timePeriodList.add(new TimePeriod(TimePeriod.Type.THIS_YEAR, from.getTime(), to.getTime(), getLocalizedLabel("label.this_year", locale)));

        //PREV YEAR
        from.set(year-1, 0, 1, 0, 0);
        to.set(year-1, 11, 1, 0, 0);
        maxDayInMmonth = to.getActualMaximum(Calendar.DAY_OF_MONTH);
        to.set(year-1, 11, maxDayInMmonth, 23, 59);
        timePeriodList.add(new TimePeriod(TimePeriod.Type.PREV_YEAR, from.getTime(), to.getTime(), getLocalizedLabel("label.prev_year", locale)));

        //CUSTOM
        Date date_ = null;
        if(manual == null){
            manual = new TimePeriod(TimePeriod.Type.CUSTOM, new Date(), new Date(), getLocalizedLabel("label.custom", locale));
        }
        else{
            manual.setLabel(getLocalizedLabel("label.custom", locale));
        }
        timePeriodList.add(manual);

        return timePeriodList;
    }

    public static List getTimePeriodList(Locale locale, String from, String to){
        TimePeriod timePeriod = new TimePeriod(TimePeriod.Type.CUSTOM, null, null, getLocalizedLabel("label.custom", locale));
        timePeriod.setLabel(getLocalizedLabel("label.custom", locale));
        timePeriod.setTimeFromString(from);
        timePeriod.setTimeToString(to);
        return getTimePeriodList(locale, timePeriod);
    }

    private static String getLocalizedLabel(String key, Locale locale){
        final String bundleName = "tpe_labels";
        return MessageResources.getInstance().getOwnedLocalizedMessage(TimePeriodEvaluator.class, bundleName, locale, key, null);
    }

}
