package com.tmx.util;

import java.util.Set;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;

/** Provides methods to manipulate over data sets */
public class DatasetUtil {

    /** Converts Set into List */
    public static List convert(Set dataSet){
        if(dataSet == null)
            return null;
        List list = new ArrayList();
        Iterator iter = dataSet.iterator();
        while(iter.hasNext()){
            list.add(iter.next());
        }
        return list;
    }
}
