package com.tmx.beng.csapi.java.base;

import com.tmx.beng.access.Connection;
import com.tmx.beng.base.BillingMessage;
import com.tmx.util.MD5;

import java.util.Date;


abstract public class DefaultClientReq<T> extends ClientRequest<T> {

    DefaultClientReq(Connection connect) {
        super(connect);
    }

    public void setProcessingCode(String processingCode) {
        requestMsg.setAttributeString(com.tmx.beng.base.BillingMessage.PROCESSING_CODE, processingCode);
    }

    public void setPassword(String password) {
        requestMsg.setAttributeString(BillingMessage.PASSWORD, password);
    }

    public void setPasswordMD5(String password) {
        requestMsg.setAttributeString(BillingMessage.PASSWORD, MD5.getHashString(password).toUpperCase());
    }

    public void setLogin(String login) {
        requestMsg.setAttributeString(BillingMessage.LOGIN, login);
    }

    public void setTerminalSN(String terminalSN) {
        requestMsg.setAttributeString(BillingMessage.TERMINAL_SN, terminalSN);
    }

    public void setSellerCode(String sellerCode) {
        requestMsg.setAttributeString(BillingMessage.SELLER_CODE, sellerCode);
    }

    public void setClientTime(Date date) {
        requestMsg.setAttribute(BillingMessage.CLIENT_TIME, date);
    }

    public void setCliTransactionNum(String number) {
        requestMsg.setAttributeString(BillingMessage.CLI_TRANSACTION_NUM, number);
    }

}
