package com.tmx.beng.base.cache;

import com.tmx.as.entities.bill.processing.Processing;

import java.util.List;
import java.util.Vector;
import java.util.Map;
import java.util.HashMap;

/**
 */
class ProcessingWrapper {
    Processing processing;
    List restriction = new Vector();
    Map sellerId_seller = new HashMap();

    ProcessingWrapper(Processing processing){
        this.processing = processing;
    }

    public Processing getProcessing() {
        return processing;
    }

    public List getRestriction() {
        return restriction;
    }
}
