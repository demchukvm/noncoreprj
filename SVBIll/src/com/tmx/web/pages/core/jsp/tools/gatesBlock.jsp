<%@ page import="com.tmx.web.forms.core.tools.GatesBlockForm" %>
<%@ page import="java.util.ArrayList" %>
<%@ taglib prefix="nested" uri="http://jakarta.apache.org/struts/tags-nested" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/tld/tmx-controls.tld" prefix="tmx_ctrl" %>

<html:form action="GatesBlockAction">

<table border="0">
    <tr valign="top">
<%
    ArrayList gateOn = ((GatesBlockForm) pageContext.getSession().getAttribute("GatesBlockForm")).getGateOn();
    ArrayList gateOff = ((GatesBlockForm) pageContext.getSession().getAttribute("GatesBlockForm")).getGateOff();
%>
        <td align="left">
            <table border="0" width="210" style="border: black 0px dotted; background-color: white;">
                <tr style="background-color: white; color: green;">
                    <th colspan="2" align="center">
                        <bean:message bundle="core.labels" key="gatesBlock.label.gatesOn"/>
                    </th>
                </tr>
				<tr style="background-color: white;">
					<td width="200" style="border: black 1px dotted;">
                        <bean:message bundle="core.labels" key="gatesBlock.label.closeAll"/>
                    </td>
					<td style="border: black 1px dotted;"><input type="checkbox" name="cbAllOff"></td>
				</tr>
				<tr><td colspan="2" align="center"><tmx_ctrl:button property="btnRemoveMediumAccesses"/></td></tr>
                <%
                    for(Object gate : gateOn) {
                %>
                <tr style="background-color: white;">
                    <td width="200" style="border: black 1px dotted;">
                     <%=gate%>
                    </td>
                    <td style="border: black 1px dotted;">
                        <input type="checkbox" name="<%="cb_"+gate%>">
                    </td>
                </tr>
                <%
                    }
                %>
            </table>
        </td>

		<td width="5"/>

        <td align="left">
            <table border="0" width="210" style="border: black 0px dotted; background-color: white;">
                <tr style="background-color: white; color: red;">
                    <th colspan="2" align="center">
                        <bean:message bundle="core.labels" key="gatesBlock.label.gatesOff"/>
                    </th>
                </tr>
				<tr style="background-color: white;">
					<td width="200" style="border: black 1px dotted;">
                        <bean:message bundle="core.labels" key="gatesBlock.label.openAll"/>
                    </td>
					<td style="border: black 1px dotted;"><input type="checkbox" name="cbAllOn"></td>
				</tr>
				<tr><td colspan="2" align="center"><tmx_ctrl:button property="btnRestoreMediumAccesses"/></td></tr>
                <%
                    for(Object gate : gateOff) {
                %>
                <tr style="background-color: white;">
                    <td width="200" style="border: black 1px dotted;">
                    <%=gate%>
                    </td>
                    <td style="border: black 1px dotted;">
                        <input type="checkbox" name="<%="cb_"+gate%>">
                    </td>
                </tr>
                <%
                    }
                %>
            </table>
        </td>

		<td width="100"/>

		<td>
			<table border="0">
				<tr align="center">
					<th><bean:message bundle="core.labels" key="gatesBlock.label.product"/></th>
					<th><bean:message bundle="core.labels" key="gatesBlock.label.gate"/></th>
				</tr>
			<%
				ArrayList productGate = ((GatesBlockForm) pageContext.getSession().getAttribute("GatesBlockForm")).getProductGate();
				for(Object line : productGate)
				{
			%>
				<tr align="left">
					<%=line%>
				</tr>
			<%
				}
			%>
			</table>
		</td>
    </tr>
</table>

</html:form>
