package com.tmx.util.i18n;

import com.tmx.util.PropertiesLocalized;

import java.util.*;

/**
 * Localized message dictionary 
 * Singleton design pattern.
 */
public class MessageDictionary extends MessageResources{
    /** Only single instance available */
    private static MessageDictionary messageDictionary;


    /** to hide default constructor */
    protected MessageDictionary(){
    }

    /** Return single instance */
    public static MessageDictionary getDictionaryInstance(){
        if(messageDictionary == null)
            messageDictionary = new MessageDictionary();

        return messageDictionary;
    }

    /** Convert PropertiesLocalized into the List of Entris */
    public List getList(Class ownerClass, String bundleName, Locale locale){
        List entries = new ArrayList();
        PropertiesLocalized propertiesLocalized = getPropertiesLocalized(ownerClass, bundleName, locale);
        if(propertiesLocalized == null)
            return entries;
        for(Iterator iter = propertiesLocalized.keySet().iterator(); iter.hasNext();){
            String key = (String)iter.next();
            entries.add(new Entry(key, propertiesLocalized.getProperty(key)));
        }
        return entries;
    }

    /** Return list of string dictionary keys */
    public List getKeys(Class ownerClass, String bundleName, Locale locale){
        List keys = new ArrayList();
        PropertiesLocalized propertiesLocalized = getPropertiesLocalized(ownerClass, bundleName, locale);
        if(propertiesLocalized == null)
            return keys;
        for(Iterator iter = propertiesLocalized.keySet().iterator(); iter.hasNext();)
            keys.add(iter.next());
        return keys;        
    }

    /** Readonly getter */
    public class Entry{
        private String key;
        private String name;

        public Entry(String key, String name){
            this.key = key;
            this.name = name;
        }

        public String getKey() {
            return key;
        }

        public String getName() {
            return name;
        }
    }

}
