package com.tmx.gate.datastream.reporter;

import com.tmx.as.base.EntityManager;
import com.tmx.as.base.FilterWrapper;
import com.tmx.as.base.OrderWrapper;
import com.tmx.as.entities.bill.operator.Operator;
import com.tmx.as.entities.mobipay.beeline.Beeline;
import com.tmx.as.entities.mobipay.report.Report;
import com.tmx.as.exceptions.DatabaseException;
import com.tmx.gate.datastream.base.DataStreamException;
import com.tmx.util.Configuration;
import org.apache.log4j.Logger;
import org.ftp4che.FTPConnection;
import org.ftp4che.exception.FtpIOException;
import org.ftp4che.util.ftpfile.FTPFile;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;

/** FINSTAT datastream loader */
public class BeelineFtpReporter extends AbstractFtpReporter {
    private static final DecimalFormat moneyFormat = new DecimalFormat("###.00");

    private static final String HTTPS_GATE_NAME = "beeline_https_gate";
    private static final String USER_NAME = "username";
    /** Local directory to tempore store generated report file before upload  */
    private static final String TEMP_DIR = "tempDir";
    private static final String DIR = "directory";
    /** Local direcotory to store copies of uploaded report files */
    private static final String BACKUP_DIR = "backupDir";

    public BeelineFtpReporter(){
        logger = Logger.getLogger("beeline_gate_reporter.Reporter");
    }

    protected void sendReport(Operator operator, Date date) throws DataStreamException {
        EntityManager entityManager = new EntityManager();
        try{
            Properties properties = getOwnProperties();
            Properties httpsgateProperties = getProperties(HTTPS_GATE_NAME);
            String userName = httpsgateProperties.getProperty(USER_NAME);

            FilterWrapper by_date1 = new FilterWrapper("by_date2");
            by_date1.setParameter("for_date", date);

            List list = entityManager.RETRIEVE_ALL(
                    Beeline.class,
                    new FilterWrapper[]{
                            by_date1,
                            new FilterWrapper("by_good_status")
                    },
                    new OrderWrapper[]{
                            new OrderWrapper("commitDate", OrderWrapper.ASC)
                    });

            // create filename
            String fileName = userName +
                    new SimpleDateFormat("yyyyMMdd").format(date) +
                    ".csv";

            // upload is done
            if(upload(list, fileName, userName, properties)){

                // update report table
                Report report = new Report();
                report.setReportDate(date);
                report.setFileName(fileName);
                report.setOperator(operator);
                entityManager.SAVE(report);

                // update report_kyivstar table
                for(Iterator it = list.iterator(); it.hasNext(); ){
                    Beeline beeline = (Beeline)it.next();
                    beeline.setReport(report);
                    entityManager.SAVE(beeline);
                }
            }

        }catch(DatabaseException e){
             throw new DataStreamException("Database error", e);
        }
    }

    private boolean upload(List transactions, String fileName, String userName, Properties properties){
        File file = null;
        boolean ret = false;
        try{
            // create local file
            file = new File(Configuration.getInstance().substituteVariablesInString(properties.getProperty(TEMP_DIR)) + "/" + fileName);
            OutputStream os = new FileOutputStream(file);

            // write data to file
            boolean emptyFile = true;
            for(Iterator it = transactions.iterator(); it.hasNext();){
                emptyFile = false;
                Beeline beeline = (Beeline)it.next();
                os.write(userName.getBytes());
                os.write(",".getBytes());
                os.write(beeline.getMsisdn().getBytes());
                os.write(",".getBytes());
                os.write(moneyFormat.format(beeline.getAmount().doubleValue()).getBytes());
                os.write(",".getBytes());
                os.write(beeline.getCurrency().toString().getBytes());
                os.write(",".getBytes());
                os.write(beeline.getPayId().toString().getBytes());
                os.write(",".getBytes());
                os.write(beeline.getReceiptNum().toString().getBytes());
                os.write(",".getBytes());
                os.write(new SimpleDateFormat("dd.MM.yyyy HH:mm:ss").format(
                        beeline.getCommitDate()).getBytes());
                os.write("\n".getBytes());
            }
            if(emptyFile){
                os.write("empty".getBytes());
            }
            os.close();

            // open connection if not opened
            FTPConnection connection = getFTPConnection();
            if(connection == null){
                openConnection();
                connection = getFTPConnection();
            }

            // upload file
            logger.info("Uploading file " + fileName);
            connection.uploadFile(new FTPFile(file), new FTPFile(properties.getProperty(DIR), fileName));

            // optionally create local backup copy of file
            if(properties.getProperty(BACKUP_DIR) != null)
                backupSentReport(file, Configuration.getInstance().substituteVariablesInString(properties.getProperty(BACKUP_DIR)));

            ret = true;
        }catch(DataStreamException e){
            e.printStackTrace();
            logger.error(e.getLocalizedMessage(), e);
        }catch(FtpIOException e){
            e.printStackTrace();
            logger.error(e.getLocalizedMessage(), e);
        }catch(Throwable e){
            e.printStackTrace();
            logger.error(e.getLocalizedMessage(), e);
        }finally{

            // delete local file
            try{
                if(file != null) file.delete();
            }catch(Throwable e){
                logger.error(e.getLocalizedMessage());
            }

        }
        return ret;
    }

    public void test()throws Throwable{
        setName("BEELINE_FTP_REPORTER");
        
        openConnection();
        getFTPConnection().uploadFile(new FTPFile(new File("D:\\temp\\svcard20070530.csv")), new FTPFile("/in","svcard20070530.csv"));
    }
}
