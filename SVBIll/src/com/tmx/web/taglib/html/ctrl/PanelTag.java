package com.tmx.web.taglib.html.ctrl;

import com.tmx.web.taglib.base.AbstractSupportTag;
import com.tmx.web.controls.Panel;

import javax.servlet.jsp.JspException;

/**
 */
public abstract class PanelTag extends AbstractSupportTag {
    /** name of corresponding control in form bean */
    private String panelName = null;
    /** Panel title key */
    private String titleKey = null;

    //------------Properties
    public String getTitleKey() {
        return titleKey;
    }

    public void setTitleKey(String titleKey) {
        this.titleKey = titleKey;
    }

    public String getTitle(){
        return (getTitleKey() != null) ? getLocalizedLabel(getTitleKey(), null) : getLocalizedLabel("tag.panel.label.title", null);
    }

    public String getPanelName() {
        return panelName;
    }

    public void setPanelName(String panelName) {
        this.panelName = panelName;
    }

    //------------Proteced methods
    protected Panel getPanel() throws JspException {
        setProperty(getPanelName());
        Object object = getPropertyObject();
        return (object != null && object instanceof Panel) ?
               (Panel)object : null;
    }
}
