package com.tmx.as.base;

import org.hibernate.Session;

/**
 * Holds current thread local database session
 */
public class DbSessionContext{
    private Session session = null;
    private boolean isTransactionInProgress = false;
    private boolean isSessionOpened = false;
    private ConnectionOpennerLevel connectionOpenerLevel = null;
    private TransactionInitiatorLevel transactionInitiatorLevel = null;
    /** Transaction time statistic */
    private long transactionStartTime = 0;
    private long transactionLengthTime = 0;
    /** Session time statistic */
    private long sessionStartTime = 0;
    private long sessionLengthTime = 0;
    /** transaction nesting level */
    private int transactionNestingLevel = 0;

    public DbSessionContext(Session session) {
        this.session = session;
    }

    public Session getSession(){
        return session;
    }

    public boolean isTransactionInProgress() {
        return isTransactionInProgress;
    }

    public void setTransactionInProgress(boolean transactionInProgress) {
        //on start transaction
        if(!isTransactionInProgress && transactionInProgress)
            transactionStartTime = System.currentTimeMillis();
        //on close transaction
        if(isTransactionInProgress && !transactionInProgress)
            transactionLengthTime = System.currentTimeMillis() - transactionStartTime;

        isTransactionInProgress = transactionInProgress;
    }

    public boolean isSessionOpened() {
        return isSessionOpened;
    }

    public void setSessionOpened(boolean sessionOpened) {
        //on start session
        if(!isSessionOpened && sessionOpened)
            sessionStartTime = System.currentTimeMillis();
        //on close session
        if(isSessionOpened && !sessionOpened)
            sessionLengthTime = System.currentTimeMillis() - sessionStartTime;

        isSessionOpened = sessionOpened;
    }

    public ConnectionOpennerLevel getConnectionOpenerLevel() {
        return connectionOpenerLevel;
    }

    public void setConnectionOpenerLevel(ConnectionOpennerLevel connectionOpenerLevel) {
        this.connectionOpenerLevel = connectionOpenerLevel;
    }

    public TransactionInitiatorLevel getTransactionInitiatorLevel() {
        return transactionInitiatorLevel;
    }

    public void setTransactionInitiatorLevel(TransactionInitiatorLevel transactionInitiatorLevel) {
        this.transactionInitiatorLevel = transactionInitiatorLevel;
    }

    public long getTransactionLengthTime() {
        if(isTransactionInProgress)
            return System.currentTimeMillis() - transactionStartTime;
        else
            return transactionLengthTime;
    }

    public long getSessionLengthTime() {
        if(isSessionOpened)
            return System.currentTimeMillis() - sessionStartTime;
        else
            return sessionLengthTime;
    }

    public void incTransactionNestingLevel(){
        transactionNestingLevel++;
    }

    public void decTransactionNestingLevel(){
        transactionNestingLevel--;
    }

    public boolean isInNestedTransaction(){
        return transactionNestingLevel != 0;
    }

    /** Type of connection openner */
    public static class ConnectionOpennerLevel {
        private String level = null;
        public static ConnectionOpennerLevel EXPLICIT = new ConnectionOpennerLevel("EXPLICIT");
        public static ConnectionOpennerLevel TRANSACTION = new ConnectionOpennerLevel("TRANSACTION");
        public static ConnectionOpennerLevel IMPLICIT = new ConnectionOpennerLevel("IMPLICIT");

        private ConnectionOpennerLevel(String level){
            this.level = level;
        }

        public String toString(){
            return level;
        }
    }

    /** Type of transaction initiator */
    public static class TransactionInitiatorLevel {
        private String level = null;
        public static TransactionInitiatorLevel TRANSACTION = new TransactionInitiatorLevel("TRANSACTION");
        public static TransactionInitiatorLevel IMPLICIT = new TransactionInitiatorLevel("IMPLICIT");

        private TransactionInitiatorLevel(String level){
            this.level = level;
        }

        public String toString(){
            return level;
        }
    }

}
