package com.tmx.as.blogic;

import com.tmx.as.base.EntityManager;
import com.tmx.as.base.QueryParameterWrapper;
import com.tmx.as.entities.bill.balance.OperatorBalance;
import com.tmx.as.entities.bill.operator.Operator;
import com.tmx.as.exceptions.DatabaseException;
import com.tmx.web.forms.core.dictionary.operator.InfoOperatorForm;

import java.util.Date;
import java.util.List;


public class OperatorManager extends EntityManager {

    public static final String PRIMARY_BALANCE_DESCRIPTION = "Primary balance";
    public static final String PRIMARY_BALANCE_PREFIX = "_primary_balance";
    public static final String PRIMARY_BALANCE_NAME = "Primary balance";

    public List<Operator> getAllOperators() throws DatabaseException {
        return RETRIEVE_ALL(Operator.class, null);
    }

    public Operator getOperator(Long operatorId) throws DatabaseException {
        System.out.println("OperatorManager.getOperator@@@@ " + operatorId);
        return (Operator) RETRIEVE(Operator.class, operatorId, null);
    }

    //SAVE-----------------------------------------------------------
//    public void saveOperator(Operator Operator) throws DatabaseException {
//        SAVE(Operator);
//    }

    //Save seller with primary balance
//    public void saveOperator(Operator savedOperator, long primaryAmount) throws BlogicException, DatabaseException {
//        saveOperator(savedOperator, new Long(primaryAmount));
//    }

    public void saveOperator(Operator savedOperator) throws BlogicException, DatabaseException {
        saveOperator(savedOperator, createPrimaryBalance(savedOperator, new Long(0)) );
    }

    public void saveOperator(Operator savedOperator, OperatorBalance primaryBalance) throws BlogicException, DatabaseException {
        if ( !primaryBalance.isTransient() )//if this primaryBalance is assigned to another seller
            throw new BlogicException("err.operatormanager.balance_is_not_transient");//TODO(A.N.) make it unchecked

        if( savedOperator.isTransient() ) {
        try {
            BEGIN(savedOperator);
                SAVE(savedOperator);
                primaryBalance.setOperator(savedOperator);
                SAVE(primaryBalance);
                savedOperator.setPrimaryBalance(primaryBalance);
                SAVE(savedOperator, new String[]{"operatorBalance"});
            COMMIT(savedOperator);
        } catch (DatabaseException e) {
            ROLLBACK(savedOperator);
            throw new DatabaseException("err.operatormanager.operator_creation_failed", e);
        }
        } else {
            updateOperator(savedOperator);
    }
    }
    public void updateOperator(Operator operator) throws DatabaseException {
        SAVE(operator, new String[]{"code", "name", "description"});
    }
    public void deleteOperator(Long deletedOperatorId) throws DatabaseException {
        try {
            BEGIN("svbill");
                Operator operator = (Operator) RETRIEVE( Operator.class, deletedOperatorId, new String[]{"primaryBalance"} );
                DELETE( OperatorBalance.class, operator.getPrimaryBalance().getBalanceId() );
                DELETE( Operator.class, deletedOperatorId );
            COMMIT("svbill");
        } catch (DatabaseException e) {
            ROLLBACK("svbill");
            throw e;
        }
    }


    //primary Operator balance for Operator
    private OperatorBalance createPrimaryBalance(Operator operator, Long amount){
        OperatorBalance operatorBalance = new OperatorBalance();
        operatorBalance.setBlocked(false);
        operatorBalance.setDescription(PRIMARY_BALANCE_DESCRIPTION);
        operatorBalance.setName(PRIMARY_BALANCE_NAME);
        operatorBalance.setRegistrationDate(new Date());
        operatorBalance.setCode(operator.getCode() + PRIMARY_BALANCE_PREFIX);
        operatorBalance.setAmount(new Double(amount.doubleValue()));
        return operatorBalance;
    }


}
