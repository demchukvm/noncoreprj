package com.tmx.beng.base.dbfunctions;

import com.tmx.beng.base.Reservation;
import com.tmx.beng.base.BillingMessage;
import com.tmx.beng.base.cache.InvokeFunctionException;
import com.tmx.as.entities.bill.transaction.TransactionType;

import java.util.Map;

public class GraduatedSellerTariffOperatorSensetive extends SellerTariffOperatorSensitive {
    public static final String SELLER_DELTA = "seller_delta";
    public static final String OPERATOR_LIMIT = "operator_limit";

    public void applyTariff(Map actualParams, Reservation reservation) throws InvokeFunctionException {
        //get service code
        String serviceCode = task.getProcesedMessage().getAttributeString(BillingMessage.SERVICE_CODE);

        double txCost;
        double discontPercent;
        double amount;
        double sellerDelta = 0;
        double operatorLimit = 0;

        try {
            txCost = Double.parseDouble((String) actualParams.get(serviceCode + SEPARATOR + TX_COST));
            discontPercent = Double.parseDouble((String) actualParams.get(serviceCode + SEPARATOR + DISCOUNT_PERCENT));
            amount = Double.parseDouble(task.getProcesedMessage().getAttributeString(BillingMessage.AMOUNT));
            operatorLimit = Double.parseDouble(task.getProcesedMessage().getAttributeString(serviceCode + SEPARATOR + OPERATOR_LIMIT));
            sellerDelta = Double.parseDouble(task.getProcesedMessage().getAttributeString(serviceCode + SEPARATOR + SELLER_DELTA));
        }
        catch (Throwable e) {
            throw new InvokeFunctionException("err.seller_tariff_operator_sensitive.param_error", e);
        }

        if (amount < operatorLimit) {
            txCost = 0;
            discontPercent = 1;
        } else {
            sellerDelta = 0;
        }

        final double tariffedAmount = ((amount + txCost) * discontPercent) + sellerDelta;

        reservation.reserve(null, tariffedAmount, null, TransactionType.CODE_CREDIT);
    }
}
