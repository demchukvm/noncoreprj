package com.tmx.beng.iso8583Gate.pdu.substructure.tagged;

import com.tmx.beng.iso8583Gate.pdu.substructure.BasicRuntimeSubDE;

import java.util.*;

/**

 */
public class RuntimeSimpleTaggedSubDE extends BasicRuntimeSubDE {
    private Map valuesByTag;
    private List valuesByOrder;

    RuntimeSimpleTaggedSubDE(){
        this.valuesByOrder = new ArrayList();
        this.valuesByTag = new HashMap();
    }

    public String getValue(String tag){
        ValueHandler valueHandler = (ValueHandler)valuesByTag.get(tag);
        return valueHandler != null ? valueHandler.value : null;
    }

    public void addValue(String tag, String value){
        if(tag != null && value != null){
            ValueHandler valueHandler = new ValueHandler(tag, value);
            valuesByTag.put(valueHandler.tag, valueHandler);
            valuesByOrder.add(valueHandler);            
        }
    }

    Iterator iterator(){
        return valuesByOrder.iterator();
    }

    public class ValueHandler{
        private String tag;
        private String value;

        private ValueHandler(String tag, String value){
            this.tag = tag;
            this.value = value;
        }


        public String getTag() {
            return tag;
        }

        public String getValue() {
            return value;
        }
    }
}
