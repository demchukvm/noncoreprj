package com.tmx.as.blogic.balance;

import com.tmx.as.entities.bill.balance.AbstractBalance;
import com.tmx.as.entities.bill.balance.OperatorBalance;
import com.tmx.as.entities.bill.operator.Operator;
import com.tmx.as.entities.bill.restriction.AbstractRestriction;
import com.tmx.as.entities.bill.restriction.OperatorRestriction;
import com.tmx.as.entities.general.user.User;
import com.tmx.as.exceptions.DatabaseException;
import com.tmx.as.blogic.BlogicException;
import com.tmx.beng.csapi.java.base.BillingClientApi;
import com.tmx.beng.base.BillException;
import com.tmx.util.InitException;

import java.util.List;


public class OperatorBalanceManager extends AbstractBalanceManager{

    public OperatorBalanceManager() {
        super();
    }

    public void deleteBalance(Long balanceId) throws DatabaseException{
        DELETE(OperatorBalance.class, balanceId);
    }

    public void deleteRestriction(Long restrictionId) throws DatabaseException {
        DELETE(OperatorRestriction.class, restrictionId);
    }

    public AbstractBalance getBalance(Long balanceId) throws DatabaseException {
        return (AbstractBalance)RETRIEVE(OperatorBalance.class, balanceId, new String[]{"operator"});
    }

    public void saveBalance(AbstractBalance balance) throws DatabaseException{
        SAVE(balance);
    }

    public List getAllOwner(User loggedInUser) throws DatabaseException {
        return RETRIEVE_ALL(Operator.class, null);
    }

    public AbstractRestriction getRestriction(Long restrictionId) throws DatabaseException {
        String[] attr = {"operator","functionInstance.functionType"};
        return (AbstractRestriction) RETRIEVE(OperatorRestriction.class, restrictionId, attr);
    }

    public void saveRestriction(AbstractRestriction restriction) throws DatabaseException {
        SAVE(restriction.getFunctionInstance(), new String[]{"functionType"});
        SAVE(restriction);
    }

    public void changeBalance(BalanceModificationConfig config) throws BlogicException, InitException, BillException, DatabaseException {
        config.validateConfiguration();
        OperatorBalance balance = (OperatorBalance) getBalance(config.getBalanceId());

        BillingClientApi.WebOperatorRefillMessage message = billingClient.createOperatorRefillMessage();
        message.setServiceCode(balance.getOperator().getCode());
        message.setTransDescription(config.getDescription());
        message.setUserId(config.getUserActorId().toString());
        message.setTransactionType(config.getType().getCode());
        message.setAmount(config.getAmount().toString());
        message.setPaymentReason(config.getPaymentReason());
        message.setBalanceCode(balance.getCode());

        message.send();
    }
}
