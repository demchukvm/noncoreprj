package com.tmx.web.base;

import com.tmx.as.base.Reconfigurable;
import com.tmx.util.InitException;

/**
 Load and handle metadata of roles behaviors to be used by
 BasicBehaviorizedActionForm and BasicBehaviorizedAction.   
 */
public class RoleBehaviorService implements Reconfigurable {

    public void reload() throws InitException {
    }
}
