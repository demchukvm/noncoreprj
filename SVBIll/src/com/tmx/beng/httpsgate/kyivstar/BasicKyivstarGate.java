package com.tmx.beng.httpsgate.kyivstar;

import com.tmx.as.base.criterion.CriterionWrapper;
import com.tmx.as.base.criterion.Restrictions;
import com.tmx.as.entities.bill.transaction.ClientTransaction;
import com.tmx.as.entities.bill.transaction.OperatorBalanceTransaction;
import com.tmx.as.entities.bill.transaction.SellerBalanceTransaction;
import com.tmx.as.entities.bill.transaction.TerminalBalanceTransaction;
import com.tmx.as.exceptions.DatabaseException;
import com.tmx.beng.base.BasicGate;
import com.tmx.beng.base.BillingMessage;
import com.tmx.beng.base.BillingMessageImpl;
import com.tmx.beng.base.StatusDictionary;
import com.tmx.beng.access.Access;
import com.tmx.beng.base.dbfunctions.BasicTariffFunction;
import com.tmx.beng.httpsgate.HttpsGateException;
import com.tmx.beng.httpsgate.HttpsRequestException;
import com.tmx.beng.httpsgate.EasySSLProtocolSocketFactory;
import com.tmx.beng.httpsgate.BadResponseException;
import com.tmx.beng.medaccess.GateException;
import com.tmx.util.queue.ActionQueue;
import com.tmx.util.InitException;
import com.tmx.gate.datastream.base.GateContext;
import com.tmx.as.entities.mobipay.kyivstar.MobipayKyivstar;
import com.tmx.as.entities.mobipay.kyivstar.MobipayKyivstarAbonentInfo;
import com.tmx.as.base.EntityManager;
import com.tmx.as.base.FilterWrapper;
import org.apache.log4j.Logger;
import org.apache.commons.httpclient.*;
import org.apache.commons.httpclient.params.HttpMethodParams;
import org.apache.commons.httpclient.protocol.Protocol;
import org.apache.commons.httpclient.protocol.ProtocolSocketFactory;
import org.apache.commons.httpclient.methods.GetMethod;

import java.io.IOException;
import java.util.Date;
import java.net.InetAddress;
import java.util.List;


/**
 * Kyivstar Mobipay outgoing https gate
 */
public abstract class BasicKyivstarGate extends BasicGate {
    // properties names constants
    protected Logger logger = Logger.getLogger(getGateName());
    // access
    private Access access = null;
    // queue
    private ActionQueue messageQueue = null;
    // properties names constants
    private static final String PROP_URL = "url";
    private static final String PROP_USERNAME = "username";
    private static final String PROP_PASSWORD = "password";
    private static final String PROP_BRANCH = "branch";
    private static final String PROP_SOURCE_TYPE = "source_type";
    private static final String PROP_INIT_HANDLER_COUNT = "handlers.initial";
    private static final String PROP_MAX_HANDLER_COUNT = "handlers.max";
    private static final String PROP_MAX_HTTP_ATTEMPTS_CNT = "maxHttpAttemptCount";
    private static final String PROP_LOCAL_ADDRESS = "localAddress";
    /** Test mode flag */
    private static final String PROP_TEST_MODE = "enableTestMode";


    protected abstract String getGateName();

    public static BasicKyivstarGate getInstance(String gateName) throws HttpsGateException {
        if (gateName == null) {
            return null;
        }
        if (gateName.equals(KyivstarGate.GATE_NAME)) {
            return KyivstarGate.getInstance();
        } else if (gateName.equals(KyivstarBonusCommissionGate.GATE_NAME)) {
            return KyivstarBonusCommissionGate.getInstance();
        } else if (gateName.equals(KyivstarBonusNoCommissionGate.GATE_NAME)) {
            return KyivstarBonusNoCommissionGate.getInstance();
        } else if (gateName.equals(KyivstarExclusiveGate.GATE_NAME)) {
            return KyivstarExclusiveGate.getInstance();
        } else {
            return null;
        }
    }

    protected void init0()throws InitException {
        properties = GateContext.getInstance().getDatastreamProcessorConfig(getGateName());
        access = obtainBillingEngineAccess();

        int initHandlerCount = Integer.parseInt(properties.getProperty(PROP_INIT_HANDLER_COUNT, "1"));
        int maxHandlerCount = Integer.parseInt(properties.getProperty(PROP_MAX_HANDLER_COUNT, "5"));
        messageQueue = new ActionQueue(initHandlerCount, maxHandlerCount);

        //for testing start this consule.
        if(properties.getProperty(PROP_TEST_MODE) != null &&
           "true".equalsIgnoreCase(properties.getProperty(PROP_TEST_MODE).trim())){
            KyivstarGateTest testConsole = new KyivstarGateTest();
            testConsole.start();
        }
    }

    protected void shutdown0(final long timeout){
        if(messageQueue.actionCount() != 0){
            Runtime.getRuntime().addShutdownHook(new Thread(){
                public void run(){
                    try{
                        long lt = System.currentTimeMillis();
                        while(messageQueue.actionCount() != 0 &&
                                ((System.currentTimeMillis() - lt) < timeout)){
                            sleep(1000);
                        }
                    }
                    catch(Throwable e){
                        e.printStackTrace();
                    }
                }
            });
        }
    }

    public void enqueueBillMessage(BillingMessage billMessage){
        KyivstarGateTask task = new KyivstarGateTask(billMessage, this);
        messageQueue.enqueue(task);
    }

    public Double getBalance(String operatorCode)throws HttpsRequestException, HttpsGateException{
        // cancel transaction
        KyivstarResponse  payResponse = getBallance(
                properties.getProperty(PROP_URL),
                properties.getProperty(PROP_USERNAME),
                properties.getProperty(PROP_PASSWORD)
        );

        if(payResponse.getStatusCode() != 30){ // if error
        //if(!payResponse.getStatusCode().equals(30)){ // if error
            throw new HttpsGateException(
                    obtainOperatorI18nMessage(
                            operatorCode,
                            MSG_TYPE_ERROR,
                            payResponse.getStatusCode(),
                            //Long.parseLong(payResponse.getStatusCode()),
                            "Incorrect status code " + " (Code=" + payResponse.getStatusCode() + ")")
                    ,
                    new String[]{String.valueOf(payResponse.getStatusCode())});
        }
        return payResponse.getQuota();
    }

    public void runBillMessage(BillingMessage billingMessage) throws HttpsGateException{

        BillingMessageImpl respMessage = new BillingMessageImpl();
        respMessage.setAttributeString(BillingMessage.REQUEST_ID,
                billingMessage.getAttributeString(BillingMessage.REQUEST_ID));
        try{
            // refill payment
            if(BillingMessage.O_REFILL_PAYMENT.equals(billingMessage.getOperationName())){
                refillPayment(billingMessage, respMessage);
                // cancel refill
            }else if(BillingMessage.O_CANCEL_REFILL_PAYMENT.equals(billingMessage.getOperationName())){
                cancelPayment(billingMessage, respMessage);
                // unsupported operation
            }else{
                respMessage.setStatusCode(StatusDictionary.SUBMIT_TRANSACTION_ERROR);
                throw new HttpsGateException("Unsupported command",
                           new String[]{billingMessage.getOperationName()});
            }
        }catch(Throwable e){
            e.printStackTrace();
            logger.error(e.getLocalizedMessage(), e);
            if(respMessage.getAttributeString(BillingMessage.STATUS_CODE) == null){
                respMessage.setStatusCode(StatusDictionary.UNKNOWN_ERROR);
            }
            respMessage.setAttributeString(BillingMessage.STATUS_MESSAGE, e.getLocalizedMessage());
        }finally{
            // send response
            try{
                access.obtainConnection().processAsync(respMessage);
            }catch(Throwable e){
                logger.error(e.getLocalizedMessage(), e);
            }
        }

    }

    /**
     * Cancel(Annulment) pay operation
     * @param billingMessage message managing the cancel operation
     * @param respMessage message filled with report about cancelation
     */
    private void cancelPayment(BillingMessage billingMessage, BillingMessageImpl respMessage){

        EntityManager entityManager = new EntityManager();

        try{
            respMessage.setOperationName(BillingMessage.O_CANCEL_REFILL_STATUS);

            // find transaction
            FilterWrapper byBillNumFW = new FilterWrapper("mobipay_by_billnum");
            byBillNumFW.setParameter("billnum",
                    billingMessage.getAttributeString(BillingMessage.BILL_TRANSACTION_NUM));

            MobipayKyivstar mobipayKyivstar = (MobipayKyivstar) entityManager.RETRIEVE(
                    MobipayKyivstar.class,
                    new FilterWrapper[]{  byBillNumFW },
                    null
            );
            if(mobipayKyivstar == null){ // if error
                respMessage.setStatusCode(StatusDictionary.TRANSACTION_NOT_FOUND);
                throw new HttpsGateException("transaction not found",
                        new String[]{billingMessage.getAttributeString(BillingMessage.BILL_TRANSACTION_NUM)});
            }

            // cancel transaction
            KyivstarResponse  payResponse = paymentAnnulment(
                    properties.getProperty(PROP_URL),
                    properties.getProperty(PROP_USERNAME),
                    properties.getProperty(PROP_PASSWORD),
                    mobipayKyivstar.getPayId(),
                    mobipayKyivstar.getMsisdn(),
                    mobipayKyivstar.getPayAccount(),
                    mobipayKyivstar.getReceiptNum(),
                    mobipayKyivstar.getAmount()

            );

            if(payResponse.getStatusCode() != 80){ // if error
            //if(!payResponse.getStatusCode().equals(80)){ // if error
                respMessage.setStatusCode(StatusDictionary.BEGIN_TRANSACTION_ERROR);
                respMessage.setAttribute(
                        BillingMessage.STATUS_MESSAGE,
                        obtainOperatorI18nMessage(
                                billingMessage.getAttributeString(BillingMessage.SERVICE_CODE),
                                MSG_TYPE_ERROR,
                                payResponse.getStatusCode(),
                                //Long.parseLong(payResponse.getStatusCode()),
                                null)
                );

                throw new HttpsGateException("Incorrect status code",
                        new String[]{String.valueOf(payResponse.getStatusCode())});
            }
            respMessage.setStatusCode(StatusDictionary.STATUS_OK);

            // change mobipay status
            mobipayKyivstar.setStatusCode(StatusDictionary.TRANSACTION_CANCELED);
            entityManager.SAVE(mobipayKyivstar);

            // copy transactions to response message
            respMessage.setAttribute(BillingMessage.TERMINAL_TRANSACTION,
                    billingMessage.getAttribute(BillingMessage.TERMINAL_TRANSACTION));
            respMessage.setAttribute(BillingMessage.OPERATOR_TRANSACTION,
                    billingMessage.getAttribute(BillingMessage.OPERATOR_TRANSACTION));
            respMessage.setAttribute(BillingMessage.CLIENT_TRANSACTION,
                    billingMessage.getAttribute(BillingMessage.CLIENT_TRANSACTION));

        }catch(Throwable e){
            e.printStackTrace();
            logger.error(e.getLocalizedMessage(), e);
            if(respMessage.getAttributeString(BillingMessage.STATUS_CODE) == null){
                respMessage.setStatusCode(StatusDictionary.UNKNOWN_ERROR);
            }
            if(respMessage.getAttribute(BillingMessage.STATUS_MESSAGE) == null){
                respMessage.setAttributeString(BillingMessage.STATUS_MESSAGE, e.getLocalizedMessage());
            }
        }
    }

    /**
     *  Refill payment
     * @param billingMessage message managing the refill operation.
     * @param respMessage message filled with response information about refill.
     */
    private void refillPayment(BillingMessage billingMessage, BillingMessageImpl respMessage)
    {
        boolean isRedirect; // for check KS Redirect
        if(billingMessage.getAttributeString(BillingMessage.IS_REDIRECT) != null)
        {
            if(billingMessage.getAttributeString(BillingMessage.IS_REDIRECT).equals("false"))
            {
                isRedirect = false;
            }
            else
            {
                isRedirect = true;
            }
        }
        else // IS_REDIRECT = null
        {
            isRedirect = false;
        }

        EntityManager entityManager = new EntityManager();

        MobipayKyivstar mobipayKyivstar = new MobipayKyivstar();
        MobipayKyivstarAbonentInfo abonentInfo = new MobipayKyivstarAbonentInfo();
        mobipayKyivstar.setStatusCodeInt(StatusDictionary.STATUS_OK);
        KyivstarResponse payResponse;

        try {
            respMessage.setOperationName(BillingMessage.O_REFILL_PAYMENT_RESULT);
            mobipayKyivstar.setBankPayId(billingMessage.getAttributeString(BillingMessage.BILL_TRANSACTION_NUM));
            mobipayKyivstar.setMsisdn(billingMessage.getAttributeString(BillingMessage.ACCOUNT_NUMBER));
            mobipayKyivstar.setPayAccount(billingMessage.getAttributeString(BillingMessage.PAY_ACCOUNT_NUMBER));
            mobipayKyivstar.setAmount(new Double(billingMessage.getAttributeString(BillingMessage.AMOUNT)));
            mobipayKyivstar.setSourceType(new Integer(properties.getProperty(PROP_SOURCE_TYPE)));
            mobipayKyivstar.setBranch(properties.getProperty(PROP_BRANCH));
            mobipayKyivstar.setGateName(getGateName());
            mobipayKyivstar.setBankOperDay(new Date());
            if(billingMessage.getAttributeString(BillingMessage.TRADE_POINT) == null)
            {
                mobipayKyivstar.setTradePoint(billingMessage.getAttributeString(BillingMessage.CLI_TRANSACTION_NUM).substring(0, 8));
            }
            else
            {
                mobipayKyivstar.setTradePoint(billingMessage.getAttributeString(BillingMessage.TRADE_POINT));
            }

            // get abonent info  ACT = 7
            payResponse = getAbonentInfo(
                    properties.getProperty(PROP_URL),
                    properties.getProperty(PROP_USERNAME),
                    properties.getProperty(PROP_PASSWORD),
                    mobipayKyivstar.getMsisdn(),
                    mobipayKyivstar.getPayAccount()
            );

            if(payResponse.getStatusCode() != 21) // if error
            //if(!payResponse.getStatusCode().equals(21)) // if error
            {
                if(isRedirect == false) // IS_REDIRECT = false
                {
                    isRedirect = true;

                    if(billingMessage.getAttributeString(BillingMessage.SERVICE_CODE).equals("BeelineExc")
                            || billingMessage.getAttributeString(BillingMessage.SERVICE_CODE).equals("Beeline_FixConnect"))
                    {
                        respMessage.setAttributeString(BillingMessage.IS_REDIRECT, "true");

                        respMessage.setOperationName(BillingMessage.O_REFILL_PAYMENT_REDIRECT);

                        respMessage.setAttributeString(BillingMessage.TERMINAL_SN, billingMessage.getAttributeString(BillingMessage.TERMINAL_SN));
                        respMessage.setAttributeString(BillingMessage.TERMINAL_PSWD, billingMessage.getAttributeString(BillingMessage.TERMINAL_PSWD));
                        respMessage.setAttributeString(BillingMessage.SELLER_CODE, billingMessage.getAttributeString(BillingMessage.SELLER_CODE));
                        respMessage.setAttributeString(BillingMessage.PROCESSING_CODE, billingMessage.getAttributeString(BillingMessage.PROCESSING_CODE));
                        respMessage.setAttributeString(BillingMessage.LOGIN, billingMessage.getAttributeString(BillingMessage.LOGIN));
                        respMessage.setAttributeString(BillingMessage.PASSWORD, billingMessage.getAttributeString(BillingMessage.PASSWORD));
                        respMessage.setAttributeString(BillingMessage.CLIENT_TIME, billingMessage.getAttributeString(BillingMessage.CLIENT_TIME));

                        respMessage.setAttributeString(BillingMessage.CLI_TRANSACTION_NUM, billingMessage.getAttributeString(BillingMessage.CLI_TRANSACTION_NUM));
                        respMessage.setAttributeString(BillingMessage.SERVICE_CODE, billingMessage.getAttributeString(BillingMessage.SERVICE_CODE));
                        respMessage.setAttributeString(BillingMessage.AMOUNT, billingMessage.getAttributeString(BillingMessage.AMOUNT));
                        respMessage.setAttributeString(BillingMessage.ACCOUNT_NUMBER, billingMessage.getAttributeString(BillingMessage.ACCOUNT_NUMBER));
                        respMessage.setAttributeString(BillingMessage.PAY_ACCOUNT_NUMBER, billingMessage.getAttributeString(BillingMessage.PAY_ACCOUNT_NUMBER));
                        respMessage.setAttributeString(BillingMessage.REGION_NUMBER, billingMessage.getAttributeString(BillingMessage.REGION_NUMBER));
                        respMessage.setAttributeString(BillingMessage.TRADE_POINT, billingMessage.getAttributeString(BillingMessage.TRADE_POINT));

                        respMessage.setAttributeString(BillingMessage.REGISTRATION_TIME, billingMessage.getAttributeString(BillingMessage.REGISTRATION_TIME));
                        respMessage.setAttributeString(BillingMessage.BALANCE_RESERVATION_SET, billingMessage.getAttributeString(BillingMessage.BALANCE_RESERVATION_SET));
                        respMessage.setAttributeString(BasicTariffFunction.TARIFF_APPLIED, billingMessage.getAttributeString(BasicTariffFunction.TARIFF_APPLIED));
                        respMessage.setAttributeString(BillingMessage.BILL_TRANSACTION_NUM, billingMessage.getAttributeString(BillingMessage.BILL_TRANSACTION_NUM));
                        respMessage.setAttributeString(BillingMessage.ENTITY_LIST, billingMessage.getAttributeString(BillingMessage.ENTITY_LIST));

                        logger.info("Redirect from KS to Beeline  " + billingMessage.getAttributeString(BillingMessage.BILL_TRANSACTION_NUM));
                    }

//                    throw new HttpsGateException("Redirect from KS to Beeline",
//                            new String[]{String.valueOf(payResponse.getStatusCode())});
                }
                else  // IS_REDIRECT = true                          ???????????????????????????????????????
                {
                    respMessage.setStatusCode(StatusDictionary.BEGIN_TRANSACTION_ERROR);
                    respMessage.setAttribute(
                            BillingMessage.STATUS_MESSAGE,
                            obtainOperatorI18nMessage(
                                    billingMessage.getAttributeString(BillingMessage.SERVICE_CODE),
                                    MSG_TYPE_ERROR,
                                    payResponse.getStatusCode(),
                                    //Long.parseLong(payResponse.getStatusCode()),
                                    null)
                    );

                    throw new HttpsGateException("Incorrect status code",
                            new String[]{String.valueOf(payResponse.getStatusCode())});
                    }

             
            }

            if(isRedirect == false) // IS_REDIRECT = false
            {
                // prepare abonent info
                abonentInfo.setName(payResponse.getName());
                abonentInfo.setBalance(payResponse.getBalance());
                abonentInfo.setMobipayKyivstar(mobipayKyivstar);

                // begin transaction ACT = 0
                payResponse = beginTransaction(
                        properties.getProperty(PROP_URL),
                        properties.getProperty(PROP_USERNAME),
                        properties.getProperty(PROP_PASSWORD),
                        mobipayKyivstar.getMsisdn(),
                        mobipayKyivstar.getPayAccount(),
                        mobipayKyivstar.getAmount(),
                        mobipayKyivstar.getTradePoint(),
                        mobipayKyivstar.getBranch(),
                        mobipayKyivstar.getSourceType());

                if(payResponse.getStatusCode() != 20) // if error
                //if(!payResponse.getStatusCode().equals(20)) // if error
                {
                    respMessage.setStatusCode(StatusDictionary.BEGIN_TRANSACTION_ERROR);
                    respMessage.setAttribute(
                            BillingMessage.STATUS_MESSAGE,
                            obtainOperatorI18nMessage(
                                    billingMessage.getAttributeString(BillingMessage.SERVICE_CODE),
                                    MSG_TYPE_ERROR,
                                    payResponse.getStatusCode(),
                                    //Long.parseLong(payResponse.getStatusCode()),
                                    null)
                    );

                    throw new HttpsGateException("Incorrect status code",
                            new String[]{String.valueOf(payResponse.getStatusCode())});
                }

                mobipayKyivstar.setPayId(payResponse.getPayId());

                // check payment ability ACT= 4
                payResponse = queryPaymentTransaction(
                        properties.getProperty(PROP_URL),
                        properties.getProperty(PROP_USERNAME),
                        properties.getProperty(PROP_PASSWORD),
                        mobipayKyivstar.getPayId());

                if(payResponse.getStatusCode() != 21) // if error
                //if(!payResponse.getStatusCode().equals(21)) // if error
                {
                    cancelPaymentTransaction(
                            properties.getProperty(PROP_URL),
                            properties.getProperty(PROP_USERNAME),
                            properties.getProperty(PROP_PASSWORD),
                            mobipayKyivstar.getPayId());

                    //if(billingMessage.getAttributeString(BillingMessage.IS_REDIRECT).equals("false")) // IS_REDIRECT = false
                    if(isRedirect == false) // IS_REDIRECT = false
                    {
                        isRedirect = true;

                        if(billingMessage.getAttributeString(BillingMessage.SERVICE_CODE).equals("BeelineExc")
                                || billingMessage.getAttributeString(BillingMessage.SERVICE_CODE).equals("Beeline_FixConnect"))
                        {
                            respMessage.setAttributeString(BillingMessage.IS_REDIRECT, "true");

                            respMessage.setOperationName(BillingMessage.O_REFILL_PAYMENT_REDIRECT);

                            respMessage.setAttributeString(BillingMessage.TERMINAL_SN, billingMessage.getAttributeString(BillingMessage.TERMINAL_SN));
                            respMessage.setAttributeString(BillingMessage.TERMINAL_PSWD, billingMessage.getAttributeString(BillingMessage.TERMINAL_PSWD));
                            respMessage.setAttributeString(BillingMessage.SELLER_CODE, billingMessage.getAttributeString(BillingMessage.SELLER_CODE));
                            respMessage.setAttributeString(BillingMessage.PROCESSING_CODE, billingMessage.getAttributeString(BillingMessage.PROCESSING_CODE));
                            respMessage.setAttributeString(BillingMessage.LOGIN, billingMessage.getAttributeString(BillingMessage.LOGIN));
                            respMessage.setAttributeString(BillingMessage.PASSWORD, billingMessage.getAttributeString(BillingMessage.PASSWORD));
                            respMessage.setAttributeString(BillingMessage.CLIENT_TIME, billingMessage.getAttributeString(BillingMessage.CLIENT_TIME));

                            respMessage.setAttributeString(BillingMessage.CLI_TRANSACTION_NUM, billingMessage.getAttributeString(BillingMessage.CLI_TRANSACTION_NUM));
                            respMessage.setAttributeString(BillingMessage.SERVICE_CODE, billingMessage.getAttributeString(BillingMessage.SERVICE_CODE));
                            respMessage.setAttributeString(BillingMessage.AMOUNT, billingMessage.getAttributeString(BillingMessage.AMOUNT));
                            respMessage.setAttributeString(BillingMessage.ACCOUNT_NUMBER, billingMessage.getAttributeString(BillingMessage.ACCOUNT_NUMBER));
                            respMessage.setAttributeString(BillingMessage.PAY_ACCOUNT_NUMBER, billingMessage.getAttributeString(BillingMessage.PAY_ACCOUNT_NUMBER));
                            respMessage.setAttributeString(BillingMessage.REGION_NUMBER, billingMessage.getAttributeString(BillingMessage.REGION_NUMBER));
                            respMessage.setAttributeString(BillingMessage.TRADE_POINT, billingMessage.getAttributeString(BillingMessage.TRADE_POINT));

                            respMessage.setAttributeString(BillingMessage.REGISTRATION_TIME, billingMessage.getAttributeString(BillingMessage.REGISTRATION_TIME));
                            respMessage.setAttributeString(BillingMessage.BALANCE_RESERVATION_SET, billingMessage.getAttributeString(BillingMessage.BALANCE_RESERVATION_SET));
                            respMessage.setAttributeString(BasicTariffFunction.TARIFF_APPLIED, billingMessage.getAttributeString(BasicTariffFunction.TARIFF_APPLIED));
                            respMessage.setAttributeString(BillingMessage.BILL_TRANSACTION_NUM, billingMessage.getAttributeString(BillingMessage.BILL_TRANSACTION_NUM));
                            respMessage.setAttributeString(BillingMessage.ENTITY_LIST, billingMessage.getAttributeString(BillingMessage.ENTITY_LIST));

                            logger.info("Redirect from KS to Beeline  " + billingMessage.getAttributeString(BillingMessage.BILL_TRANSACTION_NUM));
                        }

//                        throw new HttpsGateException("Redirect from KS to Beeline",
//                                new String[]{String.valueOf(payResponse.getStatusCode())});
                    }
                    else  // IS_REDIRECT = true                          ???????????????????????????????????????
                    {
                        respMessage.setStatusCode(StatusDictionary.QUERY_PAYMENT_ERROR);
                        respMessage.setAttribute(
                                BillingMessage.STATUS_MESSAGE,
                                obtainOperatorI18nMessage(
                                        billingMessage.getAttributeString(BillingMessage.SERVICE_CODE),
                                        MSG_TYPE_ERROR,
                                        payResponse.getStatusCode(),
                                        //Long.parseLong(payResponse.getStatusCode()),
                                        null)
                        );

                        throw new HttpsGateException("Incorrect status code",
                                new String[]{String.valueOf(payResponse.getStatusCode())});
                    }
                }

                //if(!respMessage.getAttributeString(BillingMessage.IS_REDIRECT).equals("true")) // IS_REDIRECT = false
                if(isRedirect == false) // IS_REDIRECT = false
                {
                    // submit transaction ACT = 1
                    try {
                        payResponse = submitPaymentTransaction(properties.getProperty(PROP_URL),
                                                            properties.getProperty(PROP_USERNAME),
                                                            properties.getProperty(PROP_PASSWORD),
                                                            mobipayKyivstar.getPayId());

                        if(payResponse.getStatusCode() != 22) // if error
                        //if(!payResponse.getStatusCode().equals(22)) // if error
                        {
                            cancelPaymentTransaction(properties.getProperty(PROP_URL),
                                                properties.getProperty(PROP_USERNAME),
                                                properties.getProperty(PROP_PASSWORD),
                                                mobipayKyivstar.getPayId());

                            respMessage.setStatusCode(StatusDictionary.SUBMIT_TRANSACTION_ERROR);
                            respMessage.setAttribute(BillingMessage.STATUS_MESSAGE,
                                    obtainOperatorI18nMessage(
                                                billingMessage.getAttributeString(BillingMessage.SERVICE_CODE),
                                                MSG_TYPE_ERROR,
                                                payResponse.getStatusCode(),
                                                //Long.parseLong(payResponse.getStatusCode()),
                                                null));

                            throw new HttpsGateException("Incorrect status code",
                                    new String[]{String.valueOf(payResponse.getStatusCode())});
                        }

                    } catch(HttpsRequestException httppre) {
                        logger.error("ACT=1 HttpsRequestException. " + httppre);
                        handlingTxAfterException(mobipayKyivstar, payResponse, respMessage);
                    } catch(Exception e) {
                        logger.error("ACT=1 HttpsRequestException. " + e);
                        handlingTxAfterException(mobipayKyivstar, payResponse, respMessage);
                    }

                    mobipayKyivstar.setCommitDate(payResponse.getTimeStamp());
                    mobipayKyivstar.setReceiptNum(new Integer(payResponse.getReceipt()));
                    respMessage.setAttribute(BillingMessage.RECEIPT_NUM, mobipayKyivstar.getReceiptNum());
                    respMessage.setAttribute(BillingMessage.PAY_ID, mobipayKyivstar.getPayId());
                    respMessage.setStatusCode(StatusDictionary.STATUS_OK);

                }
            }
        } catch(HttpsRequestException e) {
            e.printStackTrace();
            logger.error(e.getLocalizedMessage(), e);

            // transaction is began 
            if(mobipayKyivstar.getPayId() != null)
            {
                try {
                    // try to get transaction info
                    payResponse = queryTransactionStatus(
                            properties.getProperty(PROP_URL),
                            properties.getProperty(PROP_USERNAME),
                            properties.getProperty(PROP_PASSWORD),
                            mobipayKyivstar.getPayId());

                    if(payResponse.getStatusCode() != 70) // if error
                    //if(!payResponse.getStatusCode().equals(70)) // if error
                    {
                        logger.info("Can't get transaction status " + payResponse.getStatusCode());

                        // try to cancel transaction
                        payResponse = cancelPaymentTransaction(
                                properties.getProperty(PROP_URL),
                                properties.getProperty(PROP_USERNAME),
                                properties.getProperty(PROP_PASSWORD),
                                mobipayKyivstar.getPayId());

                        if(payResponse.getPayStatus() == 23) // ok
                        //if(payResponse.getPayStatus().equals(23)) // ok
                        {
                            logger.info("Transaction cancelled");
                            respMessage.setStatusCode(StatusDictionary.SUBMIT_TRANSACTION_ERROR);
                        }
                        else
                        {
                            logger.info("Cancel transaction error " + payResponse.getPayStatus());
                            respMessage.setStatusCode(StatusDictionary.UNKNOWN_ERROR);
                        }
                    }
                    else
                    {
                        switch(payResponse.getPayStatus().intValue())
                        //switch(Integer.parseInt(payResponse.getPayStatus()))
                        {
                            case 111:
                            {   // transaction is done successfully
                                mobipayKyivstar.setReceiptNum(new Integer(payResponse.getReceipt()));
                                mobipayKyivstar.setCommitDate(payResponse.getTimeStamp());
                                respMessage.setAttribute(BillingMessage.RECEIPT_NUM, mobipayKyivstar.getReceiptNum());
                                respMessage.setAttribute(BillingMessage.PAY_ID, mobipayKyivstar.getPayId());
                                respMessage.setStatusCode(StatusDictionary.STATUS_OK);
                            }
                                break;
                            case 115:
                            {   // transaction is cancelled
                                mobipayKyivstar.setCommitDate(payResponse.getTimeStamp());
                                respMessage.setStatusCode(StatusDictionary.TRANSACTION_CANCELED);
                            }
                                break;
                            default:
                            {  // invalid transaction try to cancel
                                payResponse = cancelPaymentTransaction(
                                        properties.getProperty(PROP_URL),
                                        properties.getProperty(PROP_USERNAME),
                                        properties.getProperty(PROP_PASSWORD),
                                        mobipayKyivstar.getPayId());
                                if(payResponse.getPayStatus() == 23){ // ok
                                //if(payResponse.getPayStatus().equals(23)){ // ok
                                    logger.info("Transaction cancelled");
                                    respMessage.setStatusCode(StatusDictionary.SUBMIT_TRANSACTION_ERROR);
                                }else{
                                    logger.info("Cancel transaction error " + payResponse.getPayStatus());
                                    respMessage.setStatusCode(StatusDictionary.UNKNOWN_ERROR);
                                }
                            }
                                break;

                        }
                    }

                }catch(Throwable te){
                    te.printStackTrace();
                    logger.error(te.getLocalizedMessage(), te);
                    if(respMessage.getAttributeString(BillingMessage.STATUS_CODE) == null){
                        respMessage.setStatusCode(StatusDictionary.UNKNOWN_ERROR);
                    }
                    respMessage.setAttributeString(BillingMessage.STATUS_MESSAGE, te.getLocalizedMessage());
                }
            }else{
                respMessage.setStatusCode(StatusDictionary.BEGIN_TRANSACTION_ERROR);
            }

            // other exceptions
        } catch(Throwable e) {
            e.printStackTrace();
            logger.error(e.getLocalizedMessage(), e);

            if(respMessage.getAttributeString(BillingMessage.STATUS_CODE) == null)
            {
                respMessage.setStatusCode(StatusDictionary.UNKNOWN_ERROR);
            }
            if(respMessage.getAttribute(BillingMessage.STATUS_MESSAGE) == null)
            {
                respMessage.setAttributeString(BillingMessage.STATUS_MESSAGE, e.getLocalizedMessage());
            }

        } finally {
            // save mobipayKyivstar
            try {


                if(respMessage.getOperationName().equals(BillingMessage.O_REFILL_PAYMENT_REDIRECT))
                {
                    mobipayKyivstar.setStatusCodeInt(StatusDictionary.BEGIN_TRANSACTION_ERROR);
                    mobipayKyivstar.setStatusMessage("Redirect to Beeline");
                    entityManager.SAVE(mobipayKyivstar);
                }
                else
                {
                    logger.info(respMessage.getAttributeString(BillingMessage.STATUS_CODE));
                    mobipayKyivstar.setStatusCode(new Integer(respMessage.getAttributeString(BillingMessage.STATUS_CODE)));
                    if(respMessage.getAttribute(BillingMessage.STATUS_MESSAGE) != null)
                    {
                        mobipayKyivstar.setStatusMessage(respMessage.getAttributeString(BillingMessage.STATUS_MESSAGE));
                    }
                    entityManager.SAVE(mobipayKyivstar);
                    // save abonent info if ok

                    if(mobipayKyivstar.getStatusCode() == StatusDictionary.STATUS_OK)
                    {
                        entityManager.SAVE(abonentInfo);
                    }
                }

            } catch(Throwable e) {
                e.printStackTrace();
                logger.error(e.getLocalizedMessage(), e);
                //If can`t save to DB, we should cancel this tx
                try {
                    payResponse = cancelPaymentTransaction(
                                    properties.getProperty(PROP_URL),
                                    properties.getProperty(PROP_USERNAME),
                                    properties.getProperty(PROP_PASSWORD),
                                    mobipayKyivstar.getPayId());

                    if(payResponse.getPayStatus() == 23) // ok
                    //if(payResponse.getPayStatus().equals(23)) // ok
                    {
                        logger.info("Transaction cancelled after saving to DB error");
                    }
                    else
                    {
                        logger.info("Cancel transaction error after saving to DB error " + payResponse.getPayStatus());
                    }
                } catch(HttpsRequestException e1) {
                        logger.error("Failed to cancel TX after saving to DB error");
                }
            }
        }
    }

    private void refillPayment1(BillingMessage billingMessage, BillingMessageImpl respMessage)
    {
        respMessage.setOperationName(BillingMessage.O_REFILL_PAYMENT_REDIRECT);
        // переназначить все переменные из billingMessage в respMessage
        respMessage.setAttributeString(BillingMessage.TERMINAL_SN, billingMessage.getAttributeString(BillingMessage.TERMINAL_SN));
        respMessage.setAttributeString(BillingMessage.SELLER_CODE, billingMessage.getAttributeString(BillingMessage.SELLER_CODE));
        respMessage.setAttributeString(BillingMessage.PROCESSING_CODE, billingMessage.getAttributeString(BillingMessage.PROCESSING_CODE));
        respMessage.setAttributeString(BillingMessage.LOGIN, billingMessage.getAttributeString(BillingMessage.LOGIN));
        respMessage.setAttributeString(BillingMessage.PASSWORD, billingMessage.getAttributeString(BillingMessage.PASSWORD));
        respMessage.setAttributeString(BillingMessage.CLIENT_TIME, billingMessage.getAttributeString(BillingMessage.CLIENT_TIME));
        respMessage.setAttributeString(BillingMessage.CLI_TRANSACTION_NUM, billingMessage.getAttributeString(BillingMessage.CLI_TRANSACTION_NUM));
        respMessage.setAttributeString(BillingMessage.SERVICE_CODE, billingMessage.getAttributeString(BillingMessage.SERVICE_CODE));
        respMessage.setAttributeString(BillingMessage.AMOUNT, billingMessage.getAttributeString(BillingMessage.AMOUNT));
        respMessage.setAttributeString(BillingMessage.PAY_ACCOUNT_NUMBER, billingMessage.getAttributeString(BillingMessage.PAY_ACCOUNT_NUMBER));
        respMessage.setAttributeString(BillingMessage.REGION_NUMBER, billingMessage.getAttributeString(BillingMessage.REGION_NUMBER));
        respMessage.setAttributeString(BillingMessage.TRADE_POINT, billingMessage.getAttributeString(BillingMessage.TRADE_POINT));
    }
   
    private void handlingTxAfterException(MobipayKyivstar mobipayKyivstar, KyivstarResponse payResponse, BillingMessageImpl respMessage) {
        try {
        // try to get transaction info
            try {
                //ACT = 3
                payResponse = queryTransactionStatus(properties.getProperty(PROP_URL),
                                            properties.getProperty(PROP_USERNAME),
                                            properties.getProperty(PROP_PASSWORD),
                                            mobipayKyivstar.getPayId());
            } catch (HttpsRequestException getStatusException) {
                logger.error("Failed to get status of problem tx");
                
            }
            if(payResponse.getStatusCode() != 70){ // if error
            //if(!payResponse.getStatusCode().equals(70)){ // if error
                logger.info("Can't get transaction status " + payResponse.getStatusCode());

                // try to cancel transaction                       
                try {
                    payResponse = cancelPaymentTransaction (properties.getProperty(PROP_URL),
                                                properties.getProperty(PROP_USERNAME),
                                                properties.getProperty(PROP_PASSWORD),
                                                mobipayKyivstar.getPayId());
                    if(payResponse.getPayStatus() == 23) { // ok
                    //if(payResponse.getPayStatus().equals(23)) { // ok
                        logger.info("Transaction cancelled");
                        respMessage.setStatusCode(StatusDictionary.TRANSACTION_CANCELED);
                    }else{
                        logger.info("Cancel transaction error " + payResponse.getPayStatus());
                        respMessage.setStatusCode(StatusDictionary.TRANSACTION_OPENED);
                    }
                } catch (HttpsRequestException cancelTxexception) {
                        logger.info("Failed to cancel transaction" + payResponse.getPayStatus());
                        respMessage.setStatusCode(StatusDictionary.TRANSACTION_OPENED);
                }
            } else {
                switch(payResponse.getPayStatus().intValue()){
                //switch(Integer.parseInt(payResponse.getPayStatus())){
                    case 111:
                    {   // transaction is done successfully
                        mobipayKyivstar.setReceiptNum(new Integer(payResponse.getReceipt()));
                        mobipayKyivstar.setCommitDate(payResponse.getTimeStamp());
                        respMessage.setAttribute(BillingMessage.RECEIPT_NUM, mobipayKyivstar.getReceiptNum());
                        respMessage.setAttribute(BillingMessage.PAY_ID, mobipayKyivstar.getPayId());
                        respMessage.setStatusCode(StatusDictionary.STATUS_OK);
                    }
                        break;
                    case 115:
                    {   // transaction is cancelled
                        mobipayKyivstar.setCommitDate(payResponse.getTimeStamp());
                        respMessage.setStatusCode(StatusDictionary.TRANSACTION_CANCELED);
                    }
                        break;
                    default:
                    {  // invalid transaction try to cancel
                        payResponse = cancelPaymentTransaction(properties.getProperty(PROP_URL),
                                                    properties.getProperty(PROP_USERNAME),
                                                    properties.getProperty(PROP_PASSWORD),
                                                    mobipayKyivstar.getPayId());
                        if (payResponse.getPayStatus() == 23) { // ok
                        //if (payResponse.getPayStatus().equals(23)) { // ok
                            logger.info("Transaction cancelled");
                            respMessage.setStatusCode(StatusDictionary.SUBMIT_TRANSACTION_ERROR);
                        } else {
                            logger.info("Cancel transaction error " + payResponse.getPayStatus());
                            respMessage.setStatusCode(StatusDictionary.TRANSACTION_OPENED);
                        }
                    }
                        break;

                }       
            }
        } catch(Throwable te) {
            te.printStackTrace();
            logger.error(te.getLocalizedMessage(), te);
            if(respMessage.getAttributeString(BillingMessage.STATUS_CODE) == null){
                respMessage.setStatusCode(StatusDictionary.TRANSACTION_OPENED);
            }
            respMessage.setAttributeString(BillingMessage.STATUS_MESSAGE, te.getLocalizedMessage());
        }
    }

    private KyivstarResponse beginTransaction(String url,
                                              String userName,
                                              String password,
                                              String msisdn,
                                              String payAccount,
                                              double payAmount,
                                              String tradePoint,
                                              String branch,
                                              int sourceType)
            throws HttpsGateException,HttpsRequestException{
        GetMethod get = new GetMethod(url);

        if(msisdn != null){
            get.setQueryString(new NameValuePair[]{
                    new NameValuePair("ACT", "0"),
                    new NameValuePair("USERNAME", userName),
                    new NameValuePair("PASSWORD", password),
                    new NameValuePair("MSISDN", msisdn),
                    new NameValuePair("PAY_AMOUNT", Double.toString(payAmount)),
                    new NameValuePair("BRANCH", branch),
                    new NameValuePair("SOURCE_TYPE", Integer.toString(sourceType)),
                    new NameValuePair("TRADE_POINT", tradePoint)
            });
        } else if(payAccount != null){
            // 18.10.2010 Proximan
        //    if(payAccount.charAt(0) != '0')
        //    {
            //
                get.setQueryString(new NameValuePair[]{
                        new NameValuePair("ACT", "0"),
                        new NameValuePair("USERNAME", userName),
                        new NameValuePair("PASSWORD", password),
                        new NameValuePair("PAY_ACCOUNT", payAccount),
                        new NameValuePair("PAY_AMOUNT", Double.toString(payAmount)),
                        new NameValuePair("BRANCH", branch),
                        new NameValuePair("SOURCE_TYPE", Integer.toString(sourceType)),
                        new NameValuePair("TRADE_POINT", tradePoint)
                });
            // 18.10.2010 Proximan
         //   } else throw new HttpsGateException("PayAccount is not equal to KSGate");
            //


        } else throw new HttpsGateException("PayAccount and MSISDN is null");
        return makeRequest(get);
    }

    private KyivstarResponse queryTransactionStatus(String url,
                                                    String userName,
                                                    String password,
                                                    long payId)
            throws HttpsRequestException{
        GetMethod get = new GetMethod(url);

        get.setQueryString(new NameValuePair[]{
                new NameValuePair("ACT", "3"),
                new NameValuePair("USERNAME", userName),
                new NameValuePair("PASSWORD", password),
                new NameValuePair("PAY_ID", Long.toString(payId))
        });

        return makeRequest(get);
    }

    private KyivstarResponse queryPaymentTransaction(String url,
                                                     String userName,
                                                     String password,
                                                     long payId)
            throws HttpsRequestException{
        GetMethod get = new GetMethod(url);

        get.setQueryString(new NameValuePair[]{
                new NameValuePair("ACT", "4"),
                new NameValuePair("USERNAME", userName),
                new NameValuePair("PASSWORD", password),
                new NameValuePair("PAY_ID", Long.toString(payId))
        });

        return makeRequest(get);
    }

    private KyivstarResponse submitPaymentTransaction(String url,
                                                      String userName,
                                                      String password,
                                                      long payId)
            throws HttpsRequestException {
        GetMethod get = new GetMethod(url);

        get.setQueryString(new NameValuePair[]{
                new NameValuePair("ACT", "1"),
                new NameValuePair("USERNAME", userName),
                new NameValuePair("PASSWORD", password),
                new NameValuePair("PAY_ID", Long.toString(payId))
        });
        return makeRequest(get);
    }

    private KyivstarResponse cancelPaymentTransaction(String url,
                                                      String userName,
                                                      String password,
                                                      long payId)
            throws HttpsRequestException{
        GetMethod get = new GetMethod(url);

        get.setQueryString(new NameValuePair[]{
                new NameValuePair("ACT", "2"),
                new NameValuePair("USERNAME", userName),
                new NameValuePair("PASSWORD", password),
                new NameValuePair("PAY_ID", Long.toString(payId))
        });

        return makeRequest(get);
    }

    private KyivstarResponse changePassword(String url,
                                            String userName,
                                            String password,
                                            String newPassword)
            throws HttpsRequestException{
        GetMethod get = new GetMethod(url);

        get.setQueryString(new NameValuePair[]{
                new NameValuePair("ACT", "5"),
                new NameValuePair("USERNAME", userName),
                new NameValuePair("PASSWORD", password),
                new NameValuePair("NEWPASSWORD", newPassword)
        });
        return makeRequest(get);
    }

    private KyivstarResponse getBallance(String url,
                                            String userName,
                                            String password)
            throws HttpsRequestException{
        GetMethod get = new GetMethod(url);

        get.setQueryString(new NameValuePair[]{
                new NameValuePair("ACT", "8"),
                new NameValuePair("USERNAME", userName),
                new NameValuePair("PASSWORD", password)
        });
        return makeRequest(get);
    }

    private KyivstarResponse paymentAnnulment(String url,
                                              String userName,
                                              String password,
                                              long payId,
                                              String msisdn,
                                              String payAccount,
                                              long receiptNum,
                                              double payAmount)
            throws HttpsGateException,HttpsRequestException{
        GetMethod get = new GetMethod(url);

        if(msisdn != null){
            get.setQueryString(new NameValuePair[]{
                    new NameValuePair("ACT", "6"),
                    new NameValuePair("USERNAME", userName),
                    new NameValuePair("PASSWORD", password),
                    new NameValuePair("MSISDN", msisdn),
                    new NameValuePair("PAY_AMOUNT", Double.toString(payAmount)),
                    new NameValuePair("PAY_ID", Long.toString(payId)),
                    new NameValuePair("RECEIPT_NUM", Long.toString(receiptNum))
            });
        }else if(payAccount != null){
            // 18.10.2010 Proximan
        //    if(payAccount.charAt(0) != '0')
        //    {
            //
                get.setQueryString(new NameValuePair[]{
                        new NameValuePair("ACT", "6"),
                        new NameValuePair("USERNAME", userName),
                        new NameValuePair("PASSWORD", password),
                        new NameValuePair("PAY_ACCOUNT", payAccount),
                        new NameValuePair("PAY_AMOUNT", Double.toString(payAmount)),
                        new NameValuePair("PAY_ID", Long.toString(payId)),
                        new NameValuePair("RECEIPT_NUM", Long.toString(receiptNum))
                });
            // 18.10.2010 Proximan
         //   } else throw new HttpsGateException("PayAccount is not equal to KSGate");
            //
        }else throw new HttpsGateException("PayAccount and MSISDN is null");
        return makeRequest(get);
    }

    private KyivstarResponse getAbonentInfo(String url,
                                            String userName,
                                            String password,
                                            String msisdn,
                                            String payAccount)
            throws HttpsGateException,HttpsRequestException{
        GetMethod get = new GetMethod(url);

        if(msisdn != null){
            get.setQueryString(new NameValuePair[]{
                    new NameValuePair("ACT", "7"),
                    new NameValuePair("USERNAME", userName),
                    new NameValuePair("PASSWORD", password),
                    new NameValuePair("MSISDN", msisdn)
            });
        }else if(payAccount != null){
            // 18.10.2010 Proximan
         //   if(payAccount.charAt(0) != '0')
         //   {
            //
                get.setQueryString(new NameValuePair[]{
                        new NameValuePair("ACT", "7"),
                        new NameValuePair("USERNAME", userName),
                        new NameValuePair("PASSWORD", password),
                        new NameValuePair("PAY_ACCOUNT", payAccount)
                });
            // 18.10.2010 Proximan
         //   } else throw new HttpsGateException("PayAccount is not equal to KSGate");
            //
        }else throw new HttpsGateException("PayAccount and MSISDN is null");
        return makeRequest(get);
    }

    static int cnt = 0;

    private KyivstarResponse makeRequest(HttpMethod method)throws HttpsRequestException {
        KyivstarResponse response = null;
        try{
            final InetAddress localAddress = InetAddress.getByName(properties.getProperty(PROP_LOCAL_ADDRESS, "localhost"));
            final HostConfiguration hostConfiguration = new HostConfiguration();
            hostConfiguration.setLocalAddress(localAddress);
            final HttpClient httpClient = new HttpClient();
            httpClient.setHostConfiguration(hostConfiguration);

            //setup query retry handler 
            final int maxAttemptsCount = Integer.parseInt(properties.getProperty(PROP_MAX_HTTP_ATTEMPTS_CNT, "1"));
            method.getParams().setParameter(HttpMethodParams.RETRY_HANDLER, new DefaultHttpMethodRetryHandler(maxAttemptsCount, false));

            logger.info(method.getQueryString()
                    .replaceFirst("PASSWORD=([^&]+)", "PASSWORD=*")
                    .replaceFirst("NEWPASSWORD=([^&]+)", "NEWPASSWORD=*"));

            Protocol.registerProtocol("https", new Protocol("https", (ProtocolSocketFactory)new EasySSLProtocolSocketFactory(), 443));
            httpClient.executeMethod(method);

            if(method.getStatusCode() != HttpStatus.SC_OK){
                throw new HttpsGateException("Incorrect response code from server " + method.getStatusCode());
            }
            String strResp = method.getResponseBodyAsString();

            logger.info(strResp);

            response = KyivstarResponse.parseResponse(strResp);
        }catch(BadResponseException e){
            logger.error(e.getLocalizedMessage(), e);
            throw new HttpsRequestException("XML exception " + e, e);
        }catch(IOException e){
            logger.error(e.getLocalizedMessage(), e);
            throw new HttpsRequestException("IO exception " + e, e);
        }catch(Throwable e){
            e.printStackTrace();
            logger.error(e.getLocalizedMessage(), e);
            throw new HttpsRequestException("Throwable exception " + e, e);
        }finally{
            method.releaseConnection();
        }

        return response;
    }


    public void test1() throws Throwable{
        KyivstarResponse resp = queryTransactionStatus(properties.getProperty(PROP_URL),
                properties.getProperty(PROP_USERNAME),
                properties.getProperty(PROP_PASSWORD),
                6693834);
        System.out.print(resp);


    }

    public Integer getTxStatus (MobipayKyivstar mobipayKyivstar) throws GateException {
        Integer txStatus;
        KyivstarResponse response = getPayResponse(mobipayKyivstar, null);
        if (response == null) {
            txStatus = 22;
        } else {
            txStatus = response.getPayStatus().intValue();
            //txStatus = Integer.parseInt(response.getPayStatus());
        }
        return txStatus;
    }



    /**
     * Sends HTTP query to Kyivstar gate, and returns resoult of tx
     * @param mobipayKyivstar
     * @return
     * @throws GateException
     */
    public KyivstarResponse getPayResponse(MobipayKyivstar mobipayKyivstar, String billTransactionNum) throws GateException {
        KyivstarResponse payResponse = null;
        if (mobipayKyivstar.getPayId() == null) {
            throw new GateException("err.pay_ID_is_null");
        }
        Long payId = mobipayKyivstar.getPayId();
        payResponse = getPayResponse(payId, null);

        updateTransactionsStatus(payResponse, billTransactionNum);

        return payResponse;
    }

    public KyivstarResponse getPayResponse(Long payId, String billTransactionNum) throws HttpsRequestException {
        KyivstarResponse payResponse = null;
        payResponse = queryTransactionStatus(properties.getProperty(PROP_URL),
                                               properties.getProperty(PROP_USERNAME),
                                               properties.getProperty(PROP_PASSWORD),
                                               payId);

        updateTransactionsStatus(payResponse, billTransactionNum);

        return payResponse;
    }


    private void updateTransactionsStatus(KyivstarResponse payResponse, String billTransactionNum)
    {
        MobipayKyivstar kyivstarTransaction = null;
        ClientTransaction clientTransaction = null;
        OperatorBalanceTransaction operatorTransaction = null;
        List<SellerBalanceTransaction> sellerTransactions = null;
        TerminalBalanceTransaction terminalTransaction = null;

        EntityManager entityManager = null;
        try {
            entityManager = new EntityManager();

            CriterionWrapper criterions_1[] = new CriterionWrapper[]{Restrictions.eq("bankPayId", billTransactionNum)};
            CriterionWrapper criterions_2[] = new CriterionWrapper[]{Restrictions.eq("transactionGUID", billTransactionNum)};
            CriterionWrapper criterions_3[] = new CriterionWrapper[]{Restrictions.eq("transactionGUID", billTransactionNum)};
            CriterionWrapper criterions_4[] = new CriterionWrapper[]{Restrictions.eq("transactionGUID", billTransactionNum)};
            CriterionWrapper criterions_5[] = new CriterionWrapper[]{Restrictions.eq("transactionGUID", billTransactionNum)};

            kyivstarTransaction = (MobipayKyivstar) entityManager.RETRIEVE(MobipayKyivstar.class, null, criterions_1, null);
            clientTransaction = (ClientTransaction) entityManager.RETRIEVE(ClientTransaction.class, null, criterions_2, null);
            operatorTransaction = (OperatorBalanceTransaction) entityManager.RETRIEVE(OperatorBalanceTransaction.class, null, criterions_3, null);
            sellerTransactions = entityManager.RETRIEVE_ALL(SellerBalanceTransaction.class, criterions_4, null);
            terminalTransaction = (TerminalBalanceTransaction) entityManager.RETRIEVE(TerminalBalanceTransaction.class, null, criterions_5, null);

        } catch (DatabaseException e) {
            logger.error("Error of load previousTransaction 1", e);
            e.printStackTrace();
        } catch (Exception ee) {
            logger.error("Error of load previousTransaction 2", ee);
            ee.printStackTrace();
        }

        Integer status = 201;
        if(payResponse.getPayStatus() == 111) status = 0;
        else if(payResponse.getPayStatus() == 101 ||
                payResponse.getPayStatus() == 102 ||
                payResponse.getPayStatus() == 110 ||
                payResponse.getPayStatus() == 112 ||
                payResponse.getPayStatus() == 120) status = 1;
        else status = 201;

        try
        {
            if(kyivstarTransaction != null)
            {
                kyivstarTransaction.setStatusCode(status);
                kyivstarTransaction.setStatusCodeInt(status);
                entityManager.SAVE(kyivstarTransaction);
            }
            if(clientTransaction != null)
            {
                clientTransaction.setStatus(status+"");
                entityManager.SAVE(clientTransaction);
            }
            if(operatorTransaction != null)
            {
                operatorTransaction.setStatus(status+"");
                entityManager.SAVE(operatorTransaction);
            }
            if(terminalTransaction != null)
            {
                terminalTransaction.setStatus(status+"");
                entityManager.SAVE(terminalTransaction);
            }
            if(sellerTransactions != null)
            {
                for(SellerBalanceTransaction sellerTransaction : sellerTransactions)
                {
                    if(sellerTransaction != null)
                    {
                        sellerTransaction.setStatus(status+"");
                        entityManager.SAVE(sellerTransaction);
                    }
                }
            }

        } catch (DatabaseException e) {
            logger.error("Error of load previousTransaction 1", e);
            e.printStackTrace();
        } catch (Exception ee) {
            logger.error("Error of load previousTransaction 2", ee);
            ee.printStackTrace();
        }


    }



}
            
