package com.tmx.web.actions.core.dictionary.user;


import com.tmx.web.base.BasicDispatchedAction;
import com.tmx.web.forms.core.dictionary.user.ManageUsersForm;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class ManageUsersAction extends BasicDispatchedAction {
    private static final String FORWARD_MANAGE_USERS_PAGE = "manageUsersPage";

    public ActionForward defaultAction(ActionMapping mapping,
                                       ActionForm form,
                                       HttpServletRequest request,
                                       HttpServletResponse response,
                                       String methodName) throws Throwable {

        ManageUsersForm manageUsersForm = (ManageUsersForm) verifyForm(form, ManageUsersForm.class);
        manageUsersForm.getUsersTable().setRefreshActionName(this.getName());
        manageUsersForm.getUsersTable().refresh();
        return mapping.findForward(FORWARD_MANAGE_USERS_PAGE);
    }
}
