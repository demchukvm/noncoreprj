package com.tmx.web.forms.core.dictionary.news;

import com.tmx.web.base.BasicActionForm;
import com.tmx.web.controls.table.ControlManagerTable;
import com.tmx.web.controls.table.Filter;

public class NewsForm extends BasicActionForm
{
    private ControlManagerTable newsMessagesTable;

    public void init()
    {
        newsMessagesTable = new ControlManagerTable("query(query_type=retrieve_all," +
                "entity_class=com.tmx.as.entities.bill.messages.Messages," +
                "order(name=dateStart,type=desc)," +
                "page_size=5" +
                ")");

        newsMessagesTable.setRefreshActionName("/NewsAction");
        newsMessagesTable.setRefreshCommandName("defaultAction");


//        Filter notEmptyTitle = new Filter("not_empty_title");
//        newsMessagesTable.getFilterSet().addFilter(notEmptyTitle);
//        Filter byObservers = new Filter("by_observers");
//        newsMessagesTable.getFilterSet().addFilter(byObservers);
    }

    public ControlManagerTable getNewsMessagesTable() {
        return newsMessagesTable;
    }

    public void setNewsMessagesTable(ControlManagerTable newsMessagesTable) {
        this.newsMessagesTable = newsMessagesTable;
    }
}