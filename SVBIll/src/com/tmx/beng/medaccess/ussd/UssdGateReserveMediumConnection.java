package com.tmx.beng.medaccess.ussd;

import com.tmx.beng.ussdgate.UssdGate;
import com.tmx.beng.ussdgate.UssdGateException;
import com.tmx.beng.base.BillingMessage;
import com.tmx.beng.medaccess.MediumConnection;
import org.apache.log4j.Logger;

/**
 * TODO!!! Rube rewrite this
 * @author Andrey Nagorniy
 */
public class UssdGateReserveMediumConnection implements MediumConnection {

    private UssdGate ussdGate = null;
    private Logger logger = Logger.getLogger("ussdgate."+getClass());

    UssdGateReserveMediumConnection() throws UssdGateException {
        ussdGate = UssdGate.getInstance( "ussd_gate_reserve" );
    }

    public void send(BillingMessage msg) {
        ussdGate.enqueueBillMessage(msg);
    }

}
