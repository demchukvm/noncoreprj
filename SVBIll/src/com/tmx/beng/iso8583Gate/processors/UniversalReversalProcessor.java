package com.tmx.beng.iso8583Gate.processors;

import com.tmx.beng.iso8583Gate.pdu.RuntimeMessage;
import com.tmx.beng.iso8583Gate.pdu.IncorrectFormatException;
import com.tmx.beng.iso8583Gate.mapper.BillingMapper;
import com.tmx.beng.iso8583Gate.mapper.BillingMapperMetadata;
import com.tmx.beng.base.BillException;
import com.tmx.beng.base.BillingMessageWritable;
import com.tmx.beng.base.BillingMessageImpl;
import com.tmx.beng.base.BillingMessage;
import com.tmx.util.InitException;
import com.tmx.as.exceptions.DatabaseException;
import com.tmx.as.entities.iso8583.transaction.Iso8583Transaction;
import com.tmx.as.base.EntityManager;
import com.tmx.as.base.FilterWrapper;

/**

 */
public class UniversalReversalProcessor extends BasicProcessor{

    public int getMessageTypeId() {
        return 410;
    }

    public int getProcessingCode() {
        return 0;//will be overriden in getDEToCopy() 
    }

    public int[] getDEToCopy() {
        return new int[]{2, 3, 4, 11, 14, 24, 41, 42};
    }

    public void processSpecificTask(RuntimeMessage reqMsg, RuntimeMessage respMsg)
            throws ProcessingException, BillException, IncorrectFormatException, InitException, DatabaseException {

        logger.debug("Universal reversal");
        BillingMapper billingMapper = BillingMapperMetadata.getInstance().getBillMapper(reqMsg);

        //prepare and pre-save iso transaction in iso datasource
        Iso8583Transaction tx = billingMapper.preSaveTransaction(reqMsg);

        //prepare billing message and send it to process into billing engine
        BillingMessageWritable reqBM = new BillingMessageImpl();
        billingMapper.prepare0400Msg(reqMsg, reqBM);

        BillingMessage respBM = getConnection().processSync(reqBM);

        //post-save iso transaction in iso datasource
        billingMapper.postSaveTransaction(tx, respBM);
        updateSellTransaction(tx);

        //prepare iso response
        billingMapper.prepare0410Msg(respMsg, respBM, tx);
    }

    private void updateSellTransaction(Iso8583Transaction revTx) throws DatabaseException{
        EntityManager em = new EntityManager();
        FilterWrapper byMessageTypeIdFW = new FilterWrapper("by_msgtypeid");
        byMessageTypeIdFW.setParameter("msgtypeid", new Long(200));
        FilterWrapper byProcessingCodeFW = new FilterWrapper("by_processingcode");
        byProcessingCodeFW.setParameter("proccode", new Long(0));        
        FilterWrapper bySystemTraceNumFW = new FilterWrapper("by_systemtracenum");
        bySystemTraceNumFW.setParameter("systracenum", revTx.getSystemsTraceNo());
        FilterWrapper byTerminalIdFW = new FilterWrapper("by_isoterminalid");
        byTerminalIdFW.setParameter("terminalid", revTx.getTerminalId());
        FilterWrapper byMerchantIdFW = new FilterWrapper("by_merchantid");
        byMerchantIdFW.setParameter("merchantid", revTx.getCardAcqId());
        FilterWrapper byIssuerTransDataFW = new FilterWrapper("by_isstransdata");
        byIssuerTransDataFW.setParameter("isstransdata", revTx.getIssuerTransportData());
        FilterWrapper byAmountFW = new FilterWrapper("by_amount");
        byAmountFW.setParameter("amount", revTx.getAmount());        

        Iso8583Transaction finTx = (Iso8583Transaction)em.RETRIEVE(
                Iso8583Transaction.class,
                new FilterWrapper[]{
                        byMessageTypeIdFW, byProcessingCodeFW, bySystemTraceNumFW, byTerminalIdFW,
                        byMerchantIdFW, byIssuerTransDataFW, byAmountFW
                },
                null);

        if(finTx != null){
            finTx.setReversed(true);
            em.SAVE(finTx);            
        }
    }
}