package com.tmx.web.actions.core.transactions.seller_balance_transaction;

import com.tmx.web.base.BasicDispatchedAction;
import com.tmx.web.forms.core.transaction.seller_balance_transaction.InfoSellerBalanceTransactionForm;
import com.tmx.as.blogic.TransactionManager;
import com.tmx.as.entities.bill.transaction.SellerBalanceTransaction;
import com.tmx.as.entities.bill.transaction.TransactionSupport;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForm;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class InfoSellerBalanceTransactionAction extends BasicDispatchedAction {

     private static final String FORWARD_INFO_SELLER_TRANSACTION_PAGE = "infoSellerTransactionPage";

    public ActionForward defaultAction(ActionMapping mapping,
                                       ActionForm form,
                                       HttpServletRequest request,
                                       HttpServletResponse response,
                                       String methodName) throws Throwable {
        InfoSellerBalanceTransactionForm infoSellerBalanceTransactionForm =
                (InfoSellerBalanceTransactionForm) verifyForm(form, InfoSellerBalanceTransactionForm.class);

        infoSellerBalanceTransactionForm.reset();
        TransactionManager transactionManager = new TransactionManager();

        SellerBalanceTransaction sellerBalanceTransaction =
                transactionManager.getSellerBalanceTransaction(infoSellerBalanceTransactionForm.getSelectedTransactionId());

        infoSellerBalanceTransactionForm.bindData(sellerBalanceTransaction);

        return mapping.findForward(FORWARD_INFO_SELLER_TRANSACTION_PAGE);
    }

    public ActionForward preloadForCancel(ActionMapping mapping,
                                       ActionForm form,
                                       HttpServletRequest request,
                                       HttpServletResponse response) throws Throwable {
        return mapping.findForward((String) getRoadmap().getParameterValue(this.getName(), "onCancelAction", true));
    }

    public ActionForward preloadForSave(ActionMapping mapping,
                                       ActionForm form,
                                       HttpServletRequest request,
                                       HttpServletResponse response) throws Throwable {
        InfoSellerBalanceTransactionForm infoSellerBalanceTransactionForm =
                (InfoSellerBalanceTransactionForm) verifyForm(form, InfoSellerBalanceTransactionForm.class);

        TransactionManager transactionManager = new TransactionManager();

        TransactionSupport transactionSupport = infoSellerBalanceTransactionForm.populateTransactionSupport();

        transactionManager.SAVE(transactionSupport);

        return mapping.findForward(FORWARD_INFO_SELLER_TRANSACTION_PAGE);
    }

}
