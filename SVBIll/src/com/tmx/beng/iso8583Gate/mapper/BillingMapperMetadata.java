package com.tmx.beng.iso8583Gate.mapper;

import com.tmx.beng.iso8583Gate.pdu.RuntimeMessage;
import com.tmx.beng.iso8583Gate.ISO8583Service;
import com.tmx.as.base.Reconfigurable;
import com.tmx.util.InitException;
import com.tmx.util.Configuration;

import java.util.*;
import java.io.File;

import org.apache.commons.digester.Digester;
import org.apache.log4j.Logger;

/**

 */
public class BillingMapperMetadata implements Reconfigurable {
    private Logger logger = ISO8583Service.getLogger(this.getClass());
    /** Basic mapping */
    private Mapping mapping;
    /** Extended mapping (over basic mapping) */
    private ExtMapping extMapping;
    private static BillingMapperMetadata billingMapperMetadata = null;
    private boolean reloaded = false;

    public static BillingMapperMetadata getInstance(){
        if(billingMapperMetadata != null)
            return billingMapperMetadata;
        billingMapperMetadata = new BillingMapperMetadata();
        return billingMapperMetadata;
    }

    public BillingMapper getBillMapper(RuntimeMessage reqMsg) throws InitException{
        if(!reloaded)
            throw new InitException("err.iso8583_billing_helper.mapping_not_loaded_yet");
        RequestMapping reqMaping = getRequestMapping(reqMsg);
        return  (reqMaping.getUseExtendedMapping() != null &&
                Boolean.TRUE.equals(Boolean.valueOf(reqMaping.getUseExtendedMapping()))) ?

                new ExtendedBillingMapper(
                reqMaping,
                getParameterMapping(reqMaping.getProtocolVersion()),
                getErrorMapping(reqMaping.getProtocolVersion()),
                getAmountDenomination(reqMaping.getProtocolVersion()),
                getCrossParamMapping(reqMaping.getProtocolVersion())) :

                new BasicBillingMapper(
                reqMaping, 
                getParameterMapping(reqMaping.getProtocolVersion()),
                getErrorMapping(reqMaping.getProtocolVersion()),
                getAmountDenomination(reqMaping.getProtocolVersion()));

    }

    public void reload() throws InitException {
        reloadBasic0();
        reloadExtended0();
        reloaded = true;
    }

    private void reloadBasic0() throws InitException {
        try {
            logger.debug("Start basic initialization of the Billing mapper");
            String basicMappingFile = Configuration.getInstance().getProperty("iso8583service.mapping_file");
            if (basicMappingFile == null)
                throw new InitException("err.iso8583_billing_mapper.basic_mapping_file_is_not_specified");

            File configFile = new File(basicMappingFile);
            Digester d = new Digester();
            d.addObjectCreate("billingMapping", Mapping.class);

            d.addObjectCreate("billingMapping/requestMappings/requestMapping", RequestMapping.class);
            d.addSetProperties("billingMapping/requestMappings/requestMapping");
            d.addSetNext("billingMapping/requestMappings/requestMapping", "addRequestMapping");

            d.addObjectCreate("billingMapping/parameterMappings/parameterMapping", ParameterMapping.class);
            d.addSetProperties("billingMapping/parameterMappings/parameterMapping");
            d.addObjectCreate("billingMapping/parameterMappings/parameterMapping/param", Param.class);
            d.addSetProperties("billingMapping/parameterMappings/parameterMapping/param");
            d.addCallMethod("billingMapping/parameterMappings/parameterMapping/param/value", "addValueMapping", 2);
            d.addCallParam("billingMapping/parameterMappings/parameterMapping/param/value", 0, "originalValue");
            d.addCallParam("billingMapping/parameterMappings/parameterMapping/param/value", 1, "mappedValue");
            d.addSetNext("billingMapping/parameterMappings/parameterMapping/param", "addParam");
            d.addSetNext("billingMapping/parameterMappings/parameterMapping", "addParameterMapping");

            d.addObjectCreate("billingMapping/errorMappings/errorMapping", ErrorMapping.class);
            d.addSetProperties("billingMapping/errorMappings/errorMapping");
            d.addObjectCreate("billingMapping/errorMappings/errorMapping/error", Error.class);
            d.addSetProperties("billingMapping/errorMappings/errorMapping/error");
            d.addSetNext("billingMapping/errorMappings/errorMapping/error", "addError");
            d.addSetNext("billingMapping/errorMappings/errorMapping", "addErrorMapping");

            d.addObjectCreate("billingMapping/amountDenominations/amountDenomination", AmountDenomination.class);
            d.addSetProperties("billingMapping/amountDenominations/amountDenomination");
            d.addSetNext("billingMapping/amountDenominations/amountDenomination", "addAmountDenomination");

            d.parse(configFile);
            Mapping localMapping = (Mapping)d.getRoot();
            logger.debug("Successful completed basic initialization of the Billing mapper");
            //apply for global variable
            mapping = localMapping;
        }
        catch (InitException e) {
            //rethrow
            throw e;
        }
        catch (Exception e) {
            throw new InitException("err.iso8583_billing_mapper.basic_reload_failed", e);
        }
    }

    private void reloadExtended0() throws InitException{
        try {
            logger.debug("Start extended initialization of the Billing mapper");
            String extMappingFile = Configuration.getInstance().getProperty("iso8583service.mapping_file_ext");
            if (extMappingFile == null)
                throw new InitException("err.iso8583_billing_helper.extended_mapping_file_is_not_specified");

            File configFile = new File(extMappingFile);
            Digester d = new Digester();
            d.addObjectCreate("extendedBillMapping", ExtMapping.class);

            d.addObjectCreate("extendedBillMapping/crossParameterMappings/crossParameterMapping", CrossParameterMapping.class);
            d.addSetProperties("extendedBillMapping/crossParameterMappings/crossParameterMapping");
            d.addObjectCreate("extendedBillMapping/crossParameterMappings/crossParameterMapping/crossParam", CrossParam.class);
            d.addSetProperties("extendedBillMapping/crossParameterMappings/crossParameterMapping/crossParam");
            d.addCallMethod("extendedBillMapping/crossParameterMappings/crossParameterMapping/crossParam/value", "addValueMapping", 2);
            d.addCallParam("extendedBillMapping/crossParameterMappings/crossParameterMapping/crossParam/value", 0, "originalValue");
            d.addCallParam("extendedBillMapping/crossParameterMappings/crossParameterMapping/crossParam/value", 1, "mappedValue");
            d.addSetNext("extendedBillMapping/crossParameterMappings/crossParameterMapping/crossParam", "addCrossParam");
            d.addSetNext("extendedBillMapping/crossParameterMappings/crossParameterMapping", "addCrossParamMapping");

            d.parse(configFile);
            ExtMapping localMapping = (ExtMapping)d.getRoot();
            logger.debug("Successful completed extended initialization of the Billing mapper");
            //apply for global variable
            extMapping = localMapping;
        }
        catch (InitException e) {
            //rethrow
            throw e;
        }
        catch (Exception e) {
            throw new InitException("err.iso8583_billing_mapper.extended_reload_failed", e);
        }
    }

    private RequestMapping getRequestMapping(RuntimeMessage reqMsg) throws InitException{
        String termianalSn = reqMsg.getRuntimeDataElement(41).getString();
        for(Iterator iter = mapping.getRequestMappings().iterator(); iter.hasNext();){
            RequestMapping rm = (RequestMapping)iter.next();

            if(rm.getPattern().matcher(termianalSn).matches())
                return rm;
        }
        throw new InitException("err.iso8583_billing_helper.no_mapping_for_terminal_sn", new String[]{termianalSn});
    }

    private ParameterMapping getParameterMapping(String protocolVersion){
        return mapping.getParameterMapping(protocolVersion);
    }

    private ErrorMapping getErrorMapping(String protocolVersion){
        return mapping.getErrorMapping(protocolVersion);
    }

    private AmountDenomination getAmountDenomination(String protocolVersion){
        return mapping.getAmountDenomination(protocolVersion);
    }

    private CrossParameterMapping getCrossParamMapping(String protocolVersion){
        return extMapping.getCrossParamMapping(protocolVersion);
    }

}
