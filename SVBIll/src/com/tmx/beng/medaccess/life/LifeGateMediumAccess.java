package com.tmx.beng.medaccess.life;

import com.tmx.beng.medaccess.MediumAccess;
import com.tmx.beng.medaccess.MediumConnection;
import com.tmx.beng.medaccess.GateException;

/**
  Medium Access to obtain UMC medium connection
 */
public class LifeGateMediumAccess implements MediumAccess {

    public void init() {
    }

    public MediumConnection getConnection() throws GateException {
        return new LifeGateMediumConnection();
    }
}