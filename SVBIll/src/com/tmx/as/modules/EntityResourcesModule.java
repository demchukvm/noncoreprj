package com.tmx.as.modules;

import com.tmx.beng.base.BillingEngine;
import com.tmx.util.InitException;
import com.tmx.as.base.EntityResources;

import java.util.Properties;

/**
 * Created by IntelliJ IDEA.
 * User: shake
 * Date: 25.06.2007
 * Time: 15:56:19
 */
public class EntityResourcesModule extends AbstractModule{
    private static EntityResources entityResources = null;

    public void start(Properties props)throws ModuleException{
        try{
            entityResources = new EntityResources();
            entityResources.reload();
        }catch(InitException e){
            throw new ModuleException(e.getLocalizedMessage());
        }
    }

    public void shutdownGraceful(Properties props)throws ModuleException{
    }

    public void shutdownAbort(Properties props)throws ModuleException{
    }

    public static EntityResources getEntityResources() {
        return entityResources;
    }
}
