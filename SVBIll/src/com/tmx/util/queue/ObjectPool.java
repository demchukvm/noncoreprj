package com.tmx.util.queue;

import java.io.PrintStream;
import java.io.ByteArrayOutputStream;
import java.sql.Timestamp;

/**
 * This Object array based pool implementation is used by applications for
 * pooling simple constructor objects.
 */
public class ObjectPool {
    /**
     * Objects array.
     */
    protected Object objects[];
    protected ObjectInPoolDescription[] desc;

    private static class ObjectInPoolDescription {
        Object obj;
        java.sql.Timestamp time;
        String stack;

        public ObjectInPoolDescription(Object obj) {
            this.obj = obj;
            time = new Timestamp(System.currentTimeMillis());
            stack = exceptionToString(new Exception("Stack"));
        }

        public String toString() {
            return "Obj: " + obj + " (" + obj.getClass() + ')' + ", Time: " + time + "\nstack=" + stack;
        }
    }

    private static String exceptionToString(Throwable ex) {
        if (ex == null) {
            return "";
        }
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        PrintStream ps = new PrintStream(os);
        ex.printStackTrace(ps);
        String ret = os.toString();
        try {
            ps.close();
        } catch (Exception ignore) {
        }
        try {
            os.close();
        } catch (Exception ignore) {
        }
        return ret;
    }

    /**
     * Number of objects in pool.
     */
    protected int head;
    /**
     * Statistic counter
     */
    protected int peak;
    /**
     * Statistic counter
     */
    public int returned;
    /**
     * Statistic counter
     */
    public int created;
    /**
     * Statistic counter
     */
    public int asked;
    /**
     * Statistic counter
     */
    public int alive;

    public int getReturned() {
        return returned;
    }

    public void setReturned(int returned) {
        this.returned = returned;
    }

    public int getCreated() {
        return created;
    }

    public void setCreated(int created) {
        this.created = created;
    }

    public int getAsked() {
        return asked;
    }

    public void setAsked(int asked) {
        this.asked = asked;
    }

    public int getAlive() {
        return alive;
    }

    public void setAlive(int alive) {
        this.alive = alive;
    }

    public synchronized void decrementAlive() {
        this.alive--;
        this.returned--;
    }

    /**
     * Interface is used for creation and initialization of new pool objects.
     * Constructor of pooled object has to be empty.
     */
    private ObjectPoolElement poolObjectsInitializer;
    private boolean initialized;
    private String poolName;

    private static boolean NO_RETURNS = Boolean.valueOf(System.getProperty("not_returning_object_pools", "false")).booleanValue();

    public ObjectPool(String poolName, ObjectPoolElement objectInitializer) {
        this(poolName, 0, objectInitializer);
    }

    /**
     * ObjectPool constructor.
     *
     * @param size              initial pool capacity
     * @param objectInitializer is used to create/initialize pooled objects.
     */
    public ObjectPool(String poolName, int size, ObjectPoolElement objectInitializer) {
        this(poolName, size, Integer.MAX_VALUE, objectInitializer);
    }

    public ObjectPool(String poolName, int size, int maxSize, ObjectPoolElement objectInitializer) {
        if (size < 0) {
            throw new IllegalArgumentException("Invalid size argument");
        }
        this.poolName = poolName;
        setMaxPoolSize(maxSize);
        this.poolObjectsInitializer = objectInitializer;
        initialized = false;
        peak = 0;
        head = 0;
        returned = 0;
        alive = 0;
        created = 0;
        asked = 0;
        objects = new Object[size > 0 ? size : 16];
//        desc = new ObjectInPoolDescription[size];
        init(size);
    }

    /**
     * ObjectPool initialization. Allows to fill pool with pooled object instances to avoid
     * expensive creation routine during performance critical pool use.
     */
    public ObjectPool init() {
        return init(getCapacity());
    }

    /**
     * ObjectPool initialization. Allows to fill pool with initial amount of
     * pooled object instances to avoid expensive creation routine during
     * performance critical pool use.
     */
    public ObjectPool init(int initialObjects) {
        if (initialObjects <= getCapacity()) {
            synchronized (this) {
                if (!initialized) {
                    for (int i = 0; i < initialObjects; ++i) {
                        objects[i] = poolObjectsInitializer.newPoolObjectInstance();
                        if (desc != null) {
                            desc[i] = new ObjectInPoolDescription(objects[i]);
                        }
                        head = i + 1;
                        created++;
                    }
                    initialized = true;
                }
            }
        }
        return this;
    }

    /**
     * This method has to be used to get Pooled object from pool. If no objects found in pool
     * then new one is created.
     */
    public Object getObject() {
        if (!NO_RETURNS) {
            synchronized (this) {
                if (head > 0) {
                    Object obj = null;
                    head--;
                    asked++;
                    alive++;
                    obj = objects[head];
                    objects[head] = null;
                    if (desc != null) {
                        desc[head] = null;
                    }
                    return obj;
                } else {
                    created++;
                    asked++;
                    alive++;
                }
            }
        }
        return poolObjectsInitializer.newPoolObjectInstance();
    }

    private boolean isNoReturns;

    public boolean isNoReturns() {
        return isNoReturns;
    }

    public ObjectPool setNoReturns(boolean noReturns) {
        isNoReturns = noReturns;
        return this;
    }

    int maxPoolSize;

    public int getMaxPoolSize() {
        return maxPoolSize;
    }

    public void setMaxPoolSize(int maxPoolSize) {
        this.maxPoolSize = maxPoolSize;
    }

    /**
     * This method has to be used to return object to pool.
     * If pool capacity is reached then pool is expanded first.
     */
    public void returnObject(Object obj) {
        if (obj == null || NO_RETURNS || isNoReturns()) {
            return;
        }
        synchronized (this) {
            if (desc != null) {
                for (int i = 0; i < head; i++) {
                    if (objects[i] == obj) {
                        throw new RuntimeException("Pool already contains an object #" + i + '/' + head + ": " + desc[i] + ", obj=" + new ObjectInPoolDescription(obj));
                    }
                }
            }
            if (objects.length >= maxPoolSize) {
                return;
            }
            if (objects.length <= head) {
                expandPool();
            }
            objects[head] = obj;
            if (desc != null) {
                desc[head] = new ObjectInPoolDescription(objects[head]);
            }
            initialized = true;
            head++;
            returned++;
            alive--;
            peak = peak < head ? head : peak;
        }
    }

    /**
     * Doubles pool capacity.
     */
    private void expandPool() {
        Object newObjectPool[];
        int newSize = objects.length << 1;
        newObjectPool = new Object[newSize <= 0 ? 16 : newSize];
        System.arraycopy(objects, 0, newObjectPool, 0, objects.length);
        objects = newObjectPool;
        //
        if (desc != null) {
            ObjectInPoolDescription[] _desc = new ObjectInPoolDescription[objects.length];
            System.arraycopy(desc, 0, _desc, 0, desc.length);
            desc = _desc;
        }

//        System.out.println(String.valueOf(++expPoolIter)+": expandPool, capacity="+capacity+", poolClassName="+getClass().getName().substring(getClass().getName().lastIndexOf('.')));
    }
//    private static int expPoolIter = 0;

    /**
     * Returns current pool size.
     */
    public int getSize() {
        return head;
    }

    /**
     * Returns ever peak pool size.
     */
    public int getPeak() {
        return peak;
    }

    /**
     * Returns current pool capacity.
     */
    public int getCapacity() {
        return objects == null ? 0 : objects.length;
    }

    public void printStatistics(PrintStream pw) {
        pw.println(new java.sql.Timestamp(System.currentTimeMillis()).toString().substring(0, 19) + "> " +
                poolName + ": \tSize: " + getSize() + "\tPeak: " + getPeak() + "\tCr: " + created + "\tAskd: " + asked + "\tRet: " + returned + "\tAlive: " + alive);
    }
}

