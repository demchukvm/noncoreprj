<%@ page language="java" pageEncoding="UTF-8" contentType="text/html; charset=utf-8" errorPage="/core/jsp/message/jsperror.jsp" isELIgnored="false" %>
<%@ page import="org.apache.struts.action.ActionErrors" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/tmx-controls.tld" prefix="tmx_ctrl" %>

<br/><br/>
    <table border="0" width="100%" height="100%">
        <tr>
            <td valign="center" align="center" class="form_title">
                <bean:message bundle="core.labels" key="core.page.message.accessdenied.title"/>
                <table border="0" class="login_lines">
                    <tr>
                        <td align="center">
                            <!-- Print global errors -->
                            <tmx_ctrl:writeError
                                errorMsgStyleClass="error_msg_div"
                                errorCauseStyleClass="error_cause_div"
                                errorStackStyleClass="error_stack_div"
                                errorLabelStyleClass="error_label_div"
                                imgRollout="/core/images/gray/ctrl/drill_out.gif"
                                imgRollin="/core/images/gray/ctrl/drill_in.gif"/>
                        </td>
                    </tr>
                </table>

                <br/>
                <input type="button" class="button" onclick="javascript:history.back()"
                       value="<bean:message bundle="core.labels" key="core.page.message.accessdenied.button.repeat"/>">
                <input type="button" class="button" onclick="javascript:submitForm('<tmx_ctrl:rewriteUrl href="/core/auth/logout.do"/>')"
                       value="<bean:message bundle="core.labels" key="core.page.message.accessdenied.button.authorization"/>">
            </td>
        </tr>
    </table>
<br/><br/>