package com.tmx.beng.medaccess.globalmoney;

import com.tmx.beng.base.BillingMessage;
import com.tmx.beng.httpsgate.HttpsGateException;
import com.tmx.beng.httpsgate.globalmoney.GlobalMoneyGate;
import com.tmx.beng.medaccess.MediumConnection;

public class GlobalMoneyGateMediumConnection implements MediumConnection {
    private GlobalMoneyGate globalmoneyGate = null;

    GlobalMoneyGateMediumConnection() throws HttpsGateException {
        globalmoneyGate = GlobalMoneyGate.getInstance();
    }

    public void send(BillingMessage msg) {
        globalmoneyGate.enqueueBillMessage(msg);
    }
}