package com.tmx.web.controls.table;

import com.tmx.web.controls.*;

import java.util.Date;


public class Parameter {
    private BasicControl control = null;
    private Object value = null;
    private Filter parent;
    private String name;
    private int id;

    public Parameter(){
    }

    public Parameter(String name){
        this.name = name;
    }

    /** Take value from control  */
    public Parameter(BasicControl control){
        this.control = control;
    }

    /** Take given value */
    public Parameter(Object value){
        this.value = value;
    }

    public Parameter(String name, BasicControl control){
        this.name = name;
        this.control = control;
    }

    public Parameter(String name, Object value){
        this.name = name;
        this.value = value;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Filter getParent() {
        return parent;
    }

    public void setParent(Filter parent) {
        this.parent = parent;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BasicControl getControl() {
        return control;
    }

    public void setControl(BasicControl control) {
        this.control = control;
    }

    public String getPath(){
        StringBuffer sb = new StringBuffer();
        if(parent != null)
            sb.append(parent.getPath()).append(".");
//        sb.append("parameter[").append(id).append("].control");//by idx
        sb.append("parameter(").append(name).append(").control");//by name        
        return sb.toString();
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }

    private String getFormattedValue(){
        StringBuffer sb = new StringBuffer();
        sb.append("parameter(name=").append(name).append(",value=");
        if (value instanceof Long){
            sb.append(value);
            sb.append(",type=long)");
        }
        if (value instanceof String){
            sb.append(value);
            sb.append(",type=string)");
        }
        if (value instanceof Date){
            sb.append(((Date)value).getTime());
            sb.append(",type=timestamp)");
        }
        return sb.toString();
    }

    public String getAppliedParameters(){
        StringBuffer sb = new StringBuffer();

        if(value == null && control == null){
            return null;
        } else
        if(value != null && control == null){
            return getFormattedValue();
        } else

        if(control instanceof CheckBox){
            sb.append("parameter(name=").append(name).append(",value=");
            sb.append(((CheckBox)control).isSelected()).append(",type=Boolean)");
        }
        else if(control instanceof EditBox){
            EditBox editBox = (EditBox)control;
            if(editBox.getValue() != null && !"".equals(editBox.getValue())){
                sb.append("parameter(name=").append(name).append(",value=");
                sb.append(editBox.getValue()).append(",type=String)");
            }
        }
        else if(control instanceof ScrollBox){
            ScrollBox scrollBox = (ScrollBox)control;
            if(scrollBox.getSelectedKey() != null && !ScrollBox.NULL_KEY.equals(scrollBox.getSelectedKey())){
                sb.append("parameter(name=").append(name).append(",value=");
                sb.append(scrollBox.getSelectedKey()).append(",type=long)");
            }
        }
        else if(control instanceof BrowseBox){
            BrowseBox browseBox = (BrowseBox)control;
            if(browseBox.getKey() != null && !"".equals(browseBox.getKey())){
                sb.append("parameter(name=").append(name).append(",value=");
                sb.append(browseBox.getKey()).append(",type=long)");
            }
        }
        else if(control instanceof TimeSelector){
            TimeSelector ts = (TimeSelector)control;
            if(ts.getTime() != null){
                sb.append("parameter(name=").append(name).append(",value=");
                sb.append(ts.getTime().getTime()).append(",type=timestamp)");
            }
        }
        return sb.toString();
    }
}
