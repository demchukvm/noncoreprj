package com.tmx.as.entities.bill.tariff;

import com.tmx.as.entities.bill.balance.OperatorBalance;
import com.tmx.as.entities.bill.operator.Operator;

/**

 */
public class OperatorTariff extends AbstractTariff{
    private Operator operator;
    private Long operatorId;
    private Long operatorBalanceId;
    private OperatorBalance operatorBalance;

    //--------Properties

    public Operator getOperator() {
        return operator;
    }

    public void setOperator(Operator operator) {
        this.operator = operator;
    }

    public Long getOperatorId() {
        return operatorId;
    }

    public void setOperatorId(Long operatorId) {
        this.operatorId = operatorId;
    }

    public Long getOperatorBalanceId() {
        return operatorBalanceId;
    }

    public void setOperatorBalanceId(Long operatorBalanceId) {
        this.operatorBalanceId = operatorBalanceId;
    }

    public OperatorBalance getOperatorBalance() {
        return operatorBalance;
    }

    public void setOperatorBalance(OperatorBalance operatorBalance) {
        this.operatorBalance = operatorBalance;
    }
}
