package com.tmx.web.controls.grid;

import com.tmx.as.resolver.*;

import java.util.List;

abstract public class QueryableGrid extends Grid{
    private Query initialQuery;
    private String queryString;
    public void bind(String queryString){
        this.queryString=queryString;
    }
    public void refresh() throws GridException{
        List data=null;
        if(queryString!=null){
            try {
                initialQuery=new FunctionProcessor().parseQueryText(queryString);
            } catch (FunctionSyntaxException e) {
                throw new GridException("err.grid.incorrect_grid_query", new String[]{queryString}, e);
            }
            try {
                QueryResolver queryResolver = new QueryResolver();
                QueryResult queryResult = queryResolver.executeQuery(initialQuery);
                if(!(queryResult.getRetrievedResult() instanceof List))
                    throw new ResolverException("err.table.incorrect_query_result_type_for_table", new String[]{queryResult.getRetrievedResult().getClass().getName()});
                data = (List)queryResult.getRetrievedResult();
            }catch(Throwable t){
                throw new GridException("err.grid.executeQueryFaled", new String[]{queryString}, t);
            }
            bind(data);
        }else{
            throw new GridException("err.grid.queryString_not_bind");
        }
    }
}
