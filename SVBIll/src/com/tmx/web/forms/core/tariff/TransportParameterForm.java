package com.tmx.web.forms.core.tariff;

import com.tmx.web.base.BasicActionForm;
import com.tmx.web.base.RequestEnvironment;
import com.tmx.web.base.SessionEnvironment;

import javax.servlet.http.HttpServletRequest;

//Delivery  parameter from info tariff forms to parameter form, over session
abstract public class TransportParameterForm extends BasicActionForm {
    
    protected void setOnSaveButtonAction(String action) {
        HttpServletRequest request = RequestEnvironment.currentReqContext().getRequest();
        if(request != null)
            SessionEnvironment.setAttr(request.getSession(true), "onSave", action);
    }

    protected void setOnApplyButtonAction(String action) {
        HttpServletRequest request = RequestEnvironment.currentReqContext().getRequest();
        if(request != null)
            SessionEnvironment.setAttr(request.getSession(true), "onApply", action);
    }

    protected void setOnCancelButtonAction(String action) {
        HttpServletRequest request = RequestEnvironment.currentReqContext().getRequest();
        if(request != null)
            SessionEnvironment.setAttr(request.getSession(true), "onCancel", action);
    }


    protected String getOnSaveButtonAction() {
        HttpServletRequest request = RequestEnvironment.currentReqContext().getRequest();
        if(request == null)
            return null;
        return (String)SessionEnvironment.getAttr(request.getSession(true), "onSave");
    }

    protected String getOnApplyButtonAction() {
        HttpServletRequest request = RequestEnvironment.currentReqContext().getRequest();
        if(request == null)
            return null;
        return (String)SessionEnvironment.getAttr(request.getSession(true), "onApply");
    }

    protected String getOnCancelButtonAction() {
        HttpServletRequest request = RequestEnvironment.currentReqContext().getRequest();
        if(request == null)
            return null;
        return (String)SessionEnvironment.getAttr(request.getSession(true), "onCancel");
    }

}
