package com.tmx.gate.datastream.util;

import org.apache.log4j.Logger;

import java.io.*;
import java.util.zip.CRC32;

import com.tmx.gate.datastream.base.DataStreamException;
import com.tmx.gate.datastream.base.DataEncodingException;

/**
 * Created by IntelliJ IDEA.
 * User: Shake
 * Date: 11.07.2006
 * Time: 17:44:24
 * To change this template use File | Settings | File Templates.
 */
public class Encoder {
    public static final String CROX_SIGNATURE = "CROX";
    private static final int Root = 0;
    private static final int PredMax = 255;
    private int[] Up;
    private int BitPos;
    private int[] Left;
    private int[] Right;
    private int OutByte;
    protected Logger logger = Logger.getLogger("dataloader.Encoder");

    public Encoder(Logger logger){
        this.logger = logger;
        InitEncoder();
    }

    public Encoder(){
        InitEncoder();
    }

    public void InitEncoder(){
        Up = new int[513];
        Left = new int[256];
        Right = new int[256];
    }

    private void InitializeSplay(){
        for(int i=1; i < Up.length; i++)
            Up[i] = ((i - 1) >> 1);
        for(int i = 0; i <= 255; i++){
            Right[i] = (i + 1) * 2 - 1;
            Left[i] = (i + 1) * 2;
        }
    }

    private void Splay(int Plain){
        int A, B;
        int C, D;
        A = 256 + Plain;
        C = Up[A];
        while(C != Root){
            D = Up[C];
            if(Left[D] == C){
                B = Right[D];
                Right[D] = A;
            }else{
                B = Left[D];
                Left[D] = A;
            }
            if (Right[C] == A){
                Right[C] = B;
            }else{
                Left[C] = B;
            }
            Up[A] = D;
            Up[B] = C;
            A = D;
            C = Up[A];
        }
    }

    private ByteArrayOutputStream Compress(int Plain){
        int A;
        ByteArrayOutputStream pTarget = new ByteArrayOutputStream();
        A = 256 + Plain;
        boolean Stack[] = new boolean[256];
        int Sp = 0;
        do{
            Stack[Sp] = (Right[Up[A]] == A);
            Sp++;
            A = Up[A];
        }while(A != Root);
        do{
            Sp--;
            if(Stack[Sp]){
                OutByte |= BitPos;
            }
            BitPos <<= 1;
            if((BitPos & 0xff) == 0){
                pTarget.write(OutByte);
                BitPos = 1;
                OutByte = 0;
            }
        }while(Sp != 0);
        Splay(Plain);
        return pTarget;
    }

    public byte[] Compress(byte[] pSource){
        OutByte = 0;
        BitPos = 1;
        ByteArrayOutputStream retOs = new ByteArrayOutputStream();
        try{
            InitializeSplay();
            for(int i = 0; i < pSource.length; i++){
                ByteArrayOutputStream ba = Compress(pSource[i]);
                ba.writeTo(retOs);
            }
            if(BitPos != 0){
                retOs.write(OutByte);
            }
        }catch(Exception e){
            logger.error(e);
        }
        return retOs.toByteArray();
    }


    public byte[] Decompress(byte[] pSource){
        int A;
        BitPos = 128;
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        int i = 0;
        try{
            InitializeSplay();
            while(i <= pSource.length){
                A = Root;
                do{
                    BitPos <<= 1;
                    if((BitPos & 0xff) == 0){
                        i++;
                        BitPos = 1;
                    }
                    if(i <= pSource.length && (pSource[i - 1] & BitPos) != 0){
                        A = Right[A];
                    }else{
                        A = Left[A];
                    }
                }while (A <= PredMax);
                A -= 256;
                Splay(A);
                os.write(A);
            }
        }catch(Exception e){
            logger.error(e);
        }

        return os.toByteArray();
    }

    public static long byteArrayToLong(byte[] buf, int offset){
        long l = 0;
        l |= buf[offset + 3] & 0xFF;
        l <<= 8;
        l |= buf[offset + 2] & 0xFF;
        l <<= 8;
        l |= buf[offset + 1] & 0xFF;
        l <<= 8;
        l |= buf[offset] & 0xFF;
        return l;
    }


    public byte[] CheckAndDecompress(byte[] content) throws Throwable{
        int offset = CROX_SIGNATURE.length();
        if(content.length < (offset + 8)){
            throw new DataEncodingException("err.encoder.corrupted_file_error",
                    new String[]{
                            "File size too small"
                    });
        }

        if(!(new String(content, 0, offset)).equals(CROX_SIGNATURE)){
            throw new DataEncodingException("err.encoder.crox_signature_error");
        }
        long DataSize = byteArrayToLong(content, offset);
        offset += 4;
        long headerCRC32 = byteArrayToLong(content, offset);
        offset += 8;
        CRC32 crc32  = new CRC32();
        crc32.update(content, offset, content.length - offset);
        long fileCRC32 = crc32.getValue();
        if(fileCRC32 != headerCRC32){
            throw new DataEncodingException("err.encoder.incorrect_crc32");
        }

        ByteArrayOutputStream os = new ByteArrayOutputStream();
        os.write(content, offset, content.length - offset);

        content = Decompress(os.toByteArray());
        if(content.length < DataSize){
            throw new DataEncodingException("err.encoder.incorrect_length");
        }
        os.reset();
        os.write(content, 0, (int)DataSize);

        return os.toByteArray();
    }
}
