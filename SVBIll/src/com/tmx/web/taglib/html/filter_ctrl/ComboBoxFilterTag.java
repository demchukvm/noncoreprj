package com.tmx.web.taglib.html.filter_ctrl;

import com.tmx.web.controls.ScrollBox;

import javax.servlet.jsp.JspException;

/**
 */
public class ComboBoxFilterTag extends AbstractFilterTag{
    private String parameterName = null;
    //CSS styles properties
    private String styleClass = null;

    //------------------------------Properties

    public String getStyleClass() {
        return (styleClass != null) ? styleClass : getDefaultProperty("tag.comboBoxFilter.style.styleClass");
    }

    public void setStyleClass(String styleClass) {
        this.styleClass = styleClass;
    }

    public String getParameterName() {
        return parameterName;
    }

    public void setParameterName(String parameterName) {
        this.parameterName = parameterName;
    }

    //------------------------------Protected methods
    protected String renderControlHTML() throws JspException {
//        super.renderControlHTML();
        StringBuffer resultHTML = new StringBuffer();
        String path = getProperty() + "." + getFilter().getParameter(parameterName).getPath() + "." + ScrollBox.SELECTEDKEY_SUFFIX;
        ScrollBox scrollBox = (ScrollBox)getFilter().getParameter(parameterName).getControl();

        if(!scrollBox.isBounded())
            return "Data is not bound to ScrollBox '"+getProperty()+"'";

        //Render HTML
        resultHTML.append("\r");
        String name = " name=\""+path + "\" ";
        String styleClass = " class=\"" + getStyleClass() + "\" ";
        String readonly = (scrollBox.isReadonly()) ? "disabled" : "";

        resultHTML.append("<select "+ name + styleClass + readonly +">\r");
        scrollBox.reset();
        while(scrollBox.hasNext()){
            ScrollBox.Entry entry = scrollBox.next();
            String attrSelectedHTML = (entry.getKey().equals(scrollBox.getSelectedEntry().getKey()) ? "selected=\"selected\"" : "");
            resultHTML.append("    <option value="+entry.getKey()+ " " + attrSelectedHTML + ">\r");
            resultHTML.append((entry.getValue() == null) ? "" : entry.getValue() + "\r");
            resultHTML.append("    </option>\r");
        }
        resultHTML.append("</select>\r");
        resultHTML.append("\r");

        return resultHTML.toString();
    }

}
