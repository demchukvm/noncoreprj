package com.tmx.web.actions.core.dictionary.processing;

import com.tmx.web.base.BasicDispatchedAction;
import com.tmx.web.base.Roadmap;
import com.tmx.web.forms.core.dictionary.processing.InfoProcessingForm;
import com.tmx.as.blogic.ProcessingManager;
import com.tmx.as.entities.bill.processing.Processing;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForm;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class InfoProcessingAction extends BasicDispatchedAction {
    
    private static final String FORWARD_MANAGE_PROCESSINGS_ACTION = "manageProcessingsAction";
    private static final String FORWARD_INFO_PROCESSING_PAGE = "infoProcessingPage";
    private static final String FORWARD_MANAGE_DELETE_ACTION = "manageDeleteAction";


    public ActionForward preloadForCreate(ActionMapping mapping,
                                          ActionForm form,
                                          HttpServletRequest request,
                                          HttpServletResponse response) throws Throwable {
        InfoProcessingForm infoProcessingForm = (InfoProcessingForm) verifyForm(form, InfoProcessingForm.class);
        infoProcessingForm.bindData();
        return mapping.findForward(FORWARD_INFO_PROCESSING_PAGE);
    }

    public ActionForward preloadForUpdate(ActionMapping mapping,
                                          ActionForm form,
                                          HttpServletRequest request,
                                          HttpServletResponse response) throws Throwable {
        InfoProcessingForm infoProcessingForm = (InfoProcessingForm) verifyForm(form, InfoProcessingForm.class);
        infoProcessingForm.bindData(new ProcessingManager().getProcessing(infoProcessingForm.getSelectedProcessingId()));
        return mapping.findForward(FORWARD_INFO_PROCESSING_PAGE);
    }

    public ActionForward save(ActionMapping mapping,
                              ActionForm form,
                              HttpServletRequest request,
                              HttpServletResponse response) throws Throwable {
        InfoProcessingForm infoProcessingForm = (InfoProcessingForm) verifyForm(form, InfoProcessingForm.class);
        saveProcessing( infoProcessingForm );
        return mapping.findForward(FORWARD_MANAGE_PROCESSINGS_ACTION);
    }

    public ActionForward apply(ActionMapping mapping,
                              ActionForm form,
                              HttpServletRequest request,
                              HttpServletResponse response) throws Throwable {
        InfoProcessingForm infoProcessingForm = (InfoProcessingForm) verifyForm(form, InfoProcessingForm.class);
        saveProcessing( infoProcessingForm );
        return mapping.findForward(FORWARD_INFO_PROCESSING_PAGE);
    }

    public ActionForward preloadForDelete(ActionMapping mapping,
                                          ActionForm form,
                                          HttpServletRequest request,
                                          HttpServletResponse response) throws Throwable {
        Roadmap roadmap=getRoadmap();
        roadmap.collectParameter("/core/message/confirmation", "leftMenuName", "dictionaries");
        roadmap.setPageForward("confirmation","onOk","local:"+this.getName()+"#"+FORWARD_MANAGE_DELETE_ACTION);
        roadmap.setPageForward("confirmation","onCancel","local:"+this.getName()+"#"+ FORWARD_MANAGE_PROCESSINGS_ACTION);
        return mapping.findForward("confirmation");
    }

    public ActionForward delete(ActionMapping mapping,
                              ActionForm form,
                              HttpServletRequest request,
                              HttpServletResponse response) throws Throwable {
        InfoProcessingForm infoProcessingForm = (InfoProcessingForm) verifyForm(form, InfoProcessingForm.class);
        ProcessingManager processingManager = new ProcessingManager();
        processingManager.deleteProcessing( infoProcessingForm.getSelectedProcessingId() );
        return mapping.findForward(FORWARD_MANAGE_PROCESSINGS_ACTION);
    }

    public ActionForward defaultAction(ActionMapping mapping,
                                       ActionForm form,
                                       HttpServletRequest request,
                                       HttpServletResponse response,
                                       String methodName) throws Throwable {
        return preloadForCreate(mapping, form, request, response);
    }

    private void saveProcessing( InfoProcessingForm infoProcessingForm ) throws Throwable {
        infoProcessingForm.validate();
        ProcessingManager sellerManager = new ProcessingManager();
        Processing seller = infoProcessingForm.populateFromControls();
        sellerManager.saveProcessing( seller );
    }

}
