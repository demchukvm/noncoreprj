package com.tmx.beng.iso8583Gate.mac;

import com.tmx.beng.iso8583Gate.base.ProtocolMetadata;
import com.tmx.beng.iso8583Gate.ISO8583Service;
import com.tmx.beng.iso8583Gate.processors.ProcessingException;
import com.tmx.beng.iso8583Gate.pdu.*;
import com.tmx.beng.iso8583Gate.pdu.types.BinaryType;
import com.tmx.beng.iso8583Gate.pdu.types.TypeFactory;
import com.tmx.beng.iso8583Gate.pdu.length_types.LengthTypeFactory;
import com.tmx.beng.iso8583Gate.pdu.util.HexConverter;
import com.tmx.util.jce.mac.Mac;
import com.tmx.util.jce.mac.X919AlgParameterSpec;
import com.tmx.util.jce.mac.X99AlgParameterSpec;
import com.tmx.as.base.EntityManager;
import com.tmx.as.base.FilterWrapper;
import com.tmx.as.exceptions.DatabaseException;
import com.tmx.as.entities.iso8583.key.MasterKey;

import javax.crypto.Cipher;
import javax.crypto.SecretKeyFactory;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.DESKeySpec;
import javax.crypto.spec.DESedeKeySpec;
import java.security.spec.AlgorithmParameterSpec;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.security.InvalidKeyException;
import java.util.*;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

/**
Responsible for:
 1) MAC validation on message parsing
 2) MAC calculation on message assembling
 3) MAC keys changing.
 */
public class MacController {
    public static final String X9_9_ALG = "X9_9";
    public static final String X9_19_ALG = "X9_19";
    private final String DES_ALG = "DES";
    private final String DES_EDE_ALG = "DESede";


    public void validateMac(String protocolVersion, RuntimeMessage msg) throws MacException{
        //validate MAC
        ProtocolMetadata.ProtocolVersion.MacConfig macConfig = ISO8583Service.getInstance().getProtocolMetadata().
                getProtocolVersion(protocolVersion).getMacConfig();
        if(macConfig == null || !macConfig.isEnabled() || isMacExceptForMessage(macConfig, msg))
            return;//do not validate

        //1. Get received mac
        byte[] receivedMacBytes;
        try{
            RuntimeDataElement macRDE = msg.getRuntimeDataElement(64);
            if(macRDE == null || macRDE.getBinary() == null)
                throw new MacException("err.mac_controller.no_mac_rde_found_in_message");
            receivedMacBytes = macRDE.getBinary();
        }
        catch(IncorrectFormatException e){
            throw new MacException("err.mac_controller.failed_to_read_mac_value", e);
        }

        //2. Calculate mac
        byte[] calculatedMacBytes = calculateMac(macConfig, msg);

        //3. Compare calculated and received MACs
        compareMacs(receivedMacBytes, calculatedMacBytes);
    }

    /** Calculate mac and inject it into the given message */
    public void injectMac(String protocolVersion, RuntimeMessage msg) throws MacException{
        ProtocolMetadata.ProtocolVersion.MacConfig macConfig = ISO8583Service.getInstance().getProtocolMetadata().
                getProtocolVersion(protocolVersion).getMacConfig();
        if(macConfig == null || !macConfig.isEnabled() || isMacExceptForMessage(macConfig, msg))
            return;//do not inject MAC

        RuntimeDataElement macRDE = new RuntimeDataElement(msg.getMessageCommonFormat().getDataElementFormat(64));
        msg.addRuntimeDataElement(macRDE);//add before calculation to cover this rde too.
        byte[] calculatedMac = calculateMac(macConfig, msg);

        //adopt length
        int length;
        if(!LengthTypeFactory.FIXED.equals(macRDE.getFormat().getLengthType()) ||
           !TypeFactory.BINARY.equals(macRDE.getFormat().getType()))
                throw new MacException("err.mac_controller.failed_to_adopt_mac.fixed_binary_type_required");

        length = macRDE.getFormat().getLength();
        BinaryType bt = new BinaryType();
        length = bt.getByteLength(length);//convert to byte

        byte[] adoptedMac = new byte[length];
        System.arraycopy(calculatedMac, 0, adoptedMac, 0, adoptedMac.length);//copy all what is possible

        macRDE.setValue(adoptedMac);
    }

    private byte[] calculateMac(ProtocolMetadata.ProtocolVersion.MacConfig macConfig, RuntimeMessage msg) throws MacException{
        RuntimeDataElement terminalIdRDE = msg.getRuntimeDataElement(41);
        if(terminalIdRDE == null)
            throw new MacException("err.mac_controller.terminal_rde_is_null");
        byte[] terminalRawMacKey = getTerminalMacKey(terminalIdRDE.getString());

        Mac mac;
        try{
            mac = Mac.getInstance(macConfig.getAlgorithm());
            mac.init(readKey(terminalRawMacKey, macConfig.getAlgorithm()),
                     getAlgorithmParameterSpec(macConfig.getAlgorithm()));
        }
        catch(Exception e){
            throw new MacException("err.mac_controller.failed_to_init_mac", e);
        }

        byte[] calculatedMacBytes;
        try{
            calculatedMacBytes = mac.doFinal(getSourceForMac(msg));
        }
        catch(IsoSerializeException e){
            throw new MacException("err.mac_controller.failed_to_get_mac_source", e);
        }
        return calculatedMacBytes;
    }

    protected byte[] getTerminalMacKey(String terminalId) throws MacException {
        try{
            EntityManager em = new EntityManager();
            HexConverter hexConv = new HexConverter();
            //retrieve key
            FilterWrapper byTerminalIdFW = new FilterWrapper("by_isoterminalid");
            byTerminalIdFW.setParameter("terminalid", terminalId);
            com.tmx.as.entities.iso8583.key.Key key = (com.tmx.as.entities.iso8583.key.Key)em.RETRIEVE(com.tmx.as.entities.iso8583.key.Key.class, new FilterWrapper[]{byTerminalIdFW}, null);
            if(key != null){
                //retrieve master key
                FilterWrapper byIndexFW = new FilterWrapper("by_index");
                byIndexFW.setParameter("index", key.getCurrentMasterKeyIndex());
                MasterKey masterKey = (MasterKey)em.RETRIEVE(MasterKey.class, new FilterWrapper[]{byTerminalIdFW, byIndexFW}, null);
                if(masterKey == null)
                    throw new MacException("err.auth_key_upd_proc.no_master_key_for_terminal_found", new String[]{terminalId});

                //decrypt master key
                byte[] masterKeyHex = decryptKey(hexConv.getHexFromString(masterKey.getEncryptedMasterKey()),
                                                 getHklmKey(),
                                                 key.getMacGenKeyEncryptAlg());

                //no mac key - create new
                if(key.getCurrentMacGenKeyEncrypted() == null)
                    key = updateMacKey(terminalId);

                //decrypt mac key
                return decryptKey(hexConv.getHexFromString(key.getCurrentMacGenKeyEncrypted()), masterKeyHex, key.getMacGenKeyEncryptAlg());                
            }
            else{
                throw new MacException("err.mac_controller.no_mac_key_for_terminal", new String[]{terminalId});
            }
        }
        catch(DatabaseException e){
            throw new MacException("err.mac_controller.failed_to_obtain_raw_terminal_mac_key", new String[]{terminalId}, e);
        }
    }


    public com.tmx.as.entities.iso8583.key.Key updateMacKey(String terminalId) throws DatabaseException, MacException {
        EntityManager em = new EntityManager();
        FilterWrapper byTerminalIdFW = new FilterWrapper("by_isoterminalid");
        byTerminalIdFW.setParameter("terminalid", terminalId);
        com.tmx.as.entities.iso8583.key.Key key = (com.tmx.as.entities.iso8583.key.Key) em.RETRIEVE(com.tmx.as.entities.iso8583.key.Key.class, new FilterWrapper[]{byTerminalIdFW}, null);

        if (key == null)
            throw new MacException("err.mac_controller.no_key_for_terminal_found", new String[]{terminalId});

        FilterWrapper byIndexFW = new FilterWrapper("by_index");
        byIndexFW.setParameter("index", key.getCurrentMasterKeyIndex());

        MasterKey masterKey = (MasterKey) em.RETRIEVE(MasterKey.class, new FilterWrapper[]{byTerminalIdFW, byIndexFW}, null);
        if (masterKey == null)
            throw new MacException("err.mac_controller.no_master_key_for_terminal_found", new String[]{terminalId});

        HexConverter hexConv = new HexConverter();
        //decrypt master key
        byte[] masterKeyHex = decryptKey(hexConv.getHexFromString(masterKey.getEncryptedMasterKey()),
                                         getHklmKey(),
                                         key.getMacGenKeyEncryptAlg());

        byte[] newMacGenKey = generateRawKey();
        byte[] encNewMacGenKey = encryptKey(newMacGenKey, masterKeyHex, key.getMacGenKeyEncryptAlg());
        //todo remove opened key
        key.setCurrentMacGenKey(hexConv.getStringFromHex(newMacGenKey));
        //use only first 16 bytes (from 24) of DESede key. 1 byte = 2 0xdigits
        key.setCurrentMacGenKeyEncrypted(hexConv.getStringFromHex(encNewMacGenKey));
        key.setCurrentMacGenKeyUpdTime(new Date());
        em.SAVE(key);
        return key;
    }


    public byte[] encryptKey(byte[] rawEncryptedKey, byte[] rawMasterKey, String algorithm) throws MacException{
        try{
            KeySpec keyspec;
            if(DES_ALG.equals(algorithm)){
                keyspec = new DESKeySpec(rawMasterKey);
            }
            else if(DES_EDE_ALG.equals(algorithm)){
                keyspec = new DESedeKeySpec(rawMasterKey);
            }
            else{
                throw new MacException("err.mac_controller.unsupported_algorithm", new String[]{algorithm});
            }
            SecretKeyFactory keyfactory = SecretKeyFactory.getInstance(algorithm);
            SecretKey secretKey = keyfactory.generateSecret(keyspec);
            Cipher cipher = Cipher.getInstance(algorithm);
            cipher.init(Cipher.ENCRYPT_MODE, secretKey);
            return cipher.doFinal(rawEncryptedKey);
        }
        catch(Exception e){
            throw new MacException("err.mac_controller.failed_to_encript_generated_key", e);
        }
    }

    public byte[] decryptKey(byte[] rawEncryptedKey, byte[] rawMasterKey, String algorithm) throws MacException{
        try{
            KeySpec keyspec;
            if(DES_ALG.equals(algorithm)){
                keyspec = new DESKeySpec(rawMasterKey);
            }
            else if(DES_EDE_ALG.equals(algorithm)){
                keyspec = new DESedeKeySpec(rawMasterKey);
            }
            else{
                throw new MacException("err.mac_controller.unsupported_algorithm", new String[]{algorithm});
            }
            SecretKeyFactory keyfactory = SecretKeyFactory.getInstance(algorithm);
            SecretKey secretKey = keyfactory.generateSecret(keyspec);
            Cipher cipher = Cipher.getInstance(algorithm);
            cipher.init(Cipher.DECRYPT_MODE, secretKey);
            return cipher.doFinal(rawEncryptedKey);
        }
        catch(Exception e){
            throw new MacException("err.mac_controller.failed_to_decrypt_generated_key", e);
        }
    }

    /** Generate DESede key appropriate for DES too */
    public byte[] generateRawKey(){
        byte[] rawKey = new byte[24];
        byte[] keyPart = new byte[8];
        Random random = new Random(System.currentTimeMillis());
        random.nextBytes(keyPart);
        System.arraycopy(keyPart, 0, rawKey, 0, keyPart.length);
        random.nextBytes(keyPart);
        System.arraycopy(keyPart, 0, rawKey, keyPart.length, keyPart.length);
        System.arraycopy(rawKey, 0, rawKey, keyPart.length*2, keyPart.length);
        return rawKey;
    }

    private AlgorithmParameterSpec getAlgorithmParameterSpec(String algorithm) throws NoSuchAlgorithmException, NoSuchPaddingException {
        if (X9_9_ALG.equals(algorithm)) {
            return new X99AlgParameterSpec(
                    Cipher.getInstance(DES_ALG));
        }
        else if (X9_19_ALG.equals(algorithm)) {
            return new X919AlgParameterSpec(
                    Cipher.getInstance(DES_EDE_ALG),
                    Cipher.getInstance(DES_ALG));
        }
        else {
            throw new NoSuchAlgorithmException("No such algorithm "+algorithm);
        }
    }

    private Key readKey(byte[] rawKey, String algorithm) throws InvalidKeyException, NoSuchAlgorithmException, InvalidKeySpecException {
        if (X9_9_ALG.equals(algorithm)) {
            DESKeySpec keyspec = new DESKeySpec(rawKey);
            SecretKeyFactory keyfactory = SecretKeyFactory.getInstance(DES_ALG);
            return keyfactory.generateSecret(keyspec);
        }
        else if (X9_19_ALG.equals(algorithm)) {
            DESedeKeySpec keyspec = new DESedeKeySpec(rawKey);
            SecretKeyFactory keyfactory = SecretKeyFactory.getInstance(DES_EDE_ALG);
            return keyfactory.generateSecret(keyspec);
        }
        else {
            throw new NoSuchAlgorithmException("No such algorithm "+algorithm);
        }
    }

    private byte[] getSourceForMac(RuntimeMessage msg) throws IsoSerializeException {
        try {

            List byteChanks = new ArrayList();
            ByteArrayOutputStream os = new ByteArrayOutputStream();

            RuntimeDataElement messageTypeRDE = msg.getRuntimeDataElement(DataElementFormat.DEF_MESSAGETYPEID);
            byteChanks.add(messageTypeRDE.getFormat().serialize(messageTypeRDE));

            RuntimeDataElement bitMapRDE = msg.getRuntimeDataElement(DataElementFormat.DEF_BITMAP);
            byteChanks.add(bitMapRDE.getFormat().serialize(bitMapRDE));

            for (Iterator iter = msg.getRuntimeDataElements().iterator(); iter.hasNext();) {
                RuntimeDataElement nextRDE = (RuntimeDataElement) iter.next();
                if (nextRDE.getFormat().getBitmapNumber() > 0 && nextRDE.getFormat().getBitmapNumber() < 64) //get only elements with bitmap number >= 1 and <= 63
                    byteChanks.add(nextRDE.getFormat().serialize(nextRDE));
            }

            for (Iterator iter = byteChanks.iterator(); iter.hasNext();)
                os.write((byte[]) iter.next());

//debug            
System.out.println("MAC SOURCE");
System.out.println(new HexConverter().getStringFromHex(os.toByteArray()));

            return os.toByteArray();
        }
        catch (IOException e) {
            throw new IsoSerializeException("err.runtime_message.failed_to_obtain_source_for_mac", e);
        }
    }

    private void compareMacs(byte[] receivedMac, byte[] calcualatedMac) throws MacException{
        //compare only thouse bytes of MAC that are exist in received message.
        //so calculated MAC length could be longer and it's ok.
        for(int i=0; i<receivedMac.length; i++)
            if(receivedMac[i] != calcualatedMac[i]){
                HexConverter hexConverter = new HexConverter();
                throw new MacException("err.mac_controller.compared_macs_are_different", new String[]{
                        hexConverter.getStringFromHex(receivedMac),
                        hexConverter.getStringFromHex(calcualatedMac)
                });
            }

    }


    private boolean isMacExceptForMessage(ProtocolMetadata.ProtocolVersion.MacConfig macConfig, RuntimeMessage msg)
        throws MacException{
        try{
            return macConfig.isExceptMac(
                    new Long(msg.getRuntimeDataElement(DataElementFormat.DEF_MESSAGETYPEID).getLong()),
                    new Long(msg.getRuntimeDataElement(DataElementFormat.DEF_PROCESSINGCODE).getLong())
            );
        }
        catch(IncorrectFormatException e){
            throw new MacException("err.mac_controller.failed_to_check_mac_except_rules", e);
        }
    }

    public byte[] getHklmKey(){
        return new HexConverter().getHexFromString(System.getProperty("hklmKey"));        
    }
}
