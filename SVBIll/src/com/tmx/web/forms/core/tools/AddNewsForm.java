package com.tmx.web.forms.core.tools;

import com.tmx.as.blogic.UserManager;
import com.tmx.as.entities.bill.seller.Seller;
import com.tmx.as.entities.general.user.User;
import com.tmx.as.entities.bill.messages.Messages;
import com.tmx.as.exceptions.DatabaseException;
import com.tmx.web.controls.Button;
import com.tmx.web.base.BasicActionForm;
import com.tmx.web.controls.EditBox;
import com.tmx.web.controls.ScrollBox;
import com.tmx.web.controls.TimeSelector;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class AddNewsForm extends BasicActionForm
{
    //private boolean isEmpty;

    private TimeSelector timeSelectorActivation;
    private TimeSelector timeSelectorDeactivation;

    private ScrollBox scrollBoxUsers;
    private ScrollBox scrollBoxMessages;

    private Button saveMessage = null;
    private Button resetMessage = null;
    private Button deleteMessage = null;
    private String messageTitle = null;
    private String messageText = null;
    private String responseText = null;

    public void init()
    {
        //isEmpty = false;

        saveMessage = new Button();
        saveMessage.setLabelKey("ctrl.button.label.save");
        saveMessage.setAction("/AddNewsAction");
        saveMessage.setCommand("saveMessage");

        resetMessage = new Button();
        resetMessage.setLabelKey("ctrl.button.label.reset");
        resetMessage.setAction("/AddNewsAction");
        resetMessage.setCommand("resetMessage");

        deleteMessage = new Button();
        deleteMessage.setLabelKey("ctrl.button.label.delete");
        deleteMessage.setAction("/AddNewsAction");
        deleteMessage.setCommand("deleteMessage");

        messageTitle = "";
        messageText = "";

        responseText = "";

        timeSelectorActivation = new TimeSelector();
        timeSelectorActivation.setReadonly(true);
        timeSelectorDeactivation = new TimeSelector();
        timeSelectorDeactivation.setReadonly(true);

        scrollBoxUsers = new ScrollBox(){
            protected String getKey(Object dataListElement) {
                return ((User)dataListElement).getUserId().toString();
            }
//            protected String getValue(Object dataListElement) {
//                return ((User)dataListElement).getLogin() + " - " +
//                        (((User)dataListElement).getLastName()==null?"":((User)dataListElement).getLastName()) +
//                        " " +
//                        (((User)dataListElement).getFirstName()==null?"":((User)dataListElement).getFirstName());
//            }
            protected String getValue(Object dataListElement) {
                return ((User)dataListElement).getLogin();
            }
        };


        scrollBoxMessages = new ScrollBox(){
            protected String getKey(Object dataListElement) {
                return ((Messages)dataListElement).getMesId().toString();
            }
            protected String getValue(Object dataListElement) {
                return ((Messages)dataListElement).getTitle();
            }
        };


//        try {
//            scrollBoxUsers.bind(new UserManager().getAllUsers());
//            scrollBoxMessages.bind(new UserManager().getAllMessages());
//
//        } catch (DatabaseException e) {
//            e.printStackTrace();
//        }
        scrollBoxUsers.setReadonly(false);
        scrollBoxUsers.setNullable(true);

        scrollBoxMessages.setReadonly(false);
        scrollBoxMessages.setNullable(true);
        
    }

    public Button getSaveMessage() {
        return saveMessage;
    }

    public void setSaveMessage(Button saveMessage) {
        this.saveMessage = saveMessage;
    }

    public String getMessageTitle() {
        return messageTitle;
    }

    public void setMessageTitle(String messageTitle) {
        this.messageTitle = messageTitle;
    }

    public String getMessageText() {
        return messageText;
    }

    public void setMessageText(String messageText) {
        this.messageText = messageText;
    }

    public String getResponseText() {
        return responseText;
    }

    public void setResponseText(String responseText) {
        this.responseText = responseText;
    }

    public Button getResetMessage() {
        return resetMessage;
    }

    public void setResetMessage(Button resetMessage) {
        this.resetMessage = resetMessage;
    }

    public TimeSelector getTimeSelectorActivation() {
        return timeSelectorActivation;
    }

    public void setTimeSelectorActivation(TimeSelector timeSelectorActivation) {
        this.timeSelectorActivation = timeSelectorActivation;
    }

    public TimeSelector getTimeSelectorDeactivation() {
        return timeSelectorDeactivation;
    }

    public void setTimeSelectorDeactivation(TimeSelector timeSelectorDeactivation) {
        this.timeSelectorDeactivation = timeSelectorDeactivation;
    }

    public ScrollBox getScrollBoxUsers() {
        return scrollBoxUsers;
    }

    public void setScrollBoxUsers(ScrollBox scrollBoxUsers) {
        this.scrollBoxUsers = scrollBoxUsers;
    }

    public ScrollBox getScrollBoxMessages() {
        return scrollBoxMessages;
    }

    public void setScrollBoxMessages(ScrollBox scrollBoxMessages) {
        this.scrollBoxMessages = scrollBoxMessages;
    }

    public Button getDeleteMessage() {
        return deleteMessage;
    }

    public void setDeleteMessage(Button deleteMessage) {
        this.deleteMessage = deleteMessage;
    }

//    public boolean isEmpty() {
//        return isEmpty;
//    }
//
//    public void setEmpty(boolean empty) {
//        isEmpty = empty;
//    }

    
}
