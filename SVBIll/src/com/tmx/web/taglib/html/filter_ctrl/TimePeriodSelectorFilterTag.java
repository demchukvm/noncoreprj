package com.tmx.web.taglib.html.filter_ctrl;

import com.tmx.web.controls.TimePeriodSelector;
import com.tmx.web.controls.TimeSelector;
import com.tmx.web.base.ApplicationEnvironment;
import com.tmx.web.taglib.html.ctrl.TimePeriodSelectorTag;
import com.tmx.engines.reportengine.util.TimePeriod;
import javax.servlet.jsp.JspException;
import java.util.Iterator;
import java.util.List;

/**
Render html for time selector control and handle its actions
 */
public class TimePeriodSelectorFilterTag extends AbstractFilterTag {
    private String allowManualChange = "true";
    private String toDateParamName = null;
    private String fromDateParamName = null;
    private String calendarButtonImage = null; //"/management/images/img.gif
    private String editBoxStyleClass = null;
    private String dropdownBoxStyleClass = null;
    //-----------------------------Constants
    //JS calls
    public static final String ON_TIME_PERIOD_SELECT = "onTimePeriodSelect()";
    public static final String ON_CUSTOM_DATE_CHANGE = "onCustomDate�hange()";
    //Style classes
    private final String CALENDAR_BUTTON_CLASS = "calendar_button";
    //html id suffix of calendar buttons 'From' and 'To'
    public static final String CALENDER_BUTTON_FROM_ID_SUFFIX = "TimeFromButton";
    public static final String CALENDER_BUTTON_TO_ID_SUFFIX = "TimeToButton";

    //------------------------------Properties

    public String getToDateParamName() {
        return toDateParamName;
    }

    public void setToDateParamName(String toDateParamName) {
        this.toDateParamName = toDateParamName;
    }

    public String getFromDateParamName() {
        return fromDateParamName;
    }

    public void setFromDateParamName(String fromDateParamName) {
        this.fromDateParamName = fromDateParamName;
    }

    public String getCalendarButtonImage() {
        return (calendarButtonImage != null) ? calendarButtonImage : getDefaultProperty("tag.timePeriodSelectorFilter.img.calendarButtonImage");
    }

    public void setCalendarButtonImage(String calendarButtonImage) {
        this.calendarButtonImage = (calendarButtonImage != null && calendarButtonImage.startsWith("/") && !calendarButtonImage.startsWith(ApplicationEnvironment.getAppDeploymentName()) ? ApplicationEnvironment.getAppDeploymentName() : "") + calendarButtonImage;
    }

    public String getEditBoxStyleClass() {
        return (editBoxStyleClass != null) ? editBoxStyleClass : getDefaultProperty("tag.timePeriodSelectorFilter.style.editBoxStyleClass");
    }

    public void setEditBoxStyleClass(String editBoxStyleClass) {
        this.editBoxStyleClass = editBoxStyleClass;
    }

    public String getDropdownBoxStyleClass() {
        return (dropdownBoxStyleClass != null) ? dropdownBoxStyleClass : getDefaultProperty("tag.timePeriodSelectorFilter.style.dropdownBoxStyleClass");
    }

    public void setDropdownBoxStyleClass(String dropdownBoxStyleClass) {
        this.dropdownBoxStyleClass = dropdownBoxStyleClass;
    }

    public String getAllowManualChange() {
        return allowManualChange;
    }

    public void setAllowManualChange(String allowManualChange) {
        this.allowManualChange = allowManualChange;
    }

    //------------------------------Rendering methods
    protected String renderControlHTML() throws JspException {
        StringBuffer resultHTML = new StringBuffer();
        TimePeriodSelector tps = getTimePeriodSelector();
        String fromDatePath = getProperty() + "." + getFilter().getParameter(getFromDateParamName()).getPath() + "." + TimeSelector.TIME_SUFFIX;
        String replacedDotFromDatePath = getSeparatorReplacedName(fromDatePath);
        String toDatePath = getProperty() + "." + getFilter().getParameter(getToDateParamName()).getPath() + "." + TimeSelector.TIME_SUFFIX;
        String replacedDotToDatePath = getSeparatorReplacedName(toDatePath);
        String replacedDotFilterPath = getSeparatorReplacedName(getProperty()+"."+getFilter().getPath());

        //Render HTML
        resultHTML.append("<table border=\"0\" cellpadding=\"2\" cellspacing=\"2\">\r");
        resultHTML.append(" <tr>\r");

        //dropdown list
        resultHTML.append("     <td>\r");
        resultHTML.append("         <select name=\""+replacedDotFilterPath +"."+TimePeriodSelector.TIME_PERIOD_SUFFIX +
                getOnEnterPress() + "\" class=\"" + getDropdownBoxStyleClass() + "\" onchange=\"" + replacedDotFilterPath +
                "_" + ON_TIME_PERIOD_SELECT + "\">\r");
        
        List orderedTimePeriods = tps.getOrderedTimePeriods();
        for(Iterator iter = orderedTimePeriods.iterator(); iter.hasNext();){
            TimePeriod timePeriod = (TimePeriod)iter.next();
            String attrSelectedHTML = (timePeriod.getType().equals(tps.getCurrentTimePeriod().getType()) ? "selected=\"selected\"" : "");
            resultHTML.append("             <option value="+timePeriod.getType()+ " " + attrSelectedHTML + ">\r");
            resultHTML.append(timePeriod.getLabel());
            resultHTML.append("             </option>\r");
        }
        resultHTML.append("     </td>\r");

        String readonly = (getAllowManualChange().equalsIgnoreCase("false")) ? " readonly=\"true\" " :"";

        //time from
        resultHTML.append("     <td>\r");
        resultHTML.append("         "+getLocalizedLabel("tag.timePeriodSelectorFilter.label.timeFrom", null)+"\r");
        resultHTML.append("     </td>\r");
        resultHTML.append("     <td>\r");
        resultHTML.append("         <input id=\""+replacedDotFromDatePath+"\" name=\""+fromDatePath+ "\" value=\""+tps.getCurrentTimePeriod().getTimeFromString()+"\" type=\"text\" class=\""+getEditBoxStyleClass()+"\" "+readonly+" onchange=\""+ getSeparatorReplacedName(getProperty()) + "_" + ON_CUSTOM_DATE_CHANGE +";\">\r");
        resultHTML.append("     </td>\r");
        resultHTML.append("     <td>\r");
        resultHTML.append("         <img alt=\"\" src=\""+getCalendarButtonImage()+"\" id=\""+replacedDotFromDatePath + "." + CALENDER_BUTTON_FROM_ID_SUFFIX+"\" class=\""+CALENDAR_BUTTON_CLASS+"\"\r");
        resultHTML.append("     </td>\r");

        //time to
        resultHTML.append("     <td>\r");
        resultHTML.append("         "+getLocalizedLabel("tag.timePeriodSelectorFilter.label.timeTo", null)+"\r");
        resultHTML.append("     </td>\r");
        resultHTML.append("     <td>\r");
        resultHTML.append("         <input id=\""+replacedDotToDatePath+"\" name=\""+toDatePath+ "\" value=\""+tps.getCurrentTimePeriod().getTimeToString()+"\" type=\"text\" class=\""+getEditBoxStyleClass()+"\" "+readonly+" onchange=\""+ getSeparatorReplacedName(getProperty()) + "_" + ON_CUSTOM_DATE_CHANGE +";\">\r");
        resultHTML.append("     </td>\r");
        resultHTML.append("     <td>\r");
        resultHTML.append("         <img alt=\"\" src=\""+getCalendarButtonImage()+"\" id=\""+replacedDotToDatePath + "." + CALENDER_BUTTON_TO_ID_SUFFIX +"\" class=\""+CALENDAR_BUTTON_CLASS+"\"\r");
        resultHTML.append("     </td>\r");

        resultHTML.append("  </tr>\r");

        resultHTML.append("</table>\r");

        //Render JS
        // calendar for 'time From'
        resultHTML.append("<script type=\"text/javascript\">");
        resultHTML.append("     Calendar.setup({\r");
        resultHTML.append("         inputField     :    \""+ replacedDotFromDatePath +"\",      // id of the input field\r");
        resultHTML.append("         ifFormat       :    \"%Y-%m-%d %H:%M\",       // format of the input field\r");
        resultHTML.append("         button         :    \""+ replacedDotFromDatePath +"."+ TimePeriodSelectorTag.CALENDER_BUTTON_FROM_ID_SUFFIX +"\",   // trigger for the calendar (button ID)\r");
        resultHTML.append("         singleClick    :    true,         // one-click mode\r");
        resultHTML.append("         step           :    1,            // show all years in drop-down boxes (instead of every other year as default)\r");
        resultHTML.append("         showsTime      :    true,\r");
        resultHTML.append("         timeFormat     :    \"24\"\r");
        resultHTML.append("     });\r");
        resultHTML.append("\r");
        // calendar for 'time To'
        resultHTML.append("     Calendar.setup({\r");
        resultHTML.append("         inputField     :    \""+ replacedDotToDatePath +"."+ TimePeriodSelector.TO_DATE_SUFFIX +"\",      // id of the input field\r");
        resultHTML.append("         ifFormat       :    \"%Y-%m-%d %H:%M\",       // format of the input field\r");
        resultHTML.append("         button         :    \""+ replacedDotToDatePath +"."+ TimePeriodSelectorTag.CALENDER_BUTTON_TO_ID_SUFFIX +"\",   // trigger for the calendar (button ID)\r");
        resultHTML.append("         singleClick    :    true,         // one-click mode\r");
        resultHTML.append("         step           :    1,            // show all years in drop-down boxes (instead of every other year as default)\r");
        resultHTML.append("         showsTime      :    true,\r");
        resultHTML.append("         timeFormat     :    \"24\"\r");
        resultHTML.append("     });\r");
        resultHTML.append("\r");
        resultHTML.append("     //Prepare time period selector 'time from' and 'time to' arrays. They are the same for all\r");
        resultHTML.append("     //TimePeriodSelector objects on the page.\r");
        resultHTML.append("     var "+ replacedDotFromDatePath + "_TimeFromMap = new Object();\r");
        resultHTML.append("     var "+ replacedDotToDatePath + "_TimeToMap = new Object();\r");

        //populate associated arrays
        for(Iterator periodsIter = tps.getOrderedTimePeriods().iterator(); periodsIter.hasNext();){
            TimePeriod tp = (TimePeriod)periodsIter.next();
            String timeFrom = tp.getTimeFromString();
            String timeTo = tp.getTimeToString();

            resultHTML.append("     "+replacedDotFilterPath +"_TimeFromMap['"+ tp.getType().getName() +"'] = '"+ timeFrom +"';\r");
            resultHTML.append("     "+replacedDotFilterPath +"_TimeToMap['"+ tp.getType().getName() +"'] = '"+ timeTo +"';\r");
        }
        resultHTML.append("\r");
        resultHTML.append("     //Called on time period selection in the TimePeriodSelectors. Set appropriate 'From' and 'To' dates into TimePeriodSelector.\r");
        resultHTML.append("     //@param timePeriodSelectorObjName name of used TimePeriodSelector object\r");
        resultHTML.append("     function "+ replacedDotFilterPath +"_"+ ON_TIME_PERIOD_SELECT +"{\r");
        resultHTML.append("         var selectObject = getRawObject('"+ tps.getFullName() +"."+ TimePeriodSelector.TIME_PERIOD_SUFFIX +"');\r");
        resultHTML.append("         var from = getRawObject('"+ replacedDotFromDatePath +"');\r");
        resultHTML.append("         from.value = "+ replacedDotFilterPath +"_TimeFromMap[selectObject.value];\r");
        resultHTML.append("         var to = getRawObject('"+ replacedDotToDatePath +"');\r");
        resultHTML.append("         to.value = "+ replacedDotFilterPath +"_TimeToMap[selectObject.value];\r");
        resultHTML.append("     }\r");
        resultHTML.append("\r");
        resultHTML.append("     //Called on custom 'From' or 'To' date change. Sets TimePeriod type = CUSTOM\r");
        resultHTML.append("     function "+ replacedDotFilterPath +"_"+ TimePeriodSelectorTag.ON_CUSTOM_DATE_CHANGE +"{\r");
        resultHTML.append("         var selectObject = getRawObject('"+ replacedDotFilterPath +"." +TimePeriodSelector.TIME_PERIOD_SUFFIX +"');\r");
        resultHTML.append("         selectObject.value = '"+ TimePeriod.Type.CUSTOM +"';\r");
        resultHTML.append("}\r");
        resultHTML.append("</script>\r");
        resultHTML.append("\r");

        return resultHTML.toString();
    }

    private TimePeriodSelector getTimePeriodSelector() throws JspException{
        Object object = getPropertyObject();
        return (object != null && object instanceof TimePeriodSelector) ? (TimePeriodSelector)object : new TimePeriodSelector();
    }

}