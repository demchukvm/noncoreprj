package com.tmx.as.base;

import com.tmx.as.resolver.ResolverException;

public class FilterParameterWrapper implements UnifiedPropertySetter {
    private String parameterName = null;
    private Object parameterValue = null;
    private String parameterType = null;

    public String getParameterName() {
        return parameterName;
    }

    public Object getParameterValue() {
        return parameterValue;
    }

    /**
     * RM: Returns type of parameter using Hibernate types naming model.
     */
    public Object getParameterType() {
        return parameterType;
    }

    public void set(String fieldName, String fieldValue) throws ResolverException {
        if (fieldName.toLowerCase().equals("name")) {
            parameterName = fieldValue;
            return;
        }
        if (fieldName.toLowerCase().equals("value")) {
            parameterValue = fieldValue;
            if(parameterType != null)//to be independed from the "type" and "value" order in parsing string
                parameterValue = TypeWrapperUtil.parameterClassCasting((String)parameterValue, parameterType);//it is really String before class casting
            return;
        }
        if (fieldName.toLowerCase().equals("type")) {
            parameterType = fieldValue.toLowerCase();
            if(parameterValue != null)//to be independed from the "type" and "value" order in parsing string
                parameterValue = TypeWrapperUtil.parameterClassCasting((String)parameterValue, parameterType);//it is really String before class casting
            return;
        }
        //RM: throw exception if field not found
        throw new ResolverException("err.resolver.unknown_parameter_for", new String[]{this.getClass().getName(), fieldName, fieldValue.toString()});
    }

    /** The parameterValue and parameterType should be already defined before this
     * method call */
//RM: moved to TypeWrapperUtil

//    private void parameterClassCasting() throws ResolverException{
//        String currentParameterValue = (String) parameterValue;//it is really String before class casting
//        try {
//            //RM: look here to define missing type
//            // http://www.kickjava.com/src/org.hibernate.type.index.htm
//            if (parameterType.equals("long")) {
//                parameterValue = new org.hibernate.type.LongType().fromStringValue(currentParameterValue);
//                return;
//            }
//            if (parameterType.equals("short")) {
//                parameterValue = new org.hibernate.type.ShortType().fromStringValue(currentParameterValue);
//                return;
//            }
//            if (parameterType.equals("byte")) {
//                parameterValue = new org.hibernate.type.ByteType().fromStringValue(currentParameterValue);
//                return;
//            }
//            if (parameterType.equals("double")) {
//                parameterValue = new org.hibernate.type.DoubleType().fromStringValue(currentParameterValue);
//                return;
//            }
//            if (parameterType.equals("float")) {
//                parameterValue = new org.hibernate.type.FloatType().fromStringValue(currentParameterValue);
//                return;
//            }
//            if (parameterType.equals("integer")) {
//                parameterValue = new org.hibernate.type.IntegerType().fromStringValue(currentParameterValue);
//                return;
//            }
//            if (parameterType.equals("big_decimal")) {
//                parameterValue = new org.hibernate.type.BigDecimalType().fromStringValue(currentParameterValue);
//                return;
//            }
//            if (parameterType.equals("string")) {
//                parameterValue = new org.hibernate.type.StringType().fromStringValue(currentParameterValue);
//                return;
//            }
//            if (parameterType.equals("text")) {
//                parameterValue = new org.hibernate.type.TextType().fromStringValue(currentParameterValue);
//                return;
//            }
//            if (parameterType.equals("character")) {
//                parameterValue = new org.hibernate.type.CharacterType().fromStringValue(currentParameterValue);
//                return;
//            }
//            if (parameterType.equals("boolean")) {
//                parameterValue = new org.hibernate.type.BooleanType().fromStringValue(currentParameterValue);
//                return;
//            }
//            if (parameterType.equals("timestamp")) {
//                parameterValue = new org.hibernate.type.TimestampType().fromStringValue(currentParameterValue);
//                return;
//            }
//            if (parameterType.equals("time")) {
//                parameterValue = new org.hibernate.type.TimeType().fromStringValue(currentParameterValue);
//                return;
//            }
//            if (parameterType.equals("date")) {
//                parameterValue = new org.hibernate.type.DateType().fromStringValue(currentParameterValue);
//                return;
//            }
//            if (parameterType.equals("binary")) {
//                parameterValue = new org.hibernate.type.BinaryType().fromStringValue(currentParameterValue);
//                return;
//            }
///*
//            if (parameterType.equals("clob")) {
//                parameterValue = (java.sql.Clob) parameterValue;
//                return;
//            }
//            if (parameterType.equals("blob")) {
//                parameterValue = (java.sql.Blob) parameterValue;
//                return;
//            }
//*/
//
//            //the default value's type is "string"
//            parameterValue = new org.hibernate.type.StringType().fromStringValue(currentParameterValue);
//            return;
//
//        }
//        catch (ClassCastException e) {
//            throw new ResolverException("err.resolver.failed_to_cast_parmeter_type", new String[]{parameterName, parameterType, parameterValue.toString()}, e);
//        }
//    }
}
