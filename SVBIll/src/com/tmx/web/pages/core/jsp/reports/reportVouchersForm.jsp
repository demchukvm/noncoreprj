<%@ page language="java" pageEncoding="UTF-8" contentType="text/html; charset=utf-8" isELIgnored="false" %>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-nested" prefix="nested" %>
<%@ taglib uri="http://tmx.com/taglib/tmx-controls" prefix="tmx_ctrl" %>
<%@ taglib uri="http://tmx.com/taglib/tmx-logic" prefix="tmx_logic" %>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html" %>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean" %>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>

<html:form action="/core/report/reportVauchersAction">
    <table  border="0" width="100%">
        <tr>
           <td valign="center" align="center" class="tabForm">
            <table width="100%" border="0">
                <tr>
                    <td class="dataField">
                        <tmx_ctrl:button property="buttonSubmit"/>
                    </td>
                    <td class="dataLabel">
                    </td>
                    <td class="dataField">
                    </td>
                </tr>
                <tr>
                    <td class="dataLabel">
                        <bean:message bundle="core.labels" key="core.report.reportPayment.period"/>&nbsp;<bean:message bundle="core.labels" key="core.monitor.timeFrom"/>:
                    </td>
                    <td class="dataLabel">
                        <tmx_ctrl:timeSelector property="timeSelRangeFrom"/>
                    </td>

                    <td class="dataLabel">
                        <bean:message bundle="core.labels" key="core.monitor.timeTo"/>:
                    </td>
                    <td class="dataLabel">
                        <tmx_ctrl:timeSelector property="timeSelRangeTo"/>
                    </td>

                    <td class="dataLabel">
                    </td>
                    <td class="dataField">
                    </td>

                    <td class="dataField">
                    </td>
                </tr>

                <tr>
                    <td class="dataLabel">
                        <tmx_ctrl:writeEntityLabel attrName="nominal"
                                            entityClassName="com.tmx.as.entities.bill.voucher.Voucher"/>:
                    </td>
                    <td class="dataField">
                        <tmx_ctrl:selectBox property="scrollBoxNominal" />
                    </td>

                    <td class="dataLabel">
                        <tmx_ctrl:writeEntityLabel attrName="{@name}"
                                            entityClassName="com.tmx.as.entities.bill.operator.Operator"/>:
                    </td>
                    <td class="dataField">
                        <tmx_ctrl:selectBox property="scrollBoxOperator" />
                    </td>
                 </tr>
                 <tr>
                    <%--<td class="dataLabel">--%>
                        <%--<tmx_ctrl:writeEntityLabel attrName="nominal"--%>
                                            <%--entityClassName="com.tmx.as.entities.bill.voucher.Voucher"/>:--%>
                    <%--</td>--%>
                    <%--<td class="dataField">--%>
                        <%--<tmx_ctrl:selectBox property="scrollBoxNominal" />--%>
                    <%--</td>--%>
                    <td class="dataLabel">
                        <tmx_ctrl:writeEntityLabel attrName="{@name}"
                                            entityClassName="com.tmx.as.entities.bill.seller.Seller"/>:
                    </td>
                    <td class="dataField">
                        <tmx_ctrl:selectBox property="scrollBoxSeller" />
                    </td>                        

                    <td class="dataLabel">
                        <tmx_ctrl:writeEntityLabel attrName="{@name}"
                                            entityClassName="com.tmx.as.entities.bill.terminal.Terminal"/>:
                    </td>
                    <td class="dataField">
                        <tmx_ctrl:browseBox property="browseBoxTerminals" />
                    </td>
                </tr>
             </table>
            </td>
        </tr>
        <tr>
            <tmx_ctrl:grid property="gridVouchersReport"/>
        </tr>
    </table>
 </html:form>