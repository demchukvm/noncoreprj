package com.tmx.beng.medaccess.cyberplat;

import com.tmx.beng.medaccess.GateException;
import com.tmx.beng.medaccess.MediumAccess;
import com.tmx.beng.medaccess.MediumConnection;

public class CyberplatGateMediumAccess implements MediumAccess {

    public void init()
    {

    }

    public MediumConnection getConnection() throws GateException
    {
        return new CyberplatGateMediumConnection();
    }
}