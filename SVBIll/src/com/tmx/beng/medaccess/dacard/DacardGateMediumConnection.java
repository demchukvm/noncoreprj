package com.tmx.beng.medaccess.dacard;

import com.tmx.beng.httpsgate.HttpsGateException;
import com.tmx.beng.httpsgate.dacard.DacardGate;
import com.tmx.beng.base.BillingMessage;
import com.tmx.beng.medaccess.MediumConnection;

/**
  Medium connection to work with Dacard gate
 */
public class DacardGateMediumConnection implements MediumConnection {
    private DacardGate dacardGate = null;

    DacardGateMediumConnection() throws HttpsGateException {
        dacardGate = DacardGate.getInstance();
    }

    public void send(BillingMessage msg) {
        dacardGate.enqueueBillMessage(msg);
    }
}
