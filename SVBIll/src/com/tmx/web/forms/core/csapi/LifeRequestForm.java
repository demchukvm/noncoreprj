package com.tmx.web.forms.core.csapi;

import com.tmx.beng.access.Access;
import com.tmx.web.forms.xmlengine.BasicXMLPublisherForm;


/**
 */
public class LifeRequestForm extends BasicXMLPublisherForm {
    private Access billingEngineAccess = null;
    private String data = null;


    //------------------Properties

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }


    public Access getBillingEngineAccess() {
        return billingEngineAccess;
    }

    public void setBillingEngineAccess(Access billingEngineAccess) {
        this.billingEngineAccess = billingEngineAccess;
    }
}