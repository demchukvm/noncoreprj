package com.tmx.beng.testgate;

import com.tmx.beng.base.*;
import com.tmx.beng.access.Access;
import com.tmx.util.InitException;
import com.tmx.util.queue.ActionQueue;
import com.tmx.gate.datastream.base.GateContext;
import org.apache.log4j.Logger;

import java.util.Random;

/**
    Gate used to test all transaction life-cycle in billing.
    Emulates processing delay and erroneous status of response.
    See gate.xml for configuration. 
 */
public class TestGate extends BasicGate {
    // properties names constants
    private static final String TEST_GATE_NAME = "test_gate";
    private static final String PROP_INIT_HANDLER_COUNT = "handlers.initial";
    private static final String PROP_MAX_HANDLER_COUNT = "handlers.max";

    private static Logger logger = Logger.getLogger("gate." + TEST_GATE_NAME);
    private static TestGate testGate = null;
    private Access access = null;
    private boolean shutdown = false;
    private Random random;
    // queue
    private ActionQueue messageQueue = null;


    public static TestGate getInstance() throws TestGateException {
        if(testGate == null){
            logger.error("Attempt to obtain not inited billing engine");
            throw new TestGateException("err.test_gate.null_instance");
        }
        return testGate;
    }

    public static void init() throws InitException {
        logger.info("Start Test Gate initialization");
        testGate = new TestGate();
        testGate.init0();
        int initHandlerCount = Integer.parseInt(testGate.properties.getProperty(PROP_INIT_HANDLER_COUNT, "1"));
        int maxHandlerCount = Integer.parseInt(testGate.properties.getProperty(PROP_MAX_HANDLER_COUNT, "5"));
        testGate.messageQueue = new ActionQueue(initHandlerCount, maxHandlerCount);
        logger.info("Test Gate initialized");
    }

    public static void shutdown(boolean abort, final long timeout) throws InitException {
        logger.info("Start Test Gate Shutdown");
        if(testGate.messageQueue.actionCount() == 0 || abort){
            testGate.shutdown = true;
        }
        else{
            Runtime.getRuntime().addShutdownHook(new Thread(){
                public void run(){
                    try{
                        long lt = System.currentTimeMillis();
                        while(testGate.messageQueue.actionCount() != 0 &&
                            ((System.currentTimeMillis() - lt) < timeout)){
                            sleep(1000);
                        }
                        testGate.shutdown = true;
                    }catch(Throwable e){
                        e.printStackTrace();
                    }
                }
            });
        }
        logger.info("Test Gate shutdown");
    }

    private void init0()throws InitException{
        random = new Random();
        properties = GateContext.getInstance().getDatastreamProcessorConfig(TEST_GATE_NAME);
        access = obtainBillingEngineAccess();
    }

    public void enqueueBillMessage(BillingMessage billMessage){
        TestGateTask task = new TestGateTask(billMessage);
        messageQueue.enqueue(task);
    }

    /** Method to process given billing message
     * @param billingMessage billing message to process
     * @throws TestGateException test gate exception */
    public void runBillMessage(BillingMessage billingMessage) throws TestGateException{
        logger.info("Start processing request message "+billingMessage.getOperationName());
        BillingMessageImpl respMsg = new BillingMessageImpl();

        //operation specific processing
        if(BillingMessage.O_REFILL_PAYMENT.equals(billingMessage.getOperationName())){
            respMsg.setOperationName(BillingMessage.O_REFILL_PAYMENT_RESULT);
        }
        else{
            throw new TestGateException("Unsupported command");
        }

        //copy request id
        respMsg.setAttributeString(BillingMessage.REQUEST_ID, billingMessage.getAttributeString(BillingMessage.REQUEST_ID));

        //evaluate erroneous status
        final int errorPercent = Integer.parseInt(properties.getProperty("errorPercent", "0"));
        if(isErroneous(errorPercent)){
            respMsg.setStatus(StatusDictionary.TEST_ERROR, "test error resp");
            logger.info("Processed as ERRONEOUS");
        }
        else{
            respMsg.setStatus(StatusDictionary.STATUS_OK, "test ok resp");
            logger.info("Processed as OK");
        }

        //sleep to emulate processing delay
        int minProcessingDelay = Integer.parseInt(properties.getProperty("processingDelay.min", "0"));
        minProcessingDelay = (minProcessingDelay < 0) ? 0 : minProcessingDelay;
        int maxProcessingDelay = Integer.parseInt(properties.getProperty("processingDelay.max", "0"));
        maxProcessingDelay = (maxProcessingDelay < 0) ? 0 : (minProcessingDelay > maxProcessingDelay ? minProcessingDelay : maxProcessingDelay);
        if(maxProcessingDelay > 0){
            try{
                final int sleepTime = (random.nextInt(maxProcessingDelay - minProcessingDelay) + minProcessingDelay) * 1000;
                logger.info("Sleep for " + sleepTime + " ms");
                Thread.sleep(sleepTime);
            }
            catch(InterruptedException e){
                //ignore
            }
        }

        //send response to billing
        try{
            logger.info("Response to billing engine");
            access.obtainConnection().processAsync(respMsg);
        }
        catch(BillException e){
            logger.error("Failed to response to billing", e);
        }
        logger.info("Processing finished");
    }

    private boolean isErroneous(int errPercent){
        if(errPercent <=0 || errPercent > 100)
            return false;//on incorrect config all response is 'OK'
        return errPercent - random.nextInt(100) > 0;
    }
}
