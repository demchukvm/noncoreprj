package com.tmx.beng.medaccess.kyivstar;

import com.tmx.beng.medaccess.MediumConnection;
import com.tmx.beng.medaccess.GateException;
import com.tmx.beng.medaccess.MediumAccess;

public class KyivstarBonusCommissionGateMediumAccess implements MediumAccess {

    public void init() {
    }

    public MediumConnection getConnection() throws GateException {
        return new KyivstarBonusCommissionGateMediumConnection();
    }
}
