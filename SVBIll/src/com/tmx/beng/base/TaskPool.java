package com.tmx.beng.base;

import com.tmx.beng.base.memory.InMemoryTable;


public class TaskPool extends InMemoryTable {

    /** Create new task based on client request */
    public Task allocateTask(BillingMessage message) throws BillException{
        if(message == null)
            throw new BillException("err.bill.null_billmessage_to_allocate_task");

        Task task = new Task( (BillingMessageWritable)message );
        insert(task);
        return task;
    }

    void remove(Task task){
        delete(task.getId());
    }

    public int size() {
        return super.rows.size();        
    }

}
