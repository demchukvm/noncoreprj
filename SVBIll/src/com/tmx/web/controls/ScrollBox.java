package com.tmx.web.controls;

import java.util.List;
import java.util.Set;
import java.util.Iterator;
import java.util.ArrayList;

/**
 * Scroll Box control. Uses DNTML.
 */
public abstract class ScrollBox<T> extends BasicControl{
    private List<T> data = null;
    /** Key of the selected row. Set by bean auto-population. */
    private String selectedKey = null;
    /** Flag to identify wheather scrollbox supports NULL state. */
    private boolean nullable = false;
    /** Key to identify NULL state */
    public static final String NULL_KEY = "________NULL_KEY_________";
    /** Selected row Key suffix to render in HTML. Value should be the same as field name */
    public static String SELECTEDKEY_SUFFIX = "selectedKey";

    //-----------Methods for usage from action
    /**
     * Bind control to data list
     * */
    public void bind(List<T> data){
        //assign data list
        this.data = data;
        //select first row in list, if current selected is null
        if(selectedKey == null){
            if(nullable || data == null || data.size() <= 0){
                selectedKey = NULL_KEY;
            }
            else  if(data.size() > 0 && data.get(0) != null){
                selectedKey = getKey(data.get(0));
            }
        }
        notifyUpdate();
    }

    public boolean isNullKeySelected(){
        return getSelectedKey().equals(NULL_KEY);
    }

    protected ScrollBox(boolean nullable) {
        this.nullable = nullable;
    }

    protected ScrollBox() {
    }

    public void bind(Set<T> data){
        bind(new ArrayList<T>(data));
    }

    /**
     * Return true if data is bounded to scroll box  
     * */
    public boolean isBounded(){
        return (data != null);
    }

    /**
     * Get selected entry
     * */
    public Entry getSelectedEntry(){
        if(data == null || selectedKey == null || NULL_KEY.equals(selectedKey)){
            Entry entry = new Entry();
            entry.setKey(NULL_KEY);
            entry.setValue(null);
            return entry;
        }
        for (T aData : data) {
            String key = getKey(aData);
            if (selectedKey.equals(key)) {
                Entry entry = new Entry();
                entry.setKey(key);
                entry.setValue(getValue(aData));
                return entry;
            }
        }
        //if selected element is not found in scroll box data, then return NULL_KEY value
        Entry entry = new Entry();
        entry.setKey(NULL_KEY);
        entry.setValue(null);
        return entry;
    }

    public void setMandatory(boolean mandatory) {
        super.setMandatory(mandatory);
        if (mandatory)
            addValidationRule(createValidationRule_NOT_NULL_KEY());
    }

    public static ValidationRule createValidationRule_NOT_NULL_KEY(){
        return new ValidationRule("not_null_key", null) {
            public boolean validate(BasicControl control, Object[] args) {
                return (control instanceof ScrollBox) && !ScrollBox.NULL_KEY.equals(((ScrollBox)control).getSelectedKey());  
            }
        };
    }

    /** Get selected key. Useful for forms are saved in request context, they lost data between
     * requests and getSelectedEntry() returns null. */
    public String getSelectedKey(){
        return selectedKey;
    }

    public void setNullable(boolean nullable) {
        this.nullable = nullable;
    }

    //-----------Methods to be implemented in the particular successor
    /**
     * Returns Key fetched from given <code>dataListElement</code>
     * */
    protected abstract String getKey(T dataListElement);


    /**
     * Returns Value fetched from given <code>dataListElement</code>
     * */
    protected abstract String getValue(T dataListElement);



    //-----------Methods to be used from rendering Tag
    private int _currentElementId = -1;
    public void resetForRendering(){
        _currentElementId = -1;
    }

    public boolean hasNext(){
        int nullableCorrection = (nullable) ? 1 : 0;
        return (data != null) && (_currentElementId +1 < (data.size()) + nullableCorrection);
    }

    public Entry next(){
        Entry entry = new Entry();
        _currentElementId++;
        if(nullable && _currentElementId == 0){
            entry.setKey(NULL_KEY);
            entry.setValue(null);
        }
        else{
            int nullableCorrection = (nullable) ? -1 : 0;
            entry.setKey(getKey(data.get(_currentElementId + nullableCorrection)));
            entry.setValue(getValue(data.get(_currentElementId + nullableCorrection)));
        }
        return entry;
    }


    //-----------Methods for auto-population
    public void setSelectedKey(String key){
        selectedKey = key;
        if(!isUpdated()){
            notifyUpdate();
        }
    }

    /** Initialization */
    public void initialize() {
        /** Do nothing */
    }


    /**
     * @see com.tmx.web.controls.BasicControl
     */
    protected void safeReset(){       
    	if(nullable || data == null || data.size() <= 0){
        	selectedKey = NULL_KEY;
		}
        else if(data.size() > 0 && data.get(0) != null){
			selectedKey = getKey(data.get(0));
		}
    }


    public class Entry{
        private String key = null;
        private String value = null;

        //----------Properties

        public String getKey() {
            return key;
        }

        public void setKey(String key) {
            this.key = key;
        }

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }
    }


}
