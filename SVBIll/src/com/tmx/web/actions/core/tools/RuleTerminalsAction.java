/*//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                        L O C A L     V A R I A N T
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////*/

/*
package com.tmx.web.actions.core.tools;

import com.tmx.as.base.EntityManager;
import com.tmx.as.base.QueryParameterWrapper;
import com.tmx.as.blogic.ExclusiveManager;
import com.tmx.as.entities.bill.terminal.Terminal;
import com.tmx.as.entities.general.user.User;
import com.tmx.as.exceptions.DatabaseException;
import com.tmx.beng.base.ChannelResolver;
import com.tmx.web.actions.core.AbstractSvbillAction;
import com.tmx.web.controls.ScrollBox;
import com.tmx.web.controls.table.ControlManagerTable;
import com.tmx.web.forms.core.tools.RuleTerminalsForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForm;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

public class RuleTerminalsAction extends AbstractSvbillAction
{
    private final String FORWARD = "ruleTerminals";

    protected ActionForward defaultAction(ActionMapping mapping,
                                          ActionForm form,
                                          HttpServletRequest request,
                                          HttpServletResponse response,
                                          String methodName) throws Throwable {

        RuleTerminalsForm ruleTerminalsForm = (RuleTerminalsForm) form;

        String sellerCode = ruleTerminalsForm.getSbSellers().getSelectedKey();
        List data = new EntityManager().EXECUTE_QUERY(Terminal.class, "RetrieveAllBySellerCode", new QueryParameterWrapper[]{new QueryParameterWrapper("sellerCode", sellerCode)});
        ruleTerminalsForm.getTradePointsTable().setData(data);

        ruleTerminalsForm.setBeeline(false);
        ruleTerminalsForm.setKyivstar(false);
        ruleTerminalsForm.setLife(false);
        ruleTerminalsForm.setMts(false);
        ruleTerminalsForm.getEbCheckTerminal().setValue("");

        return mapping.findForward(FORWARD);
    }


    public ActionForward showTable(ActionMapping mapping,
                                        ActionForm form,
                                        HttpServletRequest request,
                                        HttpServletResponse response) throws Throwable {

        RuleTerminalsForm ruleTerminalsForm = (RuleTerminalsForm) form;

        String chosen = ruleTerminalsForm.getSbExclusiveTable().getSelectedKey();
        if(!chosen.equals(ScrollBox.NULL_KEY))
        {
            bindTableDate(ruleTerminalsForm.getExclusiveTable(), chosen);
            ruleTerminalsForm.setChosenTable(chosen);

            ruleTerminalsForm.getEbAddTerminal().setReadonly(false);
            ruleTerminalsForm.getbAddTerminal().setReadonly(false);
            ruleTerminalsForm.getbRemoveTerminals().setReadonly(false);
        }
        else
        {
            ruleTerminalsForm.setChosenTable("");

            ruleTerminalsForm.getEbAddTerminal().setReadonly(true);
            ruleTerminalsForm.getbAddTerminal().setReadonly(true);
            ruleTerminalsForm.getbRemoveTerminals().setReadonly(true);
        }
        return mapping.findForward(FORWARD);
    }

    private void bindTableDate(ControlManagerTable table, String chosenTable) throws Throwable
    {
        table.setData(new ExclusiveManager().getAllFromExclusiveTable(chosenTable));
    }

    private void reloadChannelResolver(String exclusive) throws DatabaseException
    {
        if(exclusive.equals("ExclusiveBeeline"))
            ChannelResolver.getInstance().loadBeelineExc();
        else if(exclusive.equals("ExclusiveKyivstar"))
            ChannelResolver.getInstance().loadKyivstarExc();
        else if(exclusive.equals("ExclusiveLife"))
            ChannelResolver.getInstance().loadLifeExc();
        else if(exclusive.equals("ExclusiveMTS"))
            ChannelResolver.getInstance().loadMtsExc();
    }

    public ActionForward addToChosenTable(ActionMapping mapping,
                                        ActionForm form,
                                        HttpServletRequest request,
                                        HttpServletResponse response) throws Throwable {

        RuleTerminalsForm ruleTerminalsForm = (RuleTerminalsForm) form;

        if(!ruleTerminalsForm.getEbAddTerminal().getValue().equals(""))
        {
            String chosen = ruleTerminalsForm.getChosenTable();
            if(!chosen.equals(ScrollBox.NULL_KEY))
            {
                String terminal = ruleTerminalsForm.getEbAddTerminal().getValue().trim();
                User user = getLoggedInUser(request);

                if(new ExclusiveManager().addTerminalToTable(chosen, terminal, user))
                {
                    bindTableDate(ruleTerminalsForm.getExclusiveTable(), chosen);
                    ruleTerminalsForm.getEbAddTerminal().reset();

                    reloadChannelResolver(chosen);
                }

            }
            ruleTerminalsForm.getEbAddTerminal().reset();
        }
        return mapping.findForward(FORWARD);
    }

    public ActionForward removeFromChosenTable(ActionMapping mapping,
                                        ActionForm form,
                                        HttpServletRequest request,
                                        HttpServletResponse response) throws Throwable {

        RuleTerminalsForm ruleTerminalsForm = (RuleTerminalsForm) form;

        String chosen = ruleTerminalsForm.getChosenTable();
        if(!chosen.equals(ScrollBox.NULL_KEY))
        {
            String terminalId = ruleTerminalsForm.getChosenTerminalId();
            if(!terminalId.equals(""))
            {
                new ExclusiveManager().removeTerminalFromTable(chosen, Long.parseLong(terminalId));

                bindTableDate(ruleTerminalsForm.getExclusiveTable(), chosen);
                ruleTerminalsForm.getEbAddTerminal().reset();

                ruleTerminalsForm.setChosenTerminalId("");

                reloadChannelResolver(chosen);
            }
        }
        return mapping.findForward(FORWARD);
    }

    public ActionForward info(ActionMapping mapping,
                                        ActionForm form,
                                        HttpServletRequest request,
                                        HttpServletResponse response) throws Throwable {

        RuleTerminalsForm ruleTerminalsForm = (RuleTerminalsForm) form;

        ruleTerminalsForm.setBeeline(false);
        ruleTerminalsForm.setKyivstar(false);
        ruleTerminalsForm.setLife(false);
        ruleTerminalsForm.setMts(false);

        String terminal = ruleTerminalsForm.getEbCheckTerminal().getValue().trim();

        ruleTerminalsForm.setBeeline(new ExclusiveManager().isBeelineExclusive(terminal));
        ruleTerminalsForm.setKyivstar(new ExclusiveManager().isKyivstarExclusive(terminal));
        ruleTerminalsForm.setLife(new ExclusiveManager().isLifeExclusive(terminal));
        ruleTerminalsForm.setMts(new ExclusiveManager().isMTSExclusive(terminal));

        return mapping.findForward(FORWARD);
    }

}
*/

/*//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                        S E R V E R     V A R I A N T
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////*/

package com.tmx.web.actions.core.tools;

import com.tmx.as.base.EntityManager;
import com.tmx.as.base.QueryParameterWrapper;
import com.tmx.as.blogic.ExclusiveManager;
import com.tmx.as.entities.bill.terminal.Terminal;
import com.tmx.as.entities.general.user.User;
import com.tmx.as.exceptions.DatabaseException;
import com.tmx.as.modules.SpringModule;
import com.tmx.beng.access.GateConnection;
import com.tmx.web.actions.core.AbstractSvbillAction;
import com.tmx.web.controls.ScrollBox;
import com.tmx.web.controls.table.ControlManagerTable;
import com.tmx.web.forms.core.tools.RuleTerminalsForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForm;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

public class RuleTerminalsAction extends AbstractSvbillAction {

    private final String FORWARD = "ruleTerminals";

    protected ActionForward defaultAction(ActionMapping mapping,
                                          ActionForm form,
                                          HttpServletRequest request,
                                          HttpServletResponse response,
                                          String methodName) throws Throwable {

        RuleTerminalsForm ruleTerminalsForm = (RuleTerminalsForm) form;

        String sellerCode = ruleTerminalsForm.getSbSellers().getSelectedKey();
        List data = new EntityManager().EXECUTE_QUERY(Terminal.class, "RetrieveAllBySellerCode", new QueryParameterWrapper[]{new QueryParameterWrapper("sellerCode", sellerCode)});
        ruleTerminalsForm.getTradePointsTable().setData(data);

        ruleTerminalsForm.setBeeline(false);
        ruleTerminalsForm.setKyivstar(false);
        ruleTerminalsForm.setLife(false);
        ruleTerminalsForm.setMts(false);
        ruleTerminalsForm.getEbCheckTerminal().setValue("");

        return mapping.findForward(FORWARD);
    }


    public ActionForward showTable(ActionMapping mapping,
                                        ActionForm form,
                                        HttpServletRequest request,
                                        HttpServletResponse response) throws Throwable {

        RuleTerminalsForm ruleTerminalsForm = (RuleTerminalsForm) form;

        String chosen = ruleTerminalsForm.getSbExclusiveTable().getSelectedKey();
        if(!chosen.equals(ScrollBox.NULL_KEY))
        {
            bindTableDate(ruleTerminalsForm.getExclusiveTable(), chosen);
            ruleTerminalsForm.setChosenTable(chosen);

            ruleTerminalsForm.getEbAddTerminal().setReadonly(false);
            ruleTerminalsForm.getbAddTerminal().setReadonly(false);
            ruleTerminalsForm.getbRemoveTerminals().setReadonly(false);
        }
        else
        {
            ruleTerminalsForm.setChosenTable("");

            ruleTerminalsForm.getEbAddTerminal().setReadonly(true);
            ruleTerminalsForm.getbAddTerminal().setReadonly(true);
            ruleTerminalsForm.getbRemoveTerminals().setReadonly(true);
        }

        return mapping.findForward(FORWARD);
    }

    private void bindTableDate(ControlManagerTable table, String chosenTable) throws Throwable
    {
        table.setData(new ExclusiveManager().getAllFromExclusiveTable(chosenTable));
    }

    private void reloadChannelResolver(String exclusive) throws DatabaseException
    {
        GateConnection conn = (GateConnection) SpringModule.getApplicationContext().getBean("gateConnection");
        if(exclusive.equals("ExclusiveBeeline"))
            conn.loadBeelineExc();
        else if(exclusive.equals("ExclusiveKyivstar"))
            conn.loadKyivstarExc();
        else if(exclusive.equals("ExclusiveLife"))
            conn.loadLifeExc();
        else if(exclusive.equals("ExclusiveMTS"))
            conn.loadMtsExc();
    }

    public ActionForward addToChosenTable(ActionMapping mapping,
                                        ActionForm form,
                                        HttpServletRequest request,
                                        HttpServletResponse response) throws Throwable {

        RuleTerminalsForm ruleTerminalsForm = (RuleTerminalsForm) form;

        if(!ruleTerminalsForm.getEbAddTerminal().getValue().equals(""))
        {
            String chosen = ruleTerminalsForm.getChosenTable();
            if(!chosen.equals(ScrollBox.NULL_KEY))
            {
                String terminal = ruleTerminalsForm.getEbAddTerminal().getValue().trim();
                User user = getLoggedInUser(request);

                if(new ExclusiveManager().addTerminalToTable(chosen, terminal, user))
                {
                    bindTableDate(ruleTerminalsForm.getExclusiveTable(), chosen);
                    ruleTerminalsForm.getEbAddTerminal().reset();

                    reloadChannelResolver(chosen);
                }

            }
            ruleTerminalsForm.getEbAddTerminal().reset();
        }

        return mapping.findForward(FORWARD);
    }

    public ActionForward removeFromChosenTable(ActionMapping mapping,
                                        ActionForm form,
                                        HttpServletRequest request,
                                        HttpServletResponse response) throws Throwable {

        RuleTerminalsForm ruleTerminalsForm = (RuleTerminalsForm) form;

        String chosen = ruleTerminalsForm.getChosenTable();
        if(!chosen.equals(ScrollBox.NULL_KEY))
        {
            String terminalId = ruleTerminalsForm.getChosenTerminalId();
            if(!terminalId.equals(""))
            {
                new ExclusiveManager().removeTerminalFromTable(chosen, Long.parseLong(terminalId));

                bindTableDate(ruleTerminalsForm.getExclusiveTable(), chosen);
                ruleTerminalsForm.getEbAddTerminal().reset();

                ruleTerminalsForm.setChosenTerminalId("");

                reloadChannelResolver(chosen);
            }
        }
        return mapping.findForward(FORWARD);
    }

    public ActionForward info(ActionMapping mapping,
                                        ActionForm form,
                                        HttpServletRequest request,
                                        HttpServletResponse response) throws Throwable {

        RuleTerminalsForm ruleTerminalsForm = (RuleTerminalsForm) form;

        ruleTerminalsForm.setBeeline(false);
        ruleTerminalsForm.setKyivstar(false);
        ruleTerminalsForm.setLife(false);
        ruleTerminalsForm.setMts(false);

        String terminal = ruleTerminalsForm.getEbCheckTerminal().getValue().trim();

        ruleTerminalsForm.setBeeline(new ExclusiveManager().isBeelineExclusive(terminal));
        ruleTerminalsForm.setKyivstar(new ExclusiveManager().isKyivstarExclusive(terminal));
        ruleTerminalsForm.setLife(new ExclusiveManager().isLifeExclusive(terminal));
        ruleTerminalsForm.setMts(new ExclusiveManager().isMTSExclusive(terminal));

        return mapping.findForward(FORWARD);
    }

}
