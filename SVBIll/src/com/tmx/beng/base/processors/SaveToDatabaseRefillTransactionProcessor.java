package com.tmx.beng.base.processors;

import com.tmx.beng.base.Task;
import com.tmx.beng.base.BillingMessage;
import com.tmx.beng.base.BillException;
import com.tmx.beng.base.cache.DBCache;

import java.util.List;

/**

 */
public class SaveToDatabaseRefillTransactionProcessor extends SaveToDatabaseTransactionProcessor{

    protected List getSavedTransactionEntitiesList(Task task, DBCache dbCache, BillingMessage msg) throws BillException {
        Task senderTask = task.getGlobalContext().getTransactionPool().lookupTask(
                msg.getAttributeString(BillingMessage.REQUEST_ID),
                BillingMessage.O_REFILL_PAYMENT
            );
        return (List)senderTask.getProcesedMessage().getAttribute(BillingMessage.ENTITY_LIST);
    }
}
