package com.tmx.as.exceptions;

import com.tmx.util.StructurizedException;
import java.util.Locale;

/**
 */
public class SecurityException extends StructurizedException {

    public SecurityException(String errKey, Throwable cause){
        super(errKey, cause);
    }

    public SecurityException(String errKey, Throwable cause, Locale locale){
        super(errKey, cause, locale);
    }

    public SecurityException(String errKey, String[] params, Throwable cause){
        super(errKey, params, cause);
    }

    public SecurityException(String errKey){
        super(errKey);
    }

    public SecurityException(String errKey, String[] params){
        super(errKey, params);
    }

    public SecurityException(String errKey, String[] params, Throwable cause, Locale locale){
        super(errKey, params, cause, locale);
    }
}

