package com.tmx.as.blogic.tariff;

import com.tmx.as.blogic.BlogicException;
import com.tmx.as.entities.bill.tariff.AbstractTariff;
import com.tmx.as.entities.bill.tariff.OperatorTariff;
import com.tmx.as.exceptions.DatabaseException;


public class OperatorTariffManager extends AbstractTariffManager{
    
    //template methods
    protected AbstractTariff getSpecificTariff(Long tariffId) throws DatabaseException, BlogicException {
        return (AbstractTariff) RETRIEVE(OperatorTariff.class, tariffId, new String[]{"operator","functionInstance.functionType"});
    }

    protected void saveSpecificTariff(AbstractTariff tariff) throws DatabaseException, BlogicException {
        OperatorTariff operatorTariff = (OperatorTariff) tariff;
        if(operatorTariff.getOperator() == null)
            throw new BlogicException("err.operator_tariff_manager.save_tariff.absent_operator_in_seller_tariff");
        if(operatorTariff.getOperator().getOperatorId() == null)
            throw new BlogicException("err.operator_tariff_manager.save_tariff.absent_operator_id_in_operator");
        SAVE(operatorTariff);
    }

    protected void deleteSpecificTariff(Long tariffId) throws DatabaseException, BlogicException {
        DELETE(OperatorTariff.class, tariffId);
    }

    protected void copySpecificTariff(Long tariffId, Long destOwnerId) throws DatabaseException, BlogicException {
        
    }

    protected void copyAllSpecificTariffs(Long srcOwnerId, Long destOwnerId) throws DatabaseException, BlogicException {
        
    }

}
