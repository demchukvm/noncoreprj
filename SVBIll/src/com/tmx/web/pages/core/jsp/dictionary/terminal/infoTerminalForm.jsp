<%@ page language="java" pageEncoding="UTF-8" contentType="text/html; charset=utf-8"
         errorPage="/core/jsp/message/jsperror.jsp" isELIgnored="false" %>
<%@ taglib uri="/WEB-INF/tld/struts-nested.tld" prefix="nested" %>
<%@ taglib uri="/WEB-INF/tld/tmx-controls.tld" prefix="tmx_ctrl" %>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>


<html:form action="/core/dictionary/terminal/infoTerminal">
<table border="0" width="100%">

    <!-- BUTTONS -->
    <tr>
        <td style="text-align: left">
            <%--<tmx_ctrl:button property="onApplyButton"/>--%>
            <tmx_ctrl:button property="onSaveButton"/>
            <tmx_ctrl:button property="onResetButton" onClick="javascript:document.forms[0].reset()"/>
            <tmx_ctrl:button property="onCancelButton"/>
        </td>
    </tr>

    
    <tr>
        <td valign="center" align="center" class="tabForm">  
        <table width="100%" border="0">
            <%--<!-- Form header -->--%>

            <tr>
                <td colspan="2">
                    <h4 class="dataLabel">
                        <bean:message bundle="core.labels" key="core.dictionary.terminal.infoterminalform.titlegeneral"/>
                    </h4>
                </td>
            </tr>

            <tr>
                <td class="dataLabel" width="15%">
                    <tmx_ctrl:writeEntityLabel attrName="terminalId"
                                               entityClassName="com.tmx.as.entities.bill.terminal.Terminal"/>:
                </td>
                <td class="dataField" width="35%">
                    <tmx_ctrl:editBox property="editBoxTerminalId"/>
                </td>

                <td class="dataLabel" width="15%">
                       <tmx_ctrl:writeEntityLabel attrName="serialNumber"
                                                  entityClassName="com.tmx.as.entities.bill.terminal.Terminal"/>:
                </td>
                <td class="dataField">
                    <tmx_ctrl:editBox property="editBoxSerialNumber"/>
                </td>

            </tr>

            <tr>

                <td class="dataLabel">
                    <tmx_ctrl:writeEntityLabel attrName="registrationDate"
                                               entityClassName="com.tmx.as.entities.bill.terminal.Terminal"/>:
                </td>
                <td class="dataField">
                    <tmx_ctrl:editBox property="editBoxRegistrationDate"/>
                </td>

                <td class="dataLabel">
                    <tmx_ctrl:writeEntityLabel attrName="hardwareSerialNumber"
                                               entityClassName="com.tmx.as.entities.bill.terminal.Terminal"/>:
                </td>
                <td class="dataField">
                    <tmx_ctrl:editBox property="editBoxHardwareSerialNumber"/>
                </td>
            </tr>

            <tr>

                <td class="dataLabel">
                    <tmx_ctrl:writeEntityLabel attrName="login"
                                               entityClassName="com.tmx.as.entities.bill.terminal.Terminal"/>:
                </td>
                <td class="dataField">
                    <tmx_ctrl:editBox property="editBoxLogin"/>
                </td>


                <td class="dataLabel">
                    <tmx_ctrl:writeEntityLabel attrName="blocked"
                                               entityClassName="com.tmx.as.entities.bill.terminal.Terminal"/>:
                </td>
                <td class="dataField" width="35%">
                       <tmx_ctrl:checkBox property="checkBoxBlocked" />
                </td>

            </tr>

            <tr>
                <td class="dataLabel">
                    <tmx_ctrl:writeEntityLabel attrName="password"
                                               entityClassName="com.tmx.as.entities.bill.terminal.Terminal"/>:
                </td>
                <td class="dataField">
                    <tmx_ctrl:passwordEditBox property="editBoxPassword"/>
                </td>

                <td class="dataLabel">
                    <tmx_ctrl:writeEntityLabel attrName="{@name}"
                                               entityClassName="com.tmx.as.entities.bill.seller.Seller"/>:
                </td>
                <td class="dataField">
                    <tmx_ctrl:selectBox property="scrollBoxSellers"/>
                </td>
            </tr>

            <tr>
                <td class="dataLabel">
                    <tmx_ctrl:writeEntityLabel attrName="password2"
                                               entityClassName="com.tmx.as.entities.bill.terminal.Terminal"/>:
                </td>
                <td class="dataField">
                    <tmx_ctrl:passwordEditBox property="editBoxPassword2"/>
                </td>
                <td class="dataLabel">
                    <tmx_ctrl:writeEntityLabel attrName="name"
                                               entityClassName="com.tmx.as.entities.bill.terminal.EquipmentType"/>:
                </td>
                <td class="dataField">
                    <tmx_ctrl:selectBox property="scrollBoxEqTypes"/>
                </td>
            </tr>
    </table>
        </td>
    </tr>

    <%--BILLING PARAMETERS--%>
    <tr>
        <td valign="center" align="center" class="tabForm">
            <table width="100%" border="0">
               <tr>

            <tr>
                    <td colspan="2">
                        <h4 class="dataLabel">
                            <tmx_ctrl:writeEntityLabel attrName="billingParameters"
                                               entityClassName="com.tmx.as.entities.bill.terminal.Terminal"/>:
                        </h4>                                                                                                                               
                    </td>
            </tr>               

            <tr>
                <td class="dataLabel">
                    <tmx_ctrl:writeEntityLabel attrName="isAttachRestriction"
                                               entityClassName="com.tmx.as.entities.bill.terminal.Terminal"/>:
                </td>
                <td class="dataField" width="35%">
                       <tmx_ctrl:checkBox property="checkBoxIsAttachRestriction" />
                </td>

                <td class="dataLabel">
                    <tmx_ctrl:writeEntityLabel attrName="isAttachTariffOnline"
                                               entityClassName="com.tmx.as.entities.bill.terminal.Terminal"/>:
                </td>
                <td class="dataField" width="35%">
                       <tmx_ctrl:checkBox property="checkBoxIsAttachTariffOnline" />
                </td>
            </tr>
                
            <tr>
                <td class="dataLabel">
                    <tmx_ctrl:writeEntityLabel attrName="isAttachTariffVoucher"
                                               entityClassName="com.tmx.as.entities.bill.terminal.Terminal"/>:
                </td>
                <td class="dataField" width="35%">
                       <tmx_ctrl:checkBox property="checkBoxIsAttachTariffVouchers" />
                </td>

            </tr>
        </table>
        </td>

    </tr>

    <tr>
        <td valign="center" align="center" class="tabForm">
            <table width="100%" border="0">
               <tr>
                    <td colspan="2">
                        <h4 class="dataLabel">
                            <bean:message bundle="core.labels" key="core.dictionary.terminal.infoterminalform.titleputlet"/>
                        </h4>
                    </td>
               </tr>

               <tr>
                    <td class="dataLabel">
                        <tmx_ctrl:writeEntityLabel attrName="name"
                                                   entityClassName="com.tmx.as.entities.bill.trade_putlet.TradePutlet"/>:
                    </td>
                    <td class="dataField">
                        <tmx_ctrl:editBox width="270px" property="editBoxTradePutlet"/>
                    </td>

                    <td class="dataLabel">
                        <tmx_ctrl:writeEntityLabel attrName="name"
                                                   entityClassName="com.tmx.as.entities.bill.trade_putlet.TradePutletType"/>:
                    </td>
                    <td class="dataField">
                        <tmx_ctrl:selectBox property="scrollBoxTradePutletType"/>
                    </td>
                </tr>

                <tr>
                    <td class="dataLabel">
                        <tmx_ctrl:writeEntityLabel attrName="directorEmail"
                                                   entityClassName="com.tmx.as.entities.bill.trade_putlet.TradePutlet"/>:
                    </td>
                    <td class="dataField">
                        <tmx_ctrl:editBox property="editBoxDirectorEmail"/>
                    </td>

                    <td class="dataLabel">
                        <tmx_ctrl:writeEntityLabel attrName="directorPhone"
                                                   entityClassName="com.tmx.as.entities.bill.trade_putlet.TradePutlet"/>:
                    </td>
                    <td class="dataField">
                        <tmx_ctrl:editBox property="editBoxDirectorPhone"/>
                    </td>
                </tr>
                  <tr>
                    <td class="dataLabel">
                        <tmx_ctrl:writeEntityLabel attrName="managerEmail"
                                                   entityClassName="com.tmx.as.entities.bill.trade_putlet.TradePutlet"/>:
                    </td>
                    <td class="dataField">
                        <tmx_ctrl:editBox property="editBoxManagerEmail"/>
                    </td>

                    <td class="dataLabel">
                        <tmx_ctrl:writeEntityLabel attrName="managerPhone"
                                                   entityClassName="com.tmx.as.entities.bill.trade_putlet.TradePutlet"/>:
                    </td>
                    <td class="dataField">
                        <tmx_ctrl:editBox property="editBoxManagerPhone"/>
                    </td>
                </tr>

                  <tr>
                    <td class="dataLabel">
                        <tmx_ctrl:writeEntityLabel attrName="addressLine1"
                                                   entityClassName="com.tmx.as.entities.bill.address.Address"/>:
                    </td>
                    <td class="dataField">
                        <tmx_ctrl:editBox property="editBoxAddress"/>
                    </td>

                    <td class="dataLabel">
                        <tmx_ctrl:writeEntityLabel attrName="region"
                                                   entityClassName="com.tmx.as.entities.bill.address.Address"/>:
                    </td>
                    <td class="dataField">
                        <tmx_ctrl:editBox property="editBoxRegion"/>
                    </td>
                </tr>

                <tr>
                    <td class="dataLabel">
                        <tmx_ctrl:writeEntityLabel attrName="city"
                                                   entityClassName="com.tmx.as.entities.bill.address.Address"/>:
                    </td>
                    <td class="dataField">
                        <tmx_ctrl:editBox property="editBoxCity"/>
                    </td>

                    <td class="dataLabel">
                        <tmx_ctrl:writeEntityLabel attrName="building"
                                                   entityClassName="com.tmx.as.entities.bill.address.Address"/>:
                    </td>
                    <td class="dataField">
                        <tmx_ctrl:editBox property="editBoxBuilding"/>
                    </td>
                </tr>

            </table>
        </td>
    </tr>

    <%--<!-- BUTTONS -->--%>
    <tr>
        <td style="text-align: left">
            <%--<tmx_ctrl:button property="onApplyButton"/>--%>
            <tmx_ctrl:button property="onSaveButton"/>
            <tmx_ctrl:button property="onResetButton" onClick="javascript:document.forms[0].reset()"/>
            <tmx_ctrl:button property="onCancelButton"/>
        </td>
    </tr>
</table>
</html:form>