package com.tmx.web.controls.table;

import java.util.*;

import com.tmx.as.resolver.*;
import com.tmx.as.exceptions.DatabaseException;
import com.tmx.as.base.criterion.CriterionWrapper;
import com.tmx.web.base.WebException;
import com.tmx.web.controls.BasicControl;
import com.tmx.web.controls.CriterionFactory;
import org.apache.log4j.Logger;

/** 
   Table web control
   <p/>
   Usage of CriterianWrapper in Table:
   <img src="doc-files/criterion_wrapper_used_in_table_cd_1_1.png">
   @example 
	<a href="doc-files/TableUsageExample1.txt">Example #1: Use controls defined inside form to parameterize table's CriterionWrapper</a><p/>
	<a href="doc-files/TableUsageExample2.txt">Example #2: Use controls defined inside form's panel to parameterize table's CriterionWrapper</a><p/>
	<a href="doc-files/TableUsageExample3.txt">Example #3: Update CriterionFactory parameters from Action</a><p/>

  
*/
public class Table extends BasicControl {
    private List data;
    private Query initialQuery;
    private Query query;
    protected Integer resultSize = new Integer(0);
    /** It is used to hold parameters string to be applied for the table's query */
    private String appliedParameters;
    /** This is name of action (in struts-config.xml notation)
     * which is used to refresh this table */
    private String refreshActionName;
    /** This optoinal command name id used inside refresh action
     * to refresh this table */
    private String refreshCommandName;
    private Logger logger = Logger.getLogger("web.Table");
    private FilterSet filterSet;
    private String filtersAppliedParameters;
    private CriterionFactory criterionFactory;

    public void initialize() {
        for(Iterator iterFilters = filterSet.getFilterNames().iterator(); iterFilters.hasNext();){
            Filter filter = filterSet.getFilter((String)iterFilters.next());
            if(filter.getFilterEnabler() != null){
                BasicControl ctrl =  (BasicControl)filter.getFilterEnabler();
                ctrl.init(getName()+".filterEnabler", getOwner(), getParent());
            }
            for(Iterator iterPrams = filter.getParameterNames().iterator(); iterPrams.hasNext();){
                Parameter param = filter.getParameter((String)iterPrams.next());
                BasicControl ctrl = param.getControl();
                if (ctrl != null)
                    ctrl.init(getName()+"."+param.getPath(), getOwner(), getParent());
            }
        }
    }

    public FilterSet getFilterSet() {
        return filterSet;
    }

    public void setFilterSet(FilterSet filterSet) {
        filterSet.setParent(this);
        this.filterSet = filterSet;
    }


    /** it is private to hide default constructor*/
    public Table(){
        query = new Query();
        clean();
        setFilterSet(new FilterSet());
    }

    /** Catch all posible programmatic exception and put them into log to prevent their handling
     * in form beans. */
    public Table(String queryString) {
        loadQuery(queryString);
        clean();
        setFilterSet(new FilterSet());
    }

    public void loadQuery(String queryString){
        initialQuery = null;
        try{
            initialQuery=new FunctionProcessor().parseQueryText(queryString);
        }
        catch(Throwable e){
            //programmatic error possible. Put into log
            WebException we = new WebException("err.table.incorrect_table_query", new String[]{queryString}, e);
            initialQuery = null;
            logger.error(we.getLocalizedMessage(), e);
        }
        if(initialQuery != null)
            query = (Query)initialQuery.clone();        
    }

    public List getData() {
        return data;
    }

    public void setData(List data) {
        this.data = data;
    }

    public Query getQuery() {
        return query;
    }

    public Integer getResultSize() {
        return resultSize;
    }

    public String getAppliedParameters() {
        return appliedParameters;
    }

    public void setAppliedParameters(String appliedParameters) {
        this.appliedParameters = appliedParameters;
    }

    public String getRefreshActionName() {
        return refreshActionName;
    }

    public void setRefreshActionName(String refreshActionName) {
        this.refreshActionName = refreshActionName;
    }

    public String getRefreshCommandName() {
        return refreshCommandName;
    }

    public CriterionFactory getCriterionFactory() {
        return criterionFactory;
    }

    public void setCriterionFactory(CriterionFactory criterionFactory) {
        this.criterionFactory = criterionFactory;
    }

    public void setRefreshCommandName(String refreshCommandName) {
        this.refreshCommandName = refreshCommandName;
    }

    private void resetFilters(){
        query.setFilters(initialQuery.getFilters());
    }

    public void clean(){
        data = new ArrayList();
        resultSize = new Integer(0);
    }

    public void refresh() throws ResolverException, FunctionSyntaxException, DatabaseException {
        //refresh filter set
        filterSet.refresh();

        //validate contorls included in table
        validate();

        QueryResolver queryResolver = new QueryResolver();

        //reset initial filters value (defined on initialization)
        resetFilters();
        String oldFiltersAppliedParameters = filtersAppliedParameters;
        String newFiltersAppliedParameters = filterSet.getAppliedParameters();
        /**Applies filters*/
        if(newFiltersAppliedParameters != null && !newFiltersAppliedParameters.equals("")){
            filtersAppliedParameters = newFiltersAppliedParameters;
            Query queryParams=new FunctionProcessor().parseQueryText(filtersAppliedParameters);
            query = queryResolver.applyQueryParameters(query,  queryParams);
        }
        /**Applies pagings, orderings*/
        if(appliedParameters != null){
            Query queryParams=new FunctionProcessor().parseQueryText(appliedParameters);
            query = queryResolver.applyQueryParameters(query,  queryParams);
        }
        //reset paging if filters is changed
        if(oldFiltersAppliedParameters != null &&
           filtersAppliedParameters != null &&
           !filtersAppliedParameters.equals(oldFiltersAppliedParameters)){

            query.setPageNumber(1);
            appliedParameters = null;//reset paging and orderings on filter change
        }
        // set criterion
        if(criterionFactory!=null){
            CriterionWrapper criterion=criterionFactory.createCriterion();
            query.setCriterions(initialQuery.getCriterions());
            if(criterion!=null) query.setCriterions(CriterionWrapper.appendToCriterions(query.getCriterions(),criterion));
        }

        QueryResult queryResult = queryResolver.executeQuery(query);
        if(!(queryResult.getRetrievedResult() instanceof List))
            throw new ResolverException("err.table.incorrect_query_result_type_for_table", new String[]{queryResult.getRetrievedResult().getClass().getName()});

        data = (List)queryResult.getRetrievedResult();
        query = queryResult.getExecutedQuery();
        resultSize = queryResult.getResultSize();
    }

    /**
     *  Validate all controls conteined in filter set
     * @return
     * @see com.tmx.web.controls.BasicControl
     */
    public boolean validate() {
        //validate Table as common control
        boolean valid = super.validate();
        //validate enclosed filterset's controls
        valid &= filterSet.validate();
        return valid;
    }

    /**
     * Reset Table.
     * 1) Reset all controls contained in the filterSet
     */
    protected void safeReset() {
        appliedParameters = null;
        filtersAppliedParameters = null;
        filterSet.safeReset();
    }
    
}
