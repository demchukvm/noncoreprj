package com.tmx.beng.base.processors;

import com.tmx.beng.base.*;
import com.tmx.beng.base.cache.DBCache;
import com.tmx.beng.medaccess.MediumAccess;
import com.tmx.beng.medaccess.MediumConnection;

import java.util.Random;

/**
 */
public class SendMediumProcessor extends TaskProcessor {

    public Task process0(Task task, DBCache dbCache, BillingMessageWritable msg) throws BillException {

        logger.info("Sending medium...");
        logger.info("-----------------------------------");
        logger.info("IS_REDIRECT = " + msg.getAttributeString(BillingMessage.IS_REDIRECT));
        logger.info("SERVICE_CODE = " + msg.getAttributeString(BillingMessage.SERVICE_CODE));

        try{
            /*String mediumName = MediumResolver.getInstance().getMediumAccessName(
                    msg.getAttributeString(BillingMessage.SERVICE_CODE),
                    configFile
            );*/


            /* **
            String mediumName = "";
            if(msg.getAttributeString(BillingMessage.IS_REDIRECT).equals("false"))
            {
                if(msg.getAttributeString(BillingMessage.SERVICE_CODE).equals("BeelineExc"))
                {
                    mediumName = "kyivstar_bonus_no_commission_gate";
                }
                else if(msg.getAttributeString(BillingMessage.SERVICE_CODE).equals("Beeline_FixConnect"))
                {
                    mediumName = "kyivstar_exclusive_gate";
                }
                else
                {
                    mediumName = MediumResolver.getInstance().getMediumAccessName(msg.getAttributeString(BillingMessage.SERVICE_CODE));
                }
            }
            else// if(!msg.getAttributeString(BillingMessage.IS_REDIRECT).equals("false"))
            {
                mediumName = MediumResolver.getInstance().getMediumAccessName(msg.getAttributeString(BillingMessage.SERVICE_CODE));
            }
            logger.info("mediumName = " + mediumName);
            ** */

            /*
            String mediumName = "";
            if(msg.getAttributeString(BillingMessage.IS_REDIRECT) == null)  // IS NULL
            {
                if(msg.getAttributeString(BillingMessage.SERVICE_CODE).equals("BeelineExc"))
                {
                    mediumName = "kyivstar_bonus_no_commission_gate";
                }
                else if(msg.getAttributeString(BillingMessage.SERVICE_CODE).equals("Beeline_FixConnect"))
                {
                    mediumName = "kyivstar_exclusive_gate";
                }
                else
                {
                    mediumName = MediumResolver.getInstance().getMediumAccessName(msg.getAttributeString(BillingMessage.SERVICE_CODE));
                }
            }
            else // NOT NULL
            {
                if(msg.getAttributeString(BillingMessage.IS_REDIRECT).equals("false")) // IS_REDIRECT = false
                {
                    if(msg.getAttributeString(BillingMessage.SERVICE_CODE).equals("BeelineExc"))
                    {
                        mediumName = "kyivstar_bonus_no_commission_gate";
                    }
                    else if(msg.getAttributeString(BillingMessage.SERVICE_CODE).equals("Beeline_FixConnect"))
                    {
                        mediumName = "kyivstar_exclusive_gate";
                    }
                    else
                    {
                        mediumName = MediumResolver.getInstance().getMediumAccessName(msg.getAttributeString(BillingMessage.SERVICE_CODE));
                    }
                }
                else // IS_REDIRECT = true
                {
                    mediumName = MediumResolver.getInstance().getMediumAccessName(msg.getAttributeString(BillingMessage.SERVICE_CODE));
                }
            }

            */

            String mediumName = MediumResolver.getInstance().getMediumAccessName(msg.getAttributeString(BillingMessage.SERVICE_CODE));










            //choose instance name                                                  .
            if(isMultiInstanceGate(mediumName))
                mediumName = chooseGateInstanceAccessName(mediumName);

            MediumAccess mediumAcess = task.getGlobalContext().getMediumAccessPool().getMediumAccess(mediumName);
            if(mediumAcess == null) {
                msg.setStatusCode(StatusDictionary.MEDIUM_CONNECT_ERROR);
                throw new BillException("err.send_medium_processor.medium_access_not_found", new String[]{mediumName});
            }
            MediumConnection conn = mediumAcess.getConnection();
            if(conn == null) {
                msg.setStatusCode(StatusDictionary.MEDIUM_CONNECT_ERROR);
                throw new BillException("err.send_medium_processor.no_medium_connection", new String[]{mediumName});
            }
            conn.send(msg);
        }
        catch(BillException e){
            msg.setStatusCode(StatusDictionary.MEDIUM_CONNECT_ERROR);
            throw e;
        }
        catch(Throwable e){
            msg.setStatusCode(StatusDictionary.MEDIUM_CONNECT_ERROR);
            throw new BillException(e.getLocalizedMessage(), e);
        }
        return task;
    }

    private static final String NAMES_DELIMITER = ",";

    private boolean isMultiInstanceGate(String instancesNames){
        return instancesNames.indexOf(NAMES_DELIMITER) > 0;
    }

    /** Choose the instance name of multinstance gate on the random basis.
     * @param instancesNames the list of gate names separated by NAMES_DELIMITER.
     * @return String the name of chosen instance from given list. */
    private String chooseGateInstanceAccessName(String instancesNames){
        final String[] names = instancesNames.trim().split(NAMES_DELIMITER);
        Random random = new Random();
        int nameIdx = Math.abs(random.nextInt(names.length));
        return names[nameIdx].trim();
    }
}