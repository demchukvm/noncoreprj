package com.tmx.engines.reportengine.aggrtable;

import java.util.*;
import java.io.PrintStream;

/**
 Incapsulates Object[][] array and provides methods to decrease
 this array sizes. These methods are used during the table aggregation
 instead of creating new instance of array with decreased sizes
 and its data populating.

 Actually colums and rows do not removed from array. Wrapped2DArray
 just uses index projection to hide such columns.
 */
public class Wrapped2DArray {
    /** Indexes projection. Client uses index of element from
     * this projection and Wrapped2DArray fetchs real index in
     * wrapped array. */
    private List rowIndexProjection = null;
    private List colIndexProjection = null;
    private Map rows = null;


    public Wrapped2DArray(int rowsCount, int colsCount) {
        rows = new HashMap(rowsCount);
        rowIndexProjection = new ArrayList();
        colIndexProjection = new ArrayList();
        for(int i=0; i<rowsCount; i++){
            String key = String.valueOf(i);
            rowIndexProjection.add(key);
            rows.put(key, new HashMap());            
        }
        for(int i=0; i<colsCount; i++){
            colIndexProjection.add(String.valueOf(i));
        }
    }

    public Object get(int rowIdx, int colIdx){
        return ((Map)rows.get(rowIndexProjection.get(rowIdx))).
                get(colIndexProjection.get(colIdx));
    }

    public void set(int rowIdx, int colIdx, Object value){
        ((Map)rows.get(rowIndexProjection.get(rowIdx))).
                put(colIndexProjection.get(colIdx), value);
    }

    /** Use this method to move data from one cell to another for table aggreagtion
     * with memory release. */
    public void move(int fromRowIdx, int fromColIdx, int toRowIdx, int toColIdx){
        //set(toRowIdx, toColIdx, get(fromRowIdx, fromColIdx));
        Map fromColumn = (Map)rows.get(rowIndexProjection.get(fromRowIdx));
        set(toRowIdx, toColIdx, fromColumn.get(colIndexProjection.get(fromColIdx)));
        set(fromRowIdx, fromColIdx, null);
    }

    public void deleteRow(int rowIdx){
        rowIndexProjection.remove(rowIdx);
    }

    public void deleteColumn(int colIdx){
        colIndexProjection.remove(colIdx);
    }

    public int getRowsCount(){
        return rowIndexProjection.size();
    }

    public int getColsCount(){
        return colIndexProjection.size();
    }

    public Object[] getRow(int rowIdx){
        Object[] row = new Object[getColsCount()];
        for(int colIdx=0; colIdx<getColsCount(); colIdx++)
            row[colIdx] = get(rowIdx, colIdx);
        return row;
    }

    //------------------debug methods
    public void writeTable(PrintStream out){
        for(int i=0; i<getRowsCount(); i++){
            for(int j=0; j<getColsCount(); j++)
                out.print(get(i, j) + " ");
            out.println();
        }
    }

    private long _printElapsedTime(long prevTime, String phaseName){
        long currTime = System.currentTimeMillis();
        System.out.println(phaseName + " = " + (currTime - prevTime));
        return currTime;
    }
}
