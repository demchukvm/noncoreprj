package com.tmx.web.taglib.html;

import org.apache.struts.taglib.TagUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.JspException;
import java.util.Locale;

import com.tmx.web.taglib.base.LocalizableTag;


/**
 * Created by IntelliJ IDEA.
 * User: galyamov
 * Date: 12/6/2006
 * Time: 15:45:30
 * To change this template use File | Settings | File Templates.
 */
public class WriteHelpHrefTag extends LocalizableTag /** RM: commented. BaseHandlerTag */ {

    protected String helpPageUrl;
    protected String text = null;

    //--------------------Properties
    public String getHelpPageUrl() {
        return helpPageUrl;
    }

    public void setHelpPageUrl(String helpPageUrl) {
        this.helpPageUrl = helpPageUrl;
    }

    //------------------------------Parsing events
    public int doStartTmxTag() throws JspException {
        String labelHTML = renderLabelHTML();
        TagUtils.getInstance().write(pageContext, labelHTML);
        text = null;
        return (EVAL_BODY_TAG);
    }

    public int doAfterBody() throws JspException {
        if (super.bodyContent != null) {
            String value = super.bodyContent.getString().trim();
            if (value.length() > 0) {
                text = value;
            }
        }
        return 0;
    }

    //------------------------------Rendering methods
    private String renderLabelHTML() {
        Locale currentLocale = getLocale();
        HttpServletRequest httpServletRequest = (HttpServletRequest) pageContext.getRequest();
        StringBuffer resultHTML = new StringBuffer();
        resultHTML.append("<a href=\"")
                .append(httpServletRequest.getContextPath()).append("/")
                .append(helpPageUrl.substring(0, helpPageUrl.lastIndexOf(".")))
                .append("_").append(currentLocale.getLanguage())
                .append(helpPageUrl.substring(helpPageUrl.lastIndexOf("."), helpPageUrl.length()))
                .append("\">");
        return resultHTML.toString();
    }

    public int doEndTmxTag() throws JspException {
        StringBuffer results = new StringBuffer();
        if (text != null) {
            results.append(text);
        }
        results.append("</a>");
        TagUtils.getInstance().write(super.pageContext, results.toString());
        return 6;
    }
}
