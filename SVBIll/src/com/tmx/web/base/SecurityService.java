package com.tmx.web.base;

import com.tmx.as.blogic.UserManager;
import com.tmx.as.entities.general.user.User;
import com.tmx.as.entities.general.organization.Organization;
import com.tmx.as.entities.general.role.Role;
import com.tmx.as.exceptions.DatabaseException;
import com.tmx.as.exceptions.SecurityException;
import com.tmx.util.XMLUtil;
import com.tmx.util.InitException;
import com.tmx.web.base.authentication.CommandFragmentMap;
import com.tmx.web.base.authentication.FragmentMapFactory;
import com.sun.org.apache.xpath.internal.XPathAPI;
import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.net.URI;
import java.util.HashMap;
import java.util.Locale;
import java.util.Vector;


/**
 * SecurityService provides login and realms management.
 * Used as singleton. Is held in the application scope by ApplicationEnvironment.
 */
public class SecurityService implements Service {
    /**
     * Path to the realms config file
     */
    private String realmFilePath = null;
    /**
     * To hold pairs: {String roleName}{Role role}
     */
    private HashMap roles = new HashMap();
    /**
     * Defines the default name of the default role. Default role could be assigned in realms file using /role@default="true" attribute.
     */
    private String defaultRoleName = null;

    private Logger logger = Logger.getLogger("web.SecurityService");

    private FragmentMapFactory fragmentMapFactory;

    public void init(ServletContext context) throws InitException {
        try {
            //Get the relms filename
            String realmFile = ApplicationEnvironment.getInitParameter("security.realm_file", null);
            if (realmFile == null)
                throw new InitException("err.security_service.realm_file_not_specified", Locale.getDefault());
            realmFilePath = System.getProperty("tmx.home") + realmFile;

            //Load realms from XML
            Document doc = XMLUtil.getDocumentFromFile(realmFilePath);
            String xpathRole = "/realms/role";
            NodeList rolesNode = XPathAPI.selectNodeList(doc, xpathRole);
            for (int i = 0; i < rolesNode.getLength(); i++) {
                Node roleNode = rolesNode.item(i);
                if (roleNode.getNodeType() == Node.ATTRIBUTE_NODE)
                    continue;
                RoleHandler role = new RoleHandler(((Element) roleNode).getAttribute("name"));

                //Is role default?
                String xPathDefaultRole = "/realms/role[@name='" + role.name + "']/@default";
                Node isDefault = XPathAPI.selectSingleNode(doc, xPathDefaultRole);
                if (isDefault != null)
                    defaultRoleName = role.name;

                //Load allow-all or deny-all
                String xPathAllowAll = "/realms/role[@name='" + role.name + "']/allow-all";
                String xPathDenyAll = "/realms/role[@name='" + role.name + "']/deny-all";
                Node allowAll = XPathAPI.selectSingleNode(doc, xPathAllowAll);
                Node denyAll = XPathAPI.selectSingleNode(doc, xPathDenyAll);
                if (allowAll != null && denyAll != null)
                    throw new InitException("err.security_service.both_allowall_denyall", Locale.getDefault());
                if (allowAll != null)
                    role.isDefaultDeny = false;
                if (denyAll != null)
                    role.isDefaultDeny = true;

                //Load allowed URLs
                String xPathAllow = "/realms/role[@name='" + role.name + "']/allow/path";
                NodeList allows = XPathAPI.selectNodeList(doc, xPathAllow);
                for (int j = 0; j < allows.getLength(); j++) {
                    Node pathNode = allows.item(j);
                    if (pathNode.getNodeType() == Node.ATTRIBUTE_NODE)
                        continue;
                    String path = ((Element) pathNode).getChildNodes().item(0).getNodeValue();//text node
                    role.addAllow(path);
                }

                //Load denied URLs
                String xPathDeny = "/realms/role[@name='" + role.name + "']/deny/path";
                NodeList denies = XPathAPI.selectNodeList(doc, xPathDeny);
                for (int j = 0; j < denies.getLength(); j++) {
                    Node pathNode = denies.item(j);
                    if (pathNode.getNodeType() == Node.ATTRIBUTE_NODE)
                        continue;
                    String path = ((Element) pathNode).getChildNodes().item(0).getNodeValue();//text node
                    role.addDeny(path);
                }
                roles.put(role.name, role);

                fragmentMapFactory = FragmentMapFactory.getInstance();//TODO AN
            }

            if (defaultRoleName == null)
                throw new InitException("err.security_service.default_role_not_specified", Locale.getDefault());

        }
        catch (Exception e) {
            throw new InitException("err.security_service.initialization_failed", e, Locale.getDefault());
        }
    }

    /**
     * Check is session valid.
     */
    public boolean isSessionValid(HttpServletRequest request) {
        HttpSession session = request.getSession(false);
        boolean ret = false;
        if (session != null) {
            ret = SessionEnvironment.getAttr(request.getSession(), SessionEnvironment.USER) != null;
        }
        return ret;
    }

    /**
     * Check for login, password, action = login.do
     */
    public boolean isLoginRequested(HttpServletRequest req) {
        if ((req.getParameter("login") != null) &&
                (req.getParameter("password") != null) &&
                (req.getRequestURI().indexOf("login.do") >= 0))
            return true;
        else
            return false;
    }

    /**
     * Public access is access for user from "norole" group.
     * All users before login belongs to this group.
     */
    public boolean isPublicAccessGranted(HttpServletRequest request) {
        return isPublicAccessGranted0(request, defaultRoleName);
    }

    /**
     * Checks access for User to the URL. USer role and URL are got from given
     * HttpServletRequest
     */
    public boolean isAccessGranted(HttpServletRequest request) {
        if (request == null)
            return false;

        User user = (User) SessionEnvironment.getAttr(request.getSession(), SessionEnvironment.USER);
        if (user == null)
            return false;
        com.tmx.as.entities.general.role.Role role = user.getRole();
        String roleName = defaultRoleName;
        if (role != null)
            roleName = role.getKey();
        if (roleName == null)
            roleName = defaultRoleName;

        return isPublicAccessGranted0(request, roleName);
    }

    private boolean isPublicAccessGranted0(HttpServletRequest request, String roleName) {
        if (request == null)
            return false;
        String path = getStrippedPath(request);
        if (path == null)
            return false;
        RoleHandler role = (RoleHandler) roles.get(roleName);
        if (role == null)
            return false;
        return role.hasAccess(path);
    }

    private String getStrippedPath(HttpServletRequest request) {
        String strippedPath = null;
        String fullPath = null;
        try {
            fullPath = request.getRequestURI();
            URI uri = new URI(fullPath);
            strippedPath = uri.getPath();
            String appBase = ApplicationEnvironment.getInitParameter("app_base", null);
            int appContextLength = (appBase != null) ? appBase.length() : 0;
            strippedPath = strippedPath.substring(appContextLength);
            if (!strippedPath.startsWith("/"))
                strippedPath = "/" + strippedPath;
        }
        catch (Exception e) {
            logger.error("Failed to get stripped path from URL=" + fullPath);
        }
        return strippedPath;
    }

    public CommandFragmentMap getCommandFragmentMap(HttpSession session){
        String roleName = getLoggedInUserRoleName(session);
        //get map from session
        return null;
    }

    public void initFragmentMap(HttpSession session){
        //add map to the session for curent session
    }

    public User login(String login, String password, HttpSession session) throws DatabaseException, SecurityException {
        User loggedInUser = new UserManager().login(login, password);
        SessionEnvironment.setAttr(session, SessionEnvironment.USER, loggedInUser);
        return loggedInUser;
    }

    public void logout(HttpSession session){
        //clear global filters
        UserContextForm userContextForm = (UserContextForm)SessionEnvironment.getAttr(session, SessionEnvironment.USER_CONTEXT_FORM);
        userContextForm.getGlobalFiltersHelper().clean();
    }


    public void updateCurrentUser(Long userId, HttpSession session) throws DatabaseException {
        User currentUser = (User) SessionEnvironment.getAttr(session, SessionEnvironment.USER);
        if (currentUser != null && currentUser.getUserId().longValue() == userId.longValue()) {
            User updatedUser = new UserManager().getUser( userId );
            SessionEnvironment.setAttr(session, SessionEnvironment.USER, updatedUser);
        }
    }

    public User getLoggedInUser(HttpSession session) throws SecurityException{
        User currentUser = (User) SessionEnvironment.getAttr(session, SessionEnvironment.USER);
        if(currentUser == null)
            throw new SecurityException("err.security_service.session_expired");
        return currentUser;
    }

    public String getLoggedInUserRoleName(HttpSession session){
        try{
            User userActor = getLoggedInUser(session);
            Role role = userActor.getRole();
            return role == null ? Role.GUEST : role.getKey();
        }
        catch(SecurityException e){
            return Role.GUEST;
        }
    }

    public Organization getCurrentOrganization(HttpSession session) throws SecurityException {
        User currentUser = getLoggedInUser(session);
        if(currentUser == null)
            throw new SecurityException("err.security_service.session_expired");
        return currentUser.getOrganization();
    }

    /**
     * Role configuration handler
     */
    private class RoleHandler {
        String name = null;
        /**
         * isDefaultDeny = true    -> DenyAll
         * isDefaultDeny = false   -> AllowAll
         */
        boolean isDefaultDeny = true;
        Vector allow = null;
        Vector deny = null;

        RoleHandler(String roleName) {
            name = roleName;
            allow = new Vector();
            deny = new Vector();
        }

        void addAllow(String path) {
            allow.add(path);
        }

        void addDeny(String path) {
            deny.add(path);
        }

        boolean hasAccess(String path) {
            if (isDefaultDeny)
                return allow.contains(path); //Look for path between allowed
            else
                return !deny.contains(path); //Look for path between denied
        }

    }


}
