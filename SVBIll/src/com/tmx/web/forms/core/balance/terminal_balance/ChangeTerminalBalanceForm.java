package com.tmx.web.forms.core.balance.terminal_balance;

import com.tmx.web.forms.core.balance.AbstractChangeBalanceForm;
import com.tmx.as.entities.bill.balance.AbstractBalance;
import com.tmx.as.entities.bill.balance.OperatorBalance;
import com.tmx.as.entities.bill.balance.TerminalBalance;


public class ChangeTerminalBalanceForm extends AbstractChangeBalanceForm {

    protected void init() {
        super.init();

        getOnCancelButton().setAction("/core/balance/terminal_balance/infoTerminalBalance");
        getOnChangeButton().setAction("/core/balance/terminal_balance/changeTerminalBalance");
    }

    public void bindData(AbstractBalance abstractBalance) {
        super.bindData(abstractBalance);
        TerminalBalance terminalBalance = (TerminalBalance) abstractBalance;
        editBoxOwnerName.setValue(terminalBalance.getTerminal().getSerialNumber());
    }
}
