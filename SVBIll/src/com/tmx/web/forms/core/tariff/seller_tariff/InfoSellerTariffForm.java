package com.tmx.web.forms.core.tariff.seller_tariff;

import com.tmx.as.blogic.SellerManager;
import com.tmx.as.entities.bill.seller.Seller;
import com.tmx.as.entities.bill.tariff.AbstractTariff;
import com.tmx.as.entities.bill.tariff.SellerTariff;
import com.tmx.as.entities.general.user.User;
import com.tmx.as.exceptions.DatabaseException;
import com.tmx.web.controls.PopulateUtil;
import com.tmx.web.controls.ScrollBox;
import com.tmx.web.forms.core.tariff.AbstractInfoTariffForm;
import com.tmx.web.forms.core.base.AbstractFormFactory;

import java.util.Date;


public class InfoSellerTariffForm extends AbstractInfoTariffForm {

    //maybe mandutory???
    private ScrollBox scrollBoxSellers;

    protected void init() {
        super.init();
        initButtons();
        initScrollBoxes();
        initParameterButtonsAction();
        initTables();
        initBrowseBoxes();
    }

    private void initTables(){
        if(getFuncParametrsTable() != null){
            getFuncParametrsTable().setRefreshActionName("/core/tariff/seller_tariff/infoSellerTariff");
        }
    }

    private void initScrollBoxes(){
        scrollBoxSellers = new ScrollBox(){
            protected String getKey(Object dataListElement) {
                return ((Seller)dataListElement).getSellerId().toString();
            }
            protected String getValue(Object dataListElement) {
                return ((Seller)dataListElement).getName();
            }
        };
    }

    private void initBrowseBoxes(){
        getBrowseBoxFunctionTypes().setAction("/core/tariff/seller_tariff/browseSellerFuncTypesAction");
    }

    private void initButtons(){
        getOnSaveButton().setAction("/core/tariff/seller_tariff/infoSellerTariff");
        getOnApplyButton().setAction("/core/tariff/seller_tariff/infoSellerTariff");
        getOnCancelButton().setAction("/core/tariff/seller_tariff/manageSellerTariffs");
        
        getOnRefreshParamButton().setAction("/core/tariff/seller_tariff/infoSellerTariff");
        getOnCreateParamButton().setAction("/core/tariff/seller_tariff/infoSellerTariff");
        getOnEditParamButton().setAction("/core/tariff/seller_tariff/sellerTariffParameter");
        getOnDeleteParamButton().setAction("/core/tariff/seller_tariff/sellerTariffParameter");
    }

    protected AbstractTariff populateFromSpecificControls() {
        SellerTariff tariff = new SellerTariff();
        tariff.setSeller(getSellerInstance());
        return tariff;
    }

    private Seller getSellerInstance() {
        Seller seller = new Seller();
        seller.setSellerId(PopulateUtil.getLongKey(scrollBoxSellers));
        return seller;
    }

    private void initParameterButtonsAction(){
        setOnApplyButtonAction("/core/tariff/seller_tariff/sellerTariffParameter");
        setOnCancelButtonAction("/core/tariff/seller_tariff/infoSellerTariff");
        setOnSaveButtonAction("/core/tariff/seller_tariff/sellerTariffParameter");
    }

    protected void bindSpecificData(AbstractTariff tariff, User loggedInUser) throws DatabaseException {
        SellerTariff sellerTariff = (SellerTariff) tariff;
        AbstractFormFactory.getInstance(loggedInUser).setUpForm(this);
        if(sellerTariff.getSeller()!=null && sellerTariff.getSeller().getSellerId()!=null)//if seller is present
            scrollBoxSellers.setSelectedKey(sellerTariff.getSeller().getSellerId().toString());
        scrollBoxSellers.setReadonly(true);
    }

    protected void bindSpecificData(User loggedInUser) throws DatabaseException {
        scrollBoxSellers.bind(new SellerManager().getNestedSellersInSeller(loggedInUser));
        scrollBoxSellers.setReadonly(false);
    }

    public ScrollBox getScrollBoxSellers() {
        return scrollBoxSellers;
    }

    public void setScrollBoxSellers(ScrollBox scrollBoxSellers) {
        this.scrollBoxSellers = scrollBoxSellers;
    }
    
}
