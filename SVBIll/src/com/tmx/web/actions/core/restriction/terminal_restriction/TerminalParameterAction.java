package com.tmx.web.actions.core.restriction.terminal_restriction;

import com.tmx.web.actions.core.restriction.AbstractRestrictionParameterAction;
import com.tmx.web.forms.core.restriction.AbstractInfoRestrictionForm;
import com.tmx.web.forms.core.restriction.terminal_restriction.InfoTerminalRestrictionForm;
import com.tmx.web.base.WebException;
import com.tmx.web.base.SessionEnvironment;
import com.tmx.as.blogic.balance.AbstractBalanceManager;
import com.tmx.as.blogic.balance.OperatorBalanceManager;
import com.tmx.as.blogic.balance.SellerBalanceManager;
import com.tmx.util.InitException;
import com.tmx.beng.base.BillException;
import org.apache.struts.action.ActionMapping;

import javax.servlet.http.HttpServletRequest;


public class TerminalParameterAction extends AbstractRestrictionParameterAction {

    protected AbstractBalanceManager getBalanceManager() throws InitException, BillException {
        return new SellerBalanceManager();
    }

    protected AbstractInfoRestrictionForm getInfoRestrictionForm(ActionMapping mapping, HttpServletRequest request) throws WebException {
        return (AbstractInfoRestrictionForm) SessionEnvironment.allocateForm(
                InfoTerminalRestrictionForm.class, "core.restriction.terminal_restriction.infoTerminalRestrictionForm", mapping, request);

    }
}
