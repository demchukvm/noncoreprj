CREATE SEQUENCE svbill.terminal_seq
START WITH 1 
INCREMENT BY 1
NOCACHE;
/
ALTER TABLE svbill.TR_TERMINAL ADD CONSTRAINT terminal_sn
UNIQUE (TR_SerialNumber) USING INDEX TABLESPACE INDX
STORAGE (INITIAL 10K NEXT 5K PCTINCREASE 0 MAXEXTENTS UNLIMITED)
/
ALTER TABLE svbill.TR_TERMINAL MODIFY TR_SerialNumber NOT NULL;
/