package com.tmx.beng.httpsgate.dacard.pdu;

import org.apache.log4j.Logger;

import java.io.InputStream;
import java.io.OutputStream;
import java.io.IOException;

import com.tmx.util.HexConverter;

/**

 */
public class DataElement<T> {
    private DataElementName name;
    private DataElementType type;
    private T value;
    //utility classes
    private Logger logger;
    private HexConverter hexConv;


    DataElement(Logger logger) {
        this.logger = logger;
        this.hexConv = new HexConverter();
    }

    /** Read and parse data from input stream. All data produced during the parsing is
     * applied to this instance of DataElement.
     * Returns the amount of read bytes.
     * @param is input stream to take data from
     * @return amount of read bytes.
     * @throws DacardPadException on parsing error. */
    int parse(InputStream is) throws DacardPadException{
        try{
            int readBytesAmount;
            //1. parse type
            byte[] buffer = new byte[2];
            readBytesAmount = is.read(buffer);
            type = DataElementType.valueOf(buffer[0]);
            //2. parse name length
            final byte nameSize = buffer[1];
            //3. parse name
            name = DataElementName.parse(is, nameSize);
            readBytesAmount = readBytesAmount + nameSize;
            //4. parse data length
            buffer = new byte[2];
            readBytesAmount = readBytesAmount + is.read(buffer);
            final short dataLength = ByteUtil.convertLittleEndianToShort(buffer);
            //5. read data
            value = (T)type.getPad().parse(is, dataLength);
            readBytesAmount = readBytesAmount + dataLength;

            StringBuffer log = new StringBuffer("DataElement[").
                append("TYPE: ").append(hexConv.getStringFromHex(new byte[]{type.getCode()})).append("; ").
                append("NAME_LENGTH: ").append(name.getSize()).append("; ").
                append("NAME: ").append(name.getValue()).append("; ").
                append("DATA: ").append(value.toString()).append("]");
            logger.debug(log.toString());
            
            return readBytesAmount;
        }
        catch (IOException e){
            throw new DacardPadException("DataElement parsing failed", e);
        }
    }

    void serialize(OutputStream os) throws DacardPadException{
        StringBuffer log = new StringBuffer("DataElement[").
            append("TYPE: ").append(hexConv.getStringFromHex(new byte[]{type.getCode()})).append("; ").
            append("NAME_LENGTH: ").append(name.getSize()).append("; ").
            append("NAME: ").append(name.getValue()).append("; ").
            append("DATA: ").append(value.toString());

        try{
            //1. write type code byte
            os.write(type.getCode());
            //2. write name length
            os.write(name.getSize());
            //3. write name
            name.serialize(os);
            //4. write data length
            type.getPad().serializeLengthInBytes(os, value);
            //5. write data
            type.getPad().serialize(os, value);
//debug (comment row above)
//java.io.ByteArrayOutputStream baos = new java.io.ByteArrayOutputStream();
//type.getPad().serialize(baos, value);
//log.append("; DATA_HEX: ").append(new com.tmx.util.HexConverter().getStringFromHex(baos.toByteArray()));
//baos.writeTo(os);
//debug
        }
        catch (IOException e){
            throw new DacardPadException("DataElement serialization failed", e);
        }
        logger.debug(log.toString());
    }


    public DataElementName getName() {
        return name;
    }

    public void setName(DataElementName name) {
        this.name = name;
    }

    public DataElementType getType() {
        return type;
    }

    public void setType(DataElementType type) {
        this.type = type;
    }

    public T getValue() {
        return value;
    }

    public void setValue(T value) {
        this.value = value;
    }
}
