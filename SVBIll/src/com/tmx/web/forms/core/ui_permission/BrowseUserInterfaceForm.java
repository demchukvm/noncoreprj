package com.tmx.web.forms.core.ui_permission;

import com.tmx.web.controls.*;
import com.tmx.web.controls.table.*;
import com.tmx.web.forms.core.base.AbstractBrowseForm;
import com.tmx.as.entities.general.ui_permission.UserInterfaceType;


public class BrowseUserInterfaceForm extends AbstractBrowseForm {
    protected void init() {
        setBrowseTable(new ControlManagerTable("query(query_type=retrieve_all," +
                "entity_class=com.tmx.as.entities.general.ui_permission.UserInterface," +
                "table_attr=type," +
                "table_attr=parent," +
                "order(name=id,type=asc)," +
                "page_size=10" +
                ")"));
        Filter filter = new Filter();
        filter.addParameter("value", new Parameter(new EditBox()));
        getBrowseTable().getFilterSet().addFilter
                ("com_tmx_as_entities_general_ui_permission_UserInterface_name", filter);

        filter = new Filter();
        ScrollBox scrollBox=new ScrollBox() {
            protected String getKey(Object dataListElement) {
                return ((UserInterfaceType)dataListElement).getUserInterfaceTypeId().toString();
            }
            protected String getValue(Object dataListElement) {
                return ((UserInterfaceType)dataListElement).getName();
            }
        };
        scrollBox.setNullable(true);
        filter.addParameter("value", new Parameter(scrollBox));
        getBrowseTable().getFilterSet().addFilter
                ("com_tmx_as_entities_general_ui_permission_UserInterface_type", filter);
    }
}

