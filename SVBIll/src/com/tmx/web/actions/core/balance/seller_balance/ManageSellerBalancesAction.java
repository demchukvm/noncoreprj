package com.tmx.web.actions.core.balance.seller_balance;

import com.tmx.web.actions.core.balance.AbstractManageBalancesAction;
import com.tmx.as.exceptions.DatabaseException;
import org.apache.struts.action.ActionForm;


public class ManageSellerBalancesAction extends AbstractManageBalancesAction {

    private static final String FORWARD_MANAGE_SEL_BAL_PAGE = "manageSellerBalancesPage";

    protected String getManageForwardPage() {
        return FORWARD_MANAGE_SEL_BAL_PAGE;  
    }
}
