package com.tmx.as.entities.bill.voucher_type;

import com.tmx.as.base.BasicEntity;
import com.tmx.as.entities.bill.operator.Operator;
import com.tmx.as.entities.bill.modem_group.ModemGroup;

import java.io.Serializable;

/**
 */
public class VoucherType extends BasicEntity {
    private Long voucherTypeId;
    private Operator operator;
    private ModemGroup modemGroup;
    private String name;
    private Double amount;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Operator getOperator() {
        return operator;
    }

    public void setOperator(Operator operator) {
        this.operator = operator;
    }

    public ModemGroup getModemGroup() {
        return modemGroup;
    }

    public void setModemGroup(ModemGroup modemGroup) {
        this.modemGroup = modemGroup;
    }

    public Long getVoucherTypeId() {
        return voucherTypeId;
    }

    public void setVoucherTypeId(Long voucherTypeId) {
        this.voucherTypeId = voucherTypeId;
    }

    public Serializable getId() {
        return getVoucherTypeId();
    }

    public int getValid() {
        return valid;
    }

    public void setValid(int valid) {
        this.valid = valid;
    }
}
