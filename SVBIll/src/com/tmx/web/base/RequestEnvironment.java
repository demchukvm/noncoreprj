package com.tmx.web.base;

import com.tmx.util.StructurizedException;
import org.apache.struts.Globals;
import org.apache.struts.taglib.html.Constants;
import org.apache.struts.action.ActionErrors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.PageContext;
import java.util.Map;
import java.util.HashMap;
import java.util.Iterator;
/*
 * Holds parameters in the HttpServletRequest
 */

public class RequestEnvironment {
    /** Maps of requests identified by Thread objects. */
    private static Map requestMap = new HashMap();

    /** Reallocate new request. Remove old requests of this sessoin. Call it on every request in filter */
    public synchronized static void reallocateReqContext(RequestContext newContext){
        Map tmpMap = new HashMap();
        for(Iterator iter = requestMap.keySet().iterator(); iter.hasNext();){
            Thread threadKey = (Thread)iter.next();
            if(!((RequestContext)requestMap.get(threadKey)).isExpired()){
                tmpMap.put(threadKey, requestMap.get(threadKey));
            }

        }
        requestMap = tmpMap;
        //allocate new request
        requestMap.put(Thread.currentThread(), newContext);
//RM:Debug:
System.out.println("Count of reallocated RequestContextes="+requestMap.size());        
    }

    /** release current request. Call it on logout */
    public synchronized static void releaseReqContext(){
        requestMap.remove(Thread.currentThread());
        //also release all requests with null sessions
        Map tmpMap = new HashMap();
        for(Iterator iter = requestMap.keySet().iterator(); iter.hasNext();){
            Thread threadKey = (Thread)iter.next();
            if(((RequestContext)requestMap.get(threadKey)).isBelongToValidSession()){
                tmpMap.put(threadKey, requestMap.get(threadKey));
            }
        }
        requestMap = tmpMap;
    }

    /** Get current session */
    public synchronized static RequestContext currentReqContext(){
        return (RequestContext)requestMap.get(Thread.currentThread());
    }

    public static void saveError(HttpServletRequest request, StructurizedException e) {
        saveError(request, null, e);
    }

// RM: use with struts
//    public static void saveError(HttpServletRequest request, String property, StructurizedException e) {
//        ActionMessage errorMessage = new ActionMessage(e.getKey(), e.getParams());
//        ActionMessages errors = new ActionMessages();
//        if (property == null)
//            property = ActionErrors.GLOBAL_ERROR;
//        errors.add(property, errorMessage);
//        setAttr(request, Globals.ERROR_KEY, errors);
//    }

    public static void saveError(HttpServletRequest request, String property, StructurizedException e) {
        if (property == null)
            property = ActionErrors.GLOBAL_ERROR;
        setAttr(request, Globals.ERROR_KEY, e);
    }

    public static StructurizedException getError(HttpServletRequest request, String property){
        if (property == null)
            property = Globals.ERROR_KEY;
        return (StructurizedException)getAttr(request, property);
    }

    private static Object getAttr(HttpServletRequest request, String attrName) {
        if (request == null || attrName == null)
            return null;
        return request.getAttribute(attrName);
    }


    public static void setAttr(HttpServletRequest request, String attrName, Object attrValue) {
        if (request == null || attrName == null || attrValue == null)
            return;
        request.setAttribute(attrName, attrValue);
    }

    public static boolean removeAttr(HttpServletRequest request, String attrName) {
        if (request == null || attrName == null)
            return false;

        if (request.getAttribute(attrName) != null) {
            request.setAttribute(attrName, null);
            return true;
        }

        return false;
    }

}
