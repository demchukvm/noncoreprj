package com.tmx.web.actions.core.balance.terminal_balance;

import com.tmx.as.blogic.balance.AbstractBalanceManager;
import com.tmx.as.blogic.balance.TerminalBalanceManager;
import com.tmx.as.blogic.TransactionManager;
import com.tmx.as.entities.bill.balance.AbstractBalance;
import com.tmx.as.entities.bill.balance.TerminalBalance;
import com.tmx.as.entities.general.user.User;
import com.tmx.as.exceptions.DatabaseException;
import com.tmx.beng.base.BillException;
import com.tmx.util.InitException;
import com.tmx.web.actions.core.balance.AbstractInfoBalanceAction;
import com.tmx.web.controls.table.Parameter;
import com.tmx.web.forms.core.balance.AbstractInfoBalanceForm;
import com.tmx.web.forms.core.balance.terminal_balance.InfoTerminalBalanceForm;


public class InfoTerminalBalanceAction extends AbstractInfoBalanceAction {

    protected String getManageAction() {
        return "manageTerminalBalancesAction";
    }

    protected String getInfoPage() {
        return "infoTerminalBalancePage";
    }

    protected String getManageDeleteAction() {
        return "manageDeleteAction";
    }

    protected String getInfoTransactionAction() {
        return "/core/transactions/terminal_transaction/infoTerminalTransaction";
    }

    protected AbstractBalanceManager getBalanceManager() throws InitException, BillException {
        return new TerminalBalanceManager();
    }
   
    protected void preloadForUpdate(AbstractInfoBalanceForm form, AbstractBalance balance, User user) {
        InfoTerminalBalanceForm balanceForm = (InfoTerminalBalanceForm) form;
        TerminalBalance termBalance = (TerminalBalance) balance;
        balanceForm.setTerminalId(termBalance.getTerminalId());

        balanceForm.getTransactionsTable().addParameterToFilter("by_terminal_balance_id", new Parameter("terminal_balance_id", termBalance.getBalanceId()));
        balanceForm.getTransactionsTable().addParameterToFilter("by_terminal", new Parameter("terminal", termBalance.getTerminalId()));
    }

    protected void preloadForCreate(AbstractInfoBalanceForm form) {
        form.getTransactionsTable().addParameterToFilter("by_terminal_balance_id", new Parameter("terminal_balance_id", (long) 0));
        form.getTransactionsTable().addParameterToFilter("by_terminal", new Parameter("terminal", (long) 0));
    }

    protected void preload(AbstractInfoBalanceForm form, AbstractBalance balance) throws Throwable {
        TerminalBalance terminalBalance = (TerminalBalance)balance;

        if(terminalBalance.getTerminal()!=null)
            form.getScrollBoxBalanceOwner().setSelectedKey(terminalBalance.getTerminal().getTerminalId().toString());
    }

    protected TransactionManager.CashCalculation getCashCalculation(AbstractInfoBalanceForm form) throws DatabaseException {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }


}
