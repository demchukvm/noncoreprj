package com.tmx.beng.base.processors;

import com.tmx.as.base.EntityManager;
import com.tmx.as.base.ProcedureWrapper;
import com.tmx.as.base.QueryParameterWrapper;
import com.tmx.as.entities.bill.balance.TerminalBalance;
import com.tmx.as.entities.bill.terminal.Terminal;
import com.tmx.as.entities.bill.transaction.TerminalBalanceTransaction;
import com.tmx.as.entities.bill.transaction.TransactionSupport;
import com.tmx.as.entities.general.user.User;
import com.tmx.as.exceptions.DatabaseException;
import com.tmx.beng.base.*;
import com.tmx.beng.base.cache.DBCache;
import com.tmx.beng.base.cache.DBCacheException;

import java.sql.Types;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class ChangeTerminalBalanceProcessor extends TaskProcessor{
    private final String SCHEMA_NAME = "svbill";

    protected Task process0(Task task, DBCache dbCache, BillingMessageWritable msg) throws BillException {
//        try {
//            //create
//            TerminalBalanceTransaction terminalBalanceTransaction = new TerminalBalanceTransaction();
//            //setup
//            terminalBalanceTransaction.setTransactionTime(getTransRegTime(msg));
//            terminalBalanceTransaction.setClientTime(getClientTime(msg));
//            terminalBalanceTransaction.setAmount(getAmount(msg));
//            terminalBalanceTransaction.setTerminal(getTerminal(msg, dbCache));
//            terminalBalanceTransaction.setTerminalBalance(getTerminalBalance(msg, dbCache));
//            terminalBalanceTransaction.setTransactionSupport(getTransactionSupport(msg));
//            terminalBalanceTransaction.setTransactionGUID(getTransactionGUID(msg));
//            terminalBalanceTransaction.setCliTransactionNum(getCliTransactionNum(msg));
//
//            dbCache.reserveTerminalBalance(msg.getAttributeString(BillingMessage.TERMINAL_SN), getRealAmount(msg));
//            msg.setAttributeString(BillingMessage.TERMINAL_TARIFFED_AMOUNT_CHANGED, "true");
//
//            msg.setStatusCode(StatusDictionary.STATUS_OK);
//
//            /*TODO:(AN) rube! SaveToDatabaseTransactionProcessor has hardcode:
//            task.getGlobalContext().getTransactionPool().lookupTask(
//                    msg.getAttributeString(BillingMessage.REQUEST_ID),
//                    BillingMessage.O_REFILL_PAYMENT
//                );*/
//            msg.setOperationName(BillingMessage.O_REFILL_PAYMENT);
//
//            //add to message
//            msg.setAttribute(BillingMessage.ENTITY_LIST, getEntityList(msg, terminalBalanceTransaction));
//        } catch (Throwable e) {
//            task.getProcesedMessage().setStatusCode(StatusDictionary.RESERVATION_ERROR);
//            logger.error(e.getLocalizedMessage(), e);
//            if(e instanceof BillException)
//                throw (BillException)e;
//            throw new BillException(e.getLocalizedMessage(), e);
//        }
        return task;
    }

//    private List getEntityList(BillingMessageWritable msg, TerminalBalanceTransaction terminalBalanceTransaction) {
//        List list = new ArrayList();
//        list.add(terminalBalanceTransaction);
//        list.add(getProcedureWrapper(msg, terminalBalanceTransaction));
//        return list;
//    }
//
//    private ProcedureWrapper getProcedureWrapper(BillingMessageWritable msg, TerminalBalanceTransaction terminalBalanceTransaction) {
//        return new ProcedureWrapper(
//                new QueryParameterWrapper[]{
//                        QueryParameterWrapper.createOutParameter(1, Types.INTEGER),
//                        new QueryParameterWrapper(2, terminalBalanceTransaction.getTerminal().getTerminalId()),
//                        new QueryParameterWrapper(3, getRealAmount(msg)),
//                },
//                Terminal.class,
//                "change_terminal_balance");
//    }
//
//    private String getCliTransactionNum(BillingMessageWritable msg) {
//        return msg.getAttributeString(BillingMessage.CLI_TRANSACTION_NUM);
//    }
//
//    private String getTransactionGUID(BillingMessageWritable msg) {
//        return msg.getAttributeString(BillingMessage.BILL_TRANSACTION_NUM);
//    }
//
//    private TransactionSupport getTransactionSupport(BillingMessageWritable msg) {
//        //transaction support
//        TransactionSupport transSupport = new TransactionSupport();
//        //user for transaction support
//        Long userActorId = (Long)msg.getAttribute(BillingMessage.USER_ACTOR_ID);
//        User userActor = new User();
//        userActor.setUserId(userActorId);
//        transSupport.setUserActor(userActor);
//        //-----------------------------
//        transSupport.setDescription( msg.getAttributeString(BillingMessage.TRANSACTION_DESCRIPTION) );
//        transSupport.setPaymentReason( msg.getAttributeString(BillingMessage.PAYMENT_REASON) );
//        return transSupport;
//    }
//
//    private TerminalBalance getTerminalBalance(BillingMessageWritable msg, DBCache dbCache) throws DBCacheException {
//        TerminalBalance terminalBalance = new TerminalBalance();
//        terminalBalance.setBalanceId(dbCache.getTerminalBalanceId(msg.getAttributeString(BillingMessage.TERMINAL_SN)));
//        return terminalBalance;
//    }
//
//    private Date getClientTime(BillingMessageWritable msg) {
//        return (Date)msg.getAttribute(BillingMessage.CLIENT_TIME);
//    }
//
//    private Date getTransRegTime(BillingMessageWritable msg) {
//        return (Date)msg.getAttribute(BillingMessage.REGISTRATION_TIME);
//    }
//
//    private Terminal getTerminal(BillingMessageWritable msg, DBCache dbCache) throws DBCacheException {
//        Terminal terminal = new Terminal();
//        terminal.setTerminalId(dbCache.getTerminalId(msg.getAttributeString(BillingMessage.TERMINAL_SN)));
//        return terminal;
//    }
//
//    private Double getAmount(BillingMessageWritable msg) throws DBCacheException {
//        return (Double)msg.getAttribute(BillingMessage.TERMINAL_TARIFFED_AMOUNT);
//    }
//
//    private Double getRealAmount(BillingMessageWritable msg){
//        Double amount = (Double)msg.getAttribute(BillingMessage.TERMINAL_TARIFFED_AMOUNT);
//        String transactionType = msg.getAttributeString(BillingMessage.TRANSACTION_TYPE_ID);
//        return "2".equals(transactionType) ? new Double(amount.doubleValue() * (-1)) : amount;
//    }
//
//
//    public Task rollback(Task task, Throwable e) {
//        logger.info("Rollback Change...");
//        try{
//
//            // operator transaction
//            BillingMessageWritable msg = task.getProcesedMessage();
//            DBCache dbCache = task.getGlobalContext().getDbCache();
//
//            String changed = msg.getAttributeString(BillingMessage.TERMINAL_TARIFFED_AMOUNT_CHANGED);
//            if(changed != null && changed.equalsIgnoreCase("true")){
//                dbCache.reserveOperatorBalance(
//                        msg.getAttributeString(BillingMessage.TERMINAL_SN),
//                        new Double( -getRealAmount(msg).doubleValue() )
//                );
//            }
//
//        }
//        catch(Throwable te){
//            logger.error(te.getLocalizedMessage(), te);
//        }
//
//        return super.rollback(task, e);
//    }

}
