package com.tmx.web.forms.core.message;

import com.tmx.web.base.BasicActionForm;
import com.tmx.web.controls.Button;

public class ConfirmationForm extends BasicActionForm {
    private Button onOkButton;
    private Button onCancelButton;
    protected void init() {
        onOkButton=new Button();
        onOkButton.setLabelKey("ctrl.button.label.ok");
        onOkButton.setListenOnEvent("confirmation", "onOk");
        onCancelButton=new Button();
        onCancelButton.setLabelKey("ctrl.button.label.cancel");
        onCancelButton.setListenOnEvent("confirmation", "onCancel");
    }
    public Button getOnOkButton() {
        return onOkButton;
    }

    public void setOnOkButton(Button onOkButton) {
        this.onOkButton = onOkButton;
    }

    public Button getOnCancelButton() {
        return onCancelButton;
    }

    public void setOnCancelButton(Button onCancelButton) {
        this.onCancelButton = onCancelButton;
    }
}
