package com.tmx.web.taglib.html.ctrl;

import com.tmx.web.taglib.base.LocalizableTag;

import javax.servlet.jsp.JspException;

import org.apache.struts.taglib.TagUtils;

/**
 * Renders support script to return value on opener page.
 * Also render hiddens to hold references on opener page's elemetns
 * through any browse page submit (for paging, ordering or any other).
 *
 * This tag should be rendered in any case on browse page to hold
 * required parameters as request params.
 *
 * BrowsePickTag will check if this tag is not rendered on page,
 * so you should put this tag before any BrowsePickTag.
 *   
 */
public class BrowseCallBackTag extends LocalizableTag {
    final static String IS_BROWSE_CALLBACK_RENDERED_FLAG = "browseCallBackTag.isRendered";


    public int doStartTmxTag() throws JspException {
//        super.doStartTag();
        StringBuffer renderedHtml = new StringBuffer();
        renderSupportScript(renderedHtml);
        renderSupportHtml(renderedHtml);
        TagUtils.getInstance().write(pageContext, renderedHtml.toString());
        pageContext.setAttribute(IS_BROWSE_CALLBACK_RENDERED_FLAG, Boolean.TRUE);
        return EVAL_BODY_INCLUDE;
    }

    /**
        Renders support scripts in the opened browse window to return
        picked-up value back to the main window.
     * */
    protected void renderSupportScript(StringBuffer renderedHtml) throws JspException {
        renderedHtml.append("<script language=\"JavaScript\">").
           append("<!--\r").
           append("     function ").append(BrowseBoxTag.PICK_FUNCTION_PREFIX).append("(key, value)").append("{\r").
           append("         if (window.opener && !window.opener.closed)\n").
           append("         var keyHandler = window.opener.document.getElementById(\""+ pageContext.getRequest().getParameter(BrowseBoxTag.KEY_ID_PARAM_NAME)+"\");\r").
           append("         var valueHandler = window.opener.document.getElementById(\""+ pageContext.getRequest().getParameter(BrowseBoxTag.VALUE_ID_PARAM_NAME)+"\");\r").
           append("         if(keyHandler != null) keyHandler.value = key;\r").
           append("         if(valueHandler != null) valueHandler.value = value;\r").
           append("         window.close();\r").
           append("     }\r").
           append("-->\r").
           append("</script>\r\r");
    }

    /**
     * Renders suppoprt html hidden elements to store opener keyHanlder and valueHandler id between
     * browse page's ordering, paging, filtering and other submits.
     * */
    protected void renderSupportHtml(StringBuffer renderedHtml) throws JspException{
        renderedHtml.append("<input type=\"hidden\" name=\"").append(BrowseBoxTag.KEY_ID_PARAM_NAME).append("\" value=\"").append(pageContext.getRequest().getParameter(BrowseBoxTag.KEY_ID_PARAM_NAME)).append("\"/>\r").
                     append("<input type=\"hidden\" name=\"").append(BrowseBoxTag.VALUE_ID_PARAM_NAME).append("\" value=\"").append(pageContext.getRequest().getParameter(BrowseBoxTag.VALUE_ID_PARAM_NAME)).append("\"/>\r").
                     append("<input type=\"hidden\" name=\"").append(BrowseBoxTag.BROWSE_PARAM_NAME).append("\" value=\"true\"/>\r");
    }
}
