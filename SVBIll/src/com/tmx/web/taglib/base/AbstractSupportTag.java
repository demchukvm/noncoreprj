package com.tmx.web.taglib.base;

import org.apache.struts.taglib.nested.NestedPropertyHelper;
import org.apache.struts.taglib.TagUtils;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.JspException;


public abstract class AbstractSupportTag extends LocalizableTag {

    protected Object getPropertyObject() throws JspException {
        HttpServletRequest request = (HttpServletRequest) pageContext.getRequest();
        String originalName = (getName() != null) ? getName() : NestedPropertyHelper.getCurrentName(request, this);
        return TagUtils.getInstance().lookup(pageContext, originalName, getProperty(), null);
    }

    /** Replaces all "."  "]"   "["   ")"   "("   on '_' symbol. Used to generate unique plain name
     * based on given complex dot-notated name. */
    protected String getSeparatorReplacedName(String name){
        return name.replaceAll("[\\.\\]\\[\\)\\(]", "_");
    }

}
