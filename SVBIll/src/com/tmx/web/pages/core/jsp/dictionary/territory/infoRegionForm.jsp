<%@ page language="java" pageEncoding="UTF-8" contentType="text/html; charset=utf-8"
         errorPage="/core/jsp/message/jsperror.jsp" isELIgnored="false" %>
<%@ taglib uri="/WEB-INF/tld/struts-nested.tld" prefix="nested" %>
<%@ taglib uri="/WEB-INF/tld/tmx-controls.tld" prefix="tmx_ctrl" %>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>


<html:form action="/core/dictionary/territory/infoRegion">
<table border="0"  width="100%">
    <!-- BUTTONS -->
    <tr>
        <td style="text-align: left">
            <tmx_ctrl:button property="onSaveButton"/>
            <tmx_ctrl:button property="onResetButton" onClick="javascript:document.forms[0].reset()"/>
            <tmx_ctrl:button property="onCancelButton"/>
        </td>
    </tr>
    <tr>
        <td valign="center" align="center" class="tabForm">
        <table width="100%" border="0">
            <!-- Form header -->
            <tr>
                <td colspan="2">
                    <h4 class="dataLabel">
                        <bean:message bundle="core.labels" key="core.dictionary.territory.infoRegion.title"/>
                    </h4>
                </td>
            </tr>
            <tr>
                <!-- ID -->
                <td class="dataLabel">
                    <tmx_ctrl:writeEntityLabel attrName="regionId"
                                               entityClassName="com.tmx.as.entities.network.territory.Region"/>:
                </td>
                <td class="dataField">
                    <tmx_ctrl:editBox property="id" width="110px"/>
                </td>
                <!-- name -->
                <td class="dataLabel">
                    <tmx_ctrl:writeEntityLabel attrName="name"
                                               entityClassName="com.tmx.as.entities.network.territory.Region"/>:
                </td>
                <td class="dataField">
                    <tmx_ctrl:editBox property="name" width="290px"/>
                </td>
            </tr>
            <tr>
                <!-- shortName -->
                <td class="dataLabel">
                    <tmx_ctrl:writeEntityLabel attrName="shortName"
                                               entityClassName="com.tmx.as.entities.network.territory.Region"/>:
                </td>
                <td class="dataField">
                    <tmx_ctrl:editBox property="shortName" width="110px"/>
                </td>
                <!-- country -->
                <td class="dataLabel">
                    <tmx_ctrl:writeEntityLabel attrName="country"
                                               entityClassName="com.tmx.as.entities.network.territory.Region"/>:
                </td>
                <td class="dataField">
                    <tmx_ctrl:selectBox property="country"/>
                </td>
            </tr>
        </table>
        </td>
    </tr>
    <!-- BUTTONS -->
    <tr>
        <td style="text-align: left">
            <tmx_ctrl:button property="onSaveButton"/>
            <tmx_ctrl:button property="onResetButton" onClick="javascript:document.forms[0].reset()"/>
            <tmx_ctrl:button property="onCancelButton"/>
        </td>
    </tr>
</table>
</html:form>