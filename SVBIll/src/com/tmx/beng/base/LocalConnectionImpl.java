package com.tmx.beng.base;

import com.tmx.beng.access.Connection;

import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.List;


/**
 * 
 */
public class LocalConnectionImpl implements Connection {
    private BillingEngine engine = null;
    private Logger logger = Logger.getLogger("bill."+getClass());
    private long strtTime;

    public LocalConnectionImpl() throws BillException{
        engine = BillingEngine.getInstance();
    }

    public BillingMessage processSync(BillingMessage billingMessage) throws BillException{
        SimpleCallBack callBack = new SimpleCallBack();
        strtTime = System.currentTimeMillis();
        engine.processBillMessage(billingMessage, callBack);
        long waitTimeOutLong = Long.parseLong(System.getProperty("com.tmx.beng.connection.wait_time_out", String.valueOf(150000)));
        return callBack.receiveBilledMessage(waitTimeOutLong, billingMessage.getOperationName());
    }

    public void processAsync(BillingMessage billingMessage) throws BillException{
        engine.processBillMessage(billingMessage, null);
    }

    private BillingMessage buildFaultMessage(int code, String operationName){
        BillingMessageImpl msg = new BillingMessageImpl();
        msg.setOperationName(operationName);
        msg.setAttributeString(BillingMessage.STATUS_CODE, String.valueOf(code));
        msg.setAttributeString(BillingMessage.STATUS_MESSAGE, StatusDictionary.resolveStatusMessage(code));
        msg.setAttributeString(BillingMessage.WAIT_TIME, String.valueOf(engine.getReporter().getWaitTimeEstimate()));
        return msg;
    }

    private BillingMessage buildPleaseWaitMessage(String operationName){
        BillingMessageImpl msg = new BillingMessageImpl();
        msg.setOperationName(operationName);
        msg.setAttributeString(BillingMessage.STATUS_CODE, String.valueOf(0));
        msg.setAttributeString(BillingMessage.STATUS_MESSAGE, StatusDictionary.resolveStatusMessage(1));
        msg.setAttributeString(BillingMessage.WAIT_TIME, String.valueOf(engine.getReporter().getWaitTimeEstimate()));
        return msg;
    }

    //---------simple call back
    private class SimpleCallBack implements CallBack{
        private BillingMessage billedMessage = null;
        private boolean isSent = false;

        public synchronized void sendBilledMessage(BillingMessage message) {
            this.billedMessage = message;
            this.isSent = true;
            this.notify();
        }


        public synchronized BillingMessage receiveBilledMessage(long waitTimeOut, String operationName) {
            if(billedMessage != null)
                return billedMessage;

            try{
                this.wait(waitTimeOut);
            }
            catch(InterruptedException e){
                logger.error("Synchronous billing message processing was interrupted: "+e.toString());
                return buildFaultMessage(StatusDictionary.SYNC_BILL_WAIT_INTERRUPED, operationName);
            }

            if(isSent && billedMessage == null){
                isSent = false;
                return buildFaultMessage(StatusDictionary.BILLING_RETURNS_NULL_MESSAGE, operationName);
            }
            else if(!isSent){
                logger.error("!!!Local_timeout: " +
                                            (System.currentTimeMillis() - strtTime) +
                                            " " + operationName);
                isSent = false;
                //Demchuk V.M. temporary decision
                return buildFaultMessage(StatusDictionary.WAIT_FOR_BILLING_TIME_OUT, operationName);
            }
            else{
                    isSent = false;

                return billedMessage;

            }
        }
    }
}
