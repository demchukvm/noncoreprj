package com.tmx.as.blogic;

import java.util.Random;
import java.util.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

/**
    Used to generate client transaction numbers
    Output transaction format:
    suffix + "_" + date + "_" + random_byte + "_" + suffix
 */
public class ClientTransactionNumGenerator {
    private String suffix;
    private String prefix;
    private Random random;
    private final String DELIMITER = "_";
    private final DateFormat dateFormat = new SimpleDateFormat("yyyy.dd.MM_HH:mm:ss");

    public ClientTransactionNumGenerator(String prefix, String suffix){
        this.suffix = suffix;
        this.prefix = prefix;
        this.random = new Random();
    }

    public String nextNumber(){
        return new StringBuffer(prefix).
                append(DELIMITER).
                append(dateFormat.format(new Date())).
                append(DELIMITER).
                append(Math.abs(random.nextInt())).
                append(DELIMITER).
                append(suffix).toString();
    }
}
