package com.tmx.beng.base.channels;

import com.tmx.beng.base.BillingMessage;
import com.tmx.beng.base.BillException;
import com.tmx.beng.base.beans.ParameterListType;

import java.util.List;

/**
    Resolves resulting channel based on refilled msisdn
 */
public class LenghtMaskChannel extends AbstractChannel
{
/*    private static final String DEFAULT_CHANNEL_ID = "defaultChannelId";
    private static final String NUMBER_PREFIX = "numberPrefix";*/

    public OutChannel process(BillingMessage msg) throws BillException
    {
        int beelineAccountLength = Integer.parseInt(getParamValue("beelineAccountLength"));     // 10
        int ksAccountLength = Integer.parseInt(getParamValue("ksAccountLength"));               // 7
        
        String beelinePrefix = getParamValue("beelinePrefix");                                  // 0
                                    
        int refillAccountLength = msg.getAttributeString(BillingMessage.PAY_ACCOUNT_NUMBER).length();
        String refillAccount = msg.getAttributeString(BillingMessage.PAY_ACCOUNT_NUMBER);

        boolean isValid = false;
        int accountOutChannelId = 0;

        if(refillAccountLength == beelineAccountLength) // Beeline_FixConnect 1
        {
            if(refillAccount.startsWith(beelinePrefix))
            {
                isValid = true;
                accountOutChannelId = 2;
            }
            else
            {
                throw new BillException("err.msisdn_is_not_valid");
            }
        }
        else if(refillAccountLength == ksAccountLength) // KS_FixConnect 2
        {
            if(!refillAccount.startsWith(beelinePrefix))
            {
                isValid = true;
                accountOutChannelId = 1;
            }
            else
            {
                throw new BillException("err.msisdn_is_not_valid");
            }
        }


        if(isValid)
        {
            int outChannelId = resolveOutChannelId(msg.getAttributeString(BillingMessage.CLI_TRANSACTION_NUM));
            OutChannel outChannel = getOutChannel(outChannelId);
            if(outChannel == null)
                throw new BillException("err.client_transaction_mask_channel.no_channel_found_with_id", new String[]{String.valueOf(outChannelId)});

            if(outChannelId == accountOutChannelId)
            {
                return outChannel;
            }
            else
            {
                throw new BillException("err.bill.out_channel_is_absent.service_is_blocked", new String[]{String.valueOf(outChannelId)});
            }
        }
        else
        {
            throw new BillException("err.msisdn_is_not_valid");   
        }
    }

    private int resolveOutChannelId(String cliTransactionNum) throws BillException
    {
        String[] outChannelsIds = getParamListsNames();
        for (String outChannelId : outChannelsIds) {
            List cliTransNumTemplateList = getParamListValues(outChannelId);
            for (Object cliTransNumTemplate : cliTransNumTemplateList) {
                String cliTransNumPrefix = (String) cliTransNumTemplate;
                if (cliTransactionNum.startsWith(cliTransNumPrefix))
                    return Integer.parseInt(outChannelId);
            }
        }

        return 0;
    }

    /*
    public OutChannel process(BillingMessage msg) throws BillException
    {
     //   String op = msg.getAttributeString(BillingMessage.SERVICE_CODE);

        int maxNumLenght = Integer.parseInt(getParamValue("maxAccountLenght"));         //10
        int minNumLenght = Integer.parseInt(getParamValue("minAccountLenght"));         //7

        int refillNumLenght = msg.getAttributeString(BillingMessage.PAY_ACCOUNT_NUMBER).length();

        String refillNum = msg.getAttributeString(BillingMessage.PAY_ACCOUNT_NUMBER);
        List numPrefix = getParamListValues("2");

        int outChannelId = 1;


        if(maxNumLenght == refillNumLenght)        // Beeline 10
        {
            boolean isValid = false;
            for (Object prfx : numPrefix)
            {
                if(refillNum.startsWith(prfx.toString()))
                {
                    outChannelId = 2;
                    isValid = true;
                    break;
                }
            }
            if(isValid)
            {
                OutChannel outChannel = getOutChannel(outChannelId);
                return outChannel;
            }
            else throw new BillException("err.msisdn_is_not_valid");

        }
        else if(maxNumLenght > refillNumLenght && minNumLenght == refillNumLenght)     // KS 7
        {
            for (Object prfx : numPrefix)
            {
                if(refillNum.startsWith(prfx.toString()))
                {
                    throw new BillException("err.msisdn_is_not_valid");
                }
            }
            OutChannel outChannel = getOutChannel(outChannelId);
            return outChannel;
        }
        throw new BillException("err.msisdn_is_not_valid");
        
    }
    */


/*        if(numLenght == 10) throw new BillException("err.msisdn_lenght_is_not_valid");


        if(numLenght != 10) throw new BillException("err.msisdn_lenght_is_not_valid");

        String normilizedPhoneNumber = normalizeMsisdn(msg.getAttributeString(BillingMessage.ACCOUNT_NUMBER));
        String pauAccountNumber = normalizeMsisdn(msg.getAttributeString(BillingMessage.PAY_ACCOUNT_NUMBER));
        final int outChannelId = resolveOutChannelId(normilizedPhoneNumber, pauAccountNumber);
        OutChannel outChannel = getOutChannel(outChannelId);
        if(outChannel == null)
            throw new BillException("err.msisdn_mask_channel.no_channel_found_with_id", new String[]{String.valueOf(outChannelId)});
        return outChannel;
    }*/
    /** convert msisdn to international format like 380 50 XXX XX XX */
    
}
