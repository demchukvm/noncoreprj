package com.tmx.web.actions.core.balance.terminal_balance;

import com.tmx.web.actions.core.base.AbstractBrowseAction;
import com.tmx.web.forms.core.base.AbstractFormFactory;
import com.tmx.web.forms.core.balance.terminal_balance.BrowseTerminalsForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForm;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class BrowseTerminalsAction extends AbstractBrowseAction {

    protected String getForwardName() {
        return "browseTermianlsPage";  
    }

    public ActionForward defaultAction(ActionMapping mapping, ActionForm form, HttpServletRequest request,
                                       HttpServletResponse response, String methodName) throws Throwable {
        AbstractFormFactory.getInstance(getLoggedInUser(request)).setUpForm((BrowseTerminalsForm) form);
        return super.defaultAction(mapping, form, request, response, methodName);    
    }
    
}
