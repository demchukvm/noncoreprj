<%@ page language="java" pageEncoding="UTF-8" contentType="text/html; charset=utf-8" errorPage="/core/jsp/message/jsperror.jsp" isELIgnored="false" %>
<%@ page import="com.tmx.web.base.SessionEnvironment" %>
<%@ page import="com.tmx.web.base.Roadmap" %>
<%@ taglib uri="/WEB-INF/tld/tmx-controls.tld" prefix="tmx_ctrl" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>

<table width="100%" border="0">
    <tr>
        <td width="20%" valign="top">
            <!-- left menu -->
            <tmx_ctrl:menu menuName="settings" styleName="html_leftmenu"/>
        </td>
        <td valign="top" width="100%">
            <%
                Roadmap roadmap = (Roadmap) SessionEnvironment.getAttr(request.getSession(), "roadmap");
                String actionOk = roadmap.getPageForwardURL("confirmation", "onOk");
                String actionCancel = roadmap.getPageForwardURL("confirmation", "onCancel");
            %>
            <!-- CONTENT -->
            <form action="" method="post">
                <br><br><br><br>
                <table width="350" cellspacing="0" cellpadding="0" border="0" align="center">
                    <tr>
                        <td align="center">

                            <table cellspacing='0' cellpadding='0' width="301">
                                <table cellspacing='1' cellpadding='1' width="300" class="column_header_cell_">
                                    <tr>
                                        <td>
                                            <bean:message bundle="core.labels" key="core.page.message.deleteconfirmation.msg.warning"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="300" bgcolor="#ffffff">
                                            <br>
                                            <br>

                                            <div style="width:300px; padding-left:2px;">
                                                <bean:message bundle="core.labels" key="core.page.message.deleteconfirmation.msg.confirm_delete"/>
                                            </div>
                                            <br>
                                            <br>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class='column_header_cell' width="300">
                                            <%
                                                if (actionOk != null) {
                                            %>
                                            <input class="button" type="submit"
                                                   value="<bean:message bundle="core.labels" key="core.common.button.delete"/>"
                                                   onClick="javascript:submitForm('<%=actionOk%>')"/>
                                            <% } %>
                                            <%
                                                if (actionCancel != null) {
                                            %>
                                            <input class="button" type="submit"
                                                   value="<bean:message bundle="core.labels" key="core.common.button.cancel"/>"
                                                   onClick="javascript:submitForm('<%=actionCancel%>')"/>
                                            <% }
                                            %>
                                        </td>
                                    </tr>
                                </TABLE>
                            </table>
                        </td>
                    </tr>
                </table>

            </form>

        </td>
    </tr>


</table>