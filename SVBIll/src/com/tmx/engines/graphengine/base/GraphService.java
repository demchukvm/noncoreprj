package com.tmx.engines.graphengine.base;

import com.tmx.as.base.Reconfigurable;
import com.tmx.util.Configuration;
import com.tmx.util.InitException;
import com.tmx.util.XMLUtil;
import com.tmx.util.cache.FileCache;
import com.tmx.util.cache.CacheException;
import com.tmx.engines.graphengine.beans.*;
import com.tmx.engines.graphengine.util.MathTransform;

import java.util.*;
import java.io.File;
import java.io.InputStream;
import java.net.URL;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.w3c.dom.Node;
import org.w3c.dom.Element;
import com.sun.org.apache.xpath.internal.XPathAPI;
import org.apache.log4j.Logger;
import org.apache.xmlbeans.XmlOptions;
import org.apache.commons.math.MathException;

/**
 */
public class GraphService implements Reconfigurable {
    /** Only single instance available */
    private static GraphService graphService;
    private Config serviceConfig = null;
    public static String LOGCLASS_GRAPH = "graph";
    /** Logger */
    private static Logger logger = Logger.getLogger(LOGCLASS_GRAPH + ".GraphService");
    private Map graphConfigs = null;
    private GraphTemplateCache cache = new GraphTemplateCache();


    /** to hide default constructor */
    private GraphService(){
    }

    /** Return gate context instance */
    public static GraphService getInstance(){
        if(graphService == null)
            graphService = new GraphService();

        return graphService;
    }

    /** Reload configuration */
    public void reload() throws InitException{
        try {
            logger.debug("Start initialization of the Graph Service");
            String graphConfigFile = Configuration.getInstance().getProperty("graph_engine.config_file");
            if (graphConfigFile == null)
                throw new InitException("err.graph_service.config_file_is_not_specified", Locale.getDefault());

            //Load config from XML
            Document doc = XMLUtil.getDocumentFromFile(graphConfigFile);

            String xpathGraphService = "/graphEngine";
            Node graphServiceNode = XPathAPI.selectSingleNode(doc, xpathGraphService);
            Config localConfig = new Config();
            String repository = ((Element)graphServiceNode).getAttribute("repository");
            if(repository == null || "".equals(repository.trim()))
                throw new InitException("err.graph_service.required_attr_not_found", new String[]{"/graphEngine@repository"});

            localConfig.setRepository(Configuration.getInstance().substituteVariablesInString(repository));

            //retrieve graphs configs
            String xpathGraphs = "/graphEngine/graphs/graph";
            NodeList graphsNodes = XPathAPI.selectNodeList(doc, xpathGraphs);
            Map localGraphsConfigs = new HashMap();
            GraphConfig graphConfig = null;
            for (int i = 0; i < graphsNodes.getLength(); i++) {
                Node graphNode = graphsNodes.item(i);
                if (graphNode.getNodeType() == Node.ATTRIBUTE_NODE)
                    continue;

                graphConfig = new GraphConfig();

                //graph name
                graphConfig.setName(((Element)graphNode).getAttribute("name"));
                if (graphConfig.getName() == null || "".equals(graphConfig.getName()))
                    throw new InitException("err.graph_service.graph_name_is_null_in_config");

                //impl class
                String implClassName = ((Element)graphNode).getAttribute("implClass");
                if (implClassName == null || "".equals(implClassName))
                    throw new InitException("err.graph_service.graph_class_is_null_in_config");

                try {
                    Class implClass = Class.forName(implClassName);
                    if(!Graph.class.isAssignableFrom(implClass))
                        throw new InitException("err.graph_service.graph_class_is_not_successor_of", new String[]{implClass.getName(), Graph.class.getName()});

                    graphConfig.setImplClass(implClass);
                }
                catch (Exception e) {
                    throw new InitException("err.graph_service.failed_get_graph_class", e);
                }

                //default template file
                graphConfig.setDefaultTemplatePath(((Element)graphNode).getAttribute("defaultTemplate"));
                if (graphConfig.getDefaultTemplatePath() == null || "".equals(graphConfig.getDefaultTemplatePath()))
                    throw new InitException("err.graph_service.graph_template_is_null_in_config");

                //xsd schema file
                graphConfig.setSchemaPath(((Element)graphNode).getAttribute("schema"));
                if (graphConfig.getSchemaPath() == null || "".equals(graphConfig.getSchemaPath()))
                    throw new InitException("err.graph_service.graph_schema_is_null_in_config");

                localGraphsConfigs.put(graphConfig.getName(), graphConfig);
            }
            //set reloaded configs
            graphConfigs = localGraphsConfigs;
            serviceConfig = localConfig;
            logger.debug("Successful completed initialization of the Graph service");

        }
        catch (InitException e) {
            //rethrow
            throw e;
        }
        catch (Exception e) {
            throw new InitException("err.graph_service.reload_failed", e, Locale.getDefault());
        }
    }


    /** Loads Graph by specified dot-notated template file path-name.
     * This path should be started from repository and do not include file extension (.xml).
     *
     * Example:
     *
     * Template file path:      "d:/tmx_home/repository/graphs/my_graphs/my_graph1.xml"
     *
     * Repository path
     * (from service config):   "d:/tmx_home/repository"
     *
     * dot-notated template
     * name:                    "my_graphs.my_graph1"
     * */
    public Graph initializeGraph(String templateName) throws GraphException{
        templateName = (templateName.endsWith(".xml")) ? templateName.substring(0, templateName.length() - ".xml".length()) : templateName;
        templateName = templateName.replace('.', File.separatorChar);
        String templatePath = serviceConfig.getRepository() + File.separator + templateName + ".xml";
        return initializeGraph0(templatePath);
    }

    /** Loads Graph of specified type. The default template assigned with
     * given graph type is used. */
    public Graph initializeDefaultGraph(String graphType) throws GraphException{
        GraphConfig graphConfig = (GraphConfig)graphConfigs.get(graphType);
        if(graphConfig == null)
            throw new GraphException("err.graph_service.graph_config_not_found", new String[]{graphType});

        URL templateURL = graphConfig.getImplClass().getResource(graphConfig.getDefaultTemplatePath());
        if(templateURL == null)
            throw new GraphException("err.graph_service.graph_config_not_found", new String[]{graphType});

        return initializeGraph0(templateURL.getPath());
   }

    /** Goes along all data block inside graph data and approximate their enclosed Sets
     * @param graph target graph
     * @param approximateToPoints approximate to points
     *  */
    public ApproximationReport approximate(Graph graph, int approximateToPoints) throws MathException {
        long timeCost = System.currentTimeMillis();
        ApproximationReport apprxReport = new ApproximationReport();
        Block[] blockArray = graph.getRootDocument().getRoot().getData().getBlockArray();
        if(blockArray == null)
            return apprxReport;//nothing to approximate

        //store initial data block sizes for reporting
        for(int blockIdx = 0; blockIdx < blockArray.length; blockIdx++){
            ApproximationReport.BlockReport bReport = apprxReport.addBlockReport(blockIdx);
            bReport.setInitialPointsCount(blockArray[blockIdx].getSetArray().length);
            bReport.setName(blockArray[blockIdx].getName());
            bReport.setBlockEnabled("yes".equals(blockArray[blockIdx].getEnabled()));
        }

        if(approximateToPoints <= 0)
            return apprxReport;//do not approximate at all.

        MathTransform mt = new MathTransform();

        /** prepare for rounding value */
        int pos = 2;//remain digits after point
        int mul = 1;
        while(pos-- > 0)
            mul = mul*10;//power

        for(int blockIdx = 0; blockIdx < blockArray.length; blockIdx++){
            com.tmx.engines.graphengine.beans.Set[] setArray = blockArray[blockIdx].getSetArray();

            if(setArray.length <= approximateToPoints){
                //do not approximate this block - it size less then required for approximation
                apprxReport.getBlockReport(blockIdx).setApproxUsed(false);
                continue;
            }

            //fetch initial data from graph
            double[] args = new double[setArray.length];
            double[] vals = new double[setArray.length];
            for(int setIdx = 0; setIdx < setArray.length; setIdx++){
                com.tmx.engines.graphengine.beans.Set set = setArray[setIdx];
                if(set != null){
                    args[setIdx] = set.getArgument();
                    vals[setIdx] = set.getValue();
                }
                else{
                    args[setIdx] = 0;
                    vals[setIdx] = 0;
                }
            }

            //approximate
            MathTransform.Function function = mt.createInitialFunction(args, vals);
            function = mt.approximate(function, approximateToPoints + 1);

            //replace initial data with approximated. Do not evaluete last point (its value always = 0)
            blockArray[blockIdx].setSetArray(new com.tmx.engines.graphengine.beans.Set[]{});
            for(int setIdx = 0; setIdx < function.getPointsCount() - 1; setIdx++){
                com.tmx.engines.graphengine.beans.Set set = blockArray[blockIdx].addNewSet();
                set.setValue((double)Math.round(function.getValue()[setIdx] * mul)/mul);//round the value
                set.setArgument(Math.round(function.getArgument()[setIdx]));
                set.setLabel("Approximated");
            }
            apprxReport.getBlockReport(blockIdx).setApproxUsed(true);
            apprxReport.getBlockReport(blockIdx).setApproxedPointsCount(blockArray[blockIdx].getSetArray().length);
        }
        logger.info("Appoximation time (ms): "+(System.currentTimeMillis() - timeCost));
        return apprxReport;
    }

    /** Apply dimensions for graph */
    public void applyDimensions(Graph graph){
        if(graph == null)
            return;

        final int GRID_LINES_COUNT = 6;//How much grid  lines you wish to see
        double maxValue = Double.MIN_VALUE;
        double minValue = Double.MAX_VALUE;
        long maxArgument = Long.MIN_VALUE;
        long minArgument = Long.MAX_VALUE;

        Block[] blockArray = graph.getRootDocument().getRoot().getData().getBlockArray();
        if(blockArray == null)
            return;

        for(int blockIdx = 0; blockIdx < blockArray.length; blockIdx++){
            com.tmx.engines.graphengine.beans.Set[] setArray = blockArray[blockIdx].getSetArray();
            if(setArray == null)
                continue;
            for(int setIdx = 0; setIdx < setArray.length; setIdx++){
                com.tmx.engines.graphengine.beans.Set set = setArray[setIdx];
                if(minValue > set.getValue()){
                    minValue = set.getValue();
                }
                if(maxValue < set.getValue()){
                    maxValue = set.getValue();
                }
                if(minArgument > set.getArgument()){
                    minArgument = set.getArgument();
                }
                if(maxArgument < set.getArgument()){
                    maxArgument = set.getArgument();
                }
            }
        }
        /** If actual dimension not found then set it to 0 */
        if(maxValue == Double.MIN_VALUE)
            maxValue = 0;
        if(minValue == Double.MAX_VALUE)
            minValue = 0;
        if(maxArgument == Long.MIN_VALUE)
            maxArgument = 0;
        if(minArgument == Long.MAX_VALUE)
            minArgument = 0;

        graph.getRootDocument().getRoot().getType().getChart().setMinimumValue(minValue - 1);
        graph.getRootDocument().getRoot().getType().getChart().setMaximumArgument(maxArgument + 1);
        graph.getRootDocument().getRoot().getType().getChart().setMaximumValue(maxValue + 1);
        graph.getRootDocument().getRoot().getType().getChart().setMinimumArgument(minArgument - 1);


        long valueStep = Math.round((maxValue - minValue)/GRID_LINES_COUNT);
        long argumentStep = Math.round((maxArgument - minArgument)/GRID_LINES_COUNT);
        graph.getRootDocument().getRoot().getType().getWorkspace().getGrid().getValues().setLinesStep((valueStep == 0) ? 1 : valueStep);
        graph.getRootDocument().getRoot().getType().getWorkspace().getGrid().getArguments().setLinesStep((argumentStep == 0) ? 1 : argumentStep);
    }

    //---------private

    /** initialize graph */
    private Graph initializeGraph0(String realTemplateFilePath) throws GraphException{
        //load template
        RootDocument rootDoc = null;
        try{
            rootDoc = (RootDocument)cache.get(realTemplateFilePath);
        }
        catch(CacheException e){
            throw new GraphException("err.graph_service.failed_to_get_graph_template", new String[]{realTemplateFilePath}, e);
        }

        //check type and get config
        String graphType = rootDoc.getRoot().getType().getChart().getType();
        GraphConfig graphConfig = (GraphConfig)graphConfigs.get(graphType);
        if(graphConfig == null)
            throw new GraphException("err.graph_service.graph_config_not_found", new String[]{graphType});

        //instaniate graph
        Graph graph = null;
        try{
            graph = (Graph)graphConfig.getImplClass().newInstance();
            graph.init(graphConfig, rootDoc);
        }
        catch(Exception e){
            throw new GraphException("err.graph_service.graph_instantiation_failed", e);
        }
        return graph;
    }

    /** Clear all sets in graph datablocks */
    public void clearGraphSets(Graph graph){
        if(graph == null)
            return;
        Block[] blockArray = graph.getRootDocument().getRoot().getData().getBlockArray();
        if(blockArray != null){
            for(int i = 0; i < blockArray.length; i++){
                blockArray[i].setEnabled("no");
                blockArray[i].setSetArray(new com.tmx.engines.graphengine.beans.Set[]{});
            }
        }
    }

    /** graph config container */
    class GraphConfig{
        private String name = null;
        private Class implClass = null;
        private String defaultTemplatePath = null;
        private String schemaPath = null;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public Class getImplClass() {
            return implClass;
        }

        public void setImplClass(Class implClass) {
            this.implClass = implClass;
        }

        public String getDefaultTemplatePath() {
            return defaultTemplatePath;
        }

        public void setDefaultTemplatePath(String defaultTemplatePath) {
            this.defaultTemplatePath = defaultTemplatePath;
        }

        public String getSchemaPath() {
            return schemaPath;
        }

        public void setSchemaPath(String schemaPath) {
            this.schemaPath = schemaPath;
        }
    }

    /** service config */
    class Config{
        private String repository = null;

        private String getRepository() {
            return repository;
        }

        private void setRepository(String repository) {
            this.repository = repository;
        }
    }

    private class GraphTemplateCache extends FileCache {

        private GraphTemplateCache() {
            maxSize = 100;
            cleanCount = 10;
            cleanType = CleanAlgorithm.Type.CLEAN_N_SELDOM_ACCESSED;
        }

        protected Object getRealObjectFromFile(InputStream is, String templateFilePath) throws CacheException {
            RootDocument rootDoc = null;
            try{
                // Bind the instance to the generated XMLBeans types.
                rootDoc = RootDocument.Factory.parse(is);
            }
            catch(Throwable e){
                throw new CacheException("err.graph_service.failed_to_parse_graph_template", new String[]{templateFilePath}, e);
            }

            //validate
            ArrayList errors = new ArrayList();
            XmlOptions opts = new XmlOptions();
            opts.setErrorListener(errors);
            if(!rootDoc.validate(opts)){
                StringBuffer errorBuffer = new StringBuffer("Validation failed of XML "+templateFilePath+":\n");
                Iterator iter = errors.iterator();
                while(iter.hasNext())
                    errorBuffer.append("   >> " + iter.next() + "\n");
                logger.error(errorBuffer.toString());
                throw new CacheException("err.graph_service.template_validation_failed", new String[]{templateFilePath});
            }

            return rootDoc;
        }
    }
}
