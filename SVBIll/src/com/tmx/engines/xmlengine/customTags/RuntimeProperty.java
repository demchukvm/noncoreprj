package com.tmx.engines.xmlengine.customTags;

import com.tmx.as.base.Entity;
import org.w3c.dom.Node;
import org.w3c.dom.Element;

/** To modify entity's property according to the context requirements. */
public abstract class RuntimeProperty extends AbstractNode {


    public RuntimeProperty(String propertyName){
        setName(propertyName);
    }


    /** RuntimeProperty descendants will implements their evaluateRuntimeValue()
     * and its value will be populated into the CDATA of AbstractNode */
    public String getCDATASectionValue(){
        return evaluateRuntimeValue();
    }

    public Node renderAndInjectXMLNode(Node parentNode, Entity entity) {
        Element runtimePropertyNode = (Element)super.renderAndInjectXMLNode(parentNode, entity);
        runtimePropertyNode.setAttribute("type", String.class.getName());
        runtimePropertyNode.setAttribute("runtime", "true");
        return runtimePropertyNode;
    }

    public abstract String evaluateRuntimeValue();

    public String getTagName() {
        return "property";//RM use "runtime_property";
    }

}
