package com.tmx.beng.base;

import java.io.Serializable;

/**
 */
public interface BillingMessage extends Serializable{
    public final String O_REFILL_PAYMENT = "request.refillPayment";
    public final String O_REFILL_PAYMENT_REDIRECT = "request.refillPaymentRedirect";
    public final String O_REFILL_PAYMENT_RESULT = "response.refillPayment";
    public final String O_REFILL_PAYMENT_STATUS = "request.refillPaymentStatus";
    public final String O_REFILL_PAYMENT_STATUS_EXT = "request.refillPaymentStatusExt";
    public final String O_REFILL_PAYMENT_STATUS_RESULT = "response.refillPaymentStatus";
    public final String O_CANCEL_REFILL_PAYMENT = "request.cancelRefillPayment";
    public final String O_CANCEL_REFILL_STATUS = "response.cancelRefillPayment";
    public final String O_BUY_VOUCHER = "request.buyVoucher";
    public final String O_GEN_VOUCHER = "request.genVoucher";       // используется только в BatchBuyVouchersForm

    public final String O_INFO_4_TERMINAL = "request.info4Terminal";
    
    /** RM: this operation type should be used instead of O_REFILL_PAYMENT to acquire vouchers from operator.
     * But then we need to implement it pipelines and make this new operation recognizable at our gates.*/
    public final String O_BUY_VOUCHER_FROMSERVICE = "request.buyVoucherFromService";
    public final String O_BUY_VOUCHER_RESULT = "response.buyVoucher";
    public final String O_BUY_VOUCHER_STATUS = "request.buyVoucherStatus";
    public final String O_BUY_VOUCHER_STATUS_RESULT = "response.buyVoucherStatus";
    public final String O_RETURN_VOUCHER = "request.returnVoucher";
    public final String O_REGISTER_TERMINAL = "request.registerTerminal";
    public final String O_RELOAD_DB_CACHE = "request.reloadDbCache";
    public final String O_VALIDATE_FUNCTION_PARAMS = "request.validateFunctionParams";
    public final String O_RETRIEVE_DEFAULT_FUNCTION_PARAMS = "request.retrieveDefaultFunctionParams";
    public final String O_CHAHGE_SELLER_BALLANCE = "request.changeSellerBalance";
    public final String O_CHAHGE_OPERATOR_BALLANCE = "request.changeOperatorBalance";
    public final String O_CHAHGE_TERMINAL_BALLANCE = "request.changeTerminalBalance";
	public final String O_DB_CACHE_DUMP = "request.DBCacheDump";
	public final String O_PROCESSING_RESOLVER_TEST = "request.processingResolverTest";

    //Operation attributes

    //Proximan 01.11.2010
    //public final String GEN_VOUCHER_NOMINAL = "genVoucherNominal";
    public final String VOUCHER_COUNT = "voucherCount";
    //public final String GEN_VOUCHER_COUNT = "genVoucherCount";
    //public final String GEN_SERVICE = "genService";
    //public final String GEN_SERVICE_CODE = "genServiceCode";
    //public final String GEN_TERMINAL_SN = "genTerminalSN";
    //public final String GEN_SELLER_CODE = "genSellerCode";
    //

    public final String IS_REDIRECT = "isRedirect";

    public final String Y = "y";

    public final String BILL_TRANSACTION_NUM = "billTransactionNum";
    public final String REQUEST_ID = "requestId";
    public final String CLI_TRANSACTION_NUM = "cliTransactionNum";
    public final String SERVICE_CODE = "serviceCode";
    public final String AMOUNT = "amount";
    public final String TOTAL_AMOUNT = "amount";
    public final String TRANSACTION_DESCRIPTION = "transaction description";
    public final String USER_ACTOR_ID = "user actor id";
    public final String ACCOUNT_NUMBER = "accountNumber";
    public final String PAY_ACCOUNT_NUMBER = "payAccountNumber";
    public final String VOUCHER = "voucher";
    public final String INFO_4_TERMINAL = "info";
    public final String VOUCHER_NOMINAL = "voucherNominal";
    public final String VOUCHER_CODE = "voucherCode";
    public final String VOUCHER_SECRET_KEY = "voucherSecretKey";
    public final String VOUCHER_BEST_BEFORE_DATE = "voucherBestBeforeDate";
    public final String VOUCHER_RENEVAL_TIME = "voucherRenewalTime";
    public final String STATUS_CODE = "statusCode";
    public final String STATUS_CODE_STACK = "statusCodeStack";
    public final String STATUS_MESSAGE = "statusMessage";
    public final String WAIT_TIME = "waitTime";
    public final String REGISTRATION_TIME = "registrationTime";
    public final String CLIENT_TIME = "clientTime";    
    public final String RECEIPT_NUM = "receiptNum";
    public final String PAY_ID = "pay_id";
    public final String REGION_NUMBER = "regionNumber";
    public final String TRADE_POINT = "tradePoint";
    public final String TRANSACTION_TYPE = "transactionType";
    public final String FUNCTION_INSTANCE_ID = "functionInstanceId";
    public final String FUNCTION_TYPE_ID = "functionTypeId";
    public final String ACTUAL_PARAMS_LIST = "params";
    public final String BALANCE_CODE = "balanceCode";
    public final String ERROR_LIST = "errorList";
    public final String ISSUER_TRANSPORT_DATA = "issuerTransportData";

    public final String CHANGE_SELLER_BALANCE_TYPE = "sellerType";
    public final String CHANGE_OPERATOR_BALANCE_TYPE = "operatorType";
    public final String CHANGE_TERMINAL_BALANCE_TYPE = "terminalType";
	public final String TRANSACTION_TYPE_ID = "transaction_type_id";
    public final String PAYMENT_REASON = "payment_reason";

    public final String CLIENTCERTIFICATE = "clientCertificate";
    public final String CLIENTCERTIFICATEKEY = "clientCertificateKey";

    //Auth part
    public final String TERMINAL_SN = "terminalSN";
    public final String TERMINAL_PSWD = "terminalPswd";
    public final String SELLER_CODE = "sellerCode";
    public final String PROCESSING_CODE = "processingCode";
    public final String LOGIN = "login";
    public final String PASSWORD = "password";

    // Inner attributes
    public final String BALANCE_RESERVATION_SET = "balanceReservationSet";
    public final String ENTITY_LIST = "entityList";

    // Registartinon
    public final String TERMINAL_SIGNATURE ="terminalSignature";

    // Transactions
    public final String CLIENT_TRANSACTION = "transaction.client";
    public final String OPERATOR_TRANSACTION = "transaction.operator";
    public final String TERMINAL_TRANSACTION = "transaction.terminal";
    public final String MOBIPAY_KYIVSTAR = "transaction.mobipay.kyivstar";
    //For tariffication and rservation
    public static final String IS_OPERATOR_AMOUNT_SET = "is_o_amount_set";

    // for Gen Voucher
    public final String O_GEN_VOUCHER_SEND = "request.genVoucherSend";
    public final String O_GEN_VOUCHER_STAT = "request.genVoucherStat";

    // for MobileJavaTerminal only
    public final String O_ACTIVATE_TERMINAL = "request.activateMobileTerminal";
    public final String O_ACTIVATE_TERMINAL_RESULT = "response.activateMobileTerminal";
    public final String O_SERVICE_PARAMETERS = "request.servicesParameters";
    public final String O_SERVICE_PARAMETERS_RESULT = "response.servicesParameters";
    public final String O_BALANCES = "request.balances";
    public final String O_BALANCES_RESULT = "response.balances";
    public final String O_REPORT_FULL = "request.reportFull";
    public final String O_REPORT_FULL_RESULT = "response.reportFull";
    public final String O_REPORT_SHORT = "request.reportShort";
    public final String O_REPORT_SHORT_RESULT = "response.reportShort";
    public final String IS_MOBILE_JAVA = "isMobileJava";
    public final String ACCOUNT = "account";
    public final String FROM = "from";
    public final String TO = "to";
    public final String SELLER_BALANCE = "sellerBalance";
    public final String TERMINAL_BALANCE = "terminalBalance";
    public final String FEE_BEELINE = "feeBeeline";
    public final String FEE_CDMA = "feeCDMA";
    public final String FEE_INTERTELECOM = "feeInterTelecom";
    public final String FEE_KS0124 = "feeKS0124";
    public final String FEE_KS2599 = "feeKS2599";
    public final String FEE_KS100 = "feeKS100";
    public final String FEE_LIFE = "feeLife";
    public final String FEE_UMCPOP = "feeUMCPOP";
    public final String TRANSACTION_LIST = "transactionsList";
    public final String STATUS_OK = "statusOk";
    public final String STATUS_ERROR = "statusError";
    public final String STATUS_UNKNOWN = "statusUnknown";
    public final String COUNT_BEELINE = "countBeeline";
    public final String COUNT_CDMA = "countCDMA";
    public final String COUNT_INTERTELECOM = "countInterTelecom";
    public final String COUNT_KS0124 = "countKS0124";
    public final String COUNT_KS2599 = "countKS2599";
    public final String COUNT_KS100 = "countKS100";
    public final String COUNT_LIFE = "countLife";
    public final String COUNT_UMCPOP = "countUMCPOP";
    public final String SUM_BEELINE = "sumBeeline";
    public final String SUM_CDMA = "sumCDMA";
    public final String SUM_INTERTELECOM = "sumInterTelecom";
    public final String SUM_KS0124 = "sumKS0124";
    public final String SUM_KS2599 = "sumKS2599";
    public final String SUM_KS100 = "sumKS100";
    public final String SUM_LIFE = "sumLife";
    public final String SUM_UMCPOP = "sumUMCPOP";
    public final String COUNT_TOTAL = "countTotal";
    public final String SUM_TOTAL = "sumTotal";

    /** Return billing message operation name
     * @return operation name */
    public String getOperationName();
    /** Return message string attribute value by name
     * @param name attribute name
     * @return attribute value */
    public String getAttributeString(String name);
    /** Return message attribute value by name
     * @param name attribute name
     * @return attribute value */
    public Object getAttribute(String name);
    /** Return message attributes names as array of String
     * @return array of attributes names */
    public String[] getAttributesNames();
}


