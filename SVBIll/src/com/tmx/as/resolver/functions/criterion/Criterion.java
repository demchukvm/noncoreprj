package com.tmx.as.resolver.functions.criterion;

import com.tmx.as.resolver.functions.Function;
import com.tmx.as.resolver.FunctionSyntaxException;
import com.tmx.as.base.criterion.CriterionWrapper;

public class Criterion implements Function {
    private CriterionWrapper criterion;
    public void set(int paramIndex, String paramName, Object paramValue) throws FunctionSyntaxException {
        criterion=(CriterionWrapper)paramValue;
    }

    public Object returnValue() throws FunctionSyntaxException {
        return criterion;
    }
}
