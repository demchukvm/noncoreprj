package com.tmx.web.actions.core.tools;

import com.tmx.beng.httpsgate.cyberplat.CyberplatGate;
import com.tmx.web.forms.core.tools.CyberplatTransactionInfoForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForm;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.tmx.web.base.BasicDispatchedAction;

public class CyberplatTransactionInfoAction extends BasicDispatchedAction {

    private final String FORWARD = "cyberplatTransactionInfo";

    protected ActionForward defaultAction(ActionMapping mapping,
                                          ActionForm form,
                                          HttpServletRequest request,
                                          HttpServletResponse response,
                                          String methodName) throws Throwable {
        return getStatus(mapping, form, request, response);
    }




    public ActionForward getInfo(ActionMapping mapping,
                                        ActionForm form,
                                        HttpServletRequest request,
                                        HttpServletResponse response) throws Throwable {

        CyberplatTransactionInfoForm f = (CyberplatTransactionInfoForm) form;
        f.validate();

        String billNumber = f.getEbBillNumber().getValue();
        String productCode = f.getSbProduct().getSelectedKey();

        

        return mapping.findForward(FORWARD);
    }






    public ActionForward getStatus(ActionMapping mapping,
                                        ActionForm form,
                                        HttpServletRequest request,
                                        HttpServletResponse response) throws Throwable {

        CyberplatTransactionInfoForm f = (CyberplatTransactionInfoForm) form;

        if(f.getBillingNumber().getValue() != null)
        {
            try{
                f.setCyberplatStatusInfo(CyberplatGate.getInstance().getStatus(f.getBillingNumber().getValue()));
            }catch(Throwable e){
                f.setCyberplatStatusInfo(e.getLocalizedMessage());
            }
        }

        return mapping.findForward(FORWARD);
    }

}
