package com.tmx.beng.iso8583Gate.mapper;

import org.xml.sax.Attributes;

import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;

/**

 */
public class Mapping {
    private List requestMappings;
    /** by protocolVersion */
    private Map parameterMappings;
    /** by protocolVersion */
    private Map errorMappings;
    /** by protocolVersion */
    private Map amountDenominations;    

    public Mapping() {
        requestMappings = new ArrayList();
        parameterMappings = new HashMap();
        errorMappings = new HashMap();
        amountDenominations = new HashMap();
    }

    public void addRequestMapping(RequestMapping rm) {
        requestMappings.add(rm);
    }

    public void addParameterMapping(ParameterMapping pm) {
        parameterMappings.put(pm.getProtocolVersion(), pm);
    }

    public void addErrorMapping(ErrorMapping em){
        errorMappings.put(em.getProtocolVersion(), em);
    }

    public void addAmountDenomination(AmountDenomination ad){
        amountDenominations.put(ad.getProtocolVersion(), ad);
    }

    public RequestMapping createRequestMapping(Attributes attributes) throws Exception {
        return new RequestMapping();
    }

    public List getRequestMappings(){
        return requestMappings;
    }

    public ParameterMapping getParameterMapping(String protocolVersion) {
        return (ParameterMapping)parameterMappings.get(protocolVersion);
    }

    public ErrorMapping getErrorMapping(String protocolVersion) {
        return (ErrorMapping)errorMappings.get(protocolVersion);
    }

    public AmountDenomination getAmountDenomination(String protocolVersion) {
        return (AmountDenomination)amountDenominations.get(protocolVersion);
    }
}

