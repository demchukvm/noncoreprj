package com.tmx.as.base;

import com.tmx.util.InitException;
import com.tmx.as.entities.general.ui_permission.Permission;
import com.tmx.as.entities.general.ui_permission.PermissionType;
import com.tmx.as.entities.general.ui_permission.UserInterface;
import com.tmx.as.entities.general.ui_permission.UserInterfaceType;
import com.tmx.as.entities.general.role.Role;
import com.tmx.as.exceptions.DatabaseException;
import com.tmx.as.exceptions.SecurityException;
import java.util.*;

/**
 * Provide security logic for applicatoin:
 * 1) Provide list of user's credentials
 */
public class SecurityService implements Reconfigurable {
    /** Map of roles permissions [String roleName][List of permissionBundles] */
    private Map rolesPermissoins = new HashMap();
    /** Only single instance available */
    private static SecurityService securityService;

    private SecurityService(){
    }

    /** Return gate context instance */
    public static SecurityService getInstance(){
        if(securityService == null)
            securityService = new SecurityService();

        return securityService;
    }

    public void reload() throws InitException {
        rolesPermissoins = new HashMap();
    }

    /** Reloads role's permissions. Called on user login. */
    public void reloadUIPermissionsForRole(String roleKey) throws DatabaseException, SecurityException {
        /** permission bundle: [permissionType][userInterface][userInterfaceType] */
        List permissionBundleList = new EntityManager().EXECUTE_QUERY(
            Permission.class,
            "getRolePermissions",
            new QueryParameterWrapper[]{
                new QueryParameterWrapper("roleKey", roleKey)
            });

        //prepare permissoin bundle for store
        Map permissionBundle = new HashMap();
        for(Iterator iter = permissionBundleList.iterator(); iter.hasNext();){
            Object[] permBundlObjs = (Object[])iter.next();
            PermissionBundle pb = new PermissionBundle(
                    (PermissionType)permBundlObjs[0],
                    (UserInterface)permBundlObjs[1],
                    (UserInterfaceType)permBundlObjs[2]
            );
            PermissionBundleKey pbk = new PermissionBundleKey(pb.userInterface.getName(), pb.userUnterfaceType.getName());
            permissionBundle.put(pbk, pb);
        }
        rolesPermissoins.put(roleKey, permissionBundle);
    }

    /** Returns permission type assigned for given role, user interface element and user interface type. */
    public String getPermissionType(String roleName, String uiType, String uiName) throws DatabaseException, SecurityException{
        if(Role.SUPERUSER.equals(roleName)){
            return PermissionType.PERMITTED;
        }

        Map permissionBundle = (Map)rolesPermissoins.get(roleName);
        if(permissionBundle == null)
            reloadUIPermissionsForRole(roleName);
        permissionBundle = (Map)rolesPermissoins.get(roleName);
        if(permissionBundle == null)
            throw new SecurityException("err.failed_to_get_permission_bundle_even_afeter_permissions_reload");

        PermissionBundle pb = (PermissionBundle)permissionBundle.get(new PermissionBundleKey(uiName, uiType));
        if(pb != null && pb.permissionType != null)
            return pb.permissionType.getName();

        //RM: This default permission type should be configured for role in DB
        return PermissionType.PERMITTED;
    }


    private class PermissionBundleKey{
        private String uiName = null;
        private String uiType = null;
        private int hashCode = 0;

        private PermissionBundleKey(String uiName, String uiType) throws SecurityException{
            if(uiType == null || uiName == null)
                throw new SecurityException("err.security_service.ui_type_or_name_is_null", new String[]{uiName, uiType});
            this.uiName = uiName;
            this.uiType = uiType;
            hashCode = (uiName + "__???UNIQUE_DELIMITER???__" + uiType).hashCode();
        }

        public boolean equals(Object object){
            if(object == null || !(object instanceof PermissionBundleKey))
                return false;
            if(((PermissionBundleKey)object).uiName.equals(uiName) &&
               ((PermissionBundleKey)object).uiType.equals(uiType))
                return true;
            else
                return false;
        }

        public int hashCode(){
            return hashCode;
        }
    }

    private class PermissionBundle{
        private PermissionType permissionType = null;
        private UserInterface userInterface = null;
        private UserInterfaceType userUnterfaceType = null;

        private PermissionBundle(PermissionType permissionType, UserInterface userInterface, UserInterfaceType userUnterfaceType){
            this.permissionType = permissionType;
            this.userInterface = userInterface;
            this.userUnterfaceType = userUnterfaceType;
        }
    }

}

