package com.tmx.as.entities.bill.balance;

import com.tmx.as.entities.bill.operator.Operator;

public class OperatorBalance extends AbstractBalance {
    private Operator operator;
    private Long operatorId;

    public Operator getOperator() {
        return operator;
    }

    public void setOperator(Operator operator) {
        this.operator = operator;
    }

    public Long getOperatorId() {
        return operatorId;
    }

    public void setOperatorId(Long operatorId) {
        this.operatorId = operatorId;
    }
}