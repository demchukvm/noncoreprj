package com.tmx.as.entities.bill.transaction;

import com.tmx.as.base.BasicEntity;

import java.io.Serializable;

/**
 */
public class TransactionError extends BasicEntity{
    private String errorId;
    private String code;
    private String message;
    private String cause;

    //---------------Constructors
    public TransactionError(){
    }

    /** Used when transaciton with error is saved */
    public TransactionError(String code){
        this.code = code;
    }

    //---------------Properties


    public String getErrorId() {
        return errorId;
    }

    public void setErrorId(String errorId) {
        this.errorId = errorId;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getCause() {
        return cause;
    }

    public void setCause(String cause) {
        this.cause = cause;
    }

    public Serializable getId() {
        return getCode();
    }

    public int getValid() {
        return valid;
    }

    public void setValid(int valid) {
        this.valid = valid;
    }
}
