<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
    <head>
        <meta charset="<?php bloginfo('charset'); ?>" />
        <title><?php
/*
 * Print the <title> tag based on what is being viewed.
 */
global $page, $paged;

wp_title('|', true, 'right');

// Add the blog name.
bloginfo('name');

// Add the blog description for the home/front page.
$site_description = get_bloginfo('description', 'display');
if ($site_description && ( is_home() || is_front_page() ))
    echo " | $site_description";

// Add a page number if necessary:
if ($paged >= 2 || $page >= 2)
    echo ' | ' . sprintf(__('Page %s', 'twentyten'), max($paged, $page));
?></title>
        
        <!--[if IE]>
        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>  
<![endif]-->


        <link rel="profile" href="http://gmpg.org/xfn/11" />
        <link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo('stylesheet_url'); ?>" />
                
        
        
        <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
        <script type="text/javascript" src="<?php bloginfo('template_url');?>/js/jquery-1.7.min.js"></script>
        <script type="text/javascript" src="<?php bloginfo('template_url');?>/js/bless.js"></script>
        <?php
        /* We add some JavaScript to pages with the comment form
         * to support sites with threaded comments (when in use).
         */
        if (is_singular() && get_option('thread_comments'))
            wp_enqueue_script('comment-reply');

        /* Always have wp_head() just before the closing </head>
         * tag of your theme, or you will break many plugins, which
         * generally use this hook to add elements to <head> such
         * as styles, scripts, and meta tags.
         */
        wp_head();

        function is_firefox()
{
return(eregi("firefox", $_SERVER['HTTP_USER_AGENT']));
}
        
        if (isset($_GET['lang'])) {
            $search_value = 84;
        } else {
            $search_value = 5;
        }
        ?>
        <?php if(is_firefox()):?>
  <STYLE type="text/css">
    .gsc-search-button{
        margin-left: -68px;
        margin-top: 1px;
    }
  </STYLE>
<? endif;?>
    </head>



    <body>
		<?php if(isset($_GET['lang']) && $_GET['lang'] == 'en') { ?>
		<style type="text/css">
		#comingsoon {
		 position: absolute;
		 top: 0px;
		 left: 0px;
		 z-index: 9999999;
		 width: 100%;
		 height: 1120px;
		 background: transparent url(/wp-content/themes/bless/images/transp.png);
		}

		#comingsoon h1 {
		 display: table;
		 margin: 200px auto 0 auto;
		 font-size: 36px;
		 color: #fff;
		}
		</style>
		<div id="comingsoon">
		 <h1>English version coming soon…</h1>
		</div>
			<?php if(isset($_SERVER['REDIRECT_URL'])) { ?>
				<script type="text/javascript">document.location.href = '/?lang=en';</script>
			<?php } ?>
		<?php } ?>
        <nav id="top">

            <!-- language switch -->

<?php bless_language_switcher(); ?>
            <!--		<a href="#" class="rus"></a> <a href="#" class="eng active"></a>-->

            <!-- social buttons -->	
            <div class="social">
                <ul>
                    <li class="vk"><a href="#"></a></li>
                    <li class="fb"><a href="#"></a></li>
                    <!-- <li class="lj"><a href="#"></a></li> -->
                    <li class="twi"><a href="#"></a></li>
                </ul>
            </div>

            <!-- search form -->
            <div class="search">
                <form method="GET" action="<?php bloginfo('url');?>">
                    <input name="s" type="text"><input type="submit" value="<?php echo get_field('text_3', $search_value); ?>">
                </form>
            </div>
        </nav>
        <header>
            <div class="logo">
                <a href="<?php bloginfo('url'); ?>"><img src="<?php bloginfo('template_url'); ?>/images/logo.png" width="231" height="77" alt="BLESS"></a>
            </div>

            <div class="slogan">
                <img src="<?php bloginfo('template_url'); ?>/images/new_standart<?php if(isset($_GET['lang']) & $_GET['lang'] == 'en'){echo '-eng';}?>.gif" alt="Новый стандарт связи">
                <p><?php echo get_field('text_1', $search_value); ?></p>
            </div>

            <div class="buy_now" <?php if(isset($_GET['lang']) & $_GET['lang'] == 'en'){echo 'style="width:210px;"';}?>>
                <h2><a href="<?php echo get_permalink(16); ?><?php if(isset($_GET['lang']) & $_GET['lang'] == 'en'){echo '?lang=en';}?>"><img src="<?php bloginfo('template_url'); ?>/images/buy_online<?php if(isset($_GET['lang']) & $_GET['lang'] == 'en'){echo '-eng';}?>.gif" alt="Купить телефон online"></a></h2>
                <p><?php echo get_field('text_2', $search_value); ?></p>
            </div>

        </header>


<?php include_once 'blocks/coloured_menu.php'; ?>