<%@ page language="java" pageEncoding="UTF-8" contentType="text/html; charset=utf-8"
         errorPage="/core/jsp/message/jsperror.jsp" isELIgnored="false" %>
<%@ taglib uri="/WEB-INF/tld/struts-nested.tld" prefix="nested" %>
<%@ taglib uri="/WEB-INF/tld/tmx-controls.tld" prefix="tmx_ctrl" %>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>


<html:form action="/core/restriction/terminal_restriction/manageTerminalRestrictions">
<table width="100%" border="0">
    <!-- BUTTONS -->
    <tr>
        <td style="text-align: left">
            <tmx_ctrl:button property="onCreateButton"/>
            <tmx_ctrl:button property="onEditButton"/>
            <tmx_ctrl:button property="onDeleteButton"/>
        </td>
    </tr>

    <!-- TABLE -->

    <tr>
        <td valign="top" width="100%" align="left">
            <tmx_ctrl:panelFilterSet tableName="restrictionsTable">
            <table>
                <tr>
                    <td class="dataLabel">
                        <tmx_ctrl:writeEntityLabel
                                attrName="{@name}"
                                entityClassName="com.tmx.as.entities.bill.restriction.SellerRestriction"/>:
                    </td>
                    <td class="dataField">
                        <tmx_ctrl:editBox
                                property="restrictionsTable.filterSet.filter(by_restriction_name).parameter(restriction_name).control"/>
                    </td>

                    <td class="dataLabel">
                        <tmx_ctrl:writeEntityLabel
                                attrName="{@name}"
                                entityClassName="com.tmx.as.entities.bill.terminal.Terminal"/>:
                    </td>
                    <td class="dataField">
                        <tmx_ctrl:browseBox
                                property="restrictionsTable.filterSet.filter(by_terminal_id).parameter(terminal_id).control"/>
                    </td>
                </tr>
            </table>
        </tmx_ctrl:panelFilterSet>
        </td>
    </tr>

    <tr>
        <td>
        <table class='one_outer_cell_table' cellspacing='0' cellpadding='0' width="100%">
            <tr>
                <td>
                    <table cellspacing='1' cellpadding='1' width="100%">
                        <!-- Table header -->
                        <tr>
                            <th class="column_header_cell" width="110">
                                <bean:message bundle="core.labels" key="core.common.th.choose"/>
                            </th>

                            <th class="column_header_cell" width="110">
                                <tmx_ctrl:imgTableOrdering tableName="restrictionsTable"
                                                           orderByFieldName="name"
                                                           labelBundle="as:com.tmx.as.entities.bill.restriction.SellerRestriction"
                                                           labelKey="{@name}"/>
                            </th>

                            <th class="column_header_cell" width="110">
                                <tmx_ctrl:imgTableOrdering tableName="restrictionsTable"
                                                           orderByFieldName="terminal.serialNumber"
                                                           labelBundle="as:com.tmx.as.entities.bill.terminal.Terminal"
                                                           labelKey="serialNumber"/>
                            </th>

                            <th class="column_header_cell" width="110">
                                <tmx_ctrl:imgTableOrdering tableName="restrictionsTable"
                                                           orderByFieldName="functionInstance.functionType.name"
                                                           labelBundle="as:com.tmx.as.entities.bill.function.FunctionType"
                                                           labelKey="{@name}"/>
                            </th>


                        </tr>
                        <nested:iterate id="terminalRestriction" property="restrictionsTable.data" type="com.tmx.as.entities.bill.restriction.TerminalRestriction">
                            <tr>
                                <td class='column_data_cell'>
                                    <input type="radio" name="restrictionId" value="<c:out value="${terminalRestriction.id}"/>"
                                           checked="false">
                                </td>
                                <td class='column_data_cell'>
                                        <c:out value="${terminalRestriction.name}"/>
                                </td>
                                <td class='column_data_cell'>
                                    <c:if test="${terminalRestriction.terminal != null}">
                                        <c:out value="${terminalRestriction.terminal.serialNumber}" />
                                    </c:if>
                                </td>
                                <td class='column_data_cell'>
                                    <c:if test="${terminalRestriction.functionInstance != null}">
                                        <c:if test="${terminalRestriction.functionInstance.functionType != null}">
                                            <c:out value="${terminalRestriction.functionInstance.functionType.name}" />
                                        </c:if>
                                    </c:if>
                                </td>

                            </tr>
                        </nested:iterate>
                        <!-- Empty row -->
                        <nested:define id="tableSize" property="restrictionsTable.resultSize" type="java.lang.Integer"/>
                        <c:if test="${tableSize <= 0}">
                            <tr>
                                <td class='column_data_cell' colspan="5" align="center">
                                    <bean:message bundle="core.labels" key="core.common.th.empty_table" />
                                </td>
                            </tr>
                        </c:if>
                    </table>
                </td>
            </tr>
        </table>
        </td>
    </tr>

    <tr>
        <td style="text-align: right">
            <tmx_ctrl:imgTablePaging maxBookmarkCount="10" tableName="restrictionsTable"/>
        </td>
    </tr>

    
    <!-- BUTTONS -->
    <tr>
        <td style="text-align: left">
            <tmx_ctrl:button property="onCreateButton"/>
            <tmx_ctrl:button property="onEditButton"/>
            <tmx_ctrl:button property="onDeleteButton"/>
        </td>
    </tr>
</table>
</html:form>