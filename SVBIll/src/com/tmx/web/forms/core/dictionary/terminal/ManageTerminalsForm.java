package com.tmx.web.forms.core.dictionary.terminal;

import com.tmx.as.entities.bill.seller.Seller;
import com.tmx.as.entities.bill.terminal.EquipmentType;
import com.tmx.as.base.criterion.CriterionWrapper;
import com.tmx.as.base.criterion.Restrictions;
import com.tmx.web.base.BasicActionForm;
import com.tmx.web.controls.Button;
import com.tmx.web.controls.ScrollBox;
import com.tmx.web.controls.EditBox;
import com.tmx.web.controls.CriterionFactory;
import com.tmx.web.controls.table.ControlManagerTable;
import com.tmx.web.controls.table.CheckBoxFilterEnabler;

//this form form
public class ManageTerminalsForm extends BasicActionForm {

    private ControlManagerTable terminalsTable;
    private Button onCreateButton;
    private Button onEditButton;
    private Button onDeleteButton;

    private ScrollBox scrollBoxSellerOwner;
    private ScrollBox scrollBoxEquipmentType;

    private EditBox editBoxBySerialNumber;
    private EditBox editBoxByHardwareSerialNumber;

    protected void init() {
        initScrollBoxes();
        initEditBoxes();
        initButtons();
        initTables();
    }                                            

    private void initEditBoxes() {
        editBoxByHardwareSerialNumber = new EditBox();
        editBoxBySerialNumber = new EditBox();
    }

    private void initScrollBoxes() {
        scrollBoxSellerOwner = new ScrollBox() {
            protected String getKey(Object dataListElement) {return ((Seller) dataListElement).getSellerId().toString();}
            protected String getValue(Object dataListElement) {return ((Seller) dataListElement).getName();}
        };
        scrollBoxSellerOwner.setNullable(true);
        scrollBoxSellerOwner.setSelectedKey(ScrollBox.NULL_KEY);

        scrollBoxEquipmentType = new ScrollBox<EquipmentType>() {
            protected String getKey(EquipmentType dataListElement) {return dataListElement.getEquipmentTypeId().toString();}
            protected String getValue(EquipmentType dataListElement) {return dataListElement.getName();}
        };
        scrollBoxEquipmentType.setNullable(true);
        scrollBoxEquipmentType.setSelectedKey(ScrollBox.NULL_KEY);
    }

    private void initTables() {
        terminalsTable = new ControlManagerTable("query(query_type=retrieve_all," +
                "entity_class=com.tmx.as.entities.bill.terminal.Terminal," +
                "table_attr=sellerOwner," +
                "order(name=id,type=asc)," +
                "page_size=15" +
                ")");
        //terminalsTable.addDependedControl(onDeleteButton).addDependedControl(onEditButton);    -> SuperUserFormFactory
        terminalsTable.addParameterToFilter("by_blocked", new CheckBoxFilterEnabler());

        CriterionFactory criterion = new CriterionFactory() {

            public CriterionWrapper createCriterion() {

                //initialize criterionFactory wrapper
                CriterionWrapper resultCriterion = Restrictions.isNotNull("terminalId");

                //add restrictions
                if (editBoxBySerialNumber.getValue() != null && !editBoxBySerialNumber.getValue().equals("")) {
                    resultCriterion = Restrictions.and(resultCriterion,
                            Restrictions.ilike("serialNumber", "%" + editBoxBySerialNumber.getValue() + "%"));
                }
                if (editBoxByHardwareSerialNumber.getValue() != null && !editBoxByHardwareSerialNumber.getValue().equals("")) {
                    resultCriterion = Restrictions.and(resultCriterion,
                            Restrictions.ilike("hardwareSerialNumber", "%" + editBoxByHardwareSerialNumber.getValue() + "%"));
                }
                if (scrollBoxSellerOwner.getSelectedKey() != null && !scrollBoxSellerOwner.isNullKeySelected()) {
                    resultCriterion = Restrictions.and(resultCriterion,
                            Restrictions.eq("sellerOwner.sellerId", new Long(scrollBoxSellerOwner.getSelectedKey())));
                }
                if (scrollBoxEquipmentType.getSelectedKey() != null && !scrollBoxEquipmentType.isNullKeySelected()) {
                    resultCriterion = Restrictions.and(resultCriterion,
                            Restrictions.eq("equipmentType.equipmentTypeId", new Long(scrollBoxEquipmentType.getSelectedKey())));
                }
                return resultCriterion;
            }
        };
        terminalsTable.setCriterionFactory(criterion);
    }

    private void initButtons() {
        String action = "/core/dictionary/terminal/infoTerminal";
        onCreateButton = new Button();
        onCreateButton.setLabelKey("ctrl.button.label.create");
        onCreateButton.setAction(action);
        onCreateButton.setCommand("preloadForCreate");

        onEditButton = new Button();
        onEditButton.setLabelKey("ctrl.button.label.edit");
        onEditButton.setAction(action);
        onEditButton.setCommand("preloadForUpdate");

        onDeleteButton = new Button();
        onDeleteButton.setLabelKey("ctrl.button.label.delete");
        onDeleteButton.setAction(action);
        onDeleteButton.setCommand("preloadForDelete");
    }

    //--------Properties
    public ControlManagerTable getTerminalsTable() {
        return terminalsTable;
    }

    public void setTerminalsTable(ControlManagerTable terminalsTable) {
        this.terminalsTable = terminalsTable;
    }

    public Button getOnCreateButton() {
        return onCreateButton;
    }

    public void setOnCreateButton(Button onCreateButton) {
        this.onCreateButton = onCreateButton;
    }

    public Button getOnEditButton() {
        return onEditButton;
    }

    public void setOnEditButton(Button onEditButton) {
        this.onEditButton = onEditButton;
    }

    public Button getOnDeleteButton() {
        return onDeleteButton;
    }

    public void setOnDeleteButton(Button onDeleteButton) {
        this.onDeleteButton = onDeleteButton;
    }

    public ScrollBox getScrollBoxSellerOwner() {
        return scrollBoxSellerOwner;
    }

    public void setScrollBoxSellerOwner(ScrollBox scrollBoxSellerOwner) {
        this.scrollBoxSellerOwner = scrollBoxSellerOwner;
    }

    public EditBox getEditBoxBySerialNumber() {
        return editBoxBySerialNumber;
    }

    public void setEditBoxBySerialNumber(EditBox editBoxBySerialNumber) {
        this.editBoxBySerialNumber = editBoxBySerialNumber;
    }


    public EditBox getEditBoxByHardwareSerialNumber() {
        return editBoxByHardwareSerialNumber;
    }

    public void setEditBoxByHardwareSerialNumber(EditBox editBoxByHardwareSerialNumber) {
        this.editBoxByHardwareSerialNumber = editBoxByHardwareSerialNumber;
    }

    public ScrollBox getScrollBoxEquipmentType() {
        return scrollBoxEquipmentType;
    }

    public void setScrollBoxEquipmentType(ScrollBox scrollBoxEquipmentType) {
        this.scrollBoxEquipmentType = scrollBoxEquipmentType;
    }
}
