package com.tmx.beng.base.dbfunctions;

import java.util.List;
import java.util.Arrays;


public class BeelineVoucherTariff extends VoucherTariff{

    protected List<String> getServiceCodes() {
        return Arrays.asList("Beeline");  
    }

}
