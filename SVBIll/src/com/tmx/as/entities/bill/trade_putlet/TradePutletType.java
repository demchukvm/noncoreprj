package com.tmx.as.entities.bill.trade_putlet;

import com.tmx.as.base.BasicEntity;

import java.io.Serializable;


public class TradePutletType extends BasicEntity {
    private Long tradePutletTypeId;
    private String name;
    private String description;

    public Serializable getId() {
        return tradePutletTypeId;
    }

    public int getValid() {
        return valid;
    }

    public void setValid(int valid) {
        this.valid = valid;
    }

    public Long getTradePutletTypeId() {
        return tradePutletTypeId;
    }

    public void setTradePutletTypeId(Long tradePutletTypeId) {
        this.tradePutletTypeId = tradePutletTypeId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
