package com.tmx.as.blogic.tariff;

import com.tmx.as.base.FilterWrapper;
import com.tmx.as.base.criterion.CriterionWrapper;
import com.tmx.as.base.criterion.Restrictions;
import com.tmx.as.blogic.BlogicException;
import com.tmx.as.entities.bill.seller.Seller;
import com.tmx.as.entities.bill.tariff.AbstractTariff;
import com.tmx.as.entities.bill.tariff.SellerTariff;
import com.tmx.as.exceptions.DatabaseException;

import java.util.Iterator;
import java.util.List;


public class SellerTariffManager extends AbstractTariffManager{

    //template methods
    protected AbstractTariff getSpecificTariff(Long tariffId) throws DatabaseException, BlogicException {
        return (AbstractTariff) RETRIEVE(SellerTariff.class, tariffId, new String[]{"seller","functionInstance.functionType"});  
    }


    public List<SellerTariff> getSellerTarifs(Long sellerId) throws DatabaseException {
        CriterionWrapper criterions [] = new CriterionWrapper[] {
                Restrictions.eq("sellerId", sellerId)};
       return RETRIEVE_ALL(SellerTariff.class, criterions, new String[]{"functionInstance.functionParameters"});
    }

    protected void saveSpecificTariff(AbstractTariff tariff) throws DatabaseException, BlogicException {
        SellerTariff sellerTariff = (SellerTariff) tariff;
        if(sellerTariff.getSeller() == null)
            throw new BlogicException("err.seller_tariff_manager.save_tariff.absent_seller_in_seller_tariff");
        if(sellerTariff.getSeller().getSellerId() == null)
            throw new BlogicException("err.seller_tariff_manager.save_tariff.absent_seller_id_in_seller");
        SAVE(sellerTariff);
    }

    protected void deleteSpecificTariff(Long tariffId) throws DatabaseException, BlogicException {
        DELETE(SellerTariff.class, tariffId);
    }

    protected void copySpecificTariff(Long tariffId, Long destOwnerId) throws DatabaseException, BlogicException {
        BEGIN("svbill");
            Seller seller = new Seller();
            seller.setSellerId(destOwnerId);
            SellerTariff tariff = (SellerTariff) RETRIEVE(SellerTariff.class, tariffId, new String[]{"functionInstance.functionType","functionInstance.functionParameters"});

            if (tariff == null)
                throw new BlogicException("err.seller_tariff_manager.copy_tariff.absent_tariff_in_db");

            try {
                SellerTariff cloneTariff = (SellerTariff) tariff.clone();
                cloneTariff.setSeller(seller);

                if(cloneTariff.getFunctionInstance() == null)
                    throw new BlogicException("err.seller_tariff_manager.copy_tariff.function_instance_is_absent");
                
                SAVE(cloneTariff.getFunctionInstance());
                SAVE(cloneTariff);
            } catch (CloneNotSupportedException e) {
                ROLLBACK("svbill");
                throw new BlogicException("err.seller_tariff_manager.copy_tariff.clone_fail",e);
            }
        COMMIT("svbill");
    }

    protected void copyAllSpecificTariffs(Long srcOwnerId, Long destOwnerId) throws DatabaseException, BlogicException {
        BEGIN("svbill");
            FilterWrapper bySellerId = new FilterWrapper("by_seller_id");
            bySellerId.setParameter("seller_id", srcOwnerId);
            List tariffs = RETRIEVE_ALL(SellerTariff.class, new FilterWrapper[]{bySellerId});
        
            if( tariffs==null || tariffs.size()<=0)
                throw new BlogicException("err.seller_tariff_manager.copy_all_tariffs.absent_tariff_in_db");

            Seller seller = new Seller();
            seller.setSellerId(destOwnerId);

            Iterator iterator = tariffs.iterator();
            try {
                while (iterator.hasNext()){
                    SellerTariff cloneTariff = (SellerTariff) ((SellerTariff)iterator.next()).clone();
                    cloneTariff.setSeller(seller);

                    if(cloneTariff.getFunctionInstance() == null)
                        throw new BlogicException("err.seller_tariff_manager.copy_tariff.function_instance_is_absent");

                    SAVE(cloneTariff.getFunctionInstance());
                    SAVE(cloneTariff);
                }
            } catch (CloneNotSupportedException e) {
                ROLLBACK("svbill");
                throw new BlogicException("err.seller_tariff_manager.copy_all_tariffs.clone_fail");
            }
        COMMIT("svbill");    
    }

}
