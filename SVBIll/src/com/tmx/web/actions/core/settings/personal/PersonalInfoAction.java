package com.tmx.web.actions.core.settings.personal;

import com.tmx.as.base.EntityManager;
import com.tmx.as.blogic.UserManager;
import com.tmx.as.entities.general.organization.Organization;
import com.tmx.as.entities.general.role.Role;
import com.tmx.as.entities.general.user.User;
import com.tmx.as.exceptions.SecurityException;
import com.tmx.web.base.ApplicationEnvironment;
import com.tmx.web.base.BasicDispatchedAction;
import com.tmx.web.base.SecurityService;
import com.tmx.web.forms.core.settings.personal.PersonalInfoForm;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Manage personal settings
 */
public class PersonalInfoAction extends BasicDispatchedAction {

    private static final String FORWARD_PERSONAL_INFO_PAGE = "personalInfoPage";

    public ActionForward preload(ActionMapping mapping,
                                               ActionForm form,
                                               HttpServletRequest request,
                                               HttpServletResponse response) throws Throwable {
        PersonalInfoForm  PersonalInfoForm = (PersonalInfoForm)verifyForm(form, PersonalInfoForm.class);
        PersonalInfoForm.bindData(getLoggedInUser(request));
        return mapping.findForward(FORWARD_PERSONAL_INFO_PAGE);
    }

    public ActionForward save(ActionMapping mapping,
                                               ActionForm form,
                                               HttpServletRequest request,
                                               HttpServletResponse response) throws Throwable {
        PersonalInfoForm  personalInfoForm = (PersonalInfoForm)verifyForm(form, PersonalInfoForm.class);
        personalInfoForm.validate();
        new UserManager().selfChangeUser(personalInfoForm.populateFromControls());
        personalInfoForm.bindData(getLoggedInUser(request));
        return mapping.findForward(FORWARD_PERSONAL_INFO_PAGE);
    }


    public ActionForward defaultAction(ActionMapping mapping,
                                               ActionForm form,
                                               HttpServletRequest request,
                                               HttpServletResponse response,
                                               String methodName) throws Throwable {
        return preload(mapping, form, request, response);
    }

    private User getLoggedInUser(HttpServletRequest request) throws SecurityException {
        SecurityService securityService = (SecurityService) ApplicationEnvironment.getService(SecurityService.class);
        return securityService.getLoggedInUser(request.getSession());
    }
}
