package com.tmx.engines.reportengine.aggrtable.validation;

import java.util.Map;

/**
 * Rule to validate fact function cell data
 * (1) without affect from other facts values
 * or
 * (2) based on  value of other fact functions in the same point of
 * metacube. 
 *
 */
public interface ValidationRule {

    public void init(int runNumber, String factFunctionName);

    /**
     * @param metacubePoint  Map of [String factFunctionName][Object factValue]
     * @return return null if validation success or DataValidationError instance on fail. */
    public DataValidationError validate(Map metacubePoint);

    /** @return ownerFactName name of fact function that is owner of this rule */
    public String getOwnerFactName();

    public int getRunNumber();
}
