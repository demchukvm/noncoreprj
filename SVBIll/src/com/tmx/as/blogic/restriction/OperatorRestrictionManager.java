package com.tmx.as.blogic.restriction;

import com.tmx.as.entities.bill.restriction.AbstractRestriction;
import com.tmx.as.entities.bill.restriction.OperatorRestriction;
import com.tmx.as.exceptions.DatabaseException;
import com.tmx.as.blogic.BlogicException;


public class OperatorRestrictionManager extends AbstractRestrictionManager{
    public void saveRestriction(AbstractRestriction rest) throws DatabaseException, BlogicException {
        OperatorRestriction operatorRestriction = (OperatorRestriction) rest;
        try {
            BEGIN(rest);
            SAVE(operatorRestriction.getFunctionInstance());
            if(operatorRestriction.getOperator() == null)
               throw new BlogicException("err.operator_restriction_manager.save_restriction.absent_seller_in_seller_restriction");
            if(operatorRestriction.getOperator().getOperatorId() == null)
                throw new BlogicException("err.operator_restriction_manager.save_restriction.absent_seller_id_in_seller");
            SAVE(operatorRestriction);
            COMMIT(rest);
        } catch (DatabaseException e) {
            ROLLBACK(rest);
            throw e;
        }
    }

    public OperatorRestriction getRestriction(Long restId) throws DatabaseException, BlogicException {
        return (OperatorRestriction) RETRIEVE(OperatorRestriction.class, restId, new String[]{"operator","functionInstance.functionType"});
    }

    public void deleteRestriction(Long restId) throws DatabaseException, BlogicException {
        DELETE(OperatorRestriction.class, restId);
    }
}
