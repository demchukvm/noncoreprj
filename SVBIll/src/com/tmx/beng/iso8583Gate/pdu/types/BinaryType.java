package com.tmx.beng.iso8583Gate.pdu.types;

import com.tmx.beng.iso8583Gate.pdu.RuntimeDataElement;
import com.tmx.beng.iso8583Gate.pdu.IsoParseException;
import com.tmx.beng.iso8583Gate.pdu.IsoSerializeException;
import com.tmx.beng.iso8583Gate.pdu.IncorrectFormatException;
import com.tmx.beng.iso8583Gate.pdu.util.HexConverter;

import java.io.InputStream;
import java.io.IOException;
import java.io.OutputStream;

/**
 */
public class BinaryType extends Type {
    {
        name = "b";
    }
    private final HexConverter hexConverter = new HexConverter();

    public void parse(InputStream is, int length, RuntimeDataElement rde) throws IsoParseException {
        try{
            byte[] data = new byte[getByteLength(length)];
            is.read(data);
            rde.setValue(data);
        }
        catch(IOException e){
            throw new IsoParseException("err.binary_type.parsing_failed", e);
        }
    }

    /** used to parse sub fields */
    public byte[] parse(InputStream is, int length) throws IsoParseException {
        try{
            byte[] data = new byte[getByteLength(length)];
            is.read(data);
            return data;
        }
        catch(IOException e){
            throw new IsoParseException("err.binary_type.parsing_failed", e);
        }
    }

    public SerializedValue serialize(boolean fixedLength, int bitLengthLimit, RuntimeDataElement rde) throws IsoSerializeException{
        try{
            return serialize(fixedLength, bitLengthLimit, rde.getBinary());
        }
        catch(IncorrectFormatException e){
            throw new IsoSerializeException("err.binary_type.serialization_failed", e);
        }
    }

    public SerializedValue serialize(boolean fixedLength, int bitLengthLimit, byte[] binaryData) throws IsoSerializeException{
        try{
            if(binaryData.length > getByteLength(bitLengthLimit))
                throw new IncorrectFormatException("err.binary_type.byte_array_size_more_then_maxlength", new String[]{String.valueOf(binaryData.length), String.valueOf(getByteLength(bitLengthLimit))});

            if(fixedLength && (binaryData.length < getByteLength(bitLengthLimit)))
                binaryData = expandFixedLengthValue(binaryData, getByteLength(bitLengthLimit));

            SerializedValue sv = new SerializedValue();
            sv.data = binaryData;
            sv.length = getBitLength(sv.data.length);
            return sv;
        }
        catch(IncorrectFormatException e){
            throw new IsoSerializeException("err.binary_type.serialization_failed", e);
        }
    }

    protected byte[] expandFixedLengthValue0(byte[] value, int fixedByteLength) throws IncorrectFormatException{
        //fill tail of byte array by 0 to match fixed length
        byte[] expandedData = new byte[fixedByteLength];
        System.arraycopy(value, 0, expandedData, 0, value.length);
        return expandedData;
    }

    public boolean getBit(int bitnum, byte[] binaryData){
        int byteNum = bitnum / 8;
        int localBitnum = bitnum - byteNum * 8;
        byte data = binaryData[byteNum];

        //prepare mask
        int mask = 0x1;
        mask = mask << localBitnum;
        return (data & mask) > 0;
    }

    /** bits in bitmaps start from #1 and go from left to right along all bytes */
    public boolean getBitmapBit(int bitnum, byte[] bitmap){
        if(bitnum <=0 || bitnum > bitmap.length*8)
            return false;

        int byteNum = (bitnum-1) / 8;
        int localBitnum = 8 - (bitnum - byteNum * 8);
        byte data = bitmap[byteNum];

        //prepare mask
        int mask = 0x1;
        mask = mask << localBitnum;
        return (data & mask) > 0;
    }

    public String getHexString(byte[] data){
        return hexConverter.getStringFromHex(data, 0, data.length*2);//2 hex symbols in 1 byte
    }

    public int getByteLength(int bitLength){
        return (bitLength >> 3) + ((bitLength % 8 == 0) ? 0 : 1); //8 bits in 1 byte
    }

    private int getBitLength(int byteLength){
        return byteLength*8;
    }

}
