package com.tmx.web.actions.core.dictionary.territory;

import com.tmx.web.base.BasicDispatchedAction;
import com.tmx.web.base.Roadmap;
import com.tmx.web.forms.core.dictionary.territory.InfoRegionForm;
import com.tmx.as.base.EntityManager;
import com.tmx.as.entities.network.territory.Region;
import com.tmx.as.entities.network.territory.Country;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForm;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

public class InfoRegionAction extends BasicDispatchedAction {

    private static final String FORWARD_MANAGE_ACTION = "manageAction";
    private static final String FORWARD_INFO_PAGE = "infoPage";
    private static final String FORWARD_MANAGE_DELETE_ACTION = "manageDeleteAction";

    public void preload(EntityManager entityManager, InfoRegionForm form) throws Throwable{
        List data=entityManager.RETRIEVE_ALL(Country.class,null);
        form.getCountry().bind(data);
    }

    public ActionForward preloadForCreate(ActionMapping mapping,
                                          ActionForm form,
                                          HttpServletRequest request,
                                          HttpServletResponse response) throws Throwable {
        InfoRegionForm formT = (InfoRegionForm) verifyForm(form, InfoRegionForm.class);
        preload(new EntityManager(),formT);
        formT.bindData(new Region());
        return mapping.findForward(FORWARD_INFO_PAGE);
    }

    public ActionForward preloadForUpdate(ActionMapping mapping,
                                          ActionForm form,
                                          HttpServletRequest request,
                                          HttpServletResponse response) throws Throwable {
        InfoRegionForm formT = (InfoRegionForm) verifyForm(form, InfoRegionForm.class);
        EntityManager entityManager=new EntityManager();
        preload(entityManager,formT);
        Region data=(Region)entityManager.RETRIEVE(Region.class,formT.getSelectedId(),null);
        formT.bindData(data);
        return mapping.findForward(FORWARD_INFO_PAGE);
    }

    public ActionForward update(ActionMapping mapping,
                              ActionForm form,
                              HttpServletRequest request,
                              HttpServletResponse response) throws Throwable {
        validateTransactionToken(request);
        InfoRegionForm formT = (InfoRegionForm) verifyForm(form, InfoRegionForm.class);
        formT.validate(mapping.findForward(FORWARD_INFO_PAGE));
        Region data = formT.populateFromControls(new Region());
        new EntityManager().SAVE(data);
        return mapping.findForward(FORWARD_MANAGE_ACTION);
    }

    public ActionForward preloadForDelete(ActionMapping mapping,
                                          ActionForm form,
                                          HttpServletRequest request,
                                          HttpServletResponse response) throws Throwable {
        Roadmap roadmap=getRoadmap();
        roadmap.collectParameter("/core/message/confirmation", "leftMenuName", "dictionaries");
        roadmap.setPageForward("confirmation","onOk","local:"+this.getName()+"#"+FORWARD_MANAGE_DELETE_ACTION);
        roadmap.setPageForward("confirmation","onCancel","local:"+this.getName()+"#"+FORWARD_MANAGE_ACTION);
        return mapping.findForward("confirmation");
    }

    public ActionForward delete(ActionMapping mapping,
                              ActionForm form,
                              HttpServletRequest request,
                              HttpServletResponse response) throws Throwable {
        InfoRegionForm formT = (InfoRegionForm) verifyForm(form, InfoRegionForm.class);
        new EntityManager().DELETE(Region.class,formT.getSelectedId());
        return mapping.findForward(FORWARD_MANAGE_ACTION);
    }

    public ActionForward defaultAction(ActionMapping mapping,
                                       ActionForm form,
                                       HttpServletRequest request,
                                       HttpServletResponse response,
                                       String methodName) throws Throwable {

        return preloadForCreate(mapping, form, request, response);
    }
}
