package com.tmx.beng.medaccess.beeline;

import com.tmx.beng.medaccess.MediumAccess;
import com.tmx.beng.medaccess.MediumConnection;
import com.tmx.beng.medaccess.GateException;

/**
  Medium Access to obtain Beeline medium connection
 */
public class BeelineGateMediumAccess implements MediumAccess {

    public void init() {
    }

    public MediumConnection getConnection() throws GateException {
        return new BeelineGateMediumConnection();
    }
}
