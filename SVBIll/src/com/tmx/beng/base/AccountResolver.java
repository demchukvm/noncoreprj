package com.tmx.beng.base;

import com.tmx.util.cache.FileCache;
import com.tmx.util.cache.CacheException;
import com.tmx.beng.base.beans.*;

import java.io.InputStream;
import java.util.HashMap;

public class AccountResolver extends FileCache {
    private static AccountResolver instance = null;

    protected Object getRealObjectFromFile(InputStream is, String path) throws CacheException {
        AccountResolverDocument root;

        try{
            root = AccountResolverDocument.Factory.parse(is);
        }catch(Throwable e){
            throw new CacheException("err.account_resolver.failed_to_parse_config", new String[]{ path }, e);
        }

        AccountType[] accountTypes = root.getAccountResolver().getAccountArray();
        HashMap accountMap = new HashMap();
        for(int i = 0; i < accountTypes.length; i++){
            accountMap.put(accountTypes[i].getChannel(), accountTypes[i].getPrefix());
        }

        return accountMap;
    }

    public static AccountResolver getInstance(){
        if(instance == null){
            instance = new AccountResolver();
        }
        return instance;
    }

    private AccountResolver(){
        maxSize = 100;
        cleanCount = 10;
        cleanType = CleanAlgorithm.Type.CLEAN_N_SELDOM_ACCESSED;
    }

    public String getPrefix(String channel, String configFile) throws BillException{
        HashMap accountMap;
        try{
            accountMap = (HashMap)get(configFile);
        }catch(CacheException e){
            throw new BillException("err.bill.failed_to_get_codemap", new String[]{ configFile }, e);
        }

        return (String) accountMap.get(channel);
    }

}
