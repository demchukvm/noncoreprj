package com.tmx.as.entities.mobipay.umc;

import com.tmx.as.base.BasicEntity;
import java.util.Date;
import java.io.Serializable;

/**
    Entity to handle UMC gate transaction
 */
public class UmcGateTransaction extends BasicEntity {
    private Long umcGateTransactionId;
    private String msisdn;
    private String payAccountNumber;
    private Double amount;
    private String terminal;
    /** PhoneType:
     * = 0 - refill for this abonent is impossible 
     * = 1 - postpaid abonent
     * = 2 - prepaid abonent
     *  */
    private Long phoneType;
    /** Error code (handled by UMC billing)*/
    private Long error;
    /** SVBILL billing transaction identifier */
    private String billTransactionNum;
    /** SVBILL UMC gate transaction identifier. There are several deliery attempts for one 'billTransactionNum'
     * so partner id is unique for each one. */
    private String partnerId;
    /** UMC billing transaction identifier */
    private String transId;
    /** Operation result code (handled by UMC billing):
     *  = 0 - ok
     *  = 1 - failed, look error attribute for details. */
    private Long result;
    /** SVBILL local time of transaction initiation.
     * Set by umc https gate on transaction begin */
    private Date localTime;
    /** Transaction status code handled by SBILL */
    private Integer statusCode;
    private String statusMessage;


    public Long getUmcGateTransactionId() {
        return umcGateTransactionId;
    }

    public void setUmcGateTransactionId(Long umcGateTransactionId) {
        this.umcGateTransactionId = umcGateTransactionId;
    }

    public String getMsisdn() {
        return msisdn;
    }

    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }

    public String getPayAccountNumber() {
        return payAccountNumber;
    }

    public void setPayAccountNumber(String payAccountNumber) {
        this.payAccountNumber = payAccountNumber;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Long getPhoneType() {
        return phoneType;
    }

    public void setPhoneType(Long phoneType) {
        this.phoneType = phoneType;
    }

    public Long getError() {
        return error;
    }

    public void setError(Long error) {
        this.error = error;
    }

    public String getBillTransactionNum() {
        return billTransactionNum;
    }

    public void setBillTransactionNum(String billTransactionNum) {
        this.billTransactionNum = billTransactionNum;
    }

    public String getPartnerId() {
        return partnerId;
    }

    public void setPartnerId(String partnerId) {
        this.partnerId = partnerId;
    }

    public String getTransId() {
        return transId;
    }

    public void setTransId(String transId) {
        this.transId = transId;
    }

    public Long getResult() {
        return result;
    }

    public void setResult(Long result) {
        this.result = result;
    }

    public Integer getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }

    public void setStatusCodeInt(int statusCode){
        this.statusCode = new Integer(statusCode);        
    }

    public String getStatusMessage() {
        return statusMessage;
    }

    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }

    public Date getLocalTime() {
        return localTime;
    }

    public void setLocalTime(Date localTime) {
        this.localTime = localTime;
    }

    public Serializable getId() {
        return getUmcGateTransactionId();
    }

    public int getValid() {
        return valid;
    }

    public void setValid(int valid) {
        this.valid = valid;
    }

    public String getTerminal() {
        return terminal;
    }

    public void setTerminal(String terminal) {
        this.terminal = terminal;
    }
}
