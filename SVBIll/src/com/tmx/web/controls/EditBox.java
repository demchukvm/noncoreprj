package com.tmx.web.controls;

import java.text.ParseException;
import java.text.NumberFormat;
import java.text.DecimalFormat;
import java.util.Locale;

/**
 * Edit box control
 */
public class EditBox extends BasicControl{
    private String value = null;
    private String firstUpdateValue = null;

    /** Constants */
    public static String VALUE_SUFFIX = "value";//should be exactly the same as value property name

    public void initialize() {
        /** Do nothing */
    }

    public String getValue() {
        return value;
    }

    /**
     * This method make first update changes.
     * now, it set "" on first update.
     * @param value
     * @see com.tmx.web.controls.BasicControl
     */
    public void setValue(String value) {
        if(!isUpdated()){
            firstUpdateValue = "";
            notifyUpdate();
        }
        this.value = value;
    }

    /** Override setMandatory() method */    
    public void setMandatory(boolean mandatory) {
        super.setMandatory(mandatory);
        if(mandatory)
            addValidationRule(createValidationRule_NOT_NULL());
    }

    //------------------Common used validation rules
    public static ValidationRule createValidationRule_NOT_NULL() {

        return new ValidationRule("not_null", null) {
            public boolean validate(BasicControl control, Object[] args) {
                return !(control instanceof EditBox &&
                        (((EditBox) control).getValue() == null ||
                        ((EditBox) control).getValue().trim().length() <= 0));
            }
        };
    }


    public static ValidationRule createValidationRule_MIN_FIELD_LENGTH(int minLength) {

        return new ValidationRule("min_field_length", new Object[]{new Integer(minLength)}) {
            public boolean validate(BasicControl control, Object[] args) {
                return !(control instanceof EditBox &&
                        args != null && args[0] != null && args[0] instanceof Integer &&
                        (((EditBox) control).getValue() == null ||
                        ((EditBox) control).getValue().trim().length() < ((Integer) args[0]).intValue()));
            }
        };
    }

    public static ValidationRule createValidationRule_MAX_FIELD_LENGTH(int maxLength) {

        return new ValidationRule("max_field_length", new Object[]{new Integer(maxLength)}) {
            public boolean validate(BasicControl control, Object[] args) {
                return !(control instanceof EditBox &&
                        args != null && args[0] != null && args[0] instanceof Integer &&
                        (((EditBox) control).getValue() == null ||
                        ((EditBox) control).getValue().trim().length() > ((Integer) args[0]).intValue()));
            }
        };
    }

    public static ValidationRule createValidationRule_PASSWD_FIELDS_MATCH(EditBox editBox) {

        return new MultiControlValidationRule("password_fields_match", null, new BasicControl[]{editBox}) {
            public boolean validate(BasicControl control, Object[] args, BasicControl[] coupledCtrls) {
                return !(control instanceof EditBox &&
                        coupledCtrls != null && coupledCtrls[0] != null && coupledCtrls[0] instanceof EditBox &&
                        ((EditBox) control).getValue() != null && ((EditBox)coupledCtrls[0]).getValue() != null &&
                        !((EditBox) control).getValue().trim().equals(((EditBox)coupledCtrls[0]).getValue().trim()));
            }
        };
    }

    private static ValidationRule createValidationRule_NUMBER_FORMAT(DecimalFormat formatter) {

        return new ValidationRule("number_format", new Object[]{formatter.toLocalizedPattern() /** pattern is used to publish in error message */, formatter}) {

            public boolean validate(BasicControl control, Object[] args) {
                if(control instanceof EditBox &&
                  ((EditBox) control).getValue() != null &&
                  ((EditBox) control).getValue().trim().length() > 0){
                    if(args != null && args[1] != null && args[1] instanceof DecimalFormat){
                        try{
                            DecimalFormat decimalFormat=(DecimalFormat)args[1];
                            decimalFormat.setGroupingUsed(false); //prevent groupping to avoid mismatch in orignial and formatted strings. 
                            //What a problem? after parsing, even if exception is absent number can be incorrect
                            String valueOld=((EditBox) control).getValue();//<---| we take value from control,
                            Number number=decimalFormat.parse(valueOld);//       | then parse it to a namber, in this line can be exception
                            String valueNew=decimalFormat.format(number);//      | then number to string again, if exception is absent 
                            if(!valueOld.equals(valueNew))//<--------------------| then compare string value before parsing with value after parsing
                                return false;
                        }
                        catch(ParseException e){
                            return false;
                        }
                    }
                }
                return true;
            }
        };
    }

    public static ValidationRule createValidationRule_NUMBER_FORMAT(String pattern) {
        DecimalFormat formatter =  (DecimalFormat)DecimalFormat.getInstance();
        formatter.applyPattern(pattern);
        return createValidationRule_NUMBER_FORMAT(formatter);
    }

    public static ValidationRule createValidationRule_INTEGER_FORMAT(Locale locale) {
        DecimalFormat formatter = (DecimalFormat)DecimalFormat.getIntegerInstance(locale);
        return createValidationRule_NUMBER_FORMAT(formatter);
    }

    public static ValidationRule createValidationRule_FLOAT_FORMAT(Locale locale) {
        DecimalFormat formatter = (DecimalFormat)DecimalFormat.getNumberInstance(locale);
        return createValidationRule_NUMBER_FORMAT(formatter);
    }

    protected void safeReset() {
        value = firstUpdateValue;
    }

}
