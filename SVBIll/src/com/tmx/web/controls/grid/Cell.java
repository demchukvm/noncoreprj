package com.tmx.web.controls.grid;

public interface Cell {
    public int getColSpan();
    public int getRowSpan();
    public String getValue();
    public String getStyleClass();
    public String getStyle();
}
