package com.tmx.beng.base.processors;

import com.tmx.as.base.EntityManager;
import com.tmx.as.entities.bill.function.ActualFunctionParameter;
import com.tmx.as.entities.bill.function.FunctionType;
import com.tmx.as.exceptions.DatabaseException;
import com.tmx.beng.base.*;
import com.tmx.beng.base.dbfunctions.BasicTariffFunction;
import com.tmx.beng.base.dbfunctions.BasicFunction;
import com.tmx.beng.base.cache.DBCache;
import com.tmx.beng.base.cache.FunctionWrapper;
import com.tmx.beng.base.cache.InvokeFunctionException;
import com.tmx.util.InitException;

import java.util.Arrays;
import java.util.ArrayList;

public class RetrieveDefaultParamsProcessor extends TaskProcessor {

	protected Task process0(Task task, DBCache dbCache, BillingMessageWritable msg) throws BillException {
        logger.debug("Retrieve default params...");
        BillingMessageWritable respMsg = new BillingMessageImpl();
        respMsg.setAttribute(BillingMessage.STATUS_CODE, 0);
        Long functionTypeId = (Long) msg.getAttribute(BillingMessage.FUNCTION_TYPE_ID);
        try {
			FunctionType type = (FunctionType) new EntityManager().RETRIEVE(FunctionType.class, functionTypeId);

            if (type == null)
                throw new BillException("err.retrieve_null_object.", new String[] {functionTypeId.toString()});

            logger.debug("Seller tariff name: " + type.getName());
            FunctionWrapper wrapper = new FunctionWrapper(type);
            ((BasicFunction)wrapper.getObjectInstance()).init(task);
            ActualFunctionParameter[] params = wrapper.getDefaultFunctionParam();
            dumpParams(params);

            if (params == null || params.length == 0)
                respMsg.setAttribute(BillingMessage.ACTUAL_PARAMS_LIST, new ArrayList());
            else
                respMsg.setAttribute(BillingMessage.ACTUAL_PARAMS_LIST, new ArrayList<ActualFunctionParameter>(Arrays.asList(params)));

            CallBack callBack = task.getCallBack();
	        if(callBack != null)
	            callBack.sendBilledMessage(respMsg);
		} catch (DatabaseException e) {
			throw new BillException("err.load_function_type.error", e);
		} catch (InitException e) {
			throw new BillException("err.create_function_wrapper.error", e);
		} catch (InvokeFunctionException e) {
			throw new BillException("err.invoke_get_params_method.error", e);
		}
        return task;
	}

    private void dumpParams(ActualFunctionParameter[] parameters) {
        String result = "Default params: ";
        if (parameters == null)
            result += "null";
        else {
            for (ActualFunctionParameter parameter : parameters) {
                result += parameter.getName() + " " + parameter.getOrderNum() + " " + parameter.getType() + " " +
                        parameter
                                .getValue() + " " + parameter.getValid();
            }
        }
        logger.debug(result);
    }



}
