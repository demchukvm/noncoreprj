package com.tmx.beng.base.processors;

import com.tmx.beng.base.*;
import com.tmx.beng.base.cache.DBCache;
import com.tmx.beng.base.cache.FunctionWrapper;
import com.tmx.beng.base.cache.DBCacheException;
import com.tmx.beng.base.dbfunctions.BasicLimitFunction;

import java.util.*;

/**

 */
public class LimitControlProcessor extends TaskProcessor {

    public Task process0(Task task, DBCache dbCache, BillingMessageWritable msg)throws BillException{
        logger.debug("Check limits...");
        try{

            // terminal limits
            List list = dbCache.getTerminalRestrictions(msg.getAttributeString(BillingMessage.TERMINAL_SN));

            if(list != null){
                for(Iterator it = list.iterator(); it.hasNext();){
                    FunctionWrapper functionWrapper = (FunctionWrapper)it.next();
                    if(!(functionWrapper.getObjectInstance() instanceof BasicLimitFunction))
                        throw new BillException("err.not_limit_function_of_terminal", new String[]{functionWrapper.getObjectInstance().getClass().toString()});
                    ((BasicLimitFunction)functionWrapper.getObjectInstance()).init(task);
                    functionWrapper.invoke();
                }
            }

            // terminal Group limits
            try{
                list = dbCache.getTerminalGroupRestrictions(msg.getAttributeString(BillingMessage.TERMINAL_SN));
                if(list != null){
                    for(Iterator it = list.iterator(); it.hasNext();){
                        FunctionWrapper functionWrapper = (FunctionWrapper)it.next();
                        if(!(functionWrapper.getObjectInstance() instanceof BasicLimitFunction))
                            throw new BillException("err.not_limit_function_of_terminalgroup", new String[]{functionWrapper.getObjectInstance().getClass().toString()});
                        ((BasicLimitFunction)functionWrapper.getObjectInstance()).init(task);
                        functionWrapper.invoke();
                    }
                }
            }
            catch(DBCacheException e){
                //ignore if terminal group not found
            }


            // seller limits
            Map<String, List> sellerRestrictionsMap = new HashMap<String, List>();
            String sellerCode = msg.getAttributeString(BillingMessage.SELLER_CODE);
//            list = dbCache.getSellerRestrictions(sellerCode);
//            sellerRestrictionsMap.put(sellerCode, list);
            //Retrive parent sellers code
            List<String> sellersParentsChainCodesIncludeSeller = dbCache.getSellerWrapperParentsChainCodes(sellerCode);
//            sellersParentsChainCodesIncludeSeller.add(sellerCode);//add this seller code to
            for (String parentSellerCode : sellersParentsChainCodesIncludeSeller) {
                List sellerRestrictions = dbCache.getSellerRestrictions(parentSellerCode);
                if (sellerRestrictions.size() > 0) {
                    //collect all retsrictions
                    sellerRestrictionsMap.put(parentSellerCode, sellerRestrictions);
                }
            }

            Map<String, String> funcParameters = new HashMap<String, String>(1);
            //Invoke limit funkions for each sellers
            for (String codKey : sellerRestrictionsMap.keySet()) {
                //add Seller code as function parameter, to separate sellers in function
                funcParameters.put("seller_code", codKey);
                //Retrive list of restriction for this seller
                List sellerRestrictionList = sellerRestrictionsMap.get(codKey);
                if (sellerRestrictionList != null) {
                    for (Object sellerRestriction : sellerRestrictionList) {
                        FunctionWrapper functionWrapper = (FunctionWrapper) sellerRestriction;
                        if (!(functionWrapper.getObjectInstance() instanceof BasicLimitFunction)) {
                            throw new BillException("err.not_limit_function_of_seller", new String[]{functionWrapper.getObjectInstance().getClass().toString()});
                        }
                        ((BasicLimitFunction) functionWrapper.getObjectInstance()).init(task);
                        functionWrapper.invoke(funcParameters, new Object[]{null});
                    }
                }
            }

            // processing limits
            list = dbCache.getProcessingRestrictions(msg.getAttributeString(BillingMessage.PROCESSING_CODE));

            if(list != null){
                for(Iterator it = list.iterator(); it.hasNext();){
                    FunctionWrapper functionWrapper = (FunctionWrapper)it.next();
                    if(!(functionWrapper.getObjectInstance() instanceof BasicLimitFunction))
                        throw new BillException("err.not_limit_function_of_processing", new String[]{functionWrapper.getObjectInstance().getClass().toString()});
                    ((BasicLimitFunction)functionWrapper.getObjectInstance()).init(task);
                    functionWrapper.invoke();
                }
            }

            // operator limits
            list = dbCache.getOperatorRestrictions(msg.getAttributeString(BillingMessage.SERVICE_CODE));

            if(list != null){
                for(Iterator it = list.iterator(); it.hasNext();){
                    FunctionWrapper functionWrapper = (FunctionWrapper)it.next();
                    if(!(functionWrapper.getObjectInstance() instanceof BasicLimitFunction))
                        throw new BillException("err.not_limit_function_of_operator", new String[]{functionWrapper.getObjectInstance().getClass().toString()});
                    ((BasicLimitFunction)functionWrapper.getObjectInstance()).init(task);
                    functionWrapper.invoke();
                }
            }
        }
        catch(Throwable e){
            msg.setStatusCode(StatusDictionary.LIMITS_ERROR);
            logger.error(e.getLocalizedMessage(), e);
            if(e instanceof BillException)
                throw (BillException)e;
            throw new BillException(e.getLocalizedMessage(), e);
        }
        return task;
    }
}
