package com.tmx.beng.httpsgate.dacard.pdu;

import java.io.InputStream;
import java.io.OutputStream;

import org.apache.log4j.Logger;

/**
    Dacard protocol assembler disassembler
 */
public class DacardPAD {

    public DacardMessage createMessage(byte[] sessionKey, Logger logger){
        return new DacardMessage(sessionKey, logger);
    }

    public DacardMessage parseMessage(InputStream is, byte[] sessionKey, Logger logger) throws DacardPadException, DacardCypherException {
        DacardMessage dm = new DacardMessage(sessionKey, logger);
        dm.parse(is);
        return dm;
    }

    public void serializeMessage(OutputStream os, DacardMessage dm) throws DacardPadException, DacardCypherException {
        dm.serialize(os);
    }

}
