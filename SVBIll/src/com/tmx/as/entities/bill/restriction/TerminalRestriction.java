package com.tmx.as.entities.bill.restriction;

import com.tmx.as.entities.bill.terminal.Terminal;

/**

 */
public class TerminalRestriction extends AbstractRestriction{
    private Terminal terminal;
    private Long terminalId;

    //--------------Properties
    public Terminal getTerminal() {
        return terminal;
    }

    public void setTerminal(Terminal terminal) {
        this.terminal = terminal;
    }

    public Long getTerminalId() {
        return terminalId;
    }

    public void setTerminalId(Long terminalId) {
        this.terminalId = terminalId;
    }
}
