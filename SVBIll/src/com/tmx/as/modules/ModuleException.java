package com.tmx.as.modules;

import com.tmx.util.StructurizedException;

import java.util.Locale;

/**
 */
public class ModuleException extends StructurizedException {

    public ModuleException(String errKey, Throwable cause){
        super(errKey, cause);
    }

    public ModuleException(String errKey, Throwable cause, Locale locale){
        super(errKey, cause, locale);
    }

    public ModuleException(String errKey, String[] params, Throwable cause){
        super(errKey, params, cause);
    }

    public ModuleException(String errKey){
        super(errKey);
    }

    public ModuleException(String errKey, String[] params){
        super(errKey, params);
    }

    public ModuleException(String errKey, String[] params, Throwable cause, Locale locale){
        super(errKey, params, cause, locale);
    }
}
