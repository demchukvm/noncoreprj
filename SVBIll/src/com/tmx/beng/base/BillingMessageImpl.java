package com.tmx.beng.base;

import java.util.Map;
import java.util.HashMap;
import java.util.List;
import java.util.Vector;

/**
 */
public class BillingMessageImpl implements BillingMessageWritable{
    private Map props = null;
    private String operationName = null;

    public BillingMessageImpl(){
        props = new HashMap();
    }

    public String getOperationName() {
        return operationName;
    }

    public void setAttributeString(String name, String value){
        props.put(name, value);
    }

    public void setAttribute(String name, Object value){
        props.put(name, value);
    }

    public String getAttributeString(String name){
        Object obj = props.get(name);
        if(obj == null) return null;
        if(obj instanceof String){
            return (String)obj;
        }else{
            return obj.toString();
        }
    }

    public Object getAttribute(String name){
        return props.get(name);
    }

    public String[] getAttributesNames(){
        return (String[])props.keySet().toArray(new String[0]);
    }

    public void setOperationName(String operationName) {
        this.operationName = operationName;
    }

    public void setStatusCode(int statusCode){
        List list = (List)props.get(STATUS_CODE_STACK);
        if(list == null){
            list = new Vector();
        }
        list.add(new Integer(statusCode));
        this.props.put(STATUS_CODE_STACK, list);
        this.props.put(STATUS_CODE, Integer.toString(statusCode));
    }

    public void setStatus(int code, String message) {
        setStatusCode(code);
        this.props.put(STATUS_MESSAGE, message);
    }
}
