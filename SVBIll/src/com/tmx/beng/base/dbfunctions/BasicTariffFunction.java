package com.tmx.beng.base.dbfunctions;

import com.tmx.beng.base.BillingMessage;
import com.tmx.beng.base.Reservation;
import com.tmx.beng.base.ReservationSet;
import com.tmx.beng.base.BillingMessageWritable;
import com.tmx.beng.base.cache.InvokeFunctionException;

import java.util.Map;

/**

 */
public abstract class BasicTariffFunction extends BasicFunction{
    public static final String TARIFF_APPLIED = "tariffApplied";
    /** These constants are used to identify tariff's params in billing message */
    /** Tariff owner could be Terminal, TerminalGroup, Seller, Operator. */
    public static final String TARIFF_OWNER_TYPE = "BasicTariffFunction.TARIFF_OWNER_TYPE";
    /** For operator's tariff function affected owner code holds this operator's code. For seller - seller code and so on.*/
    public static final String TARIFF_OWNER_CODE = "BasicTariffFunction.TARIFF_OWNER_CODE";
    /** For operator's tariff function affected owner code holds this operator's balance code. For seller - seller balance code and so on.*/    
    public static final String TARIFF_OWNER_PRIMARY_BALANCE_CODE = "BasicTariffFunction.TARIFF_OWNER_PRIMARY_BALANCE_CODE";

    /** A set of tariffs could be assigned to tariffed entity (Seller, Operator, Terminal etc.) to calculate different services.
     * For example once for vouchers, another for online refill. Only one tariff from that set could be applied for tariffed transaction. Before
     * tariff application it evaluates is it possible to apply for current transaction. Tariff provides logic of this evaluation by this method.
     *
     * This method shouldn't be used to validate tariff's parameters.
     *
     * @param actualParams map of actual params
     * @return true if this tariff could be applied to current transaction*/
    abstract boolean isTariffApplicable(Map actualParams);

    /** apply tariff
     * @param actualParams map of actual params
     * @param reservation allocated for processed owner to be used in tariff.  
     * @throws InvokeFunctionException on tariff calculation error
     * */
    abstract void applyTariff(Map actualParams, Reservation reservation) throws InvokeFunctionException;

    public void invoke(Map actualParams) throws InvokeFunctionException {
        if(isTariffApplicable(actualParams)){
            final Class ownerType = (Class)actualParams.get(TARIFF_OWNER_TYPE);
            final String ownerCode = (String)actualParams.get(TARIFF_OWNER_CODE);
            final String ownerPrimaryBalanceCode = (String)actualParams.get(TARIFF_OWNER_PRIMARY_BALANCE_CODE);
            applyTariff(actualParams, allocateReservationSet().allocateReservation(ownerType, ownerCode, ownerPrimaryBalanceCode));
            task.getProcesedMessage().setAttribute(TARIFF_APPLIED, Boolean.TRUE);
        }
    }

    private ReservationSet allocateReservationSet(){
        BillingMessageWritable billingMessage = task.getProcesedMessage();
        ReservationSet reservationSet = (ReservationSet)billingMessage.getAttribute(BillingMessage.BALANCE_RESERVATION_SET);
        if(reservationSet == null){
            reservationSet = ReservationSet.createReservationSet();
            billingMessage.setAttribute(BillingMessage.BALANCE_RESERVATION_SET, reservationSet);
        }
        return reservationSet;
    }

}
