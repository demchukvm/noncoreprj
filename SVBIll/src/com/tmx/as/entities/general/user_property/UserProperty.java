package com.tmx.as.entities.general.user_property;

import com.tmx.as.base.BasicEntity;
import com.tmx.as.entities.general.user.User;

import java.io.Serializable;

/** Entity to work with properties stored in the DB */
public class UserProperty extends BasicEntity {
    private Long userPropertyId;
    private String key;
    private String value;
    private User user;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Long getUserPropertyId() {
        return userPropertyId;
    }

    public void setUserPropertyId(Long userPropertyId) {
        this.userPropertyId = userPropertyId;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Serializable getId() {
        return getUserPropertyId();
    }

    public int getValid() {
        return valid;
    }

    public void setValid(int valid) {
        this.valid = valid;
    }

}
