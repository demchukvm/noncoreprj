<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<%-- Include common set of tag library declarations for each layout --%>
<%@ page language="java" pageEncoding="UTF-8" contentType="text/html; charset=utf-8" isELIgnored="false" %>
<!--errorPage="/core/jsp/message/jsperror.jsp"-->
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean" %>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html" %>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic" %>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://tmx.com/taglib/tmx-logic" prefix="tmx_logic" %>
<%@ taglib uri="http://tmx.com/taglib/tmx-controls" prefix="tmx_ctrl" %>

<html:xhtml />


<html:html xhtml="true">
	<head>
		<c:if test="${!empty requestScope[pageScope.IS_NOCACHE]}">
            <meta http-equiv="Cache-Control" content="no-cache,no-store,max-age=0"/>
            <meta http-equiv="Pragma" content="no-cache"/>
		</c:if>
        <!-- Set contextPath variable to be used in the inserted tiles -->
        <c:set var="ctxPath" value="${pageContext.request.contextPath}" scope="request"/>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <!--<meta name="copyright" content="Copyright (c) 2003-2007 Technomatix http://www.tmx.com.ua" />-->
        <!--<meta name="publisher" content="Technomatix http://www.tmx.com.ua" />-->
        <meta name="robots" content="index,follow" />
        <!--<meta http-equiv="refresh" content="60">-->

		<%--<html:base />--%>

		<%-- Push tiles attributes in request context --%>
		<tiles:importAttribute scope="request" ignore="true"/>
		<title>
            <bean:message bundle="core.labels" key="core.commons.webapp.prefix"/> -
            <tiles:useAttribute id="titleKey" name="tile.abstractLayout.titleKey" ignore="true"/>
            <c:if test="${titleKey != null}">
                <bean:message bundle="core.labels" key="${titleKey}" />
            </c:if>
        </title>

        <%-- Get list of local Javascripts (scripts used on concrete page, built on this layout) --%>
        <tiles:useAttribute id="pageScriptList" name="tile.abstractLayout.pageScripts" classname="java.util.List" ignore="true"/>

        <tmx_logic:iterator var="js" items="${pageScriptList}">
            <script type="text/javascript" src="<tmx_ctrl:rewriteUrl href="${js}" />"></script>
        </tmx_logic:iterator>

        <%-- Get list of global Javascripts (scripts used on the most of pages or even on every page) --%>
        <tiles:useAttribute id="layoutScriptList" name="tile.abstractLayout.layoutScripts" classname="java.util.List" ignore="true"/>

        <tmx_logic:iterator var="js" items="${layoutScriptList}">
            <script type="text/javascript" src="<tmx_ctrl:rewriteUrl href="${js}" />"></script>
        </tmx_logic:iterator>

        <%-- Get list of hlobal Stylesheets --%>
        <tiles:useAttribute id="layoutStyleList" name="tile.abstractLayout.layoutStyles" classname="java.util.List" ignore="true"/>

        <tmx_logic:iterator var="css" items="${layoutStyleList}">
            <link rel="stylesheet" type="text/css" media="all" href="<tmx_ctrl:rewriteUrl href="${css}" />" />
        </tmx_logic:iterator>

        <%-- Get list of local Stylesheets --%>
        <tiles:useAttribute id="pageStyleList" name="tile.abstractLayout.pageStyles" classname="java.util.List" ignore="true"/>

        <tmx_logic:iterator var="css" items="${pageStyleList}">
            <link rel="stylesheet" type="text/css" media="all" href="<tmx_ctrl:rewriteUrl href="${css}" />" />
        </tmx_logic:iterator>

        <%-- Get List of Print Stylesheets --%>
        <tiles:useAttribute id="printList" name="tile.abstractLayout.printStyles" classname="java.util.List" ignore="true"/>
        
        <tmx_logic:iterator var="css" items="${printList}">
            <link rel="stylesheet" type="text/css" media="print" href="<tmx_ctrl:rewriteUrl href="${css}" />" />
        </tmx_logic:iterator>

        <link rel="SHORTCUT ICON" href="<html:rewrite href="/favicon.ico"/>" />

    </head>

    <body onload="Init();">
         <tiles:insert name="tile.abstractLayout.nestedForm.url" beanScope="request"/>
    </body>

</html:html>
