package com.tmx.util.jce;

import com.tmx.util.StructurizedException;

import java.util.Locale;

/**
 */
public class JceException extends StructurizedException {

    public JceException(String msg, String[] params, Throwable e, Locale locale) {
        super(msg, params, e, locale);
    }

    public JceException(String msg, String[] params) {
        super(msg, params);
    }

    public JceException(String msg, String[] params, Locale locale) {
        super(msg, params, locale);
    }

    public JceException(String msg, Throwable e, Locale locale) {
        super(msg, e, locale);
    }

    public JceException(String msg, Throwable e) {
        super(msg, e);
    }

    public JceException(String msg, Locale locale) {
        super(msg, locale);
    }

    public JceException(String msg) {
        super(msg);
    }

    public JceException(String msg, String[] params, Throwable e) {
        super(msg, params, e);
    }
}
