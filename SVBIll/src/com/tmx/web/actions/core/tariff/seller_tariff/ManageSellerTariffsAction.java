package com.tmx.web.actions.core.tariff.seller_tariff;

import com.tmx.web.actions.core.tariff.AbstractManageTariffsAction;
import com.tmx.as.blogic.tariff.AbstractTariffManager;

import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForm;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class ManageSellerTariffsAction extends AbstractManageTariffsAction {

    protected AbstractTariffManager getTariffManager() {
        return null;
    }

    //roadmap setuping
    public ActionForward defaultAction(ActionMapping mapping,
                                       ActionForm form,
                                       HttpServletRequest request,
                                       HttpServletResponse response,
                                       String methodName) throws Throwable {
        getRoadmap().collectParameter(DEFAULT_VALID_ACTION, "onCancelAction", "/core/tariff/seller_tariff/manageSellerTariffs");
        getRoadmap().collectParameter(DEFAULT_VALID_ACTION, "onSaveAction", "manageTariffsAction");
        return super.defaultAction(mapping, form, request, response, methodName);
    }

}
