package com.tmx.as.entities.bill.tariff;

import com.tmx.as.base.BasicEntity;
import com.tmx.as.entities.bill.function.FunctionInstance;

import java.util.Date;
import java.io.Serializable;

/**
*/
public abstract class AbstractTariff extends BasicEntity implements Cloneable{
    private Long tariffId;
    private String name;
    private Date activationDate;
    private Date deactivationDate;
    private FunctionInstance functionInstance;
    private Boolean active;

    //-------------Properties
    public Long getTariffId() {
        return tariffId;
    }

    public void setTariffId(Long tariffId) {
        this.tariffId = tariffId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getActivationDate() {
        return activationDate;
    }

    public void setActivationDate(Date activationDate) {
        this.activationDate = activationDate;
    }

    public Date getDeactivationDate() {
        return deactivationDate;
    }

    public void setDeactivationDate(Date deactivationDate) {
        this.deactivationDate = deactivationDate;
    }

    public FunctionInstance getFunctionInstance() {
        return functionInstance;
    }

    public void setFunctionInstance(FunctionInstance functionInstance) {
        this.functionInstance = functionInstance;
    }

    public Serializable getId() {
        return getTariffId();
    }

    public int getValid() {
        return valid;
    }

    public void setValid(int valid) {
        this.valid = valid;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }
}
