package com.tmx.beng.iso8583Gate.mapper;

import java.util.Map;
import java.util.HashMap;

/**
 * Direct and Reverse mapping for parameters value.
 * If there is no mutualy uniqueness between original and mapped values
 * then duplicated values would be overriden by last occured in mapping file. 
 */
public class Param {
    private String originalName;
    private String mappedName;
    private Map valueMapping;
    private Map reverseValueMapping;


    public Param() {
        this.valueMapping = new HashMap();
        this.reverseValueMapping = new HashMap();
    }

    public String getOriginalName() {
        return originalName;
    }

    public void setOriginalName(String originalName) {
        this.originalName = originalName;
    }

    public String getMappedName() {
        return mappedName;
    }

    public void setMappedName(String mappedName) {
        this.mappedName = mappedName;
    }

    public void addValueMapping(String originalVal, String mappedVal){
        valueMapping.put(originalVal, mappedVal);
        reverseValueMapping.put(mappedVal, originalVal);
    }

    public String getMappedValue(String originalValue){
        String mappedValue = null;
        if(originalValue != null){
            mappedValue = (String)valueMapping.get(originalValue);
            mappedValue = mappedValue != null ? mappedValue : originalValue;
        }
        else
            mappedValue = null;

        return mappedValue;
    }

    public String getOriginalValue(String mappedValue){
        return (String)reverseValueMapping.get(mappedValue);
    }
}
