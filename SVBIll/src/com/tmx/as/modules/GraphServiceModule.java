package com.tmx.as.modules;

import com.tmx.as.base.SecurityService;
import com.tmx.util.InitException;
import com.tmx.engines.graphengine.base.GraphService;

import java.util.Properties;

/**
 * Created by IntelliJ IDEA.
 * User: shake
 * Date: 25.06.2007
 * Time: 15:56:19
 */
public class GraphServiceModule extends AbstractModule{

    public void start(Properties props)throws ModuleException{
        try{
            GraphService.getInstance().reload();
        }catch(InitException e){
            throw new ModuleException(e.getLocalizedMessage());
        }
    }

    public void shutdownGraceful(Properties props)throws ModuleException{
    }

    public void shutdownAbort(Properties props)throws ModuleException{
    }
}
