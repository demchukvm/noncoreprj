package com.tmx.web.taglib.html.ctrl;

import com.tmx.web.controls.CheckBox;

import javax.servlet.jsp.JspException;

import org.apache.struts.taglib.TagUtils;

/**
 * Check box tag. Supports both set "true" and set "false" valuses
 * to check box. Thus you don't need to reset this check box to false
 * in form reset() method before form population.
 */
public class CheckBoxTag extends AbstractControlTag {
    //CSS styles properties
    private String checkBoxStyleClass = null;
    //-----------------------------Constants
    //html name suffix of chechckbox check field
    private static final String CHECKBOX_CHECK_SUFFIX = "check";
    private static final String CHECKBOX_HIDDEN_SUFFIX = "selected";
    //JS
    private static final String ON_CLICK_SUFFIX = "onClick()";

    //-----------------Properties

    public String getCheckBoxStyleClass() {
        return (checkBoxStyleClass != null) ? checkBoxStyleClass : getDefaultProperty("tag.checkBox.style.checkBoxStyleClass");
    }

    public void setCheckBoxStyleClass(String checkBoxStyleClass) {
        this.checkBoxStyleClass = checkBoxStyleClass;
    }

    //------------------------------Abstract methods
    protected int controlStartTag() throws JspException {
        StringBuffer resultHTML = new StringBuffer();
        CheckBox cb = getCheckBox();
        if(cb == null){
            TagUtils.getInstance().write(pageContext,"CheckBox '"+getProperty()+"' is not found");
            return SKIP_BODY;
        }

        String type = " type=\"checkbox\" ";
        String name = " name=\""+getProperty()+"."+CHECKBOX_CHECK_SUFFIX+"\" ";
        String value = " value=\""+cb.isSelected() +"\" ";
        String checked = cb.isSelected() ? " checked=\"checked\" " : "";
        String styleClass = " class=\""+getCheckBoxStyleClass()+"\" ";
        String onClick = " onclick=\""+ getSeparatorReplacedName(getProperty())+"_"+ON_CLICK_SUFFIX +"\" ";
        String readonly = (cb.isReadonly()) ? " disabled " : "";

        resultHTML.append("<input "+type+name+value+styleClass+checked+onClick+readonly+"\\>\r");
        resultHTML.append(cb.getLabel()+"\r");
        resultHTML.append("<input type=\"hidden\" name=\""+getProperty()+"."+CHECKBOX_HIDDEN_SUFFIX+"\" value=\""+cb.isSelected()+"\" \\>\r"); 

        //Scripts
        resultHTML.append("    <script type=\"text/javascript\">\r");
        resultHTML.append("\r");
        resultHTML.append("        function "+ getSeparatorReplacedName(getProperty())+"_"+ON_CLICK_SUFFIX +"{\r");
        resultHTML.append("            check = getRawObject('"+getProperty()+"."+CHECKBOX_CHECK_SUFFIX+"');\r");
        resultHTML.append("            hidden = getRawObject('"+getProperty()+"."+CHECKBOX_HIDDEN_SUFFIX+"');\r");
        resultHTML.append("            if(check.value == 'true' || check.value == 'on'){\r");
        resultHTML.append("               check.value = false;\r");
        resultHTML.append("               hidden.value = false;\r");
        resultHTML.append("            }\r");
        resultHTML.append("            else{\r");
        resultHTML.append("               check.value = true;\r");
        resultHTML.append("               hidden.value = true;\r");
        resultHTML.append("            }\r");
        resultHTML.append("        }\r");
        resultHTML.append("\r");
        resultHTML.append("    </script>\r");
        resultHTML.append("\r");

         TagUtils.getInstance().write(pageContext,resultHTML.toString());
        return SKIP_BODY;
    }

   private CheckBox getCheckBox() throws JspException{
        Object object = getPropertyObject();
        return (object != null && object instanceof CheckBox) ?
               (CheckBox)object : null;
    }
}
