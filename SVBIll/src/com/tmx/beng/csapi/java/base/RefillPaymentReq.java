package com.tmx.beng.csapi.java.base;

import com.tmx.beng.access.Connection;
import com.tmx.beng.base.BillingMessage;

import java.util.Date;


public class RefillPaymentReq extends DefaultClientReq<RefillPaymentResp>{
    
    RefillPaymentReq(Connection connect) {
        super(connect);
        requestMsg.setOperationName(BillingMessage.O_REFILL_PAYMENT);
    }

    protected RefillPaymentResp handleResponseMessage(BillingMessage responseMsg) {
        RefillPaymentResp resp = new RefillPaymentResp();
        resp.setStatusCode(responseMsg.getAttributeString(BillingMessage.STATUS_CODE));
        final String waitTime = responseMsg.getAttributeString(BillingMessage.WAIT_TIME);
        resp.setWaitTime(waitTime != null ? Long.parseLong(waitTime) : null);
        resp.setStatusMessage(responseMsg.getAttributeString(BillingMessage.STATUS_MESSAGE));
        resp.setNominal(responseMsg.getAttributeString(BillingMessage.VOUCHER_NOMINAL));
        resp.setCode(responseMsg.getAttributeString(BillingMessage.VOUCHER_CODE));
        resp.setSecretCode(responseMsg.getAttributeString(BillingMessage.VOUCHER_SECRET_KEY));
        resp.setBestBeforeDate((Date)responseMsg.getAttribute(BillingMessage.VOUCHER_BEST_BEFORE_DATE));
        resp.setPrice(resp.getNominal() != null ? Double.parseDouble(resp.getNominal()) : null);
        return resp;        
    }

    public void setServiceCode(String serviceCode) {
        requestMsg.setAttributeString(BillingMessage.SERVICE_CODE, serviceCode);
    }

    public void setAmount(String amount) {
        requestMsg.setAttributeString(BillingMessage.AMOUNT, amount);
    }

    public void setPayAccountNumber(String number) {
       requestMsg.setAttributeString(BillingMessage.PAY_ACCOUNT_NUMBER, number);
    }

    public void setMsisdn(String number){
       requestMsg.setAttributeString(BillingMessage.ACCOUNT_NUMBER, number);
    }

    // Proximan 01.11.2010
    /*public void setGenSellerCode(String genSellerCode) {
        requestMsg.setAttributeString(BillingMessage.GEN_SELLER_CODE, genSellerCode);
    }

    public void setGenServiceCode(String genServiceCode) {
        requestMsg.setAttributeString(BillingMessage.GEN_SERVICE_CODE, genServiceCode);
    }

    public void setGenService(String genService) {
        requestMsg.setAttributeString(BillingMessage.GEN_SERVICE, genService);
    }

    public void setGenTerminalSN(String genTerminalSN) {
        requestMsg.setAttributeString(BillingMessage.GEN_TERMINAL_SN, genTerminalSN);
    }*/
    // поле с поддиллером
    // сервайс
    // тип ГЕН


}
