package com.tmx.as.blogic;

import com.tmx.as.base.EntityManager;
import com.tmx.as.base.FilterWrapper;
import com.tmx.as.exceptions.DatabaseException;
import com.tmx.as.entities.network.territory.City;
import com.tmx.as.entities.network.territory.District;
import com.tmx.as.entities.network.territory.Region;
import com.tmx.as.entities.network.territory.Country;
import com.tmx.as.entities.bill.address.Address;

import java.util.List;

public class AddressManager extends EntityManager {

    public Address getAddress(Long id) throws DatabaseException  {
        return (Address)RETRIEVE(Address.class, id, new String[]{"district","region","city"});
    }

    public Country getCountry(Long id) throws DatabaseException  {
        return (Country)RETRIEVE(Country.class, id, null);
    }

    public District getDistrict(Long id) throws DatabaseException  {
        return (District)RETRIEVE(District.class, id, null);
    }

    public Region getRegion(Long id) throws DatabaseException  {
        return (Region)RETRIEVE(Region.class, id, null);
    }

    public City getCity(Long id) throws DatabaseException  {
        return (City)RETRIEVE(City.class, id, null);
    }


    public List getAllDistricts() throws DatabaseException {
        return RETRIEVE_ALL(District.class, null);
    }
    
    public List getAllRegions() throws DatabaseException {
        return RETRIEVE_ALL(Region.class, null);
    }

    public List getAllCities() throws DatabaseException {
        return RETRIEVE_ALL(City.class, null);
    }

    public List getRegionsByDistrict(Long districtId) throws DatabaseException {
        FilterWrapper regionByDistrict = new FilterWrapper("by_district_id");
        regionByDistrict.setParameter("district_id", districtId);
        return RETRIEVE_ALL(Region.class, new FilterWrapper[] {regionByDistrict});
    }
    public List getCitiesByRegion(Long regionId) throws DatabaseException {
        FilterWrapper regionByDistrict = new FilterWrapper("by_region_id");
        regionByDistrict.setParameter("region_id", regionId);
        return RETRIEVE_ALL(City.class, new FilterWrapper[] {regionByDistrict});
    }
}
