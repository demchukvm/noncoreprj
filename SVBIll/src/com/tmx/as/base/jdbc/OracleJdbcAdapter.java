package com.tmx.as.base.jdbc;

import oracle.sql.ARRAY;
import oracle.sql.ArrayDescriptor;

import java.sql.Array;
import java.sql.Connection;
import java.sql.SQLException;

/**

 */
public class OracleJdbcAdapter extends BasicJdbcAdapter implements JdbcAdapter, JdbcAdapterSpi{

    public Array adoptArray(Object[] array, String typeName, Connection jdbcConn) throws SQLException {
        Connection oracleJdbcConnection;
        /** To support apache connection pool unwrap oracle connection.
         *  Property 'accessToUnderlyingConnectionAllowed' should be set to true for this DataSource.
         * @see
         *  */
        if (jdbcConn instanceof org.apache.commons.dbcp.DelegatingConnection)
            oracleJdbcConnection = ((org.apache.commons.dbcp.DelegatingConnection)jdbcConn).getDelegate();
        else
            oracleJdbcConnection = jdbcConn;
        ArrayDescriptor arrayDescriptor = new ArrayDescriptor(typeName, oracleJdbcConnection);
        return new ARRAY(arrayDescriptor, oracleJdbcConnection, array);
    }
}
