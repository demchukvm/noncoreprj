package com.tmx.as.blogic.balance;

import com.tmx.beng.csapi.java.base.BillingClientApi;
import com.tmx.as.blogic.BlogicException;
import com.tmx.as.entities.bill.balance.AbstractBalance;
import com.tmx.as.entities.bill.balance.TerminalBalance;
import com.tmx.as.entities.bill.restriction.AbstractRestriction;
import com.tmx.as.entities.bill.restriction.TerminalRestriction;
import com.tmx.as.entities.bill.terminal.Terminal;
import com.tmx.as.entities.general.user.User;
import com.tmx.as.exceptions.DatabaseException;
import com.tmx.as.base.criterion.CriterionWrapper;
import com.tmx.as.base.criterion.Restrictions;
import com.tmx.beng.base.BillException;
import com.tmx.util.InitException;

import java.util.List;


public class TerminalBalanceManager extends AbstractBalanceManager{

    public TerminalBalanceManager() {
        super();
    }

    public void deleteBalance(Long balanceId) throws DatabaseException{
        DELETE(TerminalBalance.class, balanceId);
    }

    public void deleteRestriction(Long restrictionId) throws DatabaseException {
        DELETE(TerminalRestriction.class, restrictionId);
    }

    public AbstractBalance getBalance(Long balanceId) throws DatabaseException {
        return (AbstractBalance)RETRIEVE(TerminalBalance.class, balanceId, new String[]{"terminal"});
    }

    public AbstractBalance getBalanceByCode(String balanceCode) throws DatabaseException {
        CriterionWrapper wrappers[] = new CriterionWrapper[] {
                                        Restrictions.eq("code", balanceCode)};
        return (TerminalBalance)RETRIEVE(TerminalBalance.class, null, wrappers, null);
    }

    public void saveBalance(AbstractBalance balance) throws DatabaseException{
        SAVE(balance);
    }

    public List getAllOwner(User loggedInUser) throws DatabaseException {
        return RETRIEVE_ALL(Terminal.class, null);
    }

    public List getBalancesByName(User loggedInUser) throws DatabaseException {
        return RETRIEVE_ALL(Terminal.class, null);
    }

    public AbstractRestriction getRestriction(Long restrictionId) throws DatabaseException {
        String[] attr = {"terminal", "functionInstance.functionType"};
        return (AbstractRestriction) RETRIEVE(TerminalRestriction.class, restrictionId, attr);
    }

    public void saveRestriction(AbstractRestriction restriction) throws DatabaseException {
        SAVE(restriction.getFunctionInstance(), new String[]{"functionType"} );
        SAVE(restriction);        
    }

    public void changeBalance(BalanceModificationConfig config) throws BlogicException, InitException, BillException, DatabaseException {
        config.validateConfiguration();
        TerminalBalance balance = (TerminalBalance) getBalance(config.getBalanceId());
        
        BillingClientApi.WebTerminalRefillMessage message = billingClient.createTerminalRefillMessage();
        message.setTerminalSN(balance.getTerminal().getSerialNumber());
        message.setTransDescription(config.getDescription());
        message.setUserId(config.getUserActorId().toString());
        message.setTransactionType(config.getType().getCode());
        message.setAmount(config.getAmount().toString());
        message.setPaymentReason(config.getPaymentReason());
        message.setBalanceCode(balance.getCode());
        message.send();
    }
    
}
