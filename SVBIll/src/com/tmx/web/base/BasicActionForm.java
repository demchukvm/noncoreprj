package com.tmx.web.base;

import com.tmx.web.controls.ActionExecStatus;
import com.tmx.web.controls.BasicControl;
import com.tmx.web.controls.ControlContainer;
import com.tmx.web.controls.WebEvent;
import org.apache.log4j.Logger;
import org.apache.struts.action.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;


public class BasicActionForm extends ActionForm {
    protected ActionErrorHandler errorsHandler = new ActionErrorHandler();
    /** Form name defined in the struts mapping xml. */
    private String formName = null;
    protected ActionMapping mapping = null;
    private Logger logger = Logger.getLogger("web.BasicActionForm");
    /** Flag is form was inited */
    private boolean isInited = false;
    /** Control contaimner.  */
    private ControlContainer ctrlContainer = null;
    /** TagRenderingHelper */
    private TagRenderingHelper tagRenderingHelper = null;
    /** To track action execuciton statuses */
    private ActionExecStatus actionExecStatus;


    /** Basic mandatory init  */
    private void basicInit(){
        initOwnControls();
        /** Run form successor's initialization */
        init();
        /** After form successor's init() is completed
         * create controlContainer and register all created controls */
        ctrlContainer = new ControlContainer(this);
        ctrlContainer.init(this, null);
        /** Initialize tagRenderingHelper
         * RM: Now we support only HTML helper.
         * RM: TODO remove that. do not use rendering logic in form bean */
        tagRenderingHelper = new TagHtmlRenderingHelper();
    }

    private void initOwnControls(){
        actionExecStatus = new ActionExecStatus();
    }

    /** Override it in successors to initialize form.
     *
     * TODO RM make it abstract
     *
     */
    protected void init(){
    }

    /** Returns form's owned controls. They were previously
     * registered by addControl method. */
    public List getOwnedControlsByClass(Class controlsClass){
        return ctrlContainer.getOwnedControlsByClass(controlsClass);
    }

    /** ActionErrorHandler is used to create an instance of ActionsError on demand */
    protected class ActionErrorHandler{
        private ActionErrors errors = null;

        public ActionErrors getErrors(){
            return errors;
        }

        public void addError(String property, ActionError error){
            if(errors == null)
                errors = new ActionErrors();
            errors.add(property, error);
        }

        public void addErrors(ActionErrors actionErrors){
            if(actionErrors == null)
                return;
            if(errors == null)
                errors = new ActionErrors();
            errors.add(actionErrors);
        }

        public void clearErrors(){
            errors = null;
        }
    }

    /** Getter for TagRenderingHelper */
    public TagRenderingHelper getTagRenderingHelper() {
        return tagRenderingHelper;
    }

    /**
     * To reset form from struts controller before auto-populate
     * */
    public void reset(ActionMapping mapping, HttpServletRequest request){
        //Assing params
        this.mapping = mapping;
        //RM: don't support this fild. this.session = request.getSession();
        /** Warning!!
         * We couldn't save and use Request here, because for ActionForms are held in session
         * this reset() method doesn't called on every request.
         * So, the solvation is: we hold session in every form  instance
         * (via BasicActionForm) and then we need Request object we get it from
         * corresponding UserContextForm. Request in that form are updated all time
         * by RequestUpdateFilter.
         * */
        if(!isInited){
            basicInit();
            isInited = true;
        }

        /** Reset assigned tag rendering helper  */
        getTagRenderingHelper().reset();
    }

    /** To reset form from particular action */
    public void reset(){
        basicInit();
    }

    public ActionExecStatus getActionExecStatus() {
        return actionExecStatus;
    }

    protected Roadmap getRoadmap() throws WebException{
        HttpServletRequest request = RequestEnvironment.currentReqContext().getRequest();
        if(request == null)
            throw new WebException("err.bacic_action_form.there_is_no_request_assigned_with_form", new String[]{this.getClass().getName()});

        Roadmap roadmap = (Roadmap)SessionEnvironment.getAttr(request.getSession(), "roadmap");
        if(roadmap == null){
            roadmap = new Roadmap();
            SessionEnvironment.setAttr(RequestEnvironment.currentReqContext().getRequest().getSession(true), "roadmap", roadmap);
        }
        return roadmap;
    }

    /** Returns the forward's URL from Roadmap for given page name. */
    public String getDefaultPageForward(String pageName){
        String pageForward = null;
        try{
            pageForward = getRoadmap().getDefaultPageForwardURL(pageName);
            if(pageForward == null)
                throw new WebException("err.bacic_action_form.default_page_forward_not_found", new String[]{pageName});
        }
        catch(WebException e){
            logger.error(e.toString());
            try{
                //Publish the Web exception
                RequestEnvironment.saveError(RequestEnvironment.currentReqContext().getRequest(), e);
                //Return the forward on error page
                pageForward = getRoadmap().getDefaultPageForwardURL("error");
            }
            catch(WebException e2){
                e.printStackTrace();
                //Publish the Web exception
                /** Return this string instead of error page URL to ease the error investigation*/
                pageForward = "error_page_is_not_defined";
            }
        }
        return pageForward;
    }

    /** Returns the action's URL from Roadmap for given page name. */
    public String getDefaultPageAction(String pageName){
        String pageAction = null;
        try{
            pageAction = getRoadmap().getDefaultPageActionURL(pageName);
            if(pageAction == null)
                throw new WebException("err.bacic_action_form.default_page_action_not_found", new String[]{pageName});
        }
        catch(WebException e){
            logger.error(e.toString());
            try{
                //Publish the Web exception
                RequestEnvironment.saveError(RequestEnvironment.currentReqContext().getRequest(), e);
                //Return the forward on error page
                UserInterfaceService uiService = (UserInterfaceService)ApplicationEnvironment.getService(UserInterfaceService.class);
                pageAction = uiService.getForwardURL("error");
            }
            catch(WebException e2){
                e.printStackTrace();
                /** Return this string instead of error page URL to ease the error investigation*/
                pageAction = "error_page_is_not_defined";
            }
        }
        return pageAction;
    }

    public void setOwnedControlsReadonly(boolean readonly){
        List list = ctrlContainer.getAllChildControls();
        Iterator iterator=list.iterator();
        while (iterator.hasNext()) {
            Object o =  iterator.next();
            if(o instanceof BasicControl){
                ((BasicControl)o).setReadonly(readonly);
            }
        }
    }

    protected Locale getLocale(){
        Locale locale = null;
        try{
            locale = SessionEnvironment.getLocale(RequestEnvironment.currentReqContext().getRequest().getSession());
        }
        catch(NullPointerException e){
            /** Possible if session for selected request will be invalidated */
            locale = Locale.getDefault();
        }
        return locale;
    }

    public String getFormName() {
        return formName;
    }

    /** Set form name in the BasicAction before particulare logic execution. */
    void setFormName(String formName) {
        this.formName = formName;
    }

    //-------------Events launchers
    public void fireOnBeforeAction(WebEvent e){
        for(Iterator iter = ctrlContainer.getAllChildControls().iterator(); iter.hasNext();)
            ((BasicControl)iter.next()).onBeforeAction(e);
    }

    public void fireOnAfterAction(WebEvent e){
        for(Iterator iter = ctrlContainer.getAllChildControls().iterator(); iter.hasNext();)
            ((BasicControl)iter.next()).onAfterAction(e);
    }

    //-------------Validation support

    /** Traverse and validate all enclosed controls. On error returns to
     * the page specified in 'input' parameter of corresponding action entry of struts mapping.
     * @throws ValidationException if any control's validation fails. */
    public void validate() throws ValidationException{
        validate0(null, ctrlContainer.getAllChildControls());
    }

    /** Traverse and validate all enclosed controls. On error returns to
     * the page identified by the 'inputPageForward'.
     * @throws ValidationException if any control's validation fails. */
    public void validate(ActionForward inputPageForward) throws ValidationException{
        validate0(inputPageForward, ctrlContainer.getAllChildControls());
    }

    /** Traverse and validate controls from the given list. On error returns to
     * the page specified in 'input' parameter of corresponding action entry of struts mapping.
     * @throws ValidationException if any control's validation fails. */
    public void validate(List controls) throws ValidationException{
        validate0(null, controls);
    }

    /** Traverse and validate controls from the given list. On error returns to
     * the page identified by the 'inputPageForward'. 
     * @throws ValidationException if any control's validation fails. */
    public void validate(ActionForward inputPageForward, List controls) throws ValidationException{
        validate0(inputPageForward, controls);
    }

    private void validate0(ActionForward inputPageForward, List controls) throws ValidationException{
        boolean validationResult = true;
        for(Iterator iter = controls.iterator(); iter.hasNext();)
            validationResult &= ((BasicControl)iter.next()).validate();
        if(!validationResult)
            throw new ValidationException("err.validation_error", inputPageForward);
    }
}
