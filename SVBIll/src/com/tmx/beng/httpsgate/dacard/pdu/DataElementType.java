package com.tmx.beng.httpsgate.dacard.pdu;

/**

 */
public enum DataElementType {
    ID_TYPE_EMPTY((byte) 0, new EmptyTypePAD()),
    ID_TYPE_NUMERIC((byte) 1, new NumericTypePAD()),
    ID_TYPE_STRING((byte) 2, new StringTypePAD()),
    ID_TYPE_BLOB((byte) 3, new BlobTypePAD()),
    ID_TYPE_ENCODEDBLOB((byte) 4, new StringTypePAD());

    private byte code;
    private TypePAD pad;

    DataElementType(byte code, TypePAD pad) {
        this.code = code;
        this.pad = pad;
    }

    byte getCode(){
        return code;
    }

    TypePAD getPad(){
        return pad;
    }

    /** Get DataElementType by code. Returns null if found nothing.
     * @param code byte code of DataElementType
     * @return corresponded DataElementType. */
    static DataElementType valueOf(byte code){
        for(DataElementType det : DataElementType.values()){
            if(det.code == code)
                return det;
        }
        return null;
    }

}