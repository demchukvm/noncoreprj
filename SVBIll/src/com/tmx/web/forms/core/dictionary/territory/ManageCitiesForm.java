package com.tmx.web.forms.core.dictionary.territory;

import com.tmx.web.base.BasicActionForm;
import com.tmx.web.controls.*;
import com.tmx.web.controls.table.ControlManagerTable;

public class ManageCitiesForm extends BasicActionForm {
    private ControlManagerTable table;
    private Button onCreateButton;
    private Button onEditButton;
    private Button onDeleteButton;

    protected void init() {
        //setup buttons
        String action="/core/dictionary/territory/infoCity";
        onCreateButton = new Button();
        onCreateButton.setLabelKey("ctrl.button.label.create");
        onCreateButton.setAction(action);
        onCreateButton.setCommand("preloadForCreate");

        onEditButton = new Button();
        onEditButton.setLabelKey("ctrl.button.label.edit");
        onEditButton.setAction(action);
        onEditButton.setCommand("preloadForUpdate");

        onDeleteButton = new Button();
        onDeleteButton.setLabelKey("ctrl.button.label.delete");
        onDeleteButton.setAction(action);
        onDeleteButton.setCommand("preloadForDelete");

        initTable();
    }

    private void initTable() {
        table = new ControlManagerTable("query(query_type=retrieve_all," +
                "entity_class=com.tmx.as.entities.network.territory.City," +
                "table_attr=region," +
                "order(name=id,type=asc)," +
                "page_size=15" +
                ")");
        table.setRefreshActionName("/core/dictionary/territory/manageCities");

        table.addParameterToFilter("cityId", "value", new EditBox());
        table.addParameterToFilter("shortName", "value", new EditBox());
        table.addParameterToFilter("name", "value", new EditBox());
        
        table.addDependedControl(onDeleteButton).addDependedControl(onEditButton);
    }

    //--------Properties

    public ControlManagerTable getTable() {
        return table;
    }

    public void setTable(ControlManagerTable table) {
        this.table = table;
    }

    public Button getOnCreateButton() {
        return onCreateButton;
    }

    public void setOnCreateButton(Button onCreateButton) {
        this.onCreateButton = onCreateButton;
    }

    public Button getOnEditButton() {
        return onEditButton;
    }

    public void setOnEditButton(Button onEditButton) {
        this.onEditButton = onEditButton;
    }

    public Button getOnDeleteButton() {
        return onDeleteButton;
    }

    public void setOnDeleteButton(Button onDeleteButton) {
        this.onDeleteButton = onDeleteButton;
    }
}
