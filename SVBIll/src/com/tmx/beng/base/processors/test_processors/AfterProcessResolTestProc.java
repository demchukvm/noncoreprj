package com.tmx.beng.base.processors.test_processors;

import com.tmx.beng.base.processors.TaskProcessor;
import com.tmx.beng.base.*;
import com.tmx.beng.base.cache.DBCache;


public class AfterProcessResolTestProc extends TaskProcessor {

    protected Task process0(Task task, DBCache dbCache, BillingMessageWritable msg) throws BillException {

        BillingMessageWritable respMsg = new BillingMessageImpl();
        
        respMsg.setAttributeString(BillingMessage.PROCESSING_CODE, msg.getAttributeString(BillingMessage.PROCESSING_CODE));
        respMsg.setAttributeString(BillingMessage.TERMINAL_SN, msg.getAttributeString(BillingMessage.TERMINAL_SN));
        respMsg.setAttributeString(BillingMessage.SELLER_CODE, msg.getAttributeString(BillingMessage.SELLER_CODE));

        CallBack callBack = task.getCallBack();
        if(callBack != null){
            callBack.sendBilledMessage(respMsg);
        }

        return task;
    }
    
}
