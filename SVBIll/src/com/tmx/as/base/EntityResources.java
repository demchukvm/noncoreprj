package com.tmx.as.base;

import com.tmx.util.PropertiesLocalized;
import com.tmx.util.InitException;
import com.tmx.util.Configuration;
import com.tmx.util.StringUtil;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.io.InputStream;
import java.io.IOException;

import org.hibernate.EntityMode;
import org.hibernate.metadata.ClassMetadata;
import org.apache.log4j.Logger;


public class EntityResources implements Reconfigurable {
    private Logger logger = Logger.getLogger("core.EntityResources");
    private String labelsFileName = "labels";
    private Map entites = null;
    private Map supportedLocales = null;
    private final String PROPERTY_DELIMITER = ".";
    private final String ENTITY_PATH_DELIMITER = ".";

    public String getEntityLabel(Class entityClass, String attrName, Locale locale) {
        Map localizedEntities = (Map) entites.get(entityClass);
        if (localizedEntities == null)
            return "";
        Properties labelProps = (Properties) localizedEntities.get(locale);
        if (labelProps == null) {
            //try without country
            locale = new Locale(locale.getLanguage());
            labelProps = (Properties) localizedEntities.get(locale);
        }
        if (labelProps == null) {
            labelProps = (Properties) localizedEntities.get(getSupportedLocaleByLanguage(locale));
        }
        if (labelProps == null) {
            labelProps = (Properties) localizedEntities.get(Locale.getDefault());
        }
        if (labelProps == null)
            return "";
        String label = labelProps.getProperty(attrName);
        return (label == null) ? /** if not found: */("???"+entityClass.getName()+"."+attrName+"???") : label;
    }

    /**
     * reload entities's labels from properties files into memory
     */
    public void reload() throws InitException {
        logger.info("Entities resources loading...");

        supportedLocales = getSupportedLocales();
        Map entites = new HashMap();

        LocalizedPropsMap unitedLocalizedProps = new LocalizedPropsMap();//key=Locale; value=united props

        Map entitiesMetadata = HibernateSessionFactoryProvider.getAllClassMetadata();
        Iterator iter = entitiesMetadata.keySet().iterator();
        while (iter.hasNext()) {
            String entityName = (String) iter.next();
            ClassMetadata entityMetadata = (ClassMetadata) entitiesMetadata.get(entityName);
            Class entityClass = entityMetadata.getMappedClass(EntityMode.POJO);

            //create localizedEntities Map
            Map localizedEntities = new HashMap();
            entites.put(entityClass, localizedEntities);

            Iterator iterLocales = supportedLocales.values().iterator();
            while (iterLocales.hasNext()) {
                Locale currentLocale = (Locale) iterLocales.next();
                String fileName = labelsFileName + "_" + currentLocale.toString() + ".properties";
                InputStream is = entityClass.getResourceAsStream(fileName);
                if (is != null) {
                    Properties props = loadLocalizedProperties(is, fileName);
                    logger.debug("for entity: ['" + entityName + "'] locale: ['" + currentLocale + "'] the labels have been loaded ['" + fileName + "']");
                    String entityPath = entityClass.getPackage().getName();
                    String localEntityName = entityName.substring(entityPath.length() + ".".length());

                    unitedLocalizedProps.addLocalizedProps(currentLocale, replaceLocalNames(localEntityName, entityPath, props));
                }
            }
        }

        //substitute variables
        try {
            for (Iterator localeIter = supportedLocales.values().iterator(); localeIter.hasNext();) {
                Locale currentLocale = (Locale) localeIter.next();
                Properties localizedProps = (Properties) unitedLocalizedProps.get(currentLocale);
                unitedLocalizedProps.replaceAll(currentLocale, substituteVariables(localizedProps));
            }
        }
        catch (Exception e) {
            throw new InitException("err.failed_to_substitute_entity_props", e);
        }

        //assign subsituted props with entities
        for (Iterator entitiesIter = entites.keySet().iterator(); entitiesIter.hasNext();) {
            Class entityClass = (Class)entitiesIter.next();
            Map localizedEntities = (Map)entites.get(entityClass);
            for(Iterator iterLocales = supportedLocales.values().iterator(); iterLocales.hasNext();){
                Locale currentLocale = (Locale)iterLocales.next();
                localizedEntities.put(currentLocale, collectEntityProps(entityClass, (Properties)unitedLocalizedProps.get(currentLocale)));
            }
        }
        this.entites = entites;
        logger.info("Resources have been loaded");
    }

    /**
     * return empty set if any locales found
     */
    public Map getSupportedLocales() {
        Map supportedLocalesMap = new HashMap();

        String[] supportedLocales = Configuration.getInstance().getPropertyAsArray("supported_locales");
        supportedLocales = (supportedLocales == null) ? new String[0] : supportedLocales;

        for (int i = 0; i < supportedLocales.length; i++) {
            Locale locale = StringUtil.createLocale(supportedLocales[i]);
            supportedLocalesMap.put(locale.getLanguage(), locale);
        }
        return supportedLocalesMap;
    }

    private Properties replaceLocalNames(String entityName, String entityPath, Properties props) {
        final String TMP_SPLIT_DELIMITER = "@___!!!DELIMITER!!!___@";
        final Pattern pattern = Pattern.compile(".*\\$\\{(.*)\\..*\\}.*");

        Properties newProperties = new Properties();
        for (Enumeration e = props.keys(); e.hasMoreElements();) {
            String key = (String) e.nextElement();


            if (key.startsWith(entityName + PROPERTY_DELIMITER) ||
                    key.startsWith(entityPath + ENTITY_PATH_DELIMITER + entityName + PROPERTY_DELIMITER)) {

                String value = props.getProperty(key);
                //preprocess value. change local referenced entity name to full one:
                // myEntity1.attr1={myEntity2.attr1}
                //
                // changed to:
                //
                // myEntity1.attr1=${myEntity1.full.package.path.myEntity2.attr1}
                value = value.replaceAll("\\$\\{", TMP_SPLIT_DELIMITER+"\\$\\{");
                String[] valueLexems = value.split(TMP_SPLIT_DELIMITER);

                value = "";//clear before accumulation
                for (int i = 0; i < valueLexems.length; i++) {
                    Matcher matcher = pattern.matcher(valueLexems[i]);
                    if (matcher.matches()) {
                        String extractedEntityName = matcher.group(1);
                        if (extractedEntityName != null &&
                                extractedEntityName.trim().length() > 0 &&
                                extractedEntityName.indexOf(ENTITY_PATH_DELIMITER) == -1) {

                            //found local entity name -> replace it with full name
                            valueLexems[i] = "${" + entityPath + ENTITY_PATH_DELIMITER + extractedEntityName + valueLexems[i].substring("${".length() + extractedEntityName.length());
                        }
                    }
                    value += valueLexems[i];
                }

                //copy property to new props,
                if (key.startsWith(entityName + PROPERTY_DELIMITER))
                    //adding entity path prefix
                    newProperties.put(entityPath + ENTITY_PATH_DELIMITER + entityName + PROPERTY_DELIMITER + key.substring(entityName.length() + PROPERTY_DELIMITER.length()), value);
                else
                    //as is
                    newProperties.put(key, value);

            }
        }
        return newProperties;
    }

    private Properties collectEntityProps(Class entityClass, Properties unitedLocalizedProps){
        Properties entityProps = new Properties();
        String entityPath = entityClass.getPackage().getName();
        String entityFullName = entityClass.getName();
        for(Enumeration propsEnum = unitedLocalizedProps.keys(); propsEnum.hasMoreElements();){
            String propKey = (String)propsEnum.nextElement();
            if(propKey.startsWith(entityFullName)){
                //entityProps.put(propKey.substring(entityPath.length()+".".length()), unitedLocalizedProps.getProperty(propKey));
                //Strip property key: entity.full.name.attrName -> attrName
                entityProps.put(propKey.substring(entityFullName.length()+".".length()), unitedLocalizedProps.getProperty(propKey));
            }
        }
        return entityProps;
    }

    /** Look for substitution variables and evaluate them.
     * substitution variabl looks like ${var_name}. Its value should be defined in this properties:
     *
     * var_A = value_of_A
     * var_B = ${var_A}/some_value
     *
     *  */
    private Properties substituteVariables(Properties props) throws Exception{
        java.util.Enumeration keys = props.keys();
        Properties evaluatedProps = new Properties();
        while(keys.hasMoreElements()){
            String currentKey = (String)keys.nextElement();
            substituteVariable(currentKey, new Stack(), props, evaluatedProps);
        }
        return evaluatedProps;
    }


    /** Recursively substitute variables from <code>props</code> into <code>evaluatedVars</code>.*/
    private String substituteVariable(String varName, Stack parentVars, Properties props, Properties evaluatedVars) throws Exception {
        String requsetedVarValue = evaluatedVars.getProperty(varName);
        if(requsetedVarValue == null){
            requsetedVarValue = props.getProperty(varName);
            if(requsetedVarValue == null)
                //return null;
                return "???"+varName+"???";//substituted variable not found
        }
        else
            return requsetedVarValue;

        parentVars.push(varName);
        final String TMP_SPLIT_DELIMITER = "@___!!!DELIMITER!!!___@";
        requsetedVarValue = requsetedVarValue.replaceAll("\\$\\{", TMP_SPLIT_DELIMITER+"\\$\\{");
        String[] lexems = requsetedVarValue.split(TMP_SPLIT_DELIMITER);
        requsetedVarValue = "";//reset
        Pattern pattern = Pattern.compile("(.*)\\$\\{(.*)\\}(.*)");//group(1) - start of the string, group(2) - var_name, group(3) - rest of string after ${var_name}
        for(int i = 0; i < lexems.length; i++){
            //substitute from processed props
            if(lexems[i].length() > 0){
                Matcher matcher = pattern.matcher(lexems[i]);
                if (matcher.matches()) {
                    String foundVarName = matcher.group(2);
                    if(parentVars.contains(foundVarName))
                        throw new Exception("Recursive substitution for variable '"+foundVarName+"'");

                    lexems[i] = matcher.group(1) + substituteVariable(foundVarName, parentVars, props, evaluatedVars) + matcher.group(3);
                }
            }

            //accumulate
            requsetedVarValue += lexems[i];
        }
        //copy from 'props' into the 'evaluated'
        evaluatedVars.put(varName, requsetedVarValue);
        parentVars.pop();
        return requsetedVarValue;
    }

    /**
     * Look between supported locales for locale with the same language.
     * Used when given locale is not in the list of supported locales.
     * Then we look for analogous one by language and ignore the country.
     */
    private Locale getSupportedLocaleByLanguage(Locale givenLocale) {
        return (givenLocale == null || givenLocale.getLanguage() == null) ? null : (Locale) supportedLocales.get(givenLocale.getLanguage());
/*            Iterator iter = supportedLocales.iterator();
            while(iter.hasNext()){
                Locale currentSupportedLocale = (Locale)iter.next();
                if(givenLocale != null &&
                   givenLocale.getLanguage() != null &&
                   givenLocale.getLanguage().equals(currentSupportedLocale.getLanguage()))
                return currentSupportedLocale;
            }
            return null;*/
    }

    /** Load localized properties in specified encoding and wrap them into pure JSDK 'Properties' class */
    private Properties loadLocalizedProperties(InputStream is, String fileName) throws InitException {
        Properties loadedProps = new Properties();
        PropertiesLocalized propsLocalized = new PropertiesLocalized();
        try {
            propsLocalized.load(is, Configuration.getInstance().getProperty("resources_encoding"));
            is.close();
        }
        catch (IOException e) {
            throw new InitException("err.as.failed_to_load_resource_file", new String[]{fileName});
        }
        loadedProps.putAll(propsLocalized);
        return loadedProps;
    }


    private class LocalizedPropsMap extends HashMap implements Map{

        public void addLocalizedProps(Locale locale, Map props){
            if(get(locale) == null)
                put(locale, new Properties());

            Properties storedProps = (Properties)get(locale);
            storedProps.putAll(props);
        }

        public void replaceAll(Locale locale, Map props){
            put(locale, props);
        }
    }
}