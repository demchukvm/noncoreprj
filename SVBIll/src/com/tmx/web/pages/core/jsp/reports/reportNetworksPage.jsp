<%@ page language="java" pageEncoding="UTF-8" contentType="text/html; charset=utf-8" isELIgnored="false" %>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-nested" prefix="nested" %>
<%@ taglib uri="http://tmx.com/taglib/tmx-controls" prefix="tmx_ctrl" %>
<%@ taglib uri="http://tmx.com/taglib/tmx-logic" prefix="tmx_logic" %>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html" %>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean" %>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>

<%@ page import="com.tmx.web.forms.core.reports.ReportNetworksForm" %>

<html:form action="/core/report/reportNetworks">
    <table  border="0" width="100%">
        <tr>
            <td class="tabForm" align="left">
                <table>
                    <tr>
                        <td class="dataLabel">  <!-- Период с -->          <!-- 1 -->
                            <bean:message bundle="core.labels" key="core.report.reportPayment.period"/> <!-- Период -->
                            &nbsp;
                            <bean:message bundle="core.labels" key="core.monitor.timeFrom"/> <!-- с -->
                            :
                        </td>
                        <td class="dataField">    <!-- 2 -->
                            <tmx_ctrl:timeSelector property="from"/>  <!-- calendar -->
                        </td>

                        <td class="dataLabel">    <!-- 3 -->
                            <bean:message bundle="core.labels" key="core.monitor.timeTo"/>  <!-- по -->
                            :
                        </td>
                        <td class="dataField">    <!-- 4 -->
                            <tmx_ctrl:timeSelector property="to"/>  <!-- calendar -->
                        </td>

                        <td class="dataLabel">           <!-- 5 -->
                            <tmx_ctrl:writeEntityLabel attrName="description"
                                entityClassName="com.tmx.as.entities.bill.seller.Seller"/>:   <!-- Описание -->
                        </td>


                        <td class="dataField">    <!-- 6 -->
                            <%-- <tmx_ctrl:checkBox property="checkBoxDescription"/> --%>  <!-- checkBox -->
                            <%-- <input type="checkbox" name="checkBoxDescription" /> --%>
                            <% 
                                boolean flag = ((ReportNetworksForm) pageContext.getSession().getAttribute("core.report.reportNetworksForm")).is_checkBoxDescription();
                                if(flag == true)
                                {
                            %>
                                <input type="checkbox" name="checkBoxDescription" checked="checked"/>
                            <%
                                }
                                else
                                {
                            %>
                                <input type="checkbox" name="checkBoxDescription" />
                            <%
                                }
                            %>
                        </td>

                        <%--<td class="dataLabel">--%>
                            <%--<tmx_ctrl:writeEntityLabel attrName="gropuByTerminals"--%>
                                <%--entityClassName="com.tmx.as.entities.bill.seller.Seller"/>:--%>
                        <td class="dataLabel">      <!-- 7 -->
                            <tmx_ctrl:writeEntityLabel attrName="serialNumbers"
                                entityClassName="com.tmx.as.entities.bill.terminal.Terminal"/>:

                        </td>
                        <td class="dataField">          <!-- 8 -->
                            <tmx_ctrl:editBox property="editBoxTerminals"/>

                        </td>


                    </tr>
                    
                    <tr>
                        <td class="dataLabel" rowspan="3">  <!-- 9 -->
                            Диллеры
                        </td>
                        <td class="dataField" rowspan="3">  <!-- 10 -->
                            <tmx_ctrl:multipleSelectBox property="multipleSelectBoxSellerBalances"/>
                        </td>

                        <td class="dataLabel" rowspan="3">    <!-- 11 -->
                            Поддиллеры
                        </td>
                        <td class="dataField" rowspan="3">    <!-- 12 -->
                            <tmx_ctrl:multipleSelectBox property="multipleSelectBoxSubSellerBalances"/>
                        </td>

                        <td class="dataLabel">     <!-- 13 -->
                            По терминалам:

                        </td>
                        <td class="dataField">      <!-- 14 -->
                            <%-- <tmx_ctrl:checkBox property="checkBoxGroupByTerminals"/> --%>
                            <%
                                flag = ((ReportNetworksForm) pageContext.getSession().getAttribute("core.report.reportNetworksForm")).is_checkBoxGroupByTerminals();
                                if(flag == true)
                                {
                            %>
                                <input type="checkbox" name="checkBoxGroupByTerminals" checked="checked"/>
                            <%
                                }
                                else
                                {
                            %>
                                <input type="checkbox" name="checkBoxGroupByTerminals" />
                            <%
                                }
                            %>
                        </td>

                        <%--<td class="dataLabel" rowspan="3">        <!-- 15 -->
                             Тип оборудования:
                        </td>
                        <td class="dataField" rowspan="3">      <!-- 16 -->

                        </td>--%>

                        <td class="dataLabel" rowspan="3">      <!-- 15 -->
                        <tmx_ctrl:writeEntityLabel
                                attrName="{@name}"
                                entityClassName="com.tmx.as.entities.bill.terminal.EquipmentType"/>
                        </td>
                        <td class="dataField" rowspan="3">        <!-- 16 -->
                            <tmx_ctrl:selectBox property="scrollBoxEquipmentType"/>
                        </td>
                    </tr>

                    <tr>
                        <td class="dataField">
                            <tmx_ctrl:button property="buttonSubmit"/>
                        </td>     <!-- 17 -->
                        <td class="dataField">
                            <tmx_ctrl:button property="buttonUploadReport"/>
                        </td>     <!-- 18 -->
                    </tr>

                </table>
            </td>
        </tr>
        <tr>
            <td>
                <tmx_ctrl:grid property="grid"/>
            </td>
        </tr>
    </table>

</html:form>