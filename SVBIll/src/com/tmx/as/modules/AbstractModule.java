package com.tmx.as.modules;

import com.tmx.as.base.EmbeddedModule;

import org.apache.log4j.Logger;

/**
 * Created by IntelliJ IDEA.
 * User: shake
 * Date: 25.06.2007
 * Time: 15:56:19
 */
public abstract class AbstractModule implements EmbeddedModule {
    protected Logger logger = Logger.getLogger("core.Module");
}
