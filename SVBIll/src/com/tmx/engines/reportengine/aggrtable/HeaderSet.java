package com.tmx.engines.reportengine.aggrtable;

import com.tmx.engines.reportengine.base.ReportException;
import com.tmx.util.i18n.MessageResources;

import java.util.*;
import java.math.BigDecimal;

/**
 */
public class HeaderSet {
    /** List of header's rows. Each row is List of Headers */
    private List rows = null;
    /** List of header's rows names. */
    private List rowsNames = null;
    /** header set type used then table's datas are aggregated */
    private AggregatedTable.MappingRule.Type headerSetType = null;
    private final String HEADER_PREFIX = "header.";
    private AggregatedTable parentTable = null;

    /** gets rows count in this header set*/
    public int getRowsCount(){
        return (rows == null) ? 0 : rows.size();
    }

    /** gets headers of specifies row */
    public List getRowHeaders(int rowNumber){
        return (rows == null) ? new ArrayList() : (List)rows.get(rowNumber);
    }

    /** returns headerset row label by index. it was set via mapping rules */
    public String getRowLabel(int rowIndex, Locale locale){
        String key = (String)rowsNames.get(rowIndex);
        return (key != null) ? MessageResources.getInstance().getOwnedLocalizedMessage(this.getClass(), AggregatedTable.RESOURCE_BUNDLE, locale, HEADER_PREFIX + key, null) : null;
    }

    /** returns headerset row name by index. it was set via mapping rules */
    public String getRowName(int rowIndex){
        return (String)rowsNames.get(rowIndex);
    }

    //----------package local methods

    /** package local constructor. Initialize internal structure
     * according to the given mappingRules
     * @param mappingRules     Map of AggregatedTable.MappingRule - table mapping rules
     * @param type             descibes how current HeaderSet used in aggregated table
     * */
    HeaderSet(Map mappingRules, AggregatedTable.MappingRule.Type type, AggregatedTable parentTable){
        headerSetType = type;
        rows = new SafeArrayList();
        rowsNames = new SafeArrayList();
        this.parentTable = parentTable;
        for(Iterator iter = mappingRules.values().iterator(); iter.hasNext();){
            AggregatedTable.MappingRule rule = (AggregatedTable.MappingRule)iter.next();
            if(headerSetType.equals(rule.getType())){
                ((SafeArrayList)rows).safeSet(rule.getDestIdx().intValue(), new SafeArrayList());
                ((SafeArrayList)rowsNames).safeSet(rule.getDestIdx().intValue(), rule.getName());
            }
        }
    }


    /** Pushs header name into header. used AggregatoinTable.reload().
     *
     * Important: to understand usage of incoming params
     * both column and row headers should be considered like column headers
     *
     * Does not perform any aggregation
     * @param rule mapping rule for this header set
     * @param colIdx column idx in this header set
     * @param srcRow row from source table where header label and label order idx are hold.
     *  */
    void put(AggregatedTable.MappingRule rule, int colIdx, Object[] srcRow) throws com.tmx.engines.reportengine.base.ReportException {
        if(rows == null)
            throw new com.tmx.engines.reportengine.base.ReportException("err.report_engine.aggr_table.header_set_is_not_initialized");

        if(rule.getDestIdx().intValue() >= rows.size())
            throw new com.tmx.engines.reportengine.base.ReportException("err.report_engine.aggr_table.row_index_out_of_bounds", new String[]{String.valueOf(rule.getDestIdx().intValue()), String.valueOf(rows.size())});

        Header newHeader = new Header();
        //set mandatory name
        String label = (srcRow[rule.getSrcLabelIdx().intValue()] != null) ? srcRow[rule.getSrcLabelIdx().intValue()].toString() : "";
        newHeader.setLabel(label);

        //set optional label ordering index
        if(rule.getSrcLabelOrderIdx() != null)
            newHeader.setLabelOrder(srcRow[rule.getSrcLabelOrderIdx().intValue()]);
        else
            newHeader.setLabelOrder(srcRow[rule.getSrcLabelIdx().intValue()]); //by default use label in its original type to order headers

        //newHeader.setSpan(1);     //alredy 1 straight after creation
        SafeArrayList row = (SafeArrayList)rows.get(rule.getDestIdx().intValue());
        row.safeSet(colIdx, newHeader);
    }


    /** Important: to understand how this method works row header set
     * should be considered like column header set.
     *
     * Aggregate neighbour columns if all they headers are equal row-by-row.
     * Table size (both for headerset row and data) is decremented after
     * each aggregation.
     *
     * @param data data to be aggregated with this header set.
     * */
    void aggregate(Data data) throws ReportException {
        //inital columns count
        int colCount = ((List) rows.get(0) == null) ? 0 : ((List) rows.get(0)).size();
        int currColIdx = 0;

        while (currColIdx < colCount - 1)
        startAgain:{//do not process last column. it has not right neighbour
            //currColIdx is index of current etalon column
            for (int colIdx = currColIdx+1; colIdx < colCount; colIdx++)
            exit2ColumnsAggrAttempt:{ //do not process last column. it has not right neighbour
                for (int rowIdx = 0; rowIdx < rows.size(); rowIdx++) {
                    Header etalonHeader = (Header) ((List) rows.get(rowIdx)).get(currColIdx);
                    Header candidateHeader = (Header) ((List) rows.get(rowIdx)).get(colIdx);
                    if (!etalonHeader.equals(candidateHeader)) {
                        //current column pair could not be aggregated
                        if (colIdx == colCount - 1){
                            //if last column processed then start with next etalon header column
                            currColIdx++;
                            break startAgain;
                        }
                        else
                            break exit2ColumnsAggrAttempt;
                    }
                }

                //aggregate
                //1) aggregate headers
                removeAggregatedHeaders(colIdx);
                //2) aggregate data
                if (AggregatedTable.MappingRule.Type.COLUMN_HEADER.equals(headerSetType))
                    data.aggregateColumns(currColIdx, colIdx);
                else if (AggregatedTable.MappingRule.Type.ROW_HEADER.equals(headerSetType))
                    data.aggregateRows(currColIdx, colIdx);
                else
                    throw new ReportException("err.report_engine.aggr_table.unknown_column_type", new String[]{(headerSetType != null) ? headerSetType.toString() : null});

                //decrements table size
                colCount--;
                colIdx--;//to check column with this idx again. After aggregation columns were shifted to the left
            }
        }
    }


     /** Aggregate neighbour headers in row if theay are equals. Do not
     * affect on headers in other rows and data. Increment span value
     * of header after each aggregation. Table size couldn't be changed.
      *
      * Call after sorting by headers.
      * */
    void aggregateHeaders(){
         int maxColCount = ((List)rows.get(0)).size();//initially all headers have the same col count
         int[] currHeaderColIdx = new int[rows.size()];//to hold current header col idx
         Arrays.fill(currHeaderColIdx, 0);//init

         for(int colIdx=0; colIdx<maxColCount-1; colIdx++){//maxColCount-1 - this is the max aggr attempts in any header row
             boolean isAggrPossible = true;//assumption
             for(int rowIdx=0; rowIdx<rows.size(); rowIdx++){
                 if(isAggrPossible){
                    List currRow = (List)rows.get(rowIdx);
                    Header leftHeader = (Header)currRow.get(currHeaderColIdx[rowIdx]);
                    Header rightHeader = (Header)currRow.get(currHeaderColIdx[rowIdx]+1);
                    if(leftHeader.equals(rightHeader)){
                         currRow.remove(currHeaderColIdx[rowIdx]+1);//remove right column
                         leftHeader.setSpan(leftHeader.getSpan()+1);//increment span
                     }
                     else{
                         currHeaderColIdx[rowIdx]++;//shift col idx to next right header
                         isAggrPossible = false;
                     }
                 }
                 else{
                    currHeaderColIdx[rowIdx]++;//shift col idx to next right header
                 }
             }
         }
    }

    /** @return array with columns before sorting  */
    Integer[] sortHeaderSet(){
        //get ordering
        String orderType = null;
        if(parentTable != null){
            if(headerSetType.equals(AggregatedTable.MappingRule.Type.COLUMN_HEADER))
                orderType = parentTable.getLookAndFeel().getColumnHeader().getOrdering() != null ? parentTable.getLookAndFeel().getColumnHeader().getOrdering().getType() : null;
            else if(headerSetType.equals(AggregatedTable.MappingRule.Type.ROW_HEADER))
                orderType = parentTable.getLookAndFeel().getRowHeader().getOrdering() != null ? parentTable.getLookAndFeel().getRowHeader().getOrdering().getType() : null;
        }

        TreeMap sortedColHeaders = new TreeMap(new HeaderComparator(orderType));
        int colCount = ((List)rows.get(0)).size();//all headers rows now have the same count of cols
        for(int colIdx = 0; colIdx < colCount; colIdx++){
            List headersColumnKey = new ArrayList();
            for(int rowIdx = 0; rowIdx < rows.size(); rowIdx++){
                headersColumnKey.add(((List)rows.get(rowIdx)).get(colIdx));
            }
            //put headers key into tree map and current col idx as data
            sortedColHeaders.put(headersColumnKey, new Integer(colIdx));
        }

        //set sorted headers
        int colIdx = 0;
        for(Iterator iter = sortedColHeaders.keySet().iterator(); iter.hasNext();){
            List headersColumnKey = (List)iter.next();
            for(int rowIdx = 0; rowIdx < headersColumnKey.size(); rowIdx++){
                ((List)rows.get(rowIdx)).set(colIdx, headersColumnKey.get(rowIdx));
            }
            colIdx++;
        }

        //return idx before headers sorting to sort data
        Object[] tmpSortedColIdxInt = sortedColHeaders.values().toArray();
        Integer[] sortedColIdxInt = new Integer[tmpSortedColIdxInt.length];
        System.arraycopy(tmpSortedColIdxInt, 0 , sortedColIdxInt, 0, tmpSortedColIdxInt.length);
        return sortedColIdxInt;
    }

    /** Aggregate headers */
    private void removeAggregatedHeaders(int colIdx){
        for(Iterator iter = rows.iterator(); iter.hasNext();){
            List headerRow = (List)iter.next();
            headerRow.remove(colIdx);
        }
    }


    private class HeaderComparator implements Comparator {
        //default column order in the header
        private String orderType = "asc";

        public HeaderComparator(String orderType){
            if(orderType != null && ("asc".equals(orderType.toLowerCase()) || "desc".equals(orderType.toLowerCase())))
                this.orderType = orderType.toLowerCase();
        }

        public int compare(Object o1, Object o2) {
            if (!(o1 instanceof List) || !(o2 instanceof List)) {
                throw new IllegalArgumentException("Comparated objects should have List type");
            }
            List headerColumn1 = (List) o1;
            List headerColumn2 = (List) o2;

            for (int rowIdx = 0; rowIdx < headerColumn1.size(); rowIdx++) {
                Object labelOrder1 = ((Header) headerColumn1.get(rowIdx)).getLabelOrder();
                Object labelOrder2 = ((Header) headerColumn2.get(rowIdx)).getLabelOrder();

                //to be safe with null values
                if(labelOrder1 == null && labelOrder2 == null)
                    continue;//go to compare sub headers
                else if(labelOrder1 != null && labelOrder2 == null)
                    return compare0(1);
                else if(labelOrder1 == null && labelOrder2 != null)
                    return compare0(-1);

                if (!labelOrder1.getClass().equals(labelOrder2.getClass())) {
                    throw new IllegalArgumentException("Comparated headers orders should the same type 1:'" + labelOrder1.getClass().getName() + "' 2:'" + labelOrder2.getClass().getName() + "'");
                }

                //compare order objects
                int compVal = 0;
                if (labelOrder1 instanceof Long) {
                    compVal = ((Long) labelOrder1).compareTo((Long) labelOrder2);
                }
                else if (labelOrder1 instanceof Integer) {
                    compVal = ((Integer) labelOrder1).compareTo((Integer) labelOrder2);
                }
                else if (labelOrder1 instanceof String) {
                    compVal = ((String) labelOrder1).compareTo((String) labelOrder2);
                }
                else if (labelOrder1 instanceof Date) {
                    compVal = ((Date) labelOrder1).compareTo((Date) labelOrder2);
                }
                else if(labelOrder1 instanceof BigDecimal){
                    compVal = ((BigDecimal) labelOrder1).compareTo((BigDecimal) labelOrder2);
                }
                else {
                    //by default compare orders as strings
                    compVal = labelOrder1.toString().compareTo(labelOrder2.toString());
                }

                if (compVal != 0)
                    return compare0(compVal);
                //look into next row
            }
            return 0;//if all headers in both columns are equal row-by-row
        }

        /** @param  nativeComparisonResult is in the set {-1; 0; 1} */
        private int compare0(int nativeComparisonResult){
            return "desc".equals(orderType) ? -nativeComparisonResult : nativeComparisonResult;
        }
    }

}
