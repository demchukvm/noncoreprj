package com.tmx.beng.iso8583Gate.pdu;

import com.tmx.beng.iso8583Gate.pdu.length_types.LengthType;
import com.tmx.beng.iso8583Gate.pdu.length_types.LengthTypeFactory;
import com.tmx.beng.iso8583Gate.pdu.types.Type;
import com.tmx.beng.iso8583Gate.pdu.types.TypeFactory;

import java.util.Map;
import java.util.HashMap;
import java.io.*;

/**
 * Run-time ISO8583 message data element 
 */
public class DataElementFormat extends BasicDataElementFormat{
    /** bit number of this DataElementFormat in the message bitmap */
    private Type type;
    private LengthType lengthType;
    private int length;
    private int maxLength;
    private String extendedFormat;


    DataElementFormat(int bitmapNumber, String name, String type, String lengthType, int length, int maxLength, String extendedFormat){
        this(   bitmapNumber,
                name,
                TypeFactory.getInstance(type),
                LengthTypeFactory.getInstance(lengthType),
                length,
                maxLength,
                extendedFormat);
    }

    /** package private constructor */
    DataElementFormat(int bitmapNumber, String name, Type type, LengthType lengthType, int length, int maxLength, String extendedFormat){
        this.bitmapNumber = bitmapNumber;
        this.name = name;
        this.type = type;
        this.lengthType = lengthType;
        this.length = length;
        this.maxLength = maxLength;
        this.extendedFormat = extendedFormat;
    }

    //--------properties
    public Type getType() {
        return type;
    }

    public LengthType getLengthType() {
        return lengthType;
    }

    public int getLength() {
        return length;
    }

    public int getMaxLength() {
        return maxLength;
    }

    public String getExtendedFormat() {
        return extendedFormat;
    }

    /** Parse byte stream to produce runtime data element
     * @param is parsed input stream
     * @return parsed RuntimeDataElement
     * @throws IsoParseException parsing excetpion */
    public RuntimeDataElement parse(InputStream is) throws IsoParseException {
        RuntimeDataElement rde = new RuntimeDataElement(this);

        //take a length from common format or parsed stream
        int actualLength = (LengthTypeFactory.FIXED.equals(lengthType)) ?
                rde.getFormat().getLength() :
                lengthType.parseLength(is, rde.getFormat().getMaxLength());
        rde.setActualLength(actualLength);

        type.parse(is, actualLength, rde);

        return rde;
    }

    public void serialize(OutputStream os, RuntimeDataElement rde) throws IsoSerializeException{
        try{
            os.write(serialize(rde));
        }
        catch(IOException e){
            throw new IsoSerializeException("err.dataelementformat.serialization_failed", e);
        }
    }

    public byte[] serialize(RuntimeDataElement rde) throws IsoSerializeException{
        int lengthLimit = 0;
        boolean fixedLength = false;
        if(LengthTypeFactory.FIXED.equals(lengthType)){
            lengthLimit = rde.getFormat().getLength();
            fixedLength = true;
        }
        else{
            lengthLimit = rde.getFormat().getMaxLength();
            fixedLength = false;
        }

        
        Type.SerializedValue serializedVal = type.serialize(fixedLength, lengthLimit, rde);
        byte[] lengthData = lengthType.serializeLength(serializedVal.length);

        byte[] serializedElement = new byte[lengthData.length + serializedVal.data.length];
        System.arraycopy(lengthData, 0, serializedElement, 0, lengthData.length);
        System.arraycopy(serializedVal.data, 0, serializedElement, lengthData.length, serializedVal.data.length);
        return serializedElement;
    }

}