package com.tmx.web.controls;

import org.apache.log4j.Logger;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Serves to populate data from controls to desired types
 */
public class PopulateUtil {
    private final static Logger logger = Logger.getLogger("web.PopulateUtil");
    private static SimpleDateFormat DATE_FORMATER=new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");

    public static String getString(Date value){
        return DATE_FORMATER.format(value);
    }

    public static String getString(Byte value){
        return value==null?null:value.toString();
    }

    public static String getString(Integer value){
        return value==null?null:value.toString();
    }

    public static String getString(Long value){
        return value==null?null:value.toString();
    }

    public static Long getLongValue(EditBox editBox){
        Long resultValue = null;
        if (editBox == null || editBox.getValue() == null || "".equals(editBox.getValue().trim())){
            resultValue = null;
        }
        else{
            try{
                resultValue = new Long(Long.parseLong(editBox.getValue()));
            }
            catch(Exception e){
                logger.error("Failed to convert '"+editBox.getValue()+"' to Long, cause "+e.toString());
            }

        }
        return resultValue;
    }

    public static String getStringValue(EditBox editBox){
        String resultValue;
        if (editBox == null || editBox.getValue() == null || "".equals(editBox.getValue().trim())){
            resultValue = null;
        }
        else{
            resultValue = editBox.getValue();
        }
        return resultValue;
    }

    public static Double getDoubleValue(EditBox editBox){
        if(editBox != null && editBox.getValue() != null && !"".equals(editBox.getValue().trim())){
            try{
                return Double.valueOf(editBox.getValue());
            }
            catch(NumberFormatException e){
                logger.error("Failed to convert '"+editBox.getValue()+"' to Double, cause "+e.toString());
            }
        }
        return null;
    }

    public static Integer getIntValue(EditBox editBox){
        Integer resultValue = null;
        if (editBox == null || editBox.getValue() == null || "".equals(editBox.getValue().trim())){
            resultValue = null;
        }
        else{
            try{
                resultValue = new Integer(Integer.parseInt(editBox.getValue()));
            }
            catch(Exception e){
                logger.error("Failed to convert '"+editBox.getValue()+"' to Integer, cause "+e.toString());
            }

        }
        return resultValue;
    }

    public static int getBaseIntValue(EditBox editBox){
        Integer resultValue = getIntValue(editBox);
        return resultValue == null ? 0 : resultValue.intValue();
    }


    public static Long getLongKey(ScrollBox scrollBox){
        Long resultValue = null;
        if (scrollBox == null || scrollBox.getSelectedKey() == null || ScrollBox.NULL_KEY.equals(scrollBox.getSelectedKey())){
            resultValue = null;
        }
        else{
            try{
                resultValue = new Long(Long.parseLong(scrollBox.getSelectedKey()));
            }
            catch(Exception e){
                logger.error("Failed to convert '"+scrollBox.getSelectedKey()+"' to Long, cause "+e.toString());
            }

        }
        return resultValue;
    }

    public static Long getLongKey(BrowseBox browseBox){
        Long resultValue = null;
        if (browseBox == null || browseBox.getKey() == null){
            resultValue = null;
        }
        else{
            try{
                resultValue = new Long(Long.parseLong(browseBox.getKey()));
            }
            catch(Exception e){
                logger.error("Failed to convert '"+browseBox.getKey()+"' to Long, cause "+e.toString());
            }
        }
        return resultValue;
    }

    /**  Use this method in form to work with indexed controls */
    public static void setIndexedControl(int idx, String value, BasicControl[] ctrls){
        if(ctrls != null && ctrls.length > idx){
            if(ctrls instanceof EditBox[]){
                ((EditBox)ctrls[idx]).setValue(value);
            }
            else {}
        }
    }

    public static BasicControl getIndexedControl(int idx, BasicControl[] ctrls){
        if(ctrls != null && ctrls.length > idx)
            return ctrls[idx];
        else
            return null;
    }
}
