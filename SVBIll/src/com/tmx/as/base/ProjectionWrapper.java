package com.tmx.as.base;

import com.tmx.as.resolver.ResolverException;

/**

 */
public class ProjectionWrapper implements UnifiedPropertySetter{
    private String name = null;
    private String alias = null;
    private ProjectionType type = null;

    //Projection types
    public final static ProjectionType SUM = new ProjectionType("SUM");
    public final static ProjectionType COUNT = new ProjectionType("COUNT");
    public final static ProjectionType MIN = new ProjectionType("MIN");
    public final static ProjectionType MAX = new ProjectionType("MAX");
    public final static ProjectionType AVG = new ProjectionType("AVG");
    public final static ProjectionType PROPERTY = new ProjectionType("PROPERTY");
    public final static ProjectionType GROUPBY = new ProjectionType("GROUPBY");

    /** Constructor */
    public ProjectionWrapper(String name, String alias, ProjectionType type){
        this.name = name;
        this.alias = alias;
        this.type = type;
    }

    //------Properties
    public String getName() {
        return name;
    }

    public String getAlias() {
        return alias;
    }

    public ProjectionType getType() {
        return type;
    }

    /** Publish geven projections array into the string
    * @param projections array of orders to publish into String
    * @param formatString enble formating result string */
    public static String projectionsToString(ProjectionWrapper[] projections, boolean formatString){
        StringBuffer projectionsString = new StringBuffer();
        //Pepare formatting strings
        String retSymb = (formatString) ? "\r" : "";

        if(projections != null){
            for(int i = 0; i < projections.length; i++){
                projectionsString.append("[name='"+projections[i].getName()+"' ");
                projectionsString.append("alias='"+projections[i].getAlias()+"' ");
                projectionsString.append("type='"+((projections[i].getType() == null) ? "null" : projections[i].getType().toString())+"']"+ retSymb);
            }
        }
        //Return NULL instead of empty String. It is more obviosly for publishing
        String result = projectionsString.toString().trim();
        return (result.length() > 0) ? result : "[no projections]";
    }

    /** Creates new OrderWrapper[] array with size have increased for one position.
     * Then put given order into the last cell of new array. */
    public static ProjectionWrapper[] appendToProjections(ProjectionWrapper[] projections, ProjectionWrapper projection){
        if(projections == null)
            projections = new ProjectionWrapper[0];
        if(projection == null)
            return projections;
        ProjectionWrapper[] newProjections = new ProjectionWrapper[projections.length + 1];
        System.arraycopy(projections, 0, newProjections, 0, projections.length);
        newProjections[newProjections.length-1] = projection;
        return newProjections;
    }

    public void set(String fieldName, String fieldValue) throws ResolverException {
        if(fieldName.toLowerCase().equals("name")){
            name = fieldValue;
            return;
        }
        if(fieldName.toLowerCase().equals("alias")){
            alias = fieldValue;
            return;
        }
        if(fieldName.toLowerCase().equals("type")){
            if(fieldValue.toLowerCase().equals(SUM.toString().toLowerCase())){
                type = SUM;
                return;
            }
            if(fieldValue.toLowerCase().equals(COUNT.toString().toLowerCase())){
                type = COUNT;
                return;
            }
            if(fieldValue.toLowerCase().equals(MIN.toString().toLowerCase())){
                type = MIN;
                return;
            }
            if(fieldValue.toLowerCase().equals(MAX.toString().toLowerCase())){
                type = MAX;
                return;
            }
            if(fieldValue.toLowerCase().equals(AVG.toString().toLowerCase())){
                type = AVG;
                return;
            }
            if(fieldValue.toLowerCase().equals(PROPERTY.toString().toLowerCase())){
                type = PROPERTY;
                return;
            }
            if(fieldValue.toLowerCase().equals(GROUPBY.toString().toLowerCase())){
                type = GROUPBY;
                return;
            }
            throw new ResolverException("err.resolver.unknown_order_type", new String[]{this.getClass().getName(), fieldValue.toString()});
        }
        //RM: throw exception if field not found
        throw new ResolverException("err.resolver.unknown_parameter_for", new String[]{this.getClass().getName(), fieldName, fieldValue.toString()});
    }

    //-------Type incapsulation
    public static class ProjectionType {
        private String type = "UNKNOWN";

        private ProjectionType(String type){
            this.type = type;
        }

        public String toString(){
            return type;
        }
    }
}
