package com.tmx.engines.reportengine.aggrtable;

import java.util.Comparator;

/**
 * package local Integer idx comparator
 */
class IntIdxComparator implements Comparator {

    public int compare(Object o1, Object o2) {
        if (!(o1 instanceof Integer) || !(o2 instanceof Integer)) {
            throw new IllegalArgumentException("Comparated objects should have Integer type");
        }
        Integer i1 = (Integer) o1;
        Integer i2 = (Integer) o2;
        if (i1.intValue() < i2.intValue()) {
            return -1;
        } else if (i1.intValue() > i2.intValue()) {
            return 1;
        } else {
            //to avoid objects overwriting in TreeMap
            int dif = i1.hashCode() - i2.hashCode();
            if (dif > 0)
                return 1;
            else if (dif < 0)
                return -1;
            else
                return 0;
        }
    }
}
