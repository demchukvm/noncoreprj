package com.tmx.web.forms.core.reports;

import com.tmx.web.base.BasicActionForm;
import com.tmx.web.controls.grid.GridException;
import com.tmx.web.controls.grid.Grid;
import com.tmx.web.controls.MultipleSelectBox;
import com.tmx.web.controls.TimeSelector;
import com.tmx.web.controls.Button;
import com.tmx.as.entities.bill.seller.Seller;
import com.tmx.as.entities.bill.operator.Operator;
import com.tmx.as.entities.bill.transaction.ClientTransaction;
import com.tmx.as.entities.bill.transaction.OperationType;
import com.tmx.as.entities.bill.terminal.Terminal;
import com.tmx.as.entities.general.user.User;
import com.tmx.as.base.QueryParameterWrapper;
import com.tmx.as.base.criterion.CriterionWrapper;
import com.tmx.as.base.criterion.Restrictions;
import com.tmx.as.blogic.SellerManager;
import com.tmx.as.blogic.TerminalManager;
import com.tmx.as.exceptions.DatabaseException;
import com.tmx.util.i18n.MessageResources;

import java.util.*;
import java.text.DecimalFormat;

public class ReportSellesForm extends BasicActionForm {
    private MultipleSelectBox sellers;
    private MultipleSelectBox subSellers;
    private MultipleSelectBox terms;
    private MultipleSelectBox operators;
    private MultipleSelectBox msbOperationType;
    private TimeSelector from;
    private TimeSelector to;
    private Grid grid;
    private Button buttonSubmit;

    protected void init() {
        String action = "/core/report/reportSelles";
        buttonSubmit = new Button();
        buttonSubmit.setLabelKey("ctrl.button.label.apply");
        buttonSubmit.setCommand("refreshGrid");
        buttonSubmit.setAction(action);
        sellers = new MultipleSelectBox() {
            protected Object getKey(Object dataListElement) {
                return ((Seller) dataListElement).getSellerId();
            }

            protected String getValue(Object dataListElement) {
                return ((Seller) dataListElement).getName();
            }
        };
        sellers.setAllItem(true);

        subSellers = new MultipleSelectBox() {
            protected Object getKey(Object dataListElement) {
                return ((Seller) dataListElement).getSellerId();
            }

            protected String getValue(Object dataListElement) {
                return ((Seller) dataListElement).getName();
            }
        };
        subSellers.setAllItem(true);
        subSellers.setNoneItem(true);

        terms = new MultipleSelectBox() {
            protected Object getKey(Object dataListElement) {
                return ((Terminal) dataListElement).getTerminalId();
            }

            protected String getValue(Object dataListElement) {
                return ((Terminal) dataListElement).getSerialNumber();
            }
        };
        terms.setAllItem(true);

        operators = new MultipleSelectBox() {
            protected Object getKey(Object dataListElement) {
                return ((Operator) dataListElement).getOperatorId();
            }

            protected String getValue(Object dataListElement) {
                return ((Operator) dataListElement).getName();
            }
        };
        operators.setAllItem(true);

        msbOperationType = new MultipleSelectBox() {
            protected Object getKey(Object dataListElement) {
                return ((OperationType) dataListElement).getOperationTypeId();
            }

            protected String getValue(Object dataListElement) {
                return ((OperationType) dataListElement).getDescription();
            }
        };
        msbOperationType.setAllItem(true);
        //operators.setOnKeyDown(onKeyUpScript);

        from = new TimeSelector();
        //from.setOnKeyDown(onKeyUpScript);
        to = new TimeSelector();
        //to.setOnKeyDown(onKeyUpScript);
        grid = new Grid() {
            public void bind(List data) throws GridException {
                clear();

                DecimalFormat formatter = (DecimalFormat) DecimalFormat.getInstance();
                formatter.applyPattern("####0.00");

                int operColumnCount = 3;
                setCell(0, 0, getLable("terminals"), "column_header_cell", "font-weight: bold", 1, 2);
                HashMap operatorColumnIndexes = new HashMap();
                List operatorsList = new ArrayList();
                operatorsList.addAll(operators.getSelectedEntities());
                for (int i = 0; i < operatorsList.size(); i++) {
                    Operator operator = (Operator) operatorsList.get(i);
                    //Object[] ret = (Object[]) data.get(i);
                    int operatorColumnIndex = 1 + i * operColumnCount;
                    setCell(0, operatorColumnIndex, operator.getName(), "column_header_cell", "font-weight: bold", operColumnCount, 1);// oper name

                    //} else {
                       // setCell(0, operatorColumnIndex, operator.getName() + " " + i, "column_header_cell", "font-weight: bold", operColumnCount, 1);// oper name
                    //}

                    String sellerSumHeader = new StringBuilder().append("<div title=\"").append(getLable("sellerSumHelp"))
                                                    .append("\" >").append(getLable("sellerSum")).toString();
                    setCell(1, operatorColumnIndex, sellerSumHeader, "column_header_cell", "font-weight: bold");// sellerSum

                    String clientSumHeader = new StringBuilder().append("<div title=\"").append(getLable("clientSumHelp"))
                                                    .append("\" >").append(getLable("clientSum")).toString();
                    setCell(1, operatorColumnIndex + 1, clientSumHeader, "column_header_cell", "font-weight: bold");// clientSum

                    setCell(1, operatorColumnIndex + 2, getLable("countSum"), "column_header_cell", "font-weight: bold");// countSum
                    operatorColumnIndexes.put(operator.getOperatorId(), operatorColumnIndex);// save operatorColumnIndex
                    //Character header = '\u2211';
                }
                int columnCount = 1 + operatorsList.size() * operColumnCount;
                int generalSellerSumIndex = columnCount;
                setCell(0, generalSellerSumIndex, getLable("sellerSum"), "column_header_cell", "font-weight: bold", 1, 2);// general sellerSum
                int generalClientSumIndex = columnCount + 1;
                setCell(0, generalClientSumIndex, getLable("clientSum"), "column_header_cell", "font-weight: bold", 1, 2);// general clientSum

                // terminals names HashMap
                HashMap termNames = new HashMap();
                List selectedTerms = terms.getSelectedEntities();

                //DVM cheating
                try {
                    selectedTerms.clear();
                    selectedTerms.addAll(new TerminalManager().getAllTerminals());
                } catch (DatabaseException e) {
                    e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                }

                for (int i = 0; i < selectedTerms.size(); i++) {
                    Terminal term = (Terminal) selectedTerms.get(i);
                    termNames.put(term.getTerminalId(), term.getSerialNumber());
                }
                // sellers names HashMap
                HashMap sellerNames = new HashMap();
                List selectedSellers = sellers.getSelectedEntities();
                selectedSellers.addAll(subSellers.getSelectedEntities());

                //DVM cheating
                try {
                    selectedSellers.clear();
                    selectedSellers.addAll(new SellerManager().getAllSellers());
                } catch (DatabaseException e) {
                    e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                }
                for (Object selectedSeller : selectedSellers) {
                    Seller seller = (Seller) selectedSeller;
                    sellerNames.put(seller.getSellerId(), seller.getName());
                }
                //----------------------data output-----------------------------------------------------
                Long oldSellerId = null;
                float oldTerminalSellerSum = 0;
                float oldTerminalClientSum = 0;
                float oldSellerSum = 0;// seller sums
                float oldClientSum = 0;
                float allSellerSum = 0;
                float allClientSum = 0;
                Object[] netSums = new Object[operatorsList.size() * operColumnCount];
                Long oldTermId = null;
                int curRow = 1;// add head
                for (int i = 0; i < data.size(); i++) {
                    Object[] ret = (Object[]) data.get(i);
                    if (oldSellerId != null && !oldSellerId.equals(ret[0])) {// end seller
                        curRow++;
                        setCell(curRow, 0, getLable("totalGroup") + "" + (String) sellerNames.get(oldSellerId), "column_header_cell", "text-align: right;font-weight: bold", columnCount, 1);
                        setCell(curRow, generalSellerSumIndex, "" + oldSellerSum, "column_header_cell", "font-weight: bold");
                        setCell(curRow, generalClientSumIndex, "" + oldClientSum, "column_header_cell", "font-weight: bold");
                    }
                    if (oldSellerId == null || !oldSellerId.equals(ret[0])) {// start seller
                        oldSellerSum = 0;
                        oldClientSum = 0;
                        curRow++;
                        setCell(curRow, 0, (String) sellerNames.get(ret[0]), "column_header_cell", "font-weight: bold", columnCount + 2, 1);
                    }
                    if (oldSellerId == null || oldTermId == null || !(oldSellerId.equals(ret[0]) && oldTermId.equals(ret[1]))) {
                        oldTerminalSellerSum = 0;
                        oldTerminalClientSum = 0;
                        curRow++;
                        // set 0 to all sells
                        for (int j = 1; j < columnCount; j++) {
                            setCell(curRow, j, "0", "column_data_cell");
                        }
                    }
                    setCell(curRow, 0, (String) termNames.get(ret[1]), "column_data_cell");
                    setCell(curRow, (Integer) operatorColumnIndexes.get(ret[2]), formatter.format(ret[3]), "column_data_cell");//operator
                    setCell(curRow, (Integer) operatorColumnIndexes.get(ret[2]) + 1, formatter.format(ret[4]), "column_data_cell");//seller sum
                    setCell(curRow, (Integer) operatorColumnIndexes.get(ret[2]) + 2, ret[5].toString(), "column_data_cell");// client sum
                    // counters
                    allSellerSum += (Float) ret[3];
                    allClientSum += (Float) ret[4];
                    oldTerminalSellerSum += (Float) ret[3];
                    oldTerminalClientSum += (Float) ret[4];
                    oldSellerSum += (Float) ret[3];
                    oldClientSum += (Float) ret[4];
                    int ind = (Integer) operatorColumnIndexes.get(ret[2]) - 1;
                    netSums[ind] = (Float) ret[3] + (netSums[ind] == null ? 0 : ((Float) netSums[ind]));
                    ind = (Integer) operatorColumnIndexes.get(ret[2]);
                    netSums[ind] = (Float) ret[4] + (netSums[ind] == null ? 0 : ((Float) netSums[ind]));
                    ind = (Integer) operatorColumnIndexes.get(ret[2]) + 1;
                    netSums[ind] = (Integer) ret[5] + (netSums[ind] == null ? 0 : (Integer) netSums[ind]);
                    // terminal sums output
                    setCell(curRow, generalSellerSumIndex, "" + oldTerminalSellerSum, "column_data_cell");
                    setCell(curRow, generalClientSumIndex, "" + oldTerminalClientSum, "column_data_cell");

                    oldSellerId = (Long) ret[0];
                    oldTermId = (Long) ret[1];
                }
                // end seller
                curRow++;
                setCell(curRow, 0, getLable("totalGroup") + " " + sellerNames.get(oldSellerId), "column_header_cell", "text-align: right;font-weight: bold", columnCount, 1);// seller name
                setCell(curRow, generalSellerSumIndex, formatter.format(oldSellerSum), "column_header_cell", "font-weight: bold");// seler sum
                setCell(curRow, generalClientSumIndex, formatter.format(oldClientSum), "column_header_cell", "font-weight: bold");// client sum
                // net sum
                curRow++;
                setCell(curRow, 0, getLable("netSum") + ":", "column_header_cell", "font-weight: bold");
                for (int i = 0; i < netSums.length; i++) {
                    Object netSum = netSums[i];
                    setCell(curRow, i + 1, netSum == null ? "0" : formatter.format(netSum), "column_header_cell", "font-weight: bold");
                }
                setCell(curRow, netSums.length + 1, formatter.format(allSellerSum), "column_header_cell", "font-size: large");
                setCell(curRow, netSums.length + 2, formatter.format(allClientSum), "column_header_cell", "font-size: large");
            }

            private String getLable(String key) {
                return MessageResources.getInstance().getOwnedLocalizedMessage(this.getClass(), "labels", getLocale(), key, null);
            }
        };
    }

    private List oldTermSellers;

    public void refresh(SellerManager sellerManager, User user) throws Throwable {
        QueryParameterWrapper[] params = prepareFilters(sellerManager, user);
        bindGrid(sellerManager, params);
    }

    protected void bindGrid(SellerManager sellerManager, QueryParameterWrapper[] params) throws GridException, DatabaseException {
        grid.bind(sellerManager.EXECUTE_QUERY(ClientTransaction.class, "retrieveSelles", params));
    }

    private QueryParameterWrapper[] prepareFilters(SellerManager sellerManager, User user) throws DatabaseException, Exception {// fill selers

        //bind sellers boxes
        Set sellerSelectedKeys = sellers.getSelectedKeys();
        if (user.getRole().getRoleName().equals("SUPERUSER")) {
            boolean bound = sellers.isBounded();
            sellers.bind(sellerManager.getAllSellers());
            if (!bound && user.getSellerOwner() != null) {
                //                sellers.setSelectedKeys(new Object[]{user.getSellerOwner().getSellerId()});
                sellers.addSelectedIndex(0);
            }
        } else {
            if (user.getSellerOwner() == null) throw new Exception("ISBuser without seller");
            ArrayList arrayList = new ArrayList();
            arrayList.add(user.getSellerOwner());
            arrayList.addAll(sellerManager.getSellersInSeller(user.getSellerOwner().getSellerId()));
            sellers.bind(arrayList);
            sellers.addSelectedIndex(0);
        }
        if (sellerSelectedKeys != null) sellers.setSelectedKeys(sellerSelectedKeys);
        // bind subselers
        Set subSellerSelectedKeys = subSellers.getSelectedKeys();
        Iterator it = sellers.getSelectedKeys().iterator();
        ArrayList subSellersList = new ArrayList();
        while (it.hasNext()) {
            Long sellerId = (Long) it.next();
            subSellersList.addAll(sellerManager.getSellersInSeller(sellerId));
        }
        subSellers.bind(subSellersList);
        if (subSellerSelectedKeys != null) subSellers.setSelectedKeys(subSellerSelectedKeys);

        // bind terms
        ArrayList<Long> termSellers = new ArrayList<Long>();
//        termSellers.addAll(sellers.getFullData());
//        termSellers.addAll(subSellers.getFullData());
        termSellers.addAll(sellers.getSelectedKeys());
        termSellers.addAll(subSellers.getSelectedKeys());
        List<Long> sellersId = new ArrayList<Long>();
//        for (Seller seller : termSellers) {
//            sellersId.add(seller.getSellerId());
//        }
        if (oldTermSellers == null || !oldTermSellers.equals(termSellers)) {

            if (termSellers.size() > 0) {
                CriterionWrapper[] criterionWrappers = null;
                if (termSellers.size() > 0) {
                    criterionWrappers = new CriterionWrapper[]{Restrictions.in("sellerOwnerId", termSellers)};
                }

                terms.bind(sellerManager.RETRIEVE_ALL(Terminal.class, criterionWrappers, null));
                terms.selectAll();
            }
        }
        oldTermSellers = termSellers;

        //bind operators
        Set operatorSelectedKeys = operators.getSelectedKeys();
        operators.bind(sellerManager.RETRIEVE_ALL(Operator.class));
        if (operatorSelectedKeys != null && operatorSelectedKeys.size() != 0) {
            operators.setSelectedKeys(operatorSelectedKeys);
        } else {
            operators.selectAll();
        }

        // bind grid
        Set sellersSet = new HashSet();
        if (!sellers.isSelectedAll() || !subSellers.isSelectedAll()) {
            sellersSet.addAll(sellers.getSelectedKeys());
            sellersSet.addAll(subSellers.getSelectedKeys());
        }
        //        if (sellersSet.size() == 0) {
        //            sellersSet.add(1l);
        //        }

        Set operationTypes = new HashSet();
        if (!msbOperationType.isSelectedAll()) {
            operationTypes.addAll(msbOperationType.getSelectedKeys());
        }


        //TODO: remove IT!!!! Make filtering by terminal!!
        Set terminalSet = new HashSet();
        terminalSet.addAll((new TerminalManager().getAllTerminals()));
        return new QueryParameterWrapper[]{
//                new QueryParameterWrapper("terminalFiltering", terms.isSelectedAll() ? 0 : 1), //ANREM it, and make filter
//                new QueryParameterWrapper("terminals", terms.getSelectedKeys()),

                new QueryParameterWrapper("terminalFiltering", terminalSet.size() == 0 ? 0 : 1),
                new QueryParameterWrapper("terminals", terminalSet),

                new QueryParameterWrapper("sellerFiltering", sellersSet.size() == 0 ? 0 : 1),
                new QueryParameterWrapper("sellers", sellersSet),

                new QueryParameterWrapper("operatorFiltering", operators.isSelectedAll() ? 0 : 1),
                new QueryParameterWrapper("operators", operators.getSelectedKeys()),

                new QueryParameterWrapper("fromFiltering", from.getTime() == null ? 1 : 0),
                new QueryParameterWrapper("from", from.getTime() == null ? new Date() : from.getTime()),

                new QueryParameterWrapper("toFiltering", to.getTime() == null ? 0 : 1),
                new QueryParameterWrapper("to", to.getTime() == null ? new Date() : to.getTime()),

                new QueryParameterWrapper("isFilterByTypeSet", operationTypes.size() == 0 ? 0 : 1),
                new QueryParameterWrapper("types", msbOperationType.getSelectedKeys()),
        };
    }

    public MultipleSelectBox getSellers() {
        return sellers;
    }

    public void setSellers(MultipleSelectBox sellers) {
        this.sellers = sellers;
    }

    public MultipleSelectBox getSubSellers() {
        return subSellers;
    }

    public void setSubSellers(MultipleSelectBox subSellers) {
        this.subSellers = subSellers;
    }

    public MultipleSelectBox getTerms() {
        return terms;
    }

    public void setTerms(MultipleSelectBox terms) {
        this.terms = terms;
    }

    public MultipleSelectBox getOperators() {
        return operators;
    }

    public void setOperators(MultipleSelectBox operators) {
        this.operators = operators;
    }

    public TimeSelector getFrom() {
        return from;
    }

    public void setFrom(TimeSelector from) {
        this.from = from;
    }

    public TimeSelector getTo() {
        return to;
    }

    public void setTo(TimeSelector to) {
        this.to = to;
    }

    public Grid getGrid() {
        return grid;
    }

    public void setGrid(Grid grid) {
        this.grid = grid;
    }


    public Button getButtonSubmit() {
        return buttonSubmit;
    }

    public void setButtonSubmit(Button buttonSubmit) {
        this.buttonSubmit = buttonSubmit;
    }

    public MultipleSelectBox getMsbOperationType() {
        return msbOperationType;
    }

    public void setMsbOperationType(MultipleSelectBox msbOperationType) {
        this.msbOperationType = msbOperationType;
    }


}


