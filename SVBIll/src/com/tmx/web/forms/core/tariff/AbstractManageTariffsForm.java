package com.tmx.web.forms.core.tariff;

import com.tmx.web.base.BasicActionForm;
import com.tmx.web.controls.Button;
import com.tmx.web.controls.EditBox;
import com.tmx.web.controls.table.*;

import java.util.Date;


abstract public class AbstractManageTariffsForm extends BasicActionForm {

    private ControlManagerTable tariffsTable;
    private Button onCreateButton;
    private Button onEditButton;
    private Button onDeleteButton;

    protected void init(){
        initTables();
        initButtons();
        addCtrlToTable();
    }

    //--------------private methods-------------------
    private void initButtons() {
        onCreateButton = new Button();
        onCreateButton.setLabelKey("ctrl.button.label.create");
        onCreateButton.setCommand("onCreateTariffAction");

        onEditButton = new Button();
        onEditButton.setLabelKey("ctrl.button.label.edit");
        onEditButton.setCommand("preloadForUpdate");

        onDeleteButton = new Button();
        onDeleteButton.setLabelKey("ctrl.button.label.delete");
        onDeleteButton.setCommand("preloadForDelete");
    }

    private void initTables(){
        tariffsTable = new ControlManagerTable();
        tariffsTable.addParameterToFilter("by_tariff_name", new Parameter("tariff_name", new EditBox()));

        Filter filter = new Filter("by_active_tariff", new Parameter("current_date", new Date()));
        filter.setFilterEnabler(new CheckBoxFilterEnabler());
        tariffsTable.addFilter(filter);
    }

    private void addCtrlToTable() {
        tariffsTable.addDependedControl(onEditButton).addDependedControl(onDeleteButton);
    }
    
    //--------------------------------------------------
    public ControlManagerTable getTariffsTable() {
        return tariffsTable;
    }

    public void setTariffsTable(ControlManagerTable tariffsTable) {
        this.tariffsTable = tariffsTable;
    }

    public Button getOnCreateButton() {
        return onCreateButton;
    }

    public void setOnCreateButton(Button onCreateButton) {
        this.onCreateButton = onCreateButton;
    }

    public Button getOnEditButton() {
        return onEditButton;
    }

    public void setOnEditButton(Button onEditButton) {
        this.onEditButton = onEditButton;
    }

    public Button getOnDeleteButton() {
        return onDeleteButton;
    }

    public void setOnDeleteButton(Button onDeleteButton) {
        this.onDeleteButton = onDeleteButton;
    }
   
}
