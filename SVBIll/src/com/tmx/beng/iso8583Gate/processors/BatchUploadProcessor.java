package com.tmx.beng.iso8583Gate.processors;

import com.tmx.beng.iso8583Gate.pdu.RuntimeMessage;
import com.tmx.beng.iso8583Gate.pdu.IncorrectFormatException;
import com.tmx.beng.iso8583Gate.mapper.BillingMapper;
import com.tmx.beng.iso8583Gate.mapper.BillingMapperMetadata;
import com.tmx.beng.base.BillException;
import com.tmx.util.InitException;
import com.tmx.as.exceptions.DatabaseException;
import com.tmx.as.entities.iso8583.transaction.Iso8583Transaction;
import com.tmx.as.entities.iso8583.collation.Collation;
import com.tmx.as.base.EntityManager;
import com.tmx.as.base.FilterWrapper;

/**
 */
public class BatchUploadProcessor extends AbstractBatchProcessor{

    public int getMessageTypeId() {
        return 330;
    }

    public int getProcessingCode() {
        return 0;//will be overriden in getDEToCopy()
    }

    public int[] getDEToCopy() {
        return new int[]{2, 3, 4, 11, 37, 38, 39, 41, 61};
    }

    public void processSpecificTask(RuntimeMessage reqMsg, RuntimeMessage respMsg)
            throws ProcessingException, BillException, IncorrectFormatException, InitException, DatabaseException {

        logger.debug("Batch upload");
        BillingMapper billingMapper = BillingMapperMetadata.getInstance().getBillMapper(reqMsg);

        //prepare and pre-save iso transaction in iso datasource
        Iso8583Transaction tx = billingMapper.prepareTransaction(reqMsg);
        Collation collation = retrieveLastCollationAttempt(tx);
        assignBatchTransactionToCollation(tx, collation);
        assignFinTransactionToCollation(tx, collation);

        //prepare iso response
        billingMapper.prepare0330Msg(respMsg, tx);
    }

    private void assignFinTransactionToCollation(Iso8583Transaction batchTx, Collation collation) throws DatabaseException{
        EntityManager em = new EntityManager();
        FilterWrapper byTerminalIdFW = new FilterWrapper("by_isoterminalid");
        byTerminalIdFW.setParameter("terminalid", batchTx.getTerminalId());
        FilterWrapper byMerchantIdFW = new FilterWrapper("by_merchantid");
        byMerchantIdFW.setParameter("merchantid", batchTx.getCardAcqId());
        FilterWrapper byRetrievalRefNumFW = new FilterWrapper("by_retrrefnum");
        byRetrievalRefNumFW.setParameter("rrn", batchTx.getRetrievalRefNo());
        FilterWrapper byBatchNumFW = new FilterWrapper("by_batchnum");
        byBatchNumFW.setParameter("batchnum", batchTx.getBatchNumber());
        FilterWrapper byFinTxFW = new FilterWrapper("by_fin_tx");


        Iso8583Transaction finTx = (Iso8583Transaction)em.RETRIEVE(
                Iso8583Transaction.class,
                new FilterWrapper[]{byTerminalIdFW, byMerchantIdFW, byRetrievalRefNumFW, byBatchNumFW, byFinTxFW},
                new String[]{"collation"});

        //Assign to collation only found tx. Do not assign transaction is under alredy closed collation
        if(finTx == null || finTx.getCollation() != null && finTx.getCollation().getClosed())
            return;

        finTx.setCollation(collation);
        em.SAVE(finTx);
    }
}
