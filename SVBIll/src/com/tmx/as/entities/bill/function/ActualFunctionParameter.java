package com.tmx.as.entities.bill.function;

import com.tmx.as.base.BasicEntity;

import java.io.Serializable;

/**
 * Actual function parameter
 */
public class ActualFunctionParameter extends BasicEntity implements Cloneable, Serializable {
    private Long actualFunctionParameterId;
    private FunctionInstance functionInstance;
    private Long orderNum;
    private String type;
    private String name;
    private String value;
    private Long functionInstanceId;
    private Boolean mandatory;

    //----------------Properties
    public Long getActualFunctionParameterId() {
        return actualFunctionParameterId;
    }

    public void setActualFunctionParameterId(Long actualFunctionParameterId) {
        this.actualFunctionParameterId = actualFunctionParameterId;
    }

    public FunctionInstance getFunctionInstance() {
        return functionInstance;
    }

    public void setFunctionInstance(FunctionInstance functionInstance) {
        this.functionInstance = functionInstance;
    }

    public Long getOrderNum() {
        return orderNum;
    }

    public void setOrderNum(Long orderNum) {
        this.orderNum = orderNum;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Serializable getId() {
        return getActualFunctionParameterId();
    }

    public int getValid() {
        return valid;
    }

    public void setValid(int valid) {
        this.valid = valid;
    }

    public Long getFunctionInstanceId() {
        return functionInstanceId;
    }

    public void setFunctionInstanceId(Long functionInstanceId) {
        this.functionInstanceId = functionInstanceId;
    }

    public Boolean getMandatory() {
        return mandatory;
    }

    public void setMandatory(Boolean mandatory) {
        this.mandatory = mandatory;
    }

    public ActualFunctionParameter clone() throws CloneNotSupportedException {
        return (ActualFunctionParameter) super.clone();
    }
}
