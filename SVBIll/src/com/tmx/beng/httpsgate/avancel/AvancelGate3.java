package com.tmx.beng.httpsgate.avancel;

import org.apache.log4j.Logger;
import com.tmx.beng.httpsgate.HttpsGateException;
import com.tmx.util.InitException;

/**

 */
public class AvancelGate3 extends BasicAvancelGate{
    protected static final String GATE_NAME = "avancel_gate_3";
    protected static Logger logger = Logger.getLogger("avancel_gate_3." + GATE_NAME);
    private static AvancelGate3 avancelGate = null;


    public static AvancelGate3 getInstance() throws HttpsGateException {
        if(avancelGate == null){
            logger.error("Attempt to obtain not inited AvancelGate3");
            throw new HttpsGateException("err.AvancelGate3.null_instance");
        }
        return avancelGate;
    }

    public static void init()throws InitException {
        logger.info("Start AvancelGate3 initialization");
        avancelGate = new AvancelGate3();
        avancelGate.init0();
        logger.info("AvancelGate3 initialized");
    }

    public static void shutdown(final long timeout) {
        logger.info("Start AvancelGate3 Shutdown");
        avancelGate.shutdown0(timeout);
        logger.info("AvancelGate3 shutdown");
    }

    protected String getGateName(){
        return GATE_NAME;
    }

}
