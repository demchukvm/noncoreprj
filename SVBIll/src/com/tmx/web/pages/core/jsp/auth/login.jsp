<%@ page language="java" pageEncoding="UTF-8" contentType="text/html; charset=utf-8"
         isELIgnored="false" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="http://tmx.com/taglib/tmx-controls" prefix="tmx_ctrl" %>
<%@ page import="java.util.Random" %>

<html:form action="/core/auth/login">

<%
    Random random = new Random();
    double i = random.nextDouble();
	int val = (int) (i*1000);
	if(val>=0 && val<250)
	{
%>

<!-- Variant 1 start -->
<table id="Table_01" width="684" height="436" border="0" cellpadding="0" cellspacing="0" style="position:absolute; left:24%; top:8%;">
	<tr>
		<td width="229" height="165" background="<tmx_ctrl:rewriteUrl href="/core/images/common/login/1/Untitled-1_01.png"/>" />
		<td width="233" height="165" background="<tmx_ctrl:rewriteUrl href="/core/images/common/login/1/Untitled-1_02.png"/>" />
		<td width="222" height="165" background="<tmx_ctrl:rewriteUrl href="/core/images/common/login/1/Untitled-1_03.png"/>" />
	</tr>
	<tr>
		<td width="229" height="171" background="<tmx_ctrl:rewriteUrl href="/core/images/common/login/1/Untitled-1_04.png"/>" />
		<td width="233" height="171" background="<tmx_ctrl:rewriteUrl href="/core/images/common/login/1/Untitled-1_05.png"/>" align="center"/>
			<table width="100%" border="0" align="center" style="border: solid 0px black;">
				<tr>
					<td colspan="2" align="center">
                        <bean:message bundle="core.labels" key="login.title.authorization" />
					</td>
				</tr>
				<tr>
					<td colspan="2" align="left" height="10px" />
				</tr>
				<tr>
					<td align="right">
                        <bean:message bundle="core.labels" key="login.label.login" />:
					</td>
					<td align="left">
						<tmx_ctrl:editBox property="ebLogin"/>
					</td>
				</tr>
				<tr>
					<td align="right">
                        <bean:message bundle="core.labels" key="login.label.password" />:
					</td>
					<td align="left">
						<tmx_ctrl:passwordEditBox property="ebPassword"/>
					</td>
				</tr>
				<tr>
					<td colspan="2" align="left" />
				</tr>
				<tr>
					<td align="center" colspan="2">
						<tmx_ctrl:button property="bEnter"/>
					</td>
				</tr>
			</table>
		</td>
		<td width="222" height="171" background="<tmx_ctrl:rewriteUrl href="/core/images/common/login/1/Untitled-1_06.png"/>" />
	</tr>
	<tr>
		<td width="229" height="100" background="<tmx_ctrl:rewriteUrl href="/core/images/common/login/1/Untitled-1_07.png"/>" />
		<td width="233" height="100" background="<tmx_ctrl:rewriteUrl href="/core/images/common/login/1/Untitled-1_08.png"/>" />
		<td width="222" height="100" background="<tmx_ctrl:rewriteUrl href="/core/images/common/login/1/Untitled-1_09.png"/>" />
	</tr>
</table>

<!-- Variant 1 end -->
<%
	}
	else if(val>=250 && val<500)
	{
%>
<!-- Variant 2 start -->

<table id="Table_01" width="525" height="425" border="0" cellpadding="0" cellspacing="0" style="position:absolute; left:35%; top:7%;">
	<tr>
		<td width="90" height="172" background="<tmx_ctrl:rewriteUrl href="/core/images/common/login/2/Untitled-2_01.png"/>" />
		<td width="228" height="172" background="<tmx_ctrl:rewriteUrl href="/core/images/common/login/2/Untitled-2_02.png"/>" />
		<td width="207" height="172" background="<tmx_ctrl:rewriteUrl href="/core/images/common/login/2/Untitled-2_03.png"/>" />
	</tr>
	<tr>
		<td width="90" height="170" background="<tmx_ctrl:rewriteUrl href="/core/images/common/login/2/Untitled-2_04.png"/>" />
		<td width="228" height="170" background="<tmx_ctrl:rewriteUrl href="/core/images/common/login/2/Untitled-2_05.png"/>" align="center"/>
			<table width="100%" border="0" align="center" style="border: solid 0px black;">
				<tr>
					<td colspan="2" align="center">
                        <bean:message bundle="core.labels" key="login.title.authorization" />
					</td>
				</tr>
				<tr>
					<td colspan="2" align="left" height="10px" />
				</tr>
				<tr>
					<td align="right">
                        <bean:message bundle="core.labels" key="login.label.login" />:
					</td>
					<td align="left">
						<tmx_ctrl:editBox property="ebLogin"/>
					</td>
				</tr>
				<tr>
					<td align="right">
                        <bean:message bundle="core.labels" key="login.label.password" />:
					</td>
					<td align="left">
						<tmx_ctrl:passwordEditBox property="ebPassword"/>
					</td>
				</tr>
				<tr>
					<td colspan="2" align="left" />
				</tr>
				<tr>
					<td align="center" colspan="2">
						<tmx_ctrl:button property="bEnter"/>
					</td>
				</tr>
			</table>
		</td>
		<td width="207" height="170" background="<tmx_ctrl:rewriteUrl href="/core/images/common/login/2/Untitled-2_06.png"/>" />
	</tr>
	<tr>
		<td width="90" height="83" background="<tmx_ctrl:rewriteUrl href="/core/images/common/login/2/Untitled-2_07.png"/>" />
		<td width="228" height="83" background="<tmx_ctrl:rewriteUrl href="/core/images/common/login/2/Untitled-2_08.png"/>" />
		<td width="207" height="83" background="<tmx_ctrl:rewriteUrl href="/core/images/common/login/2/Untitled-2_09.png"/>" />
	</tr>
</table>

<!-- Variant 2 end -->
<%
	}
	else if(val>=500 && val<750)
	{
%>
<!-- Variant 3 start -->

<table id="Table_01" width="569" height="464" border="0" cellpadding="0" cellspacing="0" style="position:absolute; left:30%; top:15%;">
	<tr>
		<td width="144" height="110" background="<tmx_ctrl:rewriteUrl href="/core/images/common/login/3/Untitled-3_01.png"/>" />
		<td width="237" height="110" background="<tmx_ctrl:rewriteUrl href="/core/images/common/login/3/Untitled-3_02.png"/>" />
		<td width="188" height="110" background="<tmx_ctrl:rewriteUrl href="/core/images/common/login/3/Untitled-3_03.png"/>" />
	</tr>
	<tr>
		<td width="144" height="153" background="<tmx_ctrl:rewriteUrl href="/core/images/common/login/3/Untitled-3_04.png"/>" />
		<td width="237" height="153" background="<tmx_ctrl:rewriteUrl href="/core/images/common/login/3/Untitled-3_05.png"/>" align="center"/>
			<table width="100%" border="0" align="center" style="border: solid 0px black;">
				<tr>
					<td colspan="2" align="center">
                        <bean:message bundle="core.labels" key="login.title.authorization" />
					</td>
				</tr>
				<tr>
					<td colspan="2" align="left" height="4px" />
				</tr>
				<tr>
					<td align="right">
                        <bean:message bundle="core.labels" key="login.label.login" />:
					</td>
					<td align="left">
						<tmx_ctrl:editBox property="ebLogin"/>
					</td>
				</tr>
				<tr>
					<td align="right">
                        <bean:message bundle="core.labels" key="login.label.password" />:
					</td>
					<td align="left">
						<tmx_ctrl:passwordEditBox property="ebPassword"/>
					</td>
				</tr>
				<tr>
					<td colspan="2" align="left" />
				</tr>
				<tr>
					<td align="center" colspan="2">
						<tmx_ctrl:button property="bEnter"/>
					</td>
				</tr>
			</table>
		</td>
		<td width="188" height="153" background="<tmx_ctrl:rewriteUrl href="/core/images/common/login/3/Untitled-3_06.png"/>" />
	</tr>
	<tr>
		<td width="144" height="201" background="<tmx_ctrl:rewriteUrl href="/core/images/common/login/3/Untitled-3_07.png"/>" />
		<td width="237" height="201" background="<tmx_ctrl:rewriteUrl href="/core/images/common/login/3/Untitled-3_08.png"/>" />
		<td width="188" height="201" background="<tmx_ctrl:rewriteUrl href="/core/images/common/login/3/Untitled-3_09.png"/>" />
	</tr>
</table>

<!-- Variant 3 end -->
<%
	}
	else
	{
%>
<!-- Variant 4 start -->

<table id="Table_01" width="683" height="424" border="0" cellpadding="0" cellspacing="0" style="position:absolute; left:27%; top:9%;">
	<tr>
		<td width="173" height="149" background="<tmx_ctrl:rewriteUrl href="/core/images/common/login/4/Untitled-4_01.png"/>" />
		<td width="221" height="149" background="<tmx_ctrl:rewriteUrl href="/core/images/common/login/4/Untitled-4_02.png"/>" />
		<td width="188" height="110" background="<tmx_ctrl:rewriteUrl href="/core/images/common/login/4/Untitled-4_03.png"/>" />
	</tr>
	<tr>
		<td width="173" height="175" background="<tmx_ctrl:rewriteUrl href="/core/images/common/login/4/Untitled-4_04.png"/>" />
		<td width="221" height="175" background="<tmx_ctrl:rewriteUrl href="/core/images/common/login/4/Untitled-4_05.png"/>" align="center"/>
			<table width="100%" border="0" align="center" style="border: solid 0px black;">
				<tr>
					<td colspan="2" align="center">
                        <bean:message bundle="core.labels" key="login.title.authorization" />
					</td>
				</tr>
				<tr>
					<td colspan="2" align="left" height="10px" />
				</tr>
				<tr>
					<td align="right">
                        <bean:message bundle="core.labels" key="login.label.login" />:
					</td>
					<td align="left">
						<tmx_ctrl:editBox property="ebLogin"/>
					</td>
				</tr>
				<tr>
					<td align="right">
                        <bean:message bundle="core.labels" key="login.label.password" />:
					</td>
					<td align="left">
						<tmx_ctrl:passwordEditBox property="ebPassword"/>
					</td>
				</tr>
				<tr>
					<td colspan="2" align="left" />
				</tr>
				<tr>
					<td align="center" colspan="2">
						<tmx_ctrl:button property="bEnter"/>
					</td>
				</tr>
			</table>
		</td>
		<td width="289" height="175" background="<tmx_ctrl:rewriteUrl href="/core/images/common/login/4/Untitled-4_06.png"/>" />
	</tr>
	<tr>
		<td width="173" height="100" background="<tmx_ctrl:rewriteUrl href="/core/images/common/login/4/Untitled-4_07.png"/>" />
		<td width="221" height="100" background="<tmx_ctrl:rewriteUrl href="/core/images/common/login/4/Untitled-4_08.png"/>" />
		<td width="289" height="100" background="<tmx_ctrl:rewriteUrl href="/core/images/common/login/4/Untitled-4_09.png"/>" />
	</tr>
</table>

<!-- Variant 4 end -->
<%
	}
%>

<table style="position:absolute;bottom:0;right:5px;" border="0">
	<tr>
    	<td height="5" />
    </tr>
	<tr>
		<td align='center' class='copyRight' style="border-top: #ccc 1px solid;">
            &copy; 2003-2007 <a href="http://www.tmx.com.ua">Technomatix Inc.</a> All Rights Reserved<br>
			&copy; 2008-pres. <a href="http://www.svcard.com.ua">SVCard.</a> All Rights Reserved
        </td>
    </tr>
</table>

</html:form>