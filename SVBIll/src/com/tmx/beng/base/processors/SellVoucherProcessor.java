package com.tmx.beng.base.processors;

import com.tmx.beng.base.*;
import com.tmx.beng.base.cache.DBCache;
import com.tmx.as.base.EntityManager;
import com.tmx.as.entities.bill.voucher.Voucher;
import com.tmx.as.exceptions.DatabaseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 */
public class SellVoucherProcessor extends TaskProcessor{

    protected Task process0(Task task, DBCache dbCache, BillingMessageWritable msg) throws BillException {
        logger.debug("Sell voucher...");
        Voucher voucher = (Voucher)msg.getAttribute(BillingMessage.VOUCHER);
        voucher.setStatus(Voucher.STATUS_SOLD);
        voucher.setSoldTime(new Date());
        voucher.setSoldInBillTransactionNum(msg.getAttributeString(BillingMessage.BILL_TRANSACTION_NUM));
        try{
            new EntityManager().SAVE(voucher, new String[]{"status", "soldTime", "soldInBillTransactionNum"});
        }
        catch(DatabaseException e){
            throw new BillException("err.failed_to_update_voucher", e);
        }
        msg.setStatusCode(StatusDictionary.STATUS_OK);
        return task;
    }
}
