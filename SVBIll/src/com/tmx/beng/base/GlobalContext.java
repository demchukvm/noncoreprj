package com.tmx.beng.base;

import com.tmx.util.InitException;
import com.tmx.beng.base.cache.DBCache;
import com.tmx.beng.base.cache.DBCacheStack;
import javax.servlet.*;
import java.util.Iterator;

/**
 */
public class GlobalContext {
    private TaskPool taskPool;
    private TransactionPool transactionPool;
    private Reporter reporter;
    private MediumAccessPool mediumAccessPool;
    private double reloadCounter;
//    private DBCache dbCache;

    private DBCacheStack dbCacheStack;

    public GlobalContext(){
        taskPool = new TaskPool();
        transactionPool = new TransactionPool();
        mediumAccessPool = new MediumAccessPool();
        reporter = new Reporter();
    }

    public void init() throws InitException {
        DBCache dbCache = new DBCache();
        dbCache.reload();
        dbCacheStack = new DBCacheStack(dbCache);
        //initialize last
        mediumAccessPool.init();
    }


    public TaskPool getTaskPool() {
        return taskPool;
    }

    public TransactionPool getTransactionPool() {
        return transactionPool;
    }

    public MediumAccessPool getMediumAccessPool() {
        return mediumAccessPool;
    }

    public Reporter getReporter() {
        return reporter;
    }


    //TODO(A.N.) Specific place, bad code---------------
    public void reloadDBCache() throws InitException {
        reloadCounter++;
        long time = System.currentTimeMillis();
        DBCache dbCache = new DBCache();
        dbCache.reload();
        dbCacheStack.push(dbCache);
        dbCacheStack.dump(time, getTaskPool().size(), getTransactionPool().size(), reloadCounter);
    }
     
    //
    DBCache getDbCache() {
        return dbCacheStack.peek();
    }
    //--------------------------------------------------

    public Task allocateTask(BillingMessage message, CallBack callBack) throws BillException {
        Task result = taskPool.allocateTask(message);
        result.setGlobalContext(this);
        result.setCallBack(callBack);
        return result;
    }
    
}
