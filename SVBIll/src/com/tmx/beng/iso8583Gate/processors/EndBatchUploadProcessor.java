package com.tmx.beng.iso8583Gate.processors;

import com.tmx.beng.iso8583Gate.pdu.RuntimeMessage;
import com.tmx.beng.iso8583Gate.pdu.IncorrectFormatException;
import com.tmx.beng.iso8583Gate.pdu.substructure.RuntimeSubDataElement;
import com.tmx.beng.iso8583Gate.pdu.substructure.collation.BasicCollationSDEF;
import com.tmx.beng.iso8583Gate.mapper.BillingMapper;
import com.tmx.beng.iso8583Gate.mapper.BillingMapperMetadata;
import com.tmx.beng.base.BillException;
import com.tmx.util.InitException;
import com.tmx.as.exceptions.DatabaseException;
import com.tmx.as.entities.iso8583.transaction.Iso8583Transaction;
import com.tmx.as.entities.iso8583.collation.Collation;
import com.tmx.as.base.EntityManager;
import com.tmx.as.base.FilterWrapper;

import java.util.List;
import java.util.Date;
import java.util.Iterator;

/**
 */
public class EndBatchUploadProcessor extends AbstractBatchProcessor{

    public int getMessageTypeId() {
        return 530;
    }

    public int getProcessingCode() {
        return 930000;
    }

    public int[] getDEToCopy() {
        return new int[]{11, 24, 41, 61};
    }

    public void processSpecificTask(RuntimeMessage reqMsg, RuntimeMessage respMsg)
            throws ProcessingException, BillException, IncorrectFormatException, InitException, DatabaseException {

        logger.debug("End of batch");
        BillingMapper billingMapper = BillingMapperMetadata.getInstance().getBillMapper(reqMsg);

        //prepare and pre-save iso transaction in iso datasource
        Iso8583Transaction tx = billingMapper.prepareTransaction(reqMsg);
        billingMapper.assignRetrievalRefNum(tx);  
        Collation collation = retrieveLastCollationAttempt(tx);
        assignBatchTransactionToCollation(tx, collation);

        List batchTransactions = retrieveBatchTransactions(tx, collation);

        calculateBillingCollation(collation, batchTransactions);
        calculateTerminalCollation(collation, reqMsg);

        //compare billing and terminal transactions
        collation.setEndCollationTime(new Date());
        new EntityManager().SAVE(collation);

        //close collation attempts
        closeCollationAttempts(tx, collation);

        billingMapper.prepare0530Msg(respMsg, collation);
    }

    private List retrieveBatchTransactions(Iso8583Transaction endBatchTx, Collation collation) throws DatabaseException {
        FilterWrapper byMsgTypeIdFW = new FilterWrapper("by_msgtypeid");
        byMsgTypeIdFW.setParameter("msgtypeid", new Long(320));
        FilterWrapper byTerminalIdFW = new FilterWrapper("by_isoterminalid");
        byTerminalIdFW.setParameter("terminalid", endBatchTx.getTerminalId());
        FilterWrapper byMerchantIdFW = new FilterWrapper("by_merchantid");
        byMerchantIdFW.setParameter("merchantid", endBatchTx.getCardAcqId());
        FilterWrapper byBatchNumFW = new FilterWrapper("by_batchnum");
        byBatchNumFW.setParameter("batchnum", collation.getNumberOfBatch());
        FilterWrapper byCollationFW = new FilterWrapper("by_collation");
        byCollationFW.setParameter("collationid", collation.getCollationId());


        return new EntityManager().RETRIEVE_ALL(
                Iso8583Transaction.class,
                new FilterWrapper[]{byMsgTypeIdFW, byTerminalIdFW, byMerchantIdFW, byCollationFW});
    }

    private void calculateBillingCollation(Collation collation, List batchTransactions){
        long billingSalesCount = 0;
        long billingSalesAmount = 0;
        for(Iterator iter = batchTransactions.iterator(); iter.hasNext();){
            Iso8583Transaction batchTx = (Iso8583Transaction)iter.next();
            billingSalesCount++;
            billingSalesAmount = billingSalesAmount + (batchTx.getAmount() != null ? batchTx.getAmount().longValue() : 0);
        }
        collation.setBillingSalesAmountEndBatch(new Long(billingSalesAmount));
        collation.setBillingSalesCountEndBatch(new Long(billingSalesCount));
    }

    protected void calculateTerminalCollation(Collation collation, RuntimeMessage reqMsg){
        RuntimeSubDataElement rsde = reqMsg.getRuntimeDataElement(63).getSubstructureDataElement();
        String tmp = rsde.getValue(BasicCollationSDEF.SALES_COUNT);
        long terminalSalesCount = tmp != null ? Long.parseLong(tmp) : 0;
        tmp = rsde.getValue(BasicCollationSDEF.SALES_AMOUNT);
        long terminalSalesAmount = tmp != null ? Long.parseLong(tmp) : 0;

        collation.setTerminalSalesAmountEndBatch(new Long(terminalSalesAmount));
        collation.setTerminalSalesCountEndBatch(new Long(terminalSalesCount));
    }


}
