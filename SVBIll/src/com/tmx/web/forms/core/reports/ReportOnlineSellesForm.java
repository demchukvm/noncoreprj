package com.tmx.web.forms.core.reports;

import com.tmx.as.blogic.SellerManager;
import com.tmx.as.base.QueryParameterWrapper;
import com.tmx.as.exceptions.DatabaseException;
import com.tmx.as.entities.bill.transaction.ClientTransaction;
import com.tmx.web.controls.grid.GridException;

public class ReportOnlineSellesForm extends ReportSellesForm {

    protected void init() {
        super.init();
    }

    protected void bindGrid(SellerManager sellerManager, QueryParameterWrapper[] params) throws GridException, DatabaseException {
        super.getGrid().bind(sellerManager.EXECUTE_QUERY(ClientTransaction.class,"retrieveOnlineSelles",params));
    }
}
