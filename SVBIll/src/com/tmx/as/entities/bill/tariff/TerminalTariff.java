package com.tmx.as.entities.bill.tariff;

import com.tmx.as.entities.bill.balance.TerminalBalance;
import com.tmx.as.entities.bill.terminal.Terminal;

/**

 */
public class TerminalTariff extends AbstractTariff{
    private Terminal terminal;
    private Long terminalId;

    private Long terminalBalanceId;
    private TerminalBalance terminalBalance;

    //--------Properties

    public Terminal getTerminal() {
        return terminal;
    }

    public void setTerminal(Terminal terminal) {
        this.terminal = terminal;
    }

    public Long getTerminalId() {
        return terminalId;
    }

    public void setTerminalId(Long terminalId) {
        this.terminalId = terminalId;
    }

    public Long getTerminalBalanceId() {
        return terminalBalanceId;
    }

    public void setTerminalBalanceId(Long terminalBalanceId) {
        this.terminalBalanceId = terminalBalanceId;
    }

    public TerminalBalance getTerminalBalance() {
        return terminalBalance;
    }

    public void setTerminalBalance(TerminalBalance terminalBalance) {
        this.terminalBalance = terminalBalance;
    }
}
