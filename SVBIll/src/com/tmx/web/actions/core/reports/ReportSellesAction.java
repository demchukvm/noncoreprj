package com.tmx.web.actions.core.reports;

import com.tmx.web.base.BasicDispatchedAction;
import com.tmx.web.base.SecurityService;
import com.tmx.web.base.ApplicationEnvironment;
import com.tmx.web.forms.core.reports.ReportSellesForm;
import com.tmx.as.blogic.SellerManager;
import com.tmx.as.base.EntityManager;
import com.tmx.as.base.criterion.CriterionWrapper;
import com.tmx.as.base.criterion.Restrictions;
import com.tmx.as.entities.general.user.User;
import com.tmx.as.entities.bill.transaction.OperationType;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForm;
import org.apache.xmlbeans.impl.xb.xsdschema.RestrictionDocument;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.GregorianCalendar;
import java.util.Calendar;

public class ReportSellesAction extends BasicDispatchedAction {
    private static final String FORWARD = "page";
    public ActionForward defaultAction(ActionMapping mapping,
                                       ActionForm form,
                                       HttpServletRequest request,
                                       HttpServletResponse response,
                                       String methodName) throws Throwable {
        ReportSellesForm reportSellesForm = (ReportSellesForm) verifyForm(form, ReportSellesForm.class);

        setUpTimeSelectors(reportSellesForm);

        CriterionWrapper[] wrappers = new CriterionWrapper[]{
                Restrictions.or(Restrictions.eq("name", "refillPayment"), Restrictions.eq("name", "buyVoucher")) 
        };
        reportSellesForm.getMsbOperationType().bind(new EntityManager().RETRIEVE_ALL(OperationType.class, wrappers, null));
        reportSellesForm.getMsbOperationType().selectAll();
        reportSellesForm.refresh(new SellerManager(), getLoggedInUser(request));
        return mapping.findForward(FORWARD);
    }

    private void setUpTimeSelectors(ReportSellesForm reportSellesForm) {
        GregorianCalendar calendarFrom = new GregorianCalendar();
        GregorianCalendar calendarTo = (GregorianCalendar)calendarFrom.clone();

        reportSellesForm.getTo().setTime(calendarTo.getTime());

        calendarFrom.set(Calendar.DAY_OF_MONTH, 1);
        calendarFrom.set(Calendar.HOUR_OF_DAY, 0);
        calendarFrom.set(Calendar.MINUTE, 0);
        reportSellesForm.getFrom().setTime(calendarFrom.getTime());

    }

    public ActionForward refreshGrid(ActionMapping mapping,
                                  ActionForm form,
                                  HttpServletRequest request,
                                  HttpServletResponse response) throws Throwable {

        ReportSellesForm reportSellesForm = (ReportSellesForm) verifyForm(form, ReportSellesForm.class);
        reportSellesForm.refresh(new SellerManager(),getLoggedInUser(request));
        return mapping.findForward(FORWARD);        
    }

    private User getLoggedInUser(HttpServletRequest request) throws com.tmx.as.exceptions.SecurityException {
        SecurityService securityService = (SecurityService) ApplicationEnvironment.getService(SecurityService.class);
        return securityService.getLoggedInUser(request.getSession());
    }
}
