package com.tmx.web.controls.grid;

import com.tmx.web.controls.BasicControl;

import java.util.List;

/**
   Grid web control
   <p/>
   Usage of Grid:
   <img src="doc-files/grid_structure_cd_1_1.png">
*/
abstract public class Grid extends BasicControl {
    abstract public void bind(List data) throws GridException;
    public void initialize() {
    }
    private Matrix matrix=new Matrix();
    public void clear(){
        matrix=new Matrix();
    }
    public void setCell(int row, int col, Cell cell) throws GridException {
        if(cell instanceof CellLink && matrix.get(row,col)!=null) throw new GridException("CellLink can't be set to not null cell ("+row+":"+col+")");
        deleteCell(row,col);
        matrix.set(row,col,cell);
        if(cell instanceof CellLink) return;
        for (int i = 1; i < cell.getColSpan(); i++) {
            setCell(row,col+i,new CellLink(cell));
            for (int j = 1; j < cell.getRowSpan(); j++) {
                setCell(row+j,col+i,new CellLink(cell));
            }
        }
        for (int i = 1; i < cell.getRowSpan(); i++) {
            setCell(row+i,col,new CellLink(cell));
        }
    }
    public void setCell(int row, int col, String value, String styleClass, String style) throws GridException {
        setCell(row,col,new SimpleCell(value,styleClass,style));
    }
    public void setCell(int row, int col, String value, String styleClass) throws GridException {
        setCell(row,col,new SimpleCell(value,styleClass,null));
    }
    public void setCell(int row, int col, String value) throws GridException {
        setCell(row,col,new SimpleCell(value,null,null));
    }
    public void setCell(int row, int col, String value, int colSpan, int rowSpan) throws GridException {
        setCell(row,col,new SimpleCell(value,null,null,colSpan,rowSpan));
    }
    public void setCell(int row, int col, String value, String styleClass, String style, int colSpan, int rowSpan) throws GridException {
        setCell(row,col,new SimpleCell(value,styleClass,style,colSpan,rowSpan));
    }
    public void setCell(int row, int col, String value, String styleClass, int colSpan, int rowSpan) throws GridException {
        setCell(row,col,new SimpleCell(value,styleClass,null,colSpan,rowSpan));
    }
    public void setCell(int row, int col, int colSpan, int rowSpan) throws GridException {
        setCell(row,col,new SimpleCell(colSpan,rowSpan));
    }
    public void deleteCell(int row, int col) throws GridException {
        if(matrix.get(row,col) instanceof CellLink) throw new GridException("cell ("+row+":"+col+") is containing the CellLink");
        Cell cell=(Cell)matrix.get(row,col);
        if(cell==null)return;
        for (int i = 1; i < cell.getColSpan(); i++) {
            matrix.delete(row,col+i);
            for (int j = 1; j < cell.getRowSpan(); j++) {
                matrix.delete(row+j,col+i);
            }
        }
        for (int i = 1; i < cell.getRowSpan(); i++) {
            matrix.delete(row+i,col);
        }
        matrix.delete(row,col);
    }
    public Cell[][] getCells(){
        List rowList=matrix.toList();
        Cell[][] cells=new Cell[rowList.size()][]; 
        for (int i = 0; i < rowList.size(); i++) {
            List colList = (List)rowList.get(i);
            if(colList==null)continue;
            Cell[] colArr=new Cell[colList.size()]; 
            colList.toArray(colArr);
            cells[i]=colArr;
        }
        return cells;
    }
}
