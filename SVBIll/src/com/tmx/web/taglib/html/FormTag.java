package com.tmx.web.taglib.html;

import org.apache.struts.taglib.TagUtils;
import org.apache.struts.taglib.nested.NestedPropertyHelper;
import org.apache.struts.taglib.html.Constants;
import org.apache.xerces.impl.dv.util.Base64;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.PageContext;
import javax.servlet.http.HttpServletRequest;

import com.tmx.as.entities.general.ui_permission.PermissionType;
import com.tmx.as.entities.general.ui_permission.UserInterfaceType;
import com.tmx.as.exceptions.DatabaseException;
import com.tmx.web.base.BasicActionForm;
import com.tmx.web.base.SecurityService;
import com.tmx.web.base.ApplicationEnvironment;
import com.tmx.web.taglib.html.filter_ctrl.PermissionFormTagInterface;

import java.util.List;

/**
 * Form tag. Builds HTML form.
 * Extends Struts form tag
 */
public class FormTag extends org.apache.struts.taglib.html.FormTag{

     public int doStartTag() throws JspException {
        super.doStartTag();
        // set NestedReference name
        String beanName=getBeanName();
        if(beanName!=null)
        NestedPropertyHelper.setName((HttpServletRequest)pageContext.getRequest(),getBeanName());
        NestedPropertyHelper.setProperty((HttpServletRequest)pageContext.getRequest(),"");
        TagUtils.getInstance().write(pageContext, "\r\n<!-- permission form name: "+getBeanName()+" -->\r\n");
        String permission=getPermission(getBeanName());
        if(permission.equals(PermissionType.READONLY)){
            int scope = PageContext.SESSION_SCOPE;
            if ("request".equalsIgnoreCase(beanScope)) {
                scope = PageContext.REQUEST_SCOPE;
            }
            Object bean = pageContext.getAttribute(beanName, scope);
            if(bean instanceof BasicActionForm){
                BasicActionForm basicActionForm=(BasicActionForm)bean;
                basicActionForm.setOwnedControlsReadonly(true);
            }
        }
        if(getPermission(getBeanName()).equals(PermissionType.FORBIDDEN))
            return SKIP_BODY;
        TagUtils.getInstance().write(pageContext, getPermissionSetupRenderer(getBeanName()));
        return EVAL_BODY_INCLUDE;
    }

    private String getPermission(String formPath) {
        String permission = PermissionType.FORBIDDEN;
        String roleName = ((SecurityService) ApplicationEnvironment.getService(SecurityService.class)).getLoggedInUserRoleName(((HttpServletRequest)pageContext.getRequest()).getSession());
        com.tmx.as.base.SecurityService securityService = com.tmx.as.base.SecurityService.getInstance();
        try{
            permission = securityService.getPermissionType(roleName, UserInterfaceType.FORM, formPath);
        }
        catch(DatabaseException e){
            /** Ignore with FORBIDDEN permission */
        }
        catch(com.tmx.as.exceptions.SecurityException e){
            /** Ignore with FORBIDDEN permission */
        }
        return permission;
    }

    private String getPermissionSetupRenderer(String beanName) throws JspException {
        PermissionFormTagInterface permissionFormTagInterface=(PermissionFormTagInterface)((HttpServletRequest)pageContext.getRequest()).getSession().getAttribute("core.uipermsetup.toolBarPermissionForm");
        if(permissionFormTagInterface!=null&&permissionFormTagInterface.isActive()){
            StringBuffer pControl= new StringBuffer("<select class=\"permissionControlSelectBox dropdownbox\" name=\"applyPermissionsForm(")
                    .append(Base64.encode(beanName.getBytes())).append(")\">\n");
            List permissionTypes=permissionFormTagInterface.getPermissionTypes();
            Long selected=permissionFormTagInterface.getFormPermission(beanName);
            for (int i = 0; i < permissionTypes.size(); i++) {
                Object[] bean=(Object[])permissionTypes.get(i);
                pControl.append("<option value=\"").append(bean[0]).append("\"")
                        .append(selected.equals(bean[0]) ? "selected" : "")
                        .append(">")
                        .append(bean[1])
                        .append("</option>\n");
            }
            pControl.append("</select>");
            return pControl.toString();
        } else return "";
    }

}
