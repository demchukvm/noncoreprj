package com.tmx.web.actions.core.auth;

import com.tmx.web.base.BasicAction;
import com.tmx.web.base.SessionEnvironment;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


/**
 * Implements the logout function.
 */
public class LogoutAction extends BasicAction {

    public ActionForward executeSpecificAction(ActionMapping mapping,
                                               ActionForm form,
                                               HttpServletRequest request,
                                               HttpServletResponse response) throws Exception {

        HttpSession session = request.getSession();
        session.invalidate();
        return mapping.findForward("login");
    }
}
