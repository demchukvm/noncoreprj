package com.tmx.web.actions.core.dictionary.terminal;

import com.tmx.as.entities.general.user.User;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForm;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.tmx.web.forms.core.dictionary.terminal.ManageTerminalsForm;
import com.tmx.web.forms.core.dictionary.seller.ManageSellersForm;
import com.tmx.web.forms.core.base.AbstractFormFactory;
import com.tmx.web.actions.core.AbstractSvbillAction;
import com.tmx.web.base.Roadmap;


public class ManageSubSellersTerminalsAction extends AbstractSvbillAction {

     private static final String FORWARD_MANAGE_TERMINALS_PAGE = "manageSubSellersTerminalsPage";

    public ActionForward defaultAction(ActionMapping mapping,
                                       ActionForm form,
                                       HttpServletRequest request,
                                       HttpServletResponse response,
                                       String methodName) throws Throwable {
        ManageTerminalsForm manageTerminalsForm = (ManageTerminalsForm) form;
        AbstractFormFactory.getInstance(getLoggedInUser(request)).setUpForm(manageTerminalsForm);

        manageTerminalsForm.getTerminalsTable().setRefreshActionName("/core/dictionary/terminal/manageSubSellersTerminals");
        manageTerminalsForm.getTerminalsTable().setRefreshCommandName("refreshTable");

        getRoadmap().collectParameter("/core/dictionary/terminal/infoTerminal",
                                                        "cancelActionFrominfoTerminalForm",
                                                        "/core/dictionary/terminal/manageSubSellersTerminals");
        getRoadmap().collectParameter("/core/dictionary/terminal/infoTerminal",
                                                        "cancelActionOnDelete",
                                                        "manageSubSellersTerminalsAction");
//        manageTerminalsForm.getTerminalsTable().initialize();
        manageTerminalsForm.getTerminalsTable().refresh();
        return mapping.findForward(FORWARD_MANAGE_TERMINALS_PAGE);
    }

    public ActionForward refreshTable(ActionMapping mapping,
                                  ActionForm form,
                                  HttpServletRequest request,
                                  HttpServletResponse response) throws Throwable {
        ManageTerminalsForm manageTerminalsForm = (ManageTerminalsForm) form;
        manageTerminalsForm.getTerminalsTable().refresh();

        return mapping.findForward(FORWARD_MANAGE_TERMINALS_PAGE);
    }
}
