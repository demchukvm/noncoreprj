-- SQL Loader Control and Data File created by TOAD
-- Variable length, terminated enclosed data formatting
-- 
-- The format for executing this file with SQL Loader is:
-- SQLLDR control=<filename> Be sure to substitute your
-- version of SQL LOADER and the filename for this file.
LOAD DATA
INFILE *
BADFILE 'D:\SVBILL\src\com\tmx\db\general\data\UP_PERMISSION.BAD'
DISCARDFILE 'D:\SVBILL\src\com\tmx\db\general\data\UP_PERMISSION.DSC'
REPLACE INTO TABLE UP_PERMISSION
Fields terminated by ";" Optionally enclosed by '"'
(
  UP_PERMISSIONID NULLIF (UP_PERMISSIONID="NULL"),
  UI_USERINTERFACEID NULLIF (UI_USERINTERFACEID="NULL"),
  PT_PERMISSIONTYPEID NULLIF (PT_PERMISSIONTYPEID="NULL"),
  RL_ROLEID NULLIF (RL_ROLEID="NULL"),
  UP_VALID NULLIF (UP_VALID="NULL")
)
BEGINDATA
3;10;2;4;1
1;308;2;4;1
2;312;2;4;1
4;310;2;4;1
5;11;2;4;1
6;12;2;4;1
7;13;2;4;1
8;14;2;4;1
9;15;2;4;1
11;309;3;4;1
10;16;2;4;1
12;313;3;4;1
13;314;3;4;1
17;315;2;4;1
14;316;2;4;1
20;14;2;4;1
15;317;3;4;1
21;318;3;4;1
22;319;3;4;1
23;320;2;4;1
24;321;2;4;1
