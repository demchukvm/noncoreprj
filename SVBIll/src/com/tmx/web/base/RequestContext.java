package com.tmx.web.base;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
Pair of request and response
 */
public class RequestContext {
    private HttpServletRequest request = null;
    private HttpServletResponse response = null;
    private long creationTime = -1;
    /** Request will be removed after EXPIRATION_TIME from creation time.
     * This could be couse of NullPointerException then you call RequestContext.currentRequest() */
    private final long EXPIRATION_TIME = 600000; //10 min.

    public RequestContext(HttpServletRequest request, HttpServletResponse response){
        this.request = request;
        this.response = response;
        this.creationTime = System.currentTimeMillis();
    }

    public HttpServletRequest getRequest() {
        return request;
    }

    public HttpServletResponse getResponse() {
        return response;
    }

    public long getCreationTime() {
        return creationTime;
    }

    /** Returns true if both RequestContext belong to the same session */
    public boolean isBelongToSameSession(RequestContext requestContext){
        if(requestContext == null)
            return false;
        if(requestContext.getRequest().getSession() != null && getRequest().getSession(true) != null)
            return requestContext.getRequest().getSession().getId().equals(getRequest().getSession(true).getId());
        else
            return false;
    }

    public boolean isBelongToValidSession(){
        return getRequest().getSession() != null;
    }

    public boolean isExpired(){
        return (EXPIRATION_TIME < System.currentTimeMillis() - creationTime);
    }
}
