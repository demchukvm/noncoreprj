package com.tmx.beng.iso8583Gate.processors;

import com.tmx.beng.iso8583Gate.pdu.RuntimeMessage;
import com.tmx.beng.iso8583Gate.pdu.IncorrectFormatException;
import com.tmx.beng.iso8583Gate.mapper.BillingMapper;
import com.tmx.beng.iso8583Gate.mapper.BillingMapperMetadata;
import com.tmx.beng.base.BillingMessage;
import com.tmx.beng.base.BillingMessageImpl;
import com.tmx.beng.base.BillException;
import com.tmx.beng.base.BillingMessageWritable;
import com.tmx.util.InitException;
import com.tmx.as.entities.iso8583.transaction.Iso8583Transaction;
import com.tmx.as.base.EntityManager;
import com.tmx.as.exceptions.DatabaseException;

/**
 */
public class SellServiceProcessor extends BasicProcessor{

    public int getMessageTypeId() {
        return 210;
    }

    public int getProcessingCode() {
        return 0;
    }

    public int[] getDEToCopy() {
        return new int[]{4, 11, 24, 41};
    }

    public void processSpecificTask(RuntimeMessage reqMsg, RuntimeMessage respMsg)
            throws ProcessingException, BillException, IncorrectFormatException, InitException, DatabaseException {
        
        logger.debug("Sale");
        BillingMapper billingMapper = BillingMapperMetadata.getInstance().getBillMapper(reqMsg);

        //prepare and pre-save iso transaction in iso datasource
        Iso8583Transaction tx = billingMapper.preSaveTransaction(reqMsg);
        billingMapper.assignRetrievalRefNum(tx);
        billingMapper.assignAuthIdResponse(tx);

        //prepare billing message and send it to process into billing engine
        BillingMessageWritable reqBM = new BillingMessageImpl();
        billingMapper.prepare0200Msg(reqMsg, reqBM);

        BillingMessage respBM = getConnection().processSync(reqBM);

        //post-save iso transaction in iso datasource
        billingMapper.postSaveTransaction(tx, respBM);
        //prepare iso response
        billingMapper.prepare0210Msg(respMsg, respBM, tx);
    }
}
