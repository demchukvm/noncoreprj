package com.tmx.web.actions.core.dictionary.territory;

import com.tmx.web.base.BasicDispatchedAction;
import com.tmx.web.forms.core.dictionary.territory.ManageCitiesForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForm;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class ManageCitiesAction extends BasicDispatchedAction {
    private static final String FORWARD_MANAGE_ALARM_LEVELS_PAGE = "managePage";

    public ActionForward defaultAction(ActionMapping mapping,
                                       ActionForm form,
                                       HttpServletRequest request,
                                       HttpServletResponse response,
                                       String methodName) throws Throwable {
        ManageCitiesForm manageCitiesForm = (ManageCitiesForm) verifyForm(form, ManageCitiesForm.class);
        manageCitiesForm.getTable().setRefreshActionName(this.getName());
        manageCitiesForm.getTable().refresh();
        return mapping.findForward(FORWARD_MANAGE_ALARM_LEVELS_PAGE);
    }
}
