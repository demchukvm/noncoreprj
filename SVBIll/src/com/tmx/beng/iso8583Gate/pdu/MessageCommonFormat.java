package com.tmx.beng.iso8583Gate.pdu;

import com.tmx.util.InitException;
import com.tmx.beng.iso8583Gate.pdu.commonmsgformat.beans.DataElement;
import com.tmx.beng.iso8583Gate.pdu.commonmsgformat.beans.Iso8583CommonMsgFormatDocument;
import com.tmx.beng.iso8583Gate.ISO8583Service;

import java.io.FileInputStream;
import java.io.File;
import java.io.IOException;
import java.util.*;

import org.apache.xmlbeans.XmlOptions;
import org.apache.log4j.Logger;

/**
    Message Common Format (protocol vaersion related)
 */
public class MessageCommonFormat {
    private Iso8583CommonMsgFormatDocument commonMsgFormatDoc;
    private Map dataElementsFormatsByBitmapNum;
    private Map dataElementsFormatsByName;
    private Logger logger = ISO8583Service.getLogger(this.getClass());
    private String protocolVersion;

    public void load(String path, String messageFormat, String protocolVersion) throws InitException {
        this.protocolVersion = protocolVersion;
        
        Iso8583CommonMsgFormatDocument localMessageFormatDocument;
        FileInputStream fis = null;
        final String messagePath = path + File.separator + messageFormat;
        try {

            fis = new FileInputStream(messagePath);
            // Bind the instance to the generated XMLBeans formats.
            localMessageFormatDocument = Iso8583CommonMsgFormatDocument.Factory.parse(fis);
        }
        catch (Throwable e) {
            throw new InitException("err.iso8583commonmsgformat.failed_to_parse_template", new String[]{messagePath}, e);
        }
        finally {
            try {
                if (fis != null)
                    fis.close();
            }
            catch (IOException e) {/** ignore */}
        }

        //validate
        ArrayList errors = new ArrayList();
        XmlOptions opts = new XmlOptions();
        opts.setErrorListener(errors);
        if (!localMessageFormatDocument.validate(opts)) {
            StringBuffer errorBuffer = new StringBuffer("Validation failed of XML " + messagePath + ":\n");
            Iterator iter = errors.iterator();
            while (iter.hasNext())
                errorBuffer.append("   >> " + iter.next() + "\n");
            logger.error(errorBuffer.toString());
            throw new InitException("err.iso8583commonmsgformat.template_validation_failed", new String[]{messagePath});
        }

        Map localDataElsFormatsByBitmapNum = new HashMap();
        Map localDataElsFormatsByName = new HashMap();
        DataElement[] dataElement = localMessageFormatDocument.getIso8583CommonMsgFormat().getApplicationData().getDataElementArray();
        for (int i = 0; i < dataElement.length; i++){
            DataElementFormat def = new DataElementFormat(
                dataElement[i].getBitmapNumber(),
                dataElement[i].getName(),
                dataElement[i].getType(),
                dataElement[i].getLengthType(),
                dataElement[i].getLength(),
                dataElement[i].getMaxLength(),
                dataElement[i].getExtendedFormat()
            );

            localDataElsFormatsByBitmapNum.put(String.valueOf(def.getBitmapNumber()), def);
            localDataElsFormatsByName.put(def.getName(), def);
        }


        //assign to properties
        commonMsgFormatDoc = localMessageFormatDocument;
        dataElementsFormatsByBitmapNum = localDataElsFormatsByBitmapNum;
        dataElementsFormatsByName = localDataElsFormatsByName;
    }

    public DataElementFormat getDataElementFormat(int bitmapNumber){
        return (DataElementFormat) dataElementsFormatsByBitmapNum.get(String.valueOf(bitmapNumber));
    }

    public DataElementFormat getDataElementFormat(String dataElementName){
        return (DataElementFormat) dataElementsFormatsByName.get(dataElementName);
    }

    /** Returns ordered by bitmap list of DataElements */
    public List getDataElementFormats(){
        List orderedDataElements = new ArrayList();
        int bitmapNum = 0;
        while(orderedDataElements.size() < dataElementsFormatsByBitmapNum.size() && bitmapNum < 1000/** to prevent infinity loop */){
            DataElementFormat def = getDataElementFormat(bitmapNum);
            if(def != null){
                orderedDataElements.add(def);
            }
            bitmapNum++;
        }
        return orderedDataElements;
    }


    public String getProtocolVersion() {
        return protocolVersion;
    }
}
