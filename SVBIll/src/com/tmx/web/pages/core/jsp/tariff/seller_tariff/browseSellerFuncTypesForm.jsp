<%@ page language="java" pageEncoding="UTF-8" contentType="text/html; charset=utf-8" isELIgnored="false" %>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-nested" prefix="nested" %>
<%@ taglib uri="http://tmx.com/taglib/tmx-controls" prefix="tmx_ctrl" %>
<%@ taglib uri="http://tmx.com/taglib/tmx-logic" prefix="tmx_logic" %>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html" %>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean" %>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>

<html:form action="/core/tariff/seller_tariff/browseSellerFuncTypesAction">
<tmx_ctrl:browseCallBack/>
<table width="100%" border="0">
    <tr>
        <td colspan="2">
            <h4 class="dataLabel">
                <bean:message bundle="core.labels" key="core.balance.sellertariff.infosellertariffform.titleFunction"/>
            </h4>
        </td>
    </tr>
    <tr>
        <td width="100%">
            <table class='one_outer_cell_table' cellspacing='0' cellpadding='0' width="100%">
                <tr>
                    <td>
                        <table cellspacing='1' cellpadding='1' width="100%">
                            <!-- Table header -->
                            <tr>
                                <th class="column_header_cell" width="110">
                                    <tmx_ctrl:imgTableOrdering tableName="browseTable"
                                                               orderByFieldName="name"
                                                               labelBundle="as:com.tmx.as.entities.bill.function.FunctionType"
                                                               labelKey="name"/>
                                </th>

                                <th class="column_header_cell" width="110">
                                    <tmx_ctrl:imgTableOrdering tableName="browseTable"
                                                               orderByFieldName="implClass"
                                                               labelBundle="as:com.tmx.as.entities.bill.function.FunctionType"
                                                               labelKey="implClass"/>
                                </th>

                                <th class="column_header_cell" width="110">
                                    <tmx_ctrl:imgTableOrdering tableName="browseTable"
                                                               orderByFieldName="implMethod"
                                                               labelBundle="as:com.tmx.as.entities.bill.function.FunctionType"
                                                               labelKey="implMethod"/>
                                </th>

                                <th class="column_header_cell" width="110">
                                    <tmx_ctrl:imgTableOrdering tableName="browseTable"
                                                               orderByFieldName="description"
                                                               labelBundle="as:com.tmx.as.entities.bill.function.FunctionType"
                                                               labelKey="description"/>
                                </th>
                            </tr>
                            <nested:iterate id="functionType" property="browseTable.data" type="com.tmx.as.entities.bill.function.FunctionType">
                                <tr>
                                    <td class='column_data_cell'>
                                        <tmx_ctrl:browsePick pickKey="${functionType.functionTypeId}" pickValue="${functionType.name}"/>
                                    </td>
                                    <td class='column_data_cell'>
                                        <c:out value="${functionType.implClass}"/>
                                    </td>
                                    <td class='column_data_cell'>
                                        <c:out value="${functionType.implMethod}"/>
                                    </td>
                                    <td class='column_data_cell'>
                                        <c:out value="${functionType.description}"/>
                                    </td>
                                </tr>
                            </nested:iterate>
                            <!-- Empty row -->
                            <nested:define id="tableSize" property="browseTable.resultSize" type="java.lang.Integer"/>
                            <c:if test="${tableSize <= 0}">
                                <tr>
                                    <td class='column_data_cell' colspan="5" align="center">
                                        <bean:message bundle="core.labels" key="core.common.th.empty_table" />
                                    </td>
                                </tr>
                            </c:if>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td style="text-align: right">
            <tmx_ctrl:imgTablePaging maxBookmarkCount="10" tableName="browseTable"/>
        </td>
    </tr>

</table>
</html:form>