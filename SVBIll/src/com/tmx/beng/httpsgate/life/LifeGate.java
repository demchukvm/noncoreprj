package com.tmx.beng.httpsgate.life;

import com.tmx.beng.httpsgate.HttpsGateException;
import com.tmx.util.InitException;
import org.apache.log4j.Logger;

/**
 */
public class LifeGate extends BasicLifeGate{
    protected static final String GATE_NAME = "life_gate";
    protected static Logger logger = Logger.getLogger(GATE_NAME);
    private static LifeGate gate = null;

    public static LifeGate getInstance() throws HttpsGateException {
        if(gate == null){
            logger.error("Attempt to obtain not inited billing engine");
            throw new HttpsGateException("err.LifeGate.null_instance");
        }
        return gate;
    }

    public static void init() throws InitException {
        logger.info("Start LifeGate initialization");
        gate = new LifeGate();
        gate.init0();
        logger.info("LifeGate initialized");
    }


    public static void shutdown(final long timeout) throws InitException {
        logger.info("Start LifeGate Shutdown");
        gate.shutdown0(timeout);
        logger.info("LifeGate shutdown");
    }

    protected String getGateName(){
        return GATE_NAME;
    }

}
