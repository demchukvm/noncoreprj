package com.tmx.as.resolver;

import com.tmx.as.resolver.functions.Function;

import java.util.Vector;
import java.util.Properties;
import java.util.Hashtable;
import java.util.Enumeration;
import java.io.IOException;

/**
 * Execute our query string to load list of data
 * <p/>
 * QUERY FORMAT EXAMPLE:
 * <p/>
 * [entity_class=com.tmx.as.performer.Performer;
 * order_asc=name;
 * order_desc=date;
 * page_number=2;
 * page_size=20]
 */
public class FunctionProcessor {
    private String EQUATION = "=";
    private String PARAM_DELIMITER = ",";
    private String START_BRACKET = "(";
    private String END_BRACKET = ")";
    
    // buffered values
    private static Hashtable functionClasses;

    public FunctionProcessor() throws FunctionSyntaxException{
        if(functionClasses == null){
            Properties properties = new Properties();
            try {
                properties.load(FunctionProcessor.class.getResourceAsStream("functions/functions.properties"));
            } catch (IOException e) {
                throw new FunctionSyntaxException("can't load 'functions/functions.properties'", e);
            }
            functionClasses = new Hashtable();
            Enumeration keys=properties.propertyNames();
            while (keys.hasMoreElements()) {
                String key = (String)keys.nextElement();
                try {
                    functionClasses.put(key,Class.forName(properties.getProperty(key)));
                } catch (ClassNotFoundException e) {
                    throw new FunctionSyntaxException("can't load property '" + key + "' from 'functions/functions.properties'",e);
                }
            }
        }
    }

    public Query parseQueryText(String text) throws FunctionSyntaxException{
        try {
            StringBuffer functionName = new StringBuffer();
            undressFunction(text, functionName, new StringBuffer());
            if(!functionName.toString().equalsIgnoreCase("query")) throw new Exception("root function isn't 'query'");
            return (Query)parseFunction(text);
        } catch (Throwable throwable) {
            throw new FunctionSyntaxException("can't parse query",throwable);
        }
    }

    private Object parseFunction(String functionText) throws FunctionSyntaxException{
        //cut functionText name and brackets
        StringBuffer functionName = new StringBuffer();
        StringBuffer functionBody = new StringBuffer();

        undressFunction(functionText, functionName, functionBody);

        Function function = getFunction(functionName);

        //parse functionText body
        String[] params = splitLevel(functionBody.toString());
        for (int i = 0; i < params.length; i++) {
            if(isParamValuePair(params[i])){
                StringBuffer paramName = new StringBuffer();
                StringBuffer paramValue = new StringBuffer();
                parseParamPair(params[i],paramName,paramValue);
                function.set(i,paramName.toString(),paramValue.toString());
            }else if(isSingleValue(params[i])){
                function.set(i,"",params[i]);
            }else if(isParamFunctionPair(params[i])){
                StringBuffer paramName = new StringBuffer();
                StringBuffer innerFunctionText = new StringBuffer();
                parseParamPair(params[i],paramName,innerFunctionText);
                function.set(i,paramName.toString(),parseFunction(innerFunctionText.toString()));
            }else if(isSingleFunction(params[i])){
                undressFunction(functionText, functionName, functionBody);
                StringBuffer innerFunctionName = new StringBuffer();
                undressFunction(params[i], innerFunctionName, new StringBuffer());
                function.set(i,innerFunctionName.toString(),parseFunction(params[i]));
            }else if (params[i].length() == 0){
                throw new FunctionSyntaxException("err.function_processor.empty_lexem_found", new String[]{functionName.toString(), String.valueOf(i)});
            }else{
                throw new FunctionSyntaxException("err.function_processor.neither_function_nor_atomic_parameter_found", new String[]{functionName.toString(), String.valueOf(i), params[i]});
            }
        }
        return function.returnValue();
    }

    private Function getFunction(StringBuffer functionName) throws FunctionSyntaxException {
        Class functionClass = (Class) functionClasses.get( functionName.toString().trim() );
        if( functionClass == null )
            throw new FunctionSyntaxException("undefined lexeme - " + functionName);

        try {
            return (Function)functionClass.newInstance();
        } catch (InstantiationException e) {
            throw new FunctionSyntaxException("error", e);
        } catch (IllegalAccessException e) {
            throw new FunctionSyntaxException("error", e);
        }
    }

    private void parseParamPair(String paramPair, StringBuffer param, StringBuffer value) throws FunctionSyntaxException{
        try{
            String[] pair = paramPair.split(EQUATION,2);
            param = (param == null) ? new StringBuffer() : param;
            param.append(pair[0].trim());
            value = (value == null) ? new StringBuffer() : value;
            //for (int i = 1; i < pair.length; i++) {
            value.append(pair[1].trim());
            //}
        }
        catch(Exception e){
            throw new FunctionSyntaxException("err.function_processor.failed_to_parse_parameter_pair", new String[]{paramPair}, e);
        }
    }

    private void undressFunction(String function, StringBuffer functionName, StringBuffer functionBody) throws FunctionSyntaxException{
        function = function.trim();
        if(!function.endsWith(END_BRACKET))
            throw new FunctionSyntaxException("err.function_processor.function_not_terminated_by_end_bracket", new String[]{function, END_BRACKET});

        int startBracketIdx = function.indexOf(START_BRACKET);
        if(startBracketIdx < 0)
            throw new FunctionSyntaxException("err.function_processor.function_have_not_start_bracket", new String[]{function, START_BRACKET});

        functionName = (functionName == null) ? new StringBuffer() : functionName;
        functionName.append(function.substring(0, startBracketIdx));

        functionBody = (functionBody == null) ? new StringBuffer() : functionBody;
        functionBody.append(function.substring(startBracketIdx + 1, function.length() - 1));
    }

    private boolean isParamValuePair(String parameter){
        if(parameter.indexOf(EQUATION) > 0 &&
           parameter.indexOf(START_BRACKET) < 0 &&
           parameter.indexOf(END_BRACKET) < 0)
            return true;
        else
            return false;
    }

    private boolean isSingleValue(String parameter){
        if(parameter.indexOf(EQUATION) < 0 &&
           parameter.indexOf(START_BRACKET) < 0 &&
           parameter.indexOf(END_BRACKET) < 0)
            return true;
        else
            return false;
    }

    private boolean isSingleFunction(String parameter){
        int equationIndex=parameter.indexOf(EQUATION);
        int startBracketIndex=parameter.indexOf(START_BRACKET);
        if(startBracketIndex>0&&(equationIndex>startBracketIndex||equationIndex<0)&&
           parameter.indexOf(END_BRACKET) > 0)
            return true;
        else
            return false;
    }

    private boolean isParamFunctionPair(String parameter){
        int equationIndex=parameter.indexOf(EQUATION);
        int startBracketIndex=parameter.indexOf(START_BRACKET);
        if( equationIndex>0&&startBracketIndex>0&&equationIndex<startBracketIndex&&
           parameter.indexOf(END_BRACKET) > 0)
            return true;
        else
            return false;
    }

    private String[] splitLevel(String functionBody){
        int level = 0;
        Vector lexemes = new Vector();
        String currentLexeme = new String();
        String currentSymbol = null;
        for(int i=0; i<functionBody.length(); i++){
            currentSymbol = functionBody.substring(i,i+1);

            if(currentSymbol.equals(PARAM_DELIMITER)){
                if(level == 0){
                    //collect current lexeme
                    lexemes.add(currentLexeme);
                    currentLexeme = new String();//clear current lexeme
                    continue;
                }
                //else ignore collecting
            }

            if(currentSymbol.equals(START_BRACKET)){
                level++;
            }

            if(currentSymbol.equals(END_BRACKET)){
                level--;
            }

            //add curent symbol to the current lexeme
            currentLexeme = currentLexeme + currentSymbol;
        }
        lexemes.add(currentLexeme);//collect last lexeme

        String[] params = new String[lexemes.size()];
        for(int j=0; j<params.length; j++)
            params[j] = (String)lexemes.get(j);

        return params;
    }
}