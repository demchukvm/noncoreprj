package com.tmx.beng.base.cache;

import com.tmx.as.base.EntityManager;
import com.tmx.as.base.FilterWrapper;
import com.tmx.as.entities.bill.balance.SellerBalance;
import com.tmx.as.entities.bill.restriction.SellerRestriction;
import com.tmx.as.entities.bill.seller.Seller;
import com.tmx.as.entities.bill.tariff.SellerTariff;
import com.tmx.as.exceptions.DatabaseException;
import com.tmx.util.InitException;

import java.util.*;

/**
 */
class Sellers {
    Map sellerId_seller = new HashMap();
    Map sellerCode_seller = new HashMap();

    public Sellers(Processings processings)throws DatabaseException, InitException {
        EntityManager entityManager = new EntityManager();

        // load sellers
        Iterator it;
        SellerWrapper sellerWrapper;
        List list = entityManager.RETRIEVE_ALL(Seller.class, (FilterWrapper[])null, new String[]{"primaryBalance", "parentSeller"});
        for(it = list.iterator(); it.hasNext();){
            Seller seller = (Seller)it.next();
            sellerWrapper = new SellerWrapper(seller);
            sellerId_seller.put(seller.getSellerId(), sellerWrapper);
            sellerCode_seller.put(seller.getCode(), sellerWrapper);
        }

        //assign selers parents
        for(Iterator iter = sellerId_seller.values().iterator(); iter.hasNext();){
            sellerWrapper = (SellerWrapper)iter.next();
            Seller parentSeller = sellerWrapper.seller.getParentSeller();
            if(parentSeller != null){
                //look for parent
                for(Iterator subIter = sellerId_seller.values().iterator(); subIter.hasNext();){
                    SellerWrapper sellerWrapper2 = (SellerWrapper)subIter.next();
                    if(parentSeller.getSellerId().longValue() == sellerWrapper2.seller.getSellerId().longValue() &&
                       sellerWrapper.seller.getSellerId().longValue() != sellerWrapper2.seller.getSellerId().longValue()/** always true */){
                        sellerWrapper.seller.setParentSeller(sellerWrapper2.seller);
                        sellerWrapper.parentSellerWrapper = sellerWrapper2;
                    }
                }
            }
        }

        //assign sellers processings
        for(Iterator iter = sellerId_seller.values().iterator(); iter.hasNext();){
            sellerWrapper = (SellerWrapper)iter.next();
            Long processingId = getSellerProcessingId(sellerWrapper);
            if(processingId != null){
                ProcessingWrapper processingWrapper = (ProcessingWrapper)processings.processingId_processing.get(processingId);
                if(processingWrapper != null)
                    processingWrapper.sellerId_seller.put(sellerWrapper.seller.getSellerId(), sellerWrapper);
            }
        }

        // load sellers ballances
        list  = entityManager.RETRIEVE_ALL(SellerBalance.class, null);
        for(it = list.iterator(); it.hasNext();){
            SellerBalance balance = (SellerBalance)it.next();
            sellerWrapper = (SellerWrapper)sellerId_seller.get(balance.getSellerId());
            if(sellerWrapper != null && balance.getBalanceId() != null && balance.getCode() != null){
                sellerWrapper.balanceId_balance.put(balance.getBalanceId(), balance);
                sellerWrapper.balanceCode_balanceId.put(balance.getCode(), balance.getBalanceId());
            }
        }

        // load sellers tariffs
        list  = entityManager.RETRIEVE_ALL(SellerTariff.class, (FilterWrapper[])null, new String[]{"functionInstance"});
        for(it = list.iterator(); it.hasNext();){
            SellerTariff tariff = (SellerTariff)it.next();
            sellerWrapper = (SellerWrapper)sellerId_seller.get(tariff.getSellerId());
            if(sellerWrapper != null){
                sellerWrapper.tariffId_tariffEntity.put(tariff.getTariffId(), tariff);

                FunctionWrapper functionWrapper = new FunctionWrapper(tariff.getFunctionInstance());
                sellerWrapper.tariffId_tariffFunctionInstance.put(tariff.getTariffId(), functionWrapper);
                //cache tariff by name if name is not null
//                if(tariff.getName() != null){
//                    if(sellerWrapper.tariffName_tariffFunctionInstance.containsKey(tariff.getName()))
//                        throw new InitException("Duplicate tariff name for seller", new String[]{tariff.getName(), sellerWrapper.getSeller().getName()});
//                    sellerWrapper.tariffName_tariffFunctionInstance.put(tariff.getName(), functionWrapper);
//                }
            }
        }

        // load sellers restrictions
        list  = entityManager.RETRIEVE_ALL(SellerRestriction.class, (FilterWrapper[])null, new String[]{"functionInstance"});
        for(it = list.iterator(); it.hasNext();){
            SellerRestriction restriction = (SellerRestriction)it.next();
            sellerWrapper = (SellerWrapper)sellerId_seller.get(restriction.getSellerId());
            if(sellerWrapper != null){
                FunctionWrapper functionWrapper = new FunctionWrapper(restriction.getFunctionInstance());
                sellerWrapper.restriction.add(functionWrapper);
            }
        }
    }

    public Seller getSeller(Long sellerId) {
        if(sellerId == null)
            return null;
        SellerWrapper sellerWrapper = (SellerWrapper)sellerId_seller.get(sellerId);
        if(sellerWrapper == null)
            return null;
        return sellerWrapper.getSeller();
    }

    public SellerWrapper getSellerWrapper(String code){
        return (SellerWrapper)sellerCode_seller.get(code);
    }
    public SellerWrapper getSellerWrapper(Long sellerId){
        return (SellerWrapper)sellerId_seller.get(sellerId);
    }
    
    public Seller getSeller(String sellerCode) {
        SellerWrapper sellerWrapper = getSellerWrapper(sellerCode);
        if(sellerWrapper == null)
            return null;
        return sellerWrapper.getSeller();
    }

    /** get specified seller and his parent and parent of this parent and so on up to the
     * hierarchy root.
     * @param sellerCode initial seller code to start up and go forward to the tree root.
     * @return list of sellers related as child-parents.
     * */
    public List getSellerWrapperParentsChain(String sellerCode){
        List sellersChain = new ArrayList();
        SellerWrapper sellerWrapper = getSellerWrapper(sellerCode);
        sellersChain.add(sellerWrapper);
        while(sellerWrapper.parentSellerWrapper != null){
            sellerWrapper = sellerWrapper.parentSellerWrapper;
            sellersChain.add(sellerWrapper);
        }
        return sellersChain;
    }

    private Long getSellerProcessingId(SellerWrapper sellerWrapper){
        if(sellerWrapper != null && sellerWrapper.seller != null){
            if(sellerWrapper.seller.getProcessingId() != null)
                return sellerWrapper.seller.getProcessingId();
            else if(sellerWrapper.parentSellerWrapper != null)
                return getSellerProcessingId(sellerWrapper.parentSellerWrapper);
            else
                return null;
        }
        else
            return null;
    }
}
