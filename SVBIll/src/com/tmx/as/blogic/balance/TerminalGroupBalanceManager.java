package com.tmx.as.blogic.balance;

import com.tmx.as.entities.bill.balance.AbstractBalance;
import com.tmx.as.entities.bill.balance.SellerBalance;
import com.tmx.as.entities.bill.balance.TerminalGroupBalance;
import com.tmx.as.entities.bill.restriction.AbstractRestriction;
import com.tmx.as.entities.general.user.User;
import com.tmx.as.exceptions.DatabaseException;
import com.tmx.as.blogic.BlogicException;
import com.tmx.beng.access.Connection;
import com.tmx.beng.base.BillException;
import com.tmx.beng.base.BillingMessage;
import com.tmx.beng.base.BillingMessageImpl;
import com.tmx.util.InitException;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class TerminalGroupBalanceManager extends AbstractBalanceManager{

    public TerminalGroupBalanceManager() {
        super();
    }

    public void deleteBalance(Long balanceId) throws DatabaseException{
        DELETE(TerminalGroupBalance.class, balanceId);
    }

    public void deleteRestriction(Long restrictionId) throws DatabaseException {

    }

    public AbstractBalance getBalance(Long balanceId) throws DatabaseException {
        return (AbstractBalance)RETRIEVE(TerminalGroupBalance.class, balanceId, new String[]{"terminalGroup"});
    }

    public void saveBalance(AbstractBalance balance) throws DatabaseException{
        SAVE(balance);
    }

    public List getAllOwner(User loggedInUser) throws DatabaseException {
        return new ArrayList(0);
    }

    public AbstractRestriction getRestriction(Long restrictionId) throws DatabaseException {
        return null;
    }

    public void saveRestriction(AbstractRestriction restriction) throws DatabaseException {

    }

    //TODO change this
    public void changeBalance(BalanceModificationConfig config) throws BlogicException, InitException, BillException, DatabaseException {
        
    }

}
