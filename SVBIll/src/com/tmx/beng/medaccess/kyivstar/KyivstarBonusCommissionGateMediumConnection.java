package com.tmx.beng.medaccess.kyivstar;

import com.tmx.beng.httpsgate.HttpsGateException;
import com.tmx.beng.base.BillingMessage;
import com.tmx.beng.medaccess.MediumConnection;
import com.tmx.beng.httpsgate.kyivstar.KyivstarBonusCommissionGate;

public class KyivstarBonusCommissionGateMediumConnection implements MediumConnection {
    private KyivstarBonusCommissionGate gate = null;

    KyivstarBonusCommissionGateMediumConnection() throws HttpsGateException {
        gate = KyivstarBonusCommissionGate.getInstance();
    }

    public void send(BillingMessage msg) {
        gate.enqueueBillMessage(msg);
    }
}
