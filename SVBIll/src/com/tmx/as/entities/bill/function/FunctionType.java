package com.tmx.as.entities.bill.function;

import com.tmx.as.base.BasicEntity;
import java.io.Serializable;


public class FunctionType extends BasicEntity implements Serializable{

    public final static String CATEGORY_TARIFF = "TARIFF";
    public final static String CATEGORY_LIMIT = "LIMIT";

    private Long functionTypeId;
    private String name;
    private String functionSignature;
    private String implClass;
    private String implMethod;
    private String description;
    private String category;

    //-------------Properties
    public Long getFunctionTypeId() {
        return functionTypeId;
    }

    public void setFunctionTypeId(Long functionTypeId) {
        this.functionTypeId = functionTypeId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFunctionSignature() {
        return functionSignature;
    }

    public void setFunctionSignature(String functionSignature) {
        this.functionSignature = functionSignature;
    }

    public String getImplClass() {
        return implClass;
    }

    public void setImplClass(String implClass) {
        this.implClass = implClass;
    }

    public String getImplMethod() {
        return implMethod;
    }

    public void setImplMethod(String implMethod) {
        this.implMethod = implMethod;
    }

    public Serializable getId() {
        return getFunctionTypeId();
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getValid() {
        return valid;
    }

    public void setValid(int valid) {
        this.valid = valid;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }
    
}
