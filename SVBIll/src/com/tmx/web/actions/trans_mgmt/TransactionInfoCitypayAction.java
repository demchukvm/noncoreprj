package com.tmx.web.actions.trans_mgmt;

import com.tmx.web.forms.trans_mgmt.TransactionInfoCitypayForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForm;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.tmx.web.base.BasicDispatchedAction;
import com.tmx.beng.httpsgate.citypay.CitypayGate;


public class TransactionInfoCitypayAction extends BasicDispatchedAction {

    private final String FORWARD = "transactionInfoCitypay";

    protected ActionForward defaultAction(ActionMapping mapping,
                                          ActionForm form,
                                          HttpServletRequest request,
                                          HttpServletResponse response,
                                          String methodName) throws Throwable {
        return refreshBalance(mapping, form, request, response);
    }

    public ActionForward refreshBalance(ActionMapping mapping,
                                        ActionForm form,
                                        HttpServletRequest request,
                                        HttpServletResponse response) throws Throwable {
        TransactionInfoCitypayForm transactionInfoForm = (TransactionInfoCitypayForm) form;

        try {
            transactionInfoForm.getEditBoxCitypayBalance().setValue(CitypayGate.getInstance().getBalance().toString());
        } catch (Throwable e) {
            transactionInfoForm.getEditBoxCitypayBalance().setValue(e.getLocalizedMessage());
        }
        return mapping.findForward(FORWARD);
    }
}
