package com.tmx.beng.iso8583Gate.pdu.substructure;

import com.tmx.beng.iso8583Gate.pdu.RuntimeDataElement;
import com.tmx.beng.iso8583Gate.pdu.IsoParseException;
import com.tmx.beng.iso8583Gate.pdu.IsoSerializeException;

/**

 */
public interface SubDataElementFormat {

    /** Take appropriate plain value from RDE parse it and produce RuntimeSubDataElement.
    */
    public void parse(RuntimeDataElement rde) throws IsoParseException;

    /** Serialize RDES into RDE plain value.
     * RDE holds its plain type in valueHandler property as ov ofe seeral plain basic types.
     * Particualar implementations of SubDataElementFormat should serialize into appropriate plain value. */
    public void serialize(RuntimeDataElement rde) throws IsoSerializeException;

    /** Prepare empty runtime data element structure for given RDE */
    public RuntimeSubDataElement prepare(RuntimeDataElement rde);
}
