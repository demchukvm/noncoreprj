package com.tmx.as.jsse;

import com.tmx.as.base.Reconfigurable;
import com.tmx.as.jsse.beans.JsseDocument;
import com.tmx.as.jsse.beans.Property;
import com.tmx.as.jsse.beans.Provider;
import com.tmx.as.jsse.beans.Keystore;
import com.tmx.util.InitException;
import com.tmx.util.Configuration;
import org.apache.log4j.Logger;
import org.apache.xmlbeans.XmlOptions;

import java.util.*;
import java.io.FileInputStream;
import java.io.IOException;
import java.security.Security;
import java.security.KeyStore;

/**
 */
public class JsseSimpleService implements Reconfigurable {
    /** Only single instance available */
    private static JsseSimpleService jsseSimpleService;
    private Config serviceConfig = null;

    public static String LOGCLASS_REPORT = "jsse";
    /** Logger */
    private static Logger logger = Logger.getLogger(LOGCLASS_REPORT + ".JsseSimpleService");
    private Map keystores = new HashMap();

    private JsseSimpleService(){
    }

    public static JsseSimpleService getInstance(){
        if(jsseSimpleService == null)
            jsseSimpleService = new JsseSimpleService();

        return jsseSimpleService;
    }

    public void reload() throws InitException {
        String configFile = Configuration.getInstance().getProperty("jsse_service.config_file");
        if (configFile == null)
            throw new InitException("err.jsse_simple.config_file_is_not_specified", Locale.getDefault());

        //Load config from XML
        serviceConfig = new Config();
        serviceConfig.reload(configFile);

        //set security properties
        JsseDocument.Jsse jsseRoot = serviceConfig.getJsseDocument().getJsse();
        for(int i=0; i<jsseRoot.getProperties().getPropertyArray().length; i++){
            Property property = jsseRoot.getProperties().getPropertyArray()[i];
            try{
                System.setProperty(property.getName(), Configuration.getInstance().substituteVariablesInString(property.getValue()));
            }
            catch(Exception e){
                throw new InitException("err.jsse_simple.failed_to_set_propert�", new String[]{property.getValue()}, e);
            }
        }

        //initialize security providers
        for(int i=0; i<jsseRoot.getProviders().getProviderArray().length; i++){
            Provider providerBean = jsseRoot.getProviders().getProviderArray(i);
            try{
                java.security.Provider provider = (java.security.Provider)Class.forName(providerBean.getImplClass()).newInstance();
                Security.insertProviderAt(provider, providerBean.getPriority());
            }
            catch(Exception e){
                throw new InitException("err.jsse_simple.failed_to_prepare_security_provider", new String[]{providerBean.getImplClass()}, e);
            }
        }

        //initialize keystores
        keystores = new HashMap();
        if(jsseRoot.getKeystores() != null)
            for(int i=0; i<jsseRoot.getKeystores().getKeystoreArray().length; i++){
                Keystore keystoreBean = jsseRoot.getKeystores().getKeystoreArray(i);
                KeyStoreMetadata keyStoreMetadata = new KeyStoreMetadata();
                keyStoreMetadata.init(keystoreBean);
                keystores.put(keyStoreMetadata.getAlias(), keyStoreMetadata);
            }
    }

    public KeyStoreMetadata getKeystoreMetadata(String alias){
        return (KeyStoreMetadata)keystores.get(alias);
    }

    private class Config{
        private JsseDocument configDocument;

        public void reload(String configFilePath) throws InitException{
            JsseDocument localJsseDocument;
            FileInputStream fis = null;
            try{
                fis = new FileInputStream(configFilePath);
                // Bind the instance to the generated XMLBeans types.
                localJsseDocument = JsseDocument.Factory.parse(fis);
            }
            catch(Throwable e){
                throw new InitException("err.iso8583service.failed_to_parse_config", new String[]{configFilePath}, e);
            }
            finally{
                try{if(fis != null)
                        fis.close();}
                catch(IOException e){}
            }

            //validate
            ArrayList errors = new ArrayList();
            XmlOptions opts = new XmlOptions();
            opts.setErrorListener(errors);
            if(!localJsseDocument.validate(opts)){
                StringBuffer errorBuffer = new StringBuffer("Validation failed of XML "+configFilePath+":\n");
                Iterator iter = errors.iterator();
                while(iter.hasNext())
                    errorBuffer.append("   >> " + iter.next() + "\n");
                logger.error(errorBuffer.toString());
                throw new InitException("err.jsse_simple.config_validation_failed", new String[]{configFilePath});
            }
            //assign to property
            configDocument = localJsseDocument;
        }

        public JsseDocument getJsseDocument(){
            return configDocument;
        }

    }

    public class KeyStoreMetadata {
        private Keystore keyStoreBean;
        private KeyStore keyStore;

        private KeyStoreMetadata(){
        }

        private void init(Keystore keyStoreBean) throws InitException{
            this.keyStoreBean = keyStoreBean;
            try{
                keyStore = KeyStore.getInstance(keyStoreBean.getType());
                String path = Configuration.getInstance().substituteVariablesInString(keyStoreBean.getPath());
                keyStore.load(new FileInputStream(path), keyStoreBean.getPassphrase().toCharArray());
            }
            catch(Exception e){
                throw new InitException("err.jsse_simple.failed_to_init_keystore", new String[]{keyStoreBean.getAlias()}, e);
            }
        }

        public char[] getPassphrase(){
            return keyStoreBean != null ? keyStoreBean.getPassphrase().toCharArray() : null;
        }

        public String getAlias(){
            return keyStoreBean != null ? keyStoreBean.getAlias() : null;
        }

        public KeyStore getKeyStore() {
            return keyStore;
        }
    }

}
