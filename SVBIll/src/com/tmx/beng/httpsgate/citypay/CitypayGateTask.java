package com.tmx.beng.httpsgate.citypay;

import com.tmx.beng.base.BillingMessage;
import com.tmx.beng.httpsgate.HttpsGateException;
import com.tmx.util.queue.ActionQueueElement;

public class CitypayGateTask extends ActionQueueElement
{
    private BillingMessage procesedMessage;
    private BasicCitypayGate gate;

    public CitypayGateTask(BillingMessage billingMessage, BasicCitypayGate gate)
    {
        this.gate = gate;
        this.procesedMessage = billingMessage;
    }

    public void run() {
        try{
            gate.runBillMessage(procesedMessage);
        }
        catch(HttpsGateException e){
            gate.logger.error(e.getLocalizedMessage(), e);
        }
    }
}
