package com.tmx.engines.graphengine.base;

import com.tmx.util.StructurizedException;
import java.util.Locale;


public class GraphException extends StructurizedException {

    public GraphException(String errKey){
        super(errKey);
    }

    public GraphException(String errKey, Locale locale){
        super(errKey, locale);
    }

    public GraphException(String errKey, String[] params){
        super(errKey, params);
    }

    public GraphException(String errKey, String[] params, Throwable cause){
        super(errKey, params, cause);
    }

    public GraphException(String errKey, String[] params, Locale locale){
        super(errKey, params, locale);
    }

    public GraphException(String errKey, Throwable cause){
        super(errKey, cause);
    }
}
