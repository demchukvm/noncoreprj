package com.tmx.web.actions.core.tariff.seller_tariff;

import com.tmx.web.actions.core.tariff.AbstractTariffParameterAction;
import com.tmx.web.forms.core.tariff.AbstractInfoTariffForm;
import com.tmx.web.forms.core.tariff.seller_tariff.InfoSellerTariffForm;
import com.tmx.web.base.SessionEnvironment;
import com.tmx.web.base.WebException;
import com.tmx.as.blogic.balance.AbstractBalanceManager;
import com.tmx.as.blogic.balance.SellerBalanceManager;
import com.tmx.util.InitException;
import com.tmx.beng.base.BillException;
import org.apache.struts.action.ActionMapping;

import javax.servlet.http.HttpServletRequest;


public class SellerTariffParameterAction  extends AbstractTariffParameterAction {
    protected AbstractBalanceManager getBalanceManager() throws InitException, BillException {
        return new SellerBalanceManager();
    }

    protected AbstractInfoTariffForm getInfoTariffForm(ActionMapping mapping, HttpServletRequest request) throws WebException {
        return (AbstractInfoTariffForm) SessionEnvironment.allocateForm(
                InfoSellerTariffForm.class, "core.tariff.seller_tariff.infoSellerTariffForm", mapping, request);  
    }
}
