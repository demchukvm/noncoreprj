package com.tmx.as.base;

import java.io.Serializable;
import java.util.Locale;


public interface Entity {
    public void setDefaults();
    public Serializable getId();
    public boolean isTransient();
    public String getLabel(String attrName, Locale locale);
}
