package com.tmx.beng.httpsgate.dacard.pdu;

import java.io.OutputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 */
public abstract class AbstractTypePAD<T> implements TypePAD<T>{

    public void serializeLengthInBytes(OutputStream os, T value) throws DacardPadException{
        serializeDataSize(os, resolveNumberSizeInBytes(value));
    }

    protected abstract short resolveNumberSizeInBytes(T value) throws DacardPadException;

    /** serialize data size into 2 bytes in little-endians format.
     * @param os output stream to write data into;
     * @param size the size to be converted and written into output stream.
     * @throws DacardPadException on serialization error. */
    private void serializeDataSize(OutputStream os, short size) throws DacardPadException{
        try{
            os.write(ByteUtil.convertShortIntoLittleEndian(size));
//            os.write((byte)size);
//            os.write((byte)(size >>> 8)); old worked code. going to delete it.
        }
        catch (IOException e){
            throw new DacardPadException("Failed to serialize data size", new String[]{Integer.toString(size)}, e);
        }
    }

}
