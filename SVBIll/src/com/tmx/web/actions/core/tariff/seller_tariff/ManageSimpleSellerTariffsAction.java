package com.tmx.web.actions.core.tariff.seller_tariff;

import com.tmx.web.actions.core.tariff.AbstractManageSimpleTariffsAction;
import com.tmx.web.forms.core.tariff.seller_tariff.ManageSimpleSellerTariffsForm;
import com.tmx.web.forms.core.base.AbstractFormFactory;
import com.tmx.web.controls.ScrollBox;
import com.tmx.as.blogic.SellerManager;
import com.tmx.as.blogic.tariff.AbstractTariffManager;
import com.tmx.as.blogic.tariff.SellerTariffManager;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Collection;
import java.util.Collections;
import java.util.List;


public class ManageSimpleSellerTariffsAction extends AbstractManageSimpleTariffsAction {

    public static final String PARAMETER_OWNER_NAME = "ownerId";

    protected AbstractTariffManager getTariffManager() {
        return new SellerTariffManager();  
    }

    //roadmap setuping
    public ActionForward defaultAction(ActionMapping mapping,
                                       ActionForm form,
                                       HttpServletRequest request,
                                       HttpServletResponse response,
                                       String methodName) throws Throwable {
        ManageSimpleSellerTariffsForm manageSellerForm = (ManageSimpleSellerTariffsForm) verifyForm(form, ManageSimpleSellerTariffsForm.class);
        AbstractFormFactory.getInstance(getLoggedInUser(request)).setUpForm(manageSellerForm);
        manageSellerForm.setUpTariffsTableFilters();
        manageSellerForm.getIncomingTriffsTable().refresh();
        getRoadmap().collectParameter(DEFAULT_VALID_ACTION, "onCancelAction", "/core/tariff/seller_tariff/manageSimpleSellerTariffs");
        getRoadmap().collectParameter(DEFAULT_VALID_ACTION, "onSaveAction", "manageSimpleTariffsAction");
        return super.defaultAction(mapping, form, request, response, methodName);
    }

    public ActionForward refreshIncomingTariffsTable(ActionMapping mapping, ActionForm form, HttpServletRequest request,
                                                     HttpServletResponse response) throws Throwable {
        ManageSimpleSellerTariffsForm manageSellerForm = (ManageSimpleSellerTariffsForm) verifyForm(form, ManageSimpleSellerTariffsForm.class);
        List subSellers = new SellerManager().getNestedSellersInSeller(manageSellerForm.getSelectedSellerIdIncomingTable(), false);
//        Collections.sort(subSellers);
        manageSellerForm.bindSubSellers(subSellers);
        manageSellerForm.setUpTariffsTableFilters();
        //---------------
        ScrollBox subSellersScrollBox = (ScrollBox)manageSellerForm.getTariffsTable().getParameterControl("by_seller_id", "seller_id");
        subSellersScrollBox.setSelectedKey(ScrollBox.NULL_KEY);
        //---------------
        manageSellerForm.getTariffsTable().refresh();
        return super.refreshIncomingTariffsTable(mapping, form, request, response);
    }

    public ActionForward onCreateTariffAction(ActionMapping mapping,
                                       ActionForm form,
                                       HttpServletRequest request,
                                       HttpServletResponse response) throws Throwable {
        ManageSimpleSellerTariffsForm manageSellerForm = (ManageSimpleSellerTariffsForm) form;
        String selectedKey = ((ScrollBox) manageSellerForm.getTariffsTable().getParameterControl("by_seller_id", "seller_id")).getSelectedKey();
        getRoadmap().collectParameter("/core/tariff/seller_tariff/infoSellerTariff", PARAMETER_OWNER_NAME, selectedKey);
        return mapping.findForward("infoTariffAction");
    }

    public ActionForward onCreateIncomingTariffAction(ActionMapping mapping,
                                       ActionForm form,
                                       HttpServletRequest request,
                                       HttpServletResponse response) throws Throwable {
        ManageSimpleSellerTariffsForm manageSellerForm = (ManageSimpleSellerTariffsForm) form;
        String selectedKey = ((ScrollBox) manageSellerForm.getIncomingTriffsTable().getParameterControl("by_seller_id", "seller_id")).getSelectedKey();
        getRoadmap().collectParameter("/core/tariff/seller_tariff/infoSellerTariff", PARAMETER_OWNER_NAME, selectedKey);
        return mapping.findForward("infoTariffAction");
    }
    
}
