package com.tmx.web.actions.core.transactions.client_transaction;

import com.tmx.web.base.BasicDispatchedAction;
import com.tmx.web.forms.core.transaction.client_transaction.InfoClientTransactionForm;
import com.tmx.web.forms.core.dictionary.seller.ManageSellersForm;
import com.tmx.as.blogic.TransactionManager;
import com.tmx.as.entities.bill.transaction.ClientTransaction;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForm;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;


public class InfoClientTransactionAction extends BasicDispatchedAction {

    private static final String FORWARD_INFO_CLIENT_TRANSACTION_PAGE = "infoClientTransactionPage";

    public ActionForward defaultAction(ActionMapping mapping,
                                       ActionForm form,
                                       HttpServletRequest request,
                                       HttpServletResponse response,
                                       String methodName) throws Throwable {
        InfoClientTransactionForm infoClientTransactionForm =
                (InfoClientTransactionForm) verifyForm(form, InfoClientTransactionForm.class);
        
        infoClientTransactionForm.reset();
        TransactionManager transactionManager = new TransactionManager();

        ClientTransaction clientTransaction;
        if (infoClientTransactionForm.getSelectedTransactionId() != null) {
            clientTransaction = transactionManager.getClientTransaction(
                                    infoClientTransactionForm.getSelectedTransactionId());
            List<Object[]> voucherData = new TransactionManager().getVoucherByTransactionGUID(clientTransaction.getTransactionGUID());
            if (voucherData.size() > 0) {
                infoClientTransactionForm.bindVoucherData(voucherData.get(0));
            }
        } else {
            List clientTransactions = transactionManager.getClientTransactionByGUID(
                                    infoClientTransactionForm.getSelectedBillTransactionNum());

            clientTransaction = (ClientTransaction) clientTransactions.get(0);
            List<Object[]> voucherData = new TransactionManager().getVoucherByTransactionGUID(clientTransaction.getTransactionGUID());
            if (voucherData.size() > 0) {
                infoClientTransactionForm.bindVoucherData(voucherData.get(0));
            }
        }

        infoClientTransactionForm.bindData(clientTransaction) ;

        return mapping.findForward(FORWARD_INFO_CLIENT_TRANSACTION_PAGE);
    }

        public ActionForward testMethod(ActionMapping mapping,
                                  ActionForm form,
                                  HttpServletRequest request,
                                  HttpServletResponse response) throws Throwable {

        return mapping.findForward(FORWARD_INFO_CLIENT_TRANSACTION_PAGE);
    }

}
