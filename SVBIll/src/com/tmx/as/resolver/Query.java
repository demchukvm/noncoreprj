package com.tmx.as.resolver;

import com.tmx.as.base.*;
import com.tmx.as.base.criterion.CriterionWrapper;

import java.util.Stack;


/** Used to model the query */
public class Query implements Cloneable{
    private String queryType = QueryType.RETRIEVE_ALL;//default value
    private String queryName = null;
    private String entityClass = null;
    private FilterWrapper[] filters = new FilterWrapper[0];
    private CriterionWrapper[] criterions=new CriterionWrapper[0];
    private OrderWrapper[] orders = new OrderWrapper[0];
    private QueryParameterWrapper[] queryParameters = new QueryParameterWrapper[0];
    private ProjectionWrapper[] projectionWrappers = new ProjectionWrapper[0];
    private String[] tableAttrs = new String[0];
    private int pageNumber = 1;//default value
    private int pageSize = -1;//default value

    public String getQueryType() {
        return queryType;
    }

    public void setQueryType(String queryType) {
        this.queryType = queryType;
    }

    public String getEntityClass() {
        return entityClass;
    }

    public void setEntityClass(String entityClass) {
        this.entityClass = entityClass;
    }

    public FilterWrapper[] getFilters() {
        return filters;
    }

    public FilterWrapper getFilter(String filterName){
        for(int i=0; i<filters.length; i++){
            if(filterName.equals(filters[i].getName()))
                return filters[i];
        }
        return null;
    }

    public void setFilters(FilterWrapper[] filters) {
        this.filters = filters;
    }

    public OrderWrapper[] getOrders() {
        return orders;
    }

    public void setOrders(OrderWrapper[] orders) {
        this.orders = orders;
    }

    public String[] getTableAttrs() {
        return tableAttrs;
    }

    public void setTableAttrs(String[] tableAttrs) {
        this.tableAttrs = tableAttrs;
    }

    public int getPageNumber() {
        return pageNumber;
    }

    public void setPageNumber(int pageNumber) {
        if(0<pageNumber)this.pageNumber = pageNumber;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        if(0<pageSize) this.pageSize = pageSize;
    }

    public String getStringPresentation(){
        StringBuffer query = new StringBuffer();
        query.append("NOT IMPLEMENTED YET");
        return query.toString();
    }

    public static class QueryType{
        public static String RETRIEVE_ALL = "retrieve_all";
        public static String RETRIEVE = "retrieve";
        public static String EXECUTE_QUERY = "execute_query";
    }

    public String getQueryName() {
        return queryName;
    }

    public void setQueryName(String queryName) {
        this.queryName = queryName;
    }

    public QueryParameterWrapper[] getQueryParameters() {
        return queryParameters;
    }

    public void setQueryParameters(QueryParameterWrapper[] queryParameters) {
        this.queryParameters = queryParameters;
    }

    public ProjectionWrapper[] getProjectionWrappers() {
        return projectionWrappers;
    }

    public void setProjectionWrappers(ProjectionWrapper[] projectionWrappers) {
        this.projectionWrappers = projectionWrappers;
    }

    public CriterionWrapper[] getCriterions() {
        return criterions;
    }

    public void setCriterions(CriterionWrapper[] criterions) {
        this.criterions = criterions;
    }

    public void set(String fieldName, String fieldValue) throws ResolverException{
        if(fieldName.toLowerCase().equals("query_type")){
            if("retrieve_all".equals(fieldValue.toLowerCase()))
                queryType = QueryType.RETRIEVE_ALL;
            if("retrieve".equals(fieldValue.toLowerCase()))
                queryType = QueryType.RETRIEVE;
            if("execute_query".equals(fieldValue.toLowerCase()))
                queryType = QueryType.EXECUTE_QUERY;
            //RM: add missing query type mapping here...

            return;
        }
        if("query_name".equals(fieldName.toLowerCase())){
            queryName = fieldValue;
            return;
        }
        if("entity_class".equals(fieldName.toLowerCase())){
            entityClass = fieldValue;
            return;
        }
        if("page_number".equals(fieldName.toLowerCase())){
            pageNumber = Integer.parseInt(fieldValue);
            return;
        }
        if("page_size".equals(fieldName.toLowerCase())){
            pageSize = Integer.parseInt(fieldValue);
            return;
        }
        if("table_attr".equals(fieldName.toLowerCase())){
            tableAttrs = TableAttrWrapper.appendToTableAttrs(tableAttrs, fieldValue);
            return;
        }
        //RM: throw exception if field not found
        throw new ResolverException("err.resolver.unknown_parameter_for", new String[]{this.getClass().getName(), fieldName, fieldValue.toString()});
    }

    public Object clone(){
        Object q = null;
        try{
            q = super.clone();
        }
        catch(CloneNotSupportedException e){
            q = null;
        }
        return q;
    }

}

