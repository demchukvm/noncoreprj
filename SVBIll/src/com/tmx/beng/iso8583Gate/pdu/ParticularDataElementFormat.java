package com.tmx.beng.iso8583Gate.pdu;

import com.tmx.beng.iso8583Gate.pdu.substructure.SubDataElementFormat;
import com.tmx.beng.iso8583Gate.pdu.substructure.RuntimeSubDataElement;
import com.tmx.util.InitException;

import java.util.Map;
import java.util.HashMap;

/**
 Data Element Format particular to Message type
 */
public class ParticularDataElementFormat extends BasicDataElementFormat{
    private OccuranceType occuranceType;
    private String fixedValue;
    /** Sub data element format implementation class */
    private SubDataElementFormat subDataElementFormat;

    /** package private constructor */
    ParticularDataElementFormat(int bitmapNumber, String name, String fixedValue, String occuranceType, String subDataElFormatImpl) throws InitException {
        this.bitmapNumber = bitmapNumber;
        this.name = name;
        this.fixedValue = fixedValue;
        this.occuranceType = (OccuranceType)OccuranceType.getInstance(occuranceType);
        try{
            if(subDataElFormatImpl != null)
                this.subDataElementFormat = (SubDataElementFormat)Class.forName(subDataElFormatImpl).newInstance();
        }
        catch(Exception e){
            throw new InitException("err.part_data_el_format.failed_to_init_sub_data_el_format", new String[]{subDataElFormatImpl}, e);
        }
    }


    /** serialize substructures from rde if available into rde string. */
    public void serialize(RuntimeDataElement rde) throws IsoSerializeException{
        if(subDataElementFormat != null)
            subDataElementFormat.serialize(rde);
    }

    /** parse rde raw data to substrucutes  */
    public void parse(RuntimeDataElement rde) throws IsoParseException{
        if(subDataElementFormat != null)
            subDataElementFormat.parse(rde);
    }


    public RuntimeSubDataElement prepare(RuntimeDataElement rde){
        return subDataElementFormat != null ? subDataElementFormat.prepare(rde) : null;        
    }

    //-------------Properties

    public String getFixedValue() {
        return fixedValue;
    }


    //---------------------Inner classes

    public static class OccuranceType extends NamedType{
        public static final OccuranceType MANDATORY = new OccuranceType("M");
        public static final OccuranceType OPTIONAL = new OccuranceType("O");

        private OccuranceType(String name){
            super(name);
        }
    }

    public static abstract class NamedType{
        private String name;
        private static Map instances = new HashMap();

        protected NamedType(String name){
            this.name = name;
            instances.put(toString(), this);
        }

        public static NamedType getInstance(String name){
            return (NamedType)instances.get(name);
        }

        public String toString(){
            return name;
        }
    }
}
