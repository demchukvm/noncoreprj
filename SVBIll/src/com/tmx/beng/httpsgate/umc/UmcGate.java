package com.tmx.beng.httpsgate.umc;

import com.tmx.beng.base.*;
import com.tmx.beng.access.Access;
import com.tmx.beng.httpsgate.HttpsGateException;
import com.tmx.beng.httpsgate.HttpsRequestException;
import com.tmx.beng.httpsgate.BadResponseException;
import com.tmx.util.queue.ActionQueue;
import com.tmx.util.InitException;
import com.tmx.util.Configuration;
import com.tmx.gate.datastream.base.GateContext;
import com.tmx.as.base.EntityManager;
import com.tmx.as.entities.mobipay.umc.UmcGateTransaction;
import org.apache.log4j.Logger;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.NameValuePair;
import org.apache.commons.httpclient.HttpMethod;
import org.apache.commons.httpclient.HostConfiguration;
import org.apache.commons.httpclient.contrib.ssl.AuthSSLProtocolSocketFactory;
import org.apache.commons.httpclient.protocol.Protocol;
import org.apache.commons.httpclient.protocol.ProtocolSocketFactory;
import org.apache.commons.httpclient.methods.PostMethod;

import java.io.IOException;
import java.util.Date;
import java.util.regex.Pattern;
import java.util.regex.Matcher;
import java.text.DecimalFormat;
import java.net.URL;
import java.net.InetAddress;

/**
 * UMC outgoing https gate
 */
public class UmcGate extends BasicGate {
    // properties names constants
    private static final String HTTPS_GATE_NAME = "umc_gate";

    private static Logger logger = Logger.getLogger("umc_gate." + HTTPS_GATE_NAME);
    private static UmcGate umcGate = null;
    private Access access = null;

    // queue
    private ActionQueue messageQueue = null;

    // properties names constants
    private static final String PROP_URL = "url";
    private static final String PROP_USERNAME = "username";
    private static final String PROP_PASSWORD = "password";
    private static final String PROP_INIT_HANDLER_COUNT = "handlers.initial";
    private static final String PROP_MAX_HANDLER_COUNT = "handlers.max";
    private static final String PROP_PAYREQ_MAX_COUNT = "paymentRequest.maxAttemptCount";
    private static final String PROP_CONFPAY_MAX_COUNT = "confPayment.maxAttemptCount";
    private static final String PROP_SERVUNAV_MAX_COUNT = "serviceUnavailable.maxAttemptCount";
    private static final String PROP_KEYSTORE_PATH = "keystore.path";
    private static final String PROP_KEYSTORE_PASSWORD = "keystore.password";
    private static final String PROP_TRUSTSTORE_PATH = "truststore.path";
    private static final String PROP_TRUSTSTORE_PASSWORD = "truststore.password";
    private static final String PROP_LOCAL_ADDRESS = "localAddress";


    public static UmcGate getInstance()throws HttpsGateException {
        if(umcGate == null){
            logger.error("Attempt to obtain not inited billing engine");
            System.out.println("UmcGate.getInstance:: umcGate: " + umcGate.toString());
            throw new HttpsGateException("err.https_gate.null_instance");
        }
        return umcGate;
    }

    public static void init()throws InitException{
        logger.info("Start UMC HTTPS Gate initialization");
        umcGate = new UmcGate();
        umcGate.init0();
        int initHandlerCount = Integer.parseInt(umcGate.properties.getProperty(PROP_INIT_HANDLER_COUNT, "1"));
        int maxHandlerCount = Integer.parseInt(umcGate.properties.getProperty(PROP_MAX_HANDLER_COUNT, "5"));
        umcGate.messageQueue = new ActionQueue(initHandlerCount, maxHandlerCount);
        //init ssl with client authintification
        String keyStorePath = null;
        String trustStorePath = null;
        try{
            keyStorePath = Configuration.getInstance().substituteVariablesInString(umcGate.properties.getProperty(PROP_KEYSTORE_PATH));
            trustStorePath = Configuration.getInstance().substituteVariablesInString(umcGate.properties.getProperty(PROP_TRUSTSTORE_PATH));            
        }
        catch(Exception e){
            throw new InitException("err.umc_https_gate.failed_to_get_parameter", e);
        }
        String keyStorePassword = umcGate.properties.getProperty(PROP_KEYSTORE_PASSWORD);
        String trustStorePassword = umcGate.properties.getProperty(PROP_TRUSTSTORE_PASSWORD);
        String url = umcGate.properties.getProperty(PROP_URL);
        try{
            Matcher portMatcher = Pattern.compile("https-umc\\:.*\\:(\\d*).*").matcher(url);
            if(!portMatcher.matches())
                throw new InitException("err.umc_https_gate.failed_to_parse_port_from_url", new String[]{url});
            int port = Integer.parseInt(portMatcher.group(1));
            
            Protocol authhttps = new Protocol("https-umc",
	    	    (ProtocolSocketFactory) new AuthSSLProtocolSocketFactory(
                        new URL("file:" + keyStorePath), keyStorePassword,
                        new URL("file:" + trustStorePath), trustStorePassword), port);
            Protocol.registerProtocol("https-umc", authhttps);
        }
        catch(Exception e){
            logger.error("Failed to initialize 'https-umc' protocol", e);
            throw new InitException("err.umc_https_gate.failed_to_init_https_umc_protocol", e);
        }
        logger.info("UMC HTTPS Gate initialized");
    }

    public static void shutdown(final long timeout) throws InitException {
        logger.info("Start UMC Gate Shutdown");
        if(umcGate.messageQueue.actionCount() != 0){
            Runtime.getRuntime().addShutdownHook(new Thread(){
                public void run(){
                    try{
                        long lt = System.currentTimeMillis();
                        while(umcGate.messageQueue.actionCount() != 0 &&
                                ((System.currentTimeMillis() - lt) < timeout)){
                            sleep(1000);
                        }
                    }
                    catch(Throwable e){
                        e.printStackTrace();
                    }
                }
            });
        }
        logger.info("UMC Gate shutdown");
    }

    private void init0()throws InitException {
        properties = GateContext.getInstance().getDatastreamProcessorConfig(HTTPS_GATE_NAME);
        access = obtainBillingEngineAccess();
    }

    public void enqueueBillMessage(BillingMessage billMessage){
        UmcHttpsGateTask task = new UmcHttpsGateTask(billMessage);
        messageQueue.enqueue(task);
    }

    public void runBillMessage(BillingMessage billingMessage) throws HttpsGateException
    {
        BillingMessageImpl respMessage = new BillingMessageImpl();
        respMessage.setAttributeString(BillingMessage.REQUEST_ID,
                billingMessage.getAttributeString(BillingMessage.REQUEST_ID));
        try{
            // refill payment
            if(BillingMessage.O_REFILL_PAYMENT.equals(billingMessage.getOperationName())){
                refillPayment(billingMessage, respMessage);
            }
            else if(BillingMessage.O_REFILL_PAYMENT_REDIRECT.equals(billingMessage.getOperationName())){
                refillPayment(billingMessage, respMessage);
            }
            // unsupported operation
            else{
                respMessage.setStatusCode(StatusDictionary.SUBMIT_TRANSACTION_ERROR);
                throw new HttpsGateException("Unsupported command",
                           new String[]{billingMessage.getOperationName()});
            }
        }
        catch(Throwable e){
            if(respMessage.getAttributeString(BillingMessage.STATUS_CODE) == null)
                respMessage.setStatusCode(StatusDictionary.UNKNOWN_ERROR);
            respMessage.setAttributeString(BillingMessage.STATUS_MESSAGE, e.getLocalizedMessage());
        }
        finally{
            // send response to billing engine
            try{
                access.obtainConnection().processAsync(respMessage);
            }
            catch(Throwable e){
                logger.error(e.getLocalizedMessage(), e);
            }
        }
    }

    /**
     *  Refill payment
     * @param billingMessage
     * @param respMessage
     */
    private void refillPayment(BillingMessage billingMessage, BillingMessageWritable respMessage){
        EntityManager entityManager = new EntityManager();

        UmcGateTransaction umcGateTransaction = new UmcGateTransaction();
        umcGateTransaction.setStatusCodeInt(StatusDictionary.STATUS_OK);
        UmcResponse response = null;

        try{
            respMessage.setOperationName(BillingMessage.O_REFILL_PAYMENT_RESULT);
            umcGateTransaction.setMsisdn(billingMessage.getAttributeString(BillingMessage.ACCOUNT_NUMBER));
            umcGateTransaction.setPayAccountNumber(billingMessage.getAttributeString(BillingMessage.PAY_ACCOUNT_NUMBER));
            umcGateTransaction.setBillTransactionNum(billingMessage.getAttributeString(BillingMessage.BILL_TRANSACTION_NUM));
            umcGateTransaction.setAmount(new Double(billingMessage.getAttributeString(BillingMessage.AMOUNT)));

            // 2010.09.06 Proximan
            if(billingMessage.getAttribute(BillingMessage.TERMINAL_SN).equals("SV-00001")
                    || billingMessage.getAttribute(BillingMessage.TERMINAL_SN).equals("localssm-01")
                    || billingMessage.getAttribute(BillingMessage.SELLER_CODE).equals("Winner_h2h"))
            {
                umcGateTransaction.setTerminal((String)billingMessage.getAttribute(BillingMessage.TRADE_POINT));
            }
            else umcGateTransaction.setTerminal((String)billingMessage.getAttribute(BillingMessage.TERMINAL_SN));
            //
            //umcGateTransaction.setTerminal((String)billingMessage.getAttribute(BillingMessage.TERMINAL_SN));

            umcGateTransaction.setLocalTime(new Date());

            // STEP1: check is msisdn valid (ony if msisdn is given)
            if(umcGateTransaction.getMsisdn() != null)
                checkIsMsisdnValid(umcGateTransaction, billingMessage, respMessage);

            int serviceUnavailableMaxAttempt = Integer.parseInt(properties.getProperty(PROP_SERVUNAV_MAX_COUNT, "1"));
            serviceUnavailableMaxAttempt = serviceUnavailableMaxAttempt <= 0 ? 1 : serviceUnavailableMaxAttempt;
            //redo steps #1-2 on error=100 "Service Unavailable"
            boolean redo = true;
            for(int i=0; i<serviceUnavailableMaxAttempt && redo; i++){
                // STEP2: insistent payment request
                paymentRequest(umcGateTransaction, billingMessage, respMessage, i);

                // STEP3: insistent confirm payment
                response = confirmPayment(umcGateTransaction, billingMessage, respMessage);
                redo = (response != null &&
                        response.getError() != null &&
                        response.getError().longValue() == 100);
            }
        }
        catch(Throwable e){
            logger.error(e.getLocalizedMessage(), e);
            if(respMessage.getAttributeString(BillingMessage.STATUS_CODE) == null)
                respMessage.setStatusCode(StatusDictionary.UNKNOWN_ERROR);
            if(respMessage.getAttribute(BillingMessage.STATUS_MESSAGE) == null)
                respMessage.setAttributeString(BillingMessage.STATUS_MESSAGE, e.getLocalizedMessage());
        }
        finally{
            // save umc gate transaction
            try{
                umcGateTransaction.setStatusCode(new Integer(respMessage.getAttributeString(BillingMessage.STATUS_CODE)));
                if(respMessage.getAttribute(BillingMessage.STATUS_MESSAGE) != null)
                    umcGateTransaction.setStatusMessage(respMessage.getAttributeString(BillingMessage.STATUS_MESSAGE));
                entityManager.SAVE(umcGateTransaction);
            }
            catch(Throwable e){
                logger.error(e.getLocalizedMessage(), e);
            }
        }
    }

    /** STEP1: check is msisdn valid */
    private UmcResponse checkIsMsisdnValid(UmcGateTransaction umcGateTransaction,
                                           BillingMessage billingMessage,
                                           BillingMessageWritable respMessage) throws HttpsGateException{
        UmcResponse response = null;
        try{
            response = checkIsMsisdnValid0(
                    properties.getProperty(PROP_URL),
                    properties.getProperty(PROP_USERNAME),
                    properties.getProperty(PROP_PASSWORD),
                    umcGateTransaction.getMsisdn());
        }
        catch(HttpsRequestException e){
            logger.error("Check msisdn failed", e);
            respMessage.setStatusCode(StatusDictionary.FAILED_TO_CHECK_MSISDN);
            respMessage.setAttribute(BillingMessage.STATUS_MESSAGE, "Failed to check msisdn");
            throw new HttpsGateException("err.umc_https_gate.msisdn_not_valid_or_check_failed", e);
        }

        // handle possible error
        umcGateTransaction.setPhoneType(response.getPhoneType());
        if(response.getPhoneType().longValue() != 1 && response.getPhoneType().longValue() != 2){

            //then error specified in 'error' attribute
            umcGateTransaction.setError(response.getError());
            respMessage.setStatusCode(StatusDictionary.MSISDN_NOT_VALID);
            respMessage.setAttribute(
                    BillingMessage.STATUS_MESSAGE,
                    obtainOperatorI18nMessage(
                            billingMessage.getAttributeString(BillingMessage.SERVICE_CODE),
                            MSG_TYPE_ERROR,
                            response.getError(),
                            null)
            );
            throw new HttpsGateException("err.umc_https_gate.msisdn_not_valid_or_check_failed",
                    new String[]{String.valueOf(response.getError())});
        }
        return response;
    }

    /** STEP2: insistent payment request
     * @param steps2_3Attempt the number of attempts to redo step 1 and 2 on service unavailable */
    private UmcResponse paymentRequest(UmcGateTransaction umcGateTransaction,
                                       BillingMessage billingMessage,
                                       BillingMessageWritable respMessage,
                                       int steps2_3Attempt) throws HttpsGateException{
        UmcResponse response = null;
        int paymentRequestMaxAttempt = Integer.parseInt(properties.getProperty(PROP_PAYREQ_MAX_COUNT, "1"));
        paymentRequestMaxAttempt = paymentRequestMaxAttempt <= 0 ? 1 : paymentRequestMaxAttempt;
        boolean isRequestDelivered = false;
        final String accountNumber = umcGateTransaction.getMsisdn() != null ? umcGateTransaction.getMsisdn() : umcGateTransaction.getPayAccountNumber();
        for(int i=0; i<paymentRequestMaxAttempt && !isRequestDelivered; i++){
            try{
                umcGateTransaction.setPartnerId(resolvePartnerId(umcGateTransaction.getBillTransactionNum(), steps2_3Attempt, i));

                response = paymentRequest0(
                    properties.getProperty(PROP_URL),
                    properties.getProperty(PROP_USERNAME),
                    properties.getProperty(PROP_PASSWORD),
                    accountNumber,
                    umcGateTransaction.getAmount(),
                    umcGateTransaction.getTerminal(),
                    umcGateTransaction.getPartnerId());
                isRequestDelivered = true;
            }
            catch(HttpsRequestException e){
                logger.error("Payment request failed on #"+i+" attempt, try again. Error: ", e);
                try{
                    if(i < paymentRequestMaxAttempt-1)
                        Thread.sleep(1000);
                }
                catch(InterruptedException ei){
                    logger.error("Failed to sleep after erroneous attempt ", ei);
                }
            }
        }                          
        if(!isRequestDelivered){
            logger.error("Max paymentRequest0 attempts exceeded");
            respMessage.setStatusCode(StatusDictionary.REQUEST_PAYMENT_ERROR);
            respMessage.setAttribute(BillingMessage.STATUS_MESSAGE, "Failed to make paymentRequest0 in " + paymentRequestMaxAttempt + " attempts");
            throw new HttpsGateException("err.umc_https_gate.payment_request_failed_after_maximum_attempts_exceeded", new String[]{String.valueOf(paymentRequestMaxAttempt)});
        }


        // handle possible error (if transId <= 0)
        umcGateTransaction.setTransId(response.getTransId());
        if(response.getTransId() == null ||
           response.getTransId().equals("0") ||
           response.getTransId().startsWith("-")){

            //then error specified in 'error' attribute
            umcGateTransaction.setError(response.getError());
            respMessage.setStatusCode(StatusDictionary.REQUEST_PAYMENT_ERROR);
            respMessage.setAttribute(
                    BillingMessage.STATUS_MESSAGE,
                    obtainOperatorI18nMessage(
                            billingMessage.getAttributeString(BillingMessage.SERVICE_CODE),
                            MSG_TYPE_ERROR,
                            response.getError(),
                            null)
            );
            throw new HttpsGateException("err.umc_https_gate.payement_request_failed",
                    new String[]{String.valueOf(response.getError())});
        }
        respMessage.setAttribute(BillingMessage.PAY_ID, umcGateTransaction.getTransId());
        respMessage.setStatusCode(StatusDictionary.STATUS_OK);
        return response;
    }


    /** STEP3: insistent confirm payment */
    private UmcResponse confirmPayment(UmcGateTransaction umcGateTransaction,
                                       BillingMessage billingMessage,
                                       BillingMessageWritable respMessage) throws HttpsGateException{

        UmcResponse response = null;
        int confirmPaymentMaxAttempt = Integer.parseInt(properties.getProperty(PROP_CONFPAY_MAX_COUNT, "1"));
        confirmPaymentMaxAttempt = confirmPaymentMaxAttempt <= 0 ? 1 : confirmPaymentMaxAttempt;
        boolean isRequestDelivered = false;
        for (int i = 0; i < confirmPaymentMaxAttempt && !isRequestDelivered; i++) {
            try {
                response = confirmPayment0(
                        properties.getProperty(PROP_URL),
                        properties.getProperty(PROP_USERNAME),
                        properties.getProperty(PROP_PASSWORD),
                        umcGateTransaction.getTransId());

                isRequestDelivered = true;
            }
            catch (HttpsRequestException e) {
                logger.error("Confirm failed on #" + i + " attempt, try again. Error: ", e);
                try {
                    if (i < confirmPaymentMaxAttempt - 1)
                        Thread.sleep(1000);
                }
                catch (InterruptedException ei) {
                    logger.error("Failed to sleep after erroneous attempt ", ei);
                }
            }
        }
        if (!isRequestDelivered) {
            logger.error("Max confirmPayment attempts exceeded");
            respMessage.setStatusCode(StatusDictionary.REQUEST_PAYMENT_ERROR);
            respMessage.setAttribute(BillingMessage.STATUS_MESSAGE, "Failed to make paymentConfirmation in " + confirmPaymentMaxAttempt + " attempts");
            throw new HttpsGateException("err.umc_https_gate.payment_confirmationt_failed_after_maximum_attempts_exceeded", new String[]{String.valueOf(confirmPaymentMaxAttempt)});
        }

        // handle possible error (if result == 0)
        umcGateTransaction.setResult(response.getResult());
        if (response.getResult() == null ||
            response.getResult().longValue() == 0) {

            //then error specified in 'error' attribute
            umcGateTransaction.setError(response.getError());
            respMessage.setStatusCode(StatusDictionary.CONFIRM_PAYMENT_ERROR);
            respMessage.setAttribute(
                    BillingMessage.STATUS_MESSAGE,
                    obtainOperatorI18nMessage(
                            billingMessage.getAttributeString(BillingMessage.SERVICE_CODE),
                            MSG_TYPE_ERROR,
                            response.getError(),
                            null)
            );
            //do not interrupt on error code 100 (service unavailable. The next attempt will be done on higher level
            if(response.getError() != null && response.getError().longValue() != 100)
                throw new HttpsGateException("err.umc_https_gate.payment_confirmation_failed",
                    new String[]{String.valueOf(response.getError())});
        }
        else{
            //transaction closed ok
            respMessage.setStatusCode(StatusDictionary.STATUS_OK);
        }
        return response;
    }

    private UmcResponse checkIsMsisdnValid0(String url,
                                              String userName,
                                              String password,
                                              String msisdn)
            throws HttpsRequestException{

        PostMethod postMethod = new PostMethod(url+"/getPhoneStatus");
        postMethod.addParameter(new NameValuePair("user", userName));
        postMethod.addParameter(new NameValuePair("passwd", password));
        postMethod.addParameter(new NameValuePair("msisdn", msisdn));
        logger.info("checkIsMsisdnValid [msisdn="+msisdn+"]");
        return makeRequest(postMethod);
    }

    private UmcResponse getBallance(String url,
                                    String userName,
                                    String password)
            throws HttpsRequestException {

        PostMethod postMethod = new PostMethod(url + "/getDealerBalance");
        postMethod.addParameter(new NameValuePair("user", userName));
        postMethod.addParameter(new NameValuePair("passwd", password));
        System.out.println("UmcGate.getBallance:: postMethod: " + postMethod);
        return makeRequest(postMethod);
    }

     public Double getBalance(String operatorCode)throws HttpsRequestException, HttpsGateException{
        // cancel transaction
        UmcResponse  payResponse = getBallance(
                properties.getProperty(PROP_URL),
                properties.getProperty(PROP_USERNAME),
                properties.getProperty(PROP_PASSWORD)
        );

        if(payResponse.getError() != null) { // if error
            throw new HttpsGateException(
                    obtainOperatorI18nMessage(
                            operatorCode,
                            MSG_TYPE_ERROR,
                            payResponse.getError(),
                            "Error " + payResponse.getError()),
                    new String[]{String.valueOf(payResponse.getError())});
        }
        return payResponse.getBalance();
    }

    private UmcResponse paymentRequest0(String url,
                                        String userName,
                                        String password,
                                        String msisdn,
                                        Double amount,
                                        String d_term,
                                        String partnerId)
            throws HttpsRequestException{

      //  logger.info("test [amount="+amount+", partnerId="+partnerId+", d_term="+d_term+"]");

        PostMethod postMethod = new PostMethod(url+"/insertPayment");
        postMethod.addParameter(new NameValuePair("user", userName));
        postMethod.addParameter(new NameValuePair("passwd", password));
        postMethod.addParameter(new NameValuePair("msisdn", msisdn));
        postMethod.addParameter(new NameValuePair("amount", new DecimalFormat("###").format(amount.doubleValue())));
        postMethod.addParameter(new NameValuePair("partnerId", partnerId));
        postMethod.addParameter(new NameValuePair("d_term", d_term));
        logger.info("paymentRequest [amount="+amount+", partnerId="+partnerId+", d_term="+d_term+"]");
        return makeRequest(postMethod);
    }

    
    private UmcResponse confirmPayment0(String url,
                                        String userName,
                                        String password,
                                        String transId)
            throws HttpsRequestException{

        PostMethod postMethod = new PostMethod(url+"/confirmPayment");
        postMethod.addParameter(new NameValuePair("user", userName));
        postMethod.addParameter(new NameValuePair("passwd", password));
        postMethod.addParameter(new NameValuePair("transId", transId));
        logger.info("confirmPayment [transId="+transId+"]");
        return makeRequest(postMethod);
    }


    private UmcResponse makeRequest(HttpMethod method)throws HttpsRequestException {
        UmcResponse response = null;
        try{
            final InetAddress localAddress = InetAddress.getByName(properties.getProperty(PROP_LOCAL_ADDRESS, "localhost"));
            final HostConfiguration hostConfiguration = new HostConfiguration();
            hostConfiguration.setLocalAddress(localAddress);
            HttpClient httpClient = new HttpClient();
            httpClient.setHostConfiguration(hostConfiguration);

            httpClient.executeMethod(method);
            if(method.getStatusCode() != 200)
                throw new HttpsRequestException("Incorrect response code from server " + method.getStatusCode());
            String strResp = method.getResponseBodyAsString();
            logger.info(strResp);
            System.out.println("UmcGate.makeRequest:: strResp" + strResp);
            response = UmcResponse.parseResponse(strResp);
        }
        catch(BadResponseException e){
            logger.error(e);
            throw new HttpsRequestException("XML exception " + e, e);
        }
        catch(IOException e){
            logger.error(e);
            throw new HttpsRequestException("IO exception " + e, e);
        }
        finally{
            method.releaseConnection();
        }
        return response;
    }


    private String resolvePartnerId(String billTransactionNum, int step2_3Attempt, int deliveryAttempt){
        return billTransactionNum + step2_3Attempt + deliveryAttempt;
    }
}
