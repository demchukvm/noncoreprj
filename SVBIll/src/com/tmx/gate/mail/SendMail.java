package com.tmx.gate.mail;

import com.tmx.util.Configuration;


public class SendMail {

    public static void sendMessage(String host, int port, String recipient, String sender, String subject, String message, String encoding) throws Exception{
        SMTPSession s = new SMTPSession(host,
                                        port,
                                        (recipient == null) ? "" : recipient,
                                        (sender == null) ? "" : sender,
                                        new String[]{"Subject: "+subject, message},
                                        (encoding == null) ? Configuration.getInstance().getProperty("smtp.default_encoding","KOI8") : encoding);
        s.sendMessage();
    }

    public static void sendMessage(String recipient, String sender, String subject, String message, String encoding) throws Exception{
        Configuration conf = Configuration.getInstance();
        int port = Integer.parseInt(conf.getProperty("smtp.port", "25"));
        String host = conf.getProperty("smtp.host","not_specified");
        sender = (sender == null) ? conf.getProperty("smtp.default_sender") : sender;

        SendMail.sendMessage(host, port, recipient, sender, subject, message, encoding);
    }


    public static void sendMessage(String recipient, String sender, String subject, String message) throws Exception{
        SendMail.sendMessage(recipient, sender, subject, message, null);
    }


}
