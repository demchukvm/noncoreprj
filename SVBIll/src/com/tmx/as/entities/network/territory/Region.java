package com.tmx.as.entities.network.territory;

import com.tmx.as.base.BasicEntity;

import java.io.Serializable;

/**

 */
public class Region extends BasicEntity {
    private Long regionId;
    private String name;
    private String shortName;
    private District district;

    public Long getRegionId() {
        return regionId;
    }

    public void setRegionId(Long regionId) {
        this.regionId = regionId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public District getDistrict() {
        return district;
    }

    public void setDistrict(District district) {
        this.district = district;
    }

    public Serializable getId() {
        return getRegionId();
    }

    public int getValid() {
        return valid;
    }

    public void setValid(int valid) {
        this.valid = valid;
    }
}
