package com.tmx.web.controls;

import org.apache.struts.upload.FormFile;

/**

 */
public class FileUpload extends BasicControl{
    public static String FILE_SUFFIX = "uploadedFile";//shoud be the same as FormFile property name 
    private FormFile uploadedFile;

    public void initialize() {
    }

    //---------Properties
    public FormFile getUploadedFile() {
        return uploadedFile;
    }

    public void setUploadedFile(FormFile uploadedFile) {
        this.uploadedFile = uploadedFile;
    }

    public String getUploadedFileName(){
        return uploadedFile != null ? uploadedFile.getFileName() : "";
    }
}
