package com.tmx.web.forms.core.tariff;

import com.tmx.as.entities.general.user.User;
import com.tmx.web.controls.table.ControlManagerTable;
import com.tmx.web.forms.core.base.AbstractBrowseForm;

/**
 * For tariffs
 */
public class BrowseFunctionTypesForm extends AbstractBrowseForm {

    public void setUpCategory(String category) {
        setBrowseTable(new ControlManagerTable("query(query_type=retrieve_all," +
                "entity_class=com.tmx.as.entities.bill.function.FunctionType," +
                "filter(name=by_category, parameter(name=category, value=" + category + ", type=String))," +
                "order(name=id,type=asc)," +
                "page_size=30" +
                ")"));
    }

    public void bindData(User loggedInUser) throws Throwable {
        getBrowseTable().refresh();
    }
}
