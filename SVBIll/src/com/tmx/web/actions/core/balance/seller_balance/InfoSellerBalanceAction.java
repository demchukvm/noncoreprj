package com.tmx.web.actions.core.balance.seller_balance;

import com.tmx.as.blogic.balance.AbstractBalanceManager;
import com.tmx.as.blogic.balance.SellerBalanceManager;
import com.tmx.as.blogic.OperatorManager;
import com.tmx.as.blogic.TransactionManager;
import com.tmx.as.entities.bill.balance.AbstractBalance;
import com.tmx.as.entities.bill.balance.SellerBalance;
import com.tmx.as.entities.bill.transaction.ClientTransaction;
import com.tmx.as.entities.bill.operator.Operator;
import com.tmx.as.entities.general.user.User;
import com.tmx.as.exceptions.DatabaseException;
import com.tmx.as.base.criterion.CriterionWrapper;
import com.tmx.as.base.criterion.Restrictions;
import com.tmx.as.base.EntityManager;
import com.tmx.beng.base.BillException;
import com.tmx.util.InitException;
import com.tmx.web.actions.core.balance.AbstractInfoBalanceAction;
import com.tmx.web.controls.table.Parameter;
import com.tmx.web.controls.CriterionFactory;
import com.tmx.web.controls.ScrollBox;
import com.tmx.web.forms.core.balance.AbstractInfoBalanceForm;
import com.tmx.web.forms.core.balance.seller_balance.InfoSellerBalanceForm;
import com.tmx.web.forms.core.base.AbstractFormFactory;

import java.util.List;
import java.util.ArrayList;


public class InfoSellerBalanceAction extends AbstractInfoBalanceAction {


    protected String getManageAction() {
        return "manageSellerBalancesAction";
    }

    protected String getInfoPage() {
        return "infoSellerBalancePage";
    }

    protected String getManageDeleteAction() {
        return "manageDeleteAction";
    }

    protected String getInfoTransactionAction() {
        return "/core/transactions/seller_transaction/infoSellerTransaction";
    }

    protected AbstractBalanceManager getBalanceManager() throws InitException, BillException {
        return new SellerBalanceManager();
    }

    protected void preloadForUpdate(AbstractInfoBalanceForm form, AbstractBalance balance, User user) throws DatabaseException {
        final InfoSellerBalanceForm sellerBalanceForm = (InfoSellerBalanceForm) form;
        AbstractFormFactory.getInstance(user).setUpForm(sellerBalanceForm);
        SellerBalance sellerBalance = (SellerBalance) balance;
        sellerBalanceForm.setSellerId(sellerBalance.getSellerId());
        form.getTransactionsTable().addParameterToFilter("by_seller_balance_id", new Parameter("seller_balance_id", sellerBalance.getBalanceId()));
        form.getTransactionsTable().addParameterToFilter("by_seller", new Parameter("seller", sellerBalance.getSellerId()));

        sellerBalanceForm.getScrollBoxOperator().bind(new OperatorManager().getAllOperators());

//        filterByOperator(sellerBalanceForm);
    }

    private void filterByOperator(InfoSellerBalanceForm sellerBalanceForm) throws DatabaseException {
        if (!sellerBalanceForm.getScrollBoxOperator().getSelectedKey().equals(ScrollBox.NULL_KEY)) {

//            CriterionWrapper[] criterions = new CriterionWrapper[] {
//                    Restrictions.eq("operator", new OperatorManager().getOperator(
//                            new Long(sellerBalanceForm.getScrollBoxOperator().getSelectedKey())))
//            };
//            final List<ClientTransaction> transactions = new EntityManager().RETRIEVE_ALL(ClientTransaction.class, criterions, null);
//            final List<Long> transactionIds = new ArrayList<Long>();
//            for (ClientTransaction transaction : transactions) {
//              transactionIds.add(transaction.getTansactionId());
//            }
            final Operator operator = new OperatorManager().getOperator(
                    new Long(sellerBalanceForm.getScrollBoxOperator().getSelectedKey()));
            CriterionFactory criterionFactory = new CriterionFactory() {
                public CriterionWrapper createCriterion() {
                    return Restrictions.eq("clientTransaction.operator", operator);
                }
            };
            sellerBalanceForm.getTransactionsTable().setCriterionFactory(criterionFactory);
        }
    }

    protected void preloadForCreate(AbstractInfoBalanceForm form) {
        form.getTransactionsTable().addParameterToFilter("by_seller_balance_id", new Parameter("seller_balance_id", (long) 0));
        form.getTransactionsTable().addParameterToFilter("by_seller", new Parameter("seller", (long) 0));
    }

    protected void preload(AbstractInfoBalanceForm form, AbstractBalance balance) throws Throwable {
        SellerBalance sellerBalance = (SellerBalance)balance;

        if(sellerBalance.getSeller()!=null)
            form.getScrollBoxBalanceOwner().setSelectedKey(sellerBalance.getSeller().getSellerId().toString());
    }

    @Override
    protected TransactionManager.CashCalculation getCashCalculation(AbstractInfoBalanceForm form) throws DatabaseException {
        return (new TransactionManager()).getCashCalculation((InfoSellerBalanceForm)form);
    }


}
