package com.tmx.web.base.authentication;

import java.util.List;

public interface CommandFragmentMap {

    public List getCommandFragments(String actionName, String commandName);

}
