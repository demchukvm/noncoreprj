package com.tmx.as.entities.bill.transaction;

import com.tmx.as.base.BasicEntity;

import java.util.Date;
import java.io.Serializable;

/**
 */
public abstract class AbstractTransaction extends BasicEntity{
    private Long tansactionId;
    private Date transactionTime;
    private Date clientTime;    
    private String transactionGUID;
    private String clitransactionGUID;
    private String cliTransactionNum;
    private Double amount;
    private TransactionType type;
    private OperationType operationType;
    private TransactionSupport transactionSupport;
    private String status;
    private TransactionError error;
    private ClientTransaction clientTransaction;

    //------------------Properties

    public Long getTansactionId() {
        return tansactionId;
    }

    public void setTansactionId(Long tansactionId) {
        this.tansactionId = tansactionId;
    }

    public Date getTransactionTime() {
        return transactionTime;
    }

    public void setTransactionTime(Date transactionTime) {
        this.transactionTime = transactionTime;
    }

    public Date getClientTime() {
        return clientTime;
    }

    public void setClientTime(Date clientTime) {
        this.clientTime = clientTime;
    }

    public String getTransactionGUID() {
        return transactionGUID;
    }

    public void setTransactionGUID(String transactionGUID) {
        this.transactionGUID = transactionGUID;
    }
    public String getClitransactionGUID() {
        return clitransactionGUID;
    }

    public void setClitransactionGUID(String clitransactionGUID) {
        this.clitransactionGUID = clitransactionGUID;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public TransactionType getType() {
        return type;
    }

    public void setType(TransactionType type) {
        this.type = type;
    }

    public OperationType getOperationType() {
        return operationType;
    }

    public void setOperationType(OperationType operationType) {
        this.operationType = operationType;
    }

    public TransactionSupport getTransactionSupport() {
        return transactionSupport;
    }

    public void setTransactionSupport(TransactionSupport transactionSupport) {
        this.transactionSupport = transactionSupport;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public TransactionError getError() {
        return error;
    }

    public void setError(TransactionError error) {
        this.error = error;
    }


    public String getCliTransactionNum() {
        return cliTransactionNum;
    }

    public void setCliTransactionNum(String cliTransactionNum) {
        this.cliTransactionNum = cliTransactionNum;
    }

    public ClientTransaction getClientTransaction() {
        return clientTransaction;
    }

    public void setClientTransaction(ClientTransaction clientTransaction) {
        this.clientTransaction = clientTransaction;
    }

    public Serializable getId() {
        return getTansactionId();
    }

    public int getValid() {
        return valid;
    }

    public void setValid(int valid) {
        this.valid = valid;
    }

}
