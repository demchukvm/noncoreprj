package com.tmx.beng.base.dbfunctions;

import java.util.List;
import java.util.Arrays;
import java.util.LinkedList;


public class AllOperatorsVoucherTariff extends VoucherTariff{
    protected List<String> getServiceCodes() {
        return Arrays.asList("BeelineVoucher5", "BeelineVoucher15", "BeelineVoucher30", "BeelineVoucher60", "BeelineVoucher90",
                                "KyivstarVoucher30", "KyivstarVoucher100","KyivstarVoucher300",
                                "LifeVoucher10", "LifeVoucher25", "LifeVoucher50", "LifeVoucher100", "LifeVoucher35",
                                "UmcVoucher25", "UmcVoucher30", "UmcVoucher50", "UmcVoucher100",
                                "IntertelecomVoucher100", "IntertelecomVoucher25", "IntertelecomVoucher50",
                                "PeopleNetVoucher10","PeopleNetVoucher100","PeopleNetVoucher20","PeopleNetVoucher50",
                                "UtelVoucher20","UtelVoucher50", "UtelVoucher10");
    }
}
