package com.tmx.web.controls;

/**
 */
public class ExpandingPanel extends Panel{
    private boolean expanded = true;//by default it is expanded

    public void init() {
        //do nothing
    }

    //-------------------Properties

    public boolean isExpanded() {
        return expanded;
    }

    public void setExpanded(boolean expanded) {
        this.expanded = expanded;
    }
}
