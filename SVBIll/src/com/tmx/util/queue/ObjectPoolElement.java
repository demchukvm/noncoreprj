package com.tmx.util.queue;

public interface ObjectPoolElement {
    Object newPoolObjectInstance();
}
