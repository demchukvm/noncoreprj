<%@ page language="java" pageEncoding="UTF-8" contentType="text/html; charset=utf-8"
         errorPage="/core/jsp/message/jsperror.jsp" isELIgnored="false" %>
<%@ taglib uri="/WEB-INF/tld/struts-nested.tld" prefix="nested" %>
<%@ taglib uri="/WEB-INF/tld/tmx-controls.tld" prefix="tmx_ctrl" %>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>


<html:form action="/core/restriction/seller_restriction/manageSellerRestrictions">
<table width="100%" border="0">
    <bean:define id="form" name="core.restriction.seller_restriction.manageSellerRestrictionsForm" type="com.tmx.web.forms.core.restriction.seller_restriction.ManageSellerRestrictionsForm"/>
    <tr>
        <td colspan="2">
            <h4 class="dataLabel">
                <bean:message bundle="core.labels" key="core.restriction.sellerrestriction.managesellerrestrictionform.titleIncomingRestriction"/>
                    <c:out value="${form.selectedIncomingSellerName}"/>
            </h4>
        </td>
    </tr>
    <!-- BUTTONS -->
    <tr>
        <td style="text-align: left">
            <tmx_ctrl:button property="onCopyButton"/>
            <tmx_ctrl:button property="onCopyAllButton"/>
            <tmx_ctrl:button property="onDeleteIRButton"/>
            <tmx_ctrl:button property="onCreateIRButton"/>
            <tmx_ctrl:button property="onEditIRButton"/>
        </td>
    </tr>


    <tr>
        <td valign="top" width="100%" align="left">
            <tmx_ctrl:panelFilterSet tableName="incomingRestrictionsTable">
                <table>
                    <tr>
                        <td class="dataLabel">
                            <tmx_ctrl:writeEntityLabel
                                    attrName="{@name}"
                                    entityClassName="com.tmx.as.entities.bill.restriction.SellerRestriction"/>:
                        </td>
                        <td class="dataField">
                            <tmx_ctrl:selectBox
                                    property="incomingRestrictionsTable.filterSet.filter(by_restriction_id).parameter(restriction_id).control"/>
                        </td>

                        <td class="dataLabel">
                            <tmx_ctrl:writeEntityLabel
                                    attrName="{@name}"
                                    entityClassName="com.tmx.as.entities.bill.seller.Seller"/>
                        </td>
                        <td class="dataField">
                            <tmx_ctrl:selectBox
                                    property="incomingRestrictionsTable.filterSet.filter(by_seller_id).parameter(seller_id).control"/>
                        </td>
                    </tr>
                </table>
            </tmx_ctrl:panelFilterSet>
        </td>
    </tr>

    <tr>
        <td>
        <table class='one_outer_cell_table' cellspacing='0' cellpadding='0' width="100%">
            <tr>
                <td>
                    <table cellspacing='1' cellpadding='1' width="100%">
                        <!-- Table header -->
                        <tr>
                            <th class="column_header_cell" width="110">
                                <bean:message bundle="core.labels" key="core.common.th.choose"/>
                            </th>

                            <th class="column_header_cell" width="110">
                                <tmx_ctrl:imgTableOrdering tableName="restrictionsTable"
                                                           orderByFieldName="name"
                                                           labelBundle="as:com.tmx.as.entities.bill.restriction.SellerRestriction"
                                                           labelKey="{@name}"/>
                            </th>

                            <th class="column_header_cell" width="110">
                                <tmx_ctrl:imgTableOrdering tableName="restrictionsTable"
                                                           orderByFieldName="seller.name"
                                                           labelBundle="as:com.tmx.as.entities.bill.seller.Seller"
                                                           labelKey="{@name}"/>
                            </th>

                            <th class="column_header_cell" width="110">
                                <tmx_ctrl:imgTableOrdering tableName="restrictionsTable"
                                                           orderByFieldName="functionInstance.functionType.name"
                                                           labelBundle="as:com.tmx.as.entities.bill.function.FunctionType"
                                                           labelKey="restrictionType"/>
                            </th>
                            <th class="column_header_cell" width="110">
                                <tmx_ctrl:imgTableOrdering tableName="restrictionsTable"
                                                           orderByFieldName="functionInstance.functionType.name"
                                                           labelBundle="as:com.tmx.as.entities.bill.function.ActualFunctionParameter"
                                                           labelKey="{@name}"/>
                            </th>
                        </tr>
                        <nested:iterate id="sellerRestriction" property="incomingRestrictionsTable.data" type="com.tmx.as.entities.bill.restriction.SellerRestriction">
                            <tr>
                                <td class='column_data_cell'>
                                    <input type="radio" name="incomingRestrictionId" value="<c:out value="${sellerRestriction.id}" />"
                                           checked="false">
                                </td>
                                <td class='column_data_cell'>
                                        <c:out value="${sellerRestriction.name}"/>
                                </td>
                                <td class='column_data_cell'>
                                    <c:if test="${sellerRestriction.seller != null}">
                                        <c:out value="${sellerRestriction.seller.name}" />
                                    </c:if>
                                </td>
                                <td class='column_data_cell'>
                                    <c:if test="${sellerRestriction.functionInstance != null}">
                                        <c:if test="${sellerRestriction.functionInstance.functionType != null}">
                                            <c:out value="${sellerRestriction.functionInstance.functionType.name}" />
                                        </c:if>
                                    </c:if>
                                </td>
                                <td class='column_data_cell'>
                                    <c:if test="${sellerRestriction.functionInstance.functionParameters != null}">
                                        <table>
                                            <c:forEach items="${sellerRestriction.functionInstance.functionParameters}" var="parameter">
                                                <tr>
                                                    <td><c:out value="${parameter.name}"/></td>
                                                    <td> = </td>
                                                    <td><c:out value="${parameter.value}"/></td>
                                                </tr>
                                            </c:forEach>
                                        </table>
                                    </c:if>
                                </td>
                            </tr>
                        </nested:iterate>
                        <!-- Empty row -->
                        <nested:define id="tableSize" property="incomingRestrictionsTable.resultSize" type="java.lang.Integer"/>
                        <c:if test="${tableSize <= 0}">
                            <tr>
                                <td class='column_data_cell' colspan="4" align="center">
                                    <bean:message bundle="core.labels" key="core.common.th.empty_table" />
                                </td>
                            </tr>
                        </c:if>
                    </table>
                </td>
            </tr>
        </table>
        </td>
    </tr>

    <tr>
        <td style="text-align: right">
            <tmx_ctrl:imgTablePaging maxBookmarkCount="10" tableName="incomingRestrictionsTable"/>
        </td>
    </tr>

    <tr>
        <td colspan="2">
            <h4 class="dataLabel">
                <bean:message bundle="core.labels" key="core.restriction.sellerrestriction.managesellerrestrictionform.titleCustomizableRestriction"/>
                <c:out value="${form.selectedCustomizableSellerName}"/>
            </h4>
        </td>
    </tr>

    <!--table-->
    <tr>
        <td valign="top" width="100%" align="left">
            <tmx_ctrl:panelFilterSet tableName="restrictionsTable">
            <table>
                <tr>
                    <td class="dataLabel">
                        <tmx_ctrl:writeEntityLabel
                                attrName="{@name}"
                                entityClassName="com.tmx.as.entities.bill.restriction.SellerRestriction"/>:
                    </td>
                    <td class="dataField">
                        <tmx_ctrl:editBox
                                property="restrictionsTable.filterSet.filter(by_restriction_name).parameter(restriction_name).control"/>
                    </td>

                    <td class="dataLabel">
                        <tmx_ctrl:writeEntityLabel
                                attrName="subordinateSeller"
                                entityClassName="com.tmx.as.entities.bill.restriction.SellerRestriction"/>:
                    </td>
                    <td class="dataField">
                        <tmx_ctrl:selectBox
                                property="restrictionsTable.filterSet.filter(by_seller_id).parameter(seller_id).control"/>
                    </td>
                </tr>
            </table>
        </tmx_ctrl:panelFilterSet>
        </td>
    </tr>

    <tr>
        <td>
        <table class='one_outer_cell_table' cellspacing='0' cellpadding='0' width="100%">
            <tr>
                <td>
                    <table cellspacing='1' cellpadding='1' width="100%">
                        <!-- Table header -->
                        <tr>
                            <th class="column_header_cell" width="110">
                                <bean:message bundle="core.labels" key="core.common.th.choose"/>
                            </th>

                            <th class="column_header_cell" width="110">
                                <tmx_ctrl:imgTableOrdering tableName="restrictionsTable"
                                                           orderByFieldName="name"
                                                           labelBundle="as:com.tmx.as.entities.bill.restriction.SellerRestriction"
                                                           labelKey="{@name}"/>
                            </th>

                            <th class="column_header_cell" width="110">
                                <tmx_ctrl:imgTableOrdering tableName="restrictionsTable"
                                                           orderByFieldName="seller.name"
                                                           labelBundle="as:com.tmx.as.entities.bill.seller.Seller"
                                                           labelKey="{@name}"/>
                            </th>

                            <th class="column_header_cell" width="110">
                                <tmx_ctrl:imgTableOrdering tableName="restrictionsTable"
                                                           orderByFieldName="functionInstance.functionType"
                                                           labelBundle="as:com.tmx.as.entities.bill.function.FunctionType"
                                                           labelKey="restrictionType"/>
                            </th>
                            <th class="column_header_cell" width="110">
                                <tmx_ctrl:imgTableOrdering tableName="restrictionsTable"
                                                           orderByFieldName="functionInstance.functionType.name"
                                                           labelBundle="as:com.tmx.as.entities.bill.function.ActualFunctionParameter"
                                                           labelKey="{@name}"/>
                            </th>
                        </tr>
                        <nested:iterate id="sellerRestriction" property="restrictionsTable.data" type="com.tmx.as.entities.bill.restriction.SellerRestriction">
                            <tr>
                                <td class='column_data_cell'>
                                    <input type="radio" name="restrictionId" value="<c:out value="${sellerRestriction.id}" />"
                                           checked="false">
                                </td>
                                <td class='column_data_cell'>
                                        <c:out value="${sellerRestriction.name}"/>
                                </td>
                                <td class='column_data_cell'>
                                    <c:if test="${sellerRestriction.seller != null}">
                                        <c:out value="${sellerRestriction.seller.name}" />
                                    </c:if>
                                </td>
                                <td class='column_data_cell'>
                                    <c:if test="${sellerRestriction.functionInstance != null}">
                                        <c:if test="${sellerRestriction.functionInstance.functionType != null}">
                                            <c:out value="${sellerRestriction.functionInstance.functionType.name}" />
                                        </c:if>
                                    </c:if>
                                </td>
                                <td class='column_data_cell'>
                                    <c:if test="${sellerRestriction.functionInstance.functionParameters != null}">
                                        <table>
                                            <c:forEach items="${sellerRestriction.functionInstance.functionParameters}" var="parameter">
                                                <tr>
                                                    <td><c:out value="${parameter.name}"/></td>
                                                    <td> = </td>
                                                    <td><c:out value="${parameter.value}"/></td>
                                                </tr>
                                            </c:forEach>
                                        </table>
                                    </c:if>
                                </td>
                            </tr>
                        </nested:iterate>
                        <!-- Empty row -->
                        <nested:define id="tableSize" property="restrictionsTable.resultSize" type="java.lang.Integer"/>
                        <c:if test="${tableSize <= 0}">
                            <tr>
                                <td class='column_data_cell' colspan="5" align="center">
                                    <bean:message bundle="core.labels" key="core.common.th.empty_table" />
                                </td>
                            </tr>
                        </c:if>
                    </table>
                </td>
            </tr>
        </table>
        </td>
    </tr>

    <tr>
        <td style="text-align: right">
            <tmx_ctrl:imgTablePaging maxBookmarkCount="10" tableName="restrictionsTable"/>
        </td>
    </tr>


    <!-- BUTTONS -->
    <tr>
        <td style="text-align: left">
            <tmx_ctrl:button property="onCreateButton"/>
            <tmx_ctrl:button property="onEditButton"/>
            <tmx_ctrl:button property="onDeleteButton"/>
        </td>
    </tr>

</table>
</html:form>