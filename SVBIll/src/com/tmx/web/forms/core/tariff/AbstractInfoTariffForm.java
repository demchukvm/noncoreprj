package com.tmx.web.forms.core.tariff;

import com.tmx.as.blogic.BlogicException;
import com.tmx.as.blogic.FunctionManager;
import com.tmx.as.entities.bill.function.FunctionInstance;
import com.tmx.as.entities.bill.function.FunctionType;
import com.tmx.as.entities.bill.tariff.AbstractTariff;
import com.tmx.as.entities.general.user.User;
import com.tmx.as.exceptions.DatabaseException;
import com.tmx.util.InitException;
import com.tmx.web.actions.core.tariff.AbstractManageTariffsAction;
import com.tmx.web.base.WebException;
import com.tmx.web.controls.*;
import com.tmx.web.controls.table.ControlManagerTable;

import java.util.HashSet;


abstract public class AbstractInfoTariffForm extends TransportParameterForm {

    private Long tariffId;//tariffId == null if form create
    private Long incomingTariffId;//tariffId == null if form create
    private Long funcInstanceId;// for AbstractParameterPage

    //buttons
    private Button onApplyButton;
    private Button onSaveButton;
    private Button onResetButton;
    private Button onCancelButton;

    //param buttons
    private Button onRefreshParamButton;
    private Button onCreateParamButton;
    private Button onEditParamButton;
    private Button onDeleteParamButton;

    //time selectors
    private TimeSelector timeSelectorActivation;
    private TimeSelector timeSelectorDeactivation;

    //table
    private ControlManagerTable funcParametrsTable;
    private ControlManagerTable paramErrorTable;
    private ControlManagerTable defaultFuncParametrsTable;

    //edit boxes
    private EditBox editBoxTariffId;
    private EditBox editBoxName;
    private EditBox editBoxFunctionInstanceId;

    private BrowseBox browseBoxFunctionTypes;

    private TextArea textAreaFuncDescription;

    //------------------template methods------------------------------------
    abstract protected AbstractTariff populateFromSpecificControls();
    abstract protected void bindSpecificData(AbstractTariff tariff, User loggedInUser) throws DatabaseException;
    abstract protected void bindSpecificData(User loggedInUser) throws DatabaseException;
    //----------------------------------------------------------------------

    protected void init() {
        initEditBoxes();
        initButtons();
        initTables();
        initTimeSelectors();
        initTextAreas();
        initBrowseBoxes();
    }

    private void initTextAreas(){
        textAreaFuncDescription = new TextArea();
        textAreaFuncDescription.setReadonly(true);
    }

    private void initBrowseBoxes(){
        browseBoxFunctionTypes = new BrowseBox();
        browseBoxFunctionTypes.setMandatory(true);
    }

    private void initTables(){
        funcParametrsTable = new ControlManagerTable("query(query_type=retrieve_all," +
                "entity_class=com.tmx.as.entities.bill.function.ActualFunctionParameter," +
                "table_attr=functionInstance.functionType," +
                "order(name=actualFunctionParameterId,type=asc)," +
                "page_size=25" +
                ")");
        funcParametrsTable.addFilter("by_function_instance_id");
        funcParametrsTable.setRefreshCommandName("refreshFuncParametrsParamTable");
        funcParametrsTable.addDependedControl(onDeleteParamButton).addDependedControl(onEditParamButton);

        defaultFuncParametrsTable = new ControlManagerTable();
        paramErrorTable = new ControlManagerTable();
    }

    private void initTimeSelectors(){
        timeSelectorActivation = new TimeSelector();
        timeSelectorDeactivation = new TimeSelector();
    }

    private void initEditBoxes(){
        editBoxTariffId = new EditBox();
        editBoxTariffId.setReadonly(true);
        editBoxName = new EditBox();
        editBoxName.setMandatory(true);

        editBoxFunctionInstanceId = new EditBox();
        editBoxFunctionInstanceId.setReadonly(true);
    }

    private void initButtons(){
        onSaveButton = new Button();
        onSaveButton.setLabelKey("ctrl.button.label.save");
        onSaveButton.setCommand("save");

        onResetButton = new Button();
        onResetButton.setLabelKey("ctrl.button.label.reset");

        onCancelButton = new Button();
        onCancelButton.setLabelKey("ctrl.button.label.cancel");
        onCancelButton.setCommand("preloadForUpdate");

        onApplyButton = new Button();
        onApplyButton.setLabelKey("ctrl.button.label.apply");
        onApplyButton.setCommand("apply");

        //---param buttons----------------------------------
        onRefreshParamButton = new Button();
        onRefreshParamButton.setLabelKey("ctrl.button.label.refresh");
        onRefreshParamButton.setCommand("refreshDefaultParams");

        onCreateParamButton = new Button();
        onCreateParamButton.setLabelKey("ctrl.button.label.create");
        onCreateParamButton.setCommand("createParam");

        onEditParamButton = new Button();
        onEditParamButton.setLabelKey("ctrl.button.label.edit");
        onEditParamButton.setCommand("preloadForUpdate");
        onEditParamButton.setReadonly(true);

        onDeleteParamButton = new Button();
        onDeleteParamButton.setLabelKey("ctrl.button.label.delete");
        onDeleteParamButton.setCommand("preloadForDelete");
        onDeleteParamButton.setReadonly(true);
    }

    public AbstractTariff populateFromControls(){
        AbstractTariff tariff = populateFromSpecificControls();

        tariff.setTariffId(PopulateUtil.getLongValue(editBoxTariffId));
        tariff.setName(PopulateUtil.getStringValue(editBoxName));
        
        tariff.setActivationDate(timeSelectorActivation.getTime());
        tariff.setDeactivationDate(timeSelectorDeactivation.getTime());

        Long instanceId = PopulateUtil.getLongValue(editBoxFunctionInstanceId);
        tariff.setFunctionInstance(populateFunctionInstance(instanceId));

        return tariff;
    }

    private FunctionInstance populateFunctionInstance(Long instanceId) {
        FunctionInstance functionInstance = new FunctionInstance();
        functionInstance.setFunctionType(populateFunctionType());
        functionInstance.setFunctionInstanceId(instanceId);
        functionInstance.setFunctionParameters(new HashSet(funcParametrsTable.getData()));
        return functionInstance;
    }

    private FunctionType populateFunctionType() {
        FunctionType functionType = new FunctionType();
        functionType.setFunctionTypeId(PopulateUtil.getLongKey(browseBoxFunctionTypes));
        functionType.setName(browseBoxFunctionTypes.getValue());
        return functionType;
    }

    /**
     * for update...
     */
    public void bindData(AbstractTariff tariff, User loggedInUser) throws DatabaseException, WebException {
        reset();
        bindEditBoxes(tariff);
        bindBrowseBoxes(tariff);
        bindValueForPopulating(tariff);
        bindTimeSelectors(tariff);
        bindTextAreas(tariff);
        bindSpecificData(tariff, loggedInUser);
        bindButtonActions();
    }

    private void bindValueForPopulating(AbstractTariff tariff) {
        funcInstanceId = tariff.getFunctionInstance().getFunctionInstanceId();
        tariffId = tariff.getTariffId();
    }

    private void bindTextAreas(AbstractTariff tariff) {
        textAreaFuncDescription.setValue(tariff.getFunctionInstance().getFunctionType().getDescription());
    }

    private void bindTimeSelectors(AbstractTariff tariff) {
        timeSelectorActivation.setTime(tariff.getActivationDate());
        timeSelectorDeactivation.setTime(tariff.getDeactivationDate());
    }

    private void bindBrowseBoxes(AbstractTariff tariff) {
        String selectedKey = PopulateUtil.getString(tariff.getFunctionInstance().getFunctionType().getFunctionTypeId());
        browseBoxFunctionTypes.setKey(selectedKey);
        browseBoxFunctionTypes.setValue(tariff.getFunctionInstance().getFunctionType().getName());
    }

    private void bindEditBoxes(AbstractTariff tariff) {
        editBoxTariffId.setValue(PopulateUtil.getString(tariff.getTariffId()));
        editBoxName.setValue(tariff.getName());
        editBoxFunctionInstanceId.setValue(PopulateUtil.getString(tariff.getFunctionInstance().getFunctionInstanceId()));
    }

    /**
     *  for create, when AbstractTariff object is absent
     */
    public void bindData(User loggedInUser) throws DatabaseException, BlogicException, InitException, WebException {
        reset();
        bindFunctionTypeDescription();
        bindSpecificData(loggedInUser);
        bindButtonActions();//from roadmap
    }

    //from roadmap
    private void bindButtonActions() throws WebException {
        String action = (String) getRoadmap().getParameterValue(AbstractManageTariffsAction.DEFAULT_VALID_ACTION,"onCancelAction", true);
        getOnCancelButton().setAction(action);
    }

    public void bindFunctionTypeDescription() throws DatabaseException, BlogicException, InitException {
        if(browseBoxFunctionTypes.getKey() != null){
            FunctionType functionType = new FunctionManager().getFunctionType(browseBoxFunctionTypes.getKey());
            if(getFuncParametrsTable() != null)
            textAreaFuncDescription.setValue(functionType.getDescription());
        }
    }

    public Long getTariffId() {
        return tariffId;
    }

    public void setTariffId(Long tariffId) {
        this.tariffId = tariffId;
    }


    public Button getOnApplyButton() {
        return onApplyButton;
    }

    public void setOnApplyButton(Button onApplyButton) {
        this.onApplyButton = onApplyButton;
    }

    public Button getOnSaveButton() {
        return onSaveButton;
    }

    public void setOnSaveButton(Button onSaveButton) {
        this.onSaveButton = onSaveButton;
    }

    public Button getOnResetButton() {
        return onResetButton;
    }

    public void setOnResetButton(Button onResetButton) {
        this.onResetButton = onResetButton;
    }

    public Button getOnCancelButton() {
        return onCancelButton;
    }

    public void setOnCancelButton(Button onCancelButton) {
        this.onCancelButton = onCancelButton;
    }

    public Button getOnCreateParamButton() {
        return onCreateParamButton;
    }

    public void setOnCreateParamButton(Button onCreateParamButton) {
        this.onCreateParamButton = onCreateParamButton;
    }

    public Button getOnEditParamButton() {
        return onEditParamButton;
    }

    public void setOnEditParamButton(Button onEditParamButton) {
        this.onEditParamButton = onEditParamButton;
    }

    public Button getOnDeleteParamButton() {
        return onDeleteParamButton;
    }

    public void setOnDeleteParamButton(Button onDeleteParamButton) {
        this.onDeleteParamButton = onDeleteParamButton;
    }

    public ControlManagerTable getFuncParametrsTable() {
        return funcParametrsTable;
    }

    public void setFuncParametrsTable(ControlManagerTable funcParametrsTable) {
        this.funcParametrsTable = funcParametrsTable;
    }

    public EditBox getEditBoxTariffId() {
        return editBoxTariffId;
    }

    public void setEditBoxTariffId(EditBox editBoxTariffId) {
        this.editBoxTariffId = editBoxTariffId;
    }

    public EditBox getEditBoxName() {
        return editBoxName;
    }

    public void setEditBoxName(EditBox editBoxName) {
        this.editBoxName = editBoxName;
    }

    public EditBox getEditBoxFunctionInstanceId() {
        return editBoxFunctionInstanceId;
    }

    public void setEditBoxFunctionInstanceId(EditBox editBoxFunctionInstanceId) {
        this.editBoxFunctionInstanceId = editBoxFunctionInstanceId;
    }

    public Long getFuncInstanceId() {
        return funcInstanceId;
    }

    public TextArea getTextAreaFuncDescription() {
        return textAreaFuncDescription;
    }

    public void setTextAreaFuncDescription(TextArea textAreaFuncDescription) {
        this.textAreaFuncDescription = textAreaFuncDescription;
    }

    public void setFuncInstanceId(Long funcInstanceId) {
        this.funcInstanceId = funcInstanceId;
    }

    public TimeSelector getTimeSelectorActivation() {
        return timeSelectorActivation;
    }

    public void setTimeSelectorActivation(TimeSelector timeSelectorActivation) {
        this.timeSelectorActivation = timeSelectorActivation;
    }

    public TimeSelector getTimeSelectorDeactivation() {
        return timeSelectorDeactivation;
    }

    public void setTimeSelectorDeactivation(TimeSelector timeSelectorDeactivation) {
        this.timeSelectorDeactivation = timeSelectorDeactivation;
    }

    public BrowseBox getBrowseBoxFunctionTypes() {
        return browseBoxFunctionTypes;
    }

    public void setBrowseBoxFunctionTypes(BrowseBox browseBoxFunctionTypes) {
        this.browseBoxFunctionTypes = browseBoxFunctionTypes;
    }

    public ControlManagerTable getParamErrorTable() {
        return paramErrorTable;
    }

    public void setParamErrorTable(ControlManagerTable paramErrorTable) {
        this.paramErrorTable = paramErrorTable;
    }

    public Button getOnRefreshParamButton() {
        return onRefreshParamButton;
    }

    public void setOnRefreshParamButton(Button onRefreshParamButton) {
        this.onRefreshParamButton = onRefreshParamButton;
    }

    public ControlManagerTable getDefaultFuncParametrsTable() {
        return defaultFuncParametrsTable;
    }

    public void setDefaultFuncParametrsTable(ControlManagerTable defaultFuncParametrsTable) {
        this.defaultFuncParametrsTable = defaultFuncParametrsTable;
    }

    public Long getIncomingTariffId() {
        return incomingTariffId;
    }

    public void setIncomingTariffId(Long incomingTariffId) {
        this.incomingTariffId = incomingTariffId;
    }
}
