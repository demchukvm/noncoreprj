package com.tmx.beng.base.processors;

import com.tmx.beng.base.Task;
import com.tmx.beng.base.BillException;
import com.tmx.beng.base.BillingMessageWritable;
import com.tmx.beng.base.BillingMessage;
import com.tmx.beng.base.beans.ProcessingResolverDocument;
import com.tmx.beng.base.beans.Processings;
import com.tmx.beng.base.cache.DBCache;
import com.tmx.beng.base.cache.DBCacheException;
import com.tmx.as.entities.bill.terminal.Terminal;
import com.tmx.as.entities.bill.seller.Seller;
import com.tmx.util.cache.FileCache;
import com.tmx.util.cache.CacheException;
import com.tmx.util.MD5;

import java.io.InputStream;
import java.util.Set;
import java.util.HashSet;


public class TerminalDefinerProcessor extends TaskProcessor{

    protected Task process0(Task task, DBCache dbCache, BillingMessageWritable msg) throws BillException {
        logger.debug("Terminal definer processor...");
        try {
            String processingCode = msg.getAttributeString(BillingMessage.PROCESSING_CODE);
            if (isProcessingRedefined(processingCode))
                redefineMsg(dbCache, msg);
        } catch (DBCacheException e) {
            throw new BillException("err.processing_resolver.db_cache_err " + e.getMessage(), e);
        } catch (CacheException e) {
            throw new BillException("err.processing_resolver.file_cache_err", e);
        }
        return task;
    }

    private BillingMessageWritable redefineMsg(DBCache dbCache, BillingMessageWritable msg) throws DBCacheException {
        String terminalSN = msg.getAttributeString(BillingMessage.TERMINAL_SN);
        Terminal terminal = dbCache.getTerminal(terminalSN);

        Seller seller = dbCache.getSellerById(terminal.getSellerOwnerId());
        msg.setAttributeString(BillingMessage.SELLER_CODE, seller.getCode());
        msg.setAttributeString(BillingMessage.PROCESSING_CODE, dbCache.getProcessingById(seller.getProcessingId()).getCode());
        msg.setAttributeString(BillingMessage.PASSWORD, MD5.getHashString(dbCache.getProcessingById(seller.getProcessingId()).getPassword()));
        msg.setAttributeString(BillingMessage.LOGIN, dbCache.getProcessingById(seller.getProcessingId()).getLogin());

        return msg;
    }

    private boolean isProcessingRedefined(String processingCode) throws CacheException {
        return ((Set) ProcessingsCache.getInstance().get(configFile)).contains(processingCode);
    }

    public Task rollback(Task task, Throwable e) {
        return task;
    }

}

class ProcessingsCache extends FileCache {

    private static ProcessingsCache cache;

    private ProcessingsCache() {
        maxSize = 100;
        cleanCount = 10;
        cleanType = CleanAlgorithm.Type.CLEAN_N_SELDOM_ACCESSED;
    }

    public static ProcessingsCache getInstance() {
        if (cache == null)
            cache = new ProcessingsCache();

        return cache;
    }

    protected Object getRealObjectFromFile(InputStream is, String path) throws CacheException {
        ProcessingResolverDocument doc = getProcessingResolverDocument(is, path);

        //copy to set of processings codes
        Set<String> result = new HashSet<String>();
        for (Processings processings : doc.getProcessingResolver().getProcessingsArray())
            result.add(processings.getCode());

        return result;
    }

    private ProcessingResolverDocument getProcessingResolverDocument(InputStream is, String path) throws CacheException {
        try{
            return ProcessingResolverDocument.Factory.parse(is);
        }catch(Throwable e){
            throw new CacheException("err.processing_resolver.failed_to_parse_config", new String[]{ path }, e);
        }
    }
}
