package com.tmx.beng.httpsgate.dacard;

import com.tmx.util.queue.ActionQueueElement;
import com.tmx.beng.base.BillingMessage;
import com.tmx.beng.httpsgate.HttpsGateException;

/**
    Gate task. Initiated from gate via medium connection.
    Processed by asynchronous thread.
 */
public class DacardGateTask extends ActionQueueElement {
    private BillingMessage procesedMessage;
    private BasicDacardGate gate;

    public DacardGateTask(BillingMessage billingMessage, BasicDacardGate gate){
        this.gate = gate;
        this.procesedMessage = billingMessage;
    }

    public void run() {
        try{
            gate.runBillMessage(procesedMessage);
        }
        catch(DacardGateException e){
            gate.logger.error(e.getLocalizedMessage(), e);
        }
    }
}
