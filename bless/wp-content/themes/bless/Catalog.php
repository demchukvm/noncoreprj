<?php
/*
  Template Name: Catalog
 */
?>
<?php get_header();?>

<script type="text/javascript">
    jQuery(document).ready(function($) {
 
	$(".scroll").click(function(event){		
		event.preventDefault();
		$('html,body').animate({scrollTop:$(this.hash).offset().top}, 1000);
	});
        
        $('.scroll').click();
        
}); 
</script>

<div class="catalog_currblock">

	<div class="left">
		<img src="<?php bloginfo('template_url');?>/images/catalog-shape<?php if(isset($_GET['lang']) & $_GET['lang'] == 'en'){echo '-eng';}?>.gif" alt="Каталог BLESS">
	</div>
	
	<div class="right">
		<p><?php echo get_field('text_1');?></p>
	</div>

</div>

    <?php include 'blocks/catalog_list.php';?>




<?php get_footer(); ?>