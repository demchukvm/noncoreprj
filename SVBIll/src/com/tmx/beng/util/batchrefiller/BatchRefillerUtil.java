package com.tmx.beng.util.batchrefiller;

import com.tmx.util.InitException;
import com.tmx.util.MD5;
import com.tmx.util.Configuration;
import com.tmx.beng.httpsgate.EasySSLProtocolSocketFactory;
import com.tmx.beng.httpsgate.HttpsRequestException;

import java.util.List;
import java.util.ArrayList;
import java.util.Date;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.io.*;
import java.text.SimpleDateFormat;
import java.net.Socket;
import java.net.UnknownHostException;
import java.net.InetAddress;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.ConnectTimeoutException;
import org.apache.commons.httpclient.params.HttpConnectionParams;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.protocol.Protocol;
import org.apache.commons.httpclient.protocol.ProtocolSocketFactory;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import javax.net.SocketFactory;

/**

 */
public class BatchRefillerUtil {
    private String terminalsFile;
    private String paymentsFile;
    private String reportFile;
    private String terminalSn;
    private String sellerCode;
    private String processingCode;
    private String login;
    private String password;
    private String url;
    private String serviceCode;
    private int minSecInterval;
    private int maxSecInterval;
    private List terminals;
    private List payments;
    private Logger logger = Logger.getLogger("batch_refiller");
    private static final String DELIMITER = ";";

    public static void main(String[] args) throws InitException, Exception{
        BatchRefillerUtil util = new BatchRefillerUtil();
        util.init();
        util.run();
    }

    private void init() throws InitException{
        String tmxHome = System.getProperty("tmx.home");
        if (tmxHome == null)
            throw new InitException("System property 'tmx.home' is not set");

        //2. Initiate properties loading
        Configuration config = Configuration.getInstance();

        //3. Configure logging
        PropertyConfigurator.configure(config.getProperty("log_config", tmxHome + "/conf/log4j.properties"));
        
        //required props
        logger.info("Initialization...");
        
        terminalsFile = System.getProperty("terminalsFile");
        if(terminalsFile == null)
            throw new InitException("System property 'terminalsFile' is not specified");

        paymentsFile = System.getProperty("paymentsFile");
        if(paymentsFile == null)
            throw new InitException("System property 'paymentsFile' is not specified");

        reportFile = System.getProperty("reportFile");
        if(reportFile == null)
            throw new InitException("System property 'reportFile' is not specified");

        terminalSn = System.getProperty("terminalSn");
        if(terminalSn == null)
            throw new InitException("System property 'terminalSn' is not specified");

        sellerCode = System.getProperty("sellerCode");
        if(sellerCode == null)
            throw new InitException("System property 'sellerCode' is not specified");

        processingCode = System.getProperty("processingCode");
        if(processingCode == null)
            throw new InitException("System property 'processingCode' is not specified");

        login = System.getProperty("login");
        if(login == null)
            throw new InitException("System property 'login' is not specified");

        password = System.getProperty("password");
        if(password == null)
            throw new InitException("System property 'password' is not specified");
        password = MD5.getHashString(password);

        url = System.getProperty("url");
        if(url == null)
            throw new InitException("System property 'url' is not specified");

        serviceCode = System.getProperty("serviceCode");
        if(serviceCode == null)
            throw new InitException("System property 'serviceCode' is not specified");

        String minSecIntervalStr = System.getProperty("minSecInterval");
        if(minSecIntervalStr == null)
            throw new InitException("System property 'minSecInterval' is not specified");
        minSecInterval = Integer.parseInt(minSecIntervalStr);

        String maxSecIntervalStr = System.getProperty("maxSecInterval");
        if(maxSecIntervalStr == null)
            throw new InitException("System property 'maxSecInterval' is not specified");
        maxSecInterval = Integer.parseInt(maxSecIntervalStr);

        logger.info(" successful");
    }

    private void run() throws IOException{
        loadTerminals();
        loadPeyments();
        StringBuffer report = new StringBuffer();
        for(int i=0; i<payments.size(); i++){
            for(int j=0; j<terminals.size() && i<payments.size(); j++){
                TerminalWrapper terminal = (TerminalWrapper)terminals.get(j);
                Payment payment = (Payment)payments.get(i);
                String xmlPayment = buildPaymentXml(terminal, payment);
                try{
                    randomSleep();
                    String status = wirePayment(xmlPayment);
                    report.append(payment.msisdn).append(DELIMITER).
                            append(payment.amount).append(DELIMITER).
                            append(status).append(DELIMITER).
                            append(terminal.terminalName).append("\n");
                }
                catch(Throwable e){
                    logger.error("wire payement filed", e);
                }
                //shift to next payment. prevent double shifting on last terminal by outer 'for' loop
                if(j < terminals.size()-1)
                    i++;
            }
        }
        outputReport(report);
    }

    private void loadTerminals() throws IOException {
        FileInputStream fis = null;
        try{
            terminals = new ArrayList();
            BufferedReader reader = new BufferedReader(new FileReader(terminalsFile));
            String terminalString;
            while((terminalString = reader.readLine()) != null)
                terminals.add(new TerminalWrapper(terminalString));
        }
        finally{
            try{fis.close();}catch(Exception e){}
        }
    }

    private void loadPeyments() throws IOException{
        FileInputStream inputStream = null;
        try{
            payments = new ArrayList();
            BufferedReader reader = new BufferedReader(new FileReader(paymentsFile));
            String paymentString;
            while((paymentString = reader.readLine()) != null){
                String[] fields = paymentString.split(DELIMITER);
                payments.add(new Payment(fields[0], fields[1]));// TODO:(AN) if DELIMITER is present, but filds absent we have uncheked exception
            }
        } catch(Exception e) {
            e.printStackTrace();
        }
        finally{
            try {
                inputStream.close();
            } catch(Exception e){
                e.printStackTrace();
            }
        }
    }

    private String buildPaymentXml(TerminalWrapper termianal, Payment payment){
        StringBuffer xml = new StringBuffer();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd HH:mm:ss");
        Date now = new Date();
/*        xml.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n").
                append("<root xmlns=\"http://tmx.com/gate/csapi/v1/base/beans\">\n").
                append("<request protocol=\"1.0\">\n").
                append("\t<auth>\n").
                append("\t\t<terminalSn>").append(terminalSn).append("</terminalSn>\n").
                append("\t\t<sellerCode>").append(sellerCode).append("</sellerCode>\n").
                append("\t\t<processingCode>").append(processingCode).append("</processingCode>\n").
                append("\t\t<login>").append(login).append("</login>\n").
                append("\t\t<password>").append(password).append("</password>\n").
                append("\t\t<clientTime>").append(sdf.format(now)).append("</clientTime>\n").
                append("\t</auth>\n").
                append("\t<operations>\n").
                append("\t\t<refillPayment>\n").
                append("\t\t\t<cliTransactionNum>").append(termianal.terminalName + "_" + payment.msisdn + sdf.format(now)).append("</cliTransactionNum>\n").
                append("\t\t\t<serviceCode>").append(serviceCode).append("</serviceCode>\n").
                append("\t\t\t<amount>").append(payment.amount).append("</amount>\n").
                append("\t\t\t<accountNumber>").append(payment.msisdn).append("</accountNumber>\n").
                append("\t\t</refillPayment>\n").
                append("\t</operations>\n").
                append("</request>\n").
                append("</root>\n"); */

        String tradePoint = terminalSn;
        if(serviceCode.equals("MTSExc"))
        {
            tradePoint = "UN_00004";
        }
        if(serviceCode.equals("KyivstarPoP"))
        {
            tradePoint = "local-01";    
        }

        xml.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n").
                append("<root xmlns=\"http://tmx.com/gate/csapi/v1/base/beans\">\n").
                append("<request protocol=\"1.0\">\n").
                append("\t<auth>\n").
                append("\t\t<terminalSn>").append(terminalSn).append("</terminalSn>\n").
                append("\t\t<sellerCode>").append(sellerCode).append("</sellerCode>\n").
                append("\t\t<processingCode>").append(processingCode).append("</processingCode>\n").
                append("\t\t<login>").append(login).append("</login>\n").
                append("\t\t<password>").append(password).append("</password>\n").
                append("\t\t<clientTime>").append(sdf.format(now)).append("</clientTime>\n").
                append("\t</auth>\n").
                append("\t<operations>\n").
                append("\t\t<refillPayment>\n").
                append("\t\t\t<cliTransactionNum>").append(termianal.terminalName + "_" + payment.msisdn + sdf.format(now)).append("</cliTransactionNum>\n").
                append("\t\t\t<serviceCode>").append(serviceCode).append("</serviceCode>\n").
                append("\t\t\t<amount>").append(payment.amount).append("</amount>\n").
                append("\t\t\t<accountNumber>").append(payment.msisdn).append("</accountNumber>\n").
                append("\t\t\t<tradePoint>").append(tradePoint).append("</tradePoint>\n").
                append("\t\t</refillPayment>\n").
                append("\t</operations>\n").
                append("</request>\n").
                append("</root>\n");

        return xml.toString();
    }                                                     

    private String parseResponse(String responseXml){
        if(responseXml == null)
            return "-1";
        Pattern pattern = Pattern.compile(".*<statusCode>(.*)</statusCode>.*");
        responseXml = responseXml.replaceAll("\n", "");
        responseXml = responseXml.replaceAll("\r", "");
        responseXml = responseXml.replaceAll("\t", "");
        Matcher matcher = pattern.matcher(responseXml);
        if(matcher.matches()){
            return matcher.group(1);
        }
        else
            return "-2";
    }

    /** return status */
    private String wirePayment(String requestXml) throws IOException, RefillException {
        logger.debug(requestXml);
        PostMethod postMethod = new PostMethod(url);
        postMethod.setParameter("requestXml", requestXml);
        HttpClient httpClient = new HttpClient();
        Protocol.registerProtocol("http", new Protocol("http", new SimpleSocketFactory(), 31000));

        httpClient.executeMethod(postMethod);
        if(postMethod.getStatusCode() != 200){
            throw new RefillException("Incorrect response code from server " + postMethod.getStatusCode());
        }
        String strResp = postMethod.getResponseBodyAsString();
        logger.debug(strResp);
        return parseResponse(strResp);
    }

    private void randomSleep(){
        Random random = new Random();
        int randomDelay = random.nextInt(maxSecInterval - minSecInterval);
        try{
            Thread.sleep((minSecInterval + randomDelay)*1000);
        }
        catch(InterruptedException e){
            /** ignore */
        }
    }

    private void outputReport(StringBuffer report) throws IOException{
        FileOutputStream fos = null;
        try{
            fos = new FileOutputStream(reportFile);
            fos.write(report.toString().getBytes());
            fos.flush();
        }
        finally{
            try{fos.close();}catch(Exception e){}
        }
    }

    private class TerminalWrapper{
        private String terminalName;

        private TerminalWrapper(String terminalName){
            this.terminalName = terminalName;
        }
    }

    private class Payment{
        private String msisdn;
//        private Long amount;
        private Double amount;

        private Payment(String msisdn, String amount){
            this.msisdn = msisdn;
            this.amount = new Double(amount);
        }
    }

    private class RefillException extends Exception{
        public RefillException(String msg){
            super(msg);
        }
    }

    private class SimpleSocketFactory implements ProtocolSocketFactory {

        public Socket createSocket(String host, int port, InetAddress localAddress, int localPort) throws IOException, UnknownHostException {
            return new Socket(host, port, localAddress, localPort);
        }

        public Socket createSocket(String host, int port, InetAddress localAddress, int localPort, HttpConnectionParams httpConnectionParams) throws IOException, UnknownHostException, ConnectTimeoutException {
            return new Socket(host, port, localAddress, localPort);
        }

        public Socket createSocket(String host, int port) throws IOException, UnknownHostException {
            return new Socket(host, port);
        }
    }
    
}
