package com.tmx.beng.iso8583Gate.mapper;

import com.tmx.beng.iso8583Gate.pdu.RuntimeMessage;
import com.tmx.beng.iso8583Gate.pdu.IncorrectFormatException;
import com.tmx.beng.base.BillingMessage;

/**
    Mapper based on BasicMapper with additional mapping for
    'service' and 'nominal' parameters
 */
public class ExtendedBillingMapper extends BasicBillingMapper implements BillingMapper{
    private CrossParameterMapping crossParameterMapping;
    
    /** to create ExtendedBillingMapper instance from BillHelperMetadata */
    ExtendedBillingMapper(RequestMapping requestMapping,
                          ParameterMapping paramMapping,
                          ErrorMapping errorMapping,
                          AmountDenomination amountDenomination,
                          CrossParameterMapping crossParameterMapping){
        super(requestMapping, paramMapping, errorMapping, amountDenomination);
        this.crossParameterMapping = crossParameterMapping;
    }

    /** Resolve value through both basic and extended mappings.
     * @param runMsg - message to get original param value from
     * @param originalParamName the name of parameter produced after first (basic) mapping.
     * @throws IncorrectFormatException on incorrect runtime message format. */
    protected String resolveMappedParamValue(RuntimeMessage runMsg, String originalParamName) throws IncorrectFormatException {
        String crossMappedVal = null;
        CrossParam crossParam = crossParameterMapping.getExtValueByOriginalValue(originalParamName);
        if(crossParam == null){
            //no param entry found in extended mapping, so try to get param mapping through basic mapping
            crossMappedVal = super.resolveMappedParamValue(runMsg, originalParamName);
        }
        else{
            //resolve through the basic mapping
            String mappedSourceParamValue = super.resolveMappedParamValue(runMsg, crossParam.getSourceParamName());
            if(mappedSourceParamValue != null)
                mappedSourceParamValue = crossParam.getMappedValue(mappedSourceParamValue);

            //get value from extended mapping
            crossMappedVal = crossParam.getMappedValue(mappedSourceParamValue);
        }
        return crossMappedVal;
    }


}
