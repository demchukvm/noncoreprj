package com.tmx.engines.graphengine.util;

//for Commons math
//import org.apache.commons.math.analysis.*;
import org.apache.commons.math.MathException;
//for SSCC lib
//import ru.sscc.spline.polynomial.POddSplineCreator;
//import ru.sscc.spline.polynomial.OddSplinePreparator;
//import ru.sscc.spline.Spline;
//import ru.sscc.util.data.DoubleVector;
//import ru.sscc.util.CalculatingException;

import java.util.Arrays;
import java.util.ArrayList;
import java.util.List;

/**
Math suppport for graphs
 */
public class MathTransform {
    /** Spline difference order. Defines the spline degree as: (degree=2*order-1) */
    private final int SPLINE_DIFFERENCE_ORDER = 2;

    /** Creates initial function to be approximated */
    public Function createInitialFunction(double[] arg, double[] val){
        return new Function(arg, val);
    }

    /** @param initialFunction function to be approximated
     *  @param toPointsCount approximate function to given points count
        @return Function approximated function */
    public Function approximate(Function initialFunction, int toPointsCount) throws MathException {
        initialFunction = resolveIssues(initialFunction);
        //Approximation is impossible for function has less then 3 datapoints
        if(initialFunction.getPointsCount() < getMinApproximationPoints())
            return initialFunction;

        /** Works with simple value avaluation */
        initialFunction.calculateCoeffs();


        /** Works withs SSCC lib
        Spline spline = null;
        POddSplineCreator pOddSplineCreator = new POddSplineCreator(SPLINE_DIFFERENCE_ORDER, new DoubleVector(initialFunction.getArgument()));
        DoubleVector coef = new DoubleVector(new double[initialFunction.getArgument().length]);
        DoubleVector residual = new DoubleVector(new double[initialFunction.getArgument().length]);
        OddSplinePreparator oddSplinePreparator = pOddSplineCreator.getPreparator();
        try{

//epsilon = 275
//weights = 10 000

//epsilon = 100
//weights = 80 000

//epsilon = 1
//weights = 20 000 000

            double epsilon = 5000;
//            oddSplinePreparator.prepareSolver(Double.POSITIVE_INFINITY);
//            oddSplinePreparator.selectAlpha(
//                    new DoubleVector(initialFunction.getValue()),
//                    epsilon,
//                    1,//alpha
//                    coef,
//                    residual
//                    );

//            oddSplinePreparator.solve(new DoubleVector(initialFunction.getValue()), coef, residual);
//double[] weights = new double[initialFunction.getArgument().length];
//Arrays.fill(weights, 20000000);
//            spline = POddSplineCreator.createSpline(SPLINE_DIFFERENCE_ORDER, initialFunction.getArgument(), initialFunction.getValue(), epsilon, weights);
            spline = POddSplineCreator.createSpline(SPLINE_DIFFERENCE_ORDER, initialFunction.getArgument(), initialFunction.getValue(), epsilon);
        }
        catch(CalculatingException e){
            throw new MathException(e);
        }*/

        /** RM: works with Apache Math
        UnivariateRealInterpolator interpolator = new SplineInterpolator(); //DividedDifferenceInterpolator(); //NevilleInterpolator();//new SplineInterpolator();
        UnivariateRealFunction univariateRealFunction = interpolator.interpolate(initialFunction.getArgument(), initialFunction.getValue());
        */

        double[] args = initialFunction.getArgument();
        Arrays.sort(args);
        double minArg = args[0];
        double maxArg = args[args.length-1];
        double argStep = (maxArg - minArg)/(toPointsCount-1);// intervals = points - 1
        Function apprFunction = new Function(toPointsCount);

        double apprArg = minArg;
        double apprVal;
        for(int i=0; i<toPointsCount-1; i++){
            /** RM: works with Apache Math
             * apprVal = univariateRealFunction.value(apprArg);
             */

            /** RM: Works with SSCC lib
            apprVal = spline.value(apprArg);
            */

            /** RM: Works with simple value evaluation */
            apprVal = initialFunction.getApproximatedValue(apprArg);

            apprFunction.putPoint(i, apprArg, apprVal);
            apprArg = apprArg + argStep;
        }
        return apprFunction;
    }

    /** Preprocess function by resolving possible issues */
    private Function resolveIssues(Function function) throws MathException{
        if(function == null || function.getValue() == null || function.getArgument() == null)
            throw new MathException("A processed function, its arguments or values are null");

        List args = new ArrayList();
        List vals = new ArrayList();
        int resolvedIdx = -1;

        for(int i = 0; i < function.getPointsCount(); i++){
            if(resolvedIdx == -1 || function.getArgument()[i] > ((Double)args.get(resolvedIdx)).doubleValue()){
                resolvedIdx++;
                args.add(resolvedIdx, new Double(function.getArgument()[i]));
                vals.add(resolvedIdx, new Double(function.getValue()[i]));
            }
            else if(args.get(resolvedIdx) != null && function.getArgument()[i] == ((Double)args.get(resolvedIdx)).doubleValue()){
                //take the argument with maximum value
                if(vals.get(resolvedIdx) != null && ((Double)vals.get(resolvedIdx)).doubleValue() < function.getValue()[i]){
                    //replace
                    args.set(resolvedIdx, new Double(function.getArgument()[i]));
                    vals.set(resolvedIdx, new Double(function.getValue()[i]));
                }
                //else ignore current element
            }
            else{
                throw new MathException("Dataset of arguments is not strongly increased. See point #"+i);
            }
        }
        double[] argsArray = new double[args.size()];
        double[] valsArray = new double[vals.size()];
        for(int i=0; i<args.size(); i++){
            argsArray[i] = ((Double)args.get(i)).doubleValue();
            valsArray[i] = ((Double)vals.get(i)).doubleValue();
        }
        return new Function(argsArray, valsArray);
    }

    private int getMinApproximationPoints(){
        return 2*SPLINE_DIFFERENCE_ORDER - 1;
    }

    public class Function{
        private double[] argument = null;
        private double[] value = null;
        private double[] k_coeff = null;
        private double[] b_coeff = null;

        private Function(double[] arg, double[] val){
            argument = arg;
            value = val;
        }

        private Function(int points){
            argument = new double[points];
            value = new double[points];
        }

        private void putPoint(int pointIdx, double arg, double val){
            argument[pointIdx] = arg;
            value[pointIdx] = val;
        }

        public double[] getArgument() {
            return argument;
        }

        public void setArgument(double[] argument) {
            this.argument = argument;
        }

        public double[] getValue() {
            return value;
        }

        public void setValue(double[] value) {
            this.value = value;
        }

        public int getPointsCount(){
            return (argument != null) ? argument.length : 0;
        }

        private void calculateCoeffs(){
            k_coeff = new double[argument.length - 1];
            b_coeff = new double[argument.length - 1];
            for(int i=0; i<argument.length-1; i++){
                k_coeff[i] = (value[i+1] - value[i])/(argument[i+1] - argument[i]);
                b_coeff[i] = value[i] - k_coeff[i]*argument[i];
            }
        }

        private double getApproximatedValue(double arg){
            //identify interval
            int intrvalIdx = -1;
            for(int i=0; i < argument.length; i++){
                if(i == 0 && arg < argument[i]){
                    intrvalIdx = -1;//out of arguments definition area
                    break;
                }
                else if(i > 0 && arg < argument[i]){
                    intrvalIdx = i-1;
                    break;
                }
                else if(i == argument.length-1 && arg > argument[i-1] && arg < argument[i]){
                    intrvalIdx = i-1;
                    break;
                }
                else if(i==argument.length && arg > argument[i]){
                    intrvalIdx = -1;
                    break;
                }
            }
            //calculate value
            return (intrvalIdx == -1) ? 0 : k_coeff[intrvalIdx]*arg + b_coeff[intrvalIdx];
        }

    }

}
