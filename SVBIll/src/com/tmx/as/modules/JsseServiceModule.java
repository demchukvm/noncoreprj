package com.tmx.as.modules;

import com.tmx.as.base.HibernateSessionFactoryProvider;
import com.tmx.as.jsse.JsseSimpleService;
import com.tmx.util.InitException;

import java.util.Properties;

/**
 * Created by IntelliJ IDEA.
 * User: shake
 * Date: 25.06.2007
 * Time: 15:56:19
 */
public class JsseServiceModule extends AbstractModule{

    public void start(Properties props)throws ModuleException{
        try{
            JsseSimpleService.getInstance().reload();
        }catch(InitException e){
            throw new ModuleException(e.getLocalizedMessage());
        }
    }

    public void shutdownGraceful(Properties props)throws ModuleException{
    }

    public void shutdownAbort(Properties props)throws ModuleException{
    }
}
