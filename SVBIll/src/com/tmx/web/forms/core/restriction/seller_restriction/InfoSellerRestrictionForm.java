package com.tmx.web.forms.core.restriction.seller_restriction;

import com.tmx.web.forms.core.restriction.AbstractInfoRestrictionForm;
import com.tmx.web.forms.core.base.AbstractFormFactory;
import com.tmx.web.controls.ScrollBox;
import com.tmx.web.controls.PopulateUtil;
import com.tmx.as.entities.bill.restriction.AbstractRestriction;
import com.tmx.as.entities.bill.restriction.SellerRestriction;
import com.tmx.as.entities.bill.seller.Seller;
import com.tmx.as.entities.general.user.User;
import com.tmx.as.exceptions.DatabaseException;

public class InfoSellerRestrictionForm extends AbstractInfoRestrictionForm {

    private ScrollBox<Seller> scrollBoxSeller;

    protected void init() {
        super.init();
        initButtons();
        initTables();
        initScrollBoxes();
        initBrowseBoxes();
    }

    private void initBrowseBoxes() {
        getBrowseBoxFunctionTypes().setAction("/core/restriction/seller_restriction/browseSellerFuncTypesAction");
    }

    private void initScrollBoxes() {
        scrollBoxSeller = new ScrollBox<Seller>() {
            protected String getKey(Seller dataListElement) {
                return dataListElement.getSellerId().toString();
            }
            protected String getValue(Seller dataListElement) {
                return dataListElement.getName();
            }
        };
        scrollBoxSeller.setNullable(true);
        scrollBoxSeller.setMandatory(true);
    }

    private void initButtons(){
        getOnSaveButton().setAction("/core/restriction/seller_restriction/infoSellerRestriction");
        getOnApplyButton().setAction("/core/restriction/seller_restriction/infoSellerRestriction");
        getOnCancelButton().setAction("/core/restriction/seller_restriction/manageSellerRestrictions");

        getOnRefreshParamButton().setAction("/core/restriction/seller_restriction/infoSellerRestriction");
        getOnCreateParamButton().setAction("/core/restriction/seller_restriction/infoSellerRestriction");
        getOnEditParamButton().setAction("/core/restriction/seller_restriction/sellerRestrictionParameter");
        getOnDeleteParamButton().setAction("/core/restriction/seller_restriction/sellerRestrictionParameter");
    }

    private void initTables(){
        getFuncParametrsTable().setRefreshActionName("/core/restriction/seller_restriction/infoSellerRestriction");
        getDefaultFuncParametrsTable().setRefreshActionName("/core/restriction/seller_restriction/infoSellerRestriction");
    }

    protected AbstractRestriction populateFromSpecificControls() {
        SellerRestriction restriction = new SellerRestriction();

        Seller seller = new Seller();
        seller.setSellerId(PopulateUtil.getLongKey(scrollBoxSeller));
        restriction.setSeller(seller);
        
        return restriction;
    }

    protected void bindSpecificData(AbstractRestriction restriction) {
        SellerRestriction rest = (SellerRestriction)restriction;
        scrollBoxSeller.setSelectedKey(PopulateUtil.getString(rest.getSellerId()));
    }

    protected void bindSpecificData(AbstractRestriction restrict, User loggedInUser) throws DatabaseException {
        SellerRestriction sellerRestriction = (SellerRestriction) restrict;
        AbstractFormFactory.getInstance(loggedInUser).setUpForm(this);
        if(sellerRestriction.getSeller()!=null && sellerRestriction.getSeller().getSellerId()!=null)//if seller is present
            scrollBoxSeller.setSelectedKey(sellerRestriction.getSeller().getSellerId().toString());
        scrollBoxSeller.setReadonly(true);
    }
    public ScrollBox getScrollBoxSeller() {
        return scrollBoxSeller;
    }

    public void setScrollBoxSeller(ScrollBox scrollBoxSeller) {
        this.scrollBoxSeller = scrollBoxSeller;
    }
}
