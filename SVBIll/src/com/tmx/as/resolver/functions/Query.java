package com.tmx.as.resolver.functions;

import com.tmx.as.base.FilterWrapper;
import com.tmx.as.base.OrderWrapper;
import com.tmx.as.base.ProjectionWrapper;
import com.tmx.as.base.criterion.CriterionWrapper;
import com.tmx.as.resolver.FunctionSyntaxException;
import com.tmx.as.resolver.ResolverException;

import java.util.Vector;

public class Query implements Function{
    private com.tmx.as.resolver.Query query=new com.tmx.as.resolver.Query();
    private Vector attrs=new Vector();
    private Vector filters=new Vector();
    private Vector criterions =new Vector();
    private Vector orders=new Vector();
    private ProjectionWrapper[] projections=new ProjectionWrapper[0];

    public void set(int paramIndex, String paramName, Object paramValue) throws FunctionSyntaxException {
        if(paramName.equalsIgnoreCase("table_attr")){
            attrs.add(paramValue);return;
        }
        if(paramName.equalsIgnoreCase("filter")){
            filters.add(paramValue);return;
        }
        if(paramName.equalsIgnoreCase("criterion")){
            criterions.add(paramValue);return;
        }
        if(paramName.equalsIgnoreCase("order")){
            orders.add(paramValue);return;
        }
        if(paramName.equalsIgnoreCase("SUM")||
           paramName.equalsIgnoreCase("COUNT")||
           paramName.equalsIgnoreCase("MIN")||
           paramName.equalsIgnoreCase("MAX")||
           paramName.equalsIgnoreCase("AVG")||
           paramName.equalsIgnoreCase("PROPERTY")||
           paramName.equalsIgnoreCase("GROUPBY")){
           ProjectionWrapper.appendToProjections(projections,(ProjectionWrapper)paramValue);
        }
        try {
            query.set(paramName,(String)paramValue);
        } catch (ResolverException e) {
            throw new FunctionSyntaxException("projection error", e);
        }
    }

    public Object returnValue() throws FunctionSyntaxException {

        String[] attrsArr=new String[attrs.size()];
        for (int i = 0; i < attrs.size(); i++) {
            String attr =  (String)attrs.elementAt(i);
            attrsArr[i]=attr;
        }
        query.setTableAttrs(attrsArr);


        FilterWrapper[] filtersArr=new FilterWrapper[filters.size()];
        for (int i = 0; i < filters.size(); i++) {
            FilterWrapper filter=(FilterWrapper)filters.elementAt(i);
            filtersArr[i]=filter;
        }
        query.setFilters(filtersArr);

        CriterionWrapper[] criterionsArr=new CriterionWrapper[criterions.size()];
        for (int i = 0; i < criterions.size(); i++) {
            CriterionWrapper criterion = (CriterionWrapper)criterions.elementAt(i);
            criterionsArr[i]=criterion ;
        }
        query.setCriterions(criterionsArr);

        OrderWrapper[] ordersArr=new OrderWrapper[orders.size()];
        for (int i = 0; i < orders.size(); i++) {
            OrderWrapper order=(OrderWrapper)orders.elementAt(i);
            ordersArr[i]=order;
        }
        
        query.setProjectionWrappers(projections);
        query.setOrders(ordersArr);

        return query;
    }
}