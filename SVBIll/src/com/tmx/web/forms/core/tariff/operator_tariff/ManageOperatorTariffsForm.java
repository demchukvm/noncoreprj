package com.tmx.web.forms.core.tariff.operator_tariff;

import com.tmx.as.blogic.OperatorManager;
import com.tmx.as.entities.bill.operator.Operator;
import com.tmx.as.entities.general.user.User;
import com.tmx.as.exceptions.DatabaseException;
import com.tmx.web.controls.ScrollBox;
import com.tmx.web.controls.table.Parameter;
import com.tmx.web.forms.core.tariff.AbstractManageTariffsForm;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;


public class ManageOperatorTariffsForm extends AbstractManageTariffsForm {

    protected void init(){
        super.init();
        initTariffsTable();
        initButtons();
        initFilters();
    }

    public void bindOperators(List operators) throws DatabaseException {
        ScrollBox operatorsScrollBox = (ScrollBox) getTariffsTable().getParameter("by_operator_id", "operator_id").getControl();

        Collections.sort(operators, new Comparator<Operator>()
        {
          public int compare(Operator op1, Operator op2) {
            return op1.getName().compareTo(op2.getName());
          }
        });

        operatorsScrollBox.bind(operators);
    }

    private void initTariffsTable() {
        getTariffsTable().loadQuery("query(query_type=retrieve_all," +
                "entity_class=com.tmx.as.entities.bill.tariff.OperatorTariff," +
                "table_attr=functionInstance.functionType," +
                "table_attr=operator," +
                "order(name=operatorId,type=asc)," +
                "page_size=15" +
                ")");

        getTariffsTable().setRefreshActionName("/core/tariff/operator_tariff/manageOperatorTariffs");
    }

    private void initFilters(){
        getTariffsTable().addParameterToFilter("by_operator_id", new Parameter("operator_id",initOperatorsScrollBoxFilter()));    
    }

    private ScrollBox initOperatorsScrollBoxFilter(){
        ScrollBox operatorsScrollBox = new ScrollBox(){
            protected String getKey(Object dataListElement) {
                return ((Operator)dataListElement).getId().toString();
            }

            protected String getValue(Object dataListElement) {
                return ((Operator)dataListElement).getName();
            }
        };
        operatorsScrollBox.setNullable(true);
        return operatorsScrollBox;
    }

    private void initButtons(){
        getOnCreateButton().setAction("/core/tariff/operator_tariff/manageOperatorTariffs");
        getOnEditButton().setAction("/core/tariff/operator_tariff/infoOperatorTariff");
        getOnDeleteButton().setAction("/core/tariff/operator_tariff/infoOperatorTariff");
    }
    
}
