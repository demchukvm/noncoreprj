package com.tmx.web.forms.core.settings.personal;

import com.tmx.as.blogic.UserManager;
import com.tmx.as.entities.bill.seller.Seller;
import com.tmx.as.entities.general.organization.Organization;
import com.tmx.as.entities.general.role.Role;
import com.tmx.as.entities.general.user.User;
import com.tmx.as.exceptions.DatabaseException;
import com.tmx.web.base.BasicActionForm;
import com.tmx.web.controls.Button;
import com.tmx.web.controls.EditBox;
import com.tmx.web.controls.PopulateUtil;
import com.tmx.web.controls.ScrollBox;

/**
 * Personal info
 */
public class PersonalInfoForm extends BasicActionForm {

    private EditBox userId;
    private EditBox login;
    private EditBox password;
    private EditBox password2;
    private EditBox firstName;
    private EditBox lastName;
    private ScrollBox selectRoles;
    private ScrollBox selectOrganisations;
    private ScrollBox selectSellers;
    private Button onResetButton;
    private Button onSaveButton;

    /**
     * Initialize form
     */
    protected void init() {
        initEditBoxes();
        initScrollBoxes();
        initButtons();
    }


    public void bindData(User user) throws DatabaseException {
        if(user != null){
            bindMainData(user);
            bindSelectRoles(user);
            bindSelectSellers(user);
            bindSelectOrganisations(user);
        }
    }

    public User populateFromControls() {
        User user = new User();

        assignMainData(user);
        assignRole(user);
        assignOrganization(user);
        assignSeller(user);

        return user;
    }

    private void assignOrganization(User user) {
        Organization organization = new Organization();
        organization.setOrganizationId(PopulateUtil.getLongKey(selectOrganisations));
        user.setOrganization(organization);
    }

    private void assignRole(User user) {
        Role role = new Role();
        role.setRoleId(PopulateUtil.getLongKey(selectRoles));
        user.setRole(role);
    }

    private void assignSeller(User user) {
        Seller seller = new Seller();
        seller.setSellerId(PopulateUtil.getLongKey(selectSellers));
        user.setSellerOwner(seller);
    }

    private void assignMainData(User user) {
        user.setUserId(userId.getValue() != null ? new Long(userId.getValue()) : null);//TODO: check condiotion
        user.setLogin(login.getValue());
        user.setLastName(lastName.getValue());
        user.setFirstName(firstName.getValue());
        user.setPassword(password.getValue());
    }

    private void bindSelectOrganisations(User user) throws DatabaseException {
        selectOrganisations.bind(new UserManager().getAllOrganizations());
        if (user.getOrganization() != null && user.getOrganization().getOrganizationId() != null) {
            selectOrganisations.setSelectedKey(user.getOrganization().getOrganizationId().toString());
        }
    }

    private void bindSelectRoles(User user) throws DatabaseException {
        selectRoles.bind(new UserManager().getAllRoles());
        if (user.getRole() != null && user.getRole().getRoleId() != null) {
            selectRoles.setSelectedKey(user.getRole().getRoleId().toString());
        }
    }

    private void bindSelectSellers(User user) throws DatabaseException {
        selectSellers.bind(new UserManager().getAllSellers());
        if (user.getSellerOwner() != null && user.getSellerOwner().getSellerId() != null) {
            selectSellers.setSelectedKey(user.getSellerOwner().getSellerId().toString());
        }
    }

    private void initScrollBoxes() {
        selectOrganisations = new ScrollBox() {
            protected String getKey(Object dataListElement) {
                return ((Organization) dataListElement).getOrganizationId().toString();
            }

            protected String getValue(Object dataListElement) {
                return ((Organization) dataListElement).getName();
            }
        };
        selectOrganisations.setReadonly(true);
        selectRoles = new ScrollBox() {
            protected String getKey(Object dataListElement) {
                return ((Role) dataListElement).getRoleId().toString();
            }

            protected String getValue(Object dataListElement) {
                return ((Role) dataListElement).getRoleName();
            }
        };
        selectRoles.setReadonly(true);
        selectSellers = new ScrollBox() {
            protected String getKey(Object dataListElement) {
                return ((Seller)dataListElement).getSellerId().toString();
            }

            protected String getValue(Object dataListElement) {
                return ((Seller)dataListElement).getName();
            }
        };
        selectSellers.setReadonly(true);
        selectSellers.setNullable(true);
    }

    private void initButtons() {
        onSaveButton = new Button();
        onSaveButton.setLabelKey("ctrl.button.label.save");
        onSaveButton.setAction("/core/settings/personal/personalInfo");
        onSaveButton.setCommand("save");

        onResetButton = new Button();
        onResetButton.setLabelKey("ctrl.button.label.reset");
    }

    private void initEditBoxes() {
        userId = new EditBox();
        login = new EditBox();
        login.setReadonly(true);
        password = new EditBox();
        password.setMandatory(true);
        password.addValidationRule(EditBox.createValidationRule_MIN_FIELD_LENGTH(8));
        password2 = new EditBox();
        password2.addValidationRule(EditBox.createValidationRule_PASSWD_FIELDS_MATCH(password));
        firstName = new EditBox();
        firstName.setMandatory(true);
        lastName = new EditBox();
        lastName.setMandatory(true);
    }

    private void bindMainData(User user) {
        userId.setValue(user.getUserId().toString());
        userId.setReadonly(true);
        login.setValue(user.getLogin());
        password.setValue(user.getPassword());
        password2.setValue(user.getPassword());
        lastName.setValue(user.getLastName());
        firstName.setValue(user.getFirstName());
    }

    //--------------------Properties

    public EditBox getUserId() {
        return userId;
    }

    public void setUserId(EditBox userId) {
        this.userId = userId;
    }

    public ScrollBox getSelectRoles() {
        return selectRoles;
    }

    public void setSelectRoles(ScrollBox selectRoles) {
        this.selectRoles = selectRoles;
    }

    public ScrollBox getSelectOrganisations() {
        return selectOrganisations;
    }

    public void setSelectOrganisations(ScrollBox selectOrganisations) {
        this.selectOrganisations = selectOrganisations;
    }

    public EditBox getLastName() {
        return lastName;
    }

    public void setLastName(EditBox lastName) {
        this.lastName = lastName;
    }

    public EditBox getLogin() {
        return login;
    }

    public void setLogin(EditBox login) {
        this.login = login;
    }

    public EditBox getPassword() {
        return password;
    }

    public void setPassword(EditBox password) {
        this.password = password;
    }

    public EditBox getFirstName() {
        return firstName;
    }

    public void setFirstName(EditBox firstName) {
        this.firstName = firstName;
    }

    public Button getOnResetButton() {
        return onResetButton;
    }

    public void setOnResetButton(Button onResetButton) {
        this.onResetButton = onResetButton;
    }

    public Button getOnSaveButton() {
        return onSaveButton;
    }

    public void setOnSaveButton(Button onSaveButton) {
        this.onSaveButton = onSaveButton;
    }

    public EditBox getPassword2() {
        return password2;
    }

    public void setPassword2(EditBox password2) {
        this.password2 = password2;
    }

    public ScrollBox getSelectSellers() {
        return selectSellers;
    }

    public void setSelectSellers(ScrollBox selectSellers) {
        this.selectSellers = selectSellers;
    }
}
