package com.tmx.beng.base.processors;

import com.tmx.beng.base.*;
import com.tmx.beng.base.cache.DBCache;
import com.tmx.as.base.EntityManager;
import com.tmx.as.base.FilterWrapper;
import com.tmx.as.entities.bill.request.Request;

/**
 Join to opened transaction.
 Used when
 (1) process responds from operators and 
 (2) status requests from client.
 */
public class JoinRequestProcessor extends TaskProcessor {

    public Task process0(Task task, DBCache dbCache, BillingMessageWritable msg) throws BillException{
        try{
            logger.debug("Join request...");
            logger.debug(DebugPipelineUtil.dumpBillingMessage(msg, new StringBuffer()));
            String requestNum = msg.getAttributeString(BillingMessage.REQUEST_ID);

            FilterWrapper by_reqnum = new FilterWrapper("by_reqnum");
            by_reqnum.setParameter("reqnum", requestNum);
            Request request = (Request)new EntityManager().RETRIEVE(Request.class, new FilterWrapper[]{ by_reqnum }, null);
            if(request == null){
                throw new BillException("err.bill.cant_find_request");
            }
            msg.setAttributeString(BillingMessage.BILL_TRANSACTION_NUM, request.getBillTransactionNum());

            task.getGlobalContext().getTransactionPool().joinTransaction(
                requestNum,
                task);
        }catch(BillException e){
            logger.error("join-to request not found: " + e.toString());
            msg.setStatusCode(StatusDictionary.JOINED_REQUEST_NOT_FOUND);
            throw e;
        }catch(Throwable e){
            logger.error("join-to request not found: " + e.toString());
            msg.setStatusCode(StatusDictionary.JOINED_REQUEST_NOT_FOUND);
            throw new BillException("err.bill.join_request_error");
        }
        return task;
    }
}
