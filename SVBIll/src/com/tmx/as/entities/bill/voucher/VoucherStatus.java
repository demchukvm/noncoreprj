package com.tmx.as.entities.bill.voucher;

import com.tmx.as.base.Entity;
import com.tmx.as.base.BasicEntity;

import java.io.Serializable;

/**
 * Created by IntelliJ IDEA.
 * User: informix
 * Date: 24.04.2007
 * Time: 15:29:05
 */
public class VoucherStatus extends BasicEntity {
    private Long voucherStatusId;
    private String name;
    private String description;

    public Long getVoucherStatusId() {
        return voucherStatusId;
    }

    public void setVoucherStatusId(Long voucherStatusId) {
        this.voucherStatusId = voucherStatusId;
    }

    public Serializable getId() {
        return voucherStatusId;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public int getValid() {
        return valid;
    }

    public void setValid(int valid) {
        this.valid=valid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
