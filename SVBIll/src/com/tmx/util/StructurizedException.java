package com.tmx.util;

import com.tmx.util.i18n.MessageResources;

import java.util.*;
import java.io.ByteArrayOutputStream;
import java.io.PrintWriter;

/**
 * Provides localization parametrization and detailing.
 * 1) Localization - the i18n error descriptions are retrieved from resource bundles by error key.
 * 2) Parametrization - actual parameters could be populated into the error message.
 * 3) Detailing - the causal excaption is held and could be provided for detailng.
 * */
public class StructurizedException extends Throwable {
    private String errKey = null;
    private String[] params = null;
    private Locale locale = null;

    public StructurizedException(String errKey, String[] params, Throwable cause, Locale locale){
        super(cause);
        this.errKey = errKey;
        this.params = params;
        this.locale = locale;
    }

    public StructurizedException(String msg, String[] params, Locale locale){
        this(msg, params, null, locale);
    }

    public StructurizedException(String msg, String[] params){
        this(msg, params, null, null);
    }


    public StructurizedException(String msg, Throwable cause, Locale locale){
        this(msg, null, cause, locale);
    }

    public StructurizedException(String msg, String[] params, Throwable cause){
        this(msg, params, cause, null);
    }

    public StructurizedException(String msg, Throwable cause){
        this(msg, null, cause, null);
    }

    public StructurizedException(String msg, Locale locale){
        this(msg, null, null, locale);
    }

    public StructurizedException(String msg){
        this(msg, null, null, null);
    }

    /** Returns locilized messages as String from resource properties using <code>locale</code>,
     * <code>errKey</code> and <code>params</code> from this instance of <code>StructurizedException</code>.
     * The resource bundle "errors" is used.
    */
    public String getLocalizedMessage(){
        return getLocalizedMessage(locale);
    }


/*    public String getLocalizedMessage(Locale locale){
        ResourceService resService = ApplicationEnvironment.getResourceService();
        if(resService == null){
            //Thus ResourceService is unavailable, then return just errKey without localization
            return errKey;
        }
        else{
            String msg = resService.getMessage(locale, "errors", errKey, params);
            if(msg == null)
                return errKey;
            else
                return msg;
        }
    }*/


    public String getLocalizedMessage(Locale locale){
        return MessageResources.getInstance().getOwnedLocalizedMessage(this.getClass(), getMessageBundleName(), locale, errKey, params);
    }

    /** Returns ordered list of causes */
    public List getCauses(){
        List causes = new ArrayList();
        Throwable cause = this.getCause();
        Throwable prevCause = null;
        while(cause!= null && !cause.equals(prevCause)){
            causes.add(cause);
            prevCause = cause;
            cause = cause.getCause();
        }
        return causes;
    }


    /** Defualt constructor. Do not use it. */
    public StructurizedException(){
        this("err.undefined", null, null, Locale.getDefault());
    }


    public String getKey(){
        return errKey;
    }

    public String[] getParams(){
        return params;
    }

    /** You can override this name in successors */
    protected String getMessageBundleName(){
        return "errors";
    }

    public String getTraceDump(){
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(getLocalizedMessage());
        stringBuffer.append("\n\n");
        Iterator stackTraces = Arrays.asList(getStackTrace()).iterator();
        while(stackTraces.hasNext()){
            stringBuffer.append(stackTraces.next().toString());
            stringBuffer.append("\n");
        }
        Iterator iterator = getCauses().iterator();
        while(iterator.hasNext()){
            stringBuffer.append("-------------------------------------------------\n");
            stringBuffer.append("Cause: ");
            Throwable cause = (Throwable)iterator.next();
            if(cause instanceof StructurizedException){
                stringBuffer.append(cause.getLocalizedMessage());
            }else{
                stringBuffer.append(cause.toString());
            }
            stringBuffer.append("\n\n");
            stackTraces = Arrays.asList(cause.getStackTrace()).iterator();
            while(stackTraces.hasNext()){
                stringBuffer.append(stackTraces.next().toString());
                stringBuffer.append("\n");
            }
        }

        return stringBuffer.toString();
    }

    public static String getStackTrace(Throwable e){
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        PrintWriter pw = new PrintWriter(os);
        e.printStackTrace(pw);
        pw.close();
        return os.toString();
    }
}
